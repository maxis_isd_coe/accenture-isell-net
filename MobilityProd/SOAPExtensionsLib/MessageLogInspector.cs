﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Configuration;
using System.Xml;
using System.Configuration;
using System.IO;

namespace SOAPExtensionsLib
{
    public class MessageLogInspector : IClientMessageInspector, IDispatchMessageInspector
    {
        public string Request { get; set; }
        public string Response { get; set; }

        public event EventHandler<InspectorEventArgs> RaiseSendingReply;
        public event EventHandler<InspectorEventArgs> RaiseRequestReceived;
        public event EventHandler<InspectorEventArgs> RaiseReceiveReply;
        public event EventHandler<InspectorEventArgs> RaiseSendingRequest;

        private XmlDocument MessageToXML(string strMessage)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(strMessage);
            return xmlDoc;
        }
        private string XMLDocToString(XmlDocument xmlDoc)
        {
            StringWriter writer = new StringWriter();
            XmlTextWriter xmlWriter = new XmlTextWriter(writer);
            xmlWriter.Formatting = Formatting.None;
            xmlDoc.WriteTo(xmlWriter);
            xmlWriter.Flush();
            return writer.ToString();
        }
        string FormatXMLContent(string xmlText)
        {
            int xmlTxtIndexOf = xmlText.IndexOf("<s:Body");
            if (xmlTxtIndexOf >= 0)
            {
                xmlText = xmlText.Substring(xmlTxtIndexOf);
                xmlText = xmlText.Substring(xmlText.IndexOf('>') + 1);
            }
            xmlTxtIndexOf = xmlText.IndexOf("</s:Body");
            if (xmlTxtIndexOf >= 0)
                xmlText = xmlText.Substring(0, xmlTxtIndexOf);
            return xmlText;
        }
        //unused methods
        //bool FilterCondition(string strMessage)
        //{
        //    return (!string.IsNullOrEmpty(strMessage) && strMessage.StartsWith("<s:Envelope"));
        //}

        private void LogSoapMessage(string strMessage, string Direction)
        {
            XmlDocument xmlDoc = MessageToXML(strMessage);
            string xmlText = XMLDocToString(xmlDoc);
            xmlText = FormatXMLContent(xmlText);
            //if (!xmlText.Equals("</s:Envelope>") && !xmlText.StartsWith("<Metadata"))
            //{
                using (var iSellDB = new iSellDBEntities())
                {
                    iSellDB.AddTologSOAPMessages(new logSOAPMessage()
                    {
                        XMLContent = xmlText,
                        Direction = Direction,
                        CreateDT = DateTime.Now
                    });

                    iSellDB.SaveChanges();
                }

                //MessageLogger.SoapMessageCreate(new SoapMessage()
                //{
                //    CreateDT = DateTime.Now,
                //    Direction = Direction,
                //    XMLContent = xmlText,
                //});
            //}
        }
        object IDispatchMessageInspector.AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            Request = request.ToString();
            //Logger.InfoFormat("Entering AfterReceiveRequest (InRq)", Request.Length);
            //if (FilterCondition(Request))
            //{
                try
                {
                    LogSoapMessage(Request, "InRq");
                    OnRaiseRequestReceived(Request);
                }
                catch (Exception ex)
                {
                    string errMsg = ex.Message;
                    if (ex.InnerException != null)
                        errMsg += ". " + ex.InnerException.Message;
                    //Logger.ErrorFormat("AfterReceiveRequest (InRq)", errMsg);
                }

            //}
            //Logger.InfoFormat("Exiting AfterReceiveRequest (InRq) ", Request.Length);
            return null;
        }
        void IDispatchMessageInspector.BeforeSendReply(ref Message reply, object correlationState)
        {
            Response = reply.ToString();

            //Logger.InfoFormat("Entering BeforeSendReply (OutRp)", Response.Length);
            //if (FilterCondition(Response))
            //{
                try
                {
                    LogSoapMessage(Response, "OutRp");
                    OnRaiseSendingReply(Response);
                }
                catch (Exception ex)
                {
                    string errMsg = ex.Message;
                    if (ex.InnerException != null)
                        errMsg += ". " + ex.InnerException.Message;
                    //Logger.ErrorFormat("BeforeSendReply (OutRp)", errMsg);
                }

            //}
            //Logger.InfoFormat("Exiting BeforeSendReply (OutRp)", Response.Length);
        }
        void IClientMessageInspector.AfterReceiveReply(ref Message reply, object correlationState)
        {
            Response = reply.ToString();

            //Logger.InfoFormat("Entering AfterReceiveReply (InRp)", Response.Length);
            //if (FilterCondition(Response))
            //{
                try
                {
                    LogSoapMessage(Response, "InRp");
                    OnRaiseReceiveReply(Response);
                }
                catch (Exception ex)
                {
                    string errMsg = ex.Message;
                    if (ex.InnerException != null)
                        errMsg += ". " + ex.InnerException.Message;
                    //Logger.ErrorFormat("AfterReceiveReply (InRp)", errMsg);
                }

            //}
            //Logger.InfoFormat("Exiting AfterReceiveReply (InRp)", Response.Length);
        }
        object IClientMessageInspector.BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            Request = request.ToString();

            //Logger.InfoFormat("Entering BeforeSendRequest (OutRq)", Request.Length);
            //if (FilterCondition(Request))
            //{
                try
                {
                    LogSoapMessage(Request, "OutRq");
                    OnRaiseSendingRequest(Request);
                }
                catch (Exception ex)
                {
                    string errMsg = ex.Message;
                    if (ex.InnerException != null)
                        errMsg += ". " + ex.InnerException.Message;
                    //Logger.ErrorFormat("BeforeSendRequest (OutRq)", errMsg);
                }

            //}
            //Logger.InfoFormat("Exiting BeforeSendRequest (OutRq)", Request.Length);
            return null;
        }

        protected void OnRaiseRequestReceived(string message)
        {
            EventHandler<InspectorEventArgs> handler = RaiseRequestReceived;

            if (handler != null)
            {
                handler(this, new InspectorEventArgs(message));
            }
        }
        protected void OnRaiseSendingReply(string message)
        {
            EventHandler<InspectorEventArgs> handler = RaiseSendingReply;

            if (handler != null)
            {
                handler(this, new InspectorEventArgs(message));
            }
        }

        protected void OnRaiseReceiveReply(string message)
        {
            EventHandler<InspectorEventArgs> handler = RaiseReceiveReply;

            if (handler != null)
            {
                handler(this, new InspectorEventArgs(message));
            }
        }

        protected void OnRaiseSendingRequest(string message)
        {
            EventHandler<InspectorEventArgs> handler = RaiseSendingRequest;

            if (handler != null)
            {
                handler(this, new InspectorEventArgs(message));
            }
        }
    }

    public class MessageLogBehavior : IEndpointBehavior, IServiceBehavior
    {
        private bool enabled;
        internal MessageLogBehavior(bool enabled)
        {
            this.enabled = enabled;
        }
        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }
        internal MessageLogBehavior() { }
        public void AddBindingParameters(ServiceEndpoint serviceEndpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters) { }
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            //If enable is not true in the config we do not apply the Parameter Inspector
            if (false == this.enabled) return;

            clientRuntime.MessageInspectors.Add(new MessageLogInspector());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            //If enable is not true in the config we do not apply the Parameter Inspector
            if (false == this.enabled) return;

            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new MessageLogInspector());
        }
        public void Validate(ServiceEndpoint serviceEndpoint) { }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
            return;
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            //If enable is not true in the config we do not apply the Parameter Inspector
            if (false == this.enabled) return;

            foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
            {
                foreach (EndpointDispatcher endpoint in dispatcher.Endpoints)
                {
                    endpoint.DispatchRuntime.MessageInspectors.Add(new MessageLogInspector());
                }
            }
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            return;
        }
    }

    public class MessageLogBehaviorSection : BehaviorExtensionElement
    {
        private const string EnabledAttributeName = "enabled";

        [ConfigurationProperty(EnabledAttributeName, DefaultValue = true, IsRequired = false)]
        public bool Enabled
        {
            get { return (bool)base[EnabledAttributeName]; }
            set { base[EnabledAttributeName] = value; }
        }
        protected override object CreateBehavior()
        {
            return new MessageLogBehavior(this.Enabled);
        }

        public override Type BehaviorType
        {
            get { return typeof(MessageLogBehavior); }
        }
    }

    public class InspectorEventArgs : EventArgs
    {
        public InspectorEventArgs(string message)
        {
            this.Message = message;
        }

        public string Message { get; set; }
    }
}
