﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.IO;
using System.IO.Compression;

namespace SNT.Utility
{

    public class Compress
    {
        /// <summary>
        /// Compresses the string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string Zip(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            var memoryStream = new MemoryStream();
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
            {
                gZipStream.Write(buffer, 0, buffer.Length);
            }

            memoryStream.Position = 0;

            var compressedData = new byte[memoryStream.Length];
            memoryStream.Read(compressedData, 0, compressedData.Length);

            var gZipBuffer = new byte[compressedData.Length + 4];
            Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
            return Convert.ToBase64String(gZipBuffer);
        }

        /// <summary>
        /// Decompresses the string.
        /// </summary>
        /// <param name="compressedText">The compressed text.</param>
        /// <returns></returns>
        public static string Unzip(string compressedText)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedText);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }
    }
    public static class CommonExtension
    {

        /// <summary>
        /// If DBNull or null value then return 0.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt(this object value)
        {
            if (value == null) return 0;
            if (value.GetType() == typeof(String) || (value.GetType() == typeof(Int32)))
            {
                int tempValue = 0;
                if (Int32.TryParse(value.ToString2(), out tempValue))
                    return Convert.IsDBNull(value) || value.ToString2() == "" ? 0 : Convert.ToInt32(value);
                else
                    return 0;
            }
            else
                return Convert.IsDBNull(value) || value.ToString2() == "" ? 0 : Convert.ToInt32(value);
        }

        /// <summary>
        /// If DBNull or null value then return 0.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Int64 ToLong(this object value)
        {
            return Convert.IsDBNull(value) || value.ToString2() == "" ? 0 : Convert.ToInt64(value.ToString2().Trim());
        }

        /// <summary>
        /// If DBNull or null value then return empty string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToString2(this object value)
        {
            return Convert.IsDBNull(value) || value == null ? "" : value.ToString();
        }

        public static string ToString(this object value)
        {
            return Convert.IsDBNull(value) || value == null ? "" : value.ToString();
        }
        /// <summary>
        /// Returns a copy of this string to lowercase. 
        /// If DBNull or null value then return empty string. 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToLowerStr(this object value)
        {
            return value.ToString2().ToLower();
        }

        /// <summary>
        /// If DBNull or null value then return 0.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double ToDouble(this object value)
        {
            //return Convert.IsDBNull(value) || value == null ? 0 : Convert.ToDouble(value);
            if (value != null && value.ToString() != string.Empty)
            {
                return Convert.IsDBNull(value) || value == null ? 0 : Convert.ToDouble(value);
            }
            else
            {
                return 0;
            }
        }

        public static string FormatCurrency(this double value, string format = "N2", string currencySign = "RM")
        {
            return currencySign + value.ToString(format);
        }

        public static bool ToBool(this object value)
        {
            return Convert.IsDBNull(value) || value == null ? false : Convert.ToBoolean(value);
        }

        public static bool? ToNullableBool(this object value)
        {
            if (value == null || Convert.IsDBNull(value))
                return null;

            return Convert.ToBoolean(value);
        }

        public static int? ToNullableInt(this object value)
        {
            if (value == null || Convert.IsDBNull(value))
                return null;
            else
                return Convert.ToInt32(value);
        }

        public static Decimal ToDecimal(this object value)
        {
            Decimal tempValue = 0;
            if (Decimal.TryParse(value.ToString2(), out tempValue))
                return Convert.IsDBNull(value) || value == null || value.ToString2() == string.Empty ? 0 : Convert.ToDecimal(value);
            return 0;
        }

        public static DateTime ToDateTime(this object value)
        {
            DateTime tempValue = new DateTime();
            tempValue = DateTime.MinValue;
            if (DateTime.TryParse(value.ToString2(), out tempValue))
                return Convert.IsDBNull(value) || value == null || string.IsNullOrEmpty(value.ToString2()) ? DateTime.MinValue : DateTime.ParseExact(value.ToString(), "yyyyMMdd", DateTimeFormatInfo.InvariantInfo);
            return DateTime.MinValue;
        }

        public static bool IsEmpty<T>(this IEnumerable<T> value)
        {
            if (value == null || value.Count() == 0)
                return true;

            return false;
        }

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }

        public static string ToFormatString(this DateTime? target, string format)
        {
            if (!target.HasValue)
                return "";

            return target.Value.ToString(format);
        }

        public static string DisplayValue(this string dt)
        {
            return string.IsNullOrEmpty(dt) ? "" : dt;
        }
    }
}
