﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SNT.Utility.Helper
{
    public static class ExceptionHelper
    {
        public static void GetAllExceptions(Exception ex, out string errMsg, out string stackTrace, bool concate = true)
        {
            errMsg = ex.Message;
            stackTrace = ex.StackTrace;
            Exception subEx = ex.InnerException;

            while (subEx != null)
            {
                if (concate)
                {
                    errMsg += Environment.NewLine + subEx.Message;
                    stackTrace += Environment.NewLine + subEx.StackTrace;
                }
                else
                {
                    errMsg = subEx.Message;
                    stackTrace = subEx.StackTrace;
                }
                subEx = subEx.InnerException;
            }

            return;
        }
    }
}
