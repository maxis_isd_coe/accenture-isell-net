﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Online.Registration.DAL.Models
{
    #region WelcomeEmail 
    //RST 06Dec2014 
    [DataContract]
    [Serializable]
    [Table("lnkRegWelcomeMail")]
    public class lnkRegWelcomeMail
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        public int RegID { get; set; }

        [DataMember]
        [StringLength(50)]
        public string ContactNo { get; set; }

        [DataMember]
        [StringLength(100)]
        public string EmailAddr { get; set; }

        [DataMember]
        [StringLength(400)]
        public string FullName { get; set; }

        [DataMember]
        public DateTime CreateDT { get; set; }
    }
    #endregion  

    #region Program

    [DataContract]
    [Serializable]
    [Table("refProgram")]
    public class Program : TimedEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Code *")]
        [Required]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Description *")]
        [StringLength(4000)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Outright Sales? *")]
        public bool IsOutrightSales { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Minimum Age *")]
        public int MinAge { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ProgramFind : FindBase
    {
        [DataMember]
        public DAL.Models.Program Program { get; set; }
        [DataMember]
        public DateTime? StartDateFrom { get; set; }
        [DataMember]
        public DateTime? StartDateTo { get; set; }
        [DataMember]
        public DateTime? EndDateFrom { get; set; }
        [DataMember]
        public DateTime? EndDateTo { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion 

    #region Bundle

    [DataContract]
    [Serializable]
    [Table("refBundle")]
    public class Bundle : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Code *")]
        [Required]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        [StringLength(20)]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        [StringLength(100)]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        [StringLength(4000)]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Minimum Package *")]
        [Range(0, 100)]
        public int MinPackage { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Program ID")]
        public int ProgramID { get; set; }

        [Association("refProgram", "ProgramID", "")]
        public virtual Program Program { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BundleFind : FindBase
    {
        [DataMember]
        public DAL.Models.Bundle Bundle { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region PackageType

    [DataContract]
    [Serializable]
    [Table("refPackageType")]
    public class PackageType : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Description *")]
        [StringLength(4000)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

    }

    [DataContract]
    [Serializable]
    public class PackageTypeFind : FindBase
    {
        [DataMember]
        public PackageType PackageType { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Package

    [DataContract]
    [Serializable]
    [Table("refPackage")]
    public class Package : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Description *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Mandatory? *")]
        public bool IsMandatory { get; set; }

        //[DataMember]
        //[Required]
        //[Display(Name = "Need Coverage? *")]
        //public bool NeedCoverage { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Min Component *")]
        [Range(0, 100)]
        public int MinComponent { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Min Model Group *")]
        [Range(0, 100)]
        public int MinModelGroup { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Package Type *")]
        public int PackageTypeID { get; set; }

        [Association("refPackageType", "PackageTypeID", "")]
        public virtual PackageType PackageType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageFind : FindBase
    {
        [DataMember]
        public Package Package { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public bool? IsMandatory { get; set; }
    }

    #endregion

    #region DeviceInformation

    [DataContract]
    [Serializable]
    [Table("lnkRegDeviceInfo")]
    public class DeviceInfo
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        public int RegId { get; set; }

        [DataMember]
        [Display(Name = "Installation Status *")]
        public string InstallationStatus { get; set; }

        [DataMember]

        [Display(Name = "Remarks for Installation status *")]
        public string RemarksForInstallationStatus { get; set; }

        [DataMember]

        [Display(Name = "Completed Date *")]
        public DateTime CompletedDate { get; set; }

        [DataMember]

        [Display(Name = "Completed Time *")]
        public string CompletedTime { get; set; }

        [DataMember]
        [Display(Name = "Duration of Installation *")]
        public string DurationOfInstallation { get; set; }

        [DataMember]

        [Display(Name = "Standard/Non-Standard *")]
        public string IsStandard { get; set; }

        [DataMember]
        [Display(Name = "Remarks Of Installation *")]
        public string RemarksOfInstallation { get; set; }

        [DataMember]

        [Display(Name = "Arrival Time For Maxis Installer *")]
        public DateTime? ArrivalTimeForMaxisInstaller { get; set; }

        [DataMember]

        [Display(Name = "Arrival Time For TM Installer *")]
        public DateTime? ArrivalTimeForTMInstaller { get; set; }

        [DataMember]

        [Display(Name = "ONT Serial Number *")]
        public string ONTSerialNumber { get; set; }

        [DataMember]

        [Display(Name = "ONT Password *")]
        public string ONTPassword { get; set; }

        [DataMember]

        [Display(Name = "RGW Serial Number *")]
        public string RGWSerialNumber { get; set; }

        [DataMember]

        [Display(Name = "Dect Phone Serial Number *")]
        public string DectPhoneSerialNumber { get; set; }

        [DataMember]

        [Display(Name = "MAC Address *")]
        public string MACAddress { get; set; }

        [DataMember]

        [Display(Name = "Download Speed *")]
        public string DownloadSpeed { get; set; }

        [DataMember]

        [Display(Name = "Download Upload Speed *")]
        public string DownloadUploadSpeed { get; set; }

        [DataMember]

        [Display(Name = "Extra Cable *")]
        public int ExtraCable { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Center *")]
        public string CenterId { get; set; }

        [DataMember]
        public string VOIPSalesID { get; set; }

        [DataMember]
        public string FTTHSalesID { get; set; }
    }




    #endregion

    #region ComponentType

    [DataContract]
    [Serializable]
    [Table("refComponentType")]
    public class ComponentType : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "Type ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Type Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Description *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentTypeFind : FindBase
    {
        [DataMember]
        public ComponentType ComponentType { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Component

    [DataContract]
    [Serializable]
    [Table("refComponent")]
    public class Component : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Description *")]
        [StringLength(4000)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Mandatory *")]
        public bool IsMandatory { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Min Model Group *")]
        [Range(0, 100)]
        public int MinModelGroup { get; set; }

        [DataMember]
        [Display(Name = "Value")]
        public int? PeriodValue { get; set; }

        [DataMember]
        [Display(Name = "GroupId")]
        public int? GroupId { get; set; }

        [DataMember]
        [Display(Name = "AliasName")]           //RST
        public string AliasName { get; set; }

        [DataMember]
        [Display(Name = "RestricttoKenan *")]
        public bool? RestricttoKenan { get; set; }

		[DataMember]
		[Required]
		[Display(Name = "Sharing Component *")]
		public bool IsSharing { get; set; }

		[DataMember]
		[Required]
		[Display(Name = "Data Booster *")]
		public bool VRCDataBooster { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Component Type *")]
        public int ComponentTypeID { get; set; }

        [Association("refComponentType", "ComponentTypeID", "")]
        public virtual ComponentType ComponentType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentFind : FindBase
    {
        [DataMember]
        public Component Component { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public bool? IsMandatory { get; set; }
    }

    #endregion

    #region PgmBdlPkgComponent

    [DataContract]
    [Serializable]
    [Table("lnkPgmBdlPkgComp")]
    public class PgmBdlPckComponent : TimedEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "PBPC ID")]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Parent *")]
        public int ParentID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Child *")]
        public int ChildID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Link Type *")]
        [StringLength(2)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string LinkType { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Description *")]
        [StringLength(4000)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Display(Name = "Plan Type")]
        [StringLength(2)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string PlanType { get; set; }

        [DataMember]
        [Display(Name = "KenanCode")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string KenanCode { get; set; }

        [DataMember]
        [Display(Name = "Alternate Kenan Code")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string AlternateKenanCode { get; set; }

        [DataMember]
        [Display(Name = "Price")]
        [Range(0, 100000)]
        public decimal Price { get; set; }

        [DataMember]
        [Display(Name = "Value")]
        public int? Value { get; set; }

        [DataMember]
        [Display(Name = "Filter Org Type")]
        public string FilterOrgType { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Need Provision? *")]
        public bool NeedProvision { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Need Activation? *")]
        public bool NeedActivation { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Need Coverage? *")]
        public bool NeedCoverage { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Mandatory? *")]
        public bool IsMandatory { get; set; }

        [DataMember]
        [Display(Name = "Retail Price")]
        [Range(0, 100000)]
        public float? RetailPrice { get; set; }

        [DataMember]
        [Display(Name = "Billing Code")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string BillingCode { get; set; }

        //Added by VLT on 04 June 2013 to get SpendLimt
        [DataMember]
        public bool? IsSpendLimit { get; set; }

        [DataMember]
        [Display(Name = "Contract Type")]
        [StringLength(10)]
        public string contract_type { get; set; }

        [DataMember]
        public string ModelId { get; set; }

        //CR for auto selection for Discount/Promotion component implementation by chetan
        [DataMember]
        public bool? IsReassign { get; set; }
        [DataMember]
        [Display(Name = "isdefault *")]
        public int isdefault { get; set; }

        //GTM e-Billing CR - Ricky - 2014.09.25
        [DataMember]
        [Display(Name = "IsHidden")]
        public bool? IsHidden { get; set; }

        [DataMember]
        [Display(Name = "FavoriteFlag")]
        [StringLength(5)]
        public string FavoriteFlag { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentFind : FindBase
    {
        public PgmBdlPckComponentFind()
        {
            ParentIDs = new List<int>();
            ChildIDs = new List<int>();
        }

        [DataMember]
        public PgmBdlPckComponent PgmBdlPckComponent { get; set; }
        [DataMember]
        public List<int> ParentIDs { get; set; }
        [DataMember]
        public List<int> ChildIDs { get; set; }
        [DataMember]
        public DateTime? StartDateFrom { get; set; }
        [DataMember]
        public DateTime? StartDateTo { get; set; }
        [DataMember]
        public DateTime? EndDateFrom { get; set; }
        [DataMember]
        public DateTime? EndDateTo { get; set; }
        [DataMember]
        public bool? NeedProvision { get; set; }
        [DataMember]
        public bool? NeedActivation { get; set; }
        [DataMember]
        public bool? IsMandatory { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public int? GroupId { get; set; }
        [DataMember]
        public string GroupName { get; set; }
    }

    #endregion

    #region ModelGroup

    [DataContract]
    [Serializable]
    [Table("refModelGroup")]
    public class ModelGroup : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Description *")]
        [StringLength(4000)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Type *")]
        [StringLength(1)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Type { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Is Mandatory? *")]
        public bool IsMandatory { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Min Model *")]
        [Range(0, 100)]
        public int MinModel { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "PBPC ")]
        public int PgmBdlPckComponentID { get; set; }

        [Association("lnkPgmBdlPckComponent", "PgmBdlPckComponentID", "")]
        public virtual PgmBdlPckComponent PgmBdlPckComponent { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupFind : FindBase
    {
        [DataMember]
        public ModelGroup ModelGroup { get; set; }
        [DataMember]
        public List<int> PgmBdlPkgCompIDs { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public bool? IsMandatory { get; set; }
    }

    #endregion

    #region ModelGroupModel

    [DataContract]
    [Serializable]
    [Table("lnkModelGroupModel")]
    public class ModelGroupModel : TimedEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Range(0, 10000, ErrorMessage = "Price amount must be between $0 and $10000")]
        [Display(Name = "Retail Price *")]
        public double? RetailPrice { get; set; }

        [DataMember]
        [Range(0, 10000, ErrorMessage = "Price amount must be between $0 and $10000")]
        [Display(Name = "Dealer Price *")]
        public double? DealerPrice { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "MGP Model Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "MGP Model Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        //[Association("refMdlGrpPackage", "MdlGrpPackageID", "")]
        //public virtual MdlGrpPackage MdlGrpPackage { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model")]
        public int ModelID { get; set; }

        [Association("refModel", "ModelID", "ID")]
        public virtual Model Model { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model Group")]
        public int ModelGroupID { get; set; }

        [Association("refModelGroup", "ModelGroupID", "ID")]
        public virtual ModelGroup ModelGroup { get; set; }

        [DataMember]
        //[Required]
        [Display(Name = "Fulfillment Mode")]
        public int FulfillmentModeID { get; set; }  // Installation, Self-Install, Dispatch, Collection

        //[Association("refFufillmentMode", "FulfillmentModeID", "")]
        //public FulfillmentMode FulfillmentMode { get; set; }

        //[DataMember]
        //[Required]
        //[DataType(DataType.Date)]
        //[Column(TypeName = "date")]
        //[Display(Name = "Start Date")]
        //public DateTime StartDate { get; set; }

        //[DataMember]
        //[DataType(DataType.Date)]
        //[Column(TypeName = "date")]
        //[Display(Name = "End Date")]
        //public DateTime? EndDate { get; set; }

        [DataMember]
        [Display(Name = "Warranty Period")]
        [Range(0, 120)]
        public int? WarrantyPeriod { get; set; }
        //Customer Warranty in Month(s)

        [DataMember]
        [Required]
        [Display(Name = "Recovery Needed *")]
        public bool NeedRecovery { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Is Mandatory? *")]
        public bool IsMandatory { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupModelFind : FindBase
    {
        [DataMember]
        public ModelGroupModel ModelGroupModel { get; set; }
        [DataMember]
        public List<int> ModelGroupIDs { get; set; }
        [DataMember]
        public DateTime? StartDateFrom { get; set; }
        [DataMember]
        public DateTime? StartDateTo { get; set; }
        [DataMember]
        public DateTime? EndDateFrom { get; set; }
        [DataMember]
        public DateTime? EndDateTo { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public bool? RecoveryNeeded { get; set; }
    }
    #endregion

    #region Category

    [DataContract]
    [Serializable]
    [Table("lnkContractDataPlan")]
    public class tbllnkContractDataPlan
    {
        [DataMember]
        [Key]
        [Display(Name = "Id")]
        public Int64 Id { get; set; }

        [DataMember]
        [Display(Name = "ContractId *")]
        public int ContractId { get; set; }

        [DataMember]
        [Display(Name = "DataPlanId *")]
        public int DataPlanId { get; set; }

        [DataMember]
        [Display(Name = "Status *")]
        public string Status { get; set; }

    }


    [DataContract]
    [Serializable]
    [Table("lnkDataPlanId")]
    public class PkgDataPlanId
    {
        [DataMember]
        [Key]
        [Display(Name = "Id")]
        public Decimal Id { get; set; }

        [DataMember]
        [Display(Name = "BdlDataPkgId *")]
        public int BdlDataPkgId { get; set; }

        [DataMember]
        [Display(Name = "MobileRegType *")]
        public int MobileRegType { get; set; }

        [DataMember]
        [Display(Name = "Status *")]
        public int Status { get; set; }

        [DataMember]
        [Display(Name = "Modelid")]
        public string Modelid { get; set; }

    }


    [DataContract]
    [Serializable]
    [Table("lnkPkgData")]
    public class PkgData
    {
        [DataMember]
        [Key]
        [Display(Name = "Id")]
        public Int64 Id { get; set; }


        [DataMember]
        [Display(Name = "BdlPkgId *")]
        public string BdlPkgId { get; set; }


        [DataMember]
        [Display(Name = "BdlDataPkgId *")]
        public string BdlDataPkgId { get; set; }

        [DataMember]
        [Display(Name = "Plan_Type *")]
        public string Plan_Type { get; set; }

        [DataMember]
        [Display(Name = "Status *")]
        public int Status { get; set; }

    }

    [DataContract]
    [Serializable]
    [Table("refCategory")]
    public class Category : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "Category ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Category Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Category Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allowed.")]
        public string Description { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CategoryFind : FindBase
    {
        [DataMember]
        public Category Category { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public bool? IsMandatory { get; set; }
    }

    #endregion

    #region DeviceType
    [DataContract]
    [Serializable]
    [Table("refDeviceType")]
    public class DeviceTypes
    {
        [DataMember]
        [Key]
        [Display(Name = "Device Type ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Device Type")]
        public string DeviceType { get; set; }

        [DataMember]
        public bool? Active { get; set; }
    }
    #endregion

    #region Capacity
    [DataContract]
    [Serializable]
    [Table("refDeviceCapacity")]
    public class DeviceCapacity
    {
        [DataMember]
        [Key]
        [Display(Name = "Device Capacity ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Device Capacity")]
        public string Capacity { get; set; }

        [DataMember]
        public bool? Active { get; set; }
    }
    #endregion

    #region DeviceType
    [DataContract]
    [Serializable]
    [Table("tblPriceRange")]
    public class PriceRange
    {
        [DataMember]
        [Key]
        [Display(Name = "Price Range ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Value")]
        public string Value { get; set; }

        [DataMember]
        [Display(Name = "Display Text")]
        public string DisplayText { get; set; }

        [DataMember]
        public bool? Active { get; set; }
    }
    #endregion

    #region Model
    [DataContract]
    [Serializable]
    [Table("refModel")]
    public class Model : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "Model ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

		[DataMember]
		[Display(Name = "Model Group Name")]
		public string ModelGroupName { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Is Mandatory? *")]
        public bool IsMandatory { get; set; }

        [DataMember]
        [Display(Name = "Price")]
        [Range(0, 100000)]
        public decimal? RetailPrice { get; set; }

        //public virtual ICollection<Price> Prices { get; set; }

        //[Required]
        //[Display(Name = "Model Group ID")]
        //public int ModelGroupID { get; set; }
        //public virtual ModelGroup ModelGroup { get; set; }

        //Fields from TCMv3
        [DataMember]
        [Required]
        [Display(Name = "Brand")]
        public int BrandID { get; set; }

        [Association("refBrand", "BrandID", "")]
        public virtual Brand Brand { get; set; }

        [DataMember]
        [Display(Name = "Distributor")]
        public int? DistOrgID { get; set; }

        //[Association("tblOrganization", "DistOrgID", "ID")]
        //public virtual Organization DistOrg { get; set; }

        //[DataMember]
        //[Display(Name = "RMA Group ID")]
        //public int? RMAGrpID { get; set; }

        //[Association("refRMAgroup", "RMAGrpID", "ID")]
        //public virtual RMAGroup RMAGrp { get; set; }

        [DataMember]
        [Display(Name = "Fulfiller")]
        public int? FulfillerOrgID { get; set; }

        //[Association("tblOrganization", "FulfillerOrgID", "ID")]
        //public virtual Organization FulfillerOrg { get; set; }

        [DataMember]
        [Display(Name = "Repair Organization")]
        public int? RepairOrgID { get; set; }

        //[Association("tblOrganization", "RepairOrgID", "ID")]
        //public virtual Organization RepairOrg { get; set; }

        //[DataMember]
        //[Display(Name = "Retail Price")]
        //[Range(0, 100000)]
        //public double? RetailPrice { get; set; }

        //[DataMember]
        //[Display(Name = "Dealer Price")]
        //[Range(0, 100000)]
        //public double? DealerPrice { get; set; }

        //[DataMember]
        //[Display(Name = "Cost Price")]
        //[Range(0, 100000)]
        //public double? CostPrice { get; set; }

        [DataMember]
        [Display(Name = "Charge Type")]
        [StringLength(50)]
        public string ChargeType { get; set; }

        [DataMember]
        [Display(Name = "Charge To")]
        [StringLength(3)]
        public string ChargeTo { get; set; }

        [DataMember]
        [Display(Name = "Unit")]
        [Range(0, 10000)]
        public int? Unit { get; set; }

        //[DataMember]
        //[Display(Name = "Warranty")]
        //[Range(0, 120)]
        //public int? Warranty { get; set; }
        //Supplier Warranty in Month(s)

        [DataMember]
        [Display(Name = "Got Serial?")]
        public bool HasSerial { get; set; }

        [DataMember]
        [Display(Name = "Got IMEI?")]
        public bool HasIMEI { get; set; }

        [DataMember]
        [Display(Name = "Got PIN?")]
        public bool HasPIN { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Is Serialized?")]
        public bool IsSerialized { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Need to Declare? *")]
        public bool NeedToDeclare { get; set; }

        [DataMember]
        [Display(Name = "Can Return at Any Location?")]
        public bool CanReturn { get; set; }

        [DataMember]
        [Display(Name = "Can Replace at Any Location?")]
        public bool CanReplace { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Can Reserve? *")]
        public bool CanReserve { get; set; }

        [DataMember]
        [Display(Name = "Can Scan?")]
        public bool CanScan { get; set; }

        [DataMember]
        [Display(Name = "Hero Device?")]
        public bool IsHero { get; set; }

        [DataMember]
        [Display(Name = "Device Type")]
        public int? DeviceTypeID { get; set; }

        [DataMember]
        [Display(Name = "Device Capacity")]
        public int? DeviceCapacityID { get; set; }

        [DataMember]
        public string AccessoryImageURL { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelFind : FindBase
    {
        [DataMember]
        public Model Model { get; set; }
        [DataMember]
        public bool? Active { get; set;} 
        [DataMember]
        public bool? IsMandatory { get; set; }
        [DataMember]
        public bool? IsSerialized { get; set; }
        [DataMember]
        public bool? IsSmart { get; set; }
        [DataMember]
        public bool? IsHero { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ModelFindSmart : FindBase
    {
        [DataMember]
        public ModelSmart Model { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public bool? IsMandatory { get; set; }
        [DataMember]
        public bool? IsSerialized { get; set; }
        [DataMember]
        public bool? IsSmart { get; set; }
    }

    #endregion

    #region ModelImage

    [DataContract]
    [Serializable]
    [Table("lnkModelImage")]
    public class ModelImage : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Image Path")]
        [StringLength(500)]
        public string ImagePath { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Model *")]
        public int ModelID { get; set; }

        [Association("refModel", "ModelID", "")]
        public virtual Model Model { get; set; }

        [DataMember]
        [Display(Name = "Colour")]
        public int? ColourID { get; set; }

        [Association("refColour", "ColourID", "")]
        public virtual Colour Colour { get; set; }

    }

    [DataContract]
    [Serializable]
    public class ModelImageFind : FindBase
    {
        public ModelImageFind()
        {
            ModelIDs = new List<int>();
        }

        [DataMember]
        public ModelImage ModelImage { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public List<int> ModelIDs { get; set; }
    }

    #endregion

    #region Brand

    [DataContract]
    [Serializable]
    [Table("refBrand")]
    public class Brand : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "Brand ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Brand Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Brand Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Display(Name = "Brand Description")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Display(Name = "Image Path")]
        [StringLength(500)]
        public string ImagePath { get; set; }

        //[Required]
        [DataMember]
        [Display(Name = "Category")]
        public int CategoryID { get; set; }

        [Association("refCategory", "CategoryID", "")]
        public virtual Category Category { get; set; }

    }

    [DataContract]
    [Serializable]
    public class BrandFind : FindBase
    {
        [DataMember]
        public Brand Brand { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public bool? IsMandatory { get; set; }
    }
    [DataContract]
    [Serializable]
    public class BrandFindSmart : FindBase
    {
        [DataMember]
        public BrandSmart Brand { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public bool? IsMandatory { get; set; }
    }

    #endregion

    #region Colour

    [DataContract]
    [Serializable]
    [Table("refColour")]
    public class Colour : ActiveEntity
    {
        [Key]
        [DataMember]
        [Display(Name = "Colour ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [MaxLength(20)]
        [Display(Name = "Colour Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allowed.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [MaxLength(50)]
        [Display(Name = "Colour Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allowed.")]
        public string Name { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allowed.")]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model *")]
        public int ModelID { get; set; }

        [Association("refModel", "ModelID", "ID")]
        public virtual Model Model { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ColourFind : FindBase
    {
        [DataMember]
        public Colour Colour { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Model Part No

    [DataContract]
    [Serializable]
    [Table("lnkModelPartNo")]
    public class ModelPartNo : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model *")]
        public int ModelID { get; set; }

        [Association("refModel", "ModelID", "ID")]
        public virtual Model Model { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Colour ID")]
        public int? ColourID { get; set; }

        [Association("refColour", "ColourID", "ColourID")]
        public virtual Colour Colour { get; set; }

        //[DataMember]
        //[Required]
        //[Display(Name = "Item Type *")]
        //public int ItemTypeID { get; set; }

        //[Association("refItemType", "ItemTypeID", "ItemTypeID")]
        //public virtual ItemType ItemType { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Part No *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string PartNo { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelPartNoFind : FindBase
    {
        [DataMember(Order = 0)]
        public ModelPartNo ModelPartNo { get; set; }
        [DataMember(Order = 1)]
        public bool? Active { get; set; }
    }

    #endregion

    #region Item Price

    [DataContract]
    [Serializable]
    [Table("lnkItemPrice")]
    public class ItemPrice : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Bundle Package")]
        public int BdlPkgID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Package Component")]
        public int PkgCompID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model")]
        public int ModelID { get; set; }

        [DataMember]
        [Display(Name = "Price")]
        [Range(0, 100000)]
        public decimal Price { get; set; }

        [DataMember]
        [Display(Name = "pkgDataID")]
        public int pkgDataID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ItemPriceFind : FindBase
    {
        [DataMember]
        public ItemPrice ItemPrice { get; set; }
        [DataMember]
        public List<int> ContractIds { get; set; }
        [DataMember]
        public List<int> DataPlanIds { get; set; }
        public bool? Active { get; set; }
    }

    #endregion

    //added by Deepika
    #region Advance & Deposit Price for Device

    [DataContract]
    [Serializable]
    [Table("tblPlanDeviceDeposit")]
    public class AdvDepositPrice : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model")]
        public int modelId { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Plan")]
        public int planId { get; set; }

        [DataMember]
        [Display(Name = "MalyDeviceAdvance")]
        [Range(0, 100000)]
        public decimal malyDevAdv { get; set; }

        [DataMember]
        [Display(Name = "otherDeviceAdvance")]
        [Range(0, 100000)]
        public decimal othDevAdv { get; set; }

        [DataMember]
        [Display(Name = "MalyPlanAdvance")]
        [Range(0, 100000)]
        public decimal malayPlanAdv { get; set; }

        [DataMember]
        [Display(Name = "otherPlanAdvance")]
        [Range(0, 100000)]
        public decimal othPlanAdv { get; set; }

        [DataMember]
        [Display(Name = "malyPlanDeposit")]
        [Range(0, 100000)]
        public decimal malyPlanDeposit { get; set; }

        [DataMember]
        [Display(Name = "otherPlanDeposit")]
        [Range(0, 100000)]
        public decimal othPlanDeposit { get; set; }

        [DataMember]
        [Display(Name = "malayDevDeposit")]
        [Range(0, 100000)]
        public decimal malayDevDeposit { get; set; }

        [DataMember]
        [Display(Name = "malayDevDeposit")]
        [Range(0, 100000)]
        public decimal othDevDeposit { get; set; }

        [DataMember]
        [Display(Name = "contractID")]
        [Range(0, 100000)]
        public int contractID { get; set; }

        //added by deepika 10 march
        [DataMember]
        [Display(Name = "contractID")]
        [Range(0, 100000)]
        public int dataPlanId { get; set; }

        [DataMember]
        [Display(Name = "upfrontPayment")]
        [Range(0, 100000)]
        public decimal upfrontPayment { get; set; }

        [DataMember]
        public bool isWaiverReq { get; set; }

    }

    [DataContract]
    [Serializable]
    public class AdvDepositPriceFind : FindBase
    {
        [DataMember]
        public DAL.Models.AdvDepositPrice advDepositPrice { get; set; }
    }

    #endregion
    //end deepika

    #region SimModels Added by VLT on 09-Apr-2013
    [DataContract]
    [Serializable]
    [Table("refSIMModels")]
    public class SimModels
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Article Id")]
        public int ArticleId { get; set; }

        [DataMember]
        [StringLength(500)]
        [Display(Name = "SimTypeDescription")]
        public string SimTypeDescription { get; set; }

        [DataMember]
        [StringLength(200)]
        [Display(Name = "UOM")]
        public string UOM { get; set; }

        [DataMember]
        [StringLength(200)]
        [Display(Name = "UOMPackage")]
        public string UOMPackage { get; set; }

        [DataMember]
        public DateTime? ValidFrom { get; set; }

        [DataMember]
        public DateTime? ValidTo { get; set; }

        [DataMember]
        [Display(Name = "Price")]
        public double Price { get; set; }

        [DataMember]
        [StringLength(200)]
        [Display(Name = "PriceStatus")]
        public string PriceStatus { get; set; }

        [DataMember]
        public bool isActive { get; set; }

        [DataMember]
        [StringLength(20)]
        [Display(Name = "SimCardType")]
        public string SimCardType { get; set; }
    }

    //[DataContract]
    //public class SimModelTypeFind : FindBase
    //{
    //    [DataMember]
    //    public SimModels SimModels { get; set; }
    //    [DataMember]
    //    public bool? Active { get; set; }
    //}

    #endregion

    #region Property

    [DataContract]
    [Serializable]
    [Table("refProperty")]
    public class Property : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(10)]
        [Display(Name = "Type *")]
        public string Type { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyFind : FindBase
    {
        [DataMember]
        public Property Property { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region PropertyValue

    [DataContract]
    [Serializable]
    [Table("refPropertyValue")]
    public class PropertyValue : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(10)]
        [Display(Name = "Property *")]
        public int PropertyID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyValueFind : FindBase
    {
        public PropertyValueFind()
        {
            PropertyIDs = new List<int>();
        }

        [DataMember]
        public PropertyValue PropertyValue { get; set; }
        [DataMember]
        public List<int> PropertyIDs { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region PlanProperty

    [DataContract]
    [Serializable]
    [Table("refPlanProperty")]
    public class PlanProperty : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Package *")]
        public int BdlPkgID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Voice Usage")]
        public int VoiceUsage { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Data Usage")]
        public int DataUsage { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "SMS Usage")]
        public int SMSUsage { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Price")]
        public int Price { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PlanPropertyFind : FindBase
    {
        [DataMember]
        public PlanProperty PlanProperty { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region DeviceProperty

    [DataContract]
    [Serializable]
    [Table("refDeviceProperty")]
    public class DeviceProperty : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model *")]
        public int ModelID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Brand *")]
        public int BrandID { get; set; }

        [DataMember]
        [Display(Name = "Type of Device")]
        public int? TypeOfDevice { get; set; }

        //by Chodey
        //[DataMember]
        //[Display(Name = "Technology")]
        //public int? Technology { get; set; }

        [DataMember]
        [Display(Name = "Touch Screen")]
        public int? TouchScreen { get; set; }

        [DataMember]
        [Display(Name = "Camera")]
        public int Camera { get; set; }

        [DataMember]
        [Display(Name = "GPS")]
        public int GPS { get; set; }

        [DataMember]
        [Display(Name = "Bluetooth")]
        public int Bluetooth { get; set; }

        [DataMember]
        [Display(Name = "Screentype")]
        public int ScreenType { get; set; }

        [DataMember]
        [Display(Name = "Dual Front")]
        public int DualFront { get; set; }

        //Chodey

        [DataMember]
        [Display(Name = "SIM")]
        public int? PriceRange { get; set; }

        //[DataMember]
        //[Display(Name = "Camera")]
        //public bool Camera { get; set; }

        [DataMember]
        [Display(Name = "Height")]
        public decimal? Height { get; set; }

        [DataMember]
        [Display(Name = "Weight")]
        public decimal? Weight { get; set; }

        [DataMember]
        [Display(Name = "Thick")]
        public decimal? Thick { get; set; }
    }

    [DataContract]
    [Serializable]
    public class DevicePropertyFind : FindBase
    {
        public DevicePropertyFind()
        {
            DeviceProperty = new DeviceProperty();
            ModelIDs = new List<int>();
            BrandIDs = new List<int>();
            //TechnologyIDs = new List<int>();
            TypeOfDeviceIDs = new List<int>();
            PriceRangeIDs = new List<int>();
            TouchScreenIDs = new List<int>();
            Camera = new List<int>();
            GPS = new List<int>();
            ScreenType = new List<int>();
            Bluetooth = new List<int>();
            DualFront = new List<int>();
        }

        [DataMember]
        public DeviceProperty DeviceProperty { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public List<int> ModelIDs { get; set; }
        [DataMember]
        public List<int> BrandIDs { get; set; }
        [DataMember]
        public List<int> TypeOfDeviceIDs { get; set; }
        //[DataMember]
        //public List<int> TechnologyIDs { get; set; }
        [DataMember]
        public List<int> PriceRangeIDs { get; set; }
        [DataMember]
        public List<int> TouchScreenIDs { get; set; }
        [DataMember]
        public List<int> Camera { get; set; }
        [DataMember]
        public List<int> GPS { get; set; }
        [DataMember]
        public List<int> ScreenType { get; set; }
        [DataMember]
        public List<int> Bluetooth { get; set; }
        [DataMember]
        public List<int> DualFront { get; set; }
        //[DataMember]
        //public bool? Camera { get; set; }
    }

    #endregion

    #region added by Rajeswari on 26th March for articleid
    [DataContract]
    [Serializable]
    [Table("lnkBrandArticle")]
    public class BrandArticle
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Article ID")]
        public string ArticleID { get; set; }

        [DataMember]
        [Display(Name = "Model ID")]
        public int ModelID { get; set; }

        [DataMember]
        [Display(Name = "Colour ID")]
        public int ColourID { get; set; }

        [DataMember]
        [Display(Name = "Active")]
        public bool? Active { get; set; }

        [DataMember]
        [Display(Name = "Image Path")]
        public string ImagePath { get; set; }

        //Added by VLT on 17 May 2013 to get device image details dynamically
        [DataMember]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [DataMember]
        [Display(Name = "Model Image")]
        public string ModelImage { get; set; }
        //Added by VLT on 17 May 2013 to get device image details dynamically
    }
    #endregion

    #region Reg Type

    [DataContract]
    [Serializable]
    [Table("refMISMMandatoryComponents")]
    public class MISMMandatoryComponents : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "ComponentId *")]
        public int ComponentId { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Display(Name = "Message")]
        [StringLength(1000)]
        [Column(TypeName = "ntext")]
        public string Message { get; set; }
    }

    #endregion

    [DataContract]
    [Serializable]
    [Table("tblUOMArticle")]
    public class UOMArticle
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public decimal ID { get; set; }

        [DataMember]
        [Display(Name = "ArticleId")]
        public string ArticleId { get; set; }

        [DataMember]
        [Display(Name = "ArticleName")]
        public string ArticleName { get; set; }

        [DataMember]
        [Display(Name = "UOMID")]
        public string UOMID { get; set; }

        [DataMember]
        [Display(Name = "UOM Description")]
        public string UOMDescription { get; set; }

        [DataMember]
        [Display(Name = "Contract Duration")]
        public int ContractDuration { get; set; }

        [DataMember]
        [Display(Name = "contractID")]
        public string contractID { get; set; }

        [DataMember]
        [Display(Name = "PlanName")]
        public string PlanName { get; set; }

        [DataMember]
        [Display(Name = "planID")]
        public string planID { get; set; }

        [DataMember]
        [Display(Name = "OfferName")]
        public string OfferName { get; set; }
    }


    [DataContract]
    [Serializable]
    [Table("lnkofferuomarticleprice")]
    public class lnkofferuomarticleprice
    {

        [DataMember]
        [Display(Name = "Id")]
        public int Id { get; set; }

        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
        [DataMember]
        [Display(Name = "ArticleId")]
        public string ArticleId { get; set; }
        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid

		[DataMember]
		[Display(Name = "Price")]
		[Range(0, 100000)]
		public decimal Price { get; set; }

        [DataMember]
        [Display(Name = "OfferName")]
        public string OfferName { get; set; }

        [DataMember]
        [Display(Name = "ToDateTime")]
        public DateTime ToDateTime { get; set; }
        [DataMember]
        [Display(Name = "UOMCode")]
        public string UOMCode { get; set; }

        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
        [DataMember]
        [Display(Name = "KenanPackageId")]
        public string KenanPackageId { get; set; }
        [DataMember]
        [Display(Name = "KenanComponent1Id")]
        public string KenanComponent1Id { get; set; }

        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid

        //14012015 - Anthony - GST Trade-Up - Start
        [DataMember]
        [Display(Name = "isTradeUp")]
        public bool? isTradeUp { get; set; }

        [DataMember]
        [Display(Name = "TradeUpAmount")]
        public string TradeUpAmount { get; set; }
        //14012015 - Anthony - GST Trade Up - End

		[DataMember]
		[Display(Name = "isDeviceFinancing")]
		public bool isDeviceFinancing { get; set; }


    }
    [DataContract]
    [Serializable]
    [Table("dependentContract")]
    public class dependentContract
    {
        [DataMember]
        [Key]
        [Display(Name = "Sno")]
        public int sno { get; set; }

        [DataMember]
        [Display(Name = "OfferID")]
        public int offerID { get; set; }

        [DataMember]
        [Display(Name = "MappedContracts")]
        public string ContractsMapped { get; set; }

        [DataMember]
        [Display(Name = "Active")]
        public bool Active { get; set; }

        [DataMember]
        [Display(Name = "creatdate")]
        public DateTime creatdate { get; set; }

        [DataMember]
        [Display(Name = "updatedate")]
        public DateTime updatedate { get; set; }

        [DataMember]
        [Display(Name = "createby")]
        public int createby { get; set; }
    }


    #region Component Groups Added by VLT on 15-Apr-2013
    [DataContract]
    [Serializable]
    [Table("refComponentGroup")]
    public class ComponentGroup
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [StringLength(500)]
        [Display(Name = "Description")]
        public string Description { get; set; }


        [DataMember]
        [Required]
        [StringLength(500)]
        [Display(Name = "ComponentGroupName")]
        public string ComponentGroupName { get; set; }


        [DataMember]
        [StringLength(100)]
        [Display(Name = "ComponentGroupType")]
        public string ComponentGroupType { get; set; }

        [DataMember]
        [Display(Name = "CreatedDate *")]
        public DateTime CreatedDate { get; set; }


        [DataMember]
        public bool isActive { get; set; }
    }
    #endregion



    [DataContract]
    [Serializable]
    [Table("lnkModelOffer")]
    public class ModelOffer
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "ArticleId")]
        public string ArticleId { get; set; }


        [DataMember]
        [Display(Name = "OfferID")]
        public int OfferID { get; set; }

        //[DataMember]
        //[Key]
        //[Display(Name = "StartDate")]
        //public DateTime StartDate { get; set; }

        //[DataMember]
        //[Display(Name = "EndDate")]
        //public DateTime EndDate { get; set; }





    }


    [DataContract]
    [Serializable]
    [Table("lnkComponentDependency")]
    public class DependentComponents
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "MainComponentId")]
        public int MainComponentId { get; set; }


        [DataMember]
        [Display(Name = "DependentComponentId")]
        public int DependentComponentId { get; set; }

        [DataMember]
        [Display(Name = "PlanId")]
        public int PlanId { get; set; }

        [DataMember]
        [Display(Name = "Status")]
        public bool Status { get; set; }
        //[DataMember]
        //[Key]
        //[Display(Name = "StartDate")]
        //public DateTime StartDate { get; set; }

        //[DataMember]
        //[Display(Name = "EndDate")]
        //public DateTime EndDate { get; set; }





    }


    [DataContract]
    [Serializable]
    [Table("lnkComponentDependencyNew")]
    public class DependentComponentsNew
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "MainComponentId")]
        public int MainComponentId { get; set; }


        [DataMember]
        [Display(Name = "DependentComponentId")]
        public int DependentComponentId { get; set; }

        [DataMember]
        [Display(Name = "PlanId")]
        public int PlanId { get; set; }

        [DataMember]
        [Display(Name = "Status")]
        public bool Status { get; set; }
        //[DataMember]
        //[Key]
        //[Display(Name = "StartDate")]
        //public DateTime StartDate { get; set; }

        //[DataMember]
        //[Display(Name = "EndDate")]
        //public DateTime EndDate { get; set; }





    }


    [DataContract]
    [Serializable]
    [Table("lnkPgmBdlPkgCompNew")]
    public class AddRemoveVASComponents
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public long ID { get; set; }

        [DataMember]
        [Display(Name = "ComponentId")]
        public string ComponentId { get; set; }

        [DataMember]
        [Display(Name = "ComponentDesc")]
        public string ComponentDesc { get; set; }

        [DataMember]
        [Display(Name = "PackageId")]
        public string PackageID { get; set; }

        [DataMember]
        [Display(Name = "PackageDesc")]
        public string PackageDesc { get; set; }

        [DataMember]
        [Display(Name = "Price")]
        public decimal Price { get; set; }

        [DataMember]
        [Display(Name = "lnkPgmBdlPkgCompId")]
        public int lnkPgmBdlPkgCompId { get; set; }

        [DataMember]
        [Display(Name = "RegId")]
        public long RegId { get; set; }

        [DataMember]
        [Display(Name = "Status")]
        public string Status { get; set; }

        [DataMember]
        [Display(Name = "MSISDN")]
        public string MSISDN { get; set; }

        [DataMember]
        [Display(Name = "OrderId")]
        public string OrderId { get; set; }

        [DataMember]
        [Display(Name = "OrderIdResets")]
        public string OrderIdResets { get; set; }

        [DataMember]
        [Display(Name = "MsgCode")]
        public string MsgCode { get; set; }

        [DataMember]
        [Display(Name = "MsgDesc")]
        public string MsgDesc { get; set; }

        [DataMember]
        [Display(Name = "CreatedDate")]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        [Display(Name = "CreatedBy")]
        public string CreatedBy { get; set; }

        [DataMember]
        [Display(Name = "ModifiedBy")]
        public string ModifiedBy { get; set; }

        [DataMember]
        [Display(Name = "ModifiedDate")]
        public DateTime ModifiedDate { get; set; }





    }



    //Added Pavan
    #region Advance & Deposit Price for Device

    [DataContract]
    [Serializable]
    public class DiscountPriceDetail
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model")]
        public int modelId { get; set; }

        [DataMember]
        [Display(Name = "PlanId")]
        public int PlanID
        {
            get;
            set;
        }
        [DataMember]
        [Display(Name = "ContractId")]
        public string ContractID
        {
            get;
            set;

        }

        [DataMember]
        [Display(Name = "NewPlanName")]
        public string NewPlanName
        {
            get;
            set;

        }

        [DataMember]
        [Display(Name = "DataPlanId")]
        public int DataPlanID
        {
            get;
            set;
        }

        [DataMember]
        [Display(Name = "OrgPrice")]
        public string OrgPrice
        {
            get;
            set;
        }

        [DataMember]
        [Display(Name = "DiscountPrice")]
        public string DiscountPrice
        {
            get;
            set;
        }
        [DataMember]
        public List<DiscountPrices> DiscountPrices { get; set; }
    }
    #endregion

    //Added Pavan


    [DataContract]
    [Serializable]
    [Table("DiscountPriceDetails")]
    public class DiscountPrices
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model")]
        public int modelId { get; set; }

        [DataMember]
        [Display(Name = "PlanId")]
        public int PlanID
        {
            get;
            set;
        }
        [DataMember]
        [Display(Name = "ContractId")]
        public string ContractID
        {
            get;
            set;

        }

        [DataMember]
        [Display(Name = "NewPlanName")]
        public string NewPlanName
        {
            get;
            set;

        }

        [DataMember]
        [Display(Name = "DataPlanId")]
        public int DataPlanID
        {
            get;
            set;
        }

        [DataMember]
        [Display(Name = "OrgPrice")]
        public string OrgPrice
        {
            get;
            set;
        }

        [DataMember]
        [Display(Name = "DiscountPrice")]
        public string DiscountPrice
        {
            get;
            set;
        }
        [DataMember]
        public List<DiscountPrices> DiscountPrices1 { get; set; }

    }

    [DataContract]
    [Serializable]
    public class ContractDataplan
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }



    [DataContract]
    [Serializable]
    [Table("lnkPgmBdlPkgComp_Smart")]
    public class PgmBdlPckComponentSmart : TimedEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "PBPC ID")]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Parent *")]
        public int ParentID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Child *")]
        public int ChildID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Link Type *")]
        [StringLength(2)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string LinkType { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Description *")]
        [StringLength(4000)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Display(Name = "Plan Type")]
        [StringLength(2)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string PlanType { get; set; }

        [DataMember]
        [Display(Name = "KenanCode")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string KenanCode { get; set; }

        [DataMember]
        [Display(Name = "Alternate Kenan Code")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string AlternateKenanCode { get; set; }

        [DataMember]
        [Display(Name = "Price")]
        [Range(0, 100000)]
        public decimal Price { get; set; }

        [DataMember]
        [Display(Name = "Value")]
        public int? Value { get; set; }

        [DataMember]
        [Display(Name = "Filter Org Type")]
        public string FilterOrgType { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Need Provision? *")]
        public bool NeedProvision { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Need Activation? *")]
        public bool NeedActivation { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Need Coverage? *")]
        public bool NeedCoverage { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Mandatory? *")]
        public bool IsMandatory { get; set; }

        [DataMember]
        [Display(Name = "Retail Price")]
        [Range(0, 100000)]
        public float? RetailPrice { get; set; }

        [DataMember]
        [Display(Name = "Billing Code")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string BillingCode { get; set; }




        [DataMember]
        [Display(Name = "Contract Type")]
        [StringLength(10)]
        public string contract_type { get; set; }

        [DataMember]
        public string ModelId { get; set; }

        //**Addded for OnePlan CR**//
        [DataMember]
        [Display(Name = "isdefault *")]
        public int isdefault { get; set; }
        //**Addded for OnePlan CR**//


    }


    [DataContract]
    [Serializable]
    [Table("lnkPlanOrder")]
    public class ContractPlans
    {
        [DataMember]
        [Key]
        public int ID { set; get; }
        [DataMember]
        public string PlanName { get; set; }
        [DataMember]
        public bool Active { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("refBrandContracts")]
    public class ContractBundleModels
    {

        [DataMember]
        [Key]
        public int BrandID { set; get; }
        [DataMember]
        public string ContractBrandModel { get; set; }


        [DataMember]
        public bool Active { get; set; }
    }
    //Changes related to Multiple Contracts CR changed by code--Chetan/ db--Pavan
    [DataContract]
    [Serializable]
    [Table("tblPlanPrices")]
    public class PlanPrices
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int DeviceID { get; set; }
        [DataMember]
        public string OfferName { get; set; }
        [DataMember]
        public int PlanID { get; set; }
        [DataMember]
        public string UOMCode { get; set; }
        [DataMember]
        public int AdvPrice { get; set; }
        [DataMember]
        public int MonthlyPrice { get; set; }
        [DataMember]
        public int isIvalue { get; set; }

    }

    [DataContract]
    [Serializable]
    [Table("lnkMNPInventoryDetails")]
    public class lnkMNPInventoryDetails
    {
        [DataMember]
        [Key]
        public int Id { get; set; }
        [DataMember]
        public string externalId { get; set; }
        [DataMember]
        public string orderId { get; set; }
        [DataMember]
        public string fxAcctNo { get; set; }
        [DataMember]
        public string invdViewId { get; set; }
        [DataMember]
        public string inventoryId { get; set; }
        [DataMember]
        public string inventoryIdResets { get; set; }
        [DataMember]
        public string newInvdType { get; set; }
        [DataMember]
        public string invStatus { get; set; }
        [DataMember]
        public string oldInvdType { get; set; }

    }

    #region smart related methods
    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentFindSmart : FindBase
    {
        public PgmBdlPckComponentFindSmart()
        {
            ParentIDs = new List<int>();
            ChildIDs = new List<int>();
        }

        [DataMember]
        public PgmBdlPckComponentSmart PgmBdlPckComponentSmart { get; set; }
        [DataMember]
        public List<int> ParentIDs { get; set; }
        [DataMember]
        public List<int> ChildIDs { get; set; }
        [DataMember]
        public DateTime? StartDateFrom { get; set; }
        [DataMember]
        public DateTime? StartDateTo { get; set; }
        [DataMember]
        public DateTime? EndDateFrom { get; set; }
        [DataMember]
        public DateTime? EndDateTo { get; set; }
        [DataMember]
        public bool? NeedProvision { get; set; }
        [DataMember]
        public bool? NeedActivation { get; set; }
        [DataMember]
        public bool? IsMandatory { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public int? GroupId { get; set; }
        [DataMember]
        public string GroupName { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("refSmartRebates")]
    public class SmartRebates
    {
        [DataMember]
        [Key]
        [Display(Name = "Rebate ID")]
        public int Discount_ID { get; set; }

        [DataMember]
        [Display(Name = "Rebate")]
        public string Rebate { get; set; }
    }


    #region ModelGroupModel_Smart

    [DataContract]
    [Serializable]
    [Table("lnkModelGroupModel_Smart")]
    public class ModelGroupModel_Smart : TimedEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Range(0, 10000, ErrorMessage = "Price amount must be between $0 and $10000")]
        [Display(Name = "Retail Price *")]
        public decimal? RetailPrice { get; set; }

        [DataMember]
        [Range(0, 10000, ErrorMessage = "Price amount must be between $0 and $10000")]
        [Display(Name = "Dealer Price *")]
        public decimal? DealerPrice { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "MGP Model Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "MGP Model Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        //[Association("refMdlGrpPackage", "MdlGrpPackageID", "")]
        //public virtual MdlGrpPackage MdlGrpPackage { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model")]
        public int ModelID { get; set; }

        [Association("refModel", "ModelID", "ID")]
        public virtual Model Model { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model Group")]
        public int ModelGroupID { get; set; }

        [Association("refModelGroup", "ModelGroupID", "ID")]
        public virtual ModelGroup ModelGroup { get; set; }

        [DataMember]
        //[Required]
        [Display(Name = "Fulfillment Mode")]
        public int FulfillmentModeID { get; set; }  // Installation, Self-Install, Dispatch, Collection

        //[Association("refFufillmentMode", "FulfillmentModeID", "")]
        //public FulfillmentMode FulfillmentMode { get; set; }

        //[DataMember]
        //[Required]
        //[DataType(DataType.Date)]
        //[Column(TypeName = "date")]
        //[Display(Name = "Start Date")]
        //public DateTime StartDate { get; set; }

        //[DataMember]
        //[DataType(DataType.Date)]
        //[Column(TypeName = "date")]
        //[Display(Name = "End Date")]
        //public DateTime? EndDate { get; set; }

        [DataMember]
        [Display(Name = "Warranty Period")]
        [Range(0, 120)]
        public int? WarrantyPeriod { get; set; }
        //Customer Warranty in Month(s)

        [DataMember]
        [Required]
        [Display(Name = "Recovery Needed *")]
        public bool NeedRecovery { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Is Mandatory? *")]
        public bool IsMandatory { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupModelFind_Smart : FindBase
    {
        [DataMember]
        public ModelGroupModel_Smart ModelGroupModel { get; set; }
        [DataMember]
        public List<int> ModelGroupIDs { get; set; }
        [DataMember]
        public DateTime? StartDateFrom { get; set; }
        [DataMember]
        public DateTime? StartDateTo { get; set; }
        [DataMember]
        public DateTime? EndDateFrom { get; set; }
        [DataMember]
        public DateTime? EndDateTo { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public bool? RecoveryNeeded { get; set; }
    }
    #endregion


    [DataContract]
    [Serializable]
    //[Table("refModel")]
    [Table("refmodel_smart")]
    public class ModelSmart : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "Model ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Model Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Is Mandatory? *")]
        public bool IsMandatory { get; set; }

        [DataMember]
        [Display(Name = "Price")]
        [Range(0, 100000)]
        public decimal? RetailPrice { get; set; }

        //public virtual ICollection<Price> Prices { get; set; }

        //[Required]
        //[Display(Name = "Model Group ID")]
        //public int ModelGroupID { get; set; }
        //public virtual ModelGroup ModelGroup { get; set; }

        //Fields from TCMv3
        [DataMember]
        [Required]
        [Display(Name = "Brand")]
        public int BrandID { get; set; }

        [Association("refBrand_Smart", "BrandID", "")]
        public virtual Brand Brand { get; set; }

        [DataMember]
        [Display(Name = "Distributor")]
        public int? DistOrgID { get; set; }

        //[Association("tblOrganization", "DistOrgID", "ID")]
        //public virtual Organization DistOrg { get; set; }

        //[DataMember]
        //[Display(Name = "RMA Group ID")]
        //public int? RMAGrpID { get; set; }

        //[Association("refRMAgroup", "RMAGrpID", "ID")]
        //public virtual RMAGroup RMAGrp { get; set; }

        [DataMember]
        [Display(Name = "Fulfiller")]
        public int? FulfillerOrgID { get; set; }

        //[Association("tblOrganization", "FulfillerOrgID", "ID")]
        //public virtual Organization FulfillerOrg { get; set; }

        [DataMember]
        [Display(Name = "Repair Organization")]
        public int? RepairOrgID { get; set; }

        //[Association("tblOrganization", "RepairOrgID", "ID")]
        //public virtual Organization RepairOrg { get; set; }

        //[DataMember]
        //[Display(Name = "Retail Price")]
        //[Range(0, 100000)]
        //public double? RetailPrice { get; set; }

        //[DataMember]
        //[Display(Name = "Dealer Price")]
        //[Range(0, 100000)]
        //public double? DealerPrice { get; set; }

        //[DataMember]
        //[Display(Name = "Cost Price")]
        //[Range(0, 100000)]
        //public double? CostPrice { get; set; }

        [DataMember]
        [Display(Name = "Charge Type")]
        [StringLength(50)]
        public string ChargeType { get; set; }

        [DataMember]
        [Display(Name = "Charge To")]
        [StringLength(3)]
        public string ChargeTo { get; set; }

        [DataMember]
        [Display(Name = "Unit")]
        [Range(0, 10000)]
        public int? Unit { get; set; }

        //[DataMember]
        //[Display(Name = "Warranty")]
        //[Range(0, 120)]
        //public int? Warranty { get; set; }
        //Supplier Warranty in Month(s)

        [DataMember]
        [Display(Name = "Got Serial?")]
        public bool HasSerial { get; set; }

        [DataMember]
        [Display(Name = "Got IMEI?")]
        public bool HasIMEI { get; set; }

        [DataMember]
        [Display(Name = "Got PIN?")]
        public bool HasPIN { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Is Serialized?")]
        public bool IsSerialized { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Need to Declare? *")]
        public bool NeedToDeclare { get; set; }

        [DataMember]
        [Display(Name = "Can Return at Any Location?")]
        public bool CanReturn { get; set; }

        [DataMember]
        [Display(Name = "Can Replace at Any Location?")]
        public bool CanReplace { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Can Reserve? *")]
        public bool CanReserve { get; set; }

        [DataMember]
        [Display(Name = "Can Scan?")]
        public bool CanScan { get; set; }

    }

    [DataContract]
    [Serializable]
    [Table("lnkBrandArticle_Smart")]
    public class BrandArticleSmart
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Article ID")]
        public string ArticleID { get; set; }

        [DataMember]
        [Display(Name = "Model ID")]
        public int ModelID { get; set; }

        [DataMember]
        [Display(Name = "Colour ID")]
        public int ColourID { get; set; }

        [DataMember]
        [Display(Name = "Active")]
        public bool? Active { get; set; }

        [DataMember]
        [Display(Name = "Image Path")]
        public string ImagePath { get; set; }

        [DataMember]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [DataMember]
        [Display(Name = "Model Image")]
        public string ModelImage { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("refBrand_Smart")]
    public class BrandSmart : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "Brand ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Brand Code *")]
        [StringLength(20)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Brand Name *")]
        [StringLength(100)]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Display(Name = "Brand Description")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Display(Name = "Image Path")]
        [StringLength(500)]
        public string ImagePath { get; set; }

        //[Required]
        [DataMember]
        [Display(Name = "Category")]
        public int CategoryID { get; set; }

        [Association("refCategory", "CategoryID", "")]
        public virtual Category Category { get; set; }

    }
    #endregion

    #region supermarket2 cr

    [DataContract]
    [Serializable]
    [Table("lnkVoiceContractDetails")]
    public class VoiceContractDetails : ActiveEntity
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string ContractKenanCode { get; set; }

        [DataMember]
        public string ContractDetails { get; set; }

    }

    [DataContract]
    [Serializable]
    public class VoiceContractDetailsFind : FindBase
    {
        [DataMember]
        public DAL.Models.VoiceContractDetails voiceContractDetails { get; set; }
    }


    #endregion

    #region "Sim Replacement Model"
    [DataContract]
    [Serializable]
    public class SimReplacementComps
    {
        [DataMember]
        public int compID { get; set; }
        [DataMember]
        public string compKenanCode { get; set; }
        [DataMember]
        public string compLinkType { get; set; }
        [DataMember]
        public string compPlanType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SimRplcCompsRequest
    {
        [DataMember]
        public int compID { get; set; }
        [DataMember]
        public string compKenanCode { get; set; }
        [DataMember]
        public bool status { get; set; }
        [DataMember]
        public string respCode { get; set; }
        [DataMember]
        public string respMsg { get; set; }
        [DataMember]
        public string OrderId { get; set; }
        [DataMember]
        public string OrderIdResets { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public int RegID { get; set; }
    }

    #endregion

    #region SIM Type

    [DataContract]
    [Serializable]
    [Table("refSIMTypes")]
    public class SIMCardTypes : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(50)]
        [Display(Name = "Description *")]
        public string Description { get; set; }

        [DataMember]
        public bool Active { get; set; }
    }


    #endregion




    [DataContract]
    [Serializable]
    [Table("lnkAcctMktCodePackages")]
    public class lnkAcctMktCodePackages
    {
        [DataMember]
        [Key]
        public int Id { get; set; }

        [DataMember]
        public int AcctCategory { get; set; }

        [DataMember]
        public int MarketCode { get; set; }

        [DataMember]
        public int lnkpgmbdlpkgcompID { get; set; }

    }

	[DataContract]
    [Serializable]
	[Table("lnkAcctMktCodeOffers")]
	public class lnkAcctMktCodeOffers
    {
        [DataMember]
        [Key]
        public int Id { get; set; }

        [DataMember]
		public int AcctCategory { get; set; }

        [DataMember]
		public int MarketCode { get; set; }

        [DataMember]
		public int lnkofferuomarticlepriceID { get; set; }

    }

    #region Component Relation - GTM e-Billing CR - Ricky - 2014.09.25
    [DataContract]
    [Serializable]
    [Table("refComponentRelation")]
    public class ComponentRelation
    {
        [DataMember]
        [Key]
        [Column(Order = 0)]
        [Required]
        [Display(Name = "PlanBundle")]
        public int PlanBundle { get; set; }

        [DataMember]
        [Key]
        [Column(Order = 1)]
        [Required]
        [StringLength(20)]
        [Display(Name = "Rules")]
        public string Rules { get; set; }

        [DataMember]
        [Key]
        [Column(Order = 2)]
        [Required]
        [Display(Name = "ComponentPgm")]
        public int ComponentPgm { get; set; }

        [DataMember]
        [StringLength(20)]
        [Display(Name = "KenanCode")]
        public string KenanCode { get; set; }

        [DataMember]
        [Display(Name = "CreatedDate")]
        public DateTime CreatedDate { get; set; }

		[DataMember]
		[StringLength(20)]
		[Display(Name = "PlanKenanCode")]
		public string PlanKenanCode { get; set; }

    }
    #endregion

	#region PlanDeviceVASValidation - Quota Sharing - MM - 8 Jan 2015
	[DataContract]
	[Serializable]
	[Table("tblValidationRules")]
	public class PlanDeviceValidationRules
	{
		[DataMember]
		[Key]
		public int id { get; set; }
		[DataMember]
		public int existingPlanId { get; set; }

		[DataMember]
		public int newPlanId { get; set; }

		[DataMember]
		[StringLength(250)]
		[Display(Name = "existingDeviceArticleId")]
		public string existingDeviceArticleId { get; set; }

		[DataMember]
		[StringLength(250)]
		[Display(Name = "newDeviceArticleId")]
		public string newDeviceArticleId { get; set; }

		[DataMember]
		[StringLength(250)]
		[Display(Name = "existingVASId")]
		public string existingVASId { get; set; }

		[DataMember]
		[StringLength(250)]
		[Display(Name = "newVASId")]
		public string newVASId { get; set; }

		[DataMember]
		[StringLength(10)]
		[Display(Name = "validateComponent")]
		public string validateComponent { get; set; }

		[DataMember]
		[StringLength(10)]
		[Display(Name = "validateCriteria")]
		public string validateCriteria { get; set; }

		[DataMember]
		[StringLength(250)]
		[Display(Name = "ruleMessageLabel")]
		public string ruleMessageLabel { get; set; }

		[DataMember]
		[StringLength(10)]
		[Display(Name = "isActive")]
		public string isActive { get; set; }

	}
	#endregion

    #region 27052015 - LOV table for all configuration - Lus
    [DataContract]
    [Serializable]
    [Table("refLov")]
    public class refLovList
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        [StringLength(50)]
        public string Type { get; set; }
        [DataMember]
        [StringLength(50)]
        public string Source { get; set; }
        [DataMember]
        [StringLength(50)]
        public string Value { get; set; }
        [DataMember]
        public bool Active { get; set; }
    }
    #endregion

	#region Device Financing Table Reference

	[DataContract]
	[Serializable]
	[Table("tblDeviceFinancingInfo")]
	public class DeviceFinancingInfo
	{
		[DataMember]
		[Key]
		public int ID { get; set; }

		[DataMember]
		public string ArticleID { get; set; }

		[DataMember]
		public int ModelID { get; set; }

		[DataMember]
		public int PlanID { get; set; }

		[DataMember]
		[Range(0, 100000)]
		public decimal MonthlyRemain { get; set; }

		[DataMember]
		[Range(0, 100000)]
		public decimal MonthlyPrice { get; set; }

		[DataMember]
		public DateTime OfferFromDateTime { get; set; }

		[DataMember]
		public DateTime OfferToDateTime { get; set; }

		[DataMember]
		[Range(0, 100000)]
		public decimal UpgradeFee { get; set; }

		[DataMember]
		[Range(0, 100000)]
		public decimal OneTimeUpgrade { get; set; }

		[DataMember]
		[Range(0, 100000)]
		public int Tenure { get; set; }

		[DataMember]
		public DateTime MOCFromDateTime { get; set; }

		[DataMember]
		public DateTime MOCToDateTime { get; set; }

        [DataMember]
        [Range(0, 100000)]
        public decimal discount_amount { get; set; }

		[DataMember]
		public string UOMCode { get; set; }
	}

	#endregion

    #region 23072015 - Anthony - Reference Table [Accessory CR]

    [DataContract]
    [Serializable]
    [Table("refAccessory")]
    public class Accessory
    {
        [DataMember]
        [Key]
        public string ArticleID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Attribute { get; set; }

        [DataMember]
        public string EANCode { get; set; }

        [DataMember]
        public string ImageURL { get; set; }

        [DataMember]
        public bool IsKnockOffRequired { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("lnkOfferAccessory")]
    public class OfferAccessory
    {
        [DataMember]
        [Key]
        [Column(Order = 1)]
        public string ArticleID { get; set; }

        [DataMember]
        [Key]
        [Column(Order = 2)]
        public string UOMCode { get; set; }

        [DataMember]
        public string OfferName { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public decimal OfferPrice { get; set; }

        [DataMember]
        public DateTime OfferStartDT { get; set; }

        [DataMember]
        public DateTime OfferEndDT { get; set; }

		[DataMember]
		public bool isAccFinancing { get; set; }

        [DataMember]
        public int MarketCode { get; set; }

        [DataMember]
        public int AccountCategory { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("lnkOrgAccessory")]
    public class OrgAccessory
    {
        [DataMember]
        [Key]
        [Column(Order = 1)]
        public string ArticleID { get; set; }

        [DataMember]
        [Key]
        [Column(Order = 2)]
        public string UOMCode { get; set; }

        [DataMember]
        public int OrgID { get; set; }

        [DataMember]
        public bool Active { get; set; }
    }

	[DataContract]
	[Serializable]
	public class AccFinancingInfo
	{
		[DataMember]
		public string ArticleID { get; set; }

		[DataMember]
		public string UOMCode { get; set; }

		[DataMember]
		public string OfferName { get; set; }

		[DataMember]
		public string DisplayName { get; set; }

		[DataMember]
		[Range(0, 100000)]
		public decimal MonthlyRemain { get; set; }

		[DataMember]
		[Range(0, 100000)]
		public decimal MonthlyPrice { get; set; }

		[DataMember]
		[Range(0, 100000)]
		public decimal Discount_amount { get; set; }

		[DataMember]
		public int Tenure { get; set; }

		[DataMember]
		[Range(0, 100000)]
		public decimal RRPPrice { get; set; }
	}

    #endregion    
}
