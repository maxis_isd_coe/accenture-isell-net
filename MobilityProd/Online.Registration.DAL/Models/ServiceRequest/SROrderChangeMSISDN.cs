﻿using Online.Registration.DAL.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest
{
    [DataContract]
    [Serializable]
    [Table("tblSROrderChangeMSISDNDtl")]
    public class SROrderChangeMSISDNDtl:SROrderDtl
    {
        [DataMember]
        [Required]
        public int CustId { get; set; }

        [DataMember]
        [ForeignKey("CustId")]
        public virtual Customer Customer { get; set; }

        [Required]
        [DataMember]
        public bool CustBiometricPass { get; set; }

        [DataMember]
        [Required]
        [StringLength(15)]
        [MinLength(10)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "MSISDN must be a number.")]
        public string MSISDN { get; set; }

        [DataMember]
        [StringLength(15)]
        [MinLength(10)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "New MSISDN must be a number.")]
        [Required]
        public string NewMSISDN { get; set; }

        [DataMember]
        [StringLength(30)]
        public string AccountNo { get; set; }

        [DataMember]
        [ForeignKey("CustAddrId")]
        public virtual CommonAddress Address { get; set; }

        [DataMember]
        [Required]
        public int CustAddrId { get; set; }
    }
}
