﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest.Enums
{
    /// <summary>
    /// Change Status Types
    /// </summary>
    public enum ChangeStatusTypes
    {
        Suspension=1,
        Termination=2
    }
}
