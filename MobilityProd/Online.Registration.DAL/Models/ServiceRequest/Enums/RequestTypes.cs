﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest.Enums
{
    /// <summary>
    /// Enum type for Request Types, only use by Service Request module
    /// </summary>
    [DataContract]
    [Serializable]
    [Flags]
    public enum RequestTypes
    {

        /// <summary>
        /// Third party authorization
        /// </summary>
        [EnumMember]
        ThridPartyAuthorization =1,
        /// <summary>
        /// Transfer ownership of services to new owner, could be new new customer or existing customer
        /// </summary>
        [EnumMember]
        TransferOwnerShip =2,
        /// <summary>
        /// Postpaid customer switch to prepaid customer
        /// </summary>
        [EnumMember]
        Postpaid2Prepaid =3,
        /// <summary>
        /// Prepaid customer switch to post paid customer, require more postpaid plan in order
        /// </summary>
        [EnumMember]
        Prepaid2Postpaid =4,
        /// <summary>
        /// Split the billing address from existing account, or to new kenan account
        /// </summary>
        [EnumMember]
        BillSeparation =5,
        /// <summary>
        /// Corperate sim replacement, record purely the request only
        /// </summary>
        [EnumMember]
        CorpSimReplace =6,
        /// <summary>
        /// Change MSISDN, record purely the request only
        /// </summary>
        [EnumMember]
        ChangeMSISDN = 7,
        /// <summary>
        /// Change Status, record purely the request only
        /// </summary>
        [EnumMember]
        ChangeStatus = 8
    }
}
