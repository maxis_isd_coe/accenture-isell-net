﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest.Enums
{
    /// <summary>
    /// Service request status type
    /// </summary>
    [DataContract]
    [Serializable]
    [Flags]
    public enum SRStatus
    {
        /// <summary>
        /// When new order created, allow to process to completed,cancelled or reject
        /// </summary>
        [EnumMember]
        New =1,
        /// <summary>
        /// Change to completed after fulfillment team process it, it will be end of record cycle
        /// </summary>
        [EnumMember]
        Completed =2,
        /// <summary>
        /// After record created, user request to cancel it, no further action require
        /// </summary>
        [EnumMember]
        Cancelled =3,
        /// <summary>
        /// During fulfillment process, processing team reject it due to business constraint
        /// </summary>
        [EnumMember]
        Rejected =4
    }
}
