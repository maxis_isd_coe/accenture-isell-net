﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest.Enums
{
    /// <summary>
    /// Document types that use in iSell
    /// </summary>
    [DataContract]
    [Serializable]
    [Flags]
    public enum DocTypes
    {
        /// <summary>
        /// Registration form
        /// </summary>
        [EnumMember]
        Contract=1,

        /// <summary>
        /// Ic front scan picture
        /// </summary>
        [EnumMember]
        IdFront=2,

        /// <summary>
        /// Ic back scan picture
        /// </summary>
        [EnumMember]
        IdBack=3,

        /// <summary>
        /// Other scan document
        /// </summary>
        [EnumMember]
        OtherDoc=4,

        /// <summary>
        /// Scan contract
        /// </summary>
        [EnumMember]
        ScanContract=5,

        /// <summary>
        /// Letter of authorization
        /// </summary>
        [EnumMember]
        LetterOfAuth = 6,

        /// <summary>
        /// Third Party Id front
        /// </summary>
        [EnumMember]
        ThirdPartyIdFront = 7,

        /// <summary>
        /// Third Party Id back
        /// </summary>
        [EnumMember]
        ThirdPartyIdBack = 8,

        /// <summary>
        /// Transferee Id front
        /// </summary>
        [EnumMember]
        TransfereeIdFront = 9,

        /// <summary>
        /// Transferee Id back
        /// </summary>
        [EnumMember]
        TransfereeIdBack = 10

    }
}
