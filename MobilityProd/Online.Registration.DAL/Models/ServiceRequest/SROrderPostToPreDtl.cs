﻿using Online.Registration.DAL.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest
{
    [DataContract]
    [Serializable]
    [Table("tblSROrderPostToPreDtl")]
    public class SROrderPostToPreDtl:SROrderDtl
    {
        [DataMember]
        [Required]
        public int CustId { get; set; }

        [DataMember]
        //[Association("tblCustomer", "CustId", "")]
        [ForeignKey("CustId")]
        public virtual Customer Customer { get; set; }

        [Required]
        [DataMember]
        public bool CustBiometricPass { get; set; }

        [DataMember]
        [Required]
        public int CustAddrId { get; set; }

        [DataMember]
        //Association with Address table
        [ForeignKey("CustAddrId")]
        public virtual CommonAddress CustAddress { get; set; }

        [DataMember]
        [Required]
        [StringLength(15)]
        [MinLength(10)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "MSISDN must be a number.")]
        public string CustMSISDN { get; set; }

        [DataMember]
        [StringLength(30)]
        [Required]
        public string CustName { get; set; }

        [DataMember]
        [StringLength(10)]
        [Required]
        public string CustIDType { get; set; }

        [DataMember]
        [StringLength(14)]
        [Required]
        public string CustIDNumber { get; set; }

        [DataMember]
        [Required]
        public string PrepaidSIMTypeCode { get; set; }

        [DataMember]
        [StringLength(19)]
        [Required]
        public string PrepaidSIMSerial { get; set; }

        [DataMember]
        [StringLength(150)]
        [Required]
        public string PostpaidPlan { get; set; }

        [DataMember]
        [StringLength(10)]
        [Required]
        public string PostpaidAccNumber { get; set; }

        [DataMember]
        [StringLength(150)]
        [Required]
        public string PrepaidPlan { get; set; }

        [DataMember]
        [StringLength(10)]
        public string PrepaidAccNumber { get; set; }

        [DataMember]
        [Required]
        public decimal OutstandingAmount { get; set; }

        [DataMember]
        [Required]
        public decimal EarlyTerminateFee { get; set; }
    }
}
