﻿using Online.Registration.DAL.Models.Common;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest
{
    [DataContract]
    [Serializable]
    [Table("tblSROrderMaster")]
    public class SROrderMaster:CommonBase
    {
        public SROrderMaster()
        {
            VerificationDocs = new List<VerificationDoc>();
        }

        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string OrderId { get;private set; }

        [DataMember]
        [Required]
        public virtual RequestTypes RequestType { get; set; }

        [DataMember]
        public int RequestTypeId {
            get {
                return (int)RequestType;
            }
            set {
                RequestType = (RequestTypes)value;
            }
        }

        [DataMember]
        [Required]
        public virtual SRStatus Status { get; set; }

        [DataMember]
        public int StatusId{
            get{
                return (int)Status;
            }
            set{
                Status = (SRStatus)value;
            }
        }

        [DataMember]
        public DateTime? CompleteDt { get; set; }

        [DataMember]
        public string CompleteBy { get; set; }

        [DataMember]
        public DateTime? LockDt { get; set; }

        [DataMember]
        public string LockBy { get; set; }

        [Required]
        [DataMember]
        public int CenterOrgId { get; set; }

        [DataMember]
        [ForeignKey("CenterOrgId")]
        public virtual Organization CenterOrg { get; set; }

        [DataMember]
        [NotMapped]
        public virtual SROrderDtl Detail { get; set;}

        [DataMember]
        [NotMapped]
        public virtual List<VerificationDoc> VerificationDocs { get; set; }
    }
}
