﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Models.FilterCriterias;

namespace Online.Registration.DAL.Models.ServiceRequest.Dto
{
    [DataContract]
    [Serializable]
    [KnownType(typeof(IFilterCriteria))]
    [KnownType(typeof(FilterCriteria))]
    [KnownType(typeof(TodayFilterCriteria))]
    [KnownType(typeof(StartDateFilterCriteria))]
    [KnownType(typeof(EndDateFilterCriteria))]
    public class SRQueryOrderDto
    {
        [DataMember]
        public List<IFilterCriteria> CriteriaList { get; set; }

        [DataMember]
        public bool? SortDesc { get; set; }

        [DataMember]
        public string SortyBy { get; set; }

        [DataMember]
        public bool FullRecord { get; set; }

        [DataMember]
        public string LoginId { get; set; }

        [DataMember]
        public int CenterOrgId { get; set; }
    }
}
