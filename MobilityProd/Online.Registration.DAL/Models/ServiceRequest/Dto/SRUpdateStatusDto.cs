﻿using Online.Registration.DAL.Models.ServiceRequest.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest.Dto
{
    /// <summary>
    /// Data transfer object from outside to DAL
    /// </summary>
    [DataContract]
    [Serializable]
    public class SRUpdateStatusDto
    {
        [DataMember]
        [Required]
        public string OrderId { get; set; }

        [DataMember]
        [Required]
        public string UpdateBy { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        [Required]
        public SRStatus Status { get; set; }

        [DataMember]
        public string AccountNo { get; set; }
    }
}
