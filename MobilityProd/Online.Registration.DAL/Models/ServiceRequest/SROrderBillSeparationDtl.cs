﻿using Online.Registration.DAL.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest
{
    [DataContract]
    [Serializable]
    [Table("tblSROrderBillSeparationDtl")]
    public class SROrderBillSeparationDtl:SROrderDtl
    {
        [DataMember]
        [Required]
        public int CustId { get; set; }

        [DataMember]
        //[Association("tblCustomer", "CustId", "")]
        [ForeignKey("CustId")]
        public virtual Customer Customer { get; set; }

        [Required]
        [DataMember]
        public bool CustBiometricPass { get; set; }

        [DataMember]
        [Required]
        public int NewCustAddrId { get; set; }

        [DataMember]
        //Association with Address table for new customer address
        [ForeignKey("NewCustAddrId")]
        public virtual CommonAddress NewAddress { get; set; }

        [DataMember]
        [Required]
        public int CurrCustAddrId { get; set; }

        [DataMember]
        //Association with Address table
        [ForeignKey("CurrCustAddrId")]
        public virtual CommonAddress CurrAddress { get; set; }

        [DataMember]
        [Required]
        [StringLength(15)]
        [MinLength(10)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "MSISDN must be a number.")]
        public string CustMSISDN { get; set; }

        [DataMember]
        [StringLength(30)]
        [Required]
        public string CustAcctNumber { get; set; }

        [DataMember]
        [StringLength(150)]
        [Required]
        public string CustCurrentPlan{ get; set; }

        [DataMember]
        [StringLength(15)]
        [MinLength(10)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "MSISDN must be a number.")]
        public string ExistMSISDN { get; set; }

        //Name wrong should relook
        [DataMember]
        [StringLength(30)]
        public string ExistAcctNumber { get; set; }

        [DataMember]
        public bool IsSplitNewAcc { get; set; }
    }
}
