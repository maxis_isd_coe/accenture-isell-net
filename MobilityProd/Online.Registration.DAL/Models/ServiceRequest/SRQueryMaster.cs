﻿using Online.Registration.DAL.Models.ServiceRequest.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace Online.Registration.DAL.Models.ServiceRequest
{
    [DataContract]
    [Serializable]
    [Table("vwSRQueryMaster")]
    public class SRQueryMaster
    {
        [DataMember]
        [Key]
        public string OrderId { get; set; }

        [DataMember]
        public int RequestTypeId { get; set; }

        [DataMember]
        public virtual RequestTypes RequestType
        {
            get{
                return (RequestTypes)RequestTypeId;
            }
            set{
                RequestTypeId = (int)value;
            }
        }

        [DataMember]
        public string IdCardNo { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public DateTime CreateDt { get; set; }

        [DataMember]
        public string CreateBy { get; set; }

        [DataMember]
        public DateTime? UpdateDt { get; set; }

        [DataMember]
        public string UpdateBy { get; set; }

        [DataMember]
        public string QueueNo { get; set; }

        [DataMember]
        public int StatusId { get; set; }

        [DataMember]
        public virtual SRStatus Status
        {
            get{
                return (SRStatus)StatusId;
            }
            set{
                StatusId = (int)value;
            }
        }

        [DataMember]
        public string LockBy { get; set; }

        [DataMember]
        public int CenterOrgId { get; set; }
    }
}
