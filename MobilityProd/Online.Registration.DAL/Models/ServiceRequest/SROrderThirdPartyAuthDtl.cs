﻿using Online.Registration.DAL.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest
{
    [DataContract]
    [Serializable]
    [Table("tblSROrderThirdPartyAuthDtl")]
    public class SROrderThirdPartyAuthDtl:SROrderDtl
    {
        [DataMember]
        [Required]
        public int CustId { get; set; }

        [DataMember]
        //[Association("tblCustomer", "CustId", "")]
        [ForeignKey("CustId")]
        public virtual Customer Customer { get; set; }

        [Required]
        [DataMember]
        public bool CustBiometricPass { get; set; }

        [DataMember]
        [Required]
        [StringLength(15)]
        [MinLength(10)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "MSISDN must be a number.")]
        public string MSISDN { get; set; }

        [DataMember]
        [StringLength(30)]
        [Required]
        public string AccountNo { get; set; }

        [DataMember]
        [StringLength(10)]
        [Required]
        public string ThirdPartyIdType { get; set; }

        [DataMember]
        [StringLength(14)]
        [Required]
        public string ThirdPartyIdNumber { get; set; }

        [DataMember]
        [StringLength(30)]
        [Required]
        public string ThirdPartyName { get; set; }

        [DataMember]
        [Required]
        public int ThirdPartyAuthTypeId { get; set; }

        [Required]
        [DataMember]
        public bool ThirdPartyBiometricPass { get; set; }

        [DataMember]
        [Required]
        public int CustAddrId { get; set; }

        [DataMember]
        //Association with Address table
        [ForeignKey("CustAddrId")]
        public virtual CommonAddress CustAddress { get; set; }

    }
}
