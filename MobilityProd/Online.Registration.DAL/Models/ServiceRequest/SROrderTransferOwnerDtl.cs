﻿using Online.Registration.DAL.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest
{
    [DataContract]
    [Serializable]
    [Table("tblSROrderTransferOwnerDtl")]
    public class SROrderTransferOwnerDtl:SROrderDtl
    {
        [DataMember]
        [Required]
        public int TransferorCustId { get; set; }

        [DataMember]
        [ForeignKey("TransferorCustId")]
        public virtual Customer TransferorCustomer { get; set; }

        [DataMember]
        [Required]
        public int TransfereeCustId { get; set; }

        [DataMember]
        [ForeignKey("TransfereeCustId")]
        public virtual Customer TransfereeCustomer { get; set; }

        [Required]
        [DataMember]
        public bool Transferor_CustBiometricPass { get; set; }

        [Required]
        [DataMember]
        public bool Transferee_CustBiometricPass { get; set; }

        [DataMember]
        [Required]
        public int TransferorAddrId { get; set; }

        [DataMember]
        //Association with Address table
        [ForeignKey("TransferorAddrId")]
        public virtual CommonAddress TransferorAddress { get; set; }

        [DataMember]
        [Required]
        public int TransfereeAddrId { get; set; }

        [DataMember]
        //Association with Address table
        [ForeignKey("TransfereeAddrId")]
        public virtual CommonAddress TransfereeAddress { get; set; }

        [DataMember]
        [Required]
        [StringLength(15)]
        [MinLength(10)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "MSISDN must be a number.")]
        public string TransferorMSISDN { get; set; }

        [DataMember]
        [StringLength(30)]
        [Required]
        public string TransferorAcctNumber { get; set; }

        [DataMember]
        [Required]
        public string TransferorVAS { get; set; }

        [DataMember]
        [StringLength(100)]
        [Required]
        public string TransferorPlan { get; set; }

        [DataMember]
        [StringLength(15)]
        [MinLength(10)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "MSISDN must be a number.")]
        public string TransfereeMSISDN { get; set; }

        [DataMember]
        [StringLength(30)]
        public string TransfereeAcctNumber { get; set; }

        [DataMember]
        public string TransfereeSignature { get; set; }

        [DataMember]
        public DateTime? TransfereeSignDt { get; set; }

        [DataMember]
        public bool IsTransferNewAcc { get; set; }
    }
}
