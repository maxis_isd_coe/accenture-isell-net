﻿using Online.Registration.DAL.Models.Common;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest
{
    [DataContract]
    [Serializable]
    [Table("tblSROrderChangeStatusDtl")]
    public class SROrderChangeStatusDtl:SROrderDtl
    {
        [DataMember]
        [Required]
        public int CustId { get; set; }

        [DataMember]
        [ForeignKey("CustId")]
        public virtual Customer Customer { get; set; }

        [Required]
        [DataMember]
        public bool CustBiometricPass { get; set; }

        [DataMember]
        [Required]
        [StringLength(15)]
        [MinLength(10)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "MSISDN must be a number.")]
        public string MSISDN { get; set; }

        [DataMember]
        [StringLength(30)]
        public string AccountNo { get; set; }

        [DataMember]
        public virtual ChangeStatusTypes ChangeStatusType { get; set; }

        [DataMember]
        [Required]
        public int ChangeStatusTypeId { 
            get{
                return (int)ChangeStatusType;
            } set{
                ChangeStatusType = (ChangeStatusTypes)value;
            } 
        }

        [DataMember]
        [ForeignKey("CustAddrId")]
        public virtual CommonAddress Address { get; set; }

        [DataMember]
        [Required]
        public int CustAddrId { get; set; }

        [DataMember]
        public int ContractRemainingMonths { get; set; }

        [DataMember]
        public decimal EarlyTerminationFee { get; set; }
    }
}
