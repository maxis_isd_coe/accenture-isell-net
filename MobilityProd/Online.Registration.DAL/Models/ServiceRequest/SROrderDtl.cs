﻿using Online.Registration.DAL.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.ServiceRequest
{
    /// <summary>
    /// Represent share property for SROrder details
    /// Need add knowtype for child class to make it recognize child type
    /// </summary>
    [KnownType(typeof(SROrderCorpSimRepDtl))]
    [KnownType(typeof(SROrderBillSeparationDtl))]
    [KnownType(typeof(SROrderThirdPartyAuthDtl))]
    [KnownType(typeof(SROrderPostToPreDtl))]
    [KnownType(typeof(SROrderPreToPostDtl))]
    [KnownType(typeof(SROrderTransferOwnerDtl))]
    [KnownType(typeof(SROrderChangeMSISDNDtl))]
    [KnownType(typeof(SROrderChangeStatusDtl))]
    [DataContract]
    [Serializable]
    public abstract class SROrderDtl
    {
        [DataMember]
        [Key]
        public int Id { get; set; }

        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        [StringLength(300)]
        public string Remarks { get; set; }

        [DataMember]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Queue No must be a number.")]
        public string QueueNo { get; set; }

        [DataMember]
        public string SignatureSvg { get; set; }

        [DataMember]
        public DateTime? SignDt { get; set; }

        [DataMember]
        [StringLength(300)]
        public string Justification { get; set; }
    }
}
