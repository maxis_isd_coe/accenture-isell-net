﻿using Online.Registration.DAL.Models.Common;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Online.Registration.DAL.Models
{
    /// <summary>
    /// Search as DbContext class for Service Request entities only
    /// </summary>
    public partial class OnlineStoreDbContext
    {
        public DbSet<SROrderMaster> SROrderMasters{ get;set;}

        public DbSet<SROrderCorpSimRepDtl> SROrderCorpSimRepDtls { get; set; }

        public DbSet<SROrderBillSeparationDtl> SROrderBillSeparationDtls { get; set; }

        public DbSet<SROrderThirdPartyAuthDtl> SROrderThirdPartyAuthDtls { get; set; }

        public DbSet<SROrderPostToPreDtl> SROrderPostToPreDtls { get; set; }

        public DbSet<SROrderPreToPostDtl> SROrderPreToPostDtls { get; set; }

        public DbSet<SROrderTransferOwnerDtl> SROrderTransferOwnerDtls { get; set; }

        public DbSet<SROrderChangeMSISDNDtl> SROrderChangeMSISDNDtls { get; set; }

        public DbSet<SROrderChangeStatusDtl> SROrderChangeStatusDtls { get; set; }

        public DbSet<CommonAddress> CommonAddresses { get; set; }

        public DbSet<VerificationDoc> VerificationDocs { get; set; }

        public DbSet<SRQueryMaster> SRQueryMasters { get; set; }
    }
}
