﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Online.Registration.DAL.Models
{
    #region Complain

    [DataContract]
    [Serializable]
    [Table("trnComplain")]
    public class Complain : BaseEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required, StringLength(15)]
        public string MSISDN { get; set; }

        [DataMember]
        [Required, StringLength(200)]
        public string CustomerName { get; set; }

        [DataMember]
        [StringLength(100)]
        public string EmailAddr { get; set; }

        [DataMember]
        public DateTime? DOB { get; set; }

        [DataMember]
        [StringLength(25)]
        public string TelNo { get; set; }

        [DataMember]
        [StringLength(50)]
        public string IDNumber { get; set; }

        [DataMember]
        [StringLength(50)]
        public string IDType { get; set; }

        [DataMember]
        [StringLength(1000)]
        public string Address { get; set; }

        [DataMember]
        [Required, StringLength(30)]
        public string AccountNo { get; set; }

        [DataMember]
        [Required, Column(TypeName = "ntext")]
        public string Remark { get; set; }

        [DataMember]
        [StringLength(20)]
        public string CMSSInteractionID { get; set; }

        [DataMember]
        [StringLength(20)]
        public string CMSSCaseID { get; set; }

        [DataMember]
        [Required]
        public int TranxID { get; set; }

        [DataMember]
        public int? IntTranxID { get; set; }

        [DataMember]
        public int? ComplaintGroupID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComplainFind : FindBase
    {
        [DataMember]
        public Complain Complain { get; set; }
        [DataMember]
        public DateTime? CreateDateFrom { get; set; }
        [DataMember]
        public DateTime? CreateDateTo { get; set; }
    }

    #endregion

    #region Complaint Issues

    [DataContract]
    [Serializable]
    [Table("refComplaintIssues")]
    public class ComplaintIssues : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [StringLength(100)]
        [Required]
        public string Name { get; set; }

        [DataMember]
        [StringLength(4000)]
        public string Description { get; set; }

        [DataMember]
        [StringLength(20)]
        [Required]
        public string Type { get; set; }
    }


    [DataContract]
    [Serializable]
    public class ComplaintIssuesFind : FindBase
    {
        //public ComplaintIssuesFind()
        //{
        //    ComplaintIssues = new ComplaintIssues();
        //    CaseDispatchQIDs =  new List<int>();
        //}
        [DataMember]
        public ComplaintIssues ComplaintIssues { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public List<int> CaseDispatchQIDs { get; set; }
    }

    #endregion

    #region Complaint Issues Group

    [DataContract]
    [Serializable]
    [Table("lnkComplaintIssuesGroup")]
    public class ComplaintIssuesGroup : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        // [Required(ErrorMessage = "Product")]
        public int ProductID { get; set; }

        [DataMember]
        // [Required(ErrorMessage = "Reason1")]
        public int Reason1ID { get; set; }

        [DataMember]
        // [Required(ErrorMessage = "Reason2")]
        public int Reason2ID { get; set; }

        [DataMember]
        // [Required(ErrorMessage = "Reason3")]
        public int Reason3ID { get; set; }

        [DataMember]
        // [Required(ErrorMessage = "Queue")]
        public int QueueID { get; set; }

        [DataMember]
        public int DynamicFormTextID { get; set; }

        [DataMember]
        public int Title { get; set; }

        [DataMember]
        public int SystemName { get; set; }

        [DataMember]
        public int InteractionType { get; set; }

        [DataMember]
        public int Direction { get; set; }

        [DataMember]
        public int CasePriority { get; set; }

        [DataMember]
        public int CaseComplexity { get; set; }

    }

    [DataContract]
    [Serializable]
    public class ComplaintIssuesGroupFind : FindBase
    {
        [DataMember]
        public ComplaintIssuesGroup ComplaintIssuesGroup { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Issue Group Dispatch Queue

    [DataContract]
    [Serializable]
    [Table("lnkIssueGrpDispatchQ")]
    public class IssueGrpDispatchQ : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        public int IssueGrpID { get; set; }

        [DataMember]
        [Required]
        public int CaseDispatchQID { get; set; }

        [DataMember]
        public int TranxID { get; set; }

        [DataMember]
        public int? IntTranxID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IssueGrpDispatchQFind : FindBase
    {
        [DataMember]
        public IssueGrpDispatchQ IssueGrpDispatchQ { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public List<int> ComplaintIssuesGrpIDs { get; set; }
    }

    #endregion

    #region CMSS
    [DataContract]
    [Serializable]
    [Table("trnComplain")]
    public class CMSSComplainGet
    {
        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string CMSSCaseID { get; set; }

        [DataMember]
        public DateTime CreateDt { get; set; }

        [DataMember]
        public string IDNumber { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string LastAccessID { get; set; }


    }

    [DataContract]
    [Serializable]
    [Table("trnComplain")]
    public class CMSSComplainFInd
    {
        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string CMSSCaseID { get; set; }

        [DataMember]
        public DateTime CreateDt { get; set; }

        [DataMember]
        public DateTime ToDt { get; set; }



    }


    #endregion

}
