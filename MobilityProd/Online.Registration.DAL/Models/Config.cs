﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Online.Registration.DAL.Models
{

    #region User Group Access

    [DataContract]
    [Serializable]
    [Table("lnkUserGrpAccess")]
    public class UserGrpAccess : ActiveEntity
    {
        [Key]
        [DataMember]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "User Group *")]
        public int UserGrpID { get; set; }

        [Association("refUserGroup", "UserGrpID", "")]
        public virtual UserGroup UserGrp { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Access *")]
        public int AccessID { get; set; }

        [Association("tblAccess", "AccessID", "")]
        public virtual Access UserAccess { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpAccessFind : FindBase
    {
        [DataMember]
        public UserGrpAccess UserGrpAccess { get; set; }

        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region User Group Role

    [DataContract]
    [Serializable]
    [Table("lnkUserGrpRole")]
    public class UserGrpRole : ActiveEntity
    {
        [Key]
        [DataMember]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "User Group *")]
        public int UserGrpID { get; set; }

        [Association("refUserGroup", "UserGrpID", "")]
        public virtual UserGroup UserGrp { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "User Role *")]
        public int UserRoleID { get; set; }

        [Association("refUserRole", "UserRoleID", "")]
        public virtual UserRole UserRole { get; set; }

    }

    [DataContract]
    [Serializable]
    public class UserGrpRoleFind : FindBase
    {
        [DataMember]
        public UserGrpRole UserGrpRole { get; set; }

        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Org Type

    [DataContract]
    [Serializable]
    [Table("refOrgType")]
    public class OrgType : ActiveEntity
    {
        // Dealer, Vendor, Distributor, Center, install
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(4000)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Can Fulfill Order? *")]
        public bool CanFulfill { get; set; }    //Can Fulfill 

        [DataMember]
        [Required]
        [Display(Name = "Need To Allocate Stock? *")]
        public bool CanAllocate { get; set; }    //Can Allocate 

        [DataMember]
        [Required]
        [Display(Name = "Can Supply Stock? *")]
        public bool CanSupply { get; set; }    //Can Allocate 

    }

    [DataContract]
    [Serializable]
    public class OrgTypeFind : FindBase
    {
        [DataMember]
        public OrgType OrgType { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region User Group

    [DataContract]
    [Serializable]
    [Table("refUserGroup")]
    public class UserGroup : ActiveEntity
    {
        [Key]
        [DataMember]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Organization *")]
        public int OrgID { get; set; }

        [Association("tblOrganization", "OrgID", "")]
        public virtual Organization Org { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s,.()@-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGroupFind : FindBase
    {
        [DataMember]
        public UserGroup UserGroup { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region User Role

    [DataContract]
    [Serializable]
    [Table("refUserRole")]
    public class UserRole : ActiveEntity
    {
        [Key]
        [DataMember]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [StringLength(20)]
        [Required]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [Required]
        [DataMember]
        [StringLength(250)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s,.()&/@-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserRoleFind : FindBase
    {
        [DataMember]
        public UserRole UserRole { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Access

    [DataContract]
    [Serializable]
    [Table("tblAccess")]
    public class Access : ActiveEntity
    {
        [Key]
        [DataMember]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "User *")]
        public int UserID { get; set; }

        [Association("tblUser", "UserID", "")]
        public virtual User User { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "User Name *")]
        [StringLength(25, MinimumLength = 4)]
        public string UserName { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Password *")]
        [StringLength(40)]
        public string Password { get; set; }

        [DataMember]
        [Display(Name = "Password Change?")]
        public bool ChangePassword { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "LDAP Login?")]
        public bool LDAPLogin { get; set; }

        [DataMember]
        [Display(Name = "Description")]
        [StringLength(250)]
        public string Description { get; set; }

        [DataMember]
        public bool IsLockedOut { get; set; }

        [DataMember]
        public int FailedPasswordAttemptCount { get; set; }

        [DataMember]
        public DateTime? FailedPasswordAttemptWindowStart { get; set; }
        //added by nreddy for AgentCode
        [DataMember]
        [Display(Name = "kenanAgentCode")]
        public string kenanAgentCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccessFind : FindBase
    {
        [DataMember]
        public Access Access { get; set; }
        [DataMember]
        public bool? ChangePassword { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Organization

    [DataContract]
    [Serializable]
    [Table("tblOrganization")]
    public class Organization : ActiveEntity
    {
        [Key]
        [DataMember]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Type *")]
        public int OrgTypeID { get; set; }

        [Association("refOrgType", "OrgTypeID", "")]
        public virtual OrgType OrgType { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "DealerCode *")]
        public string DealerCode { get; set; }

        [DataMember]
        [Required]
        [StringLength(4000)]
        [Display(Name = "Description *")]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Address *")]
        [RegularExpression(@"^[\w\s-#,;]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string AddrLine1 { get; set; }

        [DataMember]
        [StringLength(100)]
        [Display(Name = " ")]
        public string AddrLine2 { get; set; }

        [DataMember]
        [StringLength(6)]
        [Display(Name = "Postcode")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Postcode { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Town/City")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string TownCity { get; set; }

        [DataMember]
        [Display(Name = "State")]
        public int StateID { get; set; }

        [Association("refCountryState", "StateID", "")]
        public virtual State State { get; set; }

        [DataMember]
        [Display(Name = "Country")]
        public int CountryID { get; set; }

        [Association("refCountry", "CountryID", "")]
        public virtual Country Country { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Person In-charge")]
        public string PersonInCharge { get; set; }

        [DataMember]
        [StringLength(25)]
        [Display(Name = "Contact No")]
        [RegularExpression(@"^[0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string ContactNo { get; set; }

        [DataMember]
        [StringLength(25)]
        [Display(Name = "Fax No")]
        [RegularExpression(@"^[0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string FaxNo { get; set; }

        [DataMember]
        [StringLength(15)]
        [Display(Name = "Mobile No")]
        [RegularExpression(@"^[0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string MobileNo { get; set; }

        [DataMember]
        [StringLength(100)]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        // [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "Invalid email address.")]
        [RegularExpression(@"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$", ErrorMessage = "Invalid email address.")]
        public string EmailAddr { get; set; }

        //public virtual ICollection<Branch> Branches { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Is a HQ? *")]
        public bool IsHQ { get; set; }

        [DataMember]
        [StringLength(100)]
        [Display(Name = "SAP Branch Code")]
        public string SAPBranchCode { get; set; }

        [DataMember]
        [StringLength(100)]
        [Display(Name = "Kenan Code")]
        public string KenanRegCode { get; set; }

        [DataMember]
        [StringLength(10), Display(Name = "Kenan Service Provider *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string KenanServiceProviderID { get; set; }

        [DataMember]
        public int RegionID { get; set; }


        [DataMember]
        [StringLength(200)]
        [Display(Name = "WSDL Url")]
        public string WSDLUrl { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "OrganisationId")]
        public string OrganisationId { get; set; }

        #region Added By NarayanaReddy
        [DataMember]
        public int Inventry_OutOfStockLower { get; set; }
        [DataMember]
        public int Inventry_OutOfStockHigher { get; set; }
        [DataMember]
        public int Inventry_LimitedStockLower { get; set; }
        [DataMember]
        public int Inventry_LimitedStockHigher { get; set; }
        [DataMember]
        public int Inventry_AvailableStockLower { get; set; }
        [DataMember]
        public int Inventry_AvailableStockHigher { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "SalesChannelID")]
        public string SalesChannelID { get; set; }
        #endregion
        #region Added By Deepika
        [DataMember]
        public string CenterCode { get; set; }
    
        #endregion

		[DataMember]
		public int SMARTTeamID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class OrganizationFind : FindBase
    {
        [DataMember]
        public Organization Organization { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public bool? IsHQ { get; set; }
    }

    #endregion

    #region User

    [DataContract]
    [Serializable]
    [Table("tblUser")]
    public class User : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Full Name *")]
        public string FullName { get; set; }

        [DataMember]
        [Display(Name = "ID Card Type *")]
        [Required]
        public int IDCardTypeID { get; set; }

        [Association("refIDCardType", "IDCardTypeID", "")]
        public virtual IDCardType IDCardType { get; set; }

        [DataMember]
        [StringLength(15)]
        [Required]
        [Display(Name = "ID Card No *")]
        public string IDCardNo { get; set; }

        [DataMember]
        [Required]
        [StringLength(15)]
        [RegularExpression(@"^(\(?\+?[0-9] *\)?)?[0-9_\- \(\)] *$", ErrorMessage = "Invalid characters.")]
        [Display(Name = "Mobile No *")]
        public string MobileNo { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Email *")]
        [StringLength(100)]
        // [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "Invalid email address.")]
        [RegularExpression(@"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$", ErrorMessage = "Invalid email address.")]
        public string EmailAddr { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Organization *")]
        public int OrgID { get; set; }

        [Association("tblOrganization", "OrgID", "")]
        public virtual Organization Org { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "isDealer*")]
        public bool isDealer { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserFind : FindBase
    {
        [DataMember]
        public User User { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserFindbyName : FindBase
    {
        [DataMember]
        public String UserName { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Market

    [DataContract]
    [Serializable]
    [Table("refMarket")]
    public class Market : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [StringLength(20)]
        [Required]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required, StringLength(20), Display(Name = "Kenan Code *")]
        public string KenanCode { get; set; }

        [DataMember]
        [Required, StringLength(100), Display(Name = "Name *")]
        public string Name { get; set; }

        [DataMember]
        [StringLength(500), Display(Name = "Description *")]
        public string Description { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MarketFind : FindBase
    {
        [DataMember]
        public Market Market { get; set; }

        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Account Category

    [DataContract]
    [Serializable]
    [Table("refAccountCategory")]
    public class AccountCategory : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [StringLength(20)]
        [Required]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required, StringLength(20), Display(Name = "Kenan Code *")]
        public string KenanCode { get; set; }

        [DataMember]
        [Required, StringLength(100), Display(Name = "Name *")]
        public string Name { get; set; }

        [DataMember]
        [StringLength(500), Display(Name = "Description *")]
        public string Description { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccountCategoryFind : FindBase
    {
        [DataMember]
        public AccountCategory AccountCategory { get; set; }

        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Status Type

    [DataContract]
    [Serializable]
    [Table("refStatusType")]
    public class StatusType : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "Type ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [MaxLength(10)]
        [Display(Name = "Type Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allowed.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [MaxLength(50)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allowed.")]
        public string Description { get; set; }

    }

    [DataContract]
    [Serializable]
    public class StatusTypeFind : FindBase
    {
        [DataMember]
        public StatusType StatusType { get; set; }

        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Status

    [DataContract]
    [Serializable]
    [Table("refStatus")]
    public class Status : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "Status ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Status Code *")]
        [Required]
        [StringLength(10)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [MaxLength(50)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Status Type *")]
        public int StatusTypeID { get; set; }

        [Association("refStatusType", "StatusTypeID", "")]
        public virtual StatusType StatusType { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Remark Required? *")]
        public bool HasRemark { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Status Date Required? *")]
        public bool HasStatusDate { get; set; }       
    }

    [DataContract]
    [Serializable]
    public class StatusFind : FindBase
    {
        [DataMember]
        public Status Status { get; set; }

        [DataMember]
        public bool? Active { get; set; }

        [DataMember]
        public string Role { get; set; }
    }

    #endregion

    #region Status Change

    [DataContract]
    [Serializable]
    [Table("lnkStatusChange")]
    public class StatusChange : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "From Status *")]
        [Required]
        public int FromStatusID { get; set; }

        [DataMember]
        [Display(Name = "To Status *")]
        [Required]
        public int ToStatusID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusChangeFind : FindBase
    {
        [DataMember]
        public StatusChange StatusChange { get; set; }

        [DataMember]
        public bool? Active { get; set; }

        [DataMember]
        public int FromToStatusID { get; set; }
    }

    #endregion

    #region Status Reason

    [DataContract]
    [Serializable]
    [Table("refStatusReason")]
    public class StatusReason : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Reason Code *")]
        [Required]
        [StringLength(10)]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Display(Name = "Name *")]
        [Required]
        [StringLength(150)]
        //        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonFind : FindBase
    {
        [DataMember]
        public StatusReason StatusReason { get; set; }

        [DataMember]
        public bool? Active { get; set; }

        [DataMember]
        public List<int> ReasonCodeIDs { get; set; }
    }

    #endregion

    #region Status Reason Code

    [DataContract]
    [Serializable]
    [Table("lnkStatusReasonCode")]
    public class StatusReasonCode : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Status *")]
        [Required]
        public int StatusID { get; set; }

        [DataMember]
        [Display(Name = "Reason *")]
        [Required]
        public int ReasonCodeID { get; set; }

        [DataMember]
        [Display(Name = "Remark")]
        [StringLength(4000)]
        public string Remark { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonCodeFind : FindBase
    {
        [DataMember]
        public StatusReasonCode StatusReasonCode { get; set; }

        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    /*  This code is writeen by VLTECH */
    #region Apistatusandmessages

    [DataContract]
    [Serializable]
    [Table("tblAPIStatusMessages")]
    public class tblAPIStatusMessages : TimedEntity
    {
        [Key]
        [DataMember]
        [Display(Name = "ID *")]
        public int ID { get; set; }


        [DataMember]
        [Display(Name = "APIName *")]
        public string APIName { get; set; }


        [Required]
        [DataMember]
        [Display(Name = "MethodName *")]
        public string MethodName { get; set; }


        [Required]
        [DataMember]
        [Display(Name = "Description *")]
        public string Description { get; set; }

        [DataMember]
        [Display(Name = "StatusCode *")]
        public string StatusCode { get; set; }

        [DataMember]
        [Display(Name = "StatusMessage *")]
        public string StatusMessage { get; set; }

        [DataMember]
        [Display(Name = "Status *")]
        public string Status { get; set; }

        [Association("tblAPICallStatus", "ID", "")]
        public virtual tblAPICallStatus objtblAPICallStatus { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("tblAPICallStatus")]
    public class tblAPICallStatus : TimedEntity
    {

        [Key]
        [DataMember]
        [Display(Name = "ID *")]
        public int ID { get; set; }


        [DataMember]
        [Display(Name = "APIName *")]
        public string APIName { get; set; }



        [DataMember]
        [Display(Name = "MethodName *")]
        public string MethodName { get; set; }


        [DataMember]
        [Display(Name = "RequestTime *")]
        public DateTime RequestTime { get; set; }


        [DataMember]
        [Display(Name = "ResponseTime *")]
        public DateTime ResponseTime { get; set; }


        [DataMember]
        [Display(Name = "UserName *")]
        public string UserName { get; set; }

        [DataMember]
        [Display(Name = "Status *")]
        public string Status { get; set; }

        [DataMember]
        public string BreXmlReq { get; set; }
        [DataMember]
        public string BreXmlRes { get; set; }
        [DataMember]
        public string IdCardNo { get; set; }

    }

    #endregion

    #region tblDonor
    [DataContract]
    [Serializable]
    [Table("tblDonor")]
    public class tblDonor : TimedEntity
    {
        [Key]
        [DataMember]
        [Display(Name = "ID *")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "DonorID *")]
        public string DonorID { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Donor *")]
        public string Donor { get; set; }
    }
    #endregion tblDonor

    #region UserTransactionLog added by Rajeswari on 25th Feb

    [DataContract]
    [Serializable]
    [Table("UserTransactionLog")]
    public class UserTransactionLog : TimedEntity
    {
        [Key]
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Action { get; set; }

        [DataMember]
        public string IDType { get; set; }

        [DataMember]
        public string IDNumber { get; set; }

        [DataMember]
        public string Result { get; set; }

        [DataMember]
        public DateTime LoggedDate { get; set; }

        [DataMember]
        public DateTime? StartDateTime { get; set; }

        [DataMember]
        public DateTime? EndDateTime { get; set; }

        [DataMember]
        public int RegId { get; set; }

        [DataMember]
        public int AccountCategory { get; set; }
    }


    #endregion

    #region WhiteListCustomers

    /*  This code is writeen by VLTECH */
    [DataContract]
    [Serializable]
    [Table("CampaignInfo")]
    public class UseeWhiteListCampaignInfo
    {
        [Key]
        [DataMember]
        [Display(Name = "CampaignId *")]
        public Int64 CampaignId { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Campaign *")]
        public string Campaign { get; set; }


        [Required]
        [DataMember]
        [Display(Name = "Description *")]
        public string Description { get; set; }

        [DataMember]
        [Association("WhiteListCustomers", "Campaignid", "")]
        public WhiteListCustomers UserWhiteListCustomers { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("WhiteListCustomers")]
    public class WhiteListCustomers
    {

        [DataMember, Required, Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "WhitelListID *")]
        public int WhitelListID { get; set; }

        [DataMember]
        [Display(Name = "CampaignId *")]
        public Int64 CampaignId { get; set; }

        [DataMember]
        [Display(Name = "ReferenceNo *")]
        public string ReferenceNo { get; set; }


        [Required]
        [DataMember]
        [Display(Name = "ICNo *")]
        public string ICNo { get; set; }


        [Required]
        [DataMember]
        [Display(Name = "MSISDN *")]
        public string MSISDN { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "AccountNo *")]
        public string AccountNo { get; set; }


        [DataMember]
        [Display(Name = "Option *")]
        public string Option { get; set; }

        [DataMember]
        [Display(Name = "CreatedDate *")]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        [Display(Name = "UpdatedDate *")]
        public DateTime UpdatedDate { get; set; }



    }





    #endregion

#region smart related methods
    [DataContract]
    [Serializable]
    [Table("tblTransactionTypes")]
    public class TransactionTypes
    {
        [Key]
        [DataMember]
        public Int32 ID { get; set; }
        [DataMember]
        public string Trn_Type_ID { get; set; }

        [DataMember]
        public string Trn_Type_Name { get; set; }

        [DataMember]
        public bool? Active { get; set; }
    }

#endregion

    [DataContract]
    [Serializable]
    [Table("lnkUserConfigurations")]
    public class UserConfigurations : ActiveEntity
    {
        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public int ActionID { get; set; }

        [DataMember]
        public string Action { get; set; }

        [DataMember]
        public string UserName { get; set; }

    }

    [DataContract]
    [Serializable]
    [Table("tblUserAuditLog")]
    public class tblUserAuditLog : TimedEntity
    {
        [Key]
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public DateTime LoginDtTime { get; set; }

        [DataMember]
        public string SessionId { get; set; }

        [DataMember]
        public string IpAddress { get; set; }

        [DataMember]
        public DateTime LogoutDtTime { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("tblUserTransactionLog")]
    public class tblUserTransactionLog : TimedEntity
    {
        [Key]
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int UsrAuditLogId { get; set; }

        [DataMember]
        public int ModuleId { get; set; }

        [DataMember]
        public string ActionDone { get; set; }

        [DataMember]
        public string AccountNo { get; set; }

        [DataMember]
        public string DealerNo { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("refBRECheck")]
    public class BRECheck : ActiveEntity
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string BRECheckCode { get; set; }

        [DataMember]
        public string BRECheckTypes { get; set; }

        [DataMember]
        public string BRECheckCondition { get; set; }

        [DataMember]
        public int UserGroupId { get; set; }

    }

	#region RefSalesChannelIds
	
	[DataContract]
	[Serializable]
	[Table("refSalesChannelIDs")]
	public class SalesChannelIDs
	{
		[DataMember]
		public int ID { get; set; }

		[DataMember]
		public string SalesChannelID { get; set; }

		[DataMember]
		public string AgentCode { get; set; }

	}

	#endregion

	#region Region
	[DataContract]
    [Serializable]
    [Table("refRegion")]
    public class Region : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "Region ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [MaxLength(20)]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegionFind : FindBase
    {
        [DataMember]
        public Region Region { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }
#endregion
}
