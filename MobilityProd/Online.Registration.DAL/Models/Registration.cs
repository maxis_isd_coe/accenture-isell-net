﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Online.Registration.DAL.Models
{
    #region Admin

    #region Address Type

    [DataContract]
    [Serializable]
    [Table("refAddressType")]
    public class AddressType : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(10)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(50)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

    }

    [DataContract]
    [Serializable]
    public class AddressTypeFind : FindBase
    {
        [DataMember]
        public AddressType AddressType { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Bank

    [DataContract]
    [Serializable]
    [Table("refBank")]
    public class Bank : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(4000)]
        [Display(Name = "Description *")]
        public string Description { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BankFind : FindBase
    {
        [DataMember]
        public Bank Bank { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Card Type

    [DataContract]
    [Serializable]
    [Table("refCardType")]
    public class CardType : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(4000)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [StringLength(20)]
        [Display(Name = "Kenan Code *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string KenanCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypeFind : FindBase
    {
        [DataMember]
        public CardType CardType { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Country

    [DataContract]
    [Serializable]
    [Table("refCountry")]
    public class Country : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(10)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CountryFind : FindBase
    {
        [DataMember]
        public Country Country { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Country State

    [DataContract]
    [Serializable]
    [Table("refCountryState")]
    public class State : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(4000)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Country *")]
        public int CountryID { get; set; }

        [Association("refCountry", "CountryID", "")]
        public virtual Country Country { get; set; }

        [DataMember]
        [Required, StringLength(50)]
        [Display(Name = "Kenan State ID *")]
        public string KenanStateID { get; set; }

        [DataMember]
        [StringLength(20)]
        [Display(Name = "VOIP Default Search *")]
        public string VOIPDefaultSearch { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StateFind : FindBase
    {
        [DataMember]
        public State CountryState { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region ID Card Type

    [DataContract]
    [Serializable]
    [Table("refIDCardType")]
    public class IDCardType : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(10)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(50)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [StringLength(10), Display(Name = "Kenan Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string KenanCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IDCardTypeFind : FindBase
    {
        [DataMember]
        public IDCardType IDCardType { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Language

    [DataContract]
    [Serializable]
    [Table("refLanguage")]
    public class Language : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }
    }

    [DataContract]
    [Serializable]
    public class LanguageFind : FindBase
    {
        [DataMember]
        public Language Language { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Nationality

    [DataContract]
    [Serializable]
    [Table("refNationality")]
    public class Nationality : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [StringLength(20)]
        [Display(Name = "Kenan Code *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string KenanCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class NationalityFind : FindBase
    {
        [DataMember]
        public Nationality Nationality { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Payment Mode

    [DataContract]
    [Serializable]
    [Table("refPaymentMode")]
    public class PaymentMode : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(4000)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [StringLength(10), Display(Name = "Kenan Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string KenanCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModeFind : FindBase
    {
        [DataMember]
        public PaymentMode PaymentMode { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Race

    [DataContract]
    [Serializable]
    [Table("refRace")]
    public class Race : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [StringLength(20)]
        [Display(Name = "Kenan Code *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string KenanCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RaceFind : FindBase
    {
        [DataMember]
        public Race Race { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Reg Type

    [DataContract]
    [Serializable]
    [Table("refRegType")]
    public class RegType : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }

        [DataMember]
        [Display(Name = "Message")]
        [StringLength(1000)]
        [Column(TypeName = "ntext")]
        public string Message { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegTypeFind : FindBase
    {
        [DataMember]
        public RegType RegType { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Cuatomer Title

    [DataContract]
    [Serializable]
    [Table("refCustomerTitle")]
    public class CustomerTitle : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(1)]
        [Display(Name = "For Gender *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string ForGender { get; set; }

        [DataMember]
        public int? DisplaySequence { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerTitleFind : FindBase
    {
        [DataMember]
        public CustomerTitle CustomerTitle { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region VIPCode

    [DataContract]
    [Serializable]
    [Table("refVIPCode")]
    public class VIPCode : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(4000)]
        [Display(Name = "Description *")]
        public string Description { get; set; }

        [DataMember]
        [StringLength(10), Display(Name = "Kenan Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string KenanCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class VIPCodeFind : FindBase
    {
        [DataMember]
        public VIPCode VIPCode { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region ExternalID Type

    [DataContract]
    [Serializable]
    [Table("refExtIDType")]
    public class ExtIDType : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [StringLength(10), Display(Name = "Kenan Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string KenanCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExtIDTypeFind : FindBase
    {
        [DataMember]
        public ExtIDType ExtIDType { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #endregion 

	#region Registration
	[DataContract]
	[Serializable]
	[Table("trnRegAttributes")]
	public class RegAttributes
	{
        public const string ATTRIB_KENAN_OUTLETCODE = "KENAN_OUTLETCODE";
        public const string ATTRIB_KENAN_OUTLETCHANNELID = "KENAN_OUTLETCHANNELID";
        public const string ATTRIB_KENAN_SALESCODE = "KENAN_SALESCODE";
        public const string ATTRIB_KENAN_SALESCHANNELID = "KENAN_SALESCHANNELID";
        public const string ATTRIB_KENAN_SALESSTAFFCODE = "KENAN_SALESSTAFFCODE";
        public const string ATTRIB_KENAN_SALESSTAFFCHANNELID = "KENAN_SALESSTAFFCHANNELID";
        public const string ATTRIB_SINGLESIGNON_STATUS = "SINGLE_SIGN_ON_STATUS";
        public const string ATTRIB_SINGLESIGNON_EMAILADDRESS = "SINGLE_SIGN_ON_EMAILADDRESS";
        public const string ATTRIB_SINGLESIGNON_EMAIL_SENT = "SINGLE_SIGN_ON_EMAIL_SENT";

		public const string ATTRIB_DF_TOTAL_PRICE = "DF_TOTAL_PRICE";
		public const string ATTRIB_DF_UPGRADE_DURATION = "DF_UPGRADE_DURATION";
		public const string ATTRIB_DF_MONTHLY_FEE = "DF_MONTHLY_FEE";
		public const string ATTRIB_DF_UPGRADE_FEE = "DF_UPGRADE_FEE";
		public const string ATTRIB_DF_TENURE = "DF_TENURE";
		public const string ATTRIB_DF_ONETIME_FEE = "DF_ONETIME_FEE";
		public const string ATTRIB_DF_DISCOUNT = "DF_DISCOUNT";
		public const string ATTRIB_DF_RC = "DF_RC_BALANCE";
		public const string ATTRIB_DF_ARTILCEID = "DF_ARTILCEID";
		
        public const string ATTRIB_PAYMENT_METHOD = "PAYMENT_METHOD";
		public const string ATTRIB_DF_ORDER = "DF_ORDER";
		public const string ATTRIB_DF_EXCEPTIONAL_ORDER = "DF_EXCEPTIONAL_ORDER";
        public const string PRIN_MSISDN = "PRIN_MSISDN";
        public const string ATTRIB_ACC_BILL_CYCLE = "ACC_BILL_CYCLE";

        public const string ATTRIB_3RDPARTY_ORDER = "3RDPARTY_ORDER";
        public const string ATTRIB_3RDPARTY_NAME = "3RDPARTY_NAME";
        public const string ATTRIB_3RDPARTY_TYPE_ID = "3RDPARTY_TYPE_ID";
        public const string ATTRIB_3RDPARTY_ID = "3RDPARTY_ID";
        public const string ATTRIB_3RDPARTY_CONTACT = "3RDPARTY_CONTACT_NUMBER";
        public const string ATTRIB_3RDPARTY_BIOMETRIC_VERIFY = "3RDPARTY_BIOMETRIC_VERIFICATION";

        public const string ATTRIB_AF_EXCEPTIONAL_ORDER = "AF_EXCEPTIONAL_ORDER";
        public const string ATTRIB_AF_PENALTY_AMOUNT = "AF_PENALTY_AMOUNT";
		public const string ATTRIB_TERMINATE_TYPE = "TERMINATION_TYPE";
        public const string ATTRIB_USER_DEVICE_AGENT = "USER_DEVICE_AGENT";
        
		[DataMember]
		[Key]
		public int ID { get; set; }

		[DataMember]
		public int RegID { get; set; }

		[DataMember]
		public int SuppLineID { get; set; }

		[DataMember]
		public string ATT_Name { get; set; }

		[DataMember]
		public string ATT_Value { get; set; }

	}
	
    [DataContract]
    [Serializable]
    [Table("refContracts")]
    public class Contracts
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [Required]
        [DataMember]
        public string KenanCode { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool Active { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public int Duration { get; set; }
        [DataMember]
        public int IsDeviceContract { get; set; }
    }

	[DataContract]
    [Serializable]
	[Table("tblRegUploadDoc")]
	public class RegUploadDoc
    {
		[Key]
		[DataMember]
		public int ID { get; set; }

        [DataMember]
        public int RegID { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public byte[] FileStream { get; set; }

		[DataMember]
		public string FileType { get; set; }

		[DataMember]
		public string Status { get; set; }

		[DataMember]
		public bool? Retrigger { get; set; }

		[DataMember]
		public DateTime? Date { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("trnRegistration")]
    public class Registration : BaseEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Type *")]
        public int RegTypeID { get; set; }

        [Association("refRegType", "RegTypeID", "ID")]
        public virtual RegType RegType { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Customer *")]
        public int CustomerID { get; set; }

        [Association("tblCustomer", "CustomerID", "ID")]
        public virtual Customer Customer { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Center *")]
        public int CenterOrgID { get; set; }

        [Association("tblOrganization", "CenterOrgID", "ID")]
        public virtual Organization CenterOrg { get; set; }

        [DataMember]
        [DataType(DataType.MultilineText)]
        [StringLength(4000)]
        [Column(TypeName = "nText")]
        [Display(Name = "Remark")]
        public string Remark { get; set; }

        [DataMember]
        [StringLength(10)]
        [Display(Name = "Queue No")]
        public string QueueNo { get; set; }

        [DataMember]
        [StringLength(15)]
        [Display(Name = "Desire MSISDN 1")]
        public string MSISDN1 { get; set; }

        [DataMember]
        [StringLength(15)]
        [Display(Name = "Desire MSISDN 2")]
        public string MSISDN2 { get; set; }

        [DataMember]
        [StringLength(30)]
        [Display(Name = "Account ID")]
        public string AccountID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Additional Charges *")]
        public decimal AdditionalCharges { get; set; }

        [DataMember]
        [StringLength(30)]
        [Display(Name = "Modem ID")]
        public string ModemID { get; set; }

        [DataMember]
        [StringLength(30)]
        [Display(Name = "Account Number")]
        public string KenanAccountNo { get; set; }

        //Sales Details
        [DataMember]
        [StringLength(50)]
        [Display(Name = "Sales Person")]
        public string SalesPerson { get; set; }

        [DataMember]
        [Display(Name = "Is Verified?")]
        public bool IsVerified { get; set; }

        [DataMember]
        [Display(Name = "BiometricVerify?")]
        public bool BiometricVerify { get; set; }

        [DataMember]
        [Display(Name = "Internal Blacklisted?")]
        public bool InternalBlacklisted { get; set; }

        [DataMember]
        [Display(Name = "External Blacklisted?")]
        public bool ExternalBlacklisted { get; set; }

        [DataMember]
        [Display(Name = "Approved Blacklist Person")]
        public string ApprovedBlacklistPerson { get; set; }

        [DataMember]
        [Display(Name = "Customer Signed Date")]
        public DateTime RFSalesDT { get; set; }

        [Required]
        [DataMember]
        public bool WelcomeEmailSent { get; set; }

        [DataMember]
        [Display(Name = "Preferred Installation Date")]
        public DateTime? InstallationDT { get; set; }

        [DataMember]
        [Display(Name = "Skipped Duplicate?")]
        public bool SkipDuplicate { get; set; }

        [DataMember]
        [Display(Name = "Signature")]
        public string SignatureSVG { get; set; }

        [DataMember]
        [Display(Name = "Photo (front)")]
        public string CustomerPhoto { get; set; }

        [DataMember]
        [Display(Name = "Photo (back)")]
        public string AltCustomerPhoto { get; set; }

        [DataMember]
        [Display(Name = "Photo")]
        public string Photo { get; set; }

        [DataMember]
        [Display(Name = "UploadedDocument")]
        public string UploadedDocument { get; set; }

        [DataMember]
        [Display(Name = "SIM Serial")]
        public string SIMSerial { get; set; }

        [DataMember]
        [Display(Name = "IMEI Number")]
        public string IMEINumber { get; set; }

        [DataMember]
        [Display(Name = "CRP Type")]
        public string CRPType { get; set; }

        #region SUTAN ADDED CODE 19thFeb2013

        [DataMember]
        public string WriteOffCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "Age Verification")]
        public string AgeCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "DDMF Verification")]
        public string DDMFCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "Address Verification")]
        public string AddressCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "Outstanding Credit Verification")]
        public string OutStandingCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "Total Line Verification")]
        public string TotalLineCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "Principal Line Verification")]
        public string PrincipleLineCheckStatus { get; set; }
        #endregion VLT ADDED CODE  19thFeb2013

        #region Chetan added code
        [DataMember]
        [Display(Name = "Whitelist Status")]
        public string Whitelist_Status { get; set; }

        [DataMember]
        [Display(Name = "MOC Status")]
        public string MOC_Status { get; set; }

        [DataMember]
        [Display(Name = "Contract Check Status")]
        public string ContractCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "User Type")]
        public string UserType { get; set; }

        [DataMember]
        [Display(Name = "Plan Advance")]
        public decimal PlanAdvance { get; set; }

        [DataMember]
        [Display(Name = "Plan Deposit")]
        public decimal PlanDeposit { get; set; }

        [DataMember]
        [Display(Name = "Device Advance")]
        public decimal DeviceAdvance { get; set; }

        [DataMember]
        [Display(Name = "Device Deposit")]
        public decimal DeviceDeposit { get; set; }

        [DataMember]
        [Display(Name = "Discount")]
        public decimal Discount { get; set; }

        [DataMember]
        [Display(Name = "OfferID")]
        public decimal OfferID { get; set; }

        [DataMember]
        [Display(Name = "Outstanding Acounts")]
        public string OutstandingAcs { get; set; }

        [DataMember]
        [Display(Name = "ArticleID")]
        public string ArticleID { get; set; }

        [DataMember]
        [Display(Name = "MNP Service Id Check Status")]
        public string MNPServiceIdCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "Pre Portin Check Status")]
        public string PrePortInCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "UOM Code")]
        public string UOMCode { get; set; }

        [DataMember]
        [Display(Name = "K2 Status")]
        public string K2_Status { get; set; }

        [DataMember]
        [Display(Name = "PrePortReqId")]
        public string PrePortReqId { get; set; }

        [DataMember]
        [Display(Name = "DonorId")]
        public string DonorId { get; set; }

        #endregion

        #region Added by Patanjali to support Supp and New Line
        [DataMember]
        [Display(Name = "FxAcctNo")]
        public string fxAcctNo { get; set; }

        [DataMember]
        [Display(Name = "ExternalId")]
        public string externalId { get; set; }
        #endregion

        [DataMember]
        [Display(Name = "AccExternalID")]
        public string AccExternalID { get; set; }

        #region "Added by Patanjali on 30-03-2013 to add remarks"
        [DataMember]
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }
        #endregion

        #region Added by Narayanareddy
        [DataMember]
        [Display(Name = "IMPOSFileName")]
        public string IMPOSFileName { get; set; }

        [DataMember]
        [Display(Name = "IMPOSStatus")]
        public int IMPOSStatus { get; set; }

        [DataMember]
        [Display(Name = "OrganisationId")]
        public string OrganisationId { get; set; }
        #endregion


        #region Added by VLT on 09-04-2013
        [DataMember]
        [Display(Name = "SimModelId")]
        public Int32 SimModelId { get; set; }
        #endregion

        [DataMember]
        [StringLength(30)]
        [Display(Name = " ")]
        public string ServiceOrderId { get; set; }

        [DataMember]
        [StringLength(30)]
        [Display(Name = " ")]
        public string Trn_Type { get; set; }

        #region "Added by VLT ON May 1 2013"
        [DataMember]
        [Display(Name = " ")]
        public bool InternationalRoaming { get; set; }

        [DataMember]
        [Display(Name = " ")]
        public bool K2Type { get; set; }

        #endregion
        [DataMember]
        public string TMOrderID { get; set; }

        #region Added by Himansu to support Sim Replacement


        [DataMember]
        public string fxSubscrNo { get; set; }

        [DataMember]
        public string fxSubScrNoResets { get; set; }


        #endregion


        #region CRP

        #region added by Nreddy

        [DataMember]
        public string SM_ContractType { get; set; }



        [DataMember]
        public bool IsSImRequired_Smart { get; set; }

        #endregion


        [DataMember]
        public decimal PenaltyAmount { get; set; }
        #endregion

        //added by Nreddy for Smart

        [DataMember]
        public bool? IsK2 { get; set; }

        [DataMember]
        public string Smart_ID { get; set; }

        [DataMember]
        public string Discount_ID { get; set; }

        [DataMember]
        [Display(Name = "upfrontPayment")]
        public decimal upfrontPayment { get; set; }

        [DataMember]
        [Display(Name = "PenaltyWaivedAmt")]
        public decimal PenaltyWaivedAmt { get; set; }

        //GTM e-Billing CR - Ricky - 2014.09.25
        [DataMember]
        [Display(Name = "Bill Delivery Via Email")]
        public bool billDeliveryViaEmail { get; set; }

        [DataMember]
        [StringLength(200)]
        [Display(Name = "Bill Delivery Email Address")]
        public string billDeliveryEmailAddress { get; set; }

        [DataMember]
        [Display(Name = "Bill Delivery SMS Notification")]
        public bool billDeliverySmsNotif { get; set; }

        [DataMember]
        [StringLength(15)]
        [Display(Name = "Bill Delivery SMS Number")]
        public string billDeliverySmsNo { get; set; }

        [DataMember]
        [StringLength(50)]
        public string billDeliverySubmissionStatus { get; set; }

		[DataMember]
		[StringLength(5)]
		public string DropVersion {get; set;}

    }

    [DataContract]
    [Serializable]
    [Table("trnRegSupplinesMap")]
    public class RegSuppLinesMap
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Supp_MSISDN")]
        public string Supp_MSISDN { get; set; }

        [DataMember]
        [Display(Name = "Prin_MSISDN")]
        public string Prin_MSISDN { get; set; }

        [DataMember]
        [Association("trnRegistration", "ID", "")]
        [Display(Name = "RegId")]
        public int RegId { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("trnRegistrationSec")]
    public class RegistrationSec : BaseEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        //[Key]
        [Association("trnRegistration", "ID", "")]
        [Display(Name = "RegistrationID")]
        public int RegistrationID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Type *")]
        public int RegTypeID { get; set; }

        [Association("refRegType", "RegTypeID", "ID")]
        public virtual RegType RegType { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Customer *")]
        public int CustomerID { get; set; }

        [Association("tblCustomer", "CustomerID", "ID")]
        public virtual Customer Customer { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Center *")]
        public int CenterOrgID { get; set; }

        [Association("tblOrganization", "CenterOrgID", "ID")]
        public virtual Organization CenterOrg { get; set; }

        [DataMember]
        [DataType(DataType.MultilineText)]
        [StringLength(4000)]
        [Column(TypeName = "nText")]
        [Display(Name = "Remark")]
        public string Remark { get; set; }

        [DataMember]
        [StringLength(10)]
        [Display(Name = "Queue No")]
        public string QueueNo { get; set; }

        [DataMember]
        [StringLength(15)]
        [Display(Name = "Desire MSISDN 1")]
        public string MSISDN1 { get; set; }

        [DataMember]
        [StringLength(15)]
        [Display(Name = "Desire MSISDN 2")]
        public string MSISDN2 { get; set; }

        [DataMember]
        [StringLength(30)]
        [Display(Name = "Account ID")]
        public string AccountID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Additional Charges *")]
        public decimal AdditionalCharges { get; set; }

        [DataMember]
        [StringLength(30)]
        [Display(Name = "Modem ID")]
        public string ModemID { get; set; }

        [DataMember]
        [StringLength(30)]
        [Display(Name = "Kenan Account Number")]
        public string KenanAccountNo { get; set; }

        //Sales Details
        [DataMember]
        [StringLength(50)]
        [Display(Name = "Sales Person")]
        public string SalesPerson { get; set; }

        [DataMember]
        [Display(Name = "Is Verified?")]
        public bool IsVerified { get; set; }

        [DataMember]
        [Display(Name = "BiometricVerify?")]
        public bool BiometricVerify { get; set; }

        [DataMember]
        [Display(Name = "Internal Blacklisted?")]
        public bool InternalBlacklisted { get; set; }

        [DataMember]
        [Display(Name = "External Blacklisted?")]
        public bool ExternalBlacklisted { get; set; }

        [DataMember]
        [Display(Name = "Approved Blacklist Person")]
        public string ApprovedBlacklistPerson { get; set; }

        [DataMember]
        //[Range(typeof(DateTime), DateTime.MinValue.Date.ToString(), DateTime.Now.Date.ToString(), "Customer Signed Date must not later than current date.")]
        [Display(Name = "Customer Signed Date")]
        public DateTime RFSalesDT { get; set; }

        [Required]
        [DataMember]
        public bool WelcomeEmailSent { get; set; }

        [DataMember]
        [Display(Name = "Preferred Installation Date")]
        public DateTime? InstallationDT { get; set; }

        [DataMember]
        [Display(Name = "Skipped Duplicate?")]
        public bool SkipDuplicate { get; set; }

        [DataMember]
        [Display(Name = "Signature")]
        public string SignatureSVG { get; set; }

        [DataMember]
        [Display(Name = "Photo (front)")]
        public string CustomerPhoto { get; set; }

        [DataMember]
        [Display(Name = "Photo (back)")]
        public string AltCustomerPhoto { get; set; }

        [DataMember]
        [Display(Name = "Photo")]
        public string Photo { get; set; }

        [DataMember]
        [Display(Name = "SIM Serial")]
        public string SIMSerial { get; set; }

        [DataMember]
        [Display(Name = "IMEI Number")]
        public string IMEINumber { get; set; }

        #region SUTAN ADDED CODE 19thFeb2013
        [DataMember]
        [Display(Name = "Age Verification")]
        public string AgeCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "DDMF Verification")]
        public string DDMFCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "Address Verification")]
        public string AddressCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "Outstanding Credit Verification")]
        public string OutStandingCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "Total Line Verification")]
        public string TotalLineCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "Principal Line Verification")]
        public string PrincipleLineCheckStatus { get; set; }
        #endregion VLT ADDED CODE  19thFeb2013

        #region Chetan added code
        [DataMember]
        [Display(Name = "Whitelist Status")]
        public string Whitelist_Status { get; set; }

        [DataMember]
        [Display(Name = "MOC Status")]
        public string MOC_Status { get; set; }

        [DataMember]
        [Display(Name = "Contract Check Status")]
        public string ContractCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "User Type")]
        public string UserType { get; set; }

        [DataMember]
        [Display(Name = "Plan Advance")]
        public decimal PlanAdvance { get; set; }

        [DataMember]
        [Display(Name = "Plan Deposit")]
        public decimal PlanDeposit { get; set; }

        [DataMember]
        [Display(Name = "Device Advance")]
        public decimal DeviceAdvance { get; set; }

        [DataMember]
        [Display(Name = "Device Deposit")]
        public decimal DeviceDeposit { get; set; }

        [DataMember]
        [Display(Name = "Discount")]
        public decimal Discount { get; set; }

        [DataMember]
        [Display(Name = "OfferID")]
        public decimal OfferID { get; set; }

        [DataMember]
        [Display(Name = "Outstanding Acounts")]
        public string OutstandingAcs { get; set; }

        [DataMember]
        [Display(Name = "ArticleID")]
        public string ArticleID { get; set; }

        [DataMember]
        [Display(Name = "MNP Service Id Check Status")]
        public string MNPServiceIdCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "Pre Portin Check Status")]
        public string PrePortInCheckStatus { get; set; }

        [DataMember]
        [Display(Name = "UOM Code")]
        public string UOMCode { get; set; }

        [DataMember]
        [Display(Name = "K2 Status")]
        public string K2_Status { get; set; }

        //[DataMember]
        //public string SimType { get; set; }

        #endregion

        #region Added by Patanjali to support Supp and New Line
        [DataMember]
        [Display(Name = "FxAcctNo")]
        public string fxAcctNo { get; set; }

        [DataMember]
        [Display(Name = "ExternalId")]
        public string externalId { get; set; }
        #endregion

        [DataMember]
        [Display(Name = "AccExternalID")]
        public string AccExternalID { get; set; }

        #region "Added by Patanjali on 30-03-2013 to add remarks"
        [DataMember]
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }
        #endregion

        #region Added by Narayanareddy
        [DataMember]
        [Display(Name = "IMPOSFileName")]
        public string IMPOSFileName { get; set; }

        [DataMember]
        [Display(Name = "IMPOSStatus")]
        public int IMPOSStatus { get; set; }

        [DataMember]
        [Display(Name = "OrganisationId")]
        public string OrganisationId { get; set; }
        #endregion


        #region Added by VLT on 09-04-2013
        [DataMember]
        [Display(Name = "SimModelId")]
        public Int32 SimModelId { get; set; }
        #endregion

        [DataMember]
        [StringLength(30)]
        [Display(Name = " ")]
        public string ServiceOrderId { get; set; }

        [DataMember]
        [StringLength(30)]
        [Display(Name = " ")]
        public string Trn_Type { get; set; }

        #region "Added by VLT ON May 1 2013"
        [DataMember]
        [Display(Name = " ")]
        public bool InternationalRoaming { get; set; }

        [DataMember]
        [Display(Name = " ")]
        public bool K2Type { get; set; }

        #endregion
        [DataMember]
        public string TMOrderID { get; set; }

        #region Added by Himansu to support Sim Replacement


        [DataMember]
        public string fxSubscrNo { get; set; }

        [DataMember]
        public string fxSubScrNoResets { get; set; }

        [DataMember]
        public int PenaltyAmount { get; set; }

        #endregion

        [DataMember]
        public bool IsSecondaryRequestSent { get; set; }
    }


    #region LnkRegDetails
    [DataContract]
    [Serializable]
    [Table("LnkRegDetails")]
    public class LnkRegDetails
    {

        [DataMember]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [DataMember]
        public int RegId { get; set; }

        [DataMember]
        public bool IsSuppNewAc { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string SimType { get; set; }

        [DataMember]
        public string CmssId { get; set; }

        [DataMember]
        public string OldSimSerial { get; set; }

        [DataMember]
        public string NewSimSerial { get; set; }

        [DataMember]
        public int? SimReplReasonCode { get; set; }

        [DataMember]
        public string SimReplReason { get; set; }

        [DataMember]
        public string SIMModel { get; set; }

        [DataMember]
        public string AccountType { get; set; }

        [DataMember]
        public string NrcId { get; set; }

        [DataMember]
        public bool? isDeviceCRP { get; set; }

        [DataMember]
        public int? SimModelId { get; set; }

        [DataMember]
        public int? SimArticalId { get; set; }

        [DataMember]
        public string ActiveDt { get; set; }

        [DataMember]
        public bool? IsWaivedOff { get; set; }

        [DataMember]
        public int? MarketCode { get; set; }

        [DataMember]
        public string Liberlization_Status { get; set; }

        [DataMember]
        public string MOC_Status { get; set; }


        [DataMember]
        public string Justification { get; set; }

        [DataMember]
        public bool? Isacknowledged { get; set; }

        [DataMember]
        public string AcknowledgedBy { get; set; }

        [DataMember]
        public string PrintVersionNo { get; set; }

        /*Changes by chetan related to PDPA implementation*/
        [DataMember]
        public string PdpaVersion { get; set; }

        [DataMember]
        public string TUTSerial { get; set; }


        //Added by ravi on Mar 04 2014 for sim replacement type
        [DataMember]
        public int? SimReplacementType { get; set; }

        [DataMember]
        public int? TUTSimModelId { get; set; }

        //Added by ravi on Mar 04 2014 for sim replacement type
        [DataMember]
        public int? ContractCheckCount { get; set; }

      [DataMember]
        public string SimCardType { get; set; }

        [DataMember]
        public string MismSimCardType { get; set; }

        [DataMember]
        public bool IsMSISDNReserved { get; set; }

        [DataMember]
        public int? TotalLineCheckCount { get; set; }//15042015 - Anthony - Drop 5 : BRE Check for non-Drop 4 Flow

        [DataMember]
        public string WriteOffDetails { get; set; }
    }

    #endregion

    [DataContract]
    [Serializable]
    public class RegistrationFind : FindBase
    {
        public RegistrationFind()
        {
            Registration = new Models.Registration();
            CustomerIDs = new List<int>();
        }

        [DataMember]
        public Registration Registration { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public DateTime? RegDTFrom { get; set; }
        [DataMember]
        public DateTime? RegDTTo { get; set; }
        [DataMember]
        public DateTime? InstallationDTFrom { get; set; }
        [DataMember]
        public DateTime? InstallationDTTo { get; set; }
        [DataMember]
        public List<int> CustomerIDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationSearchCriteria : FindBase
    {
        [DataMember]
        [RegularExpression(@"^[0-9]{0,10}$", ErrorMessage = "Registration ID must be a number.")]
        [Display(Name = "Registration ID")]
        public string RegID { get; set; }

        [DataMember]
        [Display(Name = "ID Card No")]
        public string IDCardNo { get; set; }

        [DataMember]
        [Display(Name = "Queue No")]
        public string QueueNo { get; set; }

        [DataMember]
        [Display(Name = "Status")]
        public int StatusID { get; set; }

        [DataMember]
        [Display(Name = "Organization")]
        public int? CenterOrgID { get; set; }

        [DataMember]
        [Display(Name = "User ID")]
        public int UserID { get; set; }

        [DataMember]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [DataMember]
        [Display(Name = "Contact Number")]
        public string MSISDN { get; set; }

        [DataMember]
        [Display(Name = "Customer Name")]
        public string CustName { get; set; }

        [DataMember]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [DataMember]
        [Display(Name = "Reg Date From")]
        public DateTime? RegDateFrom { get; set; }

        [DataMember]
        [Display(Name = "To")]
        public DateTime? RegDateTo { get; set; }

        [DataMember]
        public bool IsMobilePlan { get; set; }

        [DataMember]
        public bool RegTypeID { get; set; }

        [DataMember]
        public bool RegType { get; set; }

        [DataMember]
        public string IsBRN { get; set; }

        [DataMember]
        [Display(Name = "Criteria")]
        public string Criteria { get; set; }

        [DataMember]
        public List<string> CriteriaItems
        {
            get;
            set;
        }
        [DataMember]
        public string Trn_Type { get; set; }

        //Added by Ravi for Records Retrival 
        [DataMember]
        public string SearchType { get; set; }

        [DataMember]
        [Display(Name = "SIM Serial Number")]
        public string SimSerial { get; set; }

        [DataMember]
        [Display(Name = "IMEI Number")]
        public string ImeiNumber { get; set; }

        [DataMember]
        public string IsSuperAdmin { get; set; }

        [DataMember]
        public string isThirdParty { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AgentCodecByUserNameCriteria : FindBase
    {
        [DataMember]
        [Display(Name = "ID")]
        public string OrgID { get; set; }

        [DataMember]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [DataMember]
        [Display(Name = "Criteria")]
        public string Criteria { get; set; }

    }

    [DataContract]
    [Serializable]
    public class RegistrationPendingOrderslist : FindBase
    {

        [DataMember]
        [Display(Name = "ID")]
        public int Id { get; set; }
        [DataMember]
        public int RegId { get; set; }
        [DataMember]
        public string AccessId { get; set; }
        [DataMember]
        public char TransactionStatus { get; set; }
        [DataMember]
        public string ApprovedBy { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationSearchResult
    {
        [DataMember]
        public int RegID { get; set; }

        [DataMember]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTime RegDate { get; set; }
        [DataMember]
        public string QueueNo { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public string LastUpdateID { get; set; }
        [DataMember]
        public string LastAccessID { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string CenterOrg { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public int RegType { get; set; }
        [DataMember]
        public int RegTypeID { get; set; }
        [DataMember]
        public string RegTypeDescription { get; set; }
        [DataMember]
        public string CriteriaType { get; set; }

        [DataMember]
        public string FxAcctNo { get; set; }
        [DataMember]
        public string FxSubscrNo { get; set; }

        [DataMember]
        public string FxSubScrNoResets { get; set; }
        [DataMember]
        public string Trn_Type { get; set; }
        [DataMember]
        public string ApprovedBlacklistPerson { get; set; }
        [DataMember]
        public string CRPType { get; set; }
        [DataMember]
        public string UserRoles { get; set; }
        [DataMember]
        public string WebPOS { get; set; }
        [DataMember]
        public string IsVIP { get; set; }
        [DataMember]
        public string hasPrinDevice { get; set; }
        [DataMember]
        public string PenaltyAmount { get; set; }
        [DataMember]
        public string SMcontractType { get; set; }
        [DataMember]
        public int PrinOfferId { get; set; }
        [DataMember]
        public string SASupp { get; set; }
        [DataMember]
        public string SimType { get; set; }
        [DataMember]
        public string SimReplacementType { get; set; }
        [DataMember]
        public string hasPrinAccessory { get; set; }
        [DataMember]
        public string hasSuppDevice { get; set; }
        [DataMember]
        public string hasSuppline { get; set; }
        [DataMember]
        public string hasSuppAccessory { get; set; }
        [DataMember]
        public string userType { get; set; }
        [DataMember]
        public string isThirdParty { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AgentCodeByUserNameResult
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string SalesChannelId { get; set; }
        [DataMember]
        public string AgentCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationPendingOrdersResults
    {
        [Display(Name = "ID")]
        public int Id { get; set; }
        [DataMember]
        public int RegId { get; set; }
        [DataMember]
        public string RegTypeDescription { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public string TransactionStatus { get; set; }
        [DataMember]
        public string TranType { get; set; }
        [DataMember]
        public string Customer { get; set; }
        [DataMember]
        public string AccessId { get; set; }
        [DataMember]
        public string ApprovedBy { get; set; }

    }

    #region Supervisor waiver off

    [DataContract]
    [Serializable]
    public class SearchResultForSuperVisorWaieverOff
    {
        [DataMember]
        public int RegID { get; set; }

        [DataMember]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTime RegDate { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string IC { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string ServiceType { get; set; }

        [DataMember]
        public string MOCStatus { get; set; }

        [DataMember]
        public string Liberalization { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public string WaiverDetails { get; set; }

        [DataMember]
        public string FailedBREDetails { get; set; }

        [DataMember]
        public string Approver { get; set; }

        [DataMember]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTime? dtApproved { get; set; }

        [DataMember]
        public string Justification { get; set; }

        [DataMember]
        public Boolean IsAcknowledged { get; set; }

        [DataMember]
        public Boolean IsEditable { get; set; }

        [DataMember]
        public int RegTypeID { get; set; }

        [DataMember]
        public string TranType { get; set; }

        [DataMember]
        public string CRPType { get; set; }

        [DataMember]
        public string DealerName { get; set; }
        [DataMember]
        public string DealerCode { get; set; }
        [DataMember]
        public string SalesStaffCode { get; set; }

        
       
        [DataMember]
        public string TransactionStatus { get; set; }
        [DataMember]
        public DateTime? TransactionDT { get; set; }
        [DataMember]
        public string TransactionBy { get; set; }
        [DataMember]
        public string IsLockedBy { get; set; }
        [DataMember]
        public string AcknowledgedBy { get; set; }
        // add additional column for Write Off and Exceptional Wiver Approver
        [DataMember]
        public string EwAppr { get; set; }
        [DataMember]
        public DateTime? EwApprDate { get; set; }
        [DataMember]
        public string WriteAppr { get; set; }
        [DataMember]
        public List<string> JustificationList { get; set; }
        [DataMember]
        public string PaymentType { get; set; }
        [DataMember]
        public string DfAppr { get; set; }
        [DataMember]
        public string trnStatus { get; set; }
        [DataMember]
        public string hasPrinDevice { get; set; }
        [DataMember]
        public decimal PenaltyAmount { get; set; }
        [DataMember]
        public string SMcontractType { get; set; }
        [DataMember]
        public int PrinOfferId { get; set; }
        [DataMember]
        public bool SASupp { get; set; }
        [DataMember]
        public string SimType { get; set; }
        [DataMember]
        public int? SimReplacementType { get; set; }
        [DataMember]
        public string hasPrinAccessory { get; set; }
        [DataMember]
        public string hasSuppDevice { get; set; }
        [DataMember]
        public string hasSuppline { get; set; }
        [DataMember]
        public string hasSuppAccessory { get; set; }
        [DataMember]
        public string userType { get; set; }
        [DataMember]
        public string isThirdParty { get; set; }
        [DataMember]
        public string TacAppr { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationSearchCriteriaForWaiverOff : FindBase
    {
        [DataMember]
        [RegularExpression(@"^[0-9]{0,10}$", ErrorMessage = "Must be a number.")]
        [Display(Name = "Registration ID")]
        public string RegID { get; set; }

        [DataMember]
        [RegularExpression(@"^[0-9]{0,15}$", ErrorMessage = "Must be a number.")]
        [Display(Name = "MSISDN")]
        public string MSISDN { get; set; }

        [DataMember]
        [Display(Name = "Service Type")]
        public string ServiceType { get; set; }

        [DataMember]
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }


        [DataMember]
        [Display(Name = "Service Type")]
        public string ServiceTypeFo { get; set; }

        [DataMember]
        [Display(Name = "Created By")]
        public string CreatedByFo { get; set; }

        [DataMember]
        [Display(Name = "Acknowledged By")]
        public string AcknowledgedBy { get; set; }

        [DataMember]
        [Display(Name = "Approved by")]
        public string Approver { get; set; }

        [DataMember]
        [Display(Name = "Transaction Type")]
        public string Trn_Type { get; set; }

        [DataMember]
        [Display(Name="Approval Status")]
        public string TransactionStatus { get; set; }

        /// <summary>
        /// 0 - All transaction types
        /// 1 - Waiver
        /// 2 - BreFail
        /// </summary>
        [DataMember]
        public int TrnTypeId { get; set; }

        /// <summary>
        /// 0 - Quick search
        /// 1 - Advance search
        /// </summary>
        [DataMember]
        public int SearchType { get; set; }

        [DataMember]
        public int RegTypeId { get; set; }

        [DataMember]
        public int TabNo { get; set; }

        [DataMember]
        [Display(Name = "Start Date")]
        public DateTime? RegDateFrom { get; set; }

        [DataMember]
        [Display(Name = "End Date")]
        public DateTime? RegDateTo { get; set; }

        [DataMember]
        [Display(Name = "Start Date")]
        public DateTime? RegDateFromFo { get; set; }

        [DataMember]
        [Display(Name = "End Date")]
        public DateTime? RegDateToFo { get; set; }

        [DataMember]
        [RegularExpression(@"^[0-9]{0,12}$", ErrorMessage = "Must be a number.")]
        [Display(Name = "ID Card No")]
        public string IDNO { get; set; }

        /// <summary>
        /// 0 - to be acknlowedged recs
        /// -1 - to show all the recs
        /// </summary>
        [DataMember]
        public int ShowAllRecords { get; set; }

        public string SelectedRecs { get; set; }

        [DataMember]
        [Display(Name = "Stores")]
        public string SelectedOrganization { get; set; }

        [DataMember]
        public int OrgId { get; set; }

        [DataMember]
        public bool isCDPU { get; set; }

		[DataMember]
		public string ServiceTypeDescription { get; set; } 
    }

    [DataContract]
    [Serializable]
    public class PegaRecommendationSearchCriteria : FindBase
    {
        [DataMember]
        [Display(Name = "ID Card No")]
        public string IDCardNo { get; set; }

        [DataMember]
        [RegularExpression(@"^[0-9]{0,15}$", ErrorMessage = "Must be a number.")]
        [Display(Name = "MSISDN")]
        public string MSISDN { get; set; }

        [DataMember]
        [Display(Name = "Response")]
        public string Response { get; set; }

        [DataMember]
        [Display(Name = "Status")]
        public string Status { get; set; }

        [DataMember]
        [Display(Name = "Captured By")]
        public string CapturedBy { get; set; }

        [DataMember]
        [Display(Name = "Last Updated By")]
        public string LastUpdatedBy { get; set; }

        [DataMember]
        [Display(Name = "Recommendation")]
        public string Recommendation { get; set; }

        [DataMember]
        [Display(Name = "Captured Date Start")]
        public DateTime? CapturedStartDt { get; set; }

        [DataMember]
        [Display(Name = "Captured Date End")]
        public DateTime? CapturedEndtDt { get; set; }

        [DataMember]
        public string SelectedId { get; set; }

        [DataMember]
        public string SelectedRecs { get; set; }

        [DataMember]
        public int SelectedIdTotalRec { get; set; }
        
    }

    [DataContract]
    [Serializable]
    public class RegDetails : FindBase
    {
        [DataMember]
        public int RegID { get; set; }

        [DataMember]
        public bool Isacknowledged { get; set; }

        [DataMember]
        public string AcknowledgedBy { get; set; }

    }
    #endregion

    [DataContract]
    [Serializable]
    [Table("lnkPlanDevice")]
    public class lnkPlanDevice
    {
        [DataMember]
        public Int64 Id { get; set; }
        [DataMember]
        public string RegId { get; set; }
        [DataMember]
        public string RGW_Series { get; set; }
        [DataMember]
        public string Dect_Series { get; set; }
        [DataMember]
        public int Status { get; set; }
    }

    #region "Spend Limit"
    //Spend Limit by VLT On June 04 2013
    [DataContract]
    [Serializable]
    [Table("lnkRegSpendLimit")]
    public class lnkRegSpendLimit
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int RegId { get; set; }
        [DataMember]
        public int PlanId { get; set; }
        [DataMember]
        public decimal SpendLimit { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
    #endregion


    #region "Extra Ten"
    [DataContract]
    [Serializable]
    [Table("lnkExtraTenDetails")]
    public class lnkExtraTenDetails
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RegId { get; set; }

        [DataMember]
        public string Msisdn { get; set; }

        [DataMember]
        public string OldMsisdn { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string MsgCode { get; set; }

        [DataMember]
        public string MsgDesc { get; set; }

    }

    [DataContract]
    [Serializable]
    [Table("LnkExtraTenConfirmationLog")]
    public class LnkExtraTenConfirmationLog
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int RegId { get; set; }

        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string ConfirmedBy { get; set; }


        [DataMember]
        public string ConfirmedStatus { get; set; }


    }
    [DataContract]
    [Serializable]
    [Table("lnk_ExtraTenAddEditDetails")]
    public class lnkExtraTenAddEditDetails
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int RegID { get; set; }

        [DataMember]
        public string ExtraTenMSISDN { get; set; }

        [DataMember]
        public string OperationType { get; set; }

        [DataMember]
        public string OperationDesc { get; set; }

        [DataMember]
        public string OldMSISDN { get; set; }

        [DataMember]
        public string NewMSISDN { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string StatusReason { get; set; }

        [DataMember]
        public string CreatedyBy { get; set; }

        [DataMember]
        public string UpdatedBy { get; set; }

        [DataMember]
        public DateTime CreateDate { get; set; }

        [DataMember]
        public DateTime UpdateDate { get; set; }
    }
    #endregion


    [DataContract]
    [Serializable]
    [Table("lnkInstallerCenter")]
    public class InstallerCenter
    {
        [DataMember]
        [Key]
        public int Id { get; set; }
        [DataMember]
        public int AccessId { get; set; }
        [DataMember]
        public int CenterId { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("refCenters")]
    public class Center
    {
        [DataMember]
        [Key]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int CenterId { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public bool Active { get; set; }
    }

    [DataContract]
    [Serializable]
    public class FxDependChkComps
    {
        [DataMember]
        public string KenanCode { get; set; }
        [DataMember]
        public string ComponentKenanCode { get; set; }
        [DataMember]
        public int DependentKenanCode { get; set; }

    }


    [DataContract]
    [Serializable]
    public class PlanDetailsandContract
    {


        [DataMember]
        public int PackageId { get; set; }
        [DataMember]
        public string PackageName { get; set; }
        [DataMember]
        public int ModelID { get; set; }
        [DataMember]
        public string DeviceName { get; set; }
        [DataMember]
        public decimal DevicePrice { get; set; }

        [DataMember]
        public int ContractID { get; set; }
        [DataMember]
        public string ContractName { get; set; }

        [DataMember]
        public int BrandID { get; set; }

    }

    #endregion 

    #region Customer

    [DataContract]
    [Serializable]
    [Table("tblCustomer")]
    public class Customer : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        //[Required]
        [DataMember]
        [StringLength(200)]
        [Display(Name = "Full Name *")]
        public string FullName { get; set; }

        //[Required]
        [DataMember]
        [Display(Name = "Title *")]
        public int CustomerTitleID { get; set; }

        [Association("refRace", "CustomerTitleID", "")]
        public virtual CustomerTitle CustomerTitle { get; set; }

        //Personal Details
        [DataMember]
        //[Required]
        [DataType(DataType.Date)]
        [Column(TypeName = "date")]
        [Display(Name = "DOB *")]
        public DateTime DateOfBirth { get; set; }  //dd/mm/yyyy

        //[Required]
        [DataMember]
        [MaxLength(1)]
        [Display(Name = "Gender *")]
        public string Gender { get; set; }  // Male/Female

        [Required]
        [DataMember]
        [Display(Name = "ID Type *")]
        public int IDCardTypeID { get; set; }

        [Association("refIDCardType", "IDCardTypeID", "")]
		[Display(Name = "ID Card Type")]
        public virtual IDCardType IDCardType { get; set; }

        [Required]
        [DataMember]
        [StringLength(50)]
        [Display(Name = "ID No *")]
        public string IDCardNo { get; set; }


        [DataMember]
        [Display(Name = "Race *")]
        public int RaceID { get; set; }

        [Association("refRace", "RaceID", "")]
        [Display(Name = "Race *")]
        public virtual Race Race { get; set; }


        [DataMember]
        [Display(Name = "Language *")]
        public int LanguageID { get; set; }

        [Association("refLanguage", "LanguageID", "")]
        public virtual Language Language { get; set; }

        //[Required]
        [DataMember]
        [Display(Name = "Nationality *")]
        public int NationalityID { get; set; }

        [Association("refNationality", "NationalityID", "")]
        public virtual Nationality Nationality { get; set; }

        [DataMember]
        [StringLength(25)]
        [Display(Name = "Contact No *")]
        public string ContactNo { get; set; }

        [DataMember]
        [StringLength(15)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Alt Contact No must be a number.")]
        [Display(Name = "Alt Contact No")]
        public string AlternateContactNo { get; set; }

        [DataMember]
        [StringLength(15)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Maxis Principal Line must be a number.")]
        [Display(Name = "Maxis Principal Line")]
        public string PostpaidMobileNo { get; set; }

        [DataMember]
        [StringLength(100)]
        [Display(Name = "Email")]
        // [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "Invalid email address.")]
        [RegularExpression(@"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$", ErrorMessage = "Invalid email address.")]
        public string EmailAddr { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Payment Mode")]
        public int PayModeID { get; set; }    //Cash, CreditCard, Cheque, Auto Debit

        [Association("refPaymentMode", "PayModeID", "")]
        public virtual PaymentMode PayMode { get; set; }

        //Credit Card
        [DataMember]
        [Display(Name = "Card Type *")]
        public int? CardTypeID { get; set; } //AMEX, DINERS, VISA, Master

        [Association("refCardType", "CardTypeID", "")]
        public virtual CardType CardType { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Name On Card *")]
        public string NameOnCard { get; set; }

        [DataMember]
        [StringLength(100)]
        [MinLength(13, ErrorMessage = "Card No must be minimum 13 digits.")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Card No must be a number.")]
        [Display(Name = "Card No *")]
        public string CardNo { get; set; }

        [DataMember]
        [StringLength(7)]
        [Display(Name = "Expiry Date *")]
        public string CardExpiryDate { get; set; }  // mm/yyyy

        [DataMember]
        [MaxLength(100)]
        [Display(Name = "Business Registration Name")]
        public string BizRegName { get; set; }

        [DataMember]
        [MaxLength(100)]
        [Display(Name = "Business Registration No.")]
        public string BizRegNo { get; set; }

        [DataMember]
        [MaxLength(100)]
        [Display(Name = "Signatory Name")]
        public string SignatoryName { get; set; }

        [DataMember]
        [MaxLength(100)]
        [Display(Name = "Person In Charge")]
        public string PersonInCharge { get; set; }

        [DataMember]
        public int IsVIP { get; set; }




        [DataMember]
        public string CorpMasterID { get; set; }

        [DataMember]
        public string CorpParentID { get; set; }

        [DataMember]
        public string CorpParentExtId { get; set; }

        [DataMember]
        public string CorpMasterExtId { get; set; }


        [DataMember]
        public string MarketCode { get; set; }

        [DataMember]
        public string AccountCategory { get; set; }
    }

    #endregion

    #region Reg Model Group Model

    [DataContract]
    [Serializable]
    [Table("lnkRegMdlGrpModel")]
    public class RegMdlGrpModel : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Registration *")]
        public int RegID { get; set; }

        [Association("trnRegistration", "RegID", "ID")]
        public virtual Registration Reg { get; set; }

        [DataMember]
        [Display(Name = "MGP Model")]
        public int? ModelGroupModelID { get; set; }

        [Association("lnkMdlGrpPackageModel", "MdlGrpPackageModelID", "")]
        public virtual ModelGroupModel ModelGroupModel { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Model Image *")]
        public int ModelImageID { get; set; }

        [Association("lnkModelImage", "ModelImageID", "")]
        public virtual ModelImage ModelImage { get; set; }

        [DataMember]
        [Display(Name = "Price")]
        [Range(0, 100000)]
        public decimal? Price { get; set; }

        [DataMember]
        [Display(Name = "Version No *")]
        public int VersionNo { get; set; }

		[DataMember]
		public int? SuppLineID { get; set; }

        //14012015 - Anthony - GST Trade Up - Start
        [DataMember]
        [Display(Name = "IsTradeUp")]
        public bool IsTradeUp { get; set; }

        [DataMember]
        [Display(Name = "TradeUpAmount")]
        public decimal TradeUpAmount { get; set; }
        //14012015 - Anthony - GST Trade Up - End
    }


    [DataContract]
    [Serializable]
    [Table("lnkRegMdlGrpModelSec")]
    public class RegMdlGrpModelSec : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Registration *")]
        public int SecRegID { get; set; }

        [Association("trnRegistrationSec", "SecRegID", "ID")]
        public virtual Registration SecReg { get; set; }

        [DataMember]
        [Display(Name = "MGP Model")]
        public int? ModelGroupModelID { get; set; }

        [Association("lnkMdlGrpPackageModel", "MdlGrpPackageModelID", "")]
        public virtual ModelGroupModel ModelGroupModel { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Model Image *")]
        public int ModelImageID { get; set; }

        [Association("lnkModelImage", "ModelImageID", "")]
        public virtual ModelImage ModelImage { get; set; }

        [DataMember]
        [Display(Name = "Price")]
        [Range(0, 100000)]
        public decimal? Price { get; set; }

        [DataMember]
        [Display(Name = "Version No *")]
        public int VersionNo { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("lnkRegMdlGrpModelSupp")]
    public class RegMdlGrpModelSupp : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Registration *")]
        public int RegID { get; set; }

        [Association("lnkRegSuppLine", "RegID", "ID")]
        public virtual RegSuppLine Reg { get; set; }

        [DataMember]
        [Display(Name = "MGP Model")]
        public int? ModelGroupModelID { get; set; }

        [Association("lnkMdlGrpPackageModel", "MdlGrpPackageModelID", "")]
        public virtual ModelGroupModel ModelGroupModel { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Model Image *")]
        public int ModelImageID { get; set; }

        [Association("lnkModelImage", "ModelImageID", "")]
        public virtual ModelImage ModelImage { get; set; }

        [DataMember]
        [Display(Name = "Price")]
        [Range(0, 100000)]
        public decimal? Price { get; set; }

        [DataMember]
        [Display(Name = "Version No *")]
        public int VersionNo { get; set; }
    }

	[DataContract]
	[Serializable]
	public class RegAttributeReq
	{
		[DataMember]
		public RegAttributes RegAttributes { get; set; }	
	}


    [DataContract]
    [Serializable]
    public class RegMdlGrpModelFind : FindBase
    {
        [DataMember]
        public RegMdlGrpModel RegMdlGrpModel { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public bool? IsPrincipal { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelSecFind : FindBase
    {
        [DataMember]
        public RegMdlGrpModelSec RegMdlGrpModelSec { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Reg PgmBdlPkgComponent

    [DataContract]
    [Serializable]
    [Table("lnkRegPgmBdlPkgCompSec")]
    public class RegPgmBdlPkgCompSec : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Registration *")]
        public int RegID { get; set; }

        [Association("trnRegistrationSec", "RegID", "ID")]
        public virtual Registration Reg { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "PBPC *")]
        public int PgmBdlPckComponentID { get; set; }

        [Association("lnkPgmBdlPckComponent", "PgmBdlPckComponentID", "")]
        public virtual PgmBdlPckComponent PgmBdlPckComponent { get; set; }


        [DataMember]
        [Display(Name = "Supp Line")]
        public int? RegSuppLineID { get; set; }

        [Association("lnkRegSuppLine", "RegSuppLineID", "")]
        public virtual RegSuppLine RegSuppLine { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "New Account? *")]
        public bool IsNewAccount { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Version No *")]
        public int VersionNo { get; set; }

        [DataMember]
        [NotMapped]
        public int SequenceNo { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("lnkRegPgmBdlPkgComp")]
    public class RegPgmBdlPkgComp : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Registration *")]
        public int RegID { get; set; }

        [Association("trnRegistration", "RegID", "ID")]
        public virtual Registration Reg { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "PBPC *")]
        public int PgmBdlPckComponentID { get; set; }

        [Association("lnkPgmBdlPckComponent", "PgmBdlPckComponentID", "")]
        public virtual PgmBdlPckComponent PgmBdlPckComponent { get; set; }


        [DataMember]
        [Display(Name = "Supp Line")]
        public int? RegSuppLineID { get; set; }

        [Association("lnkRegSuppLine", "RegSuppLineID", "")]
        public virtual RegSuppLine RegSuppLine { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "New Account? *")]
        public bool IsNewAccount { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Version No *")]
        public int VersionNo { get; set; }

        [DataMember]
        [NotMapped]
        public int SequenceNo { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompFind : FindBase
    {
        [DataMember]
        public RegPgmBdlPkgComp RegPgmBdlPkgComp { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompSecFind : FindBase
    {
        [DataMember]
        public RegPgmBdlPkgCompSec RegPgmBdlPkgCompSec { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Reg Address

    [DataContract]
    [Serializable]
    [Table("lnkRegAddress")]
    public class Address : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        //[Display(Name = "Reg ID")]
        //public int RegID { get; set; }
        //public virtual Registration Registration { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Registration *")]
        public int RegID { get; set; }

        [Association("trnRegistration", "RegID", "ID")]
        public virtual Registration Reg { get; set; }

        [DataMember]
        //[Required]
        [Display(Name = "Version No *")]
        public int VersionNo { get; set; }

        [DataMember]
        //[Required]
        [Display(Name = "Address Type *")]
        public int AddressTypeID { get; set; } //1=Permanent, 2=Billing, 3=Installtion

        [Association("refAddressType", "AddressTypeID", "ID")]
        public virtual AddressType AddressType { get; set; }

        [Required]
        [DataMember]
        [StringLength(150)]
        [Display(Name = "Address *")]
        public string Line1 { get; set; }

        [DataMember]
        [StringLength(150)]
        [Display(Name = " ")]
        public string Line2 { get; set; }

        #region VLT ADDED CODE
        [DataMember]
        [StringLength(150)]
        [Display(Name = " ")]
        public string Line3 { get; set; }
        #endregion VLT ADDED CODE

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Unit No")]
        public string UnitNo { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Street/Area *")]
        public string Street { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Building/House No *")]
        public string BuildingNo { get; set; }

        [Required]
        [DataMember]
        [StringLength(100)]
        [Display(Name = "Town/City Name *")]
        public string Town { get; set; }

        [Required]
        [DataMember]
        [StringLength(5, ErrorMessage = "The field Postcode * must be a number with a maximum length of 5.")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Postcode must be a number.")]
        [Display(Name = "Postcode *")]
        public string Postcode { get; set; }


        [DataMember]
        [Display(Name = "State/Province *")]
        public int StateID { get; set; }

        [Association("refCountryState", "StateID", "")]
        public virtual State State { get; set; }

        [DataMember]
        [Display(Name = "Country")]
        public int CountryID { get; set; }

        [Association("refCountry", "CountryID", "")]
        public virtual Country Country { get; set; }

    }

    [DataContract]
    [Serializable]
    public class AddressFind : FindBase
    {
        [DataMember]
        public Address Address { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Reg Status

    [DataContract]
    [Serializable]
    [Table("lnkRegStatus")]
    public class RegStatus : TimedEntity
    {
		public const string CB_POS = "POS";

        [DataMember]
        [Key]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Registration *")]
        public int RegID { get; set; }

        [Association("trnRegistration", "RegID", "ID")]
        public virtual Registration Reg { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Status *")]
        public int StatusID { get; set; }

        [Association("refStatus", "StatusID", "")]
        public virtual Status Status { get; set; }

        [DataMember]
        [Display(Name = "Reason Code *")]
        public int? ReasonCodeID { get; set; }

        //[Association("refStatusReason", "ReasonCodeID", "")]
        //public virtual StatusReason StatusReason { get; set; }

        [DataMember]
        [Display(Name = "Schedule Date *")]
        public DateTime? StatusLogDT { get; set; }

        [DataMember]
        [Display(Name = "Status Remark")]
        [DataType(DataType.MultilineText)]
        [MaxLength(4000)]
        [Column(TypeName = "nText")]
        public string Remark { get; set; }

        [DataMember]
        [MaxLength(100)]
        public string ResponseCode { get; set; }

        [DataMember]
        [MaxLength(500)]
        public string ResponseDescription { get; set; }

        [DataMember]
        [MaxLength(500)]
        public string MethodName { get; set; }

        [DataMember]
        public int? BackEndEngineStatus { get; set; }

    }

    [DataContract]
    [Serializable]
    public class RegStatusFind : FindBase
    {
        public RegStatusFind()
        {
            RegistrationIDs = new List<int>();
            RegStatus = new RegStatus();
        }
        [DataMember]
        public RegStatus RegStatus { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public List<int> RegistrationIDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegStatusResult
    {
        [DataMember]
        public int RegID { get; set; }

        [DataMember]
        public string RegStatus { get; set; }

        [DataMember]
        public int RegStatusID { get; set; }

        [DataMember]
        public string ReasonCode { get; set; }

        [DataMember]
        [Display(Name = "Status Remark")]
        [DataType(DataType.MultilineText)]
        [MaxLength(4000)]
        [Column(TypeName = "nText")]
        public string Remark { get; set; }

        [DataMember]
        public DateTime? StatusLogDT { get; set; }

        [DataMember]
        public DateTime CreateDT { get; set; }

        [DataMember]
        public string LastAccessID { get; set; }

        [DataMember]
        public string ModemID { get; set; }

        [DataMember]
        public string KenanAccountNo { get; set; }
    }

    #endregion

    #region Reg Supp Line

    [DataContract]
    [Serializable]
    [Table("lnkRegSuppLine")]
    public class RegSuppLine : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "Registration *")]
        public int RegID { get; set; }

        [Association("trnRegistration", "RegID", "ID")]
        public virtual Registration Reg { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "PBPC *")]
        public int PgmBdlPckComponentID { get; set; }

        [Association("lnkPgmBdlPckComponent", "PgmBdlPckComponentID", "")]
        public virtual PgmBdlPckComponent PgmBdlPckComponent { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "New Account? *")]
        public bool IsNewAccount { get; set; }

        [DataMember]
        [StringLength(15)]
        [Display(Name = "Desire MSISDN 1")]
        public string MSISDN1 { get; set; }

        [DataMember]
        [StringLength(15)]
        [Display(Name = "Desire MSISDN 2")]
        public string MSISDN2 { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Version No *")]
        public int VersionNo { get; set; }

        [DataMember]
        [Display(Name = "SIM Model")]
        public int? SimModelId { get; set; }

        [DataMember]
        [Display(Name = "IMEI Number*")]
        public string IMEINumber { get; set; }

        [DataMember]
        [Display(Name = "SIM Serial")]
        [StringLength(30)]
        public string SIMSerial { get; set; }

        [DataMember]
        [Display(Name = "Kenan Status")]
        public bool KENAN_Req_Status { get; set; }

        [DataMember]
        [NotMapped]
        public int SequenceNo { get; set; }

        [DataMember]
        [NotMapped]
        public string Description { get; set; }

        [DataMember]
        [Display(Name = "International Roaming")]
        public bool InternationalRoaming { get; set; }

        [DataMember]
        [Display(Name = "K2Type")]
        public bool K2Type { get; set; }

        [DataMember]
        [Display(Name = "Plan Advance")]
        public decimal planadvance { get; set; }

        [DataMember]
        [Display(Name = "Device Advance")]
        public decimal deviceadvance { get; set; }

        [DataMember]
        [Display(Name = "Plan Deposit")]
        public decimal plandeposit { get; set; }

        [DataMember]
        [Display(Name = "Device Deposit")]
        public decimal devicedeposit { get; set; }

        [DataMember]
        [Display(Name = "UOM Code")]
        public string UOMCode { get; set; }

        [DataMember]
        [Display(Name = "Article ID")]
        public string ArticleID { get; set; }

        [DataMember]
        [Display(Name = "Status Description")]
        public string STATUS_DESC { get; set; }

        [DataMember]
        [Display(Name = "No Of Retries")]
        public int? NoOfRetries { get; set; }

        [DataMember]
        [Display(Name = "MNPSuppLineSimCardType")]
        public string MNPSuppLineSimCardType { get; set; }

        [DataMember]
        [Display(Name = "Brand device article")]
        public string BrandDeviceArticleID { get; set; }

        [DataMember]
        [NotMapped]
        public int? ModelGroupModelID { get; set; }

        [DataMember]
        public int? ModelImageID { get; set; }

        [DataMember]
        public decimal? Price { get; set; }

        [DataMember]
        public string EXT_ORDER_ID { get; set; }

        [DataMember]
        public string Order_Status { get; set; }

        [DataMember]
        public string MismType { get; set; }

        [DataMember]
        public decimal? OfferID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSuppLineFind : FindBase
    {
        [DataMember]
        public RegSuppLine RegSuppLine { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Biometric

    [DataContract]
    [Serializable]
    [Table("trnBiometrics")]
    public class Biometrics : BaseEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Registration")]
        public int? RegID { get; set; }

        [Association("trnRegistration", "RegID", "ID")]
        public virtual Registration Reg { get; set; }

        [DataMember]
        [StringLength(150)]
        [Display(Name = " ")]
        public string Name { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string NewIC { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string OldIC { get; set; }

        [DataMember]
        [StringLength(15)]
        [Display(Name = " ")]
        public string Gender { get; set; }

        [DataMember]
        [StringLength(10)]
        [Display(Name = " ")]
        public string DOB { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string Nationality { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string Race { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string Religion { get; set; }

        [DataMember]
        [StringLength(150)]
        [Display(Name = " ")]
        public string Address1 { get; set; }

        [DataMember]
        [StringLength(150)]
        [Display(Name = " ")]
        public string Address2 { get; set; }

        [DataMember]
        [StringLength(150)]
        [Display(Name = " ")]
        public string Address3 { get; set; }

        [DataMember]
        [StringLength(10)]
        [Display(Name = " ")]
        public string Postcode { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string City { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string State { get; set; }

        [DataMember]
        [StringLength(1)]
        [Display(Name = " ")]
        public string FingerMatch { get; set; }

        [DataMember]
        [StringLength(1)]
        [Display(Name = " ")]
        public string FingerThumb { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BiometricFind : FindBase
    {
        [DataMember]
        public Biometrics Biometric { get; set; }
    }

    #endregion

    #region Reg Account Creation

    [DataContract]
    [Serializable]
    [Table("lnkRegAccountCreation")]
    public class RegAccount : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Registration")]
        public int RegID { get; set; }

        [DataMember]
        [Display(Name = " ")]
        public DateTime? KenanCreateAccountDT { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string KenanAccountExternalNo { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string KenanAccountInternalNo { get; set; }

        [DataMember]
        [Display(Name = " ")]
        public DateTime? KenanActivateServiceDT { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string KenanServiceInternalNo { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string KenanServiceResetInternalNo { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string KenanOrderID { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = " ")]
        public string KenanResetOrderID { get; set; }

        [DataMember]
        [Display(Name = " ")]
        public string KenanCreateAccountErrNo { get; set; }

        [DataMember]
        [StringLength(2000)]
        [Display(Name = " ")]
        public string KenanCreateAccountErrMessage { get; set; }

        [DataMember]
        [Display(Name = " ")]
        public string KenanActivateServiceErrNo { get; set; }

        [DataMember]
        [StringLength(2000)]
        [Display(Name = " ")]
        public string KenanActivateServiceErrMessage { get; set; }

        [DataMember]
        [StringLength(30)]
        [Display(Name = " ")]
        public string ServiceOrderId { get; set; }
        [DataMember]
        [StringLength(10)]
        public string Lobtype { get; set; }
    }


    [DataContract]
    [Serializable]
    public class RegAccountFind : FindBase
    {
        [DataMember]
        public RegAccount RegAccount { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Payment Details

    #region Card Type Payment Details

    [DataContract]
    [Serializable]
    [Table("tblcardtype")]
    public class CardTypePaymentDetails : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(4000)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypePaymentDetailsFind : FindBase
    {
        [DataMember]
        public CardTypePaymentDetails CardTypePaymentDetails { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Payment Mode Payment Details

    [DataContract]
    [Serializable]
    [Table("tblpaymentmode")]
    public class PaymentModePaymentDetails : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        [Display(Name = "Code *")]
        [RegularExpression(@"^[a-zA-Z0-9_-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [StringLength(4000)]
        [Display(Name = "Description *")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }
    }

    [DataContract]
    public class PaymentModePaymentDetailsFind : FindBase
    {
        [DataMember]
        public PaymentModePaymentDetails PaymentModePaymentDetails { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region Customer Information

    [DataContract]
    [Serializable]
    [Table("tblCustomerinfo")]
    public class CustomerInfo : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "Payment Mode")]
        public int PayModeID { get; set; }    //Cash, CreditCard, Cheque, Auto Debit

        [Association("tblpaymentmode", "PayModeID", "")]
        public virtual PaymentModePaymentDetails PayMode { get; set; }

        //Credit Card
        [DataMember]
        [Display(Name = "Card Type *")]
        public int? CardTypeID { get; set; } //AMEX, DINERS, VISA, Master

        [Association("tblcardtype", "CardTypeID", "")]
        public virtual CardTypePaymentDetails CardType { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Name On Card *")]
        public string NameOnCard { get; set; }

        [DataMember]
        [StringLength(16)]
        [MinLength(16, ErrorMessage = "Card No must be 16 digits.")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Card No must be a number.")]
        [Display(Name = "Card No *")]
        public string CardNo { get; set; }

        [DataMember]
        [StringLength(4)]
        [MinLength(4, ErrorMessage = "Expiry Date must be 4 digits.")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Expiry Date must be a number.")]
        [Display(Name = "Expiry Date *")]
        public string CardExpiryDate { get; set; }  // mm/yyyy

        [DataMember]
        [StringLength(3)]
        [MinLength(3, ErrorMessage = "CVV must be 3 digits.")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Expiry Date must be a number.")]
        [Display(Name = "CVV *")]
        public string CVV { get; set; }

        [DataMember]
        [Display(Name = "Order ID *")]
        public string OrderID { get; set; }

        [DataMember]
        [Display(Name = "Amount *")]
        public string Amount { get; set; }

    }

    [DataContract]
    [Serializable]
    public class CustomerInfoFind : FindBase
    {
        [DataMember]
        public CustomerInfo CustomerInfo { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #endregion

    //Added by VLT on 5 Mar 2013 for adding Kenan customer Info// 

    #region KenanCustomerInfo

    [DataContract]
    [Serializable]
    [Table("kenamCustomerInfo")]
    public class KenamCustomerInfo : ActiveEntity
    {
        [DataMember]
        [Key]
        public int ID { get; set; }


        [DataMember]
        [Display(Name = "RegID *")]
        public int RegID { get; set; }

        [DataMember]
        [StringLength(200)]
        [Display(Name = "Fullname *")]
        public string FullName { get; set; }


        [DataMember]
        [Display(Name = "ID Type *")]
        public int IDCardTypeID { get; set; }   // New IC, Old IC, Police/Military, Passport, Driving License

        [Association("refIDCardType", "IDCardTypeID", "")]
        public virtual IDCardType IDCardType { get; set; }


        [Required]
        [DataMember]
        [StringLength(15)]
        //[RegularExpression(@"^[a-zA-Z0-9]{0,}$", ErrorMessage = "ID Card No special characters are not allow.")]
        [Display(Name = "ID Card No *")]
        public string IDCardNo { get; set; }


        //Personal Details
        [DataMember]
        [DataType(DataType.Date)]
        [Column(TypeName = "date")]
        [Display(Name = "DOB *")]
        public DateTime DateOfBirth { get; set; }  //dd/mm/yyyy



        [DataMember]
        [Display(Name = "Nationality *")]
        public int NationalityID { get; set; }

        [Association("refNationality", "NationalityID", "")]
        public virtual Nationality Nationality { get; set; }


        [DataMember]
        [StringLength(25)]
        //[Required]
        //[RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Mobile No must be a number.")]
        /*Changed by chetan on 06/03/2013*/
        //[Display(Name = "Mobile No *")]
        [Display(Name = "Contact No *")]
        public string ContactNo { get; set; }




        [DataMember]
        [StringLength(150)]
        [Display(Name = "Address *")]
        public string Address1 { get; set; }


        [DataMember]
        [StringLength(150)]
        [Display(Name = "Address *")]
        public string Address2 { get; set; }


        [DataMember]
        [StringLength(150)]
        [Display(Name = "Address *")]
        public string Address3 { get; set; }

        [Required]
        [DataMember]
        [StringLength(100)]
        [Display(Name = "Town/City Name *")]
        public string Town { get; set; }

        [Required]
        [DataMember]
        [StringLength(6)]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Postcode must be a number.")]
        [Display(Name = "Postcode *")]
        public string Postcode { get; set; }


        [DataMember]
        [Display(Name = "State/Province *")]
        public int StateID { get; set; }

        [Association("refCountryState", "StateID", "")]
        public virtual State State { get; set; }


    }
    //Added by VLT on 5 Mar 2013 for adding Kenan customer Info//
    #endregion



    #region CustomerFind
    [DataContract]
    [Serializable]
    public class CustomerFind : FindBase
    {
        [DataMember]
        public Customer Customer { get; set; }
        public DateTime? DateOfBirthFrom { get; set; }
        [DataMember]
        public DateTime? DateOfBirthTo { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    #region IMPOS Details
    [DataContract]
    [Serializable]
    public class IMPOSDetails
    {
        [DataMember]
        public Int32 ID { get; set; }
        [DataMember]
        public string ArticleID { get; set; }
        [DataMember]
        public string MSISDN1 { get; set; }
        [DataMember]
        public string KenanAccountNo { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public string Line1 { get; set; }
        [DataMember]
        public string Line2 { get; set; }
        [DataMember]
        public string Line3 { get; set; }
        [DataMember]
        public string Town { get; set; }
        [DataMember]
        public string Postcode { get; set; }
        [DataMember]
        public string IMEINumber { get; set; }
        [DataMember]
        public int ModelID { get; set; }
        [DataMember]
        public string UOMCODE { get; set; }
        [DataMember]
        public string OrganisationId { get; set; }
        [DataMember]
        public Int32 RegTypeID { get; set; }
        [DataMember]
        public Int32 SimModelId { get; set; }
        [DataMember]
        public string SimNo { get; set; }
        [DataMember]
        public string ModelName { get; set; }
        //added by Deepika
        [DataMember]
        public string OrgCode { get; set; }
        [DataMember]
        public string Poskey { get; set; }
        [DataMember]
        public string SmartId { get; set; }
        [DataMember]
        public string DiscountId { get; set; }
        [DataMember]
        public decimal DiscountAmt { get; set; }


        //Code Added by VLT for Plans and Deposits on Apr 20 2013
        [DataMember]
        public decimal PlanAdvance { get; set; }
        [DataMember]
        public decimal PlanDeposit { get; set; }
        [DataMember]
        public decimal Deviceadvance { get; set; }
        [DataMember]
        public decimal Devicedeposit { get; set; }
        //Endof  Code Added by VLT for Plans and Deposits on Apr 20 2013

        //Code Added by VLT for Plans and Deposits on May 01 2012
        [DataMember]
        public bool InternationalRoaming { get; set; }
        [DataMember]
        public bool K2Type { get; set; }
        //Endof  Code Added by VLT for Plans and Deposits May 01 2012

        [DataMember]
        public string Trn_Type { get; set; }

        //Added by Narayana on 13-05-2013 to get dynamically from DB for organisation
        [DataMember]
        public string POSNetworkPath { get; set; }
        [DataMember]
        public string POSUserName { get; set; }
        [DataMember]
        public string POSPassword { get; set; }
        [DataMember]
        public string POSDomain { get; set; }
        //Added by Narayana on 13-05-2013 to get dynamically from DB for organisation
        [DataMember]
        public string Smart_Id { get; set; }
        [DataMember]
        public string SimType { get; set; }
        [DataMember]
        public bool CallConferencing { get; set; }

        [DataMember]
        public string ExternalID { get; set; }

        [DataMember]
        public decimal UpfrontPayment { get; set; }
        [DataMember]
        public string TUTSerial { get; set; }
        [DataMember]
        public int TUTSimModelId { get; set; }

        //Added by ravi on 06-03-2014 to get secondary articleId and sim
        [DataMember]
        public string SecSimSerial { get; set; }

        [DataMember]
        public string SecArticleId { get; set; }
        //Added by ravi on 06-03-2014 to get secondary articleId and sim ends here

        //14012015 - Anthony - GST Trade Up - Start
        [DataMember]
        public bool IsTradeUp { get; set; }

        [DataMember]
        public decimal TradeUpAmount { get; set; }
        //14012015 - Anthony - GST Trade Up - End


    }
    #endregion

    [DataContract]
    [Serializable]
    public class IMPOSDetailsForAccessory : IMPOSDetails
    {
        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public string AccessoryName { get; set; }

        [DataMember]
        public string OfferName { get; set; }
    }

    #region WhiteList Details
    [DataContract]
    [Serializable]
    public class WhiteListDetails
    {
        [DataMember]
        public string Description { get; set; }

    }
    #endregion

    #region ExtendedDetailsForDevice
    [DataContract]
    [Serializable]
    public class ExtendedDetailsForDevice
    {
        [DataMember]
        public int RegID { get; set; }
        [DataMember]
        public string IMEINumber { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public decimal AdvancePrice { get; set; }
        [DataMember]
        public decimal Price { get; set; }

    }
    #endregion

    #region MocOfferDetails

    [DataContract]
    [Serializable]
    public class MOCOfferDetails
    {
        [DataMember]
        public Int32 ID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string OfferDescription { get; set; }
        [DataMember]
        public double DiscountAmount { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
    }
    #endregion

	#region PegaLogDetails Added by Gerry
	[DataContract]
	[Serializable]
	[Table("tblPegaXmlLogs")]
	public class PegaLogDetails
	{
		[DataMember]
		public int ID { get; set; }
		[DataMember]
		public string SalesPerson { get; set; }
		[DataMember]
		public string MethodName { get; set; }
		[DataMember]
		public string Msisdn { get; set; }
		[DataMember]
		public string SubscriberNo { get; set; }
		[DataMember]
		public string PegaXmlReq { get; set; }
		[DataMember]
		public string PegaXmlRes { get; set; }
		[DataMember]
		public string Remarks { get; set; }

	}
	#endregion

    #region MocOfferDetails


    #region KenanXmlLogs Added by Narayana
    [DataContract]
    [Serializable]
    [Table("trnKenanXmlLogs")]
    public class KenanaLogDetails
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int RegID { get; set; }
        [DataMember]
        public string MessageCode { get; set; }
        [DataMember]
        public string MessageDesc { get; set; }
        [DataMember]
        public string MethodName { get; set; }
        [DataMember]
        public string KenanXmlReq { get; set; }
        [DataMember]
        public string KenanXmlRes { get; set; }
        [DataMember]
        public string EXT_ORDER_ID { get; set; }

    }
    #endregion
    #endregion

    #region IMPOSSpecDetails Added by Nreddy
    [DataContract]
    [Serializable]
    [Table("tblIMPOSSpec")]
    public class IMPOSSpecDetails
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }
        [DataMember]
        [Display(Name = "POSKey")]
        public string POSKey { get; set; }
        [DataMember]
        public string POSNetworkPath { get; set; }
        [DataMember]
        public string POSUserName { get; set; }
        [DataMember]
        public string POSPassword { get; set; }
        [DataMember]
        public string POSDomain { get; set; }
        [DataMember]
        public string IMPOSFilePath { get; set; }
        [DataMember]
        public string Tx_type { get; set; }
        [DataMember]
        public string GSMAdvPayment { get; set; }
        [DataMember]
        public string GSMRegDeposit { get; set; }
        [DataMember]
        public string MaxisDeviceDeposit { get; set; }
        [DataMember]
        public string DepositiValue { get; set; }
        [DataMember]
        public string UOMIDForAdvPaymntAndSIM { get; set; }
        [DataMember]
        public string UOMIDForAdvPaymnt { get; set; }
        [DataMember]
        public string UOMIDForSIM { get; set; }
        [DataMember]
        public string UOMIDForPlanDeposit { get; set; }
        [DataMember]
        public string UOMIDForDeviceDeposit { get; set; }
        [DataMember]
        public string UOMIDForInternationalRoaming { get; set; }
        [DataMember]
        public string InternationalRoamingArticleId { get; set; }
        [DataMember]
        public string CallConferencingArticleID { get; set; }
        [DataMember]
        public string UOMIDForCallConferencing { get; set; }
        [DataMember]
        public string UpfrontPaymentArticleId { get; set; }
        [DataMember]
        public string UOMIDForUpfrontPayment { get; set; }
        [DataMember]
        public string ArticleIDForTUT { get; set; }
        [DataMember]
        public string UOMCodeForTUT { get; set; }

    }
    #endregion


    #region changePackage IsellHome
    [DataContract]
    [Serializable]
    [Table("refFilterChangePckgs")]
    public class FilterChangePckgs
    {
        [DataMember]
        [Key]
        public int Id { get; set; }
        [DataMember]
        public string Oldpackage { get; set; }
        [DataMember]
        public string Newpackage { get; set; }
        [DataMember]
        public bool IsVoipReq { get; set; }
        [DataMember]
        public bool IsChangePackageReq { get; set; }
        [DataMember]
        public bool IsChangeContractReq { get; set; }
        [DataMember]
        public bool IsdowngradePckg { get; set; }
    }
    #endregion

    [DataContract]
    [Serializable]
    [Table("lnkRegLob")]
    public class RegLob
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public int RegId { get; set; }
        [DataMember]
        public string Lob { get; set; }
        [DataMember]
        public DateTime CreateDt { get; set; }
        [DataMember]
        public DateTime? LastUpdateDt { get; set; }
        [DataMember]
        public int HomeServiceStatusId { get; set; }
        [DataMember]
        public int? closestatus { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("lnkRegRemarks")]
    public class RegRemark
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public int RegID { get; set; }
        [DataMember]
        public string Remark { get; set; }
        [DataMember]
        public int StatusId { get; set; }
        [DataMember]
        public int? StatusReasonId { get; set; }
        [DataMember]
        [Association("refStatusReason", "StatusReasonId", "ID")]
        public virtual StatusReason StatusReason { get; set; }
    }
    #region "Campaign"
    [DataContract]
    [Serializable]
    [Table("refCampaign")]
    public class Campaign : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(400)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-/]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }
        [DataMember]
        [Required]
        [Display(Name = "Start Date *")]
        public DateTime StartDate { get; set; }
        [DataMember]
        [Required]
        [Display(Name = "End Date *")]
        public DateTime EndDate { get; set; }

        [DataMember]
        [StringLength(400)]
        public string Remark { get; set; }

    }

    [DataContract]
    [Serializable]
    public class CampaignFind : FindBase
    {
        [DataMember]
        public Provider Campaign { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public DateTime? ViewDate { get; set; }
    }
    #endregion
    #region "Holidays"
    [DataContract]
    [Serializable]
    [Table("RefHolidays")]
    public class Holidays : ActiveEntity
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public DateTime HolidayOn { get; set; }
        [DataMember]
        public int? StateId { get; set; }
    }

    [DataContract]
    [Serializable]
    public class Slots
    {
        [DataMember]
        public int AMSlot { get; set; }
        [DataMember]
        public int PMSlot { get; set; }
    }
    #endregion
    [DataContract]
    [Serializable]
    [Table("lnkTempRegInstallers")]
    public class TempRegInstallers
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public int RegId { get; set; }
        [DataMember]
        public int AccessId { get; set; }

    }
    [DataContract]
    [Serializable]
    [Table("refBlockedInstallerGroups")]
    public class BlockedInstallerGroups : BaseEntity
    {
        [DataMember]
        [Key]
        public int Id { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public DateTime ScheduleDate { get; set; }
        [DataMember]
        public int RegId { get; set; }
    }
    [DataContract]
    [Serializable]
    [Table("refGroups")]
    public class Group
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int KPIPerDay { get; set; }
        [DataMember]
        public byte Type { get; set; }
    }
    [DataContract]
    [Serializable]
    [Table("lnkInstallerbyGroup")]
    public class InstallerbyGroup
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public int InstallerId { get; set; }
    }
    [DataContract]
    [Serializable]
    [Table("refProvider")]
    public class Provider : ActiveEntity
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(100)]
        [Display(Name = "Name *")]
        [RegularExpression(@"^[\w\s-/]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Name { get; set; }

        [DataMember]
        [StringLength(1000)]
        [Display(Name = "Description")]
        [RegularExpression(@"^[\w\s-]{0,}$", ErrorMessage = "Special characters are not allow.")]
        public string Description { get; set; }



    }

    [DataContract]
    [Serializable]
    [Table("lnkRegUserScheduling")]
    public class RegUserScheduling
    {
        [DataMember]
        [Key]
        public int Id { get; set; }

        [DataMember]
        [Required]
        public int RegStatusId { get; set; }

        [DataMember]
        [Required]
        public int AccessId { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
    [DataContract]
    [Serializable]
    [Table("refmobileregused")]
    public class Refmobileregused
    {
        [DataMember]
        public int id { get; set; }
        [DataMember]
        public long mobileno { get; set; }
        [DataMember]
        public bool active { get; set; }
        [DataMember]
        public long Regid { get; set; }

    }

    //Added by VLT on 17 May 2013 to get device image details dynamically
    [DataContract]
    [Serializable]
    public class DevicePlanData
    {
        [DataMember]
        public string PlanName { get; set; }
        [DataMember]
        public string ContractName { get; set; }
        [DataMember]
        public string DataPlan { get; set; }
        [DataMember]
        public int ContractTerm { get; set; }
        [DataMember]
        public decimal? RRP { get; set; }
        [DataMember]
		public decimal Price { get; set; }
        [DataMember]
        public decimal? MalayDeviceAdvance { get; set; }
        [DataMember]
        public decimal? MalayPlanAdvance { get; set; }
        [DataMember]
        public decimal? MalayDeviceDeposit { get; set; }
        [DataMember]
        public decimal? OtherDeviceDeposit { get; set; }

    }
    //Added by VLT on 17 May 2013 to get device image details dynamically


    //Added by VLT on 17 May 2013 to get device image details dynamically
    [DataContract]
    [Serializable]
    public class DeviceDetails
    {

        [DataMember]
        public BrandArticle BrandArticle { get; set; }

        [DataMember]
        public List<DevicePlanData> DevicePlanData { get; set; }
    }
    //Added by VLT on 17 May 2013 to get device image details dynamically
    [Serializable]
    [DataContract]
    [Table("lnkRegCancelReason")]
    public class RegistrationCancelReason
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        public int RegID { get; set; }

        [DataMember]
        public string CancelReason { get; set; }

        [DataMember]
        [Required]
        public DateTime CreateDT { get; set; }

        [DataMember]
        public DateTime? LastUpdateDT { get; set; }

        [DataMember]
        [Required]
        [MaxLength(20)]
        public string LastAccessID { get; set; }
    }



    [DataContract]
    [Serializable]
    [Table("tblSimReplacementReason")]
    public class SimReplacementReasons
    {
        [DataMember]
        [Key]
        public int Id { get; set; }

        [DataMember]
        [Required]
        [Display(Name = "Disconnect Reason")]
        public int DISCONNECT_REASON { get; set; }

        [DataMember]
        [Display(Name = "Language Code")]
        public int LANGUAGE_CODE { get; set; }

        [DataMember]
        [Display(Name = "Short Display")]
        public string SHORT_DISPLAY { get; set; }

        [DataMember]
        [Display(Name = "Display Value")]
        public string DISPLAY_VALUE { get; set; }

        [DataMember]
        public int NRC { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public bool IsWaiver { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public bool isPostPaid { get; set; }

        [DataMember]
        [Display(Name = "Active")]
        public bool? ISACTIVE { get; set; }
    }




    [DataContract]
    [Serializable]
    [Table("lnkPackageComponents")]
    public class PackageComponents
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [Required]
        [DataMember]
        public int RegID { get; set; }

        [Association("trnRegistration", "RegID", "ID")]
        public virtual Registration Reg { get; set; }
        [DataMember]
        public string PackageId { get; set; }
        [DataMember]
        public string packageInstId { get; set; }
        [DataMember]
        public string packageInstIdServ { get; set; }
        [DataMember]
        public string packageInactiveDt { get; set; }
        [DataMember]
        public string packageDesc { get; set; }
        [DataMember]
        public string componentId { get; set; }
        [DataMember]
        public string componentInstId { get; set; }
        [DataMember]
        public string componentInstIdServ { get; set; }
        [DataMember]
        public string componentActiveDt { get; set; }
        [DataMember]
        public string componentInactiveDt { get; set; }
        [DataMember]
        public string componentDesc { get; set; }
        [DataMember]
        public string componentShortDisplay { get; set; }
        [DataMember]
        public string lobType { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("trnCMSSCaseStatus")]
    public class CMSSComplain
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [Required]
        [DataMember]
        public int RegID { get; set; }

        [DataMember]
        public string CBRNo { get; set; }

        [DataMember]
        public string CMSSID { get; set; }

        /*Code for saving CMSS case details by chetan*/
        [DataMember]
        public string CMSSReqXML { get; set; }

        [DataMember]
        public string CMSSRespXML { get; set; }

        [DataMember]
        public string AccountNumber { get; set; }

        [DataMember]
        public DateTime CreateDate { get; set; }

    }


    //[DataContract]
    //[Serializable]
    //[Table("refContracts")]
    //public class Contracts
    //{
    //    [DataMember]
    //    [Key]
    //    public int ID { get; set; }

    //    [Required]
    //    [DataMember]
    //    public string KenanCode { get; set; }

    //    [DataMember]
    //    public string Name { get; set; }

    //    [DataMember]
    //    public string Description { get; set; }

    //    [DataMember]
    //    public bool Active { get; set; }
    //    [DataMember]
    //    public string Type { get; set; }
    //    [DataMember]
    //    public int Duration { get; set; }
    //    [DataMember]
    //    public int IsDeviceContract { get; set; }
    //}
    //#endregion
    //    #region CRP
    [DataContract]
    [Serializable]
    public class ModelPackageInfo
    {
        [DataMember]
        public string PackageName { get; set; }
        [DataMember]
        public int PackageId { get; set; }
        [DataMember]
        public string KenanCode { get; set; }
        [DataMember]
        public string packageType { get; set; }
        [DataMember]
        public int ComponentId { get; set; }
        [DataMember]
        public string ComponentKenanCode { get; set; }
        [DataMember]
        public string ComponentName { get; set; }
        [DataMember]
        public bool IsMandatory { get; set; }
        [DataMember]
        public string ComponentGroupId { get; set; }
        [DataMember]
        public string ComponentGroupName { get; set; }
        [DataMember]
        public bool isCRP { get; set; }
        [DataMember]
        public int isDefault { get; set; }
		[DataMember]
		public int isHidden { get; set; }
		[DataMember]
		public bool isSharing { get; set; }

    }

    [DataContract]
    [Serializable]
    public class RestrictedComponent
    {

        [DataMember]
        public string KenanCode { get; set; }
        [DataMember]
        public int ComponentId { get; set; }
        [DataMember]
        public int lnkPgmBdlId { get; set; }

    }

    [DataContract]
    [Serializable]
    [Table("lnkRegSmartComponents")]
    public class RegSmartComponents
    {
        [DataMember]
        [Key]
        public int Id { get; set; }

        [Required]
        [DataMember]
        public string RegID { get; set; }

        [DataMember]
        public string PackageID { get; set; }

        [DataMember]
        public string ComponentID { get; set; }

        [DataMember]
        public string ComponentDesc { get; set; }

        [DataMember]
        public string PackageDesc { get; set; }

    }






    [DataContract]
    [Serializable]
    [Table("trnRegistrationRequestType")]
    public class RegistrationRequestType
    {
        [DataMember]
        [Key]
        public int Id { set; get; }
        [DataMember]
        public int RegId { get; set; }
        [DataMember]
        public string RequestType { get; set; }
        [DataMember]
        public string Lob { get; set; }
        [DataMember]
        public string AcctExtId { get; set; }
        [DataMember]
        public string AcctIntId { get; set; }
        [DataMember]
        public string SusbcrNoResets { get; set; }
        [DataMember]
        public string SusbcrNo { get; set; }
        [DataMember]
        public string DisconnectReason { get; set; }
        [DataMember]
        public string waiveInstallmentNrc { get; set; }
        [DataMember]
        public string waiveUnbilledNrc { get; set; }
        [DataMember]
        public string waiveTerminationObligation { get; set; }
        [DataMember]
        public string waiveUnmetObligation { get; set; }
        [DataMember]
        public string waiveRefinanceNrc { get; set; }
        [DataMember]
        public string NewOrderInd { get; set; }
        [DataMember]
        public bool CMSSPending { get; set; }
        [DataMember]
        public string ExtId { get; set; }
        [DataMember]
        public string CmssNote { get; set; }
        [DataMember]
        public string Msisdn { get; set; }
        [DataMember]
        public bool isFromRelocation { get; set; }

    }


    [DataContract]
    [Serializable]
    [Table("trnRegPreviousDetails")]
    public class RegPreviousDetail : BaseEntity
    {
        [DataMember]
        [Key]
        public int Id { get; set; }
        [DataMember]
        public int ProviderId { get; set; }
        [DataMember]
        public string TmOrderId { get; set; }
        [DataMember]
        public string TmServiceId { get; set; }
        [DataMember]
        public int RegId { get; set; }
        [DataMember]
        public int StateId { get; set; }
        [DataMember]
        public string InstallationAddress { get; set; }
        [DataMember]
        public string BillingAddress { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("trnRetrieveAccountInfo")]
    public class RetrieveAccountInfo
    {
        [DataMember]
        [Key]
        public int Id { get; set; }
        [DataMember]
        public int RegId { get; set; }
        [DataMember]
        public string RequestType { get; set; }
        [DataMember]
        public string Lob { get; set; }
        [DataMember]
        public string fxAcctExtIdField { get; set; }
        [DataMember]
        public string fxAcctIntIdField { get; set; }
        [DataMember]
        public string fxExternalIdField { get; set; }
        [DataMember]
        public string fxSubscrNoResetsField { get; set; }
        [DataMember]
        public string fxSubscrNoField { get; set; }
        [DataMember]
        public string fxExternalIdTypeField { get; set; }

    }
    #region MISM
    [DataContract]
    [Serializable]
    [Table("tblDataPlanComponents")]
    public class tblDataPlanComponents
    {

        [DataMember]
        [Key]
        public int Id { get; set; }
        [DataMember]
        public string DataComponentID { get; set; }
        [DataMember]
        public bool Status { get; set; }
    }
    #endregion
    #region Getting Waiver Components
    [DataContract]
    [Serializable]
    [Table("lnkRegWaiverDetails")]
    public class WaiverComponents
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public int RegID { get; set; }
        [DataMember]
        public int ComponentID { get; set; }
        [DataMember]
        public string ComponentName { get; set; }
        [DataMember]
        public decimal price { get; set; }
        [DataMember]
        public bool IsWaived { get; set; }
        [DataMember]
        public string MismType { get; set; }
        [DataMember]
        public bool IsAutoWaived { get; set; }//21042015 - Anthony - To differentiate the autowaived component
    }

    #endregion

    #region supervisor waive off

    [DataContract]
    [Serializable]
    [Table("vwRegWaivedComponents")]
    public class RegWaivedComponents
    {
        [DataMember]
        public int RegID { get; set; }
        [DataMember]
        public string WaivedComponents { get; set; }
    }
    #endregion



    /// <summary>
    /// Added by sindhu in 11/09/2013 for print Version changes
    /// </summary>
    [DataContract]
    [Serializable]
    [Table("lnkPrintVersion")]
    public class PrintVersion
    {

        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        public int? RegType { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string TsCs { get; set; }

        [DataMember]
        public string Brand { get; set; }

        [DataMember]
        public string ConTC { get; set; }

        [DataMember]
        public bool? isActive { get; set; }

        [DataMember]
        public DateTime? CreateDT { get; set; }

        [DataMember]
        public DateTime? LastUpdateDT { get; set; }

        [DataMember]
        public string LastAccessID { get; set; }

        [DataMember]
        public string ConTC1 { get; set; }

        [DataMember]
        public string ConTC1ModelIds { get; set; }

        //addendum beased on plan- Sumit
        [DataMember]
        public string PlanKenanCode { get; set; }

        [DataMember]
        public string Trn_Type { get; set; }

        [DataMember]
        public bool? IsDefault { get; set; }
         
    }


    //[DataContract]
    //[Serializable]
    //[Table("lnkPrintVersion")]
    //public class PrintVersion
    //{

    //    [DataMember]
    //    [Key]
    //    public int ID { get; set; }

    //    [DataMember]
    //    public int? RegType { get; set; }

    //    [DataMember]
    //    public string Version { get; set; }

    //    [DataMember]
    //    public string TsCs { get; set; }

    //    [DataMember]
    //    public string Brand { get; set; }

    //    [DataMember]
    //    public string ConTC { get; set; }

    //    [DataMember]
    //    public bool? isActive { get; set; }

    //    [DataMember]
    //    public DateTime? CreateDT { get; set; }

    //    [DataMember]
    //    public DateTime? LastUpdateDT { get; set; }

    //    [DataMember]
    //    public string LastAccessID { get; set; }

    //    [DataMember]
    //    public string ConTC1 { get; set; }

    //    [DataMember]
    //    public string ConTC1ModelIds { get; set; }

    //    //addendum beased on plan- Sumit
    //    [DataMember]
    //    public string PlanKenanCode { get; set; }

   //[DataMember]
      //  public string Trn_Type { get; set; }

    //}





    [DataContract]
    [Serializable]
    [Table("tblUserSpecificPrinters")]
    public class UserSpecificPrintersInfo
    {
        [DataMember]
        [Key]
        public int id { get; set; }
        [DataMember]
        public int UserID { get; set; }
        [DataMember]
        public int OrgID { get; set; }
        [DataMember]
        public bool Active { get; set; }
        [DataMember]
        public int CreatedBy { get; set; }
        [DataMember]
        public int ModifiedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string PrinterName { get; set; }
        [DataMember]
        public string PrinterNetworkPath { get; set; }

    }

    [DataContract]
    [Serializable]
    [Table("TblUserPrintLog")]
    public class UserPrintLog
    {
        [DataMember]
        [Key]
        public int id { get; set; }
        [DataMember]
        public int UserID { get; set; }
        [DataMember]
        public int Orgid { get; set; }
        [DataMember]
        public string Printfile { get; set; }
        [DataMember]
        public string PrinterName { get; set; }
        [DataMember]
        public int status { get; set; }
        [DataMember]
        public string remarks { get; set; }
        [DataMember]
        public string PrinterNetworkPath { get; set; }
        [DataMember]
        public int RegID { get; set; }
        [DataMember]
        public DateTime PrintedDateTime { get; set; }


    }


    [DataContract]
    [Serializable]
    [Table("tblWaiverRules")]
    public class WaiverRules
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public string Liberization { get; set; }
        [DataMember]
        public bool Nationality { get; set; }
        [DataMember]
        public bool IsNewReg { get; set; }
        [DataMember]
        public bool PlanAdv { get; set; }
        [DataMember]
        public bool DeviceAdv { get; set; }
        [DataMember]
        public bool PlanDeposit { get; set; }
        [DataMember]
        public bool DeviceDeposit { get; set; }
        [DataMember]
        public bool IntRoaming { get; set; }
        [DataMember]
        public bool CallConf { get; set; }
        [DataMember]
        public bool CallConfPorted { get; set; }
        [DataMember]
        public bool PenaltyWaiver { get; set; }

    }


    #region Getting KenanComponentCode
    [DataContract]
    [Serializable]
    public class KenanComponentCodeInfo
    {

        [DataMember]
        public string COMPONENTKENANCODE { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public string CONTRACTTYPE { get; set; }

    }

    #endregion

    #region smart related methods


    [DataContract]
    [Serializable]
    [Table("tblRegCancellationReasons")]
    public class RegistrationCancellationReasons
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public string CancellationReason { get; set; }
        [DataMember]
        public bool? Active { get; set; }

    }
    [DataContract]
    [Serializable]
    public class DeviceDetailsSmart
    {

        [DataMember]
        public BrandArticleSmart BrandArticle { get; set; }

        [DataMember]
        public List<DevicePlanData> DevicePlanData { get; set; }
    }


    /// <summary>
    /*Changes by chetan related to PDPA implementation*/
    /// </summary>
    [DataContract]
    [Serializable]
    [Table("lnkPdpaData")]
    public class PDPAData
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string DocumentUrl { get; set; }

        [DataMember]
        public bool? isActive { get; set; }

        [DataMember]
        public string DocType { get; set; }

        [DataMember]
        public string TrnType { get; set; }

    }
    [DataContract]
    [Serializable]
    [Table("tblRebateDataContractComponents")]
    public class RebateDataContractComponents
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public string ComponentID { get; set; }
        [DataMember]
        public string ContractID { get; set; }
        [DataMember]
        public string ComponentName { get; set; }
        [DataMember]
        public string ContractName { get; set; }
        [DataMember]
        public string ContractType { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string TypeDescription { get; set; }
        [DataMember]
        public int value { get; set; }
        [DataMember]
        public string penalty { get; set; }
        [DataMember]
        public string DataUsageMB { get; set; }
        [DataMember]
        public bool Status { get; set; }
        [DataMember]
        public string nrcID { get; set; }
        [DataMember]
        public string nrcDescription { get; set; }
    }
    [DataContract]
    [Serializable]
    [Table("lnkRegRebateDatacontractPenalty")]
    public class RegRebateDatacontractPenalty
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public int RegID { get; set; }
        [DataMember]
        public string ComponetID { get; set; }
        //[DataMember]
        //public string Penalty { get; set; }
        [DataMember]
        public decimal Penalty { get; set; }
        [DataMember]
        public string nrcID { get; set; }
        [DataMember]
        public DateTime CreateDT { get; set; }
        [DataMember]
        public bool Active { get; set; }

    }

    #endregion

    #region "sim replacement secondary lines information "

    [DataContract]
    [Serializable]
    [Table("LNK_SECONDARYACCTDETAIL_SIMREPLACEMENT")]
    public class lnkSecondaryAcctDetailsSimRplc
    {
        [DataMember]
        [Key]
        public int SNO { get; set; }
        [DataMember]
        public int REGID { get; set; }
        [DataMember]
        public string EXTERNALID { get; set; }
        [DataMember]
        public string FxAcctNo { get; set; }
        [DataMember]
        public string FxSubscrNo { get; set; }
        [DataMember]
        public string FxSubscrNoResets { get; set; }
        [DataMember]
        public string SUPP_Msisdn { get; set; }
        [DataMember]
        public string PrimSecInd { get; set; }
        [DataMember]
        public DateTime CREATEDATE { get; set; }
        [DataMember]
        public bool STATUS { get; set; }
        [DataMember]
        public string DESCRIPTION { get; set; }

    }

    [DataContract]
    [Serializable]
    public class GetKenancodesByPlanID
    {
        [DataMember]
        public int RegId { get; set; }
        [DataMember]
        public int ComponentID { get; set; }
        [DataMember]
        public string ComponentKenancode { get; set; }
        [DataMember]
        public string LinkType { get; set; }
        [DataMember]
        public string PlanType { get; set; }
        [DataMember]
        public string PlanKenancode { get; set; }
    }

    #endregion


    #region Bre Check Treatment -Sharvin-
    [DataContract]
    [Serializable]
    [Table("BreCheckTreatment")]
    public class BreCheckTreatment {
    [Key]
    public int ID { get; set; }
    [DataMember]
    public string UserType { get; set; }
    [DataMember]
    public string BreCheck { get; set; }
    [DataMember]
    public string Flow { get; set; }
    [DataMember]
    public string BreTreatment { get; set; }
    }

    // w.loon - for testing purpose
    [DataContract]
    [Serializable]
    [Table("BreCheckTreatment_Test")]
    public class BreCheckTreatment_Test
    {
        [Key]
        public int ID { get; set; }
        [DataMember]
        public string UserType { get; set; }
        [DataMember]
        public string BreCheck { get; set; }
        [DataMember]
        public string Flow { get; set; }
        [DataMember]
        public string BreTreatment { get; set; }
    }
    #endregion

    [DataContract]
    [Serializable]
    [Table("TblDMEPrint")]
    public class UserDMEPrint
    {
        [DataMember]
        [Key]
        public int id { get; set; }
        [DataMember]
        public int UserID { get; set; }
        [DataMember]
        public int RegID { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("lnkRegBREStatus")]
    public class RegBREStatus
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int Id { get; set; }
        [DataMember]
        public int RegId { get; set; }
        [DataMember]
        public string TransactionStatus { get; set; }
        [DataMember]
        public string TransactionBy { get; set; }
        [DataMember]
        public string TransactionReason { get; set; }
        [DataMember]
        public DateTime? TransactionDT { get; set; }
        [DataMember]
        public string IsLockedBy { get; set; }
        
    }

    [DataContract]
    [Serializable]    
    public class PendingRegBREStatus
    {
        [DataMember]     
        [Display(Name = "ID")]
        public int Id { get; set; }
        [DataMember]
        public int RegId { get; set; }
        [DataMember]
        public string RegTypeDescription { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public string TransactionStatus { get; set; }
        [DataMember]
        public string TranType { get; set; }
        [DataMember]
        public string Customer { get; set; }         
    }

    [DataContract]
    [Serializable]    
    public class DealerPlanIds
    {
         [DataMember]     
        public int isPlanAvailable { get; set; }
         [DataMember]     
        public int Id { get; set; }
    }

    #region PostCode - RST
    [DataContract]
    [Serializable]
    [Table("tblPostCode")]
    public class PostCodeCityState
    {
        [Key]
        [DataMember]
        public string PostCode { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public bool Active { get; set; }
    }
    #endregion

    #region 13042015 - Anthony - Drop 5 : Waiver for non-Drop 4 Flow
    [DataContract]
    [Serializable]
    [Table("tblAutoWaiverRules")]
    public class AutoWaiverRules
    {
        [Key]
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string RiskCategory { get; set; }
        [DataMember]
        public bool IsLocalCustomer { get; set; }
        [DataMember]
        public bool HasDevice { get; set; }
        [DataMember]
        public bool HasDirectDebit { get; set; }
        [DataMember]
        public int PlanAdvance { get; set; }
        [DataMember]
        public int PlanDeposit { get; set; }
        [DataMember]
        public int DeviceAdvance { get; set; }
        [DataMember]
        public int DeviceDeposit { get; set; }
        [DataMember]
        public int PenaltyWaiver { get; set; }
    }
    #endregion

    //07052015 - Drop 5 - Justification for Supervisor - Lus
    [DataContract]
    [Serializable]
    [Table("trnRegJustification")]
    public class RegJustification
    {
		public const string WO = "Writeoff";
        public const string EW = "Manual Waiver";
        public const string DF = "Device Financing";
        public const string AF = "Accessory Financing";
        public const string TAC = "TAC Exceptional Approval";
        public const string BRE = "BRE Approval";

        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }
        [Required]
        [DataMember]
        public int RegId { get; set; }
        [Required]
        [DataMember]
        public string ApprType { get; set; }
        [DataMember]
        public string Justification { get; set; }
        [DataMember]
        public string ApprID { get; set; }
        [DataMember]
        public DateTime ApprDt { get; set; }
    }

	[DataContract]
	[Serializable]
	[Table("trnSurveyResponse")]
	public class RegSurveyResponse
	{
		public const string MAXIS_APP = "MyMaxis App";

		[DataMember]
		[Key]
		[Display(Name = "ID")]
		public int ID { get; set; }
		[Required]
		[DataMember]
		public int RegID { get; set; }
		[DataMember]
		public string SurveyType { get; set; }
		[DataMember]
		public string SurveyAnswer { get; set; }
		[DataMember]
        public string FeedBack { get; set; }
        [DataMember]
        public string SurveyRate { get; set; }
	}

    [DataContract]
    [Serializable]
    [Table("trnRegAccessory")]
    public class RegAccessory
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        public int RegID { get; set; }

        [DataMember]
        public int? SupplineID { get; set; }

        [DataMember]
        public string ArticleID { get; set; }

        [DataMember]
        public string EANCode { get; set; }

        [DataMember]
        public string UOMCode { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public DateTime CreatedDT { get; set; }

        [DataMember]
        public bool IsKnockOffRequired { get; set; }

		[DataMember]
		public bool IsFinancing { get; set; }

		[DataMember]
		public decimal RRP { get; set; }

		[DataMember]
		public decimal MonthlyPrice { get; set; }

		[DataMember]
		public decimal MonthlyRemain { get; set; }

		[DataMember]
		public int Tenure { get; set; }

		[DataMember]
		public decimal Discount_amount { get; set; }

        [Association("trnRegAccessoryDetails", "ID", "RegAccessoryID")]
        public virtual List<RegAccessoryDetails> RegAccessoryDetails { get; set; }

        [NotMapped]
        public string serialNumber { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("trnRegAccessoryDetails")]
    public class RegAccessoryDetails
    {
        [Key]
        [DataMember]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [DataMember]
        [Display(Name = "RegAccessoryID")]
        public int RegAccessoryID { get; set; }

        [DataMember]
        [Display(Name = "SequenceID")]
        public int SequenceID { get; set; }

        [DataMember]
        [Display(Name = "SerialNumber")]
        public string SerialNumber { get; set; }

        [DataMember]
        [Display(Name = "CreateDT")]
        public DateTime? CreateDT { get; set; }

        [DataMember]
        [Display(Name = "LastUpdateDT")]
        public DateTime? LastUpdateDT { get; set; }

        [DataMember]
        [Display(Name = "LastAccessID")]
        public string LastAccessID { get; set; }
        
        //[Association("trnRegAccessory", "RegAccessoryID", "")]
        //public virtual RegAccessory RegAccessory { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegJustificationReq
    {
        [Required]
        [DataMember]
        public int RegId { get; set; }
        [Required]
        [DataMember]
        public string ApprType { get; set; }
        [DataMember]
        public string Justification { get; set; }
        [DataMember]
        public string ApprID { get; set; }
        [DataMember]
        public DateTime ApprDt { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MnpRegDetailsVM
    {
        [DataMember]
        public string RatePlan { get; set; }
        [DataMember]
        public string Type { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("tblCustPopupMsg")]
    public class PopUpMessage
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public DateTime CreateDT { get; set; }
        [DataMember]
        public DateTime StartDT { get; set; }
        [DataMember]
        public DateTime EndDT { get; set; }
        [DataMember]
        [MaxLength(180)]
        public string Message { get; set; }
        [DataMember]
        public int RejectCount { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public DateTime LastUpdateDT { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("tblCustPopupMsgAuditLog")]
    public class PopUpMessageAuditLog
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Display(Name = "LoginID")]
        public string LoginID { get; set; }

        [DataMember]
        [Display(Name = "OrgID")]
        public int OrgID { get; set; }

        [DataMember]
        [Display(Name = "CreateDT")]
        public DateTime CreateDT { get; set; }

        [DataMember]
        [Display(Name = "SearchType")]
        public string SearchType { get; set; }

        [DataMember]
        [Display(Name = "SearchValue")]
        public string SearchValue { get; set; }

        [DataMember]
        [Display(Name = "IDCardtypeID")]
        public string IDCardTypeID { get; set; }

        [DataMember]
        [Display(Name = "IDCardNo")]
        public string IDCardNo { get; set; }

        [DataMember]
        [Display(Name = "WhitelistMsgDisplayed")]
        public bool WhitelistMsgDisplayed { get; set; }

        [DataMember]
        [Display(Name = "BusinessRuleMsgDisplayed")]
        public bool BusinessRuleMsgDisplayed { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("tblPegaRecommendation")]
    public class PegaRecommendation
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public string ProductName { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public int IDCardType { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public DateTime AcceptedDate { get; set; }
        [DataMember]
        public string CapturedById { get; set; }
        [DataMember]
        public DateTime LastUpdDate { get; set; }
        [DataMember]
        public string LastUpdId { get; set; }
        [DataMember]
        public string Response { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string KenanCode { get; set; }
        [DataMember]
        public string PrepositionId { get; set; }
		[DataMember]
		public string Application { get; set; }
		[DataMember]
		public string CaseCategory { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PegaRecommendationVM
    {
        [DataMember]
        public int ID { get; set; }
        public DateTime LastUpdDate { get; set; }
        [DataMember]
        public string LastUpdId { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string CurrentStatus { get; set; }
        [DataMember]
        public int ErrorStatus { get; set; }
        [DataMember]
        public string ErrorMsg { get; set; }
    }

    [DataContract]
    [Serializable]
    [NotMapped]
    public class ViewPopUpMessage : PopUpMessage
    {
        public string PlanName { get; set; }
    }

    [DataContract]
    [Serializable]
    [Table("tblTacAuditLog")]
    public class TacAuditLog
    {
        [DataMember]
        [Key]
        public int ID { get; set; }
        [DataMember]
        public string CustomerID { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public int RegTypeID { get; set; }
        [DataMember]
        public string Tac { get; set; }
        [DataMember]
        public DateTime TacGeneratedDT { get; set; }
        [DataMember]
        public string PerformedBy { get; set; }
    }

    #region ThirdPartyAuthType
    [DataContract]
    [Serializable]
    [Table("refThirdPartyAuthType")]
    public class ThirdPartyAuthType
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [DataMember]
        [Required]
        [StringLength(50)]
        [Display(Name = "Name *")]
        public string Name { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ThirdPartyAuthTypeFind : FindBase
    {
        [DataMember]
        public ThirdPartyAuthType ThirdPartyAuthType { get; set; }
        [DataMember]
        public bool? Active { get; set; }
    }

    #endregion

    [DataContract]
    [Serializable]
    [Table("trnRegBreFailTreatment")]
    public class RegBreFailTreatment
    {
        [DataMember]
        [Key]
        [Display(Name = "ID")]
        public int ID { get; set; }
        [Required]
        [DataMember]
        public int RegId { get; set; }
        [Required]
        [DataMember]
        public string BREType { get; set; }
        [Required]
        [DataMember]
        public string Treatment { get; set; }
    }
}
