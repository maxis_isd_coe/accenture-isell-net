﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Infrastructure;

namespace Online.Registration.DAL.Models
{
    public partial class OnlineStoreDbContext : DbContext
    {
        public DbSet<lnkRegWelcomeMail> lnkRegWelcomeMail { get; set; } //RST 06De2014
        #region Catalog

        public DbSet<lnkMNPInventoryDetails> lnkMNPInventoryDetails { get; set; }
        public DbSet<tbllnkContractDataPlan> ContractDataPlanIds { get; set; }
        public DbSet<PkgDataPlanId> DataPlanIds { get; set; }
        public DbSet<PkgData> PgmPkgData { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<Bundle> Bundles { get; set; }
        public DbSet<PackageType> PackageTypes { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<ComponentType> ComponentTypes { get; set; }
        public DbSet<Component> Components { get; set; }
        public DbSet<ComponentGroup> ComponentGroups { get; set; }
        public DbSet<ComponentRelation> ComponentRelations { get; set; }
        public DbSet<PgmBdlPckComponent> PgmBdlPckComponents { get; set; }
        public DbSet<ModelGroup> ModelGroups { get; set; }
        public DbSet<ModelGroupModel> ModelGroupModels { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<BrandSmart> BrandsSmart { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<ModelSmart> ModelsSmart { get; set; }
        public DbSet<ModelImage> ModelImages { get; set; }
        public DbSet<Colour> Colours { get; set; }
        public DbSet<ModelPartNo> ModelPartNos { get; set; }
        public DbSet<ItemPrice> ItemPrices { get; set; }
        public DbSet<AdvDepositPrice> AdvDepositPrices { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<PropertyValue> PropertyValues { get; set; }
        public DbSet<DeviceProperty> DeviceProperties { get; set; }
        public DbSet<PlanProperty> PlanProperties { get; set; }
        public DbSet<BrandArticle> ArticleIds { get; set; }
        public DbSet<BrandArticleSmart> ArticleIdsSmart { get; set; }
        public DbSet<UOMArticle> UOMArticles { get; set; }
        public DbSet<ModelOffer> ModelOffer { get; set; }
        public DbSet<DependentComponents> DependentComponents { get; set; }
        public DbSet<DependentComponentsNew> DependentComponentsNew { get; set; }
        public DbSet<lnkofferuomarticleprice> lnkofferuomarticleprice { get; set; }
        public DbSet<dependentContract> dependentContract { get; set; }
        public DbSet<IMPOSSpecDetails> IMPOSSpecDetails { get; set; }
        public DbSet<RegLob> RegLobs { get; set; }
        public DbSet<Campaign> Campaign { get; set; }
        public DbSet<Holidays> Holidays { get; set; }
        public DbSet<TempRegInstallers> TempRegInstallers { get; set; }
        public DbSet<BlockedInstallerGroups> BlockedInstallerGroups { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<InstallerbyGroup> InstallerbyGroups { get; set; }
        public DbSet<RegRemark> RegRemarks { get; set; }
        public DbSet<Refmobileregused> Refmobileregused { get; set; }
		public DbSet<PlanDeviceValidationRules> PlanDeviceValidationRules { get; set; }

        public DbSet<AddRemoveVASComponents> AddRemoveVASComponents { get; set; }

        public DbSet<RegistrationCancelReason> RegistrationCancelReason { get; set; }
        public DbSet<MISMMandatoryComponents> MISMMandatoryComponents { get; set; }
        #region SimModels Added by VLT on 09-Apr-2013
        public DbSet<SimModels> SimModels { get; set; }
        #endregion SimModels Added by VLT on 09-Apr-2013
        public DbSet<DeviceTypes> DeviceType { get; set; }
        public DbSet<DeviceCapacity> DeviceCapacity { get; set; }
        public DbSet<PriceRange> PriceRange { get; set; }

        #endregion

        #region Complain

        public DbSet<ComplaintIssues> ComplaintIssues { get; set; }
        public DbSet<Complain> Complains { get; set; }
        public DbSet<ComplaintIssuesGroup> ComplaintIssuesGroup { get; set; }
        public DbSet<IssueGrpDispatchQ> IssueGrpDispatchQ { get; set; }

        #endregion

        #region Config

        public DbSet<UserGrpAccess> UserGrpAccesses { get; set; }
        public DbSet<UserGrpRole> UserGrpRoles { get; set; }
        public DbSet<OrgType> OrgTypes { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Access> Accesses { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Market> Markets { get; set; }
        public DbSet<AccountCategory> AccountCategories { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<StatusType> StatusTypes { get; set; }
        public DbSet<StatusChange> StatusChanges { get; set; }
        public DbSet<StatusReason> StatusReasons { get; set; }
        public DbSet<StatusReasonCode> StatusReasonCodes { get; set; }

        #endregion

        #region Registration
        //Added for SimReplacementReasons Feature on june 17 2013
        public DbSet<SimReplacementReasons> SimReplacementReasonsDetails { get; set; }

        //New Registraion for Supple New Account Feature on june 15 2013
        public DbSet<LnkRegDetails> LnkRegistrationDetails { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<AddressType> AddressTypes { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<CardType> CardTypes { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<IDCardType> IDCardTypes { get; set; }
        public DbSet<PaymentMode> PaymentModes { get; set; }
        public DbSet<RegType> RegTypes { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<CustomerTitle> CustomerTitles { get; set; }
        public DbSet<Nationality> Nationalities { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<ThirdPartyAuthType> ThirdPartyAuthTypes { get; set; }
        public DbSet<VIPCode> VIPCodes { get; set; }
        public DbSet<ExtIDType> ExtIDTypes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<RegSuppLinesMap> RegSuppLineMaps { get; set; }
        //public DbSet<RegWaivedComponents> WaivedComponents { get; set; }
		public DbSet<RegAttributes> RegAttributes { get; set; }
        public DbSet<RegMdlGrpModel> RegMdlGrpModels { get; set; }
        public DbSet<RegPgmBdlPkgComp> RegPgmBdlPkgComps { get; set; }
        public DbSet<RegSuppLine> RegSuppLines { get; set; }
        public DbSet<RegStatus> RegStatuses { get; set; }
        public DbSet<Biometrics> Biometricses { get; set; }
        public DbSet<RegAccount> RegAccounts { get; set; }
        public DbSet<PaymentModePaymentDetails> PaymentModePaymentDetails { get; set; }
        public DbSet<CardTypePaymentDetails> CardTypePaymentDetails { get; set; }
        public DbSet<CustomerInfo> CustomersInfo { get; set; }
        public DbSet<KenamCustomerInfo> KenanCustomerInfos { get; set; }
        public DbSet<KenanaLogDetails> KenanaLogDetails { get; set; }
        public DbSet<RegUserScheduling> RegUserSchedulings { get; set; }
        public DbSet<DeviceInfo> DeviceInfos { get; set; }
        public DbSet<lnkPlanDevice> lnkPlanDevice { get; set; }
        public DbSet<InstallerCenter> InstallerCenters { get; set; }
        public DbSet<Center> Centers { get; set; }
        public DbSet<RetrieveAccountInfo> RetrieveAccountInfos { get; set; }

        // Added tables to store the secondary plan/data package
        public DbSet<RegPgmBdlPkgCompSec> RegPgmBdlPkgCompSecs { get; set; }
        public DbSet<RegistrationSec> RegistrationSecs { get; set; }
        public DbSet<RegMdlGrpModelSec> RegMdlGrpModelSecs { get; set; }

		public DbSet<RegUploadDoc> RegUploadDoc { get; set; }
        #endregion
        //Relocation
        public DbSet<RegistrationRequestType> RegistrationRequestTypes { get; set; }
        public DbSet<RegPreviousDetail> RegPreviousDetails { get; set; }
        //End of relocation

        #region VLT ADDED CODE
        public DbSet<tblAPICallStatus> ApiCallStatus { get; set; }
        #endregion VLT ADDED CODE

        #region VLT ADDED CODE for Spend Limit
        public DbSet<lnkRegSpendLimit> lnkRegSpendLimit { get; set; }
        #endregion VLT ADDED CODE

        #region VLT ADDED CODE for Extra Ten
        public DbSet<lnkExtraTenDetails> lnkExtraTenDetails { get; set; }
        public DbSet<LnkExtraTenConfirmationLog> LnkExtraTenConfirmationLog { get; set; }
        public DbSet<lnkExtraTenAddEditDetails> LnkExtraTenAddEditDetails { get; set; }
        #endregion VLT ADDED CODE

        #region VLT ADDED CODE by Rajeswari on Feb 25th
        public DbSet<UserTransactionLog> UserLog { get; set; }
        #endregion VLT ADDED CODE

        #region WhiteListCustomers
        /*  This code is writeen by VLTECH */
        public DbSet<WhiteListCustomers> WhiteList { get; set; }
        public DbSet<UseeWhiteListCampaignInfo> UseeWhiteList { get; set; }
        /*  This code is writeen by VLTECH */

        public DbSet<SMECIWhiteList> WhiteListCust { get; set; }

        #endregion

        #region CRP

        public DbSet<Contracts> Contracts { get; set; }
        public DbSet<PackageComponents> PackageComponents { get; set; }
        public DbSet<RegSmartComponents> RegSmartComponents { get; set; }
        public DbSet<ContractPlans> ContractPlans { get; set; }
        public DbSet<CMSSComplain> CMSSID { get; set; }

        #endregion

        #region ChangePackage - IsellHome
        public DbSet<FilterChangePckgs> FilterChangePckgs { get; set; }
        #endregion
        public DbSet<ContractBundleModels> BrandContracts { get; set; }
        public OnlineStoreDbContext()
        {
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.AutoDetectChangesEnabled = true;
        }

        #region MISM
        public DbSet<tblDataPlanComponents> DataPlanComponents { get; set; }
        #endregion

        public DbSet<WaiverComponents> WaiverComponents { get; set; }

        public DbSet<PrintVersion> PrintVersion { get; set; }

        /// <summary>
        /// Get User Specific Printers
        /// </summary>
        public DbSet<UserSpecificPrintersInfo> UserSpecificPrintersInfo { get; set; }

        public DbSet<UserPrintLog> UserPrintLogInfo { get; set; }

        #region smart related methods
        //public DbSet<PackageComponents> PackageComponents { get; set; }
        public DbSet<SmartRebates> SmartRebate { get; set; }
        //public DbSet<ContractBundleModels> BrandContracts { get; set; }
        //public DbSet<ContractPlans> ContractPlans { get; set; }
        public DbSet<PgmBdlPckComponentSmart> PgmBdlPckComponentsSmart { get; set; }
        public DbSet<ModelGroupModel_Smart> ModelGroupModels_Smart { get; set; }
        //public DbSet<CMSSID> CMSSID { get; set; }
        //public DbSet<Contracts> Contracts_Smart { get; set; }
        //public DbSet<RegSmartComponents> RegSmartComponents { get; set; }
        //public DbSet<RegistrationCancelReason> RegistrationCancelReason { get; set; }

        public DbSet<TransactionTypes> TransactionTypes { get; set; }
        public DbSet<RegistrationCancellationReasons> RegistrationCancellationReasons { get; set; }
        
        /*Changes by chetan related to PDPA implementation*/
        public DbSet<PDPAData> PDPAData { get; set; }

        public DbSet<WaiverRules> WaiverRules { get; set; }
        #endregion
        public DbSet<UserDMEPrint> UserDMEPrintInfo { get; set; }
        public DbSet<RegBREStatus> RegBREStatus { get; set; }

       
        

        #region "sim replacement secondary lines information"
        public DbSet<lnkSecondaryAcctDetailsSimRplc> lnkSecondaryAcctDetailsSimRplc { get; set; }
        #endregion

        public DbSet<RebateDataContractComponents> rebateDataContractComponents { get; set; }
        public DbSet<RegRebateDatacontractPenalty> lnkRebatePenaty { get; set; }


      

        public DbSet<lnkAcctMktCodePackages> AccountMarketPackages { get; set; }
		public DbSet<lnkAcctMktCodeOffers> AccountMarketOffers { get; set; }



        #region SimTypes
        public DbSet<SIMCardTypes> SIMCardTypes { get; set; }
        #endregion

        #region SimTypes
        public DbSet<Region> Region { get; set; }
        #endregion

        #region Bre Check Treatment -Sharvin-
        public DbSet<BreCheckTreatment> BreCheckTreatment { get; set; }
        public DbSet<BreCheckTreatment_Test> BreCheckTreatment_Test { get; set; } // w.loon - for testing purpose
        #endregion

        #region PostCode - RST
        public DbSet<PostCodeCityState> PostCodeCityState { get; set; }
        #endregion

        public DbSet<AutoWaiverRules> AutoWaiverRules { get; set; }//13042015 - Anthony - Drop 5 : Waiver for non-Drop 4 Flow

		public DbSet<SalesChannelIDs> SalesChannelIDs { get; set; }

        public DbSet<RegJustification> RegJustification { get; set; } //11052015 - Add for Write off supervisor approval CR- Lus

        public DbSet<RegBreFailTreatment> RegBreFailTreatment { get; set; } // 20160120 - w.loon - Write to TrnRegBreFailTreatment - for display in Supervisor Dashboard

		public DbSet<RegSurveyResponse> RegSurveyResponse { get; set; }

        public DbSet<refLovList> LOV { get; set; }

        public DbSet<PegaRecommendation> PegaRecommendation { get; set; }

		public DbSet<DeviceFinancingInfo> DeviceFinancingInfo { get; set; }

        #region 23072015 - Anthony - Reference Table [Accessory CR]
        public DbSet<Accessory> Accessory { get; set; }
        public DbSet<OfferAccessory> OfferAccessory { get; set; }
        public DbSet<OrgAccessory> OrgAccessory { get; set; }
        public DbSet<RegAccessory> RegAccessory { get; set; }
        #endregion

        public DbSet<RegAccessoryDetails> RegAccessoryDetails { get; set; }//Anthony - 17112015 - Reference table for serialize accessory [Accesory Financing GTM]

        public DbSet<PopUpMessage> PopUpMessage { get; set; }

        public DbSet<PopUpMessageAuditLog> PopUpMessageAuditLog { get; set; }

        public DbSet<TacAuditLog> tblTacAuditLog { get; set; }
    }
}
