﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Online.Registration.DAL.Models
{
    #region WhiteList

    [DataContract]
    [Serializable]
    [Table("tblSMECIWhiteList")]
    public class SMECIWhiteList
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required, StringLength(12)]
        public string ICNumber { get; set; }

        [DataMember]
        public int CorpId { get; set; }

        [DataMember]
        public string CorpName { get; set; }

        [DataMember]
        public string Grade { get; set; }

       

       
    }

   

    #endregion

    

}
