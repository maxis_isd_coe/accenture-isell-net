﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Online.Registration.DAL.Models
{
    #region Base

    [DataContract]
    [Serializable]
    public class BaseEntity
    {
        [DataMember]
        [Required]
        [Display(Name = "Created On *")]
        public DateTime CreateDT { get; set; }

        [DataMember]
        [Display(Name = "Last Updated")]
        //[ConcurrencyCheck]
        public DateTime? LastUpdateDT { get; set; }

        [DataMember]
        [Required]
        [MaxLength(20)]
        [Display(Name = "Last Access *")]
        public string LastAccessID { get; set; }
    }

    #endregion

    #region Active

    [DataContract]
    [Serializable]
    public class ActiveEntity : BaseEntity
    {
        [DataMember]
        [Required]
        [Display(Name = "Is Active? *")]
        public bool Active { get; set; }
    }

    #endregion

    #region Time

    [DataContract]
    [Serializable]
    public class TimedEntity : ActiveEntity
    {
        [DataMember]
        [Required]
        [DataType(DataType.Date)]
        [Column(TypeName = "date")]
        [Display(Name = "Start Date *")]
        public DateTime StartDate { get; set; }

        [DataMember]
        [DataType(DataType.Date)]
        [Column(TypeName = "date")]
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }
    }

    #endregion

    #region FindBase

    [DataContract]
    [Serializable]
    public abstract class FindBase
    {
        [DataMember]
        public int PageSize { get; set; }
    }

    #endregion

    #region ISellResponse

    [DataContract]
    [Serializable]
    public class statusToPos
    {
        [DataMember]
        public int msgCode { get; set; }

        [DataMember]
        public string msgDesc { get; set; }
    }

    #endregion
}
