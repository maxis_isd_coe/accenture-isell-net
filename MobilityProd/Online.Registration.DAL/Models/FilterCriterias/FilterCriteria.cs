﻿using Online.Registration.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Linq.Dynamic;

namespace Online.Registration.DAL.Models.FilterCriterias
{
    [KnownType(typeof(EndDateFilterCriteria))]
    [KnownType(typeof(StartDateFilterCriteria))]
    [KnownType(typeof(TodayFilterCriteria))]
    [DataContract]
    [Serializable]
    public class FilterCriteria:IFilterCriteria
    {
        [DataMember]
        private string _entityName;
         
        [DataMember]
        private object _value;
        public FilterCriteria(string entityName,object value)
        {
            _entityName = entityName;
            _value = value;
        }

        public object GetValue()
        {
            return _value;
        }

        public string GetEntityName()
        {
            return _entityName;
        }


        public virtual IQueryable<T> RunFilter<T>(IQueryable<T> query)
        {
            var entityName = GetEntityName();
            var value = GetValue();
            if (value is DateTime)
            {
                var date = (DateTime)value;
                return query.Where(string.Format("{0}.Year == @0.Year && {0}.Month == @0.Month && {0}.Day== @0.Day", entityName), date)
                    .AsQueryable();
            }
            else
                return query.Where(string.Format("{0} == @0", entityName), value).AsQueryable();
        }
    }
}
