﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Linq.Dynamic;

namespace Online.Registration.DAL.Models.FilterCriterias
{
    [DataContract]
    [Serializable]
    public class StartDateFilterCriteria:FilterCriteria
    {
        public StartDateFilterCriteria(string entityName,DateTime startDate)
            :base(entityName,startDate)
        {

        }

        public override IQueryable<T> RunFilter<T>(IQueryable<T> query)
        {
            var entityName = GetEntityName();
            DateTime value = (DateTime)GetValue();
            return query.Where(string.Format("{0} >= DateTime({1},{2},{3})", entityName, value.Year, value.Month, value.Day))
                .AsQueryable();
        }
    }
}
