﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Online.Registration.DAL.Models
{
    [DataContract]
    [Serializable]
    public class WebPosLog
    {
        [DataMember] 
        public string fileName { get; set; }
        [DataMember]
        public string method { get; set; }
        [DataMember] 
        public string request { get; set; }
        [DataMember] 
        public string response { get; set; }
        [DataMember] 
        public int userId { get; set; }
        [DataMember] 
        public string exception { get; set; }
        [DataMember]
        public string regId { get; set; }      
    }


    [DataContract]
    [Serializable]
    [Table("lnkRegWelcomeMail")]
    public class TriggerEmailLog
    {
        
        [DataMember]
        public int RegID { get; set; }
        
        [DataMember]
        public string MSISDN1 { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime CreateDT { get; set; }
    }



}
