﻿using Online.Registration.DAL.Models.ServiceRequest.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.Common
{
    [DataContract]
    [Serializable]
    [Table("tblVerificationDocs")]
    public class VerificationDoc:CommonBase
    {
        [DataMember]
        public int OrderId { get; set; }

        [DataMember]
        [Required]
        [StringLength(50)]
        public string FileName { get; set; }

        [DataMember]
        [Required]
        public string File { get; set; }

        [Required]
        public int DocTypeId {
            get{
                return (int)DocType;
            }
            set{
                DocType = (DocTypes)value;
            }
        }

        [DataMember]
        public virtual DocTypes DocType { get; set; }
    }
}
