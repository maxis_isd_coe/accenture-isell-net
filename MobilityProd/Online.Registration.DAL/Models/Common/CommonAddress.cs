﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Models.Common
{
    /// <summary>
    /// Address table solely store address information
    /// </summary>
    [DataContract]
    [Serializable]
    [Table("tblAddress")]
    public class CommonAddress:CommonBase
    {
        [DataMember]
        [Required]
        public Boolean Active { get; set; }

        [DataMember]
        [Display(Name = "Version No *")]
        public int VersionNo { get; set; }

        [DataMember]
        [Display(Name = "Address Type *")]
        public int AddressTypeID { get; set; } //1=Permanent, 2=Billing, 3=Installtion

        [Association("refAddressType", "AddressTypeID", "ID")]
        public virtual AddressType AddressType { get; set; }

        [Required]
        [DataMember]
        [StringLength(150)]
        [Display(Name = "Address *")]
        public string Line1 { get; set; }

        [DataMember]
        [StringLength(150)]
        [Display(Name = " ")]
        public string Line2 { get; set; }

        [DataMember]
        [StringLength(150)]
        [Display(Name = " ")]
        public string Line3 { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Unit No")]
        public string UnitNo { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Street/Area *")]
        public string Street { get; set; }

        [DataMember]
        [StringLength(50)]
        [Display(Name = "Building/House No *")]
        public string BuildingNo { get; set; }

        [Required]
        [DataMember]
        [StringLength(100)]
        [Display(Name = "Town/City Name *")]
        public string Town { get; set; }

        [Required]
        [DataMember]
        [StringLength(5, ErrorMessage = "The field Postcode * must be a number with a maximum length of 5.")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Postcode must be a number.")]
        [Display(Name = "Postcode *")]
        public string Postcode { get; set; }


        [DataMember]
        [Display(Name = "State/Province *")]
        public int StateID { get; set; }

        [Association("refCountryState", "StateID", "")]
        public virtual State State { get; set; }

        [DataMember]
        [Display(Name = "Country")]
        public int CountryID { get; set; }

        [Association("refCountry", "CountryID", "")]
        public virtual Country Country { get; set; }
    }
}
