﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Online.Registration.DAL.Models.Common
{
    /// <summary>
    /// Common abstract entity properties
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class CommonBase
    {
        [DataMember]
        [Key]
        public int ID { get; set; }

        [DataMember]
        [Required]
        public DateTime CreateDt { get; set; }

        [DataMember]
        [Required]
        [StringLength(20)]
        public String CreateBy { get; set; }

        [DataMember]
        public DateTime? UpdateDt { get; set; }

        [DataMember]
        [StringLength(20)]
        public String UpdateBy { get; set; }
    }
}