﻿using Online.Registration.DAL.Models.FilterCriterias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.DAL.Interfaces
{
    public interface IFilterCriteria
    {
        object GetValue();
        string GetEntityName();

        IQueryable<T> RunFilter<T>(IQueryable<T> query);
    }
}
