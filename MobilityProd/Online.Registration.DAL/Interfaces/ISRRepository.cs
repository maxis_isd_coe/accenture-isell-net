﻿using Online.Registration.DAL.Models.Common;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Online.Registration.DAL.Interfaces
{
    public interface ISRRepository
    {
        /// <summary>
        /// save new order to context
        /// </summary>
        /// <param name="oder">SROrderMaster object</param>
        /// <returns>order id : String</returns>
        string SaveOrder(SROrderMaster order);
        /// <summary>
        /// Delete an order by order id
        /// </summary>
        /// <param name="orderId">order id</param>
        /// <returns>num row delete : int</returns>
        int DeleteOrderById(string orderId);
        /// <summary>
        /// Find order master by id
        /// </summary>
        /// <param name="orderId">order id</param>
        /// <returns>Order object : SROrderMaster</returns>
        SROrderMaster FindOrderById(string orderId);
        /// <summary>
        /// Save verification doc
        /// </summary>
        /// <param name="orderId">order id with "SR" prefix</param>
        /// <param name="verificationDoc">Verificationdoc object to save</param>
        /// <returns></returns>
        Boolean SaveVerificationDoc(string orderId, List<VerificationDoc> verificationDocs);
        /// <summary>
        /// Change order lock information
        /// </summary>
        /// <param name="orderId">order id with "SR" prefix</param>
        /// <param name="lockBy">username who lock it</param>
        /// <returns>true or false for update success</returns>
        Boolean UpdateOrderLock(string orderId, string lockBy);
        /// <summary>
        /// Update order status
        /// </summary>
        /// <param name="sRUpdateStatusDto">order update status DTO object</param>
        /// <returns>success or fail for update process</returns>
        Boolean UpdateStatus(SRUpdateStatusDto dto);
        /// <summary>
        /// Get a unique create by name list
        /// </summary>
        /// <returns>List of string of create by username</returns>
        IEnumerable<String> GetDistinctCreateByList(int centerOrgId);
        /// <summary>
        /// Query record with filter conditon
        /// </summary>
        /// <param name="dto">dto for filter condition</param>
        /// <param name="pageSize">pageSize to return</param>
        /// <param name="totalCount">total record in server</param>
        /// <returns></returns>
        IEnumerable<SRQueryMaster> QueryOrder(SRQueryOrderDto dto, out int totalCount, int page = 0, int pageSize = 10);
    }
}
