﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Online.Registration.DAL.Helpers
{
    public static class DALObjectExtension
    {
        public static string Name<T, TProp>(this T o, Expression<Func<T, TProp>> propertySelector)
        {
            MemberExpression body = (MemberExpression)propertySelector.Body;
            return body.Member.Name;
        }

        public static string MethodName<T, TProp>(this T o, Expression<Func<T, TProp>> expression)
        {
            MethodCallExpression outermostExpression = expression.Body as MethodCallExpression;

            if (outermostExpression == null)
            {
                throw new ArgumentException("Invalid Expression. Expression should consist of a Method call only.");
            }

            return outermostExpression.Method.Name;
        }

        /// <summary>
        /// Use reflextion method to try update the property by name
        /// </summary>
        /// <param name="currentObj"></param>
        /// <param name="targetObj"></param>
        /// <param name="proName"></param>
        public static void TryUpdate(this object currentObj,object targetObj,string proName)
        {
            var pro = currentObj.GetType().GetProperty(proName, BindingFlags.Public | BindingFlags.Instance);
            if (pro != null)
                pro.SetValue(currentObj, targetObj.GetType().GetProperty(proName).GetValue(targetObj, null),null);
        }
    }
}
