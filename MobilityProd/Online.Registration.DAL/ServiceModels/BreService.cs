﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Online.Registration.DAL.ServiceModels.BreService
{
    /// <summary>
    /// Online.Registration.Web.breService.eaiHeader
    /// </summary>
    public class HeaderInfo
    {
        [DefaultValue(typeof(string),"")]
        public string From { get; set; }
        [DefaultValue(typeof(string), "")]
        public string To { get; set; }
        [DefaultValue(typeof(string), "")]
        public string AppId { get; set; }
        [DefaultValue(typeof(string), "")]
        public string MsgType { get; set; }
        [DefaultValue(typeof(string), "")]
        public string MsgId { get; set; }
        [DefaultValue(typeof(string), "")]
        public string CorrelationId { get; set; }
        [DefaultValue(typeof(string), "")]
        public string Timestamp { get; set; }
    }  
    /// <summary>
    /// BusinessRulesRequest mapped to Online.Registration.Web.breService.checkBusinessRulesRequestType
    /// </summary>
    public class BusinessRulesRequest
    {
        public HeaderInfo HeaderInfo { get; set; }
        [DefaultValue(typeof(string), "")]
        public string ChannelId;
        [DefaultValue(typeof(string), "")]
        public string[] RuleNameList;
        [DefaultValue(typeof(string), "")]
        public string IcNumber;
        [DefaultValue(typeof(string), "")]
        public string IcType;
        [DefaultValue(typeof(string), "")]
        public string DateOfBirth;
        [DefaultValue(typeof(string), "")]
        public string PostalCode;
        [DefaultValue(typeof(string), "")]
        public string State;
        [DefaultValue(typeof(string), "")]
        public string KenanExtAcctId;
        [DefaultValue(typeof(string), "")]
        public string TotalContractsAllowed;
        [DefaultValue(typeof(string), "")]
        public string TotalPrincipleLinesAllowed;
        [DefaultValue(typeof(string), "")]
        public string TotalLinesAllowed;
        [DefaultValue(typeof(string), "")]
        public string TotalMsisdnAllowedInContracts;
        [DefaultValue(typeof(string), "")]
        public string TotalContracts;
        //public BusinessRequestDataType[] businessDataList;
        public List<BusinessRequestDataType> businessDataList;
    }

    /// <summary>
    /// totalLineCheckRequest mapped to Online.Registration.Web.breService.totalLineCheckRequestType
    /// </summary>
    public class TotalLineCheckRequest
    {
        public HeaderInfo HeaderInfo { get; set; }
        [DefaultValue(typeof(string), "")]
        public string IcNumber;
        [DefaultValue(typeof(string), "")]
        public string IcType;
        [DefaultValue(typeof(string), "")]
        public string toFilterMism;
        [DefaultValue(typeof(string), "")]
        public string[] lob;
        [DefaultValue(typeof(string), "")]
        public string[] mktCode;
    }

    /// <summary>
    /// businessDataType mapped to Online.Registration.Web.breService.businessDataType
    /// </summary>
    public class BusinessRequestDataType
    {
        [DefaultValue(typeof(string), "")]
        public string dataName;
        [DefaultValue(typeof(string), "")]
        public string dataValue;
    }

    /// <summary>
    /// Maps to Online.Registration.Web.breService.checkBusinessRulesResponseType
    /// </summary>
    public class BusinessRulesResponseType
    {
        public HeaderInfo HeaderInfo { get; set; }
        [DefaultValue(typeof(string), "")]
        public string MsgCode;
        [DefaultValue(typeof(string), "")]
        public string MsgDesc;
        [DefaultValue(typeof(string), "")]
        public string FailureRuleName;
    }

    /// <summary>
    /// Maps to Online.Registration.Web.breService.totalLineCheckResponseType
    /// </summary>
    public class TotalLineCheckResponseType
    {
        public HeaderInfo HeaderInfo { get; set; }
        [DefaultValue(typeof(string), "")]
        public string MsgCode;
        [DefaultValue(typeof(string), "")]
        public string MsgDesc;
        [DefaultValue(typeof(string), "")]
        public int TotalNum;
    }
}