﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Online.Registration.DAL.ServiceModels.AccListPrinSupModels
{
    /// <summary>
    /// HeaderInfo Online.Registration.Web.PrinSuppService.eaiHeader
    /// </summary>
    public class HeaderInfo
    {
        [DefaultValue(typeof(string),"")]
        public string From { get; set; }
        [DefaultValue(typeof(string), "")]
        public string To { get; set; }
        [DefaultValue(typeof(string), "")]
        public string AppId { get; set; }
        [DefaultValue(typeof(string), "")]
        public string MsgType { get; set; }
        [DefaultValue(typeof(string), "")]
        public string MsgId { get; set; }
        [DefaultValue(typeof(string), "")]
        public string CorrelationId { get; set; }
        [DefaultValue(typeof(string), "")]
        public string Timestamp { get; set; }
    }
    /// <summary>
    /// Service response mapped to Online.Registration.Web.PrinSuppService.getPrinSuppResponse
    /// </summary>
    public class ServiceResponse
    {
        public HeaderInfo HeaderInfo { get; set; }
        public string MsgCode { get; set; }
        public string MsgDesc { get; set; }        
        public List<SuppLine> SuppLineList { get; set; }
    }
    /// <summary>
    /// SuppLine response mapped to Online.Registration.Web.PrinSuppService.lineList
    /// </summary>
    public class SuppLine
    {
        [DefaultValue(typeof(string), "")]
        public string Msisdn;
        [DefaultValue(typeof(string), "")]
        public string CustNm;
        [DefaultValue(typeof(string), "")]
        public string AcctNo;
        [DefaultValue(typeof(string), "")]
        public string SubscrNo;
        [DefaultValue(typeof(string), "")]
        public string SubscrNoResets;
        [DefaultValue(typeof(string), "")]
        public string SubscrStatus;
        [DefaultValue(typeof(string), "")]
        public string PrinSuppInd;
    }
}