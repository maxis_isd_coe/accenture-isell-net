﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ServiceModel;
using Online.Registration.DAL.Models;

namespace Online.Registration.DAL.ReportModels
{
       [DataContract]
    public class RegHistoryFind : FindBase
    {
        public RegHistoryFind()
        {
            RegistrationIDs = new List<int>();
            RegStatus = new RegStatus();
        }
        [DataMember]
        public RegStatus RegStatus { get; set; }
        [DataMember]
        public bool? Active { get; set; }
        [DataMember]
        public List<int> RegistrationIDs { get; set; }
    }

       [DataContract]
       public class RegHistoryResult
       {
           [DataMember]
           public int RegID { get; set; }

           [DataMember]
           public string RegStatus { get; set; }

           [DataMember]
           public int RegStatusID { get; set; }

           [DataMember]
           public string ReasonCode { get; set; }

           [DataMember]
           [Display(Name = "Status Remark")]
           [DataType(DataType.MultilineText)]
           [MaxLength(4000)]
           [Column(TypeName = "nText")]
           public string Remark { get; set; }

           [DataMember]
           public DateTime? StatusLogDT { get; set; }

           [DataMember]
           public DateTime CreateDT { get; set; }

           [DataMember]
           public string LastAccessID { get; set; }

           [DataMember]
           public string ModemID { get; set; }

           [DataMember]
           public string KenanAccountNo { get; set; }
       }

        
       //public class FailedTransHistoryDetails
       //{
       //    public List<RegHistoryResult> RegHistoryDetails = new List<RegHistoryResult>();
       //    public List<KenanaLogDetail> KenanLogDetails = new List<KenanaLogDetail>();
       //}


       //#region KenanXmlLogs
       //public class KenanaLogDetail
       //{
       //    [DataMember]
       //    public int ID { get; set; }
       //    [DataMember]
       //    public int RegID { get; set; }
       //    [DataMember]
       //    public string MessageCode { get; set; }
       //    [DataMember]
       //    public string MessageDesc { get; set; }
       //    [DataMember]
       //    public string MethodName { get; set; }
       //    [DataMember]
       //    public string KenanXmlReq { get; set; }
       //    [DataMember]
       //    public string KenanXmlRes { get; set; }
       //}
       //#endregion
}
