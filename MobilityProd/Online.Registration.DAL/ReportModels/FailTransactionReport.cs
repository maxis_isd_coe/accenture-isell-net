﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ServiceModel;
using Online.Registration.DAL.Models;


namespace Online.Registration.DAL.ReportModels
{
    [DataContract]
    public class FailTransactionsSearchCriteria : FindBase
    {
        [DataMember]
        [RegularExpression(@"^[0-9]{0,10}$", ErrorMessage = "Must be a number.")]
        [Display(Name = "Registration ID")]
        public string RegID { get; set; }

        [DataMember]
        [Display(Name = "ID Card No")]
        public string IDCardNo { get; set; }

        [DataMember]
        [Display(Name = "Queue No")]
        public string QueueNo { get; set; }

        [DataMember]
        [Display(Name = "Status")]
        public int StatusID { get; set; }

        [DataMember]
        [Display(Name = "Organization")]
        public int? CenterOrgID { get; set; }

        [DataMember]
        [Display(Name = "User ID")]
        public int UserID { get; set; }

        [DataMember]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [DataMember]
        [Display(Name = "Contact Number")]
        public string MSISDN { get; set; }

        [DataMember]
        [Display(Name = "Name")]
        public string CustName { get; set; }

        [DataMember]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [DataMember]
        [Display(Name = "Date")]
        public DateTime? RegDateFrom { get; set; }

        [DataMember]
        [Display(Name = "To")]
        public DateTime? RegDateTo { get; set; }

        [DataMember]
        public bool IsMobilePlan { get; set; }

        [DataMember]
        public bool RegTypeID { get; set; }

        [DataMember]
        public bool RegType { get; set; }
    }

    [DataContract]
    public class FailTransactionsSearchResult
    {
        [DataMember]
        public int RegID { get; set; }

        [DataMember]       
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTime RegDate { get; set; }

        [DataMember]
        public string QueueNo { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public string LastUpdateID { get; set; }
        [DataMember]
        public string LastAccessID { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string CenterOrg { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public int RegType { get; set; }
        [DataMember]
        public int RegTypeID { get; set; }
        [DataMember]
        public string RegTypeDescription { get; set; }

    }
}
