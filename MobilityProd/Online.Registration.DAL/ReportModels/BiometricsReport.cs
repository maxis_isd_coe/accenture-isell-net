﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Online.Registration.DAL.ReportModels
{
    #region Biometrics Report
    public class BiometricsReport
    {
        public int RegID { get; set; }
        public string Name { get; set; }
        public string NewIC { get; set; }
        public string OldIC { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string Nationality { get; set; }
        public string Race { get; set; }
        public string Religion { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string FingerMatch { get; set; }
        public string FingerThumb { get; set; }
        public DateTime CreateDT { get; set; }
        public DateTime? LastUpdateDT { get; set; }
        public string LastUpdatedBy { get; set; }
    }

    public class BiometricsReportCriteria
    {
        public string IDCardNo { get; set; }
        public string Name { get; set; }
    }
    #endregion
}
