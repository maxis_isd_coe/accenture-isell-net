﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Online.Registration.DAL.ReportModels
{
    public class AgingReport
    {
        public int RegID { get; set; }
        public int AgingNumber { get; set; }
        public DateTime RegDate { get; set; }
        public string CustName { get; set; }
        public string IDCardNo { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdatedBy { get; set; }
        public string CenterOrg { get; set; }
        public string CenterType { get; set; }
        public string Status { get; set; }
        public string MSISDN { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }
    }

    public class AgingReportCriteria
    {
        public int RegID { get; set; }
        public int StatusID { get; set; }
        public int CenterOrgID { get; set; }
        public int CenterTypeID { get; set; }
        public int RegTypeID { get; set; }
        public string MSISDN1 { get; set; }
        public string IDCardNo { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdatedBy { get; set; }
        public DateTime? RegDateFrom { get; set; }
        public DateTime? RegDateTo { get; set; }
    }
}
