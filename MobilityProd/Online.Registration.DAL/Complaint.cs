﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;

using Online.Registration.DAL.Models;

using log4net;

namespace Online.Registration.DAL
{
    public class Complaint
    {
        static string svcName = "Online.Registration.DAL";
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Complaint));

        #region Complain

        public static void ComplainCreate(Complain complain)
        {
            using (var context = new OnlineStoreDbContext())
            {
                context.Complains.Add(complain);
                context.SaveChanges();
            }
        }

        public static void ComplainUpdate(Complain latestComplain)
        {
            using (var context = new OnlineStoreDbContext())
            {
                var complain = context.Complains.Single(a => a.ID == latestComplain.ID);

                complain.AccountNo = latestComplain.AccountNo;
                complain.DOB = latestComplain.DOB;
                complain.CustomerName = latestComplain.CustomerName;
                complain.EmailAddr = latestComplain.EmailAddr;
                complain.Address = latestComplain.Address;
                complain.MSISDN = latestComplain.MSISDN;
                complain.Remark = latestComplain.Remark;
                complain.TelNo = latestComplain.TelNo;
                complain.IDNumber = latestComplain.IDNumber;
                complain.IDType = latestComplain.IDType;
                complain.LastAccessID = latestComplain.LastAccessID;
                complain.LastUpdateDT = latestComplain.LastUpdateDT;
                complain.CMSSCaseID = latestComplain.CMSSCaseID;
                complain.CMSSInteractionID = latestComplain.CMSSInteractionID;
                complain.TranxID = latestComplain.TranxID;

                context.SaveChanges();
            }
        }

        public static List<Complain> ComplainGet(List<int> ids)
        {
            var complain = new List<Complain>();

            using (var context = new OnlineStoreDbContext())
            {
                complain = context.Complains.Where(a => ids.Contains(a.ID)).ToList();
            }

            return complain;
        }

        public static List<int> ComplainFind(ComplainFind findCriteria)
        {
            if (findCriteria == null)
                throw new ArgumentNullException();

            var ids = new List<int>();

            using (var context = new OnlineStoreDbContext())
            {
                var query = context.Complains.AsQueryable();

                if (findCriteria.Complain != null)
                {
                    var complain = findCriteria.Complain;

                    if (false == string.IsNullOrEmpty(complain.AccountNo))
                        query = query.Where(a => a.AccountNo == complain.AccountNo);

                    if (false == string.IsNullOrEmpty(complain.CustomerName))
                        query = query.Where(a => a.CustomerName.ToLower().Contains(complain.CustomerName.Trim().ToLower()));

                    if (complain.DOB.HasValue)
                        query = query.Where(a => a.DOB.HasValue && EntityFunctions.TruncateTime(a.DOB) == EntityFunctions.TruncateTime(complain.DOB));

                    if (false == string.IsNullOrEmpty(complain.EmailAddr))
                        query = query.Where(a => a.EmailAddr.ToLower() == complain.EmailAddr.ToLower());

                    if (false == string.IsNullOrEmpty(complain.MSISDN))
                        query = query.Where(a => a.MSISDN == complain.MSISDN);

                    if (false == string.IsNullOrEmpty(complain.IDNumber))
                        query = query.Where(a => a.IDNumber == complain.IDNumber);

                    if (false == string.IsNullOrEmpty(complain.IDType))
                        query = query.Where(a => a.IDType == complain.IDType);

                    if (false == string.IsNullOrEmpty(complain.TelNo))
                        query = query.Where(a => a.TelNo == complain.TelNo);

                    if (false == string.IsNullOrEmpty(complain.Address))
                        query = query.Where(a => a.Address.ToLower().Contains(complain.Address.Trim().ToLower()));

                    if (false == string.IsNullOrEmpty(complain.Remark))
                        query = query.Where(a => a.Remark.ToLower().Contains(complain.Remark.Trim().ToLower()));

                    if (false == string.IsNullOrEmpty(complain.CMSSCaseID))
                        query = query.Where(a => a.CMSSCaseID.ToLower() == complain.CMSSCaseID.Trim().ToLower());

                    if (false == string.IsNullOrEmpty(complain.CMSSInteractionID))
                        query = query.Where(a => a.CMSSInteractionID.ToLower() == complain.CMSSInteractionID.Trim().ToLower());

                    if (complain.TranxID != 0 && complain.TranxID != null)
                        query = query.Where(a => a.TranxID == complain.TranxID);
                }

                if (findCriteria.CreateDateFrom.HasValue)
                    query = query.Where(a => EntityFunctions.TruncateTime(a.CreateDT) >= EntityFunctions.TruncateTime(findCriteria.CreateDateFrom));

                if (findCriteria.CreateDateTo.HasValue)
                    query = query.Where(a => EntityFunctions.TruncateTime(a.CreateDT) <= EntityFunctions.TruncateTime(findCriteria.CreateDateTo));

                var resultQuery = query.Select(a => a.ID).Distinct();

                if (findCriteria.PageSize > 0)
                    resultQuery = resultQuery.Take(findCriteria.PageSize);

                ids = resultQuery.ToList();
            }

            return ids;
        }

        #endregion

        #region Complaint Issues

        public static void ComplaintIssuesCreate(ComplaintIssues complaintIssues)
        {
            using (var context = new OnlineStoreDbContext())
            {
                context.ComplaintIssues.Add(complaintIssues);
                context.SaveChanges();
            }
        }

        public static void ComplaintIssuesUpdate(ComplaintIssues latestIssue)
        {
            using (var context = new OnlineStoreDbContext())
            {
                var issue = context.ComplaintIssues.Single(a => a.ID == latestIssue.ID);

                issue.Name = latestIssue.Name;
                issue.Type = latestIssue.Type;
                issue.Description = latestIssue.Description;
                issue.LastAccessID = latestIssue.LastAccessID;
                issue.LastUpdateDT = latestIssue.LastUpdateDT;
                issue.Active = latestIssue.Active;

                context.SaveChanges();
            }
        }

        public static List<ComplaintIssues> ComplaintIssuesGet(List<int> ids)
        {
            var issues = new List<ComplaintIssues>();

            using (var context = new OnlineStoreDbContext())
            {
                issues = context.ComplaintIssues.Where(a => ids.Contains(a.ID)).ToList();
            }

            return issues;
        }

        public static List<ComplaintIssues> ComplaintIssuesList()
        {
            var issues = new List<ComplaintIssues>();

            using (var context = new OnlineStoreDbContext())
            {
                issues = context.ComplaintIssues.Where(a => a.ID > 0).ToList();
            }

            return issues;
        }

        public static List<int> ComplaintIssuesFind(ComplaintIssuesFind findCriteria)
        {
            if (findCriteria == null)
                throw new ArgumentNullException();

            var ids = new List<int>();

            using (var context = new OnlineStoreDbContext())
            {
                var query = context.ComplaintIssues.AsQueryable();

                if (findCriteria.Active.HasValue)
                {
                    query = query.Where(a => a.Active == findCriteria.Active.Value);
                }

                if (findCriteria.CaseDispatchQIDs.Count() > 0)
                {
                    query = query.Where(a => findCriteria.CaseDispatchQIDs.Contains(a.ID));
                }

                if (findCriteria.ComplaintIssues != null)
                {
                    var complainIssue = findCriteria.ComplaintIssues;

                    if (false == string.IsNullOrEmpty(complainIssue.Name))
                        query = query.Where(a => a.Name.ToLower() == complainIssue.Name.ToLower());

                    if (false == string.IsNullOrEmpty(complainIssue.Description))
                        query = query.Where(a => a.Description.Contains(complainIssue.Description));

                    if (false == string.IsNullOrEmpty(complainIssue.Type))
                        query = query.Where(a => a.Type == complainIssue.Type);
                }

                var resultQuery = query.Select(a => a.ID).Distinct();

                if (findCriteria.PageSize > 0)
                    resultQuery = resultQuery.Take(findCriteria.PageSize);

                ids = resultQuery.ToList();
            }

            return ids;
        }

        public static List<ComplaintIssues> GetRequredXMLInputs(ComplaintIssuesGroupFind findCriteria)
        {
            var issues = new List<ComplaintIssues>();
            var issuesGroup = new List<ComplaintIssuesGroup>();
            List<int> ids = new List<int>();
            using (var context = new OnlineStoreDbContext())
            {
                issuesGroup = context.ComplaintIssuesGroup.Where(a => a.ProductID == findCriteria.ComplaintIssuesGroup.ProductID && a.QueueID == findCriteria.ComplaintIssuesGroup.QueueID && a.Reason1ID == findCriteria.ComplaintIssuesGroup.Reason1ID && a.Reason2ID == findCriteria.ComplaintIssuesGroup.Reason2ID && a.Reason3ID == findCriteria.ComplaintIssuesGroup.Reason3ID).ToList();
                ids.AddRange(issuesGroup.Select(a => a.SystemName));
                ids.AddRange(issuesGroup.Select(a => a.Title));
                ids.AddRange(issuesGroup.Select(a => a.InteractionType));
                ids.AddRange(issuesGroup.Select(a => a.Direction));
                ids.AddRange(issuesGroup.Select(a => a.CasePriority));
                ids.AddRange(issuesGroup.Select(a => a.CaseComplexity));
                issues = context.ComplaintIssues.Where(a => ids.Contains(a.ID)).ToList();
            }

            return issues;
        }

        #endregion

        #region Complaint Issues Group

        public static void ComplaintIssuesGroupCreate(ComplaintIssuesGroup complaintIssuesGroup)
        {
            using (var context = new OnlineStoreDbContext())
            {
                context.ComplaintIssuesGroup.Add(complaintIssuesGroup);
                context.SaveChanges();
            }
        }

        public static void ComplaintIssuesGroupUpdate(ComplaintIssuesGroup complaintIssuesGroup)
        {
            using (var context = new OnlineStoreDbContext())
            {
                var complaintIssuesGrp = context.ComplaintIssuesGroup.Single(a => a.ID == complaintIssuesGroup.ID);

                complaintIssuesGrp.ProductID = complaintIssuesGroup.ProductID;
                complaintIssuesGrp.Reason1ID = complaintIssuesGroup.Reason1ID;
                complaintIssuesGrp.Reason2ID = complaintIssuesGroup.Reason2ID;
                complaintIssuesGrp.Reason3ID = complaintIssuesGroup.Reason3ID;
                complaintIssuesGrp.LastAccessID = complaintIssuesGroup.LastAccessID;
                complaintIssuesGrp.LastUpdateDT = complaintIssuesGroup.LastUpdateDT;
                complaintIssuesGrp.Active = complaintIssuesGroup.Active;

                context.SaveChanges();
            }
        }

        public static List<ComplaintIssuesGroup> ComplaintIssuesGroupGet(List<int> ids)
        {
            var complaintIssuesGroup = new List<ComplaintIssuesGroup>();

            using (var context = new OnlineStoreDbContext())
            {
                complaintIssuesGroup = context.ComplaintIssuesGroup.Where(a => ids.Contains(a.ID)).ToList();
            }

            return complaintIssuesGroup;
        }

        public static List<int> ComplaintIssuesGroupFind(ComplaintIssuesGroupFind findCriteria)
        {
            if (findCriteria == null)
                throw new ArgumentNullException();

            var ids = new List<int>();

            using (var context = new OnlineStoreDbContext())
            {
                var query = context.ComplaintIssuesGroup.AsQueryable();

                if (findCriteria.Active.HasValue)
                {
                    query = query.Where(a => a.Active == findCriteria.Active.Value);
                }

                if (findCriteria.ComplaintIssuesGroup != null)
                {
                    var complainIssue = findCriteria.ComplaintIssuesGroup;

                    if (complainIssue.ProductID != 0)
                        query = query.Where(a => a.ProductID == complainIssue.ProductID);

                    if (complainIssue.Reason1ID != 0)
                        query = query.Where(a => a.Reason1ID == complainIssue.Reason1ID);

                    if (complainIssue.Reason2ID != 0)
                        query = query.Where(a => a.Reason2ID == complainIssue.Reason2ID);

                    if (complainIssue.Reason3ID != 0)
                        query = query.Where(a => a.Reason3ID == complainIssue.Reason3ID);

                    if (complainIssue.QueueID != 0)
                        query = query.Where(a => a.QueueID == complainIssue.QueueID);
                }

                var resultQuery = query.Select(a => a.ID).Distinct();

                if (findCriteria.PageSize > 0)
                    resultQuery = resultQuery.Take(findCriteria.PageSize);

                ids = resultQuery.ToList();
            }

            return ids;
        }

        #endregion

        #region Issue Group Dispatch Queue

        public static void IssueGrpDispatchQCreate(IssueGrpDispatchQ issueGrpDispatchQ)
        {
            using (var context = new OnlineStoreDbContext())
            {
                context.IssueGrpDispatchQ.Add(issueGrpDispatchQ);
                context.SaveChanges();
            }
        }

        public static void IssueGrpDispatchQUpdate(IssueGrpDispatchQ issueGrpDispatchQ)
        {
            using (var context = new OnlineStoreDbContext())
            {
                var issueGrpDispatchQueue = context.IssueGrpDispatchQ.Single(a => a.ID == issueGrpDispatchQ.ID);

                issueGrpDispatchQueue.IssueGrpID = issueGrpDispatchQ.IssueGrpID;
                issueGrpDispatchQueue.CaseDispatchQID = issueGrpDispatchQ.CaseDispatchQID;
                issueGrpDispatchQueue.TranxID = issueGrpDispatchQ.TranxID;
                issueGrpDispatchQueue.LastAccessID = issueGrpDispatchQ.LastAccessID;
                issueGrpDispatchQueue.LastUpdateDT = issueGrpDispatchQ.LastUpdateDT;
                issueGrpDispatchQueue.Active = issueGrpDispatchQ.Active;

                context.SaveChanges();
            }
        }

        public static List<IssueGrpDispatchQ> IssueGrpDispatchQGet(List<int> ids)
        {
            var issueGrpDispatchQ = new List<IssueGrpDispatchQ>();

            using (var context = new OnlineStoreDbContext())
            {
                issueGrpDispatchQ = context.IssueGrpDispatchQ.Where(a => ids.Contains(a.ID)).ToList();
            }

            return issueGrpDispatchQ;
        }

        public static List<int> IssueGrpDispatchQFind(IssueGrpDispatchQFind findCriteria)
        {
            if (findCriteria == null)
                throw new ArgumentNullException();

            var ids = new List<int>();

            using (var context = new OnlineStoreDbContext())
            {
                var query = context.IssueGrpDispatchQ.AsQueryable();

                if (findCriteria.Active.HasValue)
                {
                    query = query.Where(a => a.Active == findCriteria.Active.Value);
                }

                if (findCriteria.ComplaintIssuesGrpIDs.Count() > 0)
                {
                    query = query.Where(a => findCriteria.ComplaintIssuesGrpIDs.Contains(a.IssueGrpID));
                }

                if (findCriteria.IssueGrpDispatchQ != null)
                {
                    var issueGrpDispatchQ = findCriteria.IssueGrpDispatchQ;

                    if (issueGrpDispatchQ.CaseDispatchQID != 0)
                        query = query.Where(a => a.CaseDispatchQID == issueGrpDispatchQ.CaseDispatchQID);

                    if (issueGrpDispatchQ.IssueGrpID != 0)
                        query = query.Where(a => a.IssueGrpID == issueGrpDispatchQ.IssueGrpID);

                    if (issueGrpDispatchQ.TranxID != 0)
                        query = query.Where(a => a.TranxID == issueGrpDispatchQ.TranxID);
                }

                var resultQuery = query.Select(a => a.ID).Distinct();

                if (findCriteria.PageSize > 0)
                    resultQuery = resultQuery.Take(findCriteria.PageSize);

                ids = resultQuery.ToList();
            }

            return ids;
        }

        public static List<CMSSComplainGet> GetCmssList(CMSSComplainGet objComplaint)
        {
            Logger.InfoFormat("Entering {0}.GetCmssList({1})", svcName, objComplaint);
            List<CMSSComplainGet> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<CMSSComplainGet>("USP_GetCmssList @CMSSCaseID,@MSISDN", new System.Data.SqlClient.SqlParameter("@CMSSCaseID", objComplaint.CMSSCaseID), new System.Data.SqlClient.SqlParameter("@MSISDN", objComplaint.MSISDN)).ToList();

            }
            Logger.InfoFormat("Exiting {0}.GetCmssList({1})", svcName, objComplaint);
            return value;
        }

        #endregion

    }
}
