﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SNT.Utility;
using log4net;
using System.Data;
using System.Data.Objects;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Properties;
using Online.Registration.DAL.ReportModels;
using System.Transactions;

namespace Online.Registration.DAL
{
    public class Reports
    {
        static string svcName = "Online.Registration.DAL";
        private static readonly ILog Logger = LogManager.GetLogger(svcName);

        #region AgingReport

        public static List<AgingReport> AgingReportGet(AgingReportCriteria criteria)
        {
            Logger.InfoFormat("Entering {0}.AgingReportGet({1})", svcName, (criteria != null) ? criteria.RegID : 0);
            List<AgingReport> result = new List<AgingReport>();
            //int defaultTopNo = criteria.PageSize > 0 ? criteria.PageSize : 0;

            using (var context = new OnlineStoreDbContext())
            {
                if (criteria != null)
                {
                    var statusNew = context.Statuses.Where(a => a.Code == Properties.Settings.Default.Status_HomeRegNew).Select(a => a.ID).SingleOrDefault();
                    var instAddr = context.AddressTypes.Where(a => a.Code == Properties.Settings.Default.AddressType_Installation).Select(a => a.ID).SingleOrDefault();

                    var query = from registration in context.Registrations

                                join cust in context.Customers on
                                registration.CustomerID equals cust.ID //into custs
                                //from cust in custs.DefaultIfEmpty()

                                join regStatus in context.RegStatuses on
                                new { regID = registration.ID, active = true } equals new { regID = regStatus.RegID, active = regStatus.Active } //into regStatuses
                                //from regStatus in regStatuses.DefaultIfEmpty()

                                join status in context.Statuses on
                                regStatus.StatusID equals status.ID //into statuses
                                //from status in statuses.DefaultIfEmpty()

                                join organization in context.Organizations on
                                registration.CenterOrgID equals organization.ID

                                join orgType in context.OrgTypes on
                                organization.OrgTypeID equals orgType.ID

                                join addr in context.Addresses on
                                new { regID = registration.ID, addrType = instAddr } equals new { regID = addr.RegID, addrType = addr.AddressTypeID }

                                select new
                                {
                                    Reg = registration,
                                    Cust = cust,
                                    Address = addr,
                                    Status = status,
                                    RegStatus = regStatus,
                                    Organization = organization,
                                    OrgType = orgType.Name
                                };

                    if (criteria.RegID != 0)
                    {
                        query = query.Where(a => a.Reg.ID == criteria.RegID);
                    }

                    if (!string.IsNullOrEmpty(criteria.IDCardNo))
                        query = query.Where(a => a.Cust.IDCardNo == criteria.IDCardNo);

                    //if (!string.IsNullOrEmpty(criteria.QueueNo))
                    //    query = query.Where(a => a.Reg.QueueNo == criteria.QueueNo);

                    if (criteria.StatusID != 0)
                        query = query.Where(a => a.Status.ID == criteria.StatusID);

                    if (criteria.CenterOrgID != 0)
                        query = query.Where(a => a.Reg.CenterOrgID == criteria.CenterOrgID);

                    if (!string.IsNullOrEmpty(criteria.MSISDN1))
                        query = query.Where(a => a.Reg.MSISDN1.Contains(criteria.MSISDN1));

                    if (criteria.RegDateFrom.HasValue)
                    {
                        var regDTFrom = criteria.RegDateFrom.Value.Date;
                        query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) >= EntityFunctions.TruncateTime(regDTFrom));
                    }

                    if (criteria.RegDateTo.HasValue)
                    {
                        var regDTTo = criteria.RegDateTo.Value.Date;
                        query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) <= EntityFunctions.TruncateTime(regDTTo));
                    }

                    if (criteria.RegTypeID != 0)
                        query = query.Where(a => a.Reg.RegTypeID == criteria.RegTypeID);

                    //if (defaultTopNo > 0)
                    //    query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);

                    var temp = query.Select(a => new
                    {
                        RegID = a.Reg.ID,
                        RegDate = a.Reg.CreateDT,
                        QueueNo = a.Reg.QueueNo,
                        IDCardNo = a.Cust.IDCardNo,
                        CustName = a.Cust.FullName,
                        Status = a.Status.Description,
                        StatusID = a.Status.ID,
                        MSISDN = a.Cust.ContactNo,
                        CenterOrg = a.Organization.Name,
                        OrgType = a.OrgType,
                        Line1 = a.Address.Line1,
                        Line2 = a.Address.Line2,
                        Town = a.Address.Town,
                        Postcode = a.Address.Postcode,
                        LastAccessID = a.Reg.LastAccessID != null ? a.Reg.LastAccessID : string.Empty,
                        LastUpdatedBy = a.RegStatus.LastAccessID != null ? a.RegStatus.LastAccessID : string.Empty,
                        AgingNumber = (a.Status.ID == statusNew) ? (DateTime.Now.Day - a.Reg.CreateDT.Day) : 0
                        
                    }).Distinct();

                    result = temp.Select(a => new AgingReport()
                    {
                        RegID = a.RegID,
                        RegDate = a.RegDate,
                        IDCardNo = a.IDCardNo,
                        CustName = a.CustName,
                        Status = a.Status,
                        MSISDN = a.MSISDN,
                        CenterOrg = a.CenterOrg,
                        CenterType = a.OrgType,
                        Line1 = a.Line1,
                        Line2 = a.Line2,
                        Town = a.Town,
                        Postcode = a.Postcode,
                        LastUpdatedBy = a.LastUpdatedBy,
                        CreatedBy = a.LastAccessID,
                        AgingNumber = a.AgingNumber
                    }).ToList();
                }
            }

            Logger.InfoFormat("Exiting {0}.AgingReportGet({1}): {2}", svcName, (criteria != null) ? criteria.RegID : 0, result.Count());
            return result;
        }

        #endregion

        #region Biometrics Report

        public static List<BiometricsReport> BiometricsReportGet(BiometricsReportCriteria criteria)
        {
            Logger.InfoFormat("Entering {0}.BiometricsReportGet({1})", svcName, (criteria != null) ? criteria.IDCardNo : string.Empty);
            List<BiometricsReport> result = new List<BiometricsReport>();

            using (var context = new OnlineStoreDbContext())
            {
                if (criteria != null)
                {
                    var query = from biometrics in context.Biometricses

                                join registration in context.Registrations on
                                biometrics.RegID equals registration.ID

                                select new
                                {
                                    Biometrics = biometrics,
                                    Reg = registration
                                };

                    if (!string.IsNullOrEmpty(criteria.IDCardNo))
                        query = query.Where(a => a.Biometrics.NewIC == criteria.IDCardNo);

                    if (!string.IsNullOrEmpty(criteria.Name))
                        query = query.Where(a => a.Biometrics.Name == criteria.Name);

                    var temp = query.Select(a => new
                    {
                        RegID = a.Biometrics.RegID != null ? a.Biometrics.RegID : 0,
                        Name = a.Biometrics.Name,
                        NewIC = a.Biometrics.NewIC,
                        OldIC = a.Biometrics.OldIC,
                        Gender = a.Biometrics.Gender,
                        DOB = a.Biometrics.DOB,
                        Nationality = a.Biometrics.Nationality,
                        Race = a.Biometrics.Race,
                        Religion = a.Biometrics.Religion,
                        Address1 = a.Biometrics.Address1,
                        Address2 = a.Biometrics.Address2,
                        Address3 = a.Biometrics.Address3,
                        Postcode = a.Biometrics.Postcode,
                        City = a.Biometrics.City,
                        State = a.Biometrics.State,
                        FingerMatch = a.Biometrics.FingerMatch,
                        FingerThumb = a.Biometrics.FingerThumb,
                        CreateDT = a.Biometrics.CreateDT,
                        LastUpdateDT = a.Biometrics.LastUpdateDT,
                        LastAccessID = a.Biometrics.LastAccessID
                    }).Distinct();

                    result = temp.Select(a => new BiometricsReport()
                    {
                        RegID = a.RegID.Value,
                        Name = a.Name,
                        NewIC = a.NewIC,
                        OldIC = a.OldIC,
                        Gender = a.Gender,
                        DOB = a.DOB,
                        Nationality = a.Nationality,
                        Race = a.Race,
                        Religion = a.Religion,
                        Address1 = a.Address1,
                        Address2 = a.Address2,
                        Address3 = a.Address3,
                        Postcode = a.Postcode,
                        City = a.City,
                        State = a.State,
                        FingerMatch = a.FingerMatch,
                        FingerThumb = a.FingerThumb,
                        CreateDT = a.CreateDT,
                        LastUpdateDT = a.LastUpdateDT.Value,
                        LastUpdatedBy = a.LastAccessID
                    }).ToList();
                }

                if (result == null)
                    return new List<BiometricsReport>();

                Logger.InfoFormat("Exiting {0}.BiometricsReportGet({1}): {2}", svcName, (criteria != null) ? criteria.IDCardNo : string.Empty, result.Count());
                return result;
            }
        }

        #endregion

        #region [Failed Transaction Report]
        public static List<FailTransactionsSearchResult> FailedTransactionGet(FailTransactionsSearchCriteria criteria)
        {
            Logger.InfoFormat("Entering {0}.FailTransactionsSearch({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            List<FailTransactionsSearchResult> result = new List<FailTransactionsSearchResult>();
            int defaultTopNo = criteria.PageSize > 0 ? criteria.PageSize : 0;

            var regTypeCodes = new List<string>();
            if (criteria.IsMobilePlan)
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_DevicePlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_PlanOnly);
                //added by Deepika
                regTypeCodes.Add(Properties.Settings.Default.RegType_SupplementaryPlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_NewLine);
            }
            else
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomePersonal);
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomeCorporate);
            }

            using (var context = new OnlineStoreDbContext())
            {
                var userOrgID = context.Users.Single(a => a.ID == criteria.UserID).OrgID;
                var userOrgTypeID = context.Organizations.Single(a => a.ID == userOrgID).OrgTypeID;
                var userOrgTypeCode = context.OrgTypes.Single(a => a.ID == userOrgTypeID).Code;

                var orgTypeViewCertain = Properties.Settings.Default.ViewCertainRoles.Split(',').ToList();

                if (criteria != null)
                {
                    var query = from registration in context.Registrations

                                join cust in context.Customers on
                                registration.CustomerID equals cust.ID //into custs
                                //from cust in custs.DefaultIfEmpty()

                                join regStatus in context.RegStatuses on
                                new { regID = registration.ID, active = true } equals new { regID = regStatus.RegID, active = regStatus.Active } //into regStatuses
                                //from regStatus in regStatuses.DefaultIfEmpty()

                                join status in context.Statuses on
                                regStatus.StatusID equals status.ID //into statuses
                                //from status in statuses.DefaultIfEmpty()

                                join organization in context.Organizations on
                                registration.CenterOrgID equals organization.ID

                                join regType in
                                    (from rt in context.RegTypes
                                     where regTypeCodes.Contains(rt.Code)
                                     select new { ID = rt.ID }) on registration.RegTypeID equals regType.ID

                                join regTypes in context.RegTypes on
                                registration.RegTypeID equals regTypes.ID
                                //join regType in (from regTypes in context.RegTypes where regTypeCodes.Contains(regTypes.Code)) on
                                //new { regTypeID = registration.RegTypeID,  } equals new { regTypeID = regType.ID }
                                where (regStatus.StatusID == 28 && registration.Trn_Type == "R")
                                select new
                                {
                                    Reg = registration,
                                    Cust = cust,
                                    Status = status,
                                    RegStatus = regStatus,
                                    Organization = organization,
                                    RegTypes = regTypes

                                };


                    if (orgTypeViewCertain.Contains(userOrgTypeCode))
                    {
                        query = query.Where(a => a.Reg.CenterOrgID == userOrgID);
                    }

                    if (!string.IsNullOrEmpty(criteria.RegID))
                    {
                        var regID = criteria.RegID.ToInt();
                        query = query.Where(a => a.Reg.ID == regID);
                    }

                    if (!string.IsNullOrEmpty(criteria.IDCardNo))
                        query = query.Where(a => a.Cust.IDCardNo == criteria.IDCardNo);

                    if (!string.IsNullOrEmpty(criteria.QueueNo))
                        query = query.Where(a => a.Reg.QueueNo == criteria.QueueNo);

                    if (criteria.StatusID != 0)
                        query = query.Where(a => a.Status.ID == criteria.StatusID);

                    if (criteria.CenterOrgID.HasValue)
                        query = query.Where(a => a.Reg.CenterOrgID == criteria.CenterOrgID);

                    if (!string.IsNullOrEmpty(criteria.MSISDN))
                        query = query.Where(a => a.Cust.ContactNo == criteria.MSISDN);

                    if (!string.IsNullOrEmpty(criteria.CustName))
                        query = query.Where(a => a.Cust.FullName.Contains(criteria.CustName));

                    if (criteria.RegDateFrom.HasValue)
                    {
                        var regDTFrom = criteria.RegDateFrom.Value.Date;
                        //query = query.Where(a => DateTime.Compare(a.CreateDT, regDTFrom) > 0);
                        query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) <= EntityFunctions.TruncateTime(regDTFrom));
                    }

                    //if (defaultTopNo > 0)
                    //    query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);

                    var temp = query.Select(a => new
                    {
                        RegID = a.Reg.ID,
                        RegDate = a.Reg.CreateDT,
                        QueueNo = a.Reg.QueueNo,
                        IDCardNo = a.Cust.IDCardNo,
                        CustName = a.Cust.FullName,
                        Status = a.Status.Description,
                        MSISDN = a.Cust.ContactNo,
                        CenterOrg = a.Organization.Name,
                        LastAccessID = a.Reg.LastAccessID != null ? a.Reg.LastAccessID : string.Empty,
                        LastUpdateID = a.RegStatus.LastAccessID != null ? a.RegStatus.LastAccessID : string.Empty,
                        regType = a.Reg.RegTypeID,
                        regTypes = a.RegTypes.Description

                    }).Distinct();

                    result = temp.Select(a => new FailTransactionsSearchResult()
                    {
                        RegID = a.RegID,
                        RegDate = a.RegDate,
                        QueueNo = a.QueueNo,
                        IDCardNo = a.IDCardNo,
                        CustomerName = a.CustName,
                        Status = a.Status,
                        MSISDN = a.MSISDN,
                        CenterOrg = a.CenterOrg,
                        LastAccessID = a.LastAccessID,
                        LastUpdateID = a.LastUpdateID,
                        RegType = a.regType,
                        RegTypeDescription = a.regTypes

                    }).ToList();
                }
            }

            Logger.InfoFormat("Exiting {0}.FailTransactionsSearch({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
            return result;
        }
        #endregion

        #region Registration History
        public static List<RegHistoryResult> RegHistoryGet(int regID)
        {
            Logger.InfoFormat("Entering {0}.RegStatusSearch({1})", svcName, (regID != null) ? regID : 0);
            List<RegHistoryResult> result = new List<RegHistoryResult>();
            //int defaultTopNo = criteria.PageSize > 0 ? criteria.PageSize : 0;

            using (var context = new OnlineStoreDbContext())
            {
                if (regID != null || regID != 0)
                {
                    var query = from regStatus in context.RegStatuses

                                join status in context.Statuses on
                                regStatus.StatusID equals status.ID //into custs
                                //from cust in custs.DefaultIfEmpty()

                                select new
                                {
                                    RegStatus = regStatus,
                                    Status = status
                                };

                    if (regID != 0)
                    {
                        //var regID = criteria.RegID.ToInt();
                        query = query.Where(a => a.RegStatus.RegID == regID);
                    }

                    //if (defaultTopNo > 0)
                    //query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);

                    var temp = query.Select(a => new
                    {
                        RegStatusID = a.RegStatus.ID,
                        RegID = a.RegStatus.RegID,
                        RegStatusDate = a.RegStatus.CreateDT,
                        Remark = a.RegStatus.Remark,
                        Status = a.Status.Description,
                        LastAccessID = a.RegStatus.LastAccessID != null ? a.RegStatus.LastAccessID : string.Empty,
                    });

                    result = temp.Select(a => new RegHistoryResult()
                    {
                        RegStatusID = a.RegStatusID,
                        RegID = a.RegID,
                        RegStatus = a.Status,
                        CreateDT = a.RegStatusDate,
                        Remark = a.Remark,
                        LastAccessID = a.LastAccessID
                    }).ToList();
                }
            }

            Logger.InfoFormat("Exiting {0}.RegStatusSearch({1}): {2}", svcName, (regID != null) ? regID : 0, result.Count());
            return result;
        }
        #endregion
    }
}
