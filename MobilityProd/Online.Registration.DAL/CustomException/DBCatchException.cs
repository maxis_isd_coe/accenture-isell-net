﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;

namespace Online.Registration.DAL.CustomeException
{
    public class DBCatchException : Exception
    {
        private Exception ex;

        public DBCatchException(Exception ex):base(GetMsg(ex))
        {
        }

        private static string GetMsg(Exception ex)
        {
            if (ex is DbEntityValidationException)
            {
                return GetValidatinError((DbEntityValidationException)ex);
            }
            else if (ex is DbUpdateException)
            {
                return ex.InnerException.InnerException.Message;
            }
            return String.Format(String.Format("Db Error",ex.Message));
        }

        private static string GetValidatinError(DbEntityValidationException ex)
        {
            var errorMessages = ex.EntityValidationErrors
                .SelectMany(x => x.ValidationErrors)
                .Select(x => x.ErrorMessage);
            var fullErrorMessage = string.Join(";" + Environment.NewLine, errorMessages);
            var exceptionMessage = string.Concat(ex.Message, " Db exception found: ", fullErrorMessage);
            return exceptionMessage;
        }
    }
}
