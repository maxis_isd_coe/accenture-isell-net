﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SNT.Utility;
using Online.Registration.DAL.Models;
using System.Data.SqlClient;

namespace Online.Registration.DAL.Admin
{
    public static partial class OnlineRegAdmin
    { 

        #region Address Type

        public static int AddressTypeCreate(AddressType oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressTypeCreate({1})", svcName, (oReq != null) ? oReq.Description : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.AddressTypes.Add(oReq);
                ctx.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.AddressTypeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Description : "NULL", value);
            return value;
        }
        public static int AddressTypeUpdate(AddressType oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressTypeUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var addrType = ctx.AddressTypes.Find(oReq.ID);
                    addrType.Code = oReq.Code;
                    addrType.Description = oReq.Description;
                    addrType.Active = oReq.Active;
                    addrType.LastAccessID = oReq.LastAccessID;
                    addrType.LastUpdateDT = DateTime.Now;
                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.AddressTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static List<int> AddressTypeFind(AddressTypeFind oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressTypeFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.AddressTypes.Where(a => a.ID != 0);

                if (!(oReq.AddressType.ID == 0))
                    query = query.Where(a => a.ID == oReq.AddressType.ID);

                if (!string.IsNullOrEmpty(oReq.AddressType.Code))
                    query = query.Where(a => a.Code == oReq.AddressType.Code);

                if (!string.IsNullOrEmpty(oReq.AddressType.Description))
                    query = query.Where(a => a.Description.Contains(oReq.AddressType.Description));

                if (!string.IsNullOrEmpty(oReq.AddressType.LastAccessID))
                    query = query.Where(a => a.LastAccessID.Contains(oReq.AddressType.LastAccessID));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.AddressTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<AddressType> AddressTypeGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.AddressTypeGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<AddressType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.AddressTypes.Where(a => IDs.Contains(a.ID)).ToList();
            }

            //Logger.InfoFormat("Exiting {0}.AddressTypeGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<AddressType> AddressTypeList()
        {
            List<AddressType> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.AddressTypes.Where(a => a.ID > 0).ToList();
            }
            return value;
        }
        public static List<int> AddressTypeFindByCode(string Code)
        {
            Logger.InfoFormat("Entering {0}.AddressTypeFindByCode({1})", svcName, !string.IsNullOrEmpty(Code) ? Code : "NULL");
            List<int> value = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                if (!string.IsNullOrEmpty(Code))
                {
                    var query = ctx.AddressTypes.Where(a => a.ID != 0);
                    query = ctx.AddressTypes.Where(a => a.Code == Code);
                    value = query != null && query.Count() != 0 ? query.Select(a => a.ID).ToList() : value;
                }
            }
            Logger.InfoFormat("Exiting {0}.AddressTypeFindByCode({1}): {2}", svcName, !string.IsNullOrEmpty(Code) ? Code : "NULL", value.Count());
            return value;
        }

        #endregion

        #region Bank

        public static int BankCreate(Bank oReq)
        {
            Logger.InfoFormat("Entering {0}.CardBankCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Banks.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardBankCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int BankUpdate(Bank oReq)
        {
            Logger.InfoFormat("Entering {0}.CardBankUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Bank bank = ctx.Banks.First(a => a.ID == oReq.ID);
                bank.Name = oReq.Name;
                bank.Code = oReq.Code;
                bank.Description = oReq.Description;
                bank.Active = oReq.Active;
                bank.LastAccessID = oReq.LastAccessID;
                bank.LastUpdateDT = oReq.LastUpdateDT;
                //ctx.Entry<Bank>(oReq).State = EntityState.Modified;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardBankUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> BankFind(BankFind oReq)
        {
            //Logger.InfoFormat("Entering {0}.CardBankFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Banks.Where(a => a.ID != 0);

                if (!(oReq.Bank.ID == 0))
                    query = query.Where(a => a.ID == oReq.Bank.ID);

                if (!string.IsNullOrEmpty(oReq.Bank.Name))
                    query = query.Where(a => a.Name.Contains(oReq.Bank.Name));

                if (!string.IsNullOrEmpty(oReq.Bank.Code))
                    query = query.Where(a => a.Code.Contains(oReq.Bank.Code));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            // Logger.InfoFormat("Exiting {0}.CardBankFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<Bank> BankGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.CardBankGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<Bank> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Banks.Where(a => IDs.Contains(a.ID)).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.CardBankGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        #endregion

        #region Card Type

        public static int CardTypeCreate(CardType oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypeCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.CardTypes.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.CardTypeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int CardTypeUpdate(CardType oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypeUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry<CardType>(oReq).State = EntityState.Modified;
                CardType cardType = ctx.CardTypes.First(a => a.ID == oReq.ID);
                cardType.Code = oReq.Code;
                cardType.Active = oReq.Active;
                cardType.Description = oReq.Description;
                cardType.Name = oReq.Name;
                cardType.LastUpdateDT = oReq.LastUpdateDT;
                cardType.LastAccessID = oReq.LastAccessID;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> CardTypeFind(CardTypeFind oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypeFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.CardTypes.Where(a => a.ID != 0);

                if (!(oReq.CardType.ID == 0))
                    query = query.Where(a => a.ID == oReq.CardType.ID);

                if (!string.IsNullOrEmpty(oReq.CardType.Name))
                    query = query.Where(a => a.Name.Contains(oReq.CardType.Name));

                if (!string.IsNullOrEmpty(oReq.CardType.Code))
                    query = query.Where(a => a.Code.Contains(oReq.CardType.Code));

                if (!string.IsNullOrEmpty(oReq.CardType.Description))
                    query = query.Where(a => a.Description.Contains(oReq.CardType.Description));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<CardType> CardTypeGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.CardTypeGet({1})", svcName, (IDs.Count() > 0) ? IDs[0] : 0);
            List<CardType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.CardTypes.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardTypeGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<CardType> CardTypeList()
        {
            Logger.InfoFormat("Entering {0}.CardTypeList({1})", svcName, "");
            List<CardType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.CardTypes.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardTypeList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region Country

        public static int CountryCreate(Country oReq)
        {
            Logger.InfoFormat("Entering {0}.CountryCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Countries.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CountryCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int CountryUpdate(Country oReq)
        {
            Logger.InfoFormat("Entering {0}.CountryUpdate({1})", svcName, (oReq != null) ? oReq.Name.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry<Country>(oReq).State = EntityState.Modified;
                var country = ctx.Countries.FirstOrDefault(a => a.ID == oReq.ID);
                country.Name = oReq.Name;
                country.Code = oReq.Code;
                country.Active = oReq.Active;
                country.LastAccessID = oReq.LastAccessID;
                country.LastUpdateDT = oReq.LastUpdateDT;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.CountryUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name.ToString() : "NULL", value);
            return value;
        }
        public static List<int> CountryFind(CountryFind oReq)
        {
            Logger.InfoFormat("Entering {0}.CountryFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //var query = ctx.Countries.Where(a => a.ID != 0);
                var query = from a in ctx.Countries where a.ID != 0 select a;

                if (!(oReq.Country.ID == 0))
                    query = query.Where(a => a.ID == oReq.Country.ID);

                if (!string.IsNullOrEmpty(oReq.Country.Code))
                    query = query.Where(a => a.Code == oReq.Country.Code);

                if (!string.IsNullOrEmpty(oReq.Country.Name))
                    query = query.Where(a => a.Name.Contains(oReq.Country.Name));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CountryFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<Country> CountryGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.CountryGet({1})", svcName, (IDs == null || IDs.Count() == 0) ? 0 : IDs[0]);
            List<Country> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Countries.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CountryGet({1}): {2}", svcName, (IDs == null || IDs.Count() == 0) ? 0 : IDs[0], value.Count());
            return value;
        }
        public static List<Country> CountryList()
        {
            Logger.InfoFormat("Entering {0}.CountryList({1})", svcName, "");
            List<Country> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Countries.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CountryList({1}): {2}", svcName, "", value.Count());
            return value;
        }
        #endregion

        #region Customer Title

        public static int CustomerTitleCreate(CustomerTitle oReq)
        {
            Logger.InfoFormat("Entering {0}.CardCustomerTitleCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.CustomerTitles.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardCustomerTitleCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int CustomerTitleUpdate(CustomerTitle oReq)
        {
            Logger.InfoFormat("Entering {0}.CardCustomerTitleUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                CustomerTitle CustomerTitle = ctx.CustomerTitles.First(a => a.ID == oReq.ID);
                CustomerTitle.Name = oReq.Name;
                CustomerTitle.Code = oReq.Code;
                CustomerTitle.ForGender = oReq.ForGender;
                CustomerTitle.Active = oReq.Active;
                CustomerTitle.LastAccessID = oReq.LastAccessID;
                CustomerTitle.LastUpdateDT = oReq.LastUpdateDT;
                //ctx.Entry<CustomerTitle>(oReq).State = EntityState.Modified;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardCustomerTitleUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> CustomerTitleFind(CustomerTitleFind oReq)
        {
            Logger.InfoFormat("Entering {0}.CardCustomerTitleFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.CustomerTitles.Where(a => a.ID != 0);

                if (!(oReq.CustomerTitle.ID == 0))
                    query = query.Where(a => a.ID == oReq.CustomerTitle.ID);

                if (!string.IsNullOrEmpty(oReq.CustomerTitle.Name))
                    query = query.Where(a => a.Name.Contains(oReq.CustomerTitle.Name));

                if (!string.IsNullOrEmpty(oReq.CustomerTitle.Code))
                    query = query.Where(a => a.Code.Contains(oReq.CustomerTitle.Code));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardCustomerTitleFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<CustomerTitle> CustomerTitleGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.CardCustomerTitleGet({1})", svcName, (IDs.Count() > 0) ? IDs[0] : 0);
            List<CustomerTitle> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.CustomerTitles.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardCustomerTitleGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<CustomerTitle> CustomerTitleList()
        {
            Logger.InfoFormat("Entering {0}.CustomerTitleList({1})", svcName, "");
            List<CustomerTitle> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.CustomerTitles.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CustomerTitleList({1}): {2}", svcName, "", value.Count());
            return value;
        }
        #endregion

        #region State

        public static int StateCreate(State oReq)
        {
            Logger.InfoFormat("Entering {0}.StateCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.States.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.StateCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }

        public static int StateUpdate(State oReq)
        {
            Logger.InfoFormat("Entering {0}.StateUpdate({1})", svcName, (oReq != null) ? oReq.Name.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var state = ctx.States.First(a => a.ID.Equals(oReq.ID));
                state.Name = oReq.Name;
                state.Code = oReq.Code;
                state.Active = oReq.Active;
                state.CountryID = oReq.CountryID;
                state.LastUpdateDT = oReq.LastUpdateDT;
                state.LastAccessID = oReq.LastAccessID;
                state.KenanStateID = oReq.KenanStateID;
                state.VOIPDefaultSearch = oReq.VOIPDefaultSearch;

                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.StateUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name.ToString() : "NULL", value);
            return value;
        }

        public static List<int> StateFind(StateFind oReq)
        {
            Logger.InfoFormat("Entering {0}.StateFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.States.Where(a => a.ID != 0);

                if (!(oReq.CountryState.ID == 0))
                    query = query.Where(a => a.ID == oReq.CountryState.ID);

                if (!(oReq.CountryState.CountryID == 0))
                    query = query.Where(a => a.CountryID == oReq.CountryState.CountryID);

                if (!string.IsNullOrEmpty(oReq.CountryState.Code))
                    query = query.Where(a => a.Code == oReq.CountryState.Code);

                if (!string.IsNullOrEmpty(oReq.CountryState.Name))
                    query = query.Where(a => a.Name.Contains(oReq.CountryState.Name));

                if (!string.IsNullOrEmpty(oReq.CountryState.KenanStateID))
                    query = query.Where(a => a.KenanStateID.Trim() == oReq.CountryState.KenanStateID.Trim());

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StateFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }

        public static List<State> StateGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.StateGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<State> states = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                states = ctx.States.Where(a => IDs.Contains(a.ID)).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.StateGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, states.Count());
            return states;
        }

        public static List<State> StateList()
        {
            List<State> states = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                states = ctx.States.Where(a => a.ID > 0).ToList();
            }
            return states;
        }

        #endregion

        #region ID Card Type

        public static int IDCardTypeCreate(IDCardType oReq)
        {
            Logger.InfoFormat("Entering {0}.IDCardTypeCreate({1})", svcName, (oReq != null) ? oReq.Code : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.IDCardTypes.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.IDCardTypeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Code : "NULL", value);
            return value;
        }
        public static int IDCardTypeUpdate(IDCardType oReq)
        {
            Logger.InfoFormat("Entering {0}.IDCardTypeUpdate({1})", svcName, (oReq != null) ? oReq.Code : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry<IDCardType>(oReq).State = EntityState.Modified;
                IDCardType idCardType = ctx.IDCardTypes.First(a => a.ID == oReq.ID);
                idCardType.Active = oReq.Active;
                idCardType.Code = oReq.Code;
                idCardType.KenanCode = oReq.KenanCode;
                idCardType.Description = oReq.Description;
                idCardType.LastAccessID = oReq.LastAccessID;
                idCardType.LastUpdateDT = oReq.LastUpdateDT;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.IDCardTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Code : "NULL", value);
            return value;
        }
        public static List<int> IDCardTypeFind(IDCardTypeFind oReq)
        {
            Logger.InfoFormat("Entering {0}.IDCardTypeFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.IDCardTypes.Where(a => a.ID != 0);

                if (!(oReq.IDCardType.ID == 0))
                    query = query.Where(a => a.ID == oReq.IDCardType.ID);

                if (!string.IsNullOrEmpty(oReq.IDCardType.Code))
                    query = query.Where(a => a.Code == oReq.IDCardType.Code);

                if (!string.IsNullOrEmpty(oReq.IDCardType.KenanCode))
                    query = query.Where(a => a.KenanCode == oReq.IDCardType.KenanCode);

                if (!string.IsNullOrEmpty(oReq.IDCardType.Description))
                    query = query.Where(a => a.Description.Contains(oReq.IDCardType.Description));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.IDCardTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<IDCardType> IDCardTypeGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.IDCardTypeGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
            List<IDCardType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.IDCardTypes.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.IDCardTypeGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
            return value;
        }
        public static List<IDCardType> IDCardTypeList()
        {
            Logger.InfoFormat("Entering {0}.IDCardTypeList({1})", svcName, "");
            List<IDCardType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.IDCardTypes.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.IDCardTypeList({1}): {2}", svcName, "", value.Count());
            return value;
        }
        #endregion

        #region Payment Mode

        public static int PaymentModeCreate(PaymentMode oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModeCreate({1})", svcName, (oReq != null) ? oReq.Code : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.PaymentModes.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.PaymentModeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Code : "NULL", value);
            return value;
        }
        public static int PaymentModeUpdate(PaymentMode oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModeUpdate({1})", svcName, (oReq != null) ? oReq.ID.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry<PaymentMode>(oReq).State = EntityState.Modified;
                PaymentMode paymentMode = ctx.PaymentModes.First(a => a.ID.Equals(oReq.ID));
                paymentMode.Active = oReq.Active;
                paymentMode.KenanCode = oReq.KenanCode;
                paymentMode.Code = oReq.Code;
                paymentMode.Name = oReq.Name;
                paymentMode.Description = oReq.Description;
                paymentMode.LastAccessID = oReq.LastAccessID;
                paymentMode.LastUpdateDT = oReq.LastUpdateDT;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.PaymentModeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString() : "NULL", value);
            return value;
        }
        public static List<int> PaymentModeFind(PaymentModeFind oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModeFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.PaymentModes.Where(a => a.ID != 0);

                if (!(oReq.PaymentMode.ID == 0))
                    query = query.Where(a => a.ID == oReq.PaymentMode.ID);

                if (!string.IsNullOrEmpty(oReq.PaymentMode.Code))
                    query = query.Where(a => a.Code == oReq.PaymentMode.Code);

                if (!string.IsNullOrEmpty(oReq.PaymentMode.Name))
                    query = query.Where(a => a.Name.Contains(oReq.PaymentMode.Name));

                if (!string.IsNullOrEmpty(oReq.PaymentMode.Description))
                    query = query.Where(a => a.Description.Contains(oReq.PaymentMode.Description));

                if (!string.IsNullOrEmpty(oReq.PaymentMode.KenanCode))
                    query = query.Where(a => a.KenanCode == oReq.PaymentMode.KenanCode);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PaymentModeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<PaymentMode> PaymentModeGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.PaymentModeGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<PaymentMode> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PaymentModes.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PaymentModeGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }
        public static List<PaymentMode> PaymentModeList()
        {
            Logger.InfoFormat("Entering {0}.PaymentModeList({1})", svcName, "");
            List<PaymentMode> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PaymentModes.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PaymentModeList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region Reg Type

        public static int RegTypeCreate(RegType oReq)
        {
            Logger.InfoFormat("Entering {0}.RegTypeCreate({1})", svcName, (oReq != null) ? oReq.Code : "NULL");
            var value = 0;

            using (OnlineStoreDbContext context = new OnlineStoreDbContext())
            {
                context.RegTypes.Add(oReq);
                context.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.RegTypeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Code : "NULL", value);
            return value;
        }
        public static int RegTypeUpdate(RegType oReq)
        {
            Logger.InfoFormat("Entering {0}.RegTypeUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext context = new OnlineStoreDbContext())
                {
                    RegType regType = context.RegTypes.First(a => a.ID == oReq.ID);
                    regType.Active = oReq.Active;
                    regType.Code = oReq.Code;
                    regType.Description = oReq.Description;
                    regType.LastAccessID = oReq.LastAccessID;
                    regType.LastUpdateDT = oReq.LastUpdateDT;
                    regType.Message = oReq.Message;
                    value = context.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.RegTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static List<int> RegTypeFind(RegTypeFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegTypeFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = new List<int>();

            using (OnlineStoreDbContext context = new OnlineStoreDbContext())
            {
                var query = context.RegTypes.Where(a => a.ID != 0);

                if (!(oReq.RegType.ID == 0))
                    query = query.Where(a => a.ID == oReq.RegType.ID);

                if (!string.IsNullOrEmpty(oReq.RegType.Code))
                    query = query.Where(a => a.Code == oReq.RegType.Code);

                if (!string.IsNullOrEmpty(oReq.RegType.Description))
                    query = query.Where(a => a.Description.Contains(oReq.RegType.Description));

                if (!string.IsNullOrEmpty(oReq.RegType.Message))
                    query = query.Where(a => a.Message.Contains(oReq.RegType.Message));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<RegType> RegTypeGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.RegTypeGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<RegType> value = null;

            using (var context = new OnlineStoreDbContext())
            {
                value = context.RegTypes.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegTypeGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }

        public static List<RegType> RegTypeList()
        {
            Logger.InfoFormat("Entering {0}.RegTypeList({1})", svcName, "");
            List<RegType> value = null;
            using (var context = new OnlineStoreDbContext())
            {
                value = context.RegTypes.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.RegTypeList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region Race

        public static int RaceCreate(Race oReq)
        {
            Logger.InfoFormat("Entering {0}.CardRaceCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Races.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardRaceCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int RaceUpdate(Race oReq)
        {
            Logger.InfoFormat("Entering {0}.CardRaceUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Race Race = ctx.Races.First(a => a.ID == oReq.ID);
                Race.Name = oReq.Name;
                Race.Code = oReq.Code;
                Race.Active = oReq.Active;
                Race.LastAccessID = oReq.LastAccessID;
                Race.LastUpdateDT = oReq.LastUpdateDT;
                //ctx.Entry<Race>(oReq).State = EntityState.Modified;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardRaceUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> RaceFind(RaceFind oReq)
        {
            Logger.InfoFormat("Entering {0}.CardRaceFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Races.Where(a => a.ID != 0);

                if (!(oReq.Race.ID == 0))
                    query = query.Where(a => a.ID == oReq.Race.ID);

                if (!string.IsNullOrEmpty(oReq.Race.Name))
                    query = query.Where(a => a.Name.Contains(oReq.Race.Name));

                if (!string.IsNullOrEmpty(oReq.Race.Code))
                    query = query.Where(a => a.Code.Contains(oReq.Race.Code));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardRaceFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<Race> RaceGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.CardRaceGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<Race> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Races.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardRaceGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }

        public static List<Race> RaceList()
        {
            Logger.InfoFormat("Entering {0}.RaceList({1})", svcName, "");
            List<Race> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Races.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.RaceList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region Language

        public static int LanguageCreate(Language oReq)
        {
            Logger.InfoFormat("Entering {0}.CardLanguageCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Languages.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardLanguageCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int LanguageUpdate(Language oReq)
        {
            Logger.InfoFormat("Entering {0}.CardLanguageUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Language Language = ctx.Languages.First(a => a.ID == oReq.ID);
                Language.Name = oReq.Name;
                Language.Code = oReq.Code;
                Language.Active = oReq.Active;
                Language.LastAccessID = oReq.LastAccessID;
                Language.LastUpdateDT = oReq.LastUpdateDT;
                //ctx.Entry<Language>(oReq).State = EntityState.Modified;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardLanguageUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> LanguageFind(LanguageFind oReq)
        {
            Logger.InfoFormat("Entering {0}.CardLanguageFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Languages.Where(a => a.ID != 0);

                if (!(oReq.Language.ID == 0))
                    query = query.Where(a => a.ID == oReq.Language.ID);

                if (!string.IsNullOrEmpty(oReq.Language.Name))
                    query = query.Where(a => a.Name.Contains(oReq.Language.Name));

                if (!string.IsNullOrEmpty(oReq.Language.Code))
                    query = query.Where(a => a.Code.Contains(oReq.Language.Code));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardLanguageFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<Language> LanguageGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.CardLanguageGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
            List<Language> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Languages.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardLanguageGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
            return value;
        }
        public static List<Language> LanguageGetList()
        {
            Logger.InfoFormat("Entering {0}.LanguageGetList({1})", svcName, "");
            List<Language> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Languages.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.LanguageGetList({1}): {2}", svcName, "", value.Count());
            return value;
        }
        #endregion

        #region Nationality

        public static int NationalityCreate(Nationality oReq)
        {
            Logger.InfoFormat("Entering {0}.CardNationalityCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Nationalities.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardNationalityCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int NationalityUpdate(Nationality oReq)
        {
            Logger.InfoFormat("Entering {0}.CardNationalityUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Nationality Nationality = ctx.Nationalities.First(a => a.ID == oReq.ID);
                Nationality.Name = oReq.Name;
                Nationality.Code = oReq.Code;
                Nationality.KenanCode = oReq.KenanCode;
                Nationality.Active = oReq.Active;
                Nationality.LastAccessID = oReq.LastAccessID;
                Nationality.LastUpdateDT = oReq.LastUpdateDT;
                //ctx.Entry<Nationality>(oReq).State = EntityState.Modified;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardNationalityUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> NationalityFind(NationalityFind oReq)
        {
            Logger.InfoFormat("Entering {0}.CardNationalityFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Nationalities.Where(a => a.ID != 0);

                if (!(oReq.Nationality.ID == 0))
                    query = query.Where(a => a.ID == oReq.Nationality.ID);

                if (!string.IsNullOrEmpty(oReq.Nationality.Name))
                    query = query.Where(a => a.Name.Contains(oReq.Nationality.Name));

                if (!string.IsNullOrEmpty(oReq.Nationality.Code))
                    query = query.Where(a => a.Code.Contains(oReq.Nationality.Code));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardNationalityFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<Nationality> NationalityGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.CardNationalityGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
            List<Nationality> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Nationalities.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardNationalityGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
            return value;
        }

        public static List<Nationality> NationalityList()
        {
            Logger.InfoFormat("Entering {0}.NationalityList({1})", svcName, "");
            List<Nationality> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Nationalities.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.NationalityList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region VIPCode

        public static int VIPCodeCreate(VIPCode oReq)
        {
            Logger.InfoFormat("Entering {0}.CardVIPCodeCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.VIPCodes.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardVIPCodeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int VIPCodeUpdate(VIPCode oReq)
        {
            Logger.InfoFormat("Entering {0}.CardVIPCodeUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                VIPCode VIPCode = ctx.VIPCodes.First(a => a.ID == oReq.ID);
                VIPCode.Name = oReq.Name;
                VIPCode.Code = oReq.Code;
                VIPCode.Description = oReq.Description;
                VIPCode.Active = oReq.Active;
                VIPCode.LastAccessID = oReq.LastAccessID;
                VIPCode.LastUpdateDT = oReq.LastUpdateDT;
                //ctx.Entry<VIPCode>(oReq).State = EntityState.Modified;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardVIPCodeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> VIPCodeFind(VIPCodeFind oReq)
        {
            //Logger.InfoFormat("Entering {0}.CardVIPCodeFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.VIPCodes.Where(a => a.ID != 0);

                if (!(oReq.VIPCode.ID == 0))
                    query = query.Where(a => a.ID == oReq.VIPCode.ID);

                if (!string.IsNullOrEmpty(oReq.VIPCode.Name))
                    query = query.Where(a => a.Name.Contains(oReq.VIPCode.Name));

                if (!string.IsNullOrEmpty(oReq.VIPCode.Code))
                    query = query.Where(a => a.Code.Contains(oReq.VIPCode.Code));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            // Logger.InfoFormat("Exiting {0}.CardVIPCodeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<VIPCode> VIPCodeGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.CardVIPCodeGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<VIPCode> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.VIPCodes.Where(a => IDs.Contains(a.ID)).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.CardVIPCodeGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<VIPCode> VIPCodeList()
        {
            List<VIPCode> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.VIPCodes.Where(a => a.ID > 0).ToList();
            }
            return value;
        }
        #endregion

        #region ExternalID Type

        public static int ExtIDTypeCreate(ExtIDType oReq)
        {
            Logger.InfoFormat("Entering {0}.CardExtIDTypeCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.ExtIDTypes.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardExtIDTypeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int ExtIDTypeUpdate(ExtIDType oReq)
        {
            Logger.InfoFormat("Entering {0}.CardExtIDTypeUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ExtIDType ExtIDType = ctx.ExtIDTypes.First(a => a.ID == oReq.ID);
                ExtIDType.Name = oReq.Name;
                ExtIDType.Code = oReq.Code;
                ExtIDType.Active = oReq.Active;
                ExtIDType.LastAccessID = oReq.LastAccessID;
                ExtIDType.LastUpdateDT = oReq.LastUpdateDT;
                //ctx.Entry<ExtIDType>(oReq).State = EntityState.Modified;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardExtIDTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> ExtIDTypeFind(ExtIDTypeFind oReq)
        {
            //Logger.InfoFormat("Entering {0}.CardExtIDTypeFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ExtIDTypes.Where(a => a.ID != 0);

                if (!(oReq.ExtIDType.ID == 0))
                    query = query.Where(a => a.ID == oReq.ExtIDType.ID);

                if (!string.IsNullOrEmpty(oReq.ExtIDType.Name))
                    query = query.Where(a => a.Name.Contains(oReq.ExtIDType.Name));

                if (!string.IsNullOrEmpty(oReq.ExtIDType.Code))
                    query = query.Where(a => a.Code == oReq.ExtIDType.Code);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            // Logger.InfoFormat("Exiting {0}.CardExtIDTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        
        public static List<ExtIDType> ExtIDTypeGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.CardExtIDTypeGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<ExtIDType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ExtIDTypes.Where(a => IDs.Contains(a.ID)).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.CardExtIDTypeGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<ExtIDType> ExtIDTypeList()
        {
            List<ExtIDType> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ExtIDTypes.Where(a => a.ID > 0).ToList();
            }
            return value;
        }

        #endregion


        #region Payment Mode Payment Details

        public static int PaymentModePaymentDetailsCreate(PaymentModePaymentDetails oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModePaymentDetailsCreate({1})", svcName, (oReq != null) ? oReq.Code : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.PaymentModePaymentDetails.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.PaymentModePaymentDetailsCreate({1}): {2}", svcName, (oReq != null) ? oReq.Code : "NULL", value);
            return value;
        }
        public static int PaymentModePaymentDetailsUpdate(PaymentModePaymentDetails oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModePaymentDetailsUpdate({1})", svcName, (oReq != null) ? oReq.ID.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                PaymentModePaymentDetails paymentModePaymentDetails = ctx.PaymentModePaymentDetails.First(a => a.ID.Equals(oReq.ID));
                paymentModePaymentDetails.Active = oReq.Active;
                paymentModePaymentDetails.Code = oReq.Code;
                paymentModePaymentDetails.Name = oReq.Name;
                paymentModePaymentDetails.Description = oReq.Description;
                paymentModePaymentDetails.LastAccessID = oReq.LastAccessID;
                paymentModePaymentDetails.LastUpdateDT = oReq.LastUpdateDT;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.PaymentModePaymentDetailsUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString() : "NULL", value);
            return value;
        }
        public static List<int> PaymentModePaymentDetailsFind(PaymentModePaymentDetailsFind oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModePaymentDetailsFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.PaymentModePaymentDetails.Where(a => a.ID != 0);

                if (!(oReq.PaymentModePaymentDetails.ID == 0))
                    query = query.Where(a => a.ID == oReq.PaymentModePaymentDetails.ID);

                if (!string.IsNullOrEmpty(oReq.PaymentModePaymentDetails.Code))
                    query = query.Where(a => a.Code == oReq.PaymentModePaymentDetails.Code);

                if (!string.IsNullOrEmpty(oReq.PaymentModePaymentDetails.Name))
                    query = query.Where(a => a.Name.Contains(oReq.PaymentModePaymentDetails.Name));

                if (!string.IsNullOrEmpty(oReq.PaymentModePaymentDetails.Description))
                    query = query.Where(a => a.Description.Contains(oReq.PaymentModePaymentDetails.Description));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PaymentModePaymentDetailsFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<PaymentModePaymentDetails> PaymentModePaymentDetailsGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.PaymentModePaymentDetailsGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<PaymentModePaymentDetails> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PaymentModePaymentDetails.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PaymentModePaymentDetailsGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }

        #endregion

        #region Card Type Payment Details

        public static int CardTypePaymentDetailsCreate(CardTypePaymentDetails oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypePaymentDetailsCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.CardTypePaymentDetails.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.CardTypePaymentDetailsCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int CardTypePaymentDetailsUpdate(CardTypePaymentDetails oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypePaymentDetailsUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                CardTypePaymentDetails cardTypePaymentDetails = ctx.CardTypePaymentDetails.First(a => a.ID == oReq.ID);
                cardTypePaymentDetails.Code = oReq.Code;
                cardTypePaymentDetails.Active = oReq.Active;
                cardTypePaymentDetails.Description = oReq.Description;
                cardTypePaymentDetails.Name = oReq.Name;
                cardTypePaymentDetails.LastUpdateDT = oReq.LastUpdateDT;
                cardTypePaymentDetails.LastAccessID = oReq.LastAccessID;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.CardTypePaymentDetailsUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> CardTypePaymentDetailsFind(CardTypePaymentDetailsFind oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypePaymentDetailsFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.CardTypePaymentDetails.Where(a => a.ID != 0);

                if (!(oReq.CardTypePaymentDetails.ID == 0))
                    query = query.Where(a => a.ID == oReq.CardTypePaymentDetails.ID);

                if (!string.IsNullOrEmpty(oReq.CardTypePaymentDetails.Name))
                    query = query.Where(a => a.Name.Contains(oReq.CardTypePaymentDetails.Name));

                if (!string.IsNullOrEmpty(oReq.CardTypePaymentDetails.Code))
                    query = query.Where(a => a.Code.Contains(oReq.CardTypePaymentDetails.Code));

                if (!string.IsNullOrEmpty(oReq.CardTypePaymentDetails.Description))
                    query = query.Where(a => a.Description.Contains(oReq.CardTypePaymentDetails.Description));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardTypePaymentDetailsFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<CardTypePaymentDetails> CardTypePaymentDetailsGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.CardTypePaymentDetailsGet({1})", svcName, (IDs.Count() > 0) ? IDs[0] : 0);
            List<CardTypePaymentDetails> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.CardTypePaymentDetails.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardTypePaymentDetailsGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<CardTypePaymentDetails> CardTypePaymentList()
        {
            Logger.InfoFormat("Entering {0}.CardTypePaymentList({1})", svcName,"");
            List<CardTypePaymentDetails> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.CardTypePaymentDetails.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardTypePaymentList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region"Get offer Name by Id by VLT"
        /// <summary>
        /// Method to Get Offer Name
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>string</returns>
        public static string GetOfferNameById(int offerID)
        {
            Logger.InfoFormat("Exiting {0}.GetOfferNameById({1})", svcName, offerID);
            List<string> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<string>("usp_GetOfferNameById @OfferID", new SqlParameter("@OfferID", offerID)).ToList();

            }
            Logger.InfoFormat("Exiting {0}.GetOfferNameById({1})", svcName, offerID);
            return value.First().ToString2();
        }
        #endregion


        #region"GetContractByRegId"
        /// <summary>
        /// Method to Get GetContractByRegId
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>string</returns>
        public static List<int> GetContractByRegId(int regID)
        {
            try
            {
                Logger.InfoFormat("Exiting {0}.usp_GetContractByRegId({1})", svcName, regID);
                List<int> value = null;

                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    value = ctx.Database.SqlQuery<int>("usp_GetContractByRegId @RegId", new SqlParameter("@RegId", regID)).ToList();

                }
                Logger.InfoFormat("Exiting {0}.usp_GetContractByRegId({1})", svcName, regID);
                return value;
            }
            catch
            {
                return null;
            }
        }
        #endregion
        #region"GetContractByRegIdSeco"
        /// <summary>
        /// Method to Get GetContractByRegIdSeco
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>string</returns>
        public static int GetContractByRegIdSeco(int regID)
        {
            Logger.InfoFormat("Exiting {0}.usp_GetContractByRegIdSeco({1})", svcName, regID);
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_GetContractByRegIdSeco @RegId", new SqlParameter("@RegId", regID)).ToList();

            }
            Logger.InfoFormat("Exiting {0}.usp_GetContractByRegIdSeco({1})", svcName, regID);
            return value.First();
        }
        #endregion
        #region"GetContractByRegSuppLineId"
        /// <summary>
        /// Method to Get GetContractByRegSuppLineId
        /// </summary>
        /// <param name="regSuppLineID"></param>
        /// <returns>string</returns>
        public static int GetContractByRegSuppLineId(int regSuppLineID)
        {
            try
            {
                Logger.InfoFormat("Exiting {0}.usp_GetContractByRegSuppLineId({1})", svcName, regSuppLineID);
                List<int> value = null;

                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    value = ctx.Database.SqlQuery<int>("usp_GetContractByRegSuppLineId @RegSuppLineID", new SqlParameter("@RegSuppLineID", regSuppLineID)).ToList();

                }
                Logger.InfoFormat("Exiting {0}.usp_GetContractByRegSuppLineId({1})", svcName, regSuppLineID);
                return value.First();
            }
            catch
            {
                return 0;
            }
        }
        #endregion
        #region"GetContractByCode" Added by VLT 06 May 2013
        /// <summary>
        /// Method to Get GetContractByCode
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>string</returns>
        public static string GetContractByCode(string compCode)
        {
            Logger.InfoFormat("Exiting {0}.usp_GetContractByCode({1})", svcName, compCode);
            List<int> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_GetContractByCode @Code", new SqlParameter("@Code", compCode)).ToList();

            }
            Logger.InfoFormat("Exiting {0}.usp_GetContractByCode({1})", svcName, compCode);
            if (value != null && value.Count > 0)
                return value.First().ToString();

            return string.Empty;
        }
        #endregion


        #region Change Package - IsellHome
        public static List<FilterChangePckgs> GetFilterChangePckgs(string[] currentPckgIds)
        {
            List<FilterChangePckgs> lstFilterChangePckgs = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.FilterChangePckgs.Where(x => currentPckgIds.Contains(x.Oldpackage));
                lstFilterChangePckgs = query.ToList();
            }
            return lstFilterChangePckgs;
        }
        #endregion


        #region smart related methods

        public static List<int> GetContractByRegIdForSmart(int regID)
        {
            Logger.InfoFormat("Exiting {0}.usp_GetContractByRegId({1})", svcName, regID);
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_GetContractByRegId_Smart @RegId", new SqlParameter("@RegId", regID)).ToList();

            }
            Logger.InfoFormat("Exiting {0}.usp_GetContractByRegId({1})", svcName, regID);
            return value;
        }

        #endregion

        #region "SimReplacement N-M & M-N"
        public static Dictionary<string, string> GetDataComponentGreater2GB()
        {
            Dictionary <string,string> _dicQuery = new Dictionary<string,string>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                /* as discussed with rajesh on 7thMarch2014, this stmt is not need , we need just check the existence of kenancode in DataPlanComponents table
                var query = (from pbpc in ctx.PgmBdlPckComponents
                            join dpcl in ctx.DataPlanComponents on  pbpc.KenanCode equals dpcl.DataComponentID 
                            where ((dpcl.Status == true) && (pbpc.Active == true) && (pbpc.LinkType.Equals("DC")))
                            select new
                            {
                                kenanCode = pbpc.KenanCode,
                                linktype = pbpc.LinkType
                            }).Distinct();
                */

                var query = (from dpcl in ctx.DataPlanComponents
                             where (dpcl.Status == true)
                             select new { kenanCode = dpcl.DataComponentID, id = dpcl.Id }
                                  );
                              

                if (!ReferenceEquals(query, null))
                {  
                    foreach( var item in query)
                    {
                        _dicQuery.Add(item.id.ToString()  , item.kenanCode.ToString2());
                    }
                }
                    
            }
            return _dicQuery;
        }
        #endregion


        #region"GetContractByRegIdandTypeID"
        /// <summary>
        /// Method to Get GetContractByRegIdandTypeID
        /// </summary>
        /// <param name="regID"></param>
        /// <returns>list</returns>
        public static List<string> GetDataContractByRegId(int regID)
        {
            Logger.InfoFormat("Exiting {0}.GetContractByRegIdandTypeID({1})", svcName, regID);
            List<string> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<string>("GetDataContractByRegId @RegId", new SqlParameter("@RegId", regID)).ToList();

            }
            Logger.InfoFormat("Exiting {0}.GetContractByRegIdandTypeID({1})", svcName, regID);
            return value;
        }
        #endregion 

        #region ThirdPartyAuthType
        public static int ThirdPartyAuthTypeCreate(ThirdPartyAuthType oReq)
        {
            Logger.InfoFormat("Entering {0}.CardThirdPartyAuthTypeCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.ThirdPartyAuthTypes.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.ThirdPartyAuthTypeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int ThirdPartyAuthTypeUpdate(ThirdPartyAuthType oReq)
        {
            Logger.InfoFormat("Entering {0}.ThirdPartyAuthTypeUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ThirdPartyAuthType ThirdPartyAuthType = ctx.ThirdPartyAuthTypes.First(a => a.ID == oReq.ID);
                ThirdPartyAuthType.Name = oReq.Name;
                //ctx.Entry<Language>(oReq).State = EntityState.Modified;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.ThirdPartyAuthTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> ThirdPartyAuthTypeFind(ThirdPartyAuthTypeFind oReq)
        {
            Logger.InfoFormat("Entering {0}.ThirdPartyAuthTypeFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ThirdPartyAuthTypes.Where(a => a.ID != 0);

                if (!(oReq.ThirdPartyAuthType.ID == 0))
                    query = query.Where(a => a.ID == oReq.ThirdPartyAuthType.ID);

                if (!string.IsNullOrEmpty(oReq.ThirdPartyAuthType.Name))
                    query = query.Where(a => a.Name.Contains(oReq.ThirdPartyAuthType.Name));

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ThirdPartyAuthTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<ThirdPartyAuthType> ThirdPartyAuthTypeGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.ThirdPartyAuthTypeGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
            List<ThirdPartyAuthType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ThirdPartyAuthTypes.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ThirdPartyAuthTypeGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
            return value;
        }
        public static List<ThirdPartyAuthType> ThirdPartyAuthTypeGetList()
        {
            Logger.InfoFormat("Entering {0}.ThirdPartyAuthTypeList({1})", svcName, "");
            List<ThirdPartyAuthType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ThirdPartyAuthTypes.ToList();
            }
            Logger.InfoFormat("Exiting {0}.ThirdPartyAuthTypeGetList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion
    }
}
