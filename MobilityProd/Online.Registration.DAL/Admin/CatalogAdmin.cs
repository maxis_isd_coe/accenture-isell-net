﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using log4net;
using Online.Registration.DAL.Models;
using SNT.Utility;


namespace Online.Registration.DAL.Admin
{
    public static partial class OnlineRegAdmin
    {
        static string svcName = "Online.Registration.DAL";
        //private static readonly string _ConnectionString = !ReferenceEquals(ConfigurationManager.ConnectionStrings["OnlineStoreDbContext"], null) ? ConfigurationManager.ConnectionStrings["OnlineStoreDbContext"].ToString2() : "";
        private static readonly ILog Logger = LogManager.GetLogger(typeof(OnlineRegAdmin));

        #region Program

        public static int ProgramCreate(Program oReq)
        {
            ////Logger.InfoFormat("Entering {0}.ProgramCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Programs.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            ////Logger.InfoFormat("Exiting {0}.ProgramCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int ProgramUpdate(Program oReq)
        {
            Logger.InfoFormat("Entering {0}.ProgramUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                Program program = ctx.Programs.First(a => a.ID == oReq.ID);
                program.LastAccessID = oReq.LastAccessID;
                program.LastUpdateDT = oReq.LastUpdateDT;
                program.Name = oReq.Name;
                program.Code = oReq.Code;
                program.MinAge = oReq.MinAge;
                program.Active = oReq.Active;
                program.EndDate = oReq.EndDate;
                program.StartDate = oReq.StartDate;
                program.Description = oReq.Description;
                program.IsOutrightSales = oReq.IsOutrightSales;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.ProgramUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> ProgramFind(ProgramFind oReq)
        {
            ////Logger.InfoFormat("Entering {0}.ProgramFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Programs.Where(a => a.ID != 0);

                if (!(oReq.Program.ID == 0))
                    query = query.Where(a => a.ID == oReq.Program.ID);

                if (!string.IsNullOrEmpty(oReq.Program.Code))
                    query = query.Where(a => a.Code == oReq.Program.Code);

                if (!string.IsNullOrEmpty(oReq.Program.Name))
                    query = query.Where(a => a.Name.Contains(oReq.Program.Name));

                if (!(oReq.StartDateFrom == null))
                    query = query.Where(a => a.StartDate.Date >= oReq.StartDateFrom.Value.Date);

                if (!(oReq.StartDateTo == null))
                    query = query.Where(a => a.StartDate.Date <= oReq.StartDateTo.Value.Date);

                if (!(oReq.EndDateFrom == null))
                    query = query.Where(a => a.EndDate.Value.Date >= oReq.EndDateFrom.Value.Date);

                if (!(oReq.EndDateTo == null))
                    query = query.Where(a => a.EndDate.Value.Date <= oReq.EndDateTo.Value.Date);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            ////Logger.InfoFormat("Exiting {0}.ProgramFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<Program> ProgramGet(List<int> IDs)
        {
            ////Logger.InfoFormat("Entering {0}.ProgramGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<Program> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Programs.Where(a => IDs.Contains(a.ID)).ToList();
            }
            ////Logger.InfoFormat("Exiting {0}.ProgramGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<Program> ProgramsList()
        {
            List<Program> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Programs.Where(a => a.ID > 0).ToList();
            }

            return value;
        }

        #endregion

        #region Bundle

        public static int BundleCreate(Bundle oReq)
        {
            //Logger.InfoFormat("Entering {0}.BundleCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Bundles.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            //Logger.InfoFormat("Exiting {0}.BundleCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int BundleUpdate(Bundle oReq)
        {
            //Logger.InfoFormat("Entering {0}.BundleUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                Bundle bundle = ctx.Bundles.First(a => a.ID == oReq.ID);
                bundle.LastAccessID = oReq.LastAccessID;
                bundle.LastUpdateDT = oReq.LastUpdateDT;
                bundle.MinPackage = oReq.MinPackage;
                bundle.Name = oReq.Name;
                bundle.ProgramID = oReq.ProgramID;
                bundle.Active = oReq.Active;
                bundle.Code = oReq.Code;
                bundle.Description = oReq.Description;
                value = ctx.SaveChanges();
            }
            //Logger.InfoFormat("Exiting {0}.BundleUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> BundleFind(BundleFind oReq)
        {
            //Logger.InfoFormat("Entering {0}.BundleFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Bundles.Where(a => a.ID != 0);

                if (oReq.Bundle.ID != 0)
                    query = query.Where(a => a.ID == oReq.Bundle.ID);

                if (!string.IsNullOrEmpty(oReq.Bundle.Code))
                    query = query.Where(a => a.Code == oReq.Bundle.Code);

                if (!string.IsNullOrEmpty(oReq.Bundle.Name))
                    query = query.Where(a => a.Name.Contains(oReq.Bundle.Name));

                if (oReq.Bundle.MinPackage != 0)
                    query = query.Where(a => a.MinPackage >= oReq.Bundle.MinPackage);

                if (oReq.Bundle.ProgramID != 0)
                    query = query.Where(a => a.ProgramID == oReq.Bundle.ProgramID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.BundleFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<Bundle> BundleGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.BundleGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<Bundle> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Bundles.Where(a => IDs.Contains(a.ID)).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.BundleGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<Bundle> BundlesList()
        {
            List<Bundle> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Bundles.Where(a => a.ID > 0).ToList();
            }
            return value;
        }
        #endregion

        #region Package

        public static Int32 GettllnkContractDataPlan(int selectedid)
        {
            Logger.InfoFormat("Entering {0}.GettllnkContractDataPlan({1})", svcName, selectedid);

            List<tbllnkContractDataPlan> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                value = ctx.ContractDataPlanIds.AsEnumerable().Where(a => a.ContractId == selectedid && a.Status == "1").Select(a => new tbllnkContractDataPlan() { DataPlanId = a.DataPlanId }).ToList();

            }
            //Changed by ravi as it is throwing exception
            var valuefinal2 = 0;
            if (value != null)
            {
                if (value.Count > 0)
                {
                    List<Int32> valuefinal = null;
                    valuefinal = value.Select(a => Convert.ToInt32(a.DataPlanId)).ToList();
                    valuefinal2 = valuefinal != null ? valuefinal[0] : 0;
                    Logger.InfoFormat("Exiting {0}.GettllnkContractDataPlan({1})", svcName, selectedid);
                }
            }
            return valuefinal2;

        }
        // ranjeeth for islandeviceCR
        public static List<int> GettllnkContractDataPlanList(List<int> selectedid)
        {
            Logger.InfoFormat("Entering {0}.GettllnkContractDataPlan({1})", svcName, selectedid);
            List<tbllnkContractDataPlan> value = new List<tbllnkContractDataPlan>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ContractDataPlanIds.AsEnumerable().Where(a => selectedid.Contains(a.ContractId) && a.Status == "1").Select(a => new tbllnkContractDataPlan() { DataPlanId = a.DataPlanId }).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GettllnkContractDataPlan({1})", svcName, selectedid);
            if (value.Count < 1)
            {
                tbllnkContractDataPlan dummyCDP = new tbllnkContractDataPlan();
                dummyCDP.DataPlanId = 0;
                value.Add(dummyCDP);
            }
            return value.Select(a => Convert.ToInt32(a.DataPlanId)).ToList();
        }

        public static Int32 GettllnkContractDataPlan(List<int> selectedids)
        {
            Logger.InfoFormat("Entering {0}.GettllnkContractDataPlan({1})", svcName, string.Join(",", selectedids));
            List<tbllnkContractDataPlan> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //value = ctx.ContractDataPlanIds.AsEnumerable().Where(a => a.ContractId == selectedid && a.Status == "1").Select(a => new tbllnkContractDataPlan() { DataPlanId = a.DataPlanId }).ToList();
                value = ctx.ContractDataPlanIds.AsEnumerable().Where(a => a.Status == "1" && selectedids.Contains(a.ContractId)).Select(a => new tbllnkContractDataPlan() { DataPlanId = a.DataPlanId }).ToList();
            }
            //Changed by ravi as it is throwing exception
            var valuefinal2 = 0;
            if (value != null)
            {
                if (value.Count > 0)
                {
                    List<Int32> valuefinal = null;
                    valuefinal = value.Select(a => Convert.ToInt32(a.DataPlanId)).ToList();
                    valuefinal2 = valuefinal != null ? valuefinal[0] : 0;
                    Logger.InfoFormat("Exiting {0}.GettllnkContractDataPlan({1})", svcName, string.Join(",", selectedids));
                }
            }
            return valuefinal2;
        }

        public static List<Int32> GetDependentComponents(int componentId)
        {
            Logger.InfoFormat("Entering {0}.GetDependentComponents({1})", svcName, componentId);

            List<tbllnkContractDataPlan> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                value = ctx.ContractDataPlanIds.AsEnumerable().Where(a => a.ContractId == componentId && a.Status == "1").Select(a => new tbllnkContractDataPlan() { DataPlanId = a.DataPlanId }).ToList();

            }
            List<Int32> valuefinal = null;
            var depList = value.Select(a => Convert.ToInt32(a.DataPlanId));
            if (depList != null && depList.Count() > 0)
                valuefinal = depList.ToList();

            Logger.InfoFormat("Exiting {0}.GetDependentComponents({1})", svcName, componentId);
            return valuefinal;

        }

        public static List<dependentContract> GetContractsOnOfferId(string OfferIds)
        {
            /*******/
            Logger.InfoFormat("Entering {0}.GetContractsOnOfferId({1})", svcName, OfferIds);
            List<dependentContract> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<dependentContract>("USP_GetContractsOnOffers  @Offers", new SqlParameter("@Offers", OfferIds)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetContractsOnOfferId({1})", svcName, OfferIds);
            return value;
            /*******/
        }
        public static List<PkgDataPlanId> Getdataplanpackageids(int selectedtype)
        {
            Logger.InfoFormat("Entering {0}.GetDataPlanIds({1})", svcName, selectedtype);

            List<PkgDataPlanId> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                value = ctx.DataPlanIds.AsEnumerable().Where(a => a.MobileRegType == selectedtype && a.Status == 1).Select(a => new PkgDataPlanId() { BdlDataPkgId = a.BdlDataPkgId, Modelid = a.Modelid }).ToList();

            }
            Logger.InfoFormat("Exiting {0}.GetDataPlanIds({1}): {2}", svcName, selectedtype, value.Count());
            return value;

        }
        public static List<PkgData> GetMandatoryVas(int selectedid)
        {
            Logger.InfoFormat("Entering {0}.CountryFind({1})", svcName, selectedid);

            List<PkgData> value = null;
            List<PkgData> filvalue = new List<PkgData>();
            List<int> planIds = new List<int>();
            string strSelectID = Convert.ToString(selectedid);
            PgmBdlPckComponent comp = new PgmBdlPckComponent();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PgmPkgData.AsEnumerable().Where(a => a.BdlPkgId.Equals(Convert.ToString(strSelectID)) && a.Status == 1).Select(a => new PkgData() { BdlDataPkgId = a.BdlDataPkgId, Plan_Type = a.Plan_Type }).ToList();
                //value = ctx.PgmPkgData.AsEnumerable().Where(a => a.BdlPkgId.Equals(Convert.ToString(strSelectID)) && a.Status == 1).Select(a => new PkgData() { BdlDataPkgId = a.BdlDataPkgId, Plan_Type = a.Plan_Type }).ToList();
                foreach (var cp in value)
                {
                    comp = new PgmBdlPckComponent();
                    int bdlPkgid = Convert.ToInt32(cp.BdlDataPkgId);
                    comp = ctx.PgmBdlPckComponents.First(b => b.ID == bdlPkgid);
                    if (comp.Active)
                    {
                        filvalue.Add(cp);
                    }
                }
            }
            Logger.InfoFormat("Exiting {0}.PgmBdlPckComponentFindonbundleselection({1}): {2}", svcName, selectedid, value.Count());
            return filvalue;

        }

        public static int PackageCreate(Package oReq)
        {
            //Logger.InfoFormat("Entering {0}.PackageCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Packages.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            //Logger.InfoFormat("Exiting {0}.PackageCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int PackageUpdate(Package oReq)
        {
            //Logger.InfoFormat("Entering {0}.PackageUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Package package = ctx.Packages.First(a => a.ID.Equals(oReq.ID));
                package.Code = oReq.Code;
                package.Name = oReq.Name;
                package.Description = oReq.Description;
                package.MinModelGroup = oReq.MinModelGroup;
                package.MinComponent = oReq.MinComponent;
                package.PackageTypeID = oReq.PackageTypeID;
                package.Active = oReq.Active;
                package.IsMandatory = oReq.IsMandatory;
                package.LastAccessID = oReq.LastAccessID;
                package.LastUpdateDT = oReq.LastUpdateDT;
                value = ctx.SaveChanges();
            }
            //Logger.InfoFormat("Exiting {0}.PackageUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> PackageFind(PackageFind oReq)
        {
            //Logger.InfoFormat("Entering {0}.PackageFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Packages.Where(a => a.ID != 0);

                if (oReq.Package.ID != 0)
                    query = query.Where(a => a.ID == oReq.Package.ID);

                if (!string.IsNullOrEmpty(oReq.Package.Code))
                    query = query.Where(a => a.Code == oReq.Package.Code);

                if (!string.IsNullOrEmpty(oReq.Package.Name))
                    query = query.Where(a => a.Name.Contains(oReq.Package.Name));

                if (oReq.IsMandatory.HasValue)
                    query = query.Where(a => a.IsMandatory == oReq.IsMandatory.Value);

                if (oReq.Package.MinComponent != 0)
                    query = query.Where(a => a.MinComponent.ToInt() <= oReq.Package.MinComponent);

                if (oReq.Package.MinModelGroup != 0)
                    query = query.Where(a => a.MinModelGroup.ToInt() <= oReq.Package.MinModelGroup);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.Package.PackageTypeID != 0)
                    query = query.Where(a => a.PackageTypeID == oReq.Package.PackageTypeID);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.PackageFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<Package> PackageGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.PackageGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<Package> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Packages.Where(a => IDs.Contains(a.ID)).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.PackageGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<Package> PackagesList()
        {
            List<Package> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Packages.Where(a => a.ID > 0).ToList();
            }
            return value;
        }
        #endregion

        #region PackageType

        public static int PackageTypeCreate(PackageType oReq)
        {
            //Logger.InfoFormat("Entering {0}.PackageTypeCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                ctx.PackageTypes.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            //Logger.InfoFormat("Exiting {0}.PackageTypeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int PackageTypeUpdate(PackageType oReq)
        {
            //Logger.InfoFormat("Entering {0}.PackageTypeUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                PackageType packageType = ctx.PackageTypes.First(a => a.ID.Equals(oReq.ID));
                packageType.Code = oReq.Code;
                packageType.Name = oReq.Name;
                packageType.Description = oReq.Description;
                packageType.Active = oReq.Active;
                packageType.LastAccessID = oReq.LastAccessID;
                packageType.LastUpdateDT = oReq.LastUpdateDT;
                value = ctx.SaveChanges();
            }
            //Logger.InfoFormat("Exiting {0}.PackageUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> PackageTypeFind(PackageTypeFind oReq)
        {
            //Logger.InfoFormat("Entering {0}.PackageTypeFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.PackageTypes.Where(a => a.ID != 0);

                if (oReq.PackageType.ID != 0)
                    query = query.Where(a => a.ID == oReq.PackageType.ID);

                if (!string.IsNullOrEmpty(oReq.PackageType.Code))
                    query = query.Where(a => a.Code == oReq.PackageType.Code);

                if (!string.IsNullOrEmpty(oReq.PackageType.Name))
                    query = query.Where(a => a.Name.Contains(oReq.PackageType.Name));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.PackageTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<PackageType> PackageTypeGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.PackageTypeGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<PackageType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PackageTypes.Where(a => IDs.Contains(a.ID)).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.PackageTypeGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<PackageType> PackageTypesList()
        {

            List<PackageType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PackageTypes.Where(a => a.ID > 0).ToList();
            }

            return value;
        }


        #endregion

        #region Component

        public static int ComponentCreate(Component oReq)
        {
            //Logger.InfoFormat("Entering {0}.ComponentCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Components.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            //Logger.InfoFormat("Exiting {0}.ComponentCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int ComponentUpdate(Component oReq)
        {
            //Logger.InfoFormat("Entering {0}.ComponentUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Component component = ctx.Components.First(a => a.ID.Equals(oReq.ID));
                component.Code = oReq.Code;
                component.Name = oReq.Name;
                component.Description = oReq.Description;
                component.ComponentTypeID = oReq.ComponentTypeID;
                component.Active = oReq.Active;
                component.IsMandatory = oReq.IsMandatory;
                component.MinModelGroup = oReq.MinModelGroup;
                component.LastAccessID = oReq.LastAccessID;
                component.LastUpdateDT = oReq.LastUpdateDT;
                //ctx.Entry(oReq).State = EntityState.Modified;
                value = ctx.SaveChanges();
            }
            //Logger.InfoFormat("Exiting {0}.ComponentUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> ComponentFind(ComponentFind oReq)
        {
            //Logger.InfoFormat("Entering {0}.ComponentFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Components.Where(a => a.ID != 0);

                if (oReq.Component.ID != 0)
                    query = query.Where(a => a.ID == oReq.Component.ID);

                if (!string.IsNullOrEmpty(oReq.Component.Code))
                    query = query.Where(a => a.Code == oReq.Component.Code);

                if (!string.IsNullOrEmpty(oReq.Component.Name))
                    query = query.Where(a => a.Name.Contains(oReq.Component.Name));

                if (oReq.IsMandatory.HasValue)
                    query = query.Where(a => a.IsMandatory == oReq.IsMandatory.Value);

                if (oReq.Component.MinModelGroup != 0)
                    query = query.Where(a => a.MinModelGroup == oReq.Component.MinModelGroup);

                if (oReq.Component.ComponentTypeID != 0)
                    query = query.Where(a => a.ComponentTypeID == oReq.Component.ComponentTypeID);

				if (oReq.Component.IsSharing)
					query = query.Where(a => a.IsSharing == true);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.ComponentFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<Component> ComponentGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.ComponentGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<Component> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Components.Where(a => IDs.Contains(a.ID)).ToList();

                List<ComponentGroup> objGroupsList = new List<ComponentGroup>();
                objGroupsList = ctx.ComponentGroups.Where(grp => grp.isActive == true).ToList();

                if (value != null && value.Count > 0)
                {
                    foreach (Component comp in value)
                    {
                        if (comp.GroupId != null)
                            comp.Description = comp.Description + "+" + objGroupsList.First(grp => grp.ID == comp.GroupId).ComponentGroupName;

                    }
                }

            }
            //Logger.InfoFormat("Exiting {0}.ComponentGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<Component> ContractComponentGet()
        {
            //Logger.InfoFormat("Entering {0}.ComponentGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<Component> value = null;
            var voiceContractType = Properties.Settings.Default.ComponentType_VoiceContract;
            var dataContractType = Properties.Settings.Default.ComponentType_DataContract;
            var voiceContractTypeID = 0;
            var dataContractTypeID = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                voiceContractTypeID = ctx.ComponentTypes.Where(a => a.Code == voiceContractType).SingleOrDefault().ID;
                dataContractTypeID = ctx.ComponentTypes.Where(a => a.Code == dataContractType).SingleOrDefault().ID;
                value = ctx.Components.Where(a => a.ComponentTypeID == voiceContractTypeID || a.ComponentTypeID == dataContractTypeID).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.ComponentGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        #endregion

        #region ComponentType

        public static int ComponentTypeCreate(ComponentType oReq)
        {
            //Logger.InfoFormat("Entering {0}.ComponentTypeCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.ComponentTypes.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            //Logger.InfoFormat("Exiting {0}.ComponentTypeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int ComponentTypeUpdate(ComponentType oReq)
        {
            //Logger.InfoFormat("Entering {0}.ComponentTypeUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var componentType = ctx.ComponentTypes.First(a => a.ID == oReq.ID);
                componentType.Name = oReq.Name;
                componentType.Code = oReq.Code;
                componentType.Description = oReq.Description;
                componentType.Active = oReq.Active;
                componentType.LastAccessID = oReq.LastAccessID;
                componentType.LastUpdateDT = oReq.LastUpdateDT;
                //ctx.Entry(oReq).State = EntityState.Modified;
                value = ctx.SaveChanges();
            }
            //Logger.InfoFormat("Exiting {0}.ComponentTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> ComponentTypeFind(ComponentTypeFind oReq)
        {
            //Logger.InfoFormat("Entering {0}.ComponentTypeFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ComponentTypes.Where(a => a.ID != 0);

                if (oReq.ComponentType.ID != 0)
                    query = query.Where(a => a.ID == oReq.ComponentType.ID);

                if (!string.IsNullOrEmpty(oReq.ComponentType.Code))
                    query = query.Where(a => a.Code == oReq.ComponentType.Code);

                if (!string.IsNullOrEmpty(oReq.ComponentType.Name))
                    query = query.Where(a => a.Name.Contains(oReq.ComponentType.Name));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);


                value = query.Select(a => a.ID).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.ComponentTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<ComponentType> ComponentTypeGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.ComponentTypetGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<ComponentType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ComponentTypes.Where(a => IDs.Contains(a.ID)).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.ComponentTypeGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<ComponentType> ComponentTypeList()
        {

            List<ComponentType> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ComponentTypes.Where(a => a.ID > 0).ToList();
            }

            return value;
        }

        #endregion

        #region ComponentRelation

		public static List<ComponentRelation> GetAllComponentRelationByRule(string rules)
		{

			List<ComponentRelation> value = null;
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				value = ctx.ComponentRelations.Where(a => a.Rules == rules).ToList();
			}

			return value;
		}

        public static List<ComponentRelation> ComponentRelationGet(int planBundle, string rules)
        {

            List<ComponentRelation> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ComponentRelations.Where(a => a.PlanBundle == planBundle && a.Rules == rules).ToList();
            }

            return value;
        }

		public static List<ComponentRelation> ComponentRelationGetByComponent(int componentPgm, string rules)
		{

			List<ComponentRelation> value = null;
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				value = ctx.ComponentRelations.Where(a => a.ComponentPgm == componentPgm && a.Rules == rules).ToList();
			}

			return value;
		}
        #endregion

		#region planDeviceValidationRules
		public static List<PlanDeviceValidationRules> PlanDeviceValidationRulesGet(PlanDeviceValidationRules oReq)
		{

			List<PlanDeviceValidationRules> value = null;
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{

				value = ctx.PlanDeviceValidationRules.Where(a => a.existingPlanId == oReq.existingPlanId && a.existingDeviceArticleId == oReq.existingDeviceArticleId).ToList();

			}

			return value;
		}
		#endregion

        #region ModelGroup

        public static int ModelGroupCreate(ModelGroup oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.ModelGroups.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.ModelGroupCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int ModelGroupUpdate(ModelGroup oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry<ModelGroup>(oReq).State = EntityState.Modified;
                ModelGroup ModelGroup = ctx.ModelGroups.First(a => a.ID == oReq.ID);
                ModelGroup.Active = oReq.Active;
                ModelGroup.Code = oReq.Code;
                ModelGroup.Description = oReq.Description;
                ModelGroup.IsMandatory = oReq.IsMandatory;
                ModelGroup.LastAccessID = oReq.LastAccessID;
                ModelGroup.LastUpdateDT = oReq.LastUpdateDT;
                ModelGroup.MinModel = oReq.MinModel;
                ModelGroup.Name = oReq.Name;
                ModelGroup.PgmBdlPckComponentID = oReq.PgmBdlPckComponentID;
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.ModelGroupUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> ModelGroupFind(ModelGroupFind oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ModelGroups.Where(a => a.ID != 0);

                if (oReq.ModelGroup.ID != 0)
                    query = query.Where(a => a.ID == oReq.ModelGroup.ID);

                if (!string.IsNullOrEmpty(oReq.ModelGroup.Code))
                    query = query.Where(a => a.Code == oReq.ModelGroup.Code);

                if (!string.IsNullOrEmpty(oReq.ModelGroup.Name))
                    query = query.Where((a) => a.Name.Contains(oReq.ModelGroup.Name));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PgmBdlPkgCompIDs != null)
                {
                    if (oReq.PgmBdlPkgCompIDs.Count() > 0)
                        query = query.Where(a => oReq.PgmBdlPkgCompIDs.Contains(a.PgmBdlPckComponentID));
                }

                if (oReq.ModelGroup.PgmBdlPckComponentID != 0)
                    query = query.Where(a => a.PgmBdlPckComponentID == oReq.ModelGroup.PgmBdlPckComponentID);

                if (oReq.IsMandatory.HasValue)
                    query = query.Where(a => a.IsMandatory == oReq.IsMandatory.Value);
                if (oReq.ModelGroup.MinModel != 0)
                    query = query.Where(a => a.MinModel == oReq.ModelGroup.MinModel);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ModelGroupFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<ModelGroup> ModelGroupGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<ModelGroup> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ModelGroups.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ModelGroupGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<ModelGroup> ModelGroupList()
        {
            Logger.InfoFormat("Entering {0}.ModelGroupList({1})", svcName, "");
            List<ModelGroup> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ModelGroups.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ModelGroupList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        public static List<ModelGroup> ModelGroupGetByPBPCIDs(List<int> pbpcIDs)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupGetByPBPCIDs({1})", svcName, (pbpcIDs != null) ? pbpcIDs[0] : 0);
            List<ModelGroup> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ModelGroups.Where(a => pbpcIDs.Contains(a.PgmBdlPckComponentID) && a.Active == true).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ModelGroupGetByPBPCIDs({1}): {2}", svcName, (pbpcIDs != null) ? pbpcIDs[0] : 0, value.Count());
            return value;
        }

        #endregion

        #region PgmBdlPckComponent

        public static int ProgramBundlePackageComponentCreate(PgmBdlPckComponent oReq)
        {
            Logger.InfoFormat("Entering {0}.ProgramBundlePackageComponentCreate({1})", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.PgmBdlPckComponents.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.ProgramBundlePackageComponentCreate({1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL", value);
            return value;
        }
        public static int ProgramBundlePackageComponentUpdate(PgmBdlPckComponent oReq)
        {
            Logger.InfoFormat("Entering {0}.ProgramBundlePackageComponentUpdate({1})", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry<PgmBdlPckComponent>(oReq).State = EntityState.Modified;
                PgmBdlPckComponent pgmBdlPckComponent = ctx.PgmBdlPckComponents.First(a => a.ID == oReq.ID);
                pgmBdlPckComponent.Active = oReq.Active;
                pgmBdlPckComponent.ChildID = oReq.ChildID;
                pgmBdlPckComponent.Code = oReq.Code;
                pgmBdlPckComponent.Description = oReq.Description;
                pgmBdlPckComponent.IsMandatory = oReq.IsMandatory;
                pgmBdlPckComponent.Name = oReq.Name;
                pgmBdlPckComponent.Price = oReq.Price;
                pgmBdlPckComponent.Value = oReq.Value;
                pgmBdlPckComponent.EndDate = oReq.EndDate;
                pgmBdlPckComponent.KenanCode = oReq.KenanCode;
                pgmBdlPckComponent.AlternateKenanCode = oReq.AlternateKenanCode;
                pgmBdlPckComponent.LastAccessID = oReq.LastAccessID;
                pgmBdlPckComponent.LastUpdateDT = oReq.LastUpdateDT;
                pgmBdlPckComponent.NeedActivation = oReq.NeedActivation;
                pgmBdlPckComponent.NeedProvision = oReq.NeedProvision;
                pgmBdlPckComponent.ParentID = oReq.ParentID;
                pgmBdlPckComponent.StartDate = oReq.StartDate;
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.ProgramBundlePackageComponentUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL", value);
            return value;
        }
        public static List<int> ProgramBundlePackageComponentFind(PgmBdlPckComponentFind oReq)
        {
            Logger.InfoFormat("Entering {0}.ProgramBundlePackageComponentFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.PgmBdlPckComponents.Where(a => a.ID != 0);

                //query = query.Where(a => EntityFunctions.TruncateTime(a.StartDate) <= EntityFunctions.TruncateTime(DateTime.Now) &&
                //            (!a.EndDate.HasValue || EntityFunctions.TruncateTime(a.EndDate.Value) >= EntityFunctions.TruncateTime(DateTime.Now)));

                if (oReq.PgmBdlPckComponent.ID != 0)
                    query = query.Where(a => a.ID == oReq.PgmBdlPckComponent.ID);

                if (oReq.ParentIDs.Count() > 0)
                {
                    query = query.Where(a => oReq.ParentIDs.Contains(a.ParentID));
                }

                if (oReq.ChildIDs.Count() > 0)
                {
                    query = query.Where(a => oReq.ChildIDs.Contains(a.ChildID));
                }

                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.LinkType))
                    query = query.Where(a => a.LinkType == oReq.PgmBdlPckComponent.LinkType);

                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.PlanType))
                    query = query.Where(a => a.PlanType == oReq.PgmBdlPckComponent.PlanType);

                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.Code))
                    query = query.Where(a => a.Code == oReq.PgmBdlPckComponent.Code);

                if (oReq.PgmBdlPckComponent.ParentID != 0)
                    query = query.Where(a => a.ParentID == oReq.PgmBdlPckComponent.ParentID);

                if (oReq.PgmBdlPckComponent.ChildID != 0)
                    query = query.Where(a => a.ChildID == oReq.PgmBdlPckComponent.ChildID);

                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.FilterOrgType))
                    query = query.Where(a => ("," + a.FilterOrgType + ",").Contains("," + oReq.PgmBdlPckComponent.FilterOrgType + ","));

                //if (oReq.StartDateFrom.HasValue)
                //    query = query.Where(a => a.StartDate.Date >= oReq.StartDateFrom.Value.Date);

                //if (oReq.StartDateTo.HasValue)
                //    query = query.Where(a => a.StartDate.Date <= oReq.StartDateTo.Value.Date);

                //if (oReq.EndDateFrom.HasValue)
                //    query = query.Where(a => a.EndDate.Value.Date >= oReq.EndDateFrom.Value.Date);

                //if (oReq.EndDateTo.HasValue)
                //    query = query.Where(a => a.EndDate.Value.Date <= oReq.EndDateTo.Value.Date);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);
                //value = query.Select((a) => a.ID).ToList();

                if (oReq.IsMandatory.HasValue)
                    query = query.Where(a => a.IsMandatory == oReq.IsMandatory.Value);

                if (oReq.NeedProvision.HasValue)
                    query = query.Where(a => a.NeedProvision == oReq.NeedProvision.Value);
                //value = query.Select((a) => a.ID).ToList();

                if (oReq.NeedActivation.HasValue)
                    query = query.Where(a => a.NeedActivation == oReq.NeedActivation.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ProgramBundlePackageComponentFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<PgmBdlPckComponent> ProgramBundlePackageComponentGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.ProgramBundlePackageComponentGet({1})", svcName, (IDs.Count != 0) ? IDs[0] : 0);
            List<PgmBdlPckComponent> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PgmBdlPckComponents.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ProgramBundlePackageComponentGet({1}): {2}", svcName, (IDs.Count != 0) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<PgmBdlPckComponent> ProgramBundlePackageComponentGet1(PgmBdlPckComponentFind oReq)
        {
            if (oReq.PgmBdlPckComponent == null)
            {
                oReq.PgmBdlPckComponent = new PgmBdlPckComponent();
            }
            Logger.InfoFormat("Entering {0}.ProgramBundlePackageComponentGet1{1})", svcName, oReq.PgmBdlPckComponent.ID);
            List<PgmBdlPckComponent> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.PgmBdlPckComponents.Where(a => a.ID != 0);
                if (oReq.PgmBdlPckComponent.ID != 0)
                    query = query.Where(a => a.ID == oReq.PgmBdlPckComponent.ID);
                if (oReq.ParentIDs.Count() > 0)
                    query = query.Where(a => oReq.ParentIDs.Contains(a.ParentID));
                if (oReq.ChildIDs.Count() > 0)
                    query = query.Where(a => oReq.ChildIDs.Contains(a.ChildID));
                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.LinkType))
                    query = query.Where(a => a.LinkType == oReq.PgmBdlPckComponent.LinkType);
                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.PlanType))
                    query = query.Where(a => a.PlanType == oReq.PgmBdlPckComponent.PlanType);
                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.Code))
                    query = query.Where(a => a.Code == oReq.PgmBdlPckComponent.Code);
                if (oReq.PgmBdlPckComponent.ParentID != 0)
                    query = query.Where(a => a.ParentID == oReq.PgmBdlPckComponent.ParentID);
                if (oReq.PgmBdlPckComponent.ChildID != 0)
                    query = query.Where(a => a.ChildID == oReq.PgmBdlPckComponent.ChildID);
                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.FilterOrgType))
                    query = query.Where(a => ("," + a.FilterOrgType + ",").Contains("," + oReq.PgmBdlPckComponent.FilterOrgType + ","));
                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);
                if (oReq.IsMandatory.HasValue)
                    query = query.Where(a => a.IsMandatory == oReq.IsMandatory.Value);
                if (oReq.NeedProvision.HasValue)
                    query = query.Where(a => a.NeedProvision == oReq.NeedProvision.Value);
                if (oReq.NeedActivation.HasValue)
                    query = query.Where(a => a.NeedActivation == oReq.NeedActivation.Value);
                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.ToList();
            }
            Logger.InfoFormat("Exiting {0}.ProgramBundlePackageComponentGet({1})", svcName, oReq.PgmBdlPckComponent.ID);
            return value;
        }

        public static List<lnkofferuomarticleprice> GetOffersfromUOMArticleNew(string ArticleId, string PkgKenanID, string contractKenanIDs, string MOCStatus, int AcctCtg, int MktCode)
        {
            Logger.InfoFormat("Entering {0}.GetOffersfromUOMArticleNew{1})", svcName, ArticleId);
            List<lnkofferuomarticleprice> objUOMArticleDetails = null;
            //List<lnkofferuomarticleprice> objUOMArticleDetails1 = new List<lnkofferuomarticleprice>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				#region old code using sP

				objUOMArticleDetails = ctx.Database.SqlQuery<lnkofferuomarticleprice>(
					"USP_GetOfferUomArticlePrice_New_DF @ArticleID,@PkgKenanID,@contractKenanIDs,@MOCStatus,@AcctCategory,@MarketCode",
					new SqlParameter("@ArticleId", ArticleId),
					new SqlParameter("@PkgKenanID", PkgKenanID),
					new SqlParameter("@contractKenanIDs", contractKenanIDs),
					new SqlParameter("@MOCStatus", MOCStatus),
					new SqlParameter("@AcctCategory", AcctCtg),
					new SqlParameter("@MarketCode", MktCode)
					).ToList();

                //string[] list = new string[objUOMArticleDetails.Count];
                //foreach (lnkofferuomarticleprice lnk in objUOMArticleDetails)
                //{
                //    if (!list.Contains(lnk.OfferName))
                //    {
                //        objUOMArticleDetails1.Add(lnk);
                //        //added by Chodey
                //        list[objUOMArticleDetails1.Count - 1] = lnk.OfferName;
                //    }
				//}
				#endregion
            }
            Logger.InfoFormat("Entering {0}.GetOffersfromUOMArticleNew{1})", svcName, ArticleId);
            return objUOMArticleDetails.GroupBy(x => new { x.OfferName }, (x, y) => y.First()).OrderBy(a => a.OfferName).ToList();
        }

        public static Dictionary<string, string> PkgCompContractGet()
        {
            Logger.InfoFormat("Entering {0}.PkgCompContractGet({1})", svcName);
            var voiceContractType = Properties.Settings.Default.ComponentType_VoiceContract;
            var dataContractType = Properties.Settings.Default.ComponentType_DataContract;
            var dic = new Dictionary<string, string>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var voiceContractTypeID = ctx.ComponentTypes.Where(a => a.Code == voiceContractType).SingleOrDefault().ID;
                var dataContractTypeID = ctx.ComponentTypes.Where(a => a.Code == dataContractType).SingleOrDefault().ID;

                var query = from pbpc in ctx.PgmBdlPckComponents

                            join comp in ctx.Components on new { linkType = pbpc.LinkType, compID = pbpc.ChildID, active = pbpc.Active }
                                equals new { linkType = "PC", compID = comp.ID, active = true } into comps
                            from results in comps.DefaultIfEmpty()

                            join package in ctx.Packages on new { linkType = pbpc.LinkType, compID = pbpc.ParentID, active = pbpc.Active }
                                equals new { linkType = "PC", compID = package.ID, active = true } into packages
                            from results1 in packages.DefaultIfEmpty()
                            where (results.ComponentTypeID == voiceContractTypeID ||
                                results.ComponentTypeID == dataContractTypeID)
                            select new
                            {
                                id = pbpc.ID,
                                name = results1.Name + " " + results.Name
                            };

                var pbpcList = query.ToList();
                foreach (var item in pbpcList)
                {
                    dic.Add(item.id.ToString(), item.name);
                }
            }

            Logger.InfoFormat("Exiting {0}.ProgramBundlePackageComponentGet({1})", svcName, dic.Count());
            return dic;
        }

        public static Dictionary<string, string> BdlPkgGet()
        {
            Logger.InfoFormat("Entering {0}.BdlPkgGet({1})", svcName);
            var dic = new Dictionary<string, string>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = from pbpc in ctx.PgmBdlPckComponents

                            join package in ctx.Packages on new { linkType = pbpc.LinkType, pkgID = pbpc.ChildID, active = pbpc.Active }
                                equals new { linkType = "BP", pkgID = package.ID, active = true } into packages
                            from results1 in packages.DefaultIfEmpty()

                            join bundle in ctx.Bundles on new { linkType = pbpc.LinkType, pkgID = pbpc.ParentID, active = pbpc.Active }
                                equals new { linkType = "BP", pkgID = bundle.ID, active = true } into bundles
                            from results in bundles.DefaultIfEmpty()
                            where (pbpc.LinkType == "BP" && pbpc.PlanType == "CP")
                            select new
                            {
                                id = pbpc.ID,
                                name = results.Name + " " + results1.Name
                            };

                var pbpcList = query.ToList();
                foreach (var item in pbpcList)
                {
                    if (item.name != null)
                        dic.Add(item.id.ToString(), item.name);
                }
            }

            Logger.InfoFormat("Exiting {0}.BdlPkgGet({1})", svcName, dic.Count());
            return dic;
        }

        public static List<MISMMandatoryComponents> MISMMandatoryComponentGet()
        {
            Logger.InfoFormat("Entering {0}.MISMMandatoryComponentGet()", svcName);
            List<MISMMandatoryComponents> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.MISMMandatoryComponents.ToList();
            }

            Logger.InfoFormat("Exiting {0}.MISMMandatoryComponentGet(): {2}", svcName, value.Count());
            return value;
        }

        #endregion

        #region ModelGroupModel

        public static int ModelGroupModelCreate(ModelGroupModel oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelCreate({1})", svcName, (oReq != null) ? oReq.Name.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.ModelGroupModels.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.ModelGroupModelCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name.ToString2() : "NULL", value);
            return value;
        }
        public static int ModelGroupModelUpdate(ModelGroupModel oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelUpdate({1})", svcName, (oReq != null) ? oReq.Name.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry<ModelGroupModel>(oReq).State = EntityState.Modified;
                ModelGroupModel ModelGroupModel = ctx.ModelGroupModels.First(a => a.ID == oReq.ID);
                ModelGroupModel.Active = oReq.Active;
                ModelGroupModel.NeedRecovery = oReq.NeedRecovery;
                ModelGroupModel.RetailPrice = oReq.RetailPrice;
                ModelGroupModel.DealerPrice = oReq.DealerPrice;
                ModelGroupModel.Code = oReq.Code;
                ModelGroupModel.EndDate = oReq.EndDate;
                ModelGroupModel.FulfillmentModeID = oReq.FulfillmentModeID;
                ModelGroupModel.LastAccessID = oReq.LastAccessID;
                ModelGroupModel.LastUpdateDT = oReq.LastUpdateDT;
                ModelGroupModel.ModelID = oReq.ModelID;
                ModelGroupModel.Name = oReq.Name;
                ModelGroupModel.StartDate = oReq.StartDate;
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.ModelGroupModelUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name.ToString2() : "NULL", value);
            return value;
        }
        public static List<int> ModelGroupModelFind(ModelGroupModelFind oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ModelGroupModels.Where(a => a.ID != 0);

                if (oReq.ModelGroupModel.ID != 0)
                    query = query.Where(a => a.ID == oReq.ModelGroupModel.ID);

                if (oReq.ModelGroupModel.ModelID != 0)
                    query = query.Where(a => a.ModelID == oReq.ModelGroupModel.ModelID);

                if (oReq.ModelGroupModel.ModelGroupID != 0)
                    query = query.Where(a => a.ModelGroupID == oReq.ModelGroupModel.ModelGroupID);

                //** Updated by Chetan on 20/12/2013
                if (!ReferenceEquals(oReq.ModelGroupIDs, null))
                {
                    if (oReq.ModelGroupIDs.Count() > 0)
                        query = query.Where(a => oReq.ModelGroupIDs.Contains(a.ModelGroupID));
                }

                if (oReq.ModelGroupModel.FulfillmentModeID != 0)
                    query = query.Where(a => a.FulfillmentModeID == oReq.ModelGroupModel.FulfillmentModeID);

                if (!string.IsNullOrEmpty(oReq.ModelGroupModel.Code))
                    query = query.Where(a => a.Code == oReq.ModelGroupModel.Code);

                if (!string.IsNullOrEmpty(oReq.ModelGroupModel.Name))
                    query = query.Where(a => a.Name.Contains(oReq.ModelGroupModel.Name));

                if (oReq.StartDateFrom.HasValue)
                    query = query.Where(a => a.StartDate.Date >= oReq.StartDateFrom.Value.Date);

                if (oReq.StartDateTo.HasValue)
                    query = query.Where(a => a.StartDate.Date <= oReq.StartDateTo.Value.Date);

                if (oReq.EndDateFrom.HasValue)
                    query = query.Where(a => a.EndDate.Value.Date >= oReq.EndDateFrom.Value.Date);

                if (oReq.EndDateTo.HasValue)
                    query = query.Where(a => a.EndDate.Value.Date <= oReq.EndDateTo.Value.Date);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.RecoveryNeeded.HasValue)
                    query = query.Where(a => a.NeedRecovery == oReq.RecoveryNeeded.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ModelGroupModelFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<ModelGroupModel> ModelGroupModelGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<ModelGroupModel> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ModelGroupModels.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ModelGroupModelGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }
        //public static List<ModelGroupModel> ModelGroupModelGetByMdlGrpPkgIDs(List<int> mdlGrpPkgIDs)
        //{
        //    Logger.InfoFormat("Entering {0}.ModelGroupModelGetByMdlGrpPkgIDs({1})", svcName, (mdlGrpPkgIDs != null) ? mdlGrpPkgIDs[0] : 0);
        //    List<ModelGroupModel> value = null;

        //    using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
        //    {
        //        value = ctx.ModelGroupModels.Where(a => mdlGrpPkgIDs.Contains(a.ModelGroupID) && a.Active == true).ToList();
        //    }
        //    Logger.InfoFormat("Exiting {0}.ModelGroupModelGetByMdlGrpPkgIDs({1}): {2}", svcName, (mdlGrpPkgIDs != null) ? mdlGrpPkgIDs[0] : 0, value.Count());
        //    return value;
        //}

        #endregion

        #region Category

        public static int CategoryCreate(Category oReq)
        {
            Logger.InfoFormat("Entering {0}.CategoryCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Categories.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.CategoryCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int CategoryUpdate(Category oReq)
        {
            Logger.InfoFormat("Entering {0}.CategoryUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                Category category = ctx.Categories.First(a => a.ID == oReq.ID);
                category.Active = oReq.Active;
                category.Code = oReq.Code;
                category.Description = oReq.Description;
                category.LastAccessID = oReq.LastAccessID;
                category.LastUpdateDT = oReq.LastUpdateDT;
                category.Name = oReq.Name;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.CategoryUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> CategoryFind(CategoryFind oReq)
        {
            Logger.InfoFormat("Entering {0}.CategoryFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Categories.Where(a => a.ID != 0);

                if (oReq.Category.ID != 0)
                    query = query.Where(a => a.ID == oReq.Category.ID);

                if (!string.IsNullOrEmpty(oReq.Category.Code))
                    query = query.Where(a => a.Code == oReq.Category.Code);

                if (!string.IsNullOrEmpty(oReq.Category.Name))
                    query = query.Where((a) => a.Name.Contains(oReq.Category.Name));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CategoryFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<Category> CategoryGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.CategoryGet({1})", svcName, (IDs.Count() > 0) ? IDs[0] : 0);
            List<Category> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Categories.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CategoryGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<Category> CategoriesList()
        {
            Logger.InfoFormat("Entering {0}.CategoriesList({1})", svcName, "");
            List<Category> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Categories.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CategoriesList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region Brand

        public static int BrandCreate(Brand oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Brands.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.BrandCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int BrandUpdate(Brand oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                Brand brand = ctx.Brands.First(a => a.ID == oReq.ID);
                brand.Active = oReq.Active;
                brand.CategoryID = oReq.CategoryID;
                brand.Code = oReq.Code;
                brand.LastAccessID = oReq.LastAccessID;
                brand.LastUpdateDT = oReq.LastUpdateDT;
                brand.Name = oReq.Name;
                brand.Description = oReq.Description;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.BrandUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> BrandFind(BrandFind oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Brands.Where(a => a.ID != 0);

                if (oReq.Brand.ID != 0)
                    query = query.Where(a => a.ID == oReq.Brand.ID);

                if (oReq.Brand.CategoryID != 0)
                    query = query.Where(a => a.CategoryID == oReq.Brand.CategoryID);

                if (!string.IsNullOrEmpty(oReq.Brand.Code))
                    query = query.Where(a => a.Code == oReq.Brand.Code);

                if (!string.IsNullOrEmpty(oReq.Brand.Name))
                    query = query.Where((a) => a.Name.Contains(oReq.Brand.Name));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.BrandFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<int> BrandFindSmart(BrandFindSmart oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.BrandsSmart.Where(a => a.ID != 0);

                if (oReq.Brand.ID != 0)
                    query = query.Where(a => a.ID == oReq.Brand.ID);

                if (oReq.Brand.CategoryID != 0)
                    query = query.Where(a => a.CategoryID == oReq.Brand.CategoryID);

                if (!string.IsNullOrEmpty(oReq.Brand.Code))
                    query = query.Where(a => a.Code == oReq.Brand.Code);

                if (!string.IsNullOrEmpty(oReq.Brand.Name))
                    query = query.Where((a) => a.Name.Contains(oReq.Brand.Name));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.BrandFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<Brand> BrandGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.BrandGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
            List<Brand> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Brands.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.BrandGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
            return value;
        }
        public static List<BrandSmart> BrandGetSmart(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.BrandGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
            List<BrandSmart> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.BrandsSmart.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.BrandGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
            return value;
        }

        //Ranjeeth: for Performance Enhancements
        public static List<Brand> BrandsList()
        {
            Logger.InfoFormat("Entering {0}.BrandsList({1})", svcName, "");
            List<Brand> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Brands.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.BrandGBrandsListet({1}): {2}", svcName, "", value.Count());
            return value;
        }

        public static List<Brand> BrandGetCRP(string type)
        {

            //Logger.InfoFormat("Entering {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            List<Brand> value = null;
            var NewID = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<Brand>("sp_GetBrandsExtContracts @type", new SqlParameter("@type", type)).ToList();

            }
            // Logger.InfoFormat("Exiting {0}.UserInfoLogCreate({1}): {2}", svcName, inputparams.Username);
            return value;
        }


        public static List<Model> ModelCRPGet(int brandid, string type)
        {

            //Logger.InfoFormat("Entering {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            List<Model> value = null;


            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<Model>("sp_ModelsCrp @brandid,@type", new SqlParameter("@brandid", brandid), new SqlParameter("@type", type)).ToList();

            }
            // Logger.InfoFormat("Exiting {0}.UserInfoLogCreate({1}): {2}", svcName, inputparams.Username);
            return value;
        }

        public static List<ModelGroup> PackagesCRPGet(int Modelid, string type)
        {

            //Logger.InfoFormat("Entering {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            List<ModelGroup> value = null;


            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<ModelGroup>("sp_ModelGroupsCrpGet @Modelid,@type", new SqlParameter("@Modelid", Modelid), new SqlParameter("@type", type)).ToList();

            }
            // Logger.InfoFormat("Exiting {0}.UserInfoLogCreate({1}): {2}", svcName, inputparams.Username);
            return value;
        }

        #region AddContract

        public static List<Brand> GetBrandsForPackage(string packageId, string planType)
        {
            List<Brand> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<Brand>("Usp_GetBrandsForPackage @package,@type", new SqlParameter("@package", packageId), new SqlParameter("@type", planType)).ToList();
            }
            return value;
        }

        public static List<Model> GetDevicesForPackage(int brandid, string packageId, string planType)
        {
            List<Model> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<Model>("Usp_GetDevicesForPackage @brandid,@package,@type", new SqlParameter("@brandid", brandid), new SqlParameter("@package", packageId), new SqlParameter("@type", planType)).ToList();
            }
            return value;
        }

        public static List<BrandArticle> GetDeviceListForPackage(string packageId, string mktCode, string acctCategory)
        {
            List<BrandArticle> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<BrandArticle>("Usp_GetDeviceListForPackage @package,@mktcode,@acctcategory", new SqlParameter("@package", packageId), new SqlParameter("@mktcode", mktCode), new SqlParameter("@acctcategory", acctCategory)).ToList();
            }
            return value;
        }

        public static List<Model> GetModelsForExtenstionForPackage(int brandid, string packageId, string planType)
        {
            List<Model> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<Model>("Usp_GetDevicesForExtensionForPackage @brandid,@package,@type ", new SqlParameter("@brandid", brandid), new SqlParameter("@package", packageId), new SqlParameter("@type", planType)).ToList();
            }
            return value;
        }

        #endregion AddContract

        #region Devices for supplines

        public static List<Brand> GetBrandsForSupplines()
        {
            //Logger.InfoFormat("Entering {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            List<Brand> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<Brand>("Usp_GetBrandsForSupLines").ToList();

            }
            // Logger.InfoFormat("Exiting {0}.UserInfoLogCreate({1}): {2}", svcName, inputparams.Username);
            return value;
        }

        public static List<Model> GetDevicesForSupplines(int brandid)
        {

            //Logger.InfoFormat("Entering {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            List<Model> value = null;


            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<Model>("Usp_GetDevicesForSuppLines @brandid", new SqlParameter("@brandid", brandid)).ToList();

            }
            // Logger.InfoFormat("Exiting {0}.UserInfoLogCreate({1}): {2}", svcName, inputparams.Username);
            return value;
        }

        public static List<Model> GetModelsForExtenstionForSupplines(int brandid)
        {
            List<Model> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //value = ctx.RegSmartComponents.Where(a => a.RegID == regId).ToList();
                value = ctx.Database.SqlQuery<Model>("Usp_GetDevicesForExtensionForSuppLines @brandid ", new SqlParameter("@brandid", brandid)).ToList();
            }
            return value;
        }

        public static List<int> GetExtendedPackagesForSupplines(int Modelid)
        {
            List<int> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_GetExtendedPackageIdsForSupplines @ModelId", new SqlParameter("@ModelId", Modelid)).ToList();
            }
            return value;
        }

        public static List<ModelGroup> GetModelGroupsForSupplines(int Modelid)
        {

            //Logger.InfoFormat("Entering {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            List<ModelGroup> value = null;


            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<ModelGroup>("Usp_GetModelGroupsForSupplines @Modelid", new SqlParameter("@Modelid", Modelid)).ToList();

            }
            // Logger.InfoFormat("Exiting {0}.UserInfoLogCreate({1}): {2}", svcName, inputparams.Username);
            return value;
        }


        #endregion


        //Logger.InfoFormat("Entering {0}.BrandGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
        //List<Brand> value = null;

        //using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
        //{
        //    value = ctx.Brands.Where(a => IDs.Contains(a.ID)).ToList();
        //}
        //Logger.InfoFormat("Exiting {0}.BrandGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
        //return value;


        #endregion

        #region Added by VLT on 09 Apr 2013
        public static List<SimModels> SimModelTypeGet()
        {
            Logger.InfoFormat("Entering {0}.CardSimModelTypeGet({1})", svcName, "");
            List<SimModels> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.SimModels.Where(a => a.isActive == true).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardSimModelTypeGet({1}): {2}", svcName, "", value.Count());
            return value;
        }

        public static SimModels GetSimModelDetails(int id, int articalId)
        {
            Logger.InfoFormat("Entering {0}.GetSimModelDetails({1})", svcName, "");
            SimModels value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.SimModels.Where(a => a.isActive == true && a.ID == id && a.ArticleId == articalId).ToList().FirstOrDefault();
            }
            Logger.InfoFormat("Exiting {0}.GetSimModelDetails({1}): {2}", svcName, "", value.ArticleId);
            return value;
        }
        #endregion

        #region Added by  VLT on 17 June 2013
        public static List<SimReplacementReasons> SimReplacementReasonTypeGet(bool accType)
        {
            Logger.InfoFormat("Entering {0}.CardSimModelTypeGet({1})", svcName, "");
            List<SimReplacementReasons> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                if (accType)
                    value = ctx.SimReplacementReasonsDetails.Where(e => e.isPostPaid == accType && e.ISACTIVE == true).ToList();
                else
                    value = ctx.SimReplacementReasonsDetails.Where(e => e.isPostPaid == accType && e.ISACTIVE == true).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CardSimModelTypeGet({1}): {2}", svcName, "", value.Count());
            return value;
        }

        public static SimReplacementReasons GetSimReplacementReasonDetails(int reasonId)
        {
            Logger.InfoFormat("Entering {0}.GetExtraTenDetails({1})", svcName, reasonId);
            SimReplacementReasons value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.SimReplacementReasonsDetails.Where(e => e.Id == reasonId).ToList().FirstOrDefault();
            }
            Logger.InfoFormat("Exiting {0}.GetExtraTenDetails({1})", svcName, reasonId);
            return value;

        }
        #endregion

        #region Model

        public static int ModelCreate(Model oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Models.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.ModelCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int ModelUpdate(Model oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                Model model = ctx.Models.First(a => a.ID == oReq.ID);
                model.Active = oReq.Active;
                model.BrandID = oReq.BrandID;
                model.CanReplace = oReq.CanReplace;
                model.CanReserve = oReq.CanReserve;
                model.CanReturn = oReq.CanReturn;
                model.CanScan = oReq.CanScan;
                model.ChargeTo = oReq.ChargeTo;
                model.ChargeType = oReq.ChargeType;
                model.RetailPrice = oReq.RetailPrice;
                model.Code = oReq.Code;
                model.DistOrgID = oReq.DistOrgID;
                model.FulfillerOrgID = oReq.FulfillerOrgID;
                model.HasIMEI = oReq.HasIMEI;
                model.HasPIN = oReq.HasPIN;
                model.HasSerial = oReq.HasSerial;
                model.IsMandatory = oReq.IsMandatory;
                model.IsSerialized = oReq.IsSerialized;
                model.LastAccessID = oReq.LastAccessID;
                model.LastUpdateDT = oReq.LastUpdateDT;
                model.Name = oReq.Name;
                model.NeedToDeclare = oReq.NeedToDeclare;
                model.RepairOrgID = oReq.RepairOrgID;
                //model.RMAGrpID = oReq.RMAGrpID;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.ModelUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> ModelFind(ModelFind oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                if(true == oReq.IsHero)
                {
                    var query = ctx.Models.Where(a => a.ID != 0);
                    
                    if (oReq.IsHero.HasValue)
                        query = query.Where(a => a.IsHero == oReq.IsHero.Value);

                    if (oReq.Active.HasValue)
                        query = query.Where(a => a.Active == oReq.Active.Value);

                    if (oReq.IsSerialized.HasValue)
                        query = query.Where(a => a.IsSerialized == oReq.IsSerialized.Value);

                    if (oReq.PageSize > 0)
                        query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                    value = query.Select((a) => a.ID).ToList();
                }
                else if (oReq.IsSmart == null)
                {
                    var query = ctx.Models.Where(a => a.ID != 0);

                    if (!ReferenceEquals(oReq.Model, null))
                    {
                        if (oReq.Model.ID != 0)
                            query = query.Where(a => a.ID == oReq.Model.ID);

                        if (!string.IsNullOrEmpty(oReq.Model.Code))
                            query = query.Where(a => a.Code == oReq.Model.Code);

                        if (!string.IsNullOrEmpty(oReq.Model.Name))
                            query = query.Where((a) => a.Name.Contains(oReq.Model.Name));

                        if (oReq.Model.BrandID.ToInt() != 0)
                            query = query.Where(a => a.BrandID == oReq.Model.BrandID);
                    }
                    
                    if (oReq.Active.HasValue)
                        query = query.Where(a => a.Active == oReq.Active.Value);

                    if (oReq.IsSerialized.HasValue)
                        query = query.Where(a => a.IsSerialized == oReq.IsSerialized.Value);

                    if (oReq.PageSize > 0)
                        query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                    value = query.Select((a) => a.ID).ToList();
                }
                else if (oReq.IsSmart != null && oReq.IsSmart == true)
                {
                    var query = ctx.ModelsSmart.Where(a => a.ID != 0);

                    if (!ReferenceEquals(oReq.Model, null))
                    {
                        if (oReq.Model.ID != 0)
                            query = query.Where(a => a.ID == oReq.Model.ID);

                        if (!string.IsNullOrEmpty(oReq.Model.Code))
                            query = query.Where(a => a.Code == oReq.Model.Code);

                        if (!string.IsNullOrEmpty(oReq.Model.Name))
                            query = query.Where((a) => a.Name.Contains(oReq.Model.Name));

                        if (oReq.Model.BrandID.ToInt() != 0)
                            query = query.Where(a => a.BrandID == oReq.Model.BrandID);
                    }
                    
                    if (oReq.Active.HasValue)
                        query = query.Where(a => a.Active == oReq.Active.Value);

                    if (oReq.IsSerialized.HasValue)
                        query = query.Where(a => a.IsSerialized == oReq.IsSerialized.Value);

                    if (oReq.PageSize > 0)
                        query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                    value = query.Select((a) => a.ID).ToList();
                }


            }
            Logger.InfoFormat("Exiting {0}.ModelFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<int> ModelFindSmart(ModelFindSmart oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                if (oReq.IsSmart == null)
                {
                    var query = ctx.Models.Where(a => a.ID != 0);

                    if (oReq.Model.ID != 0)
                        query = query.Where(a => a.ID == oReq.Model.ID);

                    if (!string.IsNullOrEmpty(oReq.Model.Code))
                        query = query.Where(a => a.Code == oReq.Model.Code);

                    if (!string.IsNullOrEmpty(oReq.Model.Name))
                        query = query.Where((a) => a.Name.Contains(oReq.Model.Name));

                    if (oReq.Model.BrandID.ToInt() != 0)
                        query = query.Where(a => a.BrandID == oReq.Model.BrandID);

                    if (oReq.Active.HasValue)
                        query = query.Where(a => a.Active == oReq.Active.Value);

                    if (oReq.IsSerialized.HasValue)
                        query = query.Where(a => a.IsSerialized == oReq.IsSerialized.Value);

                    if (oReq.PageSize > 0)
                        query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                    value = query.Select((a) => a.ID).ToList();
                }
                else if (oReq.IsSmart != null && oReq.IsSmart == true)
                {
                    var query = ctx.ModelsSmart.Where(a => a.ID != 0);

                    if (oReq.Model.ID != 0)
                        query = query.Where(a => a.ID == oReq.Model.ID);

                    if (!string.IsNullOrEmpty(oReq.Model.Code))
                        query = query.Where(a => a.Code == oReq.Model.Code);

                    if (!string.IsNullOrEmpty(oReq.Model.Name))
                        query = query.Where((a) => a.Name.Contains(oReq.Model.Name));

                    if (oReq.Model.BrandID.ToInt() != 0)
                        query = query.Where(a => a.BrandID == oReq.Model.BrandID);

                    if (oReq.Active.HasValue)
                        query = query.Where(a => a.Active == oReq.Active.Value);

                    if (oReq.IsSerialized.HasValue)
                        query = query.Where(a => a.IsSerialized == oReq.IsSerialized.Value);

                    if (oReq.PageSize > 0)
                        query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                    value = query.Select((a) => a.ID).ToList();
                }


            }
            Logger.InfoFormat("Exiting {0}.ModelFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<Model> ModelGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.ModelGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<Model> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Models.Where(a => IDs.Contains(a.ID) && (a.Active == true)).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.ModelGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<ModelSmart> ModelGetSmart(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.ModelGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<ModelSmart> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ModelsSmart.Where(a => IDs.Contains(a.ID) && (a.Active == true)).ToList();
            }
            //Logger.InfoFormat("Exiting {0}.ModelGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<Model> ModelsList()
        {
            List<Model> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Models.Where(a => a.ID > 0 && (a.Active == true)).ToList();
            }
            return value;
        }
        public static List<ModelSmart> ModelsListSmart()
        {
            List<ModelSmart> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ModelsSmart.Where(a => a.ID > 0 && (a.Active == true)).ToList();
            }
            return value;
        }

        #endregion

        #region ModelImage

        public static int ModelImageCreate(ModelImage oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelImageCreate({1})", svcName, (oReq != null) ? oReq.ImagePath : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.ModelImages.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.ModelImageCreate({1}): {2}", svcName, (oReq != null) ? oReq.ImagePath : "NULL", value);
            return value;
        }
        public static int ModelImageUpdate(ModelImage oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelImageUpdate({1})", svcName, (oReq != null) ? oReq.ImagePath : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                ModelImage ModelImage = ctx.ModelImages.First(a => a.ID == oReq.ID);
                ModelImage.Active = oReq.Active;
                ModelImage.ModelID = oReq.ModelID;
                ModelImage.ColourID = oReq.ColourID;
                ModelImage.LastAccessID = oReq.LastAccessID;
                ModelImage.LastUpdateDT = oReq.LastUpdateDT;
                ModelImage.ImagePath = oReq.ImagePath;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.ModelImageUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ImagePath : "NULL", value);
            return value;
        }
        public static List<int> ModelImageFind(ModelImageFind oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelImageFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ModelImages.Where(a => a.ID != 0);

                if (oReq.ModelImage.ID != 0)
                    query = query.Where(a => a.ID == oReq.ModelImage.ID);

                if (oReq.ModelIDs.Count() > 0)
                    query = query.Where(a => oReq.ModelIDs.Contains(a.ModelID));

                if (oReq.ModelImage.ModelID != 0)
                    query = query.Where(a => a.ModelID == oReq.ModelImage.ModelID);

                if (oReq.ModelImage.ColourID.HasValue)
                    query = query.Where(a => a.ColourID == oReq.ModelImage.ColourID);

                if (!string.IsNullOrEmpty(oReq.ModelImage.ImagePath))
                    query = query.Where(a => a.ImagePath == oReq.ModelImage.ImagePath);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ModelImageFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<ModelImage> ModelImageGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.ModelImageGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<ModelImage> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ModelImages.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ModelImageGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }

        #endregion

        #region Colour

        public static int ColourCreate(Colour oReq)
        {
            Logger.InfoFormat("Entering {0}.ColourCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Colours.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.ColourCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int ColourUpdate(Colour oReq)
        {
            Logger.InfoFormat("Entering {0}.ColourUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                Colour Colour = ctx.Colours.First(a => a.ID == oReq.ID);
                Colour.Active = oReq.Active;
                Colour.Code = oReq.Code;
                Colour.Description = oReq.Description;
                Colour.ModelID = oReq.ModelID;
                Colour.LastAccessID = oReq.LastAccessID;
                Colour.LastUpdateDT = oReq.LastUpdateDT;
                Colour.Name = oReq.Name;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.ColourUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> ColourFind(ColourFind oReq)
        {
            Logger.InfoFormat("Entering {0}.ColourFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Colours.Where(a => a.ID != 0);

                if (oReq.Colour.ID.ToInt() != 0)
                    query = query.Where(a => a.ID == oReq.Colour.ID);

                if (oReq.Colour.ModelID.ToInt() != 0)
                    query = query.Where(a => a.ModelID == oReq.Colour.ModelID);

                if (!string.IsNullOrEmpty(oReq.Colour.Code))
                    query = query.Where(a => a.Code == oReq.Colour.Code);

                if (!string.IsNullOrEmpty(oReq.Colour.Name))
                    query = query.Where((a) => a.Name.Contains(oReq.Colour.Name));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ColourFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<Colour> ColourGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.ColourGet({1})", svcName, (IDs.Count() > 0) ? IDs[0] : 0);
            List<Colour> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Colours.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ColourGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<Colour> ColoursList()
        {
            Logger.InfoFormat("Entering {0}.ColoursList({1})", svcName, "");
            List<Colour> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Colours.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ColoursList({1}): {2}", svcName, "", value.Count());
            return value;
        }
        #endregion

        #region Model Part No

        public static int ModelPartNoCreate(ModelPartNo oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelPartNoCreate({1})", svcName, (oReq != null) ? oReq.PartNo : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.ModelPartNos.Add(oReq);
                ctx.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.ModelPartNoCreate({1}): {2}", svcName, (oReq != null) ? oReq.PartNo : "NULL", value);
            return value;
        }
        public static int ModelPartNoUpdate(ModelPartNo oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelPartNoUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    ModelPartNo mpn = ctx.ModelPartNos.First(a => a.ID == oReq.ID);
                    mpn.Active = oReq.Active;
                    mpn.ColourID = oReq.ColourID;
                    mpn.ModelID = oReq.ModelID;
                    mpn.PartNo = oReq.PartNo;
                    //mpn.ItemTypeID = oReq.ItemTypeID;
                    mpn.LastAccessID = oReq.LastAccessID;
                    mpn.LastUpdateDT = oReq.LastUpdateDT;
                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.ModelPartNoUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static List<int> ModelPartNoFind(ModelPartNoFind oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelPartNoFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ModelPartNos.Where(a => a.ID != 0);

                if (oReq.ModelPartNo.ID != 0)
                    query = query.Where(a => a.ID == oReq.ModelPartNo.ID);

                if (oReq.ModelPartNo.ModelID != 0)
                    query = query.Where(a => a.ModelID == oReq.ModelPartNo.ModelID);

                if (oReq.ModelPartNo.ColourID != 0)
                    query = query.Where(a => a.ColourID == oReq.ModelPartNo.ColourID);

                //if (!(typeID == 0))
                //    query = query.Where(a => a.ItemTypeID == typeID);

                if (!string.IsNullOrEmpty(oReq.ModelPartNo.PartNo))
                    query = query.Where(a => a.PartNo == oReq.ModelPartNo.PartNo);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ColourID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }


            Logger.InfoFormat("Exiting {0}.ModelPartNoFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<ModelPartNo> ModelPartNoGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.ModelPartNoGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<ModelPartNo> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ModelPartNos.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.ModelPartNoGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        #endregion

        #region ItemPrice

        public static int ItemPriceCreate(ItemPrice oReq)
        {
            Logger.InfoFormat("Entering {0}.ItemPriceCreate({1})", svcName, (oReq != null) ? oReq.BdlPkgID.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.ItemPrices.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.ItemPriceCreate({1}): {2}", svcName, (oReq != null) ? oReq.BdlPkgID.ToString() : "NULL", value);
            return value;
        }
        public static int ItemPriceUpdate(ItemPrice oReq)
        {
            Logger.InfoFormat("Entering {0}.ItemPriceUpdate({1})", svcName, (oReq != null) ? oReq.BdlPkgID.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                ItemPrice ItemPrice = ctx.ItemPrices.First(a => a.ID == oReq.ID);
                ItemPrice.Active = oReq.Active;
                ItemPrice.BdlPkgID = oReq.BdlPkgID;
                ItemPrice.PkgCompID = oReq.PkgCompID;
                ItemPrice.ModelID = oReq.ModelID;
                ItemPrice.LastAccessID = oReq.LastAccessID;
                ItemPrice.LastUpdateDT = oReq.LastUpdateDT;
                ItemPrice.Price = oReq.Price;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.ItemPriceUpdate({1}): {2}", svcName, (oReq != null) ? oReq.BdlPkgID.ToString() : "NULL", value);
            return value;
        }
        public static List<int> ItemPriceFind(ItemPriceFind oReq)
        {
            Logger.InfoFormat("Entering {0}.ItemPriceFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ItemPrices.Where(a => a.ID != 0);

                if (oReq.ItemPrice.ID != 0)
                    query = query.Where(a => a.ID == oReq.ItemPrice.ID);

                if (oReq.ItemPrice.BdlPkgID != 0)
                    query = query.Where(a => a.BdlPkgID == oReq.ItemPrice.BdlPkgID);

                if (oReq.ItemPrice.PkgCompID != 0)
                    query = query.Where(a => a.PkgCompID == oReq.ItemPrice.PkgCompID);

                if (oReq.ItemPrice.ModelID != 0)
                    query = query.Where(a => a.ModelID == oReq.ItemPrice.ModelID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ItemPriceFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        // ranjeeth for islandeviceCR : new method
        public static List<int> ItemPriceFindForMultipleContratsAndPlans(ItemPriceFind oReq)
        {
            Logger.InfoFormat("Entering {0}.ItemPriceFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");
            // PkgCompID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt(),
            //pkgDataID = Session["RegMobileReg_DataPkgID"].ToInt()
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ItemPrices.Where(a => a.ID != 0);

                if (oReq.ItemPrice.ID != 0)
                    query = query.Where(a => a.ID == oReq.ItemPrice.ID);

                if (oReq.ItemPrice.BdlPkgID != 0)
                    query = query.Where(a => a.BdlPkgID == oReq.ItemPrice.BdlPkgID);

                //if (oReq.ItemPrice.PkgCompID != 0)
                //    query = query.Where(a => a.PkgCompID == oReq.ItemPrice.PkgCompID);
                // ranjeeth for islandeviceCR

                if (oReq.ContractIds.Count > 0)
                    query = query.Where(a => oReq.ContractIds.Contains(a.PkgCompID));

                if (oReq.ItemPrice.ModelID != 0)
                    query = query.Where(a => a.ModelID == oReq.ItemPrice.ModelID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ItemPriceFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }

        public static List<ItemPrice> ItemPriceGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.ItemPriceGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<ItemPrice> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ItemPrices.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ItemPriceGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        #endregion

        //added by Deepika

        #region AdvDepositPrices

        public static List<AdvDepositPrice> AdvDepositPricesGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.AdvDepositPricesGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<AdvDepositPrice> value = null;
            List<AdvDepositPrice> tempvalue = null;
            //added by deepika 10 march
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                var query = ctx.AdvDepositPrices.Where(a => a.ID != 0);

                if (IDs[0] != 0)
                {
                    query = query.Where(a => IDs.Contains(a.modelId));
                    if (IDs[1] != 0)
                    {
                        query = query.Where(a => IDs.Contains(a.planId));
                        if (IDs[2] != 0)
                            query = query.Where(a => IDs.Contains(a.contractID));
                        if (IDs[3] != 0)
                            query = query.Where(a => IDs.Contains(a.dataPlanId));
                        // filter out model id = 0
                        query = query.Where(a => a.modelId != 0);
                    }
                }
                else
                {
                    query = query.Where(a => IDs.Contains(a.planId));
                    if (IDs[0] == 0)
                    {
                        query = query.Where(a => IDs.Contains(a.modelId));
                        if (IDs[2] != 0)
                            query = query.Where(a => IDs.Contains(a.contractID));
                        if (IDs[3] != 0)
                            query = query.Where(a => IDs.Contains(a.dataPlanId));
                    }

                }
                value = query.ToList();
            }
            Logger.InfoFormat("Exiting {0}.ItemPriceGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<AdvDepositPrice> AdvDepositPricesGetBySP(List<string> IDs)
        {
            Logger.InfoFormat("Entering {0}.AdvDepositPricesGet({1})", svcName, (IDs != null) ? IDs[0] : "0");
            List<AdvDepositPrice> value = null;
            string strmodelids = string.IsNullOrEmpty(IDs[0]) ? "0" : IDs[0].ToString2();
            string strPlanids = string.IsNullOrEmpty(IDs[1]) ? "0" : IDs[1].ToString2();
            string strContractIDs = string.IsNullOrEmpty(IDs[2]) ? "0" : IDs[2].ToString2();
            string strDataPlanIDs = string.IsNullOrEmpty(IDs[3]) ? "0" : IDs[3].ToString2();
            //added by deepika 10 march
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<AdvDepositPrice>("USP_ItemPriceGet @modelIDs,@planIDs,@contractIDs,@dataPlanIDs", new SqlParameter("@modelIDs", strmodelids), new SqlParameter("@planIDs", strPlanids), new SqlParameter("@contractIDs", strContractIDs), new SqlParameter("@dataPlanIDs", strDataPlanIDs)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ItemPriceGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : "0", value.Count());
            return value;
        }

        #endregion

        //end Deepika

        #region Property

        public static List<int> PropertyFind(PropertyFind oReq)
        {
            Logger.InfoFormat("Entering {0}.PropertyFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Properties.Where(a => a.ID != 0);

                if (oReq.Property.ID.ToInt() != 0)
                    query = query.Where(a => a.ID == oReq.Property.ID);

                if (!string.IsNullOrEmpty(oReq.Property.Code))
                    query = query.Where(a => a.Code == oReq.Property.Code);

                if (!string.IsNullOrEmpty(oReq.Property.Name))
                    query = query.Where((a) => a.Name.Contains(oReq.Property.Name));

                if (!string.IsNullOrEmpty(oReq.Property.Type))
                    query = query.Where((a) => a.Type.Contains(oReq.Property.Type));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PropertyFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<Property> PropertyGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.PropertyGet({1})", svcName, (IDs.Count() > 0) ? IDs[0] : 0);
            List<Property> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Properties.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PropertyGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<Property> PropertyList()
        {
            Logger.InfoFormat("Entering {0}.PropertyList({1})", svcName, "");
            List<Property> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Properties.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PropertyList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region PropertyValue

        public static List<int> PropertyValueFind(PropertyValueFind oReq)
        {
            Logger.InfoFormat("Entering {0}.PropertyValueFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.PropertyValues.Where(a => a.ID != 0);

                if (oReq.PropertyValue.ID.ToInt() != 0)
                    query = query.Where(a => a.ID == oReq.PropertyValue.ID);

                if (!string.IsNullOrEmpty(oReq.PropertyValue.Name))
                    query = query.Where((a) => a.Name.Contains(oReq.PropertyValue.Name));

                if (oReq.PropertyValue.PropertyID.ToInt() != 0)
                    query = query.Where(a => a.ID == oReq.PropertyValue.PropertyID);

                if (oReq.PropertyIDs.Count() > 0)
                    query = query.Where(a => oReq.PropertyIDs.Contains(a.PropertyID));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PropertyValueFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<PropertyValue> PropertyValueGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.PropertyValueGet({1})", svcName, (IDs.Count() > 0) ? IDs[0] : 0);
            List<PropertyValue> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PropertyValues.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PropertyValueGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }

        #endregion

        #region PlanProperty

        public static List<int> PlanPropertyFind(PlanPropertyFind oReq)
        {
            Logger.InfoFormat("Entering {0}.PlanPropertyFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.PlanProperties.Where(a => a.ID != 0);

                if (oReq.PlanProperty.ID.ToInt() != 0)
                    query = query.Where(a => a.ID == oReq.PlanProperty.ID);

                if (oReq.PlanProperty.BdlPkgID.ToInt() != 0)
                    query = query.Where(a => a.BdlPkgID == oReq.PlanProperty.BdlPkgID);

                if (oReq.PlanProperty.VoiceUsage.ToInt() != 0)
                    query = query.Where(a => a.VoiceUsage >= oReq.PlanProperty.VoiceUsage);

                if (oReq.PlanProperty.DataUsage.ToInt() != 0)
                    query = query.Where(a => a.DataUsage >= oReq.PlanProperty.DataUsage);

                if (oReq.PlanProperty.SMSUsage.ToInt() != 0)
                    query = query.Where(a => a.SMSUsage >= oReq.PlanProperty.SMSUsage);

                if (oReq.PlanProperty.Price.ToInt() != 0)
                    query = query.Where(a => a.Price >= oReq.PlanProperty.Price);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PlanPropertyFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<PlanProperty> PlanPropertyGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.PlanPropertyGet({1})", svcName, (IDs.Count() > 0) ? IDs[0] : 0);
            List<PlanProperty> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PlanProperties.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }

        #endregion

        #region DeviceProperty

        public static List<int> DevicePropertyFind(DevicePropertyFind oReq)
        {
            Logger.InfoFormat("Entering {0}.DevicePropertyFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.DeviceProperties.Where(a => a.ID != 0);

                if (oReq.DeviceProperty.ID.ToInt() != 0)
                    query = query.Where(a => a.ID == oReq.DeviceProperty.ID);

                //if (oReq.DeviceProperty.ModelID.ToInt() != 0)
                //    query = query.Where(a => a.ModelID == oReq.DeviceProperty.ModelID);

                //if (oReq.DeviceProperty.BrandID.ToInt() != 0)
                //    query = query.Where(a => a.ModelID == oReq.DeviceProperty.BrandID);

                //if (oReq.DeviceProperty.Network.ToInt() != 0)
                //    query = query.Where(a => a.Network == oReq.DeviceProperty.Network);

                //if (oReq.DeviceProperty.SIM.ToInt() != 0)
                //    query = query.Where(a => a.SIM == oReq.DeviceProperty.SIM);

                //if (oReq.DeviceProperty.OS.ToInt() != 0)
                //    query = query.Where(a => a.OS == oReq.DeviceProperty.OS);

                //if (oReq.DeviceProperty.MemoryCardSlot.ToInt() != 0)
                //    query = query.Where(a => a.MemoryCardSlot == oReq.DeviceProperty.MemoryCardSlot);

                //if (oReq.DeviceProperty.Camera.ToInt() != 0)
                //    query = query.Where(a => a.MemoryCardSlot == oReq.DeviceProperty.Camera);

                if (oReq.ModelIDs.Count() > 0)
                    query = query.Where(a => oReq.ModelIDs.Contains(a.ModelID));

                if (oReq.BrandIDs.Count() > 0)
                    query = query.Where(a => oReq.BrandIDs.Contains(a.BrandID));

                ////cHODEY
                //if (oReq.TechnologyIDs.Count() > 0)
                //    query = query.Where(a => (!a.TypeOfDevice.HasValue || oReq.TechnologyIDs.Contains(a.Technology.Value)));

                if (oReq.TypeOfDeviceIDs.Count() > 0)
                    query = query.Where(a => (!a.TypeOfDevice.HasValue || oReq.TypeOfDeviceIDs.Contains(a.TypeOfDevice.Value)));

                if (oReq.PriceRangeIDs.Count() > 0)
                    query = query.Where(a => (!a.TypeOfDevice.HasValue || oReq.PriceRangeIDs.Contains(a.PriceRange.Value)));

                //Chodey
                //if (oReq.Camera.HasValue)
                //    query = query.Where(a => a.Camera == oReq.Camera.Value);

                //Chodey

                if (oReq.TouchScreenIDs.Count() > 0)
                    query = query.Where(a => (!a.TypeOfDevice.HasValue || oReq.TouchScreenIDs.Contains(a.TouchScreen.Value)));

                if (oReq.Camera.Count() > 0)
                    query = query.Where(a => (!a.TypeOfDevice.HasValue || oReq.Camera.Contains(a.Camera)));

                if (oReq.GPS.Count() > 0)
                    query = query.Where(a => (!a.TypeOfDevice.HasValue || oReq.GPS.Contains(a.GPS)));

                if (oReq.Bluetooth.Count() > 0)
                    query = query.Where(a => (!a.TypeOfDevice.HasValue || oReq.Bluetooth.Contains(a.Bluetooth)));

                if (oReq.ScreenType.Count() > 0)
                    query = query.Where(a => (!a.TypeOfDevice.HasValue || oReq.ScreenType.Contains(a.ScreenType)));

                if (oReq.DualFront.Count() > 0)
                    query = query.Where(a => (!a.TypeOfDevice.HasValue || oReq.DualFront.Contains(a.DualFront)));
                //Chodey

                if (oReq.DeviceProperty.Weight.ToDecimal() != 0)
                    query = query.Where(a => a.Weight == oReq.DeviceProperty.Weight);

                if (oReq.DeviceProperty.Height.ToInt() != 0)
                    query = query.Where(a => a.Height == oReq.DeviceProperty.Height);

                if (oReq.DeviceProperty.Thick.ToInt() != 0)
                    query = query.Where(a => a.Thick == oReq.DeviceProperty.Thick);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.DevicePropertyFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<DeviceProperty> DevicePropertyGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.DevicePropertyGet({1})", svcName, (IDs.Count() > 0) ? IDs[0] : 0);
            List<DeviceProperty> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.DeviceProperties.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.DevicePropertyGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }

        #endregion

        #region VLT ADDED CODE by Rajeswari on Feb 25th for logging user information

        /// <summary>
        ///Insertes user log information
        /// </summary>
        /// <param name="inputparams">The inputparams.</param>
        /// <returns></returns>

        public static int UserInfoLogCreate(UserTransactionLog inputparams)
        {
            Logger.InfoFormat("Entering {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            IEnumerable<int> value = null;
            var NewID = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_AddUserLogInfo @Username,@Action,@IDType,@IDNumber,@Result,@StartDateTime,@EndDateTime,@RegId,@AccountCategory",
                    new SqlParameter("Username", inputparams.Username != null ? inputparams.Username : DBNull.Value.ToString()),
                    new SqlParameter("Action", inputparams.Action != null ? inputparams.Action : DBNull.Value.ToString()),
                    new SqlParameter("IDType", inputparams.IDType != null ? inputparams.IDType : DBNull.Value.ToString()),
                    new SqlParameter("IDNumber", inputparams.IDNumber != null ? inputparams.IDNumber : DBNull.Value.ToString()),
                    new SqlParameter("Result", inputparams.Result != null ? inputparams.Result : DBNull.Value.ToString()),
                    new SqlParameter("StartDateTime", inputparams.StartDateTime.HasValue ? inputparams.StartDateTime.Value : SqlDateTime.Null),
                    new SqlParameter("EndDateTime", inputparams.EndDateTime.HasValue ? inputparams.EndDateTime.Value : SqlDateTime.Null),
                    new SqlParameter("RegId", inputparams.RegId > 0 ? inputparams.RegId : SqlInt32.Null),
                    new SqlParameter("AccountCategory", inputparams.AccountCategory > 0 ? inputparams.AccountCategory : SqlInt32.Null));
                NewID = value.Single();
            }
            Logger.InfoFormat("Exiting {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            return NewID;
        }

        #endregion

        #region BrandArticle

        public static List<BrandArticle> GetArticleID(int p)
        {
            Int32 counter = 0;
            List<BrandArticle> brandArticles = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ArticleIds.Where(a => a.ID != 0);
                if (p != 0)
                    query = query.Where(a => a.ModelID == p && a.Active == true);
                var value = query.Select(a => new { ID = a.ID, ModelID = a.ModelID, ArticleID = a.ArticleID, ColourID = a.ColourID, ImagePath = a.ImagePath }).ToList();

                counter = value.Count();
                brandArticles = new List<BrandArticle>();

                if (!ReferenceEquals(brandArticles, null))
                {
                    foreach (var v in value)
                    {
                        brandArticles.Add(new BrandArticle
                        {
                            ID = v.ID,
                            ModelID = v.ModelID,
                            ArticleID = v.ArticleID,
                            ColourID = v.ColourID,
                            ImagePath = v.ImagePath
                        });
                    }
                }
                else
                {
                    brandArticles = null;
                }
                return brandArticles;
            }

        }
        public static List<BrandArticleSmart> GetArticleIDSmart(int p)
        {
            Int32 counter = 0;
            List<BrandArticleSmart> brandArticles = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ArticleIdsSmart.Where(a => a.ID != 0);
                if (p != 0)
                    query = query.Where(a => a.ModelID == p && a.Active == true);
                var value = query.Select(a => new { ID = a.ID, ModelID = a.ModelID, ArticleID = a.ArticleID, ColourID = a.ColourID, ImagePath = a.ImagePath }).ToList();

                counter = value.Count();
                brandArticles = new List<BrandArticleSmart>();

                if (!ReferenceEquals(brandArticles, null))
                {
                    foreach (var v in value)
                    {
                        brandArticles.Add(new BrandArticleSmart
                        {
                            ID = v.ID,
                            ModelID = v.ModelID,
                            ArticleID = v.ArticleID,
                            ColourID = v.ColourID,
                            ImagePath = v.ImagePath
                        });
                    }
                }
                else
                {
                    brandArticles = null;
                }
                return brandArticles;
            }

        }

        public static List<BrandArticle> GetArticleIDs(List<int> p)
        {
            Int32 counter = 0;
            List<BrandArticle> brandArticles = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ArticleIds.Where(a => a.ID != 0);
                if (p.Count > 0)
                    query = query.Where(a => p.Contains(a.ModelID) && a.Active == true);

                return query.ToList();
            }

        }
        public static List<BrandArticleSmart> GetArticleIDsSmart(List<int> p)
        {
            Int32 counter = 0;
            List<BrandArticleSmart> brandArticles = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ArticleIdsSmart.Where(a => a.ID != 0);
                if (p.Count > 0)
                    query = query.Where(a => p.Contains(a.ModelID) && a.Active == true);

                return query.ToList();
            }

        }
        public static List<BrandArticle> GetAllArticleIDs()
        {
            Int32 counter = 0;
            List<BrandArticle> brandArticles = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ArticleIds.Where(a => a.ID != 0 && a.Active == true);
                return query.ToList();
            }

        }

        public static List<BrandArticle> BrandArticleImageGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.BrandArticleImageGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<BrandArticle> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ArticleIds.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.BrandArticleImageGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }
        public static List<BrandArticleSmart> BrandArticleImageGetSmart(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.BrandArticleImageGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<BrandArticleSmart> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ArticleIdsSmart.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.BrandArticleImageGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }

        //Added by VLT on 17 May 2013 to get device image details dynamically
        public static BrandArticle BrandArticleGet(string articleId)
        {
            BrandArticle value = new BrandArticle();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                if (!string.IsNullOrEmpty(articleId))
                {
                    value = ctx.ArticleIds.First(a => a.ArticleID == articleId && a.Active == true);
                }
            }
            return value;
        }
        public static BrandArticleSmart BrandArticleGetSmart(string articleId)
        {
            BrandArticleSmart value = new BrandArticleSmart();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ArticleIdsSmart.First(a => a.ArticleID == articleId);
            }
            return value;
        }
        //Added by VLT on 17 May 2013 to get device image details dynamically

        public static int GetContractDuration(int Id)
        {
            int value = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.PgmBdlPckComponents.Where(a => a.ID != 0);
                if (Id > 0)
                    query = query.Where(a => a.ID == Id);
                value = query.Select((a) => a.Value).SingleOrDefault().ToInt();
            }
            return value;
        }

        public static string GetUomId(string ArticleId, int ContractDuration, string contractID, string offerName)
        {
            string value = string.Empty;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.UOMArticles.Where(a => a.ID != 0);
                if (!string.IsNullOrEmpty(ArticleId))
                    query = query.Where(a => a.ArticleId == ArticleId);

                if (ContractDuration > 0)
                    query = query.Where((a) => a.ContractDuration == ContractDuration);

                if (!string.IsNullOrEmpty(contractID))
                    query = query.Where(a => a.contractID == contractID);

                if (!string.IsNullOrEmpty(offerName))
                    query = query.Where(a => a.OfferName == offerName);

                value = query.Select((a) => a.UOMID).FirstOrDefault();
            }
            return value;
        }

        #endregion

        #region Get SIM Articleid by ID
        public static int GetArticleIdById(int SIMId)
        {
            int value = 0;
            SimModels simModel = null;
            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var query = ctx.SimModels.Where(a => a.ID != 0);
                    if (SIMId > 0)
                        query = query.Where(a => a.ID == SIMId);
                    //value = query.Select((a) => a.ArticleId).ToInt();
                    simModel = query.SingleOrDefault();
                }
                if (simModel != null)
                {
                    value = simModel.ArticleId;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return value;
        }

        public static string GetUOMCodeBySIMModelID(int SIMId)
        {
            string value = string.Empty;
            SimModels simModel = null;
            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var query = ctx.SimModels.Where(a => a.ID != 0);
                    if (SIMId > 0)
                        query = query.Where(a => a.ID == SIMId);
                    //value = query.Select((a) => a.ArticleId).ToInt();
                    simModel = query.SingleOrDefault();
                }
                if (simModel != null)
                {
                    value = simModel.UOM;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return value;
        }

        #endregion

        #region Added by NarayanaReddy
        public static IMPOSDetails getIMPOSDetails(int RegID, string OrganisationId, string strSIMType, bool isAccessory = false)
        {
            Logger.InfoFormat("Entering {0}.getIMPOSDetails({1})", svcName, RegID);
            List<IMPOSDetails> objIMPOSDetails = null;
            IMPOSDetails objIMPOSDetail = new IMPOSDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<IMPOSDetails>("usp_GetDetailsForIMPOS @RegID,@OrganisationId,@SimType", new SqlParameter("@RegID", RegID), new SqlParameter("@OrganisationId", OrganisationId), new SqlParameter("@SimType", strSIMType)).ToList();

                if (objIMPOSDetails != null && objIMPOSDetails.Count > 0)
                {
                    foreach (IMPOSDetails o in objIMPOSDetails)
                    {
                        if (!ReferenceEquals(o.ID, null))
                        {
                            objIMPOSDetail.ID = o.ID;
                        }
                        else
                        {
                            objIMPOSDetail.ID = 0;
                        }

                        if (!ReferenceEquals(o.OrganisationId, null))
                        {
                            objIMPOSDetail.OrganisationId = o.OrganisationId;
                        }
                        else
                        {
                            objIMPOSDetail.OrganisationId = string.Empty;
                        }
                        if (!ReferenceEquals(o.ArticleID, null))
                        {
                            objIMPOSDetail.ArticleID = o.ArticleID;
                        }
                        else
                        {
                            objIMPOSDetail.ArticleID = 0.ToString();
                        }
                        if (!ReferenceEquals(o.MSISDN1, null))
                        {
                            objIMPOSDetail.MSISDN1 = o.MSISDN1;
                        }
                        else
                        {
                            objIMPOSDetail.MSISDN1 = string.Empty;
                        }
                        if (!ReferenceEquals(o.KenanAccountNo, null))

                            objIMPOSDetail.KenanAccountNo = o.KenanAccountNo;

                        else
                            objIMPOSDetail.KenanAccountNo = "";
                        if (!ReferenceEquals(o.FullName, null))

                            objIMPOSDetail.FullName = o.FullName;

                        else
                            objIMPOSDetail.FullName = "";
                        if (!ReferenceEquals(o.IDCardNo, null))

                            objIMPOSDetail.IDCardNo = o.IDCardNo;

                        else
                            objIMPOSDetail.IDCardNo = "";
                        if (!ReferenceEquals(o.Line1, null))

                            objIMPOSDetail.Line1 = o.Line1.Replace('|', ' ');

                        else
                            objIMPOSDetail.Line1 = "";
                        if (!ReferenceEquals(o.Line2, null))

                            objIMPOSDetail.Line2 = o.Line2.Replace('|', ' ');

                        else
                            objIMPOSDetail.Line2 = "";

                        if (!ReferenceEquals(o.Line3, null))

                            objIMPOSDetail.Line3 = o.Line3.Replace('|', ' ');

                        else
                            objIMPOSDetail.Line3 = "";


                        if (!ReferenceEquals(o.Town, null))

                            objIMPOSDetail.Town = o.Town;

                        else
                            objIMPOSDetail.Town = "";
                        if (!ReferenceEquals(o.Postcode, null))

                            objIMPOSDetail.Postcode = o.Postcode;

                        else
                            objIMPOSDetail.Postcode = "";
                        if (!ReferenceEquals(o.IMEINumber, null))

                            objIMPOSDetail.IMEINumber = o.IMEINumber;

                        else
                            objIMPOSDetail.IMEINumber = "";
                        if (!ReferenceEquals(o.ModelID, null))

                            objIMPOSDetail.ModelID = Convert.ToInt32(o.ModelID.ToString());

                        else
                            objIMPOSDetail.ModelID = 0;

                        if (!ReferenceEquals(o.UOMCODE, null))
                        {
                            objIMPOSDetail.UOMCODE = o.UOMCODE;
                        }
                        else
                        {
                            objIMPOSDetail.UOMCODE = 0.ToString();
                        }
                        if (!ReferenceEquals(o.SimNo, null))
                        {
                            objIMPOSDetail.SimNo = o.SimNo;
                        }
                        else
                        {
                            objIMPOSDetail.SimNo = 0.ToString();
                        }
                        objIMPOSDetail.RegTypeID = o.RegTypeID > 0 ? o.RegTypeID : 0;
                        objIMPOSDetail.SimModelId = o.SimModelId > 0 ? o.SimModelId : 0;
                        /*Added By Patanjal*/
                        if (!ReferenceEquals(o.PlanAdvance, null))
                        {
                            objIMPOSDetail.PlanAdvance = o.PlanAdvance;
                        }
                        else
                        {
                            objIMPOSDetail.PlanAdvance = 0;
                        }
                        if (!ReferenceEquals(o.Deviceadvance, null))
                        {
                            objIMPOSDetail.Deviceadvance = o.Deviceadvance;
                        }
                        else
                        {
                            objIMPOSDetail.Deviceadvance = 0;
                        }
                        if (!ReferenceEquals(o.PlanDeposit, null))
                        {
                            objIMPOSDetail.PlanDeposit = o.PlanDeposit;
                        }
                        else
                        {
                            objIMPOSDetail.PlanDeposit = 0;
                        }
                        if (!ReferenceEquals(o.Devicedeposit, null))
                        {
                            objIMPOSDetail.Devicedeposit = o.Devicedeposit;
                        }
                        else
                        {
                            objIMPOSDetail.Devicedeposit = 0;
                        }
                        /*ENDHERE*/

                        //Code Added by VLT on May 01 2013
                        if (!ReferenceEquals(o.InternationalRoaming, null))
                        {
                            objIMPOSDetail.InternationalRoaming = o.InternationalRoaming;
                        }
                        else
                        {
                            objIMPOSDetail.InternationalRoaming = false;
                        }

                        if (!ReferenceEquals(o.K2Type, null))
                        {
                            objIMPOSDetail.K2Type = o.K2Type;
                        }
                        else
                        {
                            objIMPOSDetail.K2Type = false;
                        }
                        //End of Code Added by VLT on May 01 2013

                        //Added by Narayana on May 13 2013 
                        objIMPOSDetail.POSDomain = o.POSDomain;
                        objIMPOSDetail.POSNetworkPath = o.POSNetworkPath;
                        objIMPOSDetail.POSUserName = o.POSUserName;
                        objIMPOSDetail.POSPassword = o.POSPassword;
                        //Added by Narayana on May 13 2013 

                        //Added by Nreddy on may 16 2013
                        objIMPOSDetail.SmartId = o.SmartId;
                        objIMPOSDetail.Smart_Id = o.Smart_Id;
                        objIMPOSDetail.DiscountAmt = o.DiscountAmt;
                        objIMPOSDetail.DiscountId = o.DiscountId;
                        //end 

                        if (!ReferenceEquals(o.UpfrontPayment, null))
                        {
                            objIMPOSDetail.UpfrontPayment = o.UpfrontPayment;
                        }
                        else
                        {
                            objIMPOSDetail.UpfrontPayment = 0;
                        }

                        //RST GST
                        objIMPOSDetail.TradeUpAmount = !ReferenceEquals(o.TradeUpAmount, null) ? o.TradeUpAmount : 0;
                        objIMPOSDetail.IsTradeUp = !ReferenceEquals(o.IsTradeUp, null) ? o.IsTradeUp : false;
                    }
                }

            }
            Logger.InfoFormat("Exiting {0}.getIMPOSDetails({1})", svcName, RegID);
            return objIMPOSDetail;

        }

        public static IMPOSDetails getIMPOSDetails_Smart(int RegID, string OrganisationId, string strSIMType)
        {
            Logger.InfoFormat("Entering {0}.getIMPOSDetails({1})", svcName, RegID);
            List<IMPOSDetails> objIMPOSDetails = null;
            IMPOSDetails objIMPOSDetail = new IMPOSDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<IMPOSDetails>("usp_GetDetailsForIMPOS_smart @RegID,@OrganisationId,@SimType", new SqlParameter("@RegID", RegID), new SqlParameter("@OrganisationId", OrganisationId), new SqlParameter("@SimType", strSIMType)).ToList();

                if (objIMPOSDetails != null && objIMPOSDetails.Count > 0)
                {
                    foreach (IMPOSDetails o in objIMPOSDetails)
                    {
                        if (!ReferenceEquals(o.ID, null))
                        {
                            objIMPOSDetail.ID = o.ID;
                        }
                        else
                        {
                            objIMPOSDetail.ID = 0;
                        }

                        if (!ReferenceEquals(o.OrganisationId, null))
                        {
                            objIMPOSDetail.OrganisationId = o.OrganisationId;
                        }
                        else
                        {
                            objIMPOSDetail.OrganisationId = string.Empty;
                        }
                        if (!ReferenceEquals(o.ArticleID, null))
                        {
                            objIMPOSDetail.ArticleID = o.ArticleID;
                        }
                        else
                        {
                            objIMPOSDetail.ArticleID = 0.ToString();
                        }
                        if (!ReferenceEquals(o.MSISDN1, null))
                        {
                            objIMPOSDetail.MSISDN1 = o.MSISDN1;
                        }
                        else
                        {
                            objIMPOSDetail.MSISDN1 = string.Empty;
                        }
                        if (!ReferenceEquals(o.KenanAccountNo, null))

                            objIMPOSDetail.KenanAccountNo = o.KenanAccountNo;

                        else
                            objIMPOSDetail.KenanAccountNo = "";
                        if (!ReferenceEquals(o.FullName, null))

                            objIMPOSDetail.FullName = o.FullName;

                        else
                            objIMPOSDetail.FullName = "";
                        if (!ReferenceEquals(o.IDCardNo, null))

                            objIMPOSDetail.IDCardNo = o.IDCardNo;

                        else
                            objIMPOSDetail.IDCardNo = "";
                        if (!ReferenceEquals(o.Line1, null))

                            objIMPOSDetail.Line1 = o.Line1;

                        else
                            objIMPOSDetail.Line1 = "";
                        if (!ReferenceEquals(o.Line2, null))

                            objIMPOSDetail.Line2 = o.Line2;

                        else
                            objIMPOSDetail.Line2 = "";

                        if (!ReferenceEquals(o.Line3, null))

                            objIMPOSDetail.Line3 = o.Line3;

                        else
                            objIMPOSDetail.Line3 = "";


                        if (!ReferenceEquals(o.Town, null))

                            objIMPOSDetail.Town = o.Town;

                        else
                            objIMPOSDetail.Town = "";
                        if (!ReferenceEquals(o.Postcode, null))

                            objIMPOSDetail.Postcode = o.Postcode;

                        else
                            objIMPOSDetail.Postcode = "";
                        if (!ReferenceEquals(o.IMEINumber, null))

                            objIMPOSDetail.IMEINumber = o.IMEINumber;

                        else
                            objIMPOSDetail.IMEINumber = "";
                        if (!ReferenceEquals(o.ModelID, null))

                            objIMPOSDetail.ModelID = Convert.ToInt32(o.ModelID.ToString());

                        else
                            objIMPOSDetail.ModelID = 0;

                        if (!ReferenceEquals(o.UOMCODE, null))
                        {
                            objIMPOSDetail.UOMCODE = o.UOMCODE;
                        }
                        else
                        {
                            objIMPOSDetail.UOMCODE = 0.ToString();
                        }
                        if (!ReferenceEquals(o.SimNo, null))
                        {
                            objIMPOSDetail.SimNo = o.SimNo;
                        }
                        else
                        {
                            objIMPOSDetail.SimNo = 0.ToString();
                        }
                        objIMPOSDetail.RegTypeID = o.RegTypeID > 0 ? o.RegTypeID : 0;
                        objIMPOSDetail.SimModelId = o.SimModelId > 0 ? o.SimModelId : 0;
                        /*Added By Patanjal*/
                        if (!ReferenceEquals(o.PlanAdvance, null))
                        {
                            objIMPOSDetail.PlanAdvance = o.PlanAdvance;
                        }
                        else
                        {
                            objIMPOSDetail.PlanAdvance = 0;
                        }
                        if (!ReferenceEquals(o.Deviceadvance, null))
                        {
                            objIMPOSDetail.Deviceadvance = o.Deviceadvance;
                        }
                        else
                        {
                            objIMPOSDetail.Deviceadvance = 0;
                        }
                        if (!ReferenceEquals(o.PlanDeposit, null))
                        {
                            objIMPOSDetail.PlanDeposit = o.PlanDeposit;
                        }
                        else
                        {
                            objIMPOSDetail.PlanDeposit = 0;
                        }
                        if (!ReferenceEquals(o.Devicedeposit, null))
                        {
                            objIMPOSDetail.Devicedeposit = o.Devicedeposit;
                        }
                        else
                        {
                            objIMPOSDetail.Devicedeposit = 0;
                        }
                        /*ENDHERE*/

                        //Code Added by VLT on May 01 2013
                        if (!ReferenceEquals(o.InternationalRoaming, null))
                        {
                            objIMPOSDetail.InternationalRoaming = o.InternationalRoaming;
                        }
                        else
                        {
                            objIMPOSDetail.InternationalRoaming = false;
                        }

                        if (!ReferenceEquals(o.K2Type, null))
                        {
                            objIMPOSDetail.K2Type = o.K2Type;
                        }
                        else
                        {
                            objIMPOSDetail.K2Type = false;
                        }
                        //End of Code Added by VLT on May 01 2013

                        //Added by Narayana on May 13 2013 
                        objIMPOSDetail.POSDomain = o.POSDomain;
                        objIMPOSDetail.POSNetworkPath = o.POSNetworkPath;
                        objIMPOSDetail.POSUserName = o.POSUserName;
                        objIMPOSDetail.POSPassword = o.POSPassword;
                        //Added by Narayana on May 13 2013 

                        //Added by Nreddy on may 16 2013
                        objIMPOSDetail.SmartId = o.SmartId;
                        objIMPOSDetail.Smart_Id = o.Smart_Id;
                        objIMPOSDetail.DiscountAmt = o.DiscountAmt;
                        objIMPOSDetail.DiscountId = o.DiscountId;
                        //end 

                        if (!ReferenceEquals(o.UpfrontPayment, null))
                        {
                            objIMPOSDetail.UpfrontPayment = o.UpfrontPayment;
                        }
                        else
                        {
                            objIMPOSDetail.UpfrontPayment = 0;
                        }

                    }
                }

            }
            Logger.InfoFormat("Exiting {0}.getIMPOSDetails({1})", svcName, RegID);
            return objIMPOSDetail;

        }

        public static IMPOSDetails getIMPOSDetailsForPlanOnly(int RegID, string OrganisationId, string strSIMType = "P")
        {
            Logger.InfoFormat("Entering {0}.getIMPOSDetails({1})", svcName, RegID);
            List<IMPOSDetails> objIMPOSDetails = null;
            IMPOSDetails objIMPOSDetail = new IMPOSDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<IMPOSDetails>("usp_GetDetailsForIMPOS_PlanOnly @RegID,@OrganisationId,@SimType ", new SqlParameter("@RegID", RegID), new SqlParameter("@OrganisationId", OrganisationId), new SqlParameter("@SimType", strSIMType)).ToList();

                if (objIMPOSDetails != null && objIMPOSDetails.Count > 0)
                {
                    foreach (IMPOSDetails o in objIMPOSDetails)
                    {
                        if (!ReferenceEquals(o.Trn_Type, null))
                        {
                            objIMPOSDetail.Trn_Type = o.Trn_Type;
                        }
                        else
                        {
                            objIMPOSDetail.Trn_Type = string.Empty;
                        }
                        if (!ReferenceEquals(o.ID, null))
                        {
                            objIMPOSDetail.ID = o.ID;
                        }
                        else
                        {
                            objIMPOSDetail.ID = 0;
                        }
                        if (!ReferenceEquals(o.OrganisationId, null))
                        {
                            objIMPOSDetail.OrganisationId = o.OrganisationId;
                        }
                        else
                        {
                            objIMPOSDetail.OrganisationId = string.Empty;
                        }
                        if (!ReferenceEquals(o.ArticleID, null))
                        {
                            objIMPOSDetail.ArticleID = o.ArticleID;
                        }
                        else
                        {
                            objIMPOSDetail.ArticleID = 0.ToString();
                        }
                        if (!ReferenceEquals(o.MSISDN1, null))
                        {
                            objIMPOSDetail.MSISDN1 = o.MSISDN1;
                        }
                        else
                        {
                            objIMPOSDetail.MSISDN1 = o.ExternalID;
                        }
                        if (!ReferenceEquals(o.KenanAccountNo, null))

                            objIMPOSDetail.KenanAccountNo = o.KenanAccountNo;

                        else
                            objIMPOSDetail.KenanAccountNo = "";
                        if (!ReferenceEquals(o.FullName, null))

                            objIMPOSDetail.FullName = o.FullName;

                        else
                            objIMPOSDetail.FullName = "";
                        if (!ReferenceEquals(o.IDCardNo, null))

                            objIMPOSDetail.IDCardNo = o.IDCardNo;

                        else
                            objIMPOSDetail.IDCardNo = "";
                        if (!ReferenceEquals(o.Line1, null))

                            objIMPOSDetail.Line1 = o.Line1.Replace('|',' ');

                        else
                            objIMPOSDetail.Line1 = "";
                        if (!ReferenceEquals(o.Line2, null))

                            objIMPOSDetail.Line2 = o.Line2.Replace('|', ' ');

                        else
                            objIMPOSDetail.Line2 = "";

                        if (!ReferenceEquals(o.Line3, null))

                            objIMPOSDetail.Line3 = o.Line3.Replace('|', ' ');

                        else
                            objIMPOSDetail.Line3 = "";


                        if (!ReferenceEquals(o.Town, null))

                            objIMPOSDetail.Town = o.Town;

                        else
                            objIMPOSDetail.Town = "";
                        if (!ReferenceEquals(o.Postcode, null))

                            objIMPOSDetail.Postcode = o.Postcode;

                        else
                            objIMPOSDetail.Postcode = "";
                        if (!ReferenceEquals(o.IMEINumber, null))

                            objIMPOSDetail.IMEINumber = o.IMEINumber;

                        else
                            objIMPOSDetail.IMEINumber = "";
                        if (!ReferenceEquals(o.ModelID, null))

                            objIMPOSDetail.ModelID = Convert.ToInt32(o.ModelID.ToString());

                        else
                            objIMPOSDetail.ModelID = 0;

                        if (!ReferenceEquals(o.UOMCODE, null))
                        {
                            objIMPOSDetail.UOMCODE = o.UOMCODE;
                        }
                        else
                        {
                            objIMPOSDetail.UOMCODE = 0.ToString();
                        }
                        if (!ReferenceEquals(o.SimNo, null))
                        {
                            objIMPOSDetail.SimNo = o.SimNo;
                        }
                        else
                        {
                            objIMPOSDetail.SimNo = 0.ToString();
                        }
                        objIMPOSDetail.RegTypeID = o.RegTypeID > 0 ? o.RegTypeID : 0;
                        objIMPOSDetail.SimModelId = o.SimModelId > 0 ? o.SimModelId : 0;
                        /*Added By Patanjal*/
                        if (!ReferenceEquals(o.PlanAdvance, null))
                        {
                            objIMPOSDetail.PlanAdvance = o.PlanAdvance;
                        }
                        else
                        {
                            objIMPOSDetail.PlanAdvance = 0;
                        }
                        if (!ReferenceEquals(o.Deviceadvance, null))
                        {
                            objIMPOSDetail.Deviceadvance = o.Deviceadvance;
                        }
                        else
                        {
                            objIMPOSDetail.Deviceadvance = 0;
                        }
                        if (!ReferenceEquals(o.PlanDeposit, null))
                        {
                            objIMPOSDetail.PlanDeposit = o.PlanDeposit;
                        }
                        else
                        {
                            objIMPOSDetail.PlanDeposit = 0;
                        }
                        if (!ReferenceEquals(o.Devicedeposit, null))
                        {
                            objIMPOSDetail.Devicedeposit = o.Devicedeposit;
                        }
                        else
                        {
                            objIMPOSDetail.Devicedeposit = 0;
                        }
                        /*ENDHERE*/
                        //Code Added by VLT on May 01 2013
                        if (!ReferenceEquals(o.InternationalRoaming, null))
                        {
                            objIMPOSDetail.InternationalRoaming = o.InternationalRoaming;
                        }
                        else
                        {
                            objIMPOSDetail.InternationalRoaming = false;
                        }
                        if (!ReferenceEquals(o.K2Type, null))
                        {
                            objIMPOSDetail.K2Type = o.K2Type;
                        }
                        else
                        {
                            objIMPOSDetail.K2Type = false;
                        }
                        //End of Code Added by VLT on May 01 2013
                        //Added by Narayana on May 13 2013 
                        objIMPOSDetail.POSDomain = o.POSDomain;
                        objIMPOSDetail.POSNetworkPath = o.POSNetworkPath;
                        objIMPOSDetail.POSUserName = o.POSUserName;
                        objIMPOSDetail.POSPassword = o.POSPassword;
                        //Added by Narayana on May 13 2013 
                        if (!ReferenceEquals(o.TUTSerial, null))
                        {
                            objIMPOSDetail.TUTSerial = o.TUTSerial;
                        }
                        else
                        {
                            objIMPOSDetail.TUTSerial = string.Empty;
                        }
                        if (!ReferenceEquals(o.TUTSimModelId, null))
                        {
                            objIMPOSDetail.TUTSimModelId = o.TUTSimModelId;
                        }
                        else
                        {
                            objIMPOSDetail.TUTSimModelId = 0;
                        }
                    }
                }

            }
            Logger.InfoFormat("Exiting {0}.getIMPOSDetails({1})", svcName, RegID);
            return objIMPOSDetail;

        }

        public static List<IMPOSDetailsForAccessory> getIMPOSDetailsForAccessory(int RegID, string OrganisationId, string strSIMType = "P")
        {
            List<IMPOSDetailsForAccessory> objIMPOSDetails = null;
            var objIMPOSDetail = new IMPOSDetailsForAccessory();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<IMPOSDetailsForAccessory>("usp_GetDetailsForIMPOS_Accessory @RegID,@OrganisationId,@SimType", new SqlParameter("@RegID", RegID), new SqlParameter("@OrganisationId", OrganisationId), new SqlParameter("@SimType", strSIMType)).ToList();
                Logger.Info("getIMPOSDetailsForAccessory = " + objIMPOSDetails);
            }

            return objIMPOSDetails;
        }

        public static IMPOSDetails GetPosKeyandOrgID(int RegID, string strSIMType = "P")
        {
            Logger.InfoFormat("Entering {0}.getIMPOSDetails({1})", svcName, RegID);
            List<IMPOSDetails> objIMPOSDetails = null;
            IMPOSDetails objIMPOSDetail = new IMPOSDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<IMPOSDetails>("usp_GetPosKeyandOrgID_Test @RegID,@SimType", new SqlParameter("@RegID", RegID), new SqlParameter("@SimType", strSIMType)).ToList();

                if (objIMPOSDetails != null && objIMPOSDetails.Count > 0)
                {
                    foreach (IMPOSDetails o in objIMPOSDetails)
                    {
                        if (!ReferenceEquals(o.Trn_Type, null))
                        {
                            objIMPOSDetail.Trn_Type = o.Trn_Type;
                        }
                        else
                        {
                            objIMPOSDetail.Trn_Type = string.Empty;
                        }
                        if (!ReferenceEquals(o.OrganisationId, null))
                        {
                            objIMPOSDetail.OrganisationId = o.OrganisationId;
                        }
                        else
                        {
                            objIMPOSDetail.OrganisationId = string.Empty;
                        }
                        if (!ReferenceEquals(o.ArticleID, null))
                        {
                            objIMPOSDetail.ArticleID = o.ArticleID;
                        }
                        else
                        {
                            objIMPOSDetail.ArticleID = 0.ToString();
                        }
                        if (!ReferenceEquals(o.MSISDN1, null))
                        {
                            objIMPOSDetail.MSISDN1 = o.MSISDN1;
                        }
                        else
                        {
                            objIMPOSDetail.MSISDN1 = string.Empty;
                        }
                        if (!ReferenceEquals(o.KenanAccountNo, null))

                            objIMPOSDetail.KenanAccountNo = o.KenanAccountNo;

                        else
                            objIMPOSDetail.KenanAccountNo = "";
                        if (!ReferenceEquals(o.FullName, null))

                            objIMPOSDetail.FullName = o.FullName;

                        else
                            objIMPOSDetail.FullName = "";
                        if (!ReferenceEquals(o.IDCardNo, null))

                            objIMPOSDetail.IDCardNo = o.IDCardNo;

                        else
                            objIMPOSDetail.IDCardNo = "";
                        if (!ReferenceEquals(o.Line1, null))

                            objIMPOSDetail.Line1 = o.Line1;

                        else
                            objIMPOSDetail.Line1 = "";
                        if (!ReferenceEquals(o.Line2, null))

                            objIMPOSDetail.Line2 = o.Line2;

                        else
                            objIMPOSDetail.Line2 = "";

                        if (!ReferenceEquals(o.Line3, null))

                            objIMPOSDetail.Line3 = o.Line3;

                        else
                            objIMPOSDetail.Line3 = "";


                        if (!ReferenceEquals(o.Town, null))

                            objIMPOSDetail.Town = o.Town;

                        else
                            objIMPOSDetail.Town = "";
                        if (!ReferenceEquals(o.Postcode, null))

                            objIMPOSDetail.Postcode = o.Postcode;

                        else
                            objIMPOSDetail.Postcode = "";
                        if (!ReferenceEquals(o.IMEINumber, null))

                            objIMPOSDetail.IMEINumber = o.IMEINumber;

                        else
                            objIMPOSDetail.IMEINumber = "";
                        if (!ReferenceEquals(o.ModelID, null))

                            objIMPOSDetail.ModelID = Convert.ToInt32(o.ModelID.ToString());

                        else
                            objIMPOSDetail.ModelID = 0;

                        if (!ReferenceEquals(o.UOMCODE, null))
                        {
                            objIMPOSDetail.UOMCODE = o.UOMCODE;
                        }
                        else
                        {
                            objIMPOSDetail.UOMCODE = 0.ToString();
                        }
                        if (!ReferenceEquals(o.SimNo, null))
                        {
                            objIMPOSDetail.SimNo = o.SimNo;
                        }
                        else
                        {
                            objIMPOSDetail.SimNo = 0.ToString();
                        }
                        objIMPOSDetail.RegTypeID = o.RegTypeID > 0 ? o.RegTypeID : 0;
                        objIMPOSDetail.SimModelId = o.SimModelId > 0 ? o.SimModelId : 0;
                        /*Added By Patanjal*/
                        if (!ReferenceEquals(o.PlanAdvance, null))
                        {
                            objIMPOSDetail.PlanAdvance = o.PlanAdvance;
                        }
                        else
                        {
                            objIMPOSDetail.PlanAdvance = 0;
                        }
                        if (!ReferenceEquals(o.Deviceadvance, null))
                        {
                            objIMPOSDetail.Deviceadvance = o.Deviceadvance;
                        }
                        else
                        {
                            objIMPOSDetail.Deviceadvance = 0;
                        }
                        if (!ReferenceEquals(o.PlanDeposit, null))
                        {
                            objIMPOSDetail.PlanDeposit = o.PlanDeposit;
                        }
                        else
                        {
                            objIMPOSDetail.PlanDeposit = 0;
                        }
                        if (!ReferenceEquals(o.Devicedeposit, null))
                        {
                            objIMPOSDetail.Devicedeposit = o.Devicedeposit;
                        }
                        else
                        {
                            objIMPOSDetail.Devicedeposit = 0;
                        }
                        /*ENDHERE*/

                        //Code Added by VLT on May 01 2013
                        if (!ReferenceEquals(o.InternationalRoaming, null))
                        {
                            objIMPOSDetail.InternationalRoaming = o.InternationalRoaming;
                        }
                        else
                        {
                            objIMPOSDetail.InternationalRoaming = false;
                        }

                        if (!ReferenceEquals(o.K2Type, null))
                        {
                            objIMPOSDetail.K2Type = o.K2Type;
                        }
                        else
                        {
                            objIMPOSDetail.K2Type = false;
                        }
                        //End of Code Added by VLT on May 01 2013
                        objIMPOSDetail.POSDomain = o.POSDomain;
                        objIMPOSDetail.POSNetworkPath = o.POSNetworkPath;
                        objIMPOSDetail.POSUserName = o.POSUserName;
                        objIMPOSDetail.POSPassword = o.POSPassword;
                    }
                }

            }
            Logger.InfoFormat("Exiting {0}.getIMPOSDetails({1})", svcName, RegID);
            return objIMPOSDetail;

        }

        #region genarate POS for MISM
        public static List<IMPOSDetails> GetPosKeyandOrgIDNew(int RegID)
        {
            Logger.InfoFormat("Entering {0}.GetPosKeyandOrgIDNew({1})", svcName, RegID);
            List<IMPOSDetails> objIMPOSDetails = null;
            IMPOSDetails objIMPOSDetail = new IMPOSDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<IMPOSDetails>("usp_GetPosKeyandOrgID_NEW @RegID", new SqlParameter("@RegID", RegID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetPosKeyandOrgIDNew({1})", svcName, RegID);
            return objIMPOSDetails;

        }

        public static List<IMPOSDetails> getIMPOSDetailsNew(int RegID, string OrganisationId)
        {
            Logger.InfoFormat("Entering {0}.getIMPOSDetailsNew({1})", svcName, RegID);
            List<IMPOSDetails> objIMPOSDetails = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<IMPOSDetails>("usp_GetDetailsForIMPOS_NEW @RegID,@OrganisationId ", new SqlParameter("@RegID", RegID), new SqlParameter("@OrganisationId", OrganisationId)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.getIMPOSDetailsNew({1})", svcName, RegID);
            return objIMPOSDetails;

        }
        #endregion

        #region "Pos Details for Device Sales by ravi"
        //Added by ravi for device sales
        /// <summary>
        /// Method used to get the pos file for Device sales
        /// </summary>
        /// <param name="RegId">Registration Id</param>
        /// <returns>Impos Details object</returns>
        public static IMPOSDetails GetDeviceSalesIMPOSDetails(int RegID)
        {
            Logger.InfoFormat("Entering {0}.GetDeviceSalesIMPOSDetails({1})", svcName, RegID);
            List<IMPOSDetails> objIMPOSDetails = null;
            IMPOSDetails objIMPOSDetail = new IMPOSDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<IMPOSDetails>("usp_GetDeviceSalePOSDetails @RegID", new SqlParameter("@RegID", RegID)).ToList();

                if (objIMPOSDetails != null && objIMPOSDetails.Count > 0)
                {
                    IMPOSDetails objPosDetails = objIMPOSDetails[0];
                    if (!ReferenceEquals(objPosDetails.ID, null))
                    {
                        objIMPOSDetail.ID = objPosDetails.ID;
                    }
                    else
                    {
                        objIMPOSDetail.ID = 0;
                    }

                    if (!ReferenceEquals(objPosDetails.OrganisationId, null))
                    {
                        objIMPOSDetail.OrganisationId = objPosDetails.OrganisationId;
                    }
                    else
                    {
                        objIMPOSDetail.OrganisationId = string.Empty;
                    }
                    if (!ReferenceEquals(objPosDetails.ArticleID, null))
                    {
                        objIMPOSDetail.ArticleID = objPosDetails.ArticleID;
                    }
                    else
                    {
                        objIMPOSDetail.ArticleID = 0.ToString();
                    }
                    objIMPOSDetail.MSISDN1 = objPosDetails.MSISDN1;
                    objIMPOSDetail.KenanAccountNo = objPosDetails.KenanAccountNo;
                    if (!ReferenceEquals(objPosDetails.FullName, null))
                    {
                        objIMPOSDetail.FullName = objPosDetails.FullName;
                    }
                    else
                    {
                        objIMPOSDetail.FullName = string.Empty;
                    }
                    if (!ReferenceEquals(objPosDetails.IDCardNo, null))
                    {
                        objIMPOSDetail.IDCardNo = objPosDetails.IDCardNo;
                    }
                    else
                    {
                        objIMPOSDetail.IDCardNo = string.Empty;
                    }
                    objIMPOSDetail.Line1 = objPosDetails.Line1;
                    objIMPOSDetail.Line2 = objPosDetails.Line2;
                    objIMPOSDetail.Line3 = objPosDetails.Line3;
                    objIMPOSDetail.Town = objPosDetails.Town;
                    if (!ReferenceEquals(objPosDetails.Postcode, null))
                    {
                        objIMPOSDetail.Postcode = objPosDetails.Postcode;
                    }
                    else
                    {
                        objIMPOSDetail.Postcode = string.Empty;
                    }
                    objIMPOSDetail.IMEINumber = objPosDetails.IMEINumber;
                    if (!ReferenceEquals(objPosDetails.ModelID, null))
                    {
                        objIMPOSDetail.ModelID = Convert.ToInt32(objPosDetails.ModelID.ToString());
                    }
                    else
                    {
                        objIMPOSDetail.ModelID = 0;
                    }

                    objIMPOSDetail.UOMCODE = objPosDetails.UOMCODE;
                    objIMPOSDetail.SimNo = objPosDetails.SimNo;
                    objIMPOSDetail.RegTypeID = objPosDetails.RegTypeID > 0 ? objPosDetails.RegTypeID : 0;
                    objIMPOSDetail.SimModelId = objPosDetails.SimModelId > 0 ? objPosDetails.SimModelId : 0;
                    objIMPOSDetail.PlanAdvance = objPosDetails.PlanAdvance;
                    objIMPOSDetail.Deviceadvance = objPosDetails.Deviceadvance;
                    objIMPOSDetail.PlanDeposit = objPosDetails.PlanDeposit;
                    objIMPOSDetail.Devicedeposit = objPosDetails.Devicedeposit;
                    objIMPOSDetail.InternationalRoaming = objPosDetails.InternationalRoaming;
                    objIMPOSDetail.K2Type = objPosDetails.K2Type;
                    objIMPOSDetail.POSDomain = objPosDetails.POSDomain;
                    objIMPOSDetail.POSNetworkPath = objPosDetails.POSNetworkPath;
                    objIMPOSDetail.POSUserName = objPosDetails.POSUserName;
                    objIMPOSDetail.POSPassword = objPosDetails.POSPassword;
                    objIMPOSDetail.SmartId = objPosDetails.SmartId;
                    objIMPOSDetail.Smart_Id = objPosDetails.Smart_Id;
                    objIMPOSDetail.DiscountAmt = objPosDetails.DiscountAmt;
                    objIMPOSDetail.DiscountId = objPosDetails.DiscountId;
                    objIMPOSDetail.UpfrontPayment = objPosDetails.UpfrontPayment;
                    objIMPOSDetail.Poskey = objPosDetails.Poskey;

                }

            }
            Logger.InfoFormat("Exiting {0}.GetDeviceSalesIMPOSDetails({1})", svcName, RegID);
            return objIMPOSDetail;

        }

        #endregion

        #region "Pos Details for Sim replacment by ravi"
        //Added by ravi for Sim replacment
        /// <summary>
        /// Method used to get the pos details for Sim replacment
        /// </summary>
        /// <param name="RegId">Registration Id</param>
        /// <returns>Impos Details object</returns>
        public static IMPOSDetails GetSIMReplacmentPOSDetails(int RegID)
        {
            Logger.InfoFormat("Entering {0}.GetSIMReplacmentPOSDetails({1})", svcName, RegID);
            List<IMPOSDetails> objIMPOSDetails = null;
            IMPOSDetails objIMPOSDetail = new IMPOSDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<IMPOSDetails>("usp_GetSimReplacementPOSDetails @RegID", new SqlParameter("@RegID", RegID)).ToList();

                if (objIMPOSDetails != null && objIMPOSDetails.Count > 0)
                {
                    IMPOSDetails objPosDetails = objIMPOSDetails[0];
                    objIMPOSDetail.ID = objPosDetails.ID;
                    objIMPOSDetail.OrganisationId = objPosDetails.OrganisationId;
                    if (!ReferenceEquals(objPosDetails.ArticleID, null))
                    {
                        objIMPOSDetail.ArticleID = objPosDetails.ArticleID;
                    }
                    else
                    {
                        objIMPOSDetail.ArticleID = 0.ToString();
                    }
                    objIMPOSDetail.MSISDN1 = objPosDetails.MSISDN1;
                    objIMPOSDetail.KenanAccountNo = objPosDetails.KenanAccountNo;
                    if (!ReferenceEquals(objPosDetails.FullName, null))
                    {
                        objIMPOSDetail.FullName = objPosDetails.FullName;
                    }
                    else
                    {
                        objIMPOSDetail.FullName = string.Empty;
                    }
                    if (!ReferenceEquals(objPosDetails.IDCardNo, null))
                    {
                        objIMPOSDetail.IDCardNo = objPosDetails.IDCardNo;
                    }
                    else
                    {
                        objIMPOSDetail.IDCardNo = string.Empty;
                    }
                    objIMPOSDetail.Line1 = objPosDetails.Line1;
                    objIMPOSDetail.Line2 = objPosDetails.Line2;
                    objIMPOSDetail.Line3 = objPosDetails.Line3;
                    objIMPOSDetail.Town = objPosDetails.Town;
                    if (!ReferenceEquals(objPosDetails.Postcode, null))
                    {
                        objIMPOSDetail.Postcode = objPosDetails.Postcode;
                    }
                    else
                    {
                        objIMPOSDetail.Postcode = string.Empty;
                    }
                    objIMPOSDetail.UOMCODE = objPosDetails.UOMCODE;
                    objIMPOSDetail.SimNo = objPosDetails.SimNo;
                    objIMPOSDetail.RegTypeID = objPosDetails.RegTypeID > 0 ? objPosDetails.RegTypeID : 0;
                    objIMPOSDetail.SimModelId = objPosDetails.SimModelId > 0 ? objPosDetails.SimModelId : 0;
                    objIMPOSDetail.PlanAdvance = objPosDetails.PlanAdvance;
                    objIMPOSDetail.PlanDeposit = objPosDetails.PlanDeposit;
                    objIMPOSDetail.InternationalRoaming = objPosDetails.InternationalRoaming;
                    objIMPOSDetail.POSDomain = objPosDetails.POSDomain;
                    objIMPOSDetail.POSNetworkPath = objPosDetails.POSNetworkPath;
                    objIMPOSDetail.POSUserName = objPosDetails.POSUserName;
                    objIMPOSDetail.POSPassword = objPosDetails.POSPassword;
                    objIMPOSDetail.UpfrontPayment = objPosDetails.UpfrontPayment;
                    objIMPOSDetail.Poskey = objPosDetails.Poskey;
                    //secondary sim,articleid and tutsimseril
                    objIMPOSDetail.TUTSerial = objPosDetails.TUTSerial;
                    objIMPOSDetail.SecArticleId = objPosDetails.SecArticleId;
                    objIMPOSDetail.SecSimSerial = objPosDetails.SecSimSerial;
                    objIMPOSDetail.CallConferencing = objPosDetails.CallConferencing;
                    objIMPOSDetail.TUTSimModelId = objPosDetails.TUTSimModelId;

                }

            }
            Logger.InfoFormat("Exiting {0}.GetSIMReplacmentPOSDetails({1})", svcName, RegID);
            return objIMPOSDetail;
        }

        #endregion


        //added by Deepika
        public static IMPOSDetails getOrgCode(int RegID)
        {
            Logger.InfoFormat("Entering {0}.getIMPOSDetails({1})", svcName, RegID);
            List<IMPOSDetails> lstOrgDetails = null;
            IMPOSDetails ObjOrgDetails = new IMPOSDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                lstOrgDetails = ctx.Database.SqlQuery<IMPOSDetails>("usp_GetOrgDetails @RegID", new SqlParameter("@RegID", RegID)).ToList();

                if (lstOrgDetails != null && lstOrgDetails.Count > 0)
                {
                    ObjOrgDetails.OrgCode = lstOrgDetails[0].OrgCode ?? string.Empty;

                }

            }
            Logger.InfoFormat("Exiting {0}.getIMPOSDetails({1})", svcName, RegID);
            return ObjOrgDetails;

        }

        public static WhiteListDetails getWhilstDescreption(string UserICNO)
        {
            Logger.InfoFormat("Entering {0}.getWhilstDescreption({1})", svcName, UserICNO);
            List<WhiteListDetails> lstWhiteListDetails = null;
            WhiteListDetails objWhiteListDetails = new WhiteListDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                lstWhiteListDetails = ctx.Database.SqlQuery<WhiteListDetails>("usp_GetWhilistDescreption @UserICNO", new SqlParameter("@UserICNO", UserICNO)).ToList();
                if (lstWhiteListDetails != null && lstWhiteListDetails.Count > 0)
                {
                    foreach (WhiteListDetails o in lstWhiteListDetails)
                    {
                        if (!ReferenceEquals(o.Description, null))

                            objWhiteListDetails.Description = o.Description;

                        else
                            objWhiteListDetails.Description = "Not avilable";
                    }
                }

            }
            Logger.InfoFormat("Exiting {0}.getWhilstDescreption({1})", svcName, UserICNO);
            return objWhiteListDetails;

        }

        public static int UpdateIMPOSDetails(int RegID, string IMPOSFileName, int IPMOSStatus, string strSIMType = "P")
        {
            Logger.InfoFormat("Entering {0}.UpdateIMPOSDetails({1})", svcName, RegID);
            int isUpdated = -1;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                isUpdated = ctx.Database.ExecuteSqlCommand("usp_UpdateIMPOSDetails_Test @RegID,@IMPOSFileName,@IMPOSStatus,@SimType",
                    new SqlParameter("@RegID", RegID),
                    new SqlParameter("@IMPOSFileName", IMPOSFileName != null ? IMPOSFileName : DBNull.Value.ToString()),
                    new SqlParameter("@IMPOSStatus", IPMOSStatus != 0 ? IPMOSStatus : 0),
                    new SqlParameter("@SimType", strSIMType));
            }
            Logger.InfoFormat("Exiting {0}.UpdateIMPOSDetails({1})", svcName, RegID);
            return isUpdated;
        }

		public static ExtendedDetailsForDevice getExtendedDetailsSuppForDevice(int RegID)
		{
			Logger.InfoFormat("Entering {0}.getExtendedDetailsSuppForDevice({1})", svcName, RegID);
			List<ExtendedDetailsForDevice> objExtendedDetailsForDevices = null;
			ExtendedDetailsForDevice objExtendedDetailsForDevice = new ExtendedDetailsForDevice();
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				objExtendedDetailsForDevices = ctx.Database.SqlQuery<ExtendedDetailsForDevice>("usp_GetExtedDetailsSuppByRegId @RegID", new SqlParameter("@RegID", RegID)).ToList();

				if (objExtendedDetailsForDevices != null && objExtendedDetailsForDevices.Count > 0)
				{
					foreach (ExtendedDetailsForDevice o in objExtendedDetailsForDevices)
					{


						objExtendedDetailsForDevice.RegID = RegID;

						if (!ReferenceEquals(o.IMEINumber, null))
						{
							objExtendedDetailsForDevice.IMEINumber = o.IMEINumber;
						}
						else
						{
							objExtendedDetailsForDevice.IMEINumber = "";
						}
						if (!ReferenceEquals(o.AdvancePrice, null))
						{
							objExtendedDetailsForDevice.AdvancePrice = o.AdvancePrice;
						}
						else
						{
							objExtendedDetailsForDevice.AdvancePrice = 0.0M;
						}
						if (!ReferenceEquals(o.Price, null))

							objExtendedDetailsForDevice.Price = o.Price;

						else
							objExtendedDetailsForDevice.Price = 0.0M;
						if (!ReferenceEquals(o.Name, null))

							objExtendedDetailsForDevice.Name = o.Name;

						else
							objExtendedDetailsForDevice.Name = "";

					}
				}

			}
			Logger.InfoFormat("Exiting {0}.getExtendedDetailsSuppForDevice({1})", svcName, RegID);
			return objExtendedDetailsForDevice;

		}


        public static ExtendedDetailsForDevice getExtendedDetailsForDevice(int RegID)
        {
            Logger.InfoFormat("Entering {0}.getExtendedDetailsForDevice({1})", svcName, RegID);
            List<ExtendedDetailsForDevice> objExtendedDetailsForDevices = null;
            ExtendedDetailsForDevice objExtendedDetailsForDevice = new ExtendedDetailsForDevice();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objExtendedDetailsForDevices = ctx.Database.SqlQuery<ExtendedDetailsForDevice>("usp_GetExtedDetailsByRegId @RegID", new SqlParameter("@RegID", RegID)).ToList();

                if (objExtendedDetailsForDevices != null && objExtendedDetailsForDevices.Count > 0)
                {
                    foreach (ExtendedDetailsForDevice o in objExtendedDetailsForDevices)
                    {


                        objExtendedDetailsForDevice.RegID = RegID;

                        if (!ReferenceEquals(o.IMEINumber, null))
                        {
                            objExtendedDetailsForDevice.IMEINumber = o.IMEINumber;
                        }
                        else
                        {
                            objExtendedDetailsForDevice.IMEINumber = "";
                        }
                        if (!ReferenceEquals(o.AdvancePrice, null))
                        {
                            objExtendedDetailsForDevice.AdvancePrice = o.AdvancePrice;
                        }
                        else
                        {
                            objExtendedDetailsForDevice.AdvancePrice = 0.0M;
                        }
                        if (!ReferenceEquals(o.Price, null))

                            objExtendedDetailsForDevice.Price = o.Price;

                        else
                            objExtendedDetailsForDevice.Price = 0.0M;
                        if (!ReferenceEquals(o.Name, null))

                            objExtendedDetailsForDevice.Name = o.Name;

                        else
                            objExtendedDetailsForDevice.Name = "";

                    }
                }

            }
            Logger.InfoFormat("Exiting {0}.getExtendedDetailsForDevice({1})", svcName, RegID);
            return objExtendedDetailsForDevice;

        }

        public static ExtendedDetailsForDevice getExtendedDetailsForDevice_Smart(int RegID)
        {
            Logger.InfoFormat("Entering {0}.getExtendedDetailsForDevice({1})", svcName, RegID);
            List<ExtendedDetailsForDevice> objExtendedDetailsForDevices = null;
            ExtendedDetailsForDevice objExtendedDetailsForDevice = new ExtendedDetailsForDevice();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objExtendedDetailsForDevices = ctx.Database.SqlQuery<ExtendedDetailsForDevice>("usp_GetExtedDetailsByRegId_Smart @RegID", new SqlParameter("@RegID", RegID)).ToList();

                if (objExtendedDetailsForDevices != null && objExtendedDetailsForDevices.Count > 0)
                {
                    foreach (ExtendedDetailsForDevice o in objExtendedDetailsForDevices)
                    {


                        objExtendedDetailsForDevice.RegID = RegID;

                        if (!ReferenceEquals(o.IMEINumber, null))
                        {
                            objExtendedDetailsForDevice.IMEINumber = o.IMEINumber;
                        }
                        else
                        {
                            objExtendedDetailsForDevice.IMEINumber = "";
                        }
                        if (!ReferenceEquals(o.AdvancePrice, null))
                        {
                            objExtendedDetailsForDevice.AdvancePrice = o.AdvancePrice;
                        }
                        else
                        {
                            objExtendedDetailsForDevice.AdvancePrice = 0.0M;
                        }
                        if (!ReferenceEquals(o.Price, null))

                            objExtendedDetailsForDevice.Price = o.Price;

                        else
                            objExtendedDetailsForDevice.Price = 0.0M;
                        if (!ReferenceEquals(o.Name, null))

                            objExtendedDetailsForDevice.Name = o.Name;

                        else
                            objExtendedDetailsForDevice.Name = "";

                    }
                }

            }
            Logger.InfoFormat("Exiting {0}.getExtendedDetailsForDevice({1}): {2}", svcName, RegID);
            return objExtendedDetailsForDevice;

        }

        public static ExtendedDetailsForDevice getExtendedDetailsForDevice_MISM(int RegID)
        {
            Logger.InfoFormat("Entering {0}.getExtendedDetailsForDevice_MISM({1})", svcName, RegID);
            List<ExtendedDetailsForDevice> objExtendedDetailsForDevices = null;
            ExtendedDetailsForDevice objExtendedDetailsForDevice = new ExtendedDetailsForDevice();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objExtendedDetailsForDevices = ctx.Database.SqlQuery<ExtendedDetailsForDevice>("usp_GetExtedDetailsByRegId_MISM @RegID", new SqlParameter("@RegID", RegID)).ToList();

                if (objExtendedDetailsForDevices != null && objExtendedDetailsForDevices.Count > 0)
                {
                    foreach (ExtendedDetailsForDevice o in objExtendedDetailsForDevices)
                    {


                        objExtendedDetailsForDevice.RegID = RegID;

                        if (!ReferenceEquals(o.IMEINumber, null))
                        {
                            objExtendedDetailsForDevice.IMEINumber = o.IMEINumber;
                        }
                        else
                        {
                            objExtendedDetailsForDevice.IMEINumber = "";
                        }
                        if (!ReferenceEquals(o.AdvancePrice, null))
                        {
                            objExtendedDetailsForDevice.AdvancePrice = o.AdvancePrice;
                        }
                        else
                        {
                            objExtendedDetailsForDevice.AdvancePrice = 0.0M;
                        }
                        if (!ReferenceEquals(o.Price, null))

                            objExtendedDetailsForDevice.Price = o.Price;

                        else
                            objExtendedDetailsForDevice.Price = 0.0M;
                        if (!ReferenceEquals(o.Name, null))

                            objExtendedDetailsForDevice.Name = o.Name;

                        else
                            objExtendedDetailsForDevice.Name = "";

                    }
                }

            }
            Logger.InfoFormat("Exiting {0}.getExtendedDetailsForDevice_MISM({1})", svcName, RegID);
            return objExtendedDetailsForDevice;

        }
        #endregion

        public static List<int> GetOfferID(string ArticleId)
        {
            List<int> value = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                var query = ctx.ModelOffer.Where(a => a.ArticleId != null);
                if (!string.IsNullOrEmpty(ArticleId))
                    query = query.Where(a => a.ArticleId == ArticleId);


                value = query.Select((a) => a.OfferID).ToList();
            }
            return value;
        }


        public static List<int> GetAllDependentComponents()
        {
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                return ctx.DependentComponentsNew.Select(x => x.DependentComponentId).Distinct().ToList();
            }
        }
        public static List<int> GetAllDependentcomponents(int componentId, int planId)
        {
            planId = 0;
            List<int> value = new List<int>();
            List<int> kenanCodes = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                int componentKenanCode = Convert.ToInt32(ctx.PgmBdlPckComponents.Where(c => c.ID == componentId).Select(p => p.KenanCode).FirstOrDefault());
                int planKenanCode = Convert.ToInt32(ctx.PgmBdlPckComponents.Where(c => c.ID == planId).Select(p => p.KenanCode).FirstOrDefault());
                //if (componentId > 0 && planId > 0)
                //    value = ctx.DependentComponents.Where(a => a.MainComponentId == componentId && a.PlanId == planId && a.Status == true).Select((a) => a.DependentComponentId).ToList();
                //else if (componentId > 0)
                //    value = ctx.DependentComponents.Where(a => a.MainComponentId == componentId && a.Status == true).Select((a) => a.DependentComponentId).ToList();
                if (componentKenanCode > 0 && planKenanCode > 0 && planKenanCode != null && componentKenanCode != null)
                    kenanCodes = ctx.DependentComponentsNew.Where(a => a.MainComponentId == componentKenanCode && a.PlanId == planKenanCode && a.Status == true).Select((a) => a.DependentComponentId).ToList();
                else if (componentKenanCode > 0 && componentKenanCode != null)
                    kenanCodes = ctx.DependentComponentsNew.Where(a => a.MainComponentId == componentKenanCode && a.Status == true).Select((a) => a.DependentComponentId).ToList();
                foreach (var code in kenanCodes)
                {
                    string strCode = code.ToString();
                    int pbpcId = ctx.PgmBdlPckComponents.Where(c => c.KenanCode == strCode).Select(p => p.ID).FirstOrDefault();
                    if (pbpcId > 0 && pbpcId != null)
                        value.Add(pbpcId);
                }
            }
            return value;
        }

        public static List<int> GetAllSmartDependentcomponents(int componentId, int planId)
        {
            planId = 0;
            List<int> value = new List<int>();
            List<int> kenanCodes = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                int componentKenanCode = Convert.ToInt32(ctx.PgmBdlPckComponentsSmart.Where(c => c.ID == componentId).Select(p => p.KenanCode).FirstOrDefault());
                int planKenanCode = Convert.ToInt32(ctx.PgmBdlPckComponentsSmart.Where(c => c.ID == planId).Select(p => p.KenanCode).FirstOrDefault());

                if (componentKenanCode > 0 && planKenanCode > 0 && planKenanCode != null && componentKenanCode != null)
                    kenanCodes = ctx.DependentComponentsNew.Where(a => a.MainComponentId == componentKenanCode && a.PlanId == planKenanCode && a.Status == true).Select((a) => a.DependentComponentId).ToList();
                else if (componentKenanCode > 0 && componentKenanCode != null)
                    kenanCodes = ctx.DependentComponentsNew.Where(a => a.MainComponentId == componentKenanCode && a.Status == true).Select((a) => a.DependentComponentId).ToList();
                foreach (var code in kenanCodes)
                {
                    string strCode = code.ToString();
                    int pbpcId = ctx.PgmBdlPckComponentsSmart.Where(c => c.KenanCode == strCode).Select(p => p.ID).FirstOrDefault();
                    if (pbpcId > 0 && pbpcId != null)
                        value.Add(pbpcId);
                }
            }
            return value;
        }

        public static List<int> GetAllDependentcomponents(List<int> componentIds, int planId)
        {
            DateTime stDate = DateTime.Now;
            planId = 0;
            List<int> value = new List<int>();
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsObjects = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GETALLDEPENDENTCOMPONENTS", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@compid", string.Join(",", componentIds)));
                sqlCmd.Parameters.Add(new SqlParameter("@planid", planId));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsObjects);
                sqlConn.Close();
                if (dsObjects != null)
                {
                    if (dsObjects.Tables[0] != null)
                    {
                        for (int i = 0; i < dsObjects.Tables[0].Rows.Count; i++)
                        {
                            if (dsObjects.Tables[0].Rows[i]["id"] != DBNull.Value && dsObjects.Tables[0].Rows[i]["id"] != null)
                                value.Add(Convert.ToInt32(dsObjects.Tables[0].Rows[i]["id"]));
                        }
                    }
                }
            }
            TimeSpan timeSpn = (DateTime.Now - stDate);
            Common.LogMessage(string.Format("{0}.GetAllDependentcomponents({1}) TimeSpan:{2}", svcName, string.Join(",", componentIds), timeSpn.ToString("hh\\:mm\\:ss\\.fff")));
            return value;
        }

        public static MOCOfferDetails getMOCOfferDetails(int OfferID)
        {
            Logger.InfoFormat("Entering {0}.getMOCOfferDetails({1})", svcName, OfferID);
            List<MOCOfferDetails> lstMOCOfferDetails = null;
            MOCOfferDetails objMOCOfferDetails = new MOCOfferDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                lstMOCOfferDetails = ctx.Database.SqlQuery<MOCOfferDetails>("usp_GetMOCOfferDetails @OfferID", new SqlParameter("@OfferID", OfferID)).ToList();

                if (lstMOCOfferDetails != null && lstMOCOfferDetails.Count > 0)
                {
                    foreach (MOCOfferDetails o in lstMOCOfferDetails)
                    {

                        if (!ReferenceEquals(o.ID, null))
                        {
                            objMOCOfferDetails.ID = o.ID;
                        }
                        else
                        {
                            objMOCOfferDetails.ID = 0;
                        }
                        if (!ReferenceEquals(o.Name, null))
                        {
                            objMOCOfferDetails.Name = o.Name;
                        }
                        else
                        {
                            objMOCOfferDetails.Name = string.Empty;
                        }
                        if (!ReferenceEquals(o.OfferDescription, null))
                        {
                            objMOCOfferDetails.OfferDescription = o.OfferDescription;
                        }
                        else
                        {
                            objMOCOfferDetails.OfferDescription = string.Empty;
                        }
                        if (!ReferenceEquals(o.DiscountAmount, null))

                            objMOCOfferDetails.DiscountAmount = o.DiscountAmount;

                        else
                            objMOCOfferDetails.DiscountAmount = 0.0;
                        if (!ReferenceEquals(o.StartDate, null))
                        {
                            objMOCOfferDetails.StartDate = o.StartDate;
                        }
                        else
                        {
                            objMOCOfferDetails.StartDate = DateTime.MinValue;
                        }
                        if (!ReferenceEquals(o.EndDate, null))
                        {
                            objMOCOfferDetails.EndDate = o.EndDate;
                        }
                        else
                        {
                            objMOCOfferDetails.EndDate = DateTime.MinValue;
                        }

                    }
                }

            }
            Logger.InfoFormat("Exiting {0}.getMOCOfferDetails({1})", svcName, OfferID);
            return objMOCOfferDetails;

        }
        public static int SaveKenanXmlLogs(KenanaLogDetails objKenanaLogDetails)
        {
            // int i = -1;
            Logger.InfoFormat("Entering {0}.SaveKenanXmlLogs({1})", svcName, objKenanaLogDetails.RegID);

            //  List<int> value = null;
            var NewID = 0;

            try
            {
                //using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                //{
                //    // ctx.KenanaLogDetails.Add(objKenanaLogDetails);
                //    //i = ctx.SaveChanges();
                //    value = ctx.Database.SqlQuery<int>("AddKenanXMLLogs @RegID,@MsgCode,@MsgDesc,@MethodName,@KenanXmlReq,@KenanXmlRes", 
                //        new SqlParameter("RegID", objKenanaLogDetails.RegID), 
                //        new SqlParameter("MsgCode", objKenanaLogDetails.MessageCode), 
                //        new SqlParameter("MsgDesc", objKenanaLogDetails.MessageDesc), 
                //        new SqlParameter("MethodName", objKenanaLogDetails.MethodName), 
                //        new SqlParameter("KenanXmlReq", objKenanaLogDetails.KenanXmlReq),
                //        new SqlParameter("KenanXmlRes", objKenanaLogDetails.KenanXmlRes)).ToList();

                //}


                IEnumerable<int> value = null;
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var objectContext = (ctx as System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext;

                    // Sets the command timeout for all the commands
                    objectContext.CommandTimeout = 60 * 2;

                    value = ctx.Database.SqlQuery<int>("AddKenanXMLLogs @RegID,@MsgCode,@MsgDesc,@MethodName,@KenanXmlReq,@KenanXmlRes",
                      new SqlParameter("RegID", objKenanaLogDetails.RegID),
                        new SqlParameter("MsgCode", objKenanaLogDetails.MessageCode),
                        new SqlParameter("MsgDesc", objKenanaLogDetails.MessageDesc),
                        new SqlParameter("MethodName", objKenanaLogDetails.MethodName),
                        new SqlParameter("KenanXmlReq", objKenanaLogDetails.KenanXmlReq),
                        new SqlParameter("KenanXmlRes", objKenanaLogDetails.KenanXmlRes));
                    NewID = value.Single();
                }
            }
            catch (Exception ex)
            {

                //throw ex;
                Logger.Info("error while executing SaveKenanXmlLogs" + ex);
            }

            Logger.InfoFormat("Exiting {0}.SaveKenanXmlLogs({1})", svcName, objKenanaLogDetails.RegID);
            // return value.FirstOrDefault();
            return NewID;
        }

		public static int SavePegaXMLLogs(PegaLogDetails objPegaDetails)
		{
			Logger.InfoFormat("Entering {0}.SavePegaXMLLogs({1})", svcName, objPegaDetails.Msisdn);

			var NewID = 0;

			try
			{
				IEnumerable<int> value = null;
				using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
				{
					var objectContext = (ctx as System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext;

					// Sets the command timeout for all the commands
					objectContext.CommandTimeout = 60 * 2;

					value = ctx.Database.SqlQuery<int>("AddPegaXMLLogs @SalesPerson, @MethodName, @Msisdn, @SubscriberNo, @PegaXmlReq, @PegaXmlRes, @Remarks",
					  new SqlParameter("SalesPerson", objPegaDetails.SalesPerson),
						new SqlParameter("MethodName", objPegaDetails.MethodName),
						new SqlParameter("Msisdn", objPegaDetails.Msisdn),
						new SqlParameter("SubscriberNo", objPegaDetails.SubscriberNo),
						new SqlParameter("PegaXmlReq", objPegaDetails.PegaXmlReq),
						new SqlParameter("PegaXmlRes", objPegaDetails.PegaXmlRes),
						new SqlParameter("Remarks", !string.IsNullOrEmpty(objPegaDetails.Remarks) ? objPegaDetails.Remarks : "")
						);
					NewID = value.Single();
				}
			}
			catch (Exception ex)
			{

				//throw ex;
				Logger.Info("error while executing SavePegaXMLLogs" + ex);
			}

			//Logger.InfoFormat("Exiting {0}.SavePegaXMLLogs({1})", svcName);
			// return value.FirstOrDefault();
			return NewID;
		}


        public static List<lnkofferuomarticleprice> GetOffersfromUOMArticle(string ArticleId, string planID, string contractID, string MOCStatus)
        {
            List<lnkofferuomarticleprice> objUOMArticleDetails = null;
            List<lnkofferuomarticleprice> objUOMArticleDetails1 = new List<lnkofferuomarticleprice>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objUOMArticleDetails = ctx.Database.SqlQuery<lnkofferuomarticleprice>("USP_GetOfferUomArticlePrice @ArticleID,@planID,@lnkPgmBdlPkgCompID,@MOCStatus", new SqlParameter("@ArticleId", ArticleId), new SqlParameter("@planID", planID), new SqlParameter("@lnkPgmBdlPkgCompID", contractID), new SqlParameter("@MOCStatus", MOCStatus)).ToList();

                string[] list = new string[objUOMArticleDetails.Count];
                foreach (lnkofferuomarticleprice lnk in objUOMArticleDetails)
                {
                    if (!list.Contains(lnk.OfferName))
                    {
                        objUOMArticleDetails1.Add(lnk);
                        //added by Chodey
                        list[objUOMArticleDetails1.Count - 1] = lnk.OfferName;
                    }
                }
            }
            return objUOMArticleDetails1;

        }

        public static List<lnkofferuomarticleprice> GetOffersfromUOMArticle2(string ArticleId, string PackageKenanID, string MOCStatus, string contractKenanIDs)
        {
            List<lnkofferuomarticleprice> objUOMArticleDetails = null;
            List<lnkofferuomarticleprice> objUOMArticleDetails1 = new List<lnkofferuomarticleprice>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objUOMArticleDetails = ctx.Database.SqlQuery<lnkofferuomarticleprice>("USP_GetOfferUomArticle @ArticleId,@PkgKenanID,@MOCStatus,@contractKenanIDs", new SqlParameter("@ArticleId", ArticleId), new SqlParameter("@PkgKenanID", PackageKenanID), new SqlParameter("@MOCStatus", MOCStatus), new SqlParameter("@contractKenanIDs", contractKenanIDs)).ToList();
                string[] list = new string[objUOMArticleDetails.Count];
                foreach (lnkofferuomarticleprice lnk in objUOMArticleDetails)
                {
                    if (!list.Contains(lnk.OfferName))
                    {
                        objUOMArticleDetails1.Add(lnk);
                        list[objUOMArticleDetails1.Count - 1] = lnk.OfferName;
                    }
                }
            }
            return objUOMArticleDetails1;
        }

        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid

        public static List<string> GetContractsForArticleId(string articleId, string planId, string mocStatus)
        {
            List<string> objContracts = new List<string>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                if (mocStatus != null && mocStatus != string.Empty)
                    objContracts = ctx.lnkofferuomarticleprice.Where(a => a.ArticleId == articleId && a.KenanPackageId == planId && !(a.OfferName.Contains("WBO")) && a.ToDateTime >= System.DateTime.Now).Select(a => a.KenanComponent1Id).Distinct().ToList();
                else
                    objContracts = ctx.lnkofferuomarticleprice.Where(a => a.ArticleId == articleId && a.KenanPackageId == planId && !(a.OfferName.Contains("WBO")) && !(a.OfferName.Contains("MOC")) && a.ToDateTime >= System.DateTime.Now).Select(a => a.KenanComponent1Id).Distinct().ToList();


            }
            return objContracts;

        }

        public static List<string> GetPackagesForArticleId(string articleId, string mocStatus)
        {
            List<string> objPackages = new List<string>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                if (mocStatus != null && mocStatus != string.Empty)
                    objPackages = ctx.lnkofferuomarticleprice.Where(a => a.ArticleId == articleId && !(a.OfferName.Contains("WBO")) && a.ToDateTime >= System.DateTime.Now).Select(a => a.KenanPackageId).Distinct().ToList();
                else
                    objPackages = ctx.lnkofferuomarticleprice.Where(a => a.ArticleId == articleId && !(a.OfferName.Contains("WBO")) && !(a.OfferName.Contains("MOC")) && a.ToDateTime >= System.DateTime.Now).Select(a => a.KenanPackageId).Distinct().ToList();

            }
            return objPackages;

        }

        public static List<string> GetLowestContractPriceFromArticleId(string articleId, string mocStatus)
        {
            List<string> objPackages = new List<string>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
               objPackages = ctx.lnkofferuomarticleprice.Where(a => a.ArticleId == articleId && !(a.OfferName.Contains("WBO")) && !(a.OfferName.Contains("MOC")) && a.ToDateTime >= System.DateTime.Now).Select(a => a.KenanPackageId).Distinct().ToList();
            }
            return objPackages;

        }

        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid


        //Added by VLT on 17 May 2013 to get device image details dynamically
        public static DeviceDetails GetDeviceImageDetails(string articleId, int modelId)
        {
            DeviceDetails objDeviceDetails = new DeviceDetails();
            BrandArticle brandArtcile = new BrandArticle();
            List<DevicePlanData> devicePlanDetails = new List<DevicePlanData>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                devicePlanDetails = ctx.Database.SqlQuery<DevicePlanData>("USP_GetDevicePlanDetails @ArticleID,@ModelId", new SqlParameter("@ArticleId", articleId), new SqlParameter("@ModelId", modelId)).ToList();
                if (devicePlanDetails != null && devicePlanDetails.Count > 0)
                {
                    objDeviceDetails.DevicePlanData = devicePlanDetails;
                }
                if (!string.IsNullOrEmpty(articleId))
                {
                    brandArtcile = BrandArticleGet(articleId);
                }
                if (brandArtcile != null)
                {

                    objDeviceDetails.BrandArticle = brandArtcile;

                }

            }
            return objDeviceDetails;
        }

        public static DeviceDetailsSmart GetDeviceImageDetailsForSmart(string articleId, int modelId)
        {
            DeviceDetailsSmart objDeviceDetails = new DeviceDetailsSmart();
            BrandArticleSmart brandArtcile = new BrandArticleSmart();
            List<DevicePlanData> devicePlanDetails = new List<DevicePlanData>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                devicePlanDetails = ctx.Database.SqlQuery<DevicePlanData>("USP_GetDevicePlanDetails_smart @ArticleID,@ModelId", new SqlParameter("@ArticleId", articleId), new SqlParameter("@ModelId", modelId)).ToList();
                if (devicePlanDetails != null && devicePlanDetails.Count > 0)
                {
                    objDeviceDetails.DevicePlanData = devicePlanDetails;
                }

                brandArtcile = BrandArticleGetSmart(articleId);
                if (brandArtcile != null)
                {

                    objDeviceDetails.BrandArticle = brandArtcile;

                }

            }
            return objDeviceDetails;
        }
        //Added by VLT on 17 May 2013 to get device image details dynamically

        #region get IMPOSSpecDetails for genarate POS file added by Nreddy
        public static IMPOSSpecDetails getIMPOSSpecDetails(string POSKey)
        {
            Logger.InfoFormat("Entering {0}.getIMPOSSpecDetails({1})", svcName, "");
            List<IMPOSSpecDetails> lstIMPOSSpecDetails = null;
            IMPOSSpecDetails objIMPOSSpecDetails = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                lstIMPOSSpecDetails = ctx.IMPOSSpecDetails.Where(a => a.POSKey == POSKey).ToList();
            }
            if (lstIMPOSSpecDetails != null && lstIMPOSSpecDetails.Count > 0)
            {
                objIMPOSSpecDetails = new IMPOSSpecDetails();
                objIMPOSSpecDetails.POSNetworkPath = !ReferenceEquals(lstIMPOSSpecDetails[0].POSNetworkPath, null) ? lstIMPOSSpecDetails[0].POSNetworkPath : string.Empty;
                objIMPOSSpecDetails.POSUserName = !ReferenceEquals(lstIMPOSSpecDetails[0].POSUserName, null) ? lstIMPOSSpecDetails[0].POSUserName : string.Empty;
                objIMPOSSpecDetails.POSPassword = !ReferenceEquals(lstIMPOSSpecDetails[0].POSPassword, null) ? lstIMPOSSpecDetails[0].POSPassword : string.Empty;
                objIMPOSSpecDetails.POSDomain = !ReferenceEquals(lstIMPOSSpecDetails[0].POSDomain, null) ? lstIMPOSSpecDetails[0].POSDomain : string.Empty;
                objIMPOSSpecDetails.IMPOSFilePath = !ReferenceEquals(lstIMPOSSpecDetails[0].IMPOSFilePath, null) ? lstIMPOSSpecDetails[0].IMPOSFilePath : string.Empty;
                objIMPOSSpecDetails.Tx_type = !ReferenceEquals(lstIMPOSSpecDetails[0].Tx_type, null) ? lstIMPOSSpecDetails[0].Tx_type : string.Empty;
                objIMPOSSpecDetails.GSMAdvPayment = !ReferenceEquals(lstIMPOSSpecDetails[0].GSMAdvPayment, null) ? lstIMPOSSpecDetails[0].GSMAdvPayment : string.Empty;
                objIMPOSSpecDetails.GSMRegDeposit = !ReferenceEquals(lstIMPOSSpecDetails[0].GSMRegDeposit, null) ? lstIMPOSSpecDetails[0].GSMRegDeposit : string.Empty;
                objIMPOSSpecDetails.MaxisDeviceDeposit = !ReferenceEquals(lstIMPOSSpecDetails[0].MaxisDeviceDeposit, null) ? lstIMPOSSpecDetails[0].MaxisDeviceDeposit : string.Empty;
                objIMPOSSpecDetails.DepositiValue = !ReferenceEquals(lstIMPOSSpecDetails[0].DepositiValue, null) ? lstIMPOSSpecDetails[0].DepositiValue : string.Empty;
                objIMPOSSpecDetails.UOMIDForAdvPaymntAndSIM = !ReferenceEquals(lstIMPOSSpecDetails[0].UOMIDForAdvPaymntAndSIM, null) ? lstIMPOSSpecDetails[0].UOMIDForAdvPaymntAndSIM : string.Empty;
                objIMPOSSpecDetails.UOMIDForAdvPaymnt = !ReferenceEquals(lstIMPOSSpecDetails[0].UOMIDForAdvPaymnt, null) ? lstIMPOSSpecDetails[0].UOMIDForAdvPaymnt : string.Empty;
                objIMPOSSpecDetails.UOMIDForSIM = !ReferenceEquals(lstIMPOSSpecDetails[0].UOMIDForSIM, null) ? lstIMPOSSpecDetails[0].UOMIDForSIM : string.Empty;
                objIMPOSSpecDetails.UOMIDForPlanDeposit = !ReferenceEquals(lstIMPOSSpecDetails[0].UOMIDForPlanDeposit, null) ? lstIMPOSSpecDetails[0].UOMIDForPlanDeposit : string.Empty;
                objIMPOSSpecDetails.UOMIDForDeviceDeposit = !ReferenceEquals(lstIMPOSSpecDetails[0].UOMIDForDeviceDeposit, null) ? lstIMPOSSpecDetails[0].UOMIDForDeviceDeposit : string.Empty;
                objIMPOSSpecDetails.UOMIDForInternationalRoaming = !ReferenceEquals(lstIMPOSSpecDetails[0].UOMIDForInternationalRoaming, null) ? lstIMPOSSpecDetails[0].UOMIDForInternationalRoaming : string.Empty;
                objIMPOSSpecDetails.InternationalRoamingArticleId = !ReferenceEquals(lstIMPOSSpecDetails[0].InternationalRoamingArticleId, null) ? lstIMPOSSpecDetails[0].InternationalRoamingArticleId : string.Empty;
                objIMPOSSpecDetails.CallConferencingArticleID = !ReferenceEquals(lstIMPOSSpecDetails[0].CallConferencingArticleID, null) ? lstIMPOSSpecDetails[0].CallConferencingArticleID : string.Empty;
                objIMPOSSpecDetails.UOMIDForCallConferencing = !ReferenceEquals(lstIMPOSSpecDetails[0].UOMIDForCallConferencing, null) ? lstIMPOSSpecDetails[0].UOMIDForCallConferencing : string.Empty;
                objIMPOSSpecDetails.UpfrontPaymentArticleId = !ReferenceEquals(lstIMPOSSpecDetails[0].UpfrontPaymentArticleId, null) ? lstIMPOSSpecDetails[0].UpfrontPaymentArticleId : string.Empty;
                objIMPOSSpecDetails.UOMIDForUpfrontPayment = !ReferenceEquals(lstIMPOSSpecDetails[0].UOMIDForUpfrontPayment, null) ? lstIMPOSSpecDetails[0].UOMIDForUpfrontPayment : string.Empty;
                objIMPOSSpecDetails.UOMCodeForTUT = !ReferenceEquals(lstIMPOSSpecDetails[0].UOMCodeForTUT, null) ? lstIMPOSSpecDetails[0].UOMCodeForTUT : string.Empty;
                objIMPOSSpecDetails.ArticleIDForTUT = !ReferenceEquals(lstIMPOSSpecDetails[0].ArticleIDForTUT, null) ? lstIMPOSSpecDetails[0].ArticleIDForTUT : string.Empty;

            }
            Logger.InfoFormat("Exiting {0}.getIMPOSSpecDetails({1}): {2}", svcName, "", lstIMPOSSpecDetails.Count());
            return objIMPOSSpecDetails;
        }
        #endregion

        #region AddRemoveVAS

        /// <summary>
        /// Used to save the Extra Ten against the Registration Id
        /// </summary>
        /// <param name="oReq"></param>
        /// <returns>integer</returns>
        public static long SaveAddRemoveVAS(AddRemoveVASComponents oReq)
        {

            Logger.InfoFormat("Entering {0}.SaveAddRemoveVAS", svcName);
            long value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                ctx.AddRemoveVASComponents.Add(oReq);
                ctx.SaveChanges();


            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.AddRemoveVASComponents({1})", svcName, oReq.ID);
            return value;
        }

        public static int DeleteAddRemoveVasComponent(int Id)
        {
            Logger.InfoFormat("Entering {0}.DeleteAddRemoveVasComponent({1})", svcName, Id);
            List<int> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_DeleteAddRemoveVASComponentById @Id", new System.Data.SqlClient.SqlParameter("@Id", Id)).ToList();

            }
            Logger.InfoFormat("Exiting {0}.DeleteAddRemoveVasComponent({1})", svcName, Id);
            return value.First();

        }

        public static List<AddRemoveVASComponents> GetAddRemoveVASComponents(int regId)
        {
            Logger.InfoFormat("Entering {0}.GetAddRemoveVASComponents({1})", svcName, regId);
            List<AddRemoveVASComponents> value = new List<AddRemoveVASComponents>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.AddRemoveVASComponents.Where(e => e.RegId == regId).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetAddRemoveVASComponents({1})", svcName, regId);
            return value;

        }

        public static int DeleteAddRemoveVASComponents(int regId)
        {
            Logger.InfoFormat("Entering {0}.DeleteAddRemoveVASComponents({1})", svcName, regId);
            List<int> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_DeleteAddRemoveVASComponentsByRegId @RegId", new System.Data.SqlClient.SqlParameter("@RegId", regId)).ToList();

            }
            Logger.InfoFormat("Exiting {0}.DeleteAddRemoveVASComponents({1})", svcName, regId);
            return value.FirstOrDefault();

        }
        public static int UpdateAddRemoveVASComponent(DAL.Models.AddRemoveVASComponents oReq)
        {
            Logger.InfoFormat("Entering {0}.UpdateAddRemoveVASComponent({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var objExtraTen = ctx.AddRemoveVASComponents.Find(oReq.ID);
                    objExtraTen.ID = oReq.ID;
                    objExtraTen.RegId = oReq.RegId;
                    objExtraTen.Status = oReq.Status;
                    objExtraTen.CreatedDate = System.DateTime.Now;
                    objExtraTen.CreatedBy = oReq.CreatedBy;
                    objExtraTen.MsgCode = oReq.MsgCode;
                    objExtraTen.MsgDesc = oReq.MsgDesc;
                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.UpdateAddRemoveVASComponent({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        #endregion


        //Added by Pavan
        public static DiscountPriceDetail getDiscountPriceDetails(int Modleid, int planid, string contractid, int dataplanid)
        {
            Logger.InfoFormat("Entering {0}.getDiscountPriceDetails({1})", svcName, planid);
            List<DiscountPrices> lstDiscountPriceDetails = null;
            DiscountPriceDetail objDiscountPriceDetails = new DiscountPriceDetail();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                lstDiscountPriceDetails = ctx.Database.SqlQuery<DiscountPrices>("USP_GetDiscountPrices @ModelID,@PlanID,@ContractID,@DataPlanID", new SqlParameter("@ModelID", Modleid), new SqlParameter("@PlanID", planid), new SqlParameter("@ContractID", contractid), new SqlParameter("@DataPlanID", dataplanid)).ToList();

                if (lstDiscountPriceDetails != null && lstDiscountPriceDetails.Count > 0)
                {
                    foreach (DiscountPrices o in lstDiscountPriceDetails)
                    {

                        if (!ReferenceEquals(o.ID, null))
                        {
                            objDiscountPriceDetails.ID = o.ID;
                        }
                        else
                        {
                            objDiscountPriceDetails.ID = 0;
                        }
                        if (!ReferenceEquals(o.NewPlanName, null))
                        {
                            objDiscountPriceDetails.NewPlanName = o.NewPlanName;
                        }
                        else
                        {
                            objDiscountPriceDetails.NewPlanName = string.Empty;
                        }
                        if (!ReferenceEquals(o.OrgPrice, null))
                        {
                            objDiscountPriceDetails.OrgPrice = o.OrgPrice;
                        }
                        else
                        {
                            objDiscountPriceDetails.OrgPrice = string.Empty;
                        }

                        if (!ReferenceEquals(o.DiscountPrice, null))
                        {
                            objDiscountPriceDetails.DiscountPrice = o.DiscountPrice;
                        }
                        else
                        {
                            objDiscountPriceDetails.DiscountPrice = string.Empty;
                        }
                        if (!ReferenceEquals(o.ContractID, null))
                        {
                            objDiscountPriceDetails.ContractID = o.ContractID;
                        }
                        else
                        {
                            objDiscountPriceDetails.ContractID = string.Empty;
                        }
                        if (!ReferenceEquals(o.DataPlanID, null))
                        {
                            objDiscountPriceDetails.DataPlanID = o.DataPlanID;
                        }
                        else
                        {
                            objDiscountPriceDetails.DataPlanID = 0;
                        }
                    }
                }

            }
            Logger.InfoFormat("Exiting {0}.getMOCOfferDetails({1})", svcName, planid);
            return objDiscountPriceDetails;

        }
        //Added by Pavan

        #region CRP
        public static List<PgmBdlPckComponent> ProgramBundlePackageComponentGetForSmartByKenanCode(List<string> IDs)
        {
            List<PgmBdlPckComponent> PgmBdlPckComponentSmartList = new List<PgmBdlPckComponent>();
            List<Component> listComponent = new List<Component>();

            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    //PgmBdlPckComponentSmartList = ctx.PgmBdlPckComponentsSmart.Where(a => IDs.Contains(a.KenanCode) && a.Active == true && a.PlanType == "BP" && (a.PlanType == "CP" || a.PlanType == "MP" || a.PlanType == "EP" || a.PlanType == "DP")).ToList();
                    PgmBdlPckComponentSmartList = ctx.PgmBdlPckComponents.Where(a => IDs.Contains(a.KenanCode) && a.Active == true).ToList();

                    listComponent = ctx.Components.ToList();


                    //PgmBdlPckComponentSmartList.Select(c => { c.Description = ctx.Components.Where(a => a.Code == c.Code) == null ? "" : ctx.Components.Where(a => a.Code == c.Code).SingleOrDefault().Description; return c; }).ToList();
                }

                if (PgmBdlPckComponentSmartList != null && PgmBdlPckComponentSmartList.Count > 0)
                {
                    foreach (var item in PgmBdlPckComponentSmartList)
                    {
                        item.Description = listComponent.FirstOrDefault(a => a.Code == item.Code) == null ? item.Description : listComponent.FirstOrDefault(a => a.Code == item.Code).Description;
                    }
                }

            }
            catch (Exception ex)
            {

                PgmBdlPckComponentSmartList = null;
            }


            return PgmBdlPckComponentSmartList;
        }
        public static List<ModelPackageInfo> GetModelPackageInfo_smart(int ModelId, int packageId, string KenanCode, bool isSmart = false)
        {
            List<ModelPackageInfo> lstModelPackageInfo = null;
            ModelPackageInfo ObjModelPackageInfo = new ModelPackageInfo();

            if (isSmart)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    lstModelPackageInfo = ctx.Database.SqlQuery<ModelPackageInfo>("usp_getModelPackageInfo_smart @packageId,@KenanCode,@ModelId",
                        new SqlParameter("@packageId", packageId),
                        new SqlParameter("@KenanCode", KenanCode),
                        new SqlParameter("@ModelId", ModelId)).ToList();

                }
            }
            else
            {

                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    lstModelPackageInfo = ctx.Database.SqlQuery<ModelPackageInfo>("usp_getModelPackageInfo_CRP @packageId,@KenanCode,@ModelId",
                        new SqlParameter("@packageId", packageId),
                        new SqlParameter("@KenanCode", KenanCode),
                        new SqlParameter("@ModelId", ModelId)).ToList();

                }
            }

            return lstModelPackageInfo;
        }
        public static List<string> ExtendPlanBrand(ContractPlans oReq)
        {
            List<string> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ContractPlans.Where(a => a.Active == true);

                value = query.Select((a) => a.PlanName).ToList();
            }
            Logger.InfoFormat("Exiting {0}.BrandFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        #region GetSelectedComponets

        public static List<RegSmartComponents> GetSelectedComponents(string regId)
        {
            List<RegSmartComponents> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //value = ctx.RegSmartComponents.Where(a => a.RegID == regId).ToList();
                //value = ctx.Database.SqlQuery<RegSmartComponents>("usp_getRegVASComponents_CRP @RegID", new SqlParameter("@RegID", regId)).ToList();
                value = ctx.RegSmartComponents.Where(a => a.RegID == regId).ToList();
            }
            return value;
        }


        public static List<RegSmartComponents> GetSelectedComponentsForCRPPlan(string regId)
        {
            List<RegSmartComponents> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegSmartComponents.Where(a => a.RegID == regId).ToList();
                // value = ctx.Database.SqlQuery<RegSmartComponents>("usp_getRegVASComponents_CRP @RegID", new SqlParameter("@RegID", regId)).ToList();
            }
            return value;
        }
        #endregion

        #endregion

        #region CRP


        public static List<string> ExtendBrand(ContractBundleModels oReq)
        {
            List<string> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.BrandContracts.Where(a => a.Active == true);

                value = query.Select((a) => a.ContractBrandModel).ToList();
            }
            Logger.InfoFormat("Exiting {0}.BrandFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }

        public static List<int> GetExtendedPackages(int Modelid)
        {
            List<int> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //value = ctx.RegSmartComponents.Where(a => a.RegID == regId).ToList();
                value = ctx.Database.SqlQuery<int>("usp_GetExtendedPackageIds @ModelId", new SqlParameter("@ModelId", Modelid)).ToList();
            }
            return value;
        }

        public static List<Model> GetExtendedModelsDevice(int brandid, string type)
        {
            List<Model> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //value = ctx.RegSmartComponents.Where(a => a.RegID == regId).ToList();
                value = ctx.Database.SqlQuery<Model>("sp_ModelsExtendCrp @brandid,@type", new SqlParameter("@brandid", brandid), new SqlParameter("@type", type)).ToList();
            }
            return value;
        }


        public static List<ContractDataplan> GetContractDataPlan(int dataPlanid)
        {
            List<ContractDataplan> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //value = ctx.RegSmartComponents.Where(a => a.RegID == regId).ToList();
                value = ctx.Database.SqlQuery<ContractDataplan>("USP_ContractDataplan @dataplanid", new SqlParameter("@dataplanid", dataPlanid)).ToList();
            }
            return value;
        }


        #endregion

        //added by Nreddy for geeting contract type
        public static string getNRCByRegID(int RegID)
        {
            Logger.InfoFormat("Entering {0}.getNRCByRegID({1})", svcName, RegID);
            string strNRCId = string.Empty;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                strNRCId = ctx.Database.SqlQuery<string>("usp_getRegNRICId @RegID", new SqlParameter("@RegID", RegID)).SingleOrDefault();

            }
            Logger.InfoFormat("Exiting {0}.getRegNRICId({1}): ", svcName, RegID);
            return strNRCId;
        }


        public static List<RestrictedComponent> GetAllRestrictedComponents()
        {
            List<RestrictedComponent> lstRestrictedComponents = new List<RestrictedComponent>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                lstRestrictedComponents = (from pgmcomps in ctx.PgmBdlPckComponents
                                           join comps in ctx.Components on pgmcomps.ChildID equals comps.ID
                                           where comps.RestricttoKenan == true && pgmcomps.PlanType == null
                                           select new RestrictedComponent { ComponentId = comps.ID, KenanCode = pgmcomps.KenanCode, lnkPgmBdlId = pgmcomps.ID }).Distinct().ToList();

            }
            return lstRestrictedComponents;
        }

        //Changes related to Multiple Contracts CR changed by code--Chetan/ db--Pavan
        public static List<PlanPrices> GetPlanPrice(int _modelID, string _uom)
        {
            List<PlanPrices> PlanPricesDetails = new List<PlanPrices>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                PlanPricesDetails = ctx.Database.SqlQuery<PlanPrices>("USP_GetPlanPrices @ModelID,@UOMCode", new SqlParameter("@ModelID", _modelID), new SqlParameter("@UOMCode", _uom)).ToList();
            }
            return PlanPricesDetails;

        }

        #region smart related methods

        public static IMPOSDetails getIMPOSDetails(int RegID)
        {
            Logger.InfoFormat("Entering {0}.getIMPOSDetails({1})", svcName, RegID);
            List<IMPOSDetails> objIMPOSDetails = null;
            IMPOSDetails objIMPOSDetail = new IMPOSDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<IMPOSDetails>("usp_GetDetailsForIMPOS @RegID", new SqlParameter("@RegID", RegID)).ToList();

                if (objIMPOSDetails != null && objIMPOSDetails.Count > 0)
                {
                    foreach (IMPOSDetails o in objIMPOSDetails)
                    {

                        if (!ReferenceEquals(o.OrganisationId, null))
                        {
                            objIMPOSDetail.OrganisationId = o.OrganisationId;
                        }
                        else
                        {
                            objIMPOSDetail.OrganisationId = string.Empty;
                        }
                        if (!ReferenceEquals(o.ArticleID, null))
                        {
                            objIMPOSDetail.ArticleID = o.ArticleID;
                        }
                        else
                        {
                            objIMPOSDetail.ArticleID = 0.ToString();
                        }
                        if (!ReferenceEquals(o.MSISDN1, null))
                        {
                            objIMPOSDetail.MSISDN1 = o.MSISDN1;
                        }
                        else
                        {
                            objIMPOSDetail.MSISDN1 = string.Empty;
                        }
                        if (!ReferenceEquals(o.KenanAccountNo, null))

                            objIMPOSDetail.KenanAccountNo = o.KenanAccountNo;

                        else
                            objIMPOSDetail.KenanAccountNo = "";
                        if (!ReferenceEquals(o.FullName, null))

                            objIMPOSDetail.FullName = o.FullName;

                        else
                            objIMPOSDetail.FullName = "";
                        if (!ReferenceEquals(o.IDCardNo, null))

                            objIMPOSDetail.IDCardNo = o.IDCardNo;

                        else
                            objIMPOSDetail.IDCardNo = "";
                        if (!ReferenceEquals(o.Line1, null))

                            objIMPOSDetail.Line1 = o.Line1;

                        else
                            objIMPOSDetail.Line1 = "";
                        if (!ReferenceEquals(o.Line2, null))

                            objIMPOSDetail.Line2 = o.Line2;

                        else
                            objIMPOSDetail.Line2 = "";

                        if (!ReferenceEquals(o.Line3, null))

                            objIMPOSDetail.Line3 = o.Line3;

                        else
                            objIMPOSDetail.Line3 = "";


                        if (!ReferenceEquals(o.Town, null))

                            objIMPOSDetail.Town = o.Town;

                        else
                            objIMPOSDetail.Town = "";
                        if (!ReferenceEquals(o.Postcode, null))

                            objIMPOSDetail.Postcode = o.Postcode;

                        else
                            objIMPOSDetail.Postcode = "";
                        if (!ReferenceEquals(o.IMEINumber, null))

                            objIMPOSDetail.IMEINumber = o.IMEINumber;

                        else
                            objIMPOSDetail.IMEINumber = "";
                        if (!ReferenceEquals(o.ModelID, null))

                            objIMPOSDetail.ModelID = Convert.ToInt32(o.ModelID.ToString());

                        else
                            objIMPOSDetail.ModelID = 0;

                        if (!ReferenceEquals(o.UOMCODE, null))
                        {
                            objIMPOSDetail.UOMCODE = o.UOMCODE;
                        }
                        else
                        {
                            objIMPOSDetail.UOMCODE = 0.ToString();
                        }
                        if (!ReferenceEquals(o.SimNo, null))
                        {
                            objIMPOSDetail.SimNo = o.SimNo;
                        }
                        else
                        {
                            objIMPOSDetail.SimNo = 0.ToString();
                        }
                        objIMPOSDetail.RegTypeID = o.RegTypeID > 0 ? o.RegTypeID : 0;
                        objIMPOSDetail.SimModelId = o.SimModelId > 0 ? o.SimModelId : 0;
                        /*Added By Patanjal*/
                        if (!ReferenceEquals(o.PlanAdvance, null))
                        {
                            objIMPOSDetail.PlanAdvance = o.PlanAdvance;
                        }
                        else
                        {
                            objIMPOSDetail.PlanAdvance = 0;
                        }
                        if (!ReferenceEquals(o.Deviceadvance, null))
                        {
                            objIMPOSDetail.Deviceadvance = o.Deviceadvance;
                        }
                        else
                        {
                            objIMPOSDetail.Deviceadvance = 0;
                        }
                        if (!ReferenceEquals(o.PlanDeposit, null))
                        {
                            objIMPOSDetail.PlanDeposit = o.PlanDeposit;
                        }
                        else
                        {
                            objIMPOSDetail.PlanDeposit = 0;
                        }
                        if (!ReferenceEquals(o.Devicedeposit, null))
                        {
                            objIMPOSDetail.Devicedeposit = o.Devicedeposit;
                        }
                        else
                        {
                            objIMPOSDetail.Devicedeposit = 0;
                        }
                        /*ENDHERE*/

                        //Code Added by VLT on May 01 2013
                        if (!ReferenceEquals(o.InternationalRoaming, null))
                        {
                            objIMPOSDetail.InternationalRoaming = o.InternationalRoaming;
                        }
                        else
                        {
                            objIMPOSDetail.InternationalRoaming = false;
                        }

                        if (!ReferenceEquals(o.K2Type, null))
                        {
                            objIMPOSDetail.K2Type = o.K2Type;
                        }
                        else
                        {
                            objIMPOSDetail.K2Type = false;
                        }
                        //End of Code Added by VLT on May 01 2013

                    }
                }

            }
            Logger.InfoFormat("Exiting {0}.getIMPOSDetails({1}): {2}", svcName, RegID);
            return objIMPOSDetail;

        }
        public static List<PgmBdlPckComponentSmart> ProgramBundlePackageComponentGetForSmart(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.ProgramBundlePackageComponentGetForSmart({1})", svcName, (IDs.Count != 0) ? IDs[0] : 0);
            List<PgmBdlPckComponentSmart> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PgmBdlPckComponentsSmart.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ProgramBundlePackageComponentGetForSmart({1}): {2}", svcName, (IDs.Count != 0) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<int> ProgramBundlePackageComponentFindSmart(PgmBdlPckComponentFindSmart oReq)
        {
            Logger.InfoFormat("Entering {0}.ProgramBundlePackageComponentFindSmart({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.PgmBdlPckComponentsSmart.Where(a => a.ID != 0);

                //query = query.Where(a => EntityFunctions.TruncateTime(a.StartDate) <= EntityFunctions.TruncateTime(DateTime.Now) &&
                //            (!a.EndDate.HasValue || EntityFunctions.TruncateTime(a.EndDate.Value) >= EntityFunctions.TruncateTime(DateTime.Now)));

                if (oReq.PgmBdlPckComponentSmart.ID != 0)
                    query = query.Where(a => a.ID == oReq.PgmBdlPckComponentSmart.ID);

                if (oReq.ParentIDs.Count() > 0)
                {
                    query = query.Where(a => oReq.ParentIDs.Contains(a.ParentID));
                }

                if (oReq.ChildIDs.Count() > 0)
                {
                    query = query.Where(a => oReq.ChildIDs.Contains(a.ChildID));
                }

                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponentSmart.LinkType))
                    query = query.Where(a => a.LinkType == oReq.PgmBdlPckComponentSmart.LinkType);

                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponentSmart.PlanType))
                    query = query.Where(a => a.PlanType == oReq.PgmBdlPckComponentSmart.PlanType);

                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponentSmart.Code))
                    query = query.Where(a => a.Code == oReq.PgmBdlPckComponentSmart.Code);

                if (oReq.PgmBdlPckComponentSmart.ParentID != 0)
                    query = query.Where(a => a.ParentID == oReq.PgmBdlPckComponentSmart.ParentID);

                if (oReq.PgmBdlPckComponentSmart.ChildID != 0)
                    query = query.Where(a => a.ChildID == oReq.PgmBdlPckComponentSmart.ChildID);

                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponentSmart.FilterOrgType))
                    query = query.Where(a => ("," + a.FilterOrgType + ",").Contains("," + oReq.PgmBdlPckComponentSmart.FilterOrgType + ","));

                //if (oReq.StartDateFrom.HasValue)
                //    query = query.Where(a => a.StartDate.Date >= oReq.StartDateFrom.Value.Date);

                //if (oReq.StartDateTo.HasValue)
                //    query = query.Where(a => a.StartDate.Date <= oReq.StartDateTo.Value.Date);

                //if (oReq.EndDateFrom.HasValue)
                //    query = query.Where(a => a.EndDate.Value.Date >= oReq.EndDateFrom.Value.Date);

                //if (oReq.EndDateTo.HasValue)
                //    query = query.Where(a => a.EndDate.Value.Date <= oReq.EndDateTo.Value.Date);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);
                //value = query.Select((a) => a.ID).ToList();

                if (oReq.IsMandatory.HasValue)
                    query = query.Where(a => a.IsMandatory == oReq.IsMandatory.Value);

                if (oReq.NeedProvision.HasValue)
                    query = query.Where(a => a.NeedProvision == oReq.NeedProvision.Value);
                //value = query.Select((a) => a.ID).ToList();

                if (oReq.NeedActivation.HasValue)
                    query = query.Where(a => a.NeedActivation == oReq.NeedActivation.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ProgramBundlePackageComponentFindSmart({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<int> ModelGroupModelFind_Smart(ModelGroupModelFind_Smart oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.ModelGroupModels_Smart.Where(a => a.ID != 0);

                if (oReq.ModelGroupModel.ID != 0)
                    query = query.Where(a => a.ID == oReq.ModelGroupModel.ID);

                if (oReq.ModelGroupModel.ModelID != 0)
                    query = query.Where(a => a.ModelID == oReq.ModelGroupModel.ModelID);

                if (oReq.ModelGroupModel.ModelGroupID != 0)
                    query = query.Where(a => a.ModelGroupID == oReq.ModelGroupModel.ModelGroupID);

                if (oReq.ModelGroupIDs.Count() > 0)
                    query = query.Where(a => oReq.ModelGroupIDs.Contains(a.ModelGroupID));

                if (oReq.ModelGroupModel.FulfillmentModeID != 0)
                    query = query.Where(a => a.FulfillmentModeID == oReq.ModelGroupModel.FulfillmentModeID);

                if (!string.IsNullOrEmpty(oReq.ModelGroupModel.Code))
                    query = query.Where(a => a.Code == oReq.ModelGroupModel.Code);

                if (!string.IsNullOrEmpty(oReq.ModelGroupModel.Name))
                    query = query.Where(a => a.Name.Contains(oReq.ModelGroupModel.Name));

                if (oReq.StartDateFrom.HasValue)
                    query = query.Where(a => a.StartDate.Date >= oReq.StartDateFrom.Value.Date);

                if (oReq.StartDateTo.HasValue)
                    query = query.Where(a => a.StartDate.Date <= oReq.StartDateTo.Value.Date);

                if (oReq.EndDateFrom.HasValue)
                    query = query.Where(a => a.EndDate.Value.Date >= oReq.EndDateFrom.Value.Date);

                if (oReq.EndDateTo.HasValue)
                    query = query.Where(a => a.EndDate.Value.Date <= oReq.EndDateTo.Value.Date);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.RecoveryNeeded.HasValue)
                    query = query.Where(a => a.NeedRecovery == oReq.RecoveryNeeded.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.ModelGroupModelFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<ModelGroupModel_Smart> ModelGroupModelGet_Smart(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<ModelGroupModel_Smart> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.ModelGroupModels_Smart.Where(a => IDs.Contains(a.ID)).ToList();

                //foreach (int id in IDs)
                //{
                //    var val = (from a in ctx.ModelGroupModels_Smart
                //                                where a.ID == id
                //                                select a);
                //    var v = val.Select((a) => a.ID == id);
                //    //if (v != null)
                //    //{
                //    //    value.Add(val);
                //    //}
                //}
            }
            Logger.InfoFormat("Exiting {0}.ModelGroupModelGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }
        public static List<BrandSmart> GetBrandSmartsForSupplines()
        {
            //Logger.InfoFormat("Entering {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            List<BrandSmart> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<BrandSmart>("Usp_GetBrandSmartsForSupLines").ToList();

            }
            // Logger.InfoFormat("Exiting {0}.UserInfoLogCreate({1}): {2}", svcName, inputparams.Username);
            return value;
        }
        public static List<ModelGroup> GetModelGroupSmartForSupplines(int Modelid)
        {

            //Logger.InfoFormat("Entering {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            List<ModelGroup> value = null;


            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<ModelGroup>("Usp_GetModelGroupSmartForSupplines @Modelid", new SqlParameter("@Modelid", Modelid)).ToList();

            }
            // Logger.InfoFormat("Exiting {0}.UserInfoLogCreate({1}): {2}", svcName, inputparams.Username);
            return value;
        }
        public static List<ModelGroup> PackagesCRPSmartGet(int Modelid, string type)
        {

            //Logger.InfoFormat("Entering {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            List<ModelGroup> value = null;


            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<ModelGroup>("sp_ModelGroupSmartCrpGet @Modelid,@type", new SqlParameter("@Modelid", Modelid), new SqlParameter("@type", type)).ToList();

            }
            // Logger.InfoFormat("Exiting {0}.UserInfoLogCreate({1}): {2}", svcName, inputparams.Username);
            return value;
        }
        public static List<ModelSmart> GetSmartDevicesForSupplines(int brandid)
        {

            //Logger.InfoFormat("Entering {0}.UserInfoLogCreate({1})", svcName, inputparams.Username);
            List<ModelSmart> value = null;


            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<ModelSmart>("Usp_GetSmartDevicesForSuppLines @brandid", new SqlParameter("@brandid", brandid)).ToList();

            }
            // Logger.InfoFormat("Exiting {0}.UserInfoLogCreate({1}): {2}", svcName, inputparams.Username);
            return value;
        }

        //gets contract Duration by kenan code
        public static int GetContractDurationByKenanCode(string kenanCode)
        {
            int value = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Contracts.Where(a => a.KenanCode == kenanCode);
                value = query.Select((a) => a.Duration).SingleOrDefault().ToInt();
            }
            return value;
        }

        public static List<SimModels> GetModelName(int ArticleId, string simCardType)
        {
            List<SimModels> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.SimModels.Where(a => a.ID != 0);
                value = query.Where(a => a.ArticleId == ArticleId && a.SimCardType == simCardType && a.isActive == true).ToList();
            }
            return value;
        }

        public static List<int> GetSmartExtendedPackages(int Modelid)
        {
            List<int> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //value = ctx.RegSmartComponents.Where(a => a.RegID == regId).ToList();
                value = ctx.Database.SqlQuery<int>("usp_GetSmartExtendedPackageIds @ModelId", new SqlParameter("@ModelId", Modelid)).ToList();
            }
            return value;
        }

        #endregion

        #region Supermarket2 CR

        public static List<VoiceContractDetails> GetVoiceAddedendums(string contractKenanCodes)
        {
            Logger.InfoFormat("Entering {0}.GetVoiceAddedendums({1})", svcName, contractKenanCodes);
            List<VoiceContractDetails> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<VoiceContractDetails>("USP_GETLNKVOICECONTRACTDETAILS @contractKenanCodes", new SqlParameter("@contractKenanCodes", contractKenanCodes)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetVoiceAddedendums({1}): {2}", svcName, contractKenanCodes, value.Count());
            return value;
        }

        #endregion


        #region "Sim Replacement Package Find"
        public static Dictionary<string, string> FindPackageOnKenanCode(string KenanCode)
        {
            Logger.InfoFormat("Entering {0}.FindPackageOnKenanCode({1})", svcName, (KenanCode != null) ? KenanCode.ToString2() : "NULL");
            Dictionary<string, string> value = new Dictionary<string, string>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.PgmBdlPckComponents.Where(a => a.KenanCode == KenanCode && a.LinkType.ToLower() == "bp" && (a.PlanType.ToLower() == "cp" || a.PlanType.ToLower() == "sp") && a.Active == true).
                            Select(a => new { ID = a.ID, ChildID = a.ChildID, KenanCode = a.KenanCode, ParentID = a.ParentID }).ToList();

                if (!ReferenceEquals(query, null) && query.Count > 0)
                {
                    List<int> ChildIDs = query.Select(a => a.ChildID).ToList();
                    var query1 = ctx.Packages.Where(a => ChildIDs.Contains(a.ID) && a.Active == true).Select(i => new { id = i.ID, CompName = i.Name });
                    if (!ReferenceEquals(query1, null) && query1.Count() > 0)
                    {
                        foreach (var mainItem in query)
                        {
                            foreach (var item in query1)
                            {
                                if (mainItem.ChildID.Equals(item.id))
                                {
                                    if (!value.Keys.Contains(mainItem.ID.ToString()))
                                    {
                                        value.Add(mainItem.ID.ToString(), item.CompName.ToString());
                                    }
                                }
                            }
                        }

                    }
                }
            }
            Logger.InfoFormat("Exiting {0}.FindPackageOnKenanCode({1})", svcName, (KenanCode != null) ? KenanCode.ToString2() : "NULL");
            return value;
        }

        public static List<SimReplacementComps> GetComponentsByRegID(int RegID)
        {
            List<SimReplacementComps> Comps = new List<SimReplacementComps>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Comps = ctx.Database.SqlQuery<SimReplacementComps>("usp_getOnlyComponentsByRegID @regID", new SqlParameter("@regID", RegID)).ToList();
            }
            return Comps;
        }

        public static int InsertSimRplComponents(SimRplcCompsRequest request)
        {
            Logger.InfoFormat("Entering {0}.InsertSimRplComponents({1})", svcName, request.RegID);
            IEnumerable<int> value = null;
            var NewID = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("USP_INSERTSIMREPLACECOMPS @compID,@compKenanCode,@status,@respCode,@respMsg,@OrderId,@OrderIdResets,@ModifiedDate,@RegID",
                    new SqlParameter("compID", request.compID),
                    new SqlParameter("compKenanCode", request.compKenanCode != null ? request.compKenanCode : DBNull.Value.ToString()),
                    new SqlParameter("status", request.status != null ? request.status : false),
                    new SqlParameter("respCode", request.respCode != null ? request.respCode : DBNull.Value.ToString()),
                    new SqlParameter("respMsg", request.respMsg != null ? request.respMsg : DBNull.Value.ToString()),
                    new SqlParameter("OrderId", request.OrderId != null ? request.OrderId : DBNull.Value.ToString()),
                    new SqlParameter("OrderIdResets", request.OrderIdResets != null ? request.OrderIdResets : DBNull.Value.ToString()),
                    new SqlParameter("ModifiedDate", request.ModifiedDate != null ? request.ModifiedDate : DateTime.Now),
                    new SqlParameter("RegID", request.RegID != null ? request.RegID : 0));
                //NewID = value.Single(); value.SIN
            }
            Logger.InfoFormat("Exiting {0}.InsertSimRplComponents({1})", svcName, NewID);
            return NewID;
        }

        #endregion



        //CR for auto selection for Discount/Promotion component implementation by chetan
        public static List<string> GetKenanCodeForReassign()
        {
            List<string> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.PgmBdlPckComponents.Where(a => a.IsReassign == true);
                value = query.Select((a) => a.KenanCode).ToList();
            }
            return value;
        }

        public static List<DeviceTypes> GetDeviceType()
        {
            List<DeviceTypes> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.DeviceType.Where(a=> a.Active == true).ToList();
            }
            return value;
        }
        public static List<DeviceCapacity> GetDeviceCapacity()
        {
            List<DeviceCapacity> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.DeviceCapacity.Where(a=> a.Active == true).ToList();
            }
            return value;
        }

        public static List<PriceRange> GetPriceRange()
        {
            List<PriceRange> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PriceRange.Where(a => a.Active == true).ToList();
            }
            return value;
        }

        public static int SaveMNPInventoryDetails(lnkMNPInventoryDetails objlnkMNPInventoryDetails)
        {

            IEnumerable<int> value = null;
            var NewID = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("USP_INSERTMNPINVENTORYDETAILS @EXTERNALID,@ORDERID,@FXACCTNO,@INVDVIEWID,@INVENTORYID,@INVENTORYIDRESETS,@NEWINVDTYPE,@invStatus,@oldInvdType",
                    new SqlParameter("EXTERNALID", objlnkMNPInventoryDetails.externalId != null ? objlnkMNPInventoryDetails.externalId : DBNull.Value.ToString()),
                    new SqlParameter("ORDERID", objlnkMNPInventoryDetails.orderId != null ? objlnkMNPInventoryDetails.orderId : DBNull.Value.ToString()),
                    new SqlParameter("FXACCTNO", objlnkMNPInventoryDetails.fxAcctNo != null ? objlnkMNPInventoryDetails.fxAcctNo : DBNull.Value.ToString()),
                    new SqlParameter("INVDVIEWID", objlnkMNPInventoryDetails.invdViewId != null ? objlnkMNPInventoryDetails.invdViewId : DBNull.Value.ToString()),
                    new SqlParameter("INVENTORYID", objlnkMNPInventoryDetails.inventoryId != null ? objlnkMNPInventoryDetails.inventoryId : DBNull.Value.ToString()),
                    new SqlParameter("INVENTORYIDRESETS", objlnkMNPInventoryDetails.inventoryIdResets != null ? objlnkMNPInventoryDetails.inventoryIdResets : DBNull.Value.ToString()),
                    new SqlParameter("NEWINVDTYPE", objlnkMNPInventoryDetails.newInvdType != null ? objlnkMNPInventoryDetails.newInvdType : DBNull.Value.ToString()),
                    new SqlParameter("invStatus", objlnkMNPInventoryDetails.invStatus != null ? objlnkMNPInventoryDetails.invStatus : DBNull.Value.ToString()),
                    new SqlParameter("oldInvdType", objlnkMNPInventoryDetails.oldInvdType != null ? objlnkMNPInventoryDetails.oldInvdType : DBNull.Value.ToString())
                    );


                NewID = value.Single();
            }
            return NewID;

        }

        public static List<lnkMNPInventoryDetails> GetMNPInventoryDetails(string regId)
        {
            List<lnkMNPInventoryDetails> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<lnkMNPInventoryDetails>("USP_GETMNPINVENTORYDETAILS  @REGID", new SqlParameter("@REGID", regId)).ToList();
            }
            return value;
        }
        #region Added to get SIM Types
        public static List<SIMCardTypes> GetSIMCardTypes()
        {
            Logger.InfoFormat("Entering {0}.GetSIMTypes({1})", svcName, "");
            List<SIMCardTypes> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.SIMCardTypes.Where(a => a.Active == true).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetSIMTypes({1}): {2}", svcName, "", value.Count());
            return value;
        }
        #endregion

        public static List<Region> GetRegion()
        {
            Logger.InfoFormat("Entering {0}.GetRegion({1})", svcName, "");
            List<Region> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Region.Where(a => a.Active == true).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetRegion({1}): {2}", svcName, "", value.Count());
            return value;
        }

        public static List<refLovList> GetLovList(string lovtype)
        {
            Logger.InfoFormat("Entering {0}.GetLovList({1})", svcName, "");
            List<refLovList> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.LOV.Where(a => a.Active == true && a.Type == lovtype ).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetLovList({1}): {2}", svcName, "", value.Count());
            return value;
        }

		public static List<refLovList> GetAllLovList()
		{
			Logger.InfoFormat("Entering {0}.GetAllLovList({1})", svcName, "");
			List<refLovList> value = null;
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				value = ctx.LOV.Where(a => a.Active == true).ToList();
			}
			Logger.InfoFormat("Exiting {0}.GetAllLovList({1}): {2}", svcName, "", value.Count());
			return value;
		}

        public static List<lnkAcctMktCodePackages> GetPlansbyMarketandAccountCategory(int AcctCategory, int MarketCode)
        {
            Logger.InfoFormat("Entering {0}.GetPlansbyMarketandAccountCategory({1})", svcName, string.Empty);
            List<lnkAcctMktCodePackages> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.AccountMarketPackages.Where(a => a.AcctCategory == AcctCategory && a.MarketCode == MarketCode).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetPlansbyMarketandAccountCategory({1}): {2}", svcName, string.Empty, value.Count());
            return value;
        }

		public static List<lnkAcctMktCodeOffers> GetOffersbyMarketandAccountCategory(int AcctCategory, int MarketCode)
        {
            Logger.InfoFormat("Entering {0}.GetOffersbyMarketandAccountCategory({1})", svcName, string.Empty);
            List<lnkAcctMktCodeOffers> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.AccountMarketOffers.Where(a => a.AcctCategory == AcctCategory && a.MarketCode == MarketCode).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetOffersbyMarketandAccountCategory({1}): {2}", svcName, string.Empty, value.Count());
            return value;
        }

		public static List<DeviceFinancingInfo> GetAllDeviceFinancingInfo()
		{
			Logger.InfoFormat("Entering {0}.GetDeviceFinancingInfo({1})", svcName, string.Empty);
			List<DeviceFinancingInfo> value = null;

			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				value = ctx.DeviceFinancingInfo.Where(x => x.OfferToDateTime >= System.DateTime.Now).ToList();
			}
			Logger.InfoFormat("Exiting {0}.GetDeviceFinancingInfo({1}): {2}", svcName, string.Empty, value.Count());
			return value;
		}

		public static List<DeviceFinancingInfo> GetDeviceFinancingInfoByArticleIDs(List<string> _articleIDs)
		{
			Logger.InfoFormat("Entering {0}.GetDeviceFinancingInfo({1})", svcName, string.Empty);
			List<DeviceFinancingInfo> value = null;

			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				value = ctx.DeviceFinancingInfo.Where(x => x.OfferToDateTime >= System.DateTime.Now && _articleIDs.Contains(x.ArticleID)).ToList();
			}
			Logger.InfoFormat("Exiting {0}.GetDeviceFinancingInfo({1}): {2}", svcName, string.Empty, value.Count());
			return value;
		}

		public static List<DeviceFinancingInfo> GetDeviceFinancingInfoByArticleID(string _articleID, int _planID)
		{
			Logger.InfoFormat("Entering {0}.GetDeviceFinancingInfo({1})", svcName, string.Empty);
			List<DeviceFinancingInfo> value = null;

			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				if (_planID == 0)
					value = ctx.DeviceFinancingInfo.Where(x => x.OfferToDateTime >= System.DateTime.Now && x.ArticleID == _articleID).ToList();
				else
					value = ctx.DeviceFinancingInfo.Where(x => x.OfferToDateTime >= System.DateTime.Now && x.ArticleID == _articleID && x.PlanID == _planID).ToList();
			}
			Logger.InfoFormat("Exiting {0}.GetDeviceFinancingInfo({1}): {2}", svcName, string.Empty, value.Count());
			return value;
		}

        #region 23072015 - Anthony - Reference Table [Accessory CR]

        public static List<Accessory> GetAccessoryDetailsByEANKey(string eanKey)
        {
            var accessoryList = new List<Accessory>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                accessoryList = ctx.Accessory.Where(x => x.EANCode == eanKey).ToList();
            }

            return accessoryList;
        }

        public static List<Accessory> GetAccessoryDetailsByArticleID(string articleID)
        {
            var accessoryList = new List<Accessory>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                accessoryList = ctx.Accessory.Where(x => x.ArticleID == articleID).ToList();
            }

            return accessoryList;
        }

        public static List<Accessory> GetAccessoryDetailsByName(string _accessoryName, int maxSearchResult)
        {
            var accessoryList = new List<Accessory>();
            var nextKeyword = new List<string>();
            var firstKeyword = string.Empty;

            if (_accessoryName.Contains(' '))
            {
                nextKeyword = _accessoryName.Split(' ').ToList();
                firstKeyword = nextKeyword[0];
            }
            else
            {
                firstKeyword = _accessoryName;
            }

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //accessoryList = ctx.Accessory.Where(x => x.Name.Contains(_accessoryName)).ToList();
                accessoryList = ctx.Accessory.Where(x => x.Name.Contains(firstKeyword)).ToList();

                if (nextKeyword != null && nextKeyword.Count > 0)
                {
                    for (var index = 1; index < nextKeyword.Count; index++)
                    {
                        var searchKeyword = nextKeyword[index].ToString2().ToLower();

                        accessoryList = accessoryList.Where(x => x.Name.ToLower().Contains(searchKeyword)).ToList();
                    }
                }
            }

            accessoryList = accessoryList.Take(maxSearchResult).ToList();

            return accessoryList;
        }

        public static string GetEANKeyByArticleID(string articleID)
        {
            var eanKey = string.Empty;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var accessoryList = ctx.Accessory.Where(x => x.ArticleID == articleID).ToList();

                if (accessoryList != null && accessoryList.Count > 0)
                {
                    eanKey = accessoryList.FirstOrDefault().EANCode;
                }
            }

            return eanKey;
        }

        public static List<OfferAccessory> GetOfferAccessoryDetails(List<string> articleIDs)
        {
            var offerAccessoryList = new List<OfferAccessory>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                foreach (var articleID in articleIDs)
                {
                    var offerAccessories = ctx.OfferAccessory.Where(x => x.ArticleID == articleID && x.OfferEndDT > DateTime.Now && x.OfferStartDT <= DateTime.Now).ToList();

                    if (offerAccessories != null && offerAccessories.Count > 0)
                    {
                        foreach (var offerAccessory in offerAccessories)
                        {
                            offerAccessoryList.Add(offerAccessory);
                        }
                    }
                }
            }

            return offerAccessoryList;
        }

        public static List<OfferAccessory> GetAccessoryAvailability(List<OfferAccessory> offerAccessories, int orgID)
        {
            var accessoryList = new List<OfferAccessory>();
            //NULL means the accessory is NOT AVAILABLE in this store

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                foreach (var offerAccessory in offerAccessories)
                {
                    var articleID = offerAccessory.ArticleID;
                    var uomCode = offerAccessory.UOMCode;

                    var orgAccessoryList = ctx.OrgAccessory.Where(x => x.ArticleID == articleID && x.UOMCode == uomCode && x.Active == true).ToList();

                    if (orgAccessoryList != null && orgAccessoryList.Count > 0)
                    {
                        var orgAccessoryListInSpecificStore = orgAccessoryList.Where(x => x.OrgID == orgID).ToList();

                        if (orgAccessoryListInSpecificStore != null && orgAccessoryListInSpecificStore.Count > 0)
                        {
                            accessoryList.Add(offerAccessory);//the accessory available IN THIS STORE
                        }
                        else
                        {
                            accessoryList = null;//the accessory is NOT AVAILABLE in this store
                        }
                    }
                    else
                    {
                        accessoryList.Add(offerAccessory);//the accessory is available IN ALL STORE
                    }
                }
            }
            return accessoryList;
        }

		public static List<AccFinancingInfo> GetAllAccessoriesFinancingInfo()
		{
			List<AccFinancingInfo> accFinancingInfoList = null;
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				accFinancingInfoList = ctx.Database.SqlQuery<AccFinancingInfo>("USP_GetAllAccessoriesFinancingInfo").ToList();
			}
			
			return accFinancingInfoList;
		}

        #endregion
    }
}
