﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SNT.Utility;
using Online.Registration.DAL.Models;
using System.Data.SqlClient;

namespace Online.Registration.DAL.Admin
{
    public static partial class OnlineRegAdmin
    {
        #region Market

        public static int MarketCreate(Market oReq)
        {
            Logger.InfoFormat("Entering {0}.MarketCreate({1})", svcName, (oReq != null) ? oReq.Description : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Markets.Add(oReq);
                ctx.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.MarketCreate({1}): {2}", svcName, (oReq != null) ? oReq.Description : "NULL", value);
            return value;
        }
        public static int MarketUpdate(Market oReq)
        {
            Logger.InfoFormat("Entering {0}.MarketUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var market = ctx.Markets.Find(oReq.ID);
                    market.Code = oReq.Code;
                    market.Name = oReq.Name;
                    market.KenanCode = oReq.KenanCode;
                    market.Description = oReq.Description;
                    market.Active = oReq.Active;
                    market.LastAccessID = oReq.LastAccessID;
                    market.LastUpdateDT = DateTime.Now;
                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.MarketUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static List<int> MarketFind(MarketFind oReq)
        {
            Logger.InfoFormat("Entering {0}.MarketFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Markets.Where(a => a.ID != 0);

                if (!(oReq.Market.ID == 0))
                    query = query.Where(a => a.ID == oReq.Market.ID);

                if (!string.IsNullOrEmpty(oReq.Market.Code))
                    query = query.Where(a => a.Code == oReq.Market.Code);

                if (!string.IsNullOrEmpty(oReq.Market.KenanCode))
                    query = query.Where(a => a.KenanCode == oReq.Market.KenanCode);

                if (!string.IsNullOrEmpty(oReq.Market.Name))
                    query = query.Where(a => a.Name.ToLower().Contains(oReq.Market.Name.ToLower()));

                if (!string.IsNullOrEmpty(oReq.Market.Description))
                    query = query.Where(a => a.Description.ToLower().Contains(oReq.Market.Description.ToLower()));

                if (!string.IsNullOrEmpty(oReq.Market.LastAccessID))
                    query = query.Where(a => a.LastAccessID.Contains(oReq.Market.LastAccessID));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.MarketFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        /// <summary>
        /// Added new method to get markets based on search criteria
        /// </summary>
        /// <param name="oReq"></param>
        /// <returns></returns>
        public static List<Market> MarketSearch(MarketFind oReq)
        {
            Logger.InfoFormat("Entering {0}.MarketFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<Market> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Markets.Where(a => a.ID != 0);

                if (!(oReq.Market.ID == 0))
                    query = query.Where(a => a.ID == oReq.Market.ID);

                if (!string.IsNullOrEmpty(oReq.Market.Code))
                    query = query.Where(a => a.Code == oReq.Market.Code);

                if (!string.IsNullOrEmpty(oReq.Market.KenanCode))
                    query = query.Where(a => a.KenanCode == oReq.Market.KenanCode);

                if (!string.IsNullOrEmpty(oReq.Market.Name))
                    query = query.Where(a => a.Name.ToLower().Contains(oReq.Market.Name.ToLower()));

                if (!string.IsNullOrEmpty(oReq.Market.Description))
                    query = query.Where(a => a.Description.ToLower().Contains(oReq.Market.Description.ToLower()));

                if (!string.IsNullOrEmpty(oReq.Market.LastAccessID))
                    query = query.Where(a => a.LastAccessID.Contains(oReq.Market.LastAccessID));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.ToList();
            }

            Logger.InfoFormat("Exiting {0}.MarketFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<Market> MarketGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.MarketGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
            List<Market> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Markets.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.MarketGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
            return value;
        }
        public static List<Market> MarketList()
        {
            Logger.InfoFormat("Entering {0}.MarketGet({1})", svcName, "");
            List<Market> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Markets.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.MarketGet({1}): {2}", svcName, "", value.Count());
            return value;
        }
        #endregion

        #region Account Category

        public static int AccountCategoryCreate(AccountCategory oReq)
        {
            Logger.InfoFormat("Entering {0}.AccountCategoryCreate({1})", svcName, (oReq != null) ? oReq.Description : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.AccountCategories.Add(oReq);
                ctx.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.AccountCategoryCreate({1}): {2}", svcName, (oReq != null) ? oReq.Description : "NULL", value);
            return value;
        }
        public static int AccountCategoryUpdate(AccountCategory oReq)
        {
            Logger.InfoFormat("Entering {0}.AccountCategoryUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var AccountCategory = ctx.AccountCategories.Find(oReq.ID);
                    AccountCategory.Code = oReq.Code;
                    AccountCategory.Name = oReq.Name;
                    AccountCategory.KenanCode = oReq.KenanCode;
                    AccountCategory.Description = oReq.Description;
                    AccountCategory.Active = oReq.Active;
                    AccountCategory.LastAccessID = oReq.LastAccessID;
                    AccountCategory.LastUpdateDT = DateTime.Now;
                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.AccountCategoryUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static List<int> AccountCategoryFind(AccountCategoryFind oReq)
        {
            Logger.InfoFormat("Entering {0}.AccountCategoryFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.AccountCategories.Where(a => a.ID != 0);

                if (!(oReq.AccountCategory.ID == 0))
                    query = query.Where(a => a.ID == oReq.AccountCategory.ID);

                if (!string.IsNullOrEmpty(oReq.AccountCategory.Code))
                    query = query.Where(a => a.Code == oReq.AccountCategory.Code);

                if (!string.IsNullOrEmpty(oReq.AccountCategory.KenanCode))
                    query = query.Where(a => a.KenanCode == oReq.AccountCategory.KenanCode);

                if (!string.IsNullOrEmpty(oReq.AccountCategory.Name))
                    query = query.Where(a => a.Name.ToLower().Contains(oReq.AccountCategory.Name.ToLower()));

                if (!string.IsNullOrEmpty(oReq.AccountCategory.Description))
                    query = query.Where(a => a.Description.ToLower().Contains(oReq.AccountCategory.Description.ToLower()));

                if (!string.IsNullOrEmpty(oReq.AccountCategory.LastAccessID))
                    query = query.Where(a => a.LastAccessID.Contains(oReq.AccountCategory.LastAccessID));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.AccountCategoryFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<AccountCategory> AccountCategoryGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.AccountCategoryGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
            List<AccountCategory> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.AccountCategories.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.AccountCategoryGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
            return value;
        }

        public static List<AccountCategory> AccountCategoryList()
        {
            Logger.InfoFormat("Entering {0}.AccountCategoryList({1})", svcName, "");
            List<AccountCategory> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.AccountCategories.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.AccountCategoryList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region Status

        public static int StatusCreate(Status oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusCreate({1})", svcName, (oReq != null) ? oReq.Description : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Statuses.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.StatusCreate({1}): {2}", svcName, (oReq != null) ? oReq.Description : "NULL", value);
            return value;
        }
        public static int StatusUpdate(Status oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusUpdate({1})", svcName, (oReq != null) ? oReq.Description : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).Status = EntityStatus.Modified;
                Status Status = ctx.Statuses.First(a => a.ID == oReq.ID);
                Status.Active = oReq.Active;
                Status.StatusTypeID = oReq.StatusTypeID;
                Status.Code = oReq.Code;
                Status.HasRemark = oReq.HasRemark;
                Status.LastAccessID = oReq.LastAccessID;
                Status.LastUpdateDT = oReq.LastUpdateDT;
                Status.Description = oReq.Description;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.StatusUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Description : "NULL", value);
            return value;
        }
        public static List<int> StatusFind(StatusFind oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Statuses.Where(a => a.ID != 0);

                if (oReq.Status.ID != 0)
                    query = query.Where(a => a.ID == oReq.Status.ID && a.Active == true);

                //if (oReq.Status.StatusTypeID != 0)
                //query = query.Where(a => a.StatusTypeID == oReq.Status.StatusTypeID);

                if (oReq.Status.StatusTypeID.ToInt() != 0)
                    query = query.Where(a => a.StatusTypeID == oReq.Status.StatusTypeID);

                if (!string.IsNullOrEmpty(oReq.Status.Code))
                    query = query.Where(a => a.Code == oReq.Status.Code);

                if (!string.IsNullOrEmpty(oReq.Status.Description))
                    query = query.Where((a) => a.Description.Contains(oReq.Status.Description));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }

        #region Added by Patanjali to add find for Store Keeper and Cashier

        public static List<int> StoreKeeperFind(StatusFind oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Statuses.Where(a => a.ID != 0);

                if (oReq.Status.ID != 0)
                    query = query.Where(a => a.ID == oReq.Status.ID);

                //if (oReq.Status.StatusTypeID != 0)
                //query = query.Where(a => a.StatusTypeID == oReq.Status.StatusTypeID);

                if (oReq.Status.StatusTypeID.ToInt() != 0)
                    query = query.Where(a => a.StatusTypeID == oReq.Status.StatusTypeID);

                if (!string.IsNullOrEmpty(oReq.Status.Code))
                    query = query.Where(a => a.Code == oReq.Status.Code);

                if (!string.IsNullOrEmpty(oReq.Status.Description))
                    query = query.Where((a) => a.Description.Contains(oReq.Status.Description));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.Role != "TL")      // added condition for to retrive all status for TL profile
                {
                    //query = query.Where((a) => (a.Description == "NEW" || a.Description == "FAIL" || a.Description == "Account Pending Create" || a.Description == "Cancelled" || a.Description == "Cancelled – IC" || a.Description == "Cancelled – Cashier" || a.Description == "SimReplacement" || a.Description == "Reassign VAS request pending") && a.Active == true);
                    query = query.Where((a) => (a.Code == "REG_New" || a.Code == "REG_Fail" || a.Code == "REG_APC" || a.Code == "REG_Can" || a.Code == "REG_CAN_SK" || a.Code == "REG_CAN_CK") && a.Active == true);
                }

                if (oReq.Role == "Dealer")      // added condition for to retrive all status for MEPS only
                {
                    query = query.Where((a) => (a.Code == "REG_New" || a.Code == "REG_Fail" || a.Code == "REG_APC" || a.Code == "REG_Can") && a.Active == true);
                }

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);


                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }

        public static List<int> CashierFind(StatusFind oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Statuses.Where(a => a.ID != 0);

                if (oReq.Status.ID != 0)
                    query = query.Where(a => a.ID == oReq.Status.ID);

                //if (oReq.Status.StatusTypeID != 0)
                //query = query.Where(a => a.StatusTypeID == oReq.Status.StatusTypeID);

                if (oReq.Status.StatusTypeID.ToInt() != 0)
                    query = query.Where(a => a.StatusTypeID == oReq.Status.StatusTypeID);

                if (!string.IsNullOrEmpty(oReq.Status.Code))
                    query = query.Where(a => a.Code == oReq.Status.Code);

                if (!string.IsNullOrEmpty(oReq.Status.Description))
                    query = query.Where((a) => a.Description.Contains(oReq.Status.Description));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                query = query.Where((a) => (a.Description != "NEW" && a.Description != "SimReplacement" && a.Description != "Sim Replacement Request Pending" && a.Description != "Reassign VAS request pending" && a.Code != "REG_PR" && a.Code != "REG_PA" && a.Code != "REG_AC") && a.Active == true);
                
                if (oReq.Role == "Dealer")
                {
                    query = query.Where((a) => (a.Code != "REG_CAN_SK" && a.Code != "REG_CAN_CK") && a.Active == true);
                }
                else
                {
                    query = query.Where((a) => (a.Code != "REG_PendingCDPUApp" && a.Code != "REG_CDPURejected") && a.Active == true);
                }

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);


                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }

        #endregion

        public static List<Status> StatusGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.StatusGet({1})", svcName, (IDs.Count() > 0) ? IDs.Count : 0);
            List<Status> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Statuses.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs.Count : 0, value.Count());
            return value;
        }

        public static List<Status> StatusList()
        {
            Logger.InfoFormat("Entering {0}.StatusList({1})", svcName, "");
            List<Status> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Statuses.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region StatusType

        public static int StatusTypeCreate(StatusType oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusTypeCreate({1})", svcName, (oReq != null) ? oReq.Description : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.StatusTypes.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.StatusTypeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Description : "NULL", value);
            return value;
        }
        public static int StatusTypeUpdate(StatusType oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusTypeUpdate({1})", svcName, (oReq != null) ? oReq.Description : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).StatusType = EntityStatusType.Modified;
                StatusType StatusType = ctx.StatusTypes.First(a => a.ID == oReq.ID);
                StatusType.Active = oReq.Active;
                StatusType.Code = oReq.Code;
                StatusType.LastAccessID = oReq.LastAccessID;
                StatusType.LastUpdateDT = oReq.LastUpdateDT;
                StatusType.Description = oReq.Description;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.StatusTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Description : "NULL", value);
            return value;
        }
        public static List<int> StatusTypeFind(StatusTypeFind oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusTypeFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.StatusTypes.Where(a => a.ID != 0);

                if (oReq.StatusType.ID != 0)
                    query = query.Where(a => a.ID == oReq.StatusType.ID);

                if (!string.IsNullOrEmpty(oReq.StatusType.Code))
                    query = query.Where(a => a.Code == oReq.StatusType.Code);

                if (!string.IsNullOrEmpty(oReq.StatusType.Description))
                    query = query.Where((a) => a.Description.Contains(oReq.StatusType.Description));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<StatusType> StatusTypeGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.StatusTypeGet({1})", svcName, (IDs.Count > 0) ? IDs[0] : 0);
            List<StatusType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.StatusTypes.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusTypeGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<StatusType> StatusTypeList()
        {
            Logger.InfoFormat("Entering {0}.StatusTypeList({1})", svcName, "");
            List<StatusType> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.StatusTypes.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusTypeList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region Status Change

        public static int StatusChangeCreate(StatusChange oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusChangeCreate({1})", svcName, (oReq != null) ? oReq.FromStatusID.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.StatusChanges.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.StatusChangeCreate({1}): {2}", svcName, (oReq != null) ? oReq.FromStatusID.ToString() : "NULL", value);
            return value;
        }
        public static int StatusChangeUpdate(StatusChange oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusChangeUpdate({1})", svcName, (oReq != null) ? oReq.FromStatusID.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).StatusChange = EntityStatusChange.Modified;
                StatusChange StatusChange = ctx.StatusChanges.First(a => a.ID == oReq.ID);
                StatusChange.Active = oReq.Active;
                StatusChange.FromStatusID = oReq.FromStatusID;
                StatusChange.ToStatusID = oReq.ToStatusID;
                StatusChange.LastAccessID = oReq.LastAccessID;
                StatusChange.LastUpdateDT = oReq.LastUpdateDT;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.StatusChangeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.FromStatusID.ToString() : "NULL", value);
            return value;
        }
        public static List<int> StatusChangeFind(StatusChangeFind oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusChangeFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.StatusChanges.Where(a => a.ID != 0);

                if (oReq.FromToStatusID != 0)
                {
                    query = query.Where(a => a.FromStatusID == oReq.FromToStatusID || a.ToStatusID == oReq.FromToStatusID);
                }

                if (oReq.StatusChange != null)
                {
                    if (oReq.StatusChange.ID != 0)
                        query = query.Where(a => a.ID == oReq.StatusChange.ID);

                    if (oReq.StatusChange.FromStatusID != 0)
                        query = query.Where(a => a.FromStatusID == oReq.StatusChange.FromStatusID);

                    if (oReq.StatusChange.ToStatusID != 0)
                        query = query.Where(a => a.ToStatusID == oReq.StatusChange.ToStatusID);
                }

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusChangeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<StatusChange> StatusChangeGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.StatusChangeGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
            List<StatusChange> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.StatusChanges.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusChangeGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
            return value;
        }

        #endregion

        #region Status Reason

        public static int StatusReasonCreate(StatusReason oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonCreate({1})", svcName, (oReq != null) ? oReq.Code : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.StatusReasons.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.StatusReasonCreate({1}): {2}", svcName, (oReq != null) ? oReq.Code : "NULL", value);
            return value;
        }
        public static int StatusReasonUpdate(StatusReason oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonUpdate({1})", svcName, (oReq != null) ? oReq.Code : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).StatusReason = EntityStatusReason.Modified;
                StatusReason StatusReason = ctx.StatusReasons.First(a => a.ID == oReq.ID);
                StatusReason.Active = oReq.Active;
                StatusReason.Code = oReq.Code;
                StatusReason.Name = oReq.Name;
                StatusReason.LastAccessID = oReq.LastAccessID;
                StatusReason.LastUpdateDT = oReq.LastUpdateDT;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.StatusReasonUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Code : "NULL", value);
            return value;
        }
        public static List<int> StatusReasonFind(StatusReasonFind oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.StatusReasons.Where(a => a.ID != 0);

                if (oReq.StatusReason.ID != 0)
                    query = query.Where(a => a.ID == oReq.StatusReason.ID);

                if (!string.IsNullOrEmpty(oReq.StatusReason.Code))
                    query = query.Where(a => a.Code == oReq.StatusReason.Code);

                if (!string.IsNullOrEmpty(oReq.StatusReason.Name))
                    query = query.Where(a => a.Name.Contains(oReq.StatusReason.Name));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusReasonFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<StatusReason> StatusReasonGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
            List<StatusReason> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.StatusReasons.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusReasonGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
            return value;
        }
        public static List<StatusReason> StatusReasonList()
        {
            Logger.InfoFormat("Entering {0}.StatusReasonList({1})", svcName, "");
            List<StatusReason> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.StatusReasons.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusReasonList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region Status Reason Code

        public static int StatusReasonCodeCreate(StatusReasonCode oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonCodeCreate({1})", svcName, (oReq != null) ? oReq.StatusID.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.StatusReasonCodes.Add(oReq);
                ctx.SaveChanges();
            }
            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.StatusReasonCodeCreate({1}): {2}", svcName, (oReq != null) ? oReq.StatusID.ToString() : "NULL", value);
            return value;
        }
        public static int StatusReasonCodeUpdate(StatusReasonCode oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonCodeUpdate({1})", svcName, (oReq != null) ? oReq.StatusID.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).StatusReasonCode = EntityStatusReasonCode.Modified;
                StatusReasonCode StatusReasonCode = ctx.StatusReasonCodes.First(a => a.ID == oReq.ID);
                StatusReasonCode.Active = oReq.Active;
                StatusReasonCode.StatusID = oReq.StatusID;
                StatusReasonCode.ReasonCodeID = oReq.ReasonCodeID;
                StatusReasonCode.Remark = oReq.Remark;
                StatusReasonCode.LastAccessID = oReq.LastAccessID;
                StatusReasonCode.LastUpdateDT = oReq.LastUpdateDT;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.StatusReasonCodeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.StatusID.ToString() : "NULL", value);
            return value;
        }
        public static List<int> StatusReasonCodeFind(StatusReasonCodeFind oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonCodeFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.StatusReasonCodes.Where(a => a.ID != 0);

                if (oReq.StatusReasonCode.ID != 0)
                    query = query.Where(a => a.ID == oReq.StatusReasonCode.ID);

                if (oReq.StatusReasonCode.StatusID != 0)
                    query = query.Where(a => a.StatusID == oReq.StatusReasonCode.StatusID);

                if (oReq.StatusReasonCode.ReasonCodeID != 0)
                    query = query.Where(a => a.ReasonCodeID == oReq.StatusReasonCode.ReasonCodeID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select((a) => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusReasonCodeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }
        public static List<StatusReasonCode> StatusReasonCodeGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonCodeGet({1})", svcName, (IDs.Count > 0) ? IDs.Count : 0);
            List<StatusReasonCode> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.StatusReasonCodes.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.StatusReasonCodeGet({1}): {2}", svcName, (IDs.Count > 0) ? IDs.Count : 0, value.Count());
            return value;
        }
        public static List<int> StatusReasonCodeIDsFind()
        {
            //Logger.InfoFormat("Entering {0}.StatusReasonCodeIDsFind({1})", svcName, (oReq != null) ? oReq.ToString2() : "NULL");

            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.StatusReasonCodes.Select(a => a.StatusID).Distinct().ToList();

            }
            //Logger.InfoFormat("Exiting {0}.StatusReasonCodeIDsFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString2() : "NULL", value.Count());
            return value;
        }

        #endregion

        #region smart related methods
        public static List<TransactionTypes> TransactionTypeGet()
        {

            List<TransactionTypes> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.TransactionTypes.Where(a => a.Active == true).ToList();
            }

            return value;
        }
        #endregion

        #region BRE Check
        //public static List<BRECheck> GetBRECheckDetails()
        //{
        //    Logger.InfoFormat("Entering {0}.GetBRECheckDetails({1})", svcName, "");
        //    List<BRECheck> value = null;

        //    using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
        //    {
        //        value = ctx.Database.SqlQuery<BRECheck>("USP_GetBRECheckDetails").ToList();
        //    }
        //    Logger.InfoFormat("Exiting {0}.GetBRECheckDetails({1})", svcName, "");
        //    return value;
        //}
        public static List<BRECheck> GetBRECheckDetails(List<int> userGroupId)
        {
            Logger.InfoFormat("Entering {0}.GetBRECheckDetails({1})", svcName, "");
            List<BRECheck> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<BRECheck>("USP_GetBRECheckDetailsNew @userGroupId", new SqlParameter("@userGroupId", string.Join(",",userGroupId))).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetBRECheckDetails({1})", svcName, "");
            return value;
        }
        #endregion
    }
}
