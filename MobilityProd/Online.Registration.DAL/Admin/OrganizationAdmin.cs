﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using SNT.Utility;
using Online.Registration.DAL.Models;

namespace Online.Registration.DAL.Admin
{
    public static partial class OnlineRegAdmin
    {

        #region Org Type

        public static int OrgTypeCreate(OrgType oReq)
        {
            Logger.InfoFormat("Entering {0}.OrgTypeCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.OrgTypes.Add(oReq);
                ctx.SaveChanges();
                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.OrgTypeCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int OrgTypeUpdate(OrgType oReq)
        {
            Logger.InfoFormat("Entering {0}.OrgTypeUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    //ctx.Entry(oReq).State = EntityState.Modified;
                    OrgType orgType = ctx.OrgTypes.First(a => a.ID == oReq.ID);
                    orgType.Code = oReq.Code;
                    orgType.Name = oReq.Name;
                    orgType.Description = oReq.Description;
                    orgType.CanFulfill = oReq.CanFulfill;
                    orgType.CanAllocate = oReq.CanAllocate;
                    orgType.CanSupply = oReq.CanSupply;
                    orgType.Active = oReq.Active;
                    orgType.LastAccessID = oReq.LastAccessID;
                    orgType.LastUpdateDT = oReq.LastUpdateDT;
                    orgType.Name = oReq.Name;
                    value = ctx.SaveChanges();
                }
            }
            Logger.InfoFormat("Exiting {0}.OrgTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static List<int> OrgTypeFind(OrgTypeFind oReq)
        {
            Logger.InfoFormat("Entering {0}.OrgTypeFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = new List<int>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var query = ctx.OrgTypes.Where(a => a.ID != 0);

                    if (!(oReq.OrgType.ID == 0))
                        query = query.Where(a => a.ID == oReq.OrgType.ID);

                    if (!string.IsNullOrEmpty(oReq.OrgType.Code))
                        query = query.Where(a => a.Code == oReq.OrgType.Code);

                    if (!string.IsNullOrEmpty(oReq.OrgType.Name))
                        query = query.Where(a => a.Name.Contains(oReq.OrgType.Name));

                    if (!string.IsNullOrEmpty(oReq.OrgType.LastAccessID))
                        query = query.Where(a => a.LastAccessID.Contains(oReq.OrgType.LastAccessID));

                    if (oReq.Active.HasValue)
                        query = query.Where(a => a.Active == oReq.Active.Value);

                    if (oReq.PageSize > 0)
                        query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                    value = query.Select(a => a.ID).ToList();
                }
            
            Logger.InfoFormat("Exiting {0}.OrgTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<OrgType> OrgTypeGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.OrgTypeGet({1})", svcName, (IDs.Count() > 0) ? IDs[0] : 0);
            List<OrgType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.OrgTypes.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.OrgTypeGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<OrgType> OrgTypesList()
        {
            Logger.InfoFormat("Entering {0}.OrgTypesList({1})", svcName, "");
            List<OrgType> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.OrgTypes.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.OrgTypesList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region Organization

        public static int OrganizationCreate(Organization oReq)
        {
            Logger.InfoFormat("Entering {0}.OrganizationCreate({1})", svcName, (oReq != null) ? oReq.Code.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Organizations.Add(oReq);
                ctx.SaveChanges();
                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.OrganizationCreate({1}): {2}", svcName, (oReq != null) ? oReq.Code.ToString() : "NULL", value);
            return value;
        }
        public static int OrganizationUpdate(Organization oReq)
        {
            Logger.InfoFormat("Entering {0}.OrganizationUpdate({1})", svcName, (oReq != null) ? oReq.Code.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                Organization org = ctx.Organizations.First(a => a.ID.Equals(oReq.ID));
                org.Active = oReq.Active;
                org.AddrLine1 = oReq.AddrLine1;
                org.AddrLine2 = oReq.AddrLine2;
                org.Code = oReq.Code;
                org.DealerCode = oReq.DealerCode;
                org.ContactNo = oReq.ContactNo;
                org.CountryID = oReq.CountryID;
                org.EmailAddr = oReq.EmailAddr;
                org.FaxNo = oReq.FaxNo;
                org.IsHQ = oReq.IsHQ;
                org.KenanRegCode = oReq.KenanRegCode;
                org.LastAccessID = oReq.LastAccessID;
                org.LastUpdateDT = oReq.LastUpdateDT;
                org.MobileNo = oReq.MobileNo;
                org.Name = oReq.Name;
                org.OrgTypeID = oReq.OrgTypeID;
                org.PersonInCharge = oReq.PersonInCharge;
                org.Postcode = oReq.Postcode;
                //org.RegionID = oReq.RegionID;
                org.StateID = oReq.StateID;
                org.TownCity = oReq.TownCity;
                org.KenanServiceProviderID = oReq.KenanServiceProviderID;
                org.SAPBranchCode = oReq.SAPBranchCode;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.OrganizationUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Code.ToString() : "NULL", value);
            return value;
        }
        public static List<int> OrganizationFind(OrganizationFind oReq)
        {
            Logger.InfoFormat("Entering {0}.CoverageFilterFind({1})", svcName, (oReq != null) ? oReq.Organization.Name : "NULL");
            List<int> IDs = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Organizations.Where(a => a.ID != 0);
                if (oReq.Organization.ID != 0)
                    query = query.Where(a => a.ID == oReq.Organization.ID);

                if (oReq.Organization.OrgTypeID != 0)
                    query = query.Where(a => a.OrgTypeID == oReq.Organization.OrgTypeID);

                if (!string.IsNullOrEmpty(oReq.Organization.Code))
                    query = query.Where(a => a.Code == oReq.Organization.Code);

                if (!string.IsNullOrEmpty(oReq.Organization.Name))
                    query = query.Where(a => a.Name.Contains(oReq.Organization.Name));

                if (!string.IsNullOrEmpty(oReq.Organization.AddrLine1))
                    query = query.Where(a => a.AddrLine1.Contains(oReq.Organization.AddrLine1));

                if (!string.IsNullOrEmpty(oReq.Organization.AddrLine2))
                    query = query.Where(a => a.AddrLine2.Contains(oReq.Organization.AddrLine2));

                if (!string.IsNullOrEmpty(oReq.Organization.Postcode))
                    query = query.Where(a => a.Postcode == oReq.Organization.Postcode);

                if (!string.IsNullOrEmpty(oReq.Organization.TownCity))
                    query = query.Where(a => a.TownCity == oReq.Organization.TownCity);

                if (oReq.Organization.StateID != 0)
                    query = query.Where(a => a.StateID == oReq.Organization.StateID);

                if (oReq.Organization.CountryID != 0)
                    query = query.Where(a => a.CountryID == oReq.Organization.CountryID);

                if (!string.IsNullOrEmpty(oReq.Organization.PersonInCharge))
                    query = query.Where(a => a.PersonInCharge == oReq.Organization.PersonInCharge);

                if (!string.IsNullOrEmpty(oReq.Organization.ContactNo))
                    query = query.Where(a => a.PersonInCharge.Contains(oReq.Organization.ContactNo));

                if (!string.IsNullOrEmpty(oReq.Organization.FaxNo))
                    query = query.Where(a => a.FaxNo.Contains(oReq.Organization.FaxNo));

                if (!string.IsNullOrEmpty(oReq.Organization.MobileNo))
                    query = query.Where(a => a.MobileNo.Contains(oReq.Organization.MobileNo));

                if (!string.IsNullOrEmpty(oReq.Organization.EmailAddr))
                    query = query.Where(a => a.EmailAddr.Contains(oReq.Organization.EmailAddr));

                if (oReq.IsHQ.HasValue)
                    query = query.Where(a => a.IsHQ == oReq.IsHQ.Value);

                if (!string.IsNullOrEmpty(oReq.Organization.KenanRegCode))
                    query = query.Where(a => a.KenanRegCode == oReq.Organization.KenanRegCode);

                //if (RegionID != 0)
                //    query = query.Where(a => a.RegionID == RegionID);

                if (!string.IsNullOrEmpty(oReq.Organization.KenanServiceProviderID))
                    query = query.Where(a => a.KenanServiceProviderID == oReq.Organization.KenanServiceProviderID);

                if (!string.IsNullOrEmpty(oReq.Organization.SAPBranchCode))
                    query = query.Where(a => a.SAPBranchCode == oReq.Organization.SAPBranchCode);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);
                IDs = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.CoverageFilterFind({1})", svcName, (oReq != null) ? oReq.Organization.Name : "NULL");
            return IDs;
        }
        public static List<Organization> OrganizationGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.OrganizationGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<Organization> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Organizations.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.OrganizationGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }

        public static List<Organization> OrganizationList()
        {
            Logger.InfoFormat("Entering {0}.OrganizationList({1})", svcName, "");
            List<Organization> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Organizations.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.OrganizationList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        public static List<Organization> OrganizationGetALL()
        {
            Logger.InfoFormat("Entering {0}.OrganizationGet({1})", svcName);
            List<Organization> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Organizations.OrderBy (org => org.Name).ToList() ;
            }
            Logger.InfoFormat("Exiting {0}.OrganizationGet({1}): {2}", svcName);
            return value;
        }
        #endregion

    }
}
