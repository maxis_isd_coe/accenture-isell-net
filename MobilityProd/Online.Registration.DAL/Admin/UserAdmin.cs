﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using SNT.Utility;
using Online.Registration.DAL.Models;
using System.Data.SqlClient;

namespace Online.Registration.DAL.Admin
{
    public static partial class OnlineRegAdmin
    {
        private static readonly string _ConnectionString = !ReferenceEquals(ConfigurationManager.ConnectionStrings["OnlineStoreDbContext"], null) ? ConfigurationManager.ConnectionStrings["OnlineStoreDbContext"].ToString2() : "";

        #region User Group Access

        public static int UserGrpAccessCreate(UserGrpAccess oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpAccessCreate({1})", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.UserGrpAccesses.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.UserGrpAccessCreate({1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL", value);
            return value;
        }
        public static int UserGrpAccessUpdate(UserGrpAccess oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpAccessUpdate({1})", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                UserGrpAccess uga = ctx.UserGrpAccesses.First(a => a.ID == oReq.ID);
                uga.AccessID = oReq.AccessID;
                uga.Active = oReq.Active;
                uga.LastAccessID = oReq.LastAccessID;
                uga.LastUpdateDT = oReq.LastUpdateDT;
                uga.UserGrpID = oReq.UserGrpID;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.UserGrpAccessUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL", value);
            return value;
        }
        public static List<int> UserGrpAccessFind(UserGrpAccessFind oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpAccessFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;
            
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.UserGrpAccesses.Where(a => a.ID != 0);

                if (!(oReq.UserGrpAccess.ID == 0))
                    query = query.Where(a => a.ID == oReq.UserGrpAccess.ID);

                if (!(oReq.UserGrpAccess.UserGrpID == 0))
                    query = query.Where(a => a.UserGrpID == oReq.UserGrpAccess.UserGrpID);

                if (!(oReq.UserGrpAccess.AccessID == 0))
                    query = query.Where(a => a.AccessID == oReq.UserGrpAccess.AccessID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                value = query.Select(a => a.ID).ToList();

            }
            Logger.InfoFormat("Exiting {0}.UserGrpAccessFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<UserGrpAccess> UserGrpAccessGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.UserGrpAccessGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<UserGrpAccess> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.UserGrpAccesses.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.UserGrpAccessGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }

        #endregion

        #region User Group Role

        public static int UserGrpRoleCreate(UserGrpRole oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpRoleCreate({1})", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.UserGrpRoles.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.UserGrpRoleCreate({1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL", value);
            return value;
        }
        public static int UserGrpRoleUpdate(UserGrpRole oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpRoleUpdate({1})", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                UserGrpRole ugr = ctx.UserGrpRoles.First(delegate(UserGrpRole a) { return a.ID == oReq.ID; });
                ugr.Active = oReq.Active;
                ugr.LastAccessID = oReq.LastAccessID;
                ugr.LastUpdateDT = oReq.LastUpdateDT;
                ugr.UserGrpID = oReq.UserGrpID;
                ugr.UserRoleID = oReq.UserRoleID;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.UserGrpRoleUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL", value);
            return value;
        }
        public static List<int> UserGrpRoleFind(UserGrpRoleFind oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpRoleFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;
            
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.UserGrpRoles.Where(a => a.ID != 0);

                if (!(oReq.UserGrpRole.ID == 0))
                    query = query.Where(a => a.ID == oReq.UserGrpRole.ID);

                if (!(oReq.UserGrpRole.UserGrpID == 0))
                    query = query.Where(a => a.UserGrpID == oReq.UserGrpRole.UserGrpID);

                if (!(oReq.UserGrpRole.UserRoleID == 0))
                    query = query.Where(a => a.UserRoleID == oReq.UserGrpRole.UserRoleID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.UserGrpRoleFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<UserGrpRole> UserGrpRoleGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.UserGrpRoleGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<UserGrpRole> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.UserGrpRoles.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.UserGrpRoleGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }

        #endregion

        #region User Group

        public static int UserGroupCreate(UserGroup oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGroupCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.UserGroups.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.UserGroupCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int UserGroupUpdate(UserGroup oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGroupUpdate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                UserGroup ug = ctx.UserGroups.First(a => a.ID == oReq.ID);
                ug.Active = oReq.Active;
                ug.Code = oReq.Code;
                ug.LastAccessID = oReq.LastAccessID;
                ug.LastUpdateDT = oReq.LastUpdateDT;
                ug.Name = oReq.Name;
                ug.OrgID = oReq.OrgID;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.UserGroupUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static List<int> UserGroupFind(UserGroupFind oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGroupFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.UserGroups.Where(a => a.ID != 0);

                if (!(oReq.UserGroup.ID == 0))
                    query = query.Where(a => a.ID == oReq.UserGroup.ID);

                if (!(oReq.UserGroup.OrgID == 0))
                    query = query.Where(a => a.OrgID == oReq.UserGroup.OrgID);

                if (!string.IsNullOrEmpty(oReq.UserGroup.Code))
                    query = query.Where(a => a.Code == oReq.UserGroup.Code);

                if (!string.IsNullOrEmpty(oReq.UserGroup.Name))
                    query = query.Where(a => a.Name.Contains(oReq.UserGroup.Name));

                if (!(oReq.UserGroup.OrgID == 0))
                    query = query.Where(a => a.OrgID == oReq.UserGroup.OrgID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.UserGroupFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<UserGroup> UserGroupGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.UserGroupGet({1})", svcName, (IDs.Count() > 0) ? IDs.Count : 0);
            List<UserGroup> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.UserGroups.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.UserGroupGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs.Count : 0, value.Count());
            return value;
        }

        public static List<UserGroup> UserGroupList()
        {
            Logger.InfoFormat("Entering {0}.UserGroupList({1})", svcName, "");
            List<UserGroup> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.UserGroups.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.UserGroupList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        #endregion

        #region User Role

        public static int UserRoleCreate(UserRole oReq)
        {
            Logger.InfoFormat("Entering {0}.UserRoleCreate({1})", svcName, (oReq != null) ? oReq.Code : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.UserRoles.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.UserRoleCreate({1}): {2}", svcName, (oReq != null) ? oReq.Code : "NULL", value);
            return value;
        }
        public static int UserRoleUpdate(UserRole oReq)
        {
            Logger.InfoFormat("Entering {0}.UserRoleUpdate({1})", svcName, (oReq != null) ? oReq.Code : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                UserRole ur = ctx.UserRoles.First(a => a.ID == oReq.ID);
                ur.Active = oReq.Active;
                ur.Code = oReq.Code;
                ur.Description = oReq.Description;
                ur.LastAccessID = oReq.LastAccessID;
                ur.LastUpdateDT = oReq.LastUpdateDT;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.UserRoleUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Code : "NULL", value);
            return value;
        }
        public static List<int> UserRoleFind(UserRoleFind oReq)
        {
            Logger.InfoFormat("Entering {0}.UserRoleFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.UserRoles.Where(a => a.ID != 0);

                if (!(oReq.UserRole.ID == 0))
                    query = query.Where(a => a.ID == oReq.UserRole.ID);

                if (!string.IsNullOrEmpty(oReq.UserRole.Code))
                    query = query.Where(a => a.Code == oReq.UserRole.Code);

                if (!string.IsNullOrEmpty(oReq.UserRole.Description))
                    query = query.Where(a => a.Description.Contains(oReq.UserRole.Description));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.UserRoleFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<UserRole> UserRoleGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.UserRoleGet({1})", svcName, (IDs.Count() > 0) ? IDs[0] : 0);
            List<UserRole> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.UserRoles.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.UserRoleGet({1}): {2}", svcName, (IDs.Count() > 0) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<UserRole> UserRolesList()
        {
            Logger.InfoFormat("Entering {0}.UserRolesList({1})", svcName, "");
            List<UserRole> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.UserRoles.Where(a => a.ID > 0 ).ToList();
            }
            Logger.InfoFormat("Exiting {0}.UserRolesList({1}): {2}", svcName, "", value.Count());
            return value;
        }
        #endregion

        #region Access

        public static int AccessCreate(Access oReq)
        {
            Logger.InfoFormat("Entering {0}.AccessCreate({1})", svcName, (oReq != null) ? oReq.UserName : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Accesses.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.AccessCreate({1}): {2}", svcName, (oReq != null) ? oReq.UserName : "NULL", value);
            return value;
        }
        public static int AccessUpdate(Access oReq)
        {
            Logger.InfoFormat("Entering {0}.AccessUpdate({1})", svcName, (oReq != null) ? oReq.UserName : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //ctx.Entry(oReq).State = EntityState.Modified;
                Access access = ctx.Accesses.First(a => a.ID == oReq.ID);
                access.Active = oReq.Active;
                access.ChangePassword = oReq.ChangePassword;
                access.Description = oReq.Description;
                access.LastAccessID = oReq.LastAccessID;
                access.LastUpdateDT = oReq.LastUpdateDT;
                //access.Password = oReq.Password;
                access.UserID = oReq.UserID;
                access.UserName = oReq.UserName;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.AccessUpdate({1}): {2}", svcName, (oReq != null) ? oReq.UserName : "NULL", value);
            return value;
        }
        public static int AccessPasswordUpdate(Access oReq)
        {
            Logger.InfoFormat("Entering {0}.AccessPasswordUpdate({1})", svcName, (oReq != null) ? oReq.ID.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Access access = ctx.Accesses.Single(a => a.ID == oReq.ID);
                access.LastAccessID = oReq.LastAccessID;
                access.LastUpdateDT = oReq.LastUpdateDT;
                access.ChangePassword = oReq.ChangePassword;
                access.Password = oReq.Password;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.AccessPasswordUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString() : "NULL", value);
            return value;
        }
        public static int AccessFailedPasswordAttemptAdd(int accessID)
        {
            Logger.InfoFormat("Entering {0}.AccessFailedPasswordAttemptAdd({1})", svcName, accessID);
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Access access = ctx.Accesses.Single(a => a.ID == accessID);
                access.LastUpdateDT = DateTime.Now;
                if (access.FailedPasswordAttemptCount == 0)
                    access.FailedPasswordAttemptWindowStart = DateTime.Now;
                access.FailedPasswordAttemptCount += 1;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.AccessFailedPasswordAttemptAdd({1}): {2}", svcName, accessID, value);
            return value;
        }
        public static int AccessFailedPasswordAttemptReset(int accessID)
        {
            Logger.InfoFormat("Entering {0}.AccessFailedPasswordAttemptReset({1})", svcName, accessID);
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Access access = ctx.Accesses.Single(a => a.ID == accessID);
                access.LastUpdateDT = DateTime.Now;
                access.FailedPasswordAttemptWindowStart = null;
                access.FailedPasswordAttemptCount = 0;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.AccessFailedPasswordAttemptReset({1}): {2}", svcName, accessID, value);
            return value;
        }
        public static int AccessLockedOut(int accessID)
        {
            Logger.InfoFormat("Entering {0}.AccessLockedOut({1})", svcName, accessID);
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Access access = ctx.Accesses.Single(a => a.ID == accessID);
                access.LastUpdateDT = DateTime.Now;
                access.IsLockedOut = true;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.AccessLockedOut({1}): {2}", svcName, accessID, value);
            return value;
        }
        public static int AccessUnlock(int accessID, string lastAccessID)
        {
            Logger.InfoFormat("Entering {0}.AccessUnlock({1})", svcName, accessID);
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Access access = ctx.Accesses.Single(a => a.ID == accessID);
                access.LastUpdateDT = DateTime.Now;
                access.LastAccessID = lastAccessID;
                access.IsLockedOut = false;
                access.FailedPasswordAttemptWindowStart = null;
                access.FailedPasswordAttemptCount = 0;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.AccessUnlock({1}): {2}", svcName, accessID, value);
            return value;
        }
        public static List<int> AccessFind(AccessFind oReq)
        {
            Logger.InfoFormat("Entering {0}.AccessFind({1})", svcName, (oReq != null) ? oReq.Access.UserName : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Accesses.Where(a => a.ID != 0);

                if (!(oReq.Access.ID == 0))
                    query = query.Where(a => a.ID == oReq.Access.ID);

                if (!string.IsNullOrEmpty(oReq.Access.UserName))
                    query = query.Where(a => a.UserName == oReq.Access.UserName);

                if (!string.IsNullOrEmpty(oReq.Access.Password))
                    query = query.Where(a => a.Password == oReq.Access.Password);

                if (!string.IsNullOrEmpty(oReq.Access.Description))
                    query = query.Where(a => a.Description.Contains(oReq.Access.Description));

                if (!(oReq.Access.UserID == 0))
                    query = query.Where(a => a.UserID == oReq.Access.UserID);

                //if (!string.IsNullOrEmpty(oReq["ChangePassword"].ToString()))
                //    query = query.Where(a => a.ChangePassword == changePassword);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                
                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.AccessFind({1}): {2}", svcName, (oReq != null) ? oReq.Access.UserName : "NULL", value.Count());
            return value;
        }
        public static List<Access> AccessGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.AccessGet({1})", svcName, (IDs != null) ? IDs.Count : 0);
            List<Access> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Accesses.Where(a => IDs.Contains(a.ID)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.AccessGet({1}): {2}", svcName, (IDs != null) ? IDs.Count : 0, value.Count());
            return value;
        }

        public static List<Access> AccessList()
        {
            Logger.InfoFormat("Entering {0}.AccessList({1})", svcName, "");
            List<Access> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Accesses.Where(a => a.ID > 0).ToList();
            }
            Logger.InfoFormat("Exiting {0}.AccessList({1}): {2}", svcName, "", value.Count());
            return value;
        }

        public static Access AccessGetbyUserName(string strUsername)
        {
            Logger.InfoFormat("Entering {0}.AccessGetbyUserName({1})", svcName, (strUsername != null) ? strUsername : string.Empty);
            Access value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Accesses.Where(a => a.UserName == strUsername).SingleOrDefault();
            }

            //Commented by ravi as it was throwing Exception on Nov 7 2013 
            //Logger.InfoFormat("Exiting {0}.AccessGet({1}): {2}", svcName, (strUsername != null) ? strUsername : string.Empty);
            //Commented by ravi as it was throwing Exception on Nov 7 2013  ends here
           
            //New Format added by ravi on Nov 7 2013
            Logger.InfoFormat("Exiting {0}.AccessGetbyUserName({1})", svcName, (strUsername != null) ? strUsername : string.Empty);
            //New Format added by ravi on Nov 7 2013 Ends Here

            return value;
        }


        #endregion

        #region User

        public static int UserCreate(User oReq)
        {
            Logger.InfoFormat("Entering {0}.UserCreate({1})", svcName, (oReq != null) ? oReq.FullName : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Users.Add(oReq);
                ctx.SaveChanges();

                value = oReq.ID;
            }
            Logger.InfoFormat("Exiting {0}.UserCreate({1}): {2}", svcName, (oReq != null) ? oReq.FullName : "NULL", value);
            return value;
        }
        public static int UserUpdate(User oReq)
        {
            Logger.InfoFormat("Entering {0}.UserUpdate({1})", svcName, (oReq != null) ? oReq.FullName : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                User user = ctx.Users.First(a => a.ID == oReq.ID);
                user.Active = oReq.Active;
                user.EmailAddr = oReq.EmailAddr;
                user.IDCardNo = oReq.IDCardNo;
                user.IDCardTypeID = oReq.IDCardTypeID;
                user.LastAccessID = oReq.LastAccessID;
                user.FullName = oReq.FullName;
                user.LastUpdateDT = oReq.LastUpdateDT;
                user.MobileNo = oReq.MobileNo;
                user.OrgID = oReq.OrgID;
                //ctx.Entry(oReq).State = EntityState.Modified;
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.UserUpdate({1}): {2}", svcName, (oReq != null) ? oReq.FullName : "NULL", value);
            return value;
        }
        public static List<int> UserFind(UserFind oReq)
        {
            Logger.InfoFormat("Entering {0}.UserFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Users.Where(a => a.ID != 0);

                if (!(oReq.User.ID == 0))
                    query = query.Where(a => a.ID == oReq.User.ID);

                if (!string.IsNullOrEmpty(oReq.User.FullName))
                    query = query.Where(a => a.FullName.Contains(oReq.User.FullName));

                if (!string.IsNullOrEmpty(oReq.User.IDCardNo))
                    query = query.Where(a => a.IDCardNo.Contains(oReq.User.IDCardNo));

                if (!string.IsNullOrEmpty(oReq.User.MobileNo))
                    query = query.Where(a => a.MobileNo == oReq.User.MobileNo);

                if (!string.IsNullOrEmpty(oReq.User.EmailAddr))
                    query = query.Where(a => a.EmailAddr == oReq.User.EmailAddr);

                if (!(oReq.User.IDCardTypeID == 0))
                    query = query.Where(a => a.IDCardTypeID == oReq.User.IDCardTypeID);

                if (!(oReq.User.OrgID == 0))
                    query = query.Where(a => a.OrgID == oReq.User.OrgID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.UserFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<int> UserFindbyName(UserFindbyName oReq)
        {
            Logger.InfoFormat("Entering {0}.UserFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Users.Where(a => a.ID != 0);

                if (!string.IsNullOrEmpty(oReq.UserName))
                    query = query.Where(a => a.FullName == oReq.UserName);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }
            Logger.InfoFormat("Exiting {0}.UserFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<User> UserGet(List<int> IDs)
        {
            //Logger.InfoFormat("Entering {0}.UserGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<User> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Users.Where(a => IDs.Contains(a.ID)).ToList();
            }
           // Logger.InfoFormat("Exiting {0}.UserGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<User> UsersList()
        {
            //Logger.InfoFormat("Entering {0}.UserGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<User> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Users.Where(a => a.ID > 0 ).ToList();
            }
            // Logger.InfoFormat("Exiting {0}.UserGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        #endregion

        public static string[] UsersInRoleFind(string roleCode, string userName, bool active)
        {
            Logger.InfoFormat("Entering {0}.UsersInRoleFind({1})", svcName, roleCode);
            string[] value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var roleId = ctx.UserRoles.Where(a => a.Code == roleCode && a.Active == active).Select(a => a.ID).First();
                if (roleId != 0)
                {
                    if (string.IsNullOrEmpty(userName))
                    {
                        value = (from a in ctx.Accesses
                                 join g1 in ctx.UserGrpAccesses on a.ID equals g1.AccessID
                                 join g2 in ctx.UserGrpRoles on g1.UserGrpID equals g2.UserGrpID
                                 where g2.UserRoleID.Equals(roleId) && g2.Active.Equals(true) && g1.Active.Equals(true)
                                 select a.UserName).ToArray();
                    }
                    else
                    {
                        value = (from a in ctx.Accesses
                                 join g1 in ctx.UserGrpAccesses on a.ID equals g1.AccessID
                                 join g2 in ctx.UserGrpRoles on g1.UserGrpID equals g2.UserGrpID
                                 where a.UserName.Contains(userName) && g2.UserRoleID.Equals(roleId) && g2.Active.Equals(true) && g1.Active.Equals(true)
                                 select a.UserName).ToArray();
                    }
                }
            }

            Logger.InfoFormat("Exiting {0}.UsersInRoleFind({1}): {2}", svcName, roleCode, (value != null) ? value.Count() : 0);
            return value;

        }


        // Ranjeeth:  for the mobility performance , commented the below code. 
       //public static string[] RolesForUserGet(string userName, bool active)
       //{
       //    Logger.InfoFormat("Entering {0}.RolesForUserGet({1})", svcName, userName);
       //    string[] value = null;

       //    if (!string.IsNullOrEmpty(userName))
       //    {
       //        using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
       //        {
       //            var userId = ctx.Accesses.Where(a => a.UserName.Equals(userName) && a.Active == active).Select(a => a.ID).FirstOrDefault().ToInt();
       //            if (userId != 0)
       //            {
       //                value = (from a in ctx.UserGrpAccesses
       //                         join g1 in ctx.UserGrpRoles on a.UserGrpID equals g1.UserGrpID
       //                         join g2 in ctx.UserRoles on g1.UserRoleID equals g2.ID
       //                         where a.AccessID.Equals(userId) && g1.Active.Equals(true) && g2.Active.Equals(true)
       //                         select g2.Code).ToArray();
       //            }
       //        }
       //    }

       //    Logger.InfoFormat("Exiting {0}.RolesForUserGet({1}): {2}", svcName, userName, (value != null) ? value.Count() : 0);
       //    return value;

       //}



        public static string[] RolesForUserGet(string userName, bool active)
        {
            DateTime stDate = DateTime.Now;
            Logger.InfoFormat("Entering {0}.RolesForUserGet({1})", svcName, userName);
            string[] value = null;
            if (!string.IsNullOrEmpty(userName))
            {
                using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
                {
                    sqlConn.Open();
                    SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GetUserRoles_By_Username", sqlConn);
                    sqlCmd.Parameters.Add(new SqlParameter("@userName", userName));
                    sqlCmd.Parameters.Add(new SqlParameter("@Status", active));
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    object objScalar = sqlCmd.ExecuteScalar();
                    sqlConn.Close();
                    value = ReferenceEquals(objScalar, null) ? new string[] { string.Empty } : objScalar.ToString2().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                }
            }
            Logger.InfoFormat("Exiting {0}.RolesForUserGet({1}): {2}", svcName, userName, (value != null) ? value.Count() : 0);
            TimeSpan timeSpn = (DateTime.Now - stDate);
            Common.LogMessage(string.Format("{0}.RolesForUserGet({1}) TimeSpan:{2}", svcName, userName, timeSpn.ToString("hh\\:mm\\:ss\\.fff")));
            return value;
        }

        #region VLT ADDED CODE
        /// <summary>
        /// Gets the status messages relating to business checks
        /// </summary>
        /// <param name="inputparams">The inputparams.</param>
        /// <returns></returns>
        public static List<tblAPIStatusMessages> APIStatusMessagesGet(tblAPIStatusMessages inputparams)
        {
            Logger.InfoFormat("Entering {0}.APIStatusMessagesGet({1})", svcName, inputparams.APIName);
            List<tblAPIStatusMessages> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<tblAPIStatusMessages>("usp_GetAPIStatusMessages @APIName, @MethodName",
                    new SqlParameter("APIName", inputparams.APIName),
                    new SqlParameter("MethodName", inputparams.MethodName)).ToList();
            }
            Logger.InfoFormat("Exiting {0}.APIStatusMessagesGet({1})", svcName, inputparams.APIName);
            return value;
        }
        #endregion

        public static List<tblDonor> GetDonors()
        {
            Logger.InfoFormat("Entering {0}", svcName);
            List<tblDonor> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<tblDonor>("usp_GetDonors").ToList();
            }
            Logger.InfoFormat("Exiting {0}", svcName);
            return value;
        }

        #region VLT ADDED CODE
        /// <summary>
        /// Saves the API call information
        /// </summary>
        /// <param name="inputparams">The inputparams.</param>
        /// <returns></returns>
        public static int APISaveCallStatus(tblAPICallStatus inputparams)
        {
            Logger.InfoFormat("Entering {0}.APIStatusMessagesGet({1})", svcName, inputparams.APIName);
            IEnumerable<int> value = null;
            var NewID = 0;
            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    value = ctx.Database.SqlQuery<int>("usp_SaveAPICallStatus1 @APIName, @MethodName, @RequestTime, @UserName,@BreXmlReq,@BreXmlRes,@IdCardNo,@ResponseTime,@Status",
                        new SqlParameter("APIName", inputparams.APIName),
                        new SqlParameter("MethodName", inputparams.MethodName),
                        new SqlParameter("RequestTime", inputparams.RequestTime),
                        new SqlParameter("UserName", inputparams.UserName),
                        new SqlParameter("BreXmlReq", inputparams.BreXmlReq),
                        new SqlParameter("BreXmlRes", inputparams.BreXmlRes.Replace("&","&amp;")),
                        new SqlParameter("IdCardNo", inputparams.IdCardNo),
                        new SqlParameter("ResponseTime", inputparams.ResponseTime),
                        new SqlParameter("Status", inputparams.Status));
                    NewID = value.Single();
                }
                Logger.InfoFormat("Exiting {0}.APISaveCallStatus({1}): {2}", svcName, inputparams.APIName, "");
            }
            catch (Exception ex)
            {
                Logger.InfoFormat("Excepion  {0}.APISaveCallStatus({1}): {2}", svcName, inputparams.APIName, ex);
 
            }
            return NewID;
        }

        /// <summary>
        /// Updates the api call status.
        /// </summary>
        /// <param name="inputparams">The inputparams.</param>
        /// <returns>Number of records updated</returns>
        public static int APIUpdateCallStatus(tblAPICallStatus inputparams)
        {
            Logger.InfoFormat("Entering {0}.APIUpdateCallStatus({1})", svcName, inputparams.APIName);
            IEnumerable<int> value = null;
            var RecordsUpdated = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_UpdateAPICallStatus @ID, @ResponseTime, @Status",
                    new SqlParameter("ID", inputparams.ID),                    
                    new SqlParameter("ResponseTime", inputparams.ResponseTime),
                    new SqlParameter("Status", inputparams.Status));
                RecordsUpdated = value.Single();
            }
            Logger.InfoFormat("Exiting {0}.APIUpdateCallStatus({1})", svcName, inputparams.APIName);
            return RecordsUpdated;
        }
        #endregion

        #region VLT ADDED CODE
        /// <summary>
        /// Gets the min values for the plan search items
        /// </summary>
        /// <param name="inputparams">The inputparams.</param>
        /// <returns></returns>
        public static string PlanPropertyValuesGetMin(string searchItem)
        {
            Logger.InfoFormat("Entering {0}.refPlanProperty({1})", svcName, searchItem);
            string value = string.Empty;

            if (!string.IsNullOrEmpty(searchItem))
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    if (searchItem == "VoiceUsage")
                    {
                        value = Convert.ToString(ctx.PlanProperties.Select(e => e.VoiceUsage).Min());
                    }
                    else if (searchItem == "DataUsage")
                    {
                        value = Convert.ToString(ctx.PlanProperties.Select(e => e.DataUsage).Min());
                    }
                    else if (searchItem == "SMSUsage")
                    {
                        value = Convert.ToString(ctx.PlanProperties.Select(e => e.SMSUsage).Min());
                    }
                    else
                    {
                        value = Convert.ToString(ctx.PlanProperties.Select(e => e.Price).Min());
                    }
                }
            }

            Logger.InfoFormat("Exiting {0}.RolesForUserGet({1}): {2}", svcName, searchItem, (value != null) ? value.ToInt() : 0);
            return value;
        }

        /// <summary>
        /// Gets the max values for the plan search items
        /// </summary>
        /// <param name="inputparams">The inputparams.</param>
        /// <returns></returns>

        public static string PlanPropertyValuesGetMax(string searchItem)
        {
            Logger.InfoFormat("Entering {0}.refPlanProperty({1})", svcName, searchItem);
            string value = string.Empty;

            if (!string.IsNullOrEmpty(searchItem))
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    if (searchItem == "VoiceUsage")
                    {
                        value = Convert.ToString(ctx.PlanProperties.Select(e => e.VoiceUsage).Max());
                    }
                    else if (searchItem == "DataUsage")
                    {
                        value = Convert.ToString(ctx.PlanProperties.Select(e => e.DataUsage).Max());
                    }
                    else if (searchItem == "SMSUsage")
                    {
                        value = Convert.ToString(ctx.PlanProperties.Select(e => e.SMSUsage).Max());
                    }
                    else
                    {
                        value = Convert.ToString(ctx.PlanProperties.Select(e => e.Price).Max());
                    }
                }
            }

            Logger.InfoFormat("Exiting {0}.RolesForUserGet({1}): {2}", svcName, searchItem, (value != null) ? value.ToInt() : 0);
            return value;
        }
        #endregion

        public static bool MocICcheckwithwhitelistcustomers(string userName, string strIcno)
        {
            Logger.InfoFormat("Entering {0}.RolesForUserGet({1})", svcName, userName);
            long value = 0;
            bool status = false;
            if (!string.IsNullOrEmpty(userName))
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {

                    //   value = (from a in ctx.WhiteList where a.CampaignId.Equals(strIcno) select a.CampaignId).FirstOrDefault().ToString();
                    value = (from a in ctx.WhiteList where a.ICNo.Equals(strIcno) select a.CampaignId).FirstOrDefault().ToLong();
                }
            }

            Logger.InfoFormat("Exiting {0}.RolesForUserGet({1}): {2}", svcName, userName, value);

            if (value > 0)
            {
                status = true;
            }
            else
            {
                status = false;
            }

            return status;

        }
        public static int GetPPIDCountSearch(string userName,string Action)
        {
            Logger.InfoFormat("Entering {0}.GetPPIDCountSearch({1})", svcName, userName);
            int PPIDCount = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                PPIDCount = ctx.Database.SqlQuery<int>("USP_GetPPIDSearchCount @UserName, @Action", new SqlParameter("@UserName", userName), new SqlParameter("@Action", Action)).ToList().FirstOrDefault();
                
            }
            Logger.InfoFormat("Exiting {0}.GetPPIDCountSearch({1})", svcName, userName);
            return PPIDCount;
        }
        public static List<UserConfigurations> GetUserConfigurations()
        {
            Logger.InfoFormat("Entering {0}.GetUserConfigurations({1})", svcName, "");
            List<UserConfigurations> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<UserConfigurations>("USP_GetUserConfigurations").ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetUserConfigurations({1})", svcName, "");
            return value;
        }

        /// <summary>
        ///Insertes user log information
        /// </summary>
        /// <param name="inputparams">The inputparams.</param>
        /// <returns></returns>

        public static int SaveUserAuditLog(string userName, string sessionId, string ipAddress, int loggedOut)
        {
            Logger.InfoFormat("Entering {0}.SaveUserAuditLog({1})", svcName, userName);
            IEnumerable<int> value = null;
            var NewID = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_SaveUserAuditLog @Username,@SessionId,@IpAddress,@Logout",
                    new SqlParameter("Username", userName),
                    new SqlParameter("SessionId", sessionId != null ? sessionId : DBNull.Value.ToString()),
                    new SqlParameter("IpAddress", ipAddress != null ? ipAddress : DBNull.Value.ToString()),
                    new SqlParameter("Logout", loggedOut));
                NewID = value.Single();
            }
            Logger.InfoFormat("Exiting {0}.SaveUserAuditLog({1})", svcName, userName);
            return NewID;
        }

        /// <summary>
        ///Insertes user log information
        /// </summary>
        /// <param name="inputparams">The inputparams.</param>
        /// <returns></returns>

        public static int SaveUserTransactionLog(int usrAuditLogId, int moduleId, string actionDone, string accountNo, string dealerNo)
        {
            Logger.InfoFormat("Entering {0}.SaveUserTransactionLog({1})", svcName, usrAuditLogId);
            IEnumerable<int> value = null;
            var NewID = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_SaveUserTransactionLog @UsrAuditLogId,@ModuleId,@ActionDone,@AccountNo,@DealerNo",
                    new SqlParameter("UsrAuditLogId", usrAuditLogId),
                    new SqlParameter("ModuleId", moduleId),
                    new SqlParameter("ActionDone", actionDone != null ? actionDone : DBNull.Value.ToString()),
                    new SqlParameter("AccountNo", accountNo != null ? accountNo : DBNull.Value.ToString()),
                    new SqlParameter("DealerNo", dealerNo != null ? dealerNo : DBNull.Value.ToString()));
                NewID = value.Single();
            }
            Logger.InfoFormat("Exiting {0}.SaveUserTransactionLog({1})", svcName, usrAuditLogId);
            return NewID;
        }
    }
}
