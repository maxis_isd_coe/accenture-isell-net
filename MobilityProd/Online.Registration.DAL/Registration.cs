﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Data.Objects;
using System.Runtime.Serialization;
using SNT.Utility;
using System.Data.SqlClient;
using Online.Registration.DAL.Models;
using log4net;
using System.Data.Entity.Validation;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Linq;
using System.Data.SqlTypes;

namespace Online.Registration.DAL
{
    public class Registration
    {
        static string svcName = "Online.Registration.DAL";
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Registration));
        private static readonly string _ConnectionString = !ReferenceEquals(ConfigurationManager.ConnectionStrings["OnlineStoreDbContext"], null) ? ConfigurationManager.ConnectionStrings["OnlineStoreDbContext"].ToString2() : "";

        #region Registration

        /// <summary>
        /// This Method is used to save Suplementary New Account Feature against Registration Id
        /// </summary>
        /// <param name="objLnkRegDetails">Object of the Table</param>
        /// <returns></returns>
        public static int SaveLnkRegistrationDetails(DAL.Models.LnkRegDetails objLnkRegDetails)
        {

            Logger.InfoFormat("Entering {0}.SaveLnkRegistrationDetails({1})", svcName, (objLnkRegDetails != null) ? objLnkRegDetails.UserName : "NULL");
            var value = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    ctx.LnkRegistrationDetails.Add(objLnkRegDetails);
                    ctx.SaveChanges();
                    trans.Complete();
                }
            }

            value = objLnkRegDetails.Id;
            Logger.InfoFormat("Exiting {0}.SaveLnkRegistrationDetails({1}): {2}", svcName, (objLnkRegDetails != null) ? objLnkRegDetails.UserName : "NULL", value);
            return value;
        }

        public static int SaveRegSuppLines(List<DAL.Models.RegSuppLinesMap> objRegSupplinesMap)
        {
            Logger.InfoFormat("Entering {0}.regSupplinesMap RegId({1})", svcName, (objRegSupplinesMap != null && objRegSupplinesMap.Any()) ? objRegSupplinesMap.FirstOrDefault().RegId.ToString() : "");
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    foreach (var regSupplineMap in objRegSupplinesMap)
                    {
                        ctx.RegSuppLineMaps.Add(regSupplineMap);
                    }
                    ctx.SaveChanges();
                    trans.Complete();
                }
            }
            Logger.InfoFormat("Entering {0}.regSupplinesMap RegId({1})", svcName, (objRegSupplinesMap != null && objRegSupplinesMap.Any()) ? objRegSupplinesMap.FirstOrDefault().RegId.ToString() : "");
            return objRegSupplinesMap.Count;
        }

		public static int SaveListRegAttributes(List<DAL.Models.RegAttributes> objListRegAttributes)
		{
			Logger.InfoFormat("Entering {0}.SaveListRegAttributes RegId({1})", svcName, (objListRegAttributes != null && objListRegAttributes.Any()) ? objListRegAttributes.FirstOrDefault().RegID.ToString() : "");
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				using (var trans = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
				{
					IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
				}))
				{
					foreach (var regAttribute in objListRegAttributes)
					{
						ctx.RegAttributes.Add(regAttribute);
					}
					ctx.SaveChanges();
					trans.Complete();
				}
			}
			Logger.InfoFormat("Exiting {0}.SaveListRegAttributes RegId({1})", svcName, (objListRegAttributes != null && objListRegAttributes.Any()) ? objListRegAttributes.FirstOrDefault().RegID.ToString() : "");
			return objListRegAttributes.Count;
		}

		public static int SaveRegAttributes(DAL.Models.RegAttributes objRegAttributes)
		{
			Logger.InfoFormat("Entering {0}.SaveRegAttributes RegId({1})", svcName, objRegAttributes.RegID.ToString2());
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				using (var trans = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
				{
					IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
				}))
				{
					ctx.RegAttributes.Add(objRegAttributes);	
					ctx.SaveChanges();
					trans.Complete();
				}
			}
			Logger.InfoFormat("Exiting {0}.SaveRegAttributes RegId({1})", svcName, objRegAttributes.RegID.ToString2());
			return objRegAttributes.ID;
		}

		public static int SaveRegMdlGrpModels(List<DAL.Models.RegMdlGrpModel> objRegMdlGrpModels)
		{
			Logger.InfoFormat("Entering {0}.SaveRegMdlGrpModel RegId({1})", svcName, (objRegMdlGrpModels != null && objRegMdlGrpModels.Any()) ? objRegMdlGrpModels.FirstOrDefault().RegID.ToString() : "");
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				using (var trans = new TransactionScope())
				{
					foreach (var regMdlGrpModel in objRegMdlGrpModels)
					{
						ctx.RegMdlGrpModels.Add(regMdlGrpModel);
					}
					ctx.SaveChanges();
					trans.Complete();
				}
			}
			Logger.InfoFormat("Entering {0}.SaveRegMdlGrpModel RegId({1})", svcName, (objRegMdlGrpModels != null && objRegMdlGrpModels.Any()) ? objRegMdlGrpModels.FirstOrDefault().RegID.ToString() : "");
			return objRegMdlGrpModels.Count;
		}

		public static int SaveRegMdlGrpModel(RegMdlGrpModel objRegMdlGrpModel)
		{
			Logger.InfoFormat("Entering {0}.SaveRegMdlGrpModel RegId({1})", svcName, (objRegMdlGrpModel != null) ? objRegMdlGrpModel.RegID.ToString() : "");
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				using (var trans = new TransactionScope())
				{

					ctx.RegMdlGrpModels.Add(objRegMdlGrpModel);
					ctx.SaveChanges();
					trans.Complete();
				}
			}
			Logger.InfoFormat("Entering {0}.SaveRegMdlGrpModel RegId({1})", svcName, (objRegMdlGrpModel != null) ? objRegMdlGrpModel.RegID.ToString() : "");
			return objRegMdlGrpModel.ID;
		}


        public static int UpdateLnkRegistrationDetails(DAL.Models.LnkRegDetails objLnkRegDetails)
        {
            Logger.InfoFormat("Entering {0}.UpdateLnkRegistrationDetails({1})", svcName, (objLnkRegDetails != null) ? objLnkRegDetails.Id : 0);
            var value = 0;
            DAL.Models.LnkRegDetails objLnkReg;
            if (objLnkRegDetails != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    //var objLnkReg = ctx.LnkRegistrationDetails.Find(objLnkRegDetails.Id);
                    objLnkReg = (LnkRegDetails)ctx.LnkRegistrationDetails.FirstOrDefault(a => a.RegId == objLnkRegDetails.RegId);
                    if (objLnkReg != null)
                    {
                        objLnkReg.RegId = objLnkRegDetails.RegId;
                        objLnkReg.CmssId = objLnkRegDetails.CmssId;
                        objLnkReg.OldSimSerial = objLnkRegDetails.OldSimSerial;
                        objLnkReg.NewSimSerial = objLnkRegDetails.NewSimSerial;
                        objLnkReg.CreatedDate = System.DateTime.Now;
                        objLnkReg.UserName = objLnkRegDetails.UserName;
                        objLnkReg.IsSuppNewAc = objLnkRegDetails.IsSuppNewAc;
                        objLnkReg.TUTSerial = objLnkRegDetails.TUTSerial;
                        objLnkReg.TUTSimModelId = objLnkRegDetails.TUTSimModelId;
                        value = ctx.SaveChanges();
                    }
                }
            }

            Logger.InfoFormat("Exiting {0}.UpdateExtraTen({1}): {2}", svcName, (objLnkRegDetails != null) ? objLnkRegDetails.Id : 0, value);
            return value;
        }



        /// <summary>
        /// This Method is used to Get the details against Registration Id
        /// </summary>
        /// <param name="objLnkRegDetails">Object of the Table</param>
        /// <returns></returns>
        public static DAL.Models.LnkRegDetails GetLnkRegistrationDetails(int regid)
        {

            Logger.InfoFormat("Entering {0}.GetLnkRegistrationDetails({1})", svcName, (regid != null) ? regid.ToString() : "NULL");
            var value = 0;
            DAL.Models.LnkRegDetails objLnkRegDetails = new LnkRegDetails();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                objLnkRegDetails = ctx.LnkRegistrationDetails.Where(a => a.RegId == regid).ToList().OrderByDescending(a => a.CreatedDate).FirstOrDefault();

            }


            Logger.InfoFormat("Exiting {0}.GetLnkRegistrationDetails({1}): {2}", svcName, (regid != null) ? regid.ToString() : "NULL", value);
            return objLnkRegDetails;
        }

        public static DAL.Models.LnkRegDetails LnkRegistrationGetByRegId(int regId)
        {
            LnkRegDetails value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.LnkRegistrationDetails.Where(a => a.RegId == regId).FirstOrDefault();
            }
            return value;
        }

        public static int SecondaryLineRegistrationCreate(DAL.Models.RegistrationSec oReqSec, List<RegPgmBdlPkgCompSec> regPgmBdlPkgCompSecs)
        {
            Logger.InfoFormat("Entering {0}.SecondaryLineRegistrationCreate({1})", svcName, (oReqSec != null) ? oReqSec.CustomerID.ToString() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {

                    if (oReqSec != null)
                    {
                        // oReqSec.RegistrationID = oReqSec.ID;                     
                        var query = ctx.Registrations.Where(a => a.MSISDN1 == oReqSec.externalId);
                        List<int> ID = query.Select(w => w.ID).ToList();
                        oReqSec.RegistrationID = ID[0];
                        ctx.RegistrationSecs.Add(oReqSec);
                        ctx.SaveChanges();
                    }

                    if (regPgmBdlPkgCompSecs != null)
                    {
                        foreach (var regPBPC in regPgmBdlPkgCompSecs)
                        {
                            regPBPC.RegID = oReqSec.ID;
                            ctx.RegPgmBdlPkgCompSecs.Add(regPBPC);
                        }
                        ctx.SaveChanges();
                    }


                    trans.Complete();
                }
            }

            value = oReqSec.RegistrationID;

            Logger.InfoFormat("Exiting {0}.SecondaryLineRegistrationCreate({1}): {2}", svcName, (oReqSec != null) ? oReqSec.CustomerID.ToString() : "NULL", value);
            return value;
        }

        public static bool UpsertBRE (RegBREStatus objBre)
        {
            // insert or update
            bool status = false;
            Logger.InfoFormat("Entering {0}.UpsertBRE({1})", svcName, objBre.RegId);
            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    // if reg id found do update, else insert new
                    var dataRegBREStatus = ctx.RegBREStatus.FirstOrDefault(e => e.RegId == objBre.RegId);
                    if (dataRegBREStatus != null)
                    {
						using (var trans = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
						{
							IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
						}))
                        {
                            if (!string.IsNullOrEmpty(objBre.TransactionStatus))
                            {
                                dataRegBREStatus.TransactionStatus = objBre.TransactionStatus;
                            }
                            if (!string.IsNullOrEmpty(objBre.TransactionBy))
                            {
                                dataRegBREStatus.TransactionBy = objBre.TransactionBy;
                            }
                            if (!string.IsNullOrEmpty(objBre.TransactionReason))
                            {
                                dataRegBREStatus.TransactionReason = objBre.TransactionReason;
                            }
                            if (!string.IsNullOrEmpty(objBre.IsLockedBy))
                            {
                                dataRegBREStatus.IsLockedBy = objBre.IsLockedBy;
                            }
                            if (!ReferenceEquals(objBre.TransactionDT,null))
                            {
                                dataRegBREStatus.TransactionDT = objBre.TransactionDT;
                            }
                            status = ctx.SaveChanges() == 1 ? true : false;
                            trans.Complete();
							trans.Dispose();
                        }
                    }
                    else
                    {
                        ctx.RegBREStatus.Add(objBre);
                        ctx.SaveChanges();
                        status = true;
                    }

                }
                
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the UpsertBRE method:" + ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UpsertBRE({1}): ", svcName, objBre.RegId);
            }
            return status;
        }


        public static bool UpdateBRE(int regid, string accessUser, RegBREStatus objBre = null)
        {
            bool status = false;
            Logger.InfoFormat("Entering {0}.UpdateBRE({1})", svcName, regid);
            try
            {/*
                if (ReferenceEquals(objBre, null))
                {
                    objBre = new RegBREStatus()
                    {
                        RegId = regid,
                        TransactionBy = accessUser,
                        TransactionStatus = "Pending",
                        TransactionDT = DateTime.Now
                    };
                }
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    using (var trans = new TransactionScope())
                    {
                        // if reg id found do update, else insert new
                        var dataRegBREStatus = ctx.RegBREStatus.FirstOrDefault(e => e.RegId == regid);
                        if (dataRegBREStatus != null)
                        {
                            if (!string.IsNullOrEmpty(objBre.TransactionStatus))
                            {
                                dataRegBREStatus.TransactionStatus = objBre.TransactionStatus;
                            }
                            if (!string.IsNullOrEmpty(objBre.TransactionBy))
                            {
                                dataRegBREStatus.TransactionBy = objBre.TransactionBy;
                            }
                            if (!string.IsNullOrEmpty(objBre.TransactionReason))
                            {
                                dataRegBREStatus.TransactionReason = objBre.TransactionReason;
                            }
                            if (!string.IsNullOrEmpty(objBre.IsLockedBy))
                            {
                                dataRegBREStatus.IsLockedBy = objBre.IsLockedBy;
                            }
                            status = ctx.SaveChanges() == 1 ? true : false;
                            trans.Complete();
                        }
                        else
                        {
                            ctx.RegBREStatus.Add(objBre);
                            ctx.SaveChanges();
                            status = true;
                        }
                    }
                }*/
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the UpdateBRE method:" + ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UpdateBRE({1}): ", svcName, regid);
            }
            return status;
        }
        public static int RegistrationCreate(DAL.Models.Registration oReq, Customer customer, List<Address> addresses, List<RegPgmBdlPkgComp> regPgmBdlPkgComps, RegMdlGrpModel regMdlGrpModel,
                                             RegStatus regStatus, List<RegSuppLine> regSuppLines, List<RegPgmBdlPkgComp> regSuppLineVASes, List<string> CustPhotos, List<RegSmartComponents> regSmartComps, List<WaiverComponents> waiverComponents, bool isBREFail = false, int approverID = 0, DateTime? TimeApproval = null)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCreate({1})", svcName, (customer != null) ? customer.FullName : "NULL");
            var value = 0;
            int regid = 0;
            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    #region "xml assigning"
                    string oReqXml = string.Empty;
                    string customerXml = string.Empty;
                    string addressesXml = string.Empty;
                    string regPgmBdlPkgCompsXml = string.Empty;
                    string regMdlGrpModelXml = string.Empty;
                    string regStatusXml = string.Empty;
                    string regSuppLinesXml = string.Empty;
                    string regSuppLineVASesXml = string.Empty;
                    string regSmartCompsXml = string.Empty;
                    string waiverComponentsXml = string.Empty;

                    oReqXml = Common.ConvertObjectToXml(oReq, typeof(DAL.Models.Registration)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                    customerXml = Common.ConvertObjectToXml(customer, typeof(Customer)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                    addressesXml = Common.ConvertObjectToXml(addresses, typeof(List<Address>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                    regPgmBdlPkgCompsXml = Common.ConvertObjectToXml(regPgmBdlPkgComps, typeof(List<RegPgmBdlPkgComp>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                    regMdlGrpModelXml = Common.ConvertObjectToXml(regMdlGrpModel, typeof(RegMdlGrpModel)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                    regStatusXml = Common.ConvertObjectToXml(regStatus, typeof(RegStatus)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                    regSuppLinesXml = Common.ConvertObjectToXml(regSuppLines, typeof(List<RegSuppLine>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                    regSuppLineVASesXml = Common.ConvertObjectToXml(regSuppLineVASes, typeof(List<RegPgmBdlPkgComp>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                    regSmartCompsXml = Common.ConvertObjectToXml(regSmartComps, typeof(List<RegSmartComponents>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                    waiverComponentsXml = Common.ConvertObjectToXml(waiverComponents, typeof(List<WaiverComponents>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);

                    #endregion

                    #region "SP Logic implementation"
                    using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
                    {
                        sqlConn.Open();
                        SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_CREATEREGISTRATION", sqlConn);
                        if (oReq != null)
                            sqlCmd.Parameters.Add(new SqlParameter("@RegistrationXml", oReqXml));

                        if ((customer != null))
                            sqlCmd.Parameters.Add(new SqlParameter("@Customer", customerXml));

                        if (addresses != null)
                            sqlCmd.Parameters.Add(new SqlParameter("@Addresses", addressesXml));

                        if ((regPgmBdlPkgComps != null) && (regPgmBdlPkgComps.Count > 0))
                            sqlCmd.Parameters.Add(new SqlParameter("@RegPgmBdlPkgComp_Registration", regPgmBdlPkgCompsXml));

                        if ((regMdlGrpModel != null) && (regMdlGrpModel.ModelImageID > 0))
                            sqlCmd.Parameters.Add(new SqlParameter("@RegMdlGrpModel", regMdlGrpModelXml));

                        if (regStatus != null)
                            sqlCmd.Parameters.Add(new SqlParameter("@RegStatus", regStatusXml));

                        if ((regSuppLines != null) && (regSuppLines.Count > 0))
                            sqlCmd.Parameters.Add(new SqlParameter("@RegSuppLines", regSuppLinesXml));

                        if ((regSuppLineVASes != null) && (regSuppLines.Count > 0) && (regSuppLineVASes.Count > 0))
                            sqlCmd.Parameters.Add(new SqlParameter("@RegPgmBdlPkgComp_Supplines", regSuppLineVASesXml));

                        if ((waiverComponents != null) && (waiverComponents.Count > 0))
                            sqlCmd.Parameters.Add(new SqlParameter("@WaiverComponents", waiverComponentsXml));

                        SqlParameter sqlParamOut = new SqlParameter("@RegID", SqlDbType.Int);
                        sqlParamOut.Direction = ParameterDirection.Output;
                        sqlCmd.Parameters.Add(sqlParamOut);

                        sqlCmd.Parameters.Add(new SqlParameter("@ApproverID", approverID));
                        sqlCmd.Parameters.Add(new SqlParameter("@TimeApproval", TimeApproval));

                        if ((regSmartComps != null) && (regSmartComps.Count > 0))
                            sqlCmd.Parameters.Add(new SqlParameter("@RegSmartComponents", regSmartCompsXml));

                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.ExecuteNonQuery();

                        regid = Convert.ToInt32(sqlParamOut.Value);
                        sqlConn.Close();
                    }
                    #endregion
                }
                value = regid;

                //if (isBREFail)
                //{
                //    UpdateBRE(regid, "");
                //}
            }
            catch (Exception ex)
            {

                Logger.Info("Exception while excuting the RegistrationCreate method:" + ex);
            }
            Logger.InfoFormat("Exiting {0}.RegistrationCreate({1}): {2}", svcName, (customer != null) ? customer.FullName.ToString2() : "NULL", value);
            return value;
        }

        public static int RegistrationCreateWithSec(DAL.Models.Registration oReq, Customer customer, List<Address> addresses, List<RegPgmBdlPkgComp> regPgmBdlPkgComps, RegMdlGrpModel regMdlGrpModel,
                                            RegStatus regStatus, List<RegSuppLine> regSuppLines, List<RegPgmBdlPkgComp> regSuppLineVASes, List<string> CustPhotos,
                                            DAL.Models.RegistrationSec oReqSec, List<RegPgmBdlPkgCompSec> regPgmBdlPkgCompSecs, RegMdlGrpModelSec regMdlGrpModelSec, List<WaiverComponents> waiverComponents, bool isBREFail = false, int approverID = 0, DateTime? TimeApproval = null)
        {
            DateTime stDate = DateTime.Now;
            Logger.InfoFormat("Entering {0}.RegistrationCreateWithSec({1})", svcName, (customer != null) ? customer.FullName : "NULL");
            var value = 0;
            int regid = 0;
            try
            {
                #region "xml assigning"
                string oReqXml = string.Empty;
                string customerXml = string.Empty;
                string addressesXml = string.Empty;
                string regPgmBdlPkgCompsXml = string.Empty;
                string regMdlGrpModelXml = string.Empty;
                string regStatusXml = string.Empty;
                string regSuppLinesXml = string.Empty;
                string regSuppLineVASesXml = string.Empty;
                string CustPhotosXml = string.Empty;
                string oReqSecXml = string.Empty;
                string regPgmBdlPkgCompSecsXml = string.Empty;
                string regMdlGrpModelSecXml = string.Empty;
                string waiverComponentsXml = string.Empty;
                oReqXml = Common.ConvertObjectToXml(oReq, typeof(DAL.Models.Registration)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                customerXml = Common.ConvertObjectToXml(customer, typeof(Customer)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                addressesXml = Common.ConvertObjectToXml(addresses, typeof(List<Address>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                regPgmBdlPkgCompsXml = Common.ConvertObjectToXml(regPgmBdlPkgComps, typeof(List<RegPgmBdlPkgComp>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                regMdlGrpModelXml = Common.ConvertObjectToXml(regMdlGrpModel, typeof(RegMdlGrpModel)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                regStatusXml = Common.ConvertObjectToXml(regStatus, typeof(RegStatus)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                regSuppLinesXml = Common.ConvertObjectToXml(regSuppLines, typeof(List<RegSuppLine>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                regSuppLineVASesXml = Common.ConvertObjectToXml(regSuppLineVASes, typeof(List<RegPgmBdlPkgComp>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                
                oReqSecXml = Common.ConvertObjectToXml(oReqSec, typeof(DAL.Models.RegistrationSec)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                regPgmBdlPkgCompSecsXml = Common.ConvertObjectToXml(regPgmBdlPkgCompSecs, typeof(List<RegPgmBdlPkgCompSec>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                regMdlGrpModelSecXml = Common.ConvertObjectToXml(regMdlGrpModelSec, typeof(RegMdlGrpModelSec)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                waiverComponentsXml = Common.ConvertObjectToXml(waiverComponents, typeof(List<WaiverComponents>)).Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", string.Empty);
                #endregion
                Logger.Info("Rgistration For Images:" + oReqXml.ToString2());
                using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
                {
                    sqlConn.Open();
                    SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                    DataSet dsRegistrations = new DataSet();
                    SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_CREATEREGISTRATION", sqlConn);
                    if (!ReferenceEquals(oReq, null))
                        sqlCmd.Parameters.Add(new SqlParameter("@RegistrationXml", oReqXml));

                    if (!ReferenceEquals(customer, null))
                        sqlCmd.Parameters.Add(new SqlParameter("@Customer", customerXml));

                    if (!ReferenceEquals(addresses, null))
                        sqlCmd.Parameters.Add(new SqlParameter("@Addresses", addressesXml));

                    if (!ReferenceEquals(regPgmBdlPkgComps, null) && (regPgmBdlPkgComps.Count > 0))
                        sqlCmd.Parameters.Add(new SqlParameter("@RegPgmBdlPkgComp_Registration", regPgmBdlPkgCompsXml));

                    if (!ReferenceEquals(regMdlGrpModel, null) && (regMdlGrpModel.ModelImageID > 0))
                        sqlCmd.Parameters.Add(new SqlParameter("@RegMdlGrpModel", regMdlGrpModelXml));

                    if (!ReferenceEquals(regStatus, null))
                        sqlCmd.Parameters.Add(new SqlParameter("@RegStatus", regStatusXml));

                    if (!ReferenceEquals(regSuppLines, null) && (regSuppLines.Count > 0))
                        sqlCmd.Parameters.Add(new SqlParameter("@RegSuppLines", regSuppLinesXml));

                    if (!ReferenceEquals(regSuppLineVASes, null) && (regSuppLines.Count > 0) && (regSuppLineVASes.Count > 0))
                        sqlCmd.Parameters.Add(new SqlParameter("@RegPgmBdlPkgComp_Supplines", regSuppLineVASesXml));

                    if (!ReferenceEquals(oReqSec, null) && (oReqSec.Trn_Type != string.Empty))
                        sqlCmd.Parameters.Add(new SqlParameter("@RegistrationSec", oReqSecXml));

                    if (!ReferenceEquals(regPgmBdlPkgCompSecs, null) && (regPgmBdlPkgCompSecs.Count > 0))
                        sqlCmd.Parameters.Add(new SqlParameter("@RegPgmBdlPkgCompSec", regPgmBdlPkgCompSecsXml));

                    if (!ReferenceEquals(regMdlGrpModelSec, null) && (regMdlGrpModelSec.ModelImageID > 0))
                        sqlCmd.Parameters.Add(new SqlParameter("@RegMdlGrpModelSec", regMdlGrpModelSecXml));

                    if (!ReferenceEquals(waiverComponents, null) && (waiverComponents.Count > 0))
                        sqlCmd.Parameters.Add(new SqlParameter("@WaiverComponents", waiverComponentsXml));

                    sqlCmd.Parameters.Add(new SqlParameter("@ApproverID", approverID));
                    sqlCmd.Parameters.Add(new SqlParameter("@TimeApproval", TimeApproval));

                    SqlParameter sqlParamOut = new SqlParameter("@RegID", SqlDbType.Int);
                    sqlParamOut.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(sqlParamOut);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.ExecuteNonQuery();
                    //adapter.SelectCommand = sqlCmd;
                    //adapter.Fill(dsRegistrations);
                    regid = Convert.ToInt32(sqlParamOut.Value);
                    sqlConn.Close();
                }
                value = regid;
                //if (isBREFail)
                //{
                //    UpdateBRE(regid, "");
                //}
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the RegistrationCreate_CRP method:" + ex);
            }
            //Logger.InfoFormat("Exiting {0}.RegistrationSecCreate({1}): {2}", svcName, (oReq != null) ? oReq.Customer.FullName : "NULL", value);
            TimeSpan timeSpn = (DateTime.Now - stDate);
            Common.LogMessage(string.Format("RegistrationSecCreate({0}) TimeSpan:{1}", ReferenceEquals(customer, null) ? string.Empty : customer.FullName.ToString2(), timeSpn.ToString("hh\\:mm\\:ss\\.fff")));
            return value;
        }

        //  Spend Limit Commented by Ravi As per New Flow on June 15 2013

        #region "Spend Limit"
        /// <summary>
        /// Used to save the Spend Limit against the Registration Id
        /// </summary>
        /// <param name="oReq"></param>
        /// <returns>integer</returns>
        public static int SaveSpendLimit(DAL.Models.lnkRegSpendLimit oReq)
        {

            Logger.InfoFormat("Entering {0}.SaveSpendLimit({1})", svcName);
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    ctx.lnkRegSpendLimit.Add(oReq);
                    ctx.SaveChanges();
                    trans.Complete();
                }
            }

            value = oReq.Id;

            Logger.InfoFormat("Exiting {0}.SaveSpendLimit({1})", svcName, (oReq != null ? oReq.Id : 0));
            return value;
        }


        public static lnkRegSpendLimit SpendLimitGet(int regId)
        {
            lnkRegSpendLimit value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                value = ctx.lnkRegSpendLimit.Where(a => a.RegId == regId).SingleOrDefault();

            }
            return value;
        }

        #endregion

        #region "Extra Ten"
        /// <summary>
        /// Used to save the Extra Ten against the Registration Id
        /// </summary>
        /// <param name="oReq"></param>
        /// <returns>integer</returns>
        public static int SaveExtraTen(DAL.Models.lnkExtraTenDetails oReq)
        {

            Logger.InfoFormat("Entering {0}.SaveExtraTen({1})", svcName);
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    ctx.lnkExtraTenDetails.Add(oReq);
                    ctx.SaveChanges();
                    trans.Complete();
                }
            }

            value = oReq.Id;

            Logger.InfoFormat("Exiting {0}.SaveExtraTen({1})", svcName, oReq.Id);
            return value;
        }

        public static int DeleteExtraTen(int extraTenId)
        {
            Logger.InfoFormat("Entering {0}.DeleteExtraTen({1})", svcName, extraTenId);
            List<int> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_DeleteExtraTenById @ExtraTenId", new System.Data.SqlClient.SqlParameter("@ExtraTenId", extraTenId)).ToList();

            }
            Logger.InfoFormat("Exiting {0}.DeleteExtraTen({1})", svcName, extraTenId);
            return value.First();

        }

        public static List<lnkExtraTenDetails> GetExtraTenDetails(int regId)
        {
            Logger.InfoFormat("Entering {0}.GetExtraTenDetails({1})", svcName, regId);
            List<lnkExtraTenDetails> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.lnkExtraTenDetails.Where(e => e.RegId == regId).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetExtraTenDetails({1})", svcName, regId);
            return value;

        }

        public static int DeleteExtraTenDetails(int regId)
        {
            Logger.InfoFormat("Entering {0}.DeleteExtraTenDetails({1})", svcName, regId);
            List<int> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_DeleteExtraTenByRegId @RegId", new System.Data.SqlClient.SqlParameter("@RegId", regId)).ToList();

            }
            Logger.InfoFormat("Exiting {0}.DeleteExtraTenDetails({1})", svcName, regId);
            return value.FirstOrDefault();

        }
        public static int UpdateExtraTen(DAL.Models.lnkExtraTenDetails oReq)
        {
            Logger.InfoFormat("Entering {0}.UpdateExtraTen({1})", svcName, (oReq != null) ? oReq.Id : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var objExtraTen = ctx.lnkExtraTenDetails.Find(oReq.Id);
                    objExtraTen.Id = oReq.Id;
                    objExtraTen.RegId = oReq.RegId;
                    objExtraTen.Status = oReq.Status;
                    objExtraTen.Msisdn = oReq.Msisdn;
                    objExtraTen.OldMsisdn = oReq.OldMsisdn;
                    objExtraTen.CreatedDate = System.DateTime.Now;
                    objExtraTen.CreatedBy = oReq.CreatedBy;
                    objExtraTen.MsgCode = oReq.MsgCode;
                    objExtraTen.MsgDesc = oReq.MsgDesc;
                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.UpdateExtraTen({1}): {2}", svcName, (oReq != null) ? oReq.Id : 0, value);
            return value;
        }



        /// <summary>
        /// Used to save the LnkExtraTenConfirmationLog against the Registration Id
        /// </summary>
        /// <param name="oReq"></param>
        /// <returns>integer</returns>
        public static int SaveExtraTenLogs(DAL.Models.LnkExtraTenConfirmationLog oReq)
        {

            Logger.InfoFormat("Entering {0}.SaveExtraTenLogs", svcName);
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    ctx.LnkExtraTenConfirmationLog.Add(oReq);
                    ctx.SaveChanges();
                    trans.Complete();
                }
            }

            value = oReq.Id;

            Logger.InfoFormat("Exiting {0}.SaveExtraTenLogs({1})", svcName, oReq.Id);
            return value;
        }
        // Extra Ten Add Edit and Delete for external users 
        public static int SaveExtraTenAddEditDetails(DAL.Models.lnkExtraTenAddEditDetails oReq)
        {

            Logger.InfoFormat("Entering {0}.SaveExtraTenAddEditDetails({1})", svcName);
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    ctx.LnkExtraTenAddEditDetails.Add(oReq);
                    ctx.SaveChanges();
                    trans.Complete();
                }
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.SaveExtraTenAddEditDetails({1})", svcName, oReq.ID);
            return value;
        }
        #endregion

        // Home registration create
        public static int HomeRegistrationCreate(DAL.Models.Registration oReq, Customer customer, List<Address> addresses, List<RegPgmBdlPkgComp> regPgmBdlPkgComps, RegStatus regStatus)
        {
            Logger.InfoFormat("Entering {0}.HomeRegistrationCreate({1})", svcName, (customer != null) ? customer.FullName : "NULL");
            var value = 0;

            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    using (var trans = new TransactionScope())
                    {
                        ctx.Customers.Add(customer);
                        ctx.SaveChanges();

                        oReq.CustomerID = customer.ID;
                        ctx.Registrations.Add(oReq);
                        ctx.SaveChanges();

                        //if (regMdlGrpModel.ModelGroupModelID.ToInt() != 0)
                        //{
                        //    regMdlGrpModel.RegID = oReq.ID;
                        //    ctx.RegMdlGrpModels.Add(regMdlGrpModel);
                        //    ctx.SaveChanges();
                        //}

                        foreach (var address in addresses)
                        {
                            address.RegID = oReq.ID;
                            ctx.Addresses.Add(address);
                        }
                        ctx.SaveChanges();

                        foreach (var regPBPC in regPgmBdlPkgComps)
                        {
                            regPBPC.RegID = oReq.ID;
                            ctx.RegPgmBdlPkgComps.Add(regPBPC);
                        }
                        ctx.SaveChanges();

                        regStatus.RegID = oReq.ID;
                        ctx.RegStatuses.Add(regStatus);
                        ctx.SaveChanges();

                        //foreach (var regSubline in regSuppLines)
                        //{
                        //    regSubline.RegID = oReq.ID;
                        //    ctx.RegSuppLines.Add(regSubline);
                        //    ctx.SaveChanges();

                        //    var vases = regSuppLineVASes.Where(a => a.SequenceNo == regSubline.SequenceNo).ToList();
                        //    foreach (var vas in vases)
                        //    {
                        //        vas.RegID = oReq.ID;
                        //        vas.RegSuppLineID = regSubline.ID;
                        //        ctx.RegPgmBdlPkgComps.Add(vas);
                        //        ctx.SaveChanges();
                        //    }
                        //}

                        trans.Complete();
                    }
                }

                value = oReq.ID;
            }
            catch (Exception ex)
            {

                Logger.Info("Exception while excuting the HomeRegistrationCreate method:" + ex);
            }

            Logger.InfoFormat("Exiting {0}.HomeRegistrationCreate({1}): {2}", svcName, (oReq != null) ? oReq.Customer.FullName : "NULL", value);
            return value;
        }
        /// <summary>
        /// MNP supplementary lines update.
        /// </summary>
        /// <param name="oReq">The o req.</param>
        /// <returns>TypeOf(int)</returns>
        public static int RegSuppLineUpdate(List<DAL.Models.RegSuppLine> oReqs)
        {
            //DAL.Models.RegSuppLine oReq = null;
            Logger.InfoFormat("Entering {0}.RegSuppLineUpdate({1})", svcName, (oReqs != null) ? oReqs.FirstOrDefault().ID : 0);
            var value = 0;

            try
            {
                if (!ReferenceEquals(oReqs, null))
                {
                    using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                    {
                        using (var trans = new TransactionScope())
                        {
                            foreach (var oReq in oReqs)
                            {
                                var reg = ctx.RegSuppLines.Find(oReq.ID);

                                if (!string.IsNullOrEmpty(oReq.IMEINumber))
                                    reg.IMEINumber = oReq.IMEINumber;

                                if (!string.IsNullOrEmpty(oReq.SIMSerial))
                                    reg.SIMSerial = oReq.SIMSerial;

                                if (oReq.SimModelId > 0)
                                    reg.SimModelId = oReq.SimModelId;

                                if (!string.IsNullOrEmpty(oReq.UOMCode))
                                    reg.UOMCode = oReq.UOMCode;

                                if (!string.IsNullOrEmpty(oReq.ArticleID))
                                    reg.ArticleID = oReq.ArticleID;

                                if (!string.IsNullOrEmpty(oReq.EXT_ORDER_ID))
                                    reg.EXT_ORDER_ID = oReq.EXT_ORDER_ID;

                                if (!string.IsNullOrEmpty(oReq.Order_Status))
                                    reg.Order_Status = oReq.Order_Status;

								if (!string.IsNullOrEmpty(oReq.MNPSuppLineSimCardType))
									reg.MNPSuppLineSimCardType = oReq.MNPSuppLineSimCardType;

                                reg.LastAccessID = oReq.LastAccessID;
                                reg.LastUpdateDT = DateTime.Now;
                                value = ctx.SaveChanges();
                            }
                            trans.Complete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                value = -1;
            }
            Logger.InfoFormat("Exiting {0}.RegSuppLineUpdate({1}): {2}", svcName, (oReqs != null) ? oReqs.FirstOrDefault().ID : 0, value);
            return value;
        }
        /* COMMENTED FOR PERFORMANCE IMPROVEMENT, AND CONVERTED INTO SP 
        public static int RegistrationUpdate(DAL.Models.Registration oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var reg = ctx.Registrations.Find(oReq.ID);

                    if (!string.IsNullOrEmpty(oReq.IMEINumber))
                    {
                        reg.IMEINumber = oReq.IMEINumber;
                    }

                    if (!string.IsNullOrEmpty(oReq.SIMSerial))
                    {
                        reg.SIMSerial = oReq.SIMSerial;
                    }

                    if (!string.IsNullOrEmpty(oReq.SalesPerson))
                    {
                        reg.SalesPerson = oReq.SalesPerson;
                    }

                    if (oReq.SimModelId > 0)
                    {
                        reg.SimModelId = oReq.SimModelId;
                    }

                    //Added for inserting virtual number for MISM in Secondary Line
                    if (!string.IsNullOrEmpty(oReq.MSISDN1))
                        reg.MSISDN1 = oReq.MSISDN1;

                    if (oReq.PenaltyAmount >= 0)
                    {
                        reg.PenaltyAmount = oReq.PenaltyAmount;
                    }

                    if (oReq.LastAccessID != null)
                        reg.LastAccessID = oReq.LastAccessID;

                    reg.LastUpdateDT = DateTime.Now;

                    // added by Nreddy for update fxAcctNo and AccExternalID 17-06-2013
                    if (!string.IsNullOrEmpty(oReq.fxAcctNo))
                    {
                        reg.fxAcctNo = oReq.fxAcctNo;
                    }
                    if (!string.IsNullOrEmpty(oReq.AccExternalID))
                    {
                        reg.AccExternalID = oReq.AccExternalID;
                    }
                    //end  added by Nreddy

                    //reg.LastAccessID = oReq.LastAccessID;
                    //reg.LastUpdateDT = DateTime.Now;

                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.RegistrationUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
         * 
         * 
         public static int RegistrationUpdateMISM(DAL.Models.RegistrationSec oReqSec)
        {

            Logger.InfoFormat("Entering {0}.RegistrationUpdateMISM({1})", svcName, (oReqSec != null) ? oReqSec.RegistrationID : 0);
            var value = 0;

            if (oReqSec != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var query = ctx.Registrations.Where(a => a.ID == oReqSec.RegistrationID);
                    List<int> ID = query.Select(w => w.ID).ToList();
                    //oReqSec.RegistrationID = ID[0];
                    int IDsec = ID[0];
                    var regsec = ctx.RegistrationSecs.Where(w => w.RegistrationID == IDsec).FirstOrDefault();
                    if (!string.IsNullOrEmpty(oReqSec.IMEINumber))
                    {
                        regsec.IMEINumber = oReqSec.IMEINumber;
                    }

                    if (!string.IsNullOrEmpty(oReqSec.SIMSerial))
                    {
                        regsec.SIMSerial = oReqSec.SIMSerial;
                    }

                    if (!string.IsNullOrEmpty(oReqSec.SalesPerson))
                    {
                        regsec.SalesPerson = oReqSec.SalesPerson;
                    }

                    if (oReqSec.SimModelId > 0)
                    {
                        regsec.SimModelId = oReqSec.SimModelId;
                    }

                    if (oReqSec.PenaltyAmount >= 0)
                    {
                        regsec.PenaltyAmount = oReqSec.PenaltyAmount;
                    }

                    //Added for inserting virtual number for MISM 
                    if (!string.IsNullOrEmpty(oReqSec.MSISDN1))
                        regsec.MSISDN1 = oReqSec.MSISDN1;

                    if (oReqSec.LastAccessID != null)
                        regsec.LastAccessID = oReqSec.LastAccessID;

                    regsec.LastUpdateDT = DateTime.Now;

                    // added by Nreddy for update fxAcctNo and AccExternalID 17-06-2013
                    if (!string.IsNullOrEmpty(oReqSec.fxAcctNo))
                    {
                        regsec.fxAcctNo = oReqSec.fxAcctNo;
                    }
                    if (!string.IsNullOrEmpty(oReqSec.AccExternalID))
                    {
                        regsec.AccExternalID = oReqSec.AccExternalID;
                    }
                    //end  added by Nreddy                       

                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.RegistrationUpdateMISM({1}): {2}", svcName, (oReqSec != null) ? oReqSec.RegistrationID : 0, value);
            return value;

        }
        */

        public static int RegistrationUpdate(DAL.Models.Registration oReq)
        {
            DateTime stDate = DateTime.Now;
            Logger.InfoFormat("Entering {0}.RegistrationUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_REGISTRATIONUPDATE", sqlConn);

                if (!string.IsNullOrEmpty(oReq.ID.ToString2()))
                    sqlCmd.Parameters.Add(new SqlParameter("@regid", oReq.ID));

                if (!string.IsNullOrEmpty(oReq.IMEINumber))
                    sqlCmd.Parameters.Add(new SqlParameter("@IMEINumber", oReq.IMEINumber));

                if (!string.IsNullOrEmpty(oReq.SIMSerial))
                    sqlCmd.Parameters.Add(new SqlParameter("@SIMSerial", oReq.SIMSerial));

                if (!string.IsNullOrEmpty(oReq.SalesPerson))
                    sqlCmd.Parameters.Add(new SqlParameter("@SalesPerson", oReq.SalesPerson));

                if (oReq.SimModelId > 0)
                    sqlCmd.Parameters.Add(new SqlParameter("@SimModelId", oReq.SimModelId));

                if (!string.IsNullOrEmpty(oReq.MSISDN1))
                    sqlCmd.Parameters.Add(new SqlParameter("@MSISDN1", oReq.MSISDN1));

                if (oReq.PenaltyAmount > 0)
                    sqlCmd.Parameters.Add(new SqlParameter("@PenaltyAmount", oReq.PenaltyAmount));

                if (!string.IsNullOrEmpty(oReq.LastAccessID))
                    sqlCmd.Parameters.Add(new SqlParameter("@LastAccessID", oReq.LastAccessID));

                if (!string.IsNullOrEmpty(oReq.LastUpdateDT.ToString2()))
                    sqlCmd.Parameters.Add(new SqlParameter("@LastUpdateDT", oReq.LastUpdateDT));

                if ((!string.IsNullOrEmpty(oReq.fxAcctNo)))
                    sqlCmd.Parameters.Add(new SqlParameter("@fxAcctNo", oReq.fxAcctNo));

                if (!string.IsNullOrEmpty(oReq.AccExternalID))
                    sqlCmd.Parameters.Add(new SqlParameter("@AccExternalID", oReq.AccExternalID));
                // Add new update to fix PN3987 - NullUOM // Szeto
                if (!string.IsNullOrEmpty(oReq.ArticleID))
                    sqlCmd.Parameters.Add(new SqlParameter("@ArticleID", oReq.ArticleID));
                if (!string.IsNullOrEmpty(oReq.UOMCode))
                    sqlCmd.Parameters.Add(new SqlParameter("@UOMCode", oReq.UOMCode));
                //End//
                // Add new update to for Icontract // Manoj
                if (!string.IsNullOrEmpty(oReq.CustomerPhoto))
                    sqlCmd.Parameters.Add(new SqlParameter("@CustomerPhoto", oReq.CustomerPhoto));
                if (!string.IsNullOrEmpty(oReq.AltCustomerPhoto))
                    sqlCmd.Parameters.Add(new SqlParameter("@AltCustomerPhoto", oReq.AltCustomerPhoto));
                if (!string.IsNullOrEmpty(oReq.Photo))
                    sqlCmd.Parameters.Add(new SqlParameter("@Photo", oReq.Photo));
                if (!string.IsNullOrEmpty(oReq.UploadedDocument))
                    sqlCmd.Parameters.Add(new SqlParameter("@Photo", oReq.UploadedDocument));
                //End//

                //GTM e-Billing CR - Ricky - 2014.10.10
                if (!string.IsNullOrEmpty(oReq.billDeliverySubmissionStatus))
                    sqlCmd.Parameters.Add(new SqlParameter("@billDeliverySubmissionStatus", oReq.billDeliverySubmissionStatus));
                if (!string.IsNullOrEmpty(oReq.PrePortReqId))
                    sqlCmd.Parameters.Add(new SqlParameter("@PrePortReqId", oReq.PrePortReqId));

                sqlCmd.CommandType = CommandType.StoredProcedure;
                value = sqlCmd.ExecuteNonQuery();
                sqlConn.Close();
            }
            Logger.InfoFormat("Entering {0}.RegistrationUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            TimeSpan timeSpn = (DateTime.Now - stDate);
            Common.LogMessage(string.Format("RegistrationUpdate({0}) TimeSpan:{1}", string.Join(",", oReq.ID), timeSpn.ToString("hh\\:mm\\:ss\\.fff")));
            return value;
        }

        public static int RegistrationUpdateMISM(DAL.Models.RegistrationSec oReqSec)
        {
            Logger.InfoFormat("Entering {0}.RegistrationUpdateMISM({1})", svcName, (oReqSec != null) ? oReqSec.RegistrationID : 0);
            var value = 0;
            if (oReqSec != null)
            {
                #region "commented Code"
                //using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                //{
                //    var query = ctx.Registrations.Where(a => a.ID == oReqSec.RegistrationID);
                //    List<int> ID = query.Select(w => w.ID).ToList();
                //    //oReqSec.RegistrationID = ID[0];
                //    int IDsec = ID[0];
                //    var regsec = ctx.RegistrationSecs.Where(w => w.RegistrationID == IDsec).FirstOrDefault();
                //    if (!string.IsNullOrEmpty(oReqSec.IMEINumber))
                //    {
                //        regsec.IMEINumber = oReqSec.IMEINumber;
                //    }

                //    if (!string.IsNullOrEmpty(oReqSec.SIMSerial))
                //    {
                //        regsec.SIMSerial = oReqSec.SIMSerial;
                //    }

                //    if (!string.IsNullOrEmpty(oReqSec.SalesPerson))
                //    {
                //        regsec.SalesPerson = oReqSec.SalesPerson;
                //    }

                //    if (oReqSec.SimModelId > 0)
                //    {
                //        regsec.SimModelId = oReqSec.SimModelId;
                //    }

                //    if (oReqSec.PenaltyAmount >= 0)
                //    {
                //        regsec.PenaltyAmount = oReqSec.PenaltyAmount;
                //    }

                //    //Added for inserting virtual number for MISM 
                //    if (!string.IsNullOrEmpty(oReqSec.MSISDN1))
                //        regsec.MSISDN1 = oReqSec.MSISDN1;

                //    if (oReqSec.LastAccessID != null)
                //        regsec.LastAccessID = oReqSec.LastAccessID;

                //    regsec.LastUpdateDT = DateTime.Now;

                //    // added by Nreddy for update fxAcctNo and AccExternalID 17-06-2013
                //    if (!string.IsNullOrEmpty(oReqSec.fxAcctNo))
                //    {
                //        regsec.fxAcctNo = oReqSec.fxAcctNo;
                //    }
                //    if (!string.IsNullOrEmpty(oReqSec.AccExternalID))
                //    {
                //        regsec.AccExternalID = oReqSec.AccExternalID;
                //    }
                //    //end  added by Nreddy                       

                //    value = ctx.SaveChanges();
                //}
                #endregion

                using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
                {
                    sqlConn.Open();
                    SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_REGISTRATIONUPDATE_MISM", sqlConn);

                    if (!string.IsNullOrEmpty(oReqSec.ID.ToString2()))
                        sqlCmd.Parameters.Add(new SqlParameter("@regid", oReqSec.RegistrationID));

                    if (!string.IsNullOrEmpty(oReqSec.IMEINumber))
                        sqlCmd.Parameters.Add(new SqlParameter("@IMEINumber", oReqSec.IMEINumber));

                    if (!string.IsNullOrEmpty(oReqSec.SIMSerial))
                        sqlCmd.Parameters.Add(new SqlParameter("@SIMSerial", oReqSec.SIMSerial));

                    if (!string.IsNullOrEmpty(oReqSec.SalesPerson))
                        sqlCmd.Parameters.Add(new SqlParameter("@SalesPerson", oReqSec.SalesPerson));

                    if (oReqSec.SimModelId > 0)
                        sqlCmd.Parameters.Add(new SqlParameter("@SimModelId", oReqSec.SimModelId));

                    if (!string.IsNullOrEmpty(oReqSec.MSISDN1))
                        sqlCmd.Parameters.Add(new SqlParameter("@MSISDN1", oReqSec.MSISDN1));

                    if (oReqSec.PenaltyAmount > 0)
                        sqlCmd.Parameters.Add(new SqlParameter("@PenaltyAmount", oReqSec.PenaltyAmount));

                    if (!string.IsNullOrEmpty(oReqSec.LastAccessID))
                        sqlCmd.Parameters.Add(new SqlParameter("@LastAccessID", oReqSec.LastAccessID));

                    if (!string.IsNullOrEmpty(oReqSec.LastUpdateDT.ToString2()))
                        sqlCmd.Parameters.Add(new SqlParameter("@LastUpdateDT", oReqSec.LastUpdateDT));

                    if ((!string.IsNullOrEmpty(oReqSec.fxAcctNo)))
                        sqlCmd.Parameters.Add(new SqlParameter("@fxAcctNo", oReqSec.fxAcctNo));

                    if (!string.IsNullOrEmpty(oReqSec.AccExternalID))
                        sqlCmd.Parameters.Add(new SqlParameter("@AccExternalID", oReqSec.AccExternalID));

                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    value = sqlCmd.ExecuteNonQuery();
                    sqlConn.Close();
                }
            }

            Logger.InfoFormat("Exiting {0}.RegistrationUpdateMISM({1}): {2}", svcName, (oReqSec != null) ? oReqSec.RegistrationID : 0, value);
            return value;

        }

        public static List<int> RegistrationFind(RegistrationFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = new List<int>();
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegs = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GETREGISTRATIONS_ID", sqlConn);
                //sqlCmd.Parameters.Add(new SqlParameter("@IDs", string.Join(",", IDs)));
                if (!(oReq.Registration.ID == 0))
                    sqlCmd.Parameters.Add(new SqlParameter("@ID", oReq.Registration.ID));
                if (!(oReq.Registration.RegTypeID == 0))
                    sqlCmd.Parameters.Add(new SqlParameter("@RegTypeID", oReq.Registration.RegTypeID));
                if (!(oReq.Registration.CustomerID == 0))
                    sqlCmd.Parameters.Add(new SqlParameter("@CustomerID", oReq.Registration.CustomerID));
                if (!string.IsNullOrEmpty(oReq.Registration.Remark))
                    sqlCmd.Parameters.Add(new SqlParameter("@Remark", oReq.Registration.Remark));
                if (!string.IsNullOrEmpty(oReq.Registration.MSISDN1))
                    sqlCmd.Parameters.Add(new SqlParameter("@MSISDN1", oReq.Registration.MSISDN1));
                if (!string.IsNullOrEmpty(oReq.Registration.MSISDN2))
                    sqlCmd.Parameters.Add(new SqlParameter("@MSISDN2", oReq.Registration.MSISDN2));
                if (!string.IsNullOrEmpty(oReq.Registration.SalesPerson))
                    sqlCmd.Parameters.Add(new SqlParameter("@SalesPerson", oReq.Registration.SalesPerson));
                if (oReq.RegDTFrom.HasValue)
                {
                    var regDTFrom = oReq.RegDTFrom.Value.Date;
                    sqlCmd.Parameters.Add(new SqlParameter("@regDTFrom", oReq.Registration.CreateDT));
                }
                if (oReq.RegDTTo.HasValue)
                {
                    var regDTTo = oReq.RegDTTo.Value.Date;
                    sqlCmd.Parameters.Add(new SqlParameter("@regDTTo", oReq.Registration.CreateDT));
                }
                if (oReq.InstallationDTFrom.HasValue)
                {
                    var installationDTFrom = oReq.InstallationDTFrom.Value.Date;
                    sqlCmd.Parameters.Add(new SqlParameter("@InstallationDTFrom", oReq.Registration.InstallationDT));
                }
                if (oReq.InstallationDTTo.HasValue)
                {
                    var installationDTTo = oReq.InstallationDTTo.Value.Date;
                    sqlCmd.Parameters.Add(new SqlParameter("@InstallationDTTo", oReq.Registration.InstallationDT));
                }
                sqlCmd.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegs);
                sqlConn.Close();
                if ((!ReferenceEquals(dsRegs, null)) && (!ReferenceEquals(dsRegs.Tables[0], null)))
                {
                    foreach (DataRow row in dsRegs.Tables[0].Rows)
                    {
                        foreach (DataColumn column in dsRegs.Tables[0].Columns)
                        {
                            value.Add(Convert.ToInt32(row[column]));
                        }
                    }
                }
            }
            Logger.InfoFormat("Exiting {0}.RegistrationFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }


        public static List<DAL.Models.Registration> RegistrationGet(List<int> IDs)
        {
            DateTime stDate = DateTime.Now;
            Logger.InfoFormat("Entering {0}.RegistrationGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<DAL.Models.Registration> value = null;
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GETREGISTRATIONSBYIDS", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@IDs", string.Join(",", IDs)));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandTimeout = 180;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();
                //sqlCmd.CommandTimeout = 90000;
                value = FillDataSetToObject(dsRegistrations);
            }
            Logger.InfoFormat("Exiting {0}.RegistrationGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            TimeSpan timeSpn = (DateTime.Now - stDate);
            Common.LogMessage(string.Format("RegistrationGet({0}) TimeSpan:{1}", string.Join(",", IDs), timeSpn.ToString("hh\\:mm\\:ss\\.fff")));
            return value;
        }

        public static Dictionary<string,string> CustomerInfo(int regID)
        {
            Dictionary<string, string> value = new Dictionary<string,string>();
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GetWelcomeCustomerByRegId", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@ID", regID));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandTimeout = 180;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();

                value.Add("KenanAccountNo", dsRegistrations.Tables[0].Rows[0]["KenanAccountNo"].ToString2());
                value.Add("MSISDN", dsRegistrations.Tables[0].Rows[0]["MSISDN"].ToString2());
                value.Add("SIMSerial", dsRegistrations.Tables[0].Rows[0]["SIMSerial"].ToString2());
                value.Add("Salutation", dsRegistrations.Tables[0].Rows[0]["Salutation"].ToString2());
                value.Add("FullName", dsRegistrations.Tables[0].Rows[0]["FullName"].ToString2());
                value.Add("Line1", dsRegistrations.Tables[0].Rows[0]["Line1"].ToString2());
                value.Add("Line2", dsRegistrations.Tables[0].Rows[0]["Line2"].ToString2());
                value.Add("Line3", dsRegistrations.Tables[0].Rows[0]["Line3"].ToString2());
                value.Add("Postcode", dsRegistrations.Tables[0].Rows[0]["Poscode"].ToString2());
                value.Add("City", dsRegistrations.Tables[0].Rows[0]["City"].ToString2());
                value.Add("State", dsRegistrations.Tables[0].Rows[0]["State"].ToString2());
                value.Add("EmailAddress", dsRegistrations.Tables[0].Rows[0]["EmailAddress"].ToString2());
                value.Add("ArticleId", dsRegistrations.Tables[0].Rows[0]["ArticleId"].ToString2());
            }

            return value;
        }

        public static Dictionary<string, string> SelectedPackage(int regID)
        {
            Dictionary<string, string> value = new Dictionary<string, string>();
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GetPgmBdlPkgCompByRegId", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@regID", regID));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandTimeout = 180;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();

                value.Add("Name", dsRegistrations.Tables[0].Rows[0]["Name"].ToString2());
                value.Add("Price", dsRegistrations.Tables[1].Rows[0]["Price"].ToString2());
                value.Add("Description", dsRegistrations.Tables[0].Rows[0]["Description"].ToString2());
            }

            return value;
        }

        public static Dictionary<string, string> SelectedPackageBySuppline(int suppID)
        {
            Dictionary<string, string> value = new Dictionary<string, string>();
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GetPgmBdlPkgCompBySuppId", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@suppID", suppID));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandTimeout = 180;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();

                value.Add("Name", dsRegistrations.Tables[0].Rows[0]["Name"].ToString2());
                value.Add("Price", dsRegistrations.Tables[1].Rows[0]["Price"].ToString2());
                value.Add("Description", dsRegistrations.Tables[0].Rows[0]["Description"].ToString2());
            }

            return value;
        }

        public static List<Dictionary<string, string>> GetSuppline(int regID)
        {
            List<Dictionary<string, string>> value = new List<Dictionary<string, string>>();
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GetWelcomeCustomerSupplineByRegId", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@regID", regID));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandTimeout = 180;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();

                for (int i = 0; i < dsRegistrations.Tables[0].Rows.Count; i++)
                {
                    Dictionary<string, string> tempValue = new Dictionary<string, string>();
                    tempValue.Add("ID", dsRegistrations.Tables[0].Rows[i]["ID"].ToString2());
                    tempValue.Add("PgmBdlPckComponentID", dsRegistrations.Tables[0].Rows[i]["PgmBdlPckComponentID"].ToString2());
                    tempValue.Add("MSISDN1", dsRegistrations.Tables[0].Rows[i]["MSISDN1"].ToString2());
                    tempValue.Add("SIMSerial", dsRegistrations.Tables[0].Rows[i]["SIMSerial"].ToString2());
                    tempValue.Add("ArticleID", dsRegistrations.Tables[0].Rows[i]["ArticleID"].ToString2());

                    value.Add(tempValue);
                }
            }

            return value;
        }

        public static Dictionary<string, string> SelectedArticle(int articleId)
        {
            Dictionary<string, string> value = new Dictionary<string, string>();
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GetUOMByArticleId", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@ID", articleId));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandTimeout = 180;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();

                if (dsRegistrations.Tables[0].Rows.Count > 0)
                {
                    value.Add("ContractDuration", dsRegistrations.Tables[0].Rows[0]["ContractDuration"].ToString2());
                }
            }

            return value;
        }


        private static List<DAL.Models.Registration> FillDataSetToObject(DataSet resultDataSet)
        {
            List<DAL.Models.Registration> lstobjData = new List<Models.Registration>();
            DAL.Models.Registration objData = null;
            if (!ReferenceEquals(resultDataSet, null))
            {
                if (!ReferenceEquals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                    {
                        objData = new Models.Registration();
                        objData.ID = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["ID"], DBNull.Value) ? Convert.ToInt32(resultDataSet.Tables[0].Rows[i]["ID"]) : 0;
                        objData.RegTypeID = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["RegTypeID"], DBNull.Value) ? Convert.ToInt32(resultDataSet.Tables[0].Rows[i]["RegTypeID"]) : 0;
                        objData.CustomerID = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["CustomerID"], DBNull.Value) ? Convert.ToInt32(resultDataSet.Tables[0].Rows[i]["CustomerID"]) : 0;
                        objData.CenterOrgID = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["CenterOrgID"], DBNull.Value) ? Convert.ToInt32(resultDataSet.Tables[0].Rows[i]["CenterOrgID"]) : 0;
                        objData.Remark = resultDataSet.Tables[0].Rows[i]["Remark"].ToString2();
                        objData.QueueNo = resultDataSet.Tables[0].Rows[i]["QueueNo"].ToString2();
                        objData.MSISDN1 = resultDataSet.Tables[0].Rows[i]["MSISDN1"].ToString2();
                        objData.MSISDN2 = resultDataSet.Tables[0].Rows[i]["MSISDN2"].ToString2();
                        objData.AccountID = resultDataSet.Tables[0].Rows[i]["AccountID"].ToString2();
                        objData.AdditionalCharges = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["AdditionalCharges"], DBNull.Value) ? Convert.ToDecimal(resultDataSet.Tables[0].Rows[i]["AdditionalCharges"]) : 0;
                        objData.ModemID = resultDataSet.Tables[0].Rows[i]["ModemID"].ToString2();
                        objData.KenanAccountNo = resultDataSet.Tables[0].Rows[i]["KenanAccountNo"].ToString2();
                        objData.SalesPerson = resultDataSet.Tables[0].Rows[i]["SalesPerson"].ToString2();
                        objData.IsVerified = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["IsVerified"], DBNull.Value) ? Convert.ToBoolean(resultDataSet.Tables[0].Rows[i]["IsVerified"]) : false;
                        objData.BiometricVerify = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["BiometricVerify"], DBNull.Value) ? Convert.ToBoolean(resultDataSet.Tables[0].Rows[i]["BiometricVerify"]) : false;
                        objData.InternalBlacklisted = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["InternalBlacklisted"], DBNull.Value) ? Convert.ToBoolean(resultDataSet.Tables[0].Rows[i]["InternalBlacklisted"]) : false;
                        objData.ExternalBlacklisted = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["ExternalBlacklisted"], DBNull.Value) ? Convert.ToBoolean(resultDataSet.Tables[0].Rows[i]["ExternalBlacklisted"]) : false;
                        objData.ApprovedBlacklistPerson = resultDataSet.Tables[0].Rows[i]["ApprovedBlacklistPerson"].ToString2();
                        objData.RFSalesDT = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["RFSalesDT"], DBNull.Value) ? Convert.ToDateTime(resultDataSet.Tables[0].Rows[i]["RFSalesDT"]) : DateTime.Now;
                        objData.WelcomeEmailSent = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["WelcomeEmailSent"], DBNull.Value) ? Convert.ToBoolean(resultDataSet.Tables[0].Rows[i]["WelcomeEmailSent"]) : false;
                        objData.InstallationDT = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["InstallationDT"], DBNull.Value) ? Convert.ToDateTime(resultDataSet.Tables[0].Rows[i]["InstallationDT"]) : DateTime.Now;
                        objData.SkipDuplicate = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["SkipDuplicate"], DBNull.Value) ? Convert.ToBoolean(resultDataSet.Tables[0].Rows[i]["SkipDuplicate"]) : false;
                        objData.SignatureSVG = resultDataSet.Tables[0].Rows[i]["SignatureSVG"].ToString2();
                        objData.CustomerPhoto = resultDataSet.Tables[0].Rows[i]["CustomerPhoto"].ToString2();
                        objData.AltCustomerPhoto = resultDataSet.Tables[0].Rows[i]["AltCustomerPhoto"].ToString2();
                        objData.Photo = resultDataSet.Tables[0].Rows[i]["Photo"].ToString2();
                        objData.SIMSerial = resultDataSet.Tables[0].Rows[i]["SIMSerial"].ToString2();
                        objData.IMEINumber = resultDataSet.Tables[0].Rows[i]["IMEINumber"].ToString2();
                        objData.CRPType = resultDataSet.Tables[0].Rows[i]["CRPType"].ToString2();
                        objData.AgeCheckStatus = resultDataSet.Tables[0].Rows[i]["AgeCheckStatus"].ToString2();
                        objData.DDMFCheckStatus = resultDataSet.Tables[0].Rows[i]["DDMFCheckStatus"].ToString2();
                        objData.AddressCheckStatus = resultDataSet.Tables[0].Rows[i]["AddressCheckStatus"].ToString2();
                        objData.OutStandingCheckStatus = resultDataSet.Tables[0].Rows[i]["OutStandingCheckStatus"].ToString2();
                        objData.TotalLineCheckStatus = resultDataSet.Tables[0].Rows[i]["TotalLineCheckStatus"].ToString2();
                        objData.PrincipleLineCheckStatus = resultDataSet.Tables[0].Rows[i]["PrincipleLineCheckStatus"].ToString2();
                        objData.Whitelist_Status = resultDataSet.Tables[0].Rows[i]["Whitelist_Status"].ToString2();
                        objData.MOC_Status = resultDataSet.Tables[0].Rows[i]["MOC_Status"].ToString2();
                        objData.ContractCheckStatus = resultDataSet.Tables[0].Rows[i]["ContractCheckStatus"].ToString2();
                        objData.UserType = resultDataSet.Tables[0].Rows[i]["UserType"].ToString2();
                        objData.PlanAdvance = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["PlanAdvance"], DBNull.Value) ? Convert.ToDecimal(resultDataSet.Tables[0].Rows[i]["PlanAdvance"]) : 0;
                        objData.PlanDeposit = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["PlanDeposit"], DBNull.Value) ? Convert.ToDecimal(resultDataSet.Tables[0].Rows[i]["PlanDeposit"]) : 0;
                        objData.DeviceAdvance = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["DeviceAdvance"], DBNull.Value) ? Convert.ToDecimal(resultDataSet.Tables[0].Rows[i]["DeviceAdvance"]) : 0;
                        objData.DeviceDeposit = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["DeviceDeposit"], DBNull.Value) ? Convert.ToDecimal(resultDataSet.Tables[0].Rows[i]["DeviceDeposit"]) : 0;
                        objData.Discount = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["Discount"], DBNull.Value) ? Convert.ToDecimal(resultDataSet.Tables[0].Rows[i]["Discount"]) : 0;
                        objData.OfferID = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["OfferID"], DBNull.Value) ? Convert.ToDecimal(resultDataSet.Tables[0].Rows[i]["OfferID"]) : 0;
                        objData.OutstandingAcs = resultDataSet.Tables[0].Rows[i]["OutstandingAcs"].ToString2();
                        objData.ArticleID = resultDataSet.Tables[0].Rows[i]["ArticleID"].ToString2();
                        objData.MNPServiceIdCheckStatus = resultDataSet.Tables[0].Rows[i]["MNPServiceIdCheckStatus"].ToString2();
                        objData.PrePortInCheckStatus = resultDataSet.Tables[0].Rows[i]["PrePortInCheckStatus"].ToString2();
                        objData.UOMCode = resultDataSet.Tables[0].Rows[i]["UOMCode"].ToString2();
                        objData.K2_Status = resultDataSet.Tables[0].Rows[i]["K2_Status"].ToString2();
                        objData.PrePortReqId = resultDataSet.Tables[0].Rows[i]["PrePortReqId"].ToString2();
                        objData.DonorId = resultDataSet.Tables[0].Rows[i]["DonorId"].ToString2();
                        objData.fxAcctNo = resultDataSet.Tables[0].Rows[i]["fxAcctNo"].ToString2();
                        objData.externalId = resultDataSet.Tables[0].Rows[i]["externalId"].ToString2();
                        objData.AccExternalID = resultDataSet.Tables[0].Rows[i]["AccExternalID"].ToString2();
                        objData.Remarks = resultDataSet.Tables[0].Rows[i]["Remarks"].ToString2();
                        objData.IMPOSFileName = resultDataSet.Tables[0].Rows[i]["IMPOSFileName"].ToString2();
                        objData.IMPOSStatus = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["IMPOSStatus"], DBNull.Value) ? Convert.ToInt32(resultDataSet.Tables[0].Rows[i]["IMPOSStatus"]) : 0;
                        objData.OrganisationId = resultDataSet.Tables[0].Rows[i]["OrganisationId"].ToString2();
                        objData.SimModelId = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["SimModelId"], DBNull.Value) ? Convert.ToInt32(resultDataSet.Tables[0].Rows[i]["SimModelId"]) : 0;
                        objData.ServiceOrderId = resultDataSet.Tables[0].Rows[i]["ServiceOrderId"].ToString2();
                        objData.Trn_Type = resultDataSet.Tables[0].Rows[i]["Trn_Type"].ToString2();
                        objData.InternationalRoaming = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["InternationalRoaming"], DBNull.Value) ? Convert.ToBoolean(resultDataSet.Tables[0].Rows[i]["InternationalRoaming"]) : false;
                        objData.K2Type = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["K2Type"], DBNull.Value) ? resultDataSet.Tables[0].Rows[i]["K2Type"].ToBool() : false;
                        objData.TMOrderID = resultDataSet.Tables[0].Rows[i]["TMOrderID"].ToString2();
                        objData.fxSubscrNo = resultDataSet.Tables[0].Rows[i]["fxSubscrNo"].ToString2();
                        objData.fxSubScrNoResets = resultDataSet.Tables[0].Rows[i]["fxSubScrNoResets"].ToString2();
                        objData.SM_ContractType = resultDataSet.Tables[0].Rows[i]["SM_ContractType"].ToString2();
                        objData.IsSImRequired_Smart = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["IsSImRequired_Smart"], DBNull.Value) ? resultDataSet.Tables[0].Rows[i]["IsSImRequired_Smart"].ToBool() : false;
                        objData.PenaltyAmount = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["PenaltyAmount"], DBNull.Value) ? resultDataSet.Tables[0].Rows[i]["PenaltyAmount"].ToDecimal() : 0;
                        objData.IsK2 = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["IsK2"], DBNull.Value) ? resultDataSet.Tables[0].Rows[i]["IsK2"].ToBool() : false;
                        objData.CreateDT = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["CreateDT"], DBNull.Value) ? Convert.ToDateTime(resultDataSet.Tables[0].Rows[i]["CreateDT"]) : DateTime.Now;
                        objData.LastUpdateDT = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["LastUpdateDT"], DBNull.Value) ? Convert.ToDateTime(resultDataSet.Tables[0].Rows[i]["LastUpdateDT"]) : DateTime.Now;
                        objData.LastAccessID = resultDataSet.Tables[0].Rows[i]["LastAccessID"].ToString2();
                        objData.upfrontPayment = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["upfrontPayment"], DBNull.Value) ? Convert.ToDecimal(resultDataSet.Tables[0].Rows[i]["upfrontPayment"]) : 0;
                        objData.PenaltyWaivedAmt = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["PenaltyWaivedAmt"], DBNull.Value) ? Convert.ToDecimal(resultDataSet.Tables[0].Rows[i]["PenaltyWaivedAmt"]) : 0;

                        //GTM e-Billing CR - Ricky - 2014.09.25
						try
						{
							objData.DropVersion = resultDataSet.Tables[0].Rows[i]["DropVersion"].ToString2();
							objData.billDeliveryViaEmail = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["billDeliveryViaEmail"], DBNull.Value) ? resultDataSet.Tables[0].Rows[i]["billDeliveryViaEmail"].ToBool() : false;
							objData.billDeliveryEmailAddress = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["billDeliveryEmailAddress"], DBNull.Value) ? resultDataSet.Tables[0].Rows[i]["billDeliveryEmailAddress"].ToString2() : "";
							objData.billDeliverySmsNotif = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["billDeliverySmsNotif"], DBNull.Value) ? resultDataSet.Tables[0].Rows[i]["billDeliverySmsNotif"].ToBool() : false;
							objData.billDeliverySmsNo = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["billDeliverySmsNo"], DBNull.Value) ? resultDataSet.Tables[0].Rows[i]["billDeliverySmsNo"].ToString2() : "";
							objData.billDeliverySubmissionStatus = !ReferenceEquals(resultDataSet.Tables[0].Rows[i]["billDeliverySubmissionStatus"], DBNull.Value) ? resultDataSet.Tables[0].Rows[i]["billDeliverySubmissionStatus"].ToString2() : "";
						}
						catch (Exception ex)
						{
							;
						}
						lstobjData.Add(objData);
                    }
                }
            }
            return lstobjData;
        }

        /// <summary>
        /// Registrations the get MNP supplementary lines.
        /// </summary>
        /// <param name="RegID">The reg ID.</param>
        /// <returns>TypeOf(RegSuppLine)</returns>
        public static List<DAL.Models.RegSuppLine> RegistrationGetMnpSupple(Int32 RegID)
        {
            Logger.InfoFormat("Entering {0}.RegistrationGetMnpWithSupple({1})", svcName, (RegID != null) ? RegID : 0);

            ///SUPPLEMENTARY LINE DETAILS
            List<DAL.Models.RegSuppLine> RegSuppLines = null;
            Int32 RegTypeID = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                RegTypeID = ctx.RegTypes.Where(a => a.Code == "MMM").ToList().FirstOrDefault().ID;
                RegSuppLines = ctx.RegSuppLines.Where(a => a.RegID == RegID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegistrationGetMnpSupple({1}): {2}", svcName, (RegID != null) ? RegID : 0, RegSuppLines.Count());
            return RegSuppLines;
        }

        public static List<DAL.Models.RegistrationSec> RegistrationSecGet(List<int> IDs)
        {
            List<DAL.Models.RegistrationSec> value = null;
            try
            {
                Logger.InfoFormat("Entering {0}.RegistrationSecGet({1})", svcName, (IDs != null) ? IDs[0] : 0);


                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    value = ctx.RegistrationSecs.Where(a => IDs.Contains(a.RegistrationID)).ToList();
                }

            }
            catch (Exception e)
            {
                value = null;
                Logger.ErrorFormat("Exiting {0}.RegistrationSecGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, e.Message);
            }

            Logger.InfoFormat("Exiting {0}.RegistrationSecGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<DAL.Models.RegistrationSec> RegistrationSecGetBySecID(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.RegistrationSecGetBySecID({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<DAL.Models.RegistrationSec> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegistrationSecs.Where(a => IDs.Contains(a.ID)).OrderByDescending(w => w.CreateDT).Take(1).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegistrationSecGetBySecID({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static void RegistrationClose(RegStatus regStatus)
        {
            Logger.InfoFormat("Entering {0}.RegistrationClose({1})", svcName, regStatus.RegID.ToString2());

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    var oldRegStatus = ctx.RegStatuses.Where(a => a.RegID == regStatus.RegID && a.Active == true).FirstOrDefault();
                    oldRegStatus.Active = false;
                    oldRegStatus.EndDate = DateTime.Now;
                    ctx.SaveChanges();

                    ctx.RegStatuses.Add(regStatus);
                    try
                    {
                        ctx.SaveChanges();
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {

                            }
                        }
                    }


                    var reg = ctx.Registrations.SingleOrDefault(a => a.ID == regStatus.RegID);
                    reg.LastUpdateDT = DateTime.Now;
                    reg.LastAccessID = regStatus.LastAccessID;
                    ctx.SaveChanges();

                    trans.Complete();
                }
            }
            Logger.InfoFormat("Exiting {0}.RegistrationClose({1})", svcName, regStatus.RegID.ToString2());
        }

        public static void RegistrationCancel(RegStatus regStatus)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCancel({1})", svcName, regStatus.RegID.ToString2());

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            //using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            //{
            //    using (var trans = new TransactionScope())
            //    {
            //        if(ctx.RegStatuses.Any(a => a.RegID == regStatus.RegID && a.Active == true))
            //        {
            //        var oldRegStatus = ctx.RegStatuses.Where(a => a.RegID == regStatus.RegID && a.Active == true).FirstOrDefault();
            //        oldRegStatus.Active = false;
            //        oldRegStatus.EndDate = DateTime.Now;
            //        ctx.SaveChanges();
            //        }
            //        ctx.RegStatuses.Add(regStatus);
            //        ctx.SaveChanges();


            //        var oldReg = ctx.Registrations.Where(a => a.ID == regStatus.RegID).SingleOrDefault();
            //        if (oldReg != null)
            //        {
            //            oldReg.LastAccessID = regStatus.LastAccessID;
            //            oldReg.LastUpdateDT = DateTime.Now;
            //            ctx.SaveChanges();
            //        }
            //        trans.Complete();

            //    }
            //}
            ////Dual Satus Fix moving to Store Proc. 
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_UpdateStatus", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@RegID", regStatus.RegID));
                sqlCmd.Parameters.Add(new SqlParameter("@Active", regStatus.Active));
                sqlCmd.Parameters.Add(new SqlParameter("@CreateDT", regStatus.CreateDT));
                sqlCmd.Parameters.Add(new SqlParameter("@StartDate", regStatus.StartDate));
                sqlCmd.Parameters.Add(new SqlParameter("@LastAccessID", regStatus.LastAccessID));
                sqlCmd.Parameters.Add(new SqlParameter("@StatusID", regStatus.StatusID));
                sqlCmd.Parameters.Add(new SqlParameter("@LastUpdateDT", DateTime.Now));

                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandTimeout = 180;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();
                //sqlCmd.CommandTimeout = 90000;

            }
        }

        public static void RegistrationPaymentFailed(RegStatus regStatus)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCancel({1})", svcName, regStatus.RegID.ToString2());

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    var oldRegStatus = ctx.RegStatuses.Where(a => a.RegID == regStatus.RegID && a.Active == true).SingleOrDefault();
                    oldRegStatus.Active = false;
                    oldRegStatus.EndDate = DateTime.Now;
                    ctx.SaveChanges();

                    ctx.RegStatuses.Add(regStatus);
                    ctx.SaveChanges();

                    trans.Complete();
                }
            }
            Logger.InfoFormat("Exiting {0}.RegistrationCancel({1})", svcName, regStatus.RegID.ToString2());
        }

        public static void RegistrationPaymentSuccess(RegStatus regStatus)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCancel({1})", svcName, regStatus.RegID.ToString2());

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    var oldRegStatus = ctx.RegStatuses.Where(a => a.RegID == regStatus.RegID && a.Active == true).SingleOrDefault();
                    oldRegStatus.Active = false;
                    oldRegStatus.EndDate = DateTime.Now;
                    ctx.SaveChanges();

                    ctx.RegStatuses.Add(regStatus);
                    ctx.SaveChanges();

                    trans.Complete();
                }
            }
            Logger.InfoFormat("Exiting {0}.RegistrationCancel({1})", svcName, regStatus.RegID.ToString2());
        }


        public static lnkPlanDevice GetLnkPlanDeviceInfo(int Regid)
        {
            var resp = new lnkPlanDevice();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var strRegid = Convert.ToString(Regid);
                resp = ctx.lnkPlanDevice.FirstOrDefault(lpd => lpd.RegId == strRegid);
            }
            return resp;
        }
        #region Added by Patanjali on 30-03-2013 for StoreKeeper and Cashier
        #region "refined to SP so commented"
        /*
        public static List<RegistrationSearchResult> StoreKeeperSearch(RegistrationSearchCriteria criteria)
        {
            Logger.InfoFormat("Entering {0}.RegistrationSearch({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            List<RegistrationSearchResult> result = new List<RegistrationSearchResult>();
            int defaultTopNo = criteria.PageSize > 0 ? criteria.PageSize : 0;

            var regTypeCodes = new List<string>();
            if (criteria.IsMobilePlan)
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_DevicePlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_PlanOnly);
                //added by Deepika
                regTypeCodes.Add(Properties.Settings.Default.RegType_SupplementaryPlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_SecondaryPlan);

                regTypeCodes.Add(Properties.Settings.Default.RegType_NewLine);
                //Added by Patanjali for MNP
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPPlanOnly);
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPSupplementaryPlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPNewLine);
                //Added by Patanjali for MNP Ends here

                regTypeCodes.Add(Properties.Settings.Default.RegType_AddRemoveVAS);
                regTypeCodes.Add(Properties.Settings.Default.Sim_Replacement_RegType);//Added By Himansu
                regTypeCodes.Add(Properties.Settings.Default.RegType_CRP); // Added by Kotesh
                regTypeCodes.Add(Properties.Settings.Default.RegType_DeviceCRP); // Added by KiranG
                ///ADDED BY SUTAN DAN TO GET MULTIPLE SUPPLEMENTARY LINES FOR A PRINCIPLE LINE MNP ONLY
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPPlanWithMultiSuppline);
            }
            else
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomePersonal);
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomeCorporate);
            }

            using (var context = new OnlineStoreDbContext())
            {
                var userOrgID = context.Users.Single(a => a.ID == criteria.UserID).OrgID;
                var userOrgTypeID = context.Organizations.Single(a => a.ID == userOrgID).OrgTypeID;
                var userOrgTypeCode = context.OrgTypes.Single(a => a.ID == userOrgTypeID).Code;
                criteria.CenterOrgID = userOrgID;
                var orgTypeViewCertain = Properties.Settings.Default.ViewCertainRoles.Split(',').ToList();

                if (criteria != null)
                {
                    var query = from registration in context.Registrations


                                join cust in context.Customers on
                                registration.CustomerID equals cust.ID //into custs
                                //from cust in custs.DefaultIfEmpty()  


                                join regStatus in context.RegStatuses on
                                new { regID = registration.ID, active = true } equals new { regID = regStatus.RegID, active = regStatus.Active } //into regStatuses
                                //from regStatus in regStatuses.DefaultIfEmpty()

                                join status in context.Statuses on
                                regStatus.StatusID equals status.ID //into statuses
                                //from status in statuses.DefaultIfEmpty()

                                join organization in context.Organizations on
                                registration.CenterOrgID equals organization.ID

                                join regType in
                                    (from rt in context.RegTypes
                                     where regTypeCodes.Contains(rt.Code)
                                     select new { ID = rt.ID }) on registration.RegTypeID equals regType.ID


                                join regTypes in context.RegTypes on
                                registration.RegTypeID equals regTypes.ID


                                where ((regStatus.StatusID == 1 || regStatus.StatusID == 28 || regStatus.StatusID == 22 || regStatus.StatusID == 91 || (regStatus.StatusID == 93))
                                && (registration.Trn_Type == "R" || registration.Trn_Type == "S"))
                                // commented by ranjeeth : && (registration.Trn_Type == "R") -- need to retrieve records based on selected criteria



                                select new
                                {

                                    Reg = registration,
                                    Cust = cust,
                                    Status = status,
                                    RegStatus = regStatus,
                                    Organization = organization,
                                    RegTypes = regTypes

                                };



                    if (orgTypeViewCertain.Contains(userOrgTypeCode))
                    {
                        query = query.Where(a => a.Reg.CenterOrgID == userOrgID);
                    }

                    if (!string.IsNullOrEmpty(criteria.RegID))
                    {
                        var regID = criteria.RegID.ToInt();
                        query = query.Where(a => a.Reg.ID == regID);
                    }

                    if (!string.IsNullOrEmpty(criteria.IDCardNo))
                        query = query.Where(a => a.Cust.IDCardNo == criteria.IDCardNo);

                    if (!string.IsNullOrEmpty(criteria.QueueNo))
                        query = query.Where(a => a.Reg.QueueNo == criteria.QueueNo);

                    if (criteria.StatusID != 0)
                        query = query.Where(a => a.Status.ID == criteria.StatusID);

                    if (criteria.CenterOrgID.HasValue)
                        query = query.Where(a => a.Reg.CenterOrgID == criteria.CenterOrgID);

                    if (!string.IsNullOrEmpty(criteria.MSISDN))
                        query = query.Where(a => a.Cust.ContactNo == criteria.MSISDN);

                    if (!string.IsNullOrEmpty(criteria.CustName))
                        query = query.Where(a => a.Cust.FullName.Contains(criteria.CustName));

                    if (criteria.RegDateFrom.HasValue)
                    {
                        var regDTFrom = criteria.RegDateFrom.Value.Date;
                        //query = query.Where(a => DateTime.Compare(a.CreateDT, regDTFrom) > 0);
                        query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) >= EntityFunctions.TruncateTime(regDTFrom));
                    }

                    if (criteria.RegDateTo.HasValue)
                    {
                        var regDTTo = criteria.RegDateTo.Value.Date;
                        //query = query.Where(a => DateTime.Compare(a.CreateDT, regDTTo) > 0);
                        query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) <= EntityFunctions.TruncateTime(regDTTo));
                    }

                    //added by ravi : retrieve only criteria based records
                    if (criteria.Criteria == null)
                        criteria.Criteria = "R";
                    if (criteria.Criteria != null)
                    {

                        if (criteria.Criteria == "R")// mobility
                        {
                            query = query.Where(a => a.Reg.Trn_Type == "R");
                        }
                        else if (criteria.Criteria == "S")
                        {
                            query = query.Where(a => a.Reg.Trn_Type == "S");
                        }
                        else
                        {
                            // no need default selection   
                        }
                    }


                    //if (defaultTopNo > 0)
                    //    query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);

                    //added by ravi : retrieve only criteria based on search type
                    if (criteria.SearchType.ToString2().ToUpper() == "R")
                    {
                        if (defaultTopNo > 0)
                        {
                            query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);
                        }
                    }
                    else
                    {

                        DateTime dtPrevDt = DateTime.Today.AddDays(-1).Date;
                        query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) == DateTime.Today.Date || EntityFunctions.TruncateTime(a.Reg.CreateDT) == dtPrevDt);

                    }
                    //added by ravi : retrieve only criteria based on search type Ends Here

                    var temp = query.Select(a => new
                    {
                        RegID = a.Reg.ID,
                        RegDate = a.Reg.CreateDT,
                        QueueNo = a.Reg.QueueNo,
                        IDCardNo = a.Cust.IDCardNo,
                        CustName = a.Cust.FullName,
                        Status = a.Status.Description,
                        MSISDN = a.Cust.ContactNo,
                        CenterOrg = a.Organization.Name,
                        LastAccessID = a.Reg.LastAccessID != null ? a.Reg.LastAccessID : string.Empty,
                        LastUpdateID = a.RegStatus.LastAccessID != null ? a.RegStatus.LastAccessID : string.Empty,
                        regType = a.Reg.RegTypeID,
                        regTypes = a.RegTypes.Description,
                        criteriaType = a.Reg.Trn_Type,
                        fxAcctNo = a.Reg.fxAcctNo,
                        fxSubscrNo = a.Reg.fxSubscrNo,
                        fxSubScrNoResets = a.Reg.fxSubScrNoResets,
                        ApprovedBlacklistPerson = a.Reg.ApprovedBlacklistPerson,
                        CRPType = a.Reg.CRPType

                    }).Distinct();

                    result = temp.Select(a => new RegistrationSearchResult()
                    {
                        RegID = a.RegID,
                        RegDate = a.RegDate,
                        QueueNo = a.QueueNo,
                        IDCardNo = a.IDCardNo,
                        CustomerName = a.CustName,
                        Status = a.Status,
                        MSISDN = a.MSISDN,
                        CenterOrg = a.CenterOrg,
                        LastAccessID = a.LastAccessID,
                        LastUpdateID = a.LastUpdateID,
                        RegType = a.regType,
                        RegTypeDescription = a.regTypes,
                        CriteriaType = a.criteriaType,
                        FxAcctNo = a.fxAcctNo,
                        FxSubscrNo = a.fxSubscrNo,
                        FxSubScrNoResets = a.fxSubScrNoResets,
                        ApprovedBlacklistPerson = a.ApprovedBlacklistPerson,
                        CRPType = a.CRPType

                    }).ToList();
                }
            }

            //if (string.IsNullOrEmpty(criteria.RegID) && string.IsNullOrEmpty(criteria.QueueNo) && criteria.StatusID == 0 && string.IsNullOrEmpty(criteria.IDCardNo))
            //{

            //    if (result.Where(a => a.RegDate.Date.Equals(DateTime.Today) || a.RegDate.Date.Equals(DateTime.Today.AddDays(-1))) != null)
            //    {
            //        result = result.Where(a => a.RegDate.Date.Equals(DateTime.Today) || a.RegDate.Date.Equals(DateTime.Today.AddDays(-1))).ToList();
            //    }
            //    else
            //    {
            //        result = null;
            //    }
            //}

            Logger.InfoFormat("Exiting {0}.RegistrationSearch({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
            return result;
        }
        

          
          
         public static List<RegistrationSearchResult> RegistrationSearch(RegistrationSearchCriteria criteria)
         {
            Logger.InfoFormat("Entering {0}.RegistrationSearch({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            List<RegistrationSearchResult> result = new List<RegistrationSearchResult>();
            int defaultTopNo = criteria.PageSize > 0 ? criteria.PageSize : 0;

            var regTypeCodes = new List<string>();
            if (criteria.IsMobilePlan)
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_DevicePlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_PlanOnly);
                //added by Deepika
                regTypeCodes.Add(Properties.Settings.Default.RegType_SupplementaryPlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_NewLine);
            }
            else
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomePersonal);
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomeCorporate);
            }

            using (var context = new OnlineStoreDbContext())
            {
                var userOrgID = context.Users.Single(a => a.ID == criteria.UserID).OrgID;
                var userOrgTypeID = context.Organizations.Single(a => a.ID == userOrgID).OrgTypeID;
                var userOrgTypeCode = context.OrgTypes.Single(a => a.ID == userOrgTypeID).Code;

                var orgTypeViewCertain = Properties.Settings.Default.ViewCertainRoles.Split(',').ToList();

                if (criteria != null)
                {
                    var query = from registration in context.Registrations

                                join cust in context.Customers on
                                registration.CustomerID equals cust.ID //into custs
                                //from cust in custs.DefaultIfEmpty()

                                join regStatus in context.RegStatuses on
                                new { regID = registration.ID, active = true } equals new { regID = regStatus.RegID, active = regStatus.Active } //into regStatuses
                                //from regStatus in regStatuses.DefaultIfEmpty()

                                join status in context.Statuses on
                                regStatus.StatusID equals status.ID //into statuses
                                //from status in statuses.DefaultIfEmpty()

                                join organization in context.Organizations on
                                registration.CenterOrgID equals organization.ID

                                join regType in
                                    (from rt in context.RegTypes
                                     where regTypeCodes.Contains(rt.Code)
                                     select new { ID = rt.ID }) on registration.RegTypeID equals regType.ID

                                join regTypes in context.RegTypes on
                                registration.RegTypeID equals regTypes.ID
                                //join regType in (from regTypes in context.RegTypes where regTypeCodes.Contains(regTypes.Code)) on
                                //new { regTypeID = registration.RegTypeID,  } equals new { regTypeID = regType.ID }

                                select new
                                {
                                    Reg = registration,
                                    Cust = cust,
                                    Status = status,
                                    RegStatus = regStatus,
                                    Organization = organization,
                                    RegTypes = regTypes

                                };


                    if (orgTypeViewCertain.Contains(userOrgTypeCode))
                    {
                        query = query.Where(a => a.Reg.CenterOrgID == userOrgID);
                    }

                    if (!string.IsNullOrEmpty(criteria.RegID))
                    {
                        var regID = criteria.RegID.ToInt();
                        query = query.Where(a => a.Reg.ID == regID);
                    }

                    if (!string.IsNullOrEmpty(criteria.IDCardNo))
                        query = query.Where(a => a.Cust.IDCardNo == criteria.IDCardNo);

                    if (!string.IsNullOrEmpty(criteria.QueueNo))
                        query = query.Where(a => a.Reg.QueueNo == criteria.QueueNo);

                    if (criteria.StatusID != 0)
                        query = query.Where(a => a.Status.ID == criteria.StatusID);

                    if (criteria.CenterOrgID.HasValue)
                        query = query.Where(a => a.Reg.CenterOrgID == criteria.CenterOrgID);

                    if (!string.IsNullOrEmpty(criteria.MSISDN))
                        query = query.Where(a => a.Cust.ContactNo == criteria.MSISDN);

                    if (!string.IsNullOrEmpty(criteria.CustName))
                        query = query.Where(a => a.Cust.FullName.Contains(criteria.CustName));

                    if (criteria.RegDateFrom.HasValue)
                    {
                        var regDTFrom = criteria.RegDateFrom.Value.Date;
                        //query = query.Where(a => DateTime.Compare(a.CreateDT, regDTFrom) > 0);
                        query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) >= EntityFunctions.TruncateTime(regDTFrom));
                    }

                    if (criteria.RegDateTo.HasValue)
                    {
                        var regDTTo = criteria.RegDateTo.Value.Date;
                        //query = query.Where(a => DateTime.Compare(a.CreateDT, regDTTo) > 0);
                        query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) <= EntityFunctions.TruncateTime(regDTTo));
                    }

                    if (defaultTopNo > 0)
                        query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);

                    var temp = query.Select(a => new
                    {
                        RegID = a.Reg.ID,
                        RegDate = a.Reg.CreateDT,
                        QueueNo = a.Reg.QueueNo,
                        IDCardNo = a.Cust.IDCardNo,
                        CustName = a.Cust.FullName,
                        Status = a.Status.Description,
                        MSISDN = a.Cust.ContactNo,
                        CenterOrg = a.Organization.Name,
                        LastAccessID = a.Reg.LastAccessID != null ? a.Reg.LastAccessID : string.Empty,
                        LastUpdateID = a.RegStatus.LastAccessID != null ? a.RegStatus.LastAccessID : string.Empty,
                        regType = a.Reg.RegTypeID,
                        regTypes = a.RegTypes.Description

                    }).Distinct();

                    result = temp.Select(a => new RegistrationSearchResult()
                    {
                        RegID = a.RegID,
                        RegDate = a.RegDate,
                        QueueNo = a.QueueNo,
                        IDCardNo = a.IDCardNo,
                        CustomerName = a.CustName,
                        Status = a.Status,
                        MSISDN = a.MSISDN,
                        CenterOrg = a.CenterOrg,
                        LastAccessID = a.LastAccessID,
                        LastUpdateID = a.LastUpdateID,
                        RegType = a.regType,
                        RegTypeDescription = a.regTypes

                    }).ToList();
                }
            }

            Logger.InfoFormat("Exiting {0}.RegistrationSearch({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
            return result;
        } 
          
          
          
         public static List<RegistrationSearchResult> CashierSearch(RegistrationSearchCriteria criteria)
        {
            Logger.InfoFormat("Entering {0}.RegistrationSearch({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            List<RegistrationSearchResult> result = new List<RegistrationSearchResult>();
            int defaultTopNo = criteria.PageSize > 0 ? criteria.PageSize : 0;

            var regTypeCodes = new List<string>();
            if (criteria.IsMobilePlan)
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_DevicePlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_PlanOnly);
                //added by Deepika
                regTypeCodes.Add(Properties.Settings.Default.RegType_SupplementaryPlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_NewLine);
                //Added by Patanjali for MNP
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPPlanOnly);
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPSupplementaryPlan);
                //Added by Basu for Secondary Line
                regTypeCodes.Add(Properties.Settings.Default.RegType_SecondaryPlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPNewLine);
                //Added by Patanjali for MNP Ends here
                regTypeCodes.Add(Properties.Settings.Default.Sim_Replacement_RegType);//Added By Himansu
                regTypeCodes.Add(Properties.Settings.Default.RegType_AddRemoveVAS);
                //Added by kotesh for CRP
                regTypeCodes.Add(Properties.Settings.Default.RegType_CRP);
                regTypeCodes.Add(Properties.Settings.Default.RegType_DeviceCRP);
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPPlanWithMultiSuppline);

            }
            else
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomePersonal);
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomeCorporate);
            }

            using (var context = new OnlineStoreDbContext())
            {
                var userOrgID = context.Users.Single(a => a.ID == criteria.UserID).OrgID;
                var userOrgTypeID = context.Organizations.Single(a => a.ID == userOrgID).OrgTypeID;
                var userOrgTypeCode = context.OrgTypes.Single(a => a.ID == userOrgTypeID).Code;
                criteria.CenterOrgID = userOrgID;
                var orgTypeViewCertain = Properties.Settings.Default.ViewCertainRoles.Split(',').ToList();

                if (criteria != null)
                {
                    #region Added by Nreddy for canceled in Cashier search criterias
                    if (criteria.StatusID == 21)
                    {
                        var query = from registration in context.Registrations

                                    join cust in context.Customers on
                                    registration.CustomerID equals cust.ID //into custs
                                    //from cust in custs.DefaultIfEmpty()

                                    join regStatus in context.RegStatuses on
                                    new { regID = registration.ID, active = true } equals new { regID = regStatus.RegID, active = regStatus.Active }

                                    //registration.ID equals regStatus.RegID
                                    //where regStatus.StatusID == 31
                                    //into regStatuses
                                    //from regStatus in regStatuses.DefaultIfEmpty()

                                    join status in context.Statuses on
                                    regStatus.StatusID equals status.ID //into statuses
                                    //from status in statuses.DefaultIfEmpty()

                                    join organization in context.Organizations on
                                    registration.CenterOrgID equals organization.ID

                                    join regType in
                                        (from rt in context.RegTypes
                                         where regTypeCodes.Contains(rt.Code)
                                         select new { ID = rt.ID }) on registration.RegTypeID equals regType.ID

                                    join regTypes in context.RegTypes on
                                    registration.RegTypeID equals regTypes.ID
                                    //join regType in (from regTypes in context.RegTypes where regTypeCodes.Contains(regTypes.Code)) on
                                    //new { regTypeID = registration.RegTypeID, } equals new { regTypeID = regType.ID }
                                    where regStatus.StatusID != 1 && regStatus.StatusID != 28 && regStatus.StatusID != 91 && regStatus.StatusID != 96 && regStatus.StatusID != 22
                                    select new
                                    {
                                        Reg = registration,
                                        Cust = cust,
                                        Status = status,
                                        RegStatus = regStatus,
                                        Organization = organization,
                                        RegTypes = regTypes

                                    };

                        if (orgTypeViewCertain.Contains(userOrgTypeCode))
                        {
                            query = query.Where(a => a.Reg.CenterOrgID == userOrgID);
                        }

                        if (!string.IsNullOrEmpty(criteria.RegID))
                        {
                            var regID = criteria.RegID.ToInt();
                            query = query.Where(a => a.Reg.ID == regID);
                        }

                        if (!string.IsNullOrEmpty(criteria.IDCardNo))
                            query = query.Where(a => a.Cust.IDCardNo == criteria.IDCardNo);

                        if (!string.IsNullOrEmpty(criteria.QueueNo))
                            query = query.Where(a => a.Reg.QueueNo == criteria.QueueNo);

                        if (criteria.StatusID != 0)
                            query = query.Where(a => a.Status.ID == criteria.StatusID);

                        if (criteria.CenterOrgID.HasValue)
                            query = query.Where(a => a.Reg.CenterOrgID == criteria.CenterOrgID);

                        if (!string.IsNullOrEmpty(criteria.MSISDN))
                            query = query.Where(a => a.Cust.ContactNo == criteria.MSISDN);

                        if (!string.IsNullOrEmpty(criteria.CustName))
                            query = query.Where(a => a.Cust.FullName.Contains(criteria.CustName));

                        if (criteria.RegDateFrom.HasValue)
                        {
                            var regDTFrom = criteria.RegDateFrom.Value.Date;
                            //query = query.Where(a => DateTime.Compare(a.CreateDT, regDTFrom) > 0);
                            query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) >= EntityFunctions.TruncateTime(regDTFrom));
                        }

                        if (criteria.RegDateTo.HasValue)
                        {
                            var regDTTo = criteria.RegDateTo.Value.Date;
                            //query = query.Where(a => DateTime.Compare(a.CreateDT, regDTTo) > 0);
                            query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) <= EntityFunctions.TruncateTime(regDTTo));
                        }


                        //added by ravi : retrieve only criteria based on search type
                        //if (defaultTopNo > 0)
                        //    query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);

                        //added by ranjeeth : retrieve only criteria based records
                        if (criteria.Criteria == null)
                            criteria.Criteria = "R";
                        if (criteria.Criteria != null)
                        {

                            if (criteria.Criteria == "R")// mobility
                            {
                                query = query.Where(a => a.Reg.Trn_Type == "R");
                            }
                            else if (criteria.Criteria == "S")
                            {
                                query = query.Where(a => a.Reg.Trn_Type == "S");
                            }
                            else
                            {
                                // no need default selection   
                            }
                        }

                        //if (defaultTopNo > 0)
                        //    query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);

                        if (criteria.SearchType.ToString2().ToUpper() == "R")
                        {
                            if (defaultTopNo > 0)
                            {
                                query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);
                            }
                        }
                        else
                        {

                            DateTime dtPrevDt = DateTime.Today.AddDays(-1).Date;
                            query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) == DateTime.Today.Date || EntityFunctions.TruncateTime(a.Reg.CreateDT) == dtPrevDt);

                        }

                        //added by ravi : retrieve only criteria based on search type  Ends Here
                        var temp = query.Select(a => new
                        {
                            RegID = a.Reg.ID,
                            RegDate = a.Reg.CreateDT,
                            QueueNo = a.Reg.QueueNo,
                            IDCardNo = a.Cust.IDCardNo,
                            CustName = a.Cust.FullName,
                            Status = a.Status.Description,
                            MSISDN = a.Cust.ContactNo,
                            CenterOrg = a.Organization.Name,
                            LastAccessID = a.Reg.LastAccessID != null ? a.Reg.LastAccessID : string.Empty,
                            LastUpdateID = a.RegStatus.LastAccessID != null ? a.RegStatus.LastAccessID : string.Empty,
                            regType = a.Reg.RegTypeID,
                            regTypes = a.RegTypes.Description,
                            criteriaType = a.Reg.Trn_Type,
                            ApprovedBlacklistPerson = a.Reg.ApprovedBlacklistPerson

                        }).Distinct();

                        result = temp.Select(a => new RegistrationSearchResult()
                        {
                            RegID = a.RegID,
                            RegDate = a.RegDate,
                            QueueNo = a.QueueNo,
                            IDCardNo = a.IDCardNo,
                            CustomerName = a.CustName,
                            Status = a.Status,
                            MSISDN = a.MSISDN,
                            CenterOrg = a.CenterOrg,
                            LastAccessID = a.LastAccessID,
                            LastUpdateID = a.LastUpdateID,
                            RegType = a.regType,
                            RegTypeDescription = a.regTypes,
                            CriteriaType = a.criteriaType,
                            ApprovedBlacklistPerson = a.ApprovedBlacklistPerson

                        }).ToList();
                    }
                    #endregion
                    else
                    {
                        var query = from registration in context.Registrations

                                    join cust in context.Customers on
                                    registration.CustomerID equals cust.ID //into custs
                                    //from cust in custs.DefaultIfEmpty()

                                    join regStatus in context.RegStatuses on
                                    new { regID = registration.ID, active = true } equals new { regID = regStatus.RegID, active = regStatus.Active }

                                    //registration.ID equals regStatus.RegID 
                                    //where regStatus.StatusID == 31
                                    //into regStatuses
                                    //from regStatus in regStatuses.DefaultIfEmpty()

                                    join status in context.Statuses on
                                    regStatus.StatusID equals status.ID //into statuses
                                    //from status in statuses.DefaultIfEmpty()

                                    join organization in context.Organizations on
                                    registration.CenterOrgID equals organization.ID

                                    join regType in
                                        (from rt in context.RegTypes
                                         where regTypeCodes.Contains(rt.Code)
                                         select new { ID = rt.ID }) on registration.RegTypeID equals regType.ID

                                    join regTypes in context.RegTypes on
                                    registration.RegTypeID equals regTypes.ID
                                    //join regType in (from regTypes in context.RegTypes where regTypeCodes.Contains(regTypes.Code)) on
                                    //new { regTypeID = registration.RegTypeID,  } equals new { regTypeID = regType.ID }
                                    //where regStatus.StatusID != 1 && regStatus.StatusID != 28 && (from r in context.RegStatuses where r.StatusID == 31 select r.RegID).Contains(registration.ID)
                                    where (regStatus.StatusID != 22 && regStatus.StatusID != 1 && regStatus.StatusID != 28 && regStatus.StatusID != 93 && regStatus.StatusID != 91 && regStatus.StatusID != 96) && (registration.Trn_Type == "R" || registration.Trn_Type == "S")

                                    select new
                                    {
                                        Reg = registration,
                                        Cust = cust,
                                        Status = status,
                                        RegStatus = regStatus,
                                        Organization = organization,
                                        RegTypes = regTypes

                                    };

                        if (orgTypeViewCertain.Contains(userOrgTypeCode))
                        {
                            query = query.Where(a => a.Reg.CenterOrgID == userOrgID);
                        }

                        if (!string.IsNullOrEmpty(criteria.RegID))
                        {
                            var regID = criteria.RegID.ToInt();
                            query = query.Where(a => a.Reg.ID == regID);
                        }

                        if (!string.IsNullOrEmpty(criteria.IDCardNo))
                            query = query.Where(a => a.Cust.IDCardNo == criteria.IDCardNo);

                        if (!string.IsNullOrEmpty(criteria.QueueNo))
                            query = query.Where(a => a.Reg.QueueNo == criteria.QueueNo);

                        if (criteria.StatusID != 0)
                            query = query.Where(a => a.Status.ID == criteria.StatusID);

                        if (criteria.CenterOrgID.HasValue)
                            query = query.Where(a => a.Reg.CenterOrgID == criteria.CenterOrgID);

                        if (!string.IsNullOrEmpty(criteria.MSISDN))
                            query = query.Where(a => a.Cust.ContactNo == criteria.MSISDN);

                        if (!string.IsNullOrEmpty(criteria.CustName))
                            query = query.Where(a => a.Cust.FullName.Contains(criteria.CustName));

                        if (criteria.RegDateFrom.HasValue)
                        {
                            var regDTFrom = criteria.RegDateFrom.Value.Date;
                            //query = query.Where(a => DateTime.Compare(a.CreateDT, regDTFrom) > 0);
                            query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) >= EntityFunctions.TruncateTime(regDTFrom));
                        }

                        if (criteria.RegDateTo.HasValue)
                        {
                            var regDTTo = criteria.RegDateTo.Value.Date;
                            //query = query.Where(a => DateTime.Compare(a.CreateDT, regDTTo) > 0);
                            query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) <= EntityFunctions.TruncateTime(regDTTo));
                        }

                        //added by ranjeeth : retrieve only criteria based records
                        if (criteria.Criteria == null)
                            criteria.Criteria = "R";
                        if (criteria.Criteria != null)
                        {

                            if (criteria.Criteria == "R")// mobility
                            {
                                query = query.Where(a => a.Reg.Trn_Type == "R");
                            }
                            else if (criteria.Criteria == "S")
                            {
                                query = query.Where(a => a.Reg.Trn_Type == "S");
                            }
                            else
                            {
                                // no need default selection   
                            }
                        }

                        //if (defaultTopNo > 0)
                        //    query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);

                        //added by ravi : retrieve only criteria based on search type
                        if (criteria.SearchType.ToString2().ToUpper() == "R")
                        {
                            if (defaultTopNo > 0)
                            {
                                query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);
                            }
                        }
                        else
                        {

                            DateTime dtPrevDt = DateTime.Today.AddDays(-1).Date;
                            query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) == DateTime.Today.Date || EntityFunctions.TruncateTime(a.Reg.CreateDT) == dtPrevDt);

                        }
                        //added by ravi : retrieve only criteria based on search type Ends Here
                        var temp = query.Select(a => new
                        {
                            RegID = a.Reg.ID,
                            RegDate = a.Reg.CreateDT,
                            QueueNo = a.Reg.QueueNo,
                            IDCardNo = a.Cust.IDCardNo,
                            CustName = a.Cust.FullName,
                            Status = a.Status.Description,
                            MSISDN = a.Cust.ContactNo,
                            CenterOrg = a.Organization.Name,
                            LastAccessID = a.Reg.LastAccessID != null ? a.Reg.LastAccessID : string.Empty,
                            LastUpdateID = a.RegStatus.LastAccessID != null ? a.RegStatus.LastAccessID : string.Empty,
                            regType = a.Reg.RegTypeID,
                            regTypes = a.RegTypes.Description,
                            criteriaType = a.Reg.Trn_Type,
                            ApprovedBlacklistPerson = a.Reg.ApprovedBlacklistPerson,
                            CRPType = a.Reg.CRPType

                        }).Distinct();

                        result = temp.Select(a => new RegistrationSearchResult()
                        {
                            RegID = a.RegID,
                            RegDate = a.RegDate,
                            QueueNo = a.QueueNo,
                            IDCardNo = a.IDCardNo,
                            CustomerName = a.CustName,
                            Status = a.Status,
                            MSISDN = a.MSISDN,
                            CenterOrg = a.CenterOrg,
                            LastAccessID = a.LastAccessID,
                            LastUpdateID = a.LastUpdateID,
                            RegType = a.regType,
                            RegTypeDescription = a.regTypes,
                            CriteriaType = a.criteriaType,
                            ApprovedBlacklistPerson = a.ApprovedBlacklistPerson,
                            CRPType = a.CRPType

                        }).ToList();
                    }
                }
            }

            //if (string.IsNullOrEmpty(criteria.RegID) && string.IsNullOrEmpty(criteria.QueueNo) && criteria.StatusID == 0 && string.IsNullOrEmpty(criteria.IDCardNo))
            //{

            //    if (result.Where(a => a.RegDate.Date.Equals(DateTime.Today) || a.RegDate.Date.Equals(DateTime.Today.AddDays(-1))) != null)
            //    {
            //        result = result.Where(a => a.RegDate.Date.Equals(DateTime.Today) || a.RegDate.Date.Equals(DateTime.Today.AddDays(-1))).ToList();
            //    }
            //    else
            //    {
            //        result = null;
            //    }
            //}

            Logger.InfoFormat("Exiting {0}.RegistrationSearch({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
            return result;
        }  
          
        */
        #endregion
        private static List<string> generateRegTypeCode(bool IsMobilePlan)
        {
            var regTypeCodes = new List<string>();
            if (IsMobilePlan)
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_DevicePlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_PlanOnly);
                //added by Deepika
                regTypeCodes.Add(Properties.Settings.Default.RegType_SupplementaryPlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_NewLine);
                //Added by Patanjali for MNP
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPPlanOnly);
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPSupplementaryPlan);
                //Added by Basu for Secondary Line
                regTypeCodes.Add(Properties.Settings.Default.RegType_SecondaryPlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPNewLine);
                //Added by Patanjali for MNP Ends here
                regTypeCodes.Add(Properties.Settings.Default.Sim_Replacement_RegType);//Added By Himansu
                regTypeCodes.Add(Properties.Settings.Default.RegType_AddRemoveVAS);
                //Added by kotesh for CRP
                regTypeCodes.Add(Properties.Settings.Default.RegType_CRP);
                regTypeCodes.Add(Properties.Settings.Default.RegType_DeviceCRP);
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPPlanWithMultiSuppline);

                regTypeCodes.Add(Properties.Settings.Default.RegType_DeviceSale);

                regTypeCodes.Add(Properties.Settings.Default.RegType_AddContract);
                regTypeCodes.Add(Properties.Settings.Default.RegType_Accessory);
            }
            else
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomePersonal);
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomeCorporate);
            }
            return regTypeCodes;
        }

        public static List<RegistrationSearchResult> CashierSearch(RegistrationSearchCriteria criteria)
        {
            DateTime stDate = DateTime.Now;
            Logger.InfoFormat("Entering {0}.CashierSearch({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            List<RegistrationSearchResult> result = new List<RegistrationSearchResult>();
            int defaultTopNo = criteria.PageSize > 0 ? criteria.PageSize : 0;
            var regTypeCodes = generateRegTypeCode(criteria.IsMobilePlan);
            if (!ReferenceEquals(criteria.Criteria, null))
            {
                criteria.Criteria = criteria.Criteria.Equals("RS") ? "R,S" : criteria.Criteria;
            }
            #region "sql implementation"
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GETREGISTRATIONS_CASHIER", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@RegID", criteria.RegID));
                sqlCmd.Parameters.Add(new SqlParameter("@IDCardNo", criteria.IDCardNo));
                sqlCmd.Parameters.Add(new SqlParameter("@QueueNo", criteria.QueueNo));
                sqlCmd.Parameters.Add(new SqlParameter("@StatusID", criteria.StatusID));
                sqlCmd.Parameters.Add(new SqlParameter("@CenterOrgID", criteria.CenterOrgID));
                sqlCmd.Parameters.Add(new SqlParameter("@MSISDN", criteria.MSISDN));
                sqlCmd.Parameters.Add(new SqlParameter("@CustName", criteria.CustName));
                sqlCmd.Parameters.Add(new SqlParameter("@RegDateFrom", criteria.RegDateFrom));
                sqlCmd.Parameters.Add(new SqlParameter("@RegDateTo", criteria.RegDateTo));
                sqlCmd.Parameters.Add(new SqlParameter("@Criteria_Type", criteria.Criteria));
                sqlCmd.Parameters.Add(new SqlParameter("@defaultTopNo", defaultTopNo));
                sqlCmd.Parameters.Add(new SqlParameter("@orgTypeViewCertain", Properties.Settings.Default.ViewCertainRoles));
                sqlCmd.Parameters.Add(new SqlParameter("@UserID", criteria.UserID));
                sqlCmd.Parameters.Add(new SqlParameter("@regTypeCodes", string.Join(",", regTypeCodes)));
                sqlCmd.Parameters.Add(new SqlParameter("@SimSerialNumber", criteria.SimSerial));
                sqlCmd.Parameters.Add(new SqlParameter("@ImeiNumber", criteria.ImeiNumber));
                sqlCmd.Parameters.Add(new SqlParameter("@IsSuperAdmin", criteria.IsSuperAdmin));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();

                if (!ReferenceEquals(dsRegistrations, null))
                {
                    if (!ReferenceEquals(dsRegistrations.Tables[0], null) && (dsRegistrations.Tables[0].Rows.Count > 0))
                    {
                        for (int i = 0; i < dsRegistrations.Tables[0].Rows.Count; i++)
                        {
                            var SearchResult = new RegistrationSearchResult()
                            {
                                RegID = Convert.ToInt32(dsRegistrations.Tables[0].Rows[i]["RegID"]),
                                RegDate = Convert.ToDateTime(dsRegistrations.Tables[0].Rows[i]["RegDate"]),
                                QueueNo = dsRegistrations.Tables[0].Rows[i]["QueueNo"].ToString2(),
                                IDCardNo = dsRegistrations.Tables[0].Rows[i]["IDCardNo"].ToString2(),
                                CustomerName = dsRegistrations.Tables[0].Rows[i]["CustomerName"].ToString2(),
                                LastUpdateID = dsRegistrations.Tables[0].Rows[i]["LastAccessID"].ToString2(),
                                LastAccessID = dsRegistrations.Tables[0].Rows[i]["LastUpdateID"].ToString2(),
                                Status = dsRegistrations.Tables[0].Rows[i]["Status"].ToString2(),
                                CenterOrg = dsRegistrations.Tables[0].Rows[i]["CenterOrg"].ToString2(),
                                MSISDN = dsRegistrations.Tables[0].Rows[i]["MSISDN"].ToString2(),
                                RegType = Convert.ToInt32(dsRegistrations.Tables[0].Rows[i]["RegType"]),
                                RegTypeDescription = dsRegistrations.Tables[0].Rows[i]["RegTypeDescription"].ToString2(),
                                CriteriaType = dsRegistrations.Tables[0].Rows[i]["CriteriaType"].ToString2(),
                                FxAcctNo = dsRegistrations.Tables[0].Rows[i]["FxAcctNo"].ToString2(),
                                FxSubscrNo = dsRegistrations.Tables[0].Rows[i]["FxSubscrNo"].ToString2(),
                                FxSubScrNoResets = dsRegistrations.Tables[0].Rows[i]["FxSubScrNoResets"].ToString2(),
                                ApprovedBlacklistPerson = dsRegistrations.Tables[0].Rows[i]["ApprovedBlacklistPerson"].ToString2(),
                                CRPType = dsRegistrations.Tables[0].Rows[i]["CRPType"].ToString2(),
                                UserRoles = dsRegistrations.Tables[0].Rows[i]["UserRoles"].ToString2(),
                                IsVIP = dsRegistrations.Tables[0].Rows[i]["IsVIP"].ToString2(),
                                hasPrinDevice = dsRegistrations.Tables[0].Rows[i]["hasPrinDevice"].ToString2(),
                                PenaltyAmount = dsRegistrations.Tables[0].Rows[i]["PenaltyAmount"].ToString2(),
                                SMcontractType = dsRegistrations.Tables[0].Rows[i]["SMcontractType"].ToString2(),
                                PrinOfferId = dsRegistrations.Tables[0].Rows[i]["PrinOfferId"].ToString2().ToInt(),
                                SASupp = dsRegistrations.Tables[0].Rows[i]["SASupp"].ToString2(),
                                SimType = dsRegistrations.Tables[0].Rows[i]["SimType"].ToString2(),
                                SimReplacementType = dsRegistrations.Tables[0].Rows[i]["SimReplacementType"].ToString2(),
                                hasPrinAccessory = dsRegistrations.Tables[0].Rows[i]["hasPrinAccessory"].ToString2(),
                                hasSuppDevice = dsRegistrations.Tables[0].Rows[i]["hasSuppDevice"].ToString2(),
                                hasSuppline = dsRegistrations.Tables[0].Rows[i]["hasSuppline"].ToString2(),
                                hasSuppAccessory = dsRegistrations.Tables[0].Rows[i]["hasSuppAccessory"].ToString2(),
                                userType = dsRegistrations.Tables[0].Rows[i]["userType"].ToString2(),
                                isThirdParty = dsRegistrations.Tables[0].Rows[i]["isThirdParty"].ToString2()
                            };
                            result.Add(SearchResult);
                        }
                    }
                }
            }
            #endregion
            Logger.InfoFormat("Exiting {0}.CashierSearch({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
            TimeSpan timeSpn = (DateTime.Now - stDate);
            Common.LogMessage(string.Format("CashierSearch(for userid:{0}) TimeSpan:{1}", criteria.UserID, timeSpn.ToString("hh\\:mm\\:ss\\.fff")));
            return result;
        }

        public static List<RegistrationSearchResult> OrderStatusReportSearch(RegistrationSearchCriteria criteria)
        {
            DateTime stDate = DateTime.Now;
            Logger.InfoFormat("Entering {0}.CashierSearch({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            List<RegistrationSearchResult> result = new List<RegistrationSearchResult>();
            int defaultTopNo = criteria.PageSize > 0 ? criteria.PageSize : 0;
            var regTypeCodes = generateRegTypeCode(criteria.IsMobilePlan);
            if (!ReferenceEquals(criteria.Criteria, null))
            {
                criteria.Criteria = criteria.Criteria.Equals("RS") ? "R,S" : criteria.Criteria;
            }
            #region "sql implementation"
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GETREGISTRATIONS_ORDSTATUS", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@RegID", criteria.RegID));
                sqlCmd.Parameters.Add(new SqlParameter("@IDCardNo", criteria.IDCardNo));
                //sqlCmd.Parameters.Add(new SqlParameter("@QueueNo", criteria.QueueNo));
                sqlCmd.Parameters.Add(new SqlParameter("@StatusID", criteria.StatusID));
                sqlCmd.Parameters.Add(new SqlParameter("@CenterOrgID", criteria.CenterOrgID));
                sqlCmd.Parameters.Add(new SqlParameter("@MSISDN", criteria.MSISDN));
                //sqlCmd.Parameters.Add(new SqlParameter("@CustName", criteria.CustName));
                sqlCmd.Parameters.Add(new SqlParameter("@RegDateFrom", criteria.RegDateFrom));
                sqlCmd.Parameters.Add(new SqlParameter("@RegDateTo", criteria.RegDateTo));
                //sqlCmd.Parameters.Add(new SqlParameter("@Criteria_Type", criteria.Criteria));
                sqlCmd.Parameters.Add(new SqlParameter("@defaultTopNo", defaultTopNo));
                sqlCmd.Parameters.Add(new SqlParameter("@orgTypeViewCertain", Properties.Settings.Default.ViewCertainRoles));
                sqlCmd.Parameters.Add(new SqlParameter("@UserID", criteria.UserID));
                sqlCmd.Parameters.Add(new SqlParameter("@regTypeCodes", string.Join(",", regTypeCodes)));
                //sqlCmd.Parameters.Add(new SqlParameter("@SimSerialNumber", criteria.SimSerial));
                //sqlCmd.Parameters.Add(new SqlParameter("@ImeiNumber", criteria.ImeiNumber));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();

                if (!ReferenceEquals(dsRegistrations, null))
                {
                    if (!ReferenceEquals(dsRegistrations.Tables[0], null) && (dsRegistrations.Tables[0].Rows.Count > 0))
                    {
                        for (int i = 0; i < dsRegistrations.Tables[0].Rows.Count; i++)
                        {
                            var SearchResult = new RegistrationSearchResult()
                            {
                                RegID = Convert.ToInt32(dsRegistrations.Tables[0].Rows[i]["RegID"]),
                                RegDate = Convert.ToDateTime(dsRegistrations.Tables[0].Rows[i]["RegDate"]),
                                QueueNo = dsRegistrations.Tables[0].Rows[i]["QueueNo"].ToString2(),
                                IDCardNo = dsRegistrations.Tables[0].Rows[i]["IDCardNo"].ToString2(),
                                CustomerName = dsRegistrations.Tables[0].Rows[i]["CustomerName"].ToString2(),
                                LastUpdateID = dsRegistrations.Tables[0].Rows[i]["LastAccessID"].ToString2(),
                                LastAccessID = dsRegistrations.Tables[0].Rows[i]["LastUpdateID"].ToString2(),
                                Status = dsRegistrations.Tables[0].Rows[i]["Status"].ToString2(),
                                CenterOrg = dsRegistrations.Tables[0].Rows[i]["CenterOrg"].ToString2(),
                                MSISDN = dsRegistrations.Tables[0].Rows[i]["MSISDN"].ToString2(),
                                RegType = Convert.ToInt32(dsRegistrations.Tables[0].Rows[i]["RegType"]),
                                RegTypeDescription = dsRegistrations.Tables[0].Rows[i]["RegTypeDescription"].ToString2(),
                                CriteriaType = dsRegistrations.Tables[0].Rows[i]["CriteriaType"].ToString2(),
                                FxAcctNo = dsRegistrations.Tables[0].Rows[i]["FxAcctNo"].ToString2(),
                                FxSubscrNo = dsRegistrations.Tables[0].Rows[i]["FxSubscrNo"].ToString2(),
                                FxSubScrNoResets = dsRegistrations.Tables[0].Rows[i]["FxSubScrNoResets"].ToString2(),
                                ApprovedBlacklistPerson = dsRegistrations.Tables[0].Rows[i]["ApprovedBlacklistPerson"].ToString2(),
                                CRPType = dsRegistrations.Tables[0].Rows[i]["CRPType"].ToString2(),
                                UserRoles = dsRegistrations.Tables[0].Rows[i]["UserRoles"].ToString2(),
                                hasPrinDevice = dsRegistrations.Tables[0].Rows[i]["hasPrinDevice"].ToString2(),
                                PenaltyAmount = dsRegistrations.Tables[0].Rows[i]["PenaltyAmount"].ToString2(),
                                SMcontractType = dsRegistrations.Tables[0].Rows[i]["SMcontractType"].ToString2(),
                                PrinOfferId = dsRegistrations.Tables[0].Rows[i]["PrinOfferId"].ToString2().ToInt(),
                                SASupp = dsRegistrations.Tables[0].Rows[i]["SASupp"].ToString2(),
                                SimType = dsRegistrations.Tables[0].Rows[i]["SimType"].ToString2(),
                                SimReplacementType = dsRegistrations.Tables[0].Rows[i]["SimReplacementType"].ToString2(),
                                hasPrinAccessory = dsRegistrations.Tables[0].Rows[i]["hasPrinAccessory"].ToString2(),
                                hasSuppDevice = dsRegistrations.Tables[0].Rows[i]["hasSuppDevice"].ToString2(),
                                hasSuppline = dsRegistrations.Tables[0].Rows[i]["hasSuppline"].ToString2(),
                                hasSuppAccessory = dsRegistrations.Tables[0].Rows[i]["hasSuppAccessory"].ToString2(),
                                userType = dsRegistrations.Tables[0].Rows[i]["userType"].ToString2(),
                                isThirdParty = dsRegistrations.Tables[0].Rows[i]["isThirdParty"].ToString2()
                            };
                            result.Add(SearchResult);
                        }
                    }
                }
            }
            #endregion
            Logger.InfoFormat("Exiting {0}.CashierSearch({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
            TimeSpan timeSpn = (DateTime.Now - stDate);
            Common.LogMessage(string.Format("CashierSearch(for userid:{0}) TimeSpan:{1}", criteria.UserID, timeSpn.ToString("hh\\:mm\\:ss\\.fff")));
            return result;
        }

        public static List<RegistrationSearchResult> OrderSearchPOS(RegistrationSearchCriteria criteria)
        {
            DateTime stDate = DateTime.Now;
            Logger.InfoFormat("Entering {0}.OrderSearchbyRegID({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            List<RegistrationSearchResult> result = new List<RegistrationSearchResult>();
            
            #region "sql implementation"
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GETREGISTRATIONS_POS", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@RegID", criteria.RegID));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();

                if (!ReferenceEquals(dsRegistrations, null))
                {
                    if (!ReferenceEquals(dsRegistrations.Tables[0], null) && (dsRegistrations.Tables[0].Rows.Count > 0))
                    {
                        for (int i = 0; i < dsRegistrations.Tables[0].Rows.Count; i++)
                        {
                            var SearchResult = new RegistrationSearchResult()
                            {
                                RegID = Convert.ToInt32(dsRegistrations.Tables[0].Rows[i]["RegID"]),
                                RegDate = Convert.ToDateTime(dsRegistrations.Tables[0].Rows[i]["RegDate"]),
                                QueueNo = dsRegistrations.Tables[0].Rows[i]["QueueNo"].ToString2(),
                                IDCardNo = dsRegistrations.Tables[0].Rows[i]["IDCardNo"].ToString2(),
                                CustomerName = dsRegistrations.Tables[0].Rows[i]["CustomerName"].ToString2(),
                                LastUpdateID = dsRegistrations.Tables[0].Rows[i]["LastAccessID"].ToString2(),
                                LastAccessID = dsRegistrations.Tables[0].Rows[i]["LastUpdateID"].ToString2(),
                                Status = dsRegistrations.Tables[0].Rows[i]["Status"].ToString2(),
                                CenterOrg = dsRegistrations.Tables[0].Rows[i]["CenterOrg"].ToString2(),
                                MSISDN = dsRegistrations.Tables[0].Rows[i]["MSISDN"].ToString2(),
                                RegType = Convert.ToInt32(dsRegistrations.Tables[0].Rows[i]["RegType"]),
                                RegTypeDescription = dsRegistrations.Tables[0].Rows[i]["RegTypeDescription"].ToString2(),
                                CriteriaType = dsRegistrations.Tables[0].Rows[i]["CriteriaType"].ToString2(),
                                CRPType = dsRegistrations.Tables[0].Rows[i]["CRPType"].ToString2(),
                                UserRoles = dsRegistrations.Tables[0].Rows[i]["UserRoles"].ToString2(),
                                hasPrinDevice = dsRegistrations.Tables[0].Rows[i]["hasPrinDevice"].ToString2(),
                                PenaltyAmount = dsRegistrations.Tables[0].Rows[i]["PenaltyAmount"].ToString2(),
                                SMcontractType = dsRegistrations.Tables[0].Rows[i]["SMcontractType"].ToString2(),
                                PrinOfferId = dsRegistrations.Tables[0].Rows[i]["PrinOfferId"].ToString2().ToInt(),
                                SASupp = dsRegistrations.Tables[0].Rows[i]["SASupp"].ToString2(),
                                SimType = dsRegistrations.Tables[0].Rows[i]["SimType"].ToString2(),
                                SimReplacementType = dsRegistrations.Tables[0].Rows[i]["SimReplacementType"].ToString2(),
                                hasPrinAccessory = dsRegistrations.Tables[0].Rows[i]["hasPrinAccessory"].ToString2(),
                                hasSuppDevice = dsRegistrations.Tables[0].Rows[i]["hasSuppDevice"].ToString2(),
                                hasSuppline = dsRegistrations.Tables[0].Rows[i]["hasSuppline"].ToString2(),
                                hasSuppAccessory = dsRegistrations.Tables[0].Rows[i]["hasSuppAccessory"].ToString2(),
                                userType = dsRegistrations.Tables[0].Rows[i]["userType"].ToString2(),
                                isThirdParty = dsRegistrations.Tables[0].Rows[i]["isThirdParty"].ToString2()
                            };
                            result.Add(SearchResult);
                        }
                    }
                }
            }
            #endregion
            Logger.InfoFormat("Exiting {0}.OrderSearchbyRegID({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
            TimeSpan timeSpn = (DateTime.Now - stDate);
            Common.LogMessage(string.Format("OrderSearchbyRegID(for userid:{0}) TimeSpan:{1}", criteria.UserID, timeSpn.ToString("hh\\:mm\\:ss\\.fff")));
            return result;
        }

        public static AgentCodeByUserNameResult GetAgentCodeByUserName(string userName)
        {
            AgentCodeByUserNameResult result = new AgentCodeByUserNameResult();
            
            #region "sql implementation"
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("[usp_GetKenanAgentCode]", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@UserName", userName));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();

                if (!ReferenceEquals(dsRegistrations, null))
                {
                    if (!ReferenceEquals(dsRegistrations.Tables[0], null) && (dsRegistrations.Tables[0].Rows.Count > 0))
                    {
                        if (dsRegistrations.Tables[0].Rows.Count == 1)
                        {
                            result = new AgentCodeByUserNameResult()
                            {
                                UserName = dsRegistrations.Tables[0].Rows[0]["username"].ToString(),
                                SalesChannelId = dsRegistrations.Tables[0].Rows[0]["SalesChannelId"].ToString(),
                                AgentCode = dsRegistrations.Tables[0].Rows[0]["AgentCode"].ToString()
                            };
                        }
                    }
                }
            }
            #endregion

            return result;
        }

        public static List<RegistrationSearchResult> RegistrationSearch(RegistrationSearchCriteria criteria)
        {
            Logger.InfoFormat("Entering {0}.RegistrationSearch({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            return StoreKeeperSearch(criteria);
        }

        public static List<RegistrationSearchResult> StoreKeeperSearch(RegistrationSearchCriteria criteria)
        {
            DateTime stDate = DateTime.Now;
            int defaultTopNo = criteria.PageSize > 0 ? criteria.PageSize : 0;
            Logger.InfoFormat("Entering {0}.StoreKeeperSearch({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            List<RegistrationSearchResult> result = new List<RegistrationSearchResult>();
            if (!ReferenceEquals(criteria.Criteria, null))
            {
                criteria.Criteria = criteria.Criteria.Equals("RS") ? "R,S" : criteria.Criteria;
            }
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GETREGISTRATIONS", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@RegID", criteria.RegID));
                sqlCmd.Parameters.Add(new SqlParameter("@IDCardNo", criteria.IDCardNo));
                sqlCmd.Parameters.Add(new SqlParameter("@QueueNo", criteria.QueueNo));
                sqlCmd.Parameters.Add(new SqlParameter("@StatusID", criteria.StatusID));
                sqlCmd.Parameters.Add(new SqlParameter("@CenterOrgID", criteria.CenterOrgID));
                sqlCmd.Parameters.Add(new SqlParameter("@MSISDN", criteria.MSISDN));
                sqlCmd.Parameters.Add(new SqlParameter("@CustName", criteria.CustName));
                sqlCmd.Parameters.Add(new SqlParameter("@RegDateFrom", criteria.RegDateFrom));
                sqlCmd.Parameters.Add(new SqlParameter("@RegDateTo", criteria.RegDateTo));
                sqlCmd.Parameters.Add(new SqlParameter("@Criteria_Type", criteria.Criteria));
                sqlCmd.Parameters.Add(new SqlParameter("@defaultTopNo", defaultTopNo));
                sqlCmd.Parameters.Add(new SqlParameter("@orgTypeViewCertain", Properties.Settings.Default.ViewCertainRoles));
                sqlCmd.Parameters.Add(new SqlParameter("@UserID", criteria.UserID));
                sqlCmd.Parameters.Add(new SqlParameter("@IsBRN", criteria.IsBRN.ToLower()));
                sqlCmd.Parameters.Add(new SqlParameter("@IsSuperAdmin", criteria.IsSuperAdmin));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();

                if (!ReferenceEquals(dsRegistrations, null))
                {
                    if (!ReferenceEquals(dsRegistrations.Tables[0], null) && (dsRegistrations.Tables[0].Rows.Count > 0))
                    {
                        for (int i = 0; i < dsRegistrations.Tables[0].Rows.Count; i++)
                        {
                            var SearchResult = new RegistrationSearchResult()
                            {
                                RegID = Convert.ToInt32(dsRegistrations.Tables[0].Rows[i]["RegID"]),
                                RegDate = Convert.ToDateTime(dsRegistrations.Tables[0].Rows[i]["RegDate"]),
                                QueueNo = dsRegistrations.Tables[0].Rows[i]["QueueNo"].ToString2(),
                                IDCardNo = dsRegistrations.Tables[0].Rows[i]["IDCardNo"].ToString2(),
                                CustomerName = dsRegistrations.Tables[0].Rows[i]["CustomerName"].ToString2(),
                                LastUpdateID = dsRegistrations.Tables[0].Rows[i]["LastAccessID"].ToString2(),
                                LastAccessID = dsRegistrations.Tables[0].Rows[i]["LastUpdateID"].ToString2(),
                                Status = dsRegistrations.Tables[0].Rows[i]["Status"].ToString2(),
                                CenterOrg = dsRegistrations.Tables[0].Rows[i]["CenterOrg"].ToString2(),
                                MSISDN = dsRegistrations.Tables[0].Rows[i]["MSISDN"].ToString2(),
                                RegType = Convert.ToInt32(dsRegistrations.Tables[0].Rows[i]["RegType"]),
                                RegTypeDescription = dsRegistrations.Tables[0].Rows[i]["RegTypeDescription"].ToString2(),
                                CriteriaType = dsRegistrations.Tables[0].Rows[i]["CriteriaType"].ToString2(),
                                FxAcctNo = dsRegistrations.Tables[0].Rows[i]["FxAcctNo"].ToString2(),
                                FxSubscrNo = dsRegistrations.Tables[0].Rows[i]["FxSubscrNo"].ToString2(),
                                FxSubScrNoResets = dsRegistrations.Tables[0].Rows[i]["FxSubScrNoResets"].ToString2(),
                                ApprovedBlacklistPerson = dsRegistrations.Tables[0].Rows[i]["ApprovedBlacklistPerson"].ToString2(),
                                CRPType = dsRegistrations.Tables[0].Rows[i]["CRPType"].ToString2(),
                                UserRoles = dsRegistrations.Tables[0].Rows[i]["UserRoles"].ToString2(),
                                IsVIP = dsRegistrations.Tables[0].Rows[i]["IsVIP"].ToString2(),
                                hasPrinDevice = dsRegistrations.Tables[0].Rows[i]["hasPrinDevice"].ToString2(),
                                PenaltyAmount = dsRegistrations.Tables[0].Rows[i]["PenaltyAmount"].ToString2(),
                                SMcontractType = dsRegistrations.Tables[0].Rows[i]["SMcontractType"].ToString2(),
                                PrinOfferId = dsRegistrations.Tables[0].Rows[i]["PrinOfferId"].ToString2().ToInt(),
                                SASupp = dsRegistrations.Tables[0].Rows[i]["SASupp"].ToString2(),
                                SimType = dsRegistrations.Tables[0].Rows[i]["SimType"].ToString2(),
                                SimReplacementType = dsRegistrations.Tables[0].Rows[i]["SimReplacementType"].ToString2(),
                                hasPrinAccessory = dsRegistrations.Tables[0].Rows[i]["hasPrinAccessory"].ToString2(),
                                hasSuppDevice = dsRegistrations.Tables[0].Rows[i]["hasSuppDevice"].ToString2(),
                                hasSuppline = dsRegistrations.Tables[0].Rows[i]["hasSuppline"].ToString2(),
                                hasSuppAccessory = dsRegistrations.Tables[0].Rows[i]["hasSuppAccessory"].ToString2(),
                                userType = dsRegistrations.Tables[0].Rows[i]["userType"].ToString2(),
                                isThirdParty = dsRegistrations.Tables[0].Rows[i]["isThirdParty"].ToString2()
                            };
                            result.Add(SearchResult);
                        }
                    }
                }
            }
            Logger.InfoFormat("Exiting {0}.StoreKeeperSearch({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
            TimeSpan timeSpn = (DateTime.Now - stDate);
            Common.LogMessage(string.Format("StoreKeeperSearch(for userid:{0}) TimeSpan:{1}", criteria.UserID, timeSpn.ToString("hh\\:mm\\:ss\\.fff")));
            return result;
        }


        //public static List<RegistrationPendingOrdersResults> PendingOrders()
        //{
        //    DateTime stDate = DateTime.Now;
        //    //int defaultTopNo = criteria.PageSize > 0 ? criteria.PageSize : 0;
        //    //Logger.InfoFormat("Entering {0}.Pending Order({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
        //    List<RegistrationPendingOrdersResults> result = new List<RegistrationPendingOrdersResults>();
        //    //if (!ReferenceEquals(criteria.Criteria, null))
        //    //{
        //    //    criteria.Criteria = criteria.Criteria.Equals("RS") ? "R,S" : criteria.Criteria;
        //    //}
        //    using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
        //    {
        //        sqlConn.Open();
        //        SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
        //        DataSet dsRegistrations = new DataSet();
        //        SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GETPENDINGORDERS", sqlConn);
        //        //sqlCmd.Parameters.Add(new SqlParameter("@UserID", criteria.UserID));
        //        sqlCmd.CommandType = CommandType.StoredProcedure;
        //        adapter.SelectCommand = sqlCmd;
        //        adapter.Fill(dsRegistrations);
        //        sqlConn.Close();

        //        if (!ReferenceEquals(dsRegistrations, null))
        //        {
        //            if (!ReferenceEquals(dsRegistrations.Tables[0], null) && (dsRegistrations.Tables[0].Rows.Count > 0))
        //            {
        //                for (int i = 0; i < dsRegistrations.Tables[0].Rows.Count; i++)
        //                {
        //                    var SearchResult = new RegistrationPendingOrdersResults()
        //                    {
        //                        RegType = Convert.ToInt32(dsRegistrations.Tables[0].Rows[i]["RegType"]),
        //                        RegID = Convert.ToInt32(dsRegistrations.Tables[0].Rows[i]["RegID"]),
        //                        RegTypeDescription = dsRegistrations.Tables[0].Rows[i]["RegTypeDescription"].ToString2(),
        //                        CustomerName = dsRegistrations.Tables[0].Rows[i]["CustomerName"].ToString2(),
        //                        IDCardNo = dsRegistrations.Tables[0].Rows[i]["IDCardNo"].ToString2(),
        //                        LastUpdateID = dsRegistrations.Tables[0].Rows[i]["LastAccessID"].ToString2(),
        //                        Status = dsRegistrations.Tables[0].Rows[i]["Status"].ToString2(),
        //                    };
        //                    result.Add(SearchResult);
        //                }
        //            }
        //        }
        //    }
        //    // Logger.InfoFormat("Exiting {0}.StoreKeeperSearch({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
        //    TimeSpan timeSpn = (DateTime.Now - stDate);
        //    //Common.LogMessage(string.Format("StoreKeeperSearch(for userid:{0}) TimeSpan:{1}", criteria.UserID, timeSpn.ToString("hh\\:mm\\:ss\\.fff")));
        //    return result;
        //}
        #endregion

        public static List<RegistrationSearchResult> TLProfileSearch(RegistrationSearchCriteria criteria)
        {
            Logger.InfoFormat("Entering {0}.RegistrationSearch({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            List<RegistrationSearchResult> result = new List<RegistrationSearchResult>();
            int defaultTopNo = criteria.PageSize > 0 ? criteria.PageSize : 0;

            var regTypeCodes = new List<string>();
            if (criteria.IsMobilePlan)
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_DevicePlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_PlanOnly);
                //added by Deepika
                regTypeCodes.Add(Properties.Settings.Default.RegType_SupplementaryPlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_SecondaryPlan);

                regTypeCodes.Add(Properties.Settings.Default.RegType_NewLine);
                //Added by Patanjali for MNP
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPPlanOnly);
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPSupplementaryPlan);
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPNewLine);
                //Added by Patanjali for MNP Ends here

                regTypeCodes.Add(Properties.Settings.Default.RegType_AddRemoveVAS);
                regTypeCodes.Add(Properties.Settings.Default.Sim_Replacement_RegType);//Added By Himansu
                regTypeCodes.Add(Properties.Settings.Default.RegType_CRP); // Added by Kotesh
                regTypeCodes.Add(Properties.Settings.Default.RegType_DeviceCRP); // Added by KiranG
                ///ADDED BY SUTAN DAN TO GET MULTIPLE SUPPLEMENTARY LINES FOR A PRINCIPLE LINE MNP ONLY
                regTypeCodes.Add(Properties.Settings.Default.RegType_MNPPlanWithMultiSuppline);
                regTypeCodes.Add(Properties.Settings.Default.RegType_DeviceSale);
            }
            else
            {
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomePersonal);
                regTypeCodes.Add(Properties.Settings.Default.RegType_HomeCorporate);
            }

            using (var context = new OnlineStoreDbContext())
            {
                var userOrgID = context.Users.Single(a => a.ID == criteria.UserID).OrgID;
                var userOrgTypeID = context.Organizations.Single(a => a.ID == userOrgID).OrgTypeID;
                var userOrgTypeCode = context.OrgTypes.Single(a => a.ID == userOrgTypeID).Code;
                criteria.CenterOrgID = userOrgID;
                var orgTypeViewCertain = Properties.Settings.Default.ViewCertainRoles.Split(',').ToList();

                if (criteria != null)
                {
                    var query = from registration in context.Registrations
                                join cust in context.Customers on
                                registration.CustomerID equals cust.ID //into custs
                                join regStatus in context.RegStatuses on
                                new { regID = registration.ID, active = true } equals new { regID = regStatus.RegID, active = regStatus.Active } //into regStatuses
                                join status in context.Statuses on
                                regStatus.StatusID equals status.ID
                                join organization in context.Organizations on
                                registration.CenterOrgID equals organization.ID

                                join regType in
                                    (from rt in context.RegTypes
                                     where regTypeCodes.Contains(rt.Code)
                                     select new { ID = rt.ID }) on registration.RegTypeID equals regType.ID

                                join regTypes in context.RegTypes on
                                registration.RegTypeID equals regTypes.ID
                                where (registration.Trn_Type == "R" || registration.Trn_Type == "S")
                                select new
                                {

                                    Reg = registration,
                                    Cust = cust,
                                    Status = status,
                                    RegStatus = regStatus,
                                    Organization = organization,
                                    RegTypes = regTypes

                                };



                    if (orgTypeViewCertain.Contains(userOrgTypeCode))
                    {
                        query = query.Where(a => a.Reg.CenterOrgID == userOrgID);
                    }

                    if (!string.IsNullOrEmpty(criteria.RegID))
                    {
                        var regID = criteria.RegID.ToInt();
                        query = query.Where(a => a.Reg.ID == regID);
                    }

                    if (!string.IsNullOrEmpty(criteria.IDCardNo))
                        query = query.Where(a => a.Cust.IDCardNo == criteria.IDCardNo);

                    if (!string.IsNullOrEmpty(criteria.QueueNo))
                        query = query.Where(a => a.Reg.QueueNo == criteria.QueueNo);

                    if (criteria.StatusID != 0)
                        query = query.Where(a => a.Status.ID == criteria.StatusID);

                    if (criteria.CenterOrgID.HasValue)
                        query = query.Where(a => a.Reg.CenterOrgID == criteria.CenterOrgID);

                    if (!string.IsNullOrEmpty(criteria.MSISDN))
                        query = query.Where(a => a.Cust.ContactNo == criteria.MSISDN);

                    if (!string.IsNullOrEmpty(criteria.CustName))
                        query = query.Where(a => a.Cust.FullName.Contains(criteria.CustName));

                    if (criteria.IsBRN.ToString2().ToLower().Equals("true"))
                    {
                        query = query.Where(a => a.Cust.CorpMasterID != null && a.Cust.CorpParentID != null);
                    }

                    if (criteria.RegDateFrom.HasValue)
                    {
                        var regDTFrom = criteria.RegDateFrom.Value.Date;
                        query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) >= EntityFunctions.TruncateTime(regDTFrom));
                    }

                    if (criteria.RegDateTo.HasValue)
                    {
                        var regDTTo = criteria.RegDateTo.Value.Date;
                        query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) <= EntityFunctions.TruncateTime(regDTTo));
                    }

                    if (criteria.Criteria == null)
                        criteria.Criteria = "R";
                    if (criteria.Criteria != null)
                    {

                        if (criteria.Criteria == "R")// mobility
                        {
                            query = query.Where(a => a.Reg.Trn_Type == "R");
                        }
                        else if (criteria.Criteria == "S")
                        {
                            query = query.Where(a => a.Reg.Trn_Type == "S");
                        }
                        else
                        {
                            // no need default selection   
                        }
                    }

                    if (criteria.SearchType.ToString2().ToUpper() == "R")
                    {
                        if (defaultTopNo > 0)
                        {
                            query = query.OrderByDescending(a => a.Reg.ID).Take(defaultTopNo);
                        }
                    }
                    else
                    {

                        DateTime dtPrevDt = DateTime.Today.AddDays(-1).Date;
                        query = query.Where(a => EntityFunctions.TruncateTime(a.Reg.CreateDT) == DateTime.Today.Date || EntityFunctions.TruncateTime(a.Reg.CreateDT) == dtPrevDt);

                    }

                    var temp = query.Select(a => new
                    {
                        RegID = a.Reg.ID,
                        RegDate = a.Reg.CreateDT,
                        QueueNo = a.Reg.QueueNo,
                        IDCardNo = a.Cust.IDCardNo,
                        CustName = a.Cust.FullName,
                        Status = a.Status.Description,
                        MSISDN = a.Cust.ContactNo,
                        CenterOrg = a.Organization.Name,
                        LastAccessID = a.Reg.LastAccessID != null ? a.Reg.LastAccessID : string.Empty,
                        LastUpdateID = a.RegStatus.LastAccessID != null ? a.RegStatus.LastAccessID : string.Empty,
                        regType = a.Reg.RegTypeID,
                        regTypes = a.RegTypes.Description,
                        criteriaType = a.Reg.Trn_Type,
                        fxAcctNo = a.Reg.fxAcctNo,
                        fxSubscrNo = a.Reg.fxSubscrNo,
                        fxSubScrNoResets = a.Reg.fxSubScrNoResets,
                        ApprovedBlacklistPerson = a.Reg.ApprovedBlacklistPerson


                    }).Distinct();

                    result = temp.Select(a => new RegistrationSearchResult()
                    {
                        RegID = a.RegID,
                        RegDate = a.RegDate,
                        QueueNo = a.QueueNo,
                        IDCardNo = a.IDCardNo,
                        CustomerName = a.CustName,
                        Status = a.Status,
                        MSISDN = a.MSISDN,
                        CenterOrg = a.CenterOrg,
                        LastAccessID = a.LastAccessID,
                        LastUpdateID = a.LastUpdateID,
                        RegType = a.regType,
                        RegTypeDescription = a.regTypes,
                        CriteriaType = a.criteriaType,
                        FxAcctNo = a.fxAcctNo,
                        FxSubscrNo = a.fxSubscrNo,
                        FxSubScrNoResets = a.fxSubScrNoResets,
                        ApprovedBlacklistPerson = a.ApprovedBlacklistPerson

                    }).ToList();
                }
            }

            Logger.InfoFormat("Exiting {0}.RegistrationSearch({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
            return result;
        }

        public static DeviceInfo GetRegDeviceInfo(int regId)
        {
            Logger.InfoFormat("Entering {0}.RegDeviceInfo()", svcName);
            var regDeviceInfo = new DeviceInfo();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                try
                {
                    regDeviceInfo = ctx.DeviceInfos.Where(e => e.RegId == regId).ToList().FirstOrDefault();
                }
                catch (Exception ex)
                {

                }
            }

            return regDeviceInfo;
        }
        public static DeviceInfo UpdateRegDeviceInfo(int regId, string salesID, bool isFTTH)
        {
            Logger.InfoFormat("Entering {0}.RegDeviceInfo()", svcName);
            var regDeviceInfo = new DeviceInfo();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                try
                {
                    regDeviceInfo = ctx.DeviceInfos.Where(e => e.RegId == regId).ToList().FirstOrDefault();
                    if (isFTTH)
                        regDeviceInfo.FTTHSalesID = salesID;
                    else
                        regDeviceInfo.VOIPSalesID = salesID;
                    ctx.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }

            return regDeviceInfo;
        }

        public static List<FxDependChkComps> GetAllComponentsbyPlanId(int PlanID, string mandaotryComps, string linkType)
        {
            List<FxDependChkComps> Comps = new List<FxDependChkComps>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Comps = ctx.Database.SqlQuery<FxDependChkComps>("USP_GetAllComponentsbyPlanId @packageId,@strComponents,@linkType", new SqlParameter("@packageId", PlanID), new SqlParameter("@strComponents", mandaotryComps), new SqlParameter("@linkType", linkType)).ToList();
            }

            return Comps;
        }

        public static List<FxDependChkComps> GetAllComponentsbySecondaryPlanId(int PlanID, string mandaotryComps)
        {
            List<FxDependChkComps> Comps = new List<FxDependChkComps>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                Comps = ctx.Database.SqlQuery<FxDependChkComps>("USP_GetAllComponentsbySecondaryPlanId @packageId,@strComponents", new SqlParameter("@packageId", PlanID), new SqlParameter("@strComponents", mandaotryComps)).ToList();
            }

            return Comps;
        }

        public static int BlackBerryDiscount(int deviceID, int planID)
        {
            List<int> obj = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                obj = ctx.Database.SqlQuery<int>("usp_BlackBerryDiscount @DevieID,@PlanID", new SqlParameter("@DevieID", deviceID), new SqlParameter("@PlanID", planID)).ToList();
            }

            return obj.SingleOrDefault();
        }
        public static int GetBBAdvancePrice(int planID, int modelId, string uomCode = "")
        {
            List<int> obj = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                obj = ctx.Database.SqlQuery<int>("USP_GetBBAdvancePrice @UOMCode,@PlanID,@ModelId", new SqlParameter("@UOMCode", uomCode), new SqlParameter("@PlanID", planID), new SqlParameter("@ModelId", modelId)).ToList();
            }

            return obj.SingleOrDefault();
        }


        #endregion

        #region CustomerInfo
        public static int CustomerInfoCreate(CustomerInfo oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerInfoCreate(ID: {1})", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.CustomersInfo.Add(oReq);
                ctx.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.CustomerInfoCreate(ID: {1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL", value);
            return value;

        }


		public static int CustomerUpdate(Customer oReq)
		{
			Logger.InfoFormat("Entering {0}.CustomerUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
			var value = 0;

			if (oReq != null)
			{
				using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
				{
					var _cust = ctx.Customers.Find(oReq.ID);
					_cust.ID = oReq.ID;
					if (oReq.PayModeID != 0)
						_cust.PayModeID = oReq.PayModeID;

					if (oReq.CardTypeID != null)
						_cust.CardTypeID = oReq.CardTypeID;

					if (!string.IsNullOrEmpty(oReq.NameOnCard))
						_cust.NameOnCard = oReq.NameOnCard;

					if (!string.IsNullOrEmpty(oReq.CardNo))
						_cust.CardNo = oReq.CardNo;

					if (!string.IsNullOrEmpty(oReq.CardExpiryDate))
						_cust.CardExpiryDate = oReq.CardExpiryDate;

					value = ctx.SaveChanges();
				}
			}

			Logger.InfoFormat("Exiting {0}.AddressUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
			return value;
		}

        public static int CustomerInfoUpdate(CustomerInfo oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerInfoUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var custInfo = ctx.CustomersInfo.Find(oReq.ID);
                    custInfo.ID = oReq.ID;
                    custInfo.PayModeID = oReq.PayModeID;
                    custInfo.CardTypeID = oReq.CardTypeID;
                    custInfo.NameOnCard = oReq.NameOnCard;
                    custInfo.CardNo = oReq.CardNo;
                    custInfo.CardExpiryDate = oReq.CardExpiryDate;
                    custInfo.Active = oReq.Active;
                    custInfo.LastAccessID = oReq.LastAccessID;
                    custInfo.CreateDT = oReq.CreateDT;
                    custInfo.Amount = oReq.Amount;
                    custInfo.OrderID = oReq.OrderID;
                    custInfo.CVV = oReq.CVV;
                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.AddressUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }

        #endregion

        #region Reg Address

        public static int AddressCreate(Address oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressCreate(RegID: {1})", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Addresses.Add(oReq);
                ctx.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.AddressCreate(RegID: {1}): {2}", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL", value);
            return value;
        }
        public static int AddressUpdate(Address oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var addr = ctx.Addresses.Find(oReq.ID);
                    addr.ID = oReq.ID;
                    addr.RegID = oReq.RegID;
                    addr.StateID = oReq.StateID;
                    addr.CountryID = oReq.CountryID;
                    addr.UnitNo = oReq.UnitNo;
                    addr.Street = oReq.Street;
                    addr.BuildingNo = oReq.BuildingNo;
                    addr.Line1 = oReq.Line1;
                    addr.Line2 = oReq.Line2;
                    addr.Town = oReq.Town;
                    addr.Postcode = oReq.Postcode;
                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.AddressUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }

        public static Address GetAddressByRegistrationID(int id)
        {
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                return ctx.Addresses.Where(a => a.RegID != id).FirstOrDefault();
            }
        }

        public static RegPgmBdlPkgComp GetPlanByRegistrationID(int id)
        {
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                return ctx.RegPgmBdlPkgComps.Where(a => a.RegID != id).FirstOrDefault();
            }
        }

        public static Customer GetCustomerFromCustId(int id)
        {
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                return ctx.Customers.Where(a => a.ID != id).FirstOrDefault();
            }
        }

        public static List<int> AddressFind(AddressFind oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Addresses.Where(a => a.ID != 0);

                if (!(oReq.Address.ID == 0))
                    query = query.Where(a => a.ID == oReq.Address.ID);

                if (!(oReq.Address.RegID == 0))
                    query = query.Where(a => a.RegID == oReq.Address.RegID);

                if (!(oReq.Address.AddressTypeID == 0))
                    query = query.Where(a => a.AddressTypeID == oReq.Address.AddressTypeID);

                if (!(oReq.Address.StateID == 0))
                    query = query.Where(a => a.StateID == oReq.Address.StateID);

                if (!(oReq.Address.CountryID == 0))
                    query = query.Where(a => a.CountryID == oReq.Address.CountryID);

                if (!(oReq.Address.VersionNo == 0))
                    query = query.Where(a => a.VersionNo == oReq.Address.VersionNo);

                if (!string.IsNullOrEmpty(oReq.Address.UnitNo))
                    query = query.Where(a => a.UnitNo.Contains(oReq.Address.UnitNo));

                if (!string.IsNullOrEmpty(oReq.Address.Street))
                    query = query.Where(a => a.Street.Contains(oReq.Address.Street));

                if (!string.IsNullOrEmpty(oReq.Address.BuildingNo))
                    query = query.Where(a => a.BuildingNo.Contains(oReq.Address.BuildingNo));

                if (!string.IsNullOrEmpty(oReq.Address.Line1))
                    query = query.Where(a => a.Line1.Contains(oReq.Address.Line1));

                if (!string.IsNullOrEmpty(oReq.Address.Line2))
                    query = query.Where(a => a.Line2.Contains(oReq.Address.Line2));

                if (!string.IsNullOrEmpty(oReq.Address.Town))
                    query = query.Where(a => a.Town.Contains(oReq.Address.Town));

                if (!string.IsNullOrEmpty(oReq.Address.Postcode))
                    query = query.Where(a => a.Postcode.Contains(oReq.Address.Postcode));

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.AddressFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<Address> AddressGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.AddressGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<Address> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Addresses.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.AddressGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<Address> AddressGet1(AddressFind oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressGet1({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<Address> result = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var value = ctx.Addresses.Where(a => a.ID != 0);

                if (!(oReq.Address.ID == 0))
                    value = value.Where(a => a.ID == oReq.Address.ID);

                if (!(oReq.Address.RegID == 0))
                    value = value.Where(a => a.RegID == oReq.Address.RegID);

                if (!(oReq.Address.AddressTypeID == 0))
                    value = value.Where(a => a.AddressTypeID == oReq.Address.AddressTypeID);

                if (!(oReq.Address.StateID == 0))
                    value = value.Where(a => a.StateID == oReq.Address.StateID);

                if (!(oReq.Address.CountryID == 0))
                    value = value.Where(a => a.CountryID == oReq.Address.CountryID);

                if (!(oReq.Address.VersionNo == 0))
                    value = value.Where(a => a.VersionNo == oReq.Address.VersionNo);

                if (!string.IsNullOrEmpty(oReq.Address.UnitNo))
                    value = value.Where(a => a.UnitNo.Contains(oReq.Address.UnitNo));

                if (!string.IsNullOrEmpty(oReq.Address.Street))
                    value = value.Where(a => a.Street.Contains(oReq.Address.Street));

                if (!string.IsNullOrEmpty(oReq.Address.BuildingNo))
                    value = value.Where(a => a.BuildingNo.Contains(oReq.Address.BuildingNo));

                if (!string.IsNullOrEmpty(oReq.Address.Line1))
                    value = value.Where(a => a.Line1.Contains(oReq.Address.Line1));

                if (!string.IsNullOrEmpty(oReq.Address.Line2))
                    value = value.Where(a => a.Line2.Contains(oReq.Address.Line2));

                if (!string.IsNullOrEmpty(oReq.Address.Town))
                    value = value.Where(a => a.Town.Contains(oReq.Address.Town));

                if (!string.IsNullOrEmpty(oReq.Address.Postcode))
                    value = value.Where(a => a.Postcode.Contains(oReq.Address.Postcode));

                if (oReq.Active.HasValue)
                    value = value.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    value = value.OrderByDescending(a => a.ID).Take(oReq.PageSize);
                result = value.ToList();
            }

            Logger.InfoFormat("Exiting {0}.AddressGet1({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", result.Count());
            return result;
        }
        #endregion

        #region Customer
        public static List<int> CustomerFind(CustomerFind oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Customers.Where(a => a.ID != 0);

                if (!(oReq.Customer.ID == 0))
                    query = query.Where(a => a.ID == oReq.Customer.ID);

                if (!(oReq.Customer.IDCardTypeID == 0))
                    query = query.Where(a => a.IDCardTypeID == oReq.Customer.IDCardTypeID);

                if (!(oReq.Customer.PayModeID == 0 || oReq.Customer.PayModeID == null))
                    query = query.Where(a => a.PayModeID == oReq.Customer.PayModeID);

                if (!(oReq.Customer.CardTypeID == 0 || oReq.Customer.CardTypeID == null))
                    query = query.Where(a => a.CardTypeID == oReq.Customer.CardTypeID);

                if (!string.IsNullOrEmpty(oReq.Customer.FullName))
                    query = query.Where(a => a.FullName.Contains(oReq.Customer.FullName));

                if (!string.IsNullOrEmpty(oReq.Customer.IDCardNo))
                    query = query.Where(a => a.IDCardNo == oReq.Customer.IDCardNo);

                if (!string.IsNullOrEmpty(oReq.Customer.ContactNo))
                    query = query.Where(a => a.ContactNo.Contains(oReq.Customer.ContactNo));

                if (!string.IsNullOrEmpty(oReq.Customer.AlternateContactNo))
                    query = query.Where(a => a.AlternateContactNo.Contains(oReq.Customer.AlternateContactNo));

                if (!string.IsNullOrEmpty(oReq.Customer.PostpaidMobileNo))
                    query = query.Where(a => a.PostpaidMobileNo.Contains(oReq.Customer.PostpaidMobileNo));

                if (!string.IsNullOrEmpty(oReq.Customer.EmailAddr))
                    query = query.Where(a => a.EmailAddr.Contains(oReq.Customer.EmailAddr));

                if (!string.IsNullOrEmpty(oReq.Customer.NameOnCard))
                    query = query.Where(a => a.NameOnCard.Contains(oReq.Customer.NameOnCard));

                if (!string.IsNullOrEmpty(oReq.Customer.CardNo))
                    query = query.Where(a => a.CardNo.Contains(oReq.Customer.CardNo));

                if (!string.IsNullOrEmpty(oReq.Customer.CardExpiryDate))
                    query = query.Where(a => a.CardExpiryDate.Contains(oReq.Customer.CardExpiryDate));

                if (!string.IsNullOrEmpty(oReq.Customer.BizRegName))
                    query = query.Where(a => a.BizRegName.Contains(oReq.Customer.BizRegName));

                if (!string.IsNullOrEmpty(oReq.Customer.BizRegNo))
                    query = query.Where(a => a.BizRegName.Contains(oReq.Customer.BizRegNo));

                if (!string.IsNullOrEmpty(oReq.Customer.SignatoryName))
                    query = query.Where(a => a.SignatoryName.Contains(oReq.Customer.SignatoryName));

                if (!string.IsNullOrEmpty(oReq.Customer.PersonInCharge))
                    query = query.Where(a => a.PersonInCharge.Contains(oReq.Customer.PersonInCharge));

                if (oReq.DateOfBirthFrom.HasValue)
                {
                    var dobFrom = oReq.DateOfBirthFrom.Value.Date;
                    query = query.Where(a => a.DateOfBirth.Date >= dobFrom.Date);
                }

                if (oReq.DateOfBirthFrom.HasValue)
                {
                    var dobTo = oReq.DateOfBirthFrom.Value.Date;
                    query = query.Where(a => a.DateOfBirth.Date >= dobTo.Date);
                }

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.CustomerFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<Customer> CustomerGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.CustomerGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<Customer> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Customers.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.CustomerGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<Customer> CustomerGet1(CustomerFind oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerGet1({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<Customer> result = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var value = ctx.Customers.Where(a => a.ID != 0);

                if (!(oReq.Customer.ID == 0))
                    value = value.Where(a => a.ID == oReq.Customer.ID);

                if (!(oReq.Customer.IDCardTypeID == 0))
                    value = value.Where(a => a.IDCardTypeID == oReq.Customer.IDCardTypeID);

                if (!(oReq.Customer.PayModeID == 0 || oReq.Customer.PayModeID == null))
                    value = value.Where(a => a.PayModeID == oReq.Customer.PayModeID);

                if (!(oReq.Customer.CardTypeID == 0 || oReq.Customer.CardTypeID == null))
                    value = value.Where(a => a.CardTypeID == oReq.Customer.CardTypeID);

                if (!string.IsNullOrEmpty(oReq.Customer.FullName))
                    value = value.Where(a => a.FullName.Contains(oReq.Customer.FullName));

                if (!string.IsNullOrEmpty(oReq.Customer.IDCardNo))
                    value = value.Where(a => a.IDCardNo == oReq.Customer.IDCardNo);

                if (!string.IsNullOrEmpty(oReq.Customer.ContactNo))
                    value = value.Where(a => a.ContactNo.Contains(oReq.Customer.ContactNo));

                if (!string.IsNullOrEmpty(oReq.Customer.AlternateContactNo))
                    value = value.Where(a => a.AlternateContactNo.Contains(oReq.Customer.AlternateContactNo));

                if (!string.IsNullOrEmpty(oReq.Customer.PostpaidMobileNo))
                    value = value.Where(a => a.PostpaidMobileNo.Contains(oReq.Customer.PostpaidMobileNo));

                if (!string.IsNullOrEmpty(oReq.Customer.EmailAddr))
                    value = value.Where(a => a.EmailAddr.Contains(oReq.Customer.EmailAddr));

                if (!string.IsNullOrEmpty(oReq.Customer.NameOnCard))
                    value = value.Where(a => a.NameOnCard.Contains(oReq.Customer.NameOnCard));

                if (!string.IsNullOrEmpty(oReq.Customer.CardNo))
                    value = value.Where(a => a.CardNo.Contains(oReq.Customer.CardNo));

                if (!string.IsNullOrEmpty(oReq.Customer.CardExpiryDate))
                    value = value.Where(a => a.CardExpiryDate.Contains(oReq.Customer.CardExpiryDate));

                if (!string.IsNullOrEmpty(oReq.Customer.BizRegName))
                    value = value.Where(a => a.BizRegName.Contains(oReq.Customer.BizRegName));

                if (!string.IsNullOrEmpty(oReq.Customer.BizRegNo))
                    value = value.Where(a => a.BizRegName.Contains(oReq.Customer.BizRegNo));

                if (!string.IsNullOrEmpty(oReq.Customer.SignatoryName))
                    value = value.Where(a => a.SignatoryName.Contains(oReq.Customer.SignatoryName));

                if (!string.IsNullOrEmpty(oReq.Customer.PersonInCharge))
                    value = value.Where(a => a.PersonInCharge.Contains(oReq.Customer.PersonInCharge));

                if (oReq.DateOfBirthFrom.HasValue)
                {
                    var dobFrom = oReq.DateOfBirthFrom.Value.Date;
                    value = value.Where(a => a.DateOfBirth.Date >= dobFrom.Date);
                }

                if (oReq.DateOfBirthFrom.HasValue)
                {
                    var dobTo = oReq.DateOfBirthFrom.Value.Date;
                    value = value.Where(a => a.DateOfBirth.Date >= dobTo.Date);
                }

                if (oReq.Active.HasValue)
                    value = value.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    value = value.OrderByDescending(a => a.ID).Take(oReq.PageSize);
                result = value.ToList();
            }

            Logger.InfoFormat("Exiting {0}.CustomerGet1({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", result.Count());
            return result;
        }

        #endregion

        #region Reg Model Group Model
        public static List<int> RegMdlGrpModelFind(RegMdlGrpModelFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegMdlGrpModelFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.RegMdlGrpModels.Where(a => a.ID != 0);

                if (!(oReq.RegMdlGrpModel.ID == 0))
                    query = query.Where(a => a.ID == oReq.RegMdlGrpModel.ID);

                if (!(oReq.RegMdlGrpModel.RegID == 0))
                    query = query.Where(a => a.RegID == oReq.RegMdlGrpModel.RegID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                if (oReq.IsPrincipal ?? false || oReq.IsPrincipal ==null)
                    query = query.Where(a => a.SuppLineID == null);
                else
                    query = query.Where(a => a.SuppLineID != null);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegMdlGrpModelFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<int> RegMdlGrpModelSecFind(RegMdlGrpModelSecFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegMdlGrpModelSecFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.RegMdlGrpModelSecs.Where(a => a.ID != 0);

                if (!(oReq.RegMdlGrpModelSec.ID == 0))
                    query = query.Where(a => a.ID == oReq.RegMdlGrpModelSec.ID);

                if (!(oReq.RegMdlGrpModelSec.SecRegID == 0))
                    query = query.Where(a => a.SecRegID == oReq.RegMdlGrpModelSec.SecRegID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegMdlGrpModelSecFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<RegMdlGrpModel> RegMdlGrpModelGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.RegMdlGrpModelGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<RegMdlGrpModel> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegMdlGrpModels.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegMdlGrpModelGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

		public static List<RegAttributes> RegAttributesGetAll(RegAttributeReq oReq)
		{
			Logger.InfoFormat("Entering RegAtrributesFind()");
			List<RegAttributes> result = null;

			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				using (var trans = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
					{
						IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
					}))
				{
					if (oReq != null)
					{
						var value = ctx.RegAttributes.Where(a => a.ID != 0);

						if (oReq.RegAttributes.RegID != null && oReq.RegAttributes.RegID > 0)
							value = value.Where(s => s.RegID == oReq.RegAttributes.RegID);

						if (oReq.RegAttributes.SuppLineID != null && oReq.RegAttributes.SuppLineID > 0)
							value = value.Where(s => s.SuppLineID == oReq.RegAttributes.SuppLineID);

						if (!string.IsNullOrEmpty(oReq.RegAttributes.ATT_Name))
							value = value.Where(s => s.ATT_Name == oReq.RegAttributes.ATT_Name);

						result = value.ToList();
					}
				}
			}

			Logger.InfoFormat("Exiting RegAtrributesFind()");
			return result;
		}

		public static List<RegAttributes> RegAttributesGetByRegID(int _regID)
		{
			Logger.InfoFormat("Entering {0}.RegAtrributesGetByRegID({1})", svcName, (_regID != null) ? _regID.ToString2() : "NULL");
			List<RegAttributes> result = null;

			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				if (_regID > 0)
				{
					var value = ctx.RegAttributes.Where(a => a.RegID == _regID);
					result = value.ToList();
				}
			}
			Logger.InfoFormat("Exiting {0}.RegAtrributesGetByRegID({1})", svcName, (_regID != null) ? _regID.ToString2() : "NULL");
			return result;
		}

		public static List<RegAttributes> RegAttributesGetBySuppLineID(int _suppLineID)
		{
			Logger.InfoFormat("Entering {0}.RegAtrributesGetBySuppLineID({1})", svcName, (_suppLineID != null) ? _suppLineID.ToString2() : "NULL");
			List<RegAttributes> result = null;

			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				if (_suppLineID > 0)
				{
					var value = ctx.RegAttributes.Where(a => a.SuppLineID == _suppLineID);
					result = value.ToList();
				}
			}
			Logger.InfoFormat("Exiting {0}.RegAtrributesGetBySuppLineID({1})", svcName, (_suppLineID != null) ? _suppLineID.ToString2() : "NULL");
			return result;
		}



        public static List<RegMdlGrpModel> RegMdlGrpModelGet1(RegMdlGrpModelFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegMdlGrpModelGet1({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<RegMdlGrpModel> result = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var value = ctx.RegMdlGrpModels.Where(a => a.ID != 0);

                if (!(oReq.RegMdlGrpModel.ID == 0))
                    value = value.Where(a => a.ID == oReq.RegMdlGrpModel.ID);

                if (!(oReq.RegMdlGrpModel.RegID == 0))
                    value = value.Where(a => a.RegID == oReq.RegMdlGrpModel.RegID);

                if (oReq.Active.HasValue)
                    value = value.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    value = value.OrderByDescending(a => a.ID).Take(oReq.PageSize);
                result = value.ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegMdlGrpModelGet1({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", result.Count());
            return result;
        }
        public static List<RegMdlGrpModelSec> RegMdlGrpModelSecGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.RegMdlGrpModelSecGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<RegMdlGrpModelSec> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegMdlGrpModelSecs.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegMdlGrpModelSecGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        #endregion

        #region Reg PgmBdlPkgComp
        public static List<int> RegPgmBdlPkgCompFind(RegPgmBdlPkgCompFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.RegPgmBdlPkgComps.Where(a => a.ID != 0);

                if (!(oReq.RegPgmBdlPkgComp.ID == 0))
                    query = query.Where(a => a.ID == oReq.RegPgmBdlPkgComp.ID);

                if (!(oReq.RegPgmBdlPkgComp.RegID == 0))
                    query = query.Where(a => a.RegID == oReq.RegPgmBdlPkgComp.RegID);

                if (oReq.RegPgmBdlPkgComp.RegSuppLineID.HasValue)
                    query = query.Where(a => a.RegSuppLineID == oReq.RegPgmBdlPkgComp.RegSuppLineID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<int> RegPgmBdlPkgCompSecFind(RegPgmBdlPkgCompSecFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompSecFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.RegPgmBdlPkgCompSecs.Where(a => a.ID != 0);

                if (!(oReq.RegPgmBdlPkgCompSec.ID == 0))
                    query = query.Where(a => a.ID == oReq.RegPgmBdlPkgCompSec.ID);

                if (!(oReq.RegPgmBdlPkgCompSec.RegID == 0))
                    query = query.Where(a => a.RegID == oReq.RegPgmBdlPkgCompSec.RegID);

                if (oReq.RegPgmBdlPkgCompSec.RegSuppLineID.HasValue)
                    query = query.Where(a => a.RegSuppLineID == oReq.RegPgmBdlPkgCompSec.RegSuppLineID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegPgmBdlPkRegPgmBdlPkgCompSecFindgCompFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        //Begin Added by VLT on 11 Apr 2013
        public static List<int> RegPgmBdlPkgComponentFind(RegPgmBdlPkgCompFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.RegPgmBdlPkgComps.Where(a => a.ID != 0);

                if (!(oReq.RegPgmBdlPkgComp.ID == 0))
                    query = query.Where(a => a.ID == oReq.RegPgmBdlPkgComp.ID);

                if (!(oReq.RegPgmBdlPkgComp.RegID == 0))
                    query = query.Where(a => a.RegID == oReq.RegPgmBdlPkgComp.RegID || a.RegSuppLineID == oReq.RegPgmBdlPkgComp.RegSuppLineID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.PgmBdlPckComponentID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        //End Added by VLT on 11 Apr 2013
        public static List<RegPgmBdlPkgComp> RegPgmBdlPkgCompGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<RegPgmBdlPkgComp> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegPgmBdlPkgComps.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<RegPgmBdlPkgComp> RegPgmBdlPkgCompGet1(RegPgmBdlPkgCompFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompGet1({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<RegPgmBdlPkgComp> result = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var value = ctx.RegPgmBdlPkgComps.Where(a => a.ID != 0);

                if (!(oReq.RegPgmBdlPkgComp.ID == 0))
                    value = value.Where(a => a.ID == oReq.RegPgmBdlPkgComp.ID);

                if (!(oReq.RegPgmBdlPkgComp.RegID == 0))
                    value = value.Where(a => a.RegID == oReq.RegPgmBdlPkgComp.RegID);

                if (oReq.RegPgmBdlPkgComp.RegSuppLineID.HasValue)
                    value = value.Where(a => a.RegSuppLineID == oReq.RegPgmBdlPkgComp.RegSuppLineID);

                if (oReq.Active.HasValue)
                    value = value.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    value = value.OrderByDescending(a => a.ID).Take(oReq.PageSize);
                result = value.ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompGet1({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", result.Count());
            return result;
        }

        public static List<RegPgmBdlPkgCompSec> RegPgmBdlPkgCompSecGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompSecGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<RegPgmBdlPkgCompSec> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegPgmBdlPkgCompSecs.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompSecGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        //Begin Added by VLT on 11 Apr 2013
        public static List<PgmBdlPckComponent> RegPgmBdlPkgComponentGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<PgmBdlPckComponent> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PgmBdlPckComponents.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }
        //End Added by VLT on 11 Apr 2013
        #endregion

        #region Reg Status

        public static int RegStatusCreate(RegStatus oReq, DAL.Models.Registration reg)
        {
            Logger.InfoFormat("Entering {0}.RegStatusCreate({1})", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL");
            var value = 0;

            //Only for Staging TODO:Remove
            Logger.InfoFormat("Via SP ", (oReq != null) ? oReq.RegID.ToString2() : "NULL");
            //using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            //{
            //    using (var trans = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            //        {
            //            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            //        }))
            //    {
            //        // Create reg status
            //        var oldRegStatus =
            //            ctx.RegStatuses.FirstOrDefault(a => a.RegID == oReq.RegID && a.Active == true);
            //        oldRegStatus.Active = false;
            //        oldRegStatus.EndDate = DateTime.Now;
            //        ctx.SaveChanges();
            //        trans.Complete();
            //        trans.Dispose();
            //    }
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsRegistrations = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_UpdateStatus", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@RegID", oReq.RegID));
                sqlCmd.Parameters.Add(new SqlParameter("@Active", oReq.Active));
                sqlCmd.Parameters.Add(new SqlParameter("@CreateDT", oReq.CreateDT));
                sqlCmd.Parameters.Add(new SqlParameter("@StartDate", oReq.StartDate));
                sqlCmd.Parameters.Add(new SqlParameter("@LastAccessID", oReq.LastAccessID));
                sqlCmd.Parameters.Add(new SqlParameter("@StatusID", oReq.StatusID));
                sqlCmd.Parameters.Add(new SqlParameter("@LastUpdateDT", DateTime.Now));

                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandTimeout = 180;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsRegistrations);
                sqlConn.Close();
                //sqlCmd.CommandTimeout = 90000;

            }
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    //Moved to SP (USP_MOBILITY_UpdateStatus)
                    //ctx.RegStatuses.Add(oReq);
                    //ctx.SaveChanges();

                    // Update registration
                    if (reg.ModemID != null)
                    {
                        var registration = ctx.Registrations.Find(reg.ID);

                        registration.LastUpdateDT = DateTime.Now;
                        registration.LastAccessID = oReq.LastAccessID;
                        registration.ModemID = reg.ModemID;
                        registration.KenanAccountNo = reg.KenanAccountNo;
                        ctx.SaveChanges();
                    }
                    trans.Complete();
                }
            }
        

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.RegStatusCreate({1}): {2}", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL", value);
            return value;
        }
        public static int RegStatusCreate(RegStatus oReq)
        {
            Logger.InfoFormat("Entering {0}.RegStatusCreate({1})", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                {
                    var oldRegStatus =
                        ctx.RegStatuses.Where(a => a.RegID == oReq.RegID && a.Active == true).SingleOrDefault();
                    if (oldRegStatus != null)
                    {
                        oldRegStatus.Active = false;
                        oldRegStatus.EndDate = DateTime.Now;
                        ctx.SaveChanges();
                    }

                    trans.Complete();
                }
                using (var trans = new TransactionScope())
                {
                    ctx.RegStatuses.Add(oReq);
                    ctx.SaveChanges();

                    trans.Complete();
                }
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.RegStatusCreate({1}): {2}", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL", value);
            return value;
        }
		public static int regAcctCreationCreate(int _regID, RegAccount _regAcct)
		{
			try
			{
				using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
				{
					sqlConn.Open();
					SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
					DataSet dsRegistrations = new DataSet();
					SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_LnkRegAcctCreate", sqlConn);
					sqlCmd.Parameters.Add(new SqlParameter("@RegID", _regID));
					sqlCmd.Parameters.Add(new SqlParameter("@KenanCreateAccountDT", _regAcct.KenanCreateAccountDT));
					sqlCmd.Parameters.Add(new SqlParameter("@KenanAccountExternalNo", _regAcct.KenanAccountExternalNo));
					sqlCmd.Parameters.Add(new SqlParameter("@KenanAccountInternalNo", _regAcct.KenanAccountInternalNo));
					sqlCmd.Parameters.Add(new SqlParameter("@KenanActivateServiceDT", _regAcct.KenanActivateServiceDT != null ? _regAcct.KenanActivateServiceDT : DateTime.Now));
					sqlCmd.Parameters.Add(new SqlParameter("@KenanServiceInternalNo", _regAcct.KenanServiceInternalNo));
					sqlCmd.Parameters.Add(new SqlParameter("@KenanServiceResetInternalNo", _regAcct.KenanServiceResetInternalNo));
					sqlCmd.Parameters.Add(new SqlParameter("@KenanOrderID", _regAcct.KenanOrderID));
					sqlCmd.Parameters.Add(new SqlParameter("@KenanResetOrderID", _regAcct.KenanResetOrderID));
					sqlCmd.Parameters.Add(new SqlParameter("@KenanCreateAccountErrNo", _regAcct.KenanCreateAccountErrNo));
					sqlCmd.Parameters.Add(new SqlParameter("@KenanCreateAccountErrMessage", _regAcct.KenanCreateAccountErrMessage));
					sqlCmd.Parameters.Add(new SqlParameter("@Active", _regAcct.Active));
					sqlCmd.Parameters.Add(new SqlParameter("@CreateDT", _regAcct.CreateDT != null ? _regAcct.CreateDT : DateTime.Now));
					sqlCmd.Parameters.Add(new SqlParameter("@LastUpdateDT", _regAcct.LastUpdateDT != null ? _regAcct.LastUpdateDT : DateTime.Now));
					sqlCmd.Parameters.Add(new SqlParameter("@LastAccessID", !string.IsNullOrEmpty(_regAcct.LastAccessID) ? _regAcct.LastAccessID : "Kenan Call Back"));

					sqlCmd.CommandType = CommandType.StoredProcedure;
					sqlCmd.CommandTimeout = 180;
					adapter.SelectCommand = sqlCmd;
					adapter.Fill(dsRegistrations);
					sqlConn.Close();
				}
			}
			catch (Exception ex)
			{
				Logger.ErrorFormat("Exiting {0}.regAcctCreationCreate({1}): {2}", svcName, _regID, LogException(ex));
			}

			return _regID;
		}
        public static int regAcctCreationCreateSmart(int _regID, RegAccount _regAcct)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
                {
                    sqlConn.Open();
                    SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                    DataSet dsRegistrations = new DataSet();
                    SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_LnkRegAcctCreate_Smart", sqlConn);
                    sqlCmd.Parameters.Add(new SqlParameter("@RegID", _regID));
                    sqlCmd.Parameters.Add(new SqlParameter("@KenanCreateAccountDT", _regAcct.KenanCreateAccountDT));
                    sqlCmd.Parameters.Add(new SqlParameter("@KenanAccountExternalNo", _regAcct.KenanAccountExternalNo));
                    sqlCmd.Parameters.Add(new SqlParameter("@KenanAccountInternalNo", _regAcct.KenanAccountInternalNo));
                    sqlCmd.Parameters.Add(new SqlParameter("@KenanActivateServiceDT", _regAcct.KenanActivateServiceDT != null ? _regAcct.KenanActivateServiceDT : DateTime.Now));
                    sqlCmd.Parameters.Add(new SqlParameter("@KenanServiceInternalNo", _regAcct.KenanServiceInternalNo));
                    sqlCmd.Parameters.Add(new SqlParameter("@KenanServiceResetInternalNo", _regAcct.KenanServiceResetInternalNo));
                    sqlCmd.Parameters.Add(new SqlParameter("@KenanOrderID", _regAcct.KenanOrderID != null ? _regAcct.KenanOrderID : ""));
                    sqlCmd.Parameters.Add(new SqlParameter("@ServiceOrderID", _regAcct.ServiceOrderId));
                    sqlCmd.Parameters.Add(new SqlParameter("@KenanResetOrderID", _regAcct.KenanResetOrderID));
                    sqlCmd.Parameters.Add(new SqlParameter("@KenanCreateAccountErrNo", _regAcct.KenanCreateAccountErrNo));
                    sqlCmd.Parameters.Add(new SqlParameter("@KenanCreateAccountErrMessage", _regAcct.KenanCreateAccountErrMessage));
                    sqlCmd.Parameters.Add(new SqlParameter("@Active", _regAcct.Active));
                    sqlCmd.Parameters.Add(new SqlParameter("@CreateDT", _regAcct.CreateDT != null ? _regAcct.CreateDT : DateTime.Now));
                    sqlCmd.Parameters.Add(new SqlParameter("@LastUpdateDT", _regAcct.LastUpdateDT != null ? _regAcct.LastUpdateDT : DateTime.Now));
                    sqlCmd.Parameters.Add(new SqlParameter("@LastAccessID", !string.IsNullOrEmpty(_regAcct.LastAccessID) ? _regAcct.LastAccessID : "Kenan Call Back"));

                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandTimeout = 180;
                    adapter.SelectCommand = sqlCmd;
                    adapter.Fill(dsRegistrations);
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Exiting {0}.regAcctCreationCreate({1}): {2}", svcName, _regID, LogException(ex));
            }

            return _regID;
        }
        public static int RegStatusUpdate(int Regid, RegStatus regStatus)
		{
			int value = 0;
			Logger.InfoFormat("Entering {0}.RegStatusUpdate({1})", svcName, (Regid != null) ? Regid : 0);

			#region PBI000000009710 Dual status for Service Pending Activate and Service Activated
			//using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			//{
			//    using (var trans = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
			//    {
			//        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
			//    }))
			//    {
			//        var oldRegStatus =
			//        ctx.RegStatuses.SingleOrDefault(a => a.RegID == regStatus.RegID && a.Active);
			//        if (oldRegStatus != null)
			//        {
			//            oldRegStatus.Active = false;
			//            oldRegStatus.EndDate = DateTime.Now;
			//        }
			//        value = ctx.SaveChanges();
			//        trans.Complete();
			//        trans.Dispose();
			//    }
			//    using (var trans = new TransactionScope())
			//    {

			//        ctx.RegStatuses.Add(regStatus);
			//        ctx.SaveChanges();

			//        trans.Complete();
			//        trans.Dispose();
			//    }
			//}
			#endregion
			
			Logger.InfoFormat("Entering {0}.RegStatusUpdate({1})", "USP_MOBILITY_UpdateStatus", (Regid != null) ? Regid : 0);
			using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
			{
				sqlConn.Open();
				SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
				DataSet dsRegistrations = new DataSet();
				SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_UpdateStatus", sqlConn);
				sqlCmd.Parameters.Add(new SqlParameter("@RegID", regStatus.RegID));
				sqlCmd.Parameters.Add(new SqlParameter("@Active", regStatus.Active));
				sqlCmd.Parameters.Add(new SqlParameter("@CreateDT", regStatus.CreateDT));
				sqlCmd.Parameters.Add(new SqlParameter("@StartDate", regStatus.StartDate));
				sqlCmd.Parameters.Add(new SqlParameter("@LastAccessID", regStatus.LastAccessID));
				sqlCmd.Parameters.Add(new SqlParameter("@StatusID", regStatus.StatusID));
				sqlCmd.Parameters.Add(new SqlParameter("@LastUpdateDT", DateTime.Now));

				sqlCmd.CommandType = CommandType.StoredProcedure;
				sqlCmd.CommandTimeout = 180;
				adapter.SelectCommand = sqlCmd;
				adapter.Fill(dsRegistrations);
				sqlConn.Close();
				//sqlCmd.CommandTimeout = 90000;

			}
			Logger.InfoFormat("Exiting {0}.RegStatusUpdate({1}): {2}", "USP_MOBILITY_UpdateStatus", (Regid != null) ? Regid : 0, value);
			return value;
		}

        public static int RegStatusUpdate(RegStatus oReq)
        {
            Logger.InfoFormat("Entering {0}.RegStatusUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                //using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                //{
                //    var addr = ctx.RegStatuses.Find(oReq.ID);
                //    addr.ID = oReq.ID;
                //    addr.RegID = oReq.RegID;
                //    addr.StatusID = oReq.StatusID;
                //    addr.ReasonCodeID = oReq.ReasonCodeID;
                //    addr.Active = oReq.Active;
                //    addr.LastUpdateDT = oReq.LastUpdateDT;
                //    addr.LastAccessID = oReq.LastAccessID;
                //    value = ctx.SaveChanges();
                //}

                using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
                {
                    sqlConn.Open();
                    SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                    DataSet dsRegistrations = new DataSet();
                    SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_UpdateStatus", sqlConn);
                    sqlCmd.Parameters.Add(new SqlParameter("@RegID", oReq.ID));
                    sqlCmd.Parameters.Add(new SqlParameter("@Active", oReq.Active));
                    sqlCmd.Parameters.Add(new SqlParameter("@CreateDT", oReq.CreateDT));
                    sqlCmd.Parameters.Add(new SqlParameter("@StartDate", oReq.StartDate));
                    sqlCmd.Parameters.Add(new SqlParameter("@LastAccessID", oReq.LastAccessID));
                    sqlCmd.Parameters.Add(new SqlParameter("@StatusID", oReq.StatusID));
                    sqlCmd.Parameters.Add(new SqlParameter("@LastUpdateDT", oReq.LastUpdateDT));

                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandTimeout = 180;
                    adapter.SelectCommand = sqlCmd;
                    adapter.Fill(dsRegistrations);
                    sqlConn.Close();
                    //sqlCmd.CommandTimeout = 90000;
                }
            }

            Logger.InfoFormat("Exiting {0}.RegStatusUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static List<int> RegStatusFind(RegStatusFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegStatusFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.RegStatuses.Where(a => a.ID != 0);

                if (oReq.RegistrationIDs.Count > 0)
                    query = query.Where(a => oReq.RegistrationIDs.Contains(a.RegID));

                if (!(oReq.RegStatus.ID == 0))
                    query = query.Where(a => a.ID == oReq.RegStatus.ID);

                if (!(oReq.RegStatus.RegID == 0))
                    query = query.Where(a => a.RegID == oReq.RegStatus.RegID);

                if (!(oReq.RegStatus.StatusID == 0))
                    query = query.Where(a => a.StatusID == oReq.RegStatus.StatusID);

                if (!(oReq.RegStatus.ReasonCodeID.ToInt() == 0))
                    query = query.Where(a => a.ReasonCodeID == oReq.RegStatus.ReasonCodeID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);



                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegStatusFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<RegStatus> RegStatusGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.RegStatusGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<RegStatus> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegStatuses.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegStatusGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<RegStatus> RegStatusGet1(RegStatusFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegStatusGet1({1})", svcName, (oReq == null) ? "NULL" : oReq.ToString());
            List<RegStatus> result = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var value = ctx.RegStatuses.Where(a => a.ID != 0);

                if (oReq.RegistrationIDs.Count > 0)
                    value = value.Where(a => oReq.RegistrationIDs.Contains(a.RegID));

                if (!(oReq.RegStatus.ID == 0))
                    value = value.Where(a => a.ID == oReq.RegStatus.ID);

                if (!(oReq.RegStatus.RegID == 0))
                    value = value.Where(a => a.RegID == oReq.RegStatus.RegID);

                if (!(oReq.RegStatus.StatusID == 0))
                    value = value.Where(a => a.StatusID == oReq.RegStatus.StatusID);

                if (!(oReq.RegStatus.ReasonCodeID.ToInt() == 0))
                    value = value.Where(a => a.ReasonCodeID == oReq.RegStatus.ReasonCodeID);

                if (oReq.Active.HasValue)
                    value = value.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    value = value.OrderByDescending(a => a.ID).Take(oReq.PageSize);
                result = value.ToList();
            }


            Logger.InfoFormat("Exiting {0}.RegStatusGet({1}): {2}", svcName, (oReq == null) ? "NULL" : oReq.ToString(), result.Count());
            return result;
        }

        public static List<RegStatusResult> RegStatusLogGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.RegStatusLogGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<RegStatusResult> value = new List<RegStatusResult>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //value = ctx.RegStatuses.Where(a => IDs.Contains(a.ID)).ToList();
                var query = from regStatus in ctx.RegStatuses
                            join reg in ctx.Registrations on regStatus.RegID equals reg.ID
                            where IDs.Contains(regStatus.ID)
                            select new
                            {
                                RegStatus = regStatus,
                                Registration = reg
                            };

                var result = query.ToList();

                var reasonCodeIDs = result.Where(a => a.RegStatus.ReasonCodeID.HasValue && a.RegStatus.ReasonCodeID.Value != 0).Select(a => a.RegStatus.ReasonCodeID.Value).Distinct().ToList();
                var reasonCodes = new List<StatusReason>();

                if (reasonCodeIDs.Count() > 0)
                {
                    reasonCodes = ctx.StatusReasons.Where(a => reasonCodeIDs.Contains(a.ID)).ToList();
                }

                foreach (var item in result)
                {
                    var regStatusResult = new RegStatusResult()
                    {
                        RegStatusID = item.RegStatus.ID,
                        RegID = item.Registration.ID,
                        ReasonCode = reasonCodeIDs.Count > 0 ? (item.RegStatus.ReasonCodeID.HasValue ? reasonCodes.Where(a => a.ID == item.RegStatus.ReasonCodeID.Value).SingleOrDefault().Name : "") : "",
                        RegStatus = ctx.Statuses.Where(a => a.ID == item.RegStatus.StatusID).Select(a => a.Description).Single(),
                        Remark = item.RegStatus.Remark,
                        KenanAccountNo = item.Registration.KenanAccountNo == "0" ? string.Empty : item.Registration.KenanAccountNo,
                        ModemID = item.Registration.ModemID == "0" ? string.Empty : item.Registration.ModemID,
                        StatusLogDT = item.RegStatus.StatusLogDT,
                        LastAccessID = item.RegStatus.LastAccessID,
                        CreateDT = item.RegStatus.CreateDT
                    };
                    value.Add(regStatusResult);
                }
            }

            Logger.InfoFormat("Exiting {0}.RegStatusLogGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<RegStatusResult> RegStatusSearch(int regID)
        {
            Logger.InfoFormat("Entering {0}.RegStatusSearch({1})", svcName, (regID != null) ? regID : 0);
            List<RegStatusResult> result = new List<RegStatusResult>();

            using (var context = new OnlineStoreDbContext())
            {
                if (regID != null || regID != 0)
                {
                    var query = from regStatus in context.RegStatuses

                                join status in context.Statuses on
                                regStatus.StatusID equals status.ID //into custs
                                //from cust in custs.DefaultIfEmpty()

                                select new
                                {
                                    RegStatus = regStatus,
                                    Status = status
                                };

                    if (regID != 0)
                    {
                        query = query.Where(a => a.RegStatus.RegID == regID);
                    }

                    var temp = query.Select(a => new
                    {
                        RegStatusID = a.RegStatus.ID,
                        RegID = a.RegStatus.RegID,
                        RegStatusDate = a.RegStatus.CreateDT,
                        Remark = a.RegStatus.Remark,
                        Status = a.Status.Description,
                        LastAccessID = a.RegStatus.LastAccessID != null ? a.RegStatus.LastAccessID : string.Empty,
                    }).Distinct();

                    result = temp.Select(a => new RegStatusResult()
                    {
                        RegStatusID = a.RegStatusID,
                        RegID = a.RegID,
                        RegStatus = a.Status,
                        CreateDT = a.RegStatusDate,
                        Remark = a.Remark,
                        LastAccessID = a.LastAccessID
                    }).ToList();
                }
            }

            Logger.InfoFormat("Exiting {0}.RegStatusSearch({1}): {2}", svcName, (regID != null) ? regID : 0, result.Count());
            return result;
        }
        
        public static int GetCenterIdByInstaller(int regID, int statusID)
        {
            Logger.InfoFormat("Entering {0}.GetCenterIdByInstaller({1})", svcName, (regID != null) ? regID : 0);

            using (var context = new OnlineStoreDbContext())
            {
                var query = from accLast in context.Accesses
                            join rstatus in context.RegStatuses on accLast.UserName equals rstatus.LastAccessID
                            where rstatus.RegID == regID && rstatus.StatusID == statusID
                            select accLast.ID;
                var result = query.FirstOrDefault();
                var installerID = context.InstallerCenters.FirstOrDefault(e => e.AccessId == result).CenterId;

                var centerid = context.Centers.FirstOrDefault(e => e.Id == installerID).CenterId;

                return centerid;
            }
        }
        public static bool isStatusexistsRegId(int regID, int statusID)
        {
            Logger.InfoFormat("Entering {0}.GetCenterIdByInstaller({1})", svcName, (regID != null) ? regID : 0);

            using (var context = new OnlineStoreDbContext())
            {

                var installerID = context.RegStatuses.FirstOrDefault(e => e.StatusID == statusID).ID;

                if ((installerID != null) || (installerID == 0))
                {
                    return false;
                }
                else
                    return true;

            }
        }
        #endregion

        #region Reg SuppLine

        public static int RegSuppLineCreate(RegSuppLine oReq)
        {
            Logger.InfoFormat("Entering {0}.RegSuppLineCreate({1})", svcName, (oReq != null) ? oReq.Description : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.RegSuppLines.Add(oReq);
                ctx.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.RegSuppLineCreate({1}): {2}", svcName, (oReq != null) ? oReq.Description : "NULL", value);
            return value;
        }
        public static int RegSuppLineUpdate(RegSuppLine oReq)
        {
            Logger.InfoFormat("Entering {0}.RegSuppLineUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var addr = ctx.RegSuppLines.Find(oReq.ID);
                    addr.ID = oReq.ID;
                    addr.RegID = oReq.RegID;
                    addr.PgmBdlPckComponentID = oReq.PgmBdlPckComponentID;
                    addr.IsNewAccount = oReq.IsNewAccount;
                    addr.VersionNo = oReq.VersionNo;
                    addr.Active = oReq.Active;
                    addr.LastUpdateDT = oReq.LastUpdateDT;
                    addr.LastAccessID = oReq.LastAccessID;
                    addr.KENAN_Req_Status = oReq.KENAN_Req_Status;
                    addr.Order_Status = oReq.Order_Status;
                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.RegSuppLineUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static List<int> RegSuppLineFind(RegSuppLineFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegSuppLineFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.RegSuppLines.Where(a => a.ID != 0);

                if (!(oReq.RegSuppLine.ID == 0))
                    query = query.Where(a => a.ID == oReq.RegSuppLine.ID);

                if (!(oReq.RegSuppLine.RegID == 0))
                    query = query.Where(a => a.RegID == oReq.RegSuppLine.RegID);

                if (oReq.Active.HasValue)
                    query = query.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegSuppLineFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }

        public static List<RegUploadDoc> regUploadGet(int regID)
        {
            List<RegUploadDoc> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegUploadDoc.Where(a => a.RegID == regID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.regUploadGet({1}): {2}", svcName, regID, value.Count());
            return value;

        }

        //Samuel add this for Upload file if has KenanAccountNo
        public static int regUploadUpdateFile(RegUploadDoc obj, string FileName, byte[] FileStream)
        {
            Logger.InfoFormat("Entering {0}.regUploadUpdate({1})", svcName, (obj != null) ? obj.ID : 0);
            var value = 0;

            if (obj != null && obj.ID > 0)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var upload = ctx.RegUploadDoc.Find(obj.ID);
                    upload.FileName = FileName;
                    upload.FileStream = FileStream;
                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.regUploadUpdate({1})", svcName, (obj != null) ? obj.ID : 0);
            return value;
        }

		public static List<RegType> regTypeAll()
		{
			List<RegType> value = null;

			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				value = ctx.RegTypes.ToList();
			}

			Logger.InfoFormat("Exiting {0}.RegType({1})", svcName, value.Count());
			return value;

		}
		

		public static int regUploadUpdate(RegUploadDoc obj, string status)
		{
			Logger.InfoFormat("Entering {0}.regUploadUpdate({1})", svcName, (obj != null) ? obj.ID : 0);
			var value = 0;

			if (obj != null && obj.ID > 0)
			{
				using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
				{
					var upload = ctx.RegUploadDoc.Find(obj.ID);
					upload.Status = status;
					upload.ID = obj.ID;
					upload.Date = DateTime.Now;
					value = ctx.SaveChanges();
				}
			}

			Logger.InfoFormat("Exiting {0}.regUploadUpdate({1})", svcName, (obj != null) ? obj.ID : 0);
			return value;
		}

         public static List<RegSuppLine> RegSuppLineGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.RegSuppLineGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<RegSuppLine> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegSuppLines.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegSuppLineGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }
        public static List<RegSuppLine> RegSuppLineGet1(RegSuppLineFind oReq)
        {
            Logger.InfoFormat("Entering {0}.RegSuppLineGet1({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<RegSuppLine> result = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var value = ctx.RegSuppLines.Where(a => a.ID != 0);

                if (!(oReq.RegSuppLine.ID == 0))
                    value = value.Where(a => a.ID == oReq.RegSuppLine.ID);

                if (!(oReq.RegSuppLine.RegID == 0))
                    value = value.Where(a => a.RegID == oReq.RegSuppLine.RegID);

                if (oReq.Active.HasValue)
                    value = value.Where(a => a.Active == oReq.Active.Value);

                if (oReq.PageSize > 0)
                    value = value.OrderByDescending(a => a.ID).Take(oReq.PageSize);
                result = value.ToList();
            }

            Logger.InfoFormat("Exiting {0}.RegSuppLineGet1({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", result.Count());
            return result;
        }
        #endregion

        #region Biometrics

        public static int BiometricCreate(Biometrics oReq)
        {
            Logger.InfoFormat("Entering {0}.BiometricCreate({1})", svcName, (oReq != null) ? oReq.Name : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.Biometricses.Add(oReq);
                ctx.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.BiometricCreate({1}): {2}", svcName, (oReq != null) ? oReq.Name : "NULL", value);
            return value;
        }
        public static int BiometricUpdate(Biometrics oReq)
        {
            Logger.InfoFormat("Entering {0}.BiometricUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var bio = ctx.Biometricses.Find(oReq.ID);
                    bio.ID = oReq.ID;
                    bio.RegID = oReq.RegID;
                    bio.Name = oReq.Name;
                    bio.NewIC = oReq.NewIC;
                    bio.OldIC = oReq.OldIC;
                    bio.Gender = oReq.Gender;
                    bio.DOB = oReq.DOB;
                    bio.Nationality = oReq.Nationality;
                    bio.Race = oReq.Race;
                    bio.Religion = oReq.Religion;
                    bio.Address1 = oReq.Address1;
                    bio.Address2 = oReq.Address2;
                    bio.Address3 = oReq.Address3;
                    bio.Postcode = oReq.Postcode;
                    bio.City = oReq.City;
                    bio.State = oReq.State;
                    bio.FingerMatch = oReq.FingerMatch;
                    bio.FingerThumb = oReq.FingerThumb;
                    bio.LastAccessID = oReq.LastAccessID;
                    bio.LastUpdateDT = DateTime.Now;

                    value = ctx.SaveChanges();
                }
            }

            Logger.InfoFormat("Exiting {0}.BiometricUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static List<int> BiometricFind(BiometricFind oReq)
        {
            Logger.InfoFormat("Entering {0}.BiometricFind({1})", svcName, (oReq != null) ? oReq.ToString() : "NULL");
            List<int> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var query = ctx.Biometricses.Where(a => a.ID != 0);

                if (!(oReq.Biometric.ID == 0))
                    query = query.Where(a => a.ID == oReq.Biometric.ID);

                if (!(oReq.Biometric.RegID == 0))
                    query = query.Where(a => a.RegID == oReq.Biometric.RegID);

                if (oReq.PageSize > 0)
                    query = query.OrderByDescending(a => a.ID).Take(oReq.PageSize);

                value = query.Select(a => a.ID).ToList();
            }

            Logger.InfoFormat("Exiting {0}.BiometricFind({1}): {2}", svcName, (oReq != null) ? oReq.ToString() : "NULL", value.Count());
            return value;
        }
        public static List<Biometrics> BiometricGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.BiometricGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<Biometrics> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Biometricses.Where(a => IDs.Contains(a.ID)).ToList();
            }

            Logger.InfoFormat("Exiting {0}.BiometricGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        #endregion

        #region Reg Kenan Account

        public static int RegAccountCreate(RegAccount oReq)
        {
            Logger.InfoFormat("Entering {0}.RegAccountCreate(RegID: {1})", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL");
            var value = 0;

            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    using (var trans = new TransactionScope(TransactionScopeOption.RequiresNew))
                    {
                        oReq.CreateDT = DateTime.Now;
                        Logger.InfoFormat("Date" + oReq.CreateDT);
                        ctx.RegAccounts.Add(oReq);
                        ctx.SaveChanges();

                        trans.Complete();
                    }
                }

                value = oReq.ID;
            }
            catch (Exception ex)
            {

                Logger.Info("Exception while excuting the RegAccountCreate method:" + ex);
            }

            Logger.InfoFormat("Exiting {0}.RegAccountCreate(RegID: {1}): {2}", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL", value);
            return value;
        }
        public static int RegAccountUpdate(RegAccount oReq)
        {
            Logger.InfoFormat("Entering {0}.RegAccountUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    using (var trans = new TransactionScope())
                    {
                        var regAcc = ctx.RegAccounts.Find(oReq.ID);
                        regAcc.ID = oReq.ID;
                        regAcc.RegID = oReq.RegID;

                        regAcc.KenanCreateAccountDT = regAcc.KenanCreateAccountDT.HasValue ? regAcc.KenanCreateAccountDT.Value : oReq.KenanCreateAccountDT;
                        regAcc.KenanAccountExternalNo = string.IsNullOrEmpty(regAcc.KenanAccountExternalNo) ? oReq.KenanAccountExternalNo : regAcc.KenanAccountExternalNo;
                        regAcc.KenanAccountInternalNo = string.IsNullOrEmpty(regAcc.KenanAccountInternalNo) ? oReq.KenanAccountInternalNo : regAcc.KenanAccountInternalNo;
                        regAcc.KenanActivateServiceDT = oReq.KenanActivateServiceDT;
                        regAcc.KenanServiceInternalNo = oReq.KenanServiceInternalNo;
                        regAcc.KenanServiceResetInternalNo = oReq.KenanServiceResetInternalNo;
                        regAcc.KenanOrderID = oReq.KenanOrderID;
                        regAcc.KenanResetOrderID = oReq.KenanResetOrderID;
                        regAcc.KenanActivateServiceErrNo = oReq.KenanActivateServiceErrNo;
                        regAcc.KenanActivateServiceErrMessage = oReq.KenanActivateServiceErrMessage;
                        regAcc.LastAccessID = oReq.LastAccessID;
                        regAcc.LastUpdateDT = oReq.LastUpdateDT;

                        value = ctx.SaveChanges();

                        trans.Complete();
                    }
                }
            }

            Logger.InfoFormat("Exiting {0}.RegAccountUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static int RegAccountCreate(RegAccount oReq, RegStatus regStatus)
        {
            Logger.InfoFormat("Entering {0}.RegAccountCreate(RegID: {1})", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL");
            var value = 0;

            try
            {
                using (var ctx = new OnlineStoreDbContext())
				{
					#region Commented out by Gry - change to SP
					/*
					using (var trans = new TransactionScope())
                    {
                        if (oReq != null)
                        {
                            oReq.CreateDT = DateTime.Now;
                            Logger.InfoFormat("Date" + oReq.CreateDT);
                            ctx.RegAccounts.Add(oReq);
                        }
                        ctx.SaveChanges();
                        trans.Complete();
                        trans.Dispose();
                    }
					using (var trans = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
					{
						IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
					}))
					{
						var reg = ctx.Registrations.SingleOrDefault(a => a.ID == oReq.RegID);
						if (reg != null && oReq != null)
						{
							reg.KenanAccountNo = oReq.KenanAccountExternalNo;
							reg.LastUpdateDT = DateTime.Now;
							reg.LastAccessID = "Kenan Call Back";
							ctx.SaveChanges();
						}
						trans.Complete();
						trans.Dispose();
					}
					*/
					#endregion
					regAcctCreationCreate(oReq.RegID, oReq);
					oReq.ID = ctx.RegAccounts.Where(x => x.RegID == oReq.RegID).FirstOrDefault().ID;
					RegStatusUpdate(regStatus.RegID, regStatus);
                }

                if (oReq != null)
                {
                    value = oReq.ID;
                }
            }
            catch (Exception ex)
            {

                Logger.Info("Exception while excuting the RegAccountCreate method:" + ex);
            }

            Logger.InfoFormat("Exiting {0}.RegAccountCreate(RegID: {1}): {2}", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL", value);
            return value;
        }

        public static int RegAccountUpdate(RegAccount oReq, RegStatus regStatus, bool fromMNPResub=false)
        {
            Logger.InfoFormat("Entering {0}.RegAccountUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    using (var trans = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }))
                    {
						// for MNP resubmit, Kenan didnt passed the kenan Order ID, so it will update the DB to null value
						// this flag is for prevent iSell to update on null value, sample : 416348
						if (!fromMNPResub)
						{
							var regAcc = ctx.RegAccounts.Find(oReq.ID);
							regAcc.ID = oReq.ID;
							regAcc.RegID = oReq.RegID;

							regAcc.KenanCreateAccountDT = regAcc.KenanCreateAccountDT.HasValue
															  ? regAcc.KenanCreateAccountDT.Value
															  : oReq.KenanCreateAccountDT;
							regAcc.KenanAccountExternalNo = string.IsNullOrEmpty(regAcc.KenanAccountExternalNo)
																? oReq.KenanAccountExternalNo
																: regAcc.KenanAccountExternalNo;
							regAcc.KenanAccountInternalNo = string.IsNullOrEmpty(regAcc.KenanAccountInternalNo)
																? oReq.KenanAccountInternalNo
																: regAcc.KenanAccountInternalNo;
							regAcc.KenanActivateServiceDT = oReq.KenanActivateServiceDT;
							regAcc.KenanServiceInternalNo = oReq.KenanServiceInternalNo;
							regAcc.KenanServiceResetInternalNo = oReq.KenanServiceResetInternalNo;
							regAcc.KenanOrderID = oReq.KenanOrderID;
							regAcc.KenanResetOrderID = oReq.KenanResetOrderID;
							regAcc.KenanActivateServiceErrNo = oReq.KenanActivateServiceErrNo;
							regAcc.KenanActivateServiceErrMessage = oReq.KenanActivateServiceErrMessage;
							regAcc.LastAccessID = oReq.LastAccessID;
							regAcc.LastUpdateDT = oReq.LastUpdateDT;

							value = ctx.SaveChanges();
						}

						#region Commented out by Gry - change to SP
						
						//var reg = ctx.Registrations.SingleOrDefault(a => a.ID == oReq.RegID);
						//reg.LastUpdateDT = DateTime.Now;
						//reg.LastAccessID = oReq.LastAccessID;
						//ctx.SaveChanges();

						//var oldRegStatus =
						//    ctx.RegStatuses.SingleOrDefault(a => a.RegID == regStatus.RegID && a.Active);
						//if (oldRegStatus != null)
						//{
						//    oldRegStatus.Active = false;
						//    oldRegStatus.EndDate = DateTime.Now;
						//}
						//ctx.SaveChanges();
						#endregion
						trans.Complete();
                        trans.Dispose();
					}
					#region Commented out by Gry - change to SP
					//using (var trans = new TransactionScope())
					//{

					//    ctx.RegStatuses.Add(regStatus);
					//    ctx.SaveChanges();

					//    trans.Complete();
					//    trans.Dispose();
					//}
					#endregion
					RegStatusUpdate(regStatus.RegID, regStatus);
                }
            }

            Logger.InfoFormat("Exiting {0}.RegAccountUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static int RegAccountUpdate(RegAccount oReq, string LobType)
        {
            Logger.InfoFormat("Entering {0}.RegAccountUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (var ctx = new OnlineStoreDbContext())
                {
                    using (var trans = new TransactionScope())
                    {
                        var regAcc = ctx.RegAccounts.FirstOrDefault(e => e.RegID == oReq.RegID && e.Lobtype == LobType);
                        if (regAcc != null)
                        {
                            regAcc.ID = oReq.ID;
                            regAcc.RegID = oReq.RegID;
                            regAcc.KenanOrderID = oReq.KenanOrderID;
                            regAcc.ServiceOrderId = oReq.ServiceOrderId;
                            regAcc.LastAccessID = oReq.LastAccessID;
                            regAcc.LastUpdateDT = oReq.LastUpdateDT;
                        }
                        value = ctx.SaveChanges();
                        trans.Complete();
                        trans.Dispose();
                    }
                }
            }

            Logger.InfoFormat("Exiting {0}.RegAccountUpdate({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static int RegAccountUpdateVOIPFTTH(RegAccount oReq, string LobType)
        {
            Logger.InfoFormat("Entering {0}.RegAccountUpdate({1})", svcName, (oReq != null) ? oReq.ID : 0);
            var value = 0;

            if (oReq != null)
            {
                using (var ctx = new OnlineStoreDbContext())
                {
                    using (var trans = new TransactionScope())
                    {
                        var regAcc = ctx.RegAccounts.FirstOrDefault(e => e.RegID == oReq.RegID && e.Lobtype == LobType);
                        if (regAcc != null)
                        {
                            regAcc.KenanOrderID = oReq.KenanOrderID;
                            regAcc.ServiceOrderId = oReq.ServiceOrderId;
                            regAcc.KenanAccountExternalNo = oReq.KenanAccountExternalNo;
                            regAcc.LastAccessID = oReq.LastAccessID;
                            regAcc.LastUpdateDT = oReq.LastUpdateDT;
                            regAcc.KenanServiceResetInternalNo = oReq.KenanServiceResetInternalNo;
                            regAcc.KenanResetOrderID = oReq.KenanResetOrderID;
                        }
                        value = ctx.SaveChanges();
                        trans.Complete();
                    }
                }
            }

            Logger.InfoFormat("Exiting {0}.RegAccountUpdateVOIPFTTH({1}): {2}", svcName, (oReq != null) ? oReq.ID : 0, value);
            return value;
        }
        public static RegAccount RegAccountGetByRegID(int regID)
        {
            Logger.InfoFormat("Entering {0}.RegAccountGetByRegID({1})", svcName, regID);
            RegAccount value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegAccounts.SingleOrDefault(a => a.RegID == regID);
            }

            Logger.InfoFormat("Exiting {0}.RegAccountGetByRegID({1})", svcName, regID);
            return value;
        }

        #endregion

        //Added by VLT on 5 Mar 2013 for adding Kenan customer Info//
        #region KenanCustomerInfo
        public static int KenanCustomerInfoCreate(KenamCustomerInfo oReq)
        {
            Logger.InfoFormat("Entering {0}.KenanCustomerInfoCreate(ID: {1})", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.KenanCustomerInfos.Add(oReq);
                ctx.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.KenanCustomerInfoCreate(ID: {1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL", value);
            return value;

        }
        #endregion
        //Added by VLT on 5 Mar 2013 for adding Kenan customer Info//

        #region Added by Patanjali on 07-04-2013 to get payment status
        public static int PaymentStatusGet(int RegID)
        {
            int returnstatus = -1;
            try
            {
                Logger.InfoFormat("Entering {0}.PaymentStatusGet({1})", svcName, (RegID != null) ? RegID : 0);
                List<DAL.Models.RegStatus> value = null;


                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    returnstatus = ctx.RegStatuses.Where(a => a.RegID == RegID && a.StatusID == 36).Select(a => a.StatusID).FirstOrDefault();
                }


                Logger.InfoFormat("Exiting {0}.PaymentStatusGet({1}): {2}", svcName, (RegID != null) ? RegID : 0, returnstatus);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return returnstatus;
        }
        #endregion

        /*Chetan added for displaying the reson for failed transcation*/
        public static List<DAL.Models.KenanaLogDetails> KenanLogDetailsGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.KenanLogDetailsGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<DAL.Models.KenanaLogDetails> value = null;

            using (var ctx = new OnlineStoreDbContext())
            {
                if (IDs != null)
                {
                    int regId = IDs[0].ToInt();
                    value = ctx.KenanaLogDetails.Where(a => a.RegID == regId && a.MessageCode != "0").ToList();
                }
            }

            Logger.InfoFormat("Exiting {0}.KenanLogDetailsGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static List<DAL.Models.KenanaLogDetails> KenanLogHistoryDetailsGet(List<int> IDs)
        {
            Logger.InfoFormat("Entering {0}.KenanLogHistoryDetailsGet({1})", svcName, (IDs != null) ? IDs[0] : 0);
            List<DAL.Models.KenanaLogDetails> value = null;

            using (var ctx = new OnlineStoreDbContext())
            {
                if (IDs != null)
                {
                    int regId = IDs[0].ToInt();
                    value = ctx.KenanaLogDetails.Where(a => a.RegID == regId).ToList();
                }
            }

            Logger.InfoFormat("Exiting {0}.KenanLogHistoryDetailsGet({1}): {2}", svcName, (IDs != null) ? IDs[0] : 0, value.Count());
            return value;
        }

        public static int RegAccountCreate(RegAccount oReq, RegStatus regStatus, string Modelid)
        {

            // Logger.InfoFormat("Entering {0}.RegAccountCreate(RegID: {1})", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL");

            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    oReq.CreateDT = DateTime.Now;
                    Logger.InfoFormat("Date" + oReq.CreateDT);
                    ctx.RegAccounts.Add(oReq);
                    ctx.SaveChanges();

                    var reg = ctx.Registrations.Where(a => a.ID == oReq.RegID).SingleOrDefault();
                    reg.KenanAccountNo = oReq.KenanAccountExternalNo;

                    reg.ModemID = Convert.ToString(Modelid);
                    reg.LastUpdateDT = DateTime.Now;
                    reg.LastAccessID = "Kenan Call Back";
                    ctx.SaveChanges();
                    trans.Complete();
                }
            }
            value = oReq.ID;
            return value;
        }
        public static int RegAccountUpdate(RegAccount oReq, RegStatus regStatus, string md)
        {
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    using (var trans = new TransactionScope())
                    {
                        var reg = ctx.Registrations.Where(a => a.ID == oReq.RegID).SingleOrDefault();
                        reg.ModemID = Convert.ToString(md);
                        ctx.SaveChanges();

                        var regAcc = ctx.RegAccounts.Find(oReq.ID);
                        regAcc.ID = oReq.ID.ToInt();
                        regAcc.RegID = oReq.RegID.ToInt();

                        regAcc.KenanCreateAccountDT = regAcc.KenanCreateAccountDT.HasValue ? regAcc.KenanCreateAccountDT.Value : oReq.KenanCreateAccountDT;
                        regAcc.KenanAccountExternalNo = string.IsNullOrEmpty(regAcc.KenanAccountExternalNo) ? oReq.KenanAccountExternalNo : regAcc.KenanAccountExternalNo;
                        regAcc.KenanAccountInternalNo = string.IsNullOrEmpty(regAcc.KenanAccountInternalNo) ? oReq.KenanAccountInternalNo : regAcc.KenanAccountInternalNo;
                        regAcc.KenanActivateServiceDT = oReq.KenanActivateServiceDT;
                        regAcc.KenanServiceInternalNo = oReq.KenanServiceInternalNo;
                        regAcc.KenanServiceResetInternalNo = oReq.KenanServiceResetInternalNo;
                        regAcc.KenanOrderID = oReq.KenanOrderID;
                        regAcc.ServiceOrderId = oReq.ServiceOrderId;
                        regAcc.KenanResetOrderID = oReq.KenanResetOrderID;
                        regAcc.KenanActivateServiceErrNo = oReq.KenanActivateServiceErrNo;
                        regAcc.KenanActivateServiceErrMessage = oReq.KenanActivateServiceErrMessage;
                        regAcc.LastAccessID = oReq.LastAccessID;
                        regAcc.LastUpdateDT = oReq.LastUpdateDT;
                        value = ctx.SaveChanges();
                        trans.Complete();
                    }
                }
            }
            return value;
        }
        public static RegAccount RegAccountGetByRegID(int regID, string lobtype)
        {
            RegAccount value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                value = ctx.RegAccounts.Where(a => a.RegID == regID && a.Lobtype == lobtype).FirstOrDefault();
                if (value == null)
                {
                    value = ctx.RegAccounts.Where(a => a.RegID == regID).FirstOrDefault();
                }
            }
            return value;
        }
        public static int RegAccountCreateSmart(RegAccount oReq, RegStatus regStatus)
        {
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //using (var trans = new TransactionScope())
                //{
                //    ctx.RegAccounts.Add(oReq);
                //    ctx.SaveChanges();

                //    var reg = ctx.Registrations.Where(a => a.ID == oReq.RegID).SingleOrDefault();
                //    reg.KenanAccountNo = oReq.KenanAccountExternalNo;
                //    reg.LastUpdateDT = DateTime.Now;
                //    reg.LastAccessID = "Kenan Call Back";
                //    ctx.SaveChanges();

                //    var oldRegStatus = ctx.RegStatuses.Where(a => a.RegID == regStatus.RegID && a.Active == true).SingleOrDefault();
                //    oldRegStatus.Active = false;
                //    oldRegStatus.EndDate = DateTime.Now;
                //    ctx.SaveChanges();

                //    ctx.RegStatuses.Add(regStatus);
                //    ctx.SaveChanges();

                //    trans.Complete();
                //}
                regAcctCreationCreateSmart(oReq.RegID, oReq);
                oReq.ID = ctx.RegAccounts.Where(x => x.RegID == oReq.RegID).FirstOrDefault().ID;
                RegStatusUpdate(regStatus.RegID, regStatus);
            }

            value = oReq.ID;
            return value;
        }

        public static int RegAccountUpdateSmart(RegAccount oReq, RegStatus regStatus)
        {
            var value = 0;

            if (oReq != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    using (var trans = new TransactionScope())
                    {
                        var regAcc = ctx.RegAccounts.Find(oReq.ID);
                        regAcc.ID = oReq.ID;
                        regAcc.RegID = oReq.RegID;

                        regAcc.KenanCreateAccountDT = regAcc.KenanCreateAccountDT.HasValue ? regAcc.KenanCreateAccountDT.Value : oReq.KenanCreateAccountDT;
                        regAcc.KenanAccountExternalNo = string.IsNullOrEmpty(regAcc.KenanAccountExternalNo) ? oReq.KenanAccountExternalNo : regAcc.KenanAccountExternalNo;
                        regAcc.KenanAccountInternalNo = string.IsNullOrEmpty(regAcc.KenanAccountInternalNo) ? oReq.KenanAccountInternalNo : regAcc.KenanAccountInternalNo;
                        regAcc.KenanActivateServiceDT = oReq.KenanActivateServiceDT;
                        regAcc.KenanServiceInternalNo = oReq.KenanServiceInternalNo;
                        regAcc.KenanServiceResetInternalNo = oReq.KenanServiceResetInternalNo;
                        regAcc.KenanOrderID = oReq.KenanOrderID;
                        regAcc.KenanResetOrderID = oReq.KenanResetOrderID;
                        regAcc.KenanActivateServiceErrNo = oReq.KenanActivateServiceErrNo;
                        regAcc.KenanActivateServiceErrMessage = oReq.KenanActivateServiceErrMessage;
                        regAcc.LastAccessID = oReq.LastAccessID;
                        regAcc.LastUpdateDT = oReq.LastUpdateDT;

                        value = ctx.SaveChanges();

                        var oldRegStatus = ctx.RegStatuses.Where(a => a.RegID == regStatus.RegID && a.Active == true).SingleOrDefault();
                        oldRegStatus.Active = false;
                        oldRegStatus.EndDate = DateTime.Now;
                        ctx.SaveChanges();

                        ctx.RegStatuses.Add(regStatus);
                        ctx.SaveChanges();

                        trans.Complete();
                    }
                }
            }
            return value;
        }
        public static RegLob RegLobCreate(int regId, string lob, int ServiceStatusId)
        {
            var regLob = new RegLob { CreateDt = DateTime.Now, RegId = regId, Lob = lob, HomeServiceStatusId = ServiceStatusId };
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                regLob = ctx.RegLobs.Add(regLob);
                ctx.SaveChanges();
            }
            return regLob;
        }
        public static List<RegLob> RegLobGet(int regId)
        {
            List<RegLob> regLob = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                regLob = ctx.RegLobs.Where(e => e.RegId == regId).ToList();
            }
            return regLob;
        }
        public static RegLob RegLobUpdate(int regId, string lob, int ServiceStatusId)
        {
            RegLob regLob = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                regLob = ctx.RegLobs.FirstOrDefault(e => e.RegId == regId && e.Lob == lob);
                if (regLob != null)
                {
                    regLob.LastUpdateDt = DateTime.Now;
                    regLob.HomeServiceStatusId = ServiceStatusId;
                    ctx.SaveChanges();
                }
            }
            return regLob;
        }



        public static RegLob RegLobUpdateforclose(int regId, string lob, int CloseServiceStatusId)
        {
            RegLob regLob = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                regLob = ctx.RegLobs.FirstOrDefault(e => e.RegId == regId && e.Lob == lob);
                if (regLob != null)
                {
                    regLob.closestatus = CloseServiceStatusId;
                    ctx.SaveChanges();
                }
            }
            return regLob;
        }

        public static RegRemark RegRemarkCreate(RegRemark oReq)
        {
            Logger.InfoFormat("Entering {0}.RegRemarkCreate()", svcName);
            var value = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.RegRemarks.Add(oReq);
                ctx.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.RegRemarkCreate({1}): {2}", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL", value);
            return oReq;
        }

        public static RegRemark RegRemarkGet(int regId, int StatusId)
        {
            Logger.InfoFormat("Entering {0}.RegRemarkGet({1})", svcName, regId);
            RegRemark result = null;

            using (var ctx = new OnlineStoreDbContext())
            {
                result = ctx.RegRemarks.Where(e => e.RegID == regId && e.StatusId == StatusId).OrderByDescending(e => e.ID).FirstOrDefault();
                if (result != null && result.StatusReasonId.HasValue && result.StatusReasonId > 0)
                {
                    var statusReason = ctx.StatusReasons.FirstOrDefault(e => e.ID == result.StatusReasonId);
                    if (statusReason != null)
                        result.StatusReason = statusReason;
                }
            }

            Logger.InfoFormat("Exiting {0}.RegRemarkGet({1}): {2}", svcName, regId, result.ID);
            return result;
        }

        public static DAL.Models.Registration RegistrationGetByKenanAccountNo(string kenanAccountNo)
        {
            Logger.InfoFormat("Entering {0}.RegistrationGetByKenanAccountNo({1})", svcName, kenanAccountNo);
            var result = new DAL.Models.Registration();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                result = ctx.Registrations.SingleOrDefault(e => e.KenanAccountNo == kenanAccountNo);
            }

            Logger.InfoFormat("Exiting {0}.RegistrationGetByKenanAccountNo({1}): {2}", svcName, kenanAccountNo, result.ID);
            return result;
        }

        public static Access getuserNameKenan(int regID)
        {
            string userName = string.Empty;
            Access objAccess = null;
            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var query = from rusch in ctx.RegUserSchedulings
                                join rst in ctx.RegStatuses on rusch.RegStatusId equals rst.ID
                                where rst.RegID == regID
                                select rusch;

                    var access = query.ToList().OrderByDescending(e => e.Id).FirstOrDefault().AccessId;

                    objAccess = ctx.Accesses.FirstOrDefault(e => e.ID == access);
                }

            }
            catch (Exception ex)
            {
                objAccess = null;
            }
            return objAccess;
        }

        public static int RegSchedulerCreate(int regId, int accessId)
        {
            Logger.InfoFormat("Entering {0}.RegSchedulerCreate({1})", svcName, regId);
            var value = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var regStatus = ctx.RegStatuses.FirstOrDefault(e => e.RegID == regId && e.Active == true);
                ctx.RegUserSchedulings.Add(new RegUserScheduling
                {
                    CreatedDate = DateTime.Now,
                    AccessId = accessId,
                    RegStatusId = regStatus.ID
                });
                value = ctx.SaveChanges();
            }
            Logger.InfoFormat("Exiting {0}.RegSchedulerCreate()", svcName);
            return value;
        }
        public static int Deleteusedmobilenos(int regid)
        {
            int result = 0;
            if (regid > 0)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var rfM = ctx.Refmobileregused.FirstOrDefault(x => x.Regid == regid);
                    if (rfM != null)
                    {
                        rfM.active = false;
                    }

                    result = ctx.SaveChanges();
                }
            }
            return result;

        }
        public static int RegistrationCancelCreate(RegistrationCancelReason oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCancelCreate(ID: {1})", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.RegistrationCancelReason.Add(oReq);
                ctx.SaveChanges();
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.RegistrationCancelCreate(ID: {1}): {2}", svcName, (oReq != null) ? oReq.ID.ToString2() : "NULL", value);
            return value;
        }
        public static string CancelReasonGet(int regID)
        {
            Logger.InfoFormat("Entering {0}.CancelReasonGet({1})", svcName, regID);
            RegistrationCancelReason objReason = null;
            string value = string.Empty;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objReason = ctx.RegistrationCancelReason.FirstOrDefault(e => e.RegID == regID);
            }
            if (!ReferenceEquals(objReason, null))
                value = objReason.CancelReason;
            Logger.InfoFormat("Exiting {0}.CancelReasonGet({1}): {2}", svcName, regID, value);
            return value;
        }
        public static bool getSupplineDetails(int regID)
        {
            Logger.InfoFormat("Entering {0}.getSupplineDetails({1})", svcName, regID);
            LnkRegDetails objGetDetails = null;
            bool value = false;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objGetDetails = ctx.LnkRegistrationDetails.FirstOrDefault(e => e.RegId == regID);
            }
            if (!ReferenceEquals(objGetDetails, null))
                value = objGetDetails.IsSuppNewAc;
            Logger.InfoFormat("Exiting {0}.getSupplineDetails({1}): {2}", svcName, regID, value);
            return value;
        }

        public static String GetStatusByID(int StatusID)
        {
            Logger.InfoFormat("Entering {0}.GetStatusByID({1})", svcName, (StatusID != null) ? StatusID : 0);
            String value = "";

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Statuses.Where(a => (a.ID==StatusID)).Select(a => a.Description).FirstOrDefault();
            }

            Logger.InfoFormat("Exiting {0}.GetStatusByID({1}): {2}", svcName, (StatusID != null) ? StatusID : 0, value.Count());
            return value;
        }

        public static RegistrationDetails GetRegistrationFullDetails(int regID)
        {
            //Added by Pradeep to combine multiple service calls that are used in MobileRegSummary in Registration Controller
            RegistrationDetails reg = new RegistrationDetails { RegId = regID };
            reg.RegAcc = RegAccountGetByRegID(reg.RegId);
            reg.PaymentStatus = PaymentStatusGet(reg.RegId);
            reg.IsSuppNewAc = getSupplineDetails(reg.RegId);
            reg.ExtraTenDetails = GetExtraTenDetails(reg.RegId);

            List<int> regIds = new List<int> { regID };
            reg.Regs = RegistrationGet(regIds);
            reg.LogDetails = KenanLogDetailsGet(regIds);
            var regStatusID = RegStatusFind(new RegStatusFind()
            {
                RegStatus = new RegStatus()
                {
                    RegID = regID
                },
                Active = true
            }).FirstOrDefault();
            reg.RegStatus = RegStatusGet(new List<int>() { regStatusID });
            reg.CancelReason = CancelReasonGet(regID);
            reg.Customers = CustomerGet(new List<int>() { reg.Regs.FirstOrDefault().CustomerID });
            reg.RegAddresses = AddressGet1(new AddressFind() { Active = true, Address = new Address() { RegID = regID } });
            reg.RegMdlGrpMdls = RegMdlGrpModelGet1(new RegMdlGrpModelFind()
            {
                RegMdlGrpModel = new RegMdlGrpModel()
                {
                    RegID = regID
                },
                Active = true
            });
            reg.RegPgmBdlPkgComps = RegPgmBdlPkgCompGet1(new RegPgmBdlPkgCompFind()
            {
                RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                {
                    RegID = regID
                },
                Active = true
            });
            reg.SuppLines = RegSuppLineGet1(new RegSuppLineFind()
            {
                Active = true,
                RegSuppLine = new RegSuppLine()
                {
                    RegID = regID,
                    Active = true
                }
            });
            reg.lnkregdetails = LnkRegistrationGetByRegId(regID);

            //Secondary details

            reg.RegSec = RegistrationSecGet(regIds).SingleOrDefault();
            if (reg.RegSec != null)
            {
                var regMdlGrpModelIDs = RegMdlGrpModelSecFind(new RegMdlGrpModelSecFind()
                               {
                                   RegMdlGrpModelSec = new RegMdlGrpModelSec()
                                   {
                                       SecRegID = reg.RegSec.ID
                                   },
                                   Active = true
                               });
                if (regMdlGrpModelIDs.Any())
                    reg.RegMdlGrpModelsSec = RegMdlGrpModelSecGet(regMdlGrpModelIDs.ToList()).AsEnumerable();

                var regPgmBdlPkgCompIDsSec = RegPgmBdlPkgCompSecFind(new RegPgmBdlPkgCompSecFind()
                {
                    RegPgmBdlPkgCompSec = new RegPgmBdlPkgCompSec()
                    {
                        RegID = reg.RegSec.ID
                    },
                    Active = true
                });
                if (regPgmBdlPkgCompIDsSec.Any())
                    reg.RegPgmBdlPkgCompsSec = RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec.ToList()).AsEnumerable();
            }
            return reg;
        }

        #region Selected package details
        [DataContract]
        [Serializable]
        public class SelectedPackageDetail
        {
            [DataMember]
            public Int32 packageKenanId { get; set; }
            [DataMember]
            public Int32 PackageId { get; set; }
            [DataMember]
            public string MasterPackageId { get; set; }
            [DataMember]
            public string DataPackageId { get; set; }
            [DataMember]
            public string ExtraPackageId { get; set; }
        }
        public class ExistingPackagePlanType
        {
            [DataMember]
            public string PlanType { get; set; }
        }
        public static ExistingPackagePlanType ExistingPackagePlanTypes(int PackageID)
        {
            List<ExistingPackagePlanType> objIMPOSDetails = null;
            ExistingPackagePlanType objIMPOSDetail = new ExistingPackagePlanType();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<ExistingPackagePlanType>("usp_GetPackageType @packageKenanId", new SqlParameter("@packageKenanId", PackageID)).ToList();
                if (objIMPOSDetails.Any())
                    objIMPOSDetail.PlanType = objIMPOSDetails.LastOrDefault().PlanType;
            }
            return objIMPOSDetail;
        }
        public static SelectedPackageDetail SelectedPackageDetails(int PackageID)
        {
            List<SelectedPackageDetail> objIMPOSDetails = new List<SelectedPackageDetail>();
            SelectedPackageDetail objIMPOSDetail = new SelectedPackageDetail();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objIMPOSDetails = ctx.Database.SqlQuery<SelectedPackageDetail>("usp_GetPackageDetails @PackageId", new SqlParameter("@PackageId", PackageID.ToInt())).ToList();
                foreach (SelectedPackageDetail o in objIMPOSDetails)
                {
                    objIMPOSDetail.PackageId = o.PackageId;
                    objIMPOSDetail.packageKenanId = o.packageKenanId;
                    objIMPOSDetail.MasterPackageId = o.MasterPackageId;
                    objIMPOSDetail.ExtraPackageId = o.ExtraPackageId;
                    objIMPOSDetail.DataPackageId = o.DataPackageId;
                }
            }
            return objIMPOSDetail;
        }

        public static long GetLnkPgmBdlPkgCompIdByKenanCode(string kenanCode, string planType)
        {

            long Id = 0;
            List<int> ids = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ids = ctx.Database.SqlQuery<int>("usp_GetLnkPgmBdlPkgCompIdByKenanCode @KenanCode,@PlanType", new SqlParameter("@KenanCode", kenanCode), new SqlParameter("@PlanType", planType)).ToList();
                if (ids != null && ids.Count > 0)
                    Id = ids[0];
            }
            return Id;
        }
        public static string GetlnkpgmbdlpkgcompidForKenancomponent(string plan_type, int planId, string kenancode)
        {

            string comp = "";
            List<string> ids = new List<string>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ids = ctx.Database.SqlQuery<string>("usp_GetlnkpgmbdlpkgcompidForKenancomponent @plan_type,@planId,@kenancode", new SqlParameter("@plan_type", plan_type), new SqlParameter("@planId", planId), new SqlParameter("@kenancode", kenancode)).ToList();
                if (ids != null && ids.Count > 0)
                    comp = ids[0];
            }
            return comp;
        }
        //Added By Nagaraju 
        public static int RegistrationCreateNew(DAL.Models.Registration oReq, Customer customer, List<Address> addresses, RegStatus regStatus, List<WaiverComponents> waiverComponents, bool isBREFail = false)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCreateNew({1})", svcName, (customer != null) ? customer.FullName : "NULL");
            var value = 0;

            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    using (var trans = new TransactionScope())
                    {
                        ctx.Customers.Add(customer);
                        ctx.SaveChanges();

                        oReq.CustomerID = customer.ID;
                        ctx.Registrations.Add(oReq);
                        ctx.SaveChanges();


                        foreach (var address in addresses)
                        {
                            address.RegID = oReq.ID;
                            ctx.Addresses.Add(address);
                        }
                        ctx.SaveChanges();

                        regStatus.RegID = oReq.ID;
                        ctx.RegStatuses.Add(regStatus);
                        ctx.SaveChanges();

                        if (waiverComponents != null)
                            foreach (var waiverComponent in waiverComponents)
                            {
                                waiverComponent.RegID = oReq.ID;
                                ctx.WaiverComponents.Add(waiverComponent);
                                ctx.SaveChanges();

                            }
                        trans.Complete();
                    }
                }
                //if (isBREFail)
                //    UpdateBRE(oReq.ID, "");
                value = oReq.ID;
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the RegistrationCreate_CRP method:" + ex);

            }

            Logger.InfoFormat("Exiting {0}.RegistrationCreateNew({1}): {2}", svcName, (oReq != null) ? oReq.Customer.FullName : "NULL", value);
            return value;
        }

        #endregion

        #region CRP
        public static List<PackageComponents> PackageComponentsGet(int ID)
        {
            List<PackageComponents> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PackageComponents.Where(a => ID.Equals(a.RegID)).OrderBy(a => a.PackageId).ToList();
            }

            return value;
        }

        public static List<RegSmartComponents> SmartPackageComponentsGet(int ID)
        {
            List<RegSmartComponents> value = null;
            string id = ID.ToString();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegSmartComponents.Where(a => id.Equals(a.RegID)).OrderBy(a => a.PackageID).ToList();
            }

            return value;
        }

        public static List<GetKenancodesByPlanID> PackageCompsGetByLnkPgm(int ID)
        {
            Logger.InfoFormat("Entering {0}.PackageCompsGetByLnkPgm({1})", svcName, ID);
            List<GetKenancodesByPlanID> value = null;
            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    value = ctx.Database.SqlQuery<GetKenancodesByPlanID>("USP_GETKENANCODESPLANID @RegId", new SqlParameter("RegId", ID)).ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the PackageCompsGetByLnkPgm method:" + ex);

            }
            Logger.InfoFormat("Exiting {0}.PackageCompsGetByLnkPgm({1})", svcName, ID);
            return value;
        }



        public static int RegistrationCreate_CRP(DAL.Models.Registration oReq, Customer customer, List<Address> addresses, List<RegPgmBdlPkgComp> regPgmBdlPkgComps, RegMdlGrpModel regMdlGrpModel, RegStatus regStatus, List<RegSuppLine> regSuppLines, List<RegPgmBdlPkgComp> regSuppLineVASes, List<string> CustPhotos, List<PackageComponents> PackageComponents, CMSSComplain CMSS, List<RegSmartComponents> RegSmartComponents, List<WaiverComponents> waiverComponents, bool isBREFail = false)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCreate_CRP({1})", svcName, (customer != null) ? customer.FullName : "NULL");
            var value = 0;
            IEnumerable<int> custvalue = null;
            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    using (var trans = new TransactionScope(TransactionScopeOption.RequiresNew))
                    {
                        /* Modified by Vahini to stored procedure as adding the model using entity framework was throwing error for special IC's whose contact number contains special characters*/
                        custvalue = ctx.Database.SqlQuery<int>("USP_AddCustomers @FullName, @CustomerTitleID, @DateOfBirth, @Gender, @IDCardTypeID, @IDCardNo, @RaceID, @LanguageID, @NationalityID, @ContactNo, @AlternateContactNo, @PostpaidMobileNo, @EmailAddr, @PayModeID, @CardTypeID, @NameOnCard, @CardNo, @CardExpiryDate, @BizRegName, @BizRegNo, @SignatoryName, @PersonInCharge, @Active, @CreateDT, @LastUpdateDT, @LastAccessID, @IsVIP",
                     new SqlParameter("FullName", customer.FullName != null ? customer.FullName : DBNull.Value.ToString()),
         new SqlParameter("CustomerTitleID", customer.CustomerTitleID != null ? customer.CustomerTitleID : DBNull.Value.ToInt()),
         new SqlParameter("DateOfBirth", customer.DateOfBirth != DateTime.MinValue ? customer.DateOfBirth : (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue),
         new SqlParameter("Gender", customer.Gender != null ? customer.Gender : DBNull.Value.ToString()),
         new SqlParameter("IDCardTypeID", customer.IDCardTypeID != null ? customer.IDCardTypeID : DBNull.Value.ToInt()),
         new SqlParameter("IDCardNo", customer.IDCardNo != null ? customer.IDCardNo : DBNull.Value.ToString()),
         new SqlParameter("RaceID", customer.RaceID != null ? customer.RaceID : DBNull.Value.ToInt()),
         new SqlParameter("LanguageID", customer.LanguageID != null ? customer.LanguageID : DBNull.Value.ToInt()),
         new SqlParameter("NationalityID", customer.NationalityID != null ? customer.NationalityID : DBNull.Value.ToInt()),
         new SqlParameter("ContactNo", customer.ContactNo != null ? customer.ContactNo : DBNull.Value.ToString()),
         new SqlParameter("AlternateContactNo", customer.AlternateContactNo != null ? customer.AlternateContactNo : DBNull.Value.ToString()),
         new SqlParameter("PostpaidMobileNo", customer.PostpaidMobileNo != null ? customer.PostpaidMobileNo : DBNull.Value.ToString()),
         new SqlParameter("EmailAddr", customer.EmailAddr != null ? customer.EmailAddr : DBNull.Value.ToString()),
         new SqlParameter("PayModeID", customer.PayModeID != null ? customer.PayModeID : DBNull.Value.ToInt()),
         new SqlParameter("CardTypeID", customer.CardTypeID != null ? customer.CardTypeID : DBNull.Value.ToInt()),
         new SqlParameter("NameOnCard", customer.NameOnCard != null ? customer.NameOnCard : DBNull.Value.ToString()),
         new SqlParameter("CardNo", customer.CardNo != null ? customer.CardNo : DBNull.Value.ToString()),
         new SqlParameter("CardExpiryDate", customer.CardExpiryDate != null ? customer.CardExpiryDate : DBNull.Value.ToString()),
         new SqlParameter("BizRegName", customer.BizRegName != null ? customer.BizRegName : DBNull.Value.ToString()),
         new SqlParameter("BizRegNo", customer.BizRegNo != null ? customer.BizRegNo : DBNull.Value.ToString()),
         new SqlParameter("SignatoryName", customer.SignatoryName != null ? customer.SignatoryName : DBNull.Value.ToString()),
         new SqlParameter("PersonInCharge", customer.PersonInCharge != null ? customer.PersonInCharge : DBNull.Value.ToString()),
         new SqlParameter("Active", customer.Active != null ? customer.Active : false),
         new SqlParameter("CreateDT", customer.CreateDT != null ? customer.CreateDT : DateTime.Now),
         new SqlParameter("LastUpdateDT", customer.LastUpdateDT != null ? customer.LastUpdateDT : DateTime.Now),
         new SqlParameter("LastAccessID", customer.LastAccessID != null ? customer.LastAccessID : DBNull.Value.ToString()),
         new SqlParameter("IsVIP", customer.IsVIP != null ? customer.IsVIP : DBNull.Value.ToInt())).ToList();
                        //ctx.Customers.Add(customer);
                        //ctx.SaveChanges();
                        /* Modified by Vahini to stored procedure as adding the model using entity framework was throwing error for special IC's whose contact number contains special characters*/


                        try
                        {
                            oReq.CustomerID = custvalue.Single();

                            ctx.Registrations.Add(oReq);
                            ctx.SaveChanges();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }

                        if (regMdlGrpModel.ModelImageID.ToInt() != 0)
                        {
                            regMdlGrpModel.RegID = oReq.ID;
                            ctx.RegMdlGrpModels.Add(regMdlGrpModel);
                            ctx.SaveChanges();
                        }

                        foreach (var address in addresses)
                        {
                            address.RegID = oReq.ID;
                            ctx.Addresses.Add(address);
                        }
                        ctx.SaveChanges();

                        foreach (var regPBPC in regPgmBdlPkgComps)
                        {
                            regPBPC.RegID = oReq.ID;
                            ctx.RegPgmBdlPkgComps.Add(regPBPC);
                        }
                        ctx.SaveChanges();

                        regStatus.RegID = oReq.ID;
                        ctx.RegStatuses.Add(regStatus);
                        ctx.SaveChanges();

                        foreach (var regSubline in regSuppLines)
                        {
                            regSubline.RegID = oReq.ID;
                            ctx.RegSuppLines.Add(regSubline);
                            ctx.SaveChanges();

                            var vases = regSuppLineVASes.Where(a => a.SequenceNo == regSubline.SequenceNo).ToList();
                            foreach (var vas in vases)
                            {
                                vas.RegID = oReq.ID;
                                vas.RegSuppLineID = regSubline.ID;
                                ctx.RegPgmBdlPkgComps.Add(vas);
                                ctx.SaveChanges();
                            }
                        }

                        foreach (var package in PackageComponents)
                        {
                            package.RegID = oReq.ID;
                            ctx.PackageComponents.Add(package);
                            ctx.SaveChanges();
                        }

                        //Added by KD for inserting CMSS ID's 
                        if (!string.IsNullOrEmpty(CMSS.CMSSID))
                        {
                            CMSS.RegID = oReq.ID;
                            CMSS.CreateDate = DateTime.Now;
                            ctx.CMSSID.Add(CMSS);
                            ctx.SaveChanges();
                        }
                        foreach (var package in RegSmartComponents)
                        {
                            package.RegID = oReq.ID.ToString();
                            ctx.RegSmartComponents.Add(package);
                            ctx.SaveChanges();
                        }

                        if (waiverComponents != null)
                            foreach (var waiverComponent in waiverComponents)
                            {
                                waiverComponent.RegID = oReq.ID;
                                ctx.WaiverComponents.Add(waiverComponent);
                                ctx.SaveChanges();

                            }
                        trans.Complete();
                    }
                }

                value = oReq.ID;

                //if (isBREFail)
                //{
                //    UpdateBRE(oReq.ID, "");
                //}
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the RegistrationCreate_CRP method:" + ex);

            }

            Logger.InfoFormat("Exiting {0}.RegistrationCreate_CRP({1}): {2}", svcName, (oReq.Customer == null) ? customer.FullName : "NULL", value);
            return value;
        }



        public static bool isCRPregistration(int regid)
        {
            bool isCRPreg = false;
            Logger.InfoFormat("Entering {0}.isCRPregistration({1})", svcName, regid);

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var details = ctx.LnkRegistrationDetails.Where(a => regid.Equals(a.RegId) && a.isDeviceCRP == true).ToList();
                if (details.Count > 0)
                    isCRPreg = true;
            }

            Logger.InfoFormat("Exiting {0}.isCRPregistration({1})", svcName, regid);
            return isCRPreg;

        }
        public static List<RegSmartComponents> CRPPackageComponentsGet(int ID)
        {
            List<RegSmartComponents> value = null;
            string id = ID.ToString();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegSmartComponents.Where(a => id.Equals(a.RegID)).OrderBy(a => a.PackageID).ToList();
            }

            return value;
        }
        public static List<Contracts> GetActiveContractsList()
        {

            List<Contracts> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Contracts.ToList();
            }

            return value;
        }

        #endregion



        #region "Relocation"
        public static RegistrationRequestType GetRegistrationRequestTypebyRegId(int Regid)
        {
            Logger.InfoFormat("Entering {0}.GetRegistrationRequestTypebyRegId()", svcName);
            RegistrationRequestType response;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                response = (from reginstall in ctx.RegistrationRequestTypes
                            where reginstall.RegId == Regid
                            orderby reginstall.Id descending
                            select reginstall).FirstOrDefault();
            }
            Logger.InfoFormat("Exiting {0}.GetRegistrationRequestTypebyRegId({1})", svcName, Regid);
            return response;
        }
        public static RegistrationRequestType GetRegistrationRequestTypebyRegId(int Regid, string LOB)
        {
            Logger.InfoFormat("Entering {0}.GetRegistrationRequestTypebyRegId()", svcName);
            RegistrationRequestType response;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                response = (from reginstall in ctx.RegistrationRequestTypes
                            where reginstall.RegId == Regid && reginstall.Lob == LOB
                            orderby reginstall.Id descending
                            select reginstall).FirstOrDefault();
            }


            Logger.InfoFormat("Exiting {0}.GetRegistrationRequestTypebyRegId({1},{2})", svcName, Regid, LOB);
            return response;
        }
        #endregion

        #region MISM
        public static int UpdateSecondaryRequestSent(RegistrationSec regSec)
        {
			int result = 0;
            if (regSec.ID > 0)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var rfM = ctx.RegistrationSecs.FirstOrDefault(x => x.ID == regSec.ID);
                    if (rfM != null)
                    {
                        rfM.IsSecondaryRequestSent = true;
						rfM.KenanAccountNo = !string.IsNullOrEmpty(regSec.KenanAccountNo) ? regSec.KenanAccountNo : string.Empty;
                    }

                    result = ctx.SaveChanges();
                }
            }
            return result;

        }


        public static int UpdateSecondarySimSerial(int regid, string sim)
        {
            int result = 0;
            if (regid > 0)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var rfM = (RegistrationSec)ctx.RegistrationSecs.FirstOrDefault(x => x.RegistrationID == regid);
                    if (rfM != null)
                    {
                        rfM.SIMSerial = sim;
                    }
                    result = ctx.SaveChanges();
                }
            }
            return result;

        }

        #endregion

        #region IsellHome AccountManagement
        public static string GetDealerCodebyRegid(int regid)
        {
            var result = string.Empty;
            using (var ctx = new OnlineStoreDbContext())
            {
                // Instantiate the regular expression object.
                string expression = @"(^|\s)[A-Za-z0-9]{5}.[A-Za-z0-9]{5}(\s|$)";
                var r = new Regex(expression, RegexOptions.IgnoreCase);


                var regist = (from reg in ctx.Registrations
                              where reg.ID == regid
                              select reg).FirstOrDefault();

                // Match the regular expression pattern against a text string.
                if (regist != null)
                {
                    result = (from o in ctx.Organizations
                              where o.ID == regist.CenterOrgID
                              select o.DealerCode).FirstOrDefault();
                }

            }
            return result;
        }
        #endregion

        public static RegPreviousDetail RegPreviousDetailGet(int regId)
        {
            RegPreviousDetail RegPreviousDetail = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                RegPreviousDetail = ctx.RegPreviousDetails.FirstOrDefault(e => e.RegId == regId);
            }
            return RegPreviousDetail;
        }

        public static List<long> selectusedmobilenos()
        {
            List<long> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Refmobileregused.Where(n => n.active).Select(n => n.mobileno).ToList();
            }
            return value;
        }

        public static int Insertusedmobilenos(int regid, long mobileno, int active)
        {
            int result = 0;
            if (regid > 0)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var rfM = ctx.Refmobileregused.FirstOrDefault(x => x.Regid == regid);
                    if (rfM != null)
                    {
                        rfM.mobileno = mobileno;
                        rfM.active = Convert.ToBoolean(active);
                    }
                    else
                    {
                        Refmobileregused RefM = new Refmobileregused();
                        RefM.Regid = regid;
                        RefM.mobileno = mobileno;
                        RefM.active = Convert.ToBoolean(active);
                        ctx.Refmobileregused.Add(RefM);
                    }
                    result = ctx.SaveChanges();

                }
            }
            return result;

        }

        public static RetrieveAccountInfo GetRetrieveAccountInfo(int regid, string lobtype)
        {
            Logger.InfoFormat("Entering {0}.GetRetrieveAccountInfo({1})", svcName, regid);
            RetrieveAccountInfo oReq;

            using (var ctx = new OnlineStoreDbContext())
            {
                oReq = ctx.RetrieveAccountInfos.FirstOrDefault(a => a.RegId == regid && a.Lob == lobtype);
            }

            Logger.InfoFormat("Exiting {0}.InsertRetrieveAccountInfo({1})", svcName, regid);
            return oReq;
        }

        public static List<string> GetCompDescByRegid(int regID)
        {
            Logger.InfoFormat("Entering {0}.GetCompDescByRegid({1})", svcName, regID);
            List<string> value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<string>("usp_getCompDescByRegid @regid", new System.Data.SqlClient.SqlParameter("@regid", regID)).ToList();

            }
            Logger.InfoFormat("Exiting {0}.GetCompDescByRegid({1})", svcName, regID);
            return value;
        }

        //Method added by ravi to check whether the customer is whitelist or not
        public static int CheckWhiteListCustomer(string icNo)
        {
            List<int> obj = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                obj = ctx.Database.SqlQuery<int>("USP_CheckWhiteListCustomer @IcNo", new SqlParameter("@IcNo", icNo)).ToList();
            }

            return obj.FirstOrDefault();
        }

        public static List<Online.Registration.DAL.Models.lnkofferuomarticleprice> GetPromotionalOffers(int whiteListId)
        {
            List<Online.Registration.DAL.Models.lnkofferuomarticleprice> obj = new List<Online.Registration.DAL.Models.lnkofferuomarticleprice>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                obj = ctx.Database.SqlQuery<Online.Registration.DAL.Models.lnkofferuomarticleprice>("USP_GetPromotionalOffers @WhiteListId", new SqlParameter("@WhiteListId", whiteListId)).ToList();
            }

            return obj;
        }

        public static Online.Registration.DAL.Models.lnkofferuomarticleprice GetOfferDetailsByID(int OfferID)
        {
            Online.Registration.DAL.Models.lnkofferuomarticleprice respobj = new lnkofferuomarticleprice();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                respobj = ctx.lnkofferuomarticleprice.Where(a => a.Id == OfferID).FirstOrDefault();
            }
            return respobj;
        }

        public static Online.Registration.DAL.Models.PlanDetailsandContract GetPlanDetailsbyOfferId(int offerId)
        {
            List<Online.Registration.DAL.Models.PlanDetailsandContract> obj = new List<Online.Registration.DAL.Models.PlanDetailsandContract>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                obj = ctx.Database.SqlQuery<Online.Registration.DAL.Models.PlanDetailsandContract>("USP_GetPlanDetailsbyOfferId @OfferId", new SqlParameter("@OfferId", offerId)).ToList();
            }

            return obj.SingleOrDefault();
        }

        //end of new method

        public static int GetMarketCodeIdByRegid(int regid)
        {
            int marketCodeID = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                marketCodeID = ctx.Database.SqlQuery<int>("Usp_GetMarketCodeIdByRegid @Regid", new SqlParameter("@Regid", regid)).ToList().FirstOrDefault();
            }

            return marketCodeID;
        }

        #region Added for Checking Data Plans for MISM
        public static List<string> DataPlanComponents()
        {
            List<string> components = new List<string>();
            List<tblDataPlanComponents> value = new List<tblDataPlanComponents>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.DataPlanComponents.Where(a => a.Status == true).ToList();
            }

            if (value.Count > 0)
            {
                for (int plancomponent = 0; plancomponent <= value.Count - 1; plancomponent++)
                {
                    components.Add(value[plancomponent].DataComponentID);
                }
            }
            return components;
        }
        #endregion

        /// <summary>
        /// Code Added by Sindhu for Print Version on 11/09/2013
        /// </summary>
        /// <param name="versionNo"></param>
        /// <param name="regType"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        /// Plan kenan code argument added - sumit
        public static PrintVersion GetPrintTsCsInfo(string versionNo, int regType, string ArticleID, string PlanKenanCode, string trn_Type = null)
        {
            PrintVersion value = null;
            int BranID = 0;
            string strBranID = string.Empty;
            BranID = getBrandIDbyArticleID(ArticleID);
            strBranID = Convert.ToString(BranID);
            using (var ctx = new OnlineStoreDbContext())
            {
                if (string.IsNullOrEmpty(trn_Type))
                {
                    var query = from e in ctx.PrintVersion where e.Version == versionNo && e.RegType == regType select e;
                    //if (!string.IsNullOrEmpty(ArticleID))
                    
                    #region get device financing term and condition for apple and non apple brand
                    if (versionNo == "V0.DF" && strBranID == "11")
                    {
                        value = query.Where(e => e.Brand == strBranID).FirstOrDefault();
                        return value;
                    }

                    if (versionNo == "V0.DF" && strBranID != "11")
                    {
                        value = query.Where(e => e.Brand == "0").FirstOrDefault();
                        return value;
                    }
                    #endregion
                    
                    if (BranID != 0)
                    {
                        if (PlanKenanCode != null && !string.IsNullOrEmpty(PlanKenanCode))
                        {
                            query = query.Where(e => e.Brand == strBranID && e.PlanKenanCode.Contains(PlanKenanCode) && e.Trn_Type == null);
                        }
                        else
                        {
                            query = query.Where(e => e.Brand == strBranID);
                        }
                    }

                    //PN2391 - Fix to populate the general T&C infomation by default - End
                    if (query.Count() == 0)
                    {
                        query = from e in ctx.PrintVersion where e.IsDefault == true && e.isActive == true select e;
                        value = query.FirstOrDefault();
                    }
                    else
                    {
                        value = query.FirstOrDefault();
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(versionNo))
                    {
                        var query = from e in ctx.PrintVersion where e.IsDefault == true && e.Trn_Type == "s" && e.isActive == true select e;
                        value = query.FirstOrDefault();   
                    }
                    else
                    {
                        var query = from e in ctx.PrintVersion where e.Version == versionNo && e.Trn_Type == "s" && e.Brand == strBranID && e.PlanKenanCode.Contains(PlanKenanCode) select e;
                        value = query.Select(e => e).SingleOrDefault();
                    }
                }
            }
            return value;
        }

        /// <summary>
        /// Code Added by Sindhu for Print Version on 11/09/2013
        /// </summary>
        /// <returns></returns>
        public static string GetPrintVersionNum(int regType, string brand, string PlanKenanCode, string trn_Type = null)
        {
            string value = string.Empty;
            string strBranID = string.Empty;
            string ModelID = string.Empty;
            int BranID = 0;
            BranID = getBrandIDbyArticleID(brand);
            strBranID = Convert.ToString(BranID);

            if (regType == 9 && !ReferenceEquals(strBranID, null) && BranID > 0)
            {
                /*
                    Anthony: [#2391] - T&C for Newline + Device is missing
                    regtype 9 (New Line) is a new flow that support device, so there is no T&C for specific brand ID
                    so I use regtype 2 (Device Plan) data if the order type is New Line with Device instead of inserting new row
                */
                regType = 2;
            }

            using (var ctx = new OnlineStoreDbContext())
            {
                if (!string.IsNullOrEmpty(trn_Type))
                {
                    var query = from e in ctx.PrintVersion where e.isActive == true && e.RegType == regType && e.Trn_Type == "S" && e.Brand == strBranID && e.PlanKenanCode.Contains(PlanKenanCode) select e;
                    value = query.Select(e => e.Version).SingleOrDefault();
                }
                else
                {
                    //For Mobility default
                    var query = from e in ctx.PrintVersion where e.isActive == true && e.RegType == regType && e.PlanKenanCode.Contains(PlanKenanCode) && e.Trn_Type == null select e;
                    if (!string.IsNullOrEmpty(brand))
                    {
                        query = query.Where(e => e.Brand == strBranID);
                    }
                    value = query.OrderByDescending(x => x.ID).Select(e => e.Version).FirstOrDefault();
                }
            }

            return value;
        }

        /// <summary>
        /// Code Added by Sindhu for Print Version on 11/09/2013
        /// </summary>
        /// <param name="regID"></param>
        /// <returns></returns>
        public static string GetPrintVersionNoByRegid(int regID)
        {
            Logger.InfoFormat("Entering {0}.GetPrintVersionNoByRegid({1})", svcName, regID);
            LnkRegDetails objGetDetails = null;
            string value = "";

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objGetDetails = ctx.LnkRegistrationDetails.Where(e => e.RegId == regID && !string.IsNullOrEmpty(e.PrintVersionNo)).FirstOrDefault();
            }
            if (!ReferenceEquals(objGetDetails, null))
                value = objGetDetails.PrintVersionNo;
            Logger.InfoFormat("Exiting {0}.GetPrintVersionNoByRegid({1}): {2}", svcName, regID, value);
            return value;
        }

        public static int getBrandIDbyArticleID(string ArticleID)
        {
            Logger.InfoFormat("Entering {0}.getBrandIDbyArticleID({1})", svcName, ArticleID);
            int BrandID = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                BrandID = ctx.Database.SqlQuery<int>("sp_getBrandIDbyArticleID @ArticleID", new SqlParameter("@ArticleID", ArticleID)).SingleOrDefault();

            }
            Logger.InfoFormat("Exiting {0}.getRegNRICId({1}): ", svcName, ArticleID);
            return BrandID;
        }
        public static int GetBrandModelIDbyArticleID(string ArticleID)
        {
            Logger.InfoFormat("Entering {0}.GetBrandModelIDbyArticleID({1})", svcName, ArticleID);
            int BrandModelID = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                BrandModelID = ctx.Database.SqlQuery<int>("sp_getBrandModelIDbyArticleID @ArticleID", new SqlParameter("@ArticleID", ArticleID)).SingleOrDefault();

            }
            Logger.InfoFormat("Exiting {0}.GetBrandModelIDbyArticleID({1}): ", svcName, ArticleID);
            return BrandModelID;
        }
        public static List<WaiverComponents> GetWaiverComponents(int PackageID)
        {
            List<WaiverComponents> WaiverComponents = new List<WaiverComponents>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                WaiverComponents = ctx.Database.SqlQuery<WaiverComponents>("USP_GetWaiverComponents @packageId", new SqlParameter("@packageId", PackageID)).ToList();
            }
            return WaiverComponents;
        }
        public static List<DAL.Models.WaiverComponents> GetWaiverComponentsByRegID(int regID)
        {

            List<DAL.Models.WaiverComponents> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.WaiverComponents.Where(a => a.RegID == regID).ToList();
            }
            return value;
        }

        #region Supervisor waive off

        public static List<SearchResultForSuperVisorWaieverOff> SupervisorWaiveOffSearch(RegistrationSearchCriteriaForWaiverOff criteria)
        {
            Logger.InfoFormat("Entering {0}.SupervisorWaiveOffSearch({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            List<SearchResultForSuperVisorWaieverOff> result = new List<SearchResultForSuperVisorWaieverOff>();

            int count = 3000;
            if (criteria.SearchType == 0)
            {
                //Quick search
                if (criteria.RegID == null) criteria.RegID = "0";
                if (criteria.MSISDN == null) criteria.MSISDN = "";
                if (criteria.OrgId == null) criteria.OrgId = 0;
                if (criteria.TransactionStatus == null) criteria.TransactionStatus = "0";
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    var objectContext = (ctx as System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext;

                    // Sets the command timeout for all the commands
                    objectContext.CommandTimeout = 60 * 4;

                    if (criteria.isCDPU)
                    {
                        // drop 5 - cdpu dashboard
                        result = ctx.Database.SqlQuery<SearchResultForSuperVisorWaieverOff>("USP_GetBREWaiverDetailsforQuickSearch_CDPU @RegId,@MSISDN,@ShowAll,@OrgId", new SqlParameter("@RegId", criteria.RegID.ToInt()), new SqlParameter("@MSISDN", criteria.MSISDN), new SqlParameter("@ShowAll", criteria.ShowAllRecords), new SqlParameter("@OrgId", criteria.OrgId)).Take(count).ToList();
                    }
                    else
                    {
                        //result = ctx.Database.SqlQuery<SearchResultForSuperVisorWaieverOff>("USP_GetBREWaiverDetailsforQuickSearch 0,'',0,0").ToList();
                        result = ctx.Database.SqlQuery<SearchResultForSuperVisorWaieverOff>("USP_GetBREWaiverDetailsforQuickSearch @RegId,@MSISDN,@ShowAll,@OrgId", new SqlParameter("@RegId", criteria.RegID.ToInt()), new SqlParameter("@MSISDN", criteria.MSISDN), new SqlParameter("@ShowAll", criteria.ShowAllRecords), new SqlParameter("@OrgId", criteria.OrgId)).Take(count).ToList();
                    }
                }
            }
            else if (criteria.SearchType == 1)
            {
                //Advanced search
                if (criteria.RegID == null) criteria.RegID = "0";
                if (criteria.MSISDN == null) criteria.MSISDN = "";
                if (criteria.OrgId == null) criteria.OrgId = 0;
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    if (criteria.CreatedBy == null) criteria.CreatedBy = "";
                    if (criteria.AcknowledgedBy == null) criteria.AcknowledgedBy = "";
                    if (criteria.RegTypeId == null) criteria.RegTypeId = 0;
                    if (criteria.RegDateFrom == null) criteria.RegDateFrom = new DateTime(1900, 01, 01);
                    if (criteria.RegDateTo == null) criteria.RegDateTo = DateTime.Now;
                    if (criteria.Approver == null) criteria.Approver = "";

                    if (criteria.isCDPU)
                    {
                        // drop 5 - cdpu dashboard
                        result = ctx.Database.SqlQuery<SearchResultForSuperVisorWaieverOff>("USP_GetBREWaiverDetailsforAdvanceSearch_CDPU @CreatedBY,@TransactionStatus,@RegTypeId,@FromDt,@ToDt,@OrgId,@Approver", new SqlParameter("@CreatedBY", criteria.CreatedBy), new SqlParameter("@TransactionStatus", criteria.TransactionStatus), new SqlParameter("@RegTypeId", criteria.RegTypeId), new SqlParameter("@FromDt", criteria.RegDateFrom), new SqlParameter("@ToDt", criteria.RegDateTo), new SqlParameter("@OrgId", criteria.OrgId), new SqlParameter("@Approver", criteria.Approver)).ToList();
                    }
                    else
                    {
                        result = ctx.Database.SqlQuery<SearchResultForSuperVisorWaieverOff>("USP_GetBREWaiverDetailsforAdvanceSearch @CreatedBY,@AcknowledgedBy,@RegTypeId,@FromDt,@ToDt,@OrgId,@Approver", new SqlParameter("@CreatedBY", criteria.CreatedBy), new SqlParameter("@AcknowledgedBy", criteria.AcknowledgedBy), new SqlParameter("@RegTypeId", criteria.RegTypeId), new SqlParameter("@FromDt", criteria.RegDateFrom), new SqlParameter("@ToDt", criteria.RegDateTo), new SqlParameter("@OrgId", criteria.OrgId), new SqlParameter("@Approver", criteria.Approver)).ToList();
                    }
                }

                if (criteria.TrnTypeId == 1)
                {
                    //waiver
                    result = result.Where(res => res.WaiverDetails.Length > 0).ToList();
                }
                else if (criteria.TrnTypeId == 2)
                {
                    // bre fails
                    result = result.Where(res => res.FailedBREDetails.Length > 0).ToList();
                }
                else if (criteria.TrnTypeId == 3)
                {
                    // bre fails
                    result = result.Where(res => res.trnStatus == "REG_VOID").ToList();
                }
                else if (criteria.TrnTypeId == 4)
                {
                    // bre fails
                    result = result.Where(res => res.trnStatus == "REG_RETURN").ToList();
                }

            }

            Logger.InfoFormat("Exiting {0}.SupervisorWaiveOffSearch({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
            return result;
        }

        public static List<SearchResultForSuperVisorWaieverOff> SupervisorWaiveOffSearchForFailedOrders(RegistrationSearchCriteriaForWaiverOff criteria)
        {
            Logger.InfoFormat("Entering {0}.SupervisorWaiveOffSearchForFailedOrders({1})", svcName, (criteria != null) ? criteria.RegID : "NULL");
            List<SearchResultForSuperVisorWaieverOff> result = new List<SearchResultForSuperVisorWaieverOff>();

            using (var context = new OnlineStoreDbContext())
            {
                var orgTypeViewCertain = Properties.Settings.Default.ViewCertainRoles.Split(',').ToList();

                if (criteria != null)
                {


                    if (criteria.SearchType == 0)
                    {
                        //Quick search
                        if (criteria.RegID == null) criteria.RegID = "0";
                        if (criteria.MSISDN == null) criteria.MSISDN = "";
                        if (criteria.IDNO == null) criteria.IDNO = "";
                        if (criteria.OrgId == null) criteria.OrgId = 0;
                        using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                        {

                            result = ctx.Database.SqlQuery<SearchResultForSuperVisorWaieverOff>("USP_GetFailedOrdersforQuickSearch @RegId,@MSISDN,@IdCardNo,@OrgId", new SqlParameter("@RegId", criteria.RegID.ToInt()), new SqlParameter("@MSISDN", criteria.MSISDN), new SqlParameter("@IdCardNo", criteria.IDNO), new SqlParameter("@OrgId", criteria.OrgId)).ToList();
                        }

                    }

                    else if (criteria.SearchType == 1)
                    {
                        //Advanced search                       
                        using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                        {
                            if (criteria.CreatedBy == null) criteria.CreatedBy = "";
                            if (criteria.RegTypeId == null) criteria.RegTypeId = 0;
                            if (criteria.RegDateFrom == null) criteria.RegDateFrom = new DateTime(1900, 01, 01);
                            if (criteria.RegDateTo == null) criteria.RegDateTo = DateTime.Now;
                            if (criteria.OrgId == null) criteria.OrgId = 0;
                            result = ctx.Database.SqlQuery<SearchResultForSuperVisorWaieverOff>("USP_GetFailedOrdersforAdvancedSearch @CreatedBY,@RegTypeId,@FromDt,@ToDt,@OrgId", new SqlParameter("@CreatedBY", criteria.CreatedBy), new SqlParameter("@RegTypeId", criteria.RegTypeId), new SqlParameter("@FromDt", criteria.RegDateFrom.GetValueOrDefault()), new SqlParameter("@ToDt", criteria.RegDateTo.GetValueOrDefault()), new SqlParameter("@OrgId", criteria.OrgId)).ToList();
                        }
                    }

                }
            }

            Logger.InfoFormat("Exiting {0}.SupervisorWaiveOffSearchForFailedOrders({1}): {2}", svcName, (criteria != null) ? criteria.RegID : "NULL", result.Count());
            return result;
        }

        public static List<string> GetCreatedUsersForWaiveOff(int OrgId)
        {
            List<string> createdUsers = new List<string>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                try
                {
                    if (OrgId == null) OrgId = 0;
                    createdUsers = ctx.Database.SqlQuery<string>("Usp_GetCreatedUsersForWaiveOff @OrgId", new SqlParameter("@OrgId", OrgId)).ToList();

                }
                catch (Exception ex)
                {


                }

            }

            return createdUsers;
        }

        public static List<string> GetAcknowledgedUsersForWaiveOff(int OrgId,bool isCDPU)
        {
            List<string> createdUsers = new List<string>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                try
                {
                    if (OrgId == null) OrgId = 0;
                    if (isCDPU) { createdUsers = ctx.Database.SqlQuery<string>("Usp_GetSupervisorUsersForWaiveOff_CDPU @OrgId", new SqlParameter("@OrgId", OrgId)).ToList(); }
                    else { createdUsers = ctx.Database.SqlQuery<string>("Usp_GetSupervisorUsersForWaiveOff @OrgId", new SqlParameter("@OrgId", OrgId)).ToList(); }

                }
                catch (Exception ex)
                {


                }

            }

            return createdUsers;
        }

        public static int AcknowledgedUsersForWaiveOffUpdate(List<RegDetails> details)
        {
            int num = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                try
                {
                    foreach (var item in details)
                    {
                        var detail = (from s in ctx.LnkRegistrationDetails
                                      where s.RegId == item.RegID
                                      select s);
                        foreach (var dtl in detail)
                        {
                            dtl.Isacknowledged = item.Isacknowledged;
                            dtl.AcknowledgedBy = item.AcknowledgedBy;
                        }

                        num = ctx.SaveChanges();
                    }

                }
                catch (Exception)
                {

                    num = -1;
                }
            }

            return num;

        }

        #endregion

		public static int uploadDoc (RegUploadDoc details)
		{
			int num = 0;
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_UPLOADDOC", sqlConn);

                if (details.RegID > 0)
                    sqlCmd.Parameters.Add(new SqlParameter("@regid", details.RegID));

                if (!string.IsNullOrEmpty(details.FileName.ToString2()))
                    sqlCmd.Parameters.Add(new SqlParameter("@fileName", details.FileName));

                if (!string.IsNullOrEmpty(details.FileType.ToString2()))
                    sqlCmd.Parameters.Add(new SqlParameter("@fileType", details.FileType));

                if (!ReferenceEquals(details.FileStream,null))
                    sqlCmd.Parameters.Add(new SqlParameter("@fileStream", details.FileStream));

                sqlCmd.Parameters.Add(new SqlParameter("@date", DateTime.Now));

                sqlCmd.CommandType = CommandType.StoredProcedure;
                num = sqlCmd.ExecuteNonQuery();
                sqlConn.Close();
            }
            //using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            //{
            //    try
            //    {
            //        ctx.RegUploadDoc.Add(details);
            //        ctx.SaveChanges();
            //        num = 1;
            //    }
            //    catch (Exception)
            //    {

            //        num = -1;
            //    }
            //}

			return num;

		}



        public static bool IsCallConferenceWaived(int RegID)
        {
            return isComponentWaivedOff(RegID, 1);
        }

        public static bool IsInternationalRoamingWaived(int RegID)
        {
            return isComponentWaivedOff(RegID, 2);
        }

        private static bool isComponentWaivedOff(int regID, int compVAL)
        {
            bool isWaived = false;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var value = ctx.Database.SqlQuery<int>("USP_ISCOMPONENTWAIVEDOFF @REGID,@COMPVAL", new System.Data.SqlClient.SqlParameter("@REGID", regID), new System.Data.SqlClient.SqlParameter("@COMPVAL", compVAL)).ToList();
                isWaived = Convert.ToBoolean(value.FirstOrDefault());
            }
            return isWaived;
        }

        public static List<DAL.Models.UserSpecificPrintersInfo> GetUserSpecificPrinters(int UserID, int OrgID)
        {
            List<DAL.Models.UserSpecificPrintersInfo> printersList = new List<DAL.Models.UserSpecificPrintersInfo>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                printersList = ctx.Database.SqlQuery<DAL.Models.UserSpecificPrintersInfo>("USP_GETUSERSPECIFICPRINTERS @orgId", new SqlParameter("@orgId", OrgID)).ToList();
            }
            return printersList;

        }
        public static List<DAL.Models.UserPrintLog> GetPrintersLog(string File)
        {
            List<DAL.Models.UserPrintLog> printersList = new List<DAL.Models.UserPrintLog>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                printersList = ctx.UserPrintLogInfo.Where(a => a.Printfile == File).ToList();
            }


            return printersList;

        }

        public static int SaveUserPrintLog(UserPrintLog objUserPrintLog)
        {

            bool isSuccesss = true;
            List<int> value = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("USP_SavePrintLog @UserID,@Orgid,@Printfile,@PrinterName,@status,@remarks,@PrinterNetworkPath,@RegID",
                    new SqlParameter("@UserID", objUserPrintLog.UserID), new SqlParameter("@Orgid", objUserPrintLog.Orgid), new SqlParameter("@Printfile", objUserPrintLog.Printfile),
                    new SqlParameter("@PrinterName", objUserPrintLog.PrinterName), new SqlParameter("@status", objUserPrintLog.status), new SqlParameter("@remarks", objUserPrintLog.remarks),
                    new SqlParameter("@PrinterNetworkPath", objUserPrintLog.PrinterNetworkPath), new SqlParameter("@RegID", objUserPrintLog.RegID)
                    ).ToList();

            }
            return value.First();

        }

        //added to get the last user access id based on the registration id 
        public static RegStatus LastAccessIDGetByRegID(int RegID)
        {
            Logger.InfoFormat("Entering {0}.LastAccessIDGetByRegID({1})", svcName, RegID);
            RegStatus value = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegStatuses.Where(a => a.StatusID == 36 && a.RegID == RegID).FirstOrDefault(); ;
            }
            Logger.InfoFormat("Exiting {0}.LastAccessIDGetByRegID({1})", svcName, RegID);

            return value;
        }

        public static List<PgmBdlPckComponent> RegPgmBdlPkgComponentGetByKenanCode(string strKenanCode)
        {
            List<PgmBdlPckComponent> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.PgmBdlPckComponents.Where(a => a.AlternateKenanCode == strKenanCode).ToList();
            }
            return value;
        }

        public static List<KenanComponentCodeInfo> GetRegDetails(int regID)
        {
            List<KenanComponentCodeInfo> componentKenanDetails = new List<KenanComponentCodeInfo>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                componentKenanDetails = ctx.Database.SqlQuery<KenanComponentCodeInfo>("USP_GETMXSDOMINATEKENANCODE @REGID", new SqlParameter("@REGID", regID)).ToList();
            }
            return componentKenanDetails;
        }


        #region smart related methods

        public static string SmartRebateIDGet(string Rebate)
        {
            List<SmartRebates> smart = new List<SmartRebates>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                smart = ctx.SmartRebate.Where(a => Rebate.Equals(a.Rebate)).ToList();
            }

            return smart[0].Discount_ID.ToString();
        }

        public static int RegistrationCreate_Smart(DAL.Models.Registration oReq, Customer customer, List<Address> addresses, List<RegPgmBdlPkgComp> regPgmBdlPkgComps, RegMdlGrpModel regMdlGrpModel,
                                            RegStatus regStatus, List<RegSuppLine> regSuppLines, List<RegPgmBdlPkgComp> regSuppLineVASes, List<string> CustPhotos, List<PackageComponents> PackageComponents, CMSSComplain CMSS, List<RegSmartComponents> RegSmartComponents, List<WaiverComponents> RegWaiverComponents)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCreate({1})", svcName, (customer != null) ? customer.FullName : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                using (var trans = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    ctx.Customers.Add(customer);
                    ctx.SaveChanges();
                    try
                    {
                        oReq.CustomerID = customer.ID;
                        ctx.Registrations.Add(oReq);
                        ctx.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }

                    if (regMdlGrpModel.ModelImageID.ToInt() != 0)
                    {
                        regMdlGrpModel.RegID = oReq.ID;
                        ctx.RegMdlGrpModels.Add(regMdlGrpModel);
                        ctx.SaveChanges();
                    }

                    foreach (var address in addresses)
                    {
                        address.RegID = oReq.ID;
                        ctx.Addresses.Add(address);
                    }
                    ctx.SaveChanges();

                    foreach (var regPBPC in regPgmBdlPkgComps)
                    {
                        regPBPC.RegID = oReq.ID;
                        ctx.RegPgmBdlPkgComps.Add(regPBPC);
                    }

                    ctx.SaveChanges();

                    regStatus.RegID = oReq.ID;
                    ctx.RegStatuses.Add(regStatus);
                    ctx.SaveChanges();

                    foreach (var regSubline in regSuppLines)
                    {
                        regSubline.RegID = oReq.ID;
                        ctx.RegSuppLines.Add(regSubline);
                        ctx.SaveChanges();

                        var vases = regSuppLineVASes.Where(a => a.SequenceNo == regSubline.SequenceNo).ToList();
                        foreach (var vas in vases)
                        {
                            vas.RegID = oReq.ID;
                            vas.RegSuppLineID = regSubline.ID;
                            ctx.RegPgmBdlPkgComps.Add(vas);
                            ctx.SaveChanges();
                        }
                    }

                    foreach (var package in PackageComponents)
                    {
                        package.RegID = oReq.ID;
                        ctx.PackageComponents.Add(package);
                        ctx.SaveChanges();
                    }

                    //Added by KD for inserting CMSS ID's 
                    CMSS.RegID = oReq.ID;
                    CMSS.CreateDate = DateTime.Now;
                    ctx.CMSSID.Add(CMSS);
                    ctx.SaveChanges();

                    foreach (var package in RegSmartComponents)
                    {
                        package.RegID = oReq.ID.ToString();
                        ctx.RegSmartComponents.Add(package);
                        ctx.SaveChanges();
                    }

                    if (RegWaiverComponents != null)
                    {
                        foreach (var waiverComp in RegWaiverComponents)
                        {
                            waiverComp.RegID = oReq.ID;
                            ctx.WaiverComponents.Add(waiverComp);
                            ctx.SaveChanges();
                        }
                    }

                    trans.Complete();
                }
            }

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.RegistrationCreate({1}): {2}", svcName, (oReq != null) ? oReq.Customer.FullName : "NULL", value);
            return value;
        }


        public static List<RegistrationCancellationReasons> GetCancellationReasons()
        {

            List<RegistrationCancellationReasons> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.RegistrationCancellationReasons.Where(a => a.Active == true).ToList();
            }

            return value;
        }

        public static List<WaiverRules> GetWaiverRules()
        {
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                return ctx.WaiverRules.ToList();
            }
        }

        #endregion

        public static List<int> GetSpotifyCopmByPkgKenancode(string kenanCode, int Regid)
        {

            List<int> ids = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ids = ctx.Database.SqlQuery<int>("usp_GetSpotifyComponentsByPkgKenanCode @KenanCode,@Regid", new SqlParameter("@KenanCode", kenanCode), new SqlParameter("@Regid", Regid)).ToList();

            }
            return ids;
        }
        public static List<int> GetSpotifyCopmIDByCompKenancode(string kenanCode)
        {

            List<int> ids = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ids = ctx.Database.SqlQuery<int>("usp_GetSpotifyComponentIDsByCompKenanCode @KenanCode", new SqlParameter("@KenanCode", kenanCode)).ToList();

            }
            return ids;
        }
        public static long GetLnkPgmBdlPkgCompIdByKenanCode(string kenanCode, string planType, string linkType)
        {

            long Id = 0;
            List<int> ids = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ids = ctx.Database.SqlQuery<int>("usp_GetLnkPgmBdlPkgCompIdByKenanCodeandLinkType @KenanCode,@PlanType,@linkType", new SqlParameter("@KenanCode", kenanCode), new SqlParameter("@PlanType", planType), new SqlParameter("@linkType", linkType)).ToList();
                if (ids != null && ids.Count > 0)
                    Id = ids[0];
            }
            return Id;
        }

        public static int GetIdByTitleName(string title)
        {
            int Id = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                try
                {
					var value = ctx.CustomerTitles.Where(a => a.Name.Contains(title)).ToList();
					foreach (var data in value)
					{
						//Evan add- to get exact name of title
						if (string.Equals(data.Name, title, StringComparison.OrdinalIgnoreCase))
						{
							Id = data.ID;
							break;
						}
						if (Id == 0)
						{
							Id = value.FirstOrDefault().ID;
						}
					}
                }
                catch
                {

                }
            }

            return Id;
        }
        public static string GetDataKenaCode_Smart(string pkgId)
        {
            List<string> ids = new List<string>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ids = ctx.Database.SqlQuery<string>("usp_GetDataKenaCode_smart @pkgid", new SqlParameter("@pkgid", pkgId)).ToList();
                if (ids.Count <= 0)
                {
                    ids.Add("0");
                }
            }
            return ids.FirstOrDefault();
        }

        /*Changes by chetan related to PDPA implementation*/
        public static PDPAData GetPDPADataInfo(string DocType, string TrnType)
        {
            PDPAData value = null;
            using (var ctx = new OnlineStoreDbContext())
            {
                value = ctx.PDPAData.Where(a => a.isActive == true && a.DocType == DocType && a.TrnType == TrnType).SingleOrDefault();
            }
            return value;
        }

        public static PDPAData GetPDPADataInfoWithVersion(string DocType, string TrnType, string pdpdversion)
        {
            PDPAData value = null;
            using (var ctx = new OnlineStoreDbContext())
            {
                value = ctx.PDPAData.Where(a => a.DocType == DocType && a.TrnType == TrnType && a.Version == pdpdversion).SingleOrDefault();
            }
            return value;
        }
        public static int GetSIMModelIDByArticleID(string strAticleID)
        {
            List<int> ids = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ids = ctx.Database.SqlQuery<int>("usp_GetSIMModelIDByArticleID @SIMArticleID", new SqlParameter("@SIMArticleID", strAticleID)).ToList();
            }
            return ids.FirstOrDefault();
        }


        public static int SaveDocumentToDB(string photoData, string custPhoto, string altCustPhoto, int regid)
        {
            var value = 0;
            DAL.Models.Registration objRegistration;
            try
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    objRegistration = ctx.Registrations.FirstOrDefault(a => a.ID == regid);

                    if (objRegistration != null)
                    {
                        objRegistration.CustomerPhoto = custPhoto;
                        objRegistration.AltCustomerPhoto = altCustPhoto;
                        objRegistration.Photo = photoData;

                        value = ctx.SaveChanges();
                    }
                }
                Logger.InfoFormat("Exiting {0}.SaveDocumentToDB({1}): {2}", svcName, (objRegistration != null) ? objRegistration.ID : 0, value);
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the SaveDocumentToDB method:" + ex);
            }
            return value;
        }

		public static List<RegUploadDoc> getAllDocument(int regID)
		{
			List<RegUploadDoc> resultList = new List<RegUploadDoc>();
			try
			{
				using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
				{
					resultList = ctx.RegUploadDoc.Where(a => a.RegID == regID).ToList();
				}
			}
			catch (Exception ex){
				Logger.Info("Exception while excuting the getAllDocument method:" + ex);
				return resultList;
			}
			return resultList;
		}

		public static bool FindDocument(string criteria, int regID)
		{
			bool value = false;
			try
			{
				using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
				{
					var _result = ctx.RegUploadDoc.Where(a => a.RegID == regID && a.FileType.ToLower() == criteria.ToLower()).Any();

					if (!ReferenceEquals(_result, null) && _result)
					{
						return _result;
					}
					else {
						return value;
					}

				}
			}
			catch (Exception ex)
			{
				Logger.Info("Exception while excuting the FindDocument method:" + ex);
				value = false;
			}
			return value;
		}

		public static int SavePhotoToDB(string photoData, string custPhoto, string altCustPhoto, int regid)
		{
			var value = 0;
			DAL.Models.Registration objRegistration;
			try
			{
				using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
				{
					objRegistration = ctx.Registrations.FirstOrDefault(a => a.ID == regid);

					if (objRegistration != null)
					{
						objRegistration.AltCustomerPhoto = altCustPhoto;
						
						if (!string.IsNullOrEmpty(photoData))
						{
							objRegistration.Photo = photoData;
							value = ctx.SaveChanges();
						}
						if (!string.IsNullOrEmpty(custPhoto))
						{
							objRegistration.CustomerPhoto = custPhoto;
							value = ctx.SaveChanges();
						}
						if (!string.IsNullOrEmpty(altCustPhoto))
						{
							objRegistration.AltCustomerPhoto = altCustPhoto;
							value = ctx.SaveChanges();
						}
					}
				}
				Logger.InfoFormat("Exiting {0}.SaveDocumentToDB({1}): {2}", svcName, (objRegistration != null) ? objRegistration.ID : 0, value);
			}
			catch (Exception ex)
			{
				Logger.Info("Exception while excuting the SaveDocumentToDB method:" + ex);
			}
			return value;
		}

        public static int SaveDMEUserPrint(UserDMEPrint objUserDMEPrint)
        {
            bool isSuccesss = true;
            List<int> value = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_SaveDMEPrintOrder @UserID,@RegID",
                    new SqlParameter("@UserID", objUserDMEPrint.UserID), new SqlParameter("@RegID", objUserDMEPrint.RegID)).ToList();

            }
            return value.First();
        }
        public static List<DAL.Models.UserDMEPrint> GetUserDMEPrinters(int userId)
        {
            List<DAL.Models.UserDMEPrint> DMEUserprintersList = new List<DAL.Models.UserDMEPrint>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                DMEUserprintersList = ctx.Database.SqlQuery<DAL.Models.UserDMEPrint>("USP_GetDMEPrintOrder @UserID", new SqlParameter("@UserID", userId)).ToList();
            }
            return DMEUserprintersList;
        }


        public static List<DAL.Models.PendingRegBREStatus> PendingOrders()
        {

            List<DAL.Models.PendingRegBREStatus> RegBREStatusList = new List<DAL.Models.PendingRegBREStatus>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                RegBREStatusList = ctx.Database.SqlQuery<DAL.Models.PendingRegBREStatus>("USP_MOBILITY_GETPENDINGORDERS").ToList();
            }
            return RegBREStatusList;
        }

        public static string GetCDPUStatus(int regId)
        {
            string status = string.Empty;
            if (regId > 0)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    try
                    {
                        var value = ctx.RegBREStatus.Where(a => a.RegId == regId).ToList();

                        status = value.FirstOrDefault().TransactionStatus;
                    }
                    catch
                    {

                    }
                }
            }

            return status;
        }

        public static RegBREStatus GetCDPUDetails (RegBREStatus req)
        {
            RegBREStatus Details = new RegBREStatus();
            if (req.RegId > 0)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    try
                    {
                        var value = ctx.RegBREStatus.Where(a => a.RegId == req.RegId).ToList();

                        Details = value.FirstOrDefault();
                    }
                    catch
                    {

                    }
                }
            }
            return Details;
        }

        public static int UpdateBREApproveStatus(int RegId, string Status, string LastAccessID)
        {
            List<int> value = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("USP_MOBILITY_UPDATEPENDINGORDERSSTATUS @RegId,@TransactionStatus,@LastAccessID",
                    new SqlParameter("@RegId", RegId), 
                    new SqlParameter("@TransactionStatus", Status),
                    new SqlParameter("@LastAccessID", LastAccessID)).ToList();;

            }
            return value.FirstOrDefault();
        }

        public static int UpdateBRECancelReason(int RegId, string CReason, string LastAccessID)
        {
            List<int> value = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("USP_MOBILITY_UPDATEPENDINGORDERSCANCEL @RegId, @CReason, @LastAccessID",
                   new SqlParameter("@RegId", RegId), new SqlParameter("@CReason", CReason),
                   new SqlParameter("@LastAccessID", LastAccessID)).ToList(); ;

            }
            return value.First();
        }
        public static int GetRegtype(int RegId)
        {
            List<int> value = new List<int>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.Database.SqlQuery<int>("usp_GetRegtype @RegId", new SqlParameter("@RegId", RegId)).ToList();
            }
            return value.FirstOrDefault();
        }


        /*Code for saving CMSS case details by chetan*/
        public static CMSSComplain GetCMSSCaseDetails(int regId)
        {
            Logger.InfoFormat("Entering {0}.GetCMSSCaseDetails({1})", svcName, (regId != null) ? regId.ToString2() : "NULL");
            CMSSComplain objCMSSCase = new CMSSComplain();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objCMSSCase = ctx.CMSSID.FirstOrDefault(cmss => cmss.RegID == regId);
            }
            Logger.InfoFormat("Exiting {0}.GetCMSSCaseDetails({1}): {2}", svcName, (regId != null) ? regId.ToString2() : "NULL", objCMSSCase);
            return objCMSSCase;
        }


        /*Code for saving CMSS case details by chetan*/
        public static int SaveCMSSDetails(DAL.Models.CMSSComplain objCMSSID)
        {
            Logger.InfoFormat("Entering {0}.SaveCMSSDetails({1})", svcName, (objCMSSID != null) ? objCMSSID.CMSSID : "NULL");
            var value = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    ctx.CMSSID.Add(objCMSSID);
                    ctx.SaveChanges();
                    trans.Complete();
                }
            }
            value = objCMSSID.ID;
            Logger.InfoFormat("Exiting {0}.SaveCMSSDetails({1}): {2}", svcName, (objCMSSID != null) ? objCMSSID.CMSSID : "NULL", value);
            return value;
        }

        public static DateTime GetCMSSCaseDateForAcct(string AccountNumber)
        {
            CMSSComplain objCMSSID = null;
            var value = DateTime.Now.AddDays(-15);

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                objCMSSID = ctx.CMSSID.Where(e => e.AccountNumber == AccountNumber && !string.IsNullOrEmpty(e.CMSSID)).FirstOrDefault();
            }
            if (!ReferenceEquals(objCMSSID, null))
                value = objCMSSID.CreateDate;
            return value;
        }

        public static int RegSecondaryAccountsForSimRplc(List<lnkSecondaryAcctDetailsSimRplc> SecAccountLines)
        {
            Logger.InfoFormat("Entering {0}.RegSecondaryAccountsForSimRplc(RegID: {1})", svcName, "");

            int index = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                //using (var trans = new TransactionScope())
                //{
                foreach (var item in SecAccountLines)
                {
                    try
                    {
                        List<int> value = ctx.Database.SqlQuery<int>("USP_INSERT_SECONDARYACCTDETAIL_SIMREPLACEMENT @REGID,@EXTERNALID,@FxAcctNo,@FxSubscrNo,@FxSubscrNoResets,@SUPP_Msisdn,@PrimSecInd,@CREATEDATE,@STATUS,@DESCRIPTION"
                             , new System.Data.SqlClient.SqlParameter("@REGID", item.REGID)
                             , new System.Data.SqlClient.SqlParameter("@EXTERNALID", item.EXTERNALID)
                             , new System.Data.SqlClient.SqlParameter("@FxAcctNo", item.FxAcctNo)
                             , new System.Data.SqlClient.SqlParameter("@FxSubscrNo", item.FxSubscrNo)
                             , new System.Data.SqlClient.SqlParameter("@FxSubscrNoResets", item.FxSubscrNoResets)
                             , new System.Data.SqlClient.SqlParameter("@SUPP_Msisdn", item.SUPP_Msisdn)
                             , new System.Data.SqlClient.SqlParameter("@PrimSecInd", item.PrimSecInd)
                             , new System.Data.SqlClient.SqlParameter("@CREATEDATE", item.CREATEDATE)
                             , new System.Data.SqlClient.SqlParameter("@STATUS", item.STATUS)
                             , new System.Data.SqlClient.SqlParameter("@DESCRIPTION", item.DESCRIPTION)).ToList();

                        index = !ReferenceEquals(value, null) ? index + 1 : index;
                    }
                    catch (Exception ex)
                    { }
                }
                //}
            }
            Logger.InfoFormat("Exiting {0}.RegSecondaryAccountsForSimRplc(RegID: {1}): {2}", svcName, "", index);
            return index;
        }

        public static List<lnkSecondaryAcctDetailsSimRplc> GetSecondaryAccountLinesForSimRplc(int regid)
        {
            Logger.InfoFormat("Entering {0}.GetSecondaryAccountLinesForSimRplc(RegID: {1})", svcName, regid);
            List<lnkSecondaryAcctDetailsSimRplc> AcctLines = new List<lnkSecondaryAcctDetailsSimRplc>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {

                AcctLines = ctx.lnkSecondaryAcctDetailsSimRplc.Where(a => a.REGID == regid).ToList();
            }
            Logger.InfoFormat("Exiting {0}.GetSecondaryAccountLinesForSimRplc(RegID: {1}), RecordsCount(2)", svcName, regid, AcctLines.Count);
            return AcctLines;
        }

        public static int SaveLnkPackageComponet(int regid, List<PackageComponents> PackageComponents, int GroupID)
        {
            Logger.InfoFormat("Entering {0}.SaveLnkPackageComponet(RegID: {1})", svcName, regid);
            int outValue = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                foreach (var package in PackageComponents)
                {
                    if (((from refcomp in ctx.Components
                          join lnkpkg in ctx.PgmBdlPckComponents
                          on refcomp.ID equals lnkpkg.ChildID
                          where lnkpkg.KenanCode == package.componentId && lnkpkg.Active == true && refcomp.GroupId != null
                          select refcomp.GroupId).FirstOrDefault()) == GroupID || package.componentId == "40090") // as per discussion , pointing this , need to introduce componet selection page in Simreplacemnt to avoid these type of fix.
                    {
                        package.RegID = regid;
                        ctx.PackageComponents.Add(package);
                        outValue = ctx.SaveChanges();
                    }
                }
            }
            Logger.InfoFormat("Exiting {0}.SaveLnkPackageComponet(RegID: {1}), RecordsCount(2)", svcName, regid, PackageComponents.Count);

            return outValue;
        }

        public static List<string> getRebateContractIDbyComponentID(string ComponentID)
        {
            List<string> lstIDs = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                lstIDs = ctx.Database.SqlQuery<string>("USP_getRebateContractTypeIDbyComponentID @ComponentID", new SqlParameter("@ComponentID", ComponentID)).ToList();
            }
            return lstIDs;
        }
        public static List<string> getRebateContractIDs()
        {
            List<string> list = (List<string>)null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                list = ctx.Database.SqlQuery<string>("USP_getRebateContractIDs").ToList();
            return list;
        }
        public static List<DAL.Models.RebateDataContractComponents> getRebateDataContractComponents()
        {
            Logger.InfoFormat("Entering {0}.getRebateDataContractComponents", svcName);
            var result = new List<DAL.Models.RebateDataContractComponents>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                result = ctx.rebateDataContractComponents.Where(a => a.Status == true).ToList();
            }
            Logger.InfoFormat("Exiting {0}.getRebateDataContractComponents", svcName);
            return result;
        }

        public static List<DAL.Models.BreCheckTreatment> getBreCheckTreatment()
        {
            Logger.InfoFormat("Entering {0}.getBreCheckTreatment", svcName);
            var result = new List<DAL.Models.BreCheckTreatment>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                result = ctx.BreCheckTreatment.ToList();
            }
            Logger.InfoFormat("Exiting {0}.getBreCheckTreatment", svcName);
            return result;
        }

        public static List<DAL.Models.PostCodeCityState> getPostCodeCityState()
        {
            Logger.InfoFormat("Entering {0}.getPostalCode", svcName);
            var result = new List<DAL.Models.PostCodeCityState>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                result = ctx.PostCodeCityState.ToList();
            }
            Logger.InfoFormat("Exiting {0}.getPostCodeCityState", svcName);
            return result;
        }

        #region 13042015 - Anthony - Drop 5 : Waiver for non-Drop 4 Flow
        public static List<DAL.Models.AutoWaiverRules> getAutoWaiverRules()
        {
            Logger.InfoFormat("Entering {0}.getAutoWaiverRules", svcName);
            var result = new List<DAL.Models.AutoWaiverRules>();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                result = ctx.AutoWaiverRules.ToList();
            }
            Logger.InfoFormat("Exiting {0}.getAutoWaiverRules", svcName);
            return result;
        }
        #endregion

		public static List<DAL.Models.SalesChannelIDs> getAllSalesChannelIDs()
		{
			Logger.InfoFormat("Entering {0}.getAllSalesChannelIDs", svcName);
			var result = new List<DAL.Models.SalesChannelIDs>();
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				result = ctx.SalesChannelIDs.ToList();
			}
			Logger.InfoFormat("Exiting {0}.getAllSalesChannelIDs", svcName);
			return result;
		}

        public static int SaveLnkRegRebateDatacontractPenalty(List<DAL.Models.RegRebateDatacontractPenalty> lstRebatePenalty)
        {
            Logger.InfoFormat("Entering {0}.SaveLnkRegRebateDatacontractPenalty", svcName);
            var value = 0;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    foreach (var item in lstRebatePenalty)
                    {
                        ctx.lnkRebatePenaty.Add(item);
                        value = ctx.SaveChanges();
                    }
                    trans.Complete();
                }
            }
            Logger.InfoFormat("Exiting {0}.SaveLnkRegRebateDatacontractPenalty", svcName);
            return value;
        }
        public static DAL.Models.Registration GetRegistrationDetails(int regid)
        {
            DAL.Models.Registration result = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                result = ctx.Registrations.Where(a => a.ID == regid).FirstOrDefault();
            }
            return result;
        }

        public static List<DealerPlanIds> GetDealerSpecificPlans(int userId)
        {
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet dsPlanIds = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("usp_GetDealerSpecificPlans", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@UserId", userId));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(dsPlanIds);
                sqlConn.Close();

                var result = new List<DealerPlanIds>();
                if (dsPlanIds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsPlanIds.Tables[0].Rows)
                    {
                        if (dr["isPlansAvail"].ToString() == "1")
                        {
                            result.Add(new DealerPlanIds() { Id = Convert.ToInt32(dr["lnkpgmbdlcompID"]), isPlanAvailable = 1 });
                        }
                        else
                        {
                            result.Add(new DealerPlanIds() { Id = 0, isPlanAvailable = 0 });
                        }
                    }
                }
                return result;
            }
        }

        public static string ArticelIdByModelId(int modelID)
        {
            string result = string.Empty;
            Logger.InfoFormat("Entering {0}.ArticelIdByModelId({1})", svcName, modelID);

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                result = ctx.ArticleIds.Where(a => a.ID == modelID).FirstOrDefault().ArticleID;
            }

            Logger.InfoFormat("Exiting {0}.ArticelIdByModelId({1})", svcName, modelID);

            return result;

        }
        public static List<DAL.Models.RegRebateDatacontractPenalty> getRebateDatacontractPenalty(int RegID)
        {
            List<DAL.Models.RegRebateDatacontractPenalty> lstIDs = null;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                lstIDs = ctx.Database.SqlQuery<DAL.Models.RegRebateDatacontractPenalty>("USP_getRebatePenaltyIDRegID @RegID", new SqlParameter("@RegID", RegID)).ToList();
            }
            return lstIDs;
        }

        public static string getNrcDescriptionByNrcID(string nrcID)
        {
            string nrcDesc = string.Empty;
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                nrcDesc = ctx.Database.SqlQuery<string>("USP_getNrcDescriptionByNrcID @nrcID", new SqlParameter("@nrcID", nrcID)).FirstOrDefault();
            }
            return nrcDesc;
        }

        //25052015 Method to get Plan Name query by RegId - Lus
        public static MnpRegDetailsVM getPlanNamebyRegId(int RegId)
        {
            var PlanName = new MnpRegDetailsVM();
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                PlanName = ctx.Database.SqlQuery<MnpRegDetailsVM>("USP_GetPlanNamebyRegId @RegId", new SqlParameter("@RegId", RegId)).FirstOrDefault();
            }
            return PlanName;
        }

        public static void UpdateMismSimCardType(int RegID, string MismSimCardType)
        {
            Logger.InfoFormat("Entering {0}.UpdateSimCardType({1})", svcName, RegID.ToString2());

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var oldLnkRegDetails = ctx.LnkRegistrationDetails.Where(a => a.RegId == RegID).SingleOrDefault();
                oldLnkRegDetails.MismSimCardType = MismSimCardType;
                ctx.SaveChanges();

            }
            Logger.InfoFormat("Exiting {0}.UpdateSimCardType({1})", svcName, RegID.ToString2());
        }
        public static void UpdateSimCardType(int RegID, string SimCardType)
        {
            Logger.InfoFormat("Entering {0}.UpdateSimCardType({1})", svcName, RegID.ToString2());

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var oldLnkRegDetails = ctx.LnkRegistrationDetails.Where(a => a.RegId == RegID).SingleOrDefault();
                oldLnkRegDetails.SimCardType = SimCardType;
                ctx.SaveChanges();

            }
            Logger.InfoFormat("Exiting {0}.UpdateSimCardType({1})", svcName, RegID.ToString2());
        }

        public static void InsertLog(WebPosLog poslog)
        {
            int regId;
            try
            {
                regId = Convert.ToInt32(poslog.regId);
            }
            catch
            {
                regId = -1;
            }
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    //SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("usp_InsertLog", sqlConn);
                    SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("usp_InsertWebPosLog", sqlConn);
                    sqlCmd.Parameters.Add(new SqlParameter("@file", poslog.fileName));
                    sqlCmd.Parameters.Add(new SqlParameter("@method", poslog.method));
                    sqlCmd.Parameters.Add(new SqlParameter("@request", poslog.request));
                    sqlCmd.Parameters.Add(new SqlParameter("@response", poslog.response));
                    sqlCmd.Parameters.Add(new SqlParameter("@userid", poslog.userId));
                    sqlCmd.Parameters.Add(new SqlParameter("@exception", poslog.exception));
                    sqlCmd.Parameters.Add(new SqlParameter("@regid", regId));

                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    int result = sqlCmd.ExecuteNonQuery();
                    sqlConn.Close();
                }
                catch
                {
                }
            }
        }

        public static void InsertEmailLog(TriggerEmailLog emailLog)
        {
            int regId;
            try
            {
                regId = Convert.ToInt32(emailLog.RegID);
            }
            catch
            {
                regId = -1;
            }
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("usp_InsertEmailLog", sqlConn);
                    sqlCmd.Parameters.Add(new SqlParameter("@RegID", emailLog.RegID));
                    sqlCmd.Parameters.Add(new SqlParameter("@ContactNo", emailLog.MSISDN1));
                    sqlCmd.Parameters.Add(new SqlParameter("@EmailAddr", emailLog.Email));
                    sqlCmd.Parameters.Add(new SqlParameter("@FullName", emailLog.Name));

                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    int result = sqlCmd.ExecuteNonQuery();
                    sqlConn.Close();
                }
                catch
                {
                }
            }

        }

        //RST 06De2014
        public static List<DAL.Models.lnkRegWelcomeMail> GetWelcomeEmailByRegId(int regID)
        {

            List<DAL.Models.lnkRegWelcomeMail> value = null;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                value = ctx.lnkRegWelcomeMail.Where(a => a.RegID == regID).ToList();
            }
            return value;
        }

        //11052015 - Add for Write off supervisor approval CR - Lus
        public static int SaveJustificationDetails(DAL.Models.RegJustification objRegJustification)
        {
            Logger.InfoFormat("Entering {0}.SaveJustificationDetails RegId({1})", svcName, objRegJustification.RegId.ToString2());
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    ctx.RegJustification.Add(objRegJustification);
                    ctx.SaveChanges();
                    trans.Complete();
                }
            }
            Logger.InfoFormat("Exiting {0}.SaveJustificationDetails RegId({1})", svcName, objRegJustification.RegId.ToString2());
            return objRegJustification.ID;
        }

        // 20160120 - w.loon - Write to TrnRegBreFailTreatment - for display in Supervisor Dashboard
        public static int SaveBreFailTreatmentDetails(DAL.Models.RegBreFailTreatment objRegBreFailTreatment)
        {
            Logger.InfoFormat("Entering {0}.SaveBreFailTreatmentDetails RegId({1})", svcName, objRegBreFailTreatment.RegId.ToString2());
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope())
                {
                    ctx.RegBreFailTreatment.Add(objRegBreFailTreatment);
                    ctx.SaveChanges();
                    trans.Complete();
                }
            }
            Logger.InfoFormat("Exiting {0}.SaveBreFailTreatmentDetails RegId({1})", svcName, objRegBreFailTreatment.RegId.ToString2());
            return objRegBreFailTreatment.ID;
        }
		
		public static int SaveSurveyResponse(DAL.Models.RegSurveyResponse objSurveryResponse)
		{
			Logger.InfoFormat("Entering {0}.SaveSurveyResponse RegId({1})", svcName, objSurveryResponse.RegID.ToString2());
			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				using (var trans = new TransactionScope())
				{
					ctx.RegSurveyResponse.Add(objSurveryResponse);
					ctx.SaveChanges();
					trans.Complete();
				}
			}
			Logger.InfoFormat("Exiting {0}.SaveSurveyResponse RegId({1})", svcName, objSurveryResponse.RegID.ToString2());
			return objSurveryResponse.ID;
		}

        public static List<int> SaveTrnRegAccessory(List<RegAccessory> objRegAccessories)
        {
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope(
                    TransactionScopeOption.Required, new TransactionOptions
                        {
                            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                        }
                    ))
                {
                    foreach (var objRegAccessory in objRegAccessories)
                    {
                        ctx.RegAccessory.Add(objRegAccessory);
                    }
                    ctx.SaveChanges();
                    trans.Complete();
                    trans.Dispose();
                }
            }

            return objRegAccessories.Select(x => x.ID).ToList();
        }
        
        public static List<RegAccessory> GetRegAccessoryByRegID(int regID)
        {
            var regAccessoryList = new List<RegAccessory>();
            
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                regAccessoryList = ctx.RegAccessory.Where(x => x.RegID != 0 && x.RegID == regID).ToList();
            }

            return regAccessoryList;
        }

        public static bool SaveTrnRegAccessoryDetails(List<RegAccessoryDetails> objRegAccessoryDetailsList)
        {
            var _result = false;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope(
                    TransactionScopeOption.Required, new TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }
                    ))
                {
                    foreach (var objRegAccessoryDetails in objRegAccessoryDetailsList)
                    {
                        ctx.RegAccessoryDetails.Add(objRegAccessoryDetails);
                    }
                    ctx.SaveChanges();
                    trans.Complete();
                    trans.Dispose();
                    _result = true;
                }
            }

            return _result;
        }

        public static List<RegAccessoryDetails> GetRegAccessoryDetailsByRegAccessoryID(List<int> regAccessoryIDs)
        {
            var regAccessoryDetails = new List<RegAccessoryDetails>();

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                regAccessoryDetails = ctx.RegAccessoryDetails.Where(x => regAccessoryIDs.Contains(x.RegAccessoryID)).ToList();
            }

            return regAccessoryDetails;
        }

        public static int SaveAccessorySerialNumber(List<RegAccessoryDetails> objRegAccessoryDetailsList)
        {
            var _result = 0;

            if (objRegAccessoryDetailsList != null && objRegAccessoryDetailsList.Count > 0)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    foreach (var objRegAccessoryDetails in objRegAccessoryDetailsList)
                    {
                        var regAccessoryDetail = ctx.RegAccessoryDetails.Where(x => x.RegAccessoryID == objRegAccessoryDetails.RegAccessoryID && x.SequenceID == objRegAccessoryDetails.SequenceID).FirstOrDefault();

                        if (regAccessoryDetail != null)
                        {
                            regAccessoryDetail.SerialNumber = objRegAccessoryDetails.SerialNumber;
                            regAccessoryDetail.LastUpdateDT = DateTime.Now;
                            regAccessoryDetail.LastAccessID = "SYSTEM";
                            _result += ctx.SaveChanges();
                        }
                    }
                }
            }
            return _result;
        }

        public static List<PopUpMessage> GetPopUpMessage(string IDCardNo)
        {
            DateTime stDate = DateTime.Now;
            Logger.InfoFormat("Entering {0}.GetPopUpMessage({1})", svcName, IDCardNo);

            var _return = new List<PopUpMessage>();

            #region query using DBContext - commented
            /*
            using (OnlineStoreDbContext DBctx = new OnlineStoreDbContext())
            {
                var message = DBctx.PopUpMessage.Where(x => x.IDCardNo == IDCardNo && DateTime.Compare(x.EndDT, DateTime.Now) >= 0 && DateTime.Compare(x.StartDT, DateTime.Now) <= 0);
                _return = message.ToList();
            }*/
            #endregion

            #region query use SP
            #region ctx - commented
            /*
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                _return = ctx.Database.SqlQuery<PopUpMessage>("USP_MOBILITY_GETPOPUPMESSAGE @IDCardNo", new SqlParameter("@IDCardNo", IDCardNo)).ToList();
            }
            */
            #endregion

            #region manual map
            using (SqlConnection sqlConn = new SqlConnection(_ConnectionString))
            {
                sqlConn.Open();
                SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                DataSet ds = new DataSet();
                SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand("USP_MOBILITY_GETPOPUPMESSAGE", sqlConn);
                sqlCmd.Parameters.Add(new SqlParameter("@IDCardNo", IDCardNo));
                sqlCmd.CommandType = CommandType.StoredProcedure;
                // sqlCmd.CommandTimeout = 180;
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(ds);
                sqlConn.Close();

                if (!ReferenceEquals(ds, null))
                {
                    if (!ReferenceEquals(ds.Tables[0], null) && (ds.Tables[0].Rows.Count > 0))
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            var SearchResult = new PopUpMessage()
                            {
                                ID = ds.Tables[0].Rows[i]["ID"].ToInt(),
                                MSISDN = ds.Tables[0].Rows[i]["MSISDN"].ToString2(),
                                IDCardNo = ds.Tables[0].Rows[i]["IDCardNo"].ToString2(),
                                //CreateDT = ds.Tables[0].Rows[i]["CreateDT"].ToDateTime(),
                                //StartDT = ds.Tables[0].Rows[i]["StartDT"].ToDateTime(),
                                //EndDT = ds.Tables[0].Rows[i]["EndDT"].ToDateTime(),
                                Message = ds.Tables[0].Rows[i]["Message"].ToString2()
                            };
                            _return.Add(SearchResult);
                        }
                    }
                }
            }
            #endregion

            #endregion

            Logger.InfoFormat("Exiting {0}.GetPopUpMessage({1}): {2}", svcName, IDCardNo, _return.Count());
            TimeSpan timeSpn = (DateTime.Now - stDate);
            //Common.LogMessage(string.Format("GetPopUpMessage({0}) TimeSpan:{1}", IDCardNo, timeSpn.ToString("hh\\:mm\\:ss\\.fff")));
            Logger.InfoFormat("GetPopUpMessage({0}) TimeSpan:{1}", IDCardNo, timeSpn.ToString("hh\\:mm\\:ss\\.fff"));
            return _return;
        }

        public static int UpdatePopUpMessageAcceptReject(int ID, string Action)
        {
            var _return = 0;
            DateTime stDate = DateTime.Now;
            Logger.InfoFormat("Entering {0}.UpdatePopUpMessageAcceptReject({1}): {2}", svcName, ID, Action);
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                var result = ctx.Database.SqlQuery<int>("USP_MOBILITY_UPDATEPOPUPMESSAGEACCEPTREJECT @ID, @Action", new SqlParameter("@ID", ID), new SqlParameter("@Action", Action)).ToList();
                _return = (_return != null && result.Count > 0) ? result.FirstOrDefault() : 0;
            }
            TimeSpan timeSpn = (DateTime.Now - stDate);
            Logger.InfoFormat("UpdatePopUpMessageAcceptReject({0}) TimeSpan:{1}", ID, timeSpn.ToString("hh\\:mm\\:ss\\.fff"));
            return _return;
        }

        public static List<PopUpMessageAuditLog> SavePopUpMessageAuditLog(List<PopUpMessageAuditLog> objPopUpMessageAuditLogList)
        {
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope(
                TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }
                ))
                {
                    foreach (var objPopUpMessageAuditLog in objPopUpMessageAuditLogList)
                    {
                        ctx.PopUpMessageAuditLog.Add(objPopUpMessageAuditLog);
                    }
                    ctx.SaveChanges();
                    trans.Complete();
                    trans.Dispose();
                }
            }
            return objPopUpMessageAuditLogList;
        }

        public static List<PegaRecommendation> SavePegaRecommendation(List<PegaRecommendation> objPegaRecommendation)
        {
            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                using (var trans = new TransactionScope(
                    TransactionScopeOption.Required, new TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    }
                    ))
                {
                    foreach (var item in objPegaRecommendation)
                    {
                        ctx.PegaRecommendation.Add(item);
                    }
                    ctx.SaveChanges();
                    trans.Complete();
                    trans.Dispose();
                }
            }
            return objPegaRecommendation;
        }
        
        public static List<PegaRecommendation> GetPegaRecommendationbyMSISDN(PegaRecommendationSearchCriteria criteria)
        {
            var regPegaRecommendation = new List<PegaRecommendation>();

            if (criteria != null)
            {
                if (criteria.MSISDN == null) criteria.MSISDN = "0";
                if (criteria.IDCardNo == null) criteria.IDCardNo = "0";
                if (criteria.Response == null || criteria.Response == "Please Select") criteria.Response = "0";
                if (criteria.Status == null || criteria.Status == "Please Select") criteria.Status = "0";
                if (criteria.Recommendation == null) criteria.Recommendation = "0";
                if (criteria.CapturedBy == null || criteria.CapturedBy == "Please Select") criteria.CapturedBy = "0";
                if (criteria.LastUpdatedBy == null || criteria.LastUpdatedBy == "Please Select") criteria.LastUpdatedBy = "0";
                if (criteria.CapturedStartDt == null) criteria.CapturedStartDt = new DateTime(1900, 01, 01);
                if (criteria.CapturedEndtDt == null) criteria.CapturedEndtDt = DateTime.Now;

                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    regPegaRecommendation = ctx.Database.SqlQuery<PegaRecommendation>("USP_MOBILITY_PEGARECOMMENDATION @msisdn, @idCardNo, @response, @status, @recommendation, @capturedby, @lastupdatedby, @capturedstartdate, @capturedenddate", new SqlParameter("@msisdn", criteria.MSISDN), new SqlParameter("@idCardNo", criteria.IDCardNo), new SqlParameter("@response", criteria.Response), new SqlParameter("@status", criteria.Status), new SqlParameter("@recommendation", criteria.Recommendation), new SqlParameter("@capturedby", criteria.CapturedBy), new SqlParameter("@lastupdatedby", criteria.LastUpdatedBy), new SqlParameter("@capturedstartdate", criteria.CapturedStartDt), new SqlParameter("@capturedenddate", criteria.CapturedEndtDt)).ToList();
                }
            }

            return regPegaRecommendation;
        }

        public static int UpdatePegaRecommendation(List<PegaRecommendationVM> objPegaVM)
        {
            var result = 0;
            if (objPegaVM != null)
            {
                using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
                {
                    foreach (var item in objPegaVM)
                    {
                        var oldPegaDetails = ctx.PegaRecommendation.Where(a => a.ID == item.ID && a.Status == item.CurrentStatus).FirstOrDefault();
                        if (oldPegaDetails != null)
                        {
                            oldPegaDetails.Status = item.Status;
                            oldPegaDetails.LastUpdId = item.LastUpdId;
                            oldPegaDetails.LastUpdDate = DateTime.Now;
                            result += ctx.SaveChanges();
                        }
                    }
                }
            } 
            return result;
        }

		public static string LogException(Exception objException)
		{
			System.Text.StringBuilder strException = new System.Text.StringBuilder();
			strException.AppendFormat("Exception Found:\n{0}", objException.GetType().FullName);
			strException.AppendFormat("\n{0}", objException.Message);
			strException.AppendFormat("\n{0}", objException.Source);
			strException.AppendFormat("\n{0}", objException.StackTrace);
			if (objException.InnerException != null)
			{
				strException.AppendFormat("Inner Exception Found:\n{0}", objException.InnerException.GetType().FullName);
				strException.AppendFormat("\n{0}", objException.InnerException.Message);
				strException.AppendFormat("\n{0}", objException.InnerException.Source);
				strException.AppendFormat("\n{0}", objException.InnerException.StackTrace);
			}
			return strException.ToString();

		}

        public static int InsertTacAuditLog(TacAuditLog oReq)
        {
            Logger.InfoFormat("Entering {0}.InsertTacAuditLog({1})", svcName, (oReq != null) ? oReq.MSISDN : "NULL");
            var value = 0;

            using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
            {
                ctx.tblTacAuditLog.Add(oReq);
                ctx.SaveChanges();
            };

            value = oReq.ID;

            Logger.InfoFormat("Exiting {0}.InsertTacAuditLog({1}): {2}", svcName, (oReq != null) ? oReq.MSISDN : "NULL", value);
            return value;
        }

		public static List<TacAuditLog> getAllTacAuditLog(int rowCount)
		{
			var tacAuditLogList = new List<TacAuditLog>();

			using (OnlineStoreDbContext ctx = new OnlineStoreDbContext())
			{
				tacAuditLogList = ctx.tblTacAuditLog
					.Take(rowCount)
					.OrderByDescending(x=> x.ID)
					.ToList();
			}
			return tacAuditLogList;
		}

    }
}
