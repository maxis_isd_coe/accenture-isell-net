﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;

using Online.Registration.DAL.Models;

using log4net;

namespace Online.Registration.DAL
{
    public class WhiteList
    {
        static string svcName = "Online.Registration.DAL";
        private static readonly ILog Logger = LogManager.GetLogger(typeof(WhiteList));

        #region WhiteList

        public static List<SMECIWhiteList> WhiteListSearchByIC(SMECIWhiteList whiteList)
        {
            var WhiteListCustomer = new List<SMECIWhiteList>();

            //if (!string.IsNullOrEmpty(whiteList.ICNumber))
            //{
            //    using (var context = new OnlineStoreDbContext())
            //    {
            //        WhiteListCustomer = context.WhiteListCust.Where(a => a.ICNumber.Equals(whiteList.ICNumber)).ToList();
            //    }
            //}
            //else
            //{
            //    using (var context = new OnlineStoreDbContext())
            //    {
            //        WhiteListCustomer = context.WhiteListCust.Select(a => a).OrderByDescending(o => o.ID).Take(50).ToList();
            //    }
            //}
            using (var context = new OnlineStoreDbContext())
            {
                var query = from wlist in context.WhiteListCust
                            select wlist;
                if (!string.IsNullOrEmpty(whiteList.ICNumber))
                {
                    query = query.Where(a => a.ICNumber.Equals(whiteList.ICNumber));
                }
                if (whiteList.CorpId > 0)
                {
                    query = query.Where(b => b.CorpId.Equals(whiteList.CorpId));
                }

                WhiteListCustomer = query.ToList();
            }

            return WhiteListCustomer;
        }

        public static bool DeleteWhiteListCustomer(SMECIWhiteList whiteList)
        {

            using (var context = new OnlineStoreDbContext())
            {
                whiteList = context.WhiteListCust.Find(whiteList.ID);
                context.WhiteListCust.Remove(whiteList);
                context.SaveChanges();
                return true;
            }      
        }

        public static bool AddWhiteListCustomer(List<SMECIWhiteList> whiteList)
        {

            using (var context = new OnlineStoreDbContext())
            {
                for (int i = 0; i < whiteList.Count; i++)
                {
                    context.WhiteListCust.Add(whiteList[i]);                                    
                }
                context.SaveChanges();   
                return true;
            }
        }

        public static List<int> GetDependentComp(string planId)
        {
            List<int> DependComp = new List<int>();
            using (var context = new OnlineStoreDbContext())
            {
                DependComp = context.Database.SqlQuery<int>("usp_GetDependentComp @PlanId", new System.Data.SqlClient.SqlParameter("@PlanId", Convert.ToInt32(planId))).ToList();
            }
            return DependComp;
        }

        #endregion

       

    }
}
