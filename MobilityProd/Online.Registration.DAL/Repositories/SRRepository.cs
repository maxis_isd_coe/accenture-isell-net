﻿using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Models.Common;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.DAL.Repositories;
using Online.Registration.DAL.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Transactions;
using Online.Registration.DAL.Models.ServiceRequest.Dto;
using System.Data.Entity.Infrastructure;
using Online.Registration.DAL.CustomeException;
using Online.Registration.DAL.Models.FilterCriterias;
using System.Linq.Dynamic;

namespace Online.Registration.DAL.Repositories
{
    public class SRRepository:ISRRepository
    {
        public string SaveOrder(SROrderMaster order)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    using (var ctx = new OnlineStoreDbContext())
                    {
                        ctx.SROrderMasters.Add(order);
                        ctx.SaveChanges();
                        //Save docs
                        SaveVerificationDocs(order, ctx);
                        //After save will return order id
                        order.Detail.OrderId = order.OrderId;
                        AddDetail(ctx, (dynamic)order.Detail);//Use dynamic multi patches
                        ctx.SaveChanges();
                        scope.Complete();
                    }
                }
                return order.OrderId;
            }
            catch (Exception ex)
            {
                throw new DBCatchException(ex);
            }
        }

        public int DeleteOrderById(string orderId)
        {
            try
            {
                int deleteCount = 0;
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    using (var ctx = new OnlineStoreDbContext())
                    {
                        var order = ctx.SROrderMasters.Where(n => n.OrderId == orderId).FirstOrDefault();
                        if (order == null)
                            throw new Exception("Order not exist : " +orderId);
                        DeleteVerificationDocs(order, ctx);
                        var orderDtls = FindDetail(ctx,order);
                        //delete detail
                        DeleteDetail(ctx, (dynamic)orderDtls);//Use dynamic patch
                        //delete master
                        ctx.SROrderMasters.Remove(order);
                        deleteCount=ctx.SaveChanges();
                        scope.Complete();
                    }
                }
                return deleteCount;
            }
            catch (Exception ex)
            {
                throw new DBCatchException(ex);
            }
        }

        public SROrderMaster FindOrderById(string orderId)
        {
            try
            {
                using (var ctx = new OnlineStoreDbContext())
                {
                    var order = ctx.SROrderMasters
                        .Include(n => n.CenterOrg)
                        .Where(n => n.OrderId == orderId)
                        .FirstOrDefault();
                    if (order == null) return order;
                    LoadVerificationDocs(order, ctx);
                    var orderDtls = FindDetail(ctx, order);
                    //Load detail
                    LoadDetail(ctx, (dynamic)orderDtls);//Use dynamic patch
                    return order;
                }

            }
            catch (Exception ex)
            {
                throw new DBCatchException(ex);
            }
        }

        public bool SaveVerificationDoc(string orderId, List<VerificationDoc> verificationDocs)
        {
            try
            {
                int changeCount = 0;
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    using (var ctx = new OnlineStoreDbContext())
                    {
                        //Check is order exist
                        var order = ctx.SROrderMasters.Where(n => n.OrderId == orderId).SingleOrDefault();
                        if (order == null)
                            throw new Exception(string.Format("Order {0} is not exist", orderId));
                        //Remove existing for update latest
                        var docTypes = verificationDocs.Select(n => n.DocTypeId).ToList();
                        var existingDocs = ctx.VerificationDocs.Where(n => docTypes.Contains(n.DocTypeId) && n.OrderId == order.ID).ToList();
                        for(int i = 0; i < existingDocs.Count; i++)
                        {
                            ctx.VerificationDocs.Remove(existingDocs[i]);
                        }
                        foreach (var doc in verificationDocs) {
                            if (doc.ID > 0)
                            {
                                var deleteDoc = new VerificationDoc() { ID = doc.ID };
                                ctx.VerificationDocs.Attach(deleteDoc);
                                ctx.VerificationDocs.Remove(deleteDoc);
                            }
                        }
                        
                        changeCount = ctx.SaveChanges();
                        //save verification doc
                        foreach(var doc in verificationDocs){
                            doc.OrderId = order.ID;
                            ctx.VerificationDocs.Add(doc);
                        }
                        changeCount=ctx.SaveChanges();
                        scope.Complete();
                    }
                }
                return changeCount > 0;
            }
            catch (Exception ex)
            {
                throw new DBCatchException(ex);
            }
        }

        public bool UpdateOrderLock(string orderId, string lockBy)
        {
            try
            {
                int changeCount = 0;
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted })){
                    using(var ctx=new OnlineStoreDbContext()){
                        //check is order exist
                        var order = ctx.SROrderMasters.Where(n => n.OrderId == orderId).SingleOrDefault();
                        if (order == null)
                            throw new Exception(string.Format("Order {0} is not exist", orderId));
                        //if lockby is nothing will remove lock information(unlock situation)
                        if(string.IsNullOrEmpty(lockBy)){
                            order.LockBy = null;
                            order.LockDt = null;
                        }else{
                            order.LockBy = lockBy;
                            order.LockDt = DateTime.Now;
                        }
                        changeCount=ctx.SaveChanges();
                        scope.Complete();
                    }
                }
                return changeCount > 0;
            }
            catch (Exception ex)
            {
                throw new DBCatchException(ex);
            }
        }

        public bool UpdateStatus(SRUpdateStatusDto dto)
        {
            try
            {
                int changeCount = 0;
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    using (var ctx = new OnlineStoreDbContext())
                    {
                        //check is order exist
                        var order = ctx.SROrderMasters.Where(n => n.OrderId == dto.OrderId).SingleOrDefault();
                        if (order == null)
                            throw new Exception(string.Format("Order {0} is not exist", dto.OrderId));

                        UpdateOrder(order, dto);

                        var detail = FindDetail(ctx, order);
                        if(detail==null)
                            throw new Exception(string.Format("Order detail {0} is not found", dto.OrderId));
                        //if complete status will record complete info
                        if(dto.Status== SRStatus.Completed){
                            order.CompleteBy = dto.UpdateBy;
                            order.CompleteDt = DateTime.Now;
                        }
                        //update order dtl
                        UpdateDetail((dynamic)detail, dto);
                        changeCount = ctx.SaveChanges();
                        scope.Complete();
                    }
                }
                return changeCount > 0;
            }catch(Exception ex)
            {
                throw new DBCatchException(ex);
            }
        }

        public IEnumerable<string> GetDistinctCreateByList(int centerOrgId)
        {
            try
            {
                using (var ctx = new OnlineStoreDbContext())
                {
                    var result= ctx.SROrderMasters.Where(n=>n.CenterOrgId==centerOrgId)
                        .Select(n => n.CreateBy).Distinct().ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new DBCatchException(ex);
            }
        }

        public IEnumerable<SRQueryMaster> QueryOrder(SRQueryOrderDto dto,out int totalCount,int page=0,int pageSize=10)
        {
            try
            {
                using (var ctx = new OnlineStoreDbContext())
                {
                    var query = ctx.SRQueryMasters.AsQueryable();
                    //Add filter logic here
                    if(dto!=null){
                        var sql="";
                        foreach(var cri in dto.CriteriaList){
                            query = cri.RunFilter<SRQueryMaster>(query);
                            sql=query.ToString();
                        }
                    }
                    //default filter by center org id
                    query = query.Where(n => n.CenterOrgId == dto.CenterOrgId).AsQueryable();
                    //default order by create dy asc
                    query = query.OrderBy(n => n.CreateBy).AsQueryable();
                    totalCount = query.Count();
                    query = RunSorting(dto, query);
                    //If full record will return all
                    if (dto.FullRecord)
                        return query.ToList();
                    //Return filter record for quick view in screen
                    return query
                        .Skip(pageSize * page)
                        .Take(pageSize)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                throw new DBCatchException(ex);
            }
        }

        private IQueryable<SRQueryMaster> RunSorting(SRQueryOrderDto dto, IQueryable<SRQueryMaster> query)
        {
            if (string.IsNullOrEmpty(dto.SortyBy)) return query.AsQueryable();
            if(dto.SortDesc.HasValue)
                return query.OrderBy(string.Format("{0} {1}",dto.SortyBy,dto.SortDesc.Value?"desc":"asc")).AsQueryable();
            return query.OrderBy(dto.SortyBy).AsQueryable();
        }

        #region "Helper Method"

        private void UpdateOrder(SROrderMaster order, SRUpdateStatusDto dto)
        {
            //update order master
            order.UpdateBy = dto.UpdateBy;
            order.UpdateDt = DateTime.Now;
            order.Status = dto.Status;
            //clear lock info
            order.LockBy = null;
            order.LockDt = null;
        }

        private void UpdateDetail(SROrderPostToPreDtl detail, SRUpdateStatusDto dto)
        {
            detail.PrepaidAccNumber = dto.AccountNo;
            UpdateDetail(detail as SROrderDtl, dto);
        }

        private void UpdateDetail(SROrderPreToPostDtl detail, SRUpdateStatusDto dto)
        {
            detail.PostpaidAccNumber = dto.AccountNo;
            UpdateDetail(detail as SROrderDtl, dto);
        }

        private void UpdateDetail(SROrderBillSeparationDtl detail, SRUpdateStatusDto dto){
            detail.ExistAcctNumber = dto.AccountNo;
            UpdateDetail(detail as SROrderDtl, dto);
        }

        private void UpdateDetail(SROrderTransferOwnerDtl detail, SRUpdateStatusDto dto)
        {
            detail.TransfereeAcctNumber = dto.AccountNo;
            UpdateDetail(detail as SROrderDtl, dto);
        }

        private void UpdateDetail(SROrderDtl detail, SRUpdateStatusDto dto)
        {
            //Only update remarks when got value
            if (!string.IsNullOrEmpty(dto.Remarks))
                detail.TryUpdate(dto, dto.Name(n => n.Remarks));
            //Account No only update when got value
            if (!string.IsNullOrEmpty(dto.AccountNo)){
                detail.TryUpdate(dto, dto.Name(n => n.AccountNo));
            }
        }

        private void LoadVerificationDocs(SROrderMaster order, OnlineStoreDbContext ctx)
        {
            order.VerificationDocs = ctx.VerificationDocs.Where(n => n.OrderId == order.ID).ToList();
        }

        private void DeleteVerificationDocs(SROrderMaster order, OnlineStoreDbContext ctx)
        {
            for (int i = 0; i < order.VerificationDocs.Count; i++)
                ctx.VerificationDocs.Remove(order.VerificationDocs[i]);
        }

        private void SaveVerificationDocs(SROrderMaster order, OnlineStoreDbContext ctx)
        {
            foreach (var item in order.VerificationDocs)
            {
                item.OrderId = order.ID;
                ctx.VerificationDocs.Add(item);
            }
        }

        private void LoadDetail(OnlineStoreDbContext ctx, SROrderDtl orderDtls)
        {
            throw new NotImplementedException("Cannot use SROrderDtl to LoadDetail");
        }

        //Load - for Corp SIM Replacement
        private void LoadDetail(OnlineStoreDbContext ctx, SROrderCorpSimRepDtl orderDtls)
        {
            //Use include to query one for all table
            orderDtls=ctx.SROrderCorpSimRepDtls
                .Include(n=>n.Customer)
                .Where(n => n.OrderId == orderDtls.OrderId)
                .FirstOrDefault();
        }

        //Load - for Bill Separation
        private void LoadDetail(OnlineStoreDbContext ctx, SROrderBillSeparationDtl orderDtls)
        {
            //Use include to query one for all table
            orderDtls = ctx.SROrderBillSeparationDtls
                .Include(n => n.Customer)
                .Include(n => n.CurrAddress)
                .Include(n => n.NewAddress)
                .Where(n => n.OrderId == orderDtls.OrderId)
                .FirstOrDefault();            
        }

        //Load - for Third Party Authorization
        private void LoadDetail(OnlineStoreDbContext ctx, SROrderThirdPartyAuthDtl orderDtls)
        {
            //Use include to query one for all table
            orderDtls = ctx.SROrderThirdPartyAuthDtls
                .Include(n => n.Customer)
                .Include(n => n.CustAddress)
                .Where(n => n.OrderId == orderDtls.OrderId)
                .FirstOrDefault();
        }

        //Load - for Postpaid To Prepaid
        private void LoadDetail(OnlineStoreDbContext ctx, SROrderPostToPreDtl orderDtls)
        {
            //Use include to query one for all table
            orderDtls = ctx.SROrderPostToPreDtls
                .Include(n => n.Customer)
                .Include(n => n.CustAddress)
                .Where(n => n.OrderId == orderDtls.OrderId)
                .FirstOrDefault();
        }

        //Load - for Prepaid To Postpaid
        private void LoadDetail(OnlineStoreDbContext ctx, SROrderPreToPostDtl orderDtls)
        {
            //Use include to query one for all table
            orderDtls = ctx.SROrderPreToPostDtls
                .Include(n => n.Customer)
                .Include(n => n.CustAddress)
                .Where(n => n.OrderId == orderDtls.OrderId)
                .FirstOrDefault();
        }

        //Load - for Transfer Ownership
        private void LoadDetail(OnlineStoreDbContext ctx, SROrderTransferOwnerDtl orderDtls)
        {
            //Use include to query one for all table
            orderDtls = ctx.SROrderTransferOwnerDtls
                .Include(n => n.TransferorCustomer)
                .Include(n => n.TransfereeCustomer)
                .Include(n => n.TransferorAddress)
                .Include(n => n.TransfereeAddress)
                .Where(n => n.OrderId == orderDtls.OrderId)
                .FirstOrDefault();
        }

        //Load - for Change MSISDN
        private void LoadDetail(OnlineStoreDbContext ctx, SROrderChangeMSISDNDtl orderDtls)
        {
            //Use include to query one for all table
            orderDtls = ctx.SROrderChangeMSISDNDtls
                .Include(n => n.Customer)
                .Include(n => n.Address)
                .Where(n => n.OrderId == orderDtls.OrderId)
                .FirstOrDefault();
        }

        //Load - for Change Status
        private void LoadDetail(OnlineStoreDbContext ctx, SROrderChangeStatusDtl orderDtls)
        {
            //Use include to query one for all table
            orderDtls = ctx.SROrderChangeStatusDtls
                .Include(n => n.Customer)
                .Include(n=>n.Address)
                .Where(n => n.OrderId == orderDtls.OrderId)
                .FirstOrDefault();
        }

        private void AddDetail(OnlineStoreDbContext ctx, SROrderDtl orderDtls)
        {
            throw new NotImplementedException("Cannot use SROrderDtl to AddDetail");
        }

        //Add - for Corp Sim Replacement
        private void AddDetail(OnlineStoreDbContext ctx, SROrderCorpSimRepDtl orderDtls)
        {
            ctx.SROrderCorpSimRepDtls.Add(orderDtls);
        }

        //Add - for Bill Separation
        private void AddDetail(OnlineStoreDbContext ctx, SROrderBillSeparationDtl orderDtls)
        {
            ctx.SROrderBillSeparationDtls.Add(orderDtls);
        }

        //Add - for Third Party Auth
        private void AddDetail(OnlineStoreDbContext ctx, SROrderThirdPartyAuthDtl orderDtls)
        {
            ctx.SROrderThirdPartyAuthDtls.Add(orderDtls);
        }

        //Add - for Postpaid To Prepaid
        private void AddDetail(OnlineStoreDbContext ctx, SROrderPostToPreDtl orderDtls)
        {
            ctx.SROrderPostToPreDtls.Add(orderDtls);
        }

        //Add - for Prepaid To Postpaid
        private void AddDetail(OnlineStoreDbContext ctx, SROrderPreToPostDtl orderDtls)
        {
            ctx.SROrderPreToPostDtls.Add(orderDtls);
        }

        //Add - for Transfer Ownership
        private void AddDetail(OnlineStoreDbContext ctx, SROrderTransferOwnerDtl orderDtls)
        {
            ctx.SROrderTransferOwnerDtls.Add(orderDtls);
        }

        //Add - for Change MSISDN
        private void AddDetail(OnlineStoreDbContext ctx, SROrderChangeMSISDNDtl orderDtls)
        {
            ctx.SROrderChangeMSISDNDtls.Add(orderDtls);
        }

        //Add - for Change Status
        private void AddDetail(OnlineStoreDbContext ctx, SROrderChangeStatusDtl orderDtls)
        {
            ctx.SROrderChangeStatusDtls.Add(orderDtls);
        }

        private void DeleteDetail(OnlineStoreDbContext ctx, SROrderDtl orderDtls)
        {
            throw new NotImplementedException("Cannot use SROrderDtl to DeleteDetail");
        }

        //Delete - for Corp SIM Replacement
        private void DeleteDetail(OnlineStoreDbContext ctx, SROrderCorpSimRepDtl orderDtls)
        {
            //delete customer
            var deleteCust = new Customer() { ID = orderDtls.CustId };
            ctx.Customers.Attach(deleteCust);
            ctx.Customers.Remove(deleteCust);
            //Delete detail
            ctx.SROrderCorpSimRepDtls.Remove(orderDtls);
        }

        //Delete - for Bill Separation 
        private void DeleteDetail(OnlineStoreDbContext ctx, SROrderBillSeparationDtl orderDtls)
        {
            //delete customer
            var deleteCust = new Customer() { ID = orderDtls.CustId };
            ctx.Customers.Attach(deleteCust);
            ctx.Customers.Remove(deleteCust);
            //delete current customer address
            var deleteCurrAddr = new Models.Address() { ID = orderDtls.CurrCustAddrId };
            ctx.Addresses.Attach(deleteCurrAddr);
            ctx.Addresses.Remove(deleteCurrAddr);
            //delete new customer address
            var deleteNewAddr = new Models.Address() { ID = orderDtls.NewCustAddrId };
            ctx.Addresses.Attach(deleteNewAddr);
            ctx.Addresses.Remove(deleteNewAddr);

            //Delete detail
            ctx.SROrderBillSeparationDtls.Remove(orderDtls);
        }

        //Delete - for Third Party Authorization
        private void DeleteDetail(OnlineStoreDbContext ctx, SROrderThirdPartyAuthDtl orderDtls)
        {
            //delete customer
            var deleteCust = new Customer() { ID = orderDtls.CustId };
            ctx.Customers.Attach(deleteCust);
            ctx.Customers.Remove(deleteCust);
            //Delete detail
            ctx.SROrderThirdPartyAuthDtls.Remove(orderDtls);
        }

        //Delete - for Postpaid To Prepaid
        private void DeleteDetail(OnlineStoreDbContext ctx, SROrderPostToPreDtl orderDtls)
        {
            //delete customer
            var deleteCust = new Customer() { ID = orderDtls.CustId };
            ctx.Customers.Attach(deleteCust);
            ctx.Customers.Remove(deleteCust);
            //Delete detail
            ctx.SROrderPostToPreDtls.Remove(orderDtls);
        }

        //Delete - for Prepaid To Postpaid
        private void DeleteDetail(OnlineStoreDbContext ctx, SROrderPreToPostDtl orderDtls)
        {
            //delete customer
            var deleteCust = new Customer() { ID = orderDtls.CustId };
            ctx.Customers.Attach(deleteCust);
            ctx.Customers.Remove(deleteCust);
            //Delete detail
            ctx.SROrderPreToPostDtls.Remove(orderDtls);
        }

        //Delete - for Transfer Ownership
        private void DeleteDetail(OnlineStoreDbContext ctx, SROrderTransferOwnerDtl orderDtls)
        {
            //delete verification without query it
            //delete customer
            var deleteTransferorCust = new Customer() { ID = orderDtls.TransferorCustId };
            var deleteTransfereeCust = new Customer() { ID = orderDtls.TransfereeCustId };
            ctx.Customers.Attach(deleteTransferorCust);
            ctx.Customers.Remove(deleteTransferorCust);
            ctx.Customers.Attach(deleteTransfereeCust);
            ctx.Customers.Remove(deleteTransfereeCust);
            //Delete detail
            ctx.SROrderTransferOwnerDtls.Remove(orderDtls);
        }

        //Delete - for Change MSISDN
        private void DeleteDetail(OnlineStoreDbContext ctx, SROrderChangeMSISDNDtl orderDtls)
        {
            //delete customer
            var deleteCust = new Customer() { ID = orderDtls.CustId };
            ctx.Customers.Attach(deleteCust);
            ctx.Customers.Remove(deleteCust);
            //Delete detail
            ctx.SROrderChangeMSISDNDtls.Remove(orderDtls);
        }

        //Delete - for Change Status
        private void DeleteDetail(OnlineStoreDbContext ctx, SROrderChangeStatusDtl orderDtls)
        {
            //delete customer
            var deleteCust = new Customer() { ID = orderDtls.CustId };
            ctx.Customers.Attach(deleteCust);
            ctx.Customers.Remove(deleteCust);
            //Delete detail
            ctx.SROrderChangeStatusDtls.Remove(orderDtls);
        }

        private SROrderDtl FindDetail(OnlineStoreDbContext ctx, SROrderMaster order)
        {
            switch (order.RequestType)
            {
                case RequestTypes.CorpSimReplace:
                    order.Detail= ctx.SROrderCorpSimRepDtls.Where(n => n.OrderId == order.OrderId).FirstOrDefault();
                    break;
                case RequestTypes.BillSeparation:
                    order.Detail = ctx.SROrderBillSeparationDtls.Where(n => n.OrderId == order.OrderId).FirstOrDefault();
                    break;
                case RequestTypes.ThridPartyAuthorization:
                    order.Detail = ctx.SROrderThirdPartyAuthDtls.Where(n => n.OrderId == order.OrderId).FirstOrDefault();
                    break;
                case RequestTypes.Postpaid2Prepaid:
                    order.Detail = ctx.SROrderPostToPreDtls.Where(n => n.OrderId == order.OrderId).FirstOrDefault();
                    break;
                case RequestTypes.Prepaid2Postpaid:
                    order.Detail = ctx.SROrderPreToPostDtls.Where(n => n.OrderId == order.OrderId).FirstOrDefault();
                    break;
                case RequestTypes.TransferOwnerShip:
                    order.Detail = ctx.SROrderTransferOwnerDtls.Where(n => n.OrderId == order.OrderId).FirstOrDefault();
                    break;
                case RequestTypes.ChangeMSISDN:
                    order.Detail = ctx.SROrderChangeMSISDNDtls.Where(n => n.OrderId == order.OrderId).FirstOrDefault();
                    break;
                case RequestTypes.ChangeStatus:
                    order.Detail = ctx.SROrderChangeStatusDtls.Where(n => n.OrderId == order.OrderId).FirstOrDefault();
                    break;
            }
            return order.Detail;
        }
        #endregion
    }
}
