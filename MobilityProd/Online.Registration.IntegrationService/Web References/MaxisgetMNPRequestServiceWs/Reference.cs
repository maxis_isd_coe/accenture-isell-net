﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace Online.Registration.IntegrationService.MaxisgetMNPRequestServiceWs {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="maxis_eai_process_common_ws_getMNPRequestService_Binder", Namespace="http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/")]
    public partial class maxiseaiprocesscommonwsgetMNPRequestService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private eaiHeader eaiHeaderValueField;
        
        private System.Threading.SendOrPostCallback getMNPRequestOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public maxiseaiprocesscommonwsgetMNPRequestService() {
            this.Url = global::Online.Registration.IntegrationService.Properties.Settings.Default.Online_Registration_IntegrationService_MaxisgetMNPRequestServiceWs_maxis_eai_process_common_ws_getMNPRequestService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public eaiHeader eaiHeaderValue {
            get {
                return this.eaiHeaderValueField;
            }
            set {
                this.eaiHeaderValueField = value;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event getMNPRequestCompletedEventHandler getMNPRequestCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("eaiHeaderValue", Direction=System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("maxis_eai_process_common_ws_getMNPRequestService_Binder_getMNPRequest", RequestElementName="getMNPRequestRequest", RequestNamespace="http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/", ResponseNamespace="http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("getMNPRequestRespDetls", Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/", IsNullable=true)]
        public typeGetMNPRequestResponse getMNPRequest([System.Xml.Serialization.XmlElementAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/", IsNullable=true)] typeGetMNPRequestRequest getMNPRequestReqDetls) {
            object[] results = this.Invoke("getMNPRequest", new object[] {
                        getMNPRequestReqDetls});
            return ((typeGetMNPRequestResponse)(results[0]));
        }
        
        /// <remarks/>
        public void getMNPRequestAsync(typeGetMNPRequestRequest getMNPRequestReqDetls) {
            this.getMNPRequestAsync(getMNPRequestReqDetls, null);
        }
        
        /// <remarks/>
        public void getMNPRequestAsync(typeGetMNPRequestRequest getMNPRequestReqDetls, object userState) {
            if ((this.getMNPRequestOperationCompleted == null)) {
                this.getMNPRequestOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetMNPRequestOperationCompleted);
            }
            this.InvokeAsync("getMNPRequest", new object[] {
                        getMNPRequestReqDetls}, this.getMNPRequestOperationCompleted, userState);
        }
        
        private void OngetMNPRequestOperationCompleted(object arg) {
            if ((this.getMNPRequestCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.getMNPRequestCompleted(this, new getMNPRequestCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/common/soap/v1/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://schemas.maxis.com.my/EAI/common/soap/v1/", IsNullable=false)]
    public partial class eaiHeader : System.Web.Services.Protocols.SoapHeader {
        
        private string fromField;
        
        private string toField;
        
        private string appIdField;
        
        private string msgTypeField;
        
        private string msgIdField;
        
        private string correlationIdField;
        
        private string timestampField;
        
        /// <remarks/>
        public string from {
            get {
                return this.fromField;
            }
            set {
                this.fromField = value;
            }
        }
        
        /// <remarks/>
        public string to {
            get {
                return this.toField;
            }
            set {
                this.toField = value;
            }
        }
        
        /// <remarks/>
        public string appId {
            get {
                return this.appIdField;
            }
            set {
                this.appIdField = value;
            }
        }
        
        /// <remarks/>
        public string msgType {
            get {
                return this.msgTypeField;
            }
            set {
                this.msgTypeField = value;
            }
        }
        
        /// <remarks/>
        public string msgId {
            get {
                return this.msgIdField;
            }
            set {
                this.msgIdField = value;
            }
        }
        
        /// <remarks/>
        public string correlationId {
            get {
                return this.correlationIdField;
            }
            set {
                this.correlationIdField = value;
            }
        }
        
        /// <remarks/>
        public string timestamp {
            get {
                return this.timestampField;
            }
            set {
                this.timestampField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/")]
    public partial class mnpReqDoc {
        
        private string portInitiatedDtField;
        
        private string portReqIdField;
        
        private string portReqStatusField;
        
        private string msisdnField;
        
        private string msisdnStatusField;
        
        private string accountNoField;
        
        private string dispPortReqIdField;
        
        private string userIdField;
        
        private string locationField;
        
        private string errorCodeField;
        
        private string errorReasonField;
        
        private string fxOrderIdField;
        
        private string fxServiceOrderIdField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string portInitiatedDt {
            get {
                return this.portInitiatedDtField;
            }
            set {
                this.portInitiatedDtField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string portReqId {
            get {
                return this.portReqIdField;
            }
            set {
                this.portReqIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string portReqStatus {
            get {
                return this.portReqStatusField;
            }
            set {
                this.portReqStatusField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string msisdn {
            get {
                return this.msisdnField;
            }
            set {
                this.msisdnField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string msisdnStatus {
            get {
                return this.msisdnStatusField;
            }
            set {
                this.msisdnStatusField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string accountNo {
            get {
                return this.accountNoField;
            }
            set {
                this.accountNoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string dispPortReqId {
            get {
                return this.dispPortReqIdField;
            }
            set {
                this.dispPortReqIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string userId {
            get {
                return this.userIdField;
            }
            set {
                this.userIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string location {
            get {
                return this.locationField;
            }
            set {
                this.locationField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string errorCode {
            get {
                return this.errorCodeField;
            }
            set {
                this.errorCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string errorReason {
            get {
                return this.errorReasonField;
            }
            set {
                this.errorReasonField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string fxOrderId {
            get {
                return this.fxOrderIdField;
            }
            set {
                this.fxOrderIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string fxServiceOrderId {
            get {
                return this.fxServiceOrderIdField;
            }
            set {
                this.fxServiceOrderIdField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/")]
    public partial class typeGetMNPRequestResponse {
        
        private string msgCodeField;
        
        private string msgDescField;
        
        private string totalRecField;
        
        private mnpReqDoc[] mnpReqDetlField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string msgCode {
            get {
                return this.msgCodeField;
            }
            set {
                this.msgCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string msgDesc {
            get {
                return this.msgDescField;
            }
            set {
                this.msgDescField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string totalRec {
            get {
                return this.totalRecField;
            }
            set {
                this.totalRecField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("mnpReqDetl", IsNullable=true)]
        public mnpReqDoc[] mnpReqDetl {
            get {
                return this.mnpReqDetlField;
            }
            set {
                this.mnpReqDetlField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/")]
    public partial class typeGetMNPRequestRequest {
        
        private string reqTypeField;
        
        private string reqValueField;
        
        private string portReqTypeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string reqType {
            get {
                return this.reqTypeField;
            }
            set {
                this.reqTypeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string reqValue {
            get {
                return this.reqValueField;
            }
            set {
                this.reqValueField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string portReqType {
            get {
                return this.portReqTypeField;
            }
            set {
                this.portReqTypeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    public delegate void getMNPRequestCompletedEventHandler(object sender, getMNPRequestCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class getMNPRequestCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal getMNPRequestCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public typeGetMNPRequestResponse Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((typeGetMNPRequestResponse)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591