﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace Online.Registration.IntegrationService.MaxisCrmCreateCrmInteractionCaseWs {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="maxis_eai_process_crm_wsBinding", Namespace="http://www.webmethods.com/")]
    public partial class maxis_eai_process_crm_wsService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback createCrmInteractionCaseOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public maxis_eai_process_crm_wsService() {
            this.Url = global::Online.Registration.IntegrationService.Properties.Settings.Default.Online_Registration_IntegrationService_MaxisCommonRetrieveComponentInfoWs_maxis_eai_process_common_wsService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event createCrmInteractionCaseCompletedEventHandler createCrmInteractionCaseCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://www.webmethods.com/maxis.eai.process.crm.ws", ResponseNamespace="http://www.webmethods.com/maxis.eai.process.crm.ws")]
        [return: System.Xml.Serialization.SoapElementAttribute("msgCode")]
        public string createCrmInteractionCase(string applnTyp, string tranxId, string msisdn, string acctExtId, string notes, out string msgDesc, out @__crmInteractionCaseDoc interaction, out @__crmInteractionCaseDoc @case) {
            object[] results = this.Invoke("createCrmInteractionCase", new object[] {
                        applnTyp,
                        tranxId,
                        msisdn,
                        acctExtId,
                        notes});
            msgDesc = ((string)(results[1]));
            interaction = ((@__crmInteractionCaseDoc)(results[2]));
            @case = ((@__crmInteractionCaseDoc)(results[3]));
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void createCrmInteractionCaseAsync(string applnTyp, string tranxId, string msisdn, string acctExtId, string notes) {
            this.createCrmInteractionCaseAsync(applnTyp, tranxId, msisdn, acctExtId, notes, null);
        }
        
        /// <remarks/>
        public void createCrmInteractionCaseAsync(string applnTyp, string tranxId, string msisdn, string acctExtId, string notes, object userState) {
            if ((this.createCrmInteractionCaseOperationCompleted == null)) {
                this.createCrmInteractionCaseOperationCompleted = new System.Threading.SendOrPostCallback(this.OncreateCrmInteractionCaseOperationCompleted);
            }
            this.InvokeAsync("createCrmInteractionCase", new object[] {
                        applnTyp,
                        tranxId,
                        msisdn,
                        acctExtId,
                        notes}, this.createCrmInteractionCaseOperationCompleted, userState);
        }
        
        private void OncreateCrmInteractionCaseOperationCompleted(object arg) {
            if ((this.createCrmInteractionCaseCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.createCrmInteractionCaseCompleted(this, new createCrmInteractionCaseCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.SoapTypeAttribute(Namespace="http://localhost/maxis/eai/process/crm/ws/createCrmInteractionCase")]
    public partial class @__crmInteractionCaseDoc {
        
        private string returnCodeField;
        
        private string returnMsgField;
        
        private string idField;
        
        /// <remarks/>
        [System.Xml.Serialization.SoapElementAttribute(IsNullable=true)]
        public string returnCode {
            get {
                return this.returnCodeField;
            }
            set {
                this.returnCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.SoapElementAttribute(IsNullable=true)]
        public string returnMsg {
            get {
                return this.returnMsgField;
            }
            set {
                this.returnMsgField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.SoapElementAttribute(IsNullable=true)]
        public string id {
            get {
                return this.idField;
            }
            set {
                this.idField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    public delegate void createCrmInteractionCaseCompletedEventHandler(object sender, createCrmInteractionCaseCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class createCrmInteractionCaseCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal createCrmInteractionCaseCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
        
        /// <remarks/>
        public string msgDesc {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
        
        /// <remarks/>
        public @__crmInteractionCaseDoc interaction {
            get {
                this.RaiseExceptionIfNecessary();
                return ((@__crmInteractionCaseDoc)(this.results[2]));
            }
        }
        
        /// <remarks/>
        public @__crmInteractionCaseDoc @case {
            get {
                this.RaiseExceptionIfNecessary();
                return ((@__crmInteractionCaseDoc)(this.results[3]));
            }
        }
    }
}

#pragma warning restore 1591