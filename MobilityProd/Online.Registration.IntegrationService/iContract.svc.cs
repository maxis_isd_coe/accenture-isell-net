﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Online.Registration.IntegrationService.IContractUploadWS;
using System.Net;
using System.IO;
using log4net;
using Online.Registration.IntegrationService.Helper;
using System.Configuration;

namespace Online.Registration.IntegrationService
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "iContract" in code, svc and config file together.
	public class  iContract : IiContract
	{
		private static readonly ILog Logger = LogManager.GetLogger(typeof(iContract));
		#region Upload Doc to SharePoint IContract

        public string UploadFiletoIcontract(string Doctype, string regId, string IDcardValue, string MSISDN, string AccNo, string trnType, string idTypeDec, string StoreID, Byte[] file, string agentCode, string fileName)
		{

			uint Copyresults = 0;
			CopyResult[] resultupload;
			string destination = string.Empty;
			string destination1 = string.Empty;
			string destination2 = string.Empty;
			string result = string.Empty;

			switch (Doctype)
			{
				case "Contract":
					destination = Properties.Settings.Default.ContractUploadURL;
					break;
				case "IC":
					destination = Properties.Settings.Default.ICUploadURL;
					break;
				case "other_ID":
					destination = Properties.Settings.Default.OtherIDUploadURL;
					break;
				case "Passport":
					destination = Properties.Settings.Default.PassportUploadURL;
					break;
				case "Photo":
					destination = Properties.Settings.Default.PhotoUploadURL;
					break;
				case "others":
					destination = Properties.Settings.Default.OthersUploadURL;
					break;

				default:
					break;

			}

			string destinationPath = destination;

            if (Doctype.Equals("Contract"))
            {
                destination += fileName;
            }
            else
            {
                destination += regId + "-" + fileName ;   
            }

			string[] destinationURL = new string[] { destination };


			//MSISDN
			//AgentCode
			//IDcardvalue
			//iSellOrderID
			//AccountNumber
			//StoreID
            //ID Card Type
			//TransactionType

			FieldInformation info1 = new FieldInformation();
			info1.Id = System.Guid.NewGuid();
			info1.DisplayName = "MSISDN";
			info1.InternalName = "MSISDN";
			info1.Type = FieldType.Text;
			info1.Value = MSISDN;
			

			FieldInformation info2 = new FieldInformation();
			info2.Id = System.Guid.NewGuid();
			info2.DisplayName = "AgentCode";
			info2.InternalName = "AgentCode";
			info2.Type = FieldType.Text;
			info2.Value = agentCode;
			

			FieldInformation info3 = new FieldInformation();
			info3.Id = System.Guid.NewGuid();
			info3.DisplayName = "IDcardvalue";
			info3.InternalName = "IDcardvalue";
			info3.Type = FieldType.Text;
			info3.Value = IDcardValue;
			

			FieldInformation info4 = new FieldInformation();
			info4.Id = System.Guid.NewGuid();
			info4.DisplayName = "iSellOrderID";
			info4.InternalName = "iSellOrderID";
			info4.Type = FieldType.Text;
			info4.Value = regId;
			

			FieldInformation info5 = new FieldInformation();
			info5.Id = System.Guid.NewGuid();
			info5.DisplayName = "AccountNumber";
			info5.InternalName = "AccountNumber";
			info5.Type = FieldType.Text;
			info5.Value = AccNo;
			

			FieldInformation info6 = new FieldInformation();
			info6.Id = System.Guid.NewGuid();
			info6.DisplayName = "StoreID";
			info6.InternalName = "StoreID";
			info6.Type = FieldType.Text;
			info6.Value = StoreID;
			

			FieldInformation info7 = new FieldInformation();
			info7.Id = System.Guid.NewGuid();
			info7.DisplayName = "TransactionType";
			info7.InternalName = "TransactionType";
			info7.Type = FieldType.Text;
			info7.Value = trnType;
			

            FieldInformation info8 = new FieldInformation();
            info8.Id = System.Guid.NewGuid();
            info8.DisplayName = "IDType";
            info8.InternalName = "IDType";
            info8.Type = FieldType.Text;
            info8.Value = idTypeDec;


            FieldInformation[] info = { info1, info2, info3, info4, info5, info6, info7, info8 };

			Logger.Info("info iContract = " + XMLHelper.ConvertObjectToXml(info));
			Logger.Info("iContract file = "+ XMLHelper.ConvertObjectToXml(file));
			try
			{

				CopySoapClient _client = new CopySoapClient();
				_client.ClientCredentials.Windows.AllowNtlm = true;
				string username = Properties.Settings.Default.iContractUsername;
				string password = Properties.Settings.Default.iContractPassword;
				string domain = Properties.Settings.Default.iContractDomain;
				_client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.None;
				_client.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(username, password,domain);
				Logger.Info(string.Format("BEFORE UPLOAD = {0},{1}",destinationPath, destinationURL));
				Copyresults = _client.CopyIntoItems(destinationPath, destinationURL, info, file, out resultupload);

				if (resultupload != null && resultupload.Length > 0 && resultupload[0].ErrorCode == 0)
				{
                    result = resultupload[0].ErrorCode.ToString();
				}
				Logger.Info("iContractService-Upload = " + result);
			}
			catch (Exception ex)
			{
				Logger.Error("iContractService EXCEPTION = " + ex);
			}
            return result;
		}
		 #endregion
		/// <summary>
		/// Read file and return bytes
		/// </summary>
		/// <param name="path">file path</param>
		/// <returns>file bytes</returns>
		private byte[] GetFileFromFileSystem(string path)
		{
			byte[] fileBytes = null;

			if (File.Exists(path))
			{
				//read the file.
				using (FileStream fs = File.Open(path, FileMode.Open))
				{
					fileBytes = new byte[fs.Length];
					fs.Position = 0;
					fs.Read(fileBytes, 0, Convert.ToInt32(fs.Length));
				}
			}
			return fileBytes;
		}
	}


	
}

	   
	
	
