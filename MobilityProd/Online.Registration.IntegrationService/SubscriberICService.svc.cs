﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net;
using Online.Registration.IntegrationService.Models;
using System.Xml.Linq;
using Online.Registration.IntegrationService.Properties;
using log4net;
using Online.Registration.IntegrationService.Helper;
using Online.Registration.IntegrationService.RetrieveServiceInfo;
using Online.Registration.IntegrationService.ReassignVasService;
using Online.Registration.IntegrationService.MaxisChkFxVasDependencyService;
using Online.Registration.IntegrationService.ExtraTenWS;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Admin;
using System.Configuration;
using SNT.Utility;
using System.Diagnostics;
namespace Online.Registration.IntegrationService
{
    /// <summary>
    /// Class containing retrieveAcctListByIC,retrieveSubscriberDetls,retrieveServiceInfo,getPrinSupplimentarylines methods
    /// <Author>Sutan Dan</Author>
    /// </summary>
    public class SubscriberICService : ISubscriberICService
    {
        int counter = 0;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(KenanService));

        /// <summary>
        /// Retrieves MSIM sim details
        /// as proxy class cannot be generated.
        /// </summary>
        /// <param name="password">The password. Default "iselluser"</param>
        /// <param name="postUrl">The post URL. Default "http://10.200.51.122:7030/soap/rpc"</param>
        /// <returns>retrieveMobilenos</returns>
        public retrieveMismDetlsResponse retrieveMsimDetails(string MSISDN)
        {
            string userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            string password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;
            string postData = String.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:v11=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"" xmlns:v12=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
               <soapenv:Header>
                  <v1:eaiHeader>
                     <v1:from>ISELL</v1:from>
                     <v1:to>ESB</v1:to>
                     <v1:appId>OPF</v1:appId>
                     <v1:msgType>Request</v1:msgType>
                     <v1:msgId>1234567854</v1:msgId>
                     <v1:correlationId></v1:correlationId>
                     <v1:timestamp>20130622</v1:timestamp>
                  </v1:eaiHeader>
               </soapenv:Header>
               <soapenv:Body>
                  <v11:retrieveMismDetlsRequest>
                     <v12:retrieveMismDetlsRequestDetails>
                        <v12:msisdn>{0}</v12:msisdn>
                     </v12:retrieveMismDetlsRequestDetails>
                  </v11:retrieveMismDetlsRequest>
               </soapenv:Body>
            </soapenv:Envelope>", MSISDN);

            string dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
                              <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
                                <SOAP-ENV:Body>
                                <ser-root:retrieveMismDetlsResponse xmlns:ser-root=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"">
                                <enq:retrieveMismDetlsResponseDetails xmlns:enq=""maxis.eai.process.common.docType.enquiry:typeRetreiveSimDetlsResponse"">
                                <enq:msgCode>0</enq:msgCode>
                                <enq:msgDesc>success</enq:msgDesc>
                                <enq:mismDetls>
                               <enq:msisdn>60123098353</enq:msisdn>
                               <enq:fxAcctNo>112297676</enq:fxAcctNo>
                               <enq:fxSubscrNo>111581029</enq:fxSubscrNo>
                               <enq:fxSubscrNoResets>0</enq:fxSubscrNoResets>
                                <enq:primSecInd>P</enq:primSecInd>
                               </enq:mismDetls>
                                <enq:mismDetls>
                               <enq:msisdn>60179904669</enq:msisdn>
                               <enq:fxAcctNo>112297676</enq:fxAcctNo>
                               <enq:fxSubscrNo>112297676</enq:fxSubscrNo>
                               <enq:fxSubscrNoResets>0</enq:fxSubscrNoResets>
                                <enq:primSecInd>S</enq:primSecInd>
                               </enq:mismDetls>
                                <enq:mismDetls>
                               <enq:msisdn>60179904670</enq:msisdn>
                               <enq:fxAcctNo>112297677</enq:fxAcctNo>
                               <enq:fxSubscrNo>112297677</enq:fxSubscrNo>
                               <enq:fxSubscrNoResets>0</enq:fxSubscrNoResets>
                                <enq:primSecInd>S</enq:primSecInd>
                               </enq:mismDetls>
                                <enq:mismDetls>
                               <enq:msisdn>60179904671</enq:msisdn>
                               <enq:fxAcctNo>112297677</enq:fxAcctNo>
                               <enq:fxSubscrNo>112297677</enq:fxSubscrNo>
                               <enq:fxSubscrNoResets>0</enq:fxSubscrNoResets>
                                <enq:primSecInd>S</enq:primSecInd>
                               </enq:mismDetls>
                             </enq:retrieveMismDetlsResponseDetails>
                              </ser-root:retrieveMismDetlsResponse>
                                </SOAP-ENV:Body>
                                </SOAP-ENV:Envelope>";

            WebClient retrieveMsimDetlsWebClient = new WebClient();
            Uri postUri = new Uri(Properties.Settings.Default.Online_Registration_IntegrationService_Retrieve_MismDetails_Service.ToString());
            //Uri postUri = new Uri("http://10.200.51.122:7030/ws/maxis.eai.process.common.ws:retrieveSimDetlsService/maxis_eai_process_common_ws_retrieveSimDetlsService_Port");
            string postResult = string.Empty;

            retrieveMsimDetlsWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveMsimDetlsWebClient.Headers.Add("content-type", "text/xml");

            retrieveMismDetlsResponse ObjretList = null;

            try
            {

                //Depending upon settings if true then use dummy data
                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    postResult = dummyReplyData;
                }
                else
                {
                    postResult = retrieveMsimDetlsWebClient.UploadString(postUri, postData);
                }

                postResult = Util.RemoveAllNamespaces(postResult);
                Logger.Info(String.Format(" retrieveMismDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveMismDetls Response: {0} ", postResult));

                XDocument doc = XDocument.Parse(postResult);
                //Bellow code is for Parseing the xml and storing in to respetive form
                ObjretList = new retrieveMismDetlsResponse();
                foreach (var r in doc.Descendants("mismDetls"))
                {
                    ObjretList.itemList.Add(new MsimDetails
                    {
                        Msisdn = r.Element("msisdn") == null ? "" : r.Element("msisdn").Value.ToString(),
                        FxAcctNo = r.Element("fxAcctNo") == null ? "" : r.Element("fxAcctNo").Value.ToString(),
                        FxSubscrNo = r.Element("fxSubscrNo") == null ? "" : r.Element("fxSubscrNo").Value.ToString(),
                        FxSubscrNoResets = r.Element("fxSubscrNoResets") == null ? "" : r.Element("fxSubscrNoResets").Value.ToString(),
                        PrimSecInd = r.Element("primSecInd") == null ? "" : r.Element("primSecInd").Value.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                ObjretList = null;
            }

            return ObjretList;
        }


        /// <summary>
        /// Retrieves mobile nos
        /// as proxy class cannot be generated.
        /// </summary>
        /// <param name="password">The password. Default "iselluser"</param>
        /// <param name="postUrl">The post URL. Default "http://10.200.51.122:7030/soap/rpc"</param>
        /// <returns>retrieveMobilenos</returns>
        public retrieveDirectoryNumberResponse retrieveMobilenos(string salesChannelId, string inventoryTypeId, int rowNum)
        {
            Logger.Info("enter into retrieveMobilenos");
            Logger.Info("sales chaannel id#####################################" + salesChannelId);
            Logger.Info("inventoryTypeId  #####################################" + inventoryTypeId);
            Logger.Info("rowNum  id       #####################################" + rowNum);
            string userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            string password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;

            string postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:v11=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"" xmlns:v12=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
  <soapenv:Header>
      <v1:eaiHeader>
         <v1:from>ISELL</v1:from>
         <v1:to>ESB</v1:to>
         <v1:appId>OPF</v1:appId>
         <v1:msgType>Request</v1:msgType>
         <v1:msgId>ISELL2323</v1:msgId>
         <!--Optional:-->
         <v1:correlationId>?</v1:correlationId>
         <v1:timestamp>20130404121212</v1:timestamp>
      </v1:eaiHeader>
   </soapenv:Header>
   <soapenv:Body>	
      <v11:retrieveDirectoryNumberRequest>
         <v12:retrieveDirectoryNumberReqDetls>
            <v12:salesChannelId>{0}</v12:salesChannelId>
            <v12:inventoryTypeId>{1}</v12:inventoryTypeId>           
            <v12:rowNum>{2}</v12:rowNum>
         </v12:retrieveDirectoryNumberReqDetls>
      </v11:retrieveDirectoryNumberRequest>
   </soapenv:Body>
</soapenv:Envelope>", salesChannelId, inventoryTypeId, rowNum);

            string dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
                             <SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
   <SOAP-ENV:Header>
      <HDR:eaiHeader xmlns:HDR=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">
         <eai:from xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">EWB</eai:from>
         <eai:to xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">ISELL</eai:to>
         <eai:appId xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">OPF</eai:appId>
         <eai:msgType xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">Response</eai:msgType>
         <eai:msgId xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">46fbe25b-e081-441e-81d8-44ab42a1e71a</eai:msgId>
         <eai:correlationId xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">ISELL2323</eai:correlationId>
         <eai:timestamp xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">20130404105838</eai:timestamp>
      </HDR:eaiHeader>
   </SOAP-ENV:Header>
   <SOAP-ENV:Body>
      <ser-root:retrieveDirectoryNumberResponse xmlns:ser-root=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"">
         <enq:retrieveDirectoryNumberRespDetls xmlns:enq=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
            <enq:msgCode>0</enq:msgCode>
            <enq:msgDesc>success</enq:msgDesc>
            <enq:externalId>60323301053</enq:externalId>
            <enq:externalId>60323301059</enq:externalId>
            <enq:externalId>60323301061</enq:externalId>
            <enq:externalId>60323301063</enq:externalId>
            <enq:externalId>60323301067</enq:externalId>
            <enq:externalId>60323301068</enq:externalId>
            <enq:externalId>60328584000</enq:externalId>
            <enq:externalId>60328584013</enq:externalId>
            <enq:externalId>60328584042</enq:externalId>
            <enq:externalId>60328584043</enq:externalId>
            <enq:externalId>60328584049</enq:externalId>
            <enq:externalId>60328587006</enq:externalId>
            <enq:externalId>60328587007</enq:externalId>
            <enq:externalId>60328587008</enq:externalId>
            <enq:externalId>60328587009</enq:externalId>
            <enq:externalId>60328587012</enq:externalId>
            <enq:externalId>60328587014</enq:externalId>
            <enq:externalId>60328587015</enq:externalId>
            <enq:externalId>60358790051</enq:externalId>
            <enq:externalId>60358790052</enq:externalId>
            <enq:externalId>60358790092</enq:externalId>
            <enq:externalId>60358790141</enq:externalId>
            <enq:externalId>60358790142</enq:externalId>
            <enq:externalId>60358790154</enq:externalId>
            <enq:externalId>60358790162</enq:externalId>
            <enq:externalId>60358790170</enq:externalId>
            <enq:externalId>60358790175</enq:externalId>
            <enq:externalId>60358790177</enq:externalId>
            <enq:externalId>60358790178</enq:externalId>
            <enq:externalId>60358790179</enq:externalId>
            <enq:externalId>60358790186</enq:externalId>
            <enq:externalId>60358790187</enq:externalId>
            <enq:externalId>60358790189</enq:externalId>
            <enq:externalId>60358790190</enq:externalId>
            <enq:externalId>60358790191</enq:externalId>
            <enq:externalId>60358790192</enq:externalId>
            <enq:externalId>60358790193</enq:externalId>
            <enq:externalId>60358790198</enq:externalId>
            <enq:externalId>60358790202</enq:externalId>
            <enq:externalId>60358790203</enq:externalId>
            <enq:externalId>60358790204</enq:externalId>
            <enq:externalId>60358790206</enq:externalId>
            <enq:externalId>60358790213</enq:externalId>
            <enq:externalId>60358790214</enq:externalId>
            <enq:externalId>60358790215</enq:externalId>
            <enq:externalId>60358790217</enq:externalId>
            <enq:externalId>60358790218</enq:externalId>
            <enq:externalId>60358790219</enq:externalId>
            <enq:externalId>60358790221</enq:externalId>
            <enq:externalId>60358790223</enq:externalId>
            <enq:externalId>60358790226</enq:externalId>
            <enq:externalId>60358790228</enq:externalId>
            <enq:externalId>60358790231</enq:externalId>
            <enq:externalId>60358790232</enq:externalId>
            <enq:externalId>60358790233</enq:externalId>
            <enq:externalId>60358790234</enq:externalId>
            <enq:externalId>60358790235</enq:externalId>
            <enq:externalId>60358790236</enq:externalId>
            <enq:externalId>60358790237</enq:externalId>
            <enq:externalId>60358790238</enq:externalId>
            <enq:externalId>60358790239</enq:externalId>
            <enq:externalId>60358790242</enq:externalId>
            <enq:externalId>60358790243</enq:externalId>
            <enq:externalId>60358790244</enq:externalId>
            <enq:externalId>60358790245</enq:externalId>
            <enq:externalId>60358790246</enq:externalId>
            <enq:externalId>60358790248</enq:externalId>
            <enq:externalId>60358790249</enq:externalId>
            <enq:externalId>60358790250</enq:externalId>
            <enq:externalId>60358790251</enq:externalId>
            <enq:externalId>60358790252</enq:externalId>
            <enq:externalId>60358790253</enq:externalId>
            <enq:externalId>60358790254</enq:externalId>
            <enq:externalId>60358790255</enq:externalId>
            <enq:externalId>60358790256</enq:externalId>
            <enq:externalId>60358790257</enq:externalId>
            <enq:externalId>60358790258</enq:externalId>
            <enq:externalId>60358790259</enq:externalId>
            <enq:externalId>60358790260</enq:externalId>
            <enq:externalId>60358790261</enq:externalId>
            <enq:externalId>60358790262</enq:externalId>
            <enq:externalId>60358790263</enq:externalId>
            <enq:externalId>60358790264</enq:externalId>
            <enq:externalId>60358790265</enq:externalId>
            <enq:externalId>60358790266</enq:externalId>
            <enq:externalId>60358790267</enq:externalId>
            <enq:externalId>60358790268</enq:externalId>
            <enq:externalId>60358790269</enq:externalId>
            <enq:externalId>60358790270</enq:externalId>
            <enq:externalId>60358790272</enq:externalId>
            <enq:externalId>60358790273</enq:externalId>
            <enq:externalId>60358790274</enq:externalId>
            <enq:externalId>60358790275</enq:externalId>
            <enq:externalId>60358790276</enq:externalId>
            <enq:externalId>60358790277</enq:externalId>
            <enq:externalId>60358790278</enq:externalId>
            <enq:externalId>60328570279</enq:externalId>
            <enq:externalId>60328570280</enq:externalId>
            <enq:externalId>60328570281</enq:externalId>
            <enq:externalId>60328570282</enq:externalId>
         </enq:retrieveDirectoryNumberRespDetls>
      </ser-root:retrieveDirectoryNumberResponse>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>";
            WebClient retrieveDirectoryNumberResponse = new WebClient();
            Uri postUri = new Uri(Properties.Settings.Default.RetrieveMobilenosPostUrl.ToString());
            string postResult = string.Empty;

            retrieveDirectoryNumberResponse.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveDirectoryNumberResponse.Headers.Add("content-type", "text/xml");

            retrieveDirectoryNumberResponse ObjretList = null;
            Logger.Info("RetrieveMobilenumbers url is " + Properties.Settings.Default.RetrieveMobilenosPostUrl.ToString());
            Logger.Info(string.Format("Creadentenials are  uname is {0} and password is {1}", userName, password));

            try
            {

                //Depending upon settings if true then use dummy data
                //if (Properties.Settings.Default.MobileNoStub)
                //{
                //    postResult = dummyReplyData;
                //}
                //else
                //{
                postResult = retrieveDirectoryNumberResponse.UploadString(postUri, postData);
                Logger.Info("retrieveDirectoryNumberResponse" + postResult);
                //}

                postResult = Util.RemoveAllNamespaces(postResult);
                Logger.Info("retrieveDirectoryNumberResponse" + postResult);

                XDocument doc = XDocument.Parse(postResult);
                //Bellow code is for Parseing the xml and storing in to respetive form
                ObjretList = new retrieveDirectoryNumberResponse();
                foreach (var r in doc.Descendants("retrieveDirectoryNumberResponse").Descendants("retrieveDirectoryNumberRespDetls"))
                {
                    ObjretList.MsgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    ObjretList.MsgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();

                    if (ObjretList.MsgCode == "1")//Invalid Subscriber
                    {
                        ObjretList = null;
                        break;
                    }

                    // retrieveDirectoryNumberRespDetls

                    foreach (var itemsdata in r.Descendants("externalId"))
                    {
                        ObjretList.itemList.Add(new Items
                        {

                            ExternalId = itemsdata.Value == null ? "" : itemsdata.Value.ToString()

                        });
                    }
                }
                if (ObjretList != null)
                {

                    if (ObjretList.MsgCode != "0")
                    {
                        /// “0” – success
                        /// “1” - fail
                        /// “2” –system error
                        ObjretList = null;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                ObjretList = null;
            }

            return ObjretList;
        }

        public retrieveSimDetlsResponse retrieveSimDetails(string MSISDN)
        {
            string userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            string password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;
            string postData = String.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:v11=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"" xmlns:v12=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
               <soapenv:Header>
                  <v1:eaiHeader>
                     <v1:from>ISELL</v1:from>
                     <v1:to>ESB</v1:to>
                     <v1:appId>OPF</v1:appId>
                     <v1:msgType>Request</v1:msgType>
                     <v1:msgId>1234567854</v1:msgId>
                     <v1:correlationId></v1:correlationId>
                     <v1:timestamp>20130622</v1:timestamp>
                  </v1:eaiHeader>
               </soapenv:Header>
               <soapenv:Body>
                  <v11:retrieveSimDetlsRequest>
                     <v12:retrieveSimDetlsRequestDetails>
                        <v12:msisdn>{0}</v12:msisdn>
                     </v12:retrieveSimDetlsRequestDetails>
                  </v11:retrieveSimDetlsRequest>
               </soapenv:Body>
            </soapenv:Envelope>", MSISDN);

            string dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
                              <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
                                <SOAP-ENV:Body>
                                <ser-root:retrieveSimDetlsResponse xmlns:ser-root=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"">
                                <enq:retrieveSimDetlsResponseDetails xmlns:enq=""maxis.eai.process.common.docType.enquiry:typeRetreiveSimDetlsResponse"">
                                <enq:msgCode xmlns:enq=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">0</enq:msgCode>
                                <enq:msgDesc xmlns:enq=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">success</enq:msgDesc>
                                <enq:retrieveSimDetlsList xmlns:enq=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
                               <enq:externalId>8960010509306454716</enq:externalId>
                               <enq:externalIdType>12</enq:externalIdType>
                               <enq:inventoryTypeId>650</enq:inventoryTypeId>
                               <enq:inventoryDisplayValue>Postpaid - MISM SIM</enq:inventoryDisplayValue>
                                <enq:reason xsi:nil=""true""/>
                                <enq:activeDt>2006-05-08 00:00:00</enq:activeDt>
                               <enq:inactiveDt xsi:nil=""true""/>
                               </enq:retrieveSimDetlsList>
                             </enq:retrieveSimDetlsResponseDetails>
                              </ser-root:retrieveSimDetlsResponse>
                                </SOAP-ENV:Body>
                                </SOAP-ENV:Envelope>";

            WebClient retrieveSimDetlsWebClient = new WebClient();
            Uri postUri = new Uri(Properties.Settings.Default.maxis_eai_process_common_ws_retrieveSimDetlsService_Port.ToString());

            //Uri postUri = new Uri("http://10.200.51.122:7030/ws/maxis.eai.process.common.ws:retrieveSimDetlsService/maxis_eai_process_common_ws_retrieveSimDetlsService_Port");
            string postResult = string.Empty;

            retrieveSimDetlsWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveSimDetlsWebClient.Headers.Add("content-type", "text/xml");

            retrieveSimDetlsResponse ObjretList = null;

            try
            {

                //Depending upon settings if true then use dummy data
                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    postResult = dummyReplyData;
                }
                else
                {
                    postResult = retrieveSimDetlsWebClient.UploadString(postUri, postData);
                }

                postResult = Util.RemoveAllNamespaces(postResult);
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveSimDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSimDetls Response: {0} ", postResult));

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here
                XDocument doc = XDocument.Parse(postResult);
                //Bellow code is for Parseing the xml and storing in to respetive form
                ObjretList = new retrieveSimDetlsResponse();
                foreach (var r in doc.Descendants("retrieveSimDetlsList"))
                {
                    ObjretList.itemList.Add(new SimDetails
                    {
                        ExternalID = r.Element("externalId") == null ? "" : r.Element("externalId").Value.ToString(),
                        ExternalIDType = r.Element("externalIdType") == null ? 0 : Convert.ToInt32(r.Element("externalIdType").Value.ToString()),
                        InventoryTypeID = r.Element("inventoryTypeId") == null ? 0 : Convert.ToInt32(r.Element("inventoryTypeId").Value.ToString()),
                        InventoryDisplayValue = r.Element("inventoryDisplayValue") == null ? "" : r.Element("inventoryDisplayValue").Value.ToString(),
                        Reason = r.Element("reason") == null ? "" : r.Element("reason").Value.ToString(),
                        ActiveDt = r.Element("activeDt") == null ? "" : r.Element("activeDt").Value.ToString(),
                        InactiveDt = r.Element("inactiveDt") == null ? "" : r.Element("inactiveDt").Value.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                ObjretList = null;
            }

            return ObjretList;
        }

        /// <summary>
        /// Retrieves the acct list by IC Custom method to post retrieveAcctListByIC method belonging to retrieveAcctListByIC.wsdl
        /// as proxy class cannot be generated.
        /// </summary>
        /// <param name="icType">The username. Default "iselluser"</param>
        /// <param name="icValue">The password. Default "iselluser"</param>   
        /// <param name="ServiceException"></param>
        /// <returns>TypeOf(retrieveAcctListByICResponse)</returns>
        public retrieveAcctListByICResponse retrieveAcctListByIC(string icType, string icValue, string loggedUserName, ref string serviceException)
        {
            string userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            string password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;
            string postData = string.Empty;

            postData = string.Format(@"<soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:max=""http://www.webmethods.com/maxis.eai.process.common.ws"">
                                   <soapenv:Header/>
                                   <soapenv:Body>
                                      <max:retrieveAllAcctListByIC soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                                         <tranxType xsi:type=""ret:__tranxType"" xs:type=""type:__tranxType"" xmlns:ret=""http://localhost/maxis/eai/process/common/ws/retrieveAcctListByIC"" xmlns:xs=""http://www.w3.org/2000/XMLSchema-instance"">{0}</tranxType>
                                         <tranxVal xsi:type=""xsd:string"" xs:type=""type:string"" xmlns:xs=""http://www.w3.org/2000/XMLSchema-instance"">{1}</tranxVal>
                                         <filterPrepaid xsi:type=""xsd:string"" xs:type=""type:string"" xmlns:xs=""http://www.w3.org/2000/XMLSchema-instance"">N</filterPrepaid>
                                      </max:retrieveAllAcctListByIC>
                                   </soapenv:Body>
                                </soapenv:Envelope>", icType, icValue);

            DateTime dtReq = DateTime.Now;
            string dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
                              <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
                                <SOAP-ENV:Body>
                                <ser-root:retrieveAllAcctListByICResponse SOAP-ENC:root=""1"" xmlns:ser-root=""http://www.webmethods.com/maxis.eai.process.common.ws"">
                                <msgCode xsi:type=""xsd:string"">0</msgCode>
                                <msgDesc xsi:type=""xsd:string"">Valid Subscriber</msgDesc>
                                <acctDoc xsi:type=""SOAP-ENC:Array"" SOAP-ENC:arrayType=""sc1:__acctDoc[2]"" id=""id1"" xmlns:sc1=""http://localhost/maxis/eai/process/common/ws/retrieveAcctListByIC"">
                                <item xsi:type=""sc1:__acctDoc"" id=""id2"">
                                <acctIntId xsi:type=""xsd:string"">120053183</acctIntId>
                                <acctExtId xsi:type=""xsd:string"">1334274378</acctExtId>
                                <externalId xsi:type=""xsd:string"">60122000604</externalId>
                                <susbcrNo xsi:type=""xsd:string"">84189455</susbcrNo>
                                <susbcrNoResets xsi:type=""xsd:string"">0</susbcrNoResets>
                                </item>
                                <item xsi:type=""sc1:__acctDoc"" id=""id3"">
                                <acctIntId xsi:type=""xsd:string"">120053183</acctIntId>
                                <acctExtId xsi:type=""xsd:string"">1334274378</acctExtId>
                                <externalId xsi:type=""xsd:string"">60122009608</externalId>
                                <susbcrNo xsi:type=""xsd:string"">118072974</susbcrNo>
                                <susbcrNoResets xsi:type=""xsd:string"">0</susbcrNoResets>
                                </item>
                                <item xsi:type=""sc1:__acctDoc"" id=""id4"">
                                <acctIntId xsi:type=""xsd:string"">120053183</acctIntId>
                                <acctExtId xsi:type=""xsd:string"">1334274378</acctExtId>
                                <externalId xsi:type=""xsd:string"">60122009645</externalId>
                                <susbcrNo xsi:type=""xsd:string"">118072974</susbcrNo>
                                <susbcrNoResets xsi:type=""xsd:string"">0</susbcrNoResets>
                                </item>
                                <item xsi:type=""sc1:__acctDoc"" id=""id5"">
                                <acctIntId xsi:type=""xsd:string"">120053183</acctIntId>
                                <acctExtId xsi:type=""xsd:string"">1334274378</acctExtId>
                                <externalId xsi:type=""xsd:string"">60122010275</externalId>
                                <susbcrNo xsi:type=""xsd:string"">118072974</susbcrNo>
                                <susbcrNoResets xsi:type=""xsd:string"">0</susbcrNoResets>
                                </item>
                                <item xsi:type=""sc1:__acctDoc"" id=""id6"">
                                <acctIntId xsi:type=""xsd:string"">120053183</acctIntId>
                                <acctExtId xsi:type=""xsd:string"">1334537725</acctExtId>
                                <externalId xsi:type=""xsd:string"">60122377426</externalId>
                                <susbcrNo xsi:type=""xsd:string"">118072974</susbcrNo>
                                <susbcrNoResets xsi:type=""xsd:string"">0</susbcrNoResets>
                                </item>
                                </acctDoc>
                                </ser-root:retrieveAllAcctListByICResponse>
                                </SOAP-ENV:Body>
                                </SOAP-ENV:Envelope>";

            WebClient retrieveAcctListWebClient = new WebClient();
            Uri postUri = new Uri(Properties.Settings.Default.RetrieveSubscriberDetlsPostUrl.ToString());
            //Uri postUri = new Uri("http://10.200.51.122:7030/soap/rpc");
            string postResult = string.Empty;

            retrieveAcctListWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");

            retrieveAcctListByICResponse ObjretList = new retrieveAcctListByICResponse();

            try
            {
                //Depending upon settings if true then use dummy data
                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    postResult = dummyReplyData;
                }
                else
                {
                    postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                }

                postResult = Util.RemoveAllNamespaces(postResult);
                postData = Util.RemoveAllNamespaces(postData);
                DateTime dtRes = DateTime.Now;

                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveAcctListByIC Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveAcctListByIC Response: {0} ", postResult));

                DAL.Models.tblAPICallStatus tblAPICallStatusMessagesval = new DAL.Models.tblAPICallStatus();

                tblAPICallStatusMessagesval.APIName = "retrieveServiceInfoProxy";
                tblAPICallStatusMessagesval.MethodName = "APISaveCallStatus";
                tblAPICallStatusMessagesval.RequestTime = (dtReq == null ? System.DateTime.Now : Convert.ToDateTime(dtReq));
                tblAPICallStatusMessagesval.UserName = loggedUserName;
                tblAPICallStatusMessagesval.BreXmlReq = System.Web.HttpUtility.HtmlDecode(postData);
                tblAPICallStatusMessagesval.BreXmlRes = System.Web.HttpUtility.HtmlDecode(postResult);
                tblAPICallStatusMessagesval.IdCardNo = icValue;
                tblAPICallStatusMessagesval.ResponseTime = (dtRes == null ? System.DateTime.Now : Convert.ToDateTime(dtRes));
                tblAPICallStatusMessagesval.Status = "Success";


                int result = Online.Registration.DAL.Admin.OnlineRegAdmin.APISaveCallStatus(tblAPICallStatusMessagesval);

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here
                XDocument doc = XDocument.Parse(postResult);
                //Bellow code is for Parseing the xml and storing in to respetive form
                ObjretList = new retrieveAcctListByICResponse();
                //Items item = null;


                foreach (var r in doc.Descendants("retrieveAllAcctListByICResponseDetails"))
                {
                    ObjretList.MsgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    ObjretList.MsgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();

                    if (ObjretList.MsgCode == "1")//Invalid Subscriber
                    {
                        serviceException = Convert.ToString(ObjretList.MsgDesc);
                        ObjretList = null;
                        break;
                    }

                    foreach (var itemsdata in r.Descendants("acctDoc").Descendants("item"))
                    {

                        ///INDEX OUT OF ARRAY ISSUE FIXED BY SUTAN
                        ObjretList.itemList.Add(new Items
                        {
                            AcctIntId = itemsdata.Element("acctIntId") == null ? "" : itemsdata.Element("acctIntId").Value.ToString(),
                            AcctExtId = itemsdata.Element("acctExtId") == null ? "" : itemsdata.Element("acctExtId").Value.ToString(),
                            ExternalId = itemsdata.Element("externalId") == null ? "" : itemsdata.Element("externalId").Value.ToString(),
                            SusbcrNo = itemsdata.Element("susbcrNo") == null ? "" : itemsdata.Element("susbcrNo").Value.ToString(),
                            SusbcrNoResets = itemsdata.Element("susbcrNoResets") == null ? "" : itemsdata.Element("susbcrNoResets").Value.ToString(),
                            IsActive = itemsdata.Element("srvInactiveDate") == null ? true : (itemsdata.Element("srvInactiveDate").Value == string.Empty ? true : false),


                        });
                    }
                }
                if (ObjretList != null)
                {
                    if (ObjretList.MsgCode != "0")
                    {
                        /// “0” – success
                        /// “1” - fail
                        /// “2” –system error

                        serviceException = Convert.ToString(ObjretList.MsgDesc);
                        //ObjretList = null;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                serviceException = ex.Message;
                ObjretList.MsgCode = "-1";
                ObjretList.MsgDesc = ex.Message;
                //ObjretList = null;
            }

            return ObjretList;
        }

        #region RetrieveBillngInfo Added by Sindhu on 4-Jun-2013 for Retrieveing Billing Details

        public Dictionary<string, string> retrieveBillingInfoAll(List<string> acctNos)
        {
            Dictionary<string, string> billingAmounts = new Dictionary<string, string>();

            foreach (string acctNo in acctNos)
            {
                retrieveBillngInfoResponse response = retrieveBillingInfo(acctNo);
                if (!string.IsNullOrEmpty(response.totalAmtDue))
                {
                    billingAmounts.Add(acctNo, response.totalAmtDue);
                }
            }

            return billingAmounts;

        }
        /// <summary>
        /// Retrieves Billing Details based on Account number
        /// </summary>
        /// <param name="acctNo"></param>
        /// <param name="icValue"></param>
        /// <returns></returns>
        public retrieveBillngInfoResponse retrieveBillingInfo(string acctNo)
        {
            string userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            string password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;

            string postData = string.Format(@"<soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:max=""http://www.webmethods.com/maxis.eai.process.common.ws"">
                                            <soapenv:Header /> 
                                            <soapenv:Body>
                                                <max:retrieveBillingInfo soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                                                <tranxType xsi:type=""ret:__tranxType"" 
                                                xmlns:ret=""http://localhost/maxis/eai/process/common/ws/retrieveBillingInfo"">ACCT</tranxType> 
                                                <tranxVal xsi:type=""ret:__tranxVal"" 
                                                xmlns:ret=""http://localhost/maxis/eai/process/common/ws/retrieveBillingInfo"">{0}</tranxVal> 
                                                </max:retrieveBillingInfo>
                                            </soapenv:Body>
                                            </soapenv:Envelope>", acctNo);

            DateTime dtReq = DateTime.Now;
            string dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
                              <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
                                <SOAP-ENV:Body>
                                <ser-root:retrieveBillingInfoResponse SOAP-ENC:root=""1"" xmlns:ser-root=""http://www.webmethods.com/maxis.eai.process.common.ws"">
                                <msgCode xsi:type=""xsd:string"">0</msgCode>
                                 <msgDesc xsi:type=""xsd:string"">Valid Subscriber</msgDesc>
                                 <totalAmtDue xsi:type=""xsd:string"">167.5</totalAmtDue>
                                 <balanceForward xsi:type=""xsd:string"">88.75</balanceForward>
                                 <totalCurrBill xsi:type=""xsd:string"">78.75</totalCurrBill>
                                 <currBillDate xsi:type=""xsd:string"">20100617</currBillDate>
                                 <unbilledAmt xsi:type=""xsd:string"">19.03</unbilledAmt>
                                 <unbilledEffDate xsi:type=""xsd:string"">2105201317</unbilledEffDate>
                                 <creditLimit xsi:type=""xsd:string"">0</creditLimit>
                                 <payResp xsi:type=""xsd:string"">Y</payResp>
                                 <deposit xsi:type=""xsd:string"">100</deposit>
                                </ser-root:retrieveBillingInfoResponse>
                                </SOAP-ENV:Body>
                                </SOAP-ENV:Envelope>";

            WebClient retrieveBilingInfoWebClient = new WebClient();
            Uri postUri = new Uri(Properties.Settings.Default.RetrieveBillingInfoPostUrl.ToString());

            string postResult = string.Empty;
            retrieveBillngInfoResponse billingInfoDetails = new retrieveBillngInfoResponse();

            retrieveBilingInfoWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);

            // commented due getting exception
            //byte[] credentialBuffer = new UTF8Encoding().GetBytes(userName + ":" + password);
            //retrieveBilingInfoWebClient.Headers["Authorization"] = "Basic" + Convert.ToBase64String(credentialBuffer);
            retrieveBilingInfoWebClient.Headers.Add("content-type", "text/xml");

            try
            {
                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    postResult = dummyReplyData;
                }
                else
                {
                    postResult = retrieveBilingInfoWebClient.UploadString(postUri, postData);
                }

                postResult = Util.RemoveAllNamespaces(postResult);

                Logger.Info(String.Format(" retrieveBillingInfo Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveBillingInfo Response: {0} ", postResult));

                XDocument doc = XDocument.Parse(postResult);

                //Bellow code is for Parseing the xml and storing in to respetive form
                foreach (var r in doc.Descendants("retrieveBillingInfoResponse"))
                {
                    billingInfoDetails.msgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    //billingInfoDetails.msgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
                    billingInfoDetails.totalAmtDue = r.Element("totalAmtDue") == null ? "" : r.Element("totalAmtDue").Value.ToString();
                    //billingInfoDetails.balanceForward = r.Element("balanceForward") == null ? "" : r.Element("balanceForward").Value.ToString();
                    //billingInfoDetails.totalCurrBill = r.Element("totalCurrBill") == null ? "" : r.Element("totalCurrBill").Value.ToString();
                    //billingInfoDetails.currBillDate = r.Element("currBillDate") == null ? "" : r.Element("currBillDate").Value.ToString();
                    //billingInfoDetails.unbilledAmt = r.Element("unbilledAmt") == null ? "" : r.Element("unbilledAmt").Value.ToString();
                    //billingInfoDetails.unbilledEffDate = r.Element("unbilledEffDate") == null ? "" : r.Element("unbilledEffDate").Value.ToString();
                    //billingInfoDetails.creditLimit = r.Element("creditLimit") == null ? "" : r.Element("creditLimit").Value.ToString();
                    billingInfoDetails.payResp = r.Element("payResp") == null ? "" : r.Element("payResp").Value.ToString();
                    //billingInfoDetails.deposit = r.Element("deposit") == null ? "" : r.Element("deposit").Value.ToString();
                }
                if (billingInfoDetails != null)
                {
                    if (billingInfoDetails.msgCode != "0")
                    {
                        /// “0” – success
                        /// “1” - fail
                        /// “2” –system error
                        billingInfoDetails = null;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }

            return billingInfoDetails;
        }
        #endregion

        #region Packagedetails

        public IList<PackageModel> retrievePackageDetls(string MSISDN, string kenancode, string userName = "", string password = "", string postUrl = "")
        {
            userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;

            string postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:v11=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"" xmlns:v12=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
                                                <soapenv:Header>
                                                    <v1:eaiHeader>
                                                        <v1:from>ISELL</v1:from>
                                                        <v1:to>ESB</v1:to>
                                                        <v1:appId>OPF</v1:appId>
                                                        <v1:msgType>Request</v1:msgType>
                                                        <v1:msgId>ISELL12345</v1:msgId>
                                                        <!--Optional:-->
                                                        <v1:correlationId>?</v1:correlationId>
                                                        <v1:timestamp>20130316</v1:timestamp>
                                                    </v1:eaiHeader>
                                                </soapenv:Header>
                                                    <soapenv:Body>
                                                        <v11:retrievePkgCompInfoRequest>
                                                        <v12:retrievePkgCompInfoRequestDetails>
                                                        <v12:externalId>{0}</v12:externalId>
                                                        <v12:extenalIdType>{1}</v12:extenalIdType>
                                                        </v12:retrievePkgCompInfoRequestDetails>
                                                        </v11:retrievePkgCompInfoRequest>
                                                    </soapenv:Body>
                                                </soapenv:Envelope>", MSISDN, kenancode);
            WebClient retrieveAcctListWebClient = new WebClient();
            Uri postUri = new Uri(Properties.Settings.Default.Online_Registration_IntegrationService_retrievePkgCompInfoService.ToString());
            string postResult = string.Empty;

            IList<PackageModel> packs = new List<PackageModel>();

            retrieveAcctListWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");
            //            postResult =
            //                string.Format(@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
            //                                <SOAP-ENV:Header>
            //                                <HDR:eaiHeader xmlns:HDR=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">
            //                                <eai:from xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">?</eai:from>
            //                                <eai:to xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">?</eai:to>
            //                                <eai:appId xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">?</eai:appId>
            //                                <eai:msgType xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">Response</eai:msgType>
            //                                <eai:msgId xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">a570b788-82d5-4c9f-9451-ba3becf5639f</eai:msgId>
            //                                <eai:correlationId xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">?</eai:correlationId>
            //                                <eai:timestamp xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">20130613054111</eai:timestamp>
            //                                </HDR:eaiHeader>
            //                                </SOAP-ENV:Header>
            //                                <SOAP-ENV:Body>
            //                                <ser-root:retrievePkgCompInfoResponse xmlns:ser-root=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"">
            //                                <enq:retrievePkgCompInfoResponseDetails xmlns:enq=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
            //                                <enq:msgCode>0</enq:msgCode>
            //                                <enq:msgDesc>success</enq:msgDesc>
            //                                <enq:fxAcctIntId>101846553</enq:fxAcctIntId>
            //                                <enq:fxSubscrNo>102349663</enq:fxSubscrNo>
            //                                <enq:fxSubscrNoResets>0</enq:fxSubscrNoResets>
            //                                <enq:packages>
            //                                <enq:packageId>40267</enq:packageId>
            //                                <enq:packageInstId>17700881</enq:packageInstId>
            //                                <enq:packageInstIdServ>7</enq:packageInstIdServ>
            //                                <enq:packageActiveDt>20080324000000</enq:packageActiveDt>
            //                                <enq:packageInactiveDt/>
            //                                <enq:packageDesc>MOBILE DISCOUNT PACKAGES</enq:packageDesc>
            //                                <enq:components>
            //                                <enq:componentId>41304</enq:componentId>
            //                                <enq:componentInstId>54184412</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20080324000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - Special Rebate RM 20 x 5 months</enq:componentDesc>
            //                                <enq:componentShortDisplay>0</enq:componentShortDisplay>
            //                                </enq:components>
            //                                </enq:packages>
            //                                <enq:packages>
            //                                <enq:packageId>41547</enq:packageId>
            //                                <enq:packageInstId>17700879</enq:packageInstId>
            //                                <enq:packageInstIdServ>7</enq:packageInstIdServ>
            //                                <enq:packageActiveDt>20080324000000</enq:packageActiveDt>
            //                                <enq:packageInactiveDt/>
            //                                <enq:packageDesc>Mobile Supp Line (Family Plus) 50 - 01 Mandatory Package</enq:packageDesc>
            //                                <enq:components>
            //                                <enq:componentId>40021</enq:componentId>
            //                                <enq:componentInstId>54184396</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20080324000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - Access IDD Calls</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                <enq:components>
            //                                <enq:componentId>40023</enq:componentId>
            //                                <enq:componentInstId>54184402</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20080324000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - Short Message Service (MO)</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                <enq:components>
            //                                <enq:componentId>40026</enq:componentId>
            //                                <enq:componentInstId>54184397</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20080324000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - CLIP-Caller Line Id Pres</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                <enq:components>
            //                                <enq:componentId>40027</enq:componentId>
            //                                <enq:componentInstId>54184399</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20080324000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - Call Waiting</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                <enq:components>
            //                                <enq:componentId>40028</enq:componentId>
            //                                <enq:componentInstId>54184398</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20080324000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - Call Hold</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                <enq:components>
            //                                <enq:componentId>40030</enq:componentId>
            //                                <enq:componentInstId>63456521</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20081229000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - WAP</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                <enq:components>
            //                                <enq:componentId>40032</enq:componentId>
            //                                <enq:componentInstId>61409310</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20081113000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - MMS Service Package</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                <enq:components>
            //                                <enq:componentId>41028</enq:componentId>
            //                                <enq:componentInstId>54184403</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20080324000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - Telephony</enq:componentDesc>
            //                                <enq:componentShortDisplay>SUPP</enq:componentShortDisplay>
            //                                </enq:components>
            //                                <enq:components>
            //                                <enq:componentId>41316</enq:componentId>
            //                                <enq:componentInstId>54184400</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20080324000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - Family Plus Free Talktime &amp; SMS</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                </enq:packages>
            //                                <enq:packages>
            //                                <enq:packageId>40455</enq:packageId>
            //                                <enq:packageInstId>22134287</enq:packageInstId>
            //                                <enq:packageInstIdServ>7</enq:packageInstIdServ>
            //                                <enq:packageActiveDt>20090620000000</enq:packageActiveDt>
            //                                <enq:packageInactiveDt/>
            //                                <enq:packageDesc>Mobile Supp Line (Family Plus) - 02 Main Package</enq:packageDesc>
            //                                <enq:components>
            //                                <enq:componentId>40034</enq:componentId>
            //                                <enq:componentInstId>70182062</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20090620000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - Call Forwarding</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                </enq:packages>
            //                                <enq:packages>
            //                                <enq:packageId>40457</enq:packageId>
            //                                <enq:packageInstId>22134245</enq:packageInstId>
            //                                <enq:packageInstIdServ>7</enq:packageInstIdServ>
            //                                <enq:packageActiveDt>20090620000000</enq:packageActiveDt>
            //                                <enq:packageInactiveDt/>
            //                                <enq:packageDesc>Mobile Supp Line (Family Plus) - 04 3G Package</enq:packageDesc>
            //                                <enq:components>
            //                                <enq:componentId>40082</enq:componentId>
            //                                <enq:componentInstId>70182069</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20090620000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - GPRS Standard (UNET/3G)</enq:componentDesc>
            //                                <enq:componentShortDisplay>GPSPM</enq:componentShortDisplay>
            //                                </enq:components>
            //                                <enq:components>
            //                                <enq:componentId>40089</enq:componentId>
            //                                <enq:componentInstId>70182063</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20090620000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - 3G Video Mail</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                <enq:components>
            //                                <enq:componentId>40090</enq:componentId>
            //                                <enq:componentInstId>70182059</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20090620000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - 3G Video Telephony</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                </enq:packages>
            //                                <enq:packages>
            //                                <enq:packageId>40667</enq:packageId>
            //                                <enq:packageInstId>18845892</enq:packageInstId>
            //                                <enq:packageInstIdServ>7</enq:packageInstIdServ>
            //                                <enq:packageActiveDt>20080924000000</enq:packageActiveDt>
            //                                <enq:packageInactiveDt/>
            //                                <enq:packageDesc>Mobile Rebate for GPRS Usage</enq:packageDesc>
            //                                <enq:components>
            //                                <enq:componentId>41739</enq:componentId>
            //                                <enq:componentInstId>58803366</enq:componentInstId>
            //                                <enq:componentInstIdServ>7</enq:componentInstIdServ>
            //                                <enq:componentActiveDt>20080924000000</enq:componentActiveDt>
            //                                <enq:componentInactiveDt/>
            //                                <enq:componentDesc>Mobile - GPRS Capping @ RM250</enq:componentDesc>
            //                                <enq:componentShortDisplay/>
            //                                </enq:components>
            //                                </enq:packages>
            //                                </enq:retrievePkgCompInfoResponseDetails>
            //                                </ser-root:retrievePkgCompInfoResponse>
            //                                </SOAP-ENV:Body>
            //                                </SOAP-ENV:Envelope>", "");
            try
            {
                //Depending upon settings if true then use dummy data
                //if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                //{
                //    postResult = dummyResultData;
                //}
                //else
                //{
                //    postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                //}

                postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                postResult = Util.RemoveAllNamespaces(postResult);
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", postResult));

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here

                XDocument doc = XDocument.Parse(postResult);
                foreach (var p in doc.Descendants("packages"))
                {
                    PackageModel Comp = new PackageModel();
                    IList<Components> listOfThings = new List<Components>();
                    Comp.packageActiveDt = p.Element("packageActiveDt") == null ? "" : p.Element("packageActiveDt").Value.ToString();
                    Comp.PackageID = p.Element("packageId") == null ? "" : p.Element("packageId").Value.ToString();
                    Comp.PackageDesc = p.Element("packageDesc") == null ? "" : p.Element("packageDesc").Value.ToString();
                    Comp.packageInstId = p.Element("packageInstId") == null ? "" : p.Element("packageInstId").Value.ToString();
                    Comp.packageInstIdServ = p.Element("packageInstIdServ") == null ? "" : p.Element("packageInstIdServ").Value.ToString();
                    foreach (var r in p.Descendants("components"))
                    {

                        Components Components = new Components();
                        Components.componentId = r.Element("componentId") == null ? "" : r.Element("componentId").Value.ToString();
                        Components.componentInstId = r.Element("componentInstId") == null ? "" : r.Element("componentInstId").Value.ToString();
                        Components.componentInstIdServ = r.Element("componentInstIdServ") == null ? "" : r.Element("componentInstIdServ").Value.ToString();
                        Components.componentShortDisplay = r.Element("componentShortDisplay") == null ? "" : r.Element("componentShortDisplay").Value.ToString();
                        Components.componentActiveDt = r.Element("componentActiveDt") == null ? "" : r.Element("componentActiveDt").Value.ToString();
                        Components.componentDesc = r.Element("componentDesc") == null ? "" : r.Element("componentDesc").Value.ToString();
                        Components.componentInactiveDt = r.Element("componentInactiveDt") == null ? "" : r.Element("componentInactiveDt").Value.ToString();
                        listOfThings.Add(Components);
                    }

                    Comp.compList = (List<Components>)listOfThings;
                    packs.Add(Comp);
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            return packs;
        }




        #endregion

        /// <summary>
        /// Retrieveing account details for a particular acctExtId
        /// </summary>
        /// <returns></returns>
        public AccountDetails RetrieveAccountDetails(MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsRequest request)
        {
            var kenanRequest = string.Empty;
            var kenanResponse = string.Empty;
            var acctDetails = new AccountDetails();
            Logger.Info(string.Format("Entering Retrieve Account Details(): Account ExternalId({0})", request.fxAcctExtId));
            var response = new MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsResponse();
            try
            {

                using (var proxy = new MaxisRetrieveAccountDetails.maxiseaiprocesscommonwsretrieveAcctDetlsService())
                {
                    proxy.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                    proxy.PreAuthenticate = true;

                    proxy.eaiHeaderValue = new MaxisRetrieveAccountDetails.eaiHeader()
                    {
                        from = Properties.Resources.orderProcessReq_from,
                        to = Properties.Resources.orderProcessReq_to,
                        appId = Properties.Resources.orderProcessReq_appID,
                        msgType = Properties.Resources.orderProcessReq_msgType,
                        msgId = string.Format("CO{0}", ""),
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    };


                    kenanRequest = XMLHelper.ConvertObjectToXml(request, typeof(MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsRequest));
                    Logger.Info("Retrieve Account Details Request  :" + kenanRequest);
                    response = proxy.retrieveAcctDetls(request);
                    if (response.msgCode == "0")
                    {


                        acctDetails.FxAcctNo = response.fxAcctNoField;

                        acctDetails.ParentId = response.parentIdField;

                        acctDetails.HierarchyId = response.hierarchyIdField;

                        acctDetails.FxAcctExtId = response.fxAcctExtIdField;

                        acctDetails.AcctCategory = response.acctCategoryField;

                        acctDetails.AcctSegId = response.acctSegIdField;

                        acctDetails.MktCode = response.mktCodeField;

                        acctDetails.VipCode = response.vipCodeField;

                        acctDetails.CreditLimit = response.creditLimitField;

                        acctDetails.DirectDebit = response.directDebitField;

                        acctDetails.BillCycle = response.billCycleField;

                        acctDetails.PaymntResp = response.paymntRespField;

                        acctDetails.CollectionStatus = response.collectionStatusField;

                        acctDetails.AcctActiveDt = response.acctActiveDtField;

                        acctDetails.TotalWriteOff = response.totalWriteOffField;

                        acctDetails.AcctType = response.acctTypeField;
                    }
                    if (acctDetails != null && !string.IsNullOrEmpty(acctDetails.MktCode))
                    {
                        DAL.Models.Market market = new Market();
                        market.KenanCode = acctDetails.MktCode;
                        var markets = DAL.Admin.OnlineRegAdmin.MarketSearch(new MarketFind { Market = market });
                        acctDetails.CustomerType = markets.Any() ? markets[0].Name : string.Empty;
                    }
                    kenanResponse = XMLHelper.ConvertObjectToXml(response, typeof(MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsResponse));
                    Logger.Info("Retrieve Account Details Response  :" + kenanResponse);


                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            Logger.Info(string.Format("Exiting Retrieve Account Details(): Account ExternalId({0})", request.fxAcctExtId));
            return acctDetails;
        }

        /// <summary>
        /// Retrieveing account details for a particular acctExtId
        /// </summary>
        /// <returns></returns>
        public AccountDetails RetrieveAccountDetails(string acctNo)
        {
            var kenanRequest = string.Empty;
            var kenanResponse = string.Empty;
            var acctDetails = new AccountDetails();
            Logger.Info(string.Format("Entering Retrieve Account Details(): Account ExternalId({0})", acctNo));
            var response = new MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsResponse();
            MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsRequest request = new MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsRequest();
            request.fxAcctExtId = acctNo;
            try
            {

                using (var proxy = new MaxisRetrieveAccountDetails.maxiseaiprocesscommonwsretrieveAcctDetlsService())
                {
                    proxy.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                    proxy.PreAuthenticate = true;

                    proxy.eaiHeaderValue = new MaxisRetrieveAccountDetails.eaiHeader()
                    {
                        from = Properties.Resources.orderProcessReq_from,
                        to = Properties.Resources.orderProcessReq_to,
                        appId = Properties.Resources.orderProcessReq_appID,
                        msgType = Properties.Resources.orderProcessReq_msgType,
                        msgId = string.Format("CO{0}", ""),
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    };


                    kenanRequest = XMLHelper.ConvertObjectToXml(request, typeof(MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsRequest));
                    Logger.Info("Retrieve Account Details Request  :" + kenanRequest);
                    response = proxy.retrieveAcctDetls(request);
                    if (response.msgCode == "0")
                    {


                        acctDetails.FxAcctNo = response.fxAcctNoField;

                        acctDetails.ParentId = response.parentIdField;

                        acctDetails.HierarchyId = response.hierarchyIdField;

                        acctDetails.FxAcctExtId = response.fxAcctExtIdField;

                        acctDetails.AcctCategory = response.acctCategoryField;

                        acctDetails.AcctSegId = response.acctSegIdField;

                        acctDetails.MktCode = response.mktCodeField;

                        acctDetails.VipCode = response.vipCodeField;

                        acctDetails.CreditLimit = response.creditLimitField;

                        acctDetails.DirectDebit = response.directDebitField;

                        acctDetails.BillCycle = response.billCycleField;

                        acctDetails.PaymntResp = response.paymntRespField;

                        acctDetails.CollectionStatus = response.collectionStatusField;

                        acctDetails.AcctActiveDt = response.acctActiveDtField;

                        acctDetails.TotalWriteOff = response.totalWriteOffField;

                        acctDetails.AcctType = response.acctTypeField;

                    }
                    if (acctDetails != null && !string.IsNullOrEmpty(acctDetails.MktCode))
                    {
                        DAL.Models.Market market = new Market();
                        market.KenanCode = acctDetails.MktCode;
                        var markets = DAL.Admin.OnlineRegAdmin.MarketSearch(new MarketFind { Market = market });
                        acctDetails.CustomerType = markets.Any() ? markets[0].Name : string.Empty;
                    }
                    kenanResponse = XMLHelper.ConvertObjectToXml(response, typeof(MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsResponse));
                    Logger.Info("Retrieve Account Details Response  :" + kenanResponse);


                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            Logger.Info(string.Format("Exiting Retrieve Account Details(): Account ExternalId({0})", request.fxAcctExtId));
            return acctDetails;
        }
        #region old method retrieveSubscriberDetls
        /**
        /// <summary>
        /// Retrieves the subscriber detls. Posts to retrieveSubscriberDetls webmethod belonging to retrieveSubscriberDetls.wsdl
        /// as proxy class cannot be generated.
        /// </summary>
        /// <param name="Msisdn">The msisdn.</param>
        /// <param name="Msisdn">Msisdn</param>        
        /// <returns>Typeof(CustomizedCustomer)</returns>
        public CustomizedCustomer retrieveSubscriberDetlsByAcctNo(string acctNo, string loggedUserName, bool isSupAdmin)
        {
            string userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            string password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;
            string filterPrepaid = Properties.Settings.Default.filterPrepaid;
            string needMask = Properties.Settings.Default.needMask;
            if (isSupAdmin)
            {
                needMask = "N";
            }
            else
            {
                needMask = "Y";
            }

            string postData =
                string.Format(@"<soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:max=""http://www.webmethods.com/maxis.eai.process.common.ws"">
<soapenv:Header/>
<soapenv:Body>
<max:retrieveSubscriberDetls soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
<tranxType xsi:type=""ret:__tranxType"" xs:type=""type:__tranxType"" xmlns:ret=""http://localhost/maxis/eai/process/common/ws/retrieveSubscriberDetls"" xmlns:xs=""http://www.w3.org/2000/XMLSchema-instance"">ACCT</tranxType>
<tranxVal xsi:type=""xsd:string"" xs:type=""type:string"" xmlns:xs=""http://www.w3.org/2000/XMLSchema-instance"">{0}</tranxVal>
<needMask xsi:type=""xsd:string"">{1}</needMask>
</max:retrieveSubscriberDetls>
</soapenv:Body>
</soapenv:Envelope>", acctNo, needMask);

            DateTime dtReq = DateTime.Now;
            string dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
                        <SOAP-ENV:Envelope
                        xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/""
                        xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/""
                        xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
                        xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                        SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                        <SOAP-ENV:Body>
                        <ser-root:retrieveSubscriberDetlsResponse xmlns:ser-root=""http://www.webmethods.com/maxis.eai.process.common.ws"" SOAP-ENC:root='1'>
                        <msgCode xsi:type=""xsd:string"">0</msgCode>
                        <msgDesc xsi:type=""xsd:string"">Valid Subscriber</msgDesc>
                        <title xsi:type=""xsd:string"" xsi:nil=""true""></title>
                        <cust_name xsi:type=""xsd:string"">VALUELABS SDN BHD</cust_name>
                        <dob xsi:type=""xsd:string"" xsi:nil=""true""></dob>
                        <old_ic xsi:type=""xsd:string"" xsi:nil=""true""></old_ic>
                        <new_ic xsi:type=""xsd:string"" xsi:nil=""true""></new_ic>
                        <brn xsi:type=""xsd:string"">669664X</brn>
                        <passport_no xsi:type=""xsd:string"" xsi:nil=""true""></passport_no>
                        <other_ic xsi:type=""xsd:string"" xsi:nil=""true""></other_ic>
                        <addr1 xsi:type=""xsd:string"">UNIT 2.3 ENTERPRISE 3A</addr1>
                        <addr2 xsi:type=""xsd:string"">TECHNOLOGY PARK MALAYSIA</addr2>
                        <addr3 xsi:type=""xsd:string"">LEBUHRAYA PUCHONG-SUNGAI BESI BKT JALIL</addr3>
                        <post_cd xsi:type=""xsd:string"">57000</post_cd>
                        <city xsi:type=""xsd:string"">Kuala Lumpur</city>
                        <state xsi:type=""xsd:string"">Wilayah Persekutuan</state>
                        <cbr xsi:type=""xsd:string"">60389966128</cbr>
                        <moc xsi:type=""xsd:string"" xsi:nil=""true""></moc>
                        <risk_category xsi:type=""xsd:string"" xsi:nil=""true""></risk_category>
                        <acctActiveDt xsi:type=""xsd:string""></acctActiveDt>
                        <totalWriteOff xsi:type=""xsd:string""></totalWriteOff>
                        </ser-root:retrieveSubscriberDetlsResponse>
                        </SOAP-ENV:Body>
                        </SOAP-ENV:Envelope>";


            WebClient retrieveAcctListWebClient = new WebClient();
            Uri postUri = new Uri(Properties.Settings.Default.RetrieveSubscriberDetlsPostUrl);
            string postResult = string.Empty;

            retrieveAcctListWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");

            CustomizedCustomer customer = new CustomizedCustomer();

            try
            {

                //Depending upon settings if true then use dummy data
                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    postResult = dummyReplyData;
                }
                else
                {
                    postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                }

                postResult = Util.RemoveAllNamespaces(postResult);
                DateTime dtRes = DateTime.Now;
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", postResult));

                DAL.Models.tblAPICallStatus tblAPICallStatusMessagesval = new DAL.Models.tblAPICallStatus();

                tblAPICallStatusMessagesval.APIName = "retrieveServiceInfoProxy";
                tblAPICallStatusMessagesval.MethodName = "APISaveCallStatus";
                tblAPICallStatusMessagesval.RequestTime = (dtReq == null ? System.DateTime.Now : Convert.ToDateTime(dtReq));
                tblAPICallStatusMessagesval.UserName = loggedUserName;
                tblAPICallStatusMessagesval.BreXmlReq = System.Web.HttpUtility.HtmlDecode(postData);
                tblAPICallStatusMessagesval.BreXmlRes = System.Web.HttpUtility.HtmlDecode(postResult);
                tblAPICallStatusMessagesval.IdCardNo = acctNo;
                tblAPICallStatusMessagesval.ResponseTime = (dtRes == null ? System.DateTime.Now : Convert.ToDateTime(dtRes));
                tblAPICallStatusMessagesval.Status = "Success";


                int result = Online.Registration.DAL.Admin.OnlineRegAdmin.APISaveCallStatus(tblAPICallStatusMessagesval);

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here

                XDocument doc = XDocument.Parse(postResult);

                //Bellow code is for Parseing the xml and storing in to respetive form
                foreach (var r in doc.Descendants("retrieveSubscriberDetlsResponse"))
                {
                    customer.MsgCode = r.Element("msgCode").Value.ToString();
                    if (customer.MsgCode != "0")//Invalid Subscriber
                    {
                        customer = null;
                        break;
                    }
                    customer.Moc = r.Element("moc") == null ? "" : r.Element("moc").Value.ToString();
                    customer.MsgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
                    customer.NewIC = r.Element("new_ic") == null ? "" : r.Element("new_ic").Value.ToString();
                    customer.OldIC = r.Element("old_ic") == null ? "" : r.Element("old_ic").Value.ToString();
                    customer.OtherIC = r.Element("other_ic") == null ? "" : r.Element("other_ic").Value.ToString();
                    customer.PassportNo = r.Element("passport_no") == null ? "" : r.Element("passport_no").Value.ToString();
                    customer.PostCode = r.Element("post_cd") == null ? "" : r.Element("post_cd").Value.ToString();
                    customer.RiskCategory = r.Element("risk_category") == null ? "" : r.Element("risk_category").Value.ToString();
                    customer.State = r.Element("state") == null ? "" : r.Element("state").Value.ToString();
                    customer.Title = r.Element("title") == null ? "" : r.Element("title").Value.ToString();
                    customer.Cbr = r.Element("cbr") == null ? "" : r.Element("cbr").Value.ToString();
                    customer.City = r.Element("city") == null ? "" : r.Element("city").Value.ToString();
                    customer.Address1 = r.Element("addr1") == null ? "" : r.Element("addr1").Value.ToString();
                    customer.Address2 = r.Element("addr2") == null ? "" : r.Element("addr2").Value.ToString();
                    customer.Address3 = r.Element("addr3") == null ? "" : r.Element("addr3").Value.ToString();
                    customer.Brn = r.Element("brn") == null ? "" : r.Element("brn").Value.ToString();
                    customer.Dob = r.Element("dob") == null ? "" : r.Element("dob").Value.ToString();
                    customer.CustomerName = r.Element("cust_name") == null ? "" : r.Element("cust_name").Value.ToString();
                    customer.MsgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    customer.AcctActiveDt = r.Element("acctActiveDt") == null ? "" : r.Element("acctActiveDt").Value.ToString();
                    customer.TotalWriteOff = r.Element("totalWriteOff") == null ? "" : r.Element("totalWriteOff").Value.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                customer = null;
            }

            return customer;
        }
		
		/// <summary>
        /// Retrieves the subscriber detls. Posts to retrieveSubscriberDetls webmethod belonging to retrieveSubscriberDetls.wsdl
        /// as proxy class cannot be generated.
        /// </summary>
        /// <param name="Msisdn">The msisdn.</param>
        /// <param name="Msisdn">Msisdn</param>        
        /// <returns>Typeof(CustomizedCustomer)</returns>
        public CustomizedCustomer retrieveSubscriberDetls(string Msisdn, string loggedUserName, bool isSupAdmin)
        {
            string userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            string password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;
            string filterPrepaid = Properties.Settings.Default.filterPrepaid;
            string needMask = Properties.Settings.Default.needMask;
            if (isSupAdmin)
            {
                needMask = "N";
            }
            else
            {
                needMask = "Y";
            }

            string postData =
                string.Format(@"<soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:max=""http://www.webmethods.com/maxis.eai.process.common.ws"">
<soapenv:Header/>
<soapenv:Body>
<max:retrieveSubscriberDetls soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
<tranxType xsi:type=""ret:__tranxType"" xs:type=""type:__tranxType"" xmlns:ret=""http://localhost/maxis/eai/process/common/ws/retrieveSubscriberDetls"" xmlns:xs=""http://www.w3.org/2000/XMLSchema-instance"">MSISDN</tranxType>
<tranxVal xsi:type=""xsd:string"" xs:type=""type:string"" xmlns:xs=""http://www.w3.org/2000/XMLSchema-instance"">{0}</tranxVal>
<needMask xsi:type=""xsd:string"">{1}</needMask>
</max:retrieveSubscriberDetls>
</soapenv:Body>
</soapenv:Envelope>", Msisdn, needMask);

            DateTime dtReq = DateTime.Now;
            string dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
                        <SOAP-ENV:Envelope
                        xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/""
                        xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/""
                        xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
                        xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                        SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                        <SOAP-ENV:Body>
                        <ser-root:retrieveSubscriberDetlsResponse xmlns:ser-root=""http://www.webmethods.com/maxis.eai.process.common.ws"" SOAP-ENC:root='1'>
                        <msgCode xsi:type=""xsd:string"">0</msgCode>
                        <msgDesc xsi:type=""xsd:string"">Valid Subscriber</msgDesc>
                        <title xsi:type=""xsd:string"" xsi:nil=""true""></title>
                        <cust_name xsi:type=""xsd:string"">VALUELABS SDN BHD</cust_name>
                        <dob xsi:type=""xsd:string"" xsi:nil=""true""></dob>
                        <old_ic xsi:type=""xsd:string"" xsi:nil=""true""></old_ic>
                        <new_ic xsi:type=""xsd:string"" xsi:nil=""true""></new_ic>
                        <brn xsi:type=""xsd:string"">669664X</brn>
                        <passport_no xsi:type=""xsd:string"" xsi:nil=""true""></passport_no>
                        <other_ic xsi:type=""xsd:string"" xsi:nil=""true""></other_ic>
                        <addr1 xsi:type=""xsd:string"">UNIT 2.3 ENTERPRISE 3A</addr1>
                        <addr2 xsi:type=""xsd:string"">TECHNOLOGY PARK MALAYSIA</addr2>
                        <addr3 xsi:type=""xsd:string"">LEBUHRAYA PUCHONG-SUNGAI BESI BKT JALIL</addr3>
                        <post_cd xsi:type=""xsd:string"">57000</post_cd>
                        <city xsi:type=""xsd:string"">Kuala Lumpur</city>
                        <state xsi:type=""xsd:string"">Wilayah Persekutuan</state>
                        <cbr xsi:type=""xsd:string"">60389966128</cbr>
                        <moc xsi:type=""xsd:string"" xsi:nil=""true""></moc>
                        <risk_category xsi:type=""xsd:string"" xsi:nil=""true""></risk_category>
                        <acctActiveDt xsi:type=""xsd:string""></acctActiveDt>
                        <totalWriteOff xsi:type=""xsd:string""></totalWriteOff>
                        </ser-root:retrieveSubscriberDetlsResponse>
                        </SOAP-ENV:Body>
                        </SOAP-ENV:Envelope>";


            WebClient retrieveAcctListWebClient = new WebClient();
            Uri postUri = new Uri(Properties.Settings.Default.RetrieveSubscriberDetlsPostUrl);
            string postResult = string.Empty;

            retrieveAcctListWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");

            CustomizedCustomer customer = new CustomizedCustomer();

            try
            {

                //Depending upon settings if true then use dummy data
                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    postResult = dummyReplyData;
                }
                else
                {
                    postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                }

                postResult = Util.RemoveAllNamespaces(postResult);
                DateTime dtRes = DateTime.Now;
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", postResult));

                DAL.Models.tblAPICallStatus tblAPICallStatusMessagesval = new DAL.Models.tblAPICallStatus();

                tblAPICallStatusMessagesval.APIName = "retrieveServiceInfoProxy";
                tblAPICallStatusMessagesval.MethodName = "APISaveCallStatus";
                tblAPICallStatusMessagesval.RequestTime = (dtReq == null ? System.DateTime.Now : Convert.ToDateTime(dtReq));
                tblAPICallStatusMessagesval.UserName = loggedUserName;
                tblAPICallStatusMessagesval.BreXmlReq = System.Web.HttpUtility.HtmlDecode(postData);
                tblAPICallStatusMessagesval.BreXmlRes = System.Web.HttpUtility.HtmlDecode(postResult);
                tblAPICallStatusMessagesval.IdCardNo = Msisdn;
                tblAPICallStatusMessagesval.ResponseTime = (dtRes == null ? System.DateTime.Now : Convert.ToDateTime(dtRes));
                tblAPICallStatusMessagesval.Status = "Success";


                int result = Online.Registration.DAL.Admin.OnlineRegAdmin.APISaveCallStatus(tblAPICallStatusMessagesval);

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here

                XDocument doc = XDocument.Parse(postResult);

                //Bellow code is for Parseing the xml and storing in to respetive form
                foreach (var r in doc.Descendants("retrieveSubscriberDetlsResponse"))
                {
                    customer.MsgCode = r.Element("msgCode").Value.ToString();
                    if (customer.MsgCode != "0")//Invalid Subscriber
                    {
                        customer = null;
                        break;
                    }
                    customer.Moc = r.Element("moc") == null ? "" : r.Element("moc").Value.ToString();
                    customer.MsgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
                    customer.NewIC = r.Element("new_ic") == null ? "" : r.Element("new_ic").Value.ToString();
                    customer.OldIC = r.Element("old_ic") == null ? "" : r.Element("old_ic").Value.ToString();
                    customer.OtherIC = r.Element("other_ic") == null ? "" : r.Element("other_ic").Value.ToString();
                    customer.PassportNo = r.Element("passport_no") == null ? "" : r.Element("passport_no").Value.ToString();
                    customer.PostCode = r.Element("post_cd") == null ? "" : r.Element("post_cd").Value.ToString();
                    customer.RiskCategory = r.Element("risk_category") == null ? "" : r.Element("risk_category").Value.ToString();
                    customer.State = r.Element("state") == null ? "" : r.Element("state").Value.ToString();
                    customer.Title = r.Element("title") == null ? "" : r.Element("title").Value.ToString();
                    customer.Cbr = r.Element("cbr") == null ? "" : r.Element("cbr").Value.ToString();
                    customer.City = r.Element("city") == null ? "" : r.Element("city").Value.ToString();
                    customer.Address1 = r.Element("addr1") == null ? "" : r.Element("addr1").Value.ToString();
                    customer.Address2 = r.Element("addr2") == null ? "" : r.Element("addr2").Value.ToString();
                    customer.Address3 = r.Element("addr3") == null ? "" : r.Element("addr3").Value.ToString();
                    customer.Brn = r.Element("brn") == null ? "" : r.Element("brn").Value.ToString();
                    customer.Dob = r.Element("dob") == null ? "" : r.Element("dob").Value.ToString();
                    customer.CustomerName = r.Element("cust_name") == null ? "" : r.Element("cust_name").Value.ToString();
                    customer.MsgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    customer.AcctActiveDt = r.Element("acctActiveDt") == null ? "" : r.Element("acctActiveDt").Value.ToString();
                    customer.TotalWriteOff = r.Element("totalWriteOff") == null ? "" : r.Element("totalWriteOff").Value.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                customer = null;
            }

            return customer;
        }
		**/
        #endregion

        private CustomizedCustomer retrieveSubscriberDetls(string tranxType, string tranxVal, string loggedUserName, bool isSupAdmin)
        {
            string needMask = Properties.Settings.Default.needMask;
            needMask = isSupAdmin ? "N" : "Y";

            CustomizedCustomer customer = new CustomizedCustomer();

            try
            {
                using (var proxy = new RetrieveSubscriberDetlsWS.retrieveSubscriberDetlsService_PortTypeClient())
                {
                    var header = new RetrieveSubscriberDetlsWS.eaiHeader();
                    header.from = Properties.Resources.orderProcessReq_from;
                    header.to = Properties.Resources.orderProcessReq_to;
                    header.appId = Properties.Resources.orderProcessReq_appID;
                    header.msgType = Properties.Resources.orderProcessReq_msgType;
                    header.msgId = "RSD" + tranxVal;
                    header.timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

                    var request = new RetrieveSubscriberDetlsWS.typeRetrieveSubscriberDetlsRequest();
                    var response = new RetrieveSubscriberDetlsWS.typeRetrieveSubscriberDetlsResponse();

                    request.tranxType = tranxType;
                    request.tranxVal = tranxVal;
                    request.toMask = needMask;

                    //Depending upon settings if true then use dummy data
                    //if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking) response = dummyReplyData;

                    var credentialBehaviour = proxy.Endpoint.Behaviors.Find<System.ServiceModel.Description.ClientCredentials>();
                    credentialBehaviour.UserName.UserName = Properties.Settings.Default.KenanProxyUsername;
                    credentialBehaviour.UserName.Password = Properties.Settings.Default.KenanProxyPassword;
                    response = proxy.retrieveSubscriberDetlsV2(ref header, request);

                    DateTime dtRes = DateTime.Now;
                    // Log the request and Response -- Added by Patanjali on 12-3-2013
                    Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", request));
                    Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", response));
                    DAL.Models.tblAPICallStatus tblAPICallStatusMessagesval = new DAL.Models.tblAPICallStatus();

                    tblAPICallStatusMessagesval.APIName = "retrieveSubscriberDetls";
                    tblAPICallStatusMessagesval.MethodName = "APISaveCallStatus";
                    tblAPICallStatusMessagesval.RequestTime = (DateTime.Now == null ? System.DateTime.Now : Convert.ToDateTime(DateTime.Now));
                    tblAPICallStatusMessagesval.UserName = loggedUserName;
                    tblAPICallStatusMessagesval.BreXmlReq = System.Web.HttpUtility.HtmlDecode(request.ToString2());
                    tblAPICallStatusMessagesval.BreXmlRes = System.Web.HttpUtility.HtmlDecode(response.ToString2());
                    tblAPICallStatusMessagesval.IdCardNo = tranxVal;
                    tblAPICallStatusMessagesval.ResponseTime = (dtRes == null ? System.DateTime.Now : Convert.ToDateTime(dtRes));
                    tblAPICallStatusMessagesval.Status = "Success";

                    int result = Online.Registration.DAL.Admin.OnlineRegAdmin.APISaveCallStatus(tblAPICallStatusMessagesval);
                    // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here

                    //if (response.msgCode != Properties.Settings.Default.KenanSuccessCode)
                    if (response.msgCode == "1")//Invalid Subscriber
                    {
                        return null;
                    }
                    if (response.msgCode != Properties.Settings.Default.KenanSuccessCode && response.msgCode != "1")
                    {

                        //return null;
                        customer.MsgCode = response.msgCode.ToString2();
                        customer.MsgDesc = response.msgDesc.ToString2();
                        return customer;
                    }
                    customer.MsgCode = response.msgCode.ToString2();
                    customer.MsgDesc = response.msgDesc.ToString2();
                    customer.Title = response.title.ToString2();
                    customer.CustomerName = response.cust_name.ToString2();
                    customer.Dob = response.dob.ToString2();
                    customer.OldIC = response.old_ic.ToString2();
                    customer.NewIC = response.new_ic.ToString2();
                    customer.Brn = response.brn.ToString2();
                    customer.PassportNo = response.passport_no.ToString2();
                    customer.OtherIC = response.other_ic.ToString2();
                    customer.Address1 = response.addr1.ToString2();
                    customer.Address2 = response.addr2.ToString2();
                    customer.Address3 = response.addr3.ToString2();
                    customer.PostCode = response.post_cd.ToString2();
                    customer.City = response.city.ToString2();
                    customer.State = response.state.ToString2();
                    customer.Cbr = response.cbr.ToString2();
                    customer.Moc = response.moc.ToString2();
                    customer.RiskCategory = response.risk_category.ToString2();
                    customer.MarketCode = response.mktCode.ToString2();
                    customer.AccountCategory = response.acctCategory.ToString2();
                    customer.EmailAddrs = response.custEmail.ToString2();
                    customer.Races = response.race.ToString2();
                    customer.Genders = response.gender.ToString2();
                    customer.Nation = response.nationality.ToString2();
                    //customer.AcctActiveDt = response.acctActiveDt.ToString2();
                    //customer.TotalWriteOff = response.totalWriteOff.ToString2();

                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                customer.MsgCode = "-1";
                customer.MsgDesc = Util.LogException(ex);
                return customer;
            }

            return customer;
        }

        public CustomizedCustomer retrieveSubscriberDetls(string msisdn, string loggedUserName, bool isSupAdmin)
        {
            return retrieveSubscriberDetls("MSISDN", msisdn, loggedUserName, isSupAdmin);
        }

        public CustomizedCustomer retrieveSubscriberDetlsByAcctNo(string acctNo, string loggedUserName, bool isSupAdmin)
        {
            return retrieveSubscriberDetls("ACCT", acctNo, loggedUserName, isSupAdmin);
        }

        public CustomizedCustomer retrieveSubscriberDetlsbyKenan(string transType, string transValue, string loggedUserName, bool isSupAdmin)
        {
            return retrieveSubscriberDetls(transType, transValue, loggedUserName, isSupAdmin);
        }


        #region Added by Sindhu on 25 Jul 2013 fro handling Search based on Kenan A/c Number
        // comment out 
        /**
        /// <summary>
        /// Retrieves the subscriber detls based on kenan Account number. Posts to retrieveSubscriberDetls webmethod belonging to retrieveSubscriberDetls.wsdl
        /// as proxy class cannot be generated.
        /// </summary>
        /// <remarks>
        /// This method is duplication of retrieveSubscriberDetls with different parameter passing(included TransType param for passing the value "ACCT" when search is done by Kenan A/c number).
        /// This repetition of method is done inorder not to disturb the exisiting retrieveSubscriberDetls method as it is used/invoked in different files.
        /// </remarks>
        /// <param name="transType">ID Type</param>
        /// <param name="transValue">ID Value</param>
        /// <returns>Typeof(CustomizedCustomer)</returns>
        public CustomizedCustomer retrieveSubscriberDetlsbyKenan(string transType, string transValue, string loggedUserName, bool isSupAdmin)
        {
            string userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            string password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;
            string filterPrepaid = Properties.Settings.Default.filterPrepaid;
            string needMask = Properties.Settings.Default.needMask;
            if (isSupAdmin)
            {
                needMask = "N";
            }
            else
            {
                needMask = "Y";
            }

            string postData =
                string.Format(@"<soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:max=""http://www.webmethods.com/maxis.eai.process.common.ws"">
                <soapenv:Header/>
                <soapenv:Body>
                <max:retrieveSubscriberDetls soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                <tranxType xsi:type=""ret:__tranxType"" xs:type=""type:__tranxType"" xmlns:ret=""http://localhost/maxis/eai/process/common/ws/retrieveSubscriberDetls"" xmlns:xs=""http://www.w3.org/2000/XMLSchema-instance"">{0}</tranxType>
                <tranxVal xsi:type=""xsd:string"" xs:type=""type:string"" xmlns:xs=""http://www.w3.org/2000/XMLSchema-instance"">{1}</tranxVal>
                <needMask xsi:type=""xsd:string"">{2}</needMask>
                </max:retrieveSubscriberDetls>
                </soapenv:Body>
                </soapenv:Envelope>", transType, transValue, needMask);

            DateTime dtReq = DateTime.Now;
            string dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
                        <SOAP-ENV:Envelope
                        xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/""
                        xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/""
                        xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
                        xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                        SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                        <SOAP-ENV:Body>
                        <ser-root:retrieveSubscriberDetlsResponse xmlns:ser-root=""http://www.webmethods.com/maxis.eai.process.common.ws"" SOAP-ENC:root='1'>
                        <msgCode xsi:type=""xsd:string"">0</msgCode>
                        <msgDesc xsi:type=""xsd:string"">Valid Subscriber</msgDesc>
                        <title xsi:type=""xsd:string"" xsi:nil=""true""></title>
                        <cust_name xsi:type=""xsd:string"">VALUELABS SDN BHD</cust_name>
                        <dob xsi:type=""xsd:string"" xsi:nil=""true""></dob>
                        <old_ic xsi:type=""xsd:string"" xsi:nil=""true""></old_ic>
                        <new_ic xsi:type=""xsd:string"" xsi:nil=""true""></new_ic>
                        <brn xsi:type=""xsd:string"">669664X</brn>
                        <passport_no xsi:type=""xsd:string"" xsi:nil=""true""></passport_no>
                        <other_ic xsi:type=""xsd:string"" xsi:nil=""true""></other_ic>
                        <addr1 xsi:type=""xsd:string"">UNIT 2.3 ENTERPRISE 3A</addr1>
                        <addr2 xsi:type=""xsd:string"">TECHNOLOGY PARK MALAYSIA</addr2>
                        <addr3 xsi:type=""xsd:string"">LEBUHRAYA PUCHONG-SUNGAI BESI BKT JALIL</addr3>
                        <post_cd xsi:type=""xsd:string"">57000</post_cd>
                        <city xsi:type=""xsd:string"">Kuala Lumpur</city>
                        <state xsi:type=""xsd:string"">Wilayah Persekutuan</state>
                        <cbr xsi:type=""xsd:string"">60389966128</cbr>
                        <moc xsi:type=""xsd:string"" xsi:nil=""true""></moc>
                        <risk_category xsi:type=""xsd:string"" xsi:nil=""true""></risk_category>
                        </ser-root:retrieveSubscriberDetlsResponse>
                        </SOAP-ENV:Body>
                        </SOAP-ENV:Envelope>";


            WebClient retrieveAcctListWebClient = new WebClient();
            Uri postUri = new Uri(Properties.Settings.Default.RetrieveSubscriberDetlsPostUrl);
            string postResult = string.Empty;

            retrieveAcctListWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");

            CustomizedCustomer customer = new CustomizedCustomer();

            try
            {

                //Depending upon settings if true then use dummy data
                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    postResult = dummyReplyData;
                }
                else
                {
                    postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                }

                postResult = Util.RemoveAllNamespaces(postResult);
                DateTime dtRes = DateTime.Now;
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", postResult));

                DAL.Models.tblAPICallStatus tblAPICallStatusMessagesval = new DAL.Models.tblAPICallStatus();

                tblAPICallStatusMessagesval.APIName = "retrieveServiceInfoProxy";
                tblAPICallStatusMessagesval.MethodName = "APISaveCallStatus";
                tblAPICallStatusMessagesval.RequestTime = (dtReq == null ? System.DateTime.Now : Convert.ToDateTime(dtReq));
                tblAPICallStatusMessagesval.UserName = loggedUserName;
                tblAPICallStatusMessagesval.BreXmlReq = System.Web.HttpUtility.HtmlDecode(postData);
                tblAPICallStatusMessagesval.BreXmlRes = System.Web.HttpUtility.HtmlDecode(postResult);
                tblAPICallStatusMessagesval.IdCardNo = transValue;
                tblAPICallStatusMessagesval.ResponseTime = (dtRes == null ? System.DateTime.Now : Convert.ToDateTime(dtRes));
                tblAPICallStatusMessagesval.Status = "Success";


                int result = Online.Registration.DAL.Admin.OnlineRegAdmin.APISaveCallStatus(tblAPICallStatusMessagesval);

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here

                XDocument doc = XDocument.Parse(postResult);

                //Bellow code is for Parseing the xml and storing in to respetive form
                foreach (var r in doc.Descendants("retrieveSubscriberDetlsResponse"))
                {
                    customer.MsgCode = r.Element("msgCode").Value.ToString();
                    if (customer.MsgCode != "0")//Invalid Subscriber
                    {
                        customer = null;
                        break;
                    }
                    customer.Moc = r.Element("moc") == null ? "" : r.Element("moc").Value.ToString();
                    customer.MsgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
                    customer.NewIC = r.Element("new_ic") == null ? "" : r.Element("new_ic").Value.ToString();
                    customer.OldIC = r.Element("old_ic") == null ? "" : r.Element("old_ic").Value.ToString();
                    customer.OtherIC = r.Element("other_ic") == null ? "" : r.Element("other_ic").Value.ToString();
                    customer.PassportNo = r.Element("passport_no") == null ? "" : r.Element("passport_no").Value.ToString();
                    customer.PostCode = r.Element("post_cd") == null ? "" : r.Element("post_cd").Value.ToString();
                    customer.RiskCategory = r.Element("risk_category") == null ? "" : r.Element("risk_category").Value.ToString();
                    customer.State = r.Element("state") == null ? "" : r.Element("state").Value.ToString();
                    customer.Title = r.Element("title") == null ? "" : r.Element("title").Value.ToString();
                    customer.Cbr = r.Element("cbr") == null ? "" : r.Element("cbr").Value.ToString();
                    customer.City = r.Element("city") == null ? "" : r.Element("city").Value.ToString();
                    customer.Address1 = r.Element("addr1") == null ? "" : r.Element("addr1").Value.ToString();
                    customer.Address2 = r.Element("addr2") == null ? "" : r.Element("addr2").Value.ToString();
                    customer.Address3 = r.Element("addr3") == null ? "" : r.Element("addr3").Value.ToString();
                    customer.Brn = r.Element("brn") == null ? "" : r.Element("brn").Value.ToString();
                    customer.Dob = r.Element("dob") == null ? "" : r.Element("dob").Value.ToString();
                    customer.CustomerName = r.Element("cust_name") == null ? "" : r.Element("cust_name").Value.ToString();
                    customer.MsgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                customer = null;
            }

            return customer;
        }
        **/
        #endregion

        /// <summary>
        /// Retrieves the service info.
        /// </summary>
        /// <param name="request">The request of type SubscriberRetrieveServiceInfoRequest.</param>
        /// <returns>TypeOf(SubscriberRetrieveServiceInfoResponse)</returns>
        public SubscriberRetrieveServiceInfoResponse retrieveServiceInfo(SubscriberRetrieveServiceInfoRequest request)
        {
            //retrieveServiceInfoResponse response = new retrieveServiceInfoResponse();
            SubscriberRetrieveServiceInfoResponse response = new SubscriberRetrieveServiceInfoResponse();
            string userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            string password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;
            string msgdesc = string.Empty;
            string lob = string.Empty;
            string packageName = string.Empty;
            string prinSuppInd = string.Empty;
            string serviceStatus = string.Empty;
            string pukCode = string.Empty;
            string postPreInd = string.Empty;
            string mnpInd = string.Empty;

            try
            {
                string postData = string.Format(@" <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:max=""http://www.webmethods.com/maxis.eai.process.common.ws"">
<soapenv:Header/>
<soapenv:Body>
<max:retrieveServiceInfo soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
<externalId xsi:type=""xsd:string"">{0}</externalId>
<subscrNo xsi:type=""xsd:string"">{1}</subscrNo>
<subscrNoResets xsi:type=""xsd:string"">{2}</subscrNoResets>
</max:retrieveServiceInfo>
</soapenv:Body>
</soapenv:Envelope>", request.externalId, request.subscrNo, request.subscrNoResets);
                WebClient retrieveserciceinfoListWebClient = new WebClient();
                Uri postUri = new Uri(Properties.Settings.Default.RetrieveSubscriberInfoPostUrl);
                string postResult = string.Empty;
                retrieveserciceinfoListWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                retrieveserciceinfoListWebClient.Headers.Add("content-type", "text/xml");
                //Depending upon settings if true then use dummy data
                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    response = retrieveDevelopmentServiceInfo(request);
                    return response;
                }
                else
                {
                    Logger.Info(String.Format(" retrieveServiceInfo Request : {0} ", postData));
                    postResult = retrieveserciceinfoListWebClient.UploadString(postUri, postData);
                }
                postData = Util.RemoveAllNamespaces(postData);
                Logger.Info(String.Format(" retrieveServiceInfo1 Request : {0} ", postData));
                postResult = Util.RemoveAllNamespaces(postResult);
                Logger.Info(String.Format(" retrieveServiceInfo Response: {0} ", postResult));
                XDocument doc = XDocument.Parse(postResult);
                //Bellow code is for Parseing the xml and storing in to respetive form
                foreach (var r in doc.Descendants("retrieveServiceInfoResponse"))
                {
                    response.msgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    response.msgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
                    if (response.msgCode == "0")
                    {
                        response.lob = r.Element("lob") == null ? "" : r.Element("lob").Value.ToString();
                        response.mnpInd = r.Element("mnpInd") == null ? "" : r.Element("mnpInd").Value.ToString();
                        response.packageId = r.Element("packageId") == null ? "" : r.Element("packageId").Value.ToString();
                        response.packageName = r.Element("packageName") == null ? "" : r.Element("packageName").Value.ToString();
                        response.postPreInd = r.Element("postPreInd") == null ? "" : r.Element("postPreInd").Value.ToString();
                        response.prinSuppInd = r.Element("prinSuppInd") == null ? "" : r.Element("prinSuppInd").Value.ToString();
                        response.pukCode = r.Element("pukCode") == null ? "" : r.Element("pukCode").Value.ToString();
                        response.serviceStatus = r.Element("serviceStatus") == null ? "" : r.Element("serviceStatus").Value.ToString();
                        response.emf_config_id = r.Element("emfConfigId") == null ? "" : r.Element("emfConfigId").Value.ToString();

                        //Added to get service Activated date
                        if (r.Element("serviceActiveDt") != null && !string.IsNullOrEmpty(r.Element("serviceActiveDt").Value.ToString()))
                            response.serviceActiveDt = DateTime.ParseExact(r.Element("serviceActiveDt").Value.ToString().Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                    }
                    //if (response.msgCode == "1")//Invalid Subscriber
                    else
                    {
                        response = null;
                        break;
                    }

                }
                //if (response!=null && (response.msgCode != "0") && (response.msgCode != "") && (response.msgCode != null))
                //{
                //    /// “0” – success
                //    /// “1” - fail
                //    /// “2” –system error
                //    response = null;
                //}
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                response = null;
            }
            return response;
        }

        /// <summary>
        /// Gets the prin supplimentarylines.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>TypeOf(retrievegetPrinSuppResponse)</returns>
        public retrievegetPrinSuppResponse getPrinSupplimentarylines(SubscribergetPrinSuppRequest request, bool prinFlag = false)
        {
            retrievegetPrinSuppResponse prinsupp = new retrievegetPrinSuppResponse();
            string postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:v11=""http://services.maxis.com.my/EAI/COMMON01/enquiry/v1/"" xmlns:v12=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
                                               <soapenv:Header>
                                                  <v1:eaiHeader>
                                                     <v1:from>ISELL</v1:from>
                                                     <v1:to>ESB</v1:to>
                                                     <v1:appId>OPF</v1:appId>
                                                     <v1:msgType>Request</v1:msgType>
                                                     <v1:msgId>123456</v1:msgId>
                                                     <v1:correlationId></v1:correlationId>
                                                     <v1:timestamp></v1:timestamp>
                                                  </v1:eaiHeader>
                                               </soapenv:Header>
                                               <soapenv:Body>
                                                  <v11:getPrinSuppReqDoc>
                                                     <v12:msisdn>{0}</v12:msisdn>
                                                  </v11:getPrinSuppReqDoc>
                                               </soapenv:Body>
                                            </soapenv:Envelope>", request.msisdn);
            WebClient getPrinSuppRespDoc = new WebClient();
            Uri postUri = new Uri(Properties.Settings.Default.PrinSupplimentarylinesPostUrl);
            string postResult = string.Empty;
            getPrinSuppRespDoc.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            getPrinSuppRespDoc.Headers.Add("content-type", "text/xml");
            try
            {
                //Depending upon settings if true then use dummy data
                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    #region Sutan added code 12Th March 2013
                    return GetDevelopmentSuppLineList();
                    #endregion Sutan added code 12Th March 2013
                }
                else
                {
                    postResult = getPrinSuppRespDoc.UploadString(postUri, postData);
                }
                postResult = Util.RemoveAllNamespaces(postResult);
                XDocument doc = XDocument.Parse(postResult);
                //Bellow code is for Parseing the xml and storing in to respetive form
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" getPrinSupp Request : {0} ", postData));
                Logger.Info(String.Format(" getPrinSupp Response: {0} ", postResult));
                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here
                foreach (var r in doc.Descendants("getPrinSuppRespDoc"))
                {
                    prinsupp.MsgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    prinsupp.MsgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
                    if (prinsupp.MsgCode == "0")
                    {
                        foreach (var itemsdata in r.Descendants("lineList"))
                        {
                            if (itemsdata.Element("prinsupp_ind") != null)
                            {
                                // 20151026 - BUG 1199 [w.loon] - start
                                if (itemsdata.Element("prinsupp_ind").Value.ToString().ToUpper() == "P")
                                {
                                    prinsupp.ParentMsisdn = itemsdata.Element("msisdn") == null ? "" : itemsdata.Element("msisdn").Value.ToString();
                                }
                                // 20151026 - BUG 1199 [w.loon] - end
                                if (prinFlag)
                                {
                                    if (itemsdata.Element("prinsupp_ind").Value.ToString().ToUpper() == "P" || itemsdata.Element("prinsupp_ind").Value.ToString().ToUpper() == "S")
                                    {
                                        prinsupp.itemList.Add(new lineList
                                        {
                                            acct_noField = itemsdata.Element("acct_no") == null ? "" : itemsdata.Element("acct_no").Value.ToString(),
                                            cust_nmField = itemsdata.Element("cust_nm") == null ? "" : itemsdata.Element("cust_nm").Value.ToString(),
                                            msisdnField = itemsdata.Element("msisdn") == null ? "" : itemsdata.Element("msisdn").Value.ToString(),
                                            subscr_no_resetsField = itemsdata.Element("subscr_no_resets") == null ? "" : itemsdata.Element("subscr_no_resets").Value.ToString(),
                                            subscr_noField = itemsdata.Element("subscr_no") == null ? "" : itemsdata.Element("subscr_no").Value.ToString(),
                                            subscr_statusField = itemsdata.Element("subscr_status") == null ? "" : itemsdata.Element("subscr_status").Value.ToString(),
                                            prinsupp_indField = itemsdata.Element("prinsupp_ind") == null ? "" : itemsdata.Element("prinsupp_ind").Value.ToString(),
                                            IsMISM = false
                                        });
                                    }
                                }
                                else
                                {
                                    if (itemsdata.Element("prinsupp_ind").Value.ToString().ToUpper() == "S")
                                    {
                                        prinsupp.itemList.Add(new lineList
                                        {
                                            acct_noField = itemsdata.Element("acct_no") == null ? "" : itemsdata.Element("acct_no").Value.ToString(),
                                            cust_nmField = itemsdata.Element("cust_nm") == null ? "" : itemsdata.Element("cust_nm").Value.ToString(),
                                            msisdnField = itemsdata.Element("msisdn") == null ? "" : itemsdata.Element("msisdn").Value.ToString(),
                                            subscr_no_resetsField = itemsdata.Element("subscr_no_resets") == null ? "" : itemsdata.Element("subscr_no_resets").Value.ToString(),
                                            subscr_noField = itemsdata.Element("subscr_no") == null ? "" : itemsdata.Element("subscr_no").Value.ToString(),
                                            subscr_statusField = itemsdata.Element("subscr_status") == null ? "" : itemsdata.Element("subscr_status").Value.ToString(),
                                            prinsupp_indField = itemsdata.Element("prinsupp_ind") == null ? "" : itemsdata.Element("prinsupp_ind").Value.ToString(),
                                            IsMISM = false
                                        });
                                    }
                                }
                            }

                        }
                    }
                    else
                    {
                        prinsupp = null;
                        break;
                    }
                }
                //comented for null exception in production
                //if (prinsupp!=null && (prinsupp.MsgCode != "0") && (prinsupp.MsgCode != null) && (prinsupp.MsgCode != ""))
                //{
                //    /// “0” – success
                //    /// “1” - fail
                //    /// “2” –system error
                //    prinsupp = null;
                //}
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                prinsupp = null;
            }

            return prinsupp;
        }

        #region "Log PPID Req and Res"
        /// <summary>
        /// APIs the save call status.
        /// </summary>
        /// <param name="oReq">The o req.</param>
        /// <returns></returns>
        public SaveCallStatusResp APISaveCallStatus(SaveCallStatusReq oReq)
        {
            //Logger.InfoFormat("Entering {0}.APISaveCallStatus({1})", "SubscriberICService", oReq.tblAPICallStatusMessagesval.APIName);

            SaveCallStatusResp resp = new SaveCallStatusResp();

            try
            {
                resp.APIID = Online.Registration.DAL.Admin.OnlineRegAdmin.APISaveCallStatus(oReq.tblAPICallStatusMessagesval);
            }
            catch (Exception ex)
            {
                resp.APIID = null;
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                //Logger.InfoFormat("Exiting {0}.APISaveCallStatus({1}): {2}|{3}", "SubscriberICService", oReq.tblAPICallStatusMessagesval.APIName, (resp.APIID != null) ? Convert.ToString(resp.APIID) : null, resp.Code);
            }

            return resp;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Gets the development supp line list.
        /// </summary>
        /// <returns>TypeOf(retrievegetPrinSuppResponse)</returns>
        private retrievegetPrinSuppResponse GetDevelopmentSuppLineList()
        {
            counter = 0;
            retrievegetPrinSuppResponse response = null;
            response = new retrievegetPrinSuppResponse();

            if (counter <= 1)
            {
                if (counter == 0)
                {
                    response.itemList.Add(new lineList
                    {
                        acct_noField = "83214641",
                        cust_nmField = "Sutan Dan",
                        msisdnField = "60124105678",
                        prinsupp_indField = "P",
                        subscr_noField = "93504781",
                        subscr_no_resetsField = "2",
                        subscr_statusField = "Transferred In",
                        IsMISM = true
                    });
                    response.MsgCode = "0";
                    response.MsgDesc = "Success";
                }

                if (counter == 1)
                {
                    response.itemList.Add(new lineList
                    {
                        acct_noField = "83214671",
                        cust_nmField = "Capital electronics",
                        msisdnField = "60124105679",
                        prinsupp_indField = "P",
                        subscr_noField = "93504781",
                        subscr_no_resetsField = "2",
                        subscr_statusField = "Transferred In",
                        IsMISM = true
                    });
                    response.MsgCode = "0";
                    response.MsgDesc = "Success";
                }
            }
            counter++;
            return response;

        }
        /// <summary>
        /// Retrieves the development service info.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>TypeOf(SubscriberRetrieveServiceInfoResponse)</returns>
        public SubscriberRetrieveServiceInfoResponse retrieveDevelopmentServiceInfo(SubscriberRetrieveServiceInfoRequest request)
        {
            //counter = 0;
            counter = 1;
            SubscriberRetrieveServiceInfoResponse response = null;

            if (counter <= 1)
            {
                if (counter == 0)
                    ///Non GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "HSDPA",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "EXPIRED HSDPA",
                        postPreInd = "POST",
                        pukCode = "21565214",
                        serviceStatus = "A",
                        emf_config_id = "4003"
                    };
                if (counter == 1)
                    ///GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A",
                        emf_config_id = "4003"
                    };
                if (counter == 2)
                    ///GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A"
                    };
                if (counter == 3)
                    ///GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A"
                    };
                if (counter == 4)
                    ///Non GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A"
                    };
            }
            counter++;
            return response;
        }
        #endregion Private methods

        #region RAJ ADDED FOR NEW CR 10TH JUN13

        public typeChkFxVasDependencyResponse ChkFxVasDependency(typeChkFxVasDependencyRequest req)
        {
            List<componentList> _componentList = new List<componentList>();

            componentList cl;
            foreach (componentList item in req.componentList)
            {
                cl = new componentList();
                cl.componentId = item.componentId;
                cl.packageId = item.packageId;
                _componentList.Add(cl);
            }
            var reqest = new typeChkFxVasDependencyRequest()
            {
                emfConfigId = req.emfConfigId,
                mandatoryPackageId = req.mandatoryPackageId,
                componentList = _componentList.ToArray(),

            };
            MaxisChkFxVasDependencyService.typeChkFxVasDependencyResponse Serviceresponce = new MaxisChkFxVasDependencyService.typeChkFxVasDependencyResponse();
            typeChkFxVasDependencyResponse responce = new typeChkFxVasDependencyResponse();
            using (var proxy = new MaxisChkFxVasDependencyService.chkFxVasDependencyService())
            {
                proxy.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                proxy.PreAuthenticate = true;
                proxy.eaiHeaderValue = new Online.Registration.IntegrationService.MaxisChkFxVasDependencyService.eaiHeader()
                {
                    from = Properties.Resources.orderProcessReq_from,
                    to = Properties.Resources.orderProcessReq_to,
                    appId = Properties.Resources.orderProcessReq_appID,
                    msgType = Properties.Resources.orderProcessReq_msgType,
                    msgId = string.Format("FO{0}", "12345S"),
                    timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),

                };
                string KenanRequest = XMLHelper.ConvertObjectToXml(reqest, typeof(typeChkFxVasDependencyRequest));
                Logger.Info("typeChkFxVasDependencyRequest" + KenanRequest);
                Serviceresponce = proxy.chkFxVasDependency(reqest);
                string KenanResponse = XMLHelper.ConvertObjectToXml(Serviceresponce, typeof(typeChkFxVasDependencyResponse));
                Logger.Info("typeChkFxVasDependencyResponse" + KenanResponse);
            }
            if (Serviceresponce != null)
            {
                responce.msgCode = Serviceresponce.msgCode;
                responce.msgDesc = Serviceresponce.msgDesc;
                if (Serviceresponce.mxsRuleMessages != null && Serviceresponce.mxsRuleMessages.Length > 0)
                    responce.mxsRuleMessages = Serviceresponce.mxsRuleMessages.ToArray();
            }

            return responce;
        }

        public ReassignVasResponce ReassignVas(ReassignVasRequest req)
        {
            List<packagePairList> _packagePairList = new List<packagePairList>();
            packagePairList p1;
            foreach (packagePairs item in req.packagePairsList)
            {
                p1 = new packagePairList();
                p1.newPackageId = item.newPackageId;
                p1.oldPackageId = item.oldPackageId;
                _packagePairList.Add(p1);
            }
            var reqest = new typeReassignVasRequest()
            {
                fxAcctNo = req.FxAccNo,
                fxSubscrNo = req.FxSubscrNo,
                fxSubscrNoResets = req.FxSubscrNoResets,
                packagePairList = _packagePairList.ToArray()
            };
            ////TestData:
            //_packagePairList = new List<packagePairList>();
            //p1 = new packagePairList();
            //p1.newPackageId = "40694";
            //p1.oldPackageId = "40659";
            //_packagePairList.Add(p1);

            //reqest = new typeReassignVasRequest()
            //{
            //    fxAcctNo = "100559448",//req.FxAccNo,
            //    fxSubscrNo = "114389633", //req.FxSubscrNo,
            //    fxSubscrNoResets = "0",//req.FxSubscrNoResets,
            //    packagePairList = _packagePairList.ToArray()
            //};
            typeReassignVasResponse Serviceresponce = new typeReassignVasResponse();
            ReassignVasResponce responce = new ReassignVasResponce();

            using (var proxy = new ReassignVasService.maxiseaiprocesscommonwsreassignVasService())
            {
                proxy.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                proxy.PreAuthenticate = true;
                proxy.eaiHeaderValue = new Online.Registration.IntegrationService.ReassignVasService.eaiHeader()
                {
                    from = Properties.Resources.orderProcessReq_from,
                    to = Properties.Resources.orderProcessReq_to,
                    appId = Properties.Resources.orderProcessReq_appID,
                    msgType = Properties.Resources.orderProcessReq_msgType,
                    msgId = string.Format("FO{0}", "12345S"),
                    timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),

                };
                string subreq = XMLHelper.ConvertObjectToXml(reqest, typeof(typeReassignVasRequest));
                Logger.Info("typeReassignVasRequest" + subreq);
                Serviceresponce = proxy.reassignVas(reqest);
                string subres = XMLHelper.ConvertObjectToXml(Serviceresponce, typeof(typeReassignVasResponse));
                Logger.Info("typeReassignVasResponse" + subres);
            }
            if (Serviceresponce != null)
            {
                responce.msgCode = Serviceresponce.msgCode;
                responce.msgDesc = Serviceresponce.msgDesc;
                List<reassingVases> _reassingVasesList = new List<reassingVases>();
                reassingVases objreassingVases;
                if (Serviceresponce.reassignVasList != null)
                {
                    foreach (var item in Serviceresponce.reassignVasList)
                    {
                        objreassingVases = new reassingVases();
                        objreassingVases.NewComponentId = item.newComponentId;
                        objreassingVases.NewPackageId = item.newPackageId;
                        objreassingVases.OldComponentId = item.oldComponentId;
                        objreassingVases.OldPackageId = item.oldPackageId;
                        _reassingVasesList.Add(objreassingVases);
                    }
                }
                responce.reassingVasesList = _reassingVasesList;
            }

            return responce;
        }

        public AvalablePackages RetrieveEligiblePackages(int currentPackageId, int mktCode, int acctCategoryId, int rateClass)
        {
            /*
                    string URI = "http://10.200.51.122:7030/ws/maxis.eai.process.common.ws.retrieveEligiblePackageService/maxis_eai_process_common_ws_retrieveEligiblePackageService_Port";
            */
            string URI = Properties.Settings.Default.Online_Registration_IntegrationService_RetrieveEligiblePackageService_maxis_eai_process_common_ws_retrieveEligiblePackageService.ToString();

            string postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:v11=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"" xmlns:v12=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/""> <soapenv:Header> <v1:eaiHeader><v1:from>?</v1:from><v1:to>?</v1:to><v1:appId>?</v1:appId><v1:msgType>?</v1:msgType><v1:msgId>?</v1:msgId><!--Optional:--><v1:correlationId>?</v1:correlationId><v1:timestamp>?</v1:timestamp></v1:eaiHeader></soapenv:Header><soapenv:Body><v11:retrieveEligiblePackageRequest><v12:retrieveEligiblePackageReqDetls><v12:currentPackageId>{0}</v12:currentPackageId><v12:mktCode>{1}</v12:mktCode><v12:acctCategoryId>{2}</v12:acctCategoryId><v12:rateClass>{3}</v12:rateClass></v12:retrieveEligiblePackageReqDetls></v11:retrieveEligiblePackageRequest></soapenv:Body></soapenv:Envelope>", currentPackageId, mktCode, acctCategoryId, rateClass);
            WebClient retrieveEligiblePackageWebClient = new WebClient();
            Uri postUri = new Uri(URI);
            string postResult = string.Empty;
            AvalablePackages responce = new AvalablePackages();
            retrieveEligiblePackageWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveEligiblePackageWebClient.Headers.Add("content-type", "text/xml");

            try
            {

                postResult = retrieveEligiblePackageWebClient.UploadString(URI, postData);
                postResult = RemoveAllNamespaces(postResult);

                Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", postResult));

                XDocument doc = XDocument.Parse(postResult);
                foreach (var r in doc.Descendants("retrieveEligiblePackageRespDetls"))
                {
                    string abc = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();

                    responce.MsgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    responce.MsgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();

                    if (responce.MsgCode == "1")//Invalid Subscriber
                    {
                        responce = null;
                        break;
                    }

                    List<EligiblePackageList> onjlist = new List<EligiblePackageList>();

                    foreach (var itemsdata in r.Descendants("eligiblePackageList"))
                    {
                        var i = new EligiblePackageList
                        {
                            DowngradeId = itemsdata.Element("downgradeInd") == null ? "" : itemsdata.Element("downgradeInd").Value.ToString(),
                            PackageId = itemsdata.Element("packageId") == null ? "" : itemsdata.Element("packageId").Value.ToString()
                        };
                        onjlist.Add(i);
                    }
                    responce.EligiblePackagesList = onjlist;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            return responce;
        }


        public static string RemoveAllNamespaces(string xmlDocument)
        {
            XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));
            return xmlDocumentWithoutNs.ToString();
        }
        /// <summary>
        /// Removing all Namespaces
        /// </summary>
        /// <param name="xmlDocument"></param>
        /// <returns></returns>
        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;
                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }
        #endregion

        //public Dictionary<string, string> GetPrepaidSubscriberDetails(List<string> MSISDNList)
        //{
        //    GetPreSubDetails.maxis_eai_process_in_wsService client = new GetPreSubDetails.maxis_eai_process_in_wsService();
        //    Dictionary<string, string> lstPrepaidStatus = new Dictionary<string, string>();
        //    foreach (var MSISDN in MSISDNList)
        //    {
        //        string msgDesc = string.Empty;
        //        string mainAcctBalance = string.Empty;
        //        string mainAcctExpiryDt = string.Empty;
        //        string mainAcctGraceEndDt = string.Empty;
        //        string acctStatus = string.Empty;
        //        string pid = string.Empty;
        //        string firstCallDate = string.Empty;
        //        client.getPreSubDetls(MSISDN, out msgDesc, out mainAcctBalance, out mainAcctExpiryDt, out mainAcctGraceEndDt, out acctStatus, out pid, out firstCallDate);
        //        if (string.IsNullOrEmpty(msgDesc))
        //        {
        //            lstPrepaidStatus.Add(MSISDN, acctStatus);
        //        }
        //    }

        //    return lstPrepaidStatus;
        //}


        public string GetPrepaidSubscriberDetails(string MSISDN)
        {
            string lstPrepaidStatus = "";
            //foreach (var MSISDN in MSISDNList)
            //{
            string msgDesc = string.Empty;
            string mainAcctBalance = string.Empty;
            string mainAcctExpiryDt = string.Empty;
            string mainAcctGraceEndDt = string.Empty;
            string acctStatus = string.Empty;
            string pid = string.Empty;
            string firstCallDate = string.Empty;
            Logger.Info(String.Format(" GetPrepaidSubscriberDetails12 Request: {0} ", MSISDN));
            ////client.ClientCredentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);

            //client.ClientCredentials.UserName.UserName = Properties.Settings.Default.KenanProxyUsername;
            //client.ClientCredentials.UserName.Password = Properties.Settings.Default.KenanProxyPassword;
            ////client.getPreSubDetls(MSISDN, out msgDesc, out mainAcctBalance, out mainAcctExpiryDt, out mainAcctGraceEndDt, out acctStatus, out pid, out firstCallDate);
            //if (!string.IsNullOrEmpty(msgDesc))
            //{
            //    lstPrepaidStatus = acctStatus;
            //}
            //}




            string postData = string.Format(@" <soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:max=""http://www.webmethods.com/maxis.eai.process.in.ws"">
   <soapenv:Header/>     
   <soapenv:Body>
<max:getPreSubDetls soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
         <msisdn xsi:type=""xsd:string"">{0}</msisdn>
      </max:getPreSubDetls>
   </soapenv:Body>
</soapenv:Envelope>", MSISDN);
            Logger.Info(String.Format(" GetPrepaidSubscriberDetails1 Request: {0} ", postData));
            WebClient getPreSubDetlsDoc = new WebClient();
            Uri postUri = new Uri(Properties.Settings.Default.Online_Registration_IntegrationService_GetPreSubDetls_maxis_eai_process_in_wsService);
            string postResult = string.Empty;

            getPreSubDetlsDoc.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            getPreSubDetlsDoc.Headers.Add("content-type", "text/xml");
            postResult = getPreSubDetlsDoc.UploadString(postUri, postData);

            postData = Util.RemoveAllNamespaces(postData);
            postResult = Util.RemoveAllNamespaces(postResult);
            Logger.Info(String.Format(" GetPrepaidSubscriberDetails1 Request: {0} ", postData));
            Logger.Info(String.Format(" GetPrepaidSubscriberDetails1 Response: {0} ", postResult));
            XDocument doc = XDocument.Parse(postResult);
            foreach (var itemsdata in doc.Descendants("getPreSubDetlsResponse"))
            {
                lstPrepaidStatus = itemsdata.Element("acctStatus") == null ? "" : itemsdata.Element("acctStatus").Value.ToString();
                break;
            }

            Logger.Info(String.Format(" GetPrepaidSubscriberDetails13 Response: {0} ", lstPrepaidStatus));
            return lstPrepaidStatus;
        }

        public retrieveAcctListByICResponse retrieveAllDetails(string serviceException, string loggedUserName, string icType, string icValue, string kenancode, string userName = "", string password = "", string postUrl = "", bool isSupAdmin = false)
        {
            //var retrieveAllTimer = Stopwatch.StartNew();
            //Logger.Info("START retrieveAllTimer");
            retrieveAcctListByICResponse ObjAccList = new retrieveAcctListByICResponse();
            int i = 0;
            try
            {
                ObjAccList = retrieveAcctListByIC(icType, icValue, loggedUserName, ref serviceException);

                if (ObjAccList.itemList != null)
                {
                    SubscriberRetrieveServiceInfoResponse response = new SubscriberRetrieveServiceInfoResponse();
                    SubscriberRetrieveServiceInfoRequest request = new SubscriberRetrieveServiceInfoRequest();

                    for (i = 0; i < ObjAccList.itemList.Count; i++)
                    {
                        request.externalId = ObjAccList.itemList[i].ExternalId;
                        request.subscrNo = ObjAccList.itemList[i].SusbcrNo;
                        request.subscrNoResets = ObjAccList.itemList[i].SusbcrNoResets;
                        ObjAccList.itemList[i].ServiceInfoResponse = retrieveServiceInfo(request);
                    }

                    bool prinFlag = false;
                    SubscribergetPrinSuppRequest prinSuppRequest = new SubscribergetPrinSuppRequest();
                    retrievegetPrinSuppResponse prinsuppResponse = new retrievegetPrinSuppResponse();
                    PrinSupEaiHeader header = null;

                    //for (i = 0; i < ObjAccList.itemList.Count; i++)
                    //{
                    //    if (ObjAccList.itemList[i].ServiceInfoResponse != null && ObjAccList.itemList[i].ServiceInfoResponse.prinSuppInd != null && ObjAccList.itemList[i].ServiceInfoResponse.prinSuppInd.ToUpper() == "P")
                    //    {
                    //        prinSuppRequest.eaiHeader = header;
                    //        prinSuppRequest.msisdn = ObjAccList.itemList[i].ExternalId;

                    //        try
                    //        {
                    //            Logger.Info(String.Format(" retrieveAllDetails Request lob : {0} ", ObjAccList.itemList[i].ServiceInfoResponse.lob));
                    //            Logger.Info(String.Format(" retrieveAllDetails Request postPreInd : {0} ", ObjAccList.itemList[i].ServiceInfoResponse.postPreInd));
                    //            if (ObjAccList.itemList[i].ServiceInfoResponse.lob.ToUpper() == "PREGSM" && ObjAccList.itemList[i].ServiceInfoResponse.postPreInd == "PRE")
                    //            {
                    //                Logger.Info(String.Format("retrieveAllDetails GetPrepaidSubscriberDetails Request: {0} ", ObjAccList.itemList[i].ExternalId));
                    //                string prepaidAccountStatus = GetPrepaidSubscriberDetails(ObjAccList.itemList[i].ExternalId);
                    //                Logger.Info(String.Format("retrieveAllDetails GetPrepaidSubscriberDetails Response: {0} ", prepaidAccountStatus));
                    //                ObjAccList.itemList[i].ServiceInfoResponse.serviceStatus = prepaidAccountStatus;
                    //            }
                    //        }
                    //        catch (Exception ex1)
                    //        {
                    //            Logger.Info(String.Format(" GetPrepaidSubscriberDetails Response: {0} ", ex1.Message));
                    //        }
                    //        ObjAccList.itemList[i].PrinSupplimentaryResponse = getPrinSupplimentarylines(prinSuppRequest, prinFlag);
                    //    }
                    //}

                    // 20151026 - BUG 1199 [w.loon] - start
                    //var getPrinSuppTimer = Stopwatch.StartNew();
                    //Logger.Info("START getPrinSuppTimer");
                    for (i = 0; i < ObjAccList.itemList.Count; i++)
                    {
                        if (ObjAccList.itemList[i].ServiceInfoResponse != null && ObjAccList.itemList[i].ServiceInfoResponse.prinSuppInd != null && ObjAccList.itemList[i].ServiceInfoResponse.prinSuppInd.ToUpper() == "P")
                        {
                            prinSuppRequest.eaiHeader = header;
                            prinSuppRequest.msisdn = ObjAccList.itemList[i].ExternalId;
                            try
                            {
                                Logger.Info(String.Format(" retrieveAllDetails Request lob : {0} ", ObjAccList.itemList[i].ServiceInfoResponse.lob));
                                Logger.Info(String.Format(" retrieveAllDetails Request postPreInd : {0} ", ObjAccList.itemList[i].ServiceInfoResponse.postPreInd));
                                if (ObjAccList.itemList[i].ServiceInfoResponse.lob.ToUpper() == "PREGSM" && ObjAccList.itemList[i].ServiceInfoResponse.postPreInd == "PRE")
                                {
                                    Logger.Info(String.Format("retrieveAllDetails GetPrepaidSubscriberDetails Request: {0} ", ObjAccList.itemList[i].ExternalId));
                                    string prepaidAccountStatus = GetPrepaidSubscriberDetails(ObjAccList.itemList[i].ExternalId);
                                    Logger.Info(String.Format("retrieveAllDetails GetPrepaidSubscriberDetails Response: {0} ", prepaidAccountStatus));
                                    ObjAccList.itemList[i].ServiceInfoResponse.serviceStatus = prepaidAccountStatus;
                                }
                            }
                            catch (Exception ex1)
                            {
                                Logger.Info(String.Format(" GetPrepaidSubscriberDetails Response: {0} ", ex1.Message));
                            }
                        }
                        if (ObjAccList.itemList[i].ServiceInfoResponse != null && ObjAccList.itemList[i].ServiceInfoResponse.prinSuppInd != null && ObjAccList.itemList[i].ServiceInfoResponse.prinSuppInd.ToUpper() == "S")
                        {
                            bool retrieveSupp = true;
                            int j = 0;
                            // eliminating repeated call(s)
                            for (j = 0; j < ObjAccList.itemList.Count; j++)
                            {
                                if (ObjAccList.itemList[j].PrinSupplimentaryResponse != null && ObjAccList.itemList[j].PrinSupplimentaryResponse.itemList != null)
                                {
                                    int aj = 0;
                                    for (aj = 0; aj < ObjAccList.itemList[j].PrinSupplimentaryResponse.itemList.Count; aj++)
                                    {
                                        if (ObjAccList.itemList[j].PrinSupplimentaryResponse.itemList[aj].msisdnField == ObjAccList.itemList[i].ExternalId)
                                        {
                                            retrieveSupp = false;
                                        }
                                    }
                                }
                            }
                            if (retrieveSupp)
                            {
                                prinSuppRequest.eaiHeader = header;
                                prinSuppRequest.msisdn = ObjAccList.itemList[i].ExternalId;
                                retrievegetPrinSuppResponse SuppData = new retrievegetPrinSuppResponse();
                                SuppData = getPrinSupplimentarylines(prinSuppRequest, prinFlag);
                                // searching for the corresponding Principal Line and mapping the Supplimentary Lines
                                int k = 0;
                                for (k = 0; k < ObjAccList.itemList.Count; k++)
                                {
                                    if (ObjAccList.itemList[k].ServiceInfoResponse != null && ObjAccList.itemList[k].ExternalId == SuppData.ParentMsisdn)
                                    {
                                        ObjAccList.itemList[k].PrinSupplimentaryResponse = SuppData;
                                    }
                                }
                            }
                        }
                    }
                    //getPrinSuppTimer.Stop();
                    //var getPrinSuppElapsedTime = getPrinSuppTimer.ElapsedMilliseconds;
                    //Logger.Fatal("ELAPSED TIME getPrinSupp = " + getPrinSuppElapsedTime);
                    // 20151026 - BUG 1199 [w.loon] - end

                    if (ObjAccList.itemList != null)
                    {
                        //02072015 - Anthony - Enhance Customer Search: reduce call for RetrieveAccountDetails and retrieveSubscriberDetlsByAcctNo if there are multiple services in one account number - Start
                        var accNumberList = ObjAccList.itemList.Select(x => x.AcctExtId).Distinct().ToList();
                        var accountDetailsList = new Dictionary<string, AccountDetails>();
                        var customerList = new Dictionary<string, CustomizedCustomer>();

                        foreach (var accNumber in accNumberList)
                        {
                            var accountDetails = RetrieveAccountDetails(new MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsRequest { fxAcctExtId = accNumber });
                            var customer = retrieveSubscriberDetlsByAcctNo(accNumber, loggedUserName, isSupAdmin);

                            if (!accountDetailsList.ContainsKey(accNumber))
                                accountDetailsList.Add(accNumber, accountDetails);
                            if (!customerList.ContainsKey(accNumber))
                                customerList.Add(accNumber, customer);
                        }
                        //02072015 - Anthony - Enhance Customer Search: reduce call for RetrieveAccountDetails and retrieveSubscriberDetlsByAcctNo if there are multiple services in one account number - End

                        for (i = 0; i < ObjAccList.itemList.Count; i++)
                        {
                            //02072015 - Anthony - Enhance Customer Search: reduce call for RetrieveAccountDetails and retrieveSubscriberDetlsByAcctNo if there are multiple services in one account number - Start
                            //ObjAccList.itemList[i].AccountDetails = RetrieveAccountDetails(new MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsRequest { fxAcctExtId = ObjAccList.itemList[i].AcctExtId });
                            //ObjAccList.itemList[i].Customer = retrieveSubscriberDetlsByAcctNo(ObjAccList.itemList[i].AcctExtId, loggedUserName, isSupAdmin);
                            ObjAccList.itemList[i].AccountDetails = accountDetailsList[ObjAccList.itemList[i].AcctExtId];
                            ObjAccList.itemList[i].Customer = customerList[ObjAccList.itemList[i].AcctExtId];
                            //02072015 - Anthony - Enhance Customer Search: reduce call for RetrieveAccountDetails and retrieveSubscriberDetlsByAcctNo if there are multiple services in one account number - End
                            if (ObjAccList.itemList[i].AccountDetails != null && ObjAccList.itemList[i].Customer != null && ObjAccList.itemList[i].AccountDetails.AcctCategory == "1")
                                if (!string.IsNullOrEmpty(ObjAccList.itemList[i].Customer.CustomerName) && !string.IsNullOrEmpty(ObjAccList.itemList[i].Customer.Title) && !string.IsNullOrEmpty(ObjAccList.itemList[i].Customer.Title.Trim()))
                                    ObjAccList.itemList[i].Customer.CustomerName = ObjAccList.itemList[i].Customer.CustomerName.Replace(ObjAccList.itemList[i].Customer.Title, "");
                            ObjAccList.itemList[i].Packages = retrievePackageDetls(ObjAccList.itemList[i].ExternalId, kenancode, userName = "", password = "", postUrl = "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                ObjAccList = null;
            }
            //retrieveAllTimer.Stop();
            //var retrieveAllElapsedTime = retrieveAllTimer.ElapsedMilliseconds;
            //Logger.Fatal("ELAPSED TIME retrieveAllDetails = " + retrieveAllElapsedTime);
            return ObjAccList;
        }

        /// <summary>
        /// To Retrieve the LOD and Summon Indicator for Write-Off Account
        /// </summary>
        /// <param name="IDType"></param>
        /// <param name="IDNumber"></param>
        /// <param name="AcctListByICResponse"></param>
        /// <returns></returns>
        public retrieveAcctListByICResponse RetrieveUCCSInfo(string IDType, string IDNumber, retrieveAcctListByICResponse AcctListByICResponse)
        {
            var accountOverduesRequest = new UCCSAccountOverduesWS.getAccountOverduesRequest();
            var accountOverduesResponse = new UCCSAccountOverduesWS.getAccountOverduesResponse();

            try
            {
                using (var accountOverduesClient = new UCCSAccountOverduesWS.AccountOverduesClient())
                {
                    accountOverduesRequest.CustomerID = IDType;
                    accountOverduesRequest.CustomerIDType = IDNumber;
                    accountOverduesResponse = accountOverduesClient.getAccountOverdues(accountOverduesRequest);
                    //staging endpoint : http://10.200.52.232:9081/collections/services/AccountOverdues
                    //production endpoint : http://172.16.138.45:9080/collections/services/AccountOverdues
                }

                Logger.Info("Retrieve UCCS Info Request : " + Util.RemoveAllNamespaces(XMLHelper.ConvertObjectToXml(accountOverduesRequest)));
                Logger.Info("Retrieve UCCS Info Response : " + Util.RemoveAllNamespaces(XMLHelper.ConvertObjectToXml(accountOverduesResponse)));

                if (!string.IsNullOrEmpty(accountOverduesResponse.ResponseDesc) && accountOverduesResponse.ResponseDesc.ToLower() == "account exists" 
                    && accountOverduesResponse.Account != null && accountOverduesResponse.Account.ToList().Count > 0)
                {
                    foreach (var accountInfo in accountOverduesResponse.Account.ToList())
                    {
                        if (AcctListByICResponse.itemList.Where(x => x.AcctExtId == accountInfo.AccountNumber).Any())
                        {
                            AcctListByICResponse.itemList.Where(x => x.AcctExtId == accountInfo.AccountNumber).ToList()
                                .ForEach(y =>
                                {
                                    y.AccountDetails.TotalWriteOffUCCS = accountInfo.WriteOffAmount;
                                    y.AccountDetails.LODIndicator = accountInfo.LODIndicator;
                                    y.AccountDetails.SummonIndicator = accountInfo.SMMNIndicator;
                                });
                        }
                    }

                    /*foreach (var itemList in AcctListByICResponse.itemList)
                    {
                        var accNumber = itemList.AcctExtId;

                        if (accountOverduesResponse.Account.ToList().Where(x => x.AccountNumber == accNumber).Any())
                        {
                            var accountInfo = accountOverduesResponse.Account.ToList().Where(x => x.AccountNumber == accNumber).FirstOrDefault();

                            itemList.AccountDetails.TotalWriteOffUCCS = accountInfo.WriteOffAmount;
                            itemList.AccountDetails.LODIndicator = accountInfo.LODIndicator;
                            itemList.AccountDetails.SummonIndicator = accountInfo.SMMNIndicator;
                        }
                    }*/
                }
                else
                {
                    Logger.Error("Retrieve UCCS Info Request : " + Util.RemoveAllNamespaces(XMLHelper.ConvertObjectToXml(accountOverduesRequest)));
                    Logger.Error("Retrieve UCCS Info Response Error : " + accountOverduesResponse.ResponseDesc.ToString2());
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Retrieve UCCS Info Error: " + Util.LogException(ex));
            }

            return AcctListByICResponse;
        }

        /// <summary>
        /// Method used to Retrieve Master account Information
        /// </summary>
        /// <param name="TranxType">Transaction Type</param>
        /// <param name="TranxValue">Transaction Value</param>
        /// <returns>MaxisRetrieveMasterAccountWs.typeRetrieveMasterAcctListResponse Object</returns>
        public MaxisRetrieveMasterAccountWs.typeRetrieveMasterAcctListResponse RetrieveMasterAccountList(string TranxType, string TranxValue)
        {
            MaxisRetrieveMasterAccountWs.typeRetrieveMasterAcctListResponse objMasterAccountListResp = new MaxisRetrieveMasterAccountWs.typeRetrieveMasterAcctListResponse();
            try
            {
                MaxisRetrieveMasterAccountWs.typeRetrieveMasterAcctListRequest objMasterAccountListRequest = new MaxisRetrieveMasterAccountWs.typeRetrieveMasterAcctListRequest();
                objMasterAccountListRequest.querySize = ConfigurationManager.AppSettings["BRNQuerySize"].ToInt().ToString2();
                objMasterAccountListRequest.startCount = "1";
                objMasterAccountListRequest.tranxType = TranxType;
                objMasterAccountListRequest.tranxValue = TranxValue.ToUpper();
                using (var objMasterAccountDetails = new MaxisRetrieveMasterAccountWs.maxiseaiprocesscommonwsretrieveMasterAcctListService())
                {
                    objMasterAccountDetails.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                    objMasterAccountDetails.PreAuthenticate = true;
                    objMasterAccountDetails.eaiHeaderValue = new MaxisRetrieveMasterAccountWs.eaiHeader()
                    {
                        from = Properties.Resources.orderProcessReq_from,
                        to = Properties.Resources.orderProcessReq_to,
                        appId = Properties.Resources.orderProcessReq_appID,
                        msgType = Properties.Resources.orderProcessReq_msgType,
                        msgId = string.Format("FO{0}", TranxValue),
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),

                    };
                    string reqxml = XMLHelper.ConvertObjectToXml(objMasterAccountListRequest);
                    Logger.Info("RetrieveMasterAccountList " + reqxml);
                    objMasterAccountDetails.Timeout = 600000;
                    objMasterAccountListResp = objMasterAccountDetails.retrieveMasterAcctList(objMasterAccountListRequest);
                    string respxml = XMLHelper.ConvertObjectToXml(objMasterAccountListResp);
                    Logger.Info("RetrieveMasterAccountList " + respxml);

                }

            }
            catch (Exception ex)
            {
                objMasterAccountListResp.msgCode = "1";
                objMasterAccountListResp.msgDesc = ex.Message;
                Logger.Error("RetrieveMasterAccountList Error: " + Util.LogException(ex));
            }
            return objMasterAccountListResp;
        }



        /// <summary>
        /// Method used to Retrieve Parent account Information
        /// </summary>
        /// <param name="MasterAccountNumber">Master Account Number</param>
        /// <returns>MaxisRetrieveParentAccountWs.typeRetrieveParentAcctListResponse Object</returns>
        public MaxisRetrieveParentAccountWs.typeRetrieveParentAcctListResponse RetrieveParentAccountList(string MasterAccountNumber)
        {
            MaxisRetrieveParentAccountWs.typeRetrieveParentAcctListResponse objParentAccountListResp = new MaxisRetrieveParentAccountWs.typeRetrieveParentAcctListResponse();
            try
            {
                MaxisRetrieveParentAccountWs.typeRetrieveParentAcctListRequest objParentAccountListRequest = new MaxisRetrieveParentAccountWs.typeRetrieveParentAcctListRequest();
                objParentAccountListRequest.querySize = ConfigurationManager.AppSettings["BRNQuerySize"].ToInt().ToString2();
                objParentAccountListRequest.startCount = "1";
                objParentAccountListRequest.masterAcctNo = MasterAccountNumber;
                using (var objParentAccountDetails = new MaxisRetrieveParentAccountWs.maxiseaiprocesscommonwsretrieveParentAcctListService())
                {
                    objParentAccountDetails.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                    objParentAccountDetails.PreAuthenticate = true;
                    objParentAccountDetails.eaiHeaderValue = new MaxisRetrieveParentAccountWs.eaiHeader()
                    {
                        from = Properties.Resources.orderProcessReq_from,
                        to = Properties.Resources.orderProcessReq_to,
                        appId = Properties.Resources.orderProcessReq_appID,
                        msgType = Properties.Resources.orderProcessReq_msgType,
                        msgId = string.Format("FO{0}", MasterAccountNumber),
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),

                    };
                    string reqxml = XMLHelper.ConvertObjectToXml(objParentAccountListRequest);
                    Logger.Info("RetrieveParentAccountList " + reqxml);
                    objParentAccountListResp = objParentAccountDetails.retrieveParentAcctList(objParentAccountListRequest);
                    string respxml = XMLHelper.ConvertObjectToXml(objParentAccountListResp);
                    Logger.Info("RetrieveParentAccountList " + respxml);

                }

            }
            catch (Exception ex)
            {
                objParentAccountListResp.msgCode = "1";
                objParentAccountListResp.msgDesc = ex.Message;
                Logger.Error("RetrieveParent Error: " + Util.LogException(ex));
            }
            return objParentAccountListResp;
        }


        #region SmartRelatedMethods

        public IList<PackageModel> retrievePackageDetls(string MSISDN, string kenancode, string userName = "", string password = "")
        {
            //string posturl = "http://10.200.51.126:7030/ws/maxis.eai.process.common.ws:retrievePkgCompInfoService/maxis_eai_process_common_ws_retrievePkgCompInfoService_Port";
            string postUrl = System.Configuration.ConfigurationManager.AppSettings["retrievePackageDetls"].ToString();
            Uri postUri = new Uri(postUrl);

            string postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:v11=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"" xmlns:v12=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
            <soapenv:Header>
      <v1:eaiHeader>
         <v1:from>ISELL</v1:from>
         <v1:to>ESB</v1:to>
         <v1:appId>OPF</v1:appId>
         <v1:msgType>Request</v1:msgType>
         <v1:msgId>ISELL12345</v1:msgId>
         <!--Optional:-->
         <v1:correlationId>?</v1:correlationId>
         <v1:timestamp>20130316</v1:timestamp>
      </v1:eaiHeader>
   </soapenv:Header>
   <soapenv:Body>
      <v11:retrievePkgCompInfoRequest>
         <v12:retrievePkgCompInfoRequestDetails>
            <v12:externalId>{0}</v12:externalId>
            <v12:extenalIdType>{1}</v12:extenalIdType>
         </v12:retrievePkgCompInfoRequestDetails>
      </v11:retrievePkgCompInfoRequest>
   </soapenv:Body>
</soapenv:Envelope>", MSISDN, kenancode);
            WebClient retrieveAcctListWebClient = new WebClient();
            string postResult = string.Empty;
            IList<PackageModel> packs = new List<PackageModel>();

            retrieveAcctListWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");

            try
            {
                //Depending upon settings if true then use dummy data

                postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                postResult = RemoveAllNamespaces(postResult);
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", postResult));

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here

                XDocument doc = XDocument.Parse(postResult);
                foreach (var p in doc.Descendants("packages"))
                {
                    PackageModel Comp = new PackageModel();
                    IList<Components> listOfThings = new List<Components>();
                    Comp.PackageID = p.Element("packageId") == null ? "" : p.Element("packageId").Value.ToString();
                    Comp.PackageDesc = p.Element("packageDesc") == null ? "" : p.Element("packageDesc").Value.ToString();
                    Comp.packageInstId = p.Element("packageInstId") == null ? "" : p.Element("packageInstId").Value.ToString();
                    Comp.packageInstIdServ = p.Element("packageInstIdServ") == null ? "" : p.Element("packageInstIdServ").Value.ToString();
                    Comp.packageActiveDt = p.Element("packageActiveDt") == null ? "" : p.Element("packageInactiveDt").Value.ToString();
                    foreach (var r in p.Descendants("components"))
                    {

                        Components Components = new Components();
                        Components.componentId = r.Element("componentId") == null ? "" : r.Element("componentId").Value.ToString();
                        Components.componentInstId = r.Element("componentInstId") == null ? "" : r.Element("componentInstId").Value.ToString();
                        Components.componentInstIdServ = r.Element("componentInstIdServ") == null ? "" : r.Element("componentInstIdServ").Value.ToString();
                        Components.componentShortDisplay = r.Element("componentShortDisplay") == null ? "" : r.Element("componentShortDisplay").Value.ToString();
                        Components.componentActiveDt = r.Element("componentActiveDt") == null ? "" : r.Element("componentActiveDt").Value.ToString();
                        Components.componentDesc = r.Element("componentDesc") == null ? "" : r.Element("componentDesc").Value.ToString();
                        Components.componentInactiveDt = r.Element("componentInactiveDt") == null ? "" : r.Element("componentInactiveDt").Value.ToString();
                        listOfThings.Add(Components);
                    }

                    Comp.compList = (List<Components>)listOfThings;
                    packs.Add(Comp);
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            return packs;
        }

        #endregion

        #region smart related methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="MSISDN"></param>
        /// <param name="ExternalID"></param>
        /// <param name="SubscriberNo"></param>
        /// <param name="SubscriberNoResets"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="postUrl"></param>
        /// <returns></returns>        
        public List<retrievePenalty> retrievePenalty(string MSISDN, string ExternalID, string SubscriberNo, string SubscriberNoResets, string userName = "", string password = "")
        {
            //string postUrl = "http://10.200.51.126:7030/ws/maxis.eai.process.common.ws.retrieveContractInfoService/maxis_eai_process_common_ws_retrieveContractInfoService_Port";
            string postUrl = System.Configuration.ConfigurationManager.AppSettings["RetrieveContractInfoService"].ToString();
            List<retrievePenalty> penaltybyMSISDN = new List<Models.retrievePenalty>();
            string postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:max=""http://maxis71/maxis.eai.process.common.ws:retrieveContractInfoService"" xmlns:v11=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
   <soapenv:Header>
      <v1:eaiHeader>
         <v1:from>ISELL</v1:from>
         <v1:to>ESB</v1:to>
         <v1:appId>OPF</v1:appId>
         <v1:msgType>Request</v1:msgType>
         <v1:msgId>12345</v1:msgId>
         <!--Optional:-->
         <v1:correlationId>?</v1:correlationId>
         <v1:timestamp>20130316</v1:timestamp>
      </v1:eaiHeader>
   </soapenv:Header>
   <soapenv:Body>
      <max:retrieveContractInfoRequest>
         <v11:retrieveContractInfoRequestDetails>
            <v11:externalId>{0}</v11:externalId>
            <v11:extenalIdType>{1}</v11:extenalIdType>
            <!--Optional:-->
            <v11:subscrNo>{2}</v11:subscrNo>
            <!--Optional:-->
            <v11:subscrNoResets>{3}</v11:subscrNoResets>
         </v11:retrieveContractInfoRequestDetails>
      </max:retrieveContractInfoRequest>
   </soapenv:Body>
</soapenv:Envelope>", MSISDN, ExternalID, SubscriberNo, SubscriberNoResets);
            WebClient retrieveAcctListWebClient = new WebClient();
            Uri postUri = new Uri(postUrl);
            string postResult = string.Empty;

            //retrieveAcctListWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveAcctListWebClient.Credentials = new NetworkCredential(userName, password);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");

            try
            {
                //Depending upon settings if true then use dummy data

                postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                postResult = RemoveAllNamespaces(postResult);
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", postResult));

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here
                XDocument doc = XDocument.Parse(postResult);
                string penalty = string.Empty;
                string rrp = string.Empty;
                string phoneModel = string.Empty;
                string adminFee = string.Empty;
                string UpfrontPayment = string.Empty;
                decimal penaltyAmt = 0;
                string msgCode = string.Empty;
                string msgDesc = string.Empty;
                string deviceModel = string.Empty;
                string deviceIMEI = string.Empty;
                string upgradeFee = string.Empty;

                foreach (var p in doc.Descendants("retrieveContractInfoResponseDetails"))
                {

                    msgCode = p.Element("msgCode") == null ? "" : p.Element("msgCode").Value.ToString();
                    msgDesc = p.Element("msgDesc") == null ? "" : p.Element("msgDesc").Value.ToString();

                    foreach (var r in doc.Descendants("contractInfoList"))
                    {
                        List<ParamList> paramLists = new List<ParamList>();
                        retrievePenalty objretrievePenalty = new Models.retrievePenalty();
                        objretrievePenalty.startDate = r.Element("startDate") == null ? "" : r.Element("startDate").Value.ToString();
                        objretrievePenalty.endDate = r.Element("endDate") == null ? "" : r.Element("endDate").Value.ToString();
                        objretrievePenalty.contractType = r.Element("contractType") == null ? "" : r.Element("contractType").Value.ToString();
                        objretrievePenalty.penalty = r.Element("penalty") == null ? "" : r.Element("penalty").Value.ToString();
                        objretrievePenalty.contactId = r.Element("contractId") == null ? "" : r.Element("contractId").Value.ToString();
                        objretrievePenalty.ContractName = r.Element("contractName") == null ? "" : r.Element("contractName").Value.ToString();
                        objretrievePenalty.penaltyType = r.Element("penaltyType") == null ? "" : r.Element("penaltyType").Value.ToString();
                        objretrievePenalty.componentId = r.Element("componentId") == null ? "" : r.Element("componentId").Value.ToString();
                        objretrievePenalty.componentInstanceId = r.Element("componentInstId") == null ? "" : r.Element("componentInstId").Value.ToString();
                        DateTime endDate = DateTime.ParseExact(objretrievePenalty.endDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        TimeSpan noOfDaysForContract = endDate.Date - DateTime.Now.Date;
                        //if (penalty.Length == 0) 
                        if (noOfDaysForContract.Days > 0)
                        {

                            //if (!string.IsNullOrEmpty(objretrievePenalty.penalty))
                            //{
                            //    penaltyAmt+=Convert.ToDecimal(penaltybyMSISDN.penalty);
                            //}
                            //penalty = penaltyAmt.ToString("0.00");                           
                            //penaltybyMSISDN.Add(objretrievePenalty);


                            foreach (var e in r.Descendants("extendedData"))
                            {
                                string paramName = e.Element("paramName") == null ? "" : e.Element("paramName").Value.ToString();
                                if (paramName.ToUpper().Contains("RRP") == true)
                                {
                                    rrp = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
                                }
                                else if (paramName.ToUpper().Contains("PHONE MODEL") == true)
                                {
                                    phoneModel = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
                                }
                                else if (paramName.ToUpper().Contains("ADMIN FEE") == true)
                                {
                                    adminFee = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
                                }
                                else if (paramName.ToUpper().Contains("UPFRONT PAYMENT") == true)
                                {
                                    UpfrontPayment = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
                                }
                                else if (paramName.ToUpper().Contains("DEVICE MAKE AND MODEL") == true)
                                {
                                    deviceModel = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
                                }
                                else if (paramName.ToUpper().Contains("DEVICE IMEI") == true)
                                {
                                    deviceIMEI = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
                                }
                                else if (paramName.ToUpper().Contains("UPGRADE FEE") == true)
                                {
                                    upgradeFee = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
                                }
								else if (paramName.ToUpper().Contains("ACCESSORY") == true)
								{
									ParamList paramList = new ParamList();
									paramList.paramID = e.Element("paramId") == null ? "" : e.Element("paramId").Value.ToString();
									paramList.paramName = e.Element("paramName") == null ? "" : e.Element("paramName").Value.ToString();
									paramList.paramValue = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
									
									paramLists.Add(paramList);
								}

                            }

                            objretrievePenalty.penalty = objretrievePenalty.penalty;
                            objretrievePenalty.rrp = rrp;
                            objretrievePenalty.phoneModel = phoneModel;
                            objretrievePenalty.adminFee = adminFee;
                            objretrievePenalty.UpfrontPayment = UpfrontPayment;
                            objretrievePenalty.DeviceModel = deviceModel;
                            objretrievePenalty.DeviceIMEI = deviceIMEI;
                            objretrievePenalty.UpgradeFee = upgradeFee;
							objretrievePenalty.paramLists = paramLists;

                            penaltybyMSISDN.Add(objretrievePenalty);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            return penaltybyMSISDN;
        }
        public IList<retrievePenaltyByMSISDN> retrievePenalty_Smart(string MSISDN, string ExternalID, string SubscriberNo, string SubscriberNoResets)
        {

            string rrp = string.Empty;
            string phoneModel = string.Empty;
            string adminFee = string.Empty;
            string UpfrontPayment = string.Empty;
            string postUrl = System.Configuration.ConfigurationManager.AppSettings["RetrieveContractInfoService"].ToString();
            List<retrievePenaltyByMSISDN> listPenalty = new List<retrievePenaltyByMSISDN>();

            string postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:max=""http://maxis71/maxis.eai.process.common.ws:retrieveContractInfoService"" xmlns:v11=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
   <soapenv:Header>
      <v1:eaiHeader>
         <v1:from>ISELL</v1:from>
         <v1:to>ESB</v1:to>
         <v1:appId>OPF</v1:appId>
         <v1:msgType>Request</v1:msgType>
         <v1:msgId>12345</v1:msgId>
         <!--Optional:-->
         <v1:correlationId>?</v1:correlationId>
         <v1:timestamp>20130316</v1:timestamp>
      </v1:eaiHeader>
   </soapenv:Header>
   <soapenv:Body>
      <max:retrieveContractInfoRequest>
         <v11:retrieveContractInfoRequestDetails>
            <v11:externalId>{0}</v11:externalId>
            <v11:extenalIdType>{1}</v11:extenalIdType>
            <!--Optional:-->
            <v11:subscrNo>{2}</v11:subscrNo>
            <!--Optional:-->
            <v11:subscrNoResets>{3}</v11:subscrNoResets>
         </v11:retrieveContractInfoRequestDetails>
      </max:retrieveContractInfoRequest>
   </soapenv:Body>
</soapenv:Envelope>", MSISDN, ExternalID, SubscriberNo, SubscriberNoResets);
            WebClient retrieveAcctListWebClient = new WebClient();
            Uri postUri = new Uri(postUrl);
            string postResult = string.Empty;

            retrieveAcctListWebClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");

            try
            {
                //Depending upon settings if true then use dummy data

                postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                postResult = RemoveAllNamespaces(postResult);
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", postResult));

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here
                XDocument doc = XDocument.Parse(postResult);
                string penalty = string.Empty;
                foreach (var p in doc.Descendants("retrieveContractInfoResponseDetails"))
                {

                    string msgcode = p.Element("msgCode") == null ? "" : p.Element("msgCode").Value.ToString();
                    string msgDesc = p.Element("msgDesc") == null ? "" : p.Element("msgDesc").Value.ToString();
                    foreach (var r in doc.Descendants("contractInfoList"))
                    {
                        retrievePenaltyByMSISDN penaltybyMSISDN = new retrievePenaltyByMSISDN();
                        penaltybyMSISDN.msgCode = msgcode;
                        penaltybyMSISDN.msgDesc = msgDesc;
                        penaltybyMSISDN.startDate = r.Element("startDate") == null ? "" : r.Element("startDate").Value.ToString();
                        penaltybyMSISDN.endDate = r.Element("endDate") == null ? "" : r.Element("endDate").Value.ToString();
                        penaltybyMSISDN.contractType = r.Element("contractType") == null ? "" : r.Element("contractType").Value.ToString();
                        penaltybyMSISDN.penalty = r.Element("penalty") == null ? "" : r.Element("penalty").Value.ToString();

                        DateTime endDate = DateTime.ParseExact(penaltybyMSISDN.endDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        TimeSpan noOfDaysForContract = endDate.Date - DateTime.Now.Date;
                        //if (penalty.Length == 0)
                        if (noOfDaysForContract.Days > 0)
                        {
                            foreach (var e in r.Descendants("extendedData"))
                            {
                                string paramName = e.Element("paramName") == null ? "" : e.Element("paramName").Value.ToString();
                                if (paramName.ToUpper().Contains("RRP") == true)
                                {
                                    rrp = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
                                }
                                else if (paramName.ToUpper().Contains("PHONE MODEL") == true)
                                {
                                    phoneModel = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
                                }
                                else if (paramName.ToUpper().Contains("ADMIN FEE") == true)
                                {
                                    adminFee = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
                                }
                                else if (paramName.ToUpper().Contains("UPFRONT PAYMENT") == true)
                                {
                                    UpfrontPayment = e.Element("paramValue") == null ? "" : e.Element("paramValue").Value.ToString();
                                }
                            }

                            penalty = penaltybyMSISDN.penalty;
                            penaltybyMSISDN.penalty = penalty;
                            penaltybyMSISDN.rrp = rrp;
                            penaltybyMSISDN.phoneModel = phoneModel;
                            penaltybyMSISDN.adminFee = adminFee;
                            penaltybyMSISDN.UpfrontPayment = UpfrontPayment;
                            listPenalty.Add(penaltybyMSISDN);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            return listPenalty;
        }

        #endregion

        #region [Extra Ten]
        public ExtraTenRetreivalResponse RetrieveExtraTen(ExtraTenRetrieveService.typeRetrieveExtraTenRequest request)
        {
            var response = new ExtraTenRetreivalResponse();
            try
            {

                string reqXML = string.Format(@"<soapenv:Envelope 
                                    xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                                    xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" 
                                    xmlns:v11=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/""
                                    xmlns:v12=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
                                   <soapenv:Header>
                                      <v1:eaiHeader>
                                         <v1:from>ISELL</v1:from>
                                         <v1:to>ESB</v1:to>
                                         <v1:appId>OPF</v1:appId>
                                         <v1:msgType>Request</v1:msgType>
                                         <v1:msgId>ISELL{0}</v1:msgId>
                                         <!--Optional:-->
                                         <v1:correlationId>?</v1:correlationId>
                                         <v1:timestamp>{1}</v1:timestamp>
                                      </v1:eaiHeader>
                                   </soapenv:Header>
                                   <soapenv:Body>
                                      <v11:retrieveExtraTenRequest>
                                         <v12:retrieveExtraTenRequestDetails>
                                            <v12:externalId>{2}</v12:externalId>
                                            <v12:corridorId>{3}</v12:corridorId>
                                         </v12:retrieveExtraTenRequestDetails>
                                      </v11:retrieveExtraTenRequest>
                                   </soapenv:Body>
                                </soapenv:Envelope>", DateTime.Now.ToString("yyyyMMdd"), DateTime.Now.ToString("yyyyMMddHHmmssFFF"), request.externalId, request.corridorId);

                WebClient retrieveExtraTenClient = new WebClient();
                //Uri postUri = new Uri("http://10.200.51.125:7030/ws/maxis.eai.process.common.ws:retrieveExtraTenService/maxis_eai_process_common_ws_retrieveExtraTenService_Port");
                Uri postUri = new Uri(Properties.Settings.Default.Online_Registration_IntegrationService_retrieveExtraTenService_retrieveExtraTenService_Port.ToString());
                string postResult = string.Empty;
                retrieveExtraTenClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                retrieveExtraTenClient.Headers.Add("content-type", "text/xml");
                postResult = retrieveExtraTenClient.UploadString(postUri, reqXML);
                postResult = Util.RemoveAllNamespaces(postResult);
                Logger.Info(String.Format(" ExtraTenWS.RetrieveExtraTen Request : {0} ", reqXML));
                Logger.Info(String.Format(" ExtraTenWS.RetrieveExtraTen Response: {0} ", postResult));
                XDocument doc = XDocument.Parse(postResult);
                foreach (var r in doc.Descendants("retrieveExtraTenResponseDetails"))
                {
                    response.Code = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    response.Message = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
                    if (response.Code == "0")
                    {
                        response.Success = true;
                    }
                    else
                    {
                        response.Success = false;
                    }
                    if (ReferenceEquals(response.ExtraTenInfo, null))
                        response.ExtraTenInfo = new ExtraTenRetrieveService.typeRetrieveExtraTenResponse();
                    response.ExtraTenInfo.chargeAmt = r.Element("chargeAmt") == null ? "0" : r.Element("chargeAmt").Value.ToString();
                    response.ExtraTenInfo.counter = r.Element("counter") == null ? "0" : r.Element("counter").Value.ToString();
                    var fnfNumbers = r.Element("fnfDoc").Descendants("msisdn");
                    var responseFNFNumbers = new List<ExtraTenRetrieveService.fnfList>();
                    foreach (var item in fnfNumbers)
                    {
                        responseFNFNumbers.Add(new ExtraTenRetrieveService.fnfList() { msisdn = item.Value });
                    }
                    response.ExtraTenInfo.fnfDoc = responseFNFNumbers.ToArray();
                }
                response.Success = true;

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
                response.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            return response;
        }

        public ExtraTenUpdateResponse ProcessExtraTen(Online.Registration.IntegrationService.ExtraTenWS.typeProcessExtraTenRequest extraTenRequest, int regID)
        {
            var response = new ExtraTenUpdateResponse();
            string processXtraTenSOAPXML = string.Empty;
            string postResult = string.Empty;
            try
            {
                typeProcessExtraTenResponse objExtraTenResponse = new typeProcessExtraTenResponse();
                processXtraTenSOAPXML = String.Format(@"<soapenv:Envelope 
                                                xmlns:v12=""http://schemas.maxis.com.my/EAI/FX/vas/v1/"" 
                                                xmlns:v11=""http://services.maxis.com.my/EAI/FX/vasService/v1/"" 
                                                xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" 
                                                xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""> 
                                            <soapenv:Header> 
                                                    <v1:eaiHeader> 
                                                        <v1:from>?</v1:from> 
                                                        <v1:to>?</v1:to> 
                                                        <v1:appId>?</v1:appId> 
                                                        <v1:msgType>?</v1:msgType> 
                                                        <v1:msgId>?</v1:msgId> 
                                                        <!--Optional:-->
                                                        <v1:correlationId>?</v1:correlationId> 
                                                        <v1:timestamp>?</v1:timestamp> 
                                                    </v1:eaiHeader> 
                                            </soapenv:Header> 
                                <soapenv:Body> 
                                <v11:processExtraTenRequest> 
                                    <v12:processExtraTenRequestDetails> 
                                        <v12:actionType>{0}</v12:actionType> 
                                        <v12:reqType>{1}</v12:reqType> 
                                        <v12:externalId>{2}</v12:externalId> 
                                        <v12:componentId>{3}</v12:componentId> 
                                        <!--Optional:-->
                                        <v12:oldFnfMsisdn>{4}</v12:oldFnfMsisdn>
                                        <!--Optional:-->
                                        <v12:newFnfMsisdn>{5}</v12:newFnfMsisdn> 
                                        <!--Optional:-->
                                 <v12:inpF01>?</v12:inpF01> 
                                <!--Optional:-->
                                 <v12:inpF02>?</v12:inpF02> 
                                <!--Optional:-->
                                 <v12:inpF03>?</v12:inpF03> 
                                <!--Optional:-->
                                 <v12:inpF04>?</v12:inpF04> 
                                <!--Optional:-->
                                 <v12:inpF05>?</v12:inpF05> 
                                <!--Optional:-->
                                 <v12:inpF06>?</v12:inpF06> 
                                <!--Optional:-->
                                 <v12:inpF07>?</v12:inpF07> 
                                <!--Optional:-->
                                 <v12:inpF08>?</v12:inpF08> 
                                <!--Optional:-->
                                 <v12:inpF09>?</v12:inpF09> 
                                <!--Optional:-->
                                 <v12:inpF10>?</v12:inpF10>
                                </v12:processExtraTenRequestDetails> 
                        </v11:processExtraTenRequest> 
                        </soapenv:Body> 
                    </soapenv:Envelope>", extraTenRequest.actionType, extraTenRequest.reqType, extraTenRequest.externalId, extraTenRequest.componentId, extraTenRequest.oldFnfMsisdn, extraTenRequest.newFnfMsisdn);

                Logger.InfoFormat("ProcessExtraTen: URL {0}", ConfigurationManager.AppSettings.Get("Online_Registration_IntegrationService_ExtraTenWS_vasService"));
                Logger.Info(String.Format(" ExtraTenWS.processExtraTen Request : {0} ", processXtraTenSOAPXML));
                WebClient processExtraTenClient = new WebClient();
                //Uri postUri = new Uri("http://10.200.51.122:6030/ws/maxis.eai.process.vas.ws:vasService/maxis_eai_process_vas_ws_vasService_Port");
                var vasUri = ConfigurationManager.AppSettings.Get("Online_Registration_IntegrationService_ExtraTenWS_vasService");
                Uri postUri = new Uri(vasUri);
                postResult = string.Empty;
                processExtraTenClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                processExtraTenClient.Headers.Add("content-type", "text/xml");
                postResult = processExtraTenClient.UploadString(postUri, processXtraTenSOAPXML);
                postResult = Util.RemoveAllNamespaces(postResult);
                Logger.Info(String.Format(" ExtraTenWS.processExtraTen Response: {0} ", postResult));
                XDocument doc = XDocument.Parse(postResult);
                foreach (var r in doc.Descendants("processExtraTenResponseDetails"))
                {
                    objExtraTenResponse.msgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    objExtraTenResponse.msgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
                    if (objExtraTenResponse.msgCode == "0")
                    {
                        response.Success = true;
                    }
                    response.Code = objExtraTenResponse.msgCode;
                    response.Message = (objExtraTenResponse.msgCode == "2" ? "Unable to process the request." : objExtraTenResponse.msgDesc);
                }
                #region Added by Nreddy for loggings
                KenanaLogDetails objKenanaLogDetails = new KenanaLogDetails();
                objKenanaLogDetails.RegID = regID;
                //objKenanaLogDetails.MessageCode = objExtraTenResponse.msgCode;
                objKenanaLogDetails.MessageCode = response.Code;
                objKenanaLogDetails.MessageDesc = !ReferenceEquals(objExtraTenResponse.msgDesc, null) ? objExtraTenResponse.msgDesc : string.Empty;
                objKenanaLogDetails.KenanXmlReq = processXtraTenSOAPXML;
                objKenanaLogDetails.KenanXmlRes = postResult;
                objKenanaLogDetails.MethodName = "processExtraTen";
                OnlineRegAdmin.SaveKenanXmlLogs(objKenanaLogDetails);
                #endregion

                response.ExtraTenProcessResponse = objExtraTenResponse;
                if (objExtraTenResponse.msgCode == "0")
                    response.Success = true;
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                response.Success = false;
                response.Message = ex.Message;
                DAL.Registration.RegistrationCancel(new RegStatus()
                {
                    RegID = regID,
                    Active = true,
                    CreateDT = DateTime.Now,
                    StartDate = DateTime.Now,
                    LastAccessID = "SYSTEM",
                    StatusID = OnlineRegAdmin.StatusFind(new StatusFind()
                    {
                        Status = new Status()
                        {
                            Code = "REG_Fail"
                        }
                    }).ToList()[0]
                });

                #region Added by Nreddy for logging
                KenanaLogDetails objKenanaLogDetails = new KenanaLogDetails();
                objKenanaLogDetails.RegID = regID;
                objKenanaLogDetails.MessageCode = "10";
                objKenanaLogDetails.MessageDesc = ConfigurationManager.AppSettings["OPFUrlException"].ToString() + ex.Message;
                objKenanaLogDetails.KenanXmlReq = processXtraTenSOAPXML;
                objKenanaLogDetails.KenanXmlRes = postResult;
                objKenanaLogDetails.MethodName = "processExtraTen";
                OnlineRegAdmin.SaveKenanXmlLogs(objKenanaLogDetails);
                #endregion
                // PopulateExceptionResponse(objExtraTenResponse, ex);
            }
            return response;

        }

        #endregion

    }
}
