﻿using System.Collections.Generic;
using System.Net.Mail;
using System.IO;
using System.Globalization;
using System;
using System.Text;
using System.Configuration;
using System.Net;
public static class MailHelper
{

    public static string SendEmail(string to, string from, string subject, Dictionary<string, string> keyValuePairs)
    {

        try
        {
            var body = GetHTMLStringFromDictionary(keyValuePairs);
            var Message = new MailMessage(from, to)
            {
                Subject = subject,
                IsBodyHtml = true,
                Body = body
            };

            SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]);
            client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpServerPort"]);
            client.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpServerEnableSsl"]);
            client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["FromUsername"], ConfigurationManager.AppSettings["FromEmailPassword"]);
            client.Send(Message);
        }
        catch (Exception ex)
        {
            return string.Format("Exception occured in sendmail funcation: {0}", ex.Message);
        }

        return "Email sent successfully";
    }



    private static string GetHTMLStringFromDictionary(Dictionary<string, string> htmlDictionary)
    {

        StringBuilder HTMLString = new StringBuilder();

        HTMLString.Append("<table style='border:1px solid black;'><tr height='40'><td colspan='8'>CUSTOMER INFORMATION:</td></tr>");
        int index = 1;
        foreach (KeyValuePair<string, string> item in htmlDictionary)
        {
            if (index % 2 == 1)
            {
                HTMLString.Append("<tr height='30'>");
            }
            HTMLString.Append(string.Format("<td width='200'>{0}:</td><td width='350'>{1}</td>", item.Key, item.Value));
            if (index % 2 == 0)
            {
                HTMLString.Append("</tr>");
            }
            index++;
        }
        HTMLString.Append (string.Format ("<tr height='60'><td colspan='2' align='center'><a href='{0}'>Approve</a></td><td colspan='2' align='center'><a href='{1}'>Reject</a></td></tr>",  "waiver/Approve","waiver/Reject"));
        HTMLString.Append("</table>");
        return HTMLString.ToString();

    }



}
