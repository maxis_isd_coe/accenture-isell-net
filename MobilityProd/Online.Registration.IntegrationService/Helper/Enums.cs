﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.IntegrationService.Helper
{
    public struct Lob
    {
        public static string VOIP = "VOIP";
        public static string FTTH = "FTTH";

    }


    public enum ServiceStatus
    {
        HM_VOIP_PEND = 1,
        HM_VOIP_SUCC = 2,
        HM_VOIP_FAIL = 3,
        HM_FTTH_SUCC = 4,
        HM_FTTH_FAIL = 5,
        HM_VOIP_FF_SUCC = 6,
        HM_VOIP_FF_FAIL = 7,
        HM_FTTH_FF_SUCC = 8,
        HM_FTTH_FF_FAIL = 9,
        HM_FTTH_PEND = 10,
        HM_FTTH_FF_PEND = 11,
        HM_VOIP_FF_PEND = 12,
        HM_VOIP_FF_CLOSE = 13,
        HM_VOIP_FF_CLOSEFAIL = 14
      
    }
    public enum HomeKenanServiceStatus
    {
        HM_READY_TO_INSTALL=55,
        HM_VOIP_PENDING = 60,
        HM_VOIP_SUCC = 61,
        HM_VOIP_FAIL = 62,
        HM_VOIP_FF_PENDING = 63,
        HM_VOIP_FF_SUCC = 64,
        HM_VOIP_FF_FAIL = 65,
        HM_FTTH_PENDING = 66,
        HM_FTTH_SUCC = 67,
        HM_FTTH_FAIL = 68,
        HM_FTTH_FF_PENDING = 69,
        HM_FTTH_FF_SUCC = 70,
        HM_FTTH_FF_FAIL = 71,
        HM_COMPLETED_PENDING = 72,
        HM_Confirmed = 16,
        HM_VOIP_CORD_SUCC = 73,
        HM_VOIP_CORD_FAIL = 74,
        HM_FTTH_CORD_SUCC = 75,
        HM_FTTH_CORD_FAIL = 76,
        HM_VOIP_DKNOCK_SUCC = 77,
        HM_VOIP_DKNOCK_FAIL = 78,
        HM_FTTH_DKNOCK_SUCC = 79,
        HM_FTTH_DKNOCK_FAIL = 80,
        HM_PENDING_DEVICE = 81,
        HM_VOIP_CANCEL_SUCC = 86,
        HM_VOIP_CANCEL_FAIL = 87,
        HM_FTTH_CANCEL_SUCC = 88,
        HM_FTTH_CANCEL_FAIL = 89,
        HM_CAN = 8,
        HM_REL_SUB=500,
        REL_DIS_VOIP_SUB = 503,
        REL_DIS_FTTH_SUB = 504,
        REL_DIS_VOIP_SUCC = 505,
        REL_DIS_FTTH_SUCC = 506,
        REL_DIS_KENAN_SUCC = 507,
        REL_DIS_KENAN_FAIL = 508,
        REL_DIS_VOIP_FAIL = 509,
        REL_DIS_FTTH_FAIL = 510,
        HM_CP_VOIP_SUCC=513,
        HM_CP_VOIP_FAIL=514,
        HM_CP_FTTH_SUCC=515,
        HM_CP_FTTH_FAIL=516,
        HM_CP_CONF_FAIL=517,
        HM_CMSS_PEND_REL= 521,
        HM_CMSS_PEND_TER=522,
        HM_CMSS_PEND_CP=525,
        HM_CP_CONF=518,
        HM_TER_SUB = 502,
        HM_CP_COMP=100,
        HM_UNINSTALL_SUCC = 524,
        HM_INSTALL_SUCC=523,
        HM_Completed=5,
        HM_ADDR_VOIP_SUCC=539,
        HM_ADDR_FTTH_SUCC=540,
        HM_ADDR_VOIP_FAIL=541,
        HM_ADDR_FTTH_FAIL=542,
        HM_VOIP_COMMIT=543,
        HM_FTTH_COMMIT=544,
        HM_COMMITTED=547,
        HM_DIS_VOIP_COMMIT=548,
        HM_DIS_FTTH_COMMIT=549,
        HM_POSTGSM_COMMIT=552,
        HM_POSTGSM_FAIL = 556,
        HM_POSTGSM_SUCC = 557
        
    } 


}