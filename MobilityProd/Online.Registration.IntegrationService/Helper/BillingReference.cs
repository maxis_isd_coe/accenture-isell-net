﻿using System;
using System.Web;
using System.Web.Services.Protocols;
using System.Net;
using System.Text;

namespace Online.Registration.IntegrationService.MaxisBillingProcessAcctDetlWs
{
    public partial class maxis_eai_process_ivr_ws_billingService : SoapHttpClientProtocol
    {
        protected override System.Net.WebRequest GetWebRequest(Uri uri)
        {
            HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(uri);

            if (this.PreAuthenticate)
            {
                var credential = this.Credentials.GetCredential(uri, "Basic");
                if (credential != null)
                {
                    byte[] buffer = new UTF8Encoding().GetBytes(credential.UserName + ":" + credential.Password);
                    request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(buffer);
                }
            }
            return request;
        }

    }
}