﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Online.Registration.DAL.Models;

using log4net;
using System.Configuration;
using SNT.Utility;
using System.IO;
using SNT.Utility;
using Codaxy.WkHtmlToPdf;
using Online.Registration.DAL.Admin;
using Online.Registration.IntegrationService.MaxisOrderProcessWs;

namespace Online.Registration.IntegrationService.Helper
{
    public class Util
    {
		static readonly ILog Logger = LogManager.GetLogger(typeof(KenanService));

        public enum MobileRegType
        {
            DevicePlan = 1,
            PlanOnly = 2,
            DeviceOnly = 6,
            SuppPlan = 8,
            NewLine = 9,
            MNPPlanOnly = 10,
            MNPSuppPlan = 11,
            MNPNewLine = 12,
            AddRemoveVAS = 13,
            SimReplacement = 14,
            CRP = 19,
            DeviceCRP = 24,
            RegType_MNPPlanWithMultiSuppline = 26,
            SecPlan = 27,
            SupplementaryPlanwithDevice = 37,
            Contract = 36,
            DeviceSales = 38,
            ManageExtraTen = 39
        }


        public static string RemoveAllNamespaces(string xmlDocument)
        {
            XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

            return xmlDocumentWithoutNs.ToString();
        }

        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;
                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }

        public static string LogException(Exception objException)
        {
            System.Text.StringBuilder strException = new System.Text.StringBuilder();
            strException.AppendFormat("Exception Found:\n{0}", objException.GetType().FullName);
            strException.AppendFormat("\n{0}", objException.Message);
            strException.AppendFormat("\n{0}", objException.Source);
            strException.AppendFormat("\n{0}", objException.StackTrace);
            if (objException.InnerException != null)
            {
                strException.AppendFormat("Inner Exception Found:\n{0}", objException.InnerException.GetType().FullName);
                strException.AppendFormat("\n{0}", objException.InnerException.Message);
                strException.AppendFormat("\n{0}", objException.InnerException.Source);
                strException.AppendFormat("\n{0}", objException.InnerException.StackTrace);
            }
            return strException.ToString();

        }

        public static string EncodeStringForXmlReq(string data)
        {
            return System.Web.HttpUtility.UrlEncode(data);
        }

        public static string PosDateFormat()
        {
            return DateTime.Now.Date.ToString("yyyyMMdd");
        }

        public static string FormatContactNumber(string contactNumber)
        {
            // posibble value
            // 60|6xxx
            // 6|0xxx
            // 60|xxx
            // 60xxxx
            if (!string.IsNullOrEmpty(contactNumber))
            {
                if (contactNumber.Length >= 2)
                {
                    bool firstIs60 = string.Equals(contactNumber.Substring(0, 2), "60");
                    bool firstIs6 = string.Equals(contactNumber.Substring(0, 1), "6");
                    bool secondIs0 = string.Equals(contactNumber.Substring(1, 1), "0");
                    bool firstIs0 = string.Equals(contactNumber.Substring(0, 1), "0");

                    if (firstIs0)
                    {
                        contactNumber = "6" + contactNumber;
                    }
                    else if (!firstIs60 || (firstIs6 && !secondIs0))
                    {
                        contactNumber = "60" + contactNumber;
                    }
                }
            }
            return contactNumber;
        }

		public static string getDiffDate(DateTime givenDate)
		{
			string result = "";
			//Drop5 UAT - Bugzilla 961 to remove the time information
			var datetimeNow = DateTime.Now.Date;

			int datediffdays;

			datediffdays = ((
				(givenDate > datetimeNow) ? (givenDate - datetimeNow) : (datetimeNow - givenDate)
				).TotalMilliseconds / 86400000).ToInt();
			int calcDuration = (int)Math.Round((datediffdays * 12.0) / 365.0);

			result = calcDuration.ToString2();

			return result;
		}
		//Evan-150323 Change for ContractCheck sync with Kenan - end

        public static int FormatOrderNumber(string replacement, string input, bool getRegPrimeOrderID = true)
        {
            string processedInput = input.Replace(replacement, "");
            
            if ( !processedInput.Contains("-") )
                return Convert.ToInt32(processedInput);
            
            string[] prinSuppID = processedInput.Split('-');

            return getRegPrimeOrderID ? Convert.ToInt32(prinSuppID[0].ToString()) : Convert.ToInt32(prinSuppID[1].ToString());
        }

		public static bool UpdateSuppStatus(int primeRegID, int suppRegID)
		{
			bool result = false;
			List<RegSuppLine> Suplines = new List<RegSuppLine>();
			Logger.Info("####### UpdateSuppStatus - START ####### - " + primeRegID + " # "+suppRegID);
			var suppLineIDs = DAL.Registration.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { ID = suppRegID } }).ToList();
			Logger.Info("How many supps ? " + suppLineIDs.Count);
			if (suppLineIDs.Count() > 0)
			{
				var regSuppLinesList = DAL.Registration.RegSuppLineGet(suppLineIDs).ToList();
				Suplines = regSuppLinesList;
			}
			if (Suplines.Count > 0)
			{
				foreach (var _supline in Suplines)
				{
					_supline.KENAN_Req_Status = true;
					_supline.LastUpdateDT = DateTime.Now;
					_supline.STATUS_DESC = string.Empty;
					_supline.Order_Status = "Service Activated";
					_supline.ID = _supline.ID;
					int recordUPdate = DAL.Registration.RegSuppLineUpdate(_supline);

					DAL.Registration.RegStatusCreate(new DAL.Models.RegStatus()
					{
						RegID = primeRegID,
						//StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_SvcPendingActivate),
						StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_SvcActivated),
						CreateDT = DateTime.Now,
						Active = true,
						LastAccessID = "SYSTEM"
					});
				}
			}
			Logger.Info("####### UpdateSuppStatus - END ####### - " + primeRegID + " # " + suppRegID);
			return result;

		}

		public static decimal getPrinSuppTotalPrice(int regPrimeID)
		{
			// need return 1, so will not auto Fullfill
			if (regPrimeID == 0)
				return 1;

			decimal totalPrice = 0;
			decimal totalSuppPrice = 0;
			decimal totalPrinPrice = 0;

			bool _prinHaveDevice = false;
			bool _suppHaveDevice = false;

			try
			{
				// for principal
				List<int> regIDs = new List<int>();
				regIDs.Add(regPrimeID);
				var _prinLine = DAL.Registration.RegistrationGet(regIDs);
				totalPrinPrice = _prinLine[0].DeviceAdvance + _prinLine[0].DeviceDeposit + _prinLine[0].PlanDeposit + _prinLine[0].PlanAdvance;
				
				// if have device, do not do fullfill, need to go to pos
				_prinHaveDevice = !string.IsNullOrEmpty(_prinLine[0].IMEINumber) ? true : false;
				if (_prinHaveDevice)
					totalPrinPrice += 1;

				// for suppLine
				List<DAL.Models.RegSuppLine> RegSupplines = new List<RegSuppLine>();
				var suppLineIDs = DAL.Registration.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = regPrimeID } }).ToList();
				string result = string.Empty;
				if (suppLineIDs.Any())
				{
					var regSuppLinesList = DAL.Registration.RegSuppLineGet(suppLineIDs).ToList();
					if (regSuppLinesList.Any())
					{
						foreach (var _suppline in regSuppLinesList)
						{
							var eachSuppPrice = _suppline.deviceadvance + _suppline.devicedeposit + _suppline.planadvance + _suppline.plandeposit;
							totalSuppPrice += eachSuppPrice;

							// if have device, do not do fullfill, need to go to pos
							_suppHaveDevice = !string.IsNullOrEmpty(_suppline.IMEINumber) ? true : false;
							if (_suppHaveDevice)
								totalSuppPrice += 1;
						}
					}
				}

                bool isCDPUorder = Util.isCDPUorder(regPrimeID);

                totalPrinPrice = isCDPUorder ? 0 : totalPrinPrice;
                totalSuppPrice = isCDPUorder ? 0 : totalSuppPrice;

                return totalPrinPrice + totalSuppPrice;
				
			}
			catch (Exception ex)
			{
				string errMsg = LogException(ex);
				Logger.Error(errMsg);
			}
			return totalPrinPrice + totalSuppPrice;
		}

		// this method is for check whether all suppline has been processed

		public static bool CheckStatusAllSuppline(int primeRegID)
		{
			bool resp = false;
			int[] eligibleOrderMultipleSupp = new int[] { 2, 3, 9 };
			List<RegSuppLine> Suplines = new List<RegSuppLine>();

			var reg = DAL.Registration.RegistrationGet(new int[] { primeRegID }.ToList()).SingleOrDefault();
			var suppLineIDs = DAL.Registration.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = primeRegID } }).ToList();
			Logger.Info("How many supps ? " + suppLineIDs.Count);

			if (eligibleOrderMultipleSupp.Contains(reg.RegTypeID))
			{
				if (!ReferenceEquals(suppLineIDs, null) && suppLineIDs.Count() > 0)
				{
					Suplines = DAL.Registration.RegSuppLineGet(suppLineIDs).ToList();
				}
				if (Suplines.Count > 0)
				{
					foreach (var _supline in Suplines)
					{
						try
						{
							if (!string.IsNullOrEmpty(_supline.Order_Status))
							{
								if (!_supline.Order_Status.Equals("New"))
								{
									resp = true;
									break;
								}
							}
							else
							{
								resp = true;
							}
						}
						catch (Exception ex)
						{
							resp = true;
						}
					}
				}
				else
				{
					resp = true;
				}
			}
			else {
				resp = true;
			}

			return resp;
		}


		// this method for determine which store that can be use iSell drop 4
		// and will impact iContract & email capabilities.
		// this method is exist on Web layer, so if there's any changes, change it both
		#region old method of rollOutCheck
		/*
		public static bool rollOutCheck(int regID)
		{
			bool resp = false;

			List<string> storeIDs = new List<string>();
			var currentStoreID = Util.getCenterOrgIDByRegID(regID);

			if (!ReferenceEquals(ConfigurationManager.AppSettings["StoreListRollOut"], null))
			{
				storeIDs = System.Configuration.ConfigurationManager.AppSettings["StoreListRollOut"].ToString2().Split(',').ToList();
				if (storeIDs.Count > 0)
				{
					if (storeIDs.Contains(currentStoreID))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			else
			{
				// if it's null, means this is full rollout, so return true
				return true;
			}

			return resp;
		}
		*/
		#endregion

		public static bool rollOutCheck(int regID)
		{
			bool resp = false;

			try
			{
				var regDetails = DAL.Registration.RegistrationGet(new int[] { regID }.ToList()).SingleOrDefault();
				if (!ReferenceEquals(regDetails.DropVersion,null))
				{
					if (!string.IsNullOrEmpty(regDetails.DropVersion) && regDetails.DropVersion.Equals("4"))
						resp = true;
				}
			}
			catch (Exception ex)
			{
				resp = false;
				Logger.Debug("Exception on rollOutCheck, with regid " + regID);
				Logger.Debug(ex);
			}

			return resp;
		}

		public static bool isDealer(DAL.Models.Registration _reg)
		{
			var access = DAL.Admin.OnlineRegAdmin.AccessGetbyUserName(_reg.SalesPerson);
			var userDetails = DAL.Admin.OnlineRegAdmin.UserGet(new List<int> { access.UserID });

			return userDetails.SingleOrDefault().isDealer;
		}

		public static string getCenterOrgIDByRegID(int regID)
		{
			var regDetails = DAL.Registration.RegistrationGet(new int[] { regID }.ToList()).SingleOrDefault();
			//var orgDetails = DAL.Admin.OnlineRegAdmin.OrganizationGet(new int[] { regDetails.CenterOrgID }.ToList()).SingleOrDefault();

			return regDetails.CenterOrgID.ToString2();
		}

		public static void convertHTMLStringToPDF(ref string html, ref string regID, out string exception, out string outputPath)
		{
			exception = string.Empty;
			string printFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "print");
			string outputFileName = string.Format("{0}.pdf", regID);
			outputPath = Path.Combine(printFolder + @"\", outputFileName);

			try
			{
				PdfConvert.ConvertHtmlToPdf(new PdfDocument
				{
					Url = html
				}, new PdfOutput
				{
					OutputFilePath = outputPath
				});
			}
			catch (Exception ex)
			{
				exception = ex.ToString2();
				outputPath = string.Empty;
			}
		}

        public static bool isAutoKnockOff(DAL.Models.Registration _reg)
        {
            bool _return = false;
            String CDPUstatus = String.Empty;
            CDPUstatus = DAL.Registration.GetCDPUStatus(_reg.ID);
            bool isDealer = Util.isDealer(_reg);
            //bool triggerEmailByCDPU = Convert.ToBoolean(ConfigurationManager.AppSettings["triggerEmail"]);//move to PostActivation Service
            //bool isCDPU = Util.isDealer(_reg) && !ReferenceEquals(CDPUstatus, null) && Equals(CDPUstatus, Constants.StatusCDPUApprove);

            //if (isCDPU)
            //{
                
                #region total payable 0, activate Service
                if (Util.getPrinSuppTotalPrice(_reg.ID) == 0)
                {
                    var regAccount = new DAL.Models.RegAccount();
                    regAccount = DAL.Registration.RegAccountGetByRegID(_reg.ID, "");
                    var regStatus = new DAL.Models.RegStatus();
                    regStatus = new DAL.Models.RegStatus()
                        {
                            RegID = _reg.ID,
                            StartDate = DateTime.Now,
                            StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_PaymentReceived),
                            CreateDT = DateTime.Now,
                            Active = true,
                            LastAccessID = "KENAN CALLBACK",
                        };
                    DAL.Registration.RegAccountUpdate(regAccount, regStatus);
                    var proxy = new KenanService();
                    var autoProxy = new AutoActivation();
                    if (_reg.RegTypeID == 10 || _reg.RegTypeID == 11 || _reg.RegTypeID == 12 || _reg.RegTypeID == 26)
                    {
                        proxy.KenanMNPAccountFulfill(new Models.MNPtypeFullfillCenterOrderMNP { orderId = _reg.ID.ToString2() });
                    }
                    else if (_reg.RegTypeID == (int)MobileRegType.CRP)
                    {
                        proxy.CenterOrderContractCreation(new Models.CenterOrderContractRequest { orderId = _reg.ID.ToString2() });
                    }
                    else if (_reg.RegTypeID == (int)MobileRegType.SecPlan)
                    {
                        autoProxy.KenanAccountFulfill((int)MobileRegType.SecPlan, _reg.ID.ToString2());
                    }
                    else
                    {
                        proxy.KenanAccountFulfill(new Models.OrderFulfillRequest { OrderID = _reg.ID.ToString(), UserSession = "Auto KNOCK OFF" });
                    }
                }
                #endregion

                #region send email - move to PostActivation Service
                // same filter with reg/newline/supp controller method "PaymentRecived"
                //if (_reg.RegTypeID == 2 || _reg.RegTypeID == 3 || _reg.RegTypeID == 8 || _reg.RegTypeID == 9 ||
                //_reg.RegTypeID == 10 || _reg.RegTypeID == 11 || _reg.RegTypeID == 12 || _reg.RegTypeID == 26)
                //{
                //    if (triggerEmailByCDPU) AutoActivationUtils.TriggerEmail(_reg.ID);
                //}
                #endregion
                
            //}

            return _return;
        }

        private DAL.Models.RegStatus ConstructRegStatus(int regID, string statusCode)
        {
            Access username = DAL.Registration.getuserNameKenan(regID);
            if (ReferenceEquals(username, null))
            {
                username = new Access();
                username.UserName = "KENAN CALLBACK";
            }
            return new DAL.Models.RegStatus()
            {
                RegID = regID,
                StartDate = DateTime.Now,
                StatusID = KenanConfigHelper.GetStatusID(statusCode),
                CreateDT = DateTime.Now,
                Active = true,
                LastAccessID = username.UserName,
            };
        }

		public static string formatValueDecimal_DF(string _inputString)
		{
			if (_inputString.Contains("."))
				return _inputString.Replace(".", string.Empty);
			else
				return string.Format("{0}{1}", _inputString, "00");
		}

        #region #759 - To clean-up iSell transaction type LOV - Lus - *if got changes, please change in WebHelper as well
        public static string getTransactionTypeDisplay(RegistrationSearchResult registrationSearchresult)
        {
            string result = registrationSearchresult.RegTypeDescription;

            //for CRP transaction
            if (registrationSearchresult.RegType == 24)
            {
                if (registrationSearchresult.SMcontractType == "" || registrationSearchresult.SMcontractType == null || (registrationSearchresult.SMcontractType == "T" && registrationSearchresult.PrinOfferId == 0 && registrationSearchresult.CRPType != "2" && registrationSearchresult.CRPType != "4"))
                {
                    //1468 - To rename transaction type LOV for CRP-BPC
                    //result = "CRP-BPC";
                    result = "CRP";
                }
                else if (registrationSearchresult.SMcontractType == "T")
                {
                    if (registrationSearchresult.CRPType == "2")
                        result = registrationSearchresult.hasPrinDevice == "1" ? "CRP-Prin-Supp(D)" : "CRP-Prin-Supp";
                    else if (registrationSearchresult.CRPType == "4")
                        result = registrationSearchresult.hasPrinDevice == "1" ? "CRP-Supp-Prin(D)" : "CRP-Supp-Prin";
                    else
                        result = registrationSearchresult.PenaltyAmount.ToString2() == "0.00" ? "CRP-AC" : "CRP-T&AN";
                }
                return result;
            }

            //for SIM replacement transaction
            if (registrationSearchresult.RegType == 14)
            {
                if (registrationSearchresult.SimReplacementType == "1")
                    result = "SIM Rep-N-N";
                else if (registrationSearchresult.SimReplacementType == "2")
                    result = "SIM Rep-N-M";
                else if (registrationSearchresult.SimReplacementType == "3")
                    result = "SIM Rep-M-M";
                else if (registrationSearchresult.SimReplacementType == "4")
                    result = "SIM Rep-M-N";
                else if (registrationSearchresult.SimReplacementType == "5")
                    result = "SIM Rep-Prepaid";
                else if (registrationSearchresult.SimReplacementType == "6")
                    result = "SIM Rep-HSDPA";
                else if (registrationSearchresult.SimReplacementType == "7")
                    result = "SIM Rep-FWBB";
                if (registrationSearchresult.isThirdParty == "True")
                    result = "(3rd)" + result;

                return result;
            }

            //for Add Contract
            if (registrationSearchresult.RegType == 36)
            {
                if (registrationSearchresult.PenaltyAmount.ToString2() == "0.00")
                    result = result + "-AC";
                else
                    result = result + "-T&AN";

                return result;
            }

            //for SMART transaction
            if (registrationSearchresult.RegType == 2 && registrationSearchresult.CriteriaType == "S")
            {
                result = "SMART";

                if (registrationSearchresult.PenaltyAmount.ToString2() == "0.00")
                    result = result + "-AN";
                else
                    result = result + "-T&AN";

                return result;
            }

            //for add suppline without device
            if (registrationSearchresult.RegType == 8)
            {
                result = registrationSearchresult.SASupp == "True" ? "SA " + result : result;
            }

            //for add suppline with device
            if (registrationSearchresult.RegType == 2 && registrationSearchresult.CRPType == "DSP")
            {
                result = "Supp(D)";

                if (registrationSearchresult.SASupp == "True")
                    result = "SA " + result;
                if (registrationSearchresult.hasPrinAccessory == "1")
                    result = result + "(A)";
                return result;
            }

            //for MNP Newline
            if (registrationSearchresult.RegType == 26)
            {
                result = registrationSearchresult.userType == "N" ? "MNP-M" : "MNP(Line)-M";
                return result;
            }

            //for MNP Supp
            if (registrationSearchresult.RegType == 11)
            {
                result = registrationSearchresult.SASupp == "True" ? "MNP(SA Supp)" : "MNP(Supp)";
                return result;
            }

            //device for Newline transaction - principal
            if (registrationSearchresult.RegType == 9 && registrationSearchresult.hasPrinDevice == "1")
                result = result + "(D)";

            //accessory for principal
            if ((registrationSearchresult.RegType == 2 || registrationSearchresult.RegType == 6 || registrationSearchresult.RegType == 9) && registrationSearchresult.hasPrinAccessory == "1")
                result = result + "(A)";

            //suppline + MISM details
            if (registrationSearchresult.RegType == 2 || registrationSearchresult.RegType == 3 || registrationSearchresult.RegType == 9)
            {
                if (registrationSearchresult.hasSuppline == "1")
                    result = result + " + Supp";
                if (registrationSearchresult.hasSuppDevice == "1")
                    result = result + "(D)";
                if (registrationSearchresult.hasSuppAccessory == "1")
                    result = result + "(A)";
                if (registrationSearchresult.SimType == "MISM")
                    result = result + " + MISM";
            }

            return result;
        }
        #endregion

        #region device financing to determine whether order is created by cdpu user
        public static bool isCDPUorder(int Regid)
        {
            bool result = false;
            string salesperson = string.Empty;

            List<int> Registration = new List<int>();
            Registration.Add(Regid);

            DAL.Models.Registration reg = DAL.Registration.RegistrationGet(Registration).FirstOrDefault();

            if (!ReferenceEquals(reg, null))
            {
                salesperson = reg.SalesPerson;
                bool isActive = true;

                var userRole = OnlineRegAdmin.RolesForUserGet(salesperson, isActive);
                if (!ReferenceEquals(userRole, null) && userRole.Contains("MREG_CUSR"))
                {
                    result = true;
                }
            }
            
            return result;
        }
        #endregion

		public static List<packageListDisconnect> processDisconnectList_DF(DAL.Models.Registration _reg, List<packageListDisconnect> _dcList, List<packageListConnect> _pkgList , List<RegAttributes> _objRegAttributes)
		{
			// only for prin-> supp or supp->prin
			if (_reg.CRPType != "2" && _reg.CRPType != "4")
				return _dcList;

			// BPC only
			if (!string.IsNullOrEmpty(_reg.ArticleID))
				return _dcList;

			List<packageListDisconnect> _newDclist = new List<packageListDisconnect>();
			string df_eligiblePlan = ConfigurationManager.AppSettings["DF_ElgPlanKenanCode"].ToString2();

			bool isDFuser = false;
			bool isDForder = false;
			bool isEligibleTargetPlan = false;

			isDForder = _objRegAttributes.Where(a => a.ATT_Name == "DF_ORDER" && a.SuppLineID == 0).Select(x => x.ATT_Value).FirstOrDefault() != null ? true : false;
			isDFuser = _objRegAttributes.Where(a => a.ATT_Name == "DF_USER" && a.SuppLineID == 0).Select(x => x.ATT_Value).FirstOrDefault() != null ? true : false;
			
			// means existing user with DF, and do for BPC only
			if (isDFuser && !isDForder)
			{
				isEligibleTargetPlan = _pkgList.Where(x => df_eligiblePlan.Contains(x.packageId)).Any();
				foreach (var dcList in _dcList)
				{
					if (isEligibleTargetPlan && dcList.packageId != "41805")
					{
						_newDclist.Add(dcList);
					}
				}
			}
			else
			{
				_newDclist = _dcList;
			}

			if (_newDclist.Count() == 0)
				_newDclist = _dcList;

			return _newDclist;
		}

		/// <summary>
		/// There's some package should not removed for convert supp / convert principal regtype regardless the target plan
		/// If there's any other package introduced, just need to introduce to refLov Table and type "SPC_PKG_CONVERT_NOT_REMOVE"
		/// </summary>
		public static List<packageListDisconnect> processDisconnectList_Convert(DAL.Models.Registration _reg, List<packageListDisconnect> _dcList, List<packageListConnect> _pkgList, List<RegAttributes> _objRegAttributes)
		{
			// only for prin-> supp or supp->prin
			if (_reg.CRPType != "2" && _reg.CRPType != "4")
				return _dcList;

			List<packageListDisconnect> _newDclist = new List<packageListDisconnect>();
			var specialPkgCompList = OnlineRegAdmin.GetLovList("SPC_PKG_CONVERT_NOT_REMOVE");
			List<string> specialPackageList = new List<string>();
			if (specialPkgCompList != null && specialPkgCompList.Any())
				specialPackageList = specialPkgCompList.Select(x => x.Value).ToList();

			if (_newDclist.Count() == 0)
				_newDclist = _dcList;

			for (int i = 0; i < _newDclist.Count; i++)
			{
				if (specialPackageList.Contains(_newDclist[i].packageId))
				{
					int index = _newDclist.IndexOf(_newDclist[i]);
					if (index >= 0)
					{
						_newDclist.RemoveAt(index);
					}
				}
			}

			return _newDclist;
		}


	
	}
}
