﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace Online.Registration.IntegrationService.Helper
{
    public class ServiceProxy<TClient> : IDisposable where TClient : class, ICommunicationObject
    {
        public TClient Client { get; set; }

        public ServiceProxy()
        {
            Client = default(TClient);
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (this.Client == null)
                return;

            switch (this.Client.State)
            {
                case CommunicationState.Opened:
                    this.Client.Close();
                    break;

                case CommunicationState.Faulted:
                    Client.Abort();
                    break;
            }

            this.Client = null;
        }

        #endregion
    }
}