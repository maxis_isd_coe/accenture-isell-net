﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Online.Registration.IntegrationService.Helper;
using Online.Registration.DAL;
using SNT.Utility;
using System.IO;
using System.Web;
using Online.Registration.IntegrationService.Properties;
using Online.Registration.DAL.Models;
using System.Data;
using System.Data.SqlClient;
using Online.Registration.IntegrationService.IContractUploadWS;
using System.Configuration;
using log4net;
using Online.Registration.DAL.Admin;

namespace Online.Registration.IntegrationService.Helper
{
	public class iContractHelper
	{
		private static readonly ILog Logger = LogManager.GetLogger(typeof(WebPOSCallBackService));
		#region iContract

		public static POSStatusResult sendDocsToIcontract(int regID, bool singleDoc = false)
		{
            //30042015 - Anthony - Send document to iContract for non-Drop 4 flow
            //if (!Util.rollOutCheck(regID))
            //{
            //    return new POSStatusResult() { MsgCode = "0", MsgDesc = "sendDocsToIcontract turned off for this store" };
            //}

			Logger.Debug(string.Format("sendDocsToIcontract Start for {0}", regID));
			try
			{

				DAL.Models.Registration primeReg = DAL.Registration.GetRegistrationDetails(regID);

				Logger.Debug("Query result for regDet = " + XMLHelper.ConvertObjectToXml(primeReg));
				List<DAL.Models.RegUploadDoc> regUpload = DAL.Registration.regUploadGet(regID);
                var allDocumentsSent =  regUpload.Any() && !regUpload.Where(x => string.IsNullOrEmpty(x.Status)).Any();
                if (allDocumentsSent)
                    return new POSStatusResult() { MsgCode = "0", MsgDesc = "All Document already sent" };
                
				List<DAL.Models.RegType> regtypeAll = DAL.Registration.regTypeAll();
				List<RegSuppLine> Suplines = new List<RegSuppLine>();
				Logger.Debug("Query result for iContract = " + XMLHelper.ConvertObjectToXml(regUpload));

				List<int> orgInt = new List<int>();
				orgInt.Add(primeReg.CenterOrgID);
				List<DAL.Models.Organization> OrgDet = DAL.Admin.OnlineRegAdmin.OrganizationGet(orgInt);
				Logger.Debug("OrgDet = " + XMLHelper.ConvertObjectToXml(OrgDet));

				string ICFrontPath = primeReg.CustomerPhoto;
				string ICBackPath = primeReg.AltCustomerPhoto;
				string OtherDocsPath = primeReg.Photo;
				string result = string.Empty;
				//string ContractFolderPath = Path.GetDirectoryName(primeReg.CustomerPhoto);//30042015 - Anthony - remove unused variable, because it breaks the function for non-Drop 4 flow
				string MSISDN = String.Empty;
                string custIDNO = string.Empty;
                string idCardTypeDec = string.Empty;
                string IdcardDocType = string.Empty;
                int idCardTypeID = 0;
                //string ContractFilepath = ContractFolderPath + @"\" + regID + @".pdf";//30042015 - Anthony - remove unused variable, because it breaks the function for non-Drop 4 flow
				var accountNumber = primeReg.KenanAccountNo;
                
                var regType = regtypeAll.Where(x => x.ID == primeReg.RegTypeID).SingleOrDefault().Description;
              
                List<int> custInt = new List<int>();
                custInt.Add(primeReg.CustomerID);
                List<DAL.Models.Customer> cust = DAL.Registration.CustomerGet(custInt);
                if (!ReferenceEquals(cust, null) && cust.Count() > 0)
                {
                    custIDNO = cust.SingleOrDefault().IDCardNo.ToString2();
                }
                List<int> idCardType = new List<int>();
                idCardType.Add(cust[0].IDCardTypeID);
                List<IDCardType> idCardTypes = DAL.Admin.OnlineRegAdmin.IDCardTypeGet(idCardType);
                if (!ReferenceEquals(idCardTypes, null) && idCardTypes.Count() > 0)
                {
                    idCardTypeDec = idCardTypes.SingleOrDefault().Description;
                    idCardTypeID = idCardTypes.SingleOrDefault().ID;
                }

                MSISDN = primeReg.MSISDN1;
                Logger.Info("IDcard Details  ? " + idCardTypeDec.ToString2());
                Logger.Info("IDcard Type  ? " + custIDNO.ToString2());
				var suppLineIDs = DAL.Registration.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = primeReg.ID } }).ToList();
				Logger.Info("How many supps ? " + suppLineIDs.Count);
				if (suppLineIDs.Count() > 0)
				{
					var regSuppLinesList = DAL.Registration.RegSuppLineGet(suppLineIDs).ToList();
					Suplines = regSuppLinesList;
				}
				if (Suplines.Count > 0)
				{
					foreach (var _suppline in Suplines)
					{
						MSISDN += "," + _suppline.MSISDN1;
					}
				}
                //Manoj - Added since there was no document sent the Passport list in Icontract. 
                if (idCardTypeID == 1 || idCardTypeID == 3)
                { IdcardDocType = "IC"; }
                else if (idCardTypeID == 2)
                { IdcardDocType = "Passport"; }
                else if (idCardTypeID == 4)
                {  IdcardDocType = "other_ID"; }

                    if (!ReferenceEquals(regUpload, null) && regUpload.Count() > 0)
                    {
                        Logger.Info("How many file ? " + regUpload.Count());
                        Logger.Info("Details = " + XMLHelper.ConvertObjectToXml(regUpload));

						string storeName = string.Format("{0}({1})", OrgDet[0].Name, OrgDet[0].DealerCode);
						string salesAgentCode = string.Empty;
						string kenanAgentCode = string.Empty;

						var access = DAL.Admin.OnlineRegAdmin.AccessGetbyUserName(primeReg.SalesPerson);
						var salesChannelIDs = DAL.Registration.getAllSalesChannelIDs();

						try
						{
							bool salesAgentCodeExist = salesChannelIDs.Where(x => x.SalesChannelID == access.kenanAgentCode).Any();

							if (salesAgentCodeExist)
							{
								kenanAgentCode = salesChannelIDs.Where(x => x.SalesChannelID == access.kenanAgentCode).FirstOrDefault().AgentCode;
							}
						}
						catch(Exception ex)
						{
							kenanAgentCode = "";
						}

						salesAgentCode = !string.IsNullOrWhiteSpace(kenanAgentCode) ?
							string.Format("{0}({1})", primeReg.SalesPerson, kenanAgentCode) : primeReg.SalesPerson;
                        iContract iContract = new iContract();

                        if (singleDoc)
                        {
                            #region send single file in tblreguploaddoc - Drop 5
                            Logger.Info(string.Format("prepare single doc"));
                            //var SingleFileName = ConfigurationManager.AppSettings["RFDocFileName"].ToString2();
                            //var doc = regUpload.Where(x => x.FileType == "Contract" && (x.FileName == SingleFileName || x.FileName == "RFDocs.pdf"));
							var doc = regUpload.Where(x => x.FileType == "Contract" && (x.FileName.Contains("RFDocs")));
                            if (doc.Any())
                            {
                                var SelectedDoc = doc.FirstOrDefault();
                                result = iContract.UploadFiletoIcontract("Contract", regID.ToString2(), custIDNO.ToString2(), MSISDN, accountNumber, regType.ToString2(), idCardTypeDec.ToString2(), storeName, SelectedDoc.FileStream, salesAgentCode, SelectedDoc.FileName);
                                if (!string.IsNullOrEmpty(result))
                                {
                                    DAL.Models.RegUploadDoc obj = new DAL.Models.RegUploadDoc() { ID = SelectedDoc.ID};
                                    DAL.Registration.regUploadUpdate(obj, result);
                                    string responseForLog = string.Format("Single Document name {0},type {1} uploaded successfully", SelectedDoc.FileName, SelectedDoc.FileType);
                                    AutoActivationUtils.LogToDB("iContractHelper", "sendDocsToIcontractSingle", "", responseForLog, "", 0, SelectedDoc.RegID.ToString2());
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region send all doc in tblreguploaddoc
                            foreach (var doc in regUpload)
                            {
                                Logger.Info(string.Format("lopping for doc {0}-{1}", doc.FileType, doc.FileName));
                                
                                //Doctype, regId, IDcardValue, MSISDN, AccNo, trnType, StoreID, file, agentCode, fileName)
                                if (string.IsNullOrEmpty(doc.Status))
                                {
									// this is for recovery, if when retrigger still in html format
									if (doc.FileType == "Contract" && doc.FileName.EndsWith(".html"))
									{
										iContractHelper.getKenanAccountNo(regID);
									}

                                    switch (doc.FileType)
                                    {//23122014 - Anthony - As per discussed with Manoj, the Store ID is changed to Store Name (as per requirement in IFA)
                                        case "IC":
                                            result = iContract.UploadFiletoIcontract(IdcardDocType.ToString2(), regID.ToString2(), custIDNO.ToString2(), MSISDN, accountNumber, regType.ToString2(), idCardTypeDec.ToString2(), storeName, doc.FileStream, salesAgentCode, doc.FileName);
                                            break;

                                        case "Other":
                                            result = iContract.UploadFiletoIcontract("others", regID.ToString2(), custIDNO.ToString2(), MSISDN, accountNumber, regType.ToString2(), idCardTypeDec.ToString2(), storeName, doc.FileStream, salesAgentCode, doc.FileName);
                                            break;

                                        case "Contract":
                                            result = iContract.UploadFiletoIcontract("Contract", regID.ToString2(), custIDNO.ToString2(), MSISDN, accountNumber, regType.ToString2(), idCardTypeDec.ToString2(), storeName, doc.FileStream, salesAgentCode, doc.FileName);
                                            break;
                                    }
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        DAL.Models.RegUploadDoc obj = new DAL.Models.RegUploadDoc();
                                        obj.ID = doc.ID;
                                        DAL.Registration.regUploadUpdate(obj, result);
                                        string responseForLog = string.Format("Document name {0},type {1} uploaded successfully", doc.FileName, doc.FileType);
                                        AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "iContract", "", responseForLog, "", 0, doc.RegID.ToString2());
                                    }
                                }
                            }
                            #endregion
                        }
                    }
				return new POSStatusResult() { MsgCode = "0", MsgDesc = "send contract successfully" };
			}
			catch (Exception ex)
			{
				Logger.Error("ICONTRACT ERROR ? " + Util.LogException(ex));
				string responseForLog = string.Format("Some document failed to upload for regID {0}, exception{1}", regID, ex.ToString2());
				AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "iContract", "", responseForLog, "", 0, regID.ToString2());
				return new POSStatusResult() { MsgCode = "1", MsgDesc = ex.ToString2() };
			}
		}

		#endregion


        #region insert kenanaccountno into file upload - Samuel

        public static void getKenanAccountNo(int regID)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            Logger.InfoFormat("baseDirectory: {0}, {1}", regID, baseDirectory);
            int outValue;
            try
            {
                DAL.Models.Registration primeReg = DAL.Registration.GetRegistrationDetails(regID);
                String applicationVirtualPath = "[Image_replace]/";
                Logger.InfoFormat("applicationVirtualPath: {0}, {1}", regID, applicationVirtualPath);
                var accountNumber = primeReg.KenanAccountNo;

                List<DAL.Models.RegUploadDoc> regUpload = DAL.Registration.regUploadGet(regID);

                if (regUpload != null)
                {
                    if (regUpload.Where(a => a.FileType == "Contract" && a.FileName == regID + ".html").Any())
                    {
                        var objectDoc = regUpload.Where(a => a.FileType == "Contract" && a.FileName == regID + ".html").FirstOrDefault();

                        string HTMLSource = System.Text.Encoding.ASCII.GetString(objectDoc.FileStream);
                        string correctHtml = HTMLSource.Replace("[KenanAccNo]", accountNumber);

                        if (baseDirectory != null)
                        {
                            baseDirectory = baseDirectory.Replace("\\", "/");
                            Logger.InfoFormat("exiting baseDirectory replace: {0}, baseDirectory: {1}", regID, baseDirectory);
                        }

                        if (applicationVirtualPath.EndsWith("/"))
                        {
                            correctHtml = correctHtml.Replace(applicationVirtualPath + "Content/images/maxislogo.png", baseDirectory + "Content/images/maxislogo.png");
                            correctHtml = correctHtml.Replace(applicationVirtualPath + "Content/images/maxis_use_only_s.png", baseDirectory + "Content/images/maxis_use_only_s.png");
                            Logger.InfoFormat("exiting applicationVirtualPath replace: {0}, correctHtml{1}", regID, correctHtml );
                        }

                        byte[] PDFBytes = (new NReco.PdfGenerator.HtmlToPdfConverter()).GeneratePdf(correctHtml);
                        try
                        {
                            RegUploadDoc objRegUploadDoc = new RegUploadDoc();
                            objRegUploadDoc.ID = objectDoc.ID;
                            objRegUploadDoc.RegID = regID;
                            outValue = DAL.Registration.regUploadUpdateFile(objRegUploadDoc, regID + ".pdf", PDFBytes);
                        }
                        catch (Exception ex)
                        {
                            outValue = -1;
                            Logger.Error(string.Format("ErrorMessage regUploadUpdateFile: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage getKenanAccountNo: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
        }
        #endregion


	}
}
