﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.DAL.Admin;

namespace Online.Registration.IntegrationService.Helper
{
    public class KenanConfigHelper
    {
        public static int GetGender(string tcmGender)
        {
            switch (tcmGender)
            {
                case "M":
                    return 1;

                case "F":
                    return 2;

                default:
                    return 0;
            }
        }

        public static int GetMarketCodeID(int tcmMarketCodeID)
        {
            if (tcmMarketCodeID <= 0)
                return 0;
            
            var marketCode = OnlineRegAdmin.MarketGet(new List<int>() { tcmMarketCodeID }).Single();

            return int.Parse(marketCode.KenanCode);
        }

        public static int GetAccCategoryID(int tcmAccCategoryID)
        {
            if (tcmAccCategoryID <= 0)
                return 0;

            var AccCategory = OnlineRegAdmin.AccountCategoryGet(new List<int>() { tcmAccCategoryID }).Single();

            return int.Parse(AccCategory.KenanCode);
        }

        public static int GetPayModeID(int tcmPayModeID)
        {
            if (tcmPayModeID <= 0)
                return 0;

            var PayMode = OnlineRegAdmin.PaymentModeGet(new List<int>() { tcmPayModeID }).Single();

            return int.Parse(PayMode.KenanCode);
        }

        public static int GetVIPCodeID(int tcmVIPCodeID)
        {
            if (tcmVIPCodeID <= 0)
                return 0;

            var VIPCode = OnlineRegAdmin.VIPCodeGet(new List<int>() { tcmVIPCodeID }).Single();

            return int.Parse(VIPCode.KenanCode);
        }

        public static int GetExtIDTypeID(int tcmExtIDTypeID)
        {
            if (tcmExtIDTypeID <= 0)
                return 0;

            var ExtIDType = OnlineRegAdmin.ExtIDTypeGet(new List<int>() { tcmExtIDTypeID }).Single();

            return int.Parse(ExtIDType.KenanCode);
        }
        public static string GetExtIDTypeID(string code)
        {
            if (string.IsNullOrEmpty(code))
                return "";

            var ExtIDTypeID = OnlineRegAdmin.ExtIDTypeFind(new DAL.Models.ExtIDTypeFind()
            {
                ExtIDType = new DAL.Models.ExtIDType()
                {
                    Code = code
                }
            }).SingleOrDefault();

            var ExtIDType = OnlineRegAdmin.ExtIDTypeGet(new int[] { ExtIDTypeID }.ToList()).SingleOrDefault();

            return ExtIDType.KenanCode;
        }

        public static int GetIDCardTypeID(int tcmIDCardTypeID)
        {
            if (tcmIDCardTypeID <= 0)
                return 0;

            var IDCardType = OnlineRegAdmin.IDCardTypeGet(new List<int>() { tcmIDCardTypeID }).Single();

            return int.Parse(IDCardType.KenanCode);
        }

        public static int GetRaceID(int tcmRaceID)
        {
            if (tcmRaceID <= 0)
                return 0;

            var Race = OnlineRegAdmin.RaceGet(new List<int>() { tcmRaceID }).Single();

            return int.Parse(Race.KenanCode);
        }

        public static int GetNationalityID(int tcmNationalityID)
        {
            if (tcmNationalityID <= 0)
                return 0;

            var Nationality = OnlineRegAdmin.NationalityGet(new List<int>() { tcmNationalityID }).Single();

            if (Nationality.Code == Properties.Settings.Default.Nationality_Malaysian)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }

        public static int GetStatusID(string statusCode)
        {
            if (string.IsNullOrEmpty(statusCode))
                return 0;

            var statusID = OnlineRegAdmin.StatusFind(new DAL.Models.StatusFind()
            {
                Status = new DAL.Models.Status() { Code = statusCode }
            }).Single();

            return statusID;
        }

        public static string GetTitleName(int tcmTitleID)
        {
            if (tcmTitleID <= 0)
                return "";

            var Title = OnlineRegAdmin.CustomerTitleGet(new List<int>() { tcmTitleID }).Single();

            return Title.Name;
        }

        public static string GetStateName(int tcmStateID)
        {
            if (tcmStateID <= 0)
                return "";

            var State = OnlineRegAdmin.StateGet(new List<int>() { tcmStateID }).Single();

            return State.Name;
        }

        public static string GetCountryName(int tcmCountryID)
        {
            if (tcmCountryID <= 0)
                return "";

            var Country = OnlineRegAdmin.CountryGet(new List<int>() { tcmCountryID }).Single();

            return Country.Name;
        }
    }
}