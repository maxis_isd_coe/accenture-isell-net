﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using System.IO;

namespace Online.Registration.IntegrationService.Helper
{
    public static class XMLHelper
    {
        public static DataSet ConvertObjectToDataSet(object obj)
        {
            DataSet ds = new DataSet();

            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            System.IO.StringWriter writer = new System.IO.StringWriter();
            xmlSerializer.Serialize(writer, obj);
            System.IO.StringReader reader = new System.IO.StringReader(writer.ToString());
            ds.ReadXml(reader);
            return ds;
        }
        public static object ConvertXmlFileToObject(string strFile, Type objType)
        {
            object objInput = null;
            string xmlString = ConvertXmlFileToXmlString(System.Web.HttpContext.Current.Server.MapPath(strFile));
            System.Xml.Serialization.XmlSerializer objSer = new System.Xml.Serialization.XmlSerializer(objType);
            System.IO.StringReader objStringWriter = new System.IO.StringReader(xmlString);

            try
            {
                objInput = objSer.Deserialize(objStringWriter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objStringWriter.Close();
            }
            return objInput;
        }
        public static object ConvertXmlToObject(string strXml, Type objType)
        {
            object objInput = null;
            //bool blnHasError = false;

            System.Xml.Serialization.XmlSerializer objSer = new System.Xml.Serialization.XmlSerializer(objType);
            System.IO.StringReader objStringWriter = new System.IO.StringReader(strXml);

            try
            {
                objInput = objSer.Deserialize(objStringWriter);
            }
            catch (Exception ex)
            {
                //if (ex.Source == "System.Xml" & ex is System.InvalidOperationException)
                //{
                //    blnHasError = true;
                //}
                //else
                //{
                //    throw ex;
                //}
                throw ex;
            }
            finally
            {
                objStringWriter.Close();
            }

            //if (blnHasError)
            //{
            //    try
            //    {
            //        objSer = new System.Xml.Serialization.XmlSerializer(objError.GetType());
            //        objStringWriter = new System.IO.StringReader(strXml);
            //        objInput = objSer.Deserialize(objStringWriter);
            //    }
            //    catch (Exception ex)
            //    {
            //        throw ex;
            //    }
            //    finally
            //    {
            //        objStringWriter.Close();
            //    }
            //}

            return objInput;
        }

        public static string ConvertObjectToXml(object obj, Type typ)
        {
            string xml = string.Empty;
            try
            {
                System.Xml.Serialization.XmlSerializer xSerializer = new System.Xml.Serialization.XmlSerializer(typ);
                System.IO.TextWriter stringWriter = new System.IO.StringWriter();
                xSerializer.Serialize(stringWriter, obj);
                xml = stringWriter.ToString();
                stringWriter.Flush();
                stringWriter.Close();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return xml;
        }
        public static string ConvertObjectToXml(object objInput)
        {
            System.Xml.Serialization.XmlSerializer objSer = new System.Xml.Serialization.XmlSerializer(objInput.GetType());
            System.IO.StringWriter objStringWriter = new System.IO.StringWriter();
            try
            {
                objSer.Serialize(objStringWriter, objInput);
                return objStringWriter.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objStringWriter.Close();
            }
        }

        public static string ConvertXmlFileToXmlString(string strFile)
        {
          // Load the xml file into XmlDocument object.
          XmlDocument xmlDoc = new XmlDocument();
          try
          {
            xmlDoc.Load(strFile);
          }
          catch (XmlException e)
          {
            Console.WriteLine(e.Message);
          }
          // Now create StringWriter object to get data from xml document.
          StringWriter sw = new StringWriter();
          XmlTextWriter xw = new XmlTextWriter(sw);
          xmlDoc.WriteTo(xw);
          return sw.ToString();
        }
    }
}

