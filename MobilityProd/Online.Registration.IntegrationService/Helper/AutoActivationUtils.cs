﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.DAL;
using Online.Registration.IntegrationService;
using Online.Registration.IntegrationService.Models;
using log4net;
using SNT.Utility;
using System.Configuration;
using System.IO;
using Online.Registration.IntegrationService.Properties;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Admin;
using Online.Registration;


namespace Online.Registration.IntegrationService.Helper
{

    
    

    public class AutoActivationUtils
    {
        public enum SimReplacementType
        {
            N2N = 1,
            N2M = 2,
            M2M = 3,
            M2N = 4,
            PREPAID = 5
        }

        public enum RefType
        {
            AddressType,
            AccountCategory,
            Bank,
            Brand,
            Bundle,
            CardType,
            Category,
            Colour,
            ComplaintIssues,
            Component,
            ComponentType,
            Country,
            CustomerTitle,
            DeviceProperty,
            ExternalIDType,
            IDCardType,
            IssueGrpDispatchQ,
            Language,
            Nationality,
            Market,
            Model,
            Organization,
            OrgType,
            Package,
            PackageType,
            PaymentMode,
            PgmBdlPkgComp,
            PlanProperty,
            Program,
            Property,
            PropertyValues,
            Race,
            RegType,
            State,
            Status,
            StatusType,
            StatusReason,
            UserGroup,
            User,
            VIPCode,
            Access,
            UserRole,
            ModelGroup,
            CustomerInfoPaymentMode,
            CustomerInfoCardType,
            Doners,
            SimModelType,
            SimReplacementReason,
            SimReplacementprePaidReason,
            Msisdn,
            RestrictKeananComponent,
            IdCardTypes,
            SIMCardType,
            AllSimModelType
        }

        static readonly ILog Logger = LogManager.GetLogger(typeof(KenanService));
        static readonly ILog EBPSLogger = LogManager.GetLogger(typeof(EBPSService));

        public static int GetRegType(string regId)
        {
            //DataSet dsRegType = DataAccess.GetDataset("usp_GetRegtype", regId);
            int regTypeId = Registration.DAL.Registration.GetRegtype(Convert.ToInt32(regId));
            return regTypeId;
        }

        public static int GetRegStatus(int regId)
        {
            RegStatusFind objRegStatusFind=new RegStatusFind ();
            objRegStatusFind.Active=true;
            objRegStatusFind.RegistrationIDs = new List<int>(){regId};
            List<RegStatus> regStatusId = Registration.DAL.Registration.RegStatusGet1(objRegStatusFind);
            if (regStatusId != null && regStatusId.Any())
            {
                return regStatusId[0].StatusID;
            }
            else
            {
                return 0;
            }
        }

        public static bool PlanOnly(string regId)
        {
            bool result = false;

            KenanService _client = new KenanService();
            //KenanServiceClient _client = new KenanServiceClient();
            result = _client.KenanAccountFulfill(new Models.OrderFulfillRequest()
            {
                OrderID = regId,
                UserSession = "klccccc",
            });

            return result;
        }
        public static bool PlanPlusDevice(string regId)
        {
            bool result = false;

            KenanService _client = new KenanService();
            //KenanServiceClient _client = new KenanServiceClient();
            result = _client.KenanAccountFulfill(new Models.OrderFulfillRequest()
            {
                OrderID = regId,
                UserSession = "",
            });

            return result;
        }
        public static bool NewLine(string regId)
        {
            bool result = false;
            var response = FulfillKenanAccountForNewLine(Convert.ToInt32(regId));
            if ((response.Message == "success") && (response.Code == "0"))
                result = true;

            return result;
        }

        public static bool SuppLine(string regId)
        {
            bool result = false;
            bool isSuppAsNew = DAL.Registration.getSupplineDetails(Convert.ToInt32(regId));
            if (isSuppAsNew)
            {
                KenanService _client = new KenanService();
                result = _client.KenanAccountFulfill(new Models.OrderFulfillRequest()
                {
                    OrderID = regId,
                    UserSession = "",
                });
            }
            else
            {
                var response = FulfillKenanAccountForSuppLine(Convert.ToInt32(regId));
                if ((response.Message == "success") && (response.Code == "0"))
                    result = true;
            }
            return result;
        }
        public static bool AddRemoveVas(string regId)
        {
            bool result = false;
            KenanService service = new KenanService();
            result = service.CenterOrderContractCreation(new CenterOrderContractRequest() { orderId = regId, });

            return result;
        }
        public static bool SimReplacement(string regId)
        {
            bool result = false;
            KenanService service = new KenanService();
            Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
            objRegDetails = DAL.Registration.GetLnkRegistrationDetails(Convert.ToInt32(regId));
            //condition 1 : n2m
            if (!ReferenceEquals(objRegDetails,null) && objRegDetails.SimReplacementType == (int)SimReplacementType.N2M)
            {
                result = FrameSimReplacementSecondaryDetailsAndProcesstoKenan(Convert.ToInt32(regId));
                //
                if (result)
                {
                    //in case of n2m if lines had added, then we should break this logic here, as the kenan.svc methods will take care of fullfillorder.
                    return result;
                }
                else if (!result)
                    return result;
            }
            /**/
            //condition 2 : m2n
            if (!ReferenceEquals(objRegDetails, null) && objRegDetails.SimReplacementType > 0 && objRegDetails.SimReplacementType == (int)SimReplacementType.M2N)
            {
                result = FrameSimReplacementComponentsDisconnectList(Convert.ToInt32(regId));
                if (!result)
                    return result;
            }
            //condition 3 : n-n and m-m
            Online.Registration.IntegrationService.MaxisOrderProcessWs.eaiResponseType response = service.SwapCenterInventory(Convert.ToInt32(regId));
            if (!string.IsNullOrEmpty(response.msgCode))
            {
                if (int.Parse(response.msgCode) > 0)
                { result = false; }
                else { result = true; }
            }
            return result;
        }
        public static bool CRPPlan(string regId)
        {
            bool result = false;
            KenanService service = new KenanService();
            result = service.CenterOrderContractCreationForCRP(new CenterOrderContractRequest() { orderId = regId, });

            return result;
        }
        public static bool CRPDevice(string regId)
        {
            bool result = false;           
            KenanService service = new KenanService();
            result = service.CenterOrderContractCreation(new CenterOrderContractRequest() { orderId = regId, UserName = "Auto Activation API", });
            if (result == true)
            {
                DAL.Registration.RegStatusCreate(new DAL.Models.RegStatus()
                {
                    RegID = regId.ToInt(),
                    StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_SvcPendingActivate),
                    StartDate = DateTime.Now,
                    CreateDT = DateTime.Now,
                    Active = true,
                    LastAccessID = RegStatus.CB_POS
                });
            }
            return result;
        }
        public static bool MNP(string regId)
        {
            bool result = false;
            KenanService service = new KenanService();
            result = service.KenanMNPAccountFulfill(new MNPtypeFullfillCenterOrderMNP() { orderId = regId, });

            return result;
        }
        public static bool MNPMultiPort(string regId)
        {
            bool result = false;
            return result;
        }
        public static bool AddContract(string regId)
        {
            bool result = false;
            KenanService service = new KenanService();

            bool isCreated = false;

            CenterOrderContractRequest Req = new CenterOrderContractRequest();
            Req.orderId = regId;
            Req.UserName = "Auto Activation API";

            result = service.CenterOrderContractCreation(Req);

            DAL.Registration.RegistrationCancel(new DAL.Models.RegStatus()
            {
                RegID = Convert.ToInt32(regId),
                Active = true,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = "SYSTEM",
                StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_SvcPendingActivate)
            });

            return result;
        }
        public static bool Smart(string regId)
        {
            bool result = false;

            KenanService _client = new KenanService();

            DAL.Registration.RegistrationCancel(new DAL.Models.RegStatus()
                        {
                            RegID = Convert.ToInt32(regId),
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = "SYSTEM",
                            StatusID = 24,//Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                        });


            result = _client.CenterOrderContractCreation(new Models.CenterOrderContractRequest()
            {
                orderId = regId,
                UserName = "",
            });
            return result;
        }




        public static KenanTypeAdditionLineRegistrationResponse FulfillKenanAccountForNewLine(int regID)
        {
            //var result = "";
            var reg = new DAL.Models.Registration();
            var cust = new DAL.Models.Customer();
            var billAddr = new DAL.Models.Address();
            var response = new KenanTypeAdditionLineRegistrationResponse();
            MNPOrderCreationResponse rs = new MNPOrderCreationResponse();
            int? GeneratedID = null;
            //HomeController myHomeControl = new HomeController();

            try
            {

                //using (var proxy = new RegistrationServiceProxy())
                {
                    List<int> regids = new List<int>(); regids.Add(regID);
                    reg = DAL.Registration.RegistrationGet(regids).First();  //proxy.RegistrationGet(regID);
                    List<int> custIds = new List<int>(); custIds.Add(reg.CustomerID);
                    cust = DAL.Registration.CustomerGet(custIds).SingleOrDefault();   // (new int[] { reg.CustomerID }).SingleOrDefault();

                    var billAddrID = DAL.Registration.AddressFind(new DAL.Models.AddressFind()
                    {
                        Address = new DAL.Models.Address()
                        {
                            RegID = reg.ID,
                            AddressTypeID = DAL.Admin.OnlineRegAdmin.AddressTypeList().Where(a => a.Code == "AddressType_Billing").Select(a => a.ID).FirstOrDefault(), //Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Billing)
                        }
                    }).SingleOrDefault();

                    List<int> billIds = new List<int>(); billIds.Add(billAddrID);
                    billAddr = DAL.Registration.AddressGet(billIds).SingleOrDefault();  //proxy.RegAddressGet(new int[] { billAddrID }).SingleOrDefault();
                }

                var extid = new List<KenanTypeExternalId>();
                var extidnew = new List<KenanTypeExternalId>();
                extidnew.Add(new KenanTypeExternalId() { ExternalId = string.Empty, ExternalIdType = string.Empty });
                extid.Add(new KenanTypeExternalId() { ExternalId = (reg.externalId == null ? "" : reg.externalId), ExternalIdType = (reg.AccExternalID == null ? "" : reg.AccExternalID) });

                var request = new KenanTypeAdditionLineRegistration()
                {
                    OrderId = regID.ToString(),
                    FxAcctNo = (reg.fxAcctNo == null) ? "" : reg.fxAcctNo,
                    ExternalId = (extid.ToList().ToArray() == null) ? extidnew.ToList().ToArray() : extid.ToList().ToArray(),
                };

                KenanService service = new KenanService();
                response = service.KenanAdditionLineRegistration(request); //proxy.KenanAdditionLineRegistration(request);

            }
            catch (Exception ex)
            {

            }
            return response;

        }
        public static KenanNewSuppLineResponse FulfillKenanAccountForSuppLine(int regID)
        {
            //var result = "";
            var reg = new DAL.Models.Registration();
            var cust = new DAL.Models.Customer();
            var billAddr = new DAL.Models.Address();
            var response = new KenanNewSuppLineResponse();

            MNPOrderCreationResponse rs = new MNPOrderCreationResponse();

            try
            {
                var extid = new List<KenanTypeExternalId>();
                var extidnew = new List<KenanTypeExternalId>();
                extidnew.Add(new KenanTypeExternalId() { ExternalId = string.Empty, ExternalIdType = string.Empty });



                //using (var proxy = new RegistrationServiceProxy())
                {
                    List<int> regids = new List<int>(); regids.Add(regID);
                    reg = DAL.Registration.RegistrationGet(regids).First();  //proxy.RegistrationGet(regID);
                    List<int> custIds = new List<int>(); custIds.Add(reg.CustomerID);
                    cust = DAL.Registration.CustomerGet(custIds).SingleOrDefault();   // (new int[] { reg.CustomerID }).SingleOrDefault();

                    var billAddrID = DAL.Registration.AddressFind(new DAL.Models.AddressFind()
                    {
                        Address = new DAL.Models.Address()
                        {
                            RegID = reg.ID,
                            AddressTypeID = DAL.Admin.OnlineRegAdmin.AddressTypeList().Where(a => a.Code == "AddressType_Billing").Select(a => a.ID).FirstOrDefault(), //Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Billing)
                        }
                    }).SingleOrDefault();

                    List<int> billIds = new List<int>(); billIds.Add(billAddrID);
                    billAddr = DAL.Registration.AddressGet(billIds).SingleOrDefault();  //proxy.RegAddressGet(new int[] { billAddrID }).SingleOrDefault();

                }
                extid.Add(new KenanTypeExternalId() { ExternalId = (reg.externalId == null ? "" : reg.externalId), ExternalIdType = (reg.AccExternalID == null ? "" : reg.AccExternalID) });

                var request = new KenanNewSuppLineRequest()
                {

                    OrderId = regID.ToString(),
                    //suppLine = WebHelper.Instance.ConstructSuppPackages(reg.MSISDN1),
                    suppLine = new KenanSuppLine() { suppMsisdn = reg.MSISDN1, suppPackageId = "", suppComponentConnReason = "", suppPackageConnReason = "", suppComponentId = "", actionId = "1" }, //WebHelper.Instance.ConstructSuppPackages(reg.MSISDN1),
                    FxAcctNo = reg.fxAcctNo,
                    ExternalId = (extid.ToList().ToArray() == null) ? extidnew.ToList().ToArray() : extid.ToList().ToArray(),
                    //prinLine = WebHelper.Instance.ConstructPrinPackages(reg.externalId)
                    prinLine = new KenanPrinLine() { prinMsisdn = reg.externalId, prinSubscrNo = "", prinSubscrNoResets = "", prinPackageId = "", prinComponentConnReason = "", prinPackageConnReason = "", prinComponentId = "", } //WebHelper.Instance.ConstructPrinPackages(reg.externalId)
                };

                var secResponse = new KenanTypeAdditionLineRegistrationResponse();

                string preOrderID = "ISELLMISM";
                var secRequest = new KenanTypeAdditionLineRegistration()
                {

                    OrderId = preOrderID + regID,
                    FxAcctNo = (reg.fxAcctNo == null) ? "" : reg.fxAcctNo,
                    ExternalId = (extid.ToList().ToArray() == null) ? extidnew.ToList().ToArray() : extid.ToList().ToArray(),
                };

                KenanService service = new KenanService();
                response = service.KenanAddNewSuppLine(request);

                //using (var proxy = new KenanServiceProxy())
                //{
                //    if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                //    {

                //        rs = proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
                //        {
                //            orderId = regID.ToString(),
                //            externalId = reg.MSISDN1,
                //            extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
                //            Customer = WebHelper.Instance.ConstructKenanCustomerMNP(cust, billAddr),
                //            Account = WebHelper.Instance.ConstructKenanAccountMNP(cust),
                //            UserName = reg.SalesPerson,
                //        });
                //    }
                //    else
                //    {
                //        if (reg.RegTypeID == (int)MobileRegType.MNPSuppPlan)
                //        {
                //            rs = proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
                //            {
                //                orderId = regID.ToString(),
                //                externalId = reg.MSISDN1,
                //                extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
                //                Customer = WebHelper.Instance.ConstructKenanCustomerMNP(cust, billAddr),
                //                Account = WebHelper.Instance.ConstructKenanAccountMNP(cust),
                //                UserName = reg.SalesPerson,
                //            });
                //        }
                //        else if (reg.RegTypeID == (int)MobileRegType.SecPlan)
                //        {
                //            secResponse = proxy.KenanAdditionLineRegistrationMISMSecondary(secRequest);
                //            if (!ReferenceEquals(secResponse, null))
                //            {
                //                response.Message = secResponse.Message;
                //                response.Success = secResponse.Success;
                //                response.Code = secResponse.Code;
                //            }
                //            return response;
                //        }
                //        else
                //        {
                //            response = proxy.KenanAddNewSuppLine(request);
                //            return response;
                //        }


                //    }
                //    /*Added by narayanareddy*/
                //    if (reg.RegTypeID == (int)MobileRegType.MNPSuppPlan)
                //    {
                //        if (!ReferenceEquals(rs, null))
                //        {
                //            response.Message = rs.Message;
                //            response.Success = rs.Success;
                //            response.Code = rs.Code;
                //        }
                //    }

                /*End*/



            }
            catch (Exception ex)
            {
            }

            return response;
        }

        public static bool MISMFullFill(int registrationID)
        {
            Logger.InfoFormat("{0}:{1}# Primary Line in  MISMFullFill(2) ", "AutoActivationUtils", "MISMFullFill", registrationID);

            DAL.Models.Registration reg = null;
            Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
            objRegDetails = DAL.Registration.GetLnkRegistrationDetails(registrationID);
            KenanService kenanSer = new KenanService();

            List<int> regIds = new List<int>();
            regIds.Add(registrationID);
            reg = Online.Registration.DAL.Registration.RegistrationGet(regIds).FirstOrDefault();

            Logger.InfoFormat("{0}:{1}# Secondary Line Details with Registrationid{2}", "AutoActivationUtils", "MISMFullFill", registrationID);
            //var RegSec = Online.Registration.DAL.Registration.RegistrationSecGet(new List<int> { registrationID }).FirstOrDefault();
            
            var pbpcIDs = new List<int>();
            if (!ReferenceEquals(reg, null))
            {
                var regPgmBdlPkgCompIDs = Online.Registration.DAL.Registration.RegPgmBdlPkgCompFind(new DAL.Models.RegPgmBdlPkgCompFind()
                {
                    RegPgmBdlPkgComp = new DAL.Models.RegPgmBdlPkgComp()
                    {
                        RegID = reg.ID
                    }
                });
                //var pbpc = new List<DAL.Models.RegPgmBdlPkgComp>();
                //if (!ReferenceEquals(regPgmBdlPkgCompIDs, null) && regPgmBdlPkgCompIDs.Count() > 0)
                //    pbpc = Online.Registration.DAL.Registration.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                //if (pbpc.Count() > 0)
                //    pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();

                if (!ReferenceEquals(objRegDetails, null) && !ReferenceEquals(objRegDetails.SimType, null) && objRegDetails.SimType.Equals("MISM"))
                {
                    // Added the functionality of registering the additional secondary line for MISM

                    var extid = new List<KenanTypeExternalId>();
                    var extidnew = new List<KenanTypeExternalId>();
                    extidnew.Add(new KenanTypeExternalId() { ExternalId = string.Empty, ExternalIdType = string.Empty });
                    extid.Add(new KenanTypeExternalId() { ExternalId = reg.MSISDN1 == null ? "" : reg.MSISDN1, ExternalIdType = (reg.AccExternalID == null ? "" : reg.AccExternalID) });

                    if ( objRegDetails.SimType.Equals("MISM") )
                    {
                        var regAccount = new DAL.Models.RegAccount();
                        var request2 = new KenanTypeAdditionLineRegistration()
                        {
                            //OrderId = "ISELLEMISM" + reg.ID.ToString() + "SIM",
                            OrderId = "ISELLMISM" + reg.ID.ToString() + "SIM",
                            FxAcctNo = (reg.fxAcctNo == null) ? "" : reg.fxAcctNo,
                            ExternalId = (extid.ToList().ToArray() == null) ? extidnew.ToList().ToArray() : extid.ToList().ToArray(),
                        };
                        var response = kenanSer.KenanAdditionLineRegistrationMISMSecondary(request2);
                        if (response.Code != "0")
                        {
                            Logger.InfoFormat("{0}:{1}# MISMFullFill with Response{2}", "AutoActivationUtils", "FrameSimReplacementProcesstoKenan", response.Code);
                            Online.Registration.DAL.Registration.RegistrationCancel(new Online.Registration.DAL.Models.RegStatus()
                            {
                                RegID = registrationID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = reg.LastAccessID == null ? "AutoActivation" : reg.LastAccessID.ToString(),
                                StatusID = StatusList(RefType.Status, Properties.Settings.Default.REG_SR_SECCT_FAIL)
                            });
                            return false;
                        }
                        else
                        {
                            Logger.InfoFormat("{0}:{1}# MISMFullFill with Response{2}", "AutoActivationUtils", "FrameSimReplacementProcesstoKenan", response.Code);
                            Online.Registration.DAL.Registration.RegistrationCancel(new Online.Registration.DAL.Models.RegStatus()
                            {
                                RegID = registrationID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = reg.LastAccessID == null ? "AutoActivation" : reg.LastAccessID.ToString(),
                                StatusID = StatusList(RefType.Status, Properties.Settings.Default.REG_SR_SECCT)
                            });

                            Online.Registration.DAL.Registration.RegistrationCancel(new Online.Registration.DAL.Models.RegStatus()
                            {
                                RegID = registrationID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = reg.LastAccessID == null ? "AutoActivation" : reg.LastAccessID.ToString(),
                                StatusID = StatusList(RefType.Status, Properties.Settings.Default.Status_SvcPendingActivate)
                            });
                            return true;
                        }
                    }
                }
            }
            else
            {
                AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "UpdateWebPOSStatus", "", "Order doesn't exist in trnRegistration", "", 0, registrationID.ToString());
                return false;
            }
            return true;
        }

        public static bool FrameSimReplacementSecondaryDetailsAndProcesstoKenan(int registrationID)
        {
            Logger.InfoFormat("{0}:{1}# Primary Line in  FrameSimReplacementSecondaryDetailsAndProcesstoKenan(2) ", "AutoActivationUtils", "FrameSimReplacementProcesstoKenan", registrationID);
            
            bool isPrimaryLineCompsAdded = false;
            Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
            objRegDetails = DAL.Registration.GetLnkRegistrationDetails(registrationID);
            KenanService kenanSer = new KenanService();
            if (objRegDetails.SimReplacementType > 0 && objRegDetails.SimReplacementType == (int)SimReplacementType.N2M)
            {
                #region "Primary Line Details"
                DAL.Models.Registration reg = null;
                bool isPrimaryCompAdded = false;
                List<int> regIds = new List<int>();
                regIds.Add(registrationID);
                reg = Online.Registration.DAL.Registration.RegistrationGet(regIds).FirstOrDefault();
                var regPgmBdlPkgCompIDs = Online.Registration.DAL.Registration.RegPgmBdlPkgCompFind(new DAL.Models.RegPgmBdlPkgCompFind()
                {
                    RegPgmBdlPkgComp = new DAL.Models.RegPgmBdlPkgComp()
                    {
                        RegID = registrationID
                    }
                });
                if (!ReferenceEquals(regPgmBdlPkgCompIDs, null) && regPgmBdlPkgCompIDs.Count > 0)
                {
                    CenterOrderContractRequest Req = new CenterOrderContractRequest();
                    Req.orderId = registrationID.ToString();
                    Req.UserName = reg.LastAccessID;

                    isPrimaryCompAdded = kenanSer.CenterOrderContractCreation(Req);
                    isPrimaryLineCompsAdded = true;
                    if (!isPrimaryCompAdded)
                    {
                        Logger.InfoFormat("{0}:{1}# Primary Line 2GB Components Addition, response{2}", "AutoActivationUtils", "FrameSimReplacementProcesstoKenan", isPrimaryCompAdded);

                        Online.Registration.DAL.Registration.RegistrationCancel(new Online.Registration.DAL.Models.RegStatus()
                        {
                            RegID = registrationID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Req.UserName == null ? "AutoActivation" : Req.UserName.ToString(),
                            StatusID = StatusList(RefType.Status, Properties.Settings.Default.REG_SR_ADDCOMP_FAIL)
                        });
                        return false;
                    }
                    Online.Registration.DAL.Registration.RegistrationCancel(new Online.Registration.DAL.Models.RegStatus()
                    {
                        RegID = registrationID,
                        Active = true,
                        CreateDT = DateTime.Now,
                        StartDate = DateTime.Now,
                        LastAccessID = Req.UserName == null ? "AutoActivation" : Req.UserName.ToString(),
                        StatusID = StatusList(RefType.Status, Properties.Settings.Default.Status_SvcPendingActivate)
                    });
                    Logger.InfoFormat("{0}:{1}# Primary Line Components call completed, response{2}", "AutoActivationUtils", "FrameSimReplacementProcesstoKenan", registrationID);
                    return true;
                }
                #endregion

                #region "Secondary Line Details"
                if (!isPrimaryLineCompsAdded) // in case of primary line components not exists or created and only secondary line should be created.
                {
                    Logger.InfoFormat("{0}:{1}# Secondary Line Details with Registrationid{2}", "AutoActivationUtils", "FrameSimReplacementProcesstoKenan", registrationID);
                    var RegSec = Online.Registration.DAL.Registration.RegistrationSecGet(new List<int> { registrationID }).FirstOrDefault();
                    var pbpcIDsSec = new List<int>();
                    if (!ReferenceEquals(RegSec, null))
                    {
                        var regPgmBdlPkgCompIDsSec = Online.Registration.DAL.Registration.RegPgmBdlPkgCompSecFind(new DAL.Models.RegPgmBdlPkgCompSecFind()
                        {
                            RegPgmBdlPkgCompSec = new DAL.Models.RegPgmBdlPkgCompSec()
                            {
                                RegID = RegSec.ID
                            }
                        });
                        var pbpcSec = new List<DAL.Models.RegPgmBdlPkgCompSec>();
                        if (!ReferenceEquals(regPgmBdlPkgCompIDsSec, null) && regPgmBdlPkgCompIDsSec.Count() > 0)
                            pbpcSec = Online.Registration.DAL.Registration.RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec).ToList();
                        if (pbpcSec.Count() > 0)
                            pbpcIDsSec = pbpcSec.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();
                        if (!ReferenceEquals(objRegDetails, null) && !ReferenceEquals(objRegDetails.SimType, null) && objRegDetails.SimType == "MISM")
                        {
                            // Added the functionality of registering the additional secondary line for MISM
                            var regSecondary = RegSec;
                            var isMISM = regSecondary == null ? false : true;
                            var extid = new List<KenanTypeExternalId>();
                            var extidnew = new List<KenanTypeExternalId>();
                            extidnew.Add(new KenanTypeExternalId() { ExternalId = string.Empty, ExternalIdType = string.Empty });
                            extid.Add(new KenanTypeExternalId() { ExternalId = reg.MSISDN1 == null ? "" : reg.MSISDN1, ExternalIdType = (reg.AccExternalID == null ? "" : reg.AccExternalID) });
                            if (isMISM == true && regSecondary.IsSecondaryRequestSent == false)
                            {
                                var regAccount = new DAL.Models.RegAccount();
                                regSecondary.IsSecondaryRequestSent = true;
                                int result = Online.Registration.DAL.Registration.UpdateSecondaryRequestSent(regSecondary);
                                var request2 = new KenanTypeAdditionLineRegistration()
                                {
                                    OrderId = "ISELLMISM" + reg.ID.ToString() + "SIM",
                                    FxAcctNo = (reg.fxAcctNo == null) ? "" : reg.fxAcctNo,
                                    ExternalId = (extid.ToList().ToArray() == null) ? extidnew.ToList().ToArray() : extid.ToList().ToArray(),
                                };
                                var response = kenanSer.KenanAdditionLineRegistrationMISMSecondary(request2);
                                if (response.Code != "0")
                                {
                                    Logger.InfoFormat("{0}:{1}# Secondary Line Details with Response{2}", "AutoActivationUtils", "FrameSimReplacementProcesstoKenan", response.Code);
                                    Online.Registration.DAL.Registration.RegistrationCancel(new Online.Registration.DAL.Models.RegStatus()
                                    {
                                        RegID = registrationID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = reg.LastAccessID == null ? "AutoActivation" : reg.LastAccessID.ToString(),
                                        StatusID = StatusList(RefType.Status, Properties.Settings.Default.REG_SR_SECCT_FAIL)
                                    });
                                    return false;
                                }
                                else
                                {
                                    Logger.InfoFormat("{0}:{1}# Secondary Line Details with Response{2}", "AutoActivationUtils", "FrameSimReplacementProcesstoKenan", response.Code);
                                    Online.Registration.DAL.Registration.RegistrationCancel(new Online.Registration.DAL.Models.RegStatus()
                                    {
                                        RegID = registrationID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = reg.LastAccessID == null ? "AutoActivation" : reg.LastAccessID.ToString(),
                                        StatusID = StatusList(RefType.Status, Properties.Settings.Default.REG_SR_SECCT)
                                    });

                                    Online.Registration.DAL.Registration.RegistrationCancel(new Online.Registration.DAL.Models.RegStatus()
                                    {
                                        RegID = registrationID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = reg.LastAccessID == null ? "AutoActivation" : reg.LastAccessID.ToString(),
                                        StatusID = StatusList(RefType.Status, Properties.Settings.Default.Status_SvcPendingActivate)
                                    });
                                    return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        //log the error reason and return false.
                    }

                }

                #endregion
            }


            /**************************************************/
            return true;
        }
        public static bool FrameSimReplacementComponentsDisconnectList(int registrationID)
        {
            Logger.InfoFormat("{0}:{1}# FrameSimReplacementComponentsDisconnectList with RegID{2} Started", "AutoActivationUtils", "FrameSimReplacementComponentsDisconnectList", registrationID);
            bool result = false;
            Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
            objRegDetails = DAL.Registration.GetLnkRegistrationDetails(registrationID);
            KenanService kenanSer = new KenanService();
            List<int> regIds = new List<int>();
            regIds.Add(registrationID);
            var reg = Online.Registration.DAL.Registration.RegistrationGet(regIds).FirstOrDefault();
            #region "Incase of Mism(primary) to Normal flow , need to disconnect MISM secondary Lines"
            if (objRegDetails.SimReplacementType == (int)SimReplacementType.M2N && !ReferenceEquals(reg,null))
            {
                List<lnkSecondaryAcctDetailsSimRplc> SecAccountLines = new List<lnkSecondaryAcctDetailsSimRplc>();
                SecAccountLines = Online.Registration.DAL.Registration.GetSecondaryAccountLinesForSimRplc(registrationID);
                if (SecAccountLines.Count > 0)
                {
                    int indexval = 0;
                    foreach (var itemval in SecAccountLines)
                    {
                        indexval++;
                        var disresp= kenanSer.DisconnectComponents(new DisconnectCenterServiceRequest()
                        {
                            orderId = "ISELL" + registrationID + indexval + "D",
                            newOrderInd = "Y",
                            fxAcctNo = itemval.FxAcctNo,
                            fxSubscrNo = itemval.FxSubscrNo,
                            fxSubscrNoResets = itemval.FxSubscrNoResets,
                            disconnectReason = "1",
                            inpF01 = reg.LastAccessID,
                            inpF02 = reg.OrganisationId
                        });
                        if (disresp.msgCode == "0" && disresp.msgDesc.ToLower() == "success")
                        {
                            result = true;
                        }
                    }
                }
            }
            #endregion
            Logger.InfoFormat("{0}:{1}# FrameSimReplacementComponentsDisconnectList with RegID{2} Finished", "AutoActivationUtils", "FrameSimReplacementComponentsDisconnectList", registrationID);
            return result;
        }

        public static int StatusList(RefType type, string code)
        {
          int statusid = 0;
          var statusList = Online.Registration.DAL.Admin.OnlineRegAdmin.StatusList();
          if (statusList != null)
          {
              statusid = statusList.Where(a => a.Code == code).Select(a => a.ID).FirstOrDefault();
          }
          return statusid;
        }
        public static void LogToDB(string fileName, string method, string request, string response, string exception, int userId,string regId)
        {
            DAL.Registration.InsertLog(new WebPosLog()
                {
                    exception = exception,
                    fileName = fileName,
                    method = method,
                    request = request,
                    response = response,
                    userId = userId,
                    regId=regId,
                });
         }


        //GTM e-Billing CR - Ricky - 2014.10.11 - Start
        public static bool BillDeliveryUpdate(int regTypeId, int regId)
        {
            EBPSLogger.InfoFormat("{0}:{1}# Update EBPS for order {2} - Started", "AutoActivationUtils", "BillDeliveryUpdate", regId);

            bool output = true;
            try
            {
                var reg = DAL.Registration.RegistrationGet(new List<int> { regId }).First();
                var regAccount = DAL.Registration.RegAccountGetByRegID(regId);
                var IsSuppNewAc = DAL.Registration.getSupplineDetails(regId);

                //only trigger the update request to EBPS if the status is Pending
                if (!string.IsNullOrEmpty(reg.billDeliverySubmissionStatus) && reg.billDeliverySubmissionStatus == "Pending")
                {
                    switch (regTypeId)
                    {
                        //Plan+Device
                        case 2: output = AutoActivationUtils.BillDeliveryUpdate(regAccount.KenanAccountExternalNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson); break;

                        //PlanOnly
                        case 3: output = AutoActivationUtils.BillDeliveryUpdate(regAccount.KenanAccountExternalNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson); break;

                        //SuppLine
                        case 8: output = IsSuppNewAc ?
                            AutoActivationUtils.BillDeliveryUpdate(regAccount.KenanAccountExternalNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson) :
                            AutoActivationUtils.BillDeliveryUpdate(reg.KenanAccountNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson); break;

                        //NewLine
                        case 9: output = AutoActivationUtils.BillDeliveryUpdate(reg.KenanAccountNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson); break;

                        //MNP Plan Only
                        case 10: output = AutoActivationUtils.BillDeliveryUpdate(regAccount.KenanAccountExternalNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson); break;

                        //MNP Supplementary Plan
                        case 11: output = IsSuppNewAc ?
                            AutoActivationUtils.BillDeliveryUpdate(regAccount.KenanAccountExternalNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson) :
                            AutoActivationUtils.BillDeliveryUpdate(reg.KenanAccountNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson); break;

                        //MNP New Line
                        case 12: output = AutoActivationUtils.BillDeliveryUpdate(reg.KenanAccountNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson); break;

                        //CRP
                        case 24: output = AutoActivationUtils.BillDeliveryUpdate(reg.KenanAccountNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson); break;

                        //MNP Plan With Multiple Supplementary Line
                        case 26: output = AutoActivationUtils.BillDeliveryUpdate(regAccount.KenanAccountExternalNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson); break;

                        //Add Contract
                        case 36: output = AutoActivationUtils.BillDeliveryUpdate(reg.KenanAccountNo, reg.billDeliveryViaEmail, reg.billDeliveryEmailAddress, reg.billDeliverySmsNotif, reg.billDeliverySmsNo, reg.SalesPerson); break;

                        default: output = true; break;
                    }
                    EBPSLogger.InfoFormat("{0}:{1}# Update EBPS for order {2} - Finished with status {3}", "AutoActivationUtils", "BillDeliveryUpdate", regId, output);
                }
                else
                {
                    //Old order will have empty billDeliverySubmissionStatus
                    EBPSLogger.InfoFormat("{0}:{1}# Update EBPS for order {2} - Not required", "AutoActivationUtils", "BillDeliveryUpdate", regId);
                }
                return output;
            }
            catch (Exception e)
            {
                EBPSLogger.InfoFormat("{0}:{1}# Update EBPS for order {2} - Failed with error {3}", "AutoActivationUtils", "BillDeliveryUpdate", regId, e.Message);
                return false;
            }
        }

        public static bool BillDeliveryUpdate(string kenanAcctNo, bool billDeliveryViaEmail, string billDeliveryEmailAddress, bool billDeliverySmsNotif, string billDeliverySmsNo, string updatedBy)
        {
            UpdSubscriptionInfoRequest req = new UpdSubscriptionInfoRequest();
            UpdSubscriptionInfoResponse resp = new UpdSubscriptionInfoResponse();

            //Note: the value of hardCopySuppressSubscription is always the same with emailBillSubscription
            req.acctExtId = kenanAcctNo;
            req.hcSuppressSubscription = billDeliveryViaEmail ? "Y" : "N";
            req.emailBillSubscription = billDeliveryViaEmail ? "Y" : "N";
            req.emailAddress = billDeliveryViaEmail ? billDeliveryEmailAddress : "";
            req.smsAlertFlag = billDeliverySmsNotif ? "Y" : "N";
            req.smsNo = billDeliverySmsNotif ? billDeliverySmsNo : "";
            req.updatedBy = !string.IsNullOrEmpty(updatedBy) ? updatedBy : "";

            EBPSService service = new EBPSService();
            resp = service.UpdSubscriptionInfo(req);

            if (resp.Success) return true;
            else return false;
        }
        //GTM e-Billing CR - Ricky - 2014.10.11 - End


        // Common method for triggering Welcome Email
        public static POSStatusResult TriggerEmail(int regID)
        {
            //if (!Util.rollOutCheck(regID))
            //{
            //    return new POSStatusResult() { MsgCode = "0", MsgDesc = "send email feature turn off for this store" };
            //}

            bool mailSended = DAL.Registration.GetWelcomeEmailByRegId(regID).Any();
            if (mailSended)
                return new POSStatusResult() { MsgCode = "0", MsgDesc = "email already sent, if you want to retrigger please delete from lnkregWelcomeMail, and try again" };

            bool isCDPUorderRecs = Util.isCDPUorder(regID.ToInt());

            if (isCDPUorderRecs)
            {
                return new POSStatusResult() { MsgCode = "2", MsgDesc = "this is CDPU order, no send email" };
            }
            else
            {
                try
                {
                    var welcomeObjDic = Online.Registration.DAL.Registration.CustomerInfo(regID);
                    var selectedPackage = Online.Registration.DAL.Registration.SelectedPackage(regID);
                    //var selectedArticle = Online.Registration.DAL.Registration.SelectedArticle(welcomeObjDic["ArticleId"].ToLong());
                    List<int> contractId = new List<int>();
                    contractId = OnlineRegAdmin.GetContractByRegId(regID).ToList();
                    string Duration = "-";
                    if (contractId != null & contractId.Any())
                    {
                        Duration = OnlineRegAdmin.GetContractDuration(contractId.FirstOrDefault()).ToString();
                    }
                    var supplineObjDic = Online.Registration.DAL.Registration.GetSuppline(regID);

                    TriggerEmailLog emailLog = new TriggerEmailLog();
                    emailLog.RegID = regID;
                    //SelectedPackageBySuppline

                    //credit limit
                    SubscriberICService subs = new SubscriberICService();
                    MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsRequest _req = new MaxisRetrieveAccountDetails.typeRetrieveAcctDetlsRequest();
                    _req.fxAcctExtId = welcomeObjDic["KenanAccountNo"].ToString2();

                    string creditlimit = string.Empty;


                    if (!string.IsNullOrEmpty(_req.fxAcctExtId))
                    {
                        var _result = subs.RetrieveAccountDetails(_req);
                        creditlimit = !ReferenceEquals(_result, null) ? _result.CreditLimit : string.Empty;
                        if (creditlimit == null)
                        {
                            creditlimit = "-";




                        }
                    }


                    var suppLinePBPCs = new List<PgmBdlPckComponent>();
                    decimal monthlySubscription = selectedPackage["Price"].ToDecimal();

                    string recipientEmail = welcomeObjDic["EmailAddress"];

                    // stub here
                    //recipientEmail = "habs.habs@accenture.com";

                    if (!string.IsNullOrEmpty(recipientEmail))
                    {
                        string pathDirectory = HttpRuntime.AppDomainAppPath;
                        string line = "";
                        string sampleLine = "";
                        string contentLine = "";
                        string fullContentLine = "";
                        using (StreamReader sr = new StreamReader(pathDirectory + @"\Email\welcome.html"))
                        {
                            line = sr.ReadToEnd();
                            line = line.Replace("[CustomerName]", welcomeObjDic["FullName"]);
                            line = line.Replace("[Salutation]", welcomeObjDic["Salutation"]);
                            line = line.Replace("[Address1]", welcomeObjDic["Line1"]);
                            if (!string.IsNullOrEmpty(welcomeObjDic["Line2"]))
                            {
                                line = line.Replace("[Address2]", "<br>" + welcomeObjDic["Line2"]);
                            }
                            else
                            {
                                line = line.Replace("[Address2]", "");
                            }
                            if (!string.IsNullOrEmpty(welcomeObjDic["Line3"]))
                            {
                                line = line.Replace("[Address3]", "<br>" + welcomeObjDic["Line3"]);
                            }
                            else
                            {
                                line = line.Replace("[Address3]", "");
                            }
                            line = line.Replace("[Postcode]", welcomeObjDic["Postcode"]);
                            line = line.Replace("[City]", welcomeObjDic["City"]);
                            line = line.Replace("[State]", welcomeObjDic["State"]);
                            line = line.Replace("[ActivationDate]", DateTime.Now.ToString("dd/MM/yyyy"));
                            line = line.Replace("[OrderID]", regID.ToString2());
                            //line = line.Replace("[ECMDomainURL]", Settings.Default.ECMDomain); // do this later
                        }

                        using (StreamReader sr = new StreamReader(pathDirectory + @"\Email\Content.htm"))
                        {
                            sampleLine = sr.ReadToEnd();
                        }

                        string simserial1 = string.Empty;
                        simserial1 = welcomeObjDic["SIMSerial"];
                        if (string.IsNullOrEmpty(welcomeObjDic["SIMSerial"]))
                        {
                            simserial1 = "-";
                        }
                        contentLine = sampleLine.Replace("[Salutation]", welcomeObjDic["Salutation"]);
                        contentLine = contentLine.Replace("[CustomerName]", welcomeObjDic["FullName"]);
                        emailLog.Name = welcomeObjDic["FullName"];
                        contentLine = contentLine.Replace("[ECMDomainURL]", Settings.Default.ECMDomain);
                        contentLine = contentLine.Replace("[MobileNumber]", welcomeObjDic["MSISDN"]);
                        emailLog.MSISDN1 = welcomeObjDic["MSISDN"];
                        string address2 = string.Empty;
                        string address3 = string.Empty;
                        if (welcomeObjDic["Line2"] != string.Empty)
                        {
                            address2 = "<tr><td>&nbsp;</td><td>" + welcomeObjDic["Line2"] + "</td></tr>";
                        }
                        if (welcomeObjDic["Line3"] != string.Empty)
                        {
                            address3 = "<tr><td>&nbsp;</td><td>" + welcomeObjDic["Line3"] + "</td></tr>";
                        }
                        contentLine = contentLine.Replace("[Address1]", welcomeObjDic["Line1"]);
                        contentLine = contentLine.Replace("[Address2]", address2);
                        contentLine = contentLine.Replace("[Address3]", address3);
                        contentLine = contentLine.Replace("[Postcode]", welcomeObjDic["Postcode"]);
                        contentLine = contentLine.Replace("[City]", welcomeObjDic["City"]);
                        contentLine = contentLine.Replace("[State]", welcomeObjDic["State"]);
                        contentLine = contentLine.Replace("[KenanAccountNumber]", welcomeObjDic["KenanAccountNo"]);
                        contentLine = contentLine.Replace("[SimSerialNumber]", simserial1);
                        contentLine = contentLine.Replace("[CreditLimit]", creditlimit);
                        contentLine = contentLine.Replace("[Logo]", "");
                        string PlanName = selectedPackage["Name"];
                        int index = PlanName.IndexOf("-");
                        if (index > 0)
                        {
                            PlanName = PlanName.Substring(0, index);
                        }
                        contentLine = contentLine.Replace("[PlanName]", PlanName);
                        contentLine = contentLine.Replace("[MonthlySubscription]", monthlySubscription.ToString2());
                        contentLine = contentLine.Replace("[TenureLength]", Duration.ToString());
                        contentLine = contentLine.Replace("[ActivationDate]", DateTime.Now.ToString("dd/MM/yyyy"));



                        var regPBPCIDs = DAL.Registration.RegPgmBdlPkgCompFind(new DAL.Models.RegPgmBdlPkgCompFind()
                        {
                            Active = true,
                            RegPgmBdlPkgComp = new DAL.Models.RegPgmBdlPkgComp()
                            {
                                RegID = regID
                            }
                        }).ToList();


                        // Get all the registered VAS
                        List<RegPgmBdlPkgComp> regVasList = DAL.Registration.RegPgmBdlPkgCompGet(regPBPCIDs);

                        // Filter the principal line VAS
                        List<RegPgmBdlPkgComp> mainVasList = regVasList.Where(a => a.RegSuppLineID == null).ToList();

                        contentLine = contentLine.Replace("[CallServices]", generateCallServicesSection(mainVasList));
                        contentLine = contentLine.Replace("[ValueExtras]", generateValueExtrasSection(mainVasList));
                        contentLine = contentLine.Replace("[CallManagementServices]", generateCallManagementServicesSection(mainVasList));
                        contentLine = contentLine.Replace("[DataServices]", generateDataServicesSection(mainVasList));

                        fullContentLine += contentLine;

                        if (supplineObjDic != null && supplineObjDic.Count > 0)
                        {
                            foreach (var suppObj in supplineObjDic)
                            {
                                var topLogo = "<img src=\"[ECMDomainURL]Email/images/logo.jpg\" width=\"750\" height=\"119\" alt=\"\" style=\"display:block; vertical-align:top\"/>\n";
                                var selectedSuppPackage = Online.Registration.DAL.Registration.SelectedPackageBySuppline(suppObj["ID"].ToInt());
                                //var selectedSuppArticle = Online.Registration.DAL.Registration.SelectedArticle(suppObj["ArticleID"].ToLong());
                                int contractIdsupp = 0;
                                contractIdsupp = OnlineRegAdmin.GetContractByRegSuppLineId(suppObj["ID"].ToInt());
                                string DurationSupp = "-";
                                if (contractIdsupp != 0)
                                {
                                    DurationSupp = OnlineRegAdmin.GetContractDuration(contractIdsupp).ToString();
                                }
                                string simserial2 = string.Empty;
                                simserial2 = suppObj["SIMSerial"];
                                if (string.IsNullOrEmpty(suppObj["SIMSerial"]))
                                {
                                    simserial2 = "-";
                                }
                                contentLine = sampleLine.Replace("[Salutation]", welcomeObjDic["Salutation"]);
                                contentLine = contentLine.Replace("[CustomerName]", welcomeObjDic["FullName"]);
                                contentLine = contentLine.Replace("[ECMDomainURL]", Settings.Default.ECMDomain);
                                contentLine = contentLine.Replace("[MobileNumber]", suppObj["MSISDN1"]);

                                contentLine = contentLine.Replace("[Address1]", welcomeObjDic["Line1"]);
                                contentLine = contentLine.Replace("[Address2]", address2);
                                contentLine = contentLine.Replace("[Address3]", address3);
                                contentLine = contentLine.Replace("[Postcode]", welcomeObjDic["Postcode"]);
                                contentLine = contentLine.Replace("[City]", welcomeObjDic["City"]);
                                contentLine = contentLine.Replace("[State]", welcomeObjDic["State"]);
                                contentLine = contentLine.Replace("[KenanAccountNumber]", welcomeObjDic["KenanAccountNo"]);
                                contentLine = contentLine.Replace("[SimSerialNumber]", simserial2);
                                contentLine = contentLine.Replace("[CreditLimit]", creditlimit);
                                contentLine = contentLine.Replace("[Logo]", topLogo);
                                string PlanNameSupp = selectedSuppPackage["Name"];
                                int index2 = PlanNameSupp.IndexOf("-");
                                if (index2 > 0)
                                {
                                    PlanNameSupp = PlanNameSupp.Substring(0, index2);
                                }
                                contentLine = contentLine.Replace("[PlanName]", PlanNameSupp);
                                contentLine = contentLine.Replace("[MonthlySubscription]", selectedSuppPackage["Price"]);
                                contentLine = contentLine.Replace("[TenureLength]", DurationSupp.ToString());
                                contentLine = contentLine.Replace("[ActivationDate]", DateTime.Now.ToString("dd/MM/yyyy"));



                                // Filter the supplementary line VAS
                                int regSuppLineId = suppObj["ID"].ToInt();
                                List<RegPgmBdlPkgComp> supVasList = regVasList.Where(a => a.RegSuppLineID == regSuppLineId).ToList();

                                contentLine = contentLine.Replace("[CallServices]", generateCallServicesSection(supVasList));
                                contentLine = contentLine.Replace("[ValueExtras]", generateValueExtrasSection(supVasList));
                                contentLine = contentLine.Replace("[CallManagementServices]", generateCallManagementServicesSection(supVasList));
                                contentLine = contentLine.Replace("[DataServices]", generateDataServicesSection(supVasList));

                                fullContentLine += contentLine;
                            }
                        }


                        line = line.Replace("[Contents]", fullContentLine);
                        line = line.Replace("[ECMDomainURL]", Settings.Default.ECMDomain);
                        Logger.Debug("Email Message to Send: " + line);

                        System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                        msg.Subject = ConfigurationManager.AppSettings["EmailSubject"].ToString();//Anthony - Change Email Subject Setting
                        //msg.Subject = "Maxis";
                        msg.IsBodyHtml = true;
                        msg.Body = line;
                        msg.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["WelcomeEmailFrom"].ToString());//Anthony - Change Email From
                        //msg.From = new System.Net.Mail.MailAddress("VSHARVI@maxis.com.my");
                        msg.To.Add(recipientEmail);
                        emailLog.Email = recipientEmail;

                        /* Chokks - Attaching Contract with the mail - Start */
                        List<DAL.Models.RegUploadDoc> regUpload = DAL.Registration.regUploadGet(regID);
                        bool containsTxtAttachment = false;
                        if (!ReferenceEquals(regUpload, null) && regUpload.Count() > 0)
                        {
                            foreach (var doc in regUpload)
                            {
                                if (doc.FileType == "Contract" && doc.FileStream != null)
                                {
                                    Stream pdfstream = new MemoryStream(doc.FileStream);
                                    msg.Attachments.Add(new System.Net.Mail.Attachment(pdfstream, doc.FileName, "application/pdf"));
                                    // to prevent cdpu order getting send
                                    if (doc.FileName.Contains("txt"))
                                        containsTxtAttachment = true;

                                    break;
                                }
                            }
                        }
                        /* Chokks -  Attaching Contract with the mail - End */

                        /* Ricky - Add the attachment for MaxisOne Plan customer */
                        if (isExtraPdfRequired(regVasList))
                        {
                            msg.Attachments.Add(new System.Net.Mail.Attachment(pathDirectory + @"\Email\" + ConfigurationManager.AppSettings["AttachmentMOPPath"].ToString(), "application/pdf"));
                        }
                        if (!containsTxtAttachment)
                        {
                            using (System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient())
                            {
                                smtpClient.Host = ConfigurationManager.AppSettings["SmtpClientHost"].ToString();//Anthony - Move to web.config
                                //smtpClient.Host = "10.200.58.80";
                                smtpClient.EnableSsl = false;
                                smtpClient.Port = 25;
                                smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                                smtpClient.Send(msg);
                            }
                            //log to database HanRick
                            Online.Registration.DAL.Registration.InsertEmailLog(emailLog);
                        }

                    }
                    return new POSStatusResult() { MsgCode = "0", MsgDesc = "send email successfully" };
                }
                catch (Exception ex)
                {
                    AutoActivationUtils.LogToDB("PostActivationService", "TriggerEmail", "", "error", "", 0, regID.ToString2());
                    Logger.Fatal(string.Format("{0}{1}{2}", "TriggerEmail ERROR", regID, Util.LogException(ex)));

                    return new POSStatusResult() { MsgCode = "1", MsgDesc = ex.ToString2() };
                }
            } // end of if CDPU order check
        }

        public static POSStatusResult TriggerSSOEmail(int regID)
        {
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            string pathDirectory = HttpRuntime.AppDomainAppPath;
            var regAttribList = new RegAttributes();
            var result = false;
            var msgCode = string.Empty;
            var msgDesc = string.Empty;

            try
            {
                var customerInfo = Online.Registration.DAL.Registration.CustomerInfo(regID);
                var regAttributesData = DAL.Registration.RegAttributesGetByRegID(regID);
                var statusEmail = regAttributesData.Where(x => x.ATT_Name == RegAttributes.ATTRIB_SINGLESIGNON_EMAIL_SENT && !string.IsNullOrEmpty(x.ATT_Value.ToString2()) && x.ATT_Value.ToString2().ToLower() == "true").ToList();
                var regAttributeForSSOEmail = regAttributesData.Where(x => x.ATT_Name == RegAttributes.ATTRIB_SINGLESIGNON_EMAILADDRESS).ToList();
                var recipientEmail = string.Empty;

                if (statusEmail != null && statusEmail.Count > 0)
                {
                    return new POSStatusResult() { MsgCode = "-1", MsgDesc = "SSO Email is already sent for this order" };
                    //var sentStatus = statusEmail.FirstOrDefault().ATT_Value.ToString2();

                    //if (!string.IsNullOrEmpty(sentStatus) && sentStatus.ToLower() == "true")
                    //{
                        //return new POSStatusResult() { MsgCode = "-1", MsgDesc = "SSO Email is already sent for this order" };
                    //}
                }

                if (regAttributeForSSOEmail != null && regAttributeForSSOEmail.Count > 0)
                {
                    recipientEmail = regAttributeForSSOEmail.FirstOrDefault().ATT_Value;
                }

                //for stub purpose
                //recipientEmail = "santhoni@maxis.com.my";

                if (!string.IsNullOrEmpty(recipientEmail))
                {
                    var contentLine = string.Empty;
                    msg.Subject = ConfigurationManager.AppSettings["EmailSSOSubject"].ToString();
                    msg.IsBodyHtml = true;
                    msg.From = new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["WelcomeEmailFrom"].ToString());
                    msg.To.Add(recipientEmail);

                    using (StreamReader sr = new StreamReader(pathDirectory + @"\EmailSSO\welcome.html"))
                    {
                        contentLine = sr.ReadToEnd();
                    }

                    contentLine = contentLine.Replace("[Salutation]", customerInfo["Salutation"]);
                    contentLine = contentLine.Replace("[CustomerName]", customerInfo["FullName"]);
                    contentLine = contentLine.Replace("[ECMDomainURL]", Settings.Default.ECMDomain);

                    msg.Body = contentLine;

                    using (System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient())
                    {
                        smtpClient.Host = ConfigurationManager.AppSettings["SmtpClientHost"].ToString();
                        //smtpClient.Host = "10.200.58.80";
                        smtpClient.EnableSsl = false;
                        smtpClient.Port = 25;
                        smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        smtpClient.Send(msg);
                        result = true;
                        Logger.Info(string.Format("{0}-{1}", "SendSingleSignOnInvitationEmail SUCCESS", regID));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal(string.Format("{0}-{1}-{2}", "SendSingleSignOnInvitationEmail ERROR", regID, Util.LogException(ex)));
                result = false;
            }

            regAttribList.RegID = regID;
            regAttribList.ATT_Name = RegAttributes.ATTRIB_SINGLESIGNON_EMAIL_SENT;
            regAttribList.ATT_Value = result.ToString2();
            DAL.Registration.SaveRegAttributes(regAttribList);

            if (result == true)
            {
                return new POSStatusResult() { MsgCode = "0", MsgDesc = "email successfully sent" };
            }
            else
            {
                return new POSStatusResult() { MsgCode = "-1", MsgDesc = "internal server error, please check the log in KenanService.log" };
            }
        }

        private static String generateCallServicesSection(List<RegPgmBdlPkgComp> inputVasList)
        {
            // get the corresponding Kenan Codes of the VAS
            List<PgmBdlPckComponent> vasComponentList = DAL.Admin.OnlineRegAdmin.ProgramBundlePackageComponentGet(inputVasList.Select(a => a.PgmBdlPckComponentID).ToList());
            List<String> vasKenanCodeList = vasComponentList.Select(a => a.KenanCode).ToList();

            bool internationRoamingEnabled = !vasKenanCodeList.Intersect(new[] { "40043"}).IsEmpty(); // put international roaming id here.
            bool internationDirecDialEnabled = !vasKenanCodeList.Intersect(new[] { "40021", "44240"}).IsEmpty(); // put IDD id here.

            String vasString =

                "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (internationRoamingEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>International Roaming</td></tr>"
              + "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (internationDirecDialEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>International Direct Dial-IDD</td></tr>";

            //String temp = "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (internationRoamingEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>International Roaming</td></tr>";


            return vasString;
        }


        private static String generateValueExtrasSection(List<RegPgmBdlPkgComp> inputVasList)
        {
            // get the corresponding Kenan Codes of the VAS
            List<PgmBdlPckComponent> vasComponentList = DAL.Admin.OnlineRegAdmin.ProgramBundlePackageComponentGet(inputVasList.Select(a => a.PgmBdlPckComponentID).ToList());
            List<String> vasKenanCodeList = vasComponentList.Select(a => a.KenanCode).ToList();

            bool itemisedBillEnabled = !vasKenanCodeList.Intersect(new[] { "4029", "40031", "40252", "44240", "40045"}).IsEmpty();
            bool shareNSurfEnabled = !vasKenanCodeList.Intersect(new[] { "TBC", "TBC", "TBC" }).IsEmpty();

            String vasString =
                  "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (itemisedBillEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>Itemised Bill</td></tr>";
            //+ "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (shareNSurfEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>Share ‘n’ Surf</td></tr>"; // hide this as requested by Liyana

            return vasString;
        }


        private static String generateCallManagementServicesSection(List<RegPgmBdlPkgComp> inputVasList)
        {
            // get the corresponding Kenan Codes of the VAS
            List<PgmBdlPckComponent> vasComponentList = DAL.Admin.OnlineRegAdmin.ProgramBundlePackageComponentGet(inputVasList.Select(a => a.PgmBdlPckComponentID).ToList());
            List<String> vasKenanCodeList = vasComponentList.Select(a => a.KenanCode).ToList();

            bool callIdPresentationEnabled = !vasKenanCodeList.Intersect(new[] {"40026", "40124", "44240"}).IsEmpty();
            bool callIdRestrictionEnabled = !vasKenanCodeList.Intersect(new[] {"40033", "40105", "40122"}).IsEmpty();

            bool callWaitingEnabled = !vasKenanCodeList.Intersect(new[] {"40027", "40028", "40644", "44240", "40026", "40028", "44240"}).IsEmpty();
            bool callForwardingEnabled = !vasKenanCodeList.Intersect(new[] {"40034", "40117"}).IsEmpty();
            bool callConferenceEnabled = !vasKenanCodeList.Intersect(new[] {"40037", "40118"}).IsEmpty();
            bool voiceMailEnabled = !vasKenanCodeList.Intersect(new[] {"40305"}).IsEmpty(); 

            String vasString =
                 "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (callIdPresentationEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>Calling Line Identification Presentation–CLIP</td></tr>"
               + "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (callIdRestrictionEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>Calling Line ID Restriction–CLR</td></tr>"
               + "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (callWaitingEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>Call Waiting/Hold</td></tr>"
               + "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (callForwardingEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>Call Forwarding</td></tr>"
               + "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (callConferenceEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>Call Conference</td></tr>"
               + "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (voiceMailEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>Voicemail</td></tr>";

            return vasString;
        }


        private static String generateDataServicesSection(List<RegPgmBdlPkgComp> inputVasList)
        {
            // get the corresponding Kenan Codes of the VAS
            List<PgmBdlPckComponent> vasComponentList = DAL.Admin.OnlineRegAdmin.ProgramBundlePackageComponentGet(inputVasList.Select(a => a.PgmBdlPckComponentID).ToList());
            List<String> vasKenanCodeList = vasComponentList.Select(a => a.KenanCode).ToList();

            bool mobileInternetEnabled = !vasKenanCodeList.Intersect(new[] {
                "1566","1567","40030",
                "40082","40083","40089","41660","41909",
                "41942","41944","41946","42145","42452",
                "42454","43307","43309","43628","43630",
                "43631","43632","43633","43857","44058",
                "44061","44165","44166","44175","44209",
                "44225","44226","44426","44427","44811",
                "44812","44813","44838","44839","44840",
                "45342","45343","45357","45359","45360"}).IsEmpty();

            bool mmsEnabled = !vasKenanCodeList.Intersect(new[] {"40032"}).IsEmpty();
            bool smsEnabled = !vasKenanCodeList.Intersect(new[] {"40023"}).IsEmpty();

            String vasString =
                 "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (mobileInternetEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>Mobile Internet</td></tr>"
               + "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (mmsEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>Multimedia Messaging Service (MMS)</td></tr>"
               + "<tr><td width='18' valign='top'><img src='[ECMDomainURL]Email/images/" + (smsEnabled ? "checked.png" : "unchecked.png") + "'/></td><td>Short Messaging Service (SMS)</td></tr>";

            return vasString;
        }

        /*
        private static String generateVasStringContent(List<RegPgmBdlPkgComp> mainVasList)
        {
            List<PgmBdlPckComponent> vasList = DAL.Admin.OnlineRegAdmin.ProgramBundlePackageComponentGet(mainVasList.Select(a => a.PgmBdlPckComponentID).ToList());
            List<Component> vasComponentList = DAL.Admin.OnlineRegAdmin.ComponentGet(vasList.Select(a => a.ChildID).ToList());

            String vasString = "";
            foreach (Component vasComponent in vasComponentList)
            {
                vasString += "<li><input type='checkbox' checked=true disabled=true/> " + vasComponent.Name + "</li>\n";
            }

            return vasString;

        }
        */

        private static bool isExtraPdfRequired(List<RegPgmBdlPkgComp> inputVasList)
        {
            // get the corresponding Kenan Codes of the VAS
            List<PgmBdlPckComponent> vasComponentList = DAL.Admin.OnlineRegAdmin.ProgramBundlePackageComponentGet(inputVasList.Select(a => a.PgmBdlPckComponentID).ToList());
            List<String> vasKenanCodeList = vasComponentList.Select(a => a.KenanCode).ToList();

            //41755 = MaxisOne Plan, 41802 = MaxisOne Share
            return !vasKenanCodeList.Intersect(new[] { "41755", "41802", "41815", "41816", "41817", "41821", "41822", "41818", "41819", "41820", "41823", "41824", "41846" }).IsEmpty();
        }
        
    }


   
}
