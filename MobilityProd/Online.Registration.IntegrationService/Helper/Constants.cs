﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.IntegrationService.Helper
{
    public class Constants
    {
        public static string StatusCDPUApprove = "Approved";
        public static string StatusCDPUPending = "Pending";
        public static string StatusCDPURejected = "Rejected";
		public static string MDP_KENAN_CODE = "45569,45568,47535";

		// need to double check on web
		public static string TERMINATEASSIGN_DEVICE_ONLY = "AssignOrTerminateDeviceOnly";
		public static string TERMINATEASSIGN_ACC_ONLY = "AssignOrTerminateAccOnly";
		public static string TERMINATEASSIGN_ALL = "AssignOrTerminateAll";
    }
}
