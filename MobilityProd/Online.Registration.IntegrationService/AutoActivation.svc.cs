﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Online.Registration.IntegrationService.Helper;
using log4net;
using SNT.Utility;
using Online.Registration.DAL.Models;
using System.Configuration;
using System.Web;
using System.IO;
using Online.Registration.IntegrationService.Properties;
using Online.Registration.IntegrationService.Models;
namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AutoActivation" in code, svc and config file together.

    public class AutoActivation : IAutoActivation
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(WebPOSCallBackService));
        public POSStatusResult UpdateWebPOSStatus(string LoginID, string MsgCode, string MsgDesc, string OrderID)
        {
            Logger.Info(String.Format("Entered UpdateWebPOSSTAtus with parameters::{0},{1},{2},{3}", LoginID, MsgCode, MsgDesc, OrderID));
            int regTypeId = AutoActivationUtils.GetRegType(OrderID);
            if (AutoActivationUtils.GetRegStatus(OrderID.ToInt()) == 31)
            {
                switch (MsgCode)
                {
                    case "0": return SuccessOrder(regTypeId, OrderID);
                    case "1": return AbortOrder(regTypeId, OrderID, MsgDesc);
                    case "2": return ErrorOrder(regTypeId, OrderID, MsgDesc);
                    case "3": return VoidOrder(regTypeId, OrderID, MsgDesc);
                    case "4": return ReturnOrder(regTypeId, OrderID, MsgDesc);
                    default: return new POSStatusResult() { MsgCode = "1", MsgDesc = "Invalid Message Code" };
                }
            }
            else
            {
                AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "UpdateWebPOSStatus", "", "Invalid input. Order already processed at ISELL", "", 0, OrderID);
                return new POSStatusResult() { MsgCode = "0", MsgDesc = "Invalid input. Order already processed at ISELL." };
            }
        }
        private POSStatusResult SuccessOrder(int regTypeId, string OrderID)
        {
            bool response = KenanAccountFulfill(regTypeId, OrderID);
            if (response)
            {
                return new POSStatusResult() { MsgCode = "0", MsgDesc = "Success" };
            }
            else
            {
                return new POSStatusResult() { MsgCode = "1", MsgDesc = "Internal Error" };
            }
            
        }
        private POSStatusResult AbortOrder(int regTypeId, string OrderID, string MsgDesc)
        {
            DAL.Registration.RegistrationClose(new DAL.Models.RegStatus()
            {
                RegID = Convert.ToInt32(OrderID),
                Active = true,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
				StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_Aborted),
            });

            DAL.Registration.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
            {
                RegID = Convert.ToInt32(OrderID),
                CancelReason = MsgDesc,
                CreateDT = DateTime.Now,
                LastUpdateDT = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
            });

            return new POSStatusResult() { MsgCode = "0", MsgDesc = "Success" };
        }
        private POSStatusResult ErrorOrder(int regTypeId, string OrderID, string MsgDesc)
        {
            //DAL.Registration.RegistrationClose(new DAL.Models.RegStatus()
            //{
            //    RegID = Convert.ToInt32(OrderID),
            //    Active = true,
            //    CreateDT = DateTime.Now,
            //    StartDate = DateTime.Now,
            //    LastAccessID = RegStatus.CB_POS,
            //    StatusID = 21,
            //});

            DAL.Registration.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
            {
                RegID = Convert.ToInt32(OrderID),
                CancelReason = MsgDesc,
                CreateDT = DateTime.Now,
                LastUpdateDT = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
            });

            return new POSStatusResult() { MsgCode = "5", MsgDesc = "Payment not recived" };
        }
        private POSStatusResult VoidOrder(int regTypeId, string OrderID, string MsgDesc)
        {
            DAL.Registration.RegistrationClose(new DAL.Models.RegStatus()
            {
                RegID = Convert.ToInt32(OrderID),
                Active = true,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
				StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_Void),
            });

            DAL.Registration.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
            {
                RegID = Convert.ToInt32(OrderID),
                CancelReason = MsgDesc,
                CreateDT = DateTime.Now,
                LastUpdateDT = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
            });

            return new POSStatusResult() { MsgCode = "0", MsgDesc = "Success" };
        }
        private POSStatusResult ReturnOrder(int regTypeId, string OrderID, string MsgDesc)
        {
            DAL.Registration.RegistrationClose(new DAL.Models.RegStatus()
            {
                RegID = Convert.ToInt32(OrderID),
                Active = true,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
				StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_Returned),
            });

            DAL.Registration.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
            {
                RegID = Convert.ToInt32(OrderID),
                CancelReason = MsgDesc,
                CreateDT = DateTime.Now,
                LastUpdateDT = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
            });

            return new POSStatusResult() { MsgCode = "0", MsgDesc = "Success" };
        }

        //public bool SendPaymentDetails(int iSellOrderID)
        //{
        //    bool result = false;
        //    String input = String.Empty;
        //    var reg = Online.Registration.DAL.Registration.RegistrationGet(new List<int> { iSellOrderID }).SingleOrDefault();
        //    var cust = Online.Registration.DAL.Registration.CustomerGet(new List<int> { reg.CustomerID }).SingleOrDefault();
        //    var stsID = Online.Registration.DAL.Registration.RegStatusFind(new RegStatusFind()
        //    {
        //        RegStatus = new RegStatus()
        //        {
        //            RegID = iSellOrderID
        //        },
        //        Active = true
        //    }).FirstOrDefault();
        //    var sts = Online.Registration.DAL.Registration.RegStatusGet(new List<int>() { stsID });
        //    var stsdesc = Online.Registration.DAL.Admin.OnlineRegAdmin.StatusGet(new List<int>() { sts.FirstOrDefault().StatusID });
        //    var timestamp = System.DateTime.Now.ToString("yyyyMMddHHmmss");
        //    KenanService _client = new KenanService();
        //    //may need condition filter
        //    try
        //    {
        //        if (reg != null && reg.RegTypeID == 14)
        //        {
        //            input = _client.GetIMPOSDataForWEBPOS(reg.ID, "SIMReplacement");
        //        }
        //        else
        //        {
        //            input = _client.GetIMPOSDataForWEBPOS(reg.ID, "P");
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        input = "NonBilling|20046799|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000|893456789087653608|EA|||NonBilling|70000005|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||EA|||NonBilling|70000067|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||NonBilling|70000100|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||NonBilling|70000065|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||";
        //    }

        //    SendPaymentDetailsWebPosRequest request = new SendPaymentDetailsWebPosRequest
        //    {
        //        queueNo = reg.QueueNo.ToInt(),
        //        regType = reg.RegTypeID.ToString2(),
        //        iSellOrderID = iSellOrderID,
        //        storeID = reg.OrganisationId.ToInt(),
        //        customerName = cust.FullName,
        //        iSellTimeStamp = timestamp,
        //        MSISDN = reg.MSISDN1,
        //        iSellContent = input,
        //        orderStatusCode = sts.FirstOrDefault().StatusID,
        //        orderStatusDesc = stsdesc.FirstOrDefault().Description,
        //        SalesAgentID = reg.SalesPerson
        //    };
        //    WebPOSCallBackService _clientsvc = new WebPOSCallBackService();
        //    result = _clientsvc.SendPaymentDetails(request);

        //    return result;
        //}

        public bool KenanAccountFulfill(int regTypeId, string regId)
        {
            Registration.DAL.Models.Registration objRegs = new DAL.Models.Registration();
            List<int> regids = new List<int>(); regids.Add(regId.ToInt());
            objRegs = DAL.Registration.RegistrationGet(regids).First();
            var ssoEmailActive = ConfigurationManager.AppSettings["ssoEmailActive"].ToString2();
            List<int> ssoEmailRequired = new List<int> { 2, 3, 8, 9, 10, 11, 12, 19, 24, 26, 36 };

			int intRegID = int.TryParse(regId, out intRegID) ? int.Parse(regId) : 0;

            if (objRegs != null && objRegs.CRPType.ToString2() == "DSP")
            {
                regTypeId = 8;
            }

            if (!string.IsNullOrEmpty(ssoEmailActive) && ssoEmailActive.ToLower() == "true" && ssoEmailRequired.Contains(regTypeId))
            {
                TriggerSSOEmail(regId.ToInt());
            }

            if (regTypeId == 2 || regTypeId == 3 || regTypeId == 8 || regTypeId == 9 || regTypeId == 10 || regTypeId == 11 || regTypeId == 12 || regTypeId == 26)
            {
                bool triggerEmail = Convert.ToBoolean(ConfigurationManager.AppSettings["triggerEmail"]);
                if (triggerEmail)
                {
                    TriggerEmail(regId.ToInt());
                }
            }
			bool iContractActive = Convert.ToBoolean(ConfigurationManager.AppSettings["iContractActive"]);
			Logger.Debug(string.Format("iContract set to {0}-{1}", iContractActive, intRegID));
			if (iContractActive)
			{
				//sendDocsToIcontract(intRegID);
				iContractHelper.sendDocsToIcontract(intRegID);
			}


            Logger.Info(String.Format("Entered KenanAccountFulfill with parameters::{0},{1}", regTypeId, regId));
            switch (regTypeId)
            {
                case 100: return AutoActivationUtils.Smart(regId); break;

                case 2: return AutoActivationUtils.PlanPlusDevice(regId); break;
                case 3: return AutoActivationUtils.PlanOnly(regId); break;

                case 8: return AutoActivationUtils.SuppLine(regId); break;
                case 9: return AutoActivationUtils.NewLine(regId); break;

                case 13: return AutoActivationUtils.AddRemoveVas(regId); break;
                case 14: return AutoActivationUtils.SimReplacement(regId); break;
                case 19: return AutoActivationUtils.CRPPlan(regId); break;
                case 24: return AutoActivationUtils.CRPDevice(regId); break;

                case 10: return AutoActivationUtils.MNP(regId); break;
                case 11: return AutoActivationUtils.MNP(regId); break;
                case 12: return AutoActivationUtils.MNP(regId); break;
                case 26: return AutoActivationUtils.MNP(regId); break;
                case 36: return AutoActivationUtils.AddContract(regId); break;

				case 27: return AutoActivationUtils.MISMFullFill(intRegID); break;

                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;

                default: return true;
            }
        }

        public POSStatusResult GetPOSDetailsbyFilename(string LoginID, string Filename, string Parameter1, string Parameter2)
        {
            var objPOSStatusResult = new POSStatusResult();

            try
            {
                string RegId = string.Empty;

                // As per spec LoginID is mandatory,so validating
                if (string.IsNullOrEmpty(LoginID))
                {
                    objPOSStatusResult.MsgCode = "1";
                    objPOSStatusResult.MsgDesc = "Required LoginID";
                }
                else if (string.IsNullOrEmpty(Filename))
                {
                    objPOSStatusResult.MsgCode = "1";
                    objPOSStatusResult.MsgDesc = "Required File Name";
                }
                else if (Filename.Split('-').Length != 4)
                {
                    objPOSStatusResult.MsgCode = "1";
                    objPOSStatusResult.MsgDesc = "Invalid File Name";
                }
                else
                {
                    RegId = Filename.Split('-')[2];

                    if (string.IsNullOrEmpty(RegId))
                    {
                        objPOSStatusResult.MsgCode = "1";
                        objPOSStatusResult.MsgDesc = "Invalid File Name";
                    }
                    else
                    {
                        var objKenanSvc = new KenanService();
                        //converted for Dealer support later we have to write a condition for Normal and dealer                        
                        objPOSStatusResult.Content = objKenanSvc.GetIMPOSDataForWEBPOS(RegId.ToInt());
                        objPOSStatusResult.MsgCode = "0";
                        objPOSStatusResult.MsgDesc = "success";
                    }
                }

            }
            catch (Exception ex)
            {
                objPOSStatusResult.MsgCode = "1";
                objPOSStatusResult.MsgDesc = "iSell / Other systems are Down. Unable to fetch data.|" + ex.Message;
            }
            return objPOSStatusResult;
        }

        public POSStatusResult TriggerEmail(int regID)
        {
			return AutoActivationUtils.TriggerEmail(regID);
        }

        public POSStatusResult TriggerSSOEmail(int regID)
        {
            return AutoActivationUtils.TriggerSSOEmail(regID);
        }
    }
}
