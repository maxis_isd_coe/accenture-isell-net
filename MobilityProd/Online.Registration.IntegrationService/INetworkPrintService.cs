﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INetworkPrintService" in both code and config file together.
    [ServiceContract]
    public interface INetworkPrintService
    {
        [OperationContract]
        PrintResponse Print(PrintRequest Request);
    }
    [DataContract]
    public class PrintResponse
    {
        bool boolValue = true;
        string message = "";

        [DataMember]
        public bool Status
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string Message
        {
            get { return message; }
            set { message = value; }
        }


    }

    [DataContract]
    public class PrintRequest
    {
        [DataMember]
        public string PrinterName { get; set; }

        [DataMember]
        public int PrinterID { get; set; }

        [DataMember]
        public string FileUrl { get; set; }

		[DataMember]
		public string RegID { get; set; }
    }
}
