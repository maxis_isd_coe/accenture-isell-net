﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Online.Registration.IntegrationService.Models;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWebPOSCallBackService" in both code and config file together.
    [ServiceContract]
    public interface IWebPOSCallBackService
    {
        [OperationContract]
        POSStatusResult UpdateWebPOSStatus(string LoginID, string MsgCode, string MsgDesc, string OrderID);

        [OperationContract]
        POSStatusResult Exchange(string LoginID, string OrderID, string IMEI);

        #region commented, prefiously built for web pos
        //[OperationContract]
        //bool SendPaymentDetails(SendPaymentDetailsWebPosRequest request);
        #endregion

        [OperationContract]
        POSStatusResult GetPOSDetailsbyFilename(string LoginID, string Filename, string Parameter1,string Parameter2);

		[OperationContract]
		POSStatusResult sendDocsToIcontract(int regID, bool singleDoc = false);

		[OperationContract]
		POSStatusResult TriggerEmail(int regID);
       
    }
}
