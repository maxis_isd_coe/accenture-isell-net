﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Security.Principal;
using System.Xml;
using System.Xml.Linq;
using log4net;
using Online.Registration.DAL.Admin;
using Online.Registration.DAL.Models;
using Online.Registration.IntegrationService.ExtraTenWS;
using Online.Registration.IntegrationService.Helper;
using Online.Registration.IntegrationService.MaxisBillingProcessAcctDetlWs;
using Online.Registration.IntegrationService.MaxisCommonRetrieveComponentInfoWs;
using Online.Registration.IntegrationService.MaxisDDMFCheckWs;
using Online.Registration.IntegrationService.MaxisOrderProcessWs;
using Online.Registration.IntegrationService.MaxisRetrieveRulesInfoService;
using Online.Registration.IntegrationService.MbaCreditCheckWs;
using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.Properties;
using Online.Registration.IntegrationService.RetrievePkgCompInfoService;
using Online.Registration.IntegrationService.updateContractExtDetlsService;
using Online.Registration.IntegrationService.ValidateMnpInventoryServiceWS;
using SNT.Utility;
using System.Globalization;
namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MNPResubmit" in code, svc and config file together.
    public class MNPResubmit : IMNPResubmit
    {
        string kenanRequest = string.Empty;
        string kenanResponse = string.Empty;
        string[] arrOPFDownExceptions = null;
        string[] arrKenanDownExceptions = null;
        string contractKenanCode = string.Empty;
        string contractMSISDN = string.Empty;
        string contractType = string.Empty;
        static readonly ILog Logger = LogManager.GetLogger(typeof(KenanService));

            #region Vikas Added Code 14TH OCT 2014
        //**@@**
        public ResubmitOrderCreationMNPResponse ResubmitCreateCenterOrderMNP(ResubmitOrderCreationRequestMNP request)
        {
            Logger.InfoFormat("ResubmitCreateCenterOrderMNP({0}) called!", request);

            Logger.Info(string.Format("Entering ResubmitCreateCenterOrderMNP(): OrderID({0})", request.orderIdField));

			bool isRegAccountExist = false;
			int regID = 0;
			var regAccount = new DAL.Models.RegAccount();

			regID = request.orderIdField.Replace(Properties.Resources.orderProcessReq_from, "").ToInt();
			regAccount = DAL.Registration.RegAccountGetByRegID(regID, "");

            var response = new ResubmitOrderCreationMNPResponse();

			if (regAccount == null)
				regAccount = new DAL.Models.RegAccount();
			else
				isRegAccountExist = true;

            try
            {
                var orderCreateRq = ConstructResubmitOrderCreateRequestMNP(request);
                using (var proxy = new MaxisOrderProcessWs.orderCenterService())
                {
                    proxy.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                    //proxy.Credentials = new NetworkCredential("iselluser", "iselluser");
                    proxy.PreAuthenticate = true;

                    proxy.eaiHeaderValue = new MaxisOrderProcessWs.eaiHeader()
                    {
                        from = Properties.Resources.orderProcessReq_from,
                        to = Properties.Resources.orderProcessReq_to,
                        appId = Properties.Resources.orderProcessReq_appID,
                        msgType = Properties.Resources.orderProcessReq_msgType,
                        msgId = string.Format("CO{0}", request.orderIdField),
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    };
                    kenanRequest = XMLHelper.ConvertObjectToXml(orderCreateRq, typeof(typeResubmitCenterPortInRequest));
                    Logger.Info("ResubmitCreateCenterOrderMNP_Request" + kenanRequest);
                    //var resp = proxy.recreateCenterOrderMNP(orderCreateRq);
                    var resp = proxy.resubmitCenterPortIn(orderCreateRq);

                    kenanResponse = XMLHelper.ConvertObjectToXml(resp, typeof(Online.Registration.IntegrationService.MaxisOrderProcessWs.typeResubmitCenterPortInResponse));
                    Logger.Info("ResubmitCreateCenterOrderMNP_Response" + kenanResponse);
                    arrOPFDownExceptions = ConfigurationManager.AppSettings["WMDBDown"].Split('|');
                    foreach (string s in arrOPFDownExceptions)
                    {
                        if (resp.msgDesc.ToLower().Contains(s) && resp.msgCode == "1")
                        {
                            resp.msgDesc = ConfigurationManager.AppSettings["WMDBDownDesc"].ToString();
                            break;
                        }
                    }
                    arrKenanDownExceptions = ConfigurationManager.AppSettings["KenanDown"].Split('|');
                    foreach (string s1 in arrKenanDownExceptions)
                    {
                        if (resp.msgDesc.ToLower().Contains(s1) && resp.msgCode == "3")
                        {
                            resp.msgDesc = ConfigurationManager.AppSettings["KenanDownDesc"].ToString();
                            break;
                        }
                    }

                    #region Added by Nreddy for logging
                    KenanaLogDetails objKenanaLogDetails = new KenanaLogDetails();
                    objKenanaLogDetails.RegID = request.orderIdField.Replace(Properties.Resources.orderProcessReq_from, "").ToInt(); ;
                    objKenanaLogDetails.MessageCode = resp.msgCode;
                    objKenanaLogDetails.MessageDesc = !ReferenceEquals(resp.msgDesc, null) ? resp.msgDesc : string.Empty;
                    objKenanaLogDetails.KenanXmlReq = kenanRequest;
                    objKenanaLogDetails.KenanXmlRes = kenanResponse;
                    objKenanaLogDetails.MethodName = "ResubmitCreateCenterOrderMNP";
                    OnlineRegAdmin.SaveKenanXmlLogs(objKenanaLogDetails);
                    #endregion

                    if (resp.msgCode != Properties.Settings.Default.KenanSuccessCode)
                    {
                        response.Code = resp.msgCode;
                        response.Message = resp.msgDesc;

                    }
                    else if (resp.msgCode == Properties.Settings.Default.KenanSuccessCode)
                    {
                        response.Success = true;
                        response.Code = resp.msgCode;
                        response.Message = resp.msgDesc;

						// when MNP triggered, straight away make the status as service pending activate.
						if (!isRegAccountExist)
							DAL.Registration.RegAccountCreate(regAccount, ConstructRegStatus(regID, Properties.Settings.Default.Status_SvcPendingActivate));
						else
							DAL.Registration.RegAccountUpdate(regAccount, ConstructRegStatus(regID, Properties.Settings.Default.Status_SvcPendingActivate));
						
                    }
                    /*end*/
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }
            Logger.Info(string.Format("Exiting ResubmitCreateCenterOrderMNP(): OrderID({0})", request.orderIdField));
            return response;
        }
        #endregion

        #region Private Methods

        private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
        {
            resp.Code = "-1";
            resp.Message = ex.Message;
            resp.Success = false;
        }

        private typeResubmitCenterPortInRequest ConstructResubmitOrderCreateRequestMNP(ResubmitOrderCreationRequestMNP request)
        {
            typeResubmitCenterPortInRequest newResubmitRequest = new typeResubmitCenterPortInRequest();

            newResubmitRequest.orderId = request.orderIdField;
            newResubmitRequest.lob = request.lobField;

            typeNewPortInRequest portInReq = new typeNewPortInRequest();
            portInReq.accountNo = request.newPortInRequestField.accountNoField;
            portInReq.acctCategory = request.newPortInRequestField.acctCategoryField;
            portInReq.acctSegment = request.newPortInRequestField.acctSegmentField;
            portInReq.createBy = request.newPortInRequestField.createByField;
            portInReq.customerName = request.newPortInRequestField.customerNameField;
            portInReq.donorId = request.newPortInRequestField.donorIdField;
            portInReq.identity = request.newPortInRequestField.identityField;
            portInReq.identityType = request.newPortInRequestField.identityTypeField;
            portInReq.mktCode = request.newPortInRequestField.mktCodeField;
            portInReq.mpgAcctCategory = request.newPortInRequestField.mpgAcctCategoryField;
            portInReq.mpgAcctSegment = request.newPortInRequestField.mpgAcctSegmentField;
            portInReq.msisdn = request.newPortInRequestField.msisdnField;
            portInReq.msisdnType = request.newPortInRequestField.msisdnTypeField;
            portInReq.oldReqId = request.newPortInRequestField.oldReqIdField;
            portInReq.piStatus = request.newPortInRequestField.piStatusField;
            portInReq.recipientId = request.newPortInRequestField.recipientIdField;
            portInReq.reqId = request.newPortInRequestField.reqIdField;
            portInReq.reqReason = request.newPortInRequestField.reqReasonField;
            portInReq.resubmitFlag = request.newPortInRequestField.resubmitFlagField;

            newResubmitRequest.newPortInRequest = portInReq;

            List<typeCommitOrderResponse> commitOrderResponseListField = new List<typeCommitOrderResponse>();
            if (request.commitOrderResponseListField != null)
            {
                foreach (ResubmitCommitOrderResponse cs in request.commitOrderResponseListField)
                {
                    typeCommitOrderResponse orderRes = new typeCommitOrderResponse();
                    orderRes.fxOrderId = cs.fxOrderIdField;
                    orderRes.fxServiceOrderId = cs.fxServiceOrderIdField;
                    orderRes.msisdn = cs.msisdnField;
                    orderRes.subscrNo = cs.subscrNoField;
                    orderRes.subscrNoResets = cs.subscrNoResetsField;
                    commitOrderResponseListField.Add(orderRes);
                }
                commitOrderResponseListField.ToArray();

                newResubmitRequest.commitOrderResponseList = commitOrderResponseListField != null ? commitOrderResponseListField.ToArray() : null;
            }

            return newResubmitRequest;
        }

		private DAL.Models.RegStatus ConstructRegStatus(int regID, string statusCode)
		{
			Access username = DAL.Registration.getuserNameKenan(regID);
			if (ReferenceEquals(username, null))
			{
				username = new Access();
				username.UserName = "KENAN CALLBACK";
			}
			return new DAL.Models.RegStatus()
			{
				RegID = regID,
				StartDate = DateTime.Now,
				StatusID = KenanConfigHelper.GetStatusID(statusCode),
				CreateDT = DateTime.Now,
				Active = true,
				LastAccessID = username.UserName,
			};
			//setConfirmationStatus(regID);

		}

        #endregion
    }
}
