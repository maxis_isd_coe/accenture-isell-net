﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Management;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Net.NetworkInformation;
using System.Drawing.Printing;
using System.Net.Sockets;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Online.Registration.IntegrationService.Helper;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NetworkPrintService" in code, svc and config file together.
    public class NetworkPrintService : INetworkPrintService
    {
        public PrintResponse Print(PrintRequest Request)
        {
			TaskScheduler Sta = new StaTaskScheduler(1);

			PrintResponse respCode = new PrintResponse();
            string nwPrinterName = GetPrinterNetworkName(Request.PrinterName);
			//Thread thread = new Thread(() => respCode = SendToPrinter(nwPrinterName, Request.FileUrl));
			//thread.SetApartmentState(ApartmentState.STA);
			//thread.Start();
			//thread.Join();

			// Gerry - new method for network printing
			Task.Factory.StartNew(() => respCode = SendToPrinter(nwPrinterName, Request.FileUrl, Request.RegID), CancellationToken.None, TaskCreationOptions.None, Sta).Wait();

			return respCode;
        }

        static DataTable Printers;

        public enum StatusCode
        {
            Success,
            InsufficientParameters,
            PrinterNotAvailable,
            PrinterDoesNotExist,
            PrinterNotConnected,
            BrowserActiveXInstanceNull,
            FileNotFound,
            ServerException,
			PDFGenerationFailed
        }

		private static string _path = string.Empty;
        /// <summary>
        /// Prints the method.
		/// Method using Ghostscript + GSView, make sure you install first, in local / staging / prod
		/// Make sure the file location is granted for full permission
        /// </summary>
        /// <exception cref="System.InvalidOperationException"></exception>
		protected PrintResponse SendToPrinter(string printerName, string path, string regID)
        {
            Logger logger = new Logger();
			PrintResponse errorResponse = new PrintResponse();
			string PDFexception = string.Empty;
			string ghostPrintException = string.Empty;
			string outputPath = string.Empty;
			// testing purpose, so make sure html file is exist, and make sure no need to print all pages, only 1 page needed
			//path = "https://10.21.10.162/PrintBill/2015_test_acn.htm";
			//path = "http://10.21.8.82/Accenture/iSell_Mobility_UT_Test/printbill/test_print.htm";

			logger.Log(string.Format("RegID : '{0}' ,Printer Name : '{1}' , inputPath : '{2}' ", regID, printerName, path));

			Util.convertHTMLStringToPDF(ref path, ref regID, out PDFexception, out outputPath);
			logger.Log(string.Format("RegID : '{0}' ,Printer Name : '{1}' , printPathResult : '{2}' ", regID, printerName, outputPath));

			if (string.IsNullOrEmpty(PDFexception))
			{
				ghostPrint(printerName, outputPath, true, out ghostPrintException);
				if (string.IsNullOrEmpty(ghostPrintException))
				{
					errorResponse.Status = true;
					errorResponse.Message = "file printed";
				}
				else {
					errorResponse.Status = false;
					errorResponse.Message = ghostPrintException.ToString();
				}
			}
			else
			{
				errorResponse.Status = false;
				errorResponse.Message = PDFexception.ToString();
			}

			return errorResponse;

			#region old method of printing, using IE rendering class.
			/* deprecated
			try
            {
                logger.Log(string.Format("Printer Name : '{0}' , File url : '{1}' ", printerName, path));
				var exists = PrinterSettings.InstalledPrinters.Cast<object>().Any(pName => printerName.Contains(pName.ToString()) || printerName.Equals(pName.ToString()));
				logger.Log(string.Format("Printer Name : '{0}' , exists : '{1}' ", printerName, exists));
                if (exists)
                {
                    var bDefaultPrinterSet = SetDefaultPrinter(printerName);
                    if (!bDefaultPrinterSet)
                        logger.Log(string.Format("Unable to set '{0}' as default printer", printerName));

                    bool pingPrinter = ConnectToPrinter(printerName);
					logger.Log(string.Format("Printer Name : '{0}' , pingPrinter : '{1}' ", printerName, pingPrinter));
                    if (!pingPrinter)
                    {
                        logger.Log(string.Format("Printer '{0}' is offline/not connected.", printerName));
                        return StatusCode.PrinterNotConnected;
					}
					#region group commented code
					//DataTable dtPrinters = GetPrinters();
					//if (dtPrinters.Rows.Count < 1)
					//{
					//    logger.Log("Printer information not available in database.");
					//    Environment.Exit((int)StatusCodes.PrinterInfoNotAvailableInDb);
					//}
					////var rows= dtPrinters.AsEnumerable().Where(r => r.Field<string>("PrinterName") == printerName);
					//var rows = dtPrinters.AsEnumerable().Where(r => r.Field<string>("PrinterName") != "");
					//if (rows.Any())
					//{
					//    var ip = @"\\fs-01\first floor printer"; //rows.FirstOrDefault()["PrinterNetworkPath"].ToString();
					//    bool pingPrinter = PingHost(ip);

					//    if (!pingPrinter)
					//    {
					//        logger.Log("Unable to connect to printer");
					//        Environment.Exit((int)StatusCodes.PrinterNotConnected);
					//    }
					//}
					//else
					//{
					//    logger.Log("Printer information not available in database.");
					//    Environment.Exit((int)StatusCodes.PrinterInfoNotAvailableInDb);
					//}
					#endregion
					
                }
                else
                {
                    return StatusCode.PrinterDoesNotExist;
                }

                const short PRINT_WAITFORCOMPLETION = 2;
                const int OLECMDID_PRINT = 6;
                const int OLECMDEXECOPT_DONTPROMPTUSER = 2;
                isCorrectDocLoaded = false;
                using (var browser = new WebBrowser())
                {
					logger.Log(string.Format("Printer Name : '{0}' , browser : '{1}' ", printerName, browser));
                    browser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(browser_DocumentCompleted);
                    browser.Navigate(path);
					logger.Log(string.Format("Printer Name : '{0}' , path : '{1}' ", printerName, path));

                    while (browser.ReadyState != WebBrowserReadyState.Complete)
                        Application.DoEvents();

					logger.Log(string.Format("Printer Name : '{0}' , Completed : '{1}' ", printerName, "Completed"));
                    //if (!isCorrectDocLoaded)
                    //{
                    //    logger.Log("Unable to load the document in the browser, Check the file url");
                    //    Environment.Exit((int)StatusCodes.FileNotFound);
                    //}
					
					SHDocVw.InternetExplorer ie = (SHDocVw.InternetExplorer)browser.ActiveXInstance;
                    if (ie != null)
                    {
                        logger.Log("Printing file : " + path);
						//ie.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER, PRINT_WAITFORCOMPLETION);
						ie.ExecWB(SHDocVw.OLECMDID.OLECMDID_PRINT, SHDocVw.OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER);

                        logger.Log("File printed : " + path);
                        return StatusCode.Success;
                    }
                    else
                    {
                        logger.Log("ActiveX Browser Instance is null");
                        return StatusCode.BrowserActiveXInstanceNull;
                    }
                }
            }
            catch (InvalidOperationException ex)
            {
                logger.Log(ex.Message);
                return StatusCode.PrinterNotAvailable;
            }
            catch (Exception ex)
            {
                string innerException = "null";
                if (ex.InnerException != null)
                {
                    innerException = ex.InnerException.Message;
                }
                logger.Log("Failed to print file :" + path + " Exception : " + ex.ToString() + "\n InnerException : " + innerException);
				
                return StatusCode.ServerException;
			}
			*/
			#endregion

		}


		public void ghostPrint(string printerName, string fileName, bool portrait, out string ghostPrintException)
		{
			ghostPrintException = string.Empty;
			string gsArguments = string.Empty;
			string gsLocation = string.Empty;
			ProcessStartInfo gsProcessInfo;
			// dont forget to turn this off during production deployment.
			bool devMode = Convert.ToBoolean(ConfigurationManager.AppSettings["ghostPrintDevMode"]);
			if (devMode)
			{
				gsLocation = ConfigurationManager.AppSettings["ghostPrintLocationForDev"];
			}else{
				gsLocation = ConfigurationManager.AppSettings["ghostPrintLocation"];
			}
			
			Logger logger = new Logger();

			logger.Log("= Step 1 = input ");
			logger.Log("printerName = " + printerName);
			logger.Log("fileName = " + fileName);

			logger.Log("= Step 2 = preparing argument");
            gsArguments = GetGSArgument(printerName, fileName, portrait);

			logger.Log("= Step 3 = initialize printing");
			gsProcessInfo = new ProcessStartInfo();
			gsProcessInfo.LoadUserProfile = true;

			logger.Log("= Step 4 = hide window style");
			gsProcessInfo.WindowStyle = ProcessWindowStyle.Hidden;

			logger.Log("= Step 5 = location = " + gsLocation);
			gsProcessInfo.FileName = gsLocation;

			logger.Log("= Step 6 = arguments = " + gsArguments);
			gsProcessInfo.Arguments = gsArguments;

			logger.Log("= Step 7 = start printing");
			try
			{
				using (Process exeProcess = Process.Start(gsProcessInfo))
				{
					logger.Log("= Step 8 = printing in progress");
					exeProcess.WaitForExit();
				}
				ghostPrintException = string.Empty;
				logger.Log("= Step 9 = PRINTING SUCCESS !!!!");
			}
			catch (Exception ex)
			{
				ghostPrintException = ex.ToString();
				logger.Log("PRINTING FAILED !!!!");
				logger.Log(ex.ToString());
			}
		}

        protected virtual string GetGSArgument(string printerName, string fileName, bool portrait)
        {
            if (portrait)
                return string.Format("-noquery -portrait -printer \"{0}\" \"{1}\"", printerName, fileName);
            else
                return string.Format("-noquery -landscape -printer \"{0}\" \"{1}\"", printerName, fileName);
        }


        static bool isCorrectDocLoaded = false; // To check if the file exists
        static void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            isCorrectDocLoaded = e.Url.ToString().Equals(_path);
        }

        [DllImport("Winspool.drv")]
        private static extern bool SetDefaultPrinter(string printerName);

        public static bool ConnectToPrinter(string printerName)
        {
            ManagementScope scope = new ManagementScope(@"\root\cimv2");
            scope.Connect();

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");


            foreach (ManagementObject printer in searcher.Get())
            {
                var name = printer["Name"].ToString().ToLower();
				if (name.Equals(printerName.ToLower())|| name.Contains(printerName.ToLower()))
				{
					return !printer["WorkOffline"].ToString().ToLower().Equals("true");
				}
            }
            return false;
        }

        public static bool PingHost(string nameOrAddress)
        {
            System.Net.Sockets.TcpClient client = new TcpClient();
            try
            {
                client.Connect(nameOrAddress, 0);
                return true;
            }
            catch (SocketException ex)
            {
                Console.WriteLine("Connection could not be established due to: \n" + ex.Message);
                return false;
            }
            finally
            {
                client.Close();
            }


            bool pingable = false;
            Ping pinger = new Ping();

            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }

            return pingable;
        }

        private static DataTable GetPrinters()
        {
            Logger logger = new Logger();
            DataTable dt = new DataTable();
            try
            {

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OnlineStoreDbContext"].ToString();

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[USP_GetUserSpecificPrinters]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@ORGID", SqlDbType.Int).Value = 0;

                        con.Open();

                        

                        dt.Load(cmd.ExecuteReader());
                        Printers = dt;
                        return dt;
                    }
                }
            }
            catch(Exception ex)
            {
                logger.Log(string.Format("Exception in GetPrinters method. \nException message : {0}. \nInnerMsg : {1}", ex.Message, ex.InnerException != null ? ex.InnerException.Message:""));
                return dt;
            }
        }
        private string GetPrinterNetworkName(string printerName)
        {
            Logger logger = new Logger();
            DataView dv = new DataView();
            DataTable dt = new DataTable();
            string printerNwName = string.Empty;

            try
            {

                logger.Log("Retrieving Printer Network Path for the printer : " + printerName);
                if (Printers == null)
                {
                    GetPrinters();
                }

                dv = Printers.DefaultView;


                dv.RowFilter = "PrinterName='" + printerName + "'";
                dt = dv.ToTable();
                if (dt.Rows.Count > 0)
                {
                    printerNwName = dt.Rows[0]["PrinterNetworkPath"].ToString();
                }
            }
            catch (Exception ex)
            {
                //System.IO.File.Delete(sFile);
            }
	
            logger.Log("Printer Network Path : " + printerNwName);
            return printerNwName;
        }
    }


    public class Logger
    {
        public static string logsFolder = ConfigurationManager.AppSettings["printLogsFolder"] != null ?
            ConfigurationManager.AppSettings["printLogsFolder"].ToString() : @"C:\PrinterServiceLogs";

        private string fileName;
        public Logger()
        {
            try
            {
                if (!System.IO.Directory.Exists(logsFolder))
                {
                    System.IO.Directory.CreateDirectory(logsFolder);
                }

                fileName = logsFolder + @"\Printlog" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            }
            catch (Exception ex)
            {
                //Error in logging
            }

        }

        public void Log(string strMessage)
        {
            // Store the script names and test results in a output text file.

            try
            {
                using (StreamWriter writer = new StreamWriter(new FileStream(fileName, FileMode.Append)))
                {
                    writer.WriteLine(System.DateTime.Now.ToString("yyyyMMdd HH:mm:ss.ffff") + ":: {0}", strMessage);
                }
            }
            catch (Exception ex)
            {
                //Error in logging
            }
        }
    }
}
