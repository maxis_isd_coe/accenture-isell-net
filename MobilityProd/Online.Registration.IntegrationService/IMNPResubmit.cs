﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Security.Principal;
using System.Xml;
using System.Xml.Linq;
using log4net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.DAL.Admin;
using Online.Registration.DAL.Models;
using Online.Registration.IntegrationService.ExtraTenWS;
using Online.Registration.IntegrationService.Helper;
using Online.Registration.IntegrationService.MaxisBillingProcessAcctDetlWs;
using Online.Registration.IntegrationService.MaxisCommonRetrieveComponentInfoWs;
using Online.Registration.IntegrationService.MaxisDDMFCheckWs;
using Online.Registration.IntegrationService.MaxisOrderProcessWs;
using Online.Registration.IntegrationService.MaxisRetrieveRulesInfoService;
using Online.Registration.IntegrationService.MbaCreditCheckWs;
using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.Properties;
using Online.Registration.IntegrationService.RetrievePkgCompInfoService;
using Online.Registration.IntegrationService.updateContractExtDetlsService;
using Online.Registration.IntegrationService.ValidateMnpInventoryServiceWS;
using SNT.Utility;
using System.Globalization;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMNPResubmit" in both code and config file together.
    [ServiceContract]
    public interface IMNPResubmit
    {

        [OperationContract]
        ResubmitOrderCreationMNPResponse ResubmitCreateCenterOrderMNP(ResubmitOrderCreationRequestMNP request);
    }
}
