﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using SNT.Utility;
using log4net;

using Online.Registration.IntegrationService.Helper;

namespace Online.Registration.IntegrationService
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PostActivationService" in code, svc and config file together.
	public class PostActivationService : IPostActivationService
	{
		static readonly ILog Logger = LogManager.GetLogger(typeof(KenanService));

		public void executePostActivation(int _regID)
		{
            Logger.Fatal(string.Format("Entering executePostActivation(): OrderID({0})", _regID));
			var reg = DAL.Registration.RegistrationGet(new int[] { _regID }.ToList()).SingleOrDefault();
			List<int> emailRequired = new List<int> { 2, 3, 8, 9, 10, 11, 12, 26 };
            List<int> ssoEmailRequired = new List<int> { 2, 3, 8, 9, 10, 11, 12, 19, 24, 26, 36 };
            var ssoEmailActive = ConfigurationManager.AppSettings["ssoEmailActive"].ToString2();

			if (reg != null && reg.CRPType.ToString2() == "DSP")
			{
				reg.RegTypeID = 8;
			}

            // *** this is for SSO *** //
            if (!string.IsNullOrEmpty(ssoEmailActive) && ssoEmailActive.ToLower() == "true" && ssoEmailRequired.Contains(reg.RegTypeID))
            {
                try { AutoActivationUtils.TriggerSSOEmail(_regID); }
                catch (Exception ex)
                {
                    //AutoActivationUtils.LogToDB("PostActivationService", "SendSingleSignOnInvitationEmail", "", "error", "", 0, _regID.ToString2());
                    Logger.Fatal(string.Format("{0}-{1}-{2}", "SendSingleSignOnInvitationEmail ERROR", _regID, Util.LogException(ex)));
                }
            }
			
			// gry, icontract need to be send first before email, otherwise, email will come as html

			// *** this is for icontract *** //
			bool iContractActive = Convert.ToBoolean(ConfigurationManager.AppSettings["iContractActive"]);
			if (iContractActive)
			{
				try { iContractHelper.sendDocsToIcontract(_regID); }
				catch (Exception ex)
				{
					//AutoActivationUtils.LogToDB("PostActivationService", "sendDocsToIcontract", "", "error", "", 0, _regID.ToString2() );
					Logger.Fatal(string.Format("{0}-{1}-{2}", "sendDocsToIcontract ERROR", _regID, Util.LogException(ex)));
				}
			}

			// *** this is for email *** //
			if (emailRequired.Contains(reg.RegTypeID))
			{
				try { AutoActivationUtils.TriggerEmail(_regID); }
				catch (Exception ex)
				{
					//AutoActivationUtils.LogToDB("PostActivationService", "TriggerEmail", "", "error", "", 0, _regID.ToString2());
					Logger.Fatal(string.Format("{0}-{1}-{2}", "TriggerEmail ERROR", _regID, Util.LogException(ex)));
				}
			}
            Logger.Fatal(string.Format("Existing executePostActivation(): OrderID({0})", _regID));
        }
	}
}
