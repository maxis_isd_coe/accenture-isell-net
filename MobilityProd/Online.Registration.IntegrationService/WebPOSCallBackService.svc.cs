﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Online.Registration.IntegrationService.Helper;
using Online.Registration.DAL;
using SNT.Utility;
using System.IO;
using System.Web;
using Online.Registration.IntegrationService.Properties;
using Online.Registration.DAL.Models;
using System.Net;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;
using Online.Registration.IntegrationService.IContractUploadWS;
using System.Configuration;
using log4net;
using Online.Registration.IntegrationService.Models;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WebPOSCallBackService" in code, svc and config file together.
    public class WebPOSCallBackService : IWebPOSCallBackService
    {
		private static readonly ILog Logger = LogManager.GetLogger(typeof(WebPOSCallBackService));

        #region Private Methods
        private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
        {
            resp.Code = "-1";
            resp.Message = ex.Message;
            resp.Success = false;
        }
        #endregion

        public POSStatusResult UpdateWebPOSStatus(string LoginID, string MsgCode, string MsgDesc, string OrderID)
        {
            //for dummy
            //return new POSStatusResult() { MsgCode = "0", MsgDesc = "Success" };
            try
            {
                int regTypeId = AutoActivationUtils.GetRegType(OrderID);
                //DB LOG
                string request = "loginID: " + LoginID + " MsgCode: " + MsgCode + " MsgDesc: " + MsgDesc + " RegId: " + OrderID;
                AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "UpdateWebPOSStatus", request, "", "", 0, OrderID);
                if (AutoActivationUtils.GetRegStatus(OrderID.ToInt()) == 31)
                {
                    switch (MsgCode)
                    {
                        case "0": return SuccessOrder(regTypeId, OrderID);  
                        case "1": return AbortOrder(regTypeId, OrderID);
                        case "2": return ErrorOrder(regTypeId, OrderID);
                        case "3": return VoidOrder(regTypeId, OrderID);
                        case "4": return ReturnOrder(regTypeId, OrderID);
                        default: return new POSStatusResult() { MsgCode = "1", MsgDesc = "Invalid Message Code" };
                    }
                }
                else
                {
					AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "UpdateWebPOSStatus", "", "Order already processed at ISELL", "", 0, OrderID);
					AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "UpdateWebPOSStatus", "", "msgCode = " + MsgCode, "", 0, OrderID);
					switch (MsgCode)
					{
						//case "0": return SuccessOrder(regTypeId, OrderID);
						case "1": return AbortOrder(regTypeId, OrderID);
						case "2": return ErrorOrder(regTypeId, OrderID);
						case "3": return VoidOrder(regTypeId, OrderID);
						case "4": return ReturnOrder(regTypeId, OrderID);
						default: return new POSStatusResult() { MsgCode = "0", MsgDesc = "Invalid input. Order already processed at ISELL." };
					}

                }
                
            }
            catch (Exception ex)
            {
                AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "UpdateWebPOSStatus", ex.ToString(), "", "", 0, OrderID);
                return new POSStatusResult() { MsgCode = "2", MsgDesc = "Unable to fetch data for OrderID :" + OrderID };
            }
        }

        #region commented, prefiously built for web pos
        ///// <summary>
        ///// Deletegate pointing to RegistrationCreatePOS
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //private delegate SendPaymentDetailsWebPosResponse SendPaymentDetailsDelegate(SendPaymentDetailsWebPosRequest request);
        ///// <summary>
        ///// Calls the validateInventoryExtIdRequest
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns>TypeOf(bool) if valid externalid then returns true, if invalid will return false</returns>
        //public bool SendPaymentDetails(SendPaymentDetailsWebPosRequest request)
        //{
        //    Logger.InfoFormat("sendPaymentDetails({0}) called!", XMLHelper.ConvertObjectToXml(request, typeof(SendPaymentDetailsWebPosRequest)));
        //    SendPaymentDetailsDelegate abc = new SendPaymentDetailsDelegate(SendPaymentDetailsToWebPOS);
        //    SendPaymentDetailsWebPosResponse response = abc.Invoke(request);

        //    Logger.InfoFormat("RegistrationToWebPOS({0}):{1} returned!", request, response.Code.ToString2() + "(" + response.Code.ToString2() + ")");

        //    ///IF UNSUCCESSFUL THEN RETURN FALSE
        //    if (!"1".Equals(response.Code.ToString2()))
        //    {
        //        return false;
        //    }
        //    return true;
        //}

        
//        /// <summary>
//        /// Method calling RegistrationCreateToPOS
//        /// </summary>
//        /// <param name="request"></param>
//        /// <returns>TypeOf(RegistrationToWebPosResponse)</returns>
//        private SendPaymentDetailsWebPosResponse SendPaymentDetailsToWebPOS(SendPaymentDetailsWebPosRequest request)
//        {
//            Logger.Info(string.Format("Entering RegistrationCreateToPOS(): iSellOrderID({0})", request.iSellOrderID));

//            string sendPaymentDetailsXMLRequest = string.Empty;
//            string sendPaymentDetailsXMLResponse = string.Empty;
//            string userName = Properties.Settings.Default.ValidateInventoryExtIdResponseUserName;
//            string password = Properties.Settings.Default.ValidateInventoryExtIdResponsePassword;

//            var response = new SendPaymentDetailsWebPosResponse();
//            sendPaymentDetailsXMLRequest = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:tem=""http://tempuri.org/"">
//               <soapenv:Header/>
//               <soapenv:Body>
//                  <tem:UpdateISellTransactionToPos>
//                     <tem:queueNo>{0}</tem:queueNo>
//                     <!--Optional:-->
//                     <tem:regType>{1}</tem:regType>
//                     <tem:iSellOrderID>{2}</tem:iSellOrderID>
//                     <tem:storeID>{3}</tem:storeID>
//                     <!--Optional:-->
//                     <tem:customerName>{4}</tem:customerName>
//                     <!--Optional:-->
//                     <tem:iSellTransactionDate>{5}</tem:iSellTransactionDate>
//                     <!--Optional:-->
//                     <tem:MSISDN>{6}</tem:MSISDN>
//                     <!--Optional:-->
//                     <tem:iSellContent>{7}</tem:iSellContent>
//                     <tem:orderStatusCode>{8}</tem:orderStatusCode>
//                     <!--Optional:-->
//                     <tem:orderStatusDesc>{9}</tem:orderStatusDesc>
//                     <!--Optional:-->
//                     <tem:salesAgentID>{10}</tem:salesAgentID>
//                  </tem:UpdateISellTransactionToPos>
//               </soapenv:Body>
//            </soapenv:Envelope>
//            ", new String[] { request.queueNo.ToString2(), request.regType, request.iSellOrderID.ToString2(), request.storeID.ToString2(), request.customerName, request.iSellTimeStamp, request.MSISDN, request.iSellContent, request.orderStatusCode.ToString2(), request.orderStatusDesc, request.SalesAgentID });

//            string dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
//                <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
//                <SOAP-ENV:Body>
//                <ser-root:UpdateISellTransactionToPosResponse SOAP-ENC:root=""1"" xmlns:ser-root=""http://www.webmethods.com/maxis.eai.process.common.ws"">
//                <Code xsi:type=""xsd:string"">1</Code>
//                    <Message xsi:type=""xsd:string"">Success</Message>
//                </ser-root:UpdateISellTransactionToPosResponse>
//                </SOAP-ENV:Body>
//                </SOAP-ENV:Envelope>";
//            try
//            {
//                //var SendDetailsToWebPosRq = PrepareTransactionToPos(request);

//                //using (var proxy = new UpdateISellTransactionPOS.UpdateISellTransactionToPosService())
//                //{
//                    //proxy.Credentials = new NetworkCredential(userName, password);
//                    //proxy.PreAuthenticate = true;
//                    WebClient retrieveBilingInfoWebClient = new WebClient();
                    
//                    string postResult = string.Empty;

//                    retrieveBilingInfoWebClient.Credentials = new NetworkCredential(userName, password);
                    

//                    //sendPaymentDetailsXMLRequest = XMLHelper.ConvertObjectToXml(SendDetailsToWebPosRq, typeof(sendPaymentDetailsWebPosRequest));
//                    Logger.Info("RegistrationToWebPosRequest  :" + sendPaymentDetailsXMLRequest);


//                    if (Properties.Settings.Default.DevelopmentModeOnForWEBPOSChecking)
//                    {
//                        postResult = dummyReplyData;
//                    }
//                    else
//                    {
//                        Uri postUri = new Uri(Properties.Settings.Default.UpdateISellTransactionPOS.ToString());
//                        postResult = retrieveBilingInfoWebClient.UploadString(postUri, sendPaymentDetailsXMLRequest);
//                    }
                    
//                    sendPaymentDetailsXMLResponse = Util.RemoveAllNamespaces(postResult);
//                    //var resp = proxy.UpdateISellTransactionToPos(SendDetailsToWebPosRq);
//                    //sendPaymentDetailsXMLResponse = XMLHelper.ConvertObjectToXml(resp, typeof(Online.Registration.IntegrationService.MaxisOrderProcessWs.eaiResponseType));
//                    Logger.Info("RegistrationToWebPosResponse  :" + sendPaymentDetailsXMLResponse);

//                    XDocument doc = XDocument.Parse(sendPaymentDetailsXMLResponse);

//                    foreach (var r in doc.Descendants("UpdateISellTransactionToPosResponse"))
//                    {
//                        response.Code = r.Element("Code") == null ? 0 : r.Element("Code").Value.ToInt();
//                        response.Message = r.Element("Message") == null ? "" : r.Element("Message").Value.ToString();
//                    }
//                    if (response != null)
//                    {
//                        if (response.Code != 1)
//                        {
//                            /// 1 – success
//                            /// 2 - Internal Error
//                            /// 3 – Payment Already Completed
//                            /// 4 – Order aborted in POS
//                            response = null;
//                        }
//                        else
//                        {
//                            KenanService _client = new KenanService();
//                            DAL.Registration.RegStatusUpdate(request.iSellOrderID, _client.ConstructRegStatusForRegID(request.iSellOrderID, Properties.Settings.Default.Status_ReadyForPayment));
//                        }
//                    }

//                    //if (resp.msgCode != Properties.Settings.Default.KenanSuccessCode)
//                    //{
//                    //    response.Code = resp.msgCode;
//                    //    response.Message = resp.msgDesc;
//                    //}
//                //}
//            }
//            catch (Exception ex)
//            {
//                Logger.Error("", ex);
//                PopulateExceptionResponse(response, ex);
//            }
//            Logger.Info(string.Format("Exiting RegistrationCreateToPOS(): iSellOrderID({0})", request.iSellOrderID));

//            return response;
        //        }
        #endregion

        private POSStatusResult SuccessOrder(int regTypeId, string OrderID)
        {
            bool response = KenanAccountFulfill(regTypeId, OrderID);
            //DB LOG
            string responseForLog = @"Got response as ' " + response.ToString() + " ' for KenanAccountFulfill method as regtypeId as " + regTypeId + " and regID as " + OrderID;
            AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "SuccessOrder", "", responseForLog, "", 0, OrderID);

            //GTM e-Billing CR - Ricky - 2014.10.10 - Start
            //Trigger to EBPS to update Bill Delivery Information, there should be no blocking with order flow even when EBPS is down
            bool ebpsResponse = AutoActivationUtils.BillDeliveryUpdate(regTypeId, OrderID.ToInt());
            responseForLog = @"Got response as ' " + ebpsResponse.ToString() + " ' for BillDeliveryUpdate method as regtypeId as " + regTypeId + " and regID as " + OrderID;
            AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "BillDeliveryUpdate", "", responseForLog, "", 0, OrderID);

            DAL.Models.Registration regUpd = new DAL.Models.Registration();
            if (ebpsResponse)
            {
                regUpd.ID = OrderID.ToInt();
                regUpd.billDeliverySubmissionStatus = "Success";
            }
            else
            {
                regUpd.ID = OrderID.ToInt();
                regUpd.billDeliverySubmissionStatus = "Failed";
            }
            DAL.Registration.RegistrationUpdate(regUpd);
            //GTM e-Billing CR - Ricky - 2014.10.10 - End

            if (response)
            {
                return new POSStatusResult() { MsgCode = "0", MsgDesc = "Success" };
            }
            else
            {
                return new POSStatusResult() { MsgCode = "1", MsgDesc = "Internal Error" };
            }

        }

        private POSStatusResult AbortOrder(int regTypeId, string OrderID)
        {
            //DB LOG
            string responseForLog = @"regtypeId: " + regTypeId + " and regID: " + OrderID;
            AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "AbortOrder", "", responseForLog, "", 0, OrderID);

            DAL.Registration.RegistrationClose(new DAL.Models.RegStatus()
            {
                RegID = Convert.ToInt32(OrderID),
                Active = true,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
				StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_Aborted),
            });

            DAL.Registration.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
            {
                RegID = Convert.ToInt32(OrderID),
                CancelReason = "Abort from WebPOS Request",
                CreateDT = DateTime.Now,
                LastUpdateDT = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
            });

            return new POSStatusResult() { MsgCode = "0", MsgDesc = "Success" };
        }
        private POSStatusResult ErrorOrder(int regTypeId, string OrderID)
        {
            //DB LOG
            string responseForLog = @"regtypeId: " + regTypeId + " and regID: " + OrderID;
            AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "ErrorOrder", "", responseForLog, "", 0, OrderID);

            //DAL.Registration.RegistrationClose(new DAL.Models.RegStatus()
            //{
            //    RegID = Convert.ToInt32(OrderID),
            //    Active = true,
            //    CreateDT = DateTime.Now,
            //    StartDate = DateTime.Now,
            //    LastAccessID = RegStatus.CB_POS
            //    //StatusID = 21,
            //});

            DAL.Registration.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
           {
               RegID = Convert.ToInt32(OrderID),
               CancelReason = "Error from WebPOS Request",
               CreateDT = DateTime.Now,
               LastUpdateDT = DateTime.Now,
               LastAccessID = RegStatus.CB_POS,
           });

             return new POSStatusResult() { MsgCode = "5", MsgDesc = "Payment not recived" };
        }
        private POSStatusResult VoidOrder(int regTypeId, string OrderID)
        {
            //DB LOG
            string responseForLog = @"regtypeId: " + regTypeId + " and regID: " + OrderID;
            AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "VoidOrder", "", responseForLog, "", 0, OrderID);

            DAL.Registration.RegistrationClose(new DAL.Models.RegStatus()
            {
                RegID = Convert.ToInt32(OrderID),
                Active = true,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
				StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_Void),
            });

            DAL.Registration.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
            {
                RegID = Convert.ToInt32(OrderID),
                CancelReason = "Void from WebPOS Request",
                CreateDT = DateTime.Now,
                LastUpdateDT = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
            });

            return new POSStatusResult() { MsgCode = "0", MsgDesc = "Success" };
        }
        private POSStatusResult ReturnOrder(int regTypeId, string OrderID)
        {
            //DB LOG
            string responseForLog = @"regtypeId: " + regTypeId + " and regID: " + OrderID;
            AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "ReturnOrder", "", responseForLog, "", 0, OrderID);

            DAL.Registration.RegistrationClose(new DAL.Models.RegStatus()
            {
                RegID = Convert.ToInt32(OrderID),
                Active = true,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
				StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_Returned),
            });

            DAL.Registration.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
            {
                RegID = Convert.ToInt32(OrderID),
                CancelReason = "Return from WebPOS Request",
                CreateDT = DateTime.Now,
                LastUpdateDT = DateTime.Now,
                LastAccessID = RegStatus.CB_POS,
            });

            return new POSStatusResult() { MsgCode = "0", MsgDesc = "Success" };
        }

        

        private bool KenanAccountFulfill(int regTypeId, string regId)
        {
            Registration.DAL.Models.Registration objRegs = new DAL.Models.Registration();
            List<int> regids = new List<int>(); regids.Add(regId.ToInt());
            objRegs = DAL.Registration.RegistrationGet(regids).First();

            // added by Gry
            int intRegID = int.TryParse(regId, out intRegID) ? int.Parse(regId) : 0;
            if (intRegID == 0)
                return false;
            // added by Gry
            
            if (objRegs != null && objRegs.CRPType.ToString2() == "DSP")
            {
                regTypeId = 8;
            }


			// 30072015 - Gry - already catered on PostActivationService
			//if (regTypeId == 2 || regTypeId == 3 || regTypeId == 8 || regTypeId == 9 || regTypeId == 10 || regTypeId == 11 || regTypeId == 12 || regTypeId == 26)
			//{
			//    //TODO: Wen Hao to check further
			//    //Temporary commented out due to causing issue during UAT
			//    bool triggerEmail = Convert.ToBoolean(ConfigurationManager.AppSettings["triggerEmail"]);
			//    if (triggerEmail)
			//    {
			//        TriggerEmail(regId.ToInt());
			//    }
			//}

			// 01062015 - Anthony - reopen the code to cater the auto knock-off
			//bool iContractActive = Convert.ToBoolean(ConfigurationManager.AppSettings["iContractActive"]);
			//Logger.Debug(string.Format("iContract set to {0}-{1}",iContractActive,intRegID));
			//if (iContractActive)
			//{
			//    sendDocsToIcontract(intRegID);
			//}
            

            switch (regTypeId)
            {
                case 100: return AutoActivationUtils.Smart(regId); break;

                case 2: return AutoActivationUtils.PlanPlusDevice(regId); break;
                case 3: return AutoActivationUtils.PlanOnly(regId); break;

                case 8: return AutoActivationUtils.SuppLine(regId); break;
                case 9: return AutoActivationUtils.NewLine(regId); break;
				// Gry: from controller, order type Add/Remove VAS, is calling CRP Method :
                //case 13: return AutoActivationUtils.AddRemoveVas(regId); break;

				case 13: return AutoActivationUtils.CRPPlan(regId); break;
                case 14: return AutoActivationUtils.SimReplacement(regId); break;
                case 19: return AutoActivationUtils.CRPPlan(regId); break;
                case 24: return AutoActivationUtils.CRPDevice(regId); break;

                case 10: return AutoActivationUtils.MNP(regId); break;
                case 11: return AutoActivationUtils.MNP(regId); break;
                case 12: return AutoActivationUtils.MNP(regId); break;
                case 26: return AutoActivationUtils.MNP(regId); break;
                case 36: return AutoActivationUtils.AddContract(regId); break;

                case 27: return AutoActivationUtils.MISMFullFill(intRegID); break;

                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;
                //case 3:return  AutoActivationUtils.PlanOnly(regId); break;

                default: return true;
            }
        }

		public POSStatusResult TriggerEmail(int regID)
        {
			return AutoActivationUtils.TriggerEmail(regID);
		}

		#region iContract
		
		public POSStatusResult sendDocsToIcontract(int regID,bool singleDoc = false)
		{
			return iContractHelper.sendDocsToIcontract(regID,singleDoc);
		}
		
		#endregion
		
		public POSStatusResult Exchange(string LoginID, string OrderID, string IMEI)
        {
            try
            {
                #region commented code or previous Exchane logic

                //var request = new Models.UpdateOrderStatusRequest();
                //DAL.Models.Registration regDet = DAL.Registration.GetRegistrationDetails(Convert.ToInt32(OrderID));
                //request.FxAccIntID = regDet.fxAcctNo;
                //request.FxSubscrNo = regDet.fxSubscrNo;
                //request.FxSubscrNoResets = regDet.fxSubScrNoResets;
                //request.OrderID = OrderID;
                //var orgId  = regDet.OrganisationId;
                //KenanService _client = new KenanService();
                //_client.DeviceExchange(IMEI, regDet.LastAccessID, orgId, request);

                //return new POSStatusResult() { MsgCode = "0", MsgDesc = "Success" };

                #endregion

                //DB LOG
                string responseForLog = @"LoginId: " + LoginID + ", and regID: " + OrderID + ", IMEI: " + IMEI;
                AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "Exchange", "", responseForLog, "", 0, OrderID);

                DAL.Registration.RegistrationClose(new DAL.Models.RegStatus()
                {
                    RegID = Convert.ToInt32(OrderID),
                    Active = true,
                    CreateDT = DateTime.Now,
                    StartDate = DateTime.Now,
                    LastAccessID = RegStatus.CB_POS,
                    StatusID = KenanConfigHelper.GetStatusID(Properties.Settings.Default.Status_Exchanged),
                });

                DAL.Registration.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                {
                    RegID = Convert.ToInt32(OrderID),
                    CancelReason = "Exchange request from WebPOS",
                    CreateDT = DateTime.Now,
                    LastUpdateDT = DateTime.Now,
                    LastAccessID = RegStatus.CB_POS,
                });

                return new POSStatusResult() { MsgCode = "0", MsgDesc = "Exchanged Successfully. " };
            }
            catch (Exception ex)
            {
                AutoActivationUtils.LogToDB("WebPOSCallBackService.SVC.CS", "Exchange", "", ex.ToString(), "", 0, OrderID);
                return new POSStatusResult() { MsgCode = "2", MsgDesc = "Unable to fetch data for OrderID :" + OrderID };                
            }

        }

        /*Need Uncoment to run sendPaymentDetails with Class reference
        /// <summary>
        /// Method swaps value to proxy datatype from internal datatype
        /// </summary>
        /// <param name="request">TypeOf(RegistrationToWebPosRequest)</param>
        /// <returns>TypeOf(RegistrationToWebPosRequest)</returns>
        private sendPaymentDetailsWebPosRequest PrepareTransactionToPos(SendPaymentDetailsWebPosRequest request)
        {
            return new sendPaymentDetailsWebPosRequest()
            {
                queueNo = request.queueNo,
                regType = request.regType,
                iSellOrderID = request.iSellOrderID,
                storeID = request.storeID,
                customerName = request.customerName,
                iSellTimeStamp = request.iSellTimeStamp,
                MSISDN = request.MSISDN,
                iSellContent = request.iSellContent,
                orderStatusCode = request.orderStatusCode,
                SalesAgentID = request.SalesAgentID
            };
        }
         */
          
        /// <summary>
        /// 
        /// </summary>
        /// <param name="LoginID">Mandatory Input Param</param>
        /// <param name="Filename">Mandatory Input Param,expecting in following pattern : "OrganisationId-ModelID-regid-DateTime.Now.Date.ToString("yyyyMMdd")"</param>
        /// <param name="Parameter1">Optional Reserved Param1</param>
        /// <param name="Parameter2">Optional Reserved Param2</param>
        /// <returns></returns>
        public POSStatusResult GetPOSDetailsbyFilename(string LoginID, string Filename, string Parameter1, string Parameter2)
        {
            var objPOSStatusResult = new POSStatusResult();

            try
            {
                string RegId = string.Empty;

                // As per spec LoginID is mandatory,so validating
                if (string.IsNullOrEmpty(LoginID))
                {
                    objPOSStatusResult.MsgCode = "1";
                    objPOSStatusResult.MsgDesc = "Required LoginID";
                }
                else if (string.IsNullOrEmpty(Filename))
                {
                    objPOSStatusResult.MsgCode = "1";
                    objPOSStatusResult.MsgDesc = "Required File Name";
                }
                else if (Filename.Split('-').Length != 4)
                {
                    objPOSStatusResult.MsgCode = "1";
                    objPOSStatusResult.MsgDesc = "Invalid File Name";
                }
                else
                {
                    RegId = Filename.Split('-')[2];

                    if (string.IsNullOrEmpty(RegId))
                    {
                        objPOSStatusResult.MsgCode = "1";
                        objPOSStatusResult.MsgDesc = "Invalid File Name";
                    }
                    else
                    {
                        var objKenanSvc = new KenanService();
                        //converted for Dealer support later we have to write a condition for Normal and dealer                        
                        objPOSStatusResult.Content = objKenanSvc.GetIMPOSDataForWEBPOS(RegId.ToInt());
                        objPOSStatusResult.MsgCode = "0";
                        objPOSStatusResult.MsgDesc = "success";
                    }
                }

            }
            catch (Exception ex)
            {
                objPOSStatusResult.MsgCode = "1";
                objPOSStatusResult.MsgDesc = "Unable to fetch data.|";
            }
            return objPOSStatusResult;
        }
    }

    



    public class POSStatusResult
    {
        public string MsgCode { get; set; }
        public string MsgDesc { get; set; }
        public string Content { get; set; }
    }
}
