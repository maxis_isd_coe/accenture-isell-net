﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/", ConfigurationName="MaxisRetrieveEligiblePackageService.retrieveEligiblePackageService_PortType")]
    public interface retrieveEligiblePackageService_PortType {
        
        // CODEGEN: Generating message contract since the wrapper name (retrieveEligiblePackageRequest) of message retrieveEligiblePackageRequest does not match the default value (retrieveEligiblePackage)
        [System.ServiceModel.OperationContractAttribute(Action="maxis_eai_process_common_ws_retrieveEligiblePackageService_Binder_retrieveEligibl" +
            "ePackage", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageResponse retrieveEligiblePackage(Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/common/soap/v1/")]
    public partial class eaiHeader : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string fromField;
        
        private string toField;
        
        private string appIdField;
        
        private string msgTypeField;
        
        private string msgIdField;
        
        private string correlationIdField;
        
        private string timestampField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string from {
            get {
                return this.fromField;
            }
            set {
                this.fromField = value;
                this.RaisePropertyChanged("from");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string to {
            get {
                return this.toField;
            }
            set {
                this.toField = value;
                this.RaisePropertyChanged("to");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string appId {
            get {
                return this.appIdField;
            }
            set {
                this.appIdField = value;
                this.RaisePropertyChanged("appId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string msgType {
            get {
                return this.msgTypeField;
            }
            set {
                this.msgTypeField = value;
                this.RaisePropertyChanged("msgType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string msgId {
            get {
                return this.msgIdField;
            }
            set {
                this.msgIdField = value;
                this.RaisePropertyChanged("msgId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string correlationId {
            get {
                return this.correlationIdField;
            }
            set {
                this.correlationIdField = value;
                this.RaisePropertyChanged("correlationId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string timestamp {
            get {
                return this.timestampField;
            }
            set {
                this.timestampField = value;
                this.RaisePropertyChanged("timestamp");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/")]
    public partial class eligiblePackageList : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string packageIdField;
        
        private string downgradeIndField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string packageId {
            get {
                return this.packageIdField;
            }
            set {
                this.packageIdField = value;
                this.RaisePropertyChanged("packageId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=1)]
        public string downgradeInd {
            get {
                return this.downgradeIndField;
            }
            set {
                this.downgradeIndField = value;
                this.RaisePropertyChanged("downgradeInd");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/")]
    public partial class typeRetrieveEligiblePackageResponse : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string msgCodeField;
        
        private string msgDescField;
        
        private eligiblePackageList[] eligiblePackageListField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string msgCode {
            get {
                return this.msgCodeField;
            }
            set {
                this.msgCodeField = value;
                this.RaisePropertyChanged("msgCode");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=1)]
        public string msgDesc {
            get {
                return this.msgDescField;
            }
            set {
                this.msgDescField = value;
                this.RaisePropertyChanged("msgDesc");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("eligiblePackageList", IsNullable=true, Order=2)]
        public eligiblePackageList[] eligiblePackageList {
            get {
                return this.eligiblePackageListField;
            }
            set {
                this.eligiblePackageListField = value;
                this.RaisePropertyChanged("eligiblePackageList");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/")]
    public partial class typeRetrieveEligiblePackageRequest : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string currentPackageIdField;
        
        private string mktCodeField;
        
        private string acctCategoryIdField;
        
        private string rateClassField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string currentPackageId {
            get {
                return this.currentPackageIdField;
            }
            set {
                this.currentPackageIdField = value;
                this.RaisePropertyChanged("currentPackageId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=1)]
        public string mktCode {
            get {
                return this.mktCodeField;
            }
            set {
                this.mktCodeField = value;
                this.RaisePropertyChanged("mktCode");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=2)]
        public string acctCategoryId {
            get {
                return this.acctCategoryIdField;
            }
            set {
                this.acctCategoryIdField = value;
                this.RaisePropertyChanged("acctCategoryId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=3)]
        public string rateClass {
            get {
                return this.rateClassField;
            }
            set {
                this.rateClassField = value;
                this.RaisePropertyChanged("rateClass");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="retrieveEligiblePackageRequest", WrapperNamespace="http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/", IsWrapped=true)]
    public partial class retrieveEligiblePackageRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://schemas.maxis.com.my/EAI/common/soap/v1/")]
        public Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.eaiHeader eaiHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/", IsNullable=true)]
        public Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.typeRetrieveEligiblePackageRequest retrieveEligiblePackageReqDetls;
        
        public retrieveEligiblePackageRequest() {
        }
        
        public retrieveEligiblePackageRequest(Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.eaiHeader eaiHeader, Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.typeRetrieveEligiblePackageRequest retrieveEligiblePackageReqDetls) {
            this.eaiHeader = eaiHeader;
            this.retrieveEligiblePackageReqDetls = retrieveEligiblePackageReqDetls;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="retrieveEligiblePackageResponse", WrapperNamespace="http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/", IsWrapped=true)]
    public partial class retrieveEligiblePackageResponse {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://schemas.maxis.com.my/EAI/common/soap/v1/")]
        public Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.eaiHeader eaiHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/", IsNullable=true)]
        public Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.typeRetrieveEligiblePackageResponse retrieveEligiblePackageRespDetls;
        
        public retrieveEligiblePackageResponse() {
        }
        
        public retrieveEligiblePackageResponse(Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.eaiHeader eaiHeader, Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.typeRetrieveEligiblePackageResponse retrieveEligiblePackageRespDetls) {
            this.eaiHeader = eaiHeader;
            this.retrieveEligiblePackageRespDetls = retrieveEligiblePackageRespDetls;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface retrieveEligiblePackageService_PortTypeChannel : Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageService_PortType, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class retrieveEligiblePackageService_PortTypeClient : System.ServiceModel.ClientBase<Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageService_PortType>, Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageService_PortType {
        
        public retrieveEligiblePackageService_PortTypeClient() {
        }
        
        public retrieveEligiblePackageService_PortTypeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public retrieveEligiblePackageService_PortTypeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public retrieveEligiblePackageService_PortTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public retrieveEligiblePackageService_PortTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageResponse Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageService_PortType.retrieveEligiblePackage(Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageRequest request) {
            return base.Channel.retrieveEligiblePackage(request);
        }
        
        public Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.typeRetrieveEligiblePackageResponse retrieveEligiblePackage(ref Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.eaiHeader eaiHeader, Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.typeRetrieveEligiblePackageRequest retrieveEligiblePackageReqDetls) {
            Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageRequest inValue = new Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageRequest();
            inValue.eaiHeader = eaiHeader;
            inValue.retrieveEligiblePackageReqDetls = retrieveEligiblePackageReqDetls;
            Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageResponse retVal = ((Online.Registration.IntegrationService.MaxisRetrieveEligiblePackageService.retrieveEligiblePackageService_PortType)(this)).retrieveEligiblePackage(inValue);
            eaiHeader = retVal.eaiHeader;
            return retVal.retrieveEligiblePackageRespDetls;
        }
    }
}
