﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Online.Registration.IntegrationService.MaxisReassignVAS {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/", ConfigurationName="MaxisReassignVAS.reassignVasService_PortType")]
    public interface reassignVasService_PortType {
        
        // CODEGEN: Generating message contract since the wrapper name (reassignVasRequest) of message reassignVasRequest does not match the default value (reassignVas)
        [System.ServiceModel.OperationContractAttribute(Action="maxis_eai_process_common_ws_reassignVasService_Binder_reassignVas", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasResponse reassignVas(Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/common/soap/v1/")]
    public partial class eaiHeader : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string fromField;
        
        private string toField;
        
        private string appIdField;
        
        private string msgTypeField;
        
        private string msgIdField;
        
        private string correlationIdField;
        
        private string timestampField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string from {
            get {
                return this.fromField;
            }
            set {
                this.fromField = value;
                this.RaisePropertyChanged("from");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string to {
            get {
                return this.toField;
            }
            set {
                this.toField = value;
                this.RaisePropertyChanged("to");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string appId {
            get {
                return this.appIdField;
            }
            set {
                this.appIdField = value;
                this.RaisePropertyChanged("appId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string msgType {
            get {
                return this.msgTypeField;
            }
            set {
                this.msgTypeField = value;
                this.RaisePropertyChanged("msgType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string msgId {
            get {
                return this.msgIdField;
            }
            set {
                this.msgIdField = value;
                this.RaisePropertyChanged("msgId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=5)]
        public string correlationId {
            get {
                return this.correlationIdField;
            }
            set {
                this.correlationIdField = value;
                this.RaisePropertyChanged("correlationId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=6)]
        public string timestamp {
            get {
                return this.timestampField;
            }
            set {
                this.timestampField = value;
                this.RaisePropertyChanged("timestamp");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/")]
    public partial class reassignVasList : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string oldPackageIdField;
        
        private string oldComponentIdField;
        
        private string newPackageIdField;
        
        private string newComponentIdField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string oldPackageId {
            get {
                return this.oldPackageIdField;
            }
            set {
                this.oldPackageIdField = value;
                this.RaisePropertyChanged("oldPackageId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=1)]
        public string oldComponentId {
            get {
                return this.oldComponentIdField;
            }
            set {
                this.oldComponentIdField = value;
                this.RaisePropertyChanged("oldComponentId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=2)]
        public string newPackageId {
            get {
                return this.newPackageIdField;
            }
            set {
                this.newPackageIdField = value;
                this.RaisePropertyChanged("newPackageId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=3)]
        public string newComponentId {
            get {
                return this.newComponentIdField;
            }
            set {
                this.newComponentIdField = value;
                this.RaisePropertyChanged("newComponentId");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/")]
    public partial class typeReassignVasResponse : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string msgCodeField;
        
        private string msgDescField;
        
        private reassignVasList[] reassignVasListField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string msgCode {
            get {
                return this.msgCodeField;
            }
            set {
                this.msgCodeField = value;
                this.RaisePropertyChanged("msgCode");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=1)]
        public string msgDesc {
            get {
                return this.msgDescField;
            }
            set {
                this.msgDescField = value;
                this.RaisePropertyChanged("msgDesc");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("reassignVasList", IsNullable=true, Order=2)]
        public reassignVasList[] reassignVasList {
            get {
                return this.reassignVasListField;
            }
            set {
                this.reassignVasListField = value;
                this.RaisePropertyChanged("reassignVasList");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/")]
    public partial class packagePairList : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string oldPackageIdField;
        
        private string newPackageIdField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string oldPackageId {
            get {
                return this.oldPackageIdField;
            }
            set {
                this.oldPackageIdField = value;
                this.RaisePropertyChanged("oldPackageId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=1)]
        public string newPackageId {
            get {
                return this.newPackageIdField;
            }
            set {
                this.newPackageIdField = value;
                this.RaisePropertyChanged("newPackageId");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.81.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/")]
    public partial class typeReassignVasRequest : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string fxAcctNoField;
        
        private string fxSubscrNoField;
        
        private string fxSubscrNoResetsField;
        
        private packagePairList[] packagePairListField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string fxAcctNo {
            get {
                return this.fxAcctNoField;
            }
            set {
                this.fxAcctNoField = value;
                this.RaisePropertyChanged("fxAcctNo");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=1)]
        public string fxSubscrNo {
            get {
                return this.fxSubscrNoField;
            }
            set {
                this.fxSubscrNoField = value;
                this.RaisePropertyChanged("fxSubscrNo");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=2)]
        public string fxSubscrNoResets {
            get {
                return this.fxSubscrNoResetsField;
            }
            set {
                this.fxSubscrNoResetsField = value;
                this.RaisePropertyChanged("fxSubscrNoResets");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("packagePairList", IsNullable=true, Order=3)]
        public packagePairList[] packagePairList {
            get {
                return this.packagePairListField;
            }
            set {
                this.packagePairListField = value;
                this.RaisePropertyChanged("packagePairList");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="reassignVasRequest", WrapperNamespace="http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/", IsWrapped=true)]
    public partial class reassignVasRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://schemas.maxis.com.my/EAI/common/soap/v1/")]
        public Online.Registration.IntegrationService.MaxisReassignVAS.eaiHeader eaiHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/", IsNullable=true)]
        public Online.Registration.IntegrationService.MaxisReassignVAS.typeReassignVasRequest reassignVasReqDetls;
        
        public reassignVasRequest() {
        }
        
        public reassignVasRequest(Online.Registration.IntegrationService.MaxisReassignVAS.eaiHeader eaiHeader, Online.Registration.IntegrationService.MaxisReassignVAS.typeReassignVasRequest reassignVasReqDetls) {
            this.eaiHeader = eaiHeader;
            this.reassignVasReqDetls = reassignVasReqDetls;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="reassignVasResponse", WrapperNamespace="http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/", IsWrapped=true)]
    public partial class reassignVasResponse {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://schemas.maxis.com.my/EAI/common/soap/v1/")]
        public Online.Registration.IntegrationService.MaxisReassignVAS.eaiHeader eaiHeader;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/", IsNullable=true)]
        public Online.Registration.IntegrationService.MaxisReassignVAS.typeReassignVasResponse reassignVasRespDetls;
        
        public reassignVasResponse() {
        }
        
        public reassignVasResponse(Online.Registration.IntegrationService.MaxisReassignVAS.eaiHeader eaiHeader, Online.Registration.IntegrationService.MaxisReassignVAS.typeReassignVasResponse reassignVasRespDetls) {
            this.eaiHeader = eaiHeader;
            this.reassignVasRespDetls = reassignVasRespDetls;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface reassignVasService_PortTypeChannel : Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasService_PortType, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class reassignVasService_PortTypeClient : System.ServiceModel.ClientBase<Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasService_PortType>, Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasService_PortType {
        
        public reassignVasService_PortTypeClient() {
        }
        
        public reassignVasService_PortTypeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public reassignVasService_PortTypeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public reassignVasService_PortTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public reassignVasService_PortTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasResponse Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasService_PortType.reassignVas(Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasRequest request) {
            return base.Channel.reassignVas(request);
        }
        
        public Online.Registration.IntegrationService.MaxisReassignVAS.typeReassignVasResponse reassignVas(ref Online.Registration.IntegrationService.MaxisReassignVAS.eaiHeader eaiHeader, Online.Registration.IntegrationService.MaxisReassignVAS.typeReassignVasRequest reassignVasReqDetls) {
            Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasRequest inValue = new Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasRequest();
            inValue.eaiHeader = eaiHeader;
            inValue.reassignVasReqDetls = reassignVasReqDetls;
            Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasResponse retVal = ((Online.Registration.IntegrationService.MaxisReassignVAS.reassignVasService_PortType)(this)).reassignVas(inValue);
            eaiHeader = retVal.eaiHeader;
            return retVal.reassignVasRespDetls;
        }
    }
}
