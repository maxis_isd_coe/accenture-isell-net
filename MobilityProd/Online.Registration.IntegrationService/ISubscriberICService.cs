﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.RetrieveServiceInfo;
using Online.Registration.IntegrationService.MaxisChkFxVasDependencyService;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISubscriberICService" in both code and config file together.
    [ServiceContract]
    public interface ISubscriberICService
    {

        //[OperationContract]
        //Dictionary<string, string> GetPrepaidSubscriberDetails(List<string> MSISDN);
        [OperationContract]
        string GetPrepaidSubscriberDetails(string MSISDN);
        [OperationContract]
        Dictionary<string, string> retrieveBillingInfoAll(List<string> acctNos);
        [OperationContract]
        retrieveDirectoryNumberResponse retrieveMobilenos(string salesChannelId, string inventoryTypeId, int rowNum);
        [OperationContract]
        retrieveAcctListByICResponse retrieveAcctListByIC(string icType, string icValue, string loggedUserName, ref string serviceException);
        [OperationContract]
        CustomizedCustomer retrieveSubscriberDetls(string Msisdn, string loggedUserName, bool isSupAdmin = false);
        [OperationContract]
        SubscriberRetrieveServiceInfoResponse retrieveServiceInfo(SubscriberRetrieveServiceInfoRequest request);
        [OperationContract]
        retrievegetPrinSuppResponse getPrinSupplimentarylines(SubscribergetPrinSuppRequest request, bool prinFlag = false);
        [OperationContract]
        IList<PackageModel> retrievePackageDetls(string MSISDN, string kenancode, string userName = "", string password = "", string postUrl = "");
        [OperationContract]
        retrieveSimDetlsResponse retrieveSimDetails(string MSISDN);
        [OperationContract]
        retrieveMismDetlsResponse retrieveMsimDetails(string MSISDN);

        [OperationContract]
        retrieveAcctListByICResponse retrieveAllDetails(string serviceException, string loggedUserName, string icType, string icValue, string kenancode, string userName = "", string password = "", string postUrl = "", bool isSupAdmin = false);

        [OperationContract]
        retrieveAcctListByICResponse RetrieveUCCSInfo(string IDType, string IDNumber, retrieveAcctListByICResponse AcctListByICResponse);

        #region Added by Sindhu on 25 Jul 2013
        [OperationContract]
        CustomizedCustomer retrieveSubscriberDetlsbyKenan(string transType, string transValue, string loggedUserName, bool isSupAdmin = false);
        #endregion

        #region Added by Sindhu on 4-Jun-2013 for Billing Info
        [OperationContract]
        retrieveBillngInfoResponse retrieveBillingInfo(string acctNo);
        #endregion

        #region "Log PPID Req and Res"
        [OperationContract]
        SaveCallStatusResp APISaveCallStatus(SaveCallStatusReq oReq);
        #endregion

        [OperationContract]
        typeChkFxVasDependencyResponse ChkFxVasDependency(typeChkFxVasDependencyRequest req);

        [OperationContract]
        ReassignVasResponce ReassignVas(ReassignVasRequest req);

        [OperationContract]
        AvalablePackages RetrieveEligiblePackages(int currentPackageId, int mktCode, int acctCategoryId, int rateClass);


        #region smart related methods

        [OperationContract]
        IList<retrievePenaltyByMSISDN> retrievePenalty_Smart(string MSISDN, string ExternalID, string SubscriberNo, string SubscriberNoResets);
        [OperationContract]
        List<retrievePenalty> retrievePenalty(string MSISDN, string ExternalID, string SubscriberNo, string SubscriberNoResets, string userName = "", string password = "");

        #endregion

        #region "Extra Ten Methods"
        [OperationContract]
        ExtraTenRetreivalResponse RetrieveExtraTen(ExtraTenRetrieveService.typeRetrieveExtraTenRequest request);

        [OperationContract]
        ExtraTenUpdateResponse ProcessExtraTen(Online.Registration.IntegrationService.ExtraTenWS.typeProcessExtraTenRequest extraTenRequest, int regID);
        #endregion

        [OperationContract]
        MaxisRetrieveMasterAccountWs.typeRetrieveMasterAcctListResponse RetrieveMasterAccountList(string TranxType, string TranxValue);

        [OperationContract]
        MaxisRetrieveParentAccountWs.typeRetrieveParentAcctListResponse RetrieveParentAccountList(string MasterAccountNumber);

        [OperationContract]
        CustomizedCustomer retrieveSubscriberDetlsByAcctNo(string acctNo, string loggedUserName, bool isSupAdmin);

        [OperationContract]
        AccountDetails RetrieveAccountDetails(string acctNo);
    }
}
