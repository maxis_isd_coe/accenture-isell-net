﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Configuration;
using log4net;

using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.Helper;
using Online.Registration.IntegrationService.PegaServiceWS;

using System.Xml.Linq;
using SNT.Utility;
using System.Globalization;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Admin;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PegaService" in code, svc and config file together.
    public class PegaService : IPegaService
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(PegaService));

        public ExecuteDefaultProfileResponse ExecuteDefaultProfile(ExecuteDefaultProfileRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            Logger.InfoFormat("Entering (ExecuteDefaultProfile:{0})", request.subscrNo.ToString());

            PegaServiceWS.DefaultProfileResult responseFromWS = new PegaServiceWS.DefaultProfileResult();
            var filteredResponse = new ExecuteDefaultProfileResponse();

            try
            {
                using (var proxy = new PegaServiceWS.PROJ_RA_105650InterClient())
                {
                    // as per requested from PEGA, need append _0 for caseID
                    request.subscrNo += "_0";
                  
                    responseFromWS = proxy.executeDefaultProfile(request.subscrNo,null);
                }
                Logger.InfoFormat("Customer Found ? :{0}, :{1}", request.subscrNo.ToString(), responseFromWS._customerValidation.customerFound);

                if (!ReferenceEquals(responseFromWS, null) && responseFromWS._customerValidation.customerFound)
                {
                    // map result from PEGA
                    var elements = responseFromWS._profile.panels.Where(x => x.title.ToUpper().Equals("SUBSCRIBER PROFILE")).Select(y => y.elementGroups).SingleOrDefault().ToList();
                    var customerLevel = elements.Where(x => !string.IsNullOrEmpty(x.title) && x.title.ToUpper().Equals("CUSTOMER")).Select(y => y.dataElements).SingleOrDefault().ToList();
                    var accountLevel = elements.Where(x => !string.IsNullOrEmpty(x.title) && x.title.ToUpper().Equals("ACCOUNT DETAILS")).Select(y => y.dataElements).SingleOrDefault().ToList();
                    var usageSummary = responseFromWS._profile.usagePanel.usageElements;

                    filteredResponse.CustomerFound = responseFromWS._customerValidation.customerFound;
                    // usage Summary
                    filteredResponse.AverageCall = Math.Round(usageSummary.Where(x => x.name.ToUpper().Contains("VOICE")).Select(y => y.value).Single().ToDouble(), 2);
                    filteredResponse.AverageData = Math.Round(usageSummary.Where(x => x.name.ToUpper().Contains("DATA")).Select(y => y.value).Single().ToDouble(), 2);
                    filteredResponse.AverageSMS = Math.Round(usageSummary.Where(x => x.name.ToUpper().Contains("SMS")).Select(y => y.value).Single().ToDouble(), 2);
                    filteredResponse.TrendCall = Math.Round(usageSummary.Where(x => x.name.ToUpper().Contains("VOICE")).Select(y => y.trendPercentage).Single().ToDouble(), 2);
                    filteredResponse.TrendData = Math.Round(usageSummary.Where(x => x.name.ToUpper().Contains("DATA")).Select(y => y.trendPercentage).Single().ToDouble(), 2);
                    filteredResponse.TrendSMS = Math.Round(usageSummary.Where(x => x.name.ToUpper().Contains("SMS")).Select(y => y.trendPercentage).Single().ToDouble(), 2);

                    // checking gor NaN result
                    filteredResponse.AverageCall = checkForNan(filteredResponse.AverageCall);
                    filteredResponse.AverageData = checkForNan(filteredResponse.AverageData);
                    filteredResponse.AverageSMS = checkForNan(filteredResponse.AverageSMS);
                    filteredResponse.TrendCall = checkForNan(filteredResponse.TrendCall);
                    filteredResponse.TrendData = checkForNan(filteredResponse.TrendData);
                    filteredResponse.TrendSMS = checkForNan(filteredResponse.TrendSMS);

                    // customer level 
                    filteredResponse.DeviceBrand = customerLevel.Where(x => x.label.ToUpper().Contains("HANDSET BRAND")).Select(y => y.content).Single().ToString2();
                    filteredResponse.DeviceModel = customerLevel.Where(x => x.label.ToUpper().Contains("HANDSET MODEL")).Select(y => y.content).Single().ToString2();

                    // account level
                    filteredResponse.CurrentPlan = accountLevel.Where(x => x.label.ToUpper().Contains("MAIN PLAN")).Select(y => y.content).Single().ToString2();
                    filteredResponse.CustomerTenure = accountLevel.Where(x => x.label.ToUpper().Contains("CONTRACT TENURE")).Select(y => y.content).Single().ToString2();
                    filteredResponse.DurationToEnd = accountLevel.Where(x => x.label.ToUpper().Contains("DURATION TO END")).Select(y => y.content).Single().ToString2();
                    //filteredResponse.LineActiveDuration = accountLevel.Where(x => x.label.ToUpper().Contains("CONTRACT TENURE")).Select(y => y.content).Single().ToString2();
                    int contract = 0;
                    int endDuration = 0;
                    contract = !string.IsNullOrEmpty(filteredResponse.CustomerTenure) ? int.Parse(filteredResponse.CustomerTenure) : 0;
                    endDuration = !string.IsNullOrEmpty(filteredResponse.DurationToEnd) ? int.Parse(filteredResponse.DurationToEnd) : 0;
                    filteredResponse.LineActiveDuration = (contract - endDuration).ToString2();
                    filteredResponse.ContractEndDate = accountLevel.Where(x => x.label.ToUpper().Contains("CONTRACT END DATE")).Select(y => y.content).Single().ToString2();
                    filteredResponse.AverageBillAmountLine = accountLevel.Where(x => x.label.ToUpper().Contains("LINE LEVEL")).Select(y => y.content).Single().ToString2();
                    filteredResponse.AverageBillAmountAcct = accountLevel.Where(x => x.label.ToUpper().Contains("ACCOUNT LEVEL")).Select(y => y.content).Single().ToString2();
                    filteredResponse.PrincipalMSISDN = accountLevel.Where(x => x.label.ToUpper().Contains("PRINCIPAL MSISDN")).Select(y => y.content).Single().ToString2();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }

            Logger.InfoFormat("Exiting (ExecuteDefaultProfile:{0}, response:{1})", request.subscrNo.ToString(), XMLHelper.ConvertObjectToXml(filteredResponse));

            return filteredResponse;
        }

		public ExecuteAssessmentResponse ExecuteAssessment(ExecuteAssessmentRequest request)
		{
			ExecuteAssessmentResponse response = new ExecuteAssessmentResponse();
			AssessmentResult responseFromWS = new AssessmentResult();
			SessionDataStore objSessionDataStore = new SessionDataStore();

			try
			{
				objSessionDataStore = constructSessionDataStore(request);
				using (var proxy = new PegaServiceWS.PROJ_RA_105650InterClient())
				{
					responseFromWS = proxy.executeAssessment(request.Case_id, objSessionDataStore);
					Logger.InfoFormat("executeAssessment request Case ID : {0}", request.Case_id);
					Logger.InfoFormat("executeAssessment request MSISDN ID : {0}", request.ExternalID);
					Logger.InfoFormat("executeAssessment request objSessionDataStore : {0}", XMLHelper.ConvertObjectToXml(objSessionDataStore));
					Logger.InfoFormat("executeAssessment response : {0}", XMLHelper.ConvertObjectToXml(responseFromWS));
				}
				if (!ReferenceEquals(responseFromWS, null))
				{
					int value;
					response.Code = "0";
					response.Success = true;
					response.Message = "success";
					response.caseID = responseFromWS._assessment.caseId != null ? responseFromWS._assessment.caseId : request.Case_id;
					response.DecisionResultId = responseFromWS._decisionResultId;
					response.DecisionID = responseFromWS.responseHolder.decisionID;
					response.ProjectName = responseFromWS.responseHolder.projectName;
					response.Channel = responseFromWS._VBD.channel;
					response.Customer = responseFromWS._VBD.customer;
					if (!ReferenceEquals(responseFromWS._assessment.topProducts,null))
					{
						int index = 0;
						foreach (var topProduct in responseFromWS._assessment.topProducts)
						{
							if (!string.Equals(topProduct.id, "no_offer") && topProduct.relevant == true)
							{
								PegaProducts _pegaProduct = new PegaProducts();
								_pegaProduct.PegaIndex = index.ToString();
								_pegaProduct.Category = "TOP";
								_pegaProduct.KenanCode = topProduct.adviceShort;
								_pegaProduct.PrepositionID = topProduct.id;
								_pegaProduct.Name = topProduct.name;
								_pegaProduct.Relevant = topProduct.relevant;
								
								response.productList.Add(_pegaProduct);
							}
							
							index++;
						}
					}
					
					if (!ReferenceEquals(responseFromWS._assessment.products,null))
					{
						int index = 0;
						foreach (var product in responseFromWS._assessment.products)
						{
							if (!string.Equals(product.id, "no_offer") && product.relevant == true)
							{
								if (!response.productList.Where(x => x.PrepositionID == product.id).Any())
								{
									PegaProducts _pegaProduct = new PegaProducts();
									_pegaProduct.PegaIndex = index.ToString();
									_pegaProduct.Category = "ALL";
									_pegaProduct.KenanCode = product.adviceShort;
									_pegaProduct.PrepositionID = product.id;
									_pegaProduct.Name = product.name;
									_pegaProduct.Relevant = product.relevant;
									
									response.productList.Add(_pegaProduct);
								}
							}
							
							index++;
						}
					}
					#region dummy product
					bool dummyOffer = Convert.ToBoolean(ConfigurationManager.AppSettings["PegaDummyOffer"]);
					if (dummyOffer)
					{
						PegaProducts _topProduct = new PegaProducts();
						_topProduct.KenanCode = "0000";
						_topProduct.PrepositionID = "9999999";
						_topProduct.Name = "DUMMY PRODUCT";

						response.productList.Add(_topProduct);
					}
					#endregion

					#region Added by Gerry for logging
					PegaLogDetails objPegaLogDetails = new PegaLogDetails();
					objPegaLogDetails.Msisdn = request.ExternalID;
					objPegaLogDetails.SubscriberNo = request.Case_id;
					objPegaLogDetails.SalesPerson = request.SalesPerson;
					objPegaLogDetails.MethodName = "ExecuteAssessment";
					objPegaLogDetails.PegaXmlReq = XMLHelper.ConvertObjectToXml(objSessionDataStore);
					objPegaLogDetails.PegaXmlRes = XMLHelper.ConvertObjectToXml(response);

					OnlineRegAdmin.SavePegaXMLLogs(objPegaLogDetails);
					#endregion
				
				}

				return response;
			}
			catch (TimeoutException ex)
			{
				PopulateExceptionResponse(response, ex);
				response.Message = "TIMEOUT";

				#region Added by Gerry for logging
				PegaLogDetails objPegaLogDetails = new PegaLogDetails();
				objPegaLogDetails.Msisdn = request.ExternalID;
				objPegaLogDetails.SubscriberNo = request.Case_id;
				objPegaLogDetails.SalesPerson = request.SalesPerson;
				objPegaLogDetails.MethodName = "ExecuteAssessment";
				objPegaLogDetails.PegaXmlReq = XMLHelper.ConvertObjectToXml(objSessionDataStore);
				objPegaLogDetails.PegaXmlRes = ex.Message;

				OnlineRegAdmin.SavePegaXMLLogs(objPegaLogDetails);
				#endregion

				return response;
			}
			catch (Exception ex)
			{
				PopulateExceptionResponse(response, ex);
				response.Message = "Technical problem occured during this request. Please try again later.";
				Logger.Error( string.Format("Error on ExecuteAssessment for MSISDN {0}, Exception {1}",request.ExternalID, Util.LogException(ex)) );
				#region Added by Gerry for logging
				PegaLogDetails objPegaLogDetails = new PegaLogDetails();
				objPegaLogDetails.Msisdn = request.ExternalID;
				objPegaLogDetails.SubscriberNo = request.Case_id;
				objPegaLogDetails.SalesPerson = request.SalesPerson;
				objPegaLogDetails.MethodName = "ExecuteAssessment";
				objPegaLogDetails.PegaXmlReq = XMLHelper.ConvertObjectToXml(objSessionDataStore); ;
				objPegaLogDetails.PegaXmlRes = ex.Message;

				OnlineRegAdmin.SavePegaXMLLogs(objPegaLogDetails);
				#endregion
				return response;
			}
		}

		public UpdateResponseResponse UpdateResponse(UpdateResponseRequest request)
		{
			UpdateResponseResponse response = new UpdateResponseResponse();
			//AssessmentResult responseFromWS = new AssessmentResult();
			decisionResponseHelper _decisionResponseHelper = new decisionResponseHelper();
			try
			{
				_decisionResponseHelper = constructDecisionResponse(request);
				using (var proxy = new PegaServiceWS.PROJ_RA_105650InterClient())
				{
					var responseFromWS = proxy.updateResponse(request.DecisionResultId, _decisionResponseHelper);
					
					#region Added by Gerry for logging
					PegaLogDetails objPegaLogDetails = new PegaLogDetails();
					objPegaLogDetails.Msisdn = request.ExternalID;
					objPegaLogDetails.SubscriberNo = request.CaseID;
					objPegaLogDetails.SalesPerson = request.Application;
					objPegaLogDetails.MethodName = "UpdateResponse";
					objPegaLogDetails.PegaXmlReq = XMLHelper.ConvertObjectToXml(_decisionResponseHelper);
					objPegaLogDetails.PegaXmlRes = XMLHelper.ConvertObjectToXml(responseFromWS);
					objPegaLogDetails.Remarks = "DecisionResultId : " + request.DecisionResultId;

					Online.Registration.DAL.Admin.OnlineRegAdmin.SavePegaXMLLogs(objPegaLogDetails);
					#endregion

					if (responseFromWS != null && responseFromWS.Any())
					{
						response.Code = "0";
						response.Message = string.Format("success-{0}",responseFromWS.FirstOrDefault().ToString2());
						response.Success = true;
					}
					else {
						response.Code = "1";
						response.Message = "Pega response with unexpected value";
						response.Success = false ;
					}
				}

				return response;
			}
			catch (Exception ex)
			{
				PopulateExceptionResponse(response, ex);
				Logger.Error(string.Format("Error on UpdateResponse for MSISDN {0}, Exception {1}", request.ExternalID, Util.LogException(ex)));
				#region Added by Gerry for logging
				PegaLogDetails objPegaLogDetails = new PegaLogDetails();
				objPegaLogDetails.Msisdn = request.ExternalID;
				objPegaLogDetails.SubscriberNo = request.CaseID;
				objPegaLogDetails.SalesPerson = request.Application;
				objPegaLogDetails.MethodName = "UpdateResponse";
				objPegaLogDetails.PegaXmlReq = XMLHelper.ConvertObjectToXml(_decisionResponseHelper);
				objPegaLogDetails.PegaXmlRes = ex.Message;

				OnlineRegAdmin.SavePegaXMLLogs(objPegaLogDetails);
				#endregion
				return response;
			}
		
		}
		
        #region Private Methods
		private SessionDataStore constructSessionDataStore(ExecuteAssessmentRequest request)
		{
			return new SessionDataStore
			{
				callReason = request.CallReason,
				mxChannel = request.MxChannel
			};
		}

		private decisionResponseHelper constructDecisionResponse (UpdateResponseRequest request)
		{
			List<interactionResponse> objInteractionResponseList = new List<interactionResponse>();
			objInteractionResponseList = constructInteractionResponse(request);
			var arrObjInteractionResponse = objInteractionResponseList.ToArray();

			return new decisionResponseHelper
			{	
				customDimensionItem = new customDimensionItemWS()
				{
					category = request.Category,
					reason = request.Reason,
					displayCategory = request.DisplayCategory,
					topRank = request.TopRank
				},
				responseHolder = new responseHolder()
				{
					decisionID = request.DecisionID,
					projectName = request.ProjectName
				},
				vbd = new vbdFQN()
				{
					application = request.Application,
					channel = request.Channel,
					customer = request.Customer
				},
				responses = arrObjInteractionResponse
			};
		}
		
		private List<interactionResponse> constructInteractionResponse(UpdateResponseRequest request)
		{ 
			List<interactionResponse> response = new List<interactionResponse>();

			interactionResponse _interactionResponse = new interactionResponse();
			_interactionResponse.propositionIdentifier = request.PropositionIdentifier;
			_interactionResponse.response = request.Response; //accepted / later
			_interactionResponse.caseID = request.CaseID;
			response.Add(_interactionResponse);

			return response;
		}

        private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
        {
            resp.Code = "-1";
            resp.Message = ex.Message;
            resp.Success = false;
        }

        private double checkForNan(double input)
        {
            double d = 0;
            if (double.IsNaN(input))
            {
                return d;
            }
            return input;
        }

		private bool isValidNumber (string input)
		{
			return System.Text.RegularExpressions.Regex.IsMatch(input, @"^\d+$");
		}

        #endregion
    }
}
