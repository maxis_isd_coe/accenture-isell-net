﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net;

using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.Helper;
using Online.Registration.IntegrationService.MaxisCrmCreateCrmInteractionCaseWs;

using log4net;
using System.Xml.Linq;

namespace Online.Registration.IntegrationService
{
    public class CMSSService : ICMSSService
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CMSSService));

        #region Private Methods

        private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
        {
            resp.Code = "-1";
            resp.Message = ex.Message;
            resp.Success = false;
        }

        #endregion

        public CreateInteractionCaseResponse CreateInteractionCase(CreateInteractionCaseRequest request)
        {
            string userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            string password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;
            string postData = String.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:v11=""http://services.maxis.com.my/EAI/CRM/transactionService/v1/"" xmlns:v12=""http://schemas.maxis.com.my/EAI/CRM/transaction/v1/"">
           <soapenv:Header>
              <v1:eaiHeader>
                 <v1:from>ISELL</v1:from>
                 <v1:to>ESB</v1:to>
                 <v1:appId>OPF</v1:appId>
                 <v1:msgType>Request</v1:msgType>
                 <v1:msgId>ISELL423423</v1:msgId>
                 <!--Optional:-->
                 <v1:correlationId></v1:correlationId>
                 <v1:timestamp>20140113111000</v1:timestamp>
              </v1:eaiHeader>
           </soapenv:Header>
           <soapenv:Body>
              <v11:createCrmInteractionCaseDetailRequest>
                 <v12:createCrmInteractionCaseDetailRequestDetails>
                    <v12:channel>ISELL</v12:channel>
                    <!--Optional:-->
                    <v12:msisdn></v12:msisdn>
                    <v12:acctExtId>{0}</v12:acctExtId>
                    <v12:userId>{14}</v12:userId> 
                    <v12:title>{1}</v12:title>
                    <v12:sysName>{2}</v12:sysName>
                    <v12:product>{3}</v12:product>
                    <v12:reason1>{4}</v12:reason1>
                    <v12:reason2>{5}</v12:reason2>
                    <v12:reason3>{6}</v12:reason3>
                    <v12:type>{7}</v12:type>
                    <v12:direction>{8}</v12:direction>
                    <v12:notes>{9}</v12:notes>
                    <v12:toProceedWith>{10}</v12:toProceedWith>        
               
                    <v12:caseDoc>
                       <v12:casePriority>{11}</v12:casePriority>
                       <v12:caseSeverity></v12:caseSeverity>
                       <!--Optional:-->
                       <v12:caseCallType></v12:caseCallType>
                       <v12:caseComplexity>{12}</v12:caseComplexity>
                       <v12:caseDispatchQ>{13}</v12:caseDispatchQ>
                    </v12:caseDoc>
                 </v12:createCrmInteractionCaseDetailRequestDetails>
              </v11:createCrmInteractionCaseDetailRequest>
           </soapenv:Body>
        </soapenv:Envelope>",
                            request.AcctExtId, request.Title.Replace(">", "&gt;").Replace("<", "&lt;").Replace("'", "&quot;"), request.SysName.Replace(">", "&gt;").Replace("<", "&lt;").Replace("'", "&quot;"), (request.Product.Replace(">", "&gt;").Replace("<", "&lt;").Replace("'", "&quot;")),
                           (request.Reason1.Replace(">", "&gt;").Replace("<", "&lt;").Replace("'", "&quot;")), (request.Reason2.Replace(">", "&gt;").Replace("<", "&lt;").Replace("'", "&quot;")), (request.Reason3.Replace(">", "&gt;").Replace("<", "&lt;").Replace("'", "&quot;")), (request.Type.Replace(">", "&gt;").Replace("<", "&lt;").Replace("'", "&quot;")),
                           (request.Direction.Replace(">", "&gt;").Replace("<", "&lt;").Replace("'", "&quot;")), (request.Notes.Replace(">", "&gt;").Replace("<", "&lt;").Replace("'", "&quot;")), request.ToProceedWith, request.CasePriority, request.CaseComplexity, (request.CaseDispatchQ.Replace(">", "&gt;").Replace("<", "&lt;").Replace("'", "&quot;")).Replace("&", "&#038;"), request.UserId);
                            

            string dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
                              <SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
   <SOAP-ENV:Header>
      <HDR:eaiHeader xmlns:HDR=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">
         <eai:from xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/""/>
         <eai:to xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/""/>
         <eai:appId xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/""/>
         <eai:msgType xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/""/>
         <eai:msgId xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/""/>
         <eai:timestamp xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">20140116112832</eai:timestamp>
      </HDR:eaiHeader>
   </SOAP-ENV:Header>
   <SOAP-ENV:Body>
      <ser-root:createCrmInteractionCaseDetailResponse xmlns:ser-root=""http://services.maxis.com.my/EAI/CRM/transactionService/v1/"">
         <tranx:createCrmInteractionCaseDetailResponseDetails xmlns:tranx=""http://schemas.maxis.com.my/EAI/CRM/transaction/v1/"">
            <tranx:msgCode>0</tranx:msgCode>
            <tranx:msgDesc>Success</tranx:msgDesc>
            <tranx:case>
               <tranx:returnCode>I001</tranx:returnCode>
               <tranx:returnMsg xsi:nil=""true""/>
               <tranx:id>C13754280</tranx:id>
            </tranx:case>
         </tranx:createCrmInteractionCaseDetailResponseDetails>
      </ser-root:createCrmInteractionCaseDetailResponse>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>";

            WebClient CreateInteractionCaseClient = new WebClient();
            Uri postUri = new Uri(System.Configuration.ConfigurationManager.AppSettings["createCrmInteractionCaseDetailService"]);
            //Uri postUri = new Uri("http://10.200.35.78:7030/ws/maxis.eai.process.crm.ws:createCrmInteractionCaseDetailService/maxis_eai_process_crm_ws_createCrmInteractionCaseDetailService_Port");
            string postResult = string.Empty;

            CreateInteractionCaseClient.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
            CreateInteractionCaseClient.Headers.Add("content-type", "text/xml");

            CreateInteractionCaseResponse response = null;

            try
            {

                //Depending upon settings if true then use dummy data
                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    postResult = dummyReplyData;
                }
                else
                {
                    postResult = CreateInteractionCaseClient.UploadString(postUri, postData);
                }

                postResult = Util.RemoveAllNamespaces(postResult);
                Logger.Info(String.Format(" MaxisCrmCreateCrmInteractionCaseWs Request : {0} ", postData));
                Logger.Info(String.Format(" MaxisCrmCreateCrmInteractionCaseWs Response: {0} ", postResult));

                XDocument doc = XDocument.Parse(postResult);
                //Bellow code is for Parseing the xml and storing in to respetive form
                response = new CreateInteractionCaseResponse();
                foreach (var r in doc.Descendants("createCrmInteractionCaseDetailResponseDetails"))
                {
                    response = new CreateInteractionCaseResponse();
                    response.MsgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    response.MsgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
                    if (response.MsgCode == "0")
                    {
                        response.Success = true;
                        response.Id = r.Element("case").Element("id") == null ? "" : r.Element("case").Element("id").Value.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                //response = null;
            }

            return response;
        }        
    }
}
