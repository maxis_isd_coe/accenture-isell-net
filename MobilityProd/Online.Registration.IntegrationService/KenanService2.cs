﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.Helper;
using Online.Registration.IntegrationService.MaxisBillingProcessAcctDetlWs;
using Online.Registration.IntegrationService.MaxisCommonRetrieveComponentInfoWs;
using Online.Registration.IntegrationService.MaxisInternalCheckWs;
using Online.Registration.IntegrationService.MaxisDDMFCheckWs;
using Online.Registration.IntegrationService.MaxisValidationWs;
using Online.Registration.IntegrationService.MbaCreditCheckWs;
using Online.Registration.IntegrationService.MaxisOrderProcessWs;
using Online.Registration.IntegrationService.Properties;
using SNT.Utility;
using log4net;
using System.Runtime.Remoting.Messaging;
using System.Net;
using System.Xml.Linq;
using System.Globalization;
using Online.Registration.IntegrationService.ValidateInventory;
using Online.Registration.DAL.Models;
using Online.Registration.IntegrationService.RetrieveServiceInfo;
using Online.Registration.IntegrationService.MaxisChkFxVasDependencyService;
using Online.Registration.IntegrationService.MaxisgetMNPRequestServiceWs;
using Online.Registration.IntegrationService.RetrieveDealerInfoWS;
using Online.Registration.DAL.Admin;

namespace Online.Registration.IntegrationService
{
    public partial class KenanService : IKenanService
    {
        /// <summary>
        /// Method calling retrieveComponentInfo.wsdl        
        /// </summary>
        /// <param name="userName">The username. Default "iselluser"</param>
        /// <param name="password">The password. Default "iselluser"</param>
        /// <param name="postUrl">The post URL. Default "http://10.200.51.122:7030/soap/rpc"</param>
        /// <returns>NewRetrieveComponentInfoResponse</returns>
        public NewRetrieveComponentInfoResponse NewRetrieveComponentInfo(NewRetrieveComponentInfoRequest request)
        {
            string postData = string.Empty;
            string dummyReplyData = string.Empty;
            NewRetrieveComponentInfoResponse response = new NewRetrieveComponentInfoResponse();
            Uri postUri = null;
            WebClient retrieveAcctListWebClient = null;
            string postResult = string.Empty;
            string userName = Properties.Settings.Default.retrieveComponentInfoResponseUserName;
            string password = Properties.Settings.Default.retrieveComponentInfoResponsePassword;
            string postUrl = Properties.Settings.Default.retrieveComponentInfoResponseUrl;
            NewRetrieveComponentInfoResponse ObjretList = null;
            retrieveComponentInfoVas vas = null;
            retrieveComponentInfoContract component = null;
            CultureInfo enUS = new CultureInfo("en-US");
            DateTime dateValue;

            if (ReferenceEquals(request, null))
            {
                return response;
            }

            Logger.InfoFormat("Entering (ExternalId:{0},subscrNo:{1},subscrNoResets:{2})", request.externalId, request.subscrNo, request.subscrNoResets);

            postData = string.Format(@"<soapenv:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:max=""http://www.webmethods.com/maxis.eai.process.common.ws"">
                                   <soapenv:Header/>
                                   <soapenv:Body>
                                      <max:retrieveComponentInfo soapenv:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                                         <externalId xsi:type=""xsd:string"">{0}</externalId>
                                         <subscrNo xsi:type=""xsd:string"">{1}</subscrNo>
                                         <subscrNoResets xsi:type=""xsd:string"">{2}</subscrNoResets>
                                      </max:retrieveComponentInfo>
                                   </soapenv:Body>
                                </soapenv:Envelope>", request.externalId, request.subscrNo, request.subscrNoResets);

            #region DummyReply
            dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8""?>
                            <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
                               <SOAP-ENV:Body>
                                  <ser-root:retrieveComponentInfoResponse SOAP-ENC:root=""1"" xmlns:ser-root=""http://www.webmethods.com/maxis.eai.process.common.ws"">
                                     <msgCode xsi:type=""xsd:string"">0</msgCode>
                                     <msgDesc xsi:type=""xsd:string"">Valid Subscriber</msgDesc>
                                     <vas xsi:type=""SOAP-ENC:Array"" SOAP-ENC:arrayType=""sc1:__vas[27]"" id=""id1"" xmlns:sc1=""http://localhost/maxis/eai/process/common/ws/retrieveComponentInfo"">
                                        <item xsi:type=""sc1:__vas"" id=""id2"">
                                           <compId xsi:type=""xsd:string"">41739</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - GPRS Capping @ RM250</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id3"">
                                           <compId xsi:type=""xsd:string"">40021</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Access IDD Calls</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id4"">
                                           <compId xsi:type=""xsd:string"">40057</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - WAPSTK Service Pkg 1</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id5"">
                                           <compId xsi:type=""xsd:string"">40060</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - WAPSTK Service Pkg 5 (Downloads)</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id6"">
                                           <compId xsi:type=""xsd:string"">40050</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - WAPSTK Telelink (Non-Muslim)</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id7"">
                                           <compId xsi:type=""xsd:string"">40925</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - EXPIRED - MVPN 38 Hours</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id8"">
                                           <compId xsi:type=""xsd:string"">40026</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - CLIP-Caller Line Id Pres</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id9"">
                                           <compId xsi:type=""xsd:string"">40032</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - MMS Service Package</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id10"">
                                           <compId xsi:type=""xsd:string"">40023</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Short Message Service (MO)</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id11"">
                                           <compId xsi:type=""xsd:string"">41711</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Telephony</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id12"">
                                           <compId xsi:type=""xsd:string"">40041</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Caller RingTone</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id13"">
                                           <compId xsi:type=""xsd:string"">40100</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Fax Service</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id14"">
                                           <compId xsi:type=""xsd:string"">40058</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - WAPSTK Service Pkg 3 (Islamic)</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id15"">
                                           <compId xsi:type=""xsd:string"">40089</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - 3G Video Mail</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id16"">
                                           <compId xsi:type=""xsd:string"">40090</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - 3G Video Telephony</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id17"">
                                           <compId xsi:type=""xsd:string"">40082</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - GPRS Standard (UNET/3G)</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id18"">
                                           <compId xsi:type=""xsd:string"">41475</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Itemised Billing Waiver</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id19"">
                                           <compId xsi:type=""xsd:string"">40028</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Call Hold</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id20"">
                                           <compId xsi:type=""xsd:string"">40027</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Call Waiting</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id21"">
                                           <compId xsi:type=""xsd:string"">40029</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Data Service</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id22"">
                                           <compId xsi:type=""xsd:string"">41316</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Family Plus Free Talktime &amp; SMS</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id23"">
                                           <compId xsi:type=""xsd:string"">40252</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Itemised Billing</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id24"">
                                           <compId xsi:type=""xsd:string"">40030</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - WAP</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id25"">
                                           <compId xsi:type=""xsd:string"">40043</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Access International Roaming</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id26"">
                                           <compId xsi:type=""xsd:string"">40037</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Call Conferencing</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id27"">
                                           <compId xsi:type=""xsd:string"">40034</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - Call Forwarding</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                        <item xsi:type=""sc1:__vas"" id=""id28"">
                                           <compId xsi:type=""xsd:string"">41176</compId>
                                           <compDesc xsi:type=""xsd:string"">Mobile - RM30x24 Activation Contract</compDesc>
                                           <activeDt xsi:type=""xsd:string"">20090618</activeDt>
                                        </item>
                                     </vas>
                                     <contract xsi:type=""SOAP-ENC:Array"" SOAP-ENC:arrayType=""sc1:__contract[10]"" id=""id29"" xmlns:sc1=""http://localhost/maxis/eai/process/common/ws/retrieveComponentInfo"">
                                        <item xsi:type=""sc1:__contract"" id=""id30"">
                                           <contractId xsi:type=""xsd:string"">37789849</contractId>
                                           <contrName xsi:type=""xsd:string"">Mobile - Rebate for GPRS/EDGE/3G Usage (max charge @ RM250/month)</contrName>
                                           <contrType xsi:type=""xsd:string"">402186</contrType>
                                           <startDt xsi:type=""xsd:string"">20090618</startDt>
                                           <endDt xsi:type=""xsd:string"" xsi:nil=""true""/>
                                        </item>
                                        <item xsi:type=""sc1:__contract"" id=""id31"">
                                           <contractId xsi:type=""xsd:string"">36718066</contractId>
                                           <contrName xsi:type=""xsd:string"">Mobile - Call Forwarding Accessfee Waiver</contrName>
                                           <contrType xsi:type=""xsd:string"">401249</contrType>
                                           <startDt xsi:type=""xsd:string"">20090618</startDt>
                                           <endDt xsi:type=""xsd:string"">20090618</endDt>
                                        </item>
                                        <item xsi:type=""sc1:__contract"" id=""id32"">
                                           <contractId xsi:type=""xsd:string"">33612163</contractId>
                                           <contrName xsi:type=""xsd:string"">Mobile - Itemised Billing Waiver</contrName>
                                           <contrType xsi:type=""xsd:string"">401948</contrType>
                                           <startDt xsi:type=""xsd:string"">20090618</startDt>
                                           <endDt xsi:type=""xsd:string"" xsi:nil=""true""/>
                                        </item>
                                        <item xsi:type=""sc1:__contract"" id=""id33"">
                                           <contractId xsi:type=""xsd:string"">33612164</contractId>
                                           <contrName xsi:type=""xsd:string"">Mobile - Early Termination Commitment Contract</contrName>
                                           <contrType xsi:type=""xsd:string"">401755</contrType>
                                           <startDt xsi:type=""xsd:string"">20090618</startDt>
                                           <endDt xsi:type=""xsd:string"">20110618</endDt>
                                        </item>
                                        <item xsi:type=""sc1:__contract"" id=""id34"">
                                           <contractId xsi:type=""xsd:string"">33612158</contractId>
                                           <contrName xsi:type=""xsd:string"">Mobile - Family Plus Voice and Video Calls Rebate</contrName>
                                           <contrType xsi:type=""xsd:string"">401797</contrType>
                                           <startDt xsi:type=""xsd:string"">20090618</startDt>
                                           <endDt xsi:type=""xsd:string"" xsi:nil=""true""/>
                                        </item>
                                        <item xsi:type=""sc1:__contract"" id=""id35"">
                                           <contractId xsi:type=""xsd:string"">33612162</contractId>
                                           <contrName xsi:type=""xsd:string"">Mobile - Call Forwarding Accessfee Waiver</contrName>
                                           <contrType xsi:type=""xsd:string"">401249</contrType>
                                           <startDt xsi:type=""xsd:string"">20090618</startDt>
                                           <endDt xsi:type=""xsd:string"" xsi:nil=""true""/>
                                        </item>
                                        <item xsi:type=""sc1:__contract"" id=""id36"">
                                           <contractId xsi:type=""xsd:string"">33612159</contractId>
                                           <contrName xsi:type=""xsd:string"">Mobile - Family Plus SMS Rebate</contrName>
                                           <contrType xsi:type=""xsd:string"">401798</contrType>
                                           <startDt xsi:type=""xsd:string"">20090618</startDt>
                                           <endDt xsi:type=""xsd:string"" xsi:nil=""true""/>
                                        </item>
                                        <item xsi:type=""sc1:__contract"" id=""id37"">
                                           <contractId xsi:type=""xsd:string"">33612161</contractId>
                                           <contrName xsi:type=""xsd:string"">Mobile - Fax Service Registration Fee</contrName>
                                           <contrType xsi:type=""xsd:string"">400027</contrType>
                                           <startDt xsi:type=""xsd:string"">20090618</startDt>
                                           <endDt xsi:type=""xsd:string"" xsi:nil=""true""/>
                                        </item>
                                        <item xsi:type=""sc1:__contract"" id=""id38"">
                                           <contractId xsi:type=""xsd:string"">33612160</contractId>
                                           <contrName xsi:type=""xsd:string"">Mobile - Premium Talktime Waiver RM150 Contract</contrName>
                                           <contrType xsi:type=""xsd:string"">402161</contrType>
                                           <startDt xsi:type=""xsd:string"">20090618</startDt>
                                           <endDt xsi:type=""xsd:string"" xsi:nil=""true""/>
                                        </item>
                                        <item xsi:type=""sc1:__contract"" id=""id39"">
                                           <contractId xsi:type=""xsd:string"">33612340</contractId>
                                           <contrName xsi:type=""xsd:string"">Mobile - 38 Hrs MVPN Unit Credit Plan (OnNet Calls)</contrName>
                                           <contrType xsi:type=""xsd:string"">401347</contrType>
                                           <startDt xsi:type=""xsd:string"">20090618</startDt>
                                           <endDt xsi:type=""xsd:string"" xsi:nil=""true""/>
                                        </item>
                                     </contract>
                                  </ser-root:retrieveComponentInfoResponse>
                               </SOAP-ENV:Body>
                            </SOAP-ENV:Envelope>";
            #endregion DummyReply

            retrieveAcctListWebClient = new WebClient();
            postUri = new Uri(postUrl);

            retrieveAcctListWebClient.Credentials = new NetworkCredential(userName, password);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");

            try
            {

                //Depending upon settings if true then use dummy data
                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    postResult = dummyReplyData;
                }
                else
                {
                    postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                }

                postResult = Util.RemoveAllNamespaces(postResult);

                XDocument doc = XDocument.Parse(postResult);
                //Bellow code is for Parseing the xml and storing in to respetive form

                ObjretList = new NewRetrieveComponentInfoResponse();
                //1/1/0001 12:00:00 AM if not converted
                foreach (var r in doc.Descendants("retrieveComponentInfoResponse"))
                {
                    ObjretList.MsgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    ObjretList.MsgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();

                    response.MsgCode = ObjretList.MsgCode;
                    response.MsgDesc = ObjretList.MsgDesc;

                    if (!ObjretList.MsgCode.Equals("0"))//Invalid Subscriber
                    {
                        ObjretList = null;
                        break;
                    }

                    foreach (var itemsdata in r.Descendants("vas").Descendants("item"))
                    {
                        vas = new retrieveComponentInfoVas();
                        {
                            vas.ComponentId = itemsdata.Element("compId") == null ? 0 : Convert.ToInt32(itemsdata.Element("compId").Value.ToString());
                            vas.ComponentDescription = itemsdata.Element("compDesc") == null ? "" : itemsdata.Element("compDesc").Value.ToString();
                            if (!ReferenceEquals(itemsdata.Element("activeDt"), null))
                            {
                                if (DateTime.TryParseExact(itemsdata.Element("activeDt").Value.ToString(), "yyyyMMdd", enUS, DateTimeStyles.None, out dateValue))
                                {
                                    vas.ActiveDate = dateValue;
                                }
                            }
                            ObjretList.Vass.Add(vas);
                        }
                    }
                    foreach (var itemsdata in r.Descendants("contract").Descendants("item"))
                    {
                        component = new retrieveComponentInfoContract();
                        {
                            component.ContractId = itemsdata.Element("contractId") == null ? 0 : Convert.ToInt32(itemsdata.Element("contractId").Value.ToString());
                            component.ContractName = itemsdata.Element("contrName") == null ? string.Empty : itemsdata.Element("contrName").Value.ToString();
                            component.ContractType = itemsdata.Element("contrType") == null ? 0 : Convert.ToInt32(itemsdata.Element("contrType").Value.ToString());

                            if (!ReferenceEquals(itemsdata.Element("startDt"), null))
                            {
                                if (DateTime.TryParseExact(itemsdata.Element("startDt").Value.ToString(), "yyyyMMdd", enUS, DateTimeStyles.None, out dateValue))
                                {
                                    component.StartDate = dateValue;
                                }
                            }
                            if (!ReferenceEquals(itemsdata.Element("endDt"), null))
                            {
                                if (DateTime.TryParseExact(itemsdata.Element("endDt").Value.ToString(), "yyyyMMdd", enUS, DateTimeStyles.None, out dateValue))
                                {
                                    component.EndDate = dateValue;
                                }
                            }
                            ObjretList.Contracts.Add(component);
                        }
                    }
                }
                if (ObjretList != null)
                {
                    if (ObjretList.MsgCode != "0")
                    {
                        /// “0” – success
                        /// “1” - fail
                        /// “2” –system error
                        ObjretList = null;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error("", ex);
                PopulateExceptionResponse(response, ex);
                ObjretList = null;
            }
            Logger.InfoFormat("Exiting (ExternalId:{0},subscrNo:{1},subscrNoResets:{2},Success:{3})", request.externalId, request.subscrNo, request.subscrNoResets, response.Success);
            return ObjretList;
        }
        /// <summary>
        /// Deletegate pointing to InventoryCheck
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private delegate TypeValidateInventoryExtIdResponse CheckInventoryCreateDelegate(TypeValidateInventoryExtIdRequest request);
        /// <summary>
        /// Calls the validateInventoryExtIdRequest
        /// </summary>
        /// <param name="request"></param>
        /// <returns>TypeOf(bool) if valid externalid then returns true, if invalid will return false</returns>
        public bool InventoryCheck(TypeValidateInventoryExtIdRequest request)
        {
            Logger.InfoFormat("InventoryCheck({0}) called!", request);
            CheckInventoryCreateDelegate abc = new CheckInventoryCreateDelegate(ValidateInventoryExtId);
            TypeValidateInventoryExtIdResponse response = abc.Invoke(request);

            Logger.InfoFormat("InventoryCheck({0}):{1} returned!", request, true);

            ///IF UNSUCCESSFUL THEN RETURN FALSE
            if ("1".Equals(response.Code))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Method calling validateInventoryExtIdService
        /// </summary>
        /// <param name="request"></param>
        /// <returns>TypeOf(TypeValidateInventoryExtIdResponse)</returns>
        private TypeValidateInventoryExtIdResponse ValidateInventoryExtId(TypeValidateInventoryExtIdRequest request)
        {
            Logger.Info(string.Format("Entering ValidateInventoryExtId(): externalId({0})", request.externalId));

            string userName = Properties.Settings.Default.ValidateInventoryExtIdResponseUserName;
            string password = Properties.Settings.Default.ValidateInventoryExtIdResponsePassword;

            var response = new TypeValidateInventoryExtIdResponse();

            try
            {
                var inventoryCheckRq = PrepareInventoryExtIdCheck(request);

                using (var proxy = new ValidateInventory.validateInventoryExtIdService())
                {
                    proxy.Credentials = new NetworkCredential(userName, password);
                    proxy.PreAuthenticate = true;

                    proxy.eaiHeaderValue = new ValidateInventory.eaiHeader()
                    {
                        from = Properties.Resources.orderProcessReq_from,
                        to = Properties.Resources.orderProcessReq_to,
                        appId = Properties.Resources.orderProcessReq_appID,
                        msgType = Properties.Resources.orderProcessReq_msgType,
                        msgId = Convert.ToString((new Random()).Next(100)),
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss")
                    };

                    var resp = proxy.validateInventoryExtId(inventoryCheckRq);

                    if (resp.msgCode != Properties.Settings.Default.KenanSuccessCode)
                    {
                        response.Code = resp.msgCode;
                        response.Message = resp.msgDesc;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("", ex);
                PopulateExceptionResponse(response, ex);
            }
            Logger.Info(string.Format("Exiting ValidateInventoryExtId(): externalId({0})", request.externalId));

            return response;
        }

        /// <summary>
        /// Method swaps value to proxy datatype from internal datatype
        /// </summary>
        /// <param name="request">TypeOf(TypeValidateInventoryExtIdRequest)</param>
        /// <returns>TypeOf(typeValidateInventoryExtIdRequest)</returns>
        private typeValidateInventoryExtIdRequest PrepareInventoryExtIdCheck(TypeValidateInventoryExtIdRequest request)
        {
            return new typeValidateInventoryExtIdRequest()
            {
                externalId = request.externalId
            };
        }

        #region chkFxVasDependency call
        /// <summary>
        /// ChkFxVasDependency
        /// </summary>
        /// <param name="request">TypeOf(ChkFxVasDependencyRequest)</param>
        /// <returns>TypeOf(ChkFxVasDependencyResponse)</returns>
        public ChkFxVasDependencyResponse ChkFxVasDependency(ChkFxVasDependencyRequest request)
        {
            typeChkFxVasDependencyRequest CChkFxVasDependencyRequest = null;
            typeChkFxVasDependencyResponse ChkFxVasDependencyResponse = null;
            ChkFxVasDependencyResponse FxVasDependencyResponse = null;
            string xmlRequestResponse = string.Empty;

            Logger.InfoFormat("ChkFxVasDependency({0}) called!", request);

            Logger.Info(string.Format("Entering ChkFxVasDependency(): emfConfigId({0})", request.emfConfigId));

            try
            {
                using (var proxy = new MaxisChkFxVasDependencyService.chkFxVasDependencyService())
                {
                    proxy.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                    proxy.PreAuthenticate = true;

                    proxy.eaiHeaderValue = new Online.Registration.IntegrationService.MaxisChkFxVasDependencyService.eaiHeader()
                    {
                        from = Properties.Resources.orderProcessReq_from,
                        to = Properties.Resources.orderProcessReq_to,
                        appId = Properties.Resources.orderProcessReq_appID,
                        msgType = Properties.Resources.orderProcessReq_msgType,
                        msgId = string.Format("CO{0}", request.emfConfigId),
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    };

                    ///CONVERT THE REQUEST DATATYPE FROM ISell DATATYPE TO PROXY DATATYPE
                    CChkFxVasDependencyRequest = ConstructChkFxVasDependencyResponse(request);
                    ///LOG THE XML REQUEST
                    xmlRequestResponse = XMLHelper.ConvertObjectToXml(CChkFxVasDependencyRequest, typeof(typeChkFxVasDependencyRequest));
                    Logger.Info("ChkFxVasDependency_Request" + xmlRequestResponse);
                    ///INITIATE THE CALL
                    ChkFxVasDependencyResponse = proxy.chkFxVasDependency(CChkFxVasDependencyRequest);

                    xmlRequestResponse = XMLHelper.ConvertObjectToXml(ChkFxVasDependencyResponse, typeof(typeChkFxVasDependencyResponse));
                    Logger.Info("ChkFxVasDependency_Response" + xmlRequestResponse);

                    FxVasDependencyResponse.Code = ChkFxVasDependencyResponse.msgCode;
                    FxVasDependencyResponse.Message = ChkFxVasDependencyResponse.msgDesc;
                    ///IF MSGCODE EQUALS TO 0 THEN ASSIGN Success TO True OTHERWISE False
                    FxVasDependencyResponse.Success = ChkFxVasDependencyResponse.msgCode.Equals(Properties.Settings.Default.KenanSuccessCode) ? true : false;

                    ///IF typeChkFxVasDependencyResponse mxsRuleMessages IS NOT NULL OR COUNT GREATER THAN 0 THEN COPY mxsRuleMessages
                    if (!ReferenceEquals(ChkFxVasDependencyResponse.mxsRuleMessages, null) && ChkFxVasDependencyResponse.mxsRuleMessages.Count() > 0)
                    {
                        FxVasDependencyResponse.mxsRuleMessages.AddRange(ChkFxVasDependencyResponse.mxsRuleMessages);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("", ex);
                PopulateExceptionResponse(FxVasDependencyResponse, ex);
            }
            Logger.Info(string.Format("Exiting ChkFxVasDependency(): emfConfigId({0})", request.emfConfigId));
            return new ChkFxVasDependencyResponse();
        }

        /// <summary>
        /// Constructs the CHK fx vas dependency response.
        /// </summary>
        /// <param name="request">TypeOf(ChkFxVasDependencyRequest)</param>
        /// <returns>TypeOf(typeChkFxVasDependencyRequest)</returns>
        private typeChkFxVasDependencyRequest ConstructChkFxVasDependencyResponse(ChkFxVasDependencyRequest request)
        {
            typeChkFxVasDependencyRequest FxVasDependencyRequest = new typeChkFxVasDependencyRequest();
            FxVasDependencyRequest.componentList = null;
            int counter = 0;

            FxVasDependencyRequest.emfConfigId = request.emfConfigId;
            FxVasDependencyRequest.mandatoryPackageId = request.mandatoryPackageId;


            if (!ReferenceEquals(request.componentList, null) && request.componentList.Count() > 0)
            {
                //GET THE NUMBER OF COMPONENTS
                counter = request.componentList.Count();
                //CREATE ARRAY ELEMENTS AT RUNTIME
                FxVasDependencyRequest.componentList = new componentList[counter];

                for (int i = 0; i <= counter - 1; i++)
                {
                    //GET THE packageId and componentId FROM request.componentList INDIVIDUAL COMPONENTS AND ASSIGN
                    FxVasDependencyRequest.componentList[i].packageId = request.componentList[i].packageId;
                    FxVasDependencyRequest.componentList[i].componentId = request.componentList[i].componentId;
                }
            }

            return FxVasDependencyRequest;
        }
        #endregion chkFxVasDependency call

        #region removeCenterVas call

        #endregion removeCenterVas call

        #region Get MNP Request implementation
        public getMNPRequestResponse GetMNPRequest(getMNPRequest request)
        {
            typeGetMNPRequestRequest TypeGetMNPRequestRequest = null;
            typeGetMNPRequestResponse TypeGetMNPRequestResponse = null;
            getMNPRequestResponse GetMNPRequestResponse = null;

            string xmlRequestResponse = string.Empty;

            Logger.InfoFormat("ChkFxVasDependency({0}) called!", request);

            Logger.Info(string.Format("Entering GetMNPRequest(): reqValue({0})", request.reqValue));


            try
            {
                using (var proxy = new MaxisgetMNPRequestServiceWs.maxiseaiprocesscommonwsgetMNPRequestService())
                {
                    proxy.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                    proxy.PreAuthenticate = true;

                    proxy.eaiHeaderValue = new Online.Registration.IntegrationService.MaxisgetMNPRequestServiceWs.eaiHeader()
                    {
                        from = Properties.Resources.orderProcessReq_from,
                        to = Properties.Resources.orderProcessReq_to,
                        appId = Properties.Resources.orderProcessReq_appID,
                        msgType = Properties.Resources.orderProcessReq_msgType,
                        msgId = string.Format("CO{0}", request.reqValue),
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    };
                    ///CONVERT THE REQUEST DATATYPE FROM ISell DATATYPE TO PROXY DATATYPE
                    TypeGetMNPRequestRequest = ConstructGetMNPRequest(request);
                    ///LOG THE XML REQUEST
                    xmlRequestResponse = XMLHelper.ConvertObjectToXml(TypeGetMNPRequestRequest, typeof(typeGetMNPRequestRequest));
                    Logger.Info("TypeGetMNPRequestRequest_Request" + xmlRequestResponse);
                    ///INITIATE THE CALL
                    TypeGetMNPRequestResponse = proxy.getMNPRequest(TypeGetMNPRequestRequest);

                    xmlRequestResponse = XMLHelper.ConvertObjectToXml(TypeGetMNPRequestResponse, typeof(typeGetMNPRequestResponse));
                    Logger.Info("TypeGetMNPRequestRequest_Response" + xmlRequestResponse);

                    GetMNPRequestResponse = new getMNPRequestResponse();
                    GetMNPRequestResponse.Code = TypeGetMNPRequestResponse.msgCode;
                    GetMNPRequestResponse.TotalRec = TypeGetMNPRequestResponse.totalRec;

                    if (!ReferenceEquals(TypeGetMNPRequestResponse.mnpReqDetl, null))
                    {
                        GetMNPRequestResponse.RequestDetails = new List<MnpRequestDetails>();
                        foreach (var v in TypeGetMNPRequestResponse.mnpReqDetl)
                        {
                            GetMNPRequestResponse.RequestDetails.Add(new MnpRequestDetails
                            {
                                AccountNo = v.accountNo,
                                DispPortReqId = v.dispPortReqId,
                                Location = v.location,
                                Msisdn = v.msisdn,
                                MsisdnStatus = v.msisdnStatus,
                                PortInitiatedDt = v.portInitiatedDt,
                                PortRequestID = v.portReqId,
                                UserId = v.userId,
                                ErrorCode = v.errorCode,
                                ErrorReason = v.errorReason
                            });
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("", ex);
                PopulateExceptionResponse(GetMNPRequestResponse, ex);
            }
            Logger.Info(string.Format("Exiting GetMNPRequest(): reqValue({0})", request.reqValue));
            return GetMNPRequestResponse;
        }

        /// <summary>
        /// TRANSFER THE VALUES FROM INTERNAL DATA STRUCTURE TO PROXY TYPE. HELPER METHOD FOR getMNPRequest METHOD
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private typeGetMNPRequestRequest ConstructGetMNPRequest(getMNPRequest request)
        {
            typeGetMNPRequestRequest typeGetMNPRequestRequest = new typeGetMNPRequestRequest();
            ///ASSIGN THE VALUES FROM INTERNAL TYPE TO PROXY TYPE
            if (!ReferenceEquals(request, null))
            {
                typeGetMNPRequestRequest.reqType = request.reqType;
                typeGetMNPRequestRequest.reqValue = request.reqValue;
                typeGetMNPRequestRequest.portReqType = request.portReqType;
            }
            return typeGetMNPRequestRequest;
        }
        #endregion Get MNP Request implementation

        #region "PreportIn Checking Request"

        /// <summary>
        /// Preports the in validation check.
        /// </summary>
        /// <param name="_PreportInReq">The _ preport in req.</param>
        /// <returns>typeOf(PreportInResponse)</returns>
        public PreportInResponse PreportInValidationCheck(PreportInRequest _PreportInReq)
        {
            typePrePortInValidationRequest typeRequest = null;
            typePrePortInValidationResponse typeResponse = null;
            PreportInResponse getResponse = null;
            string xmlRequestResponse = string.Empty;

            try
            {
                using (var proxy = new MaxisOrderProcessWs.orderCenterService())
                {
                    proxy.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                    proxy.PreAuthenticate = true;
                    typeRequest = ConstructPreportInRequest(_PreportInReq);
                    proxy.eaiHeaderValue = new Online.Registration.IntegrationService.MaxisOrderProcessWs.eaiHeader
                    {
                        from = Properties.Resources.orderProcessReq_from,
                        to = Properties.Resources.orderProcessReq_to,
                        appId = Properties.Resources.orderProcessReq_appID,
                        msgType = Properties.Resources.orderProcessReq_msgType,
                        msgId = string.Format("CO{0}", DateTime.Now.ToString("ddMMyyyyhhmmss")),
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    };

                    xmlRequestResponse = XMLHelper.ConvertObjectToXml(typeRequest, typeof(typePrePortInValidationRequest));
                    Logger.Info("typePrePortInValidationRequest" + xmlRequestResponse);
                    ///INITIATE THE CALL
                    typeResponse = proxy.prePortInValidation(typeRequest);

                    xmlRequestResponse = XMLHelper.ConvertObjectToXml(typeResponse, typeof(typePrePortInValidationResponse));
                    Logger.Info("typePrePortInValidationResponse" + xmlRequestResponse);

                    getResponse = new PreportInResponse();
                    getResponse.msgCode = typeResponse.msgCode;
                    getResponse.msgDesc = typeResponse.msgDesc;
                    getResponse.reqId = typeResponse.reqId;

                }
            }
            catch (Exception ex)
            {
                getResponse = new PreportInResponse();
                getResponse.msgCode = "-2";
                getResponse.msgDesc = ex.Message;
                getResponse.reqId = string.Empty;
                Logger.InfoFormat("Preport-In Request{0},MSISDN{1},Error{2}", "PreportinValidationRequest", _PreportInReq.msisdn, ex.StackTrace + Environment.NewLine + ex.Message);
            }
            return getResponse;
        }

        /// <summary>
        /// Constructs the preport in request.
        /// </summary>
        /// <param name="_req">The _req.</param>
        /// <returns>typeOf(typePrePortInValidationRequest)</returns>
        private typePrePortInValidationRequest ConstructPreportInRequest(PreportInRequest _req)
        {
            typePrePortInValidationRequest typeRequest = new typePrePortInValidationRequest();

            if (_req.msisdn.Count() > 0)
            {

            }
            if (!ReferenceEquals(_req, null))
            {
                typeRequest.accountNumber = _req.accountNumber;
                typeRequest.companyName = _req.companyName;
                typeRequest.customerName = _req.customerName;
                typeRequest.donorId = _req.donorId;
                typeRequest.isConsumer = _req.isConsumer;
                //foreach(var s in _req.msisdn)
                //{
                typeRequest.msisdn = _req.msisdn;
                typeRequest.identity = _req.identity;
                typeRequest.identityType = _req.identityType;
                //}
            }
            return typeRequest;
        }

        #endregion

        #region "Disconnect Components disconnectCenterServiceRequest"



        public DisconnectCenterServiceResponse DisconnectComponents(DisconnectCenterServiceRequest compRequest)
        {
            var response = new DisconnectCenterServiceResponse();
            //TODO: IN CASE OF NULL , NEED TO HANDLE THE RESPONSE OBJECT PROPERLY
            if (ReferenceEquals(compRequest, null))
                return null;
            var thisRequst = ConstructDisconnectRequest(compRequest);
            var thisResponse = disconnectCenterService(thisRequst);
            if (!ReferenceEquals(thisResponse, null))
            {
                response.msgCode = thisResponse.msgCode;
                response.msgDesc = thisResponse.msgDesc;
            }
            return response;
        }

        private typeDisconnectCenterService ConstructDisconnectRequest(DisconnectCenterServiceRequest request)
        {
            typeDisconnectCenterService req = new typeDisconnectCenterService();
            req.fxAcctNo = request.fxAcctNo;
            req.fxSubscrNo = request.fxSubscrNo;
            req.fxSubscrNoResets = request.fxSubscrNoResets;
            req.disconnectReason = request.disconnectReason;
            req.orderId = request.orderId;
            req.newOrderInd = request.newOrderInd;
            req.waiveInstallmentNrc = request.waiveInstallmentNrc;
            req.waiveUnbilledNrc = request.waiveUnbilledNrc;
            req.waiveTerminationObligation = request.waiveTerminationObligation;
            req.waiveUnmetObligation = request.waiveUnmetObligation;
            req.waiveRefinanceNrc = request.waiveRefinanceNrc;
            req.inpF01 = request.inpF01;
            req.inpF02 = request.inpF02;
            req.inpF03 = request.inpF03;
            req.inpF04 = request.inpF04;
            req.inpF05 = request.inpF05;
            req.inpF06 = request.inpF06;
            req.inpF07 = request.inpF07;
            req.inpF08 = request.inpF08;
            req.inpF09 = request.inpF09;
            req.inpF10 = request.inpF10;
            return req;
        }

        private eaiResponseType disconnectCenterService(typeDisconnectCenterService request)
        {
            eaiResponseType response;
            try
            {
                //eaiResponseType response ;
                using (var proxy = new MaxisOrderProcessWs.orderCenterService())
                {
                    proxy.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                    proxy.PreAuthenticate = true;
                    proxy.eaiHeaderValue = new Online.Registration.IntegrationService.MaxisOrderProcessWs.eaiHeader
                    {
                        from = Properties.Resources.orderProcessReq_from,
                        to = Properties.Resources.orderProcessReq_to,
                        appId = Properties.Resources.orderProcessReq_appID,
                        msgType = Properties.Resources.orderProcessReq_msgType,
                        msgId = string.Format("CO{0}", DateTime.Now.ToString("ddMMyyyyhhmmss")),
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    };

                    var xmlRequest = XMLHelper.ConvertObjectToXml(request, typeof(typeDisconnectCenterService));
                    Logger.Info("typeDisconnectCenterServiceRequest" + xmlRequest);
                    ///INITIATE THE CALL
                    var typeResponse = proxy.disconnectCenterService(request);

                    var xmlResponse = XMLHelper.ConvertObjectToXml(typeResponse, typeof(eaiResponseType));
                    Logger.Info("typeDisconnectCenterServiceResponse" + xmlResponse);

                    response = new eaiResponseType();
                    response.msgCode = typeResponse.msgCode;
                    response.msgDesc = typeResponse.msgDesc;

                    #region Added by Nreddy for logging
                    string orderID = request.orderId.Replace("ISELL", string.Empty);
                    if (orderID.EndsWith("D") == true)
                    {
                        orderID = orderID.Replace("D", string.Empty);
                        orderID = orderID.Substring(0, orderID.Length - 1);

                    }
                    int orderIDD = orderID.ToInt();
                    KenanaLogDetails objKenanaLogDetails = new KenanaLogDetails();
                    objKenanaLogDetails.RegID = orderID.ToInt();
                    objKenanaLogDetails.MessageCode = response.msgCode;
                    objKenanaLogDetails.MessageDesc = !ReferenceEquals(response.msgDesc, null) ? response.msgDesc : string.Empty;
                    objKenanaLogDetails.KenanXmlReq = xmlRequest;
                    objKenanaLogDetails.KenanXmlRes = xmlResponse;
                    objKenanaLogDetails.MethodName = "disconnectCenterService";
                    OnlineRegAdmin.SaveKenanXmlLogs(objKenanaLogDetails);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                response = new eaiResponseType();
                response.msgCode = "-2";
                response.msgDesc = ex.Message;
                Logger.InfoFormat("disconnectCenterService Request{0},Error{2}", "disconnectCenterService", ex.StackTrace + Environment.NewLine + ex.Message);
            }
            return response;
        }
        #endregion

        #region RetrieveDealerInfo
        /// <summary>
        /// retrieveDealerInfo
        /// </summary>
        /// <param name="request">TypeOf(retrieveDealerInfoRequest)</param>
        /// <returns>TypeOf(retrieveDealerInfoResponse)</returns>
        public retrieveDealerInfoResponse RetrieveDealerInfo(retrieveDealerInfoRequest req)
        {
            typeRetrieveDealerInfoRequest dealerInfoRequest = null;
            typeRetrieveDealerInfoResponse dealerInfoResponse = null;
            retrieveDealerInfoResponse resp = new retrieveDealerInfoResponse();
            string xml = string.Empty;

            Logger.Info(string.Format("Entering retrieveDealerInfo(): dealerCode({0})", req.dealerCode));

            try
            {
                using (var proxy = new maxiseaiprocesscommonwsretrieveDealerInfoService())
                {
                    proxy.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                    proxy.PreAuthenticate = true;

                    proxy.eaiHeaderValue = new RetrieveDealerInfoWS.eaiHeader()
                    {
                        from = Properties.Resources.orderProcessReq_from,
                        to = Properties.Resources.orderProcessReq_to,
                        appId = Properties.Resources.orderProcessReq_appID,
                        msgType = Properties.Resources.orderProcessReq_msgType,
                        msgId = string.Format("RDI{0}", req.dealerCode),
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    };
                    //Note: OPF request for 12 digits input with space character
                    dealerInfoRequest = new typeRetrieveDealerInfoRequest();
                    dealerInfoRequest.dealerCode = req.dealerCode.PadRight(12, ' ');

                    //Convert request into XML and write into Logger
                    xml = XMLHelper.ConvertObjectToXml(dealerInfoRequest, typeof(typeRetrieveDealerInfoRequest));
                    Logger.Debug("retrieveDealerInfo() - Request:\n" + xml);

                    //Call the Web Service
                    dealerInfoResponse = proxy.retrieveDealerInfo(dealerInfoRequest);

                    //Convert response into XML and write into Logger
                    xml = XMLHelper.ConvertObjectToXml(dealerInfoResponse, typeof(typeRetrieveDealerInfoResponse));
                    Logger.Debug("retrieveDealerInfo() - Response:\n" + xml);

                    resp.Success = true;
                    resp.Code = dealerInfoResponse.msgCode;
                    resp.Message = dealerInfoResponse.msgDesc;
                    resp.salesChannelId = dealerInfoResponse.salesChannelId;
                    resp.salesChannelName = dealerInfoResponse.salesChannelName;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("retrieveDealerInfo() - Error:\n", ex);
                PopulateExceptionResponse(resp, ex);
            }
            Logger.Info(string.Format("Exiting retrieveDealerInfo(): dealerCode({0})", req.dealerCode));
            return resp;
        }
        #endregion RetrieveDealerInfo

    }
}
