﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.Helper;
using Online.Registration.IntegrationService.IContractSearchWS;

using System.Xml.Linq;
using SNT.Utility;
using System.Globalization;
using log4net;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "IContractSearch" in code, svc and config file together.
    public class IContractSearch : IIContractSearch
    {
		static readonly ILog Logger = LogManager.GetLogger(typeof(PegaService));

		public SearchResponse DownloadFromIContract(IContractSearchRequest request)
		{
			if (request == null)
				throw new ArgumentNullException();

			Logger.InfoFormat("Entering (DownloadFromIContract:{0})", request.OrderID.ToString());
			retrieveDocumentsListRequest reqObj = new retrieveDocumentsListRequest();
			SearchResponse finalResponse = new SearchResponse();
			reqObj.iSellOrderID = request.OrderID;
			reqObj.IDType = 0;

			IContractSearchWS.retrieveDocumentsListResponse responseFromWS = new IContractSearchWS.retrieveDocumentsListResponse();

			try
			{
				SearchDocsClient _client = new SearchDocsClient();

				responseFromWS = _client.retrieveDocumentsList(reqObj);

				if (!ReferenceEquals(responseFromWS, null) && responseFromWS.headerValue.ErrorCode.Equals("0"))
				{
					finalResponse.errorCode = responseFromWS.headerValue.ErrorCode;
					finalResponse.errorMessage = responseFromWS.headerValue.ErrorMessage;
					/**Parse the response to Models**/
					foreach (var response in responseFromWS.listDocumentsInfo)
					{
						var objResponse = new IContractSearchResponse();
						objResponse.DocumentName = response.DocumentName;
						objResponse.DocumentContent = response.DocumentContent;
						objResponse.OrderID = response.iSellOrderID;
						objResponse.IDCardValue = response.IDcardvalue;
						objResponse.MSISDN = response.MSISDN;
						objResponse.AccountNo = response.AccountNo;
						objResponse.AgentCode = response.AgentCode;
						objResponse.CreatedDate = response.CreatedDT;

						finalResponse.ContractSearchResponses.Add(objResponse);
					}
				}else{
					finalResponse.errorCode = responseFromWS.headerValue.ErrorCode;
					finalResponse.errorMessage = responseFromWS.headerValue.ErrorMessage;
				}
				
			}
			catch (Exception ex)
			{
				Logger.Error(" Error: IContractSearch" + Util.LogException(ex));
			}

			return finalResponse;
		}
    }
}
