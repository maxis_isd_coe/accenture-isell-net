﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAutoActivation" in both code and config file together.
    [ServiceContract]
    public interface IAutoActivation
    {
        [OperationContract]
        POSStatusResult UpdateWebPOSStatus(string LoginID, string MsgCode, string MsgDesc, string OrderID);

        #region commented, prefiously built for web pos
        //[OperationContract]
        //bool SendPaymentDetails(int iSellOrderID);
        #endregion

        [OperationContract]
        POSStatusResult GetPOSDetailsbyFilename(string LoginID, string Filename, string Parameter1, string Parameter2);

		[OperationContract]
		bool KenanAccountFulfill(int regTypeId, string regId);

		[OperationContract]
		POSStatusResult TriggerEmail(int regID);

        [OperationContract]
        POSStatusResult TriggerSSOEmail(int regID);
    }
}
