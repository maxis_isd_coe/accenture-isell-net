﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.Helper;
using log4net;
using System.ServiceModel.Description;
using System.Net;
using System.Xml.Linq;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EBPSService" in code, svc and config file together.
    public class EBPSService : IEBPSService
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(EBPSService));
        public GetSubscriptionInfoResponse GetSubscriptionInfo(GetSubscriptionInfoRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            Logger.InfoFormat("EBPSWS.Entering GetSubscriptionInfo: {0})", request.acctExtId.ToString());

            var filteredResponse = new GetSubscriptionInfoResponse();
            string msgCode, msgDesc, hcSuppressSubscription, emailBillSubscription, emailAddress, smsAlertFlag, smsNo, emailSubscriptionStatus, updatedDate, updatedTime, updatedBy, updatedChannel = null;
            string xmlReqs = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                    xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" 
                    xmlns:v11=""http://services.maxis.com.my/EAI/EBILLING/EBILLING/v1/"" 
                    xmlns:v12=""http://schemas.maxis.com.my/EAI/EBILLING/EBILLING/v1/"">
                       <soapenv:Header>
                          <v1:eaiHeader>
                             <v1:from>{0}</v1:from>
                             <v1:to>{1}</v1:to>
                             <v1:appId>{2}</v1:appId>
                             <v1:msgType>{3}</v1:msgType>
                             <v1:msgId>{4}</v1:msgId>	
                             <!--Optional:-->
                             <v1:correlationId>{5}</v1:correlationId>
                             <v1:timestamp>{6}</v1:timestamp>
                          </v1:eaiHeader>
                       </soapenv:Header>
                       <soapenv:Body>
                          <v11:getSubscriptionInfoReqDoc>
                             <v12:acctExtId>{7}</v12:acctExtId>
                          </v11:getSubscriptionInfoReqDoc>
                       </soapenv:Body>
                    </soapenv:Envelope>",
                Properties.Resources.orderProcessReq_from, //0
                Properties.Resources.orderProcessReq_to, //1
                Properties.Resources.orderProcessReq_appID, //2
                Properties.Resources.orderProcessReq_msgType, //3
                string.Format("BDGET{0}", request.acctExtId), //4
                string.Format("ISELL{0}", request.acctExtId), //5
                DateTime.Now.ToString("yyyyMMddHHmmss"), //6
                request.acctExtId.ToString() //7
            );
            string xmlResp = string.Empty;

            try
            {
                using (var proxy = new WebClient())
                {
                    Uri postUri = new Uri(Properties.Settings.Default.EBPS_WS_URI.ToString());
                    proxy.Credentials = new NetworkCredential(Properties.Settings.Default.EBPS_WS_Username, Properties.Settings.Default.EBPS_WS_Password);
                    proxy.Headers.Add("content-type", "text/xml");

                    Logger.DebugFormat("EBPSWS.GetSubscriptionInfoRequest.Request  : {0} ", xmlReqs);
                    xmlResp = proxy.UploadString(postUri, xmlReqs);
                    xmlResp = Util.RemoveAllNamespaces(xmlResp);
                    Logger.DebugFormat("EBPSWS.GetSubscriptionInfoRequest.Response : {0} ", xmlResp);

                    XDocument doc = XDocument.Parse(xmlResp);
                    foreach (var r in doc.Descendants("getSubscriptionInfoRespDoc"))
                    {
                        msgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                        msgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
                        hcSuppressSubscription = r.Element("hcSuppressSubscription") == null ? "" : r.Element("hcSuppressSubscription").Value.ToString();
                        emailBillSubscription = r.Element("emailBillSubscription") == null ? "" : r.Element("emailBillSubscription").Value.ToString();
                        emailAddress = r.Element("emailAddress") == null ? "" : r.Element("emailAddress").Value.ToString();
                        smsAlertFlag = r.Element("smsAlertFlag") == null ? "" : r.Element("smsAlertFlag").Value.ToString();
                        smsNo = r.Element("smsNo") == null ? "" : r.Element("smsNo").Value.ToString();
                        emailSubscriptionStatus = r.Element("emailSubscriptionStatus") == null ? "" : r.Element("emailSubscriptionStatus").Value.ToString();
                        updatedDate = r.Element("updatedDate") == null ? "" : r.Element("updatedDate").Value.ToString();
                        updatedTime = r.Element("updatedTime") == null ? "" : r.Element("updatedTime").Value.ToString();
                        updatedBy = r.Element("updatedBy") == null ? "" : r.Element("updatedBy").Value.ToString();
                        updatedChannel = r.Element("updatedChannel") == null ? "" : r.Element("updatedChannel").Value.ToString();

                        if (msgCode == "0")
                        {
                            Logger.InfoFormat("EBPSWS.Bill Delivery Options Found with msgCode: {0}", msgCode);
                            filteredResponse.Success = true;
                            filteredResponse.msgCode = msgCode;
                            filteredResponse.msgDesc = msgDesc;

                            //Requested logic by user to cater for combinations of e-bill and post (YY, YN, NY considered as e-bill)
                            if (hcSuppressSubscription == "N" && emailBillSubscription == "N")
                            {
                                filteredResponse.hcSuppressSubscription = "N";
                                filteredResponse.emailBillSubscription = "N";
                            }
                            else
                            {
                                filteredResponse.hcSuppressSubscription = "Y";
                                filteredResponse.emailBillSubscription = "Y";

                            }
                            filteredResponse.emailAddress = emailAddress;
                            filteredResponse.smsAlertFlag = smsAlertFlag;
                            filteredResponse.smsNo = smsNo;
                            filteredResponse.emailSubscriptionStatus = emailSubscriptionStatus;
                            filteredResponse.updatedDate = updatedDate;
                            filteredResponse.updatedTime = updatedTime;
                            filteredResponse.updatedBy = updatedBy;
                            filteredResponse.updatedChannel = updatedChannel;
                        }
                        else
                        {
                            Logger.InfoFormat("EBPSWS.Bill Delivery Options not found");
                            filteredResponse.Success = false;
                            filteredResponse.msgCode = msgCode;
                            filteredResponse.msgDesc = msgDesc;
                        }
                    }

                    /*
                    var clientCredentials = new ClientCredentials(); 
                    clientCredentials.UserName.UserName = Properties.Settings.Default.EBPS_WS_Username;
                    clientCredentials.UserName.Password = Properties.Settings.Default.EBPS_WS_Password;
                    
                    var eaiHeader = new EBPSWS.eaiHeader();
                    msgCode = proxy.getSubscriptionInfo(
                        ref eaiHeader, 
                        request.acctExtId, 
                        out msgDesc, 
                        out hcSuppressSubscription, 
                        out emailBillSubscription,
                        out emailAddress,
                        out smsAlertFlag,
                        out smsNo,
                        out emailSubscriptionStatus,
                        out updatedDate,
                        out updatedTime,
                        out updatedBy,
                        out updatedChannel
                    );
                    
                    if (!string.IsNullOrEmpty(msgCode) && !string.IsNullOrEmpty(hcSuppressSubscription) && !string.IsNullOrEmpty(emailBillSubscription))
                    {
                        filteredResponse.msgCode = msgCode;
                        filteredResponse.msgDesc = msgDesc;
                        filteredResponse.hcSuppressSubscription = hcSuppressSubscription;
                        filteredResponse.emailBillSubscription = emailBillSubscription;
                        filteredResponse.emailAddress = emailAddress;
                        filteredResponse.smsAlertFlag = smsAlertFlag;
                        filteredResponse.smsNo = smsNo;
                        filteredResponse.emailSubscriptionStatus = emailSubscriptionStatus;
                        filteredResponse.updatedDate = updatedDate;
                        filteredResponse.updatedTime = updatedTime;
                        filteredResponse.updatedBy = updatedBy;
                        filteredResponse.updatedChannel = updatedChannel;
                    }
                    */
                }

            }
            catch (Exception ex)
            {
                filteredResponse.Success = false;
                Logger.Error("EBPSWS.Exception GetSubscriptionInfo: " + Util.LogException(ex));
            }

            Logger.InfoFormat("EBPSWS.Exiting GetSubscriptionInfo: {0}, response: {1})", request.acctExtId.ToString(), filteredResponse.Success);

            return filteredResponse;
        }

        public UpdSubscriptionInfoResponse UpdSubscriptionInfo(UpdSubscriptionInfoRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            Logger.InfoFormat("EBPSWS.Entering UpdSubscriptionInfo: {0})", request.acctExtId.ToString());

            var filteredResponse = new UpdSubscriptionInfoResponse();
            string msgCode, msgDesc = null;

            try
            {
                string xmlReqs = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                    xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" 
                    xmlns:v11=""http://services.maxis.com.my/EAI/EBILLING/EBILLING/v1/"" 
                    xmlns:v12=""http://schemas.maxis.com.my/EAI/EBILLING/EBILLING/v1/"">
                       <soapenv:Header>
                          <v1:eaiHeader>
                             <v1:from>{0}</v1:from>
                             <v1:to>{1}</v1:to>
                             <v1:appId>{2}</v1:appId>
                             <v1:msgType>{3}</v1:msgType>
                             <v1:msgId>{4}</v1:msgId>	
                             <!--Optional:-->
                             <v1:correlationId>{5}</v1:correlationId>
                             <v1:timestamp>{6}</v1:timestamp>
                          </v1:eaiHeader>
                       </soapenv:Header>
                       <soapenv:Body>
                          <v11:updSubscriptionInfoReqDoc>
                             <v12:acctExtId>{7}</v12:acctExtId>
                             <v12:hcSuppressSubscription>{8}</v12:hcSuppressSubscription>
                             <v12:emailBillSubscription>{9}</v12:emailBillSubscription>
                             <v12:emailAddress>{10}</v12:emailAddress>
                             <!--Optional:-->
                             <v12:smsAlertFlag>{11}</v12:smsAlertFlag>
                             <!--Optional:-->
                             <v12:smsNo>{12}</v12:smsNo>
                             <v12:updatedBy>{13}</v12:updatedBy>
                          </v11:updSubscriptionInfoReqDoc>
                       </soapenv:Body>
                    </soapenv:Envelope>",
                    Properties.Resources.orderProcessReq_from, //0
                    Properties.Resources.orderProcessReq_to, //1
                    Properties.Resources.orderProcessReq_appID, //2
                    Properties.Resources.orderProcessReq_msgType, //3
                    string.Format("BDUPD{0}", request.acctExtId), //4
                    string.Format("ISELL{0}", request.acctExtId), //5
                    DateTime.Now.ToString("yyyyMMddHHmmss"), //6
                    request.acctExtId.ToString(), //7,
                    request.hcSuppressSubscription.ToString(), //8,
                    request.emailBillSubscription.ToString(), //9,
                    request.emailAddress.ToString(), //10,
                    request.smsAlertFlag.ToString(), //11,
                    request.smsNo.ToString(), //12,
                    request.updatedBy.ToString() //13
                );
                string xmlResp = string.Empty;

                using (var proxy = new WebClient())
                {
                    Uri postUri = new Uri(Properties.Settings.Default.EBPS_WS_URI.ToString());
                    proxy.Credentials = new NetworkCredential(Properties.Settings.Default.EBPS_WS_Username, Properties.Settings.Default.EBPS_WS_Password);
                    proxy.Headers.Add("content-type", "text/xml");

                    Logger.DebugFormat("EBPSWS.UpdSubscriptionInfoRequest.Request  : {0} ", xmlReqs);
                    xmlResp = proxy.UploadString(postUri, xmlReqs);
                    xmlResp = Util.RemoveAllNamespaces(xmlResp);
                    Logger.DebugFormat("EBPSWS.UpdSubscriptionInfoRequest.Response : {0} ", xmlResp);

                    XDocument doc = XDocument.Parse(xmlResp);
                    foreach (var r in doc.Descendants("updSubscriptionInfoRespDoc"))
                    {
                        msgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                        msgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();

                        if (msgCode == "0")
                        {
                            Logger.InfoFormat("EBPSWS.Bill Delivery Options Update with msgCode: {0}", msgCode);
                            filteredResponse.Success = true;
                            filteredResponse.msgCode = msgCode;
                            filteredResponse.msgDesc = msgDesc;
                        }
                        else
                        {
                            Logger.InfoFormat("EBPSWS.Bill Delivery Options Update failed");
                            filteredResponse.Success = false;
                            filteredResponse.msgCode = msgCode;
                            filteredResponse.msgDesc = msgDesc;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                filteredResponse.Success = false;
                Logger.Error("EBPSWS.Exception UpdSubscriptionInfo: " + Util.LogException(ex));
            }

            Logger.InfoFormat("EBPSWS.Exiting UpdSubscriptionInfo: {0}, response: {1})", request.acctExtId.ToString(), filteredResponse.Success);

            return filteredResponse;
        }
    }
}
