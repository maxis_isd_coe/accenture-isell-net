﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml;
using System.IO;
using System.Text;

using Online.Registration.IntegrationService.BiometricsService;
//using Online.Registration.IntegrationService.AMRMyKadWS;
using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.Helper;

using SNT.Utility;

using log4net;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AMRService" in code, svc and config file together.
    public class AMRService : IAMRService
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AMRService));

        #region Private Methods

        private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
        {
            resp.Code = "-1";
            resp.Message = ex.Message;
            resp.Success = false;
        }
        private RetrieveMyKadInfoRp ConstructMyKadInfoRp(string xmlString)
        {
            var resp = new RetrieveMyKadInfoRp();

            string str = string.Empty;

            using (XmlReader xmlReader = XmlReader.Create(new StringReader(xmlString)))
            {
                while (xmlReader.Read())
                {
                    XmlNodeType nType = xmlReader.NodeType;

                    if (nType == XmlNodeType.Element && str != xmlReader.Name.ToString())
                    {
                        str = xmlReader.Name.ToString();
                    }

                    if (nType == XmlNodeType.Text)
                    {
                        switch (str)
                        {
                            case "name":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.Name = xmlReader.Value.Trim();
                                }
                                break;
                            case "new_ic":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.NewIC = xmlReader.Value.Trim();
                                }
                                break;
                            case "old_ic":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.OldIC = xmlReader.Value.Trim();
                                }
                                break;
                            case "dob":
                                resp.DOB = xmlReader.Value.Replace('-', '/');
                                break;
                            case "nationality":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.Nationality = xmlReader.Value.Trim();
                                }
                                break;
                            case "language":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.Language = xmlReader.Value.Trim();
                                }
                                break;
                            case "race":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.Race = xmlReader.Value.Trim();
                                }
                                break;
                            case "religion":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.Religion = xmlReader.Value.Trim();
                                }
                                break;
                            case "address1":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.Address1 = xmlReader.Value.Trim();
                                }
                                break;
                            case "address2":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.Address2 = xmlReader.Value.Trim();
                                }
                                break;
                            case "address3":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.Address3 = xmlReader.Value.Trim();
                                }
                                break;
                            case "postcode":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.Postcode = xmlReader.Value.Trim();
                                }
                                break;
                            case "city":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.City = xmlReader.Value.Trim();
                                }
                                break;
                            case "state":
                                if (!string.IsNullOrEmpty(xmlReader.Value))
                                {
                                    resp.State = xmlReader.Value.Trim();
                                }
                                break;
                            case "finger_match":
                                if (xmlReader.Value == "Y")
                                    resp.FingerMatch = true;
                                else
                                    resp.FingerMatch = false;
                                break;
                            case "finger_thumb":
                                resp.FingerThumb = xmlReader.Value;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            return resp;
        }

        #endregion

        public RetrieveMyKadInfoRp RetrieveMyKadInfo(RetrieveMyKadInfoRq request)
        {
            if (request == null)
                throw new ArgumentNullException();

            Logger.InfoFormat("Entering RetrieveMyKadInfo(ApplicationID:{0},NewIC:{1},DealerCode{2})", request.ApplicationID, request.NewIC, request.DealerCode);

            //added by Deepika 
            string req = XMLHelper.ConvertObjectToXml(request, typeof(RetrieveMyKadInfoRq));
            Logger.Info("(RetrieveMyKadInfoRq  " + req);

            var response = new RetrieveMyKadInfoRp();

            try
            {
                var code = "";
                var msgDesc = "";

                if (Properties.Settings.Default.AMRStub)
                {
                    //Following is the real XML String for MARGARET in case of thumb impression match// 
                    //var xmlString = @"<amr_mykad_response><amr_result>0</amr_result><amr_message>Mykad info retrieved successfully.</amr_message><mykad_info><name>MARGARET MARY MENDEZ A/P J.C.MENDEZ</name><new_ic>801219105190</new_ic><old_ic></old_ic><gender>PEREMPUAN</gender><dob>1980-12-19</dob><nationality>WARGANEGARA</nationality><race>INDIA</race><religion>KRISTIAN</religion><address1>NO 813 JALAN E5/5</address1><address2>TAMAN EHSAN</address2><address3></address3><postcode>52100</postcode><city>KUALA LUMPUR</city><state>W. PERSEKUTUAN(KL)</state><finger_match>Y</finger_match><finger_thumb>1</finger_thumb></mykad_info></amr_mykad_response>";
                    var xmlString = System.Configuration.ConfigurationManager.AppSettings["AMRXmlString"].ToString2(); ;
                    //Following is the real XML String for MARGARET in case of thumb impression does not match// 
                    //var xmlString = @"<amr_mykad_response><amr_result>0</amr_result><amr_message>Mykad info retrieved successfully.</amr_message><mykad_info><name>MARGARET MARY MENDEZ A/P J.C.MENDEZ</name><new_ic>801219105190</new_ic><old_ic></old_ic><gender>PEREMPUAN</gender><dob>1980-12-19</dob><nationality>WARGANEGARA</nationality><race>INDIA</race><religion>KRISTIAN</religion><address1>NO 813 JALAN E5/5</address1><address2>TAMAN EHSAN</address2><address3></address3><postcode>52100</postcode><city>KUALA LUMPUR</city><state>W. PERSEKUTUAN(KL)</state><finger_match>N</finger_match><finger_thumb>0</finger_thumb></mykad_info></amr_mykad_response>";

                    //Some older XML Strings
                    //var xmlString = @"<amr_mykad_response><amr_result>0</amr_result><amr_message>Mykad info retrieved successfully.</amr_message><mykad_info><name>HO WAI HING</name><new_ic>771005145117</new_ic><old_ic>A3852199</old_ic><gender>LELAKI</gender><dob>1977-10-05</dob><nationality>WARGANEGARA</nationality><race>CINA</race><religion>BUDDHA</religion><address1>12 JALAN PUJ 6/8</address1><address2>TAMAN PUNCAK JALIL</address2><address3></address3><postcode>43300</postcode><city>SERI KEMBANGAN</city><state>SELANGOR</state><finger_match>Y</finger_match><finger_thumb>1</finger_thumb></mykad_info></amr_mykad_response>";
                    //var xmlString = @"<amr_mykad_response><amr_result>0</amr_result><amr_message>MyKad Info Retrieved Succesfully</amr_message><mykad_info><name>PARIMALA A/P BALASINGAM</name><new_ic>741021105118</new_ic><old_ic>A2877753</old_ic><gender>PEREMPUAN</gender><dob>1974/10/21</dob><nationality>WARGANEGARA </nationality><race>INDIA </race><religion>HINDU </religion><address1>NO 44 JALAN MANGGA </address1><address2>OFF TELOK GADONG </address2><address3></address3><postcode>41200</postcode><city>KLANG </city><state>SELANGOR </state><finger_match>true</finger_match><finger_thumb>1</finger_thumb></mykad_info></amr_mykad_response>";
                    //var xmlString = @"<amr_mykad_response><amr_result>0</amr_result><amr_message>MyKad Info Retrieved Succesfully</amr_message><mykad_info><name>Tan Kok Wai</name><new_ic>701031101234</new_ic><old_ic>A12345678</old_ic><gender>Lelaki</gender><dob>1970-10-31</dob><nationality>Malaysian</nationality><language>Malay</language><race>Cina</race><religion>Buddhist</religion><address1>Jalan SS 6/16A</address1><address2>Dataran Glomac, Kelana Jaya</address2><address3>Petaling Jaya</address3><postcode>47301</postcode><city>Petaling Jaya</city><state>Selangor</state><finger_match>Y</finger_match><finger_thumb>1</finger_thumb></mykad_info></amr_mykad_response>";
                    response = ConstructMyKadInfoRp(xmlString);
                }
                else
                {
                    System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
                    using (var proxy = new BiometricsService.AMRMyKadWS())
                    {
                        var xmlString = proxy.RetrieveMyKadInfo(request.DealerCode, request.NewIC, request.ApplicationID, true);
                        response = ConstructMyKadInfoRp(xmlString);
                    }
                }
                string res = XMLHelper.ConvertObjectToXml(response, typeof(RetrieveMyKadInfoRp));
                Logger.InfoFormat("######################### AMR Request And Response ########################");
                Logger.Info("(RetrieveMyKadInfoRequest  " + req);
                Logger.Info("(RetrieveMyKadInfoResponse  " + res);
                Logger.InfoFormat("######################### AMR Request And Response ########################");

                Logger.InfoFormat("msgCode:{0}, msgDesc:{1}", code, msgDesc);

                response.Success = code == "0";
                if (!response.Success)
                    response.Message = string.Format("MaxisCommonWs.retrieveSubscriberDetls return unsuccessful with msgDesc:{0}.", msgDesc);
            }
            catch (Exception ex)
            {
				var errorResponse = new RetrieveMyKadInfoRp();
                Logger.Error("", ex);
                PopulateExceptionResponse(response, ex);
				errorResponse.Code = "-1";
				return errorResponse;
            }

            Logger.InfoFormat("Exiting RetrieveMyKadInfo(ApplicationID:{0},NewIC:{1},Success:{2})", request.ApplicationID, request.NewIC, response.Success);

            return response;
        }
    }
}
