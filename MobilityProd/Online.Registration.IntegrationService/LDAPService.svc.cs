﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.Net;
using System.DirectoryServices.AccountManagement;

using log4net;

using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.Helper;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LDAPService" in code, svc and config file together.
    public class LDAPService : ILDAPService
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(LDAPService));

        #region Private Methods

        private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
        {
            resp.Code = "-1";
            resp.Message = ex.Message;
            resp.Success = false;
        }

        #endregion

        public LDAPAccessResponse ValidateCredential(LDAPAccessRequest request, string IsDealer)
        {
            var resp = new LDAPAccessResponse();

            if (IsDealer == "True")
            {

                try
                {
                    string path = Properties.Settings.Default.DealerLDAPUrl;
                    string domain = Properties.Settings.Default.DealerLDAPDomine;
                    string username = request.Username;
                    string pwd = request.Password;

                    string domainAndUsername = domain + @"\" + username;
                    DirectoryEntry entry = new DirectoryEntry(path, domainAndUsername, pwd, AuthenticationTypes.Secure);
                    Logger.Info("Citrix Dealer LDAP URL is ################################" + path);
                    Logger.Info("Citrix Dealer LDAP Domine is ############################################" + domain);                    

                    //Bind to the native AdsObject to force authentication.
                    object obj = entry.NativeObject;

                    DirectorySearcher search = new DirectorySearcher(entry);

                    search.Filter = "(SAMAccountName=" + username + ")";
                    search.PropertiesToLoad.Add("cn");
                    SearchResult result = search.FindOne();

                    if (null == result)
                    {
                        resp.IsCredentialValidated = false;
                        resp.Message = "Logon failure: unknown user name or bad password.";
                    }

                    //Update the new path to the user in the directory.
                    //_path = result.Path;
                    //_filterAttribute = (string)result.Properties["cn"][0];
                    resp.IsCredentialValidated = true;
                }
                catch (Exception ex)
                {
                    resp.IsCredentialValidated = false;
                    resp.Message = "Logon failure: unknown user name or bad password.";
                    Logger.Info("Citrix Dealer LDAP Domine is returned: " + ex.Message);    
                }
            }
            else
            {

                try
                {
                    //Logger.Info("LDAPAccessRequest is ###################################" + XMLHelper.ConvertObjectToXml(request, typeof(LDAPAccessRequest)));
                    Logger.Info("ApplicationDirectory is ################################" + ContextType.ApplicationDirectory);
                    Logger.Info("LDAP Url is ############################################" + Properties.Settings.Default.LDAPUrl);
                    Logger.Info("LDAPContainer is #######################################" + Properties.Settings.Default.LDAPContainer);
                    ////"sgbsld01.isddc.men.maxis.com.my:389", "o=users", ContextOptions.Negotiate, "cn=LDAP_IPC,ou=netservice,o=users", "Password123"))

                    using (var prinCtx = new PrincipalContext(ContextType.ApplicationDirectory,
                                                                Properties.Settings.Default.LDAPUrl,
                                                                Properties.Settings.Default.LDAPContainer, ContextOptions.SimpleBind,
                                                                Properties.Settings.Default.LDAPUsername,
                                                                Properties.Settings.Default.LDAPPassword))
                    {
                        if (!prinCtx.ValidateCredentials(request.Username, request.Password))
                        {
                            resp.IsCredentialValidated = false;
                        }
                    }

                }
                catch (Exception ex)
                {
                    resp.IsCredentialValidated = false;
                    Logger.Info("Exception in ValidateCredentials#################################################" + ex);
                    Logger.Error(" Error: " + Util.LogException(ex));
                    //throw;
                }
            }

            Logger.Info(string.Format("Entering ValidateCredential(Username: {0})", request.Username));

            Logger.Info(string.Format("Exiting ValidateCredential(Username: {0}, IsValidated: {1})", request.Username, resp.IsCredentialValidated));
            Logger.Info("LDAPAccessResponse is ###################################" + XMLHelper.ConvertObjectToXml(resp, typeof(LDAPAccessResponse)));
            return resp;
        }
    }
}
