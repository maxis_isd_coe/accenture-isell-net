﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Online.Registration.IntegrationService.MaxisDDMFCheckWs;
using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.MaxisOrderProcessWs;
using Online.Registration.IntegrationService.MaxisgetMNPRequestServiceWs;
using Online.Registration.DAL.Models;


namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISubscriberHistory" in both code and config file together.
    [ServiceContract]
    public interface ISubscriberHistory 
    {
        [OperationContract]
        GetNewCustomerInfoResponse getNewCustomerInfo(GetNewCustomerInfoRequest request);
        //[OperationContract]
        //BusinessRuleResponse BusinessRuleCheck(BusinessRuleRequest request);
       
    }
}
