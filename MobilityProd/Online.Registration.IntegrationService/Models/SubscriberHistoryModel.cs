﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Online.Registration.DAL.Models;
//using Online.Registration.Service;
using Online.Registration.IntegrationService.RetrieveServiceInfo;
using Online.Registration.IntegrationService.MaxisDDMFCheckWs;

namespace Online.Registration.IntegrationService.Models
{
    #region getNewCustomerInfo
    [DataContract]
    [Serializable]
    public class GetNewCustomerInfoRequest
    {
        [DataMember]
        public string recordTotal { get; set; }
        [DataMember]
        public string enqIcNo { get; set; }
        [DataMember]
        public string enqNewIc { get; set; }
        [DataMember]
        public string enqName { get; set; }
        [DataMember]
        public string enqBrNo { get; set; }  
       
         
    }

    [DataContract]
    [Serializable]
    public class GetNewCustomerInfoResponse : WCFResponse
    {
        [DataMember]
        public List<IndividualResponse> individualResponse { get; set; }
        [DataMember]
        public List<BusinessResponse> businessResponse { get; set; }

        public GetNewCustomerInfoResponse()
        {
            individualResponse = new List<IndividualResponse>();
            businessResponse = new List<BusinessResponse>();
        }
    }
    #endregion 

    [DataContract]
    [Serializable]
    public class IndividualResponse:IndividualResults
    {
        [DataMember]
        public IndividualEnq individualEnq { get; set; }

        [DataMember]
        public List<IndividualResults> individualResults { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IndividualEnq
    {
        [DataMember]
        public string enqIcNo { get; set; }
        [DataMember]
        public string enqNewIc { get; set; }
        [DataMember]
        public string enqName { get; set; }
        [DataMember]
        public string enqStatusCode { get; set; }
        [DataMember]
        public string enqStatusDesc { get; set; }

    }    

    [DataContract]
    [Serializable]
    public class IndividualResults 
    {
        [DataMember]
        public string dfIcNo { get; set; }
        //[DataMember]
        //public string oloNetworkAccNo { get; set; }
        [DataMember]
        public string dfNewIc { get; set; }
        [DataMember]
        public string dbName { get; set; }
        [DataMember]
        public string dfAccount { get; set; }
        [DataMember]
        public string Company { get; set; }
        [DataMember]
        public string branch { get; set; }
        [DataMember]
        public string submitDate { get; set; }
        [DataMember]
        public string entryDate { get; set; }
        [DataMember]
        public string userId { get; set; }
        [DataMember]
        public string lastEnqCom { get; set; }
        [DataMember]
        public string lastEnqDate { get; set; }
        [DataMember]
        public string state { get; set; }
        [DataMember]
        public string remarks { get; set; }
        [DataMember]
        public string matchType { get; set; }
        
    }

    [DataContract]
    [Serializable]
    public class BusinessResponse : BusinessResults
    {
        [DataMember]
        public BusinessEnq businessEnq { get; set; }

        [DataMember]
        public List<BusinessResults> businessResults { get; set; }     
    }

    [DataContract]
    [Serializable]
    public class BusinessEnq
    {
        [DataMember]
        public string enqBrNo { get; set; } 
        [DataMember]
        public string enqName { get; set; }
        [DataMember]
        public string enqStatusCode { get; set; }
        [DataMember]
        public string enqStatusDesc { get; set; }

    }

    [DataContract]
    [Serializable]
    public class BusinessResults
    {
        [DataMember]
        public string dfBrNo { get; set; }
        [DataMember]
        public string dfbrName { get; set; }
        [DataMember]
        public string dbName { get; set; }
        [DataMember]
        public string dfAccount { get; set; }
        [DataMember]
        public string company { get; set; }
        [DataMember]
        public string branch { get; set; }
        [DataMember]
        public string submitDate { get; set; }
        [DataMember]
        public string entryDate { get; set; }
        [DataMember]
        public string userId { get; set; }
        [DataMember]
        public string lastEnqCom { get; set; }
        [DataMember]
        public string lastEnqDate { get; set; }
        [DataMember]
        public string state { get; set; }
        [DataMember]
        public string remarks { get; set; }
        [DataMember]
        public string matchType { get; set; }
    }    
}
