﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Security.Cryptography;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Xml.Linq;
using System.Net;
using System.Drawing.Drawing2D;
using log4net;
using System.Runtime.Serialization;

namespace Online.Registration.IntegrationService.Models
{
    [DataContract]
    [Serializable]
    public class retrievePenaltyByMSISDN
    {
        [DataMember]
        public string msgCode { get; set; }
        [DataMember]
        public string msgDesc { get; set; }
        [DataMember]
        public string startDate { get; set; }
        [DataMember]
        public string endDate { get; set; }
        [DataMember]
        public string contractType { get; set; }
        [DataMember]
        public string contactId { get; set; }
        [DataMember]
        public string penalty { get; set; }
        [DataMember]
        public string rrp { get; set; }
        [DataMember]
        public string phoneModel { get; set; }
        [DataMember]
        public string adminFee { get; set; }
        [DataMember]
        public string UpfrontPayment { get; set; }
        [DataMember]
        public string ContractName { set; get; }
    }
}
