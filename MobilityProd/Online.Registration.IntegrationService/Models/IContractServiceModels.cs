﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Runtime.Serialization;

namespace Online.Registration.IntegrationService.Models
{
    public class IContractServiceModels
    {
        [DataContract]
        [Serializable]
        public class IContractCopyRequest
        {
            [DataMember]
            public byte[] FileName64 { get; set; }
        }

        [DataContract]
        [Serializable]
        public class IContractCopyResponse : WCFResponse
        {
            [DataMember]
            public string ErrorCode { get; set; }

            [DataMember]
            public string ErrorMessage { get; set; }

            [DataMember]
            public bool IsCopySuccess { get; set; }
        }

    }

	[DataContract]
	[Serializable]
	public class IContractSearchRequest
	{
		[DataMember]
		public string OrderID { get; set; }

		[DataMember]
		public string IDCardValue { get; set; }

		[DataMember]
		public string MSISDN { get; set; }

		[DataMember]
		public string TranType { get; set; }

		[DataMember]
		public DateTime CreatedDate { get; set; }
	}

	[DataContract]
	[Serializable]
	public class IContractSearchResponse : WCFResponse
	{
		[DataMember]
		public string ErrorCode { get; set; }

		[DataMember]
		public string ErrorMessage { get; set; }

		[DataMember]
		public string DocumentName { get; set; }

		[DataMember]
		public Byte[] DocumentContent { get; set; }

		[DataMember]
		public string OrderID { get; set; }

		[DataMember]
		public string IDCardValue { get; set; }

		[DataMember]
		public string MSISDN { get; set; }

		[DataMember]
		public string AccountNo { get; set; }

		[DataMember]
		public string AgentCode { get; set; }

		[DataMember]
		public string CreatedDate { get; set; }
	}

	[DataContract]
	[Serializable]
	public class SearchResponse
	{
		public SearchResponse()
        {
            ContractSearchResponses = new List<IContractSearchResponse>();
			errorCode = string.Empty;
			errorMessage = string.Empty;
        }

		[DataMember]
		public List<IContractSearchResponse> ContractSearchResponses;
		[DataMember]
		public string errorCode;
		[DataMember]
		public string errorMessage;
	}
}