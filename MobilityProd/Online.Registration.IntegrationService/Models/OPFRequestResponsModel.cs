﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Online.Registration.IntegrationService.Models
{
    [DataContract]
    [Serializable]
    public class processNRCResponse
    {
        [DataMember]
        public string msgCode { get; set; }
        [DataMember]
        public string msgDesc { get; set; }
        [DataMember]
        public string trackingId { get; set; }
        [DataMember]
        public string trackingIdResets { get; set; }
    }

    [DataContract]
    [Serializable]
    public class typeExternalIdRequest
    {
        [DataMember]
        public string orderId { get; set; }
        [DataMember]
        public string lob { get; set; }
        [DataMember]
        public string newOrderInd { get; set; }
        [DataMember]
        public string fxAcctNo { get; set; }
        [DataMember]
        public string fxSubscrNo { get; set; }
        [DataMember]
        public string fxSubscrNoResets { get; set; }
        [DataMember]
        public string IMEI { get; set; } 
         [DataMember]
        public string inpF01 { get; set; }
         [DataMember]
         public string inpF02 { get; set; }
       
    }
}