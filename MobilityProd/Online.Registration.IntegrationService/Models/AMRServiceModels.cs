﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

//using Online.Registration.IntegrationService.AMRMyKadWS;

namespace Online.Registration.IntegrationService.Models
{
    #region RetrieveMyKadInfo

    [DataContract]
    [Serializable]
    public class RetrieveMyKadInfoRq
    {
        [DataMember]
        public int ApplicationID { get; set; }

        [DataMember]
        public string NewIC { get; set; }

        [DataMember]
        public string DealerCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RetrieveMyKadInfoRp : WCFResponse
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string IDCardType { get; set; }
        [DataMember]
        public string NewIC { get; set; }
        [DataMember]
        public string OldIC { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string DOB { get; set; }
        [DataMember]
        public string Nationality { get; set; }
        [DataMember]
        public string Language { get; set; }
        [DataMember]
        public string Race { get; set; }
        [DataMember]
        public string Religion { get; set; }
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string Address3 { get; set; }
        [DataMember]
        public string Postcode { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public bool FingerMatch { get; set; }
        [DataMember]
        public string FingerThumb { get; set; }
    }

    #endregion
}