﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Online.Registration.IntegrationService.Models
{
    #region CreateInteractionCase

    [DataContract]
    [Serializable]
    public class CreateInteractionCaseRequest
    {
        [DataMember]
        public string AcctExtId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string SysName { get; set; }

        [DataMember]
        public string Product { get; set; }

        [DataMember]
        public string Reason1 { get; set; }

        [DataMember]
        public string Reason2 { get; set; }

        [DataMember]
        public string Reason3 { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string Direction { get; set; }

        [DataMember]
        public string Notes { get; set; }

        [DataMember]
        public string ToProceedWith { get; set; }

        [DataMember]
        public string CasePriority { get; set; }

        [DataMember]
        public string CaseComplexity { get; set; }

        [DataMember]
        public string CaseDispatchQ { get; set; }
        // title,sysName,product,reason1,reason2,reason3,type,direction,toProceedWith,casePriority,caseComplexity,caseDispatchQ
        [DataMember]
        public string TransactionID { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public string AccountExternalID { get; set; }

        [DataMember]
        public string UserId { get; set; }

    }

    [DataContract]
    [Serializable]
    public class CreateInteractionCaseResponse : WCFResponse
    {
        [DataMember]
        public string MsgCode { get; set; }

        [DataMember]
        public string MsgDesc { get; set; }

        [DataMember]
        public string ReturnCode { get; set; }

        [DataMember]
        public string ReturnMsg { get; set; }

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string InteractionID { get; set; }

        [DataMember]
        public string CaseID { get; set; }
    }

    #endregion
}
