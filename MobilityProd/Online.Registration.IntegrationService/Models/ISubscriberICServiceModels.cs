﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using Online.Registration.DAL.Models;
using System.ComponentModel;
using Online.Registration.IntegrationService.RetrieveServiceInfo;

namespace Online.Registration.IntegrationService.Models
{

    #region Msim Details

    [DataContract]
    [Serializable]
    public class MsimDetails
    {
        [DataMember]
        public string Msisdn;
        [DataMember]
        public string FxAcctNo;
        [DataMember]
        public string FxSubscrNo;
        [DataMember]
        public string FxSubscrNoResets;
        [DataMember]
        public string PrimSecInd;
    }


    [DataContract]
    [Serializable]
    public class retrieveMismDetlsResponse
    {
        [DataMember]
        public List<MsimDetails> itemList { get; set; }

        public retrieveMismDetlsResponse()
        {
            itemList = new List<MsimDetails>();
        }
    }

    #endregion

    [DataContract]
    [Serializable]
    public class lineList
    {
        [DataMember]
        public string msisdnField;
        [DataMember]
        public string cust_nmField;
        [DataMember]
        public string acct_noField;
        [DataMember]
        public string subscr_noField;
        [DataMember]
        public string subscr_no_resetsField;
        [DataMember]
        public string subscr_statusField;
        [DataMember]
        public string prinsupp_indField;
        [DataMember]
        public string acc_AccExtId;
        [DataMember]
        public bool IsMISM;
    }
    [DataContract]
    [Serializable]
    public class Items
    {
        [DataMember]
        public string AcctIntId { get; set; }
        [DataMember]
        public string AcctExtId { get; set; }
        [DataMember]
        public string EmfConfigId { get; set; }
        [DataMember]
        public bool IsMISM { get; set; }
        [DataMember]
        public string IsBillable { get; set; }
        [DataMember]
        public string ExternalId { get; set; }
        [DataMember]
        public string SusbcrNo { get; set; }
        [DataMember]
        public string SusbcrNoResets { get; set; }
        [DataMember]
        public CustomizedCustomer Customer { get; set; }
        [DataMember]
        public List<MsimDetails> SecondarySimList { get; set; }
        [DataMember]
        public retrievegetPrinSuppResponse PrinSuppResponse { get; set; }
        [DataMember]
        public CustomizedAccount Account { get; set; }
        [DataMember]
        public bool IsActive { get; set; }

        //Added for SME CI
        [DataMember]
        public AccountDetails AccountDetails { get; set; }
        //Added for SME CI
        [DataMember]
        public CustomizedBillingInfo BillingInfo { get; set; }
        [DataMember]
        public IList<Contract> ContractsList { get; set; }
        /// <summary>
        /// Members of retrieveServiceInfoResponse mapped to ExternalId {MSISDN}. retrieveServiceInfoResponse can be one{gsm/nongsm} or many{gsm,gsm,nongsm} 
        /// or none.
        /// </summary>
        [DataMember]
        public SubscriberRetrieveServiceInfoResponse ServiceInfoResponse { get; set; }

        [DataMember]
        public retrievegetPrinSuppResponse PrinSupplimentaryResponse { get; set; }

        [DataMember]
        public IList<PackageModel> Packages { get; set; }

        public Items()
        {
            ServiceInfoResponse = new SubscriberRetrieveServiceInfoResponse();
            Customer = new CustomizedCustomer();
            PrinSuppResponse = new retrievegetPrinSuppResponse();
            ContractsList = new List<Contract>();
        }
    }
    [DataContract]
    [Serializable]
    public class retrieveDirectoryNumberResponse
    {
        [DataMember]
        public string MsgCode { get; set; }
        [DataMember]
        public string MsgDesc { get; set; }
        [DataMember]
        public List<Items> itemList { get; set; }

        public retrieveDirectoryNumberResponse()
        {
            itemList = new List<Items>();
        }

    }
    [DataContract]
    [Serializable]
    public class CustomizedCustomer : Customer
    {
        [DataMember]
        public string MsgCode { get; set; }
        [DataMember]
        public string MsgDesc { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string CustomerName { get; set; }
        [DataMember]
        public string Dob { get; set; }
        [DataMember]
        public string OldIC { get; set; }
        [DataMember]
        public string NewIC { get; set; }
        [DataMember]
        public string Brn { get; set; }
        [DataMember]
        public string PassportNo { get; set; }
        [DataMember]
        public string OtherIC { get; set; }
        [DataMember]
        public string Address1 { get; set; }
        [DataMember]
        public string Address2 { get; set; }
        [DataMember]
        public string Address3 { get; set; }
        [DataMember]
        public string PostCode { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Cbr { get; set; }
        [DataMember]
        public string Moc { get; set; }
        [DataMember]
        public string RiskCategory { get; set; }
        [DataMember]
        public string AcctActiveDt { get; set; }
        [DataMember]
        public string TotalWriteOff { get; set; }
        [DataMember]
        public string MarketCode { get; set; }
        [DataMember]
        public string AccountCategory { get; set; }
        [DataMember]
        public string EmailAddrs { get; set; }
        [DataMember]
        public string Races { get; set; }
        [DataMember]
        public string Genders { get; set; }
        [DataMember]
        public string Nation { get; set; }

    }

    [DataContract]
    [Serializable]
    public class CustomizedAccount
    {
        [DataMember]
        public string AccountNumber { get; set; }
        [DataMember]
        public string Holder { get; set; }
        [DataMember]
        public string CompanyName { get; set; }
        [DataMember]
        public string Category { get; set; }
        [DataMember]
        public string MarketCode { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string SubscribeNo { get; set; }
        [DataMember]
        public string SubscribeNoResets { get; set; }
        [DataMember]
        public string ActiveDate { get; set; }
        [DataMember]
        public string Plan { get; set; }
        [DataMember]
        public string IDNumber { get; set; }
        [DataMember]
        public string IDType { get; set; }
    }
    [DataContract]
    [Serializable]
    public class CustomizedBillingInfo
    {
        [DataMember]
        public string TotalOutstanding { get; set; }
        [DataMember]
        public string Unbilled { get; set; }
        [DataMember]
        public string CurrentBilled { get; set; }
        [DataMember]
        public string DueDate { get; set; }
        [DataMember]
        public string CreditLimit { get; set; }
        [DataMember]
        public string BalanceForward { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SubscriberRetrieveServiceInfoRequest
    {
        [DataMember]
        public string externalId { get; set; }
        [DataMember]
        public string subscrNo { get; set; }
        [DataMember]
        public string subscrNoResets { get; set; }
    }
    [DataContract]
    [Serializable]
    public class SubscriberRetrieveServiceInfoResponse
    {
        [DataMember]
        public string msgCode { get; set; }
        [DataMember]
        public string msgDesc { get; set; }
        [DataMember]
        public string lob { get; set; }
        [DataMember]
        public string packageId { get; set; }
        [DataMember]
        public string packageName { get; set; }
        [DataMember]
        public string prinSuppInd { get; set; }
        [DataMember]
        public string serviceStatus { get; set; }
        [DataMember]
        public string pukCode { get; set; }
        [DataMember]
        public string postPreInd { get; set; }
        [DataMember]
        public string mnpInd { get; set; }
        [DataMember]
        public string emf_config_id { get; set; }
        [DataMember]
        public DateTime serviceActiveDt { get; set; }
    }
    [DataContract]
    [Serializable]
    public class PrinSupEaiHeader
    {
        [DataMember]
        public string from { get; set; }
        [DataMember]
        public string to { get; set; }
        [DataMember]
        public string appId { get; set; }
        [DataMember]
        public string msgType { get; set; }
        [DataMember]
        public string msgId { get; set; }
        [DataMember]
        public string correlationId { get; set; }
        [DataMember]
        public string timestamp { get; set; }
    }
    [DataContract]
    [Serializable]
    public class SubscribergetPrinSuppRequest
    {
        [DataMember]
        public string msisdn { get; set; }
        [DataMember]
        public PrinSupEaiHeader eaiHeader { get; set; }
    }
    [DataContract]
    [Serializable]
    public class retrieveAcctListByICResponse
    {
        [DataMember]
        public string MsgCode { get; set; }
        [DataMember]
        public string MsgDesc { get; set; }
        [DataMember]
        public IList<Items> itemList { get; set; }

        public retrieveAcctListByICResponse()
        {
            itemList = new List<Items>();
        }

    }
    [DataContract]
    [Serializable]
    public class retrievegetPrinSuppResponse
    {
        [DataMember]
        public string ParentMsisdn { get; set; }
        [DataMember]
        public string MsgCode { get; set; }
        [DataMember]
        public string MsgDesc { get; set; }
        [DataMember]
        public IList<lineList> itemList { get; set; }

        public retrievegetPrinSuppResponse()
        {
            itemList = new List<lineList>();
        }

    }
    [DataContract]
    [Serializable]
    public class PackageModel
    {
        [DataMember]
        public string PackageID { get; set; }
        [DataMember]
        public string PackageDesc { get; set; }
        [DataMember]
        public string packageInstId { get; set; }
        [DataMember]
        public string packageInstIdServ { get; set; }
        [DataMember]
        public string Contract12Months { get; set; }
        [DataMember]
        public string Contract24Months { get; set; }
        [DataMember]
        public string packageActiveDt { get; set; }
        [DataMember]
        public List<Components> compList { get; set; }
    }
    [DataContract]
    [Serializable]
    public class Components
    {
        [DataMember]
        public string componentId { get; set; }
        [DataMember]
        public string componentInstId { get; set; }
        [DataMember]
        public string componentInstIdServ { get; set; }
        [DataMember]
        public string componentActiveDt { get; set; }
        [DataMember]
        public string componentInactiveDt { get; set; }
        [DataMember]
        public string componentDesc { get; set; }
        [DataMember]
        public string componentShortDisplay { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ContractDetails
    {
        [DataMember]
        public string CMSSID { get; set; }
        [DataMember]
        public string PackageID { get; set; }
        [DataMember]
        public string PackageDesc { get; set; }
        [DataMember]
        public string ContractRemaining { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public DateTime ActiveDate { get; set; }
        [DataMember]
        public int ContractDuration { get; set; }
        [DataMember]
        public string ContractExtend { get; set; }
        [DataMember]
        public string ContractExtendType { get; set; }
        [DataMember]
        public string ComponentDesc { get; set; }
    }

    #region API call save
    [DataContract]
    [Serializable]
    public class SaveCallStatusResp : WCFResponse
    {
        [DataMember]
        public int? APIID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SaveCallStatusReq
    {
        [DataMember]
        public tblAPICallStatus tblAPICallStatusMessagesval { get; set; }
    }
    #endregion API call save

    #region RetrieveBillingInfo Added by Sindhu on 04-Jun-2013
    [DataContract]
    [Serializable]
    public class retrieveBillngInfoResponse
    {
        [DataMember]
        public string msgCode { get; set; }
        [DataMember]
        public string msgDesc { get; set; }
        [DataMember]
        public string totalAmtDue { get; set; }
        [DataMember]
        public string balanceForward { get; set; }
        [DataMember]
        public string totalCurrBill { get; set; }
        [DataMember]
        public string currBillDate { get; set; }
        [DataMember]
        public string unbilledAmt { get; set; }
        [DataMember]
        public string unbilledEffDate { get; set; }
        [DataMember]
        public string creditLimit { get; set; }
        [DataMember]
        public string payResp { get; set; }
        [DataMember]
        public string deposit { get; set; }
    }
    #endregion

    #region Added By Vahini for Add/Remove VAS

    [DataContract]
    [Serializable]
    public class ComponentList
    {
        [DataMember]
        public string PackageId { set; get; }
        [DataMember]
        public string ComponentId { set; get; }
    }
    #region chkFxVasDependency related
    /// <summary>
    /// <Author>Sutan Dan</Author>
    /// <Date>06102013</Date>
    /// Request properties for chkFxVasDependency
    /// </summary>
    /// 
    [DataContract]
    [Serializable]
    public class ChkFxVasDependencyRequestNew
    {
        [DataMember]
        public string emfConfigId { get; set; }
        [DataMember]
        public string mandatoryPackageId { get; set; }
        [DataMember]
        public List<VasComponent> componentList { get; set; }
    }

    /// <summary>
    /// <Author>Sutan Dan</Author>
    /// <Date>06102013</Date>
    /// Request properties for chkFxVasDependency
    /// </summary>
    [DataContract]
    [Serializable]
    public class VasComponentNew
    {
        [DataMember]
        public string packageId { get; set; }
        public string componentId { get; set; }
    }
    /// <summary>
    /// <Author>Sutan Dan</Author>
    /// <Date>06102013</Date>
    /// Response properties for chkFxVasDependency
    /// </summary>
    [DataContract]
    [Serializable]
    public class ChkFxVasDependencyResponseNew : WCFResponse
    {
        [DataMember]
        public string msgCode { get; set; }
        [DataMember]
        public string msgDesc { get; set; }
        [DataMember]
        public List<string> mxsRuleMessages { get; set; }

    }
    #endregion chkFxVasDependency related

    [DataContract]
    [Serializable]
    public class SimDetails
    {
        [DataMember]
        public string ExternalID { get; set; }
        [DataMember]
        public int ExternalIDType { get; set; }
        [DataMember]
        public int InventoryTypeID { get; set; }
        [DataMember]
        public string InventoryDisplayValue { get; set; }
        [DataMember]
        public string Reason { get; set; }
        [DataMember]
        public string ActiveDt { get; set; }
        [DataMember]
        public string InactiveDt { get; set; }
    }


    [DataContract]
    [Serializable]
    public class retrieveSimDetlsResponse
    {
        [DataMember]
        public IList<SimDetails> itemList { get; set; }

        public retrieveSimDetlsResponse()
        {
            itemList = new List<SimDetails>();
        }

    }
    [DataContract]
    [Serializable]
    public class reassingVases
    {
        [DataMember]
        public string NewComponentId { set; get; }
        [DataMember]
        public string NewPackageId { set; get; }
        [DataMember]
        public string OldComponentId { set; get; }
        [DataMember]
        public string OldPackageId { set; get; }
    }
    [DataContract]
    [Serializable]
    public class ReassignVasResponce
    {
        [DataMember]
        public string msgCode { set; get; }
        [DataMember]
        public string msgDesc { set; get; }
        [DataMember]
        public List<reassingVases> reassingVasesList { set; get; }
    }

    #endregion

    [DataContract]
    [Serializable]
    public class ReassignVasRequest
    {
        [DataMember]
        public string FxAccNo { set; get; }
        [DataMember]
        public string FxSubscrNo { set; get; }
        [DataMember]
        public string FxSubscrNoResets { set; get; }
        [DataMember]
        public List<packagePairs> packagePairsList { set; get; }
    }


    [DataContract]
    [Serializable]
    public class packagePairs
    {
        [DataMember]
        public string newPackageId { get; set; }
        [DataMember]
        public string oldPackageId { get; set; }
    }

    [DataContract]
    [Serializable]
    public class EligiblePackageList
    {
        [DataMember]
        public string DowngradeId { get; set; }
        [DataMember]
        public string PackageId { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AvalablePackages
    {
        [DataMember]
        public string MsgCode { get; set; }
        [DataMember]
        public string MsgDesc { get; set; }
        [DataMember]
        public List<EligiblePackageList> EligiblePackagesList { get; set; }

    }

    [DataContract]
    [Serializable]
    public class AccountDetails
    {

        [DataMember]
        public string FxAcctNo { get; set; }
        [DataMember]
        public string ParentId { get; set; }
        [DataMember]
        public string HierarchyId { get; set; }
        [DataMember]
        public string FxAcctExtId { get; set; }
        [DataMember]
        public string AcctCategory { get; set; }
        [DataMember]
        public string AcctSegId { get; set; }
        [DataMember]
        public string MktCode { get; set; }
        [DataMember]
        public string VipCode { get; set; }
        [DataMember]
        public string CreditLimit { get; set; }
        [DataMember]
        public string DirectDebit { get; set; }
        [DataMember]
        public string BillCycle { get; set; }
        [DataMember]
        public string PaymntResp { get; set; }
        [DataMember]
        public string CollectionStatus { get; set; }
        [DataMember]
        public string AcctActiveDt { get; set; }
        [DataMember]
        public string TotalWriteOff { get; set; }
        [DataMember]
        public string CustomerType { get; set; }
        [DataMember]
        public string AcctType { get; set; }
        [DataMember]
        public string TotalWriteOffUCCS { get; set; }
        [DataMember]
        public string LODIndicator { get; set; }
        [DataMember]
        public string SummonIndicator { get; set; }
    }


    #region smart related  methods

    public class retrievePenalty
    {
		public retrievePenalty()
		{
			paramLists = new List<ParamList>();
		}
        public string msgCode { get; set; }
        public string msgDesc { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string contractType { get; set; }
        public string contactId { set; get; }
        public string penalty { get; set; }
        public string penaltyType { get; set; }
        public string componentId { get; set; }
        public string componentInstanceId { get; set; }

        public string rrp { get; set; }
        public string phoneModel { get; set; }
        public string adminFee { get; set; }
        public string UpfrontPayment { get; set; }
        public string ContractName { set; get; }
        public string DeviceModel { get; set; }
        public string DeviceIMEI { get; set; }
        public string UpgradeFee { get; set; }
		public List<ParamList> paramLists { get; set; }
    }

	public class ParamList {
		public string paramID { get; set; }
		public string paramName { get; set; }
		public string paramValue { get; set; }
	}

    #endregion

    #region RetrieveBillingInfo Added by Sindhu on 04-Jun-2013
    [DataContract]
    [Serializable]
    public class retrievePrepaidSubscriberResponse
    {
        [DataMember]
        public string msgCode { get; set; }
        [DataMember]
        public string msgDesc { get; set; }
        [DataMember]
        public string mainAcctBalance { get; set; }
        [DataMember]
        public string mainAcctExpiryDt { get; set; }
        [DataMember]
        public string mainAcctGraceEndDt { get; set; }
        [DataMember]
        public string acctStatus { get; set; }
        [DataMember]
        public string pid { get; set; }
    }
    #endregion

}
