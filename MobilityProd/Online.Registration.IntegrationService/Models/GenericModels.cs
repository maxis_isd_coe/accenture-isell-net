﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Online.Registration.IntegrationService.Models
{
    [DataContract]
    [Serializable]
    public class WCFResponse
    {
        public WCFResponse()
        {
            Success = false;
        }

        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string StackTrace { get; set; }
    }
}
