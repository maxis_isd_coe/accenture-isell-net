﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace Online.Registration.IntegrationService.Models
{

    #region getSubscriptionInfo
    [DataContract]
    [Serializable]
    public class GetSubscriptionInfoRequest
    {
        [DataMember]
        public string acctExtId { get; set; }
    }

    [DataContract]
    [Serializable]
    public class GetSubscriptionInfoResponse : WCFResponse
    {
        [DataMember]
        public string msgCode { get; set; }
        [DataMember]
        public string msgDesc { get; set; }
        [DataMember]
        public string hcSuppressSubscription { get; set; }
        [DataMember]
        public string emailBillSubscription { get; set; }
        [DataMember]
        public string emailAddress { get; set; }
        [DataMember]
        public string smsAlertFlag { get; set; }
        [DataMember]
        public string smsNo { get; set; }
        [DataMember]
        public string emailSubscriptionStatus { get; set; }
        [DataMember]
        public string updatedDate { get; set; }
        [DataMember]
        public string updatedTime { get; set; }
        [DataMember]
        public string updatedBy { get; set; }
        [DataMember]
        public string updatedChannel { get; set; }

    }
    #endregion

    #region updSubscriptionInfo
    [DataContract]
    [Serializable]
    public class UpdSubscriptionInfoRequest
    {
        [DataMember]
        public string regId { get; set; }
        [DataMember]
        public string acctExtId { get; set; }
        [DataMember]
        public string hcSuppressSubscription { get; set; }
        [DataMember]
        public string emailBillSubscription { get; set; }
        [DataMember]
        public string emailAddress { get; set; }
        [DataMember]
        public string smsAlertFlag { get; set; }
        [DataMember]
        public string smsNo { get; set; }
        [DataMember]
        public string updatedBy { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UpdSubscriptionInfoResponse : WCFResponse
    {
        [DataMember]
        public string msgCode { get; set; }
        [DataMember]
        public string msgDesc { get; set; }
    }
    #endregion
}