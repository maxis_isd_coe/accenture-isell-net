﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using Online.Registration.IntegrationService.MbaCreditCheckWs;
using Online.Registration.DAL.Models;
using System.ComponentModel;
using Online.Registration.IntegrationService.ExtraTenWS;
//using Online.Registration.IntegrationService.RetrieveServiceInfo;


namespace Online.Registration.IntegrationService.Models
{
    #region Enums

    [DataContract]
    [Serializable]
    public enum TransactionType
    {
        [EnumMember]
        AccountNo,
        [EnumMember]
        Msisdn
    }

    [DataContract]
    [Serializable]
    public enum BusinessRuleType
    {
        [EnumMember]
        Age,
        [EnumMember]
        Address,
        [EnumMember]
        DDMF,
        [EnumMember]
        TotalLine,
        [EnumMember]
        OutstandingCreditCheck,
        [EnumMember]
        PrincipalLineCheck,
        [EnumMember]
        ContractCheck
    }

    #endregion

    [Serializable]
    [DataContract]
    public class ExtraTenRetreivalResponse : WCFResponse
    {
        [DataMember]
        public ExtraTenRetrieveService.typeRetrieveExtraTenResponse ExtraTenInfo { get; set; }
    }
    [Serializable]
    [DataContract]
    public class ExtraTenUpdateResponse : WCFResponse
    {
        [DataMember]
        public typeProcessExtraTenResponse ExtraTenProcessResponse { get; set; }
    }

    #region Retrieve Subscriber Details

    [DataContract]
    [Serializable]
    public class RetrieveSuscriberDetailsRequest
    {
        [DataMember]
        public TransactionType TransactionType { get; set; }

        [DataMember]
        public string TransactionValue { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RetrieveSuscriberDetailsResponse : WCFResponse
    {
        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public DateTime DOB { get; set; }

        [DataMember]
        public string OldICNumber { get; set; }

        [DataMember]
        public string NewICNumber { get; set; }

        [DataMember]
        public string BusinessRegistrationNumber { get; set; }

        [DataMember]
        public string PassportNumber { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string PostCode { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string CanBeReachedNumber { get; set; }

        [DataMember]
        public string MaxisOneClubStatus { get; set; }

        [DataMember]
        public string CustomerRiskCategory { get; set; }
    }

    #endregion

    #region Retrieve Billing Details

    [DataContract]
    [Serializable]
    public class RetrieveBillingDetailsRequest
    {
        [DataMember]
        public string AccountNo { get; set; }

        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public TransactionType TransactionType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RetrieveBillingDetailsResponse : WCFResponse
    {
        [DataMember]
        public double TotalDueAmount { get; set; }

        [DataMember]
        public double CurrentBillAmount { get; set; }

        [DataMember]
        public DateTime CurrentBillDueDate { get; set; }

        [DataMember]
        public double UnbilledAmount { get; set; }

        [DataMember]
        public double CreditLimit { get; set; }

        [DataMember]
        public string PaymentResponsibility { get; set; }

        [DataMember]
        public double BalanceBringForwardAmount { get; set; }

        [DataMember]
        public List<PaymentDetail> LastPayments { get; set; }

        [DataMember]
        public string UnbilledEffectiveDT { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentDetail
    {
        [DataMember]
        public double Amount { get; set; }

        [DataMember]
        public DateTime Date { get; set; }
    }

    #endregion

    #region Retrieve Component Info

    [DataContract]
    [Serializable]
    public class RetrieveComponentInfoRequest
    {
        [DataMember]
        public string ExternalID { get; set; }

        [DataMember]
        public string SubscriberNo { get; set; }

        [DataMember]
        public string SubscriberNoResets { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RetrieveComponentInfoResponse : WCFResponse
    {
        [DataMember]
        public bool ValidSubscriber { get; set; }

        [DataMember]
        public List<Vas> Components { get; set; }

        [DataMember]
        public List<Contract> Contracts { get; set; }
    }

    [DataContract]
    [Serializable]
    public class Vas
    {
        [DataMember]
        public string ComponentID { get; set; }

        [DataMember]
        public string ComponentDesc { get; set; }

        [DataMember]
        public DateTime ActivationDate { get; set; }
    }

    [DataContract]
    [Serializable]
    public class Contract
    {
        [DataMember]
        public string ContractID { get; set; }

        [DataMember]
        public string ContractName { get; set; }

        [DataMember]
        public string ContractType { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }
    }

    #endregion

    #region Retrieve External Account Details

    [DataContract]
    [Serializable]
    public class RetrieveExtAccDetailsRequest
    {
        [DataMember]
        public string ServiceNo { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RetrieveExtAccDetailsResponse : WCFResponse
    {
        [DataMember]
        public string ExtAcctNo { get; set; }
        [DataMember]
        public string ProdTyp { get; set; }
        [DataMember]
        public string MktCode { get; set; }
        [DataMember]
        public string VIPCode { get; set; }
        [DataMember]
        public string MocStat { get; set; }
        [DataMember]
        public string CustProf { get; set; }
        [DataMember]
        public string LangInd { get; set; }
        [DataMember]
        public string MsgCD { get; set; }
    }

    #endregion

    #region Blacklist Check

    [DataContract]
    [Serializable]
    public class BlacklistCheckRequest
    {
        [DataMember]
        public creditCheckOperation CreditCheckRequest { get; set; }

        [DataMember]
        public string ExternalCheckXML { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BlacklistCheckResponse : WCFResponse
    {
        [DataMember]
        public bool IsInternalBlacklisted { get; set; }

        [DataMember]
        public bool IsExternalBlacklisted { get; set; }

        [DataMember]
        public string ResultMessage { get; set; }
    }

    #endregion

    #region Number Selection

    [DataContract]
    [Serializable]
    public class MobileNoSelectionRequest
    {
        [DataMember]
        public string DesireNo { get; set; }
        [DataMember]
        public string DealerCode { get; set; }

        //Added by ravi for mobile no prefix on apr 04 2014
        [DataMember]
        public string MobileNoPrefix { get; set; }

    }

    [DataContract]
    [Serializable]
    public class MobileNoSelectionResponse : WCFResponse
    {
        [DataMember]
        public List<string> MobileNos { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MobileNoUpdateRequest
    {
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public string DealerCode { get; set; }
        [DataMember]
        public string DealerName { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ContactNo { get; set; }
        [DataMember]
        public string ICNo { get; set; }
        [DataMember]
        public string RegType { get; set; }
        [DataMember]
        public string Account { get; set; }
        [DataMember]
        public string MaxisCenter { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string SelectFlag { get; set; }
        [DataMember]
        public DateTime SelectDate { get; set; }
        [DataMember]
        public string DealerName2 { get; set; }
        [DataMember]
        public string DealerCode2 { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MobileNoUpdateResponse : WCFResponse
    {

    }

    #endregion

    #region Postcode Validation

    [DataContract]
    [Serializable]
    public class PostcodeValidationRequest
    {
        [DataMember]
        public string InputXML { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PostcodeValidationResponse : WCFResponse
    {

    }

    #endregion

    #region Update Order Status

    [DataContract]
    [Serializable]
    public class UpdateOrderStatusRequest
    {
        [DataMember]
        public string LoginID { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string MsgCode { get; set; }
        [DataMember]
        public string MsgDesc { get; set; }
        [DataMember]
        public string OrderID { get; set; }
        [DataMember]
        public string EventType { get; set; }
        [DataMember]
        public string FxAcctExtID { get; set; }
        [DataMember]
        public string FxAccIntID { get; set; }
        [DataMember]
        public string FxSubscrNo { get; set; }
        [DataMember]
        public string FxSubscrNoResets { get; set; }
        [DataMember]
        public string FxOrderID { get; set; }
        [DataMember]
        public string FxOrderIDResets { get; set; }
        [DataMember]
        public List<FxExternal> ExternalList { get; set; }
    }

    [DataContract]
    [Serializable]
    public class FxExternal
    {
        [DataMember]
        public string ExternalID { get; set; }
        [DataMember]
        public string ExternalIDType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UpdateOrderStatusResponse
    {
        public UpdateOrderStatusResponse()
        {
            MsgCode = Properties.Settings.Default.KenanSuccessCode;
        }

        [DataMember]
        public string MsgCode { get; set; }
        [DataMember]
        public string MsgDesc { get; set; }
    }

    #endregion

    #region Kenan Order Creation
    [DataContract]
    [Serializable]
    public class OrderCreationRequestMNP
    {
        [DataMember]
        public string orderId { get; set; }
        [DataMember]
        public string lob { get; set; }
        [DataMember]
        public string externalId { get; set; }

        [DataMember]
        public string extenalIdType { get; set; }
        [DataMember]
        public string inventoryType { get; set; }
        [DataMember]
        public string newCustomer { get; set; }
        [DataMember]
        public string fxAcctIntId { get; set; }
        [DataMember]
        public string fxAcctExtId { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public KenanCustomerMNP Customer { get; set; }
        [DataMember]
        public KenanAccountMNP Account { get; set; }
        [DataMember]
        public KenanTypeExternalId[] ExternalIds { get; set; }

    }

    [DataContract]
    [Serializable]
    public class ResubmitOrderCreationRequestMNP
    {
        [DataMember]
        public string orderIdField{ get; set; }
        [DataMember]
        public string lobField{ get; set; }
        [DataMember]
        public ResubmitNewPortInRequest newPortInRequestField{ get; set; }
        [DataMember]
        public ResubmitCommitOrderResponse[] commitOrderResponseListField{ get; set; }
    }


    [DataContract]
    [Serializable]
    public class ResubmitNewPortInRequest
    {
        [DataMember]
        public string[] msisdnField { get; set; }
        [DataMember]
        public string[] msisdnTypeField { get; set; }
        [DataMember]
        public string[] identityField { get; set; }
        [DataMember]
        public string[] identityTypeField { get; set; }
        [DataMember]
        public string donorIdField { get; set; }
        [DataMember]
        public string acctCategoryField { get; set; }
        [DataMember]
        public string acctSegmentField { get; set; }
        [DataMember]
        public string createByField { get; set; }
        [DataMember]
        public string customerNameField { get; set; }
        [DataMember]
        public string accountNoField { get; set; }
        [DataMember]
        public string reqReasonField { get; set; }
        [DataMember]
        public string oldReqIdField { get; set; }
        [DataMember]
        public string reqIdField { get; set; }
        [DataMember]
        public string mktCodeField { get; set; }
        [DataMember]
        public string resubmitFlagField { get; set; }
        [DataMember]
        public string mpgAcctSegmentField { get; set; }
        [DataMember]
        public string mpgAcctCategoryField { get; set; }
        [DataMember]
        public string recipientIdField { get; set; }
        [DataMember]
        public string piStatusField { get; set; }

    }

    [DataContract]
    [Serializable]
    public class ResubmitCommitOrderResponse
    {

        [DataMember]
        public string subscrNoField { get; set; }
        [DataMember]
        public string subscrNoResetsField { get; set; }
        [DataMember]
        public string msisdnField { get; set; }
        [DataMember]
        public string fxOrderIdField { get; set; }
        [DataMember]
        public string fxServiceOrderIdField { get; set; }

    }

    [DataContract]
    [Serializable]
    public class KenanExtendedDataMNP
    {

        public string paramId;

        public string paramName;

        public string paramValue;
    }
    [DataContract]
    [Serializable]
    public class KenanCustomerMNP
    {
        [DataMember]
        public string icType;
        [DataMember]
        public string icNo;
        [DataMember]
        public string billLname;
        [DataMember]
        public string billCompany;
        [DataMember]
        public string billTitle;
        [DataMember]
        public string billAddress1;
        [DataMember]
        public string billAddress2;
        [DataMember]
        public string billAddress3;
        [DataMember]
        public string billCity;
        [DataMember]
        public string billState;
        [DataMember]
        public string billZip;
        [DataMember]
        public string billCounty;
        [DataMember]
        public string billCountryCode;
        [DataMember]
        public string CBR { get; set; }
        [DataMember]
        public int NationalityID { get; set; }
        [DataMember]
        public int RaceID { get; set; }
        [DataMember]
        public int CustomerTitleID { get; set; }
        [DataMember]
        public int PayModeID { get; set; }
        [DataMember]
        public int IDCardTypeID { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public string EmailAddr { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public DateTime DateOfBirth { get; set; }
        [DataMember]
        public int IsVIP { get; set; }

    }
    [DataContract]
    [Serializable]
    public class KenanAccountMNP
    {
        [DataMember]
        public string parentAccountExternalId;
        [DataMember]
        public string parentAccountExternalIdType;
        [DataMember]
        public string accountStatus;
        [DataMember]
        public int mktCode;
        [DataMember]
        public int acctCategory;
        [DataMember]
        public string accountType;
        [DataMember]
        public string accountSegId;
        [DataMember]
        public string billDispMeth;
        [DataMember]
        public string billFmtOpt;
        [DataMember]
        public string billFranchiseTaxCode;
        [DataMember]
        public string billingFrequency;
        [DataMember]
        public string chargeThreshold;
        [DataMember]
        public string clearingHouseId;
        [DataMember]
        public string collectionIndicator;
        [DataMember]
        public string collectionStatus;
        [DataMember]
        public string custPhone1;
        [DataMember]
        public string faxNo;
        [DataMember]
        public string creditRating;
        [DataMember]
        public string currencyCode;
        [DataMember]
        public string custEmail;
        [DataMember]
        public string custFaxno;
        [DataMember]
        public string custFranchiseTaxCode;
        [DataMember]
        public string exrateClass;
        [DataMember]
        public string globalContractStatus;
        [DataMember]
        public string hierarchyId;
        [DataMember]
        public string insertGrpId;
        [DataMember]
        public string languageCode;
        [DataMember]
        public string msgGrpId;
        [DataMember]
        public string noBill;
        [DataMember]
        public string owningCostCtr;
        [DataMember]
        public string parentId;
        [DataMember]
        public int payMethod;
        [DataMember]
        public string rateClassDefault;
        [DataMember]
        public string regulatoryId;
        [DataMember]
        public string revRcvCostCtr;
        [DataMember]
        public string salesCode;
        [DataMember]
        public int vipCode;
        [DataMember]
        public string inpF01;
        [DataMember]
        public string inpF02;
        [DataMember]
        public string inpF03;
        [DataMember]
        public string inpF04;
        [DataMember]
        public string inpF05;
        [DataMember]
        public string inpF06;
        [DataMember]
        public string inpF07;
        [DataMember]
        public string inpF08;
        [DataMember]
        public string inpF09;
        [DataMember]
        public string inpF10;
        [DataMember]
        public KenanExtendedDataMNP[] extendedData;
    }
    [DataContract]
    [Serializable]
    public class OrderCreationRequest
    {
        [DataMember]
        public string Lobtype { get; set; }
        [DataMember]
        public string OrderID { get; set; }
        [DataMember]
        public string ExternalID { get; set; }

        [DataMember]
        public int ExtIDTypeID { get; set; }
        [DataMember]
        public int MarketCodeID { get; set; }
        [DataMember]
        public int AccCategoryID { get; set; }
        [DataMember]
        public int VIPCodeID { get; set; }
        [DataMember]
        public int CountryID { get; set; }

        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string UserSession { get; set; }
        [DataMember]
        public KenanCustomer Customer { get; set; }
        [DataMember]
        public BillAddress BillingAddress { get; set; }

        [DataMember]
        public string LastAccessID { get; set; }
        [DataMember]
        public KenanTypeExternalId[] ExternalIds { get; set; }



    }

    [DataContract]
    [Serializable]
    public class OrderCreationResponse : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ResubmitOrderCreationMNPResponse : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class typeKnockOffDeviceRequests
    {
        [DataMember]
        public string orderIdField { get; set; }
        [DataMember]
        public string fxAcctExtIdField { get; set; }
        [DataMember]
        public string maxisCenterIdField { get; set; }
        [DataMember]
        public string serialNoField { get; set; }
        [DataMember]
        public string statusIDField { get; set; }
        [DataMember]
        public string custNameField { get; set; }
        [DataMember]
        public string icNoField { get; set; }
        [DataMember]
        public string msisdnField { get; set; }
        [DataMember]
        public string custAddressField { get; set; }
        [DataMember]
        public string custStateField { get; set; }
        [DataMember]
        public string regFormIDField { get; set; }
        [DataMember]
        public string salesDateField { get; set; }
        [DataMember]
        public string salesTimeField { get; set; }
        [DataMember]
        public string inpF01Field { get; set; }
        [DataMember]
        public string inpF02Field { get; set; }
        [DataMember]
        public string inpF03Field { get; set; }
        [DataMember]
        public string inpF04Field { get; set; }
        [DataMember]
        public string inpF05Field { get; set; }
        [DataMember]
        public string inpF06Field { get; set; }
        [DataMember]
        public string inpF07Field { get; set; }
        [DataMember]
        public string inpF08Field { get; set; }
        [DataMember]
        public string inpF09Field { get; set; }
        [DataMember]
        public string inpF10Field { get; set; }


    }
    [DataContract]
    [Serializable]
    public class typeCancelCenterOrders
    {
        [DataMember]
        public string orderIdField { get; set; }
        [DataMember]
        public string fxAcctIntIdField { get; set; }
        [DataMember]
        public string fxServerIdField { get; set; }
        [DataMember]
        public string fxSubscrNoField { get; set; }
        [DataMember]
        public string fxSubscrNoResetsField { get; set; }
        [DataMember]
        public string fxOrderIdField { get; set; }
        [DataMember]
        public string fxServiceOrderIdField { get; set; }
        [DataMember]
        public string ncrReasonCodeField { get; set; }
        [DataMember]
        public string requiredNrcField { get; set; }
        [DataMember]
        public typeNrcInfos[] nrcInfoField { get; set; }

    }
    [DataContract]
    [Serializable]
    public class typeCloseCenterOrders
    {
        [DataMember]
        public string orderIdField { get; set; }
        [DataMember]
        public string fxAcctIntIdField { get; set; }
        [DataMember]
        public string fxServerIdField { get; set; }
        [DataMember]
        public string fxSubscrNoField { get; set; }
        [DataMember]
        public string fxSubscrNoResetsField { get; set; }
        [DataMember]
        public string fxOrderIdField { get; set; }
        [DataMember]
        public string fxServiceOrderIdField { get; set; }
        [DataMember]
        public string requiredNrcField { get; set; }
        [DataMember]
        public List<typeNrcInfos> nrcInfoField { get; set; }


    }
    [DataContract]
    [Serializable]
    public class typeNrcInfos
    {
        [DataMember]
        public string tranxTypField { get; set; }
        [DataMember]
        public string tranxValField { get; set; }
        [DataMember]
        public string nrcIdField { get; set; }
        [DataMember]
        public string nrcDescField { get; set; }
        [DataMember]
        public string amountField { get; set; }
    }
    #region Sutan Added Code 21ST March 2013
    [DataContract]
    [Serializable]
    public class MNPOrderCreationResponse : WCFResponse
    {

    }
    [DataContract]
    [Serializable]
    public class MNPFulfillCenterOrderMNPResponse : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class MNPtypeFullfillCenterOrderMNP
    {
        [DataMember]
        public string orderId { get; set; }
        [DataMember]
        public string UserSession { get; set; }
        [DataMember]
        public string assignPrinSupp { get; set; }
        [DataMember]
        public KenanTypeExternalId[] externalldList { get; set; }
        [DataMember]
        public KenanTypeService service { get; set; }
        [DataMember]
        public KenanTypePackage[] package { get; set; }
        [DataMember]
        public KenanTypeInputContract contract { get; set; }
        [DataMember]
        public MNPprinLine2 prinLine { get; set; }
        [DataMember]
        public MNPsuppLine2 suppLine { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MNPprinLine2
    {
        public string prinMsisdn { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MNPsuppLine2
    {
        public string suppMsisdn { get; set; }
        public string actionId { get; set; }
    }
    #endregion Sutan Added Code 21ST March 2013

    [DataContract]
    [Serializable]
    public class BillAddress
    {
        [DataMember]
        public string UnitNo { get; set; }
        [DataMember]
        public string BuildingNo { get; set; }
        [DataMember]
        public string Line1 { get; set; }
        [DataMember]
        public string Line2 { get; set; }
        [DataMember]
        public string Line3 { get; set; }
        [DataMember]
        public string Town { get; set; }
        [DataMember]
        public string Postcode { get; set; }
        [DataMember]
        public int StateID { get; set; }
        [DataMember]
        public int CountryID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class KenanCustomer
    {
        [DataMember]
        public int NationalityID { get; set; }
        [DataMember]
        public int RaceID { get; set; }
        [DataMember]
        public int CustomerTitleID { get; set; }
        [DataMember]
        public int PayModeID { get; set; }
        [DataMember]
        public int IDCardTypeID { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public string EmailAddr { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public DateTime DateOfBirth { get; set; }
        //Added by Patanjali on 27-03-2013 to support CBR and ALternate MSISDN
        [DataMember]
        public string CBR { get; set; }
        [DataMember]
        public string AlternateMSISDN { get; set; }
        //Added by Patanjali on 27-03-2013 to support CBR and ALternate MSISDN
        [DataMember]
        public int IsVIP { get; set; }
        //Added by Jai on 16-07-2013 for VIP Account Creation 
    }

    #endregion

    #region Kenan Order Fulfill

    [DataContract]
    [Serializable]
    public class OrderFulfillRequest
    {
        public OrderFulfillRequest()
        {
            ExternalIDList = new List<ExternalIDDetails>();
        }
        [DataMember]
        public string actiontype { get; set; }
        [DataMember]
        public string regLob { get; set; }
        [DataMember]
        public string OrderID { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public int NationalityID { get; set; }
        [DataMember]
        public int CountryID { get; set; }
        [DataMember]
        public int RaceID { get; set; }
        [DataMember]
        public string UserSession { get; set; }
        [DataMember]
        public DateTime DateOfBirth { get; set; }
        [DataMember]
        public List<ExternalIDDetails> ExternalIDList { get; set; }
        [DataMember]
        public string AccountExternalID { get; set; }
        [DataMember]
        public SvcPackage Package { get; set; }
        [DataMember]
        public SvcContract Contract { get; set; }
        [DataMember]
        public string LastAccessID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExternalIDDetails
    {
        [DataMember]
        public string ExternalID { get; set; }
        [DataMember]
        public int ExternalIDTypeID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SvcPackage
    {
        [DataMember]
        public string PackageID { get; set; }
        [DataMember]
        public List<string> ComponentIDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SvcContract
    {
        [DataMember]
        public string PackageID { get; set; }
        [DataMember]
        public string ComponentID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class OrderFulfillResponse : WCFResponse
    {

    }

    #endregion

    #region Business Rule

    [DataContract]
    [Serializable]
    public class BusinessRuleRequest
    {
        [DataMember]
        public List<string> RuleNames { get; set; }
        [DataMember]
        public string IDCardType { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public string DateOfBirth { get; set; }
        [DataMember]
        public string DDMFCheck { get; set; }
        [DataMember]
        public string postalCode { get; set; }
        [DataMember]
        public string state { get; set; }
        [DataMember]
        public string kenanExtAcctId { get; set; }
        [DataMember]
        public string totalLinesAllowed { get; set; }
        [DataMember]
        public string totalPrincipleLinesAllowed { get; set; }
        [DataMember]
        public string totalContractsAllowed { get; set; }
        [DataMember]
        public businessDataType[] businessDataList { get; set; }
        [DataMember]
        public BusinessRuleType businessRuleType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class businessDataType
    {
        [DataMember]
        public string dataName { get; set; }
        [DataMember]
        public string dataValue { get; set; }
    }
    [DataContract]
    [Serializable]
    public class BusinessRuleResponse : WCFResponse
    {
        public BusinessRuleResponse()
        {
            IsBlacklisted = true;
            #region VLT ADDED CODE
            IsAgeCheckFailed = false;
            IsDDMFCheckFailed = false;
            IsAddressCheckFailed = false;
            IsOutstandingCreditCheckFailed = false;
            IsTotalLineCheckFailed = false;
            IsPrincipleLineCheckFailed = false;
            #endregion
        }

        [DataMember]
        public bool IsBlacklisted { get; set; }
        [DataMember]
        public string ResultMessage { get; set; }
        [DataMember]
        public bool IsAgeCheckFailed { get; set; }
        [DataMember]
        public bool IsDDMFCheckFailed { get; set; }
        [DataMember]
        public bool IsAddressCheckFailed { get; set; }
        [DataMember]
        public bool IsOutstandingCreditCheckFailed { get; set; }
        [DataMember]
        public bool IsContractCheckFailed { get; set; }
        [DataMember]
        public bool IsTotalLineCheckFailed { get; set; }
        [DataMember]
        public bool IsPrincipleLineCheckFailed { get; set; }
        [DataMember]
        public bool IsPortInDonerCheckFailed { get; set; }
        [DataMember]
        public bool IsMNPServiceIdCheckFailed { get; set; }
        [DataMember]
        public BusinessRuleType businessRuleType { get; set; }
        [DataMember]
        public bool IsFxInternalCheckFailed { get; set; }
        [DataMember]
        public string KenanAccountId { get; set; }
        [DataMember]
        public string OutStandingAmount { get; set; }
    }

    #endregion


    [DataContract]
    [Serializable]
    public class TotalLineCheckRequest
    {
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public string IDCardType { get; set; }
        [DataMember]
        public string FilterMISM { get; set; }
        [DataMember]
        public List<string> Lob { get; set; }
        [DataMember]
        public List<string> MktCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class TotalLineCheckResponse : WCFResponse
    {
        public TotalLineCheckResponse()
        {
            msgCode = "1";
            msgDesc = "";
            totalNum = 0;
        }

        [DataMember]
        public string msgCode { get; set; }
        [DataMember]
        public string msgDesc { get; set; }
        [DataMember]
        public int totalNum { get; set; }
    }

    [DataContract]
    [Serializable]
    public class KenanTypeAdditionLineRegistration
    {
        [DataMember]
        public string OrderId { get; set; }
        [DataMember]
        public string FxAcctNo { get; set; }
        [DataMember]
        public KenanTypeExternalId[] ExternalId { get; set; }
        [DataMember]
        public KenanTypeService service { get; set; }
        [DataMember]
        public KenanTypePackage[] package { get; set; }
        [DataMember]
        public KenanTypeInputContract contract { get; set; }
    }
    [DataContract]
    [Serializable]
    public class KenanNewSuppLineRequest
    {
        public KenanNewSuppLineRequest(){

            prinLine = new KenanPrinLine();
            suppLine = new KenanSuppLine();

        }

        [DataMember]
        public string OrderId { get; set; }
        [DataMember]
        public string FxAcctNo { get; set; }
        [DataMember]
        public KenanTypeExternalId[] ExternalId { get; set; }
        [DataMember]
        public KenanTypeService service { get; set; }
        [DataMember]
        public KenanTypePackage[] package { get; set; }
        [DataMember]
        public KenanTypeInputContract contract { get; set; }
        [DataMember]
        public KenanPrinLine prinLine { get; set; }
        [DataMember]
        public KenanSuppLine suppLine { get; set; }
    }
    [DataContract]
    [Serializable]
    public class KenanTypeExternalId
    {
        [DataMember]
        public string ExternalId { get; set; }
        [DataMember]
        public string ExternalIdType { get; set; }
    }
    [DataContract]
    [Serializable]
    public class KenanTypeService
    {
        [DataMember]
        public string accountExternalId { get; set; }
        [DataMember]
        public string hierarchyId { get; set; }
        [DataMember]
        public string currencyCode { get; set; }
        [DataMember]
        public string displayCiemViewId { get; set; }
        [DataMember]
        public string displayExternalIdType { get; set; }
        [DataMember]
        public string emfConfigId { get; set; }
        [DataMember]
        public string exrateClass { get; set; }
        [DataMember]
        public string isPrepaid { get; set; }
        [DataMember]
        public string ixcProviderId { get; set; }
        [DataMember]
        public string lecProviderId { get; set; }
        [DataMember]
        public string noBill { get; set; }
        [DataMember]
        public string nonpubNonlist { get; set; }
        [DataMember]
        public string picDateActive { get; set; }
        [DataMember]
        public string popUnits { get; set; }
        [DataMember]
        public string privacyLevel { get; set; }
        [DataMember]
        public string rateClass { get; set; }
        [DataMember]
        public string restrictedPic { get; set; }
        [DataMember]
        public string salesChannelId { get; set; }
        [DataMember]
        public string serviceActiveDt { get; set; }
        [DataMember]
        public string statusId { get; set; }
        [DataMember]
        public string statusReasonId { get; set; }
        [DataMember]
        public string switchId { get; set; }
        [DataMember]
        public string timezone { get; set; }
        [DataMember]
        public string viewStatus { get; set; }
        [DataMember]
        public KenanTypeAddress[] KenanTypeAdderess { get; set; }
        [DataMember]
        public string inpF01 { get; set; }
        [DataMember]
        public string inpF02 { get; set; }
        [DataMember]
        public string inpF03 { get; set; }
        [DataMember]
        public string inpF04 { get; set; }
        [DataMember]
        public string inpF05 { get; set; }
        [DataMember]
        public string inpF06 { get; set; }
        [DataMember]
        public string inpF07 { get; set; }
        [DataMember]
        public string inpF08 { get; set; }
        [DataMember]
        public string inpF09 { get; set; }
        [DataMember]
        public string inpF10 { get; set; }
        [DataMember]
        public KenanTypeExtendedData[] extendedData { get; set; }
    }
    [DataContract]
    [Serializable]
    public class KenanTypeAddress
    {
        [DataMember]
        public string revRcvCostCtr { get; set; }
        [DataMember]
        public string address1 { get; set; }
        [DataMember]
        public string address2 { get; set; }
        [DataMember]
        public string address3 { get; set; }
        [DataMember]
        public string city { get; set; }
        [DataMember]
        public string company { get; set; }
        [DataMember]
        public string countryCode { get; set; }
        [DataMember]
        public string county { get; set; }
        [DataMember]
        public string fName { get; set; }
        [DataMember]
        public string franchiseTaxCode { get; set; }
        [DataMember]
        public string geocode { get; set; }
        [DataMember]
        public string lName { get; set; }
        [DataMember]
        public string minit { get; set; }
        [DataMember]
        public string phone { get; set; }
        [DataMember]
        public string phone2 { get; set; }
        [DataMember]
        public string state { get; set; }
        [DataMember]
        public string zip { get; set; }
    }
    [DataContract]
    [Serializable]
    public class KenanTypeExtendedData
    {
        [DataMember]
        public string paramId { get; set; }
        [DataMember]
        public string paramName { get; set; }
        [DataMember]
        public string paramValue { get; set; }
    }

    [DataContract]
    [Serializable]
    public class KenanTypePackage
    {
        [DataMember]
        public string connectReason { get; set; }
        [DataMember]
        public string packageId { get; set; }
        [DataMember]
        public KenanTypeComponent[] component { get; set; }
    }
    [DataContract]
    [Serializable]
    public class KenanTypeComponent
    {
        [DataMember]
        public string componentId { get; set; }
        [DataMember]
        public string connectReason { get; set; }
    }
    [DataContract]
    [Serializable]
    public class KenanTypeInputContract
    {
        [DataMember]
        public string requiredContract { get; set; }
        [DataMember]
        public string packageId { get; set; }
        [DataMember]
        public string componentId { get; set; }
        [DataMember]
        public string connectReason { get; set; }
        [DataMember]
        public KenanTypeExtendedData[] extendedData { get; set; }
    }
    [DataContract]
    [Serializable]
    public class KenanPrinLine
    {
        [DataMember]
        public string prinMsisdn { get; set; }
        [DataMember]
        public string prinSubscrNo { get; set; }
        [DataMember]
        public string prinSubscrNoResets { get; set; }
        [DataMember]
        public string prinPackageId { get; set; }
        [DataMember]
        public string prinPackageConnReason { get; set; }
        [DataMember]
        public string prinComponentId { get; set; }
        [DataMember]
        public string prinComponentConnReason { get; set; }
    }

    [DataContract]
    [Serializable]
    public class KenanSuppLine
    {
        [DataMember]
        public string suppMsisdn { get; set; }
        [DataMember]
        public string suppPackageId { get; set; }
        [DataMember]
        public string suppPackageConnReason { get; set; }
        [DataMember]
        public string suppComponentId { get; set; }
        [DataMember]
        public string suppComponentConnReason { get; set; }
        [DataMember]
        public string actionId { get; set; }
    }
    [DataContract]
    [Serializable]
    public class KenanNewSuppLineResponse : WCFResponse
    {

    }
    [DataContract]
    [Serializable]
    public class KenanTypeAdditionLineRegistrationResponse : WCFResponse
    {

    }
    #region retrieveComponentInfo RELATED CLASSES
    [DataContract]
    [Serializable]
    public class NewRetrieveComponentInfoRequest
    {
        [DataMember]
        public string externalId { get; set; }
        [DataMember]
        public string subscrNo { get; set; }
        [DataMember]
        public string subscrNoResets { get; set; }
    }
    [DataContract]
    [Serializable]
    public class NewRetrieveComponentInfoResponse : WCFResponse
    {
        [DataMember]
        public string MsgCode { get; set; }
        [DataMember]
        public string MsgDesc { get; set; }
        [DataMember]
        public List<retrieveComponentInfoContract> Contracts { get; set; }
        [DataMember]
        public List<retrieveComponentInfoVas> Vass { get; set; }
        public NewRetrieveComponentInfoResponse()
        {
            Vass = new List<retrieveComponentInfoVas>();
            Contracts = new List<retrieveComponentInfoContract>();
        }
    }
    [DataContract]
    [Serializable]
    public class retrieveComponentInfoVas
    {
        [DataMember]
        public Int32 ComponentId { get; set; }
        [DataMember]
        public string ComponentDescription { get; set; }
        [DataMember]
        public DateTime ActiveDate { get; set; }
    }
    [DataContract]
    [Serializable]
    public class retrieveComponentInfoContract
    {
        [DataMember]
        public Int32 ContractId { get; set; }
        [DataMember]
        public string ContractName { get; set; }
        [DataMember]
        public Int32 ContractType { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
    }
    #endregion retrieveComponentInfo RELATED CLASSES
    [DataContract]
    [Serializable]
    public class TypeValidateInventoryExtIdRequest
    {
        [DataMember]
        public string externalId { get; set; }
    }
    [DataContract]
    [Serializable]
    public class TypeValidateInventoryExtIdResponse : WCFResponse
    {

    }

    #region "registrationToWebPos"
    [DataContract]
    [Serializable]
    public class SendPaymentDetailsWebPosRequest
    {
        [DataMember]
        public int queueNo { get; set; }
        [DataMember]
        public string regType { get; set; }
        [DataMember]
        public int iSellOrderID { get; set; }
        [DataMember]
        public int storeID { get; set; }
        [DataMember]
        public string customerName { get; set; }
        [DataMember]
        public string iSellTimeStamp { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public string iSellContent { get; set; }
        [DataMember]
        public int orderStatusCode { get; set; }
        [DataMember]
        public string orderStatusDesc { get; set; }
        [DataMember]
        public string SalesAgentID { get; set; }
    }
    [DataContract]
    [Serializable]
    public class SendPaymentDetailsWebPosResponse : WCFResponse
    {
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
    }
    #endregion

    #region "Spend Limit by VLT"

    [DataContract]
    [Serializable]
    public class RetrieveOccSpendLimitResponse : WCFResponse
    {
        [DataMember]
        public string MsgDesc { get; set; }
        [DataMember]
        public string IsOcc { get; set; }
        [DataMember]
        public SpendLimitWS.prinSuppInfoList[] PrintSuppInfo { get; set; }

    }
    [DataContract]
    [Serializable]
    public class RetrieveOccSpendLimitRequest
    {
        [DataMember]
        public string Msisdn { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ProcessOccSpendLimitResponse
    {
        [DataMember]
        public string MsgDesc { get; set; }

        [DataMember]
        public string OriginalMaxLimit { get; set; }


        [DataMember]
        public string OriginalAvailableBalance { get; set; }


        [DataMember]
        public string CurrentMaximumLimit { get; set; }


        [DataMember]
        public string CurrentAvailableBalance { get; set; }


    }

    [DataContract]
    [Serializable]
    public class ProcessOccSpendLimitRequest
    {
        [DataMember]
        public string IsPermanent { get; set; }


        [DataMember]
        public string Msisdn { get; set; }

        [DataMember]
        public string IncrementValue { get; set; }


        [DataMember]
        public string IncrementLimit { get; set; }


        [DataMember]
        public string DesiredStatus { get; set; }

        [DataMember]
        public string FallBackStatus { get; set; }


    }

    #endregion

    #region chkFxVasDependency related
    /// <summary>
    /// <Author>Sutan Dan</Author>
    /// <Date>06102013</Date>
    /// Request properties for chkFxVasDependency
    /// </summary>
    [DataContract]
    [Serializable]
    public class ChkFxVasDependencyRequest
    {
        [DataMember]
        public string emfConfigId { get; set; }
        [DataMember]
        public string mandatoryPackageId { get; set; }
        [DataMember]
        public List<VasComponent> componentList { get; set; }
    }

    /// <summary>
    /// <Author>Sutan Dan</Author>
    /// <Date>06102013</Date>
    /// Request properties for chkFxVasDependency
    /// </summary>
    [DataContract]
    [Serializable]
    public class VasComponent
    {
        [DataMember]
        public string packageId { get; set; }
        public string componentId { get; set; }
    }
    /// <summary>
    /// <Author>Sutan Dan</Author>
    /// <Date>06102013</Date>
    /// Response properties for chkFxVasDependency
    /// </summary>
    [DataContract]
    [Serializable]
    public class ChkFxVasDependencyResponse : WCFResponse
    {
        [DataMember]
        public string msgCode { get; set; }
        [DataMember]
        public string msgDesc { get; set; }
        [DataMember]
        public List<string> mxsRuleMessages { get; set; }

    }
    #endregion chkFxVasDependency related

    #region retrieveDealerInfo related
    /// <summary>
    /// <Author>Pavan Kumar</Author>
    /// <Date>20150501</Date>
    /// Request properties for retrieveDealerInfo
    /// </summary>
    [DataContract]
    [Serializable]
    public class retrieveDealerInfoRequest
    {
        [DataMember]
        public string dealerCode { get; set; }
    }
    /// <summary>
    /// <Author>Pavan Kumar</Author>
    /// <Date>20150501</Date>
    /// Response properties for retrieveDealerInfo
    /// </summary>
    [DataContract]
    [Serializable]
    public class retrieveDealerInfoResponse : WCFResponse
    {
        [DataMember]
        public string salesChannelId { get; set; }
        [DataMember]
        public string salesChannelName { get; set; }
    }
    #endregion retrieveDealerInfo related

    #region addCenterVas related
    [DataContract]
    [Serializable]
    public class AddCenterVasRequest
    {
        [DataMember]
        public string Msisdn { get; set; }
        [DataMember]
        public string PostPreInd { get; set; }
        [DataMember]
        public string ComponentId { get; set; }
        [DataMember]
        public string GenerateWorkFlow { get; set; }
        [DataMember]
        public string SendSms { get; set; }
        [DataMember]
        public string CreateInteraction { get; set; }
        [DataMember]
        public string createNewPkgInst { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddCenterVasResponse : WCFResponse
    {
        [DataMember]
        public string MsgCode { get; set; }
        [DataMember]
        public string MsgDescription { get; set; }
        [DataMember]
        public string OrderID { get; set; }
        [DataMember]
        public string OrderIdResets { get; set; }
        [DataMember]
        public string TrackingId { get; set; }
        [DataMember]
        public string TrackingIdResets { get; set; }
    }
    #endregion addCenterVas related

    #region addCenterVas related
    [DataContract]
    [Serializable]
    public class RemoveCenterVasRequest
    {
        [DataMember]
        public string Msisdn { get; set; }
        [DataMember]
        public string PostPreInd { get; set; }
        [DataMember]
        public string ComponentId { get; set; }
        [DataMember]
        public string GenerateWorkFlow { get; set; }
        [DataMember]
        public string SendSms { get; set; }
        [DataMember]
        public string CreateInteraction { get; set; }
        [DataMember]
        public string TrackingId { get; set; }
        [DataMember]
        public string TrackingIdResets { get; set; }
        [DataMember]
        public string CreateNewPkgInst { get; set; }
    }
    #endregion addCenterVas related

    [DataContract]
    [Serializable]
    public class AddRemoveVASReq
    {
        [DataMember]
        public List<AddRemoveVASComponents> Components { get; set; }

    }


    [DataContract]
    [Serializable]
    public class RetrieveSimDetlsRequest : WCFResponse
    {

        [DataMember]
        public string Msisdn { get; set; }

    }


    [DataContract]
    [Serializable]
    public class RetrieveSimDetlsResponse : WCFResponse
    {
        [DataMember]
        public string MsgCode { get; set; }

        [DataMember]
        public string MsgDesc { get; set; }

        [DataMember]
        public SimReplacementServiceWs.retrieveSimDetlsList[] SimDetlsList { get; set; }

        //[DataMember]
        //public List<RetrieveSimDetlsList> SimDetlsList { get; set; }

    }

    [DataContract]
    [Serializable]
    public class RetrieveSimDetlsList : WCFResponse
    {
        public RetrieveSimDetlsList(string ExteranalId, string ExteranalIdType, string InventoryTypeId, string InventoryDisplayValue, string Reason, string ActiveDt, string InActiveDt)
        {

            this.ExteranalId = ExteranalId;
            this.ExteranalIdType = ExteranalIdType;
            this.InventoryTypeId = InventoryTypeId;
            this.InventoryDisplayValue = InventoryDisplayValue;
            this.Reason = Reason;
            this.ActiveDt = ActiveDt;
            this.InActiveDt = InActiveDt;
        }

        [DataMember]
        public string ExteranalId { get; set; }

        [DataMember]
        public string ExteranalIdType { get; set; }

        [DataMember]
        public string InventoryTypeId { get; set; }

        [DataMember]
        public string InventoryDisplayValue { get; set; }

        [DataMember]
        public string Reason { get; set; }

        [DataMember]
        public string ActiveDt { get; set; }

        [DataMember]
        public string InActiveDt { get; set; }

    }

    // Added for CRP by Kotesh

    [DataContract]
    [Serializable]
    public class CenterOrderContractResponse : WCFResponse
    {

    }
    [DataContract]
    [Serializable]
    public class CenterOrderContractRequest
    {
        [DataMember]
        public MaxisOrderProcessWs.contractListConnect[] contractListConnect { set; get; }
        [DataMember]
        public string fxAcctNo { set; get; }
        [DataMember]
        public string fxSubscrNo { set; get; }
        [DataMember]
        public string fxSubscrNoResets { set; get; }
        [DataMember]
        public MaxisOrderProcessWs.typeNrcInfo[] nrcInfoList { set; get; }
        [DataMember]
        public string orderId { set; get; }
        [DataMember]
        public MaxisOrderProcessWs.packageListConnect[] packageListConnect { set; get; }
        [DataMember]
        public MaxisOrderProcessWs.packageListDisconnect[] packageListDisconnect { set; get; }
        [DataMember]
        public MaxisOrderProcessWs.typeExternalId[] externalIds { set; get; }

        [DataMember]
        public string UserName { get; set; }

    }
    //EO CRP 

    #region getMNPRequest
    /// <summary>
    /// getMNPRequest request class details
    /// </summary>
    [DataContract]
    [Serializable]
    public class getMNPRequest
    {
        [DataMember]
        public string reqType { get; set; }
        [DataMember]
        public string reqValue { get; set; }
        [DataMember]
        public string portReqType { get; set; }
    }

    /// <summary>
    /// getMNPRequestResponse response class details
    /// </summary>
    [DataContract]
    [Serializable]
    public class getMNPRequestResponse : WCFResponse
    {
        [DataMember]
        public string TotalRec { get; set; }
        [DataMember]
        public List<MnpRequestDetails> RequestDetails { get; set; }
    }

    /// <summary>
    /// MnpRequestDetails class details
    /// </summary>
    [DataContract]
    [Serializable]
    public class MnpRequestDetails
    {
        [DataMember]
        public string PortInitiatedDt { get; set; }
        [DataMember]
        public string PortRequestID { get; set; }
        [DataMember]
        public string Msisdn { get; set; }
        [DataMember]
        public string MsisdnStatus { get; set; }
        [DataMember]
        public string AccountNo { get; set; }
        [DataMember]
        public string DispPortReqId { get; set; }
        [DataMember]
        public string UserId { get; set; }
        [DataMember]
        public string Location { get; set; }
        [DataMember]
        public string ErrorCode { get; set; }
        [DataMember]
        public string ErrorReason { get; set; }
    }
    #endregion getMNPRequest


    #region "Preportin Request"

    [DataContract]
    [Serializable]
    public class PreportInRequest
    {
        private string isConsumerField;
        private string[] msisdnField;
        private string donorIdField;
        private string customerNameField;
        private string companyNameField;
        private string accountNumberField;
        private string[] identityField;
        private string[] identityTypeField;

        [DataMember]
        public string isConsumer
        {
            get
            {
                return this.isConsumerField;
            }
            set
            {
                this.isConsumerField = value;
            }
        }
        [DataMember]
        public string[] msisdn
        {
            get
            {
                return this.msisdnField;
            }
            set
            {
                this.msisdnField = value;
            }
        }
        [DataMember]
        public string donorId
        {
            get
            {
                return this.donorIdField;
            }
            set
            {
                this.donorIdField = value;
            }
        }
        [DataMember]
        public string customerName
        {
            get
            {
                return this.customerNameField;
            }
            set
            {
                this.customerNameField = value;
            }
        }
        [DataMember]
        public string companyName
        {
            get
            {
                return this.companyNameField;
            }
            set
            {
                this.companyNameField = value;
            }
        }
        [DataMember]
        public string accountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }
        [DataMember]
        public string[] identity
        {
            get
            {
                return this.identityField;
            }
            set
            {
                this.identityField = value;
            }
        }
        [DataMember]
        public string[] identityType
        {
            get
            {
                return this.identityTypeField;
            }
            set
            {
                this.identityTypeField = value;
            }
        }
    }


    [DataContract]
    [Serializable]
    public class PreportInResponse
    {
        private string msgCodeField;

        private string msgDescField;

        private string reqIdField;

        [DataMember]
        public string msgCode
        {
            get
            {
                return this.msgCodeField;
            }
            set
            {
                this.msgCodeField = value;
            }
        }

        [DataMember]
        public string msgDesc
        {
            get
            {
                return this.msgDescField;
            }
            set
            {
                this.msgDescField = value;
            }
        }

        [DataMember]
        public string reqId
        {
            get
            {
                return this.reqIdField;
            }
            set
            {
                this.reqIdField = value;
            }
        }
    }


    #endregion "Preportin Request"

    #region "Disconnect Request"

    [DataContract]
    [Serializable]
    public class DisconnectCenterServiceRequest
    {
        private string orderIdField;
        private string newOrderIndField;
        private string fxAcctNoField;
        private string fxSubscrNoField;
        private string fxSubscrNoResetsField;
        private string disconnectReasonField;
        private string waiveInstallmentNrcField;
        private string waiveUnbilledNrcField;
        private string waiveTerminationObligationField;
        private string waiveUnmetObligationField;
        private string waiveRefinanceNrcField;
        private string inpF01Field;
        private string inpF02Field;
        private string inpF03Field;
        private string inpF04Field;
        private string inpF05Field;
        private string inpF06Field;
        private string inpF07Field;
        private string inpF08Field;
        private string inpF09Field;
        private string inpF10Field;

        /// <remarks/>
        [DataMember]
        public string orderId
        {
            get
            {
                return this.orderIdField;
            }
            set
            {
                this.orderIdField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string newOrderInd
        {
            get
            {
                return this.newOrderIndField;
            }
            set
            {
                this.newOrderIndField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string fxAcctNo
        {
            get
            {
                return this.fxAcctNoField;
            }
            set
            {
                this.fxAcctNoField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string fxSubscrNo
        {
            get
            {
                return this.fxSubscrNoField;
            }
            set
            {
                this.fxSubscrNoField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string fxSubscrNoResets
        {
            get
            {
                return this.fxSubscrNoResetsField;
            }
            set
            {
                this.fxSubscrNoResetsField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string disconnectReason
        {
            get
            {
                return this.disconnectReasonField;
            }
            set
            {
                this.disconnectReasonField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string waiveInstallmentNrc
        {
            get
            {
                return this.waiveInstallmentNrcField;
            }
            set
            {
                this.waiveInstallmentNrcField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string waiveUnbilledNrc
        {
            get
            {
                return this.waiveUnbilledNrcField;
            }
            set
            {
                this.waiveUnbilledNrcField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string waiveTerminationObligation
        {
            get
            {
                return this.waiveTerminationObligationField;
            }
            set
            {
                this.waiveTerminationObligationField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string waiveUnmetObligation
        {
            get
            {
                return this.waiveUnmetObligationField;
            }
            set
            {
                this.waiveUnmetObligationField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string waiveRefinanceNrc
        {
            get
            {
                return this.waiveRefinanceNrcField;
            }
            set
            {
                this.waiveRefinanceNrcField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string inpF01
        {
            get
            {
                return this.inpF01Field;
            }
            set
            {
                this.inpF01Field = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string inpF02
        {
            get
            {
                return this.inpF02Field;
            }
            set
            {
                this.inpF02Field = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string inpF03
        {
            get
            {
                return this.inpF03Field;
            }
            set
            {
                this.inpF03Field = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string inpF04
        {
            get
            {
                return this.inpF04Field;
            }
            set
            {
                this.inpF04Field = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string inpF05
        {
            get
            {
                return this.inpF05Field;
            }
            set
            {
                this.inpF05Field = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string inpF06
        {
            get
            {
                return this.inpF06Field;
            }
            set
            {
                this.inpF06Field = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string inpF07
        {
            get
            {
                return this.inpF07Field;
            }
            set
            {
                this.inpF07Field = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string inpF08
        {
            get
            {
                return this.inpF08Field;
            }
            set
            {
                this.inpF08Field = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string inpF09
        {
            get
            {
                return this.inpF09Field;
            }
            set
            {
                this.inpF09Field = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string inpF10
        {
            get
            {
                return this.inpF10Field;
            }
            set
            {
                this.inpF10Field = value;
            }
        }
    }

    [DataContract]
    [Serializable]
    public class DisconnectCenterServiceResponse
    {
        private string msgCodeField;
        private string msgDescField;
        /// <remarks/>
        [DataMember]
        public string msgCode
        {
            get
            {
                return this.msgCodeField;
            }
            set
            {
                this.msgCodeField = value;
            }
        }

        /// <remarks/>
        [DataMember]
        public string msgDesc
        {
            get
            {
                return this.msgDescField;
            }
            set
            {
                this.msgDescField = value;
            }
        }
    }

    #endregion
}
