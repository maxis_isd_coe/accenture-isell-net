﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace Online.Registration.IntegrationService.Models
{

	#region updateResponsePEGA
	[DataContract]
	[Serializable]
	public class UpdateResponseRequest
	{
		[DataMember]
		public string DecisionResultId { get; set; } // from executeAssessment
		[DataMember]
		public string Category { get; set; } // split with the first "-" get the 1st index
		[DataMember]
		public string DisplayCategory { get; set; } // split with the first "-"
		[DataMember]
		public string TopRank { get; set; } // index on pega response
		[DataMember]
		public string Reason { get; set; } // split with the first "-" get the 2nd index
		[DataMember]
		public string CaseID { get; set; } //subsciberNo + "_0"
		[DataMember]
		public string DecisionID { get; set; } // from executeAssessment
		[DataMember]
		public string ProjectName { get; set; } // from executeAssessment
		[DataMember]
		public string PropositionIdentifier { get; set; } // from executeAssessment
		[DataMember]
		public string Response { get; set; } // accepted, declined, later
		[DataMember]
		public string Application { get; set; }
		[DataMember]
		public string Channel { get; set; } // from executeAssessment
		[DataMember]
		public string Customer { get; set; } // from executeAssessment
		[DataMember]
		public string ExternalID { get; set; }
		[DataMember]
		public string KenanCode { get; set; }
		[DataMember]
		public string CaseCategory { get; set; }
		[DataMember]
		public string ProductName { get; set; }

	}

	[DataContract]
	[Serializable]
	public class UpdateResponseResponse : WCFResponse
	{
		[DataMember]
		public string Return { get; set; }
		[DataMember]
		public string FaultCode { get; set; }
		[DataMember]
		public string FaultString { get; set; }
	}
	#endregion

	#region ExecuteAssessment
	[DataContract]
	[Serializable]
	public class ExecuteAssessmentRequest
	{
		public ExecuteAssessmentRequest()
		{
			MxChannel = "iSell";
		}
		[DataMember]
		public string Case_id { get; set; }
		[DataMember]
		public string CallReason { get; set; }
		[DataMember]
		public string MxChannel { get; set; } // hardcoded to iSELL
		[DataMember]
		public string ExternalID { get; set; }
		[DataMember]
		public string SalesPerson { get; set; }
	}

	[DataContract]
	[Serializable]
	public class ExecuteAssessmentResponse : WCFResponse
	{
		public ExecuteAssessmentResponse()
		{
			productList = new List<PegaProducts>();
		}
		[DataMember]
		public string DecisionResultId { get; set; }
		[DataMember]
		public string DecisionID { get; set; }
		[DataMember]
		public string ProjectName { get; set; }
		[DataMember]
		public string Channel { get; set; }
		[DataMember]
		public string Customer { get; set; }
		[DataMember]
		public List<PegaProducts> productList { get; set; }
		[DataMember]
		public string caseID { get; set; }
	}

	[DataContract]
	[Serializable]
	public class PegaProducts
	{
		[DataMember]
		public string Category { get; set; } // for now is top / all
		[DataMember]
		public string PegaIndex { get; set; }
		[DataMember]
		public string KenanCode { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public string PrepositionID { get; set; }
		[DataMember]
		public bool Relevant { get; set; }
	}


	#endregion

	#region Cutomer One View
	[DataContract]
    [Serializable]
    public class ExecuteDefaultProfileRequest
    {
        [DataMember]
        public string subscrNo { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExecuteDefaultProfileResponse : WCFResponse
    {
        [DataMember]
        public string ResponseCode { get; set; }
        [DataMember]
        public string ResponseMessage { get; set; }
        [DataMember]
        public bool CustomerFound { get; set; }
        [DataMember]
        public string PrincipalMSISDN { get; set; }
        [DataMember]
        public string DeviceBrand { get; set; }
        [DataMember]
        public string DeviceModel { get; set; }
        [DataMember]
        public string CurrentPlan { get; set; }
        [DataMember]
        public string CustomerTenure { get; set; }
        [DataMember]
        public string LineActiveDuration { get; set; }
        [DataMember]
        public string DurationToEnd { get; set; }
        [DataMember]
        public string ContractEndDate { get; set; }
        [DataMember]
        public string AverageBillAmountLine { get; set; }
        [DataMember]
        public string AverageBillAmountAcct { get; set; }
        [DataMember]
        public double AverageCall { get; set; }
        [DataMember]
        public double AverageData { get; set; }
        [DataMember]
        public double AverageSMS { get; set; }
        [DataMember]
        public double TrendCall { get; set; }
        [DataMember]
        public double TrendData { get; set; }
        [DataMember]
        public double TrendSMS { get; set; }
        [DataMember]
        public string ArrowIndicator { get; set; }

    }
    #endregion
}