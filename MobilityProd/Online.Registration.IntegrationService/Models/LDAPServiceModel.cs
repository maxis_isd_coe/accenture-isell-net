﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Online.Registration.IntegrationService.Models
{
    [DataContract]
    [Serializable]
    public class LDAPAccessRequest
    {
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
    }

    [DataContract]
    [Serializable]
    public class LDAPAccessResponse : WCFResponse
    {
        public LDAPAccessResponse()
        {
            IsCredentialValidated = true;
        }

        [DataMember]
        public bool IsCredentialValidated { get; set; }
    }
}