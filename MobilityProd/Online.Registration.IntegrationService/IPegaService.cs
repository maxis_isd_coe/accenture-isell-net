﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.IntegrationService.Models;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPegaService" in both code and config file together.
    [ServiceContract]
    public interface IPegaService
    {
        [OperationContract]
        ExecuteDefaultProfileResponse ExecuteDefaultProfile(ExecuteDefaultProfileRequest request);

		[OperationContract]
		ExecuteAssessmentResponse ExecuteAssessment(ExecuteAssessmentRequest request);

		[OperationContract]
		UpdateResponseResponse UpdateResponse(UpdateResponseRequest request);

    }
}
