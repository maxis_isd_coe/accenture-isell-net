﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Online.Registration.IntegrationService.IContractUploadWS;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IiContract" in both code and config file together.
    [ServiceContract]
    public interface IiContract
    {
        [OperationContract]
        string UploadFiletoIcontract(string Doctype, string regId, string IDcardValue, string MSISDN, string AccNo, string trnType,string idTypeDec, string StoreID, Byte[] file, string agentCode, string fileName);
    }
}
