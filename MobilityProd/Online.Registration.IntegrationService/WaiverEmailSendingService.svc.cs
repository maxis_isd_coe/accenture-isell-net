﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WaiverEmailSendingService" in code, svc and config file together.
    public class WaiverEmailSendingService : IWaiverEmailSendingService
    {
        public string  WaiverEmailSend(string to, string from, string subject, Dictionary<string, string> keyValuePairs)
        {
            string emailSentStatus =MailHelper.SendEmail(to, from,  subject, keyValuePairs);
            return emailSentStatus.ToString ();
        }

        
    }
}
