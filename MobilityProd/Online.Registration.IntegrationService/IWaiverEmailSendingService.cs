﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWaiverEmailSendingService" in both code and config file together.
    [ServiceContract]
    public interface IWaiverEmailSendingService
    {
        [OperationContract]
        string WaiverEmailSend(string to, string from, string subject, Dictionary<string, string> keyValuePairs);
   
    }
}
