﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MaxisVASPortal.DynamicProxy;
using Online.Registration.DAL.Models;
using System.Net;
using System.Xml.Linq;
using SNT.Utility;
using Online.Registration.IntegrationService.Helper;
using Online.Registration.IntegrationService.Models;
using System.Configuration;
using System.Text.RegularExpressions;

using log4net;
using Online.Registration.IntegrationService.MaxisStoreserviceWS;

namespace Online.Registration.IntegrationService
{
    public class DynamicService : IDynamicService
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DynamicService));

        public int GetStockCount(string articleId, string wsdlUrl, int storeId)
        {
            Logger.InfoFormat("Entering (ArticleId:{0},WSDL URL:{1})", articleId, wsdlUrl);
            int count = 0;
            Logger.InfoFormat("Entering (ArticleId:{0},WSDL URL:{1},###################CountDefault{2}:)", articleId, wsdlUrl, count);
            try
            {

                DynamicProxyFactory factory = new DynamicProxyFactory(wsdlUrl);
                object objStockCounts = null;
                //Logger.Info("DynamicProxyFactory For WISDL###################"+ XMLHelper.ConvertObjectToXml(factory,typeof(DynamicProxyFactory)));
                Logger.Info("DynamicProxyFactory.Contracts.Count###################" + factory.Contracts.Count());

                if (factory.Contracts.Count() == 1)
                {
                    Logger.Info("If DynamicProxyFactory.Contracts.Count is 1 then call the Service method GetStockCount()");

                    DynamicProxy dynamicproxy = factory.CreateProxy(factory.Contracts.First().Name);
                    objStockCounts = (dynamicproxy.CallMethod("GetMultipleStockCount", storeId, string.Join("|", articleId).TrimEnd('|').TrimStart('|')));
                    if (objStockCounts != null)
                    {
                        foreach (var stock in objStockCounts.ToString().Split("|".ToCharArray()))
                        {
                            if (!string.IsNullOrEmpty(stock))
                            {
                                count = Convert.ToInt32(stock);
                            }

                        }
                    }

                    dynamicproxy.Close();
                }
                else
                {
                    Logger.InfoFormat(" DynamicProxyFactory.Contracts.Count is no equalts then call the Service method GetStockCount()");
                    //TODO: if more than one, we need to loop through all contracts and findout our method. And need to use corresponding Contract otherwise throw an error
                }
            }
            catch (Exception ex)
            {
                Logger.InfoFormat("Entering (Exception:{0}, ArticleId:{1},WSDL URL:{2})", ex.ToString(), articleId, wsdlUrl);
            }
            Logger.InfoFormat("Entering (ArticleId:{0},WSDL URL:{1},###################CountFromService{2}:)", articleId, wsdlUrl, count);
            return count;
        }


        public Dictionary<string, int> GetStockCounts(List<string> articleIds, string wsdlUrl, int storeId)
        {

            Logger.InfoFormat("Entering GetStockCounts (ArticleIds:{0},WSDL URL:{1})", string.Join(",", articleIds.ToArray()), wsdlUrl);
            Dictionary<string, int> lstcounts = new Dictionary<string, int>();

            try
            {

                DynamicProxyFactory factory = new DynamicProxyFactory(wsdlUrl);
                object objStockCounts = null;
                int count = 0;

                if (factory.Contracts.Count() == 1)
                {
                    DynamicProxy dynamicproxy = factory.CreateProxy(factory.Contracts.First().Name);

                    List<string> articles = new List<string>();
                    while (articleIds.Any())
                    {
                        articles = articleIds.Take(20).ToList();
                        string[] strArticles = articles.ToArray();

                        objStockCounts = (dynamicproxy.CallMethod("GetMultipleStockCount", storeId, string.Join("|", strArticles).TrimEnd('|').TrimStart('|')));
                        int index = 0;
                        if (objStockCounts != null)
                        {
                            foreach (var stock in objStockCounts.ToString().Split("|".ToCharArray()))
                            {
                                lstcounts.Add(articles[index], Convert.ToInt32(stock));
                                index++;
                            }
                        }
                        articleIds = articleIds.Skip(20).ToList();
                    }
                    dynamicproxy.Close();
                }
                else
                {
                    Logger.InfoFormat("GetStockCounts : DynamicProxyFactory.Contracts.Count is no equalts then call the Service method GetStockCount()");
                }
                Logger.InfoFormat("GetStockCounts: Entering (ArticleIds:{0},WSDL URL:{1},###################CountFromService{2}:)", string.Join(",", articleIds.ToArray()), wsdlUrl, String.Join(" , ", lstcounts).ToArray());
            }
            catch (Exception ex)
            {
                Logger.InfoFormat("GetStockCounts: Entering (Exception:{0}, ArticleIds:{1},WSDL URL:{2})", ex.ToString(), string.Join(",", articleIds.ToArray()), wsdlUrl);
            }
            return lstcounts;
        }

        //Will Retire
        /*public bool SendPaymentDetails(int iSellOrderID)
        {
            bool result = true;
            String input = String.Empty;
            String statusDesc = "Ready For Payment";
            var reg = Online.Registration.DAL.Registration.RegistrationGet(new List<int> { iSellOrderID }).SingleOrDefault();
            var cust = Online.Registration.DAL.Registration.CustomerGet(new List<int> { reg.CustomerID }).SingleOrDefault();
            var Org = Online.Registration.DAL.Admin.OnlineRegAdmin.OrganizationGet(new List<int> { reg.CenterOrgID.ToInt() }).FirstOrDefault();
            String wsdlUrl = Org.WSDLUrl;
            String XMLResponse = string.Empty;
            var txstreamer = new System.IO.StringWriter();
            statusToPos objSendPaymentStatus = new statusToPos();
            Logger.InfoFormat("Entering SendPaymentDetails (iSellOrderID:{0}, wsdlURL:{1})", iSellOrderID, wsdlUrl);
            //Dictionary<string, int> lstcounts = new Dictionary<string, int>();
            
            //var stsID = Online.Registration.DAL.Registration.RegStatusFind(new RegStatusFind()
            //{
            //    RegStatus = new RegStatus()
            //    {
            //        RegID = iSellOrderID
            //    },
            //    Active = true
            //}).FirstOrDefault();
            //var sts = Online.Registration.DAL.Registration.RegStatusGet(new List<int>() { stsID });
            //var stsdesc = Online.Registration.DAL.Admin.OnlineRegAdmin.StatusGet(new List<int>() { sts.FirstOrDefault().StatusID });
            var timestamp = System.DateTime.Now.ToString("yyyyMMddHHmmss");
            KenanService _client = new KenanService();
            //may need condition filter
            try
            {
                if (reg != null && reg.RegTypeID == 14)
                {
                    input = _client.GetIMPOSDataForWEBPOS(reg.ID, "SIMReplacement");
                }
                else
                {
                    input = _client.GetIMPOSDataForWEBPOS(reg.ID, "P");
                }
            }
            catch (Exception ex)
            {

                input = "NonBilling|20046799|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000|893456789087653608|EA|||NonBilling|70000005|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||EA|||NonBilling|70000067|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||NonBilling|70000100|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||NonBilling|70000065|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||";
            }

            String regdesc = Online.Registration.DAL.Admin.OnlineRegAdmin.RegTypeGet(new List<int> { reg.RegTypeID }).FirstOrDefault().Description;

            try
            {

                DynamicProxyFactory factory = new DynamicProxyFactory(wsdlUrl);
                //int count = 0;

                if (factory.Contracts.Count() == 1)
                {
                    DynamicProxy dynamicproxy = factory.CreateProxy(factory.Contracts.First().Name);

                    var webPosLog = new WebPosLog()
                    {
                        exception = "",
                        fileName = "DynamicService",
                        method = wsdlUrl,
                        request = String.IsNullOrEmpty(input.Trim()) ? "isellContent Empty" : input,
                        response = "",
                        userId = 0,
                        regId = iSellOrderID.ToString2()
                    };

                    if (!ReferenceEquals(iSellOrderID, null) && !String.IsNullOrEmpty(input))
                    {
                        object tempToPos = (dynamicproxy.CallMethod("UpdateISellTransactionToPos", reg.QueueNo.ToInt(), regdesc, iSellOrderID.ToLong(), reg.OrganisationId.ToInt(), cust.FullName, timestamp, reg.MSISDN1, input, 0, statusDesc, reg.SalesPerson));
                        System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(tempToPos.GetType());
                        xmlSerializer.Serialize(txstreamer, tempToPos);
                        XMLResponse = txstreamer.ToString2();
                        XMLResponse = Util.RemoveAllNamespaces(XMLResponse);
                        System.Xml.Linq.XDocument xdoc = System.Xml.Linq.XDocument.Parse(XMLResponse);
                        foreach (var objmap in xdoc.Descendants("ISellResponse"))
                        {
                            objSendPaymentStatus.msgCode = objmap.Element("msgCode") == null ? 0 : objmap.Element("msgCode").Value.ToInt();
                            objSendPaymentStatus.msgDesc = objmap.Element("msgDesc") == null ? string.Empty : objmap.Element("msgDesc").Value.ToString2();
                            webPosLog.response = "Status = 0 ,MsgCode = " + objSendPaymentStatus.msgCode.ToString2() + " ,MsgDesc = " + objSendPaymentStatus.msgDesc;
                        }
                        //objSendPaymentStatus = (statusToPos)tempToPos;
                    }
                    DAL.Registration.InsertLog(webPosLog);
                    dynamicproxy.Close();
                }
                else
                {
                    Logger.InfoFormat("SendPaymentDetails : DynamicProxyFactory.Contracts.Count is no equalts then call the Service method GetStockCount()");
                }
                Logger.InfoFormat("SendPaymentDetails: Entering (iSellOrderID:{0},WSDL URL:{1},###################code:{2}###desc:{3})", string.Join(",", iSellOrderID), wsdlUrl, objSendPaymentStatus.msgCode.ToString2(), objSendPaymentStatus.msgDesc);
            }
            catch (Exception ex)
            {
                Logger.InfoFormat("SendPaymentDetails: Entering (Exception:{0}, iSellOrderID:{1},WSDL URL:{2})", ex.ToString(), iSellOrderID, wsdlUrl);
            }
            if (!ReferenceEquals(objSendPaymentStatus, null))
            {
                result = ((!ReferenceEquals(objSendPaymentStatus.msgCode, null) && objSendPaymentStatus.msgCode == 1) || String.IsNullOrEmpty(input)) ? true : false;
            }
            return result;
        }*/

        public bool SendDetailsToWebPos(int iSellOrderID, int statusCode = 0)
        {
            bool result = true;
            String input = String.Empty;
            String statusDesc = "Ready For Payment";
            var reg = Online.Registration.DAL.Registration.RegistrationGet(new List<int> { iSellOrderID }).SingleOrDefault();
            var cust = new Customer();
            var Org = Online.Registration.DAL.Admin.OnlineRegAdmin.OrganizationGet(new List<int> { reg.CenterOrgID.ToInt() }).FirstOrDefault();
            var RegAttribute = Online.Registration.DAL.Registration.RegAttributesGetByRegID(iSellOrderID);
            var RegAccessory = Online.Registration.DAL.Registration.GetRegAccessoryByRegID(iSellOrderID);
            String regdesc = String.Empty;
            String wsdlUrl = Org.WSDLUrl;
            String XMLResponse = string.Empty;
            var txstreamer = new System.IO.StringWriter();
            statusToPos objSendPaymentStatus = new statusToPos();
            Logger.InfoFormat("Entering SendPaymentDetails (iSellOrderID:{0}, wsdlURL:{1})", iSellOrderID, wsdlUrl);

            var timestamp = System.DateTime.Now.ToString("yyyyMMddHHmmss");
            KenanService _client = new KenanService();
            //may need condition filter
            if (statusCode != 1)
            {
                cust = Online.Registration.DAL.Registration.CustomerGet(new List<int> { reg.CustomerID }).SingleOrDefault();
                try
                {
                    if (reg != null && reg.RegTypeID == 14)
                    {
                        input = _client.GetIMPOSDataForWEBPOS(reg.ID, "SIMReplacement");
                    }
                    else
                    {
                        input = _client.GetIMPOSDataForWEBPOS(reg.ID, "P");
                    }
                }
                catch (Exception ex)
                {

                    input = "NonBilling|20046799|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000|893456789087653608|EA|||NonBilling|70000005|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||EA|||NonBilling|70000067|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||NonBilling|70000100|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||NonBilling|70000065|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||";
                }

                RegistrationSearchResult dashboard = new RegistrationSearchResult();
                RegistrationSearchCriteria Criteria = new RegistrationSearchCriteria()
                        {
                            RegID = iSellOrderID.ToString()
                        };

                dashboard = DAL.Registration.OrderSearchPOS(Criteria).FirstOrDefault();
                regdesc = Online.Registration.IntegrationService.Helper.Util.getTransactionTypeDisplay(dashboard);

                //regdesc = Online.Registration.DAL.Admin.OnlineRegAdmin.RegTypeGet(new List<int> { reg.RegTypeID }).FirstOrDefault().Description;
                //if (reg.CRPType == "DSP")
                //{
                //    regdesc = "Device Supplementary Plan";
                //}
                //regdesc = regdesc.Length <= 20 ? regdesc : regdesc.Substring(0, 17) + "...";
            }




			try
			{
				using (var pos = new MaxisStoreserviceWS.ServicesSoapClient())
				{
					// endpoint will follow the store
					pos.Endpoint.Address = new System.ServiceModel.EndpointAddress(wsdlUrl);
					var webPosLog = new WebPosLog()
					{
						exception = "",
						fileName = "DynamicService",
						method = wsdlUrl,
						request = String.IsNullOrEmpty(input.Trim()) ? "isellContent Empty" : input,
						response = "",
						userId = 0,
						regId = iSellOrderID.ToString2()
					};

					if (statusCode == 1)
					{
						statusDesc = "Cancel";
					}

					if ((!ReferenceEquals(iSellOrderID, null) && !String.IsNullOrEmpty(input)) || statusCode == 1)
					{
						string custEmail = "";

						//empty string will be send if email lenght > 100 and contains special characters except + - _ . @ 
						if (cust.EmailAddr != null && cust.EmailAddr.Length < 101)
						{
							bool validEmail = Regex.IsMatch((cust.EmailAddr), @"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$");
							if (validEmail)
							{
								custEmail = cust.EmailAddr;
							}
						}

                        var biometricFail = "1";
                        if(reg.BiometricVerify == true){
                            biometricFail = "0"; 
                        }
                        var DFinancing = (RegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_DF_ORDER).ToList().Count > 0 ? "1" : "0");
                        var AFinancing = RegAccessory.Where(x => x.IsFinancing == true).Any() ? "1" : "0";
                        var containsZerolution = (DFinancing == "1" ? "1" : AFinancing == "1" ? "1" : "0"); 

						ISellResponse posResponse = new ISellResponse();

						if (statusCode != 1)
						{
                            posResponse = pos.UpdateISellTransactionToPos(reg.QueueNo.ToString2(), regdesc, iSellOrderID.ToLong(), reg.OrganisationId.ToInt(), cust.FullName, timestamp, reg.MSISDN1, input, statusCode, statusDesc, reg.SalesPerson.ToUpper(), custEmail, biometricFail, containsZerolution);
						}
						else
						{
                            posResponse = pos.UpdateISellTransactionToPos(reg.QueueNo, "", iSellOrderID.ToLong(), reg.OrganisationId.ToInt(), "", timestamp, "", "", statusCode, statusDesc, "", "", biometricFail, containsZerolution);
						}

						if (posResponse != null)
						{
							objSendPaymentStatus.msgCode = posResponse.msgCode;
							objSendPaymentStatus.msgDesc = posResponse.msgDesc;

							webPosLog.response = "Status = " + statusCode.ToString2() + " ,MsgCode = " + objSendPaymentStatus.msgCode.ToString2() + " ,MsgDesc = " + objSendPaymentStatus.msgDesc;

							#region Update the Order Status to Fail if the response code is 2 (POS Internal Error)
							if (objSendPaymentStatus.msgCode.ToString2() == "2")
							{
								var posErrorIndicator = System.Configuration.ConfigurationManager.AppSettings["POSErrorIndicator"].ToString2();//"String or binary data would be truncated". this is happenned if the character amount in isellContent is more than POS can handle
								var posErrorNotification = System.Configuration.ConfigurationManager.AppSettings["POSErrorNotification"].ToString2();

								if (objSendPaymentStatus.msgDesc.Contains(posErrorIndicator))
								{
									var regStatus = new RegStatus();
									var kenanLogDetails = new KenanaLogDetails();

									regStatus.RegID = iSellOrderID;
									regStatus.Active = true;
									regStatus.CreateDT = DateTime.Now;
									regStatus.StartDate = DateTime.Now;
									regStatus.LastAccessID = "POS";
									regStatus.StatusID = DAL.Admin.OnlineRegAdmin.StatusFind(new StatusFind()
									{
										Status = new Status()
										{
											Code = "REG_Fail"
										}
									}).ToList()[0];

									DAL.Registration.RegistrationCancel(regStatus);

									kenanLogDetails.RegID = iSellOrderID;
									kenanLogDetails.MessageCode = objSendPaymentStatus.msgCode.ToString2();
									kenanLogDetails.MessageDesc = posErrorNotification;
									kenanLogDetails.KenanXmlReq = webPosLog.request;
									kenanLogDetails.KenanXmlRes = webPosLog.response;
									kenanLogDetails.MethodName = "WebPosAuto";

									DAL.Admin.OnlineRegAdmin.SaveKenanXmlLogs(kenanLogDetails);
								}
							}

							#endregion
						}

					}
					DAL.Registration.InsertLog(webPosLog);
				}

				#region DynamicProxyFactory - old method
				/*
				 DynamicProxyFactory factory = new DynamicProxyFactory(wsdlUrl);
                //int count = 0;

                if (factory.Contracts.Count() == 1)
                {
                    DynamicProxy dynamicproxy = factory.CreateProxy(factory.Contracts.First().Name);

                    var webPosLog = new WebPosLog()
                    {
                        exception = "",
                        fileName = "DynamicService",
                        method = wsdlUrl,
                        request = String.IsNullOrEmpty(input.Trim()) ? "isellContent Empty" : input,
                        response = "",
                        userId = 0,
                        regId = iSellOrderID.ToString2()
                    };

                    if (statusCode == 1)
                    {
                        statusDesc = "Cancel";
                    }

                    if ((!ReferenceEquals(iSellOrderID, null) && !String.IsNullOrEmpty(input)) || statusCode == 1)
                    {
                        object tempToPos = null;
                        string custEmail = "";
                        //empty string will be send if email lenght > 25 and contains special characters except + - _ . @ 
                        if (cust.EmailAddr != null && cust.EmailAddr.Length < 25)
                        {
                            bool validEmail = Regex.IsMatch((cust.EmailAddr), @"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$");
                            if (validEmail)
                            {
                                custEmail = cust.EmailAddr;
                            }
                        }
                        if (statusCode != 1)
                        {
                            tempToPos = (dynamicproxy.CallMethod("UpdateISellTransactionToPos", reg.QueueNo.ToString2(), regdesc, iSellOrderID.ToLong(), reg.OrganisationId.ToInt(), cust.FullName, timestamp, reg.MSISDN1, input, statusCode, statusDesc, reg.SalesPerson.ToUpper(), custEmail));
                        }
                        else
                        {
                            tempToPos = (dynamicproxy.CallMethod("UpdateISellTransactionToPos", reg.QueueNo.ToString2(), "", iSellOrderID.ToLong(), reg.OrganisationId.ToInt(), "", timestamp, "", "", statusCode, statusDesc, "", ""));
                        }
                        System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(tempToPos.GetType());
                        xmlSerializer.Serialize(txstreamer, tempToPos);
                        XMLResponse = txstreamer.ToString2();
                        XMLResponse = Util.RemoveAllNamespaces(XMLResponse);
                        System.Xml.Linq.XDocument xdoc = System.Xml.Linq.XDocument.Parse(XMLResponse);
                        foreach (var objmap in xdoc.Descendants("ISellResponse"))
                        {
                            objSendPaymentStatus.msgCode = objmap.Element("msgCode") == null ? 0 : objmap.Element("msgCode").Value.ToInt();
                            objSendPaymentStatus.msgDesc = objmap.Element("msgDesc") == null ? string.Empty : objmap.Element("msgDesc").Value.ToString2();
                            webPosLog.response = "Status = " + statusCode.ToString2() + " ,MsgCode = " + objSendPaymentStatus.msgCode.ToString2() + " ,MsgDesc = " + objSendPaymentStatus.msgDesc;

                            #region Update the Order Status to Fail if the response code is 2 (POS Internal Error)
                            if (objSendPaymentStatus.msgCode.ToString2() == "2")
                            {
                                var posErrorIndicator = System.Configuration.ConfigurationManager.AppSettings["POSErrorIndicator"].ToString2();//"String or binary data would be truncated". this is happenned if the character amount in isellContent is more than POS can handle
                                var posErrorNotification = System.Configuration.ConfigurationManager.AppSettings["POSErrorNotification"].ToString2();

                                if (objSendPaymentStatus.msgDesc.Contains(posErrorIndicator))
                                {
                                    var regStatus = new RegStatus();
                                    var kenanLogDetails = new KenanaLogDetails();

                                    regStatus.RegID = iSellOrderID;
                                    regStatus.Active = true;
                                    regStatus.CreateDT = DateTime.Now;
                                    regStatus.StartDate = DateTime.Now;
                                    regStatus.LastAccessID = "POS";
                                    regStatus.StatusID = DAL.Admin.OnlineRegAdmin.StatusFind(new StatusFind()
                                    {
                                        Status = new Status()
                                        {
                                            Code = "REG_Fail"
                                        }
                                    }).ToList()[0];

                                    DAL.Registration.RegistrationCancel(regStatus);
                                    
                                    kenanLogDetails.RegID = iSellOrderID;
                                    kenanLogDetails.MessageCode = objSendPaymentStatus.msgCode.ToString2();
                                    kenanLogDetails.MessageDesc = posErrorNotification;
                                    kenanLogDetails.KenanXmlReq = webPosLog.request;
                                    kenanLogDetails.KenanXmlRes = webPosLog.response;
                                    kenanLogDetails.MethodName = "WebPosAuto";

                                    DAL.Admin.OnlineRegAdmin.SaveKenanXmlLogs(kenanLogDetails);
                                }
                            }
                            #endregion
                        }
                        //objSendPaymentStatus = (statusToPos)tempToPos;
                    }
                    DAL.Registration.InsertLog(webPosLog);
                    dynamicproxy.Close();
                }
                else
                {
                    Logger.InfoFormat("SendPaymentDetails : DynamicProxyFactory.Contracts.Count is no equalts then call the Service method GetStockCount()");
                }
				 */
				#endregion

				Logger.InfoFormat("SendPaymentDetails: Entering (iSellOrderID:{0},WSDL URL:{1},###################code:{2}###desc:{3})", string.Join(",", iSellOrderID), wsdlUrl, objSendPaymentStatus.msgCode.ToString2(), objSendPaymentStatus.msgDesc);
			}
            catch (Exception ex)
            {
                Logger.InfoFormat("SendPaymentDetails: Entering (Exception:{0}, iSellOrderID:{1},WSDL URL:{2})", ex.ToString(), iSellOrderID, wsdlUrl);
            }
            if (!ReferenceEquals(objSendPaymentStatus, null))
            {
                result = ((!ReferenceEquals(objSendPaymentStatus.msgCode, null) && objSendPaymentStatus.msgCode == 1) || String.IsNullOrEmpty(input)) ? true : false;
            }
            return result;
        }

        #region XML UpdateISellTransactionToPos_ DYnamicService
//        public bool SendPaymentDetails(int iSellOrderID)
//        {
//            bool result = false;
//            String input = String.Empty;
//            var reg = Online.Registration.DAL.Registration.RegistrationGet(new List<int> { iSellOrderID }).SingleOrDefault();
//            var cust = Online.Registration.DAL.Registration.CustomerGet(new List<int> { reg.CustomerID }).SingleOrDefault();
//            var Org = Online.Registration.DAL.Admin.OnlineRegAdmin.OrganizationGet(new List<int>{reg.CenterOrgID.ToInt()}).FirstOrDefault();
//            String wsdlUrl = Org.WSDLUrl;
//            var webPosLog = new WebPosLog()
//            {
//                exception = "",
//                fileName = "DynamicService",
//                method = "WebPosAuto",
//                request = wsdlUrl,
//                response = "",
//                userId = 0,
//                regId = iSellOrderID.ToString2()
//            };

//            DAL.Registration.InsertLog(webPosLog);
//            var stsID = Online.Registration.DAL.Registration.RegStatusFind(new RegStatusFind()
//            {
//                RegStatus = new RegStatus()
//                {
//                    RegID = iSellOrderID
//                },
//                Active = true
//            }).FirstOrDefault();
//            var sts = Online.Registration.DAL.Registration.RegStatusGet(new List<int>() { stsID });
//            var stsdesc = Online.Registration.DAL.Admin.OnlineRegAdmin.StatusGet(new List<int>() { sts.FirstOrDefault().StatusID });
//            var timestamp = System.DateTime.Now.ToString("yyyyMMddHHmmss");
//            KenanService _client = new KenanService();
//            //may need condition filter
//            try
//            {
//                if (reg != null && reg.RegTypeID == 14)
//                {
//                    input = _client.GetIMPOSDataForWEBPOS(reg.ID, "SIMReplacement");
//                }
//                else
//                {
//                    input = _client.GetIMPOSDataForWEBPOS(reg.ID, "P");
//                }
//            }
//            catch (Exception ex)
//            {

//                input = "NonBilling|20046799|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000|893456789087653608|EA|||NonBilling|70000005|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||EA|||NonBilling|70000067|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||NonBilling|70000100|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||NonBilling|70000065|60122143548|1700123514|CS|singleport|3423|3454|3432432|city|46000||ZD1|||";
//            }

//            String regdesc = Online.Registration.DAL.Admin.OnlineRegAdmin.RegTypeGet(new List<int>{ reg.RegTypeID }).FirstOrDefault().Description;

//            SendPaymentDetailsWebPosRequest request = new SendPaymentDetailsWebPosRequest
//            {
//                queueNo = reg.QueueNo.ToInt(),
//                regType = regdesc,
//                iSellOrderID = iSellOrderID,
//                storeID = reg.OrganisationId.ToInt(),
//                customerName = cust.FullName,
//                iSellTimeStamp = timestamp,
//                MSISDN = reg.MSISDN1,
//                iSellContent = input,
//                orderStatusCode = 1,
//                //orderStatusCode = sts.FirstOrDefault().StatusID,
//                orderStatusDesc = "Ready For Payment",
//                //orderStatusDesc = stsdesc.FirstOrDefault().Description,
//                SalesAgentID = reg.SalesPerson
//            };

//            //result = _clientsvc.SendPaymentDetails(request);
//            string sendPaymentDetailsXMLRequest = string.Empty;
//            string sendPaymentDetailsXMLResponse = string.Empty;
//            //string userName = Properties.Settings.Default.ValidateInventoryExtIdResponseUserName;
//            //string password = Properties.Settings.Default.ValidateInventoryExtIdResponsePassword;

//            var response = new SendPaymentDetailsWebPosResponse();
//            sendPaymentDetailsXMLRequest = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://www.w3.org/2003/05/soap-envelope"" xmlns:tem=""http://tempuri.org/"">
//                <soapenv:Header/>
//               <soapenv:Body>
//                  <tem:UpdateISellTransactionToPos>
//                     <tem:queueNo>{0}</tem:queueNo>
//                     <tem:regType>{1}</tem:regType>
//                     <tem:iSellOrderID>{2}</tem:iSellOrderID>
//                     <tem:storeID>{3}</tem:storeID>
//                     <tem:customerName>{4}</tem:customerName>
//                     <tem:iSellTimeStamp>{5}</tem:iSellTimeStamp>
//                     <tem:MSISDN>{6}</tem:MSISDN>
//                     <tem:iSellContent>{7}</tem:iSellContent>
//                     <tem:orderStatusCode>{8}</tem:orderStatusCode>
//                     <tem:orderStatusDesc>{9}</tem:orderStatusDesc>
//                     <tem:salesAgentID>{10}</tem:salesAgentID>
//                  </tem:UpdateISellTransactionToPos>
//               </soapenv:Body>
//            </soapenv:Envelope>
//            ", new String[] { request.queueNo.ToString2(), request.regType, request.iSellOrderID.ToString2(), request.storeID.ToString2(), request.customerName, request.iSellTimeStamp, request.MSISDN, request.iSellContent, request.orderStatusCode.ToString2(), request.orderStatusDesc, request.SalesAgentID });

//            string dummyReplyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
//                <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
//                <SOAP-ENV:Body>
//                <ser-root:UpdateISellTransactionToPosResponse SOAP-ENC:root=""1"" xmlns:ser-root=""http://www.webmethods.com/maxis.eai.process.common.ws"">
//                <msgCode xsi:type=""xsd:string"">1</msgCode>
//                    <msgDesc xsi:type=""xsd:string"">Success</msgDesc>
//                </ser-root:UpdateISellTransactionToPosResponse>
//                </SOAP-ENV:Body>
//                </SOAP-ENV:Envelope>";
//            try
//            {
//                WebClient sendPaymentDetailsClient = new WebClient();
//                string postResult = string.Empty;
//                sendPaymentDetailsXMLRequest = Util.RemoveAllNamespaces(sendPaymentDetailsXMLRequest);
//                //retrieveBilingInfoWebClient.Credentials = new NetworkCredential(userName, password);
//                Logger.Info("RegistrationToWebPosRequest  :" + sendPaymentDetailsXMLRequest);

//                if (Properties.Settings.Default.DevelopmentModeOnForWEBPOSChecking)
//                {
//                    postResult = dummyReplyData;
//                }
//                else
//                {
//                    //Uri postUri = new Uri(Properties.Settings.Default.UpdateISellTransactionPOS.ToString());
//                    postResult = sendPaymentDetailsClient.UploadString(wsdlUrl, sendPaymentDetailsXMLRequest);
//                }

//                sendPaymentDetailsXMLResponse = Util.RemoveAllNamespaces(postResult);
//                //var resp = proxy.UpdateISellTransactionToPos(SendDetailsToWebPosRq);
//                //sendPaymentDetailsXMLResponse = XMLHelper.ConvertObjectToXml(resp, typeof(Online.Registration.IntegrationService.MaxisOrderProcessWs.eaiResponseType));
//                Logger.Info("RegistrationToWebPosResponse  :" + sendPaymentDetailsXMLResponse);

//                XDocument doc = XDocument.Parse(sendPaymentDetailsXMLResponse);

//                foreach (var r in doc.Descendants("UpdateISellTransactionToPosResponse"))
//                {
//                    //response.Code = r.Element("Code") == null ? 0 : r.Element("Code").Value.ToInt();
//                    //response.Message = r.Element("Message") == null ? "" : r.Element("Message").Value.ToString();
//                    response.Code = r.Element("msgCode") == null ? 0 : r.Element("msgCode").Value.ToInt();
//                    response.Message = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
//                }
//                if (response != null)
//                {
//                    if (response.Code != 1)
//                    {
//                        /// 1 – success
//                        /// 2 - Internal Error
//                        /// 3 – Payment Already Completed
//                        /// 4 – Order aborted in POS
//                        response = null;
//                    }
//                }

//                //if (resp.msgCode != Properties.Settings.Default.KenanSuccessCode)
//                //{
//                //    response.Code = resp.msgCode;
//                //    response.Message = resp.msgDesc;
//                //}
//                //}
//            }
//            catch (Exception ex)
//            {
//                Logger.Error("", ex);
//                //PopulateExceptionResponse(response, ex);
//            }
//            Logger.Info(string.Format("Exiting RegistrationCreateToPOS(): iSellOrderID({0})", request.iSellOrderID));
//            if(!ReferenceEquals(response,null)){
//                result = (!ReferenceEquals(response.Code,null) && response.Code == 1) ? true : false;
//            }

//            return result;
//        }
        #endregion

        #region Eariler Method Commented---Uncommented for Production
        //public string GetSerialArtStatus(string IMEI, string wsdlUrl)
        //{
        //    Logger.InfoFormat("Entering (IMEI Number:{0},WSDL URL:{1})", IMEI, wsdlUrl);
        //    string regStatus = string.Empty;
        //    try
        //    {

        //        DynamicProxyFactory factory = new DynamicProxyFactory(wsdlUrl);
        //        if (factory.Contracts.Count() == 1)
        //        {
        //            DynamicProxy dynamicproxy = factory.CreateProxy(factory.Contracts.First().Name);
        //            if (!ReferenceEquals(IMEI, null))
        //            {
        //                regStatus = (string)(dynamicproxy.CallMethod("GetSerialArtStatus", IMEI));
        //            }

        //            dynamicproxy.Close();
        //        }
        //        else
        //        {
        //            //TODO: if more than one, we need to loop through all contracts and findout our method. And need to use corresponding Contract otherwise throw an error
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.InfoFormat("Entering (Exception:{0}, IMEI Number:{1},WSDL URL:{2})", ex.ToString(), IMEI, wsdlUrl);
        //    }
        //    return regStatus;
        //}
        #endregion Eariler Method Commented

        #region Decommisioned old method as part of Drop5
        /*
        public class GetSerialArtStatusResp
        {
            public string szSerialNmbr { get; set; }
            public string szItemID { get; set; }
            public string szStatus { get; set; }
        }

        #region Latest Method Commented---Should Uncomment after POS updation in production
        public string GetSerialArtStatus(string IMEI, string wsdlUrl)
        {
            Logger.InfoFormat("Entering (IMEI Number:{0},WSDL URL:{1})", IMEI, wsdlUrl);
            string regStatus = string.Empty;
            string postResult = string.Empty;
            string postData = string.Empty;
            try
            {
                if (!ReferenceEquals(IMEI, null))
                {
                    GetSerialArtStatusResp obj = new GetSerialArtStatusResp();
                    System.Net.WebClient processWebClient = new System.Net.WebClient();
                    Uri postUri = new Uri(wsdlUrl);
                    postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:tem=""http://tempuri.org/"">
                                        <soapenv:Header/>
                                        <soapenv:Body>
                                        <tem:GetSerialArtStatus>
                                        <tem:szSerialNmbr>{0}</tem:szSerialNmbr>
                                        </tem:GetSerialArtStatus>
                                        </soapenv:Body>
                                        </soapenv:Envelope>", IMEI);
                    processWebClient.Headers.Add("content-type", "text/xml");
                    postResult = processWebClient.UploadString(postUri, postData);
                    postResult = Online.Registration.IntegrationService.Helper.Util.RemoveAllNamespaces(postResult);
                    postData = Online.Registration.IntegrationService.Helper.Util.RemoveAllNamespaces(postData);
                    Logger.Info(postResult);
                    if (postResult != null)
                    {
                        System.Xml.Linq.XDocument xdoc = System.Xml.Linq.XDocument.Parse(postResult);
                        foreach (var or in xdoc.Descendants("GetSerialArtStatusResult"))
                        {
                            obj.szItemID = or.Element("szItemID") == null ? string.Empty : or.Element("szItemID").Value.ToString();
                            obj.szSerialNmbr = or.Element("szSerialNmbr") == null ? string.Empty : or.Element("szSerialNmbr").Value.ToString();
                            obj.szStatus = or.Element("szStatus") == null ? string.Empty : or.Element("szStatus").Value.ToString();
                        }
                    }
                    if (IMEI == obj.szSerialNmbr)
                    {
                        regStatus = obj.szItemID + "|" + obj.szStatus;
                    }
                    else
                    {
                        regStatus = null;
                    }

                    Logger.InfoFormat("Entering (regStatus:{0}, IMEI Number:{1},WSDL URL:{2})", regStatus, IMEI, wsdlUrl);
                }
            }
            catch (Exception ex)
            {
                Logger.InfoFormat("Entering (Exception:{0}, IMEI Number:{1},WSDL URL:{2})", ex.ToString(), IMEI, wsdlUrl);
            }
            return regStatus;
        }
        #endregion
        */
        #endregion

        public class GetSerialNumberStatusResp
        {
            public string szSerialNmbr { get; set; }
            public string szItemID { get; set; }
            public string szStatus { get; set; }
        }
        public string GetSerialNumberStatus(string storeId, string IMEI, string wsdlUrl)
        {
            Logger.InfoFormat("Entering (IMEI Number:{0},WSDL URL:{1},StroeId:{3})", IMEI, wsdlUrl, storeId);
            string regStatus = string.Empty;
            string postResult = string.Empty;
            string postData = string.Empty;
            try
            {
                if (!ReferenceEquals(IMEI, null))
                {
                    GetSerialNumberStatusResp obj = new GetSerialNumberStatusResp();
                    System.Net.WebClient processWebClient = new System.Net.WebClient();
                    Uri postUri = new Uri(wsdlUrl);
                    postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:tem=""http://tempuri.org/"">
                                        <soapenv:Header/>
                                        <soapenv:Body>
                                        <tem:GetSerialNumberStatus>
                                        <tem:lRetailStoreID>{1}</tem:lRetailStoreID>
                                        <tem:szSerialNmbr>{0}</tem:szSerialNmbr>
                                        </tem:GetSerialNumberStatus>
                                        </soapenv:Body>
                                        </soapenv:Envelope>", IMEI, storeId);
                    processWebClient.Headers.Add("content-type", "text/xml");
                    postResult = processWebClient.UploadString(postUri, postData);
                    postResult = Online.Registration.IntegrationService.Helper.Util.RemoveAllNamespaces(postResult);
                    postData = Online.Registration.IntegrationService.Helper.Util.RemoveAllNamespaces(postData);
                    Logger.Info(postResult);
                    if (postResult != null)
                    {
                        System.Xml.Linq.XDocument xdoc = System.Xml.Linq.XDocument.Parse(postResult);
                        foreach (var or in xdoc.Descendants("GetSerialNumberStatusResult"))
                        {
                            obj.szItemID = or.Element("szItemID") == null ? string.Empty : or.Element("szItemID").Value.ToString();
                            obj.szSerialNmbr = or.Element("szSerialNmbr") == null ? string.Empty : or.Element("szSerialNmbr").Value.ToString();
                            obj.szStatus = or.Element("szStatus") == null ? string.Empty : or.Element("szStatus").Value.ToString();
                        }
                    }
                    if (IMEI == obj.szSerialNmbr)
                    {
                        regStatus = obj.szItemID + "|" + obj.szStatus;
                    }
                    else
                    {
                        regStatus = null;
                    }

                    Logger.InfoFormat("Entering (regStatus:{0}, (IMEI Number:{0},WSDL URL:{1},StroeId:{3})", regStatus, IMEI, wsdlUrl, storeId);
                }
            }
            catch (Exception ex)
            {
                Logger.InfoFormat("Entering (Exception:{0}, IMEI Number:{1},WSDL URL:{2},StroeId:{3})", ex.ToString(), IMEI, wsdlUrl, storeId);
            }
            return regStatus;
        }
    }
}
