﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.MaxisOrderProcessWs;
using Online.Registration.IntegrationService.MaxisgetMNPRequestServiceWs;
using Online.Registration.IntegrationService.ExtraTenWS;
using Online.Registration.DAL.Models;


namespace Online.Registration.IntegrationService
{
    [ServiceContract]
    public interface IKenanService
    {
        [OperationContract]
        RetrieveSuscriberDetailsResponse RetrieveSubscriberDetails(RetrieveSuscriberDetailsRequest request);

        [OperationContract]
        RetrieveBillingDetailsResponse RetrieveBillingDetails(RetrieveBillingDetailsRequest request);

        [OperationContract]
        RetrieveComponentInfoResponse RetrieveComponentInfo(RetrieveComponentInfoRequest request);

        [OperationContract]
        RetrieveExtAccDetailsResponse RetrieveExtAccountDetails(RetrieveExtAccDetailsRequest request);

        [OperationContract]
        BlacklistCheckResponse BlacklistCheck(BlacklistCheckRequest request);

        [OperationContract]
        MobileNoSelectionResponse MobileNoSelection(MobileNoSelectionRequest request);

        [OperationContract]
        MobileNoUpdateResponse MobileNoUpdate(MobileNoUpdateRequest request);

        [OperationContract]
        PostcodeValidationResponse PostcodeValidation(PostcodeValidationRequest request);

        [OperationContract]
        bool CloseCenterOrder(typeCloseCenterOrders request);

          [OperationContract]
        bool CancelCenterOrder(typeCancelCenterOrders request);

          [OperationContract]
          bool Deviceknockoff(typeKnockOffDeviceRequests request);

        [OperationContract]
        bool KenanAccountCreate(OrderCreationRequest request);
        //OrderCreationResponse KenanAccountCreate(OrderCreationRequest request);
        
        [OperationContract]
        bool KenanAccountFulfill(OrderFulfillRequest request);
        //OrderFulfillResponse KenanAccountFulfill(OrderFulfillRequest request);

        [OperationContract]
        bool KenanMNPAccountFulfill(MNPtypeFullfillCenterOrderMNP request);

        [OperationContract]
        UpdateOrderStatusResponse UpdateOrderStatus(UpdateOrderStatusRequest request);
        
        [OperationContract]
        void UpdateSupplimentaryFullFill(int RegID);

        [OperationContract]
        BusinessRuleResponse BusinessRuleCheck(BusinessRuleRequest request);

        [OperationContract]
        TotalLineCheckResponse TotalLineCheck(TotalLineCheckRequest request);

        [OperationContract]
        ExtraTenRetreivalResponse RetrieveExtraTen(ExtraTenRetrieveService.typeRetrieveExtraTenRequest request);

        [OperationContract]
        string sendTacViaSMSOnline(string msisdn, string msg);

        [OperationContract]
        ExtraTenUpdateResponse ProcessExtraTen(typeProcessExtraTenRequest extraTenRequest, int regID);

        #region VLT ADDED CODE
        [OperationContract]
        BusinessRuleResponse BusinessRuleCheckNew(BusinessRuleRequest request);
        #endregion VLT ADDED CODE

        #region SUTAN ADDED CODE 24rthFeb2013
        [OperationContract]
        KenanTypeAdditionLineRegistrationResponse KenanAdditionLineRegistration(KenanTypeAdditionLineRegistration request);
        [OperationContract]
        KenanNewSuppLineResponse KenanAddNewSuppLine(KenanNewSuppLineRequest request);
        #endregion SUTAN ADDED CODE 24rthFeb2013

        [OperationContract]
        #region Sutan Added Code 21ST March 2013
        MNPOrderCreationResponse MNPcreateCenterOrderMNP(OrderCreationRequestMNP request);
        //[OperationContract]
        //MNPFulfillCenterOrderMNPResponse MNPfulfillCenterOrderMNP(MNPtypeFullfillCenterOrderMNP request);
        #endregion Sutan Added Code 21ST March 2013

        
        [OperationContract]
        NewRetrieveComponentInfoResponse NewRetrieveComponentInfo(NewRetrieveComponentInfoRequest request);
        [OperationContract]
        bool InventoryCheck(TypeValidateInventoryExtIdRequest request);
        [OperationContract]
        List<string> GetDepenedencyComponents(string componentId, string userName);

        //Added by Sindhu 18/0602013
        [OperationContract]
        void ProcessAddRemoveVAS(Online.Registration.IntegrationService.Models.AddRemoveVASReq request, int regId);


        #region Added by narayanareddy
        [OperationContract]
        bool saveFile(int id, string strSIMType = "P");
        #endregion
        [OperationContract]
        typeCreateCenterOrder ConstructOrderCreateHomeRequest(OrderCreationRequest request);


        //#region "Spend Limit by VLT"
        //[OperationContract]
        //RetrieveOccSpendLimitResponse RetrieveOccSpendLimit(RetrieveOccSpendLimitRequest request);

        //[OperationContract]
        //ProcessOccSpendLimitResponse ProcessOccSpendLimit(ProcessOccSpendLimitRequest request);
        //#endregion

        #region chkFxVasDependency IMPLEMENTATTION
        /// <summary>
        /// CHKs the fx vas dependency.
        /// </summary>
        /// <Author>Sutan Dan</Author>
        /// <Date>06112013</Date>
        /// <param name="request">TypeOf(ChkFxVasDependencyRequest)</param>
        /// <returns>TypeOf(ChkFxVasDependencyResponse)</returns>
        [OperationContract]
        ChkFxVasDependencyResponse ChkFxVasDependency(ChkFxVasDependencyRequest request);
        #endregion chkFxVasDependency IMPLEMENTATTION


        //Added by Himansu 21 June 2013
        [OperationContract]
        RetrieveSimDetlsResponse retreiveSIMDetls(string msisdn);

        [OperationContract]
        eaiResponseType SwapCenterInventory(int regID);
        // Added by Kotesh 23 June 2013

        [OperationContract]
        bool CenterOrderContractCreation(CenterOrderContractRequest request);
        [OperationContract]
        bool CenterOrderContractCreationForCRP(CenterOrderContractRequest request);
        
        [OperationContract]
        getMNPRequestResponse GetMNPRequest(getMNPRequest request);

        [OperationContract]
        PreportInResponse PreportInValidationCheck(PreportInRequest _PortInRequest);

        #region MISM

        [OperationContract]
        KenanTypeAdditionLineRegistrationResponse KenanAdditionLineRegistrationMISMSecondary(KenanTypeAdditionLineRegistration request);

        #endregion

        [OperationContract]
        bool UpdateContractExtDetails(int regID);

        [OperationContract]
        BusinessRuleResponse[] BusinessRuleOneTimeCheck(List<BusinessRuleRequest> breReq);


        [OperationContract]
        BusinessRuleResponse[] BusinessRuleOneTimeCheckForWriteOff(List<BusinessRuleRequest> breReq);

        [OperationContract]
        string TenureCheck();

        //Added by ravi for device sales
        [OperationContract]
        bool CreateDeviceSalesPOSFile(int RegId);

        //Added by ravi for SIM Replacement
        [OperationContract]
        bool CreateSIMReplacementPOSFile(int RegId);

        [OperationContract]
        bool ProcessAddComponentsForPrimaryLine(List<int> Ids);

		//[OperationContract]
		//MaxisRetrieveInventoryDetails.typeRetrieveInventoryDetlsResponse RetrieveInventoryInfo(string Msisdn);

		[OperationContract]
		RetrieveInventoryDetails.typeRetrieveInventoryDetlsResponse RetrieveInventoryInfo(string Msisdn);

        [OperationContract]
        DisconnectCenterServiceResponse DisconnectComponents(DisconnectCenterServiceRequest compRequest);

        [OperationContract]
        retrievePrepaidSubscriberResponse retrievePrepaidSubscriberDetails(string msisdn);
        
        [OperationContract]
        string GetIMPOSDataForWEBPOS(int regid, string strSIMType = "P");

        [OperationContract]
        bool WebPosAuto(int orderID);

        [OperationContract]
        retrieveDealerInfoResponse RetrieveDealerInfo(retrieveDealerInfoRequest req);
    }
}
