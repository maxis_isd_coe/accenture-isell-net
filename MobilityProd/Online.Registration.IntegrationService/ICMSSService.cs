﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.IntegrationService.Models;

namespace Online.Registration.IntegrationService
{
    [ServiceContract]
    public interface ICMSSService
    {
        [OperationContract]
        CreateInteractionCaseResponse CreateInteractionCase(CreateInteractionCaseRequest request);
    }
}
