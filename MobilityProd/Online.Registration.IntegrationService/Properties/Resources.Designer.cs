﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Online.Registration.IntegrationService.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Online.Registration.IntegrationService.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 7004.
        /// </summary>
        internal static string AdminFee_ExtendedData_ID {
            get {
                return ResourceManager.GetString("AdminFee_ExtendedData_ID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Admin Fee.
        /// </summary>
        internal static string AdminFee_ExtendedData_Name {
            get {
                return ResourceManager.GetString("AdminFee_ExtendedData_Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 100.
        /// </summary>
        internal static string AdminFee_ExtendedData_Value {
            get {
                return ResourceManager.GetString("AdminFee_ExtendedData_Value", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 6005|Biometric Verification|YES.
        /// </summary>
        internal static string BioMetric_ExtendedData {
            get {
                return ResourceManager.GetString("BioMetric_ExtendedData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CHANGECNTR.
        /// </summary>
        internal static string Change_Contract {
            get {
                return ResourceManager.GetString("Change_Contract", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CHANGEPKG.
        /// </summary>
        internal static string Change_Package {
            get {
                return ResourceManager.GetString("Change_Package", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDEXTERN.
        /// </summary>
        internal static string EventType_UPDEXTERN {
            get {
                return ResourceManager.GetString("EventType_UPDEXTERN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EC.
        /// </summary>
        internal static string Extra_Component {
            get {
                return ResourceManager.GetString("Extra_Component", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EP.
        /// </summary>
        internal static string Extra_PLAN {
            get {
                return ResourceManager.GetString("Extra_PLAN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 54|MyKad Registration|YES.
        /// </summary>
        internal static string MyKad_ExtendedData {
            get {
                return ResourceManager.GetString("MyKad_ExtendedData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2.
        /// </summary>
        internal static string orderCreate_accountSegId {
            get {
                return ResourceManager.GetString("orderCreate_accountSegId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -1.
        /// </summary>
        internal static string orderCreate_accountStatus {
            get {
                return ResourceManager.GetString("orderCreate_accountStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_accountType {
            get {
                return ResourceManager.GetString("orderCreate_accountType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 458.
        /// </summary>
        internal static string orderCreate_billCountry {
            get {
                return ResourceManager.GetString("orderCreate_billCountry", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.
        /// </summary>
        internal static string orderCreate_billDispMeth {
            get {
                return ResourceManager.GetString("orderCreate_billDispMeth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 6.
        /// </summary>
        internal static string orderCreate_billFmtOpt {
            get {
                return ResourceManager.GetString("orderCreate_billFmtOpt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_billFranchiseTaxCode {
            get {
                return ResourceManager.GetString("orderCreate_billFranchiseTaxCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3.
        /// </summary>
        internal static string orderCreate_billingFrequency {
            get {
                return ResourceManager.GetString("orderCreate_billingFrequency", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.
        /// </summary>
        internal static string orderCreate_chargeThreshold {
            get {
                return ResourceManager.GetString("orderCreate_chargeThreshold", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.
        /// </summary>
        internal static string orderCreate_collectionIndicator {
            get {
                return ResourceManager.GetString("orderCreate_collectionIndicator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.
        /// </summary>
        internal static string orderCreate_collectionStatus {
            get {
                return ResourceManager.GetString("orderCreate_collectionStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 13|Country.
        /// </summary>
        internal static string orderCreate_country {
            get {
                return ResourceManager.GetString("orderCreate_country", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.
        /// </summary>
        internal static string orderCreate_creditRating {
            get {
                return ResourceManager.GetString("orderCreate_creditRating", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_currencyCode {
            get {
                return ResourceManager.GetString("orderCreate_currencyCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_custFranchiseTaxCode {
            get {
                return ResourceManager.GetString("orderCreate_custFranchiseTaxCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3|BirthDate.
        /// </summary>
        internal static string orderCreate_dob {
            get {
                return ResourceManager.GetString("orderCreate_dob", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_exrateClass {
            get {
                return ResourceManager.GetString("orderCreate_exrateClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2|Gender.
        /// </summary>
        internal static string orderCreate_gender {
            get {
                return ResourceManager.GetString("orderCreate_gender", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_globalContractStatus {
            get {
                return ResourceManager.GetString("orderCreate_globalContractStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string orderCreate_hierarchyId {
            get {
                return ResourceManager.GetString("orderCreate_hierarchyId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_insertGrpId {
            get {
                return ResourceManager.GetString("orderCreate_insertGrpId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_languageCode {
            get {
                return ResourceManager.GetString("orderCreate_languageCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_msgGrpId {
            get {
                return ResourceManager.GetString("orderCreate_msgGrpId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 21|Nationality.
        /// </summary>
        internal static string orderCreate_nationality {
            get {
                return ResourceManager.GetString("orderCreate_nationality", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.
        /// </summary>
        internal static string orderCreate_noBill {
            get {
                return ResourceManager.GetString("orderCreate_noBill", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_owningCostCtr {
            get {
                return ResourceManager.GetString("orderCreate_owningCostCtr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1|Race.
        /// </summary>
        internal static string orderCreate_race {
            get {
                return ResourceManager.GetString("orderCreate_race", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_rateClassDefault {
            get {
                return ResourceManager.GetString("orderCreate_rateClassDefault", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_regulatoryId {
            get {
                return ResourceManager.GetString("orderCreate_regulatoryId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderCreate_revRcvCostCtr {
            get {
                return ResourceManager.GetString("orderCreate_revRcvCostCtr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderFulfil_connectReason {
            get {
                return ResourceManager.GetString("orderFulfil_connectReason", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderFulfil_currencyCode {
            get {
                return ResourceManager.GetString("orderFulfil_currencyCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 23.
        /// </summary>
        internal static string orderFulfil_displayExternalIdType {
            get {
                return ResourceManager.GetString("orderFulfil_displayExternalIdType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4001.
        /// </summary>
        internal static string orderFulfil_emfConfigId {
            get {
                return ResourceManager.GetString("orderFulfil_emfConfigId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderFulfil_exrateClass {
            get {
                return ResourceManager.GetString("orderFulfil_exrateClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.
        /// </summary>
        internal static string orderFulfil_isPrepaid {
            get {
                return ResourceManager.GetString("orderFulfil_isPrepaid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.
        /// </summary>
        internal static string orderFulfil_noBill {
            get {
                return ResourceManager.GetString("orderFulfil_noBill", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderFulfil_rateClass {
            get {
                return ResourceManager.GetString("orderFulfil_rateClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to N.
        /// </summary>
        internal static string orderFulfil_requiredContract {
            get {
                return ResourceManager.GetString("orderFulfil_requiredContract", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderFulfil_statusId {
            get {
                return ResourceManager.GetString("orderFulfil_statusId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.
        /// </summary>
        internal static string orderFulfil_statusReasonId {
            get {
                return ResourceManager.GetString("orderFulfil_statusReasonId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.
        /// </summary>
        internal static string orderFulfil_timezone {
            get {
                return ResourceManager.GetString("orderFulfil_timezone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2.
        /// </summary>
        internal static string orderFulfil_viewStatus {
            get {
                return ResourceManager.GetString("orderFulfil_viewStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4049|Access Level - Intra (COS)|NCOS1.
        /// </summary>
        internal static string orderFullfill_AccessLevelIntra {
            get {
                return ResourceManager.GetString("orderFullfill_AccessLevelIntra", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4041|Enterprise ID|NGBB Home.
        /// </summary>
        internal static string orderFullfill_EnterpriseID {
            get {
                return ResourceManager.GetString("orderFullfill_EnterpriseID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4042|Group ID|MaxisHome00001.
        /// </summary>
        internal static string orderFullfill_GroupID {
            get {
                return ResourceManager.GetString("orderFullfill_GroupID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4060|Home Inst Addr|Kuala Lumpur.
        /// </summary>
        internal static string orderFullfill_HomeInstAddr {
            get {
                return ResourceManager.GetString("orderFullfill_HomeInstAddr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1273|Home Optr Code|Maxis FTTH.
        /// </summary>
        internal static string orderFullfill_HomeOptrCode {
            get {
                return ResourceManager.GetString("orderFullfill_HomeOptrCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4063|Home_TM_Activation_Order_ID|98879.
        /// </summary>
        internal static string orderFullfill_HomeTMActivationOrderID {
            get {
                return ResourceManager.GetString("orderFullfill_HomeTMActivationOrderID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4062|Home_TM_Service_ID|32344.
        /// </summary>
        internal static string orderFullfill_HomeTMServiceID {
            get {
                return ResourceManager.GetString("orderFullfill_HomeTMServiceID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4043|User Device Type|Generic SIP Phone.
        /// </summary>
        internal static string orderFullfill_UserDeviceType {
            get {
                return ResourceManager.GetString("orderFullfill_UserDeviceType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CANCELORD.
        /// </summary>
        internal static string orderProcessEventType_Cancel {
            get {
                return ResourceManager.GetString("orderProcessEventType_Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CLOSEORD.
        /// </summary>
        internal static string orderProcessEventType_Close {
            get {
                return ResourceManager.GetString("orderProcessEventType_Close", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATEORD.
        /// </summary>
        internal static string orderProcessEventType_create {
            get {
                return ResourceManager.GetString("orderProcessEventType_create", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CREATEORDMNP.
        /// </summary>
        internal static string orderProcessEventType_createMNP {
            get {
                return ResourceManager.GetString("orderProcessEventType_createMNP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to FULFILORD.
        /// </summary>
        internal static string orderProcessEventType_fulfill {
            get {
                return ResourceManager.GetString("orderProcessEventType_fulfill", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RESUBORD.
        /// </summary>
        internal static string orderProcessEventType_fulfillMNP {
            get {
                return ResourceManager.GetString("orderProcessEventType_fulfillMNP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SIMSWAP.
        /// </summary>
        internal static string orderProcessEventType_SIMSwap {
            get {
                return ResourceManager.GetString("orderProcessEventType_SIMSwap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OPF.
        /// </summary>
        internal static string orderProcessReq_appID {
            get {
                return ResourceManager.GetString("orderProcessReq_appID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ISELL.
        /// </summary>
        internal static string orderProcessReq_from {
            get {
                return ResourceManager.GetString("orderProcessReq_from", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ISELLMISM.
        /// </summary>
        internal static string orderProcessReq_fromMISM {
            get {
                return ResourceManager.GetString("orderProcessReq_fromMISM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request.
        /// </summary>
        internal static string orderProcessReq_msgType {
            get {
                return ResourceManager.GetString("orderProcessReq_msgType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RESUBROC.
        /// </summary>
        internal static string orderProcessReq_ReSubROC {
            get {
                return ResourceManager.GetString("orderProcessReq_ReSubROC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ESB.
        /// </summary>
        internal static string orderProcessReq_to {
            get {
                return ResourceManager.GetString("orderProcessReq_to", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OPF.
        /// </summary>
        internal static string orderProcessResp_appID {
            get {
                return ResourceManager.GetString("orderProcessResp_appID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ESB.
        /// </summary>
        internal static string orderProcessResp_from {
            get {
                return ResourceManager.GetString("orderProcessResp_from", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Response.
        /// </summary>
        internal static string orderProcessResp_msgType {
            get {
                return ResourceManager.GetString("orderProcessResp_msgType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ISELL.
        /// </summary>
        internal static string orderProcessResp_to {
            get {
                return ResourceManager.GetString("orderProcessResp_to", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 7003.
        /// </summary>
        internal static string PhoneModel_ExtendedData_ID {
            get {
                return ResourceManager.GetString("PhoneModel_ExtendedData_ID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Phone Model.
        /// </summary>
        internal static string PhoneModel_ExtendedData_Name {
            get {
                return ResourceManager.GetString("PhoneModel_ExtendedData_Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 70|REG_CHANNEL|ISELL.
        /// </summary>
        internal static string Reg_Channel_ExtendedData {
            get {
                return ResourceManager.GetString("Reg_Channel_ExtendedData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 7001.
        /// </summary>
        internal static string RRP_ExtendeData_ID {
            get {
                return ResourceManager.GetString("RRP_ExtendeData_ID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to RRP.
        /// </summary>
        internal static string RRP_ExtendeData_Name {
            get {
                return ResourceManager.GetString("RRP_ExtendeData_Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 70|REG_CHANNEL.
        /// </summary>
        internal static string SalesChannelID {
            get {
                return ResourceManager.GetString("SalesChannelID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 7002.
        /// </summary>
        internal static string UpfrontPayment_ExtendedData_ID {
            get {
                return ResourceManager.GetString("UpfrontPayment_ExtendedData_ID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Upfront Payment.
        /// </summary>
        internal static string UpfrontPayment_ExtendedData_Name {
            get {
                return ResourceManager.GetString("UpfrontPayment_ExtendedData_Name", resourceCulture);
            }
        }
    }
}
