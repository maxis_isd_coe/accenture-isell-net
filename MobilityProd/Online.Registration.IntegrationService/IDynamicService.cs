﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Online.Registration.IntegrationService
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    public interface IDynamicService
    {
        [OperationContract]
        int GetStockCount(string articleId, string wsdlUrl, int storeId);
        
        [OperationContract]
        Dictionary<string, int> GetStockCounts(List<string> articleId, string wsdlUrl, int storeId);

        //Will Retire
        //[OperationContract]
        //bool SendPaymentDetails(int iSellOrderID);

        [OperationContract]
        bool SendDetailsToWebPos(int iSellOrderID, int statusCode = 0);

        [OperationContract]
        string GetSerialNumberStatus(string storeId, string IMEI, string wsdlUrl);
    }
}
