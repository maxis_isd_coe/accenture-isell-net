﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Security.Principal;
using System.Xml;
using System.Xml.Linq;
using log4net;
using Online.Registration.DAL.Admin;
using Online.Registration.DAL.Models;
using Online.Registration.IntegrationService.Helper;
using Online.Registration.IntegrationService.MaxisDDMFCheckWs;
using Online.Registration.IntegrationService.MaxisOrderProcessWs;
using Online.Registration.IntegrationService.MaxisRetrieveRulesInfoService;
using Online.Registration.IntegrationService.Models;
using Online.Registration.IntegrationService.Properties;
using Online.Registration.IntegrationService.ValidateMnpInventoryServiceWS;
using SNT.Utility;
using System.Globalization;
using System.Threading;

namespace Online.Registration.IntegrationService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SubscriberHistory" in code, svc and config file together.
    public class SubscriberHistory : ISubscriberHistory
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(SubscriberHistory));

        public GetNewCustomerInfoResponse getNewCustomerInfo(GetNewCustomerInfoRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            if (request.enqIcNo != "")
            {
                Logger.InfoFormat("SubscriberHistoryWS.Entering getNewCustomerInfo: {0})", request.enqIcNo.ToString2());
            }
            else
            {
                Logger.InfoFormat("SubscriberHistoryWS.Entering getNewCustomerInfo: {0})", request.enqNewIc.ToString2());
            }

            var filteredResponse = new GetNewCustomerInfoResponse();
            string enqIcNo, enqNewIc, enqName, enqBrNo, recordTotal, msgCode, msgDesc;
            string xmlReqs = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:v11=""http://services.maxis.com.my/EAI/OPF/breService/v1/"" xmlns:v12=""http://schemas.maxis.com.my/EAI/OPF/bre/v1/"">
   <soapenv:Header>
      <v1:eaiHeader>
         <v1:from>{0}</v1:from>
         <v1:to>{1}</v1:to>
         <v1:appId>{2}</v1:appId>
         <v1:msgType>{3}</v1:msgType>
         <v1:msgId>{4}</v1:msgId>
         <!--Optional:-->
         <v1:correlationId>{5}</v1:correlationId>
         <v1:timestamp>{6}</v1:timestamp>
      </v1:eaiHeader>
   </soapenv:Header>
   <soapenv:Body>
      <v11:ctosDdmfCheckRequest>
         <v12:ctosDdmfCheckReqDetails>
            <v12:recordTotal>{7}</v12:recordTotal>
            <!--Zero or more repetitions:-->
            <v12:individualRequest>
               <!--Optional:-->
               <v12:enqIcNo>{8}</v12:enqIcNo>
               <!--Optional:-->
               <v12:enqNewIc>{9}</v12:enqNewIc>
               <!--Optional:-->
               <v12:enqName>{10}</v12:enqName>
            </v12:individualRequest>
            <!--Zero or more repetitions:-->
         </v12:ctosDdmfCheckReqDetails>
      </v11:ctosDdmfCheckRequest>
   </soapenv:Body>
</soapenv:Envelope>",
                Properties.Resources.orderProcessReq_from, //0
                Properties.Resources.orderProcessReq_to, //1
                string.Format("BRE"), //2
                Properties.Resources.orderProcessReq_msgType, //3              
                string.Format("ESB{0}", DateTime.Now.ToString("yyyyMMddHHmmss")), //4
                string.Format("OLO{0}", DateTime.Now.ToString("yyyyMMddHHmmss")), //5               
                DateTime.Now.ToString("yyyyMMddHHmmss"), //6
                request.recordTotal.ToString2(),//7
                request.enqIcNo.ToString2(), //8
                request.enqNewIc.ToString2(),//9
                request.enqName.ToString2()//10
                );            
            string replyDummyData = @"<?xml version=""1.0"" encoding=""UTF-8"" ?>
                                <soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/""><soapenv:Header>
                                <HDR:eaiHeader xmlns:HDR=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">
                            <eai:from xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">ESB</eai:from>
                            <eai:to xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">ISELL</eai:to>
                            <eai:appId xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">OPF</eai:appId>
                            <eai:msgType xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">Response</eai:msgType>
                            <eai:msgId xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">4f8e255a-43a6-45ef-a83a-52e5ed0fada7</eai:msgId>
                            <eai:correlationId xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">OLOA123456</eai:correlationId>
                            <eai:timestamp xmlns:eai=""http://schemas.maxis.com.my/EAI/common/soap/v1/"">20150403083709</eai:timestamp></HDR:eaiHeader>
                            </soapenv:Header>
                            <soapenv:Body>
                            <ser-root:ctosDdmfCheckResponse xmlns:ser-root=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"">
                                <enq:ctosDdmfCheckReqDetails xmlns:enq=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
                                    <enq:msgCode>0</enq:msgCode>
                                    <enq:msgDesc>Subscriber Data Found</enq:msgDesc>  
                               <enq:individualResponse> 
                                     
                                    <enq:individualEnq>                                
                                        <enq:enqIcNo>123123123123</enq:enqIcNo>
                                        <enq:enqNewIc>123</enq:enqNewIc>
                                        <enq:enqName>Celcom</enq:enqName>
                                        <enq:enqStatusCode>0</enq:enqStatusCode>
                                         <enq:enqStatusDesc>Verified</enq:enqStatusDesc>
                                    </enq:individualEnq> 
                                   
                                    <enq:individualResults>
                                        <enq:dfIcNo>123123123123</enq:dfIcNo>
                                        <enq:dfNewIc>123</enq:dfNewIc> 
                                        <enq:dbName>DDMF db</enq:dbName>
                                        <enq:dfAccount>1365636463</enq:dfAccount>
                                        <enq:Company>Celcom</enq:Company>
                                        <enq:branch>KL Sentral</enq:branch>
                                        <enq:submitDate>10-22-2014</enq:submitDate>
                                        <enq:entryDate>09-22-2014</enq:entryDate>
                                        <enq:userId>Admin</enq:userId>
                                        <enq:lastEnqCom>Telco</enq:lastEnqCom>
                                        <enq:lastEnqDate>12-12-2014</enq:lastEnqDate>
                                        <enq:state>Kuala Lumpur</enq:state>
                                        <enq:remarks>No Remarks</enq:remarks>
                                        <enq:matchType>0</enq:matchType>
                                   </enq:individualResults>   
                                    <enq:individualResults>
                                        <enq:dfIcNo>123123123123</enq:dfIcNo>
                                        <enq:dfNewIc>123</enq:dfNewIc> 
                                        <enq:dbName>DDMF db</enq:dbName>
                                        <enq:dfAccount>99976543</enq:dfAccount>
                                        <enq:Company>DiGi</enq:Company>
                                        <enq:branch>Mid Valley</enq:branch>
                                        <enq:submitDate>10-22-2014</enq:submitDate>
                                        <enq:entryDate>09-22-2014</enq:entryDate>
                                        <enq:userId>Admin</enq:userId>
                                        <enq:lastEnqCom>Telco</enq:lastEnqCom>
                                        <enq:lastEnqDate>12-12-2014</enq:lastEnqDate>
                                        <enq:state>Kuala Lumpur</enq:state>
                                        <enq:remarks>No Remarks</enq:remarks>
                                        <enq:matchType>0</enq:matchType>
                                   </enq:individualResults>
                              </enq:individualResponse>
                              
                              <enq:businessResponse>
                                   
                                    <enq:businessEnq>                                         
                                        <enq:enqBrNo>+6032121210</enq:enqBrNo>
                                        <enq:enqName>Celcom</enq:enqName>
                                        <enq:enqStatusCode>0</enq:enqStatusCode>
                                         <enq:enqStatusDesc>Verified</enq:enqStatusDesc>
                                    </enq:businessEnq>
                                    <enq:businessResults>  
                                        <enq:dfBrNo>+6032121210</enq:dfBrNo>
                                        <enq:dbName>DDMF Database</enq:dbName>
                                        <enq:dfAccount>901212121212</enq:dfAccount>
                                        <enq:Company>Celcom</enq:Company>
                                        <enq:branch>KL Sentral</enq:branch>
                                        <enq:submitDate>10-22-2014</enq:submitDate>
                                        <enq:entryDate>09-22-2014</enq:entryDate>
                                        <enq:userId>Admin</enq:userId>
                                        <enq:lastEnqCom>Telco</enq:lastEnqCom>
                                        <enq:lastEnqDate>12-12-2014</enq:lastEnqDate>
                                        <enq:state>Kuala Lumpur</enq:state>
                                        <enq:remarks>No Remarks</enq:remarks>
                                        <enq:matchType>0</enq:matchType>
                                    </enq:businessResults> 
                                    </enq:businessResponse>             
                                </enq:ctosDdmfCheckReqDetails>
                            </ser-root:ctosDdmfCheckResponse>
                            </soapenv:Body>
                            </soapenv:Envelope>";

            string xmlResp = string.Empty;
            try
            {
                using (var proxy = new WebClient())
                {
                    Uri postUri = new Uri(Properties.Settings.Default.Online_Registration_IntegrationService_MaxisBusinessRulesWs_DDMFHistory.ToString());
                    //string postUri = "http://10.200.51.125:8040/ws/maxis.eai.process.order.ws:breService/maxis_eai_process_order_ws_breService_Port";
                    proxy.Credentials = new NetworkCredential(Properties.Settings.Default.KenanProxyUsername, Properties.Settings.Default.KenanProxyPassword);
                    proxy.Headers.Add("content-type", "text/xml");
                    //Logger.Info(String.Format(" getNewCustomerInfo Request : {0} ", xmlReqs));
                    Logger.DebugFormat("SubscriberHistoryWS.getNewCustomerInfoRequest.Request  : {0} ", xmlReqs);
                    xmlResp = proxy.UploadString(postUri, xmlReqs);
                    xmlResp = Util.RemoveAllNamespaces(xmlResp);
                        
                    //proxy.Headers.Add("content-type", "text/xml");
                    //xmlResp = Util.RemoveAllNamespaces(replyDummyData);
                    //Logger.DebugFormat("SubscriberHistoryWS.getNewCustomerInfoRequest.Request  : {0} ", xmlReqs);
                    //Logger.DebugFormat("SubscriberHistoryWS.getNewCustomerInfoRequest.Response : {0} ", xmlResp);

                    XDocument doc = XDocument.Parse(xmlResp);

                    foreach (var r in doc.Descendants("ctosDdmfCheckResponse").Descendants("ctosDdmfCheckRespDetails"))
                    {
                        foreach (var r1 in r.Descendants("individualResponse"))
                        {
                           // var individualResponseXML = new IndividualResponse();
                           // var individualEnq = new IndividualEnq();
                           /* foreach (var result in r1.Descendants("individualEnq"))
                            {
                                var individualEnqXML = new IndividualEnq();
                                individualEnqXML.enqIcNo = result.Element("enqIcNo") == null ? "" : result.Element("enqIcNo").Value.ToString();
                                individualEnqXML.enqNewIc = result.Element("enqNewIc") == null ? "" : result.Element("enqNewIc").Value.ToString();
                                individualEnqXML.enqName = result.Element("enqName") == null ? "" : result.Element("enqName").Value.ToString();
                                individualEnqXML.enqStatusCode = result.Element("enqStatusCode") == null ? "" : result.Element("enqStatusCode").Value.ToString();
                                individualEnqXML.enqStatusDesc = result.Element("enqStatusDesc") == null ? "" : result.Element("enqStatusDesc").Value.ToString();
                            } */
                            
                            foreach (var result in r1.Descendants("individualResults"))
                            {
                                filteredResponse.individualResponse.Add(new IndividualResponse
                                {
                                    dfIcNo = result.Element("dfIcNo") == null ? "" : result.Element("dfIcNo").Value.ToString(),
                                    dfNewIc = result.Element("dfNewIc") == null ? "" : result.Element("dfNewIc").Value.ToString(),
                                    dfAccount = result.Element("dfAccount") == null ? "" : result.Element("dfAccount").Value.ToString(),
                                    Company = result.Element("company") == null ? "" : result.Element("company").Value.ToString(),
                                    branch = result.Element("branch") == null ? "" : result.Element("branch").Value.ToString(),
                                    submitDate = result.Element("submitDate") == null ? "" : result.Element("submitDate").Value.ToString(),
                                    entryDate = result.Element("entryDate") == null ? "" : result.Element("entryDate").Value.ToString(),
                                    userId = result.Element("userId") == null ? "" : result.Element("userId").Value.ToString(),
                                    lastEnqCom = result.Element("lastEnqCom") == null ? "" : result.Element("lastEnqCom").Value.ToString(),
                                    lastEnqDate = result.Element("lastEnqDate") == null ? "" : result.Element("lastEnqDate").Value.ToString(),
                                    state = result.Element("state") == null ? "" : result.Element("state").Value.ToString(),
                                    remarks = result.Element("remarks") == null ? "" : result.Element("remarks").Value.ToString(),
                                    matchType = result.Element("matchType") == null ? "" : result.Element("matchType").Value.ToString()
                                    //filteredResponse.individualResults.Add(individualResultsXML);
                                    //filteredResponse.individualResponse.Add(individualResultsXML);
                                });
                            }
                        }

                        /* foreach (var r1 in doc.Descendants("businessResponse"))
                         {
                            // var businessResponseXML = new BusinessResponse();
                            // businessResponseXML.businessEnq = r1.Element("businessEnq") == null ? "" : r1.Element("businessEnq").Value.ToString();
                            //// filteredResponse.businessResponse.Add(businessResponseXML);
                             foreach (var result in doc.Descendants("businessEnq"))
                             {
                                 var businessEnqXML = new BusinessEnq();
                                 businessEnqXML.enqBrNo = result.Element("enqBrNo") == null ? "" : result.Element("enqBrNo").Value.ToString();
                                 businessEnqXML.enqName = result.Element("enqName") == null ? "" : result.Element("enqName").Value.ToString();
                                 businessEnqXML.enqStatusCode = result.Element("enqStatusCode") == null ? "" : result.Element("enqStatusCode").Value.ToString();
                                 businessEnqXML.enqStatusDesc = result.Element("enqStatusDesc") == null ? "" : result.Element("enqStatusDesc").Value.ToString();
                             }
                             foreach (var result in doc.Descendants("businessResults"))
                             {
                                 var businessResultsXML = new BusinessResponse();
                                 businessResultsXML.dfBrNo = result.Element("dfBrNo") == null ? "" : result.Element("dfBrNo").Value.ToString();
                                 businessResultsXML.dbName = result.Element("dbName") == null ? "" : result.Element("dbName").Value.ToString();
                                 businessResultsXML.dfAccount = result.Element("dfAccount") == null ? "" : result.Element("dfAccount").Value.ToString();
                                 businessResultsXML.company = result.Element("company") == null ? "" : result.Element("company").Value.ToString();
                                 businessResultsXML.branch = result.Element("branch") == null ? "" : result.Element("branch").Value.ToString();
                                 businessResultsXML.submitDate = result.Element("submitDate") == null ? "" : result.Element("submitDate").Value.ToString();
                                 businessResultsXML.entryDate = result.Element("entryDate") == null ? "" : result.Element("entryDate").Value.ToString();
                                 businessResultsXML.userId = result.Element("userId") == null ? "" : result.Element("userId").Value.ToString();
                                 businessResultsXML.lastEnqCom = result.Element("lastEnqCom") == null ? "" : result.Element("lastEnqCom").Value.ToString();
                                 businessResultsXML.lastEnqDate = result.Element("lastEnqDate") == null ? "" : result.Element("lastEnqDate").Value.ToString();
                                 businessResultsXML.state = result.Element("state") == null ? "" : result.Element("state").Value.ToString();
                                 businessResultsXML.remarks = result.Element("remarks") == null ? "" : result.Element("remarks").Value.ToString();
                                 businessResultsXML.matchType = result.Element("matchType") == null ? "" : result.Element("matchType").Value.ToString();
                                 filteredResponse.businessResponse.Add(businessResultsXML);
                             }
                         }  */
                        
                        msgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                        msgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();

                        if (msgCode == "0")
                        {
                            //filteredResponse.Code = msgCode; ;                            
                            //msgDesc = filteredResponse.Message;
                            filteredResponse.Success = true;
                            Logger.InfoFormat("SubscriberHistoryWS.Subscriber details Found with msgCode: {0}", msgCode);
                        }
                        else
                        {
                            Logger.InfoFormat("SubscriberHistoryWS. not found");
                            filteredResponse.Success = false;
                        } 
                    }
                    
                }
            }
            catch (Exception ex)
            {
                filteredResponse.Success = false;
                Logger.Error("SubscriberHistoryWS.Exception getNewCustomerInfo: " + Util.LogException(ex));
            }

            if (request.enqIcNo != "")
            {
                Logger.InfoFormat("SubscriberHistoryWS.Existing getNewCustomerInfo: {0}, response: {1})", request.enqIcNo.ToString2(), filteredResponse.Success);
            }
            else
            {
                Logger.InfoFormat("SubscriberHistoryWS.Existing getNewCustomerInfo: {0}, response: {1})", request.enqNewIc.ToString2(), filteredResponse.Success);
            }
           
            return filteredResponse;
        }

        private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
        {
            resp.Code = "-1";
            resp.Message = ex.Message;
            resp.Success = false;
        }


        private bool MxsDDMFCheck(string inputXML, GetNewCustomerInfoResponse response)
        {
            Logger.Info(string.Format("Entering MxsDDMFCheck():{0}", inputXML));

            var resultCode = "";
            var isBlacklisted = false;
            var resp = "";

            try
            {
                using (var proxy = new MxsDDMFCheckClient())
                {
                    resp = proxy.mxsDDMFCheck(inputXML);

                    // resp = @"<MxsDDMFCheckResp><ExternalIdValue>123</ExternalIdValue><ExternalIdType>10</ExternalIdType><ResultCode>-1</ResultCode><ResultDesc>Subscriber is a defaulter of other service providers</ResultDesc></MxsDDMFCheckResp>";

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(resp);
                    resultCode = xmlDoc.GetElementsByTagName("ResultCode")[0].InnerText;

                    if (!(resultCode == Properties.Settings.Default.DDMFSuccessCode))
                    {
                        isBlacklisted = true;

                        var desc = xmlDoc.GetElementsByTagName("ResultDesc")[0].InnerText;
                        //response.msgDesc = string.Format("External Check Result: {0} <br />", desc);
                    }
                    else
                    {
                        //response.msgDesc = "External Check OK. <br />";
                    }

                    //  <?xml version="1.0" encoding="UTF-8"?>
                    //  <MxsDDMFCheckResp xmlns="http://www.maxis.com.my/mxs/service/mxsDDMFCheckOutput"><ExternalIdValue>60122752558</ExternalIdValue><ExternalIdType>9</ExternalIdType><ResultCode>00</ResultCode></MxsDDMFCheckResp>
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                throw ex;
            }
            Logger.Info(string.Format("Existing mxsDDMFCheck(ResultResp: {0}, IsBlacklisted: {1})", resp, isBlacklisted));

            return isBlacklisted;
        }

    }
}
