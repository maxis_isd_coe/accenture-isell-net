﻿using Online.Registration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.SRService;

namespace Online.Registration.Web.ServiceProxy
{
    public class SRServiceProxy:ServiceProxyBase<SRServiceClient>,ISRService
    {
        public string LastErrorMessage { get; set; }

        public SRServiceProxy()
        {
            _client = new SRServiceClient();
        }

        #region Private Methods

        private void LogResponse(string code, string message, string stackTrace = "")
        {
            LastErrorMessage = "";

            if (!Util.IsSuccessCode(code))
            {
                LastErrorMessage = message;
            }
        }

        private void LogResponse(WCFResponse response)
        {
            LogResponse(response.Code, response.Message, response.StackTrace);
        }

        #endregion

        #region Public Methods

        public bool IsLastCallFailed()
        {
            return !string.IsNullOrEmpty(LastErrorMessage);
        }

        #endregion

        public SRServiceResp SaveOrder(SRSaveOrderReq request)
        {
            try
            {
                return _client.SaveOrder(request);
            }
            catch(Exception ex)
            {
                return new SRServiceResp() {
                    Code = "0",
                    Message = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }

        public SRServiceResp FindOrderById(SRFindOrderReq request)
        {
            try
            {
                return _client.FindOrderById(request);
            }
            catch (Exception ex)
            {
                return new SRServiceResp()
                {
                    Code = "0",
                    Message = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }

        public SRServiceResp SaveDocs(SRSaveDocsReq request)
        {
            try
            {
                return _client.SaveDocs(request);
            }
            catch (Exception ex)
            {
                return new SRServiceResp()
                {
                    Code = "0",
                    Message = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }


        public SRServiceResp ChangeOrderLock(SRChangeLockReq request)
        {
            try
            {
                return _client.ChangeOrderLock(request);
            }
            catch (Exception ex)
            {
                return new SRServiceResp()
                {
                    Code = "0",
                    Message = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }


        public SRServiceResp UpdateOrderStatus(SRUpdateStatusReq request)
        {
            try
            {
                return _client.UpdateOrderStatus(request);
            }
            catch (Exception ex)
            {
                return new SRServiceResp()
                {
                    Code = "0",
                    Message = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }

        public SRServiceResp GetDistinctCreateByList(SRGetCreateByReq request)
        {
            try
            {
                return _client.GetDistinctCreateByList(request);
            }
            catch (Exception ex)
            {
                return new SRServiceResp()
                {
                    Code = "0",
                    Message = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }


        public SRQueryOrderResp QueryOrder(SRQueryOrderReq request)
        {
            try
            {
                return _client.QueryOrder(request);
            }
            catch (Exception ex)
            {
                return new SRQueryOrderResp()
                {
                    Code = "0",
                    Message = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }
        }
    }
}