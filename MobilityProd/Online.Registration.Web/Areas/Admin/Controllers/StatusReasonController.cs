﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.ConfigSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class StatusReasonController : Controller
    {
        ConfigServiceProxy proxy = new ConfigServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        
        public ActionResult Index()
        {
            return GetStatusReasonResult(new StatusReason { Code = string.Empty, Name = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelStatusReason SVMStatusReason)
        {
            try
            {
                return GetStatusReasonResult(SVMStatusReason, true);
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        private IEnumerable<StatusReasonSearchResults> GetStatusReasons(StatusReason StatusReason, bool IsSearch, bool? Active = null)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            StatusReasonFind StatusReasonFind = new StatusReasonFind()
            {
                StatusReason = StatusReason,
                Active = Active,
                PageSize = IsSearch ? 0 : PageSize
            };

            var IDs = proxy.StatusReasonFind(StatusReasonFind);
            var StatusReasons = proxy.StatusReasonGet(IDs);

            var results = StatusReasons.OrderByDescending(a => a.ID).Select(a => new StatusReasonSearchResults()
            {
                StatusReasonID = a.ID,
                Code = a.Code,
                Name = a.Name,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;

        }
        private ViewResult GetStatusReasonResult(StatusReason StatusReason, bool? Active, bool IsSearch)
        {
            SearchViewModelStatusReason SVMStatusReason = new SearchViewModelStatusReason
            {
                SearchResults = GetStatusReasons(StatusReason, IsSearch, Active)
            };

            return View(SVMStatusReason);
        }
        private ViewResult GetStatusReasonResult(SearchViewModelStatusReason SVMStatusReason, bool IsSearch)
        {
            var SearchStatusReasonData = new StatusReason()
            {
                Code = SVMStatusReason.SearchCriterias.Code,
                Name = SVMStatusReason.SearchCriterias.Name
            };
            SVMStatusReason.SearchResults = GetStatusReasons(SearchStatusReasonData, IsSearch);
            return View(SVMStatusReason);
        }
        
        public ActionResult Create()
        {
            return View(new StatusReason());
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, StatusReason addSRData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    var StatusReasonGet = proxy.StatusReasonGet(proxy.StatusReasonFind(new StatusReasonFind()
                    {
                        StatusReason = new StatusReason { Code = addSRData.Code },
                        Active = true
                    }));

                    if (StatusReasonGet.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Status Reason Code already exists.");
                        return View();
                    }

                    addSRData.CreateDT = DateTime.Now;
                    addSRData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addSRData.Active = true;

                    StatusReasonCreateResp oRp = new StatusReasonCreateResp();
                    oRp = proxy.StatusReasonCreate(addSRData);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Status Reason Code ({0}) already exist!", addSRData.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new Status Reason: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        
        public ActionResult Edit(int id)
        {
            UpdateViewStatusReasonModel uvStatusReasonCode = new UpdateViewStatusReasonModel() { StatusReasonCode = new List<StatusReasonCodeSearchResults>() };
            var statusReason = proxy.StatusReasonGet(new int[] { id }).SingleOrDefault();

            uvStatusReasonCode.StatusReason = statusReason;
            uvStatusReasonCode.StatusReasonCode = GetStatusReasonCode(id);

            return View(uvStatusReasonCode);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, UpdateViewStatusReasonModel editSRData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    if (editSRData.StatusReason.Active == true)
                    {
                        var getData = proxy.StatusReasonGet(proxy.StatusReasonFind(new StatusReasonFind()
                        {
                            StatusReason = new StatusReason { Code = editSRData.StatusReason.Code },
                            Active = true
                        })).ToList();
                        var resultID = getData.Select(a => a.ID).SingleOrDefault();

                        if (getData.Count() > 0 && resultID != editSRData.StatusReason.ID)
                        {
                            ModelState.AddModelError(string.Empty, "Status Reason already exists.");
                            var statusReasonData = proxy.StatusReasonGet(new int[] { editSRData.StatusReason.ID }).SingleOrDefault();
                            UpdateViewStatusReasonModel model = new UpdateViewStatusReasonModel() 
                            {
                                StatusReasonCode = GetStatusReasonCode(editSRData.StatusReason.ID),
                                StatusReason = statusReasonData
                            };
                            return View(model);
                        }
                    }
                    editSRData.StatusReason.LastUpdateDT = DateTime.Now;
                    editSRData.StatusReason.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.StatusReasonUpdate(editSRData.StatusReason);
                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        private List<StatusReasonCodeSearchResults> GetStatusReasonCode(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<StatusReasonCodeSearchResults> results = new List<StatusReasonCodeSearchResults>();

            StatusReasonCodeFind statusReasonCodeFind = new StatusReasonCodeFind()
            {
                StatusReasonCode = new StatusReasonCode()
                {
                    ReasonCodeID = id
                },
                PageSize = PageSize
            };

            var statusReasonCodeIDs = proxy.StatusReasonCodeFind(statusReasonCodeFind);
            var statusReasonCodes = proxy.StatusReasonCodeGet(statusReasonCodeIDs);
            if (statusReasonCodes == null)
                return new List<StatusReasonCodeSearchResults>();

            results = statusReasonCodes.OrderByDescending(a => a.ID).Select(a => new StatusReasonCodeSearchResults()
            {
                StatusReasonCodeID = a.ID,
                Status = a.StatusID != 0 ? Util.GetNameByID(RefType.Status, a.StatusID).DisplayValue() : "N/A",
                //ReasonCode = a.ReasonCodeID != 0 ? Util.GetNameByID(RefType.StatusReason, a.ReasonCodeID) : "N/A",
                //Remark = a.Remark != null ? a.Remark : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = RefType.StatusReason
            }).ToList();

            return results;
        }
    }
}
