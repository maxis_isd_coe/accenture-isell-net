﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.CatalogSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class BrandController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }

        public ActionResult Index()
        {
            return GetBrandResult(new Brand { Code = string.Empty, Name = string.Empty }, null, false);
        }

        [HttpPost]
        public ActionResult Index(SearchViewModelBrand SVMBrand)
        {
            try
            {
                return GetBrandResult(SVMBrand, true);
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

        private IEnumerable<BrandSearchResults> GetBrands(Brand BrandData, bool IsSearch, bool? Active = null)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            BrandFind BrandFind = new BrandFind()
            {
                Brand = BrandData,
                Active = Active,
                PageSize = IsSearch ? 0 : PageSize
            };

            var IDs = proxy.BrandFind(BrandFind);
            var Brands = proxy.BrandGet(IDs);

            var results = Brands.OrderByDescending(a => a.ID).Select(a => new BrandSearchResults()
            {
                BrandID = a.ID,
                Code = a.Code,
                Name = a.Name,
                Description = a.Description,
                Category = a.CategoryID != 0 ? Util.GetNameByID(RefType.Category, a.CategoryID).DisplayValue() : "N/A",
                ImagePath = a.ImagePath,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });
            return results;

        }

        private ViewResult GetBrandResult(Brand Brand, bool? Active, bool IsSearch)
        {
            SearchViewModelBrand SVMBrand = new SearchViewModelBrand
            {
                SearchResults = GetBrands(Brand, IsSearch, Active)
            };

            return View(SVMBrand);
        }

        private ViewResult GetBrandResult(SearchViewModelBrand SVMBrand, bool IsSearch)
        {
            var SearchBrandData = new Brand()
            {
                Code = SVMBrand.SearchCriterias.Code,
                Name = SVMBrand.SearchCriterias.Name
            };
            SVMBrand.SearchResults = GetBrands(SearchBrandData, IsSearch);
            return View(SVMBrand);
        }

        public ActionResult Create()
        {
            return View(new Brand());
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection, Brand BrandData)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    // Check for duplicate code
                    var BrandGet = proxy.BrandGet(proxy.BrandFind(new BrandFind()
                    {
                        Brand = new Brand { Code = BrandData.Code },
                        Active = true
                    }));

                    if (BrandGet.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Brand Code already exists.");
                        return View();
                    }

                    BrandData.Active = true;
                    BrandData.CreateDT = DateTime.Now;
                    BrandData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    BrandCreateResp oRp = new BrandCreateResp();
                    oRp = proxy.BrandCreate(BrandData);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Brand Code ({0}) already exists!", BrandData.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Failed to add new Brand: " + oRp.Message);
                        }
                        return View();
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

        public ActionResult Edit(int id)
        {
            var Data = proxy.BrandGet(new int[] { id }).SingleOrDefault();
            return View(Data);
        }

        [HttpPost]
        public ActionResult Edit(FormCollection collection, Brand BData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    if (BData.Active == true)
                    {
                        // Check for duplicate code
                        var BrandGet = proxy.BrandGet(proxy.BrandFind(new BrandFind()
                        {
                            Brand = new Brand { Code = BData.Code },
                            Active = true
                        })).ToList();
                        var resultID = BrandGet.Select(a => a.ID).SingleOrDefault();

                        if (BrandGet.Count() > 0 && resultID != BData.ID)
                        {
                            var Data = proxy.BrandGet(new int[] { BData.ID }).SingleOrDefault();

                            ModelState.AddModelError(string.Empty, "Brand Code already exists.");
                            return View(Data);
                        }
                    }

                    BData.LastUpdateDT = DateTime.Now;
                    BData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.BrandUpdate(BData);
                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

    }
}
