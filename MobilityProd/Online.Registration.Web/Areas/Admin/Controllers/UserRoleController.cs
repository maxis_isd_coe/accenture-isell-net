﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.UserSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class UserRoleController : Controller
    {
        UserServiceProxy proxy = new UserServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetUserRoleResult(new UserRole() { Code = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelUserRole svUserRole)
        {
            try
            {
                return GetUserRoleResult(svUserRole, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private IEnumerable<UserRoleSearchResults> GetUserRoles(UserRole userRole, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            UserRoleFind UserRoleFind = new UserRoleFind()
            {
                UserRole = userRole,
                Active = active,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.UserRoleFind(userRole, active);
            var userRoles = proxy.UserRoleGet(IDs);

            var results = userRoles.OrderByDescending(a => a.ID).Select(a => new UserRoleSearchResults()
            {
                UserRoleID = a.ID,
                Code = a.Code,
                Description = a.Description,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;
        }
        private ViewResult GetUserRoleResult(UserRole userRole, bool? Active, bool isSearch)
        {
            SearchViewModelUserRole viewUserRoles = new SearchViewModelUserRole()
            {
                SearchResults = GetUserRoles(userRole, isSearch, Active)
            };

            return View(viewUserRoles);
        }
        private ViewResult GetUserRoleResult(SearchViewModelUserRole svUserRole, bool isSearch)
        {
            var sUserRole = new UserRole()
            {
                Code = svUserRole.SearchCriterias.Code
            };

            svUserRole.SearchResults = GetUserRoles(sUserRole, isSearch);
            return View(svUserRole);
        }
        public ActionResult Edit(int id)
        {
            UpdateViewUserRoleModel uvUserRole = new UpdateViewUserRoleModel() { UserGroupRole = new List<UserGroupRoleSearchResults>() };
            var userRole = proxy.UserRoleGet(new int[] { id }).SingleOrDefault();

            uvUserRole.UserRole = userRole;
            uvUserRole.UserGroupRole = GetUserGroupRole(id);

            return View(uvUserRole);
        }
        [HttpPost]
        public ActionResult Edit(UpdateViewUserRoleModel editUserRole, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // check for duplicate
                    if (editUserRole.UserRole.Active == true)
                    {
                        var userRoles = proxy.UserRoleGet(proxy.UserRoleFind(new UserRole()
                        {
                            Code = editUserRole.UserRole.Code,
                            Active = true
                        }, true)).ToList();

                        var resultCode = userRoles.ToList().Select(a => a.ID).SingleOrDefault();

                        if (userRoles.Count() > 0 && resultCode != editUserRole.UserRole.ID)
                        {
                            var userRole = proxy.UserRoleGet(new int[] { editUserRole.UserRole.ID }).SingleOrDefault();
                            UpdateViewUserRoleModel uvUserRole = new UpdateViewUserRoleModel() 
                            { 
                                UserRole = userRole,
                                UserGroupRole = GetUserGroupRole(editUserRole.UserRole.ID)
                            };

                            ModelState.AddModelError(string.Empty, "User Role Code already exists.");
                            return View(uvUserRole);
                        }
                    }

                    editUserRole.UserRole.LastUpdateDT = DateTime.Now;
                    editUserRole.UserRole.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.UserRoleUpdate(editUserRole.UserRole);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private List<UserGroupRoleSearchResults> GetUserGroupRole(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<UserGroupRoleSearchResults> results = new List<UserGroupRoleSearchResults>();

            UserGrpRoleFind userGrpRoleFind = new UserGrpRoleFind()
            {
                UserGrpRole = new UserGrpRole()
                {
                    UserRoleID = id
                },
                PageSize = PageSize
            };

            var userGrpRoleIDs = proxy.UserGroupRoleFind(userGrpRoleFind);
            var userGrpRoles = proxy.UserGroupRoleGet(userGrpRoleIDs);
            if (userGrpRoles == null)
                return new List<UserGroupRoleSearchResults>();

            results = userGrpRoles.OrderByDescending(a => a.ID).Select(a => new UserGroupRoleSearchResults()
            {
                UserGroupRoleID = a.ID,
                UserGroup = a.UserGrpID != 0 ? Util.GetNameByID(RefType.UserGroup, a.UserGrpID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = RefType.StatusReason
            }).ToList();

            return results;
        }
        public ActionResult Create()
        {
            return View(new UserRole());
        }
        [HttpPost]
        public ActionResult Create(UserRole addUserRole, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    var userRole = proxy.UserRoleGet(proxy.UserRoleFind(new UserRole()
                    {
                        //UserRole = new UserRole() { Code = addUserRole.Code },
                        Code = addUserRole.Code
                        
                    }, true));

                    if (userRole.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "User Role Code already exists.");
                        return View();
                    }
                    
                    addUserRole.CreateDT = DateTime.Now;
                    addUserRole.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addUserRole.Active = true;

                    UserRoleCreateResp oRp = new UserRoleCreateResp();
                    oRp = proxy.UserRoleCreate(addUserRole);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("User Role Code ({0}) already exist!", addUserRole.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new User Role: " + oRp.Message);
                        }
                        return View();
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        } 
    }
}
