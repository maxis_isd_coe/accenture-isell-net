﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.Helper;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class AdminHomeController : Controller
    {
        //
        // GET: /Admin/AdminHome/

        public ActionResult Error(string errCode, string type = "Admin")
        {
            ViewBag.Type = type;
            ViewBag.ErrorCode = errCode;

            return View();
        }

    }
}
