﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.ConfigSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class AccountCategoryController : Controller
    {
        ConfigServiceProxy proxy = new ConfigServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetAccountCategoryResult(new AccountCategory { Name = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelAccountCategory SVMAccountCategory)
        {
            try
            {
                return GetAccountCategoryResult(SVMAccountCategory, true);
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        private IEnumerable<AccountCategorySearchResults> GetAccountCategories(AccountCategory AccountCategory, bool IsSearch, bool? Active = null)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            AccountCategoryFind AccountCategoryFind = new AccountCategoryFind()
            {
                AccountCategory = AccountCategory,
                Active = Active,
                PageSize = IsSearch ? 0 : PageSize
            };

            var IDs = proxy.AccountCategoryFind(AccountCategoryFind);
            var AccountCategories = proxy.AccountCategoryGet(IDs);
            if(AccountCategories == null)
                return new List<AccountCategorySearchResults>();

            var results = AccountCategories.OrderByDescending(a => a.ID).Select(a => new AccountCategorySearchResults()
            {
                AccountCategoryID = a.ID,
                Name = a.Name,
                Description = a.Description,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;

        }
        private ViewResult GetAccountCategoryResult(AccountCategory AccountCategory, bool? Active, bool IsSearch)
        {
            SearchViewModelAccountCategory SVMAccountCategory = new SearchViewModelAccountCategory
            {
                SearchResults = GetAccountCategories(AccountCategory, IsSearch, Active)
            };

            return View(SVMAccountCategory);
        }
        private ViewResult GetAccountCategoryResult(SearchViewModelAccountCategory SVMAccountCategory, bool IsSearch)
        {
            var SearchAccountCategoryData = new AccountCategory()
            {
                Name = SVMAccountCategory.SearchCriterias.Name
            };
            SVMAccountCategory.SearchResults = GetAccountCategories(SearchAccountCategoryData, IsSearch);
            return View(SVMAccountCategory);
        }
        public ActionResult Create()
        {
            return View(new AccountCategory());
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, AccountCategory addACData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    addACData.CreateDT = DateTime.Now;
                    addACData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addACData.Active = true;

                    proxy.AccountCategoryCreate(addACData);
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        public ActionResult Edit(int id)
        {
            var Data = proxy.AccountCategoryGet(new int[] { id }).SingleOrDefault();
            return View(Data);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, AccountCategory ACData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    ACData.LastUpdateDT = DateTime.Now;
                    ACData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.AccountCategoryUpdate(ACData);
                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

    }
}
