﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.ConfigSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class StatusController : Controller
    {
        ConfigServiceProxy proxy = new ConfigServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetStatusResult(new Status() { Code = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelStatus svStatus)
        {
            try
            {
                return GetStatusResult(svStatus, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private IEnumerable<StatusSearchResults> GetStatuses(Status status, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            StatusFind statusFind = new StatusFind()
            {
                Status = status,
                Active = active,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.StatusFind(statusFind);
            var statuses = proxy.StatusGet(IDs);

            var results = statuses.OrderByDescending(a => a.ID).Select(a => new StatusSearchResults()
            {
                StatusID = a.ID,
                Code = a.Code,
                Description = a.Description,
                StatusType = a.StatusTypeID != 0 ? Util.GetNameByID(RefType.StatusType, a.StatusTypeID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;
        }
        private ViewResult GetStatusResult(Status status, bool? Active, bool isSearch)
        {
            SearchViewModelStatus viewStatuses = new SearchViewModelStatus()
            {
                SearchResults = GetStatuses(status, isSearch, Active)
            };

            return View(viewStatuses);
        }
        private ViewResult GetStatusResult(SearchViewModelStatus svStatus, bool isSearch)
        {
            var sStatus = new Status()
            {
                Code = svStatus.SearchCriterias.Code,
                StatusTypeID = svStatus.SearchCriterias.StatusTypeID
            };

            svStatus.SearchResults = GetStatuses(sStatus, isSearch);
            return View(svStatus);
        }

        public ActionResult Create()
        {
            return View(new Status());
        }
        [HttpPost]
        public ActionResult Create(Status addStatus, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                        var status = proxy.StatusGet(proxy.StatusFind(new StatusFind()
                        {
                            Status = new Status() { Code = addStatus.Code },
                            Active = true
                        }));

                        if (status.Count() > 0)
                        {
                            ModelState.AddModelError(string.Empty, "Status Code already exists.");
                            return View();
                        }
                    
                    addStatus.CreateDT = DateTime.Now;
                    addStatus.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addStatus.Active = true;

                    StatusCreateResp oRp = new StatusCreateResp();
                    oRp = proxy.StatusCreate(addStatus);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Status Code ({0}) already exist!", addStatus.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new status: " + oRp.Message);
                        }
                        return View();
                    }

                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        public ActionResult Edit(int id)
        {
            UpdateViewStatusModel uvStatusCode = new UpdateViewStatusModel() { StatusReasonCode = new List<StatusReasonCodeSearchResults>() };
            var status = proxy.StatusGet(new int[] { id }).SingleOrDefault();

            uvStatusCode.Status = status;
            uvStatusCode.StatusReasonCode = GetStatusReasonCode(id);
            uvStatusCode.StatusChange = GetStatusChange(id);

            return View(uvStatusCode);
        }
        [HttpPost]
        public ActionResult Edit(UpdateViewStatusModel editStatus, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    // check for duplicate
                    if(editStatus.Status.Active == true)
                    {
                        var status = proxy.StatusGet(proxy.StatusFind(new StatusFind()
                        {
                            Status = new Status() { Code = editStatus.Status.Code },
                            Active = true
                        })).ToList();

                        var resultCode = status.ToList().Select(a => a.ID).SingleOrDefault();

                        if (status.Count() > 0 && resultCode != editStatus.Status.ID)
                        {
                            ModelState.AddModelError(string.Empty, "Status Code already exists.");
                            var statusData = proxy.StatusGet(new int[] { editStatus.Status.ID }).SingleOrDefault();
                            UpdateViewStatusModel model = new UpdateViewStatusModel() 
                            {
                                StatusReasonCode = GetStatusReasonCode(editStatus.Status.ID),
                                StatusChange = GetStatusChange(editStatus.Status.ID),
                                Status = statusData
                            };
                            return View(model);
                        }
                    }

                    editStatus.Status.LastUpdateDT = DateTime.Now;
                    editStatus.Status.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.StatusUpdate(editStatus.Status);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private List<StatusReasonCodeSearchResults> GetStatusReasonCode(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<StatusReasonCodeSearchResults> results = new List<StatusReasonCodeSearchResults>();

            StatusReasonCodeFind statusReasonCodeFind = new StatusReasonCodeFind()
            {
                StatusReasonCode = new StatusReasonCode()
                {
                    StatusID = id
                },
                PageSize = PageSize
            };

            var statusReasonCodeIDs = proxy.StatusReasonCodeFind(statusReasonCodeFind);
            var statusReasonCodes = proxy.StatusReasonCodeGet(statusReasonCodeIDs);

            if (statusReasonCodes == null)
                return new List<StatusReasonCodeSearchResults>();

            results = statusReasonCodes.OrderByDescending(a => a.ID).Select(a => new StatusReasonCodeSearchResults()
            {
                StatusReasonCodeID = a.ID,
                //Status = a.StatusID != 0 ? Util.GetNameByID(RefType.Status, a.StatusID).DisplayValue() : "N/A",
                ReasonCode = a.ReasonCodeID != 0 ? Util.GetNameByID(RefType.StatusReason, a.ReasonCodeID) : "N/A",
                //Remark = a.Remark != null ? a.Remark : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = RefType.Status
            }).ToList();

            return results;
        }
        private List<StatusChangeSearchResults> GetStatusChange(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<StatusChangeSearchResults> results = new List<StatusChangeSearchResults>();

            StatusChangeFind statusChangeFind = new StatusChangeFind()
            {
                FromToStatusID = id,
                PageSize = PageSize
            };

            var statusChangeIDs = proxy.StatusChangeFind(statusChangeFind);
            var statusChanges = proxy.StatusChangeGet(statusChangeIDs);
            if(statusChanges == null)
                return new List<StatusChangeSearchResults>();

            results = statusChanges.OrderByDescending(a => a.ID).Select(a => new StatusChangeSearchResults()
            {
                StatusChangeID = a.ID,
                FromStatus = Util.GetNameByID(RefType.Status, a.FromStatusID),
                ToStatus = Util.GetNameByID(RefType.Status, a.ToStatusID),
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = id == a.FromStatusID ? "fromStatus" : "toStatus"
            }).ToList();

            return results;
        }

    }
}
