﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.Properties;
using Online.Registration.Web.ContentRepository;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class PackageController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        //protected override void Initialize(RequestContext requestContext)
        //{
        //    Util.CheckSessionAccess(requestContext);
        //    base.Initialize(requestContext);
        //}
        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index()
        {
            return GetPackageResult(new Package()
            {
                Code = string.Empty,
                Name = string.Empty,
                PackageTypeID = 0
            }, null, false);
        }
        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index(SearchViewPackageModel svPackage)
        {
            try
            {
                var sPackage = new Package()
                {
                    Code = svPackage.SearchCriterias.Code,
                    Name = svPackage.SearchCriterias.Name,
                    PackageTypeID = svPackage.SearchCriterias.PackageTypeID
                };
                svPackage.SearchResults = GetPackages(sPackage, true, null);
                return View(svPackage);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        private ViewResult GetPackageResult(Package package, bool? Active, bool isSearch)
        {
            SearchViewPackageModel viewModels = new SearchViewPackageModel()
            {
                SearchResults = GetPackages(package, isSearch, Active),
            };
            return View(viewModels);

        }
        private IEnumerable<PackageSearchResults> GetPackages(Package package, bool isSearch, bool? Active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            PackageFind packageFind = new PackageFind()
            {
                Active = Active,
                Package = package,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.PackageFind(packageFind);
            var packages = proxy.PackageGet(IDs);

            var results = packages.OrderByDescending(a => a.ID).Select(a => new PackageSearchResults()
            {
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                Code = a.Code,
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastAccessID = a.LastAccessID,
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT).DisplayValue(),
                Name = a.Name,
                PackageID = a.ID,
                PackageTypeID = a.PackageTypeID,
                PackageType = Util.GetNameByID(RefType.PackageType, a.PackageTypeID).DisplayValue(),
            });
            return results;
        }
        
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Create()
        {
            return View(new Package());
        }
        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Create(Package addPackage, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for code duplicate code
                    var packages = proxy.PackageGet(proxy.PackageFind(new PackageFind()
                    {
                        Package = new Package() { Code = addPackage.Code },
                        Active = true
                    })).ToList();

                    if (packages.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Package Code already exists.");
                        return View();
                    }

                    addPackage.Active = true;
                    addPackage.CreateDT = DateTime.Now;
                    addPackage.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    
                    PackageCreateResp oRp = new PackageCreateResp();
                    oRp = proxy.PackageCreate(addPackage);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Package Code ({0}) already exist!", addPackage.Code));
                        }
                        else
                        { 
                            ModelState.AddModelError(string.Empty, "Fail to add new package: " + oRp.Message); 
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(int id)
        {
            UpdateViewPackageModel uvPM = new UpdateViewPackageModel() { PgmBdlPckComponents = new List<PgmBdlPckComponentSearchResults>() };
            var package = proxy.PackageGet(new int[] { id }).SingleOrDefault();

            uvPM.Package = package;
            uvPM.PgmBdlPckComponents = GetPackageComponent(id);
            uvPM.BdlPkg = GetBundlePackage(id);
            
            return View(uvPM);
        }
        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(UpdateViewPackageModel editPackage, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    if (editPackage.Package.Active)
                    {
                        // check for duplicate code
                        var programs = proxy.ProgramGet(proxy.ProgramFind(new ProgramFind()
                        {
                            Program = new Program() { Code = editPackage.Package.Code },
                            Active = true
                        })).ToList();

                        var resultID = programs.ToList().Select(a => a.ID).SingleOrDefault();

                        if (programs.Count() > 0 && resultID != editPackage.Package.ID)
                        {
                            var Data = proxy.PackageGet(new int[] { editPackage.Package.ID }).SingleOrDefault();
                            editPackage.Package = Data;
                            ModelState.AddModelError(string.Empty, "Package Code already exists.");
                            return View(editPackage);
                        }
                    }

                    editPackage.Package.LastUpdateDT = DateTime.Now;
                    editPackage.Package.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    
                    proxy.PackageUpdate(editPackage.Package);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        private List<PgmBdlPckComponentSearchResults> GetBundlePackage(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<PgmBdlPckComponentSearchResults> results = new List<PgmBdlPckComponentSearchResults>();

            PgmBdlPckComponentFind pgmBdlPckComponentFind = new PgmBdlPckComponentFind()
            {
                PgmBdlPckComponent = new PgmBdlPckComponent()
                {
                    ChildID = id,
                    LinkType = Settings.Default.Bundle_Package
                },
                PageSize = PageSize
            };

            var pgmBdlPckComponentIDs = proxy.PgmBdlPckComponentFind(pgmBdlPckComponentFind);
            var pgmBdlPckComponentCodes = MasterDataCache.Instance.FilterComponents(pgmBdlPckComponentIDs);

            results = pgmBdlPckComponentCodes.OrderByDescending(a => a.ID).Select(a => new PgmBdlPckComponentSearchResults()
            {
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                Bundle = a.ParentID != 0 ? Util.GetNameByID(RefType.Bundle, a.ParentID).DisplayValue() : "N/A",
                CreateDT = Util.FormatDateTime(a.CreateDT),
                EndDate = Util.FormatDateTime(a.EndDate),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                PgmBdlPckComponentID = a.ID,
                StartDate = Util.FormatDateTime(a.StartDate),
                Type = RefType.Package
            }).ToList();

            return results;
        }

        private List<PgmBdlPckComponentSearchResults> GetPackageComponent(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<PgmBdlPckComponentSearchResults> results = new List<PgmBdlPckComponentSearchResults>();

            PgmBdlPckComponentFind pgmBdlPckComponentFind = new PgmBdlPckComponentFind()
            {
                PgmBdlPckComponent = new PgmBdlPckComponent()
                {
                    ParentID = id,
                    LinkType = Settings.Default.Package_Component
                },
                PageSize = PageSize
            };

            var pgmBdlPckComponentIDs = proxy.PgmBdlPckComponentFind(pgmBdlPckComponentFind);
            var pgmBdlPckComponentCodes = MasterDataCache.Instance.FilterComponents(pgmBdlPckComponentIDs);

            results = pgmBdlPckComponentCodes.OrderByDescending(a => a.ID).Select(a => new PgmBdlPckComponentSearchResults()
            {
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                Component = a.ChildID != 0 ? Util.GetNameByID(RefType.Component, a.ChildID).DisplayValue() : "N/A",
                CreateDT = Util.FormatDateTime(a.CreateDT),
                EndDate = Util.FormatDateTime(a.EndDate),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                PgmBdlPckComponentID = a.ID,
                StartDate = Util.FormatDateTime(a.StartDate),
                Type = RefType.Package
            }).ToList();

            return results;
        }
    }
}
