﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.Properties;
using Online.Registration.Web.ContentRepository;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class PgmBdlPkgCompController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();

        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        public ActionResult AddPgmBdl(int? programID, int? bundleID)
        {
            PgmBdlPkgComponentViewModel model = new PgmBdlPkgComponentViewModel()
            {
                ProgramID = programID.ToInt(),
                BundleID = bundleID.ToInt(),
                PgmBdlPckComponent = new PgmBdlPckComponent()
            };

            return View(model);
        }
        [HttpPost]
        public ActionResult AddPgmBdl(FormCollection collection, PgmBdlPkgComponentViewModel addPgmBdl)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int parentID = addPgmBdl.ProgramID == 0 ? addPgmBdl.PgmBdlPckComponent.ParentID : addPgmBdl.ProgramID;
                int childID = addPgmBdl.BundleID == 0 ? addPgmBdl.PgmBdlPckComponent.ChildID : addPgmBdl.BundleID;
                RefType type = addPgmBdl.Type;
                
                if (submit == "Save")
                {
                    PgmBdlPkgComponentViewModel model = new PgmBdlPkgComponentViewModel()
                    {
                        ProgramID = addPgmBdl.ProgramID,
                        BundleID = addPgmBdl.BundleID,
                    };

                    // Validate the start and end date
                    if (addPgmBdl.PgmBdlPckComponent.StartDate >= addPgmBdl.PgmBdlPckComponent.EndDate)
                    {
                        ModelState.AddModelError(string.Empty, "Start Date must be earlier than End Date");
                        return View(model);
                    }

                    // Check for code duplicate
                    var pgmBdls = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent() { Code = addPgmBdl.PgmBdlPckComponent.Code },
                        Active = true
                    })).ToList();

                    if (pgmBdls.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Program Bundle Code already exists.");
                        return View(model);
                    }

                    // check for parent and child duplicate
                    var getData = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                        {
                            PgmBdlPckComponent = new PgmBdlPckComponent 
                            { 
                                ParentID = parentID, 
                                ChildID = childID,
                                LinkType = Settings.Default.Program_Bundle
                            },
                            Active = true
                        }));

                    if (getData.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Combination of the Program and Bundle already exists.");
                        return View(model);
                    }
                    
                    if (addPgmBdl.ProgramID != 0)
                        addPgmBdl.PgmBdlPckComponent.ParentID = addPgmBdl.ProgramID;

                    if (addPgmBdl.BundleID != 0)
                        addPgmBdl.PgmBdlPckComponent.ChildID = addPgmBdl.BundleID;

                    addPgmBdl.PgmBdlPckComponent.Active = true;
                    addPgmBdl.PgmBdlPckComponent.CreateDT = DateTime.Now;
                    addPgmBdl.PgmBdlPckComponent.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.PgmBdlPckComponentCreate(addPgmBdl.PgmBdlPckComponent);
                }

                if (type == RefType.Program)
                {
                    return RedirectToAction("Edit", "Program", new { id = parentID });
                }
                else if (type == RefType.Bundle)
                {
                    return RedirectToAction("Edit", "Bundle", new { id = childID });
                }

                return View();
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        public ActionResult EditPgmBdl(int id, RefType type)
        {
            var data = MasterDataCache.Instance.FilterComponents(new int[] { id }).SingleOrDefault();
            PgmBdlPkgComponentViewModel editPgmBdlPkgComponentCode = new PgmBdlPkgComponentViewModel()
            {
                Type = type,
                PgmBdlPckComponent = data
            };

            return View(editPgmBdlPkgComponentCode);
        }
        [HttpPost]
        public ActionResult EditPgmBdl(FormCollection collection, PgmBdlPkgComponentViewModel editPgmBdl)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int parentID = editPgmBdl.PgmBdlPckComponent.ParentID.ToInt();
                int childID = editPgmBdl.PgmBdlPckComponent.ChildID.ToInt();
                RefType type = editPgmBdl.Type;
                var resultID = 0;

                if (submit == "Save")
                {
                    if (editPgmBdl.PgmBdlPckComponent.Active == true)
                    {
                        // Check for duplicate
                        var getData = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                        {
                            PgmBdlPckComponent = new PgmBdlPckComponent
                            {
                                ParentID = editPgmBdl.PgmBdlPckComponent.ParentID,
                                ChildID = editPgmBdl.PgmBdlPckComponent.ChildID
                            },
                            Active = true
                        })).ToList();

                        if (getData.Count() > 0)
                            resultID = getData.SingleOrDefault().ID;

                        if (getData.Count() > 0 && resultID != editPgmBdl.PgmBdlPckComponent.ID)
                        {
                            var Data = MasterDataCache.Instance.FilterComponents(new int[] { editPgmBdl.PgmBdlPckComponent.ID }).SingleOrDefault();
                            editPgmBdl.PgmBdlPckComponent = Data;
                            ModelState.AddModelError(string.Empty, "Combination of the Parent and Child Code already exist.");
                            return View(editPgmBdl);
                        }

                        // Validate the start and end date
                        if (editPgmBdl.PgmBdlPckComponent.StartDate >= editPgmBdl.PgmBdlPckComponent.EndDate)
                        {
                            var Data = MasterDataCache.Instance.FilterComponents(new int[] { editPgmBdl.PgmBdlPckComponent.ID }).SingleOrDefault();
                            editPgmBdl.PgmBdlPckComponent = Data;
                            ModelState.AddModelError(string.Empty, "Start Date must be earlier than End Date");
                            return View(editPgmBdl);
                        }

                        // Check for code duplicate Code
                        var pkgComp = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                        {
                            PgmBdlPckComponent = new PgmBdlPckComponent() 
                            { 
                                Code = editPgmBdl.PgmBdlPckComponent.Code 
                            },
                            Active = true
                        })).ToList();
                        
                        var resultCode = pkgComp.ToList().Select(a => a.ID).SingleOrDefault();

                        if (pkgComp.Count() > 0 && resultCode != editPgmBdl.PgmBdlPckComponent.ID)
                        {
                            var Data = MasterDataCache.Instance.FilterComponents(new int[] { editPgmBdl.PgmBdlPckComponent.ID }).SingleOrDefault();
                            editPgmBdl.PgmBdlPckComponent = Data;
                            ModelState.AddModelError(string.Empty, "Code already exists.");
                            return View(editPgmBdl);
                        }
                    }

                    editPgmBdl.PgmBdlPckComponent.LastUpdateDT = DateTime.Now;
                    editPgmBdl.PgmBdlPckComponent.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.PgmBdlPckComponentUpdate(editPgmBdl.PgmBdlPckComponent);
                }

                if (type == RefType.Program)
                {
                    return RedirectToAction("Edit", "Program", new { id = parentID });
                } 
                else if(type == RefType.Bundle)
                {
                    return RedirectToAction("Edit", "Bundle", new { id = childID });
                }

                return View();

            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        public ActionResult AddBdlPkg(int? bundleID, int? packageID)
        {
            PgmBdlPkgComponentViewModel model = new PgmBdlPkgComponentViewModel()
            {
                BundleID = bundleID.ToInt(),
                PackageID = packageID.ToInt(),
                PgmBdlPckComponent = new PgmBdlPckComponent()
            };

            return View(model);
        }
        [HttpPost]
        public ActionResult AddBdlPkg(FormCollection collection, PgmBdlPkgComponentViewModel addBdlPkg)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int parentID = addBdlPkg.BundleID == 0 ? addBdlPkg.PgmBdlPckComponent.ParentID : addBdlPkg.BundleID;
                int childID = addBdlPkg.PackageID == 0 ? addBdlPkg.PgmBdlPckComponent.ChildID : addBdlPkg.PackageID;
                RefType type = addBdlPkg.Type;

                if (submit == "Save")
                {
                    // Check for code duplicate
                    var pgmBdls = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent() { Code = addBdlPkg.PgmBdlPckComponent.Code },
                        Active = true
                    })).ToList();

                    PgmBdlPkgComponentViewModel model = new PgmBdlPkgComponentViewModel()
                    {
                        BundleID = addBdlPkg.BundleID,
                        PackageID = addBdlPkg.PackageID,
                    };

                    if (pgmBdls.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Code already exists.");
                        return View(model);
                    }

                    // Validate the start and end date
                    if (addBdlPkg.PgmBdlPckComponent.StartDate >= addBdlPkg.PgmBdlPckComponent.EndDate)
                    {
                        ModelState.AddModelError(string.Empty, "Start Date must be earlier than End Date");
                        return View(model);
                    }

                    // check for parent and child duplicate
                    var getData = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent 
                        { 
                            ParentID = parentID, 
                            ChildID = childID, 
                            LinkType = Settings.Default.Bundle_Package 
                        },
                        Active = true
                    }));

                    if (getData.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Combination of the Bundle and Package already exists.");
                        return View(model);
                    }
                    
                    if (addBdlPkg.BundleID != 0)
                        addBdlPkg.PgmBdlPckComponent.ParentID = addBdlPkg.BundleID;

                    if (addBdlPkg.PackageID != 0)
                        addBdlPkg.PgmBdlPckComponent.ChildID = addBdlPkg.PackageID;

                    addBdlPkg.PgmBdlPckComponent.Active = true;
                    addBdlPkg.PgmBdlPckComponent.CreateDT = DateTime.Now;
                    addBdlPkg.PgmBdlPckComponent.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.PgmBdlPckComponentCreate(addBdlPkg.PgmBdlPckComponent);
                }

                if (type == RefType.Bundle)
                {
                    return RedirectToAction("Edit", "Bundle", new { id = parentID });
                }
                else if (type == RefType.Package)
                {
                    return RedirectToAction("Edit", "Package", new { id = childID });
                }

                return View();
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        public ActionResult EditBdlPkg(int id, RefType type)
        {
            EditViewPgmBdlPckComponentModel evBdlPkg = new EditViewPgmBdlPckComponentModel { ItemPrices = new List<ItemPriceSearchResults>() };

            var data = MasterDataCache.Instance.FilterComponents(new int[] { id }).SingleOrDefault();
            PgmBdlPkgComponentViewModel editPgmBdlPkgComponentCode = new PgmBdlPkgComponentViewModel()
            {
                Type = type,
                PgmBdlPckComponent = data
            };

            evBdlPkg.PgmBdlPckComponent = editPgmBdlPkgComponentCode;
            evBdlPkg.ItemPrices = getBdlPkgItemPrices(id);

            return View(evBdlPkg);
        }
        private List<ItemPriceSearchResults> getBdlPkgItemPrices(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<ItemPriceSearchResults> results = new List<ItemPriceSearchResults>();

            ItemPriceFind itemPriceFind = new ItemPriceFind()
            {
                ItemPrice = new ItemPrice()
                {
                    BdlPkgID = id
                },
                PageSize = PageSize
            };

            var itemPriceIDs = proxy.ItemPriceFind(itemPriceFind);
            if (itemPriceIDs.Count() > 0)
            {
                var itemPriceCodes = proxy.ItemPriceGet(itemPriceIDs);

                if (itemPriceCodes != null)
                {
                    results = itemPriceCodes.OrderByDescending(a => a.ID).Select(a => new ItemPriceSearchResults()
                    {
                        ActiveCheckBox = Util.GetCheckBox(a.Active),
                        CreateDT = Util.FormatDateTime(a.CreateDT),
                        ItemPriceID = a.ID,
                        LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                        LastAccessID = a.LastAccessID,
                        Model = a.ModelID != 0 ? Util.GetNameByID(RefType.Model, a.ModelID) : "N/A",
                        PkgCompID = a.PkgCompID != 0 ? Util.GetNameByID(RefType.PgmBdlPkgComp, a.PkgCompID) : "N/A",
                        Type = Properties.Settings.Default.Bundle_Package
                    }).ToList();
                }
            }

            return results;
        }
        [HttpPost]
        public ActionResult EditBdlPkg(FormCollection collection, EditViewPgmBdlPckComponentModel editBdlPkg)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int parentID = editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.ParentID.ToInt();
                int childID = editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.ChildID.ToInt();
                RefType type = editBdlPkg.Type;
                var resultID = 0;

                if (submit == "Save")
                {
                    if (editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.Active == true)
                    {
                        // Check for duplicate Parent ID and Child ID
                        var getData = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                        {
                            PgmBdlPckComponent = new PgmBdlPckComponent
                            {
                                ParentID = editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.ParentID,
                                ChildID = editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.ChildID
                            },
                            Active = true
                        })).ToList();

                        if (getData.Count() > 0)
                            resultID = getData.SingleOrDefault().ID;

                        if (getData.Count() > 0 && resultID != editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.ID)
                        {
                            var Data = MasterDataCache.Instance.FilterComponents(new int[] { editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.ID }).SingleOrDefault();
                            editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent = Data;
                            ModelState.AddModelError(string.Empty, "Combination of the Bundle and Package already exists.");
                            return View(editBdlPkg);
                        }

                        // Validate the start and end date
                        if (editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.StartDate >= editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.EndDate)
                        {
                            var Data = MasterDataCache.Instance.FilterComponents(new int[] { editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.ID }).SingleOrDefault();
                            editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent = Data;
                            ModelState.AddModelError(string.Empty, "Start Date must be earlier than End Date");
                            return View(editBdlPkg);
                        }

                        // Check for duplicate Code
                        var pkgComp = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                        {
                            PgmBdlPckComponent = new PgmBdlPckComponent() { Code = editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.Code },
                            Active = true
                        })).ToList();

                        var resultCode = pkgComp.ToList().Select(a => a.ID).SingleOrDefault();

                        if (pkgComp.Count() > 0 && resultCode != editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.ID)
                        {
                            var Data = MasterDataCache.Instance.FilterComponents(new int[] { editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.ID }).SingleOrDefault();
                            editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent = Data;
                            ModelState.AddModelError(string.Empty, "Code already exists.");
                            return View(editBdlPkg);
                        }
                    }

                    editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.LastUpdateDT = DateTime.Now;
                    editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.PgmBdlPckComponentUpdate(editBdlPkg.PgmBdlPckComponent.PgmBdlPckComponent);
                }

                if (type == RefType.Bundle)
                {
                    return RedirectToAction("Edit", "Bundle", new { id = parentID });
                }
                else if(type == RefType.Package)
                {
                    return RedirectToAction("Edit", "Package", new { id = childID });
                }

                return View();

            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        public ActionResult AddPkgComp(int? packageID, int? componentID)
        {
            PgmBdlPkgComponentViewModel model = new PgmBdlPkgComponentViewModel()
            {
                PackageID = packageID.ToInt(),
                ComponentID = componentID.ToInt(),
                PgmBdlPckComponent = new PgmBdlPckComponent()
            };

            return View(model);
        }
        [HttpPost]
        public ActionResult AddPkgComp(FormCollection collection, PgmBdlPkgComponentViewModel addPkgComp)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int parentID = addPkgComp.PackageID == 0 ? addPkgComp.PgmBdlPckComponent.ParentID : addPkgComp.PackageID;
                int childID = addPkgComp.ComponentID == 0 ? addPkgComp.PgmBdlPckComponent.ChildID : addPkgComp.ComponentID;
                RefType type = addPkgComp.Type;

                if (submit == "Save")
                {
                    // Check for code duplicate
                    var pgmBdls = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent() { Code = addPkgComp.PgmBdlPckComponent.Code },
                        Active = true
                    })).ToList();

                    if (pgmBdls.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Package Component Code already exists.");

                        PgmBdlPkgComponentViewModel model = new PgmBdlPkgComponentViewModel()
                        {
                            PackageID = addPkgComp.PackageID,
                            ComponentID = addPkgComp.ComponentID,
                        };

                        return View(model);
                    }

                    // check for parent and child duplicate
                    var getData = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent 
                        { 
                            ParentID = parentID, 
                            ChildID = childID,
                            LinkType = Settings.Default.Package_Component 
                        },
                        Active = true
                    }));

                    if (getData.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Combination of Package and Component already exists.");

                        PgmBdlPkgComponentViewModel model = new PgmBdlPkgComponentViewModel()
                        {
                            PackageID = addPkgComp.PackageID,
                            ComponentID = addPkgComp.ComponentID,
                        };

                        return View(model);
                    }

                    // Validate the start and end date
                    if (addPkgComp.PgmBdlPckComponent.StartDate >= addPkgComp.PgmBdlPckComponent.EndDate)
                    {
                        var Data = MasterDataCache.Instance.FilterComponents(new int[] { addPkgComp.PgmBdlPckComponent.ID }).SingleOrDefault();
                        addPkgComp.PgmBdlPckComponent = Data;
                        ModelState.AddModelError(string.Empty, "Start Date must be earlier than End Date");
                        return View(addPkgComp);
                    }
                    
                    if (addPkgComp.PackageID != 0)
                        addPkgComp.PgmBdlPckComponent.ParentID = addPkgComp.PackageID;

                    if (addPkgComp.ComponentID != 0)
                        addPkgComp.PgmBdlPckComponent.ChildID = addPkgComp.ComponentID;

                    addPkgComp.PgmBdlPckComponent.Active = true;
                    addPkgComp.PgmBdlPckComponent.CreateDT = DateTime.Now;
                    addPkgComp.PgmBdlPckComponent.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.PgmBdlPckComponentCreate(addPkgComp.PgmBdlPckComponent);
                }

                if (type == RefType.Package)
                {
                    return RedirectToAction("Edit", "Package", new { id = parentID });
                }
                else if (type == RefType.Component)
                {
                    return RedirectToAction("Edit", "Component", new { id = childID });
                }

                return View();
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        public ActionResult EditPkgComp(int id, RefType type)
        {
            EditViewPgmBdlPckComponentModel evBdlPkg = new EditViewPgmBdlPckComponentModel { ItemPrices = new List<ItemPriceSearchResults>() };

            var data = MasterDataCache.Instance.FilterComponents(new int[] { id }).SingleOrDefault();
            PgmBdlPkgComponentViewModel editPgmBdlPkgComponentCode = new PgmBdlPkgComponentViewModel()
            {
                Type = type,
                PgmBdlPckComponent = data
            };

            evBdlPkg.PgmBdlPckComponent = editPgmBdlPkgComponentCode;
            evBdlPkg.ItemPrices = getPkgCompItemPrices(id);

            return View(evBdlPkg);
        }
        [HttpPost]
        public ActionResult EditPkgComp(FormCollection collection, EditViewPgmBdlPckComponentModel editPkgComp)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int parentID = editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.ParentID.ToInt();
                int childID = editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.ChildID.ToInt();
                RefType type = editPkgComp.Type;
                var resultID = 0;

                if (submit == "Save")
                {
                    if (editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.Active == true)
                    {
                        // Check for duplicate Parent ID and Child ID
                        var getData = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                        {
                            PgmBdlPckComponent = new PgmBdlPckComponent
                            {
                                ParentID = editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.ParentID,
                                ChildID = editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.ChildID
                            },
                            Active = true
                        })).ToList();

                        if (getData.Count() > 0)
                            resultID = getData.SingleOrDefault().ID;

                        if (getData.Count() > 0 && resultID != editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.ID)
                        {
                            var Data = MasterDataCache.Instance.FilterComponents(new int[] { editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.ID }).SingleOrDefault();
                            editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent = Data;
                            ModelState.AddModelError(string.Empty, "Combination of the Parent and Child Code already exist.");
                            return View(editPkgComp);
                        }

                        // Check for code duplicate Code
                        var pkgComp = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                        {
                            PgmBdlPckComponent = new PgmBdlPckComponent() { Code = editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.Code },
                            Active = true
                        })).ToList();

                        if (pkgComp.Count() > 0)
                        {
                            var Data = MasterDataCache.Instance.FilterComponents(new int[] { editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.ID }).SingleOrDefault();
                            editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent = Data;
                            ModelState.AddModelError(string.Empty, "Code already exists.");
                            return View(editPkgComp);
                        }

                        // Validate the start and end date
                        if (editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.StartDate >= editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.EndDate)
                        {
                            var Data = MasterDataCache.Instance.FilterComponents(new int[] { editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.ID }).SingleOrDefault();
                            editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent = Data;
                            ModelState.AddModelError(string.Empty, "Start Date must be earlier than End Date");
                            return View(editPkgComp);
                        }
                    }

                    editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.LastUpdateDT = DateTime.Now;
                    editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.PgmBdlPckComponentUpdate(editPkgComp.PgmBdlPckComponent.PgmBdlPckComponent);
                }

                if (type == RefType.Package)
                {
                    return RedirectToAction("Edit", "Package", new { id = parentID });
                }
                else if (type == RefType.Component)
                {
                    return RedirectToAction("Edit", "Component", new { id = childID });
                }

                return View();
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        private List<ItemPriceSearchResults> getPkgCompItemPrices(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<ItemPriceSearchResults> results = new List<ItemPriceSearchResults>();

            ItemPriceFind itemPriceFind = new ItemPriceFind()
            {
                ItemPrice = new ItemPrice()
                {
                    PkgCompID = id
                },
                PageSize = PageSize
            };

            var itemPriceIDs = proxy.ItemPriceFind(itemPriceFind);
            var itemPriceCodes = proxy.ItemPriceGet(itemPriceIDs);

            if (itemPriceCodes != null)
            {
                results = itemPriceCodes.OrderByDescending(a => a.ID).Select(a => new ItemPriceSearchResults()
                {
                    ActiveCheckBox = Util.GetCheckBox(a.Active),
                    CreateDT = Util.FormatDateTime(a.CreateDT),
                    ItemPriceID = a.ID,
                    LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                    LastAccessID = a.LastAccessID,
                    Model = a.ModelID != 0 ? Util.GetNameByID(RefType.Model, a.ModelID) : "N/A",
                    BdlPkgID = a.BdlPkgID != 0 ? Util.GetNameByID(RefType.PgmBdlPkgComp, a.BdlPkgID) : "N/A",
                    Type = Properties.Settings.Default.Package_Component
                }).ToList();
            }

            return results;
        }
    }
}
