﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ColorController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }

        public ActionResult Index()
        {
            return GetColourResult(new Colour() { Code = string.Empty, Name = string.Empty, ModelID = 0 }, null, false);
        }

        [HttpPost]
        public ActionResult Index(SearchViewModelColour svColour)
        {
            try
            {
                return GetColourResult(svColour, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        private IEnumerable<ColourSearchResults> GetColours(Colour colour, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            ColourFind colourFind = new ColourFind()
            {
                Active = active,
                Colour = colour,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.ColourFind(colourFind);
            var colours = proxy.ColourGet(IDs);

            var results = colours.OrderByDescending(a => a.ID).Select(a => new ColourSearchResults()
            {
                
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                ColourID = a.ID,
                Code = a.Code,
                CreateDT = Util.FormatDateTime(a.CreateDT),
                Description = a.Description,
                LastAccessID = a.LastAccessID,
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                Model = a.ModelID != 0 ? Util.GetNameByID(RefType.Model, a.ModelID).DisplayValue() : "N/A",
                Name = a.Name,
            });
            return results;
        }

        private ViewResult GetColourResult(Colour colour, bool? Active, bool isSearch)
        {
            SearchViewModelColour viewColours = new SearchViewModelColour()
            {
                SearchResults = GetColours(colour, isSearch, Active)
            };
            return View(viewColours);

        }

        private ViewResult GetColourResult(SearchViewModelColour svColour, bool isSearch)
        {
            var sColour = new Colour()
            {
                Code = svColour.SearchCriterias.Code,
                Name = svColour.SearchCriterias.Name,
                ModelID = svColour.SearchCriterias.ModelID
            };

            svColour.SearchResults = GetColours(sColour, isSearch);
            return View(svColour);
        }

        public ActionResult Create()
        {
            return View(new Colour());
        }

        [HttpPost]
        public ActionResult Create(Colour addColour, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    var colours = proxy.ColourGet(proxy.ColourFind(new ColourFind()
                    {
                        Colour = new Colour() { Code = addColour.Code },
                        Active = true
                    })).ToList();

                    if (colours.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Colour Code already exists.");
                        return View();
                    }
                    
                    // Add new colour
                    addColour.Active = true;
                    addColour.CreateDT = DateTime.Now;
                    addColour.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    ColourCreateResp oRp = new ColourCreateResp();
                    oRp = proxy.ColourCreate(addColour);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Colour Code ({0}) already exists!", addColour.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Failed to add new Colour: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        public ActionResult Edit(int id)
        {
            UpdateViewColourModel uvModel = new UpdateViewColourModel() { ModelImageSearchResults = new List<ModelImageSearchResults>() };
            var colour = proxy.ColourGet(new int[] { id }).SingleOrDefault();
            
            uvModel.Colour = colour;
            uvModel.ModelImageSearchResults = GetModelImages(id);
            
            return View(uvModel);
        }
        [HttpPost]
        public ActionResult Edit(UpdateViewColourModel editColour, FormCollection collection)
        {
            try
            {
                string submit = collection["Submit"].ToString2();
                if (submit == "Save")
                {
                    if (editColour.Colour.Active == true)
                    {
                        // check for duplicate
                        var colours = proxy.ColourGet(proxy.ColourFind(new ColourFind()
                        {
                            Colour = new Colour() { Code = editColour.Colour.Code },
                            Active = true
                        })).ToList();

                        var resultCode = colours.ToList().Select(a => a.ID).SingleOrDefault();

                        if (colours.Count() > 0 && resultCode != editColour.Colour.ID)
                        {
                            var colour = proxy.ColourGet(new int[] { editColour.Colour.ID }).SingleOrDefault();
                            UpdateViewColourModel uvModel = new UpdateViewColourModel()
                            {
                                Colour = colour,
                                ModelImageSearchResults = GetModelImages(editColour.Colour.ID)
                            };
                            ModelState.AddModelError(string.Empty, "Colour Code already exists.");
                            return View(uvModel);
                        }
                    }

                    editColour.Colour.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    editColour.Colour.LastUpdateDT = DateTime.Now;

                    proxy.ColourUpdate(editColour.Colour);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private List<ModelImageSearchResults> GetModelImages(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            ModelImageFind modelImageFind = new ModelImageFind()
            {
                ModelImage = new ModelImage()
                {
                    ColourID = id
                },
                Active = null
            };

            var colourIDs = proxy.ModelImageFind(modelImageFind);
            var colourData = proxy.ModelImageGet(colourIDs);
            if (colourData == null)
                return new List<ModelImageSearchResults>();

            var results = colourData.OrderByDescending(a => a.ID).Select(a => new ModelImageSearchResults()
            {
                ModelImageID = a.ID,
                Model = a.ModelID != 0 ? Util.GetNameByID(RefType.Model, a.ModelID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = RefType.Colour
            }).ToList();
            return results;
        }
        [GridAction]
        public ActionResult ModelImagePaging(int id)
        {
            var data = GetModelImages(id);
            return View(new GridModel<ModelImageSearchResults>() { Data = data });
        }
    }
}
