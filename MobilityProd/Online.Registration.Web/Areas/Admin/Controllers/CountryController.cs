﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class CountryController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin"});
        }

        public ActionResult Index()
        {
            return GetCountryResult(new Country { Name = string.Empty }, null, false);
        }
 
        [HttpPost]
        public ActionResult Index(SearchViewModelCountry svCountry)
        {
            try
            {
                return GetCountryResult(svCountry, true);
            }
            catch(Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        private IEnumerable<CountrySearchResults> GetCountries(Country country, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            CountryFind countryFind = new CountryFind()
            {
                Active = active,
                Country = country,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.CountryFind(countryFind);
            var countries = proxy.CountryGet(IDs);

            var results = countries.OrderByDescending(a => a.ID).Select(a => new CountrySearchResults()
            {
                CountryID = a.ID,
                Code = a.Code,
                Name = a.Name,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;
        }

        private ViewResult GetCountryResult(Country country, bool? Active, bool isSearch) 
        {
            SearchViewModelCountry viewModels = new SearchViewModelCountry
            {
                SearchResults = GetCountries(country, isSearch, Active)
            };
            return View(viewModels);
        }

        private ViewResult GetCountryResult(SearchViewModelCountry svCountry, bool isSearch)
        {
            var sCountry = new Country()
            {
                Code = svCountry.SearchCriterias.Code,
                Name = svCountry.SearchCriterias.Name
            };

            svCountry.SearchResults = GetCountries(sCountry, isSearch);
            return View(svCountry);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Country addCountry, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                
                if (submit == "Save")
                {
                    // Check for duplicate code
                    var country = proxy.CountryGet(proxy.CountryFind(new CountryFind()
                    {
                        Country = new Country() { Code = addCountry.Code },
                        Active = true
                    }));

                    if (country.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Country Code already exists.");
                        return View();
                    }
                    
                    // Add country
                    addCountry.CreateDT = DateTime.Now;
                    addCountry.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addCountry.Active = true;

                    CountryCreateResp oRp = new CountryCreateResp();
                    oRp = proxy.CountryCreate(addCountry);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Country Code ({0}) already exist!", addCountry.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new Country: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        public ActionResult Edit(int id)
        {
            var country = proxy.CountryGet(new int[] { id }).SingleOrDefault();
            return View(country);
        }

        [HttpPost]
        public ActionResult Edit(Country editCountry, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    if (editCountry.Active == true)
                    {
                        // check for duplicate code
                        var countries = proxy.CountryGet(proxy.CountryFind(new CountryFind()
                        {
                            Country = new Country() { Code = editCountry.Code },
                            Active = true
                        })).ToList();

                        var resultCode = countries.ToList().Select(a => a.ID).SingleOrDefault();

                        if (countries.Count() > 0 && resultCode != editCountry.ID)
                        {
                            var country = proxy.CountryGet(new int[] { editCountry.ID }).SingleOrDefault();

                            ModelState.AddModelError(string.Empty, "Country Code cannot be duplicated.");
                            return View(country);
                        }
                    }

                    // Update Country
                    editCountry.LastUpdateDT = DateTime.Now;
                    editCountry.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.CountryUpdate(editCountry);
                }
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return RedirectToException(ex);
            }
        }
    }
}
