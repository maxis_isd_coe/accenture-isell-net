﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class StateController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin"});
        }
        public ActionResult Index()
        {
            return GetStateResult(new State { Code = string.Empty, Name = string.Empty, CountryID = 0 }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelState svState)
        {
            try
            {
                return GetStateResult(svState, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private IEnumerable<StateSearchResults> GetStates(State state, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            StateFind stateFind = new StateFind()
            {
                Active = active,
                CountryState = state,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.StateFind(stateFind);
            var states = proxy.StateGet(IDs);

            var results = states.OrderByDescending(a => a.ID).Select(a => new StateSearchResults()
            {
                StateID = a.ID,
                Code = a.Code,
                Name = a.Name,
                Description = a.Description,
                Country = a.CountryID != 0 ? Util.GetNameByID(RefType.Country, a.CountryID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                KenanStateID = a.KenanStateID,
                VOIPDefaultSearch = a.VOIPDefaultSearch
            });

            return results;
        }
        private ViewResult GetStateResult(State state, bool? Active, bool isSearch)
        {
            SearchViewModelState viewModels = new SearchViewModelState()
            {
                SearchResults = GetStates(state, isSearch, Active)
            };

            return View(viewModels);
        }
        private ViewResult GetStateResult(SearchViewModelState svState, bool isSearch)
        {
            var sState = new State()
            {
                Code = svState.SearchCriterias.Code,
                Name = svState.SearchCriterias.Name,
                CountryID = svState.SearchCriterias.CountryID
            };

            svState.SearchResults = GetStates(sState, isSearch);
            return View(svState);
        }
        public ActionResult Create()
        {
            return View(new State());
        }
        [HttpPost]
        public ActionResult Create(State addState, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    // Check for duplicate code
                    var state = proxy.StateGet(proxy.StateFind(new StateFind()
                    {
                        CountryState = new State() { Code = addState.Code },
                        Active = true
                    }));

                    if (state.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "State Code already exists.");
                        return View();
                    }
                 
                    addState.CreateDT = DateTime.Now;
                    addState.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addState.Active = true;

                    StateCreateResp oRp = new StateCreateResp();
                    oRp = proxy.StateCreate(addState);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("State Code ({0}) already exists!", addState.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Failed to add new State: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        public ActionResult Edit(int id)
        {
            var state = proxy.StateGet(new int[] { id }).SingleOrDefault();
            return View(state);
        }
        [HttpPost]
        public ActionResult Edit(State editState, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    if (editState.Active == true)
                    {
                        // check for duplicate code
                        var states = proxy.StateGet(proxy.StateFind(new StateFind()
                        {
                            CountryState = new State() { Code = editState.Code },
                            Active = true
                        })).ToList();

                        var resultCode = states.ToList().Select(a => a.ID).SingleOrDefault();

                        if (states.Count() > 0 && resultCode != editState.ID)
                        {
                            var state = proxy.StateGet(new int[] { editState.ID }).SingleOrDefault();

                            ModelState.AddModelError(string.Empty, "State Code cannot be duplicated.");
                            return View(state);
                        }
                    }

                    editState.LastUpdateDT = DateTime.Now;
                    editState.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.StateUpdate(editState);
                }
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return RedirectToException(ex);
            }
        }
    }
}
