﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ComponentTypeController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        //protected override void Initialize(RequestContext requestContext)
        //{
        //    Util.CheckSessionAccess(requestContext);
        //    base.Initialize(requestContext);
        //}

        public ActionResult Index()
        {
            return GetComponentTypeResult(new ComponentType()
            {
                Code = string.Empty,
                Name = string.Empty
            }, null, false);
        }

        [HttpPost]
        public ActionResult Index(SearchViewComponentTypeModel svCT)
        {
            try
            {
                return GetComponentTypeResult(svCT, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        [GridAction]
        public ActionResult IndexPaging(string code = "", string name = "")
        {
            var data = GetComponentTypes(new ComponentType() { Code = code, Name = name }, true);
            return View(new GridModel<ComponentTypeSearchResults>() { Data = data });
        }
        private ActionResult GetComponentTypeResult(SearchViewComponentTypeModel svCT, bool isSearch)
        {
            var sCT = new ComponentType()
            {
                Code = svCT.SearchCriterias.Code,
                Name = svCT.SearchCriterias.Name,
            };
            svCT.SearchResults = GetComponentTypes(sCT, isSearch);
            return View(svCT);
        }
        private ActionResult GetComponentTypeResult(ComponentType ct, bool? active, bool isSearch)
        {
            SearchViewComponentTypeModel svCT = new SearchViewComponentTypeModel()
            {
                SearchResults = GetComponentTypes(ct, isSearch, active)
            };
            return View(svCT);
        }
        private IEnumerable<ComponentTypeSearchResults> GetComponentTypes(ComponentType ct, bool isSearch, bool? Active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            ComponentTypeFind compTypeFind = new ComponentTypeFind()
            {
                Active = Active,
                ComponentType = ct,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.ComponentTypeFind(compTypeFind);
            var componentTypes = proxy.ComponentTypeGet(IDs);

            var results = componentTypes.OrderByDescending(a => a.ID).Select(a => new ComponentTypeSearchResults()
            {
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                ComponentTypeID = a.ID,
                Code = a.Code,
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastAccessID = a.LastAccessID,
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                Name = a.Name
            });
            return results;
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ComponentType addComponentType, FormCollection collection)
        {
            string submit = collection["submit"];
            try
            {
                if (submit == "Save")
                {
                    // Check for code duplicate code
                    var componentTypes = proxy.ComponentTypeGet(proxy.ComponentTypeFind(new ComponentTypeFind()
                    {
                        ComponentType = new ComponentType() { Code = addComponentType.Code },
                        Active = true
                    })).ToList();

                    if (componentTypes.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "ComponentType Type Code already exists.");
                        return View();
                    }

                    addComponentType.Active = true;
                    addComponentType.CreateDT = DateTime.Now;
                    addComponentType.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    
                    ComponentTypeCreateResp oRp = new ComponentTypeCreateResp();
                    oRp = proxy.ComponentTypeCreate(addComponentType);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Component Type Code ({0}) already exists!", addComponentType.Code));
                        }
                        else
                        { 
                            ModelState.AddModelError(string.Empty, "Failed to add Component Type: " + oRp.Message); 
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        public ActionResult Edit(int id)
        {
            var componentTypes = proxy.ComponentTypeGet(new int[] { id }).SingleOrDefault();

            return View(componentTypes);
        }

        [HttpPost]
        public ActionResult Edit(ComponentType editComponentType, FormCollection collection)
        {
            string submit = collection["submit"];
            try
            {
                if (submit == "Save")
                {
                    if (editComponentType.Active)
                    {
                        // check for duplicate code
                        var componentTypes = proxy.ComponentTypeGet(proxy.ComponentTypeFind(new ComponentTypeFind()
                        {
                            ComponentType = new ComponentType() { Code = editComponentType.Code },
                            Active = true
                        })).ToList();

                        var resultID = componentTypes.ToList().Select(a => a.ID).SingleOrDefault();

                        if (componentTypes.Count() > 0 && resultID != editComponentType.ID)
                        {
                            var Data = proxy.ComponentTypeGet(new int[] { editComponentType.ID }).SingleOrDefault();
                            ModelState.AddModelError(string.Empty, "Component Type Type Code already exists.");
                            return View(Data);
                        }
                    }

                    editComponentType.LastUpdateDT = DateTime.Now;
                    editComponentType.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.ComponentTypeUpdate(editComponentType);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

    }
}
