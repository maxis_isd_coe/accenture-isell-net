﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.ConfigSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class StatusChangeController : Controller
    {
        ConfigServiceProxy proxy = new ConfigServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        
        //public ActionResult Index()
        //{
        //    return GetStatusChangeResult(new StatusChange { FromStatusID = 0, ToStatusID = 0 }, null, false);
        //}
        //[GridAction]
        //public ActionResult IndexPaging(int FromStatusID = 0, int TOStatusID = 0)
        //{
        //    var ResultData = GetStatusChanges(new StatusChange { FromStatusID = FromStatusID, ToStatusID = TOStatusID }, true);
        //    return View(new GridModel<StatusChangeSearchResults>() { Data = ResultData });
        //}
        //[HttpPost]
        //public ActionResult Index(SearchViewModelStatusChange SVMStatusChange)
        //{
        //    try
        //    {
        //        return GetStatusChangeResult(SVMStatusChange, true);
        //    }
        //    catch (Exception e)
        //    {
        //        return RedirectToException(e);
        //    }
        //}
        //private IEnumerable<StatusChangeSearchResults> GetStatusChanges(StatusChange StatusChange, bool IsSearch, bool? Active = null)
        //{
        //    int PageSize = Properties.Settings.Default.PageSize;

        //    StatusChangeFind StatusChangeFind = new StatusChangeFind()
        //    {
        //        StatusChange = StatusChange,
        //        Active = Active,
        //        PageSize = IsSearch ? 0 : PageSize
        //    };

        //    var IDs = proxy.StatusChangeFind(StatusChangeFind);
        //    var StatusChanges = proxy.StatusChangeGet(IDs);

        //    var results = StatusChanges.OrderByDescending(a => a.ID).Select(a => new StatusChangeSearchResults()
        //    {
        //        StatusChangeID = a.ID,
        //        FromStatus = a.FromStatusID != 0 ? Util.GetNameByID(RefType.Status, a.FromStatusID).DisplayValue(): "N/A",
        //        ToStatus = a.ToStatusID != 0 ? Util.GetNameByID(RefType.Status, a.ToStatusID).DisplayValue() : "N/A",
        //        ActiveCheckBox = Util.GetCheckBox(a.Active),
        //        CreateDT = Util.FormatDateTime(a.CreateDT),
        //        LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
        //        LastAccessID = a.LastAccessID
        //    });

        //    return results;

        //}
        //private ViewResult GetStatusChangeResult(StatusChange StatusChange, bool? Active, bool IsSearch)
        //{
        //    SearchViewModelStatusChange SVMStatusChange = new SearchViewModelStatusChange
        //    {
        //        SearchResults = GetStatusChanges(StatusChange, IsSearch, Active)
        //    };

        //    return View(SVMStatusChange);
        //}
        //private ViewResult GetStatusChangeResult(SearchViewModelStatusChange SVMStatusChange, bool IsSearch)
        //{
        //    var SearchData = new StatusChange()
        //    {
        //        FromStatusID = SVMStatusChange.SearchCriterias.FromStatusID,
        //        ToStatusID = SVMStatusChange.SearchCriterias.ToStatusID
        //    };
        //    SVMStatusChange.SearchResults = GetStatusChanges(SearchData, IsSearch);
        //    return View(SVMStatusChange);
        //}

        public ActionResult Create(string type, int statusID)
        {
            StatusChangeViewModel model = new StatusChangeViewModel()
            {
                FromStatusID = type == "fromStatus" ? statusID : 0,
                ToStatusID = type == "toStatus" ? statusID : 0,
                StatusChange = new StatusChange(),
                Type = type
            };

            return View(model);
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, StatusChangeViewModel addStatusChange)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int fromStatusID = addStatusChange.FromStatusID == 0 ? addStatusChange.StatusChange.FromStatusID : addStatusChange.FromStatusID;
                int toStatusID = addStatusChange.ToStatusID == 0 ? addStatusChange.StatusChange.ToStatusID : addStatusChange.ToStatusID;
                string type = addStatusChange.Type;
                if (submit == "Save")
                {
                    var getData = proxy.StatusChangeGet(proxy.StatusChangeFind(new StatusChangeFind()
                    {
                        Active = true,
                        StatusChange = new StatusChange() { FromStatusID = fromStatusID, ToStatusID = toStatusID}
                    }));
                    if(getData.Count() > 0)
                    {
                        StatusChangeViewModel model = new StatusChangeViewModel()
                        {
                            FromStatusID = addStatusChange.FromStatusID,
                            ToStatusID = addStatusChange.ToStatusID,
                            StatusChange = addStatusChange.StatusChange,
                            Type = addStatusChange.Type
                        };
                        ModelState.AddModelError(string.Empty, "Combination of From and To Status already exist.");
                        return View(model);
                    }
                    if (addStatusChange.FromStatusID != 0)
                        addStatusChange.StatusChange.FromStatusID = addStatusChange.FromStatusID;

                    if (addStatusChange.ToStatusID != 0)
                        addStatusChange.StatusChange.ToStatusID = addStatusChange.ToStatusID;

                    addStatusChange.StatusChange.CreateDT = DateTime.Now;
                    addStatusChange.StatusChange.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addStatusChange.StatusChange.Active = true;

                    proxy.StatusChangeCreate(addStatusChange.StatusChange);
                }

                if (type == "fromStatus")
                {
                    return RedirectToAction("Edit", "Status", new { id = fromStatusID });
                }
                else if (type == "toStatus")
                {
                    return RedirectToAction("Edit", "Status", new { id = toStatusID });
                }
                
                return View();
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

        public ActionResult Edit(int id, string type)
        {
            var data = proxy.StatusChangeGet(new int[] { id }).SingleOrDefault();
            StatusChangeViewModel editStatusReasonCode = new StatusChangeViewModel()
            {
                Type = type,
                StatusChange = data
            };
            return View(editStatusReasonCode);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, StatusChangeViewModel editStatusChange)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int fromStatusID = editStatusChange.StatusChange.FromStatusID;
                int toStatusID = editStatusChange.StatusChange.ToStatusID;
                string type = editStatusChange.Type;
                if (submit == "Save")
                {
                    // Check for duplicat
                    if (editStatusChange.StatusChange.Active == true)
                    {
                        var GetData = proxy.StatusChangeGet(proxy.StatusChangeFind(new StatusChangeFind()
                        {
                            StatusChange = new StatusChange { FromStatusID = fromStatusID, ToStatusID = toStatusID },
                            Active = true
                        })).ToList();
                        var resultID = GetData.Select(a => a.ID).SingleOrDefault();

                        if (GetData.Count() > 0 && resultID != editStatusChange.StatusChange.ID)
                        {
                            var Data = proxy.StatusChangeGet(new int[] { editStatusChange.StatusChange.ID }).SingleOrDefault();

                            ModelState.AddModelError(string.Empty, "Combination of From and To Status already exist.");
                            var data = proxy.StatusChangeGet(new int[] { editStatusChange.StatusChange.ID }).SingleOrDefault();
                            StatusChangeViewModel model = new StatusChangeViewModel()
                            {
                                Type = type,
                                StatusChange = data
                            };
                            return View(model);
                        }
                    }

                    editStatusChange.StatusChange.LastUpdateDT = DateTime.Now;
                    editStatusChange.StatusChange.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.StatusChangeUpdate(editStatusChange.StatusChange);
                }

                if (type == "fromStatus")
                {
                    return RedirectToAction("Edit", "Status", new { id = fromStatusID });
                }
                else if (type == "toStatus")
                {
                    return RedirectToAction("Edit", "Status", new { id = toStatusID });
                }

                return View();
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

    }
}
