﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.Properties;
using Online.Registration.Web.ContentRepository;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class BundleController : Controller
    {
        //protected override void Initialize(RequestContext requestContext)
        //{
        //    Util.CheckSessionAccess(requestContext);
        //    base.Initialize(requestContext);
        //}
        private CatalogServiceProxy proxy = new CatalogServiceProxy();
        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "Home", new { errCode = "xxx", type = "Admin" });
        }
        
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index()
        {
            return GetBundleResult(new Bundle()
            {
                Name = string.Empty,
                Code = string.Empty,
                ProgramID = 0,
            }, false, null);
        }

        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index(SearchViewBundleModel svBundle)
        {
            try
            {
                return GetBundleResult(svBundle, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
      
        private ViewResult GetBundleResult(Bundle bundle, bool isSearch, bool? Active)
        {
            SearchViewBundleModel svBundle = new SearchViewBundleModel()
            {
                SearchResults = GetBundles(bundle, isSearch, Active)
            };
            return View(svBundle);
        }
        private ViewResult GetBundleResult(SearchViewBundleModel svBundle, bool isSearch)
        {
            var sBundle = new Bundle()
            {
                Code = svBundle.SearchCriterias.Code,
                ProgramID = svBundle.SearchCriterias.ProgramID,
                Name = svBundle.SearchCriterias.Name,
            };
            svBundle.SearchResults = GetBundles(sBundle, true);
            return View(svBundle);
        }
        private IEnumerable<BundleSearchResults> GetBundles(Bundle bundle, bool isSearch, bool? Active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            BundleFind bundleFind = new BundleFind()
            {
                Active = Active,
                Bundle = bundle,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.BundleFind(bundleFind);
            var bundles = proxy.BundleGet(IDs);

            var results = bundles.OrderByDescending(a => a.ID).Select(a => new BundleSearchResults()
            {
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                BundleID = a.ID,
                Code = a.Code,
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                Name = a.Name,
                ProgramID = a.ProgramID,
                LastAccessID = a.LastAccessID
            });
            return results;
        }

        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Create()
        {
            return View(new Bundle());
        }
        [HttpPost]
        public ActionResult Create(Bundle addBundle, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for code duplicate code
                    var bundles = proxy.BundleGet(proxy.BundleFind(new BundleFind()
                    {
                        Bundle = new Bundle() { Code = addBundle.Code },
                        Active = true
                    })).ToList();

                    if (bundles.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Bundle Code already exists.");
                        return View();
                    }
                    
                    addBundle.Active = true;
                    addBundle.CreateDT = DateTime.Now;
                    addBundle.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    BundleCreateResp oRp = new BundleCreateResp();
                    oRp = proxy.BundleCreate(addBundle);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Bundle Code ({0}) already exist!", addBundle.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add bundle: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(int id)
        {
            UpdateViewBundleModel uvBundleModel = new UpdateViewBundleModel() { PgmBdlPckComponents = new List<PgmBdlPckComponentSearchResults>() };
            var bundle = proxy.BundleGet(new int[] { id }).SingleOrDefault();

            uvBundleModel.Bundle = bundle;
            uvBundleModel.PgmBdlPckComponents = GetBundlePackage(id);
            uvBundleModel.PgmBdl = GetProgramBundle(id);

            return View(uvBundleModel);
        }
        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(UpdateViewBundleModel editBundle)
        {
            try
            {
                if (editBundle.Bundle.Active)
                {
                    // check for duplicate code
                    var bundles = proxy.BundleGet(proxy.BundleFind(new BundleFind()
                    {
                        Bundle = new Bundle() { Code = editBundle.Bundle.Code },
                        Active = true
                    })).ToList();

                    var resultID = bundles.ToList().Select(a => a.ID).SingleOrDefault();

                    if (bundles.Count() > 0 && resultID != editBundle.Bundle.ID)
                    {
                        var Data = proxy.BundleGet(new int[] { editBundle.Bundle.ID }).SingleOrDefault();
                        editBundle.Bundle = Data;
                        ModelState.AddModelError(string.Empty, "Bundle Code already exists.");
                        return View(editBundle);
                    }
                }
                
                editBundle.Bundle.LastUpdateDT = DateTime.Now;
                editBundle.Bundle.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                
                proxy.BundleUpdate(editBundle.Bundle);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        private List<PgmBdlPckComponentSearchResults> GetProgramBundle(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            
            List<PgmBdlPckComponentSearchResults> results = new List<PgmBdlPckComponentSearchResults>();

            PgmBdlPckComponentFind pgmBdlPckComponentFind = new PgmBdlPckComponentFind()
            {
                PgmBdlPckComponent = new PgmBdlPckComponent()
                {
                    ChildID = id,
                    LinkType = Settings.Default.Program_Bundle
                },
                PageSize = PageSize
            };

            var pgmBdlPckComponentIDs = proxy.PgmBdlPckComponentFind(pgmBdlPckComponentFind);
            if (pgmBdlPckComponentIDs.Count() > 0)
            {
                var pgmBdlPckComponentCodes = MasterDataCache.Instance.FilterComponents(pgmBdlPckComponentIDs);

                results = pgmBdlPckComponentCodes.OrderByDescending(a => a.ID).Select(a => new PgmBdlPckComponentSearchResults()
                {
                    ActiveCheckBox = Util.GetCheckBox(a.Active),
                    Program = a.ParentID != 0 ? Util.GetNameByID(RefType.Program, a.ParentID).DisplayValue() : "N/A",
                    CreateDT = Util.FormatDateTime(a.CreateDT),
                    EndDate = Util.FormatDateTime(a.EndDate),
                    LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                    LastAccessID = a.LastAccessID,
                    PgmBdlPckComponentID = a.ID,
                    StartDate = Util.FormatDateTime(a.StartDate),
                    Type = RefType.Bundle
                }).ToList();
            }

            return results;
        }

        private List<PgmBdlPckComponentSearchResults> GetBundlePackage(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<PgmBdlPckComponentSearchResults> results = new List<PgmBdlPckComponentSearchResults>();

            PgmBdlPckComponentFind pgmBdlPckComponentFind = new PgmBdlPckComponentFind()
            {
                PgmBdlPckComponent = new PgmBdlPckComponent()
                {
                    ParentID = id,
                    LinkType = Settings.Default.Bundle_Package
                },
                PageSize = PageSize
            };

            var pgmBdlPckComponentIDs = proxy.PgmBdlPckComponentFind(pgmBdlPckComponentFind);

            if (pgmBdlPckComponentIDs.Count()>0)
            {
                var pgmBdlPckComponentCodes = MasterDataCache.Instance.FilterComponents(pgmBdlPckComponentIDs);

                results = pgmBdlPckComponentCodes.OrderByDescending(a => a.ID).Select(a => new PgmBdlPckComponentSearchResults()
                {
                    ActiveCheckBox = Util.GetCheckBox(a.Active),
                    Package = a.ChildID != 0 ? Util.GetNameByID(RefType.Package, a.ChildID).DisplayValue() : "N/A",
                    CreateDT = Util.FormatDateTime(a.CreateDT),
                    EndDate = Util.FormatDateTime(a.EndDate),
                    LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                    LastAccessID = a.LastAccessID,
                    PgmBdlPckComponentID = a.ID,
                    StartDate = Util.FormatDateTime(a.StartDate),
                    Type = RefType.Bundle
                }).ToList();
            }

            return results;
        }
    }
}
