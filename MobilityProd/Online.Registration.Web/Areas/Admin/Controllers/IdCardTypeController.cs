﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class IdCardTypeController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToExeption(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetIDCardTypeResult(new IDCardType { Code = string.Empty, Description = string.Empty},null,false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelIDCardType svIDCardType)
        {
            try
            {
                return GetIDCardTypeResult(svIDCardType, true);
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        private IEnumerable<IDCardTypeSearchResults> GetIDCardTypes(IDCardType IDCardType, bool IsSearch, bool? Active = null)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            IDCardTypeFind IDCardTypeFind = new IDCardTypeFind()
            {
                IDCardType = IDCardType,
                Active = Active,
                PageSize = IsSearch ? 0 : PageSize
            };

            var IDs = proxy.IDCardTypeFind(IDCardTypeFind);
            var IDCardTypes = proxy.IDCardTypeGet(IDs);

            var results = IDCardTypes.OrderByDescending(a => a.ID).Select(a => new IDCardTypeSearchResults() 
            { 
                IDCardTypeID = a.ID,
                Code = a.Code,
                Description = a.Description,
                ActiveCheckBox =Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;

        }
        private ViewResult GetIDCardTypeResult(IDCardType IDCardType, bool? Active, bool IsSearch)
        {
            SearchViewModelIDCardType ViewModelsData = new SearchViewModelIDCardType
            {
                SearchResults = GetIDCardTypes(IDCardType, IsSearch, Active)
            };

            return View(ViewModelsData);
        }
        private ViewResult GetIDCardTypeResult(SearchViewModelIDCardType svIDCardType, bool IsSearch)
        {
            var SearchIDCardTypeData = new IDCardType()
            {
                Code = svIDCardType.SearchCriterias.Code,
                Description = svIDCardType.SearchCriterias.Description
            };
            svIDCardType.SearchResults = GetIDCardTypes(SearchIDCardTypeData, IsSearch);
            return View(svIDCardType);
        }
        public ActionResult Create()
        {
            return View(new IDCardType());
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, IDCardType addIDCardTypeData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    var IDCardTypeGet = proxy.IDCardTypeGet(proxy.IDCardTypeFind(new IDCardTypeFind()
                        {
                            IDCardType = new IDCardType { Code = addIDCardTypeData.Code },
                            Active = true
                        }));

                    if (IDCardTypeGet.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "ID Card Type Code cannot be duplicated.");
                        return View();
                    }

                    // Add ID Card Type
                    addIDCardTypeData.CreateDT = DateTime.Now;
                    addIDCardTypeData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addIDCardTypeData.Active = true;

                    IDCardTypeCreateResp oRp = new IDCardTypeCreateResp();
                    oRp = proxy.IDCardTypeCreate(addIDCardTypeData);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("ID Card Type Code ({0}) already exist!", addIDCardTypeData.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new ID Card Type: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        public ActionResult Edit(int id)
        {
            var IDCardTypeData = proxy.IDCardTypeGet(new int[] { id }).SingleOrDefault();
            return View(IDCardTypeData);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, IDCardType editIDCTData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    if(editIDCTData.Active == true)
                    {
                        var getData = proxy.IDCardTypeGet(proxy.IDCardTypeFind(new IDCardTypeFind()
                            {
                                IDCardType = new IDCardType() { Code = editIDCTData.Code },
                                Active = true
                            })).ToList();
                        var resultID = getData.Select(a => a.ID).SingleOrDefault();
                        if (getData.Count() > 0 && resultID != editIDCTData.ID)
                        {
                            ModelState.AddModelError(string.Empty, "ID Card Type Code already exist");
                            var model = proxy.IDCardTypeGet(new int[] { editIDCTData.ID }).SingleOrDefault();
                            return View(model);
                        }
                    }

                    editIDCTData.LastUpdateDT = DateTime.Now;
                    editIDCTData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.IDCardTypeUpdate(editIDCTData);
                }
                return RedirectToAction("Index");
                
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
    }
}
