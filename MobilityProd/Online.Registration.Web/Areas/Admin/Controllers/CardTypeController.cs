﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class CardTypeController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetCardTypeResult(new CardType { Code = string.Empty, Name = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelCardType svCardType)
        {
            try
            {
                return GetCardTypeResult(svCardType, true);
                
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private IEnumerable<CardTypeSearchResults> GetCardTypes(CardType cardType, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            CardTypeFind cardTypeFind = new CardTypeFind()
            {
                Active = active,
                CardType = cardType,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.CardTypeFind(cardTypeFind);
            var cardTypes = proxy.CardTypeGet(IDs);

            var results = cardTypes.OrderByDescending(a => a.ID).Select(a => new CardTypeSearchResults()
            {
                CardTypeID = a.ID,
                Code = a.Code,
                Name = a.Name,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;
        }
        private ViewResult GetCardTypeResult(CardType cardType, bool? Active, bool isSearch)
        {
            SearchViewModelCardType viewModels = new SearchViewModelCardType
            {
                SearchResults = GetCardTypes(cardType, isSearch, Active)
            };
            return View(viewModels);
        }
        private ViewResult GetCardTypeResult(SearchViewModelCardType svCardType, bool isSearch)
        {
            var sCardType = new CardType()
            {
                Code = svCardType.SearchCriterias.Code,
                Name = svCardType.SearchCriterias.Name
            };

            svCardType.SearchResults = GetCardTypes(sCardType, isSearch);
            return View(svCardType);
        }
        public ActionResult Edit(int id)
        {
            var cardType = proxy.CardTypeGet(new int[] { id }).SingleOrDefault();
            return View(cardType);
        }
        [HttpPost]
        public ActionResult Edit(CardType editCardType, FormCollection collection)
        {
            try
            {
                string submit = collection["Submit"].ToString2();

                if (submit == "Save")
                {
                    // Check for duplicate
                    if (editCardType.Active == true)
                    {
                        var cardTypes = proxy.CardTypeGet(proxy.CardTypeFind(new CardTypeFind()
                        {
                            CardType = new CardType { Code = editCardType.Code },
                            Active = true
                        })).ToList();
                        var resultID = cardTypes.Select(a => a.ID).SingleOrDefault();

                        if (cardTypes.Count() > 0 && resultID != editCardType.ID)
                        {
                            var model = proxy.CardTypeGet(new int[] { editCardType.ID }).SingleOrDefault();
                            ModelState.AddModelError(string.Empty, "Card Type Code cannot be duplicated.");
                            return View(model);
                        }
                    }

                    editCardType.LastUpdateDT = DateTime.Now;
                    editCardType.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.CardTypeUpdate(editCardType);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            { 
                return RedirectToException(ex);
            }
        }
        public ActionResult Create()
        {
            return View(new CardType());
        }
        [HttpPost]
        public ActionResult Create(CardType addCardType, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    // Check for duplicate
                    var cardTypes = proxy.CardTypeGet(proxy.CardTypeFind(new CardTypeFind()
                        {
                            CardType = new CardType { Code = addCardType.Code },
                            Active = true
                        })).ToList();

                    if (cardTypes.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Card Type Code cannot be duplicated.");
                        return View();
                    }

                    // Add Card Type
                    addCardType.CreateDT = DateTime.Now;
                    addCardType.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addCardType.Active = true;

                    CardTypeCreateResp oRp = new CardTypeCreateResp();
                    oRp = proxy.CardTypeCreate(addCardType);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Card Type Code ({0}) already exist!", addCardType.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new Card Type: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
    }
}
