﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class CustomerTitleController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }

        public ActionResult Index()
        {
            return GetCustomerTitleResult(new CustomerTitle { Code = string.Empty, Name = string.Empty }, null, false);
        }

        [HttpPost]
        public ActionResult Index(SearchViewModelCustomerTitle svCardType)
        {
            try
            {
                return GetCustomerTitleResult(svCardType, true);

            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
                                                                                                
        private IEnumerable<CustomerTitleSearchResults> GetCustomerTitles(CustomerTitle customerTitle, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            CustomerTitleFind customerTitleFind = new CustomerTitleFind()
            {
                Active = active,
                CustomerTitle = customerTitle,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.CustomerTitleFind(customerTitleFind);
            var customerTitles = proxy.CustomerTitleGet(IDs);

            var results = customerTitles.OrderByDescending(a => a.ID).Select(a => new CustomerTitleSearchResults()
            {
                CustomerTitleID = a.ID,
                Code = a.Code,
                Name = a.Name,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;
        }

        private ViewResult GetCustomerTitleResult(CustomerTitle customerTitle, bool? Active, bool isSearch)
        {
            SearchViewModelCustomerTitle viewModels = new SearchViewModelCustomerTitle
            {
                SearchResults = GetCustomerTitles(customerTitle, isSearch, Active)
            };
            return View(viewModels);
        }

        private ViewResult GetCustomerTitleResult(SearchViewModelCustomerTitle svCustomerTitle, bool isSearch)
        {
            var sCustomerTitle = new CustomerTitle()
            {
                Code = svCustomerTitle.SearchCriterias.Code,
                Name = svCustomerTitle.SearchCriterias.Name
            };

            svCustomerTitle.SearchResults = GetCustomerTitles(sCustomerTitle, isSearch);
            return View(svCustomerTitle);
        }

        public ActionResult Edit(int id)
        {
            var customerTitle = proxy.CustomerTitleGet(new int[] { id }).SingleOrDefault();
            return View(customerTitle);
        }

        [HttpPost]
        public ActionResult Edit(CustomerTitle editCustomerTitle, FormCollection collection)
        {
            try
            {
                string submit = collection["Submit"].ToString2();

                if (submit == "Save")
                {
                    if (editCustomerTitle.Active == true)
                    {
                        // check for duplicate code
                        var customerTitles = proxy.CustomerTitleGet(proxy.CustomerTitleFind(new CustomerTitleFind()
                        {
                            CustomerTitle = new CustomerTitle() { Code = editCustomerTitle.Code },
                            Active = true
                        })).ToList();

                        var resultCode = customerTitles.ToList().Select(a => a.ID).SingleOrDefault();

                        if (customerTitles.Count() > 0 && resultCode != editCustomerTitle.ID)
                        {
                            var customerTitle = proxy.CustomerTitleGet(new int[] { editCustomerTitle.ID }).SingleOrDefault();

                            ModelState.AddModelError(string.Empty, "Customer Title Code already exists.");
                            return View(customerTitle);
                        }
                    }

                    editCustomerTitle.LastUpdateDT = DateTime.Now;
                    editCustomerTitle.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.CustomerTitleUpdate(editCustomerTitle);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CustomerTitle addCustomerTitle, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    // Check for duplicate code
                    var customerTitles = proxy.CustomerTitleGet(proxy.CustomerTitleFind(new CustomerTitleFind()
                    {
                        CustomerTitle = new CustomerTitle() { Code = addCustomerTitle.Code },
                        Active = true
                    }));

                    if (customerTitles.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Customer Title Code already exists.");
                        return View();
                    }

                    addCustomerTitle.Active = true;
                    addCustomerTitle.CreateDT = DateTime.Now;
                    addCustomerTitle.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    CustomerTitleCreateResp oRp = new CustomerTitleCreateResp();
                    oRp = proxy.CustomerTitleCreate(addCustomerTitle);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Customer Title Code ({0}) already exists!", addCustomerTitle.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Failed to add new Customer Title: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
    }
 
}
