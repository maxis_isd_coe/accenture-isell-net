﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.UserSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class AccessController : Controller
    {
        UserServiceProxy proxy = new UserServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetAccessResult(new Access() { UserName = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelAccess svmAccess)
        {
            try
            {
                return GetAccessResult(svmAccess, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private ViewResult GetAccessResult(SearchViewModelAccess svmAccess, bool isSearch)
        {
            var accessData = new Access()
            {
                UserName = svmAccess.SearchCriterias.UserName
            };

            svmAccess.SearchResults = GetAccesses(accessData, isSearch);
            return View(svmAccess);
        }
        private ViewResult GetAccessResult(Access access, bool? Active, bool isSearch)
        {
            SearchViewModelAccess svmAccess = new SearchViewModelAccess()
            {
                SearchResults = GetAccesses(access, isSearch, Active)
            };

            return View(svmAccess);
        }
        private IEnumerable<AccessSearchResults> GetAccesses(Access access, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            var IDs = proxy.FindAccess(access);
            var getAccesses = proxy.AccessGet(IDs);

            var results = getAccesses.OrderByDescending(a => a.ID).Select(a => new AccessSearchResults()
            {
                AccessID = a.ID,
                UserName = a.UserName,
                UserFullName = a.UserID != 0 ? Util.GetNameByID(RefType.User, a.UserID).DisplayValue() : "N/A",
                Description = a.Description,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;
        }
        public ActionResult Create()
        {
            return View(new Access());
        }
        [HttpPost]
        public ActionResult Create(Access accessData, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    // Check for duplicate
                    var getData = proxy.AccessGet(proxy.FindAccess(new Access() { UserName = accessData.UserName, Active = true }));
                    if (getData.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Username cannot be duplicated.");
                        return View();
                    }

                    accessData.Password = Properties.Settings.Default.DefaultPassword;
                    accessData.CreateDT = DateTime.Now;
                    accessData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    accessData.Active = true;

                    var result = proxy.CreateAccess(accessData);
                    if(result == 0)
                    {
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        public ActionResult Edit(int id)
        {
            UpdateViewAccessModel uvAccess = new UpdateViewAccessModel() { UserGroupAccess = new List<UserGroupAccessSearchResults>() };
            var access = proxy.AccessGet(new int[] { id }).SingleOrDefault();

            uvAccess.Access = access;
            uvAccess.UserGroupAccess = GetUserGroupAccess(id);

            return View(uvAccess);
        }
        [HttpPost]
        public ActionResult Edit(Access Access, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                UpdateViewAccessModel uvAccess = new UpdateViewAccessModel() { UserGroupAccess = new List<UserGroupAccessSearchResults>() };
                var accessData = proxy.AccessGet(new int[] { Access.ID }).SingleOrDefault();

                uvAccess.Access = accessData;
                uvAccess.UserGroupAccess = GetUserGroupAccess(Access.ID);

                if (submit == "Save")
                {
                    if (Access.Active == true)
                    {
                        // check for duplicate
                        if (Access.Active == true)
                        {
                            var getData = proxy.AccessGet(proxy.FindAccess(new Access() { UserName = Access.UserName, Active = Access.Active })).ToList();

                            var resultCode = getData.Select(a => a.ID).SingleOrDefault();

                            if (getData.Count() > 0 && resultCode != Access.ID)
                            {
                                ModelState.AddModelError(string.Empty, "Username cannot be duplicated.");
                                return View(uvAccess);
                            }
                        }
                    }

                    // Update User Group
                    Access.LastUpdateDT = DateTime.Now;
                    Access.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    var result = proxy.UpdateAccess(Access);
                    if(result == false)
                    {
                        return View(uvAccess);
                    }

                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private List<UserGroupAccessSearchResults> GetUserGroupAccess(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<UserGroupAccessSearchResults> results = new List<UserGroupAccessSearchResults>();

            UserGrpAccessFind userGrpAccessFind = new UserGrpAccessFind()
            {
                UserGrpAccess = new UserGrpAccess()
                {
                    AccessID = id
                },
                PageSize = PageSize
            };

            var userGrpAccessIDs = proxy.UserGroupAccessFind(userGrpAccessFind);
            var userGrpAccesss = proxy.UserGroupAccessGet(userGrpAccessIDs);
            if (userGrpAccesss == null)
                return new List<UserGroupAccessSearchResults>();

            results = userGrpAccesss.OrderByDescending(a => a.ID).Select(a => new UserGroupAccessSearchResults()
            {
                UserGroupAccessID = a.ID,
                UserGroup = a.UserGrpID != 0 ? Util.GetNameByID(RefType.UserGroup, a.UserGrpID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = RefType.UserGroup
            }).ToList();

            return results;
        }

    }
}
