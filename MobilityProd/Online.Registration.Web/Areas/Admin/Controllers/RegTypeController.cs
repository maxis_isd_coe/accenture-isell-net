﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class RegTypeController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToExeption(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetRegTypeResult(new RegType { Code = string.Empty, Description = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelRegType SVMRegType)
        {
            try
            {
                return GetRegTypeResult(SVMRegType, true);
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        private IEnumerable<RegTypeSearchResults> GetRegTypes(RegType RegType, bool IsSearch, bool? Active = null)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            RegTypeFind RegTypeFind = new RegTypeFind()
            {
                RegType = RegType,
                Active = Active,
                PageSize = IsSearch ? 0 : PageSize
            };

            var IDs = proxy.RegTypeFind(RegTypeFind);
            var RegTypes = proxy.RegTypeGet(IDs);

            var results = RegTypes.OrderByDescending(a => a.ID).Select(a => new RegTypeSearchResults()
            {
                RegTypeID = a.ID,
                Code = a.Code,
                Description = a.Description,
                Message = a.Message,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;

        }
        private ViewResult GetRegTypeResult(RegType RegType, bool? Active, bool IsSearch)
        {
            SearchViewModelRegType SVMRegType = new SearchViewModelRegType
            {
                SearchResults = GetRegTypes(RegType, IsSearch, Active)
            };

            return View(SVMRegType);
        }
        private ViewResult GetRegTypeResult(SearchViewModelRegType SVMRegType, bool IsSearch)
        {
            var SearchRegTypeData = new RegType()
            {
                Code = SVMRegType.SearchCriterias.Code,
                Description = SVMRegType.SearchCriterias.Description
            };
            SVMRegType.SearchResults = GetRegTypes(SearchRegTypeData, IsSearch);
            return View(SVMRegType);
        }
        public ActionResult Create()
        {
            return View(new RegType());
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, RegType addRegTypeData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    var RegTypeGet = proxy.RegTypeGet(proxy.RegTypeFind(new RegTypeFind()
                    {
                        RegType = new RegType { Code = addRegTypeData.Code },
                        Active = true
                    }));

                    if (RegTypeGet.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Reg Type Code already exists.");
                        return View();
                    }

                    addRegTypeData.CreateDT = DateTime.Now;
                    addRegTypeData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addRegTypeData.Active = true;

                    RegTypeCreateResp oRp = new RegTypeCreateResp();
                    oRp = proxy.RegTypeCreate(addRegTypeData);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Reg Type Code ({0}) already exist!", addRegTypeData.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new Reg Type: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        public ActionResult Edit(int id)
        {
            var RegTypeData = proxy.RegTypeGet(new int[] { id }).SingleOrDefault();
            return View(RegTypeData);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, RegType editRegTypeData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    if (editRegTypeData.Active == true)
                    {
                        var RegTypeGet = proxy.RegTypeGet(proxy.RegTypeFind(new RegTypeFind()
                        {
                            RegType = new RegType { Code = editRegTypeData.Code },
                            Active = true
                        }));

                        if (RegTypeGet.Count() > 0)
                        {
                            ModelState.AddModelError(string.Empty, "Reg Type Code already exists.");
                            var model = proxy.RegTypeGet(new int[] { editRegTypeData.ID }).SingleOrDefault();
                            return View(model);
                        }
                    }

                    editRegTypeData.LastUpdateDT = DateTime.Now;
                    editRegTypeData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.RegTypeUpdate(editRegTypeData);
                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }

    }
}
