﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class PaymentModeController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToExeption(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetPaymentModeResult(new PaymentMode { Code = string.Empty, Name = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelPaymentMode SVMPaymentMode)
        {
            try
            {
                return GetPaymentModeResult(SVMPaymentMode, true);
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        private IEnumerable<PaymentModeSearchResults> GetPaymentModes(PaymentMode PaymentMode, bool IsSearch, bool? Active = null)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            PaymentModeFind PaymentModeFind = new PaymentModeFind()
            {
                PaymentMode = PaymentMode,
                Active = Active,
                PageSize = IsSearch ? 0 : PageSize
            };

            var IDs = proxy.PaymentModeFind(PaymentModeFind);
            var PaymentModes = proxy.PaymentModeGet(IDs);

            var results = PaymentModes.OrderByDescending(a => a.ID).Select(a => new PaymentModeSearchResults()
            {
                PaymentModeID = a.ID,
                Code = a.Code,
                Name = a.Name,
                Description = a.Description,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;

        }
        private ViewResult GetPaymentModeResult(PaymentMode PaymentMode, bool? Active, bool IsSearch)
        {
            SearchViewModelPaymentMode SVMPaymentMode = new SearchViewModelPaymentMode
            {
                SearchResults = GetPaymentModes(PaymentMode, IsSearch, Active)
            };

            return View(SVMPaymentMode);
        }
        private ViewResult GetPaymentModeResult(SearchViewModelPaymentMode SVMPaymentMode, bool IsSearch)
        {
            var SearchPaymentModeData = new PaymentMode()
            {
                Code = SVMPaymentMode.SearchCriterias.Code,
                Name = SVMPaymentMode.SearchCriterias.Name
            };
            SVMPaymentMode.SearchResults = GetPaymentModes(SearchPaymentModeData, IsSearch);
            return View(SVMPaymentMode);
        }
        public ActionResult Create()
        {
            return View(new PaymentMode());
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, PaymentMode addPMData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    var PaymentModeGet = proxy.PaymentModeGet(proxy.PaymentModeFind(new PaymentModeFind()
                    {
                        PaymentMode = new PaymentMode { Code = addPMData.Code },
                        Active = true
                    }));

                    if (PaymentModeGet.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Payment Mode Code already exists.");
                        return View();
                    }

                    addPMData.CreateDT = DateTime.Now;
                    addPMData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addPMData.Active = true;


                    PaymentModeCreateResp oRp = new PaymentModeCreateResp();
                    oRp = proxy.PaymentModeCreate(addPMData);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Payment Mode Code ({0}) already exist!", addPMData.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new Payment Mode: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        public ActionResult Edit(int id)
        {
            var PaymentModeData = proxy.PaymentModeGet(new int[] { id }).SingleOrDefault();
            return View(PaymentModeData);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, PaymentMode editPMData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    if (editPMData.Active == true)
                    {
                        var getData = proxy.PaymentModeGet(proxy.PaymentModeFind(new PaymentModeFind()
                        {
                            PaymentMode = new PaymentMode { Code = editPMData.Code },
                            Active = true
                        })).ToList();
                        var resultID = getData.Select(a => a.ID).SingleOrDefault();
                        if (getData.Count() > 0 && resultID != editPMData.ID)
                        {
                            ModelState.AddModelError(string.Empty, "Payment Mode Code already exists.");
                            var model = proxy.PaymentModeGet(new int[] { editPMData.ID }).SingleOrDefault();
                            return View(model);
                        }
                    }

                    editPMData.LastUpdateDT = DateTime.Now;
                    editPMData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.PaymentModeUpdate(editPMData);
                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }

    }
}
