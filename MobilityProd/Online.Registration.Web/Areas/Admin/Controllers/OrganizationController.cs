﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.OrganizationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class OrganizationController : Controller
    {
        OrganizationServiceProxy proxy = new OrganizationServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }

        public ActionResult Index()
        {
            return GetOrganizationResult(new Organization { Code = string.Empty, Name = string.Empty }, null, false);
        }
 
        [HttpPost]
        public ActionResult Index(SearchViewModelOrganization SVMOrganization)
        {
            try
            {
                return GetOrganizationResult(SVMOrganization, true);
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

        private IEnumerable<OrganizationSearchResults> GetOrganizations(Organization OrgData, bool IsSearch, bool? Active = null)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            OrganizationFind OrgFind = new OrganizationFind()
            {
                Organization = OrgData,
                Active = Active,
                PageSize = IsSearch ? 0 : PageSize
            };

            var IDs = proxy.OrganizationFind(OrgFind);
            var Organizations = proxy.OrganizationGet(IDs);

            var results = Organizations.OrderByDescending(a => a.ID).Select(a => new OrganizationSearchResults()
            {
                OrganizationID = a.ID,
                Code = a.Code,
                Name = a.Name,
                Description = a.Description,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;

        }

        private ViewResult GetOrganizationResult(Organization OrgData, bool? Active, bool IsSearch)
        {
            SearchViewModelOrganization SVMOrganization = new SearchViewModelOrganization
            {
                SearchResults = GetOrganizations(OrgData, IsSearch, Active)
            };

            return View(SVMOrganization);
        }

        private ViewResult GetOrganizationResult(SearchViewModelOrganization SVMOrganization, bool IsSearch)
        {
            var SearchOrgData = new Organization()
            {
                Code = SVMOrganization.SearchCriterias.Code,
                Name = SVMOrganization.SearchCriterias.Name
            };
            SVMOrganization.SearchResults = GetOrganizations(SearchOrgData, IsSearch);
            return View(SVMOrganization);
        }

        public ActionResult Create()
        {
            return View(new Organization());
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection, Organization addOrgData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {   
                    var getOrgs = proxy.OrganizationGet(proxy.OrganizationFind(new OrganizationFind()
                    {
                        Organization = new Organization { Code = addOrgData.Code },
                        Active = true
                    }));

                    if(getOrgs.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Organization Code already exists.");
                        return View();
                    }

                    addOrgData.CreateDT = DateTime.Now;
                    addOrgData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addOrgData.RegionID = 1;
                    addOrgData.Active = true;

                    OrganizationCreateResp oRp = new OrganizationCreateResp();
                    oRp = proxy.OrganizationCreate(addOrgData);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Organization Code ({0}) already exist!", addOrgData.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new Organization: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

        public ActionResult Edit(int id)
        {
            var Data = proxy.OrganizationGet(new int[] { id }).SingleOrDefault();
            return View(Data);
        }

        [HttpPost]
        public ActionResult Edit(FormCollection collection, Organization OrgData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    if(OrgData.Active == true)
                    {
                        var getOrg = proxy.OrganizationGet(proxy.OrganizationFind(new OrganizationFind()
                            {
                                Organization = new Organization { Code = OrgData.Code },
                                Active = true
                            }));
                        var getOrgID = getOrg.ToList().Select(a => a.ID).SingleOrDefault();

                        if(getOrg.Count() > 0 && getOrgID != OrgData.ID)
                        {
                            var organization = proxy.OrganizationGet(new int[ OrgData.ID]).SingleOrDefault();
                            ModelState.AddModelError(string.Empty, "Organization Code already exists.");
                            return View(organization);
                        }
                    }

                    OrgData.LastUpdateDT = DateTime.Now;
                    OrgData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.OrganizationUpdate(OrgData);
                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

    }
}
