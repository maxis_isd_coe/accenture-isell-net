﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ModelGroupController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        //protected override void Initialize(RequestContext requestContext)
        //{
        //    Util.CheckSessionAccess(requestContext);
        //    base.Initialize(requestContext);
        //}
        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index()
        {

            return GetModelGroupResult(new ModelGroup()
            {
                Code = string.Empty,
                Name = string.Empty
            }, false, null);
        }

        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index(SearchViewModelGroupModel svPT)
        {
            try
            {
                var sPT = new ModelGroup()
                {
                    Code = svPT.SearchCriterias.Code,
                    Name = svPT.SearchCriterias.Name
                };
                svPT.SearchResults = GetModelGroups(sPT, true, null);
                return View(svPT);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private IEnumerable<ModelGroupSearchResults> GetModelGroups(ModelGroup mg, bool isSearch, bool? Active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            ModelGroupFind modelGroupFind = new ModelGroupFind()
            {
                ModelGroup = mg,
                Active = Active,
                PageSize = isSearch ? 0 : pageSize
            };
            var IDs = proxy.ModelGroupFind(modelGroupFind);
            var modelGroup = proxy.ModelGroupGet(IDs);

            var results = modelGroup.OrderByDescending(a => a.ID).Select(a => new ModelGroupSearchResults()
            {
                Code = a.Code,
                CreateDT = Util.FormatDateTime(a.CreateDT),
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                LastAccessID = a.LastAccessID,
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT).DisplayValue(),
                Name = a.Name,
                ModelGroupID = a.ID,
            });
            return results;
        }
        private ActionResult GetModelGroupResult(ModelGroup mg, bool isSearch, bool? Active = null)
        {
            SearchViewModelGroupModel svPT = new SearchViewModelGroupModel()
            {
                SearchResults = GetModelGroups(mg, isSearch, Active),
            };
            return View(svPT);
        }
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Create()
        {
            return View(new ModelGroup());
        }

        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Create(ModelGroup addMdlGrp, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
//                int pgmBdlPckCompID = collection["pgmBdlPckCompID"].ToInt();
                if (submit == "Save")
                {
                    var getData = proxy.ModelGroupGet(proxy.ModelGroupFind(new ModelGroupFind()
                    {
                        ModelGroup = new ModelGroup() { Code = addMdlGrp.Code },
                        Active = true
                    }));

                    if (getData != null)
                    {
                        ModelState.AddModelError(string.Empty, "Model Group Code already exists.");
                        return View();
                    }
//                    addMdlGrp.PgmBdlPckComponentID = pgmBdlPckCompID;
                    addMdlGrp.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addMdlGrp.CreateDT = DateTime.Now;
                    addMdlGrp.Active = true;

                    ModelGroupCreateResp oRp = new ModelGroupCreateResp();
                    oRp = proxy.ModelGroupCreate(addMdlGrp);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Model Group Code ({0}) already exist!", addMdlGrp.Code));
                        }
                        else
                        { 
                            ModelState.AddModelError(string.Empty, "Fail to add new model group: " + oRp.Message); 
                        }
//                        ViewBag.pgmBdlPckCompID = pgmBdlPckCompID.ToInt();

                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(int id)
        {
            UpdateViewModelGroup uvModelGroup = new UpdateViewModelGroup();
            var modelGroups = proxy.ModelGroupGet(new int[] { id }).SingleOrDefault();

            uvModelGroup.ModelGroup = modelGroups;
            uvModelGroup.ModelGroupModel = GetModelGroupModel(id);

            return View(uvModelGroup);
        }

        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(UpdateViewModelGroup editMdlGrp, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    editMdlGrp.ModelGroup.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    editMdlGrp.ModelGroup.LastUpdateDT = DateTime.Now;

                    proxy.ModelGroupUpdate(editMdlGrp.ModelGroup);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private List<ModelGroupModelSearchResults> GetModelGroupModel(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            ModelGroupModelFind modelGroupModelFind = new ModelGroupModelFind()
            {
                ModelGroupModel = new ModelGroupModel()
                {
                    ModelGroupID = id
                },
                Active = null
            };

            var mgmIDs = proxy.ModelGroupModelFind(modelGroupModelFind);
            var mgmData = proxy.ModelGroupModelGet(mgmIDs);

            var results = mgmData.OrderByDescending(a => a.ID).Select(a => new ModelGroupModelSearchResults()
            {
                ModelGroupModelID = a.ID,
                Code = a.Code,
                Name = a.Name,
//                ModelGroup = a.ModelGroupID != 0 ? Util.GetNameByID(RefType.ModelGroup, a.ModelGroupID).DisplayValue() : "N/A",
                Model = a.ModelID != 0 ? Util.GetNameByID(RefType.Model, a.ModelID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = RefType.ModelGroup
            }).ToList();

            if (results.Count() == 0)
                return new List<ModelGroupModelSearchResults>();

            return results;
        }

    }
}
