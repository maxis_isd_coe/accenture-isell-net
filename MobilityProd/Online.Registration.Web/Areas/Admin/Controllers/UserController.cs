﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.UserSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class UserController : Controller
    {
        UserServiceProxy proxy = new UserServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetUserResult(new User() { FullName = string.Empty, MobileNo = string.Empty, EmailAddr = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelUser svUser)
        {
            try
            {
                return GetUserResult(svUser, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private IEnumerable<UserSearchResults> GetUsers(User user, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            UserFind userFind = new UserFind()
            {
                User = user,
                Active = active,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.UserFind(user, active);
            var users = proxy.UserGet(IDs);

            var results = users.OrderByDescending(a => a.ID).Select(a => new UserSearchResults()
                {
                    UserID = a.ID,
                    FullName = a.FullName,
                    IDCardType = a.IDCardTypeID != 0 ? Util.GetNameByID(RefType.IDCardType, a.IDCardTypeID).DisplayValue() : "N/A",
                    IDCardNo = a.IDCardNo,
                    MobileNo = a.MobileNo,
                    EmailAddr = a.EmailAddr,
                    Org = a.OrgID != 0 ? Util.GetNameByID(RefType.Organization, a.OrgID).DisplayValue() : "N/A",
                    ActiveCheckBox = Util.GetCheckBox(a.Active),
                    CreateDT = Util.FormatDateTime(a.CreateDT),
                    LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                    LastAccessID = a.LastAccessID
                });

            return results;
        }
        private ViewResult GetUserResult(User user, bool? Active, bool isSearch)
        {
            SearchViewModelUser viewUsers = new SearchViewModelUser()
            {
                SearchResults = GetUsers(user, isSearch, Active)
            };

            return View(viewUsers);
        }
        private ViewResult GetUserResult(SearchViewModelUser svUser, bool isSearch)
        {
            var sUser = new User() 
            { 
                FullName = svUser.SearchCriterias.FullName,
                MobileNo = svUser.SearchCriterias.MobileNo,
                EmailAddr = svUser.SearchCriterias.EmailAddr
            };

            svUser.SearchResults = GetUsers(sUser, isSearch);
            return View(svUser);
        }
        public ActionResult Edit(int id)
        {
            var user = proxy.UserGet(new int[] { id }).SingleOrDefault();
            return View(user);
        }
        [HttpPost]
        public ActionResult Edit(User editUser, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    editUser.LastUpdateDT = DateTime.Now;
                    editUser.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.UserUpdate(editUser);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        public ActionResult Create()
        {
            return View(new User());
        }
        [HttpPost]
        public ActionResult Create(User addUser, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    addUser.CreateDT = DateTime.Now;
                    addUser.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addUser.Active = true;

                    UserCreateResp oRp = new UserCreateResp();
                    oRp = proxy.UserCreate(addUser);

                    if (oRp.Code == "-1")
                    {
                        ModelState.AddModelError(string.Empty, "Fail to add new User: " + oRp.Message);
                        return View();
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        } 
    }
}
