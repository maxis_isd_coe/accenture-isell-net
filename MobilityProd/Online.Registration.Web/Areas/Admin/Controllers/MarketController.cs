﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class MarketController : Controller
    {
        ConfigServiceProxy proxy = new ConfigServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetMarketResult(new Market { Name = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelMarket SVMMarket)
        {
            try
            {
                return GetMarketResult(SVMMarket, true);
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        private IEnumerable<MarketSearchResults> GetMarkets(Market Market, bool IsSearch, bool? Active = null)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            MarketFind MarketFind = new MarketFind()
            {
                Market = Market,
                Active = Active,
                PageSize = IsSearch ? 0 : PageSize
            };

            var IDs = proxy.MarketFind(MarketFind);
            var Markets = proxy.MarketGet(IDs);

            var results = Markets.OrderByDescending(a => a.ID).Select(a => new MarketSearchResults()
            {
                MarketID = a.ID,
                Name = a.Name,
                Description = a.Description,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;

        }
        private ViewResult GetMarketResult(Market Market, bool? Active, bool IsSearch)
        {
            SearchViewModelMarket SVMMarket = new SearchViewModelMarket
            {
                SearchResults = GetMarkets(Market, IsSearch, Active)
            };

            return View(SVMMarket);
        }
        private ViewResult GetMarketResult(SearchViewModelMarket SVMMarket, bool IsSearch)
        {
            var SearchMarketData = new Market()
            {
                Name = SVMMarket.SearchCriterias.Name
            };
            SVMMarket.SearchResults = GetMarkets(SearchMarketData, IsSearch);
            return View(SVMMarket);
        }
        public ActionResult Create()
        {
            return View(new Market());
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, Market addMarketData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    addMarketData.CreateDT = DateTime.Now;
                    addMarketData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addMarketData.Active = true;

                    proxy.MarketCreate(addMarketData);
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        public ActionResult Edit(int id)
        {
            var Data = proxy.MarketGet(new int[] { id }).SingleOrDefault();
            return View(Data);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, Market MarketData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    MarketData.LastUpdateDT = DateTime.Now;
                    MarketData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.MarketUpdate(MarketData);
                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

    }
}
