﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.CatalogSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ModelGroupModelController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        
        public ActionResult Create(int? modelID, int? modelGroupID)
        {
            ModelGroupModelViewModel model = new ModelGroupModelViewModel()
            {
                ModelID = modelID.ToInt(),
                ModelGroupID = modelGroupID.ToInt(),
                ModelGroupModel = new ModelGroupModel()
            };

            return View(model);
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, ModelGroupModelViewModel addModelGroupModel)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int modelID = addModelGroupModel.ModelID == 0 ? addModelGroupModel.ModelGroupModel.ModelID : addModelGroupModel.ModelID;
                int modelGroupID = addModelGroupModel.ModelGroupID == 0 ? addModelGroupModel.ModelGroupModel.ModelGroupID : addModelGroupModel.ModelGroupID;
                RefType type = addModelGroupModel.Type;

                ModelGroupModelViewModel modelGroupModelVM = new ModelGroupModelViewModel()
                {
                    ModelID = addModelGroupModel.ModelID,
                    ModelGroupID = addModelGroupModel.ModelGroupID
                };

                if (submit == "Save")
                {
                    if ((addModelGroupModel.Type == RefType.ModelGroup && addModelGroupModel.ModelGroupModel.ModelID == 0) ||
                        (addModelGroupModel.Type == RefType.Model && addModelGroupModel.ModelGroupModel.ModelGroupID == 0))
                    {
                        return View(modelGroupModelVM);
                    }
                    
                    //Check for Code
                    var getCodeData = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                        {
                            ModelGroupModel = new ModelGroupModel { Code = addModelGroupModel.ModelGroupModel.Code }
                        })).ToList();

                    if (getCodeData.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Model Group Model Code already exists.");
                        return View(modelGroupModelVM);
                    }

                    // Check for duplicate ModelID and ModelGroupID
                    var getData = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                    {
                        ModelGroupModel = new ModelGroupModel { ModelID = modelID, ModelGroupID = modelGroupID },
                        Active = true
                    })).ToList();

                    if (getData.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Combination of Model and Model Group already exist.");
                        return View(modelGroupModelVM);
                    }

                    if (addModelGroupModel.ModelID != 0)
                        addModelGroupModel.ModelGroupModel.ModelID = addModelGroupModel.ModelID;
                    if (addModelGroupModel.ModelGroupID != 0)
                        addModelGroupModel.ModelGroupModel.ModelGroupID = addModelGroupModel.ModelGroupID;

                    addModelGroupModel.ModelGroupModel.FulfillmentModeID = 1;
                    addModelGroupModel.ModelGroupModel.CreateDT = DateTime.Now;
                    addModelGroupModel.ModelGroupModel.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addModelGroupModel.ModelGroupModel.Active = true;

                    proxy.ModelGroupModelCreate(addModelGroupModel.ModelGroupModel);
                }

                if (type == RefType.Model)
                {
                    return RedirectToAction("Edit", "Model", new { id = modelID });
                }
                else if (type == RefType.ModelGroup)
                {
                    return RedirectToAction("Edit", "ModelGroup", new { id = modelGroupID });
                }

                return View();
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        
        public ActionResult Edit(int id, RefType type)
        {
            var data = proxy.ModelGroupModelGet(new int[] { id }).SingleOrDefault();
            ModelGroupModelViewModel editModelGroupModel = new ModelGroupModelViewModel()
            {
                Type = type,
                ModelGroupModel = data
            };

            return View(editModelGroupModel);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, ModelGroupModelViewModel editModelGroupModel)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int modelID = editModelGroupModel.ModelGroupModel.ModelID.ToInt();
                int modelGroupID = editModelGroupModel.ModelGroupModel.ModelGroupID.ToInt();
                RefType type = editModelGroupModel.Type;

                var returnData = proxy.ModelGroupModelGet(new int[] { editModelGroupModel.ModelGroupModel.ID }).SingleOrDefault();
                ModelGroupModelViewModel modelGroupModelVM = new ModelGroupModelViewModel()
                {
                    Type = type,
                    ModelGroupModel = returnData
                };

                if (submit == "Save")
                {
                    //Check for Code
                    var getCodeData = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                    {
                        ModelGroupModel = new ModelGroupModel { Code = editModelGroupModel.ModelGroupModel.Code }
                    })).ToList();

                    var getCodeDataID = getCodeData.Select(a => a.ID).SingleOrDefault();

                    if (getCodeData.Count() > 0 && getCodeDataID != editModelGroupModel.ModelGroupModel.ID)
                    {
                        ModelState.AddModelError(string.Empty, "Model Group Model Code already exists.");
                        return View(modelGroupModelVM);
                    }

                    // Check for duplicate
                    if (editModelGroupModel.ModelGroupModel.Active == true)
                    {
                        var getData = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                        {
                            ModelGroupModel = new ModelGroupModel
                            {
                                ModelID = editModelGroupModel.ModelGroupModel.ModelID,
                                ModelGroupID = editModelGroupModel.ModelGroupModel.ModelGroupID
                            },
                            Active = true
                        })).ToList();

                        var getDataID = getData.Select(a => a.ID).SingleOrDefault();

                        if (getData.Count() > 0 && getDataID != editModelGroupModel.ModelGroupModel.ID)
                        {
                            ModelState.AddModelError(string.Empty, "Combination of the Model and Model Group already exist.");
                            return View(modelGroupModelVM);
                        }
                    }

                    editModelGroupModel.ModelGroupModel.LastUpdateDT = DateTime.Now;
                    editModelGroupModel.ModelGroupModel.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.ModelGroupModelUpdate(editModelGroupModel.ModelGroupModel);
                }

                if (type == RefType.Model)
                {
                    return RedirectToAction("Edit", "Model", new { id = modelID });
                }
                else if (type == RefType.ModelGroup)
                {
                    return RedirectToAction("Edit", "ModelGroup", new { id = modelGroupID });
                }

                return View(modelGroupModelVM);

            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

    }
}
