﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using SNT.Utility;
using Telerik.Web.Mvc;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ItemPriceController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        public ActionResult Add(int? bundlePackageID, int? packageCompID)
        {
            ItemPriceViewModel model = new ItemPriceViewModel()
            {
                BundlePackageID = bundlePackageID.ToInt(),
                PackageCompID = packageCompID.ToInt(),
                ItemPrice = new ItemPrice()
            };

            return View(model);
        }
        [HttpPost]
        public ActionResult Add(FormCollection collection, ItemPriceViewModel addBdlPkgItemPrice)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int bdlPkgID = addBdlPkgItemPrice.BundlePackageID == 0 ? addBdlPkgItemPrice.ItemPrice.BdlPkgID : addBdlPkgItemPrice.BundlePackageID;
                int pkgCompID = addBdlPkgItemPrice.PackageCompID == 0 ? addBdlPkgItemPrice.ItemPrice.PkgCompID : addBdlPkgItemPrice.PackageCompID;
                string type = addBdlPkgItemPrice.Type;

                if (submit == "Save")
                {
                    // check for duplicate
                    var getData = proxy.ItemPriceGet(proxy.ItemPriceFind(new ItemPriceFind()
                    {
                        ItemPrice = new ItemPrice
                        {
                            BdlPkgID = bdlPkgID,
                            PkgCompID = pkgCompID,
                            ModelID = addBdlPkgItemPrice.ItemPrice.ModelID
                        },
                        Active = true
                    }));

                    ItemPriceViewModel model = new ItemPriceViewModel()
                    {
                        BundlePackageID = addBdlPkgItemPrice.BundlePackageID,
                        PackageCompID = addBdlPkgItemPrice.PackageCompID
                    };

                    if (getData != null)
                    {
                        ModelState.AddModelError(string.Empty, "Combination of Bundle Package, Package Component and Model already exist.");
                        return View(model);
                    }
                    
                    if (addBdlPkgItemPrice.BundlePackageID != 0)
                        addBdlPkgItemPrice.ItemPrice.BdlPkgID = addBdlPkgItemPrice.BundlePackageID;

                    if (addBdlPkgItemPrice.PackageCompID != 0)
                        addBdlPkgItemPrice.ItemPrice.PkgCompID = addBdlPkgItemPrice.PackageCompID;

                    addBdlPkgItemPrice.ItemPrice.Active = true;
                    addBdlPkgItemPrice.ItemPrice.CreateDT = DateTime.Now;
                    addBdlPkgItemPrice.ItemPrice.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.ItemPriceCreate(addBdlPkgItemPrice.ItemPrice);
                }

                if (type == Properties.Settings.Default.Bundle_Package)
                {
                    return RedirectToAction("EditBdlPkg", "PgmBdlPkgComp", new { id = bdlPkgID, type = "Bundle" });
                }
                else if (type == Properties.Settings.Default.Package_Component)
                {
                    return RedirectToAction("EditPkgComp", "PgmBdlPkgComp", new { id = pkgCompID, type = "Package" });
                }

                return View();
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        public ActionResult Edit(int id, string type)
        {
            var data = proxy.ItemPriceGet(new int[] { id }).SingleOrDefault();
            ItemPriceViewModel editItemPrice = new ItemPriceViewModel()
            {
                Type = type,
                ItemPrice = data
            };

            return View(editItemPrice);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, ItemPriceViewModel editItemPrice)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int bdlPkgID = editItemPrice.ItemPrice.BdlPkgID.ToInt() == 0 ? editItemPrice.BundlePackageID : editItemPrice.ItemPrice.BdlPkgID.ToInt();
                int pkgCompID = editItemPrice.ItemPrice.PkgCompID.ToInt() == 0 ? editItemPrice.PackageCompID : editItemPrice.ItemPrice.PkgCompID.ToInt();
                string type = editItemPrice.Type;
                var resultID = 0;

                if (submit == "Save")
                {
                    if (editItemPrice.ItemPrice.Active == true)
                    {
                        // Check for duplicate
                        ItemPriceFind itemPriceFind = new ItemPriceFind()
                        {
                            ItemPrice = new ItemPrice
                            {
                                BdlPkgID = editItemPrice.ItemPrice.BdlPkgID,
                                PkgCompID = editItemPrice.ItemPrice.PkgCompID,
                                ModelID = editItemPrice.ItemPrice.ModelID
                            },
                            Active = true
                        };

                        var IDs = proxy.ItemPriceFind(itemPriceFind);
                        var itemPriceList = proxy.ItemPriceGet(IDs);

                        if (itemPriceList != null)
                        {
                            resultID = itemPriceList.SingleOrDefault().ID;

                            if (itemPriceList.Count() > 0 && resultID != editItemPrice.ItemPrice.ID)
                            {
                                var Data = proxy.ItemPriceGet(new int[] { editItemPrice.ItemPrice.ID }).SingleOrDefault();
                                editItemPrice.ItemPrice = Data;
                                ModelState.AddModelError(string.Empty, "Combination of Bundle Package, Package Component and Model already exist.");
                                return View(editItemPrice);
                            }
                        }
                    }

                    editItemPrice.ItemPrice.LastUpdateDT = DateTime.Now;
                    editItemPrice.ItemPrice.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.ItemPriceUpdate(editItemPrice.ItemPrice);
                }

                if (type == Properties.Settings.Default.Bundle_Package)
                {
                    return RedirectToAction("EditBdlPkg", "PgmBdlPkgComp", new { id = bdlPkgID, type = RefType.Bundle });
                }
                else if (type == Properties.Settings.Default.Package_Component)
                {
                    return RedirectToAction("EditPkgComp", "PgmBdlPkgComp", new { id = pkgCompID, type = RefType.Package });
                }

                return View();
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
    }
}
