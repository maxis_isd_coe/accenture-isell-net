﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.UserSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class UserGroupRoleController : Controller
    {
        UserServiceProxy proxy = new UserServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Create(int? userGroupID, int? userRoleID)
        {
            UserGroupRoleViewModel model = new UserGroupRoleViewModel()
            {
                UserGroupID = userGroupID.ToInt(),
                UserRoleID = userRoleID.ToInt(),
                UserGroupRole = new UserGrpRole()
            };

            return View(model);
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, UserGroupRoleViewModel addUserGroupRole)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int userGroupID = addUserGroupRole.UserGroupID == 0 ? addUserGroupRole.UserGroupRole.UserGrpID : addUserGroupRole.UserGroupID;
                int userRoleID = addUserGroupRole.UserRoleID == 0 ? addUserGroupRole.UserGroupRole.UserRoleID : addUserGroupRole.UserRoleID;
                RefType type = addUserGroupRole.Type;

                UserGroupRoleViewModel userGroupRoleVM = new UserGroupRoleViewModel()
                {
                    UserGroupID = type == RefType.UserGroup? userGroupID : 0,
                    UserRoleID = type == RefType.UserRole? userRoleID : 0
                };

                if (submit == "Save")
                {
                    // Check for duplicate
                    var getData = proxy.UserGroupRoleGet(proxy.UserGroupRoleFind(new UserGrpRoleFind()
                    {
                        UserGrpRole = new UserGrpRole { UserGrpID = userGroupID, UserRoleID = userRoleID },
                        Active = true
                    }));

                    if (getData.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Combination of the User Group and User Role already exist.");
                        return View(userGroupRoleVM);
                    }

                    if (addUserGroupRole.UserGroupID != 0)
                        addUserGroupRole.UserGroupRole.UserGrpID = addUserGroupRole.UserGroupID;

                    if (addUserGroupRole.UserRoleID != 0)
                        addUserGroupRole.UserGroupRole.UserRoleID = addUserGroupRole.UserRoleID;

                    addUserGroupRole.UserGroupRole.CreateDT = DateTime.Now;
                    addUserGroupRole.UserGroupRole.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addUserGroupRole.UserGroupRole.Active = true;

                    proxy.UserGroupRoleCreate(addUserGroupRole.UserGroupRole);
                }

                if (type == RefType.UserGroup)
                {
                    return RedirectToAction("Edit", "UserGroup", new { id = userGroupID });
                }
                else if (type == RefType.UserRole)
                {
                    return RedirectToAction("Edit", "UserRole", new { id = userRoleID });
                }

                return View(userGroupRoleVM);
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        public ActionResult Edit(int id, RefType type)
        {
            var data = proxy.UserGroupRoleGet(new int[] { id }).SingleOrDefault();
            UserGroupRoleViewModel editUserGroupRole = new UserGroupRoleViewModel()
            {
                Type = type,
                UserGroupRole = data
            };

            return View(editUserGroupRole);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, UserGroupRoleViewModel editUserGroupRole)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int userGroupID = editUserGroupRole.UserGroupRole.UserGrpID.ToInt();
                int userRoleID = editUserGroupRole.UserGroupRole.UserRoleID.ToInt();
                RefType type = editUserGroupRole.Type;

                var returnData = proxy.UserGroupRoleGet(new int[] { editUserGroupRole.UserGroupRole.ID }).SingleOrDefault();
                UserGroupRoleViewModel userGroupRoleVM = new UserGroupRoleViewModel()
                {
                    Type = type,
                    UserGroupRole = returnData
                };

                if (submit == "Save")
                {
                    // Check for duplicate
                    if (editUserGroupRole.UserGroupRole.Active == true)
                    {
                        var getData = proxy.UserGroupRoleGet(proxy.UserGroupRoleFind(new UserGrpRoleFind()
                        {
                            UserGrpRole = new UserGrpRole
                            {
                                UserGrpID = editUserGroupRole.UserGroupRole.UserGrpID,
                                UserRoleID = editUserGroupRole.UserGroupRole.UserRoleID
                            },
                            Active = true
                        })).ToList();

                        var resultID = getData.Select(a => a.ID).SingleOrDefault();

                        if (getData.Count() > 0 && resultID != editUserGroupRole.UserGroupRole.ID)
                        {
                            ModelState.AddModelError(string.Empty, "Combination of the User Group and User Role already exist.");
                            return View(userGroupRoleVM);
                        }
                    }

                    editUserGroupRole.UserGroupRole.LastUpdateDT = DateTime.Now;
                    editUserGroupRole.UserGroupRole.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.UserGroupRoleUpdate(editUserGroupRole.UserGroupRole);
                }

                if (type == RefType.UserGroup)
                {
                    return RedirectToAction("Edit", "UserGroup", new { id = userGroupID });
                }
                else if (type == RefType.UserRole)
                {
                    return RedirectToAction("Edit", "UserRole", new { id = userRoleID });
                }

                return View(userGroupRoleVM);

            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

    }
}
