﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.OrganizationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class OrganizationTypeController : Controller
    {
        OrganizationServiceProxy proxy = new OrganizationServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }

        public ActionResult Index()
        {
            return GetOrgTypeResult(new OrgType() { Code = string.Empty, Name = string.Empty }, null, false);
        }

        [HttpPost]
        public ActionResult Index(SearchViewModelOrgType svOrgType)
        {
            try
            {
                return GetOrgTypeResult(svOrgType, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        private IEnumerable<OrgTypeSearchResults> GetOrgTypes(OrgType orgType, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            OrgTypeFind orgTypeFind = new OrgTypeFind()
            {
                OrgType = orgType,
                Active = active,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.OrgTypeFind(orgTypeFind);
            var orgTypes = proxy.OrgTypeGet(IDs);

            var results = orgTypes.OrderByDescending(a => a.ID).Select(a => new OrgTypeSearchResults()
            {
                OrgTypeID = a.ID,
                Code = a.Code,
                Name = a.Name,
                Description = a.Description,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;
        }

        private ViewResult GetOrgTypeResult(OrgType orgType, bool? Active, bool isSearch)
        {
            SearchViewModelOrgType viewOrgTypes = new SearchViewModelOrgType()
            {
                SearchResults = GetOrgTypes(orgType, isSearch, Active)
            };

            return View(viewOrgTypes);
        }

        private ViewResult GetOrgTypeResult(SearchViewModelOrgType svOrgType, bool isSearch)
        {
            var sOrgType = new OrgType()
            {
                Code = svOrgType.SearchCriterias.Code,
                Name = svOrgType.SearchCriterias.Name
            };

            svOrgType.SearchResults = GetOrgTypes(sOrgType, isSearch);
            return View(svOrgType);
        }

        public ActionResult Edit(int id)
        {
            var orgType = proxy.OrgTypeGet(new int[] { id }).SingleOrDefault();
            return View(orgType);
        }

        [HttpPost]
        public ActionResult Edit(OrgType editOrgType, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    if (editOrgType.Active == true)
                    {
                        var orgType = proxy.OrgTypeGet(proxy.OrgTypeFind(new OrgTypeFind()
                        {
                            OrgType = new OrgType { Code = editOrgType.Code },
                            Active = true
                        }));

                        var orgTypeID = orgType.ToList().Select(a => a.ID).SingleOrDefault();

                        if (orgType.Count() > 0 && orgTypeID != editOrgType.ID)
                        {
                            var data = proxy.OrgTypeGet(new int[editOrgType.ID]).SingleOrDefault();
                            ModelState.AddModelError(string.Empty, "Organization Type Code already exists.");
                            return View(data);
                        }
                    }

                    editOrgType.LastUpdateDT = DateTime.Now;
                    editOrgType.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.OrgTypeUpdate(editOrgType);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(OrgType addOrgType, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    var orgType = proxy.OrgTypeGet(proxy.OrgTypeFind(new OrgTypeFind()
                    {
                        OrgType = new OrgType() { Code = addOrgType.Code },
                        Active = true
                    }));

                    if (orgType.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Organization Type Code already exists.");
                        return View();
                    }
                    
                    addOrgType.CreateDT = DateTime.Now;
                    addOrgType.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addOrgType.Active = true;

                    OrgTypeCreateResp oRp = new OrgTypeCreateResp();
                    oRp = proxy.OrgTypeCreate(addOrgType);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Organization Type Code ({0}) already exists!", addOrgType.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Failed to add new Organization Type: " + oRp.Message);
                        }
                        return View();
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
    }
}
