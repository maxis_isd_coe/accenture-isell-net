﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.ComplainSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ComplaintIssuesController : Controller
    {
        ComplainServiceProxy proxy = new ComplainServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetComplaintIssuesResult(new ComplaintIssues() { Name = string.Empty, Description = string.Empty, Type = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelComplaintIssue svComplaintIssues)
        {
            try
            {
                return GetComplaintIssuesResult(svComplaintIssues, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private IEnumerable<ComplaintIssueSearchResults> GetComplaintIssues(ComplaintIssues complaintIssues, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            ComplaintIssuesFind complaintIssuesFind = new ComplaintIssuesFind()
            {
                ComplaintIssues = complaintIssues,
                Active = active,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.ComplaintIssuesFind(complaintIssuesFind);
            if (IDs == null)
                return new List<ComplaintIssueSearchResults>();
            var sComplaintIssues = proxy.ComplaintIssuesGet(IDs);
            if (sComplaintIssues == null)
                return new List<ComplaintIssueSearchResults>();

            var results = sComplaintIssues.OrderByDescending(a => a.ID).Select(a => new ComplaintIssueSearchResults()
            {
                ComplaintIssueID = a.ID,
                Name = a.Name,
                Description = a.Description,
                Type = a.Type,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;
        }
        private ViewResult GetComplaintIssuesResult(ComplaintIssues complaintIssues, bool? Active, bool isSearch)
        {
            SearchViewModelComplaintIssue viewComplaintIssues = new SearchViewModelComplaintIssue()
            {
                SearchResults = GetComplaintIssues(complaintIssues, isSearch, Active)
            };

            return View(viewComplaintIssues);
        }
        private ViewResult GetComplaintIssuesResult(SearchViewModelComplaintIssue svComplaintIssue, bool isSearch)
        {
            var sComplaintIssues = new ComplaintIssues()
            {
                Name = svComplaintIssue.SearchCriterias.Name,
                Description = svComplaintIssue.SearchCriterias.Description,
                Type = svComplaintIssue.SearchCriterias.Type
            };

            svComplaintIssue.SearchResults = GetComplaintIssues(sComplaintIssues, isSearch);
            return View(svComplaintIssue);
        }
        public ActionResult Create()
        {
            return View(new ComplaintIssues());
        }
        [HttpPost]
        public ActionResult Create(ComplaintIssues addComplaintIssues, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    addComplaintIssues.CreateDT = DateTime.Now;
                    addComplaintIssues.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addComplaintIssues.Active = true;

                    ComplaintIssuesCreateResp oRp = new ComplaintIssuesCreateResp();
                    oRp = proxy.ComplaintIssuesCreate(addComplaintIssues);

                    if (oRp.Code == "-1")
                    {
                        ModelState.AddModelError(string.Empty, "Fail to add new country: " + oRp.Message);
                        
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        public ActionResult Edit(int id)
        {
            var complaintIssues = proxy.ComplaintIssuesGet(new int[] { id }).SingleOrDefault();
            var complaintType = complaintIssues.Type;
            UpdateViewComplaintIssues uvComplaintIssues = new UpdateViewComplaintIssues();
            uvComplaintIssues.ComplaintIssues = complaintIssues;
            uvComplaintIssues.ComplaintIssuesGroup = GetComplaintIssuesGroup(id, complaintType);
            return View(uvComplaintIssues);
        }
        [HttpPost]
        public ActionResult Edit(ComplaintIssues editComplaintIssues, FormCollection collection)
        {
            try
            {
                string submit = collection["Submit"].ToString2();
                if (submit == "Save")
                {
                    editComplaintIssues.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    editComplaintIssues.LastUpdateDT = DateTime.Now;

                    proxy.ComplaintIssuesUpdate(editComplaintIssues);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private List<ComplaintIssuesGroupSearchResults> GetComplaintIssuesGroup(int id, string complaintType)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            ComplaintIssuesGroupFind dataFind = new ComplaintIssuesGroupFind() { };
            dataFind.Active = null;

            if(complaintType == Properties.Settings.Default.ComplaintIssues_Product)
                dataFind.ComplaintIssuesGroup = new ComplaintIssuesGroup() { ProductID = id };

            if (complaintType == Properties.Settings.Default.ComplaintIssues_Reason1)
                dataFind.ComplaintIssuesGroup = new ComplaintIssuesGroup() { Reason1ID = id };

            if (complaintType == Properties.Settings.Default.ComplaintIssues_Reason2)
                dataFind.ComplaintIssuesGroup = new ComplaintIssuesGroup() { Reason2ID = id };

            if (complaintType == Properties.Settings.Default.ComplaintIssues_Reason3)
                dataFind.ComplaintIssuesGroup = new ComplaintIssuesGroup() { Reason3ID = id };

            var IDs = proxy.ComplaintIssuesGroupFind(dataFind);
            var getDataPR = proxy.ComplaintIssuesGroupGet(IDs);

            if (getDataPR == null)
                return new List<ComplaintIssuesGroupSearchResults>();

            var results = getDataPR.OrderByDescending(a => a.ID).Select(a => new ComplaintIssuesGroupSearchResults()
            {
                ComplaintIssuesGroupID = a.ID,
                Product = a.ProductID != 0 ? Util.GetNameByID(RefType.ComplaintIssues, a.ProductID).DisplayValue() : "N/A",
                Reason1 = a.Reason1ID != 0 ? Util.GetNameByID(RefType.ComplaintIssues, a.Reason1ID).DisplayValue() : "N/A",
                Reason2 = a.Reason2ID != 0 ? Util.GetNameByID(RefType.ComplaintIssues, a.Reason2ID).DisplayValue() : "N/A",
                Reason3 = a.Reason3ID != 0 ? Util.GetNameByID(RefType.ComplaintIssues, a.Reason3ID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = complaintType
            }).ToList();
            return results;
        }
    }
}
