﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.CatalogSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ModelImageController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Create(int? colourID, int? modelID)
        {
            ModelImageViewModel modelImageVM = new ModelImageViewModel()
            {
                ModelID = modelID.ToInt(),
                ColourID = colourID.ToInt(),
                ModelImage = new ModelImage()
            };
            return View(modelImageVM);
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, ModelImageViewModel addModelImage)
        {
            try
            {
                var submit = collection["submit"].ToString2();
                int colourID = addModelImage.ColourID == 0 ? addModelImage.ModelImage.ColourID.ToInt() : addModelImage.ColourID;
                int modelID = addModelImage.ModelID == 0 ? addModelImage.ModelImage.ModelID : addModelImage.ModelID;
                RefType type = addModelImage.Type;
                ModelImageViewModel modelImageVM = new ModelImageViewModel()
                {
                    ModelID = addModelImage.ModelID,
                    ColourID = addModelImage.ColourID
                };

                if (submit == "Save")
                {
                    /*
                    if (addModelImage.ModelImage.Active == true)
                    {
                        var getData = proxy.ModelImageGet(proxy.ModelImageFind(new ModelImageFind()
                            {
                                ModelImage = new ModelImage() { ModelID = modelID, ColourID = colourID },
                                Active = true
                            })).ToList();

                        if (getData.Count() > 0)
                        {
                            ModelState.AddModelError(string.Empty, "Combination of Model and Colour already exist.");
                            return View(modelImageVM);
                        }
                    }
                     */
                    if (addModelImage.ModelID != 0)
                        addModelImage.ModelImage.ModelID = addModelImage.ModelID;
                        addModelImage.ModelImage.ColourID = addModelImage.ModelImage.ColourID == 0 ? null : addModelImage.ModelImage.ColourID;
                    if (addModelImage.ColourID != 0)
                        addModelImage.ModelImage.ColourID = addModelImage.ColourID;

                    addModelImage.ModelImage.CreateDT = DateTime.Now;
                    addModelImage.ModelImage.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addModelImage.ModelImage.Active = true;

                    proxy.ModelImageCreate(addModelImage.ModelImage);

                    if (type == RefType.Model)
                    {
                        return RedirectToAction("Edit", "Model", new { id = modelID });
                    }
                    else if (type == RefType.Colour)
                    {
                        return RedirectToAction("Edit", "Color", new { id = colourID });
                    }
                }
                return View();
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }   
        }
        public ActionResult Edit(int id, RefType type)
        {
            var data = proxy.ModelImageGet(new int[] {id}).SingleOrDefault();
            ModelImageViewModel modelImageVM = new ModelImageViewModel()
            {
                Type = type,
                ModelImage = data
            };
            return View(modelImageVM);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, ModelImageViewModel editModelImage)
        {
            try
            {
                var submit = collection["submit"].ToString2();
                int modelID = editModelImage.ModelImage.ModelID.ToInt();
                int? colourID = editModelImage.ModelImage.ColourID == 0 ? null : editModelImage.ModelImage.ColourID;
                RefType type = editModelImage.Type;
                var returnData = proxy.ModelImageGet(new int[] { editModelImage.ModelImage.ID }).SingleOrDefault();
                ModelImageViewModel modelImageVM = new ModelImageViewModel()
                {
                    Type = type,
                    ModelImage = returnData
                };

                if(submit == "Save")
                {
                    /*
                    if (editModelImage.ModelImage.Active == true)
                    {
                        var getData = proxy.ModelImageGet(proxy.ModelImageFind(new ModelImageFind()
                            {
                                ModelImage = new ModelImage() { ModelID = modelID, ColourID = colourID },
                                Active = true
                            })).ToList();

                        var resultID = getData.Select(a => a.ID).SingleOrDefault();
                        if(getData.Count() > 0 && editModelImage.ModelImage.ID != resultID)
                        {
                            ModelState.AddModelError(string.Empty, "Combination of the Model and Model Image already exist.");
                            return View(modelImageVM);
                        }
                    }
                    */
                    editModelImage.ModelImage.ColourID = colourID;
                    editModelImage.ModelImage.LastUpdateDT = DateTime.Now;
                    editModelImage.ModelImage.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    proxy.ModelImageUpdate(editModelImage.ModelImage);
                    if (type == RefType.Model)
                    {
                        return RedirectToAction("Edit", "Model", new { id = modelID });
                    }
                    else if (type == RefType.Colour)
                    {
                        return RedirectToAction("Edit", "Color", new { id = colourID });
                    }
                }
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
            return View();
        }

    }
}
