﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.Properties;
using Online.Registration.Web.ContentRepository;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ComponentController : Controller
    {
        private CatalogServiceProxy proxy = new CatalogServiceProxy();
        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        //protected override void Initialize(RequestContext requestContext)
        //{
        //    Util.CheckSessionAccess(requestContext);
        //    base.Initialize(requestContext);
        //}

        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index()
        {
            return GetComponentResult(new Component()
            {
                Code = string.Empty,
                Name = string.Empty,
                ComponentTypeID = 0
            }, false, null);
        }
        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index(SearchViewComponentModel svComponent)
        {
            try
            {
                return GetComponentResult(svComponent, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private ActionResult GetComponentResult(Component findComponent, bool isSearch, bool? Active)
        {
            SearchViewComponentModel svComponent = new SearchViewComponentModel()
            {
                SearchResults = GetComponents(findComponent, isSearch, Active),
            };
            return View(svComponent);
        }
        private ActionResult GetComponentResult(SearchViewComponentModel svComponent, bool isSearch)
        {
            var sComponent = new Component()
            {
                ComponentTypeID = svComponent.SearchCriterias.ComponentTypeID,
                Code = svComponent.SearchCriterias.Code,
                Name = svComponent.SearchCriterias.Name,
            };
            svComponent.SearchResults = GetComponents(sComponent, isSearch);
            return View(svComponent);
        }
        private IEnumerable<ComponentSearchResults> GetComponents(Component component, bool isSearch, bool? Active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            ComponentFind componentFind = new ComponentFind()
            {
                Active = null,
                Component = component,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.ComponentFind(componentFind);
            var components = proxy.ComponentGet(IDs);

            var results = components.OrderByDescending(a => a.ID).Select(a => new ComponentSearchResults()
            {
                Code = a.Code,
                ComponentID = a.ID,
                ComponentTypeID = a.ComponentTypeID,
                ComponentType = Util.GetNameByID(RefType.ComponentType, a.ComponentTypeID),
                LastAccessID = a.LastAccessID,
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                Name = a.Name,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
            });
            return results;
        }
        
        public ActionResult Create()
        {
            return View(new Component());
        }

        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Create(Component addComponent, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit.Equals("Save"))
                {
                    // Check for code duplicate code
                    var components = proxy.ComponentGet(proxy.ComponentFind(new ComponentFind()
                    {
                        Component = new Component() { Code = addComponent.Code },
                        Active = true
                    })).ToList();

                    if (components.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Component Code already exists.");
                        return View();
                    }

                    addComponent.Active = true;
                    addComponent.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addComponent.CreateDT = DateTime.Now;

                    ComponentCreateResp createRp = new ComponentCreateResp();
                    createRp = proxy.ComponentCreate(addComponent);

                    if (createRp.Code == "-1")
                    {
                        if (createRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Component Code ({0}) already exists!", addComponent.Code));
                        }
                        else
                        { 
                            ModelState.AddModelError(string.Empty, "Failed to add Component: " + createRp.Message); 
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(int id)
        {
            UpdateViewComponentModel uvComponentModel = new UpdateViewComponentModel() { PgmBdlPckComponents = new List<PgmBdlPckComponentSearchResults>() };
            uvComponentModel.Component = proxy.ComponentGet(new int[] { id }).SingleOrDefault();
            uvComponentModel.PgmBdlPckComponents = GetPgmBdlPckComponents(id);

            return View(uvComponentModel);
        }

        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(UpdateViewComponentModel editComponent, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit.Equals("Save"))
                {
                    if (editComponent.Component.Active)
                    {
                        // check for duplicate code
                        var components = proxy.ComponentGet(proxy.ComponentFind(new ComponentFind()
                        {
                            Component = new Component() { Code = editComponent.Component.Code },
                            Active = true
                        })).ToList();

                        var resultID = components.ToList().Select(a => a.ID).SingleOrDefault();

                        if (components.Count() > 0 && resultID != editComponent.Component.ID)
                        {
                            var Data = proxy.ComponentGet(new int[] { editComponent.Component.ID }).SingleOrDefault();
                            editComponent.Component = Data;
                            ModelState.AddModelError(string.Empty, "Component Code already exists.");
                            return View(editComponent);
                        }
                    }

                    editComponent.Component.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    editComponent.Component.LastUpdateDT = DateTime.Now;
                    
                    proxy.ComponentUpdate(editComponent.Component);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        private List<PgmBdlPckComponentSearchResults> GetPgmBdlPckComponents(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<PgmBdlPckComponentSearchResults> results = new List<PgmBdlPckComponentSearchResults>();

            PgmBdlPckComponentFind pgmBdlPckComponentFind = new PgmBdlPckComponentFind()
            {
                PgmBdlPckComponent = new PgmBdlPckComponent()
                {
                    ChildID = id,
                    LinkType = Settings.Default.Package_Component
                },
                PageSize = PageSize
            };

            var pgmBdlPckComponentIDs = proxy.PgmBdlPckComponentFind(pgmBdlPckComponentFind);
            var pgmBdlPckComponentCodes = MasterDataCache.Instance.FilterComponents(pgmBdlPckComponentIDs);

            results = pgmBdlPckComponentCodes.OrderByDescending(a => a.ID).Select(a => new PgmBdlPckComponentSearchResults()
            {
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                Package = a.ParentID != 0 ? Util.GetNameByID(RefType.Package, a.ParentID).DisplayValue() : "N/A",
                CreateDT = Util.FormatDateTime(a.CreateDT),
                EndDate = Util.FormatDateTime(a.EndDate),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                PgmBdlPckComponentID = a.ID,
                StartDate = Util.FormatDateTime(a.StartDate),
                Type = RefType.Component
            }).ToList();

            return results;
        }

        //private List<PgmBdlPckComponentSearchResults> GetPgmBdlPckComponents(int ComponentID)
        //{
        //    List<PgmBdlPckComponentSearchResults> results = new List<PgmBdlPckComponentSearchResults>();
        //    PgmBdlPckComponentFindResp findRp = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFindReq() { PgmBdlPckComponent = new PgmBdlPckComponent() { ComponentID = ComponentID } });
        //    if (findRp.Code == "-1" || findRp.IDs == null || findRp.IDs.Length == 0)
        //        return results;
        //    PgmBdlPckComponentGetResp getRp = proxy.PgmBdlPckComponentGet(new PgmBdlPckComponentGetReq() { IDs = findRp.IDs });
        //    if (getRp.Code == "-1" || getRp.PgmBdlPckComponents == null || getRp.PgmBdlPckComponents.Length == 0)
        //        return results;

        //    var pgmBdlPckComponents = getRp.PgmBdlPckComponents;
        //    results = pgmBdlPckComponents.Select(a => new PgmBdlPckComponentSearchResults()
        //    {
        //        PgmBdlPckComponentID = a.ID,
        //        Bundle = Util.GetNameByID(RefType.Bundle, a.BundleID).DisplayValue(),
        //        Component = Util.GetNameByID(RefType.Component, a.ComponentID).DisplayValue(),
        //        Program = Util.GetNameByID(RefType.Program, a.ProgramID).DisplayValue(),
        //        Package = Util.GetNameByID(RefType.Package, a.PackageID).DisplayValue(),
        //        LastAccessID = a.LastAccessID,
        //        ActiveCheckBox = Util.GetCheckBox(a.Active),
        //        CreateDT = Util.FormatDateTime(a.CreateDT),
        //        LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT).DisplayValue(),
        //        StartDate = Util.FormatDate(a.StartDate),
        //        EndDate = Util.FormatDate(a.EndDate).DisplayValue(),
        //        Type = RefType.Component,
        //    }).ToList();
        //    return results;
        //}
        //[GridAction]
        //public ActionResult PgmBdlPckComponentPaging(int ComponentID)
        //{
        //    var data = GetPgmBdlPckComponents(ComponentID);
        //    return View(new GridModel<PgmBdlPckComponentSearchResults>() { Data = data });
        //}

    }
}
