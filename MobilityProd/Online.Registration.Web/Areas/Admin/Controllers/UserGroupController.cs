﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.UserSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class UserGroupController : Controller
    {
        UserServiceProxy proxy = new UserServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetUserGroupResult(new UserGroup() { Code = string.Empty, Name = string.Empty}, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelUserGroup svUserGroup)
        {
            try
            {
                return GetUserGroupResult(svUserGroup, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private ViewResult GetUserGroupResult(SearchViewModelUserGroup svUserGroup, bool isSearch)
        {
            var sUserGroup = new UserGroup()
            {
                Code = svUserGroup.SearchCriterias.Code,
                Name = svUserGroup.SearchCriterias.Name
            };

            svUserGroup.SearchResults = GetUserGroups(sUserGroup, isSearch);
            return View(svUserGroup);
        }
        private ViewResult GetUserGroupResult(UserGroup userGroup, bool? Active, bool isSearch)
        {
            SearchViewModelUserGroup viewUserGroups = new SearchViewModelUserGroup()
            {
                SearchResults = GetUserGroups(userGroup, isSearch, Active)
            };

            return View(viewUserGroups);
        }
        private IEnumerable<UserGroupSearchResults> GetUserGroups(UserGroup userGroup, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            UserGroupFind userGroupFind = new UserGroupFind()
            {
                UserGroup = userGroup,
                Active = active,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.UserGroupFind(userGroupFind);
            var userGroups = proxy.UserGroupGet(IDs);

            var results = userGroups.OrderByDescending(a => a.ID).Select(a => new UserGroupSearchResults()
            {
                UserGroupID = a.ID,
                Name = a.Name,
                Code = a.Code,
                Org = a.OrgID != 0 ? Util.GetNameByID(RefType.Organization, a.OrgID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;
        }
        public ActionResult Create()
        {
            return View(new UserGroup());
        }
        [HttpPost]
        public ActionResult Create(UserGroup addUserGroup, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    // Check for duplicate
                    var userGroup = proxy.UserGroupGet(proxy.UserGroupFind(new UserGroupFind()
                    {
                        UserGroup = new UserGroup() { Code = addUserGroup.Code },
                        Active = true
                    }));

                    if (userGroup.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "User Group Code already exists.");
                        return View();
                    }

                    // Add user Group
                    addUserGroup.CreateDT = DateTime.Now;
                    addUserGroup.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addUserGroup.Active = true;

                    UserGroupCreateResp oRp = new UserGroupCreateResp();
                    oRp = proxy.UserGroupCreate(addUserGroup);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("User Group Code ({0}) already exist!", addUserGroup.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new User Group: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        public ActionResult Edit(int id)
        {
            var userGroup = proxy.UserGroupGet(new int[] { id }).SingleOrDefault();
            UpdateViewUserGroupModel uvUserGroup = new UpdateViewUserGroupModel() 
            {
                UserGroup = userGroup,
                UserGroupRole = GetUserGroupRole(id),
                UserGroupAccess = GetUserGroupAccess(id)
            };
            return View(uvUserGroup);
        }
        [HttpPost]
        public ActionResult Edit(UpdateViewUserGroupModel editUserGroup, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    if (editUserGroup.UserGroup.Active == true)
                    {
                        // check for duplicate
                        
                        var userGroups = proxy.UserGroupGet(proxy.UserGroupFind(new UserGroupFind()
                        {
                            UserGroup = new UserGroup() { Code = editUserGroup.UserGroup.Code },
                            Active = true
                        })).ToList();

                        var resultCode = userGroups.ToList().Select(a => a.ID).SingleOrDefault();

                        if (userGroups.Count() > 0 && resultCode != editUserGroup.UserGroup.ID)
                        {
                            var userGroupData = proxy.UserGroupGet(new int[] { editUserGroup.UserGroup.ID }).SingleOrDefault();
                            UpdateViewUserGroupModel uvUserGroup = new UpdateViewUserGroupModel() 
                            { 
                                UserGroup = userGroupData,
                                UserGroupRole = GetUserGroupRole(editUserGroup.UserGroup.ID),
                                UserGroupAccess = GetUserGroupAccess(editUserGroup.UserGroup.ID)
                            };

                            ModelState.AddModelError(string.Empty, "User Group Code already exists.");
                            return View(uvUserGroup);
                        }
                    }

                    // Update User Group
                    editUserGroup.UserGroup.LastUpdateDT = DateTime.Now;
                    editUserGroup.UserGroup.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.UserGroupUpdate(editUserGroup.UserGroup);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private List<UserGroupAccessSearchResults> GetUserGroupAccess(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<UserGroupAccessSearchResults> results = new List<UserGroupAccessSearchResults>();

            UserGrpAccessFind userGrpAccessFind = new UserGrpAccessFind()
            {
                UserGrpAccess = new UserGrpAccess()
                {
                    UserGrpID = id
                },
                PageSize = PageSize
            };

            var userGrpAccessIDs = proxy.UserGroupAccessFind(userGrpAccessFind);
            var userGrpAccesss = proxy.UserGroupAccessGet(userGrpAccessIDs);
            if (userGrpAccesss == null)
                return new List<UserGroupAccessSearchResults>();

            results = userGrpAccesss.OrderByDescending(a => a.ID).Select(a => new UserGroupAccessSearchResults()
            {
                UserGroupAccessID = a.ID,
                Access = a.AccessID != 0 ? Util.GetNameByID(RefType.Access, a.AccessID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = RefType.StatusReason
            }).ToList();

            return results;
        }
        private List<UserGroupRoleSearchResults> GetUserGroupRole(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<UserGroupRoleSearchResults> results = new List<UserGroupRoleSearchResults>();

            UserGrpRoleFind userGrpRoleFind = new UserGrpRoleFind()
            {
                UserGrpRole = new UserGrpRole()
                {
                    UserGrpID = id
                },
                PageSize = PageSize
            };

            var userGrpRoleIDs = proxy.UserGroupRoleFind(userGrpRoleFind);
            var userGrpRoles = proxy.UserGroupRoleGet(userGrpRoleIDs);
            if (userGrpRoles == null)
                return new List<UserGroupRoleSearchResults>();

            results = userGrpRoles.OrderByDescending(a => a.ID).Select(a => new UserGroupRoleSearchResults()
            {
                UserGroupRoleID = a.ID,
                UserRole = a.UserRoleID != 0 ? Util.GetNameByID(RefType.UserRole, a.UserRoleID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = RefType.StatusReason
            }).ToList();

            return results;
        }

    }
}
