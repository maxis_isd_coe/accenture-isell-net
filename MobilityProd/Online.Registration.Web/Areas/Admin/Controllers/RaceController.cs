﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class RaceController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToExeption(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetRaceResult(new Race { Code = string.Empty, Name = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelRace SVMRace)
        {
            try
            {
                return GetRaceResult(SVMRace, true);
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        private IEnumerable<RaceSearchResults> GetRaces(Race Race, bool IsSearch, bool? Active = null)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            RaceFind RaceFind = new RaceFind()
            {
                Race = Race,
                Active = Active,
                PageSize = IsSearch ? 0 : PageSize
            };

            var IDs = proxy.RaceFind(RaceFind);
            var Races = proxy.RaceGet(IDs);

            var results = Races.OrderByDescending(a => a.ID).Select(a => new RaceSearchResults()
            {
                RaceID = a.ID,
                Code = a.Code,
                Name = a.Name,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;

        }
        private ViewResult GetRaceResult(Race Race, bool? Active, bool IsSearch)
        {
            SearchViewModelRace SVMRace = new SearchViewModelRace
            {
                SearchResults = GetRaces(Race, IsSearch, Active)
            };

            return View(SVMRace);
        }
        private ViewResult GetRaceResult(SearchViewModelRace SVMRace, bool IsSearch)
        {
            var SearchRaceData = new Race()
            {
                Code = SVMRace.SearchCriterias.Code,
                Name = SVMRace.SearchCriterias.Name
            };
            SVMRace.SearchResults = GetRaces(SearchRaceData, IsSearch);
            return View(SVMRace);
        }
        public ActionResult Create()
        {
            return View(new Race());
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, Race addRaceData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    var RaceGet = proxy.RaceGet(proxy.RaceFind(new RaceFind()
                    {
                        Race = new Race { Code = addRaceData.Code },
                        Active = true
                    }));

                    if (RaceGet.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Race Code already exists.");
                        return View();
                    }

                    addRaceData.CreateDT = DateTime.Now;
                    addRaceData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addRaceData.Active = true;

                    RaceCreateResp oRp = new RaceCreateResp();
                    oRp = proxy.RaceCreate(addRaceData);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Race Code ({0}) already exist!", addRaceData.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new Race: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        public ActionResult Edit(int id)
        {
            var RaceData = proxy.RaceGet(new int[] { id }).SingleOrDefault();
            return View(RaceData);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, Race editRaceData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    if (editRaceData.Active == true)
                    {
                        var getData = proxy.RaceGet(proxy.RaceFind(new RaceFind()
                        {
                            Race = new Race { Code = editRaceData.Code },
                            Active = true
                        }));
                        var resultID = getData.Select(a => a.ID).SingleOrDefault();
                        if (getData.Count() > 0 && resultID != editRaceData.ID)
                        {
                            ModelState.AddModelError(string.Empty, "Race Code already exists.");
                            return View();
                        }
                    }

                    editRaceData.LastUpdateDT = DateTime.Now;
                    editRaceData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.RaceUpdate(editRaceData);
                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }

    }
}
