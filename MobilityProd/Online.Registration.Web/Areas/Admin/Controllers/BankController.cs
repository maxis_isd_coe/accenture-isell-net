﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class BankController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }

        public ActionResult Index()
        {
            return GetBankResult(new Bank { Code = string.Empty, Name = string.Empty }, null, false);
        }

        [HttpPost]
        public ActionResult Index(SearchViewModelBank svBank)
        {
            try
            {
                return GetBankResult(svBank, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        private IEnumerable<BankSearchResults> GetBanks(Bank bank, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            BankFind bankFind = new BankFind()
            {
                Active = active,
                Bank = bank,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.BankFind(bankFind);
            var banks = proxy.BankGet(IDs);

            var results = banks.OrderByDescending(a => a.ID).Select(a => new BankSearchResults()
            {
                BankID = a.ID,
                Code = a.Code,
                Name = a.Name,
                Description = a.Description,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;
        }

        private ViewResult GetBankResult(Bank bank, bool? Active, bool isSearch)
        {
            SearchViewModelBank viewBanks = new SearchViewModelBank
            {
                SearchResults = GetBanks(bank, isSearch, Active)
            };
            return View(viewBanks);
        }

        private ViewResult GetBankResult(SearchViewModelBank svBank, bool isSearch)
        {
            var sBank = new Bank()
            {
                Code = svBank.SearchCriterias.Code,
                Name = svBank.SearchCriterias.Name
            };

            svBank.SearchResults = GetBanks(sBank, isSearch);
            return View(svBank);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Bank addBank, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    // Check for duplicate
                    var bank = proxy.BankGet(proxy.BankFind(new BankFind()
                    {
                        Bank = new Bank() { Code = addBank.Code },
                        Active = true
                    }));

                    if (bank.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Bank Code already exists.");
                        return View();
                    }

                    // Add country
                    addBank.Active = true;
                    addBank.CreateDT = DateTime.Now;
                    addBank.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    BankCreateResp oRp = new BankCreateResp();
                    oRp = proxy.BankCreate(addBank);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Bank Code ({0}) already exists!", addBank.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Failed to add new Bank: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        public ActionResult Edit(int id)
        {
            var bank = proxy.BankGet(new int[] { id }).SingleOrDefault();
            return View(bank);
        }

        [HttpPost]
        public ActionResult Edit(Bank editBank, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    if (editBank.Active == true)
                    {
                        // check for duplicate
                        var banks = proxy.BankGet(proxy.BankFind(new BankFind()
                        {
                            Bank = new Bank() { Code = editBank.Code },
                            Active = true
                        })).ToList();

                        var resultCode = banks.ToList().Select(a => a.ID).SingleOrDefault();

                        if (banks.Count() > 0 && resultCode != editBank.ID)
                        {
                            var bank = proxy.BankGet(new int[] { editBank.ID }).SingleOrDefault();

                            ModelState.AddModelError(string.Empty, "Bank Code already exists.");
                            return View(bank);
                        }
                    }

                    // Update Bank
                    editBank.LastUpdateDT = DateTime.Now;
                    editBank.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.BankUpdate(editBank);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
    }
}
