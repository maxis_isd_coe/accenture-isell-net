﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using SNT.Utility;
using Telerik.Web.Mvc; 
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.Properties;
using Online.Registration.Web.ContentRepository;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ProgramController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        //protected override void Initialize(RequestContext requestContext)
        //{
        //    Util.CheckSessionAccess(requestContext);
        //    base.Initialize(requestContext);
        //}
        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index()
        {
            return GetProgramResult(new Program() { Code = string.Empty, Name = string.Empty }, false, null);
        }
        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index(SearchViewProgramModel svProgram)
        {
            try
            {
                var sProgram = new Program()
                {
                    Code = svProgram.SearchCriterias.Code,
                    Name = svProgram.SearchCriterias.Name,
                };
                svProgram.SearchResults = GetPrograms(sProgram, true);
                return View(svProgram);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        [GridAction]
        public ActionResult IndexPaging(string code = "", string name = "")
        {
            var data = GetPrograms(new Program() { Code = code, Name = name }, true);
            return View(new GridModel<ProgramSearchResults>() { Data = data });
        }
        private ViewResult GetProgramResult(Program program, bool isSearch, bool? Active = null)
        {
            SearchViewProgramModel viewModels = new SearchViewProgramModel()
            {
                SearchResults = GetPrograms(program, isSearch, Active),
            };
            return View(viewModels);

        }
        private IEnumerable<ProgramSearchResults> GetPrograms(Program program, bool isSearch, bool? Active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            ProgramFind ProgramFind = new ProgramFind()
            {
                Active = Active,
                Program = program,
                PageSize = isSearch ? 0 : pageSize
            };

            //ProgramFindResp findRp = new ProgramFindResp();
            //findRp = 
                var ids = proxy.ProgramFind(ProgramFind);
            //ViewBag.Code = findRp.Code;
            //ViewBag.Message = findRp.Message;
            //ViewBag.Strace = findRp.StackTrace;
            //if (findRp.Code == "-1" || findRp.IDs == null || findRp.IDs.Length == 0)
            //    return null;

            //ProgramGetReq getRq = new ProgramGetReq()
            //{
            //    IDs = ids.ToArray()
            //};
            //ProgramGetResp getRp = new ProgramGetResp();
            var programs = proxy.ProgramGet(ids);
            //ViewBag.Code = getRp.Code;
            //ViewBag.Message = getRp.Message;
            //ViewBag.Strace = getRp.StackTrace;

            //if (getRp.Code == "-1" || getRp.Programs == null || getRp.Programs.Length == 0)
            //    return null;

            //var programs = isSearch ? getRp.Programs : getRp.Programs.OrderByDescending(a => a.ID).Take(50);
            var results = programs.OrderByDescending(a => a.ID).Select(a => new ProgramSearchResults()
            {
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                Code = a.Code,
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastAccessID = a.LastAccessID,
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                Name = a.Name,
                ProgramID = a.ID
            });
            return results;

        }
        
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Create()
        {
            return View(new Program());
        }
        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Create(Program addProgram, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    if (addProgram.StartDate >= addProgram.EndDate)
                    {
                        ModelState.AddModelError("", "Start Date must be earlier than End Date");
                        return View();
                    }

                    // Check for code duplicate code
                    var programs = proxy.ProgramGet(proxy.ProgramFind(new ProgramFind()
                    {
                        Program = new Program() { Code = addProgram.Code },
                        Active = true
                    })).ToList();

                    if (programs.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Program Code already exists.");
                        return View();
                    }

                    addProgram.CreateDT = DateTime.Now;
                    addProgram.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addProgram.Active = true;
                    //ProgramCreateReq oRq = new ProgramCreateReq()
                    //{
                    //    Program = addProgram
                    //};
                    ProgramCreateResp oRp = new ProgramCreateResp();
                    oRp = proxy.ProgramCreate(addProgram);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Program Code ({0}) already exists!", addProgram.Code));
                        }
                        else
                        { 
                            ModelState.AddModelError(string.Empty, "Failed to add new program: " + oRp.Message); 
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(int id)
        {
            UpdateViewProgramModel uvProgram = new UpdateViewProgramModel() { PgmBdlPckComponents = new List<PgmBdlPckComponentSearchResults>() };
            
            uvProgram.Program = proxy.ProgramGet(new int[] { id }).SingleOrDefault();
            uvProgram.PgmBdlPckComponents = GetProgramBundle(id);

            return View(uvProgram);
        }
        
        [GridAction]
        public ActionResult PgmBdlPckCompPaging(int programID)
        {
            var data = GetProgramBundle(programID);
            return View(new GridModel<PgmBdlPckComponentSearchResults>() { Data = data });
        }

        private List<PgmBdlPckComponentSearchResults> GetProgramBundle(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;
            List<PgmBdlPckComponentSearchResults> results = new List<PgmBdlPckComponentSearchResults>();

            PgmBdlPckComponentFind pgmBdlPckComponentFind = new PgmBdlPckComponentFind()
            {
                PgmBdlPckComponent = new PgmBdlPckComponent()
                {
                    ParentID = id,
                    LinkType = Settings.Default.Program_Bundle
                },
                PageSize = PageSize
            };

            var pgmBdlPckComponentIDs = proxy.PgmBdlPckComponentFind(pgmBdlPckComponentFind);
            var pgmBdlPckComponentCodes = MasterDataCache.Instance.FilterComponents(pgmBdlPckComponentIDs);

            results = pgmBdlPckComponentCodes.OrderByDescending(a => a.ID).Select(a => new PgmBdlPckComponentSearchResults()
            {
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                Bundle = a.ChildID != 0 ? Util.GetNameByID(RefType.Bundle, a.ChildID).DisplayValue() : "N/A",
                CreateDT = Util.FormatDateTime(a.CreateDT),
                EndDate = Util.FormatDateTime(a.EndDate),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                PgmBdlPckComponentID = a.ID,
                StartDate = Util.FormatDateTime(a.StartDate),
                Type = RefType.Program
            }).ToList();

            return results;
        }

        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(UpdateViewProgramModel editProgram, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    if (editProgram.Program.StartDate >= editProgram.Program.EndDate)
                    {
                        ModelState.AddModelError(string.Empty, "Fail to edit program, Start Date must be earlier than End Date");
                        return View(editProgram);
                    }

                    if (editProgram.Program.Active)
                    {
                        // check for duplicate code
                        var programs = proxy.ProgramGet(proxy.ProgramFind(new ProgramFind()
                        {
                            Program = new Program() { Code = editProgram.Program.Code },
                            Active = true
                        })).ToList();

                        var resultID = programs.ToList().Select(a => a.ID).SingleOrDefault();

                        if (programs.Count() > 0 && resultID != editProgram.Program.ID)
                        {
                            var Data = proxy.ProgramGet(new int[] { editProgram.Program.ID }).SingleOrDefault();
                            editProgram.Program = Data;
                            ModelState.AddModelError(string.Empty, "Program Code already exists.");
                            return View(editProgram);
                        }
                    }

                    editProgram.Program.LastUpdateDT = DateTime.Now;
                    editProgram.Program.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    
                    //ProgramUpdateReq oRq = new ProgramUpdateReq()
                    //{
                    //    Program = editProgram.Program
                    //};
                    //ProgramUpdateResp oRp = new ProgramUpdateResp();
                    //oRp = 
                    proxy.ProgramUpdate(editProgram.Program);
                    //if (oRp.Code == "-1")
                    //{
                    //    if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                    //    {
                    //        ModelState.AddModelError(string.Empty, string.Format("Program Code ({0}) already exist!", editProgram.Program.Code));
                    //    }
                    //    else
                    //    { 
                    //        ModelState.AddModelError(string.Empty, "Fail to edit program: " + oRp.Message); 
                    //    }
                    //    return View(editProgram);
                    //}
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
    }
}
