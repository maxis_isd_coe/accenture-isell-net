﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.ConfigSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class StatusTypeController : Controller
    {
        ConfigServiceProxy proxy = new ConfigServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }

        public ActionResult Index()
        {
            return GetStatusTypeResult(new StatusType() { Code = string.Empty }, null, false);
        }

        [HttpPost]
        public ActionResult Index(SearchViewModelStatusType svStatusType)
        {
            try
            {
                return GetStatusTypeResult(svStatusType, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        private IEnumerable<StatusTypeSearchResults> GetStatusTypes(StatusType statusType, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            StatusTypeFind statusTypeFind = new StatusTypeFind()
            {
                StatusType = statusType,
                Active = active,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.StatusTypeFind(statusTypeFind);
            var statusTypes = proxy.StatusTypeGet(IDs);

            var results = statusTypes.OrderByDescending(a => a.ID).Select(a => new StatusTypeSearchResults()
            {
                StatusTypeID = a.ID,
                Code = a.Code,
                Description = a.Description,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;
        }

        private ViewResult GetStatusTypeResult(StatusType statusType, bool? Active, bool isSearch)
        {
            SearchViewModelStatusType viewStatusTypes = new SearchViewModelStatusType()
            {
                SearchResults = GetStatusTypes(statusType, isSearch, Active)
            };

            return View(viewStatusTypes);
        }

        private ViewResult GetStatusTypeResult(SearchViewModelStatusType svStatusType, bool isSearch)
        {
            var sStatusType = new StatusType()
            {
                Code = svStatusType.SearchCriterias.Code
            };

            svStatusType.SearchResults = GetStatusTypes(sStatusType, isSearch);
            return View(svStatusType);
        }

        public ActionResult Edit(int id)
        {
            var statusType = proxy.StatusTypeGet(new int[] { id }).SingleOrDefault();
            return View(statusType);
        }

        [HttpPost]
        public ActionResult Edit(StatusType editStatusType, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    if (editStatusType.Active == true)
                    {
                        // check for duplicate
                        var statusType = proxy.StatusTypeGet(proxy.StatusTypeFind(new StatusTypeFind()
                        {
                            StatusType = new StatusType() { Code = editStatusType.Code },
                            Active = true
                        })).ToList();

                        var resultCode = statusType.ToList().Select(a => a.ID).SingleOrDefault();

                        if (statusType.Count() > 0 && resultCode != editStatusType.ID)
                        {
                            var data = proxy.StatusTypeGet(new int[] { editStatusType.ID }).SingleOrDefault();

                            ModelState.AddModelError(string.Empty, "Status Type Code cannot be duplicated.");
                            return View(data);
                        }
                    }

                    editStatusType.LastUpdateDT = DateTime.Now;
                    editStatusType.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.StatusTypeUpdate(editStatusType);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        public ActionResult Create()
        {
            return View(new StatusType());
        }

        [HttpPost]
        public ActionResult Create(StatusType addStatusType, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    var statusType = proxy.StatusTypeGet(proxy.StatusTypeFind(new StatusTypeFind()
                    {
                        StatusType = new StatusType() { Code = addStatusType.Code },
                        Active = true
                    }));

                    if (statusType.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Status Type Code already exists.");
                        return View();
                    }
                    
                    addStatusType.CreateDT = DateTime.Now;
                    addStatusType.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addStatusType.Active = true;

                    StatusTypeCreateResp oRp = new StatusTypeCreateResp();
                    oRp = proxy.StatusTypeCreate(addStatusType);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Status Type Code ({0}) already exist!", addStatusType.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Failed to add new Status Type: " + oRp.Message);
                        }
                        return View();
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
    }
}
