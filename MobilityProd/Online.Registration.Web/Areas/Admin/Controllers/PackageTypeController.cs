﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class PackageTypeController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        //protected override void Initialize(RequestContext requestContext)
        //{
        //    Util.CheckSessionAccess(requestContext);
        //    base.Initialize(requestContext);
        //}
        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index()
        {

            return GetPackageTypeResult(new PackageType()
            {
                Code = string.Empty,
                Name = string.Empty
            }, false, null);
        }

        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Index(SearchViewPackageTypeModel svPT)
        {
            try
            {
                var sPT = new PackageType()
                {
                    Code = svPT.SearchCriterias.Code,
                    Name = svPT.SearchCriterias.Name
                };
                svPT.SearchResults = GetPackageTypes(sPT, true, null);
                return View(svPT);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        
        private IEnumerable<PackageTypeSearchResults> GetPackageTypes(PackageType pt, bool isSearch, bool? Active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            PackageTypeFind packageTypeFind = new PackageTypeFind()
            {
                PackageType = pt,
                Active = Active,
                PageSize = isSearch ? 0 : pageSize
            };
            var IDs = proxy.PackageTypeFind(packageTypeFind);
            var packageType = proxy.PackageTypeGet(IDs);

            var results = packageType.OrderByDescending(a => a.ID).Select(a => new PackageTypeSearchResults()
            {
                Code = a.Code,
                CreateDT = Util.FormatDateTime(a.CreateDT),
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                LastAccessID = a.LastAccessID,
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT).DisplayValue(),
                Name = a.Name,
                PackageTypeID = a.ID,
            });
            return results;
        }
        private ActionResult GetPackageTypeResult(PackageType pt, bool isSearch, bool? Active = null)
        {
            SearchViewPackageTypeModel svPT = new SearchViewPackageTypeModel()
            {
                SearchResults = GetPackageTypes(pt, isSearch, Active),
            };
            return View(svPT);
        }
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Create()
        {
            return View(new PackageType());
        }

        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Create(PackageType addPT, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for code duplicate code
                    var packageTypes = proxy.PackageTypeGet(proxy.PackageTypeFind(new PackageTypeFind()
                    {
                        PackageType = new PackageType() { Code = addPT.Code },
                        Active = true
                    })).ToList();

                    if (packageTypes.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Package Type Code already exists.");
                        return View();
                    }

                    addPT.CreateDT = DateTime.Now;
                    addPT.Active = true;
                    addPT.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    
                    PackageTypeCreateResp oRp = new PackageTypeCreateResp();
                    oRp = proxy.PackageTypeCreate(addPT);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Package Type Code ({0}) already exist!", addPT.Code));
                        }
                        else
                        { ModelState.AddModelError(string.Empty, "Failed to add new Package Type: " + oRp.Message); }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(int id)
        {
            var packageTypes = proxy.PackageTypeGet(new int[] { id }).SingleOrDefault();

            return View(packageTypes);
        }

        [HttpPost]
        //[Authorize(Roles = "ADM_CAT_OWN")]
        public ActionResult Edit(PackageType editPT, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    if (editPT.Active)
                    {
                        // check for duplicate code
                        var packageTypes = proxy.PackageTypeGet(proxy.PackageTypeFind(new PackageTypeFind()
                        {
                            PackageType = new PackageType() { Code = editPT.Code },
                            Active = true
                        })).ToList();

                        var resultID = packageTypes.ToList().Select(a => a.ID).SingleOrDefault();

                        if (packageTypes.Count() > 0 && resultID != editPT.ID)
                        {
                            var Data = proxy.PackageTypeGet(new int[] { editPT.ID }).SingleOrDefault();
                            ModelState.AddModelError(string.Empty, "Package Type Code already exists.");
                            return View(Data);
                        }
                    }
                    
                    editPT.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    editPT.LastUpdateDT = DateTime.Now;
                    
                    proxy.PackageTypeUpdate(editPT);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

    }
}
