﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class CategoryController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }

        public ActionResult Index()
        {
            return GetCategoryResult(new Category() { Code = string.Empty, Name = string.Empty }, null, false);
        }

        [HttpPost]
        public ActionResult Index(SearchViewModelCategory svCategory)
        {
            try
            {
                return GetCategoryResult(svCategory, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        private IEnumerable<CategorySearchResults> GetCategories(Category category, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            CategoryFind categoryFind = new CategoryFind()
            {
                Active = active,
                Category = category,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.CategoryFind(categoryFind);
            var categories = proxy.CategoryGet(IDs);

            var results = categories.OrderByDescending(a => a.ID).Select(a => new CategorySearchResults()
            {
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CategoryID = a.ID,
                Code = a.Code,
                CreateDT = Util.FormatDateTime(a.CreateDT),
                Description = a.Description,
                LastAccessID = a.LastAccessID,
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                Name = a.Name,
            });
            return results;
        }

        private ViewResult GetCategoryResult(Category category, bool? Active, bool isSearch)
        {
            SearchViewModelCategory viewCategories = new SearchViewModelCategory()
            {
                SearchResults = GetCategories(category, isSearch, Active)
            };
            
            return View(viewCategories);
        }

        private ViewResult GetCategoryResult(SearchViewModelCategory svCategory, bool isSearch)
        {
            var sCategory = new Category()
            {
                Code = svCategory.SearchCriterias.Code,
                Name = svCategory.SearchCriterias.Name
            };

            svCategory.SearchResults = GetCategories(sCategory, isSearch);
            return View(svCategory);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Category addCategory, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    var categories = proxy.CategoryGet(proxy.CategoryFind(new CategoryFind()
                    {
                        Category = new Category() { Code = addCategory.Code },
                        Active = true
                    })).ToList();

                    if (categories.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Category Code cannot be duplicated.");
                        return View();
                    }
                    
                    // Add new Category
                    addCategory.Active = true;
                    addCategory.CreateDT = DateTime.Now;
                    addCategory.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    CategoryCreateResp oRp = new CategoryCreateResp();
                    oRp = proxy.CategoryCreate(addCategory);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Category Code ({0}) already exists!", addCategory.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Failed to add new Category: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        public ActionResult Edit(int id)
        {
            var category = proxy.CategoryGet(new int[] { id }).SingleOrDefault();
            return View(category);
        }

        [HttpPost]
        public ActionResult Edit(Category editCategory, FormCollection collection)
        {
            try
            {
                string submit = collection["Submit"].ToString2();
                if (submit == "Save")
                {
                    if (editCategory.Active == true)
                    {
                        // check for duplicate
                        var categories = proxy.CategoryGet(proxy.CategoryFind(new CategoryFind()
                        {
                            Category = new Category() { Code = editCategory.Code },
                            Active = true
                        })).ToList();

                        var resultCode = categories.ToList().Select(a => a.ID).SingleOrDefault();

                        if (categories.Count() > 0 && resultCode != editCategory.ID)
                        {
                            var category = proxy.CategoryGet(new int[] { editCategory.ID }).SingleOrDefault();

                            ModelState.AddModelError(string.Empty, "Category Code cannot be duplicated.");
                            return View(category);
                        }
                    }

                    editCategory.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    editCategory.LastUpdateDT = DateTime.Now;

                    proxy.CategoryUpdate(editCategory);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
    }
}
