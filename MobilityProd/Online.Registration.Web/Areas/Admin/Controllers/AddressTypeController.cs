﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class AddressTypeController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { ErrorCode = "xxx", type = "Admin" });
        }

        public ActionResult Index()
        {
            return GetAddressTypeResult(new AddressType { Code = string.Empty }, null, false);
        }

        [HttpPost]
        public ActionResult Index(SearchViewModelAddressType svAddressType)
        {
            try
            {
                return GetAddressTypeResult(svAddressType, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        private IEnumerable<AddressTypeSearchResults> GetAddressTypes(AddressType addressType, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            AddressTypeFind addressTypeFind = new AddressTypeFind
            {
                AddressType = addressType,
                Active = active,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.AddressTypeFind(addressTypeFind);
            var addressTypes = proxy.AddressTypeGet(IDs);

            var results = addressTypes.OrderByDescending(a => a.ID).Select(a => new AddressTypeSearchResults()
            {
                AddressTypeID = a.ID,
                Code = a.Code,
                Description = a.Description,
                ActiveCheckbox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastAccessID = a.LastAccessID,
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT)
            });

            return results;
        }

        private ViewResult GetAddressTypeResult(AddressType addressType, bool? Active, bool isSearch)
        {
            SearchViewModelAddressType viewModels = new SearchViewModelAddressType
            {
                SearchResults = GetAddressTypes(addressType, isSearch, Active)
            };

            return View(viewModels);
        }

        private ViewResult GetAddressTypeResult(SearchViewModelAddressType svAddressType, bool isSearch)
        {
            var sAddressType = new AddressType()
            {
                Code = svAddressType.SearchCriterias.Code
            };

            svAddressType.SearchResults = GetAddressTypes(sAddressType, isSearch);

            return View(svAddressType);
        }

        public ActionResult Edit(int id)
        {
            var addressType = proxy.AddressTypeGet(new int[] { id }).SingleOrDefault();
            return View(addressType);
        }

        [HttpPost]
        public ActionResult Edit(AddressType editAddressType, FormCollection collection)
        {
            try
            {
                string submit = collection["Submit"].ToString2();
                if (submit == "Save")
                {
                    if (editAddressType.Active == true)
                    {
                        // check for duplicate
                        var addressTypes = proxy.AddressTypeGet(proxy.AddressTypeFind(new AddressTypeFind()
                        {
                            AddressType = new AddressType() { Code = editAddressType.Code },
                            Active = true
                        })).ToList();

                        var resultCode = addressTypes.ToList().Select(a => a.ID).SingleOrDefault();

                        if (addressTypes.Count() > 0 && resultCode != editAddressType.ID)
                        {
                            ModelState.AddModelError(string.Empty, "Address Type Code already exists.");
                            var addressType = proxy.AddressTypeGet(new int[] { editAddressType.ID }).SingleOrDefault();
                            return View(addressType);
                        }
                    }

                    editAddressType.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    editAddressType.LastUpdateDT = DateTime.Now;

                    proxy.AddressTypeUpdate(editAddressType);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(AddressType addAddressType, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    // Check for duplicate
                    var addressTypes = proxy.AddressTypeGet(proxy.AddressTypeFind(new AddressTypeFind()
                        {
                            AddressType = new AddressType() { Code = addAddressType.Code },
                            Active = true
                        }));

                    if (addressTypes.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Address Type Code already exists.");
                        return View();
                    }

                    // Add new Address Type
                    addAddressType.Active = true;
                    addAddressType.CreateDT = DateTime.Now;
                    addAddressType.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    AddressTypeCreateResp oRp = new AddressTypeCreateResp();
                    oRp = proxy.AddressTypeCreate(addAddressType);
                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Address Type Code ({0}) already exists!", addAddressType.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Failed to add new Address Type: " + oRp.Message);
                        }
                        return View();
                    }
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
    }
}
