﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class LanguageController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToExeption(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetLangaugeResult(new Language { Code = string.Empty, Name = string.Empty }, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelLanguage SVMLang)
        {
            try
            {
                return GetLangaugeResult(SVMLang, true);
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        private IEnumerable<LanguageSearchResults> GetLanguages(Language LangData, bool IsSearch, bool? Active = null)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            LanguageFind LangFind = new LanguageFind()
            {
                Language = LangData,
                Active = Active,
                PageSize = IsSearch ? 0 : PageSize
            };

            var IDs = proxy.LanguageFind(LangFind);
            var ReturnLangData = proxy.LanguageGet(IDs);

            var results = ReturnLangData.OrderByDescending(a => a.ID).Select(a => new LanguageSearchResults()
            {
                LanguageID = a.ID,
                Code = a.Code,
                Name = a.Name,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName
            });
            return results;
        }
        private ViewResult GetLangaugeResult(Language LangData, bool? Active, bool IsSearch)
        {
            SearchViewModelLanguage SVMLanguage = new SearchViewModelLanguage
            {
                SearchResults = GetLanguages(LangData, IsSearch, Active)
            };
            return View(SVMLanguage);
        }
        private ViewResult GetLangaugeResult(SearchViewModelLanguage SVMLang, bool IsSearch)
        {
            var SearchLangData = new Language()
            {
                Code = SVMLang.SearchCriterias.Code,
                Name = SVMLang.SearchCriterias.Name
            };
            SVMLang.SearchResults = GetLanguages(SearchLangData, IsSearch);
            return View(SVMLang);
        }
        public ActionResult Edit(int id)
        {
            var LangData = proxy.LanguageGet(new int[] { id }).SingleOrDefault();
            return View(LangData);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, Language editLangData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    if (editLangData.Active == true)
                    {
                        var getData = proxy.LanguageGet(proxy.LanguageFind(new LanguageFind()
                        {
                            Language = new Language { Code = editLangData.Code },
                            Active = true
                        })).ToList();
                        var resultID = getData.Select(a => a.ID).SingleOrDefault();

                        if (getData.Count() > 0 && resultID != editLangData.ID)
                        {
                            ModelState.AddModelError(string.Empty, "Language Code cannot be duplicated.");
                            var model = proxy.LanguageGet(new int[] { editLangData.ID }).SingleOrDefault();
                            return View(model);
                        }
                    }
                    editLangData.LastUpdateDT = DateTime.Now;
                    editLangData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.LanguageUpdate(editLangData);
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        public ActionResult Create()
        {
            return View(new Language());
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, Language addLangData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    var LanguageGet = proxy.LanguageGet(proxy.LanguageFind(new LanguageFind()
                    {
                        Language = new Language { Code = addLangData.Code },
                        Active = true
                    }));

                    if (LanguageGet.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Language Code cannot be duplicated.");
                        return View();
                    }

                    addLangData.CreateDT = DateTime.Now;
                    addLangData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addLangData.Active = true;

                    LanguageCreateResp oRp = new LanguageCreateResp();
                    oRp = proxy.LanguageCreate(addLangData);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Language Code ({0}) already exist!", addLangData.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new Language: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
    }
}
