﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ModelController : Controller
    {
        CatalogServiceProxy proxy = new CatalogServiceProxy();
        private RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        //protected override void Initialize(RequestContext requestContext)
        //{
        //    Util.CheckSessionAccess(requestContext);
        //    base.Initialize(requestContext);
        //}

        //[Authorize(Roles = "ADM_INV_OWN")]
        public ActionResult Index()
        {
            return GetModelResult(new Model() { Code = string.Empty, Name = string.Empty, BrandID = 0 }, null, false);
        }
        [HttpPost]
        //[Authorize(Roles = "ADM_INV_OWN")]
        public ActionResult Index(SearchViewModelModel svModel)
        {
            try
            {
                return GetModelResult(svModel, true);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        private IEnumerable<ModelSearchResults> GetModels(Model model, bool isSearch, bool? active = null)
        {
            int pageSize = Properties.Settings.Default.PageSize;

            ModelFind modelFind = new ModelFind()
            {
                Active = active,
                Model = model,
                PageSize = isSearch ? 0 : pageSize
            };

            var IDs = proxy.ModelFind(modelFind);
            var models = proxy.ModelGet(IDs);
            
            var results = models.OrderByDescending(a => a.ID).Select(a => new ModelSearchResults()
            {
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                Brand = a.BrandID != 0 ? Util.GetNameByID(RefType.Brand, a.BrandID).DisplayValue() : "N/A",
                Code = a.Code,
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastAccessID = a.LastAccessID,
                ModelID = a.ID,
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                Name = a.Name,
            });
            return results;
        }
        private ViewResult GetModelResult(Model model, bool? Active, bool isSearch)
        {
            SearchViewModelModel viewModels = new SearchViewModelModel()
            {
                SearchResults = GetModels(model, isSearch, Active)
            };
            return View(viewModels);

        }
        private ViewResult GetModelResult(SearchViewModelModel svModel, bool isSearch)
        {
            var sModel = new Model()
            {
                Code = svModel.SearchCriterias.Code,
                Name = svModel.SearchCriterias.Name,
                BrandID = svModel.SearchCriterias.BrandID
            };

            svModel.SearchResults = GetModels(sModel, isSearch);
            return View(svModel);
        }
        
        //[Authorize(Roles = "ADM_INV_OWN")]
        public ActionResult Create()
        {
            return View(new Model());
        }
        [HttpPost]
        //[Authorize(Roles = "ADM_INV_OWN")]
        public ActionResult Create(Model addModel, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    var models = proxy.ModelGet(proxy.ModelFind(new ModelFind()
                    {
                        Model = new Model() { Code = addModel.Code },
                        Active = true
                    })).ToList();

                    if (models.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Model Code cannot be duplicated.");
                        return View();
                    }

                    // Add new model
                    addModel.CreateDT = DateTime.Now;
                    addModel.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addModel.DistOrgID = addModel.DistOrgID == 0 ? null : addModel.DistOrgID;
                    addModel.Active = true;
                    
                    ModelCreateResp oRp = new ModelCreateResp();
                    oRp = proxy.ModelCreate(addModel);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Model Code ({0}) already exist!", addModel.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new model: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        //[Authorize(Roles = "ADM_INV_OWN")]
        public ActionResult Edit(int id)
        {
            UpdateViewModel uvModel = new UpdateViewModel() { ModelGroupModel = new List<ModelGroupModelSearchResults>() };
            var model = proxy.ModelGet(new int[] { id }).SingleOrDefault();

            uvModel.Model = model;
            uvModel.ModelGroupModel = GetModelGroupModel(id);
            uvModel.ModelImage = GetModelImages(id);

            return View(uvModel);
        }
        [HttpPost]
        //[Authorize(Roles = "ADM_INV_OWN")]
        public ActionResult Edit(UpdateViewModel editModel, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    if (editModel.Model.Active == true)
                    {
                        // Check for duplicate
                        var models = proxy.ModelGet(proxy.ModelFind(new ModelFind()
                        {
                            Model = new Model() { Code = editModel.Model.Code },
                            Active = true
                        })).ToList();

                        var resultID = models.SingleOrDefault().ID;

                        if (models.Count() > 0 && resultID != editModel.Model.ID)
                        {
                            UpdateViewModel uvModel = new UpdateViewModel() { ModelGroupModel = new List<ModelGroupModelSearchResults>() };
                            var modelData = proxy.ModelGet(new int[] { editModel.Model.ID }).SingleOrDefault();

                            uvModel.Model = modelData;
                            uvModel.ModelGroupModel = GetModelGroupModel(editModel.Model.ID);
                            uvModel.ModelImage = GetModelImages(editModel.Model.ID);

                            ModelState.AddModelError(string.Empty, "Model Code cannot be duplicated.");
                            return View(uvModel);
                        }
                    }

                    editModel.Model.LastUpdateDT = DateTime.Now;
                    editModel.Model.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.ModelUpdate(editModel.Model);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

        //[HttpPost]
        //public JsonResult GetBrandList(int CategoryID, string defaultText = "")
        //{
        //    JsonResult jr = new JsonResult();
        //    List<SelectListItem> resultList = new List<SelectListItem>();

        //    if (CategoryID == 0)
        //    {
        //        resultList.Insert(0, new SelectListItem() { Text = defaultText, Value = "0" });
        //        jr.Data = resultList;
        //        return jr;
        //    }

        //    var listColour = TcmCatalogServiceProxy.FindBrand(new Brand() { CategoryID = CategoryID });
        //    if (listColour != null)
        //    {
        //        resultList = listColour.Select(a => new SelectListItem()
        //        {
        //            Text = a.Name,
        //            Value = a.ID.ToString()
        //        }).ToList();
        //    }
        //    resultList.Insert(0, new SelectListItem() { Text = defaultText, Value = "0" });
        //    jr.Data = resultList;
        //    return jr;
        //}

        private List<ModelGroupModelSearchResults> GetModelGroupModel(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            ModelGroupModelFind modelGroupModelFind = new ModelGroupModelFind()
            {
                ModelGroupModel = new ModelGroupModel()
                {
                    ModelID = id
                },
                Active = null
            };

            var mgmIDs = proxy.ModelGroupModelFind(modelGroupModelFind);
            var mgmData = proxy.ModelGroupModelGet(mgmIDs);
            if (mgmData == null)
                return new List<ModelGroupModelSearchResults>();

            var results = mgmData.OrderByDescending(a => a.ID).Select(a => new ModelGroupModelSearchResults()
            {
                ModelGroupModelID = a.ID,
                Code = a.Code,
                Name = a.Name,
                ModelGroup = a.ModelGroupID != 0 ? Util.GetNameByID(RefType.ModelGroup, a.ModelGroupID).DisplayValue() : "N/A",
                Model = a.ModelID != 0 ? Util.GetNameByID(RefType.Model, a.ModelID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = RefType.Model
            }).ToList();

            return results;
        }
        private List<ModelImageSearchResults> GetModelImages(int id)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            ModelImageFind modelImageFind = new ModelImageFind()
            {
                ModelImage = new ModelImage()
                {
                    ModelID = id
                },
                Active = null
            };

            var colourIDs = proxy.ModelImageFind(modelImageFind);
            var colourData = proxy.ModelImageGet(colourIDs);
            if (colourData == null)
                return new List<ModelImageSearchResults>();

            var results = colourData.OrderByDescending(a => a.ID).Select(a => new ModelImageSearchResults()
            {
                ModelImageID = a.ID,
                Colour = a.ColourID != null ? Util.GetNameByID(RefType.Colour, a.ColourID.ToInt()).DisplayValue() : "N/A",
                ImagePath = a.ImagePath != null? a.ImagePath : "N/A",
                Model = a.ModelID != 0 ? Util.GetNameByID(RefType.Model, a.ModelID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = RefType.Model
            }).ToList();
            return results;
        }

    }
}
