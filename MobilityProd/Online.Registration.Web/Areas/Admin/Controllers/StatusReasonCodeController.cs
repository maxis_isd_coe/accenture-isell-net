﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.ConfigSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class StatusReasonCodeController : Controller
    {
        ConfigServiceProxy proxy = new ConfigServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        //public ActionResult Index()
        //{
        //    return GetStatusReasonCodeResult(new StatusReasonCode { StatusID = 0, ReasonCodeID = 0 }, null, false);
        //}
        //[GridAction]
        //public ActionResult IndexPaging(int StatusID = 0, int ReasonCodeID = 0)
        //{
        //    var ResultData = GetStatusReasonCodes(new StatusReasonCode { StatusID = StatusID, ReasonCodeID = ReasonCodeID }, true);
        //    return View(new GridModel<StatusReasonCodeSearchResults>() { Data = ResultData });
        //}
        //[HttpPost]
        //public ActionResult Index(SearchViewModelStatusReasonCode SVMStatusReasonCode)
        //{
        //    try
        //    {
        //        return GetStatusReasonCodeResult(SVMStatusReasonCode, true);
        //    }
        //    catch (Exception e)
        //    {
        //        return RedirectToException(e);
        //    }
        //}
        //private IEnumerable<StatusReasonCodeSearchResults> GetStatusReasonCodes(StatusReasonCode statusReasonCode, bool IsSearch, bool? Active = null)
        //{
        //    int PageSize = Properties.Settings.Default.PageSize;

        //    StatusReasonCodeFind StatusReasonCodeFind = new StatusReasonCodeFind()
        //    {
        //        StatusReasonCode = statusReasonCode,
        //        Active = Active,
        //        PageSize = IsSearch ? 0 : PageSize
        //    };

        //    var IDs = proxy.StatusReasonCodeFind(StatusReasonCodeFind);
        //    var GetData = proxy.StatusReasonCodeGet(IDs);

        //    var results = GetData.OrderByDescending(a => a.ID).Select(a => new StatusReasonCodeSearchResults()
        //    {
        //        StatusReasonCodeID = a.ID,
        //        Status = a.StatusID != 0 ? Util.GetNameByID(RefType.Status, a.StatusID).DisplayValue() : "N/A",
        //        ReasonCode = a.ReasonCodeID != 0 ? Util.GetNameByID(RefType.StatusReason, a.ReasonCodeID) : "N/A",
        //        Remark = a.Remark != null ? a.Remark : "N/A",
        //        ActiveCheckBox = Util.GetCheckBox(a.Active),
        //        CreateDT = Util.FormatDateTime(a.CreateDT),
        //        LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
        //        LastAccessID = a.LastAccessID
        //    });

        //    return results;

        //}
        //private ViewResult GetStatusReasonCodeResult(StatusReasonCode StatusReasonCode, bool? Active, bool IsSearch)
        //{
        //    SearchViewModelStatusReasonCode SVMStatusReasonCode = new SearchViewModelStatusReasonCode
        //    {
        //        SearchResults = GetStatusReasonCodes(StatusReasonCode, IsSearch, Active)
        //    };

        //    return View(SVMStatusReasonCode);
        //}
        //private ViewResult GetStatusReasonCodeResult(SearchViewModelStatusReasonCode SVMStatusReasonCode, bool IsSearch)
        //{
        //    var SearchData = new StatusReasonCode()
        //    {
        //        StatusID = SVMStatusReasonCode.SearchCriterias.StatusID,
        //        ReasonCodeID = SVMStatusReasonCode.SearchCriterias.ReasonCodeID
        //    };
        //    SVMStatusReasonCode.SearchResults = GetStatusReasonCodes(SearchData, IsSearch);
        //    return View(SVMStatusReasonCode);
        //}
        public ActionResult Create(int? statusID, int? statusReasonID, RefType type)
        {
            StatusReasonCodeViewModel model = new StatusReasonCodeViewModel()
            {
                StatusID = statusID.ToInt(),
                StatusReasonID = statusReasonID.ToInt(),
                Type = type,
                StatusReasonCode = new StatusReasonCode()
            };

            return View(model);
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, StatusReasonCodeViewModel addStatusReasonCode)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int statusID = addStatusReasonCode.StatusID == 0 ? addStatusReasonCode.StatusReasonCode.StatusID : addStatusReasonCode.StatusID;
                int reasonCodeID = addStatusReasonCode.StatusReasonID == 0 ? addStatusReasonCode.StatusReasonCode.ReasonCodeID : addStatusReasonCode.StatusReasonID;
                RefType type = addStatusReasonCode.Type;
                
                if (submit == "Save")
                {
                    // Check for duplicate
                    var getData = proxy.StatusReasonCodeGet(proxy.StatusReasonCodeFind(new StatusReasonCodeFind()
                    {
                        StatusReasonCode = new StatusReasonCode { StatusID = statusID, ReasonCodeID = reasonCodeID },
                        Active = true
                    }));

                    if (getData.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Combination of the Status and Reason Code already exist.");
                        StatusReasonCodeViewModel model = new StatusReasonCodeViewModel()
                        {
                            StatusID = statusID.ToInt(),
                            StatusReasonID = reasonCodeID.ToInt(),
                            Type = type
                        };

                        return View(model);
                    }

                    if (addStatusReasonCode.StatusID != 0)
                        addStatusReasonCode.StatusReasonCode.StatusID = addStatusReasonCode.StatusID;

                    if (addStatusReasonCode.StatusReasonID != 0)
                        addStatusReasonCode.StatusReasonCode.ReasonCodeID = addStatusReasonCode.StatusReasonID;

                    addStatusReasonCode.StatusReasonCode.CreateDT = DateTime.Now;
                    addStatusReasonCode.StatusReasonCode.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addStatusReasonCode.StatusReasonCode.Active = true;

                    proxy.StatusReasonCodeCreate(addStatusReasonCode.StatusReasonCode);
                }

                if (type == RefType.Status)
                {
                    return RedirectToAction("Edit", "Status", new { id = statusID });
                }
                else if (type == RefType.StatusReason)
                {
                    return RedirectToAction("Edit", "StatusReason", new { id = reasonCodeID });
                }

                return View();
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        
        public ActionResult Edit(int id, RefType type)
        {
            var data = proxy.StatusReasonCodeGet(new int[] { id }).SingleOrDefault();
            StatusReasonCodeViewModel editStatusReasonCode = new StatusReasonCodeViewModel()
            {
                Type = type,
                StatusReasonCode = data
            };

            return View(editStatusReasonCode);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, StatusReasonCodeViewModel editStatusReasonCode)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int statusID = editStatusReasonCode.StatusReasonCode.StatusID.ToInt() == 0 ? editStatusReasonCode.StatusID : editStatusReasonCode.StatusReasonCode.StatusID.ToInt();
                int reasonCodeID = editStatusReasonCode.StatusReasonCode.ReasonCodeID.ToInt() == 0 ? editStatusReasonCode.StatusID : editStatusReasonCode.StatusReasonCode.ReasonCodeID.ToInt();
                RefType type = editStatusReasonCode.Type;
                var resultID = 0;

                if (submit == "Save")
                {
                    // Check for duplicate
                    if (editStatusReasonCode.StatusReasonCode.Active == true)
                    {
                        var getData = proxy.StatusReasonCodeGet(proxy.StatusReasonCodeFind(new StatusReasonCodeFind()
                        {
                            StatusReasonCode = new StatusReasonCode
                            {
                                StatusID = editStatusReasonCode.StatusReasonCode.StatusID,
                                ReasonCodeID = editStatusReasonCode.StatusReasonCode.ReasonCodeID
                            },
                            Active = true
                        })).ToList();

                        if (getData.Count() > 0)
                            resultID = getData.Select(a => a.ID).SingleOrDefault();

                        if (getData.Count() > 0 && resultID != editStatusReasonCode.StatusReasonCode.ID)
                        {
                            ModelState.AddModelError(string.Empty, "Combination of the Status and Reason Code already exist.");
                            var srcData = proxy.StatusReasonCodeGet(new int[] { editStatusReasonCode.StatusReasonCode.ID }).SingleOrDefault();
                            StatusReasonCodeViewModel model = new StatusReasonCodeViewModel()
                            {
                                Type = editStatusReasonCode.Type,
                                StatusReasonCode = srcData
                            };
                            return View(model);
                        }
                    }

                    editStatusReasonCode.StatusReasonCode.LastUpdateDT = DateTime.Now;
                    editStatusReasonCode.StatusReasonCode.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.StatusReasonCodeUpdate(editStatusReasonCode.StatusReasonCode);
                }

                if (type == RefType.Status)
                {
                    return RedirectToAction("Edit", "Status", new { id = statusID });
                }
                else if (type == RefType.StatusReason)
                {
                    return RedirectToAction("Edit", "StatusReason", new { id = reasonCodeID });
                }

                return View();

            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }

    }
}
