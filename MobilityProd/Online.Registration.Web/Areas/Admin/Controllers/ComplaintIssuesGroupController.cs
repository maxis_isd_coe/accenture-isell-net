﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.ComplainSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ComplaintIssuesGroupController : Controller
    {
        ComplainServiceProxy proxy = new ComplainServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Create(int id, string type)
        {
            ComplaintIssuesGroupViewModel complaintIssuesGroupVM = new ComplaintIssuesGroupViewModel()
            {
                ComplaintIssueID = id,
                Type = type,
                ComplaintIssuesGroup = new ComplaintIssuesGroup()
            };
            return View(complaintIssuesGroupVM);
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, ComplaintIssuesGroupViewModel addCIGVM)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                string type = addCIGVM.Type;
                int complaintIssuesID = addCIGVM.ComplaintIssueID;
                int productID = (type == Properties.Settings.Default.ComplaintIssues_Product) ? complaintIssuesID : addCIGVM.ComplaintIssuesGroup.ProductID;
                int reason1ID = (type == Properties.Settings.Default.ComplaintIssues_Reason1) ? complaintIssuesID : addCIGVM.ComplaintIssuesGroup.Reason1ID;
                int reason2ID = (type == Properties.Settings.Default.ComplaintIssues_Reason2) ? complaintIssuesID : addCIGVM.ComplaintIssuesGroup.Reason2ID;
                int reason3ID = (type == Properties.Settings.Default.ComplaintIssues_Reason3) ? complaintIssuesID : addCIGVM.ComplaintIssuesGroup.Reason3ID;

                ComplaintIssuesGroupViewModel complaintIssuesGroupVM = new ComplaintIssuesGroupViewModel()
                {
                    ComplaintIssueID = complaintIssuesID,
                    Type = addCIGVM.Type
                };
                if(submit == "Save")
                {
                    var getData = proxy.ComplaintIssuesGroupGet(proxy.ComplaintIssuesGroupFind(new ComplaintIssuesGroupFind()
                        {
                            ComplaintIssuesGroup = new ComplaintIssuesGroup()
                            {
                                ProductID = productID,
                                Reason1ID = reason1ID,
                                Reason2ID = reason2ID,
                                Reason3ID = reason3ID
                            },
                            Active = true
                        })).ToList();
                    if(getData.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Combination of Product, Reason 1, Reason 2, and Reason 3 already exist.");
                        return View(complaintIssuesGroupVM);
                    }

                    addCIGVM.ComplaintIssuesGroup.CreateDT = DateTime.Now;
                    addCIGVM.ComplaintIssuesGroup.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addCIGVM.ComplaintIssuesGroup.ProductID = productID;
                    addCIGVM.ComplaintIssuesGroup.Reason1ID = reason1ID;
                    addCIGVM.ComplaintIssuesGroup.Reason2ID = reason2ID;
                    addCIGVM.ComplaintIssuesGroup.Reason3ID = reason3ID;
                    addCIGVM.ComplaintIssuesGroup.Active = true;

                    proxy.ComplaintIssuesGroupCreate(addCIGVM.ComplaintIssuesGroup);
                }

                return RedirectToAction("Edit", "ComplaintIssues", new { id = complaintIssuesID });
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        public ActionResult Edit(int id, string type)
        {
            var data = proxy.ComplaintIssuesGroupGet(new int[] { id }).SingleOrDefault();

            ComplaintIssuesGroupViewModel model = new ComplaintIssuesGroupViewModel()
            {
                Type = type,
                ComplaintIssuesGroup = data,
                IssuesGroupDispatchQ = GetIssuesGroupDispatchQ(id, type)
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, ComplaintIssuesGroupViewModel editCIGVM)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                string type = editCIGVM.Type;
                int complaintIssuesGroupID = 0;
                if (type == Properties.Settings.Default.ComplaintIssues_Product)
                    complaintIssuesGroupID = editCIGVM.ComplaintIssuesGroup.ProductID;
                if (type == Properties.Settings.Default.ComplaintIssues_Reason1)
                    complaintIssuesGroupID = editCIGVM.ComplaintIssuesGroup.Reason1ID;
                if (type == Properties.Settings.Default.ComplaintIssues_Reason2)
                    complaintIssuesGroupID = editCIGVM.ComplaintIssuesGroup.Reason2ID;
                if (type == Properties.Settings.Default.ComplaintIssues_Reason3)
                    complaintIssuesGroupID = editCIGVM.ComplaintIssuesGroup.Reason3ID;
                if (submit == "Save")
                {
                    if(editCIGVM.ComplaintIssuesGroup.Active == true)
                    {
                        var getData = proxy.ComplaintIssuesGroupGet(proxy.ComplaintIssuesGroupFind(new ComplaintIssuesGroupFind()
                        {
                            ComplaintIssuesGroup = new ComplaintIssuesGroup()
                            {
                                ProductID = editCIGVM.ComplaintIssuesGroup.ProductID,
                                Reason1ID = editCIGVM.ComplaintIssuesGroup.Reason1ID,
                                Reason2ID = editCIGVM.ComplaintIssuesGroup.Reason2ID,
                                Reason3ID = editCIGVM.ComplaintIssuesGroup.Reason3ID
                            },
                            Active = true
                        })).ToList();

                        var resultID = getData.Select(a => a.ID).SingleOrDefault();
                        if (getData.Count() > 0 && editCIGVM.ComplaintIssuesGroup.ID != resultID)
                        {
                            var returnData = proxy.ComplaintIssuesGroupGet(new int[] { editCIGVM.ComplaintIssuesGroup.ID }).SingleOrDefault();
                            ComplaintIssuesGroupViewModel model = new ComplaintIssuesGroupViewModel()
                            {
                                Type = type,
                                ComplaintIssuesGroup = returnData,
                                IssuesGroupDispatchQ = GetIssuesGroupDispatchQ(editCIGVM.ComplaintIssuesGroup.ID, editCIGVM.Type)
                            };

                            ModelState.AddModelError(string.Empty, "Combination of Product, Reason 1, Reason 2, and Reason 3 already exist.");
                            return View(model);
                        }
                    }
                    editCIGVM.ComplaintIssuesGroup.LastUpdateDT = DateTime.Now;
                    editCIGVM.ComplaintIssuesGroup.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.ComplaintIssuesGroupUpdate(editCIGVM.ComplaintIssuesGroup);
                }
                return RedirectToAction("Edit", "ComplaintIssues", new { id = complaintIssuesGroupID });
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
        }
        private List<IssuesGroupDispatchQSearchResults> GetIssuesGroupDispatchQ(int id, string type)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            IssueGrpDispatchQFind dataFind = new IssueGrpDispatchQFind()
            {
                Active = null,
                IssueGrpDispatchQ = null,
                ComplaintIssuesGrpIDs = new List<int> {  id }
            };

            var IDs = proxy.IssueGrpDispatchQFind(dataFind);
            if (IDs == null)
                return new List<IssuesGroupDispatchQSearchResults>();

            var getData = proxy.IssueGrpDispatchQGet(IDs);
            if (getData == null)
                return new List<IssuesGroupDispatchQSearchResults>();

            var results = getData.OrderByDescending(a => a.ID).Select(a => new IssuesGroupDispatchQSearchResults()
            {
                IssueGroupDispatchQID = a.ID,
                CaseDispatchQ = a.CaseDispatchQID != 0 ? Util.GetNameByID(RefType.ComplaintIssues, a.CaseDispatchQID).DisplayValue() : "N/A",
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID,
                Type = type
            }).ToList();
            return results;
        }
        
    }
}
