﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.UserSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class UserGroupAccessController : Controller
    {
        UserServiceProxy proxy = new UserServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception ex)
        {
            Util.SetSessionErrMsg(ex);
            return RedirectToAction("Error", "AdminHome", new { errCode = "xxx", type = "Admin" });
        }
        public ActionResult Create(int? userGroupID, int? accessID)
        {
            UserGroupAccessViewModel model = new UserGroupAccessViewModel()
            {
                UserGroupID = userGroupID.ToInt(),
                AccessID = accessID.ToInt(),
                UserGroupAccess = new UserGrpAccess()
            };

            return View(model);
        }
        [HttpPost]
        public ActionResult Create(UserGroupAccessViewModel addUserGroupAccess, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int userGroupID = addUserGroupAccess.UserGroupID == 0 ? addUserGroupAccess.UserGroupAccess.UserGrpID : addUserGroupAccess.UserGroupID;
                int accessID = addUserGroupAccess.AccessID == 0 ? addUserGroupAccess.UserGroupAccess.AccessID : addUserGroupAccess.AccessID;
                RefType type = addUserGroupAccess.Type;

                UserGroupAccessViewModel userGroupAccessVM = new UserGroupAccessViewModel()
                {
                    UserGroupID = type == RefType.UserGroup ? userGroupID : 0,
                    AccessID = type == RefType.Access ? accessID : 0
                };

                if (submit == "Save")
                {                    
                    // Check for duplicate
                    var getData = proxy.UserGroupAccessGet(proxy.UserGroupAccessFind(new UserGrpAccessFind()
                    {
                        UserGrpAccess = new UserGrpAccess() { UserGrpID = userGroupID, AccessID = accessID },
                        Active = true
                    }));

                    if (getData.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Combination of Access and User Group already exist.");
                        return View(userGroupAccessVM);
                    }

                    if (addUserGroupAccess.UserGroupID != 0)
                        addUserGroupAccess.UserGroupAccess.UserGrpID = addUserGroupAccess.UserGroupID;

                    if (addUserGroupAccess.AccessID != 0)
                        addUserGroupAccess.UserGroupAccess.AccessID = addUserGroupAccess.AccessID;

                    addUserGroupAccess.UserGroupAccess.CreateDT = DateTime.Now;
                    addUserGroupAccess.UserGroupAccess.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addUserGroupAccess.UserGroupAccess.Active = true;

                    proxy.UserGroupAccessCreate(addUserGroupAccess.UserGroupAccess);
                }
                if (type == RefType.UserGroup)
                {
                    return RedirectToAction("Edit", "UserGroup", new { id = userGroupID });
                }
                else if (type == RefType.Access)
                {
                    return RedirectToAction("Edit", "Access", new { id = accessID });
                }

                return View(userGroupAccessVM);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
        public ActionResult Edit(int id, RefType type)
        {
            var data = proxy.UserGroupAccessGet(new int[] { id }).SingleOrDefault();
            UserGroupAccessViewModel editUserGroupAccess = new UserGroupAccessViewModel()
            {
                Type = type,
                UserGroupAccess = data
            };

            return View(editUserGroupAccess);
        }
        [HttpPost]
        public ActionResult Edit(UserGroupAccessViewModel editUserGroupAccess, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                int userGroupID = editUserGroupAccess.UserGroupAccess.UserGrpID.ToInt();
                int accessID = editUserGroupAccess.UserGroupAccess.AccessID.ToInt();
                RefType type = editUserGroupAccess.Type;

                var returnData = proxy.UserGroupAccessGet(new int[] { editUserGroupAccess.UserGroupAccess.ID }).SingleOrDefault();
                UserGroupAccessViewModel userGroupAccessVM = new UserGroupAccessViewModel()
                {
                    Type = type,
                    UserGroupAccess = returnData
                };

                if (submit == "Save")
                {
                    if (editUserGroupAccess.UserGroupAccess.Active == true)
                    {
                        // check for duplicate
                        var getData = proxy.UserGroupAccessGet(proxy.UserGroupAccessFind(new UserGrpAccessFind()
                        {
                            UserGrpAccess = new UserGrpAccess() 
                            {
                                UserGrpID = editUserGroupAccess.UserGroupAccess.UserGrpID,
                                AccessID = editUserGroupAccess.UserGroupAccess.AccessID
                            },
                            Active = true
                        })).ToList();

                        var resultID = getData.Select(a => a.ID).SingleOrDefault();

                        if (getData.Count() > 0 && resultID != editUserGroupAccess.UserGroupAccess.ID)
                        {
                            ModelState.AddModelError(string.Empty, "User Access and User Group already exist.");
                            return View(userGroupAccessVM);
                        }
                    }

                    editUserGroupAccess.UserGroupAccess.LastUpdateDT = DateTime.Now;
                    editUserGroupAccess.UserGroupAccess.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.UserGroupAccessUpdate(editUserGroupAccess.UserGroupAccess);
                }
                if (type == RefType.UserGroup)
                {
                    return RedirectToAction("Edit", "UserGroup", new { id = userGroupID });
                }
                else if (type == RefType.Access)
                {
                    return RedirectToAction("Edit", "Access", new { id = accessID });
                }

                return View(userGroupAccessVM);
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }

    }
}
