﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class NationalityController : Controller
    {
        RegistrationServiceProxy proxy = new RegistrationServiceProxy();
        public RedirectToRouteResult RedirectToExeption(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Index()
        {
            return GetNationalityResult(new Nationality { Code = string.Empty, Name = string.Empty}, null, false);
        }
        [HttpPost]
        public ActionResult Index(SearchViewModelNationality SVMNationality)
        {
            try
            {
                return GetNationalityResult(SVMNationality, true);
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        private IEnumerable<NationalitySearchResults> GetNationality(Nationality Nationality, bool IsSearch, bool? Active = null)
        {
            int PageSize = Properties.Settings.Default.PageSize;

            NationalityFind NationalityFind = new  NationalityFind()
            {
                Nationality = Nationality,
                Active = Active,
                PageSize = IsSearch ? 0 : PageSize
            };

            var IDs = proxy.NationalityFind(NationalityFind);
            var Nationalities = proxy.NationalityGet(IDs);

            var results = Nationalities.OrderByDescending(a => a.ID).Select(a => new NationalitySearchResults()
            {
                NationalityID = a.ID,
                Code = a.Code,
                Name = a.Name,
                ActiveCheckBox = Util.GetCheckBox(a.Active),
                CreateDT = Util.FormatDateTime(a.CreateDT),
                LastUpdateDT = Util.FormatDateTime(a.LastUpdateDT),
                LastAccessID = a.LastAccessID
            });

            return results;

        }
        private ViewResult GetNationalityResult(Nationality Nationality, bool? Active, bool IsSearch)
        {
            SearchViewModelNationality SVMNationality = new SearchViewModelNationality
            {
                SearchResults = GetNationality(Nationality, IsSearch, Active)
            };

            return View(SVMNationality);
        }
        private ViewResult GetNationalityResult(SearchViewModelNationality SVMNationality, bool IsSearch)
        {
            var NationalityData = new Nationality()
            {
                Code = SVMNationality.SearchCriterias.Code,
                Name = SVMNationality.SearchCriterias.Name
            };
            SVMNationality.SearchResults = GetNationality(NationalityData, IsSearch);
            return View(SVMNationality);
        }
        public ActionResult Create()
        {
            return View(new Nationality());
        }
        [HttpPost]
        public ActionResult Create(FormCollection collection, Nationality addNatlData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    var NationalityGet = proxy.NationalityGet(proxy.NationalityFind(new NationalityFind()
                        {
                            Nationality = new Nationality { Code = addNatlData.Code },
                            Active = true
                        }));

                    if (NationalityGet.Count() > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Nationality Code already  exists.");
                        return View();
                    }

                    addNatlData.CreateDT = DateTime.Now;
                    addNatlData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addNatlData.Active = true;

                    NationalityCreateResp oRp = new NationalityCreateResp();
                    oRp = proxy.NationalityCreate(addNatlData);

                    if (oRp.Code == "-1")
                    {
                        if (oRp.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                        {
                            ModelState.AddModelError(string.Empty, string.Format("Nationality Code ({0}) already exist!", addNatlData.Code));
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Fail to add new Nationality Code: " + oRp.Message);
                        }
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
        public ActionResult Edit(int id)
        {
            var NationalityData = proxy.NationalityGet(new int[] { id }).SingleOrDefault();
            return View(NationalityData);
        }
        [HttpPost]
        public ActionResult Edit(FormCollection collection, Nationality editNatlData)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                if (submit == "Save")
                {
                    // Check for duplicate
                    var getData = proxy.NationalityGet(proxy.NationalityFind(new NationalityFind()
                    {
                        Nationality = new Nationality { Code = editNatlData.Code },
                        Active = true
                    })).ToList();

                    var resultID = getData.Select(a => a.ID).SingleOrDefault();

                    if (getData.Count() > 0 && resultID != editNatlData.ID)
                    {
                        ModelState.AddModelError(string.Empty, "Nationality Code already  exists.");
                        var model = proxy.NationalityGet(new int[] { editNatlData.ID }).SingleOrDefault();
                        return View(model);
                    }

                    editNatlData.LastUpdateDT = DateTime.Now;
                    editNatlData.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.NationalityUpdate(editNatlData);
                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                return RedirectToExeption(e);
            }
        }
    }
}
