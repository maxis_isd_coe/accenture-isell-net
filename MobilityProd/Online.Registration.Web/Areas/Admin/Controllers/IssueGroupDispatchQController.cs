﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SNT.Utility;
using Telerik.Web.Mvc;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Admin.Models;
using Online.Registration.Web.ComplainSvc;

namespace Online.Registration.Web.Areas.Admin.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class IssueGroupDispatchQController : Controller
    {
        ComplainServiceProxy proxy = new ComplainServiceProxy();
        public RedirectToRouteResult RedirectToException(Exception e)
        {
            Util.SetSessionErrMsg(e);
            return RedirectToAction("Error", "AdminHome", new { ErrCode = "xxx", type = "Admin" });
        }
        public ActionResult Create(int id, string type)
        {
            var complaintIssuesGrp = proxy.ComplaintIssuesGroupGet(new int[] { id }).SingleOrDefault();
            IssuesGroupDispatchQViewModel IssuesGroupDispatchQVM = new IssuesGroupDispatchQViewModel()
            {
                ComplaintIssueGroupID = id,
                Type = type,
                ComplaintIssuesGroup = complaintIssuesGrp,
                IssueGrpDispatchQ = new IssueGrpDispatchQ()
            };
            return View(IssuesGroupDispatchQVM);
        }
        [HttpPost]
        public ActionResult Create(IssuesGroupDispatchQViewModel addIssueGrpDispatchQ, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();
                addIssueGrpDispatchQ.IssueGrpDispatchQ.Active = true;
                var cigData = proxy.ComplaintIssuesGroupGet(new int[] { addIssueGrpDispatchQ.ComplaintIssueGroupID }).SingleOrDefault();
                IssuesGroupDispatchQViewModel model = new IssuesGroupDispatchQViewModel()
                {
                    ComplaintIssueGroupID = addIssueGrpDispatchQ.ComplaintIssueGroupID,
                    Type = addIssueGrpDispatchQ.Type,
                    ComplaintIssuesGroup = cigData
                };

                if (submit == "Save")
                {
                    if (addIssueGrpDispatchQ.IssueGrpDispatchQ.CaseDispatchQID == 0)
                        return View(model);

                    var getData = proxy.IssueGrpDispatchQGet(proxy.IssueGrpDispatchQFind(new IssueGrpDispatchQFind()
                    {
                        Active = true,
                        ComplaintIssuesGrpIDs = new List<int>(),
                        IssueGrpDispatchQ = new IssueGrpDispatchQ()
                        {
                            IssueGrpID = addIssueGrpDispatchQ.ComplaintIssueGroupID,
                            CaseDispatchQID = addIssueGrpDispatchQ.IssueGrpDispatchQ.CaseDispatchQID
                        }
                    })).ToList();

                    if (getData.Count() > 0)
                    {
                        var complaintIssuesGrp = proxy.ComplaintIssuesGroupGet(new int[] { addIssueGrpDispatchQ.ComplaintIssueGroupID }).SingleOrDefault();
                        IssuesGroupDispatchQViewModel IssuesGroupDispatchQVM = new IssuesGroupDispatchQViewModel()
                        {
                            ComplaintIssueGroupID = addIssueGrpDispatchQ.ComplaintIssueGroupID,
                            Type = addIssueGrpDispatchQ.Type,
                            ComplaintIssuesGroup = complaintIssuesGrp
                        };
                        ModelState.AddModelError(string.Empty, "Combination of Issue Group and Case Dispatch Queue already exist.");
                        return View(IssuesGroupDispatchQVM);
                    }

                    addIssueGrpDispatchQ.IssueGrpDispatchQ.IssueGrpID = addIssueGrpDispatchQ.ComplaintIssueGroupID;
                    addIssueGrpDispatchQ.IssueGrpDispatchQ.CreateDT = DateTime.Now;
                    addIssueGrpDispatchQ.IssueGrpDispatchQ.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;
                    addIssueGrpDispatchQ.IssueGrpDispatchQ.TranxID = 1;
                    addIssueGrpDispatchQ.IssueGrpDispatchQ.IntTranxID = null;

                    proxy.IssueGrpDispatchQCreate(addIssueGrpDispatchQ.IssueGrpDispatchQ);
                }
                return RedirectToAction("Edit", "ComplaintIssuesGroup", new { id = addIssueGrpDispatchQ.ComplaintIssueGroupID, type = addIssueGrpDispatchQ.Type });
            }
            catch (Exception ex)
            {
                return RedirectToException(ex);
            }
        }
       
        public ActionResult Edit(int id, string type)
        {
            var issueGrpDispatchQ = proxy.IssueGrpDispatchQGet(new int[] { id }).SingleOrDefault();
            var issueGrpID = issueGrpDispatchQ.IssueGrpID;
            var complaintIssueGrp = proxy.ComplaintIssuesGroupGet(new int[] { issueGrpID }).SingleOrDefault();
            IssuesGroupDispatchQViewModel IssuesGroupDispatchQVM = new IssuesGroupDispatchQViewModel() 
            { 
                Type = type,
                IssueGrpDispatchQ = issueGrpDispatchQ,
                ComplaintIssuesGroup = complaintIssueGrp
            };
            return View(IssuesGroupDispatchQVM);
        }
        [HttpPost]
        public ActionResult Edit(IssuesGroupDispatchQViewModel editIssuesGrpDispatchQ, FormCollection collection)
        {
            try
            {
                string submit = collection["submit"].ToString2();

                if (submit == "Save")
                {
                    if (editIssuesGrpDispatchQ.IssueGrpDispatchQ.Active == true)
                    {
                        var getData = proxy.IssueGrpDispatchQGet(proxy.IssueGrpDispatchQFind(new IssueGrpDispatchQFind()
                        {
                            Active = true,
                            ComplaintIssuesGrpIDs = new List<int>(),
                            IssueGrpDispatchQ = new IssueGrpDispatchQ()
                            {
                                IssueGrpID = editIssuesGrpDispatchQ.IssueGrpDispatchQ.IssueGrpID,
                                CaseDispatchQID = editIssuesGrpDispatchQ.IssueGrpDispatchQ.CaseDispatchQID
                            }
                        })).ToList();
                        var resultID = getData.Select(a => a.ID).SingleOrDefault();

                        if (getData.Count() > 0 && editIssuesGrpDispatchQ.IssueGrpDispatchQ.ID != resultID)
                        {
                            var issueGrpDispatchQ = proxy.IssueGrpDispatchQGet(new int[] { editIssuesGrpDispatchQ.IssueGrpDispatchQ.ID }).SingleOrDefault();
                            var issueGrpID = issueGrpDispatchQ.IssueGrpID;
                            var complaintIssueGrp = proxy.ComplaintIssuesGroupGet(new int[] { issueGrpID }).SingleOrDefault();
                            IssuesGroupDispatchQViewModel IssuesGroupDispatchQVM = new IssuesGroupDispatchQViewModel()
                            {
                                Type = editIssuesGrpDispatchQ.Type,
                                IssueGrpDispatchQ = issueGrpDispatchQ,
                                ComplaintIssuesGroup = complaintIssueGrp
                            };
                            ModelState.AddModelError(string.Empty, "Combination of Issue Group and Case Dispatch Queue already exist.");
                            return View(IssuesGroupDispatchQVM);
                        }
                    }

                    editIssuesGrpDispatchQ.IssueGrpDispatchQ.LastUpdateDT = DateTime.Now;
                    editIssuesGrpDispatchQ.IssueGrpDispatchQ.LastAccessID = Util.SessionAccess == null ? "System" : Util.SessionAccess.UserName;

                    proxy.IssueGrpDispatchQUpdate(editIssuesGrpDispatchQ.IssueGrpDispatchQ);
                }
                return RedirectToAction("Edit", "ComplaintIssuesGroup", new { id = editIssuesGrpDispatchQ.IssueGrpDispatchQ.IssueGrpID, type = editIssuesGrpDispatchQ.Type });
            }
            catch (Exception e)
            {
                return RedirectToException(e);
            }
            
        }

    }
}
