﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Web.Security;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using System.ComponentModel.DataAnnotations;

namespace Online.Registration.Admin.Models
{

    #region Bundle
    public class SearchViewBundleModel
    {
        public SearchViewBundleModel()
        {
            SearchCriterias = new SearchBundle();
        }
        public class SearchBundle
        {
            [Display(Name = "Bundle Code")]
            public string Code { get; set; }
            [Display(Name = "Bundle Name")]
            public string Name { get; set; }
            [Display(Name = "Program")]
            public int ProgramID { get; set; }
        }
        public SearchBundle SearchCriterias { get; set; }
        public IEnumerable<BundleSearchResults> SearchResults { get; set; }
    }
    public class UpdateViewBundleModel
    {
        public Bundle Bundle { get; set; }
        public IEnumerable<PgmBdlPckComponentSearchResults> PgmBdlPckComponents { get; set; }
        public IEnumerable<PgmBdlPckComponentSearchResults> PgmBdl { get; set; }
    }

    public class BundleSearchResults
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int ProgramID { get; set; }
        public int BundleID { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    #endregion

    #region Category
    public class SearchViewModelCategory
    {
        public SearchViewModelCategory()
        {
            SearchCriterias = new SearchCategories();
        }
        public SearchCategories SearchCriterias { get; set; }
        public IEnumerable<CategorySearchResults> SearchResults { get; set; }
    }
    public class CategorySearchResults
    {
        public int CategoryID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ActiveCheckBox { get; set; }
        public string LastUpdateDT { get; set; }
        public string CreateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchCategories
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
    #endregion

    #region Colour
    public class SearchViewModelColour
    {
        public SearchViewModelColour()
        {
            SearchCriterias = new SearchColours();
        }
        public SearchColours SearchCriterias { get; set; }
        public IEnumerable<ColourSearchResults> SearchResults { get; set; }
    }
    public class ColourSearchResults
    {
        public int ColourID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Model { get; set; }
        public string ActiveCheckBox { get; set; }
        public string LastUpdateDT { get; set; }
        public string CreateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchColours
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Model")]
        public int ModelID { get; set; }
    }
    public class UpdateViewColourModel
    {
        public Colour Colour { get; set; }
        public List<ModelImageSearchResults> ModelImageSearchResults { get; set; }
    }
    #endregion

    #region Component
    public class SearchViewComponentModel
    {
        public SearchViewComponentModel()
        {
            SearchCriterias = new SearchComponent();
        }
        public SearchComponent SearchCriterias { get; set; }
        public class SearchComponent
        {
            [Display(Name = "Code")]
            [MaxLength(20)]
            public string Code { get; set; }

            [Display(Name = "Name")]
            [MaxLength(100)]
            public string Name { get; set; }

            [Display(Name = "Component Type")]
            public int ComponentTypeID { get; set; }
        }
        public IEnumerable<ComponentSearchResults> SearchResults { get; set; }
    }
    public class ComponentSearchResults
    {
        public int ComponentID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ComponentType { get; set; }
        public int ComponentTypeID { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class UpdateViewComponentModel
    {
        public Component Component { get; set; }
        public List<PgmBdlPckComponentSearchResults> PgmBdlPckComponents { get; set; }
    }

    #endregion

    #region ComponentType
    public class SearchViewComponentTypeModel
    {
        public SearchViewComponentTypeModel()
        {
            SearchCriterias = new SearchComponentType();
        }
        public SearchComponentType SearchCriterias { get; set; }
        public class SearchComponentType
        {
            [Display(Name = "Code")]
            [MaxLength(20)]
            public string Code { get; set; }

            [Display(Name = "Name")]
            [MaxLength(100)]
            public string Name { get; set; }
        }
        public IEnumerable<ComponentTypeSearchResults> SearchResults { get; set; }
    }
    public class ComponentTypeSearchResults
    {
        public int ComponentTypeID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class UpdateViewComponentTypeModel
    {
        public ComponentType ComponentType { get; set; }
        public List<PgmBdlPckComponentSearchResults> PgmBdlPckComponents { get; set; }
    }

    #endregion

    #region Item Price

    public class SearchViewModelItemPrice
    {
        public SearchViewModelItemPrice()
        {
            SearchCriterias = new SearchItemPrices();
        }
        public SearchItemPrices SearchCriterias { get; set; }
        public IEnumerable<ItemPriceSearchResults> SearchResults { get; set; }
    }
    public class ItemPriceSearchResults
    {
        public int ItemPriceID { get; set; }
        public string BdlPkgID { get; set; }
        public string PkgCompID { get; set; }
        public string Model { get; set; }
        public string Price { get; set; }
        public string ActiveCheckBox { get; set; }
        public string LastUpdateDT { get; set; }
        public string CreateDT { get; set; }
        public string LastAccessID { get; set; }
        public string Type { get; set; }
    }
    public class SearchItemPrices
    {
        [Display(Name = "Bundle Package")]
        public string BdlPkgID { get; set; }
        [Display(Name = "Package Component")]
        public string PkgCompID { get; set; }
        [Display(Name = "Model")]
        public int Model { get; set; }
    }
    public class ItemPriceViewModel
    {
        public int BundlePackageID { get; set; }
        public int PackageCompID { get; set; }
        public string Type { get; set; }
        public ItemPrice ItemPrice { get; set; }
    }

    #endregion

    #region Model
    public class SearchViewModelModel
    {
        public SearchViewModelModel()
        {
            SearchCriterias = new SearchModels();
        }
        public SearchModels SearchCriterias { get; set; }
        public IEnumerable<ModelSearchResults> SearchResults { get; set; }
    }
    public class ModelSearchResults
    {
        public int ModelID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public string ActiveCheckBox { get; set; }
        public string LastUpdateDT { get; set; }
        public string CreateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchModels
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Brand")]
        public int BrandID { get; set; }
    }
    public class UpdateViewModel
    {
        public Model Model { get; set; }
        public List<ModelGroupModelSearchResults> ModelGroupModel { get; set; }
        public List<ModelImageSearchResults> ModelImage { get; set; }
    }
    #endregion

    #region Model Group
    public class SearchViewModelGroupModel
    {
        public SearchViewModelGroupModel()
        {
            SearchCriterias = new SearchModelGroup();
        }
        public class SearchModelGroup
        {
            [Display(Name = "Code")]
            [MaxLength(20)]
            public string Code { get; set; }

            [Display(Name = "Name")]
            [MaxLength(100)]
            public string Name { get; set; }
        }
        public IEnumerable<ModelGroupSearchResults> SearchResults { get; set; }
        public SearchModelGroup SearchCriterias { get; set; }
    }
    public class ModelGroupSearchResults
    {
        public int ModelGroupID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastAccessID { get; set; }
        public string LastUpdateDT { get; set; }
    }
    public class UpdateViewModelGroup
    {
        public ModelGroup ModelGroup { get; set; }
        public List<ModelGroupModelSearchResults> ModelGroupModel { get; set; }
    }
    #endregion

    #region Package
    public class SearchViewPackageModel
    {
        public SearchViewPackageModel()
        {
            SearchCriterias = new SearchPackage();
        }
        public SearchPackage SearchCriterias { get; set; }
        public class SearchPackage
        {
            [Display(Name = "Package Code")]
            [MaxLength(20)]
            public string Code { get; set; }

            [Display(Name = "Package Name")]
            [MaxLength(100)]
            public string Name { get; set; }

            [Display(Name = "Package Type")]
            public int PackageTypeID { get; set; }
        }
        public IEnumerable<PackageSearchResults> SearchResults { get; set; }
    }
    public class PackageSearchResults
    {
        public int PackageID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string PackageType { get; set; }
        public int PackageTypeID { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class UpdateViewPackageModel
    {
        public Package Package { get; set; }
        public List<PgmBdlPckComponentSearchResults> PgmBdlPckComponents { get; set; }
        public List<PgmBdlPckComponentSearchResults> BdlPkg { get; set; }
    }

    #endregion

    #region Package Type
    public class SearchViewPackageTypeModel
    {
        public SearchViewPackageTypeModel()
        {
            SearchCriterias = new SearchPackageType();
        }
        public class SearchPackageType
        {
            [Display(Name = "Code")]
            [MaxLength(20)]
            public string Code { get; set; }

            [Display(Name = "Name")]
            [MaxLength(100)]
            public string Name { get; set; }
        }
        public IEnumerable<PackageTypeSearchResults> SearchResults { get; set; }
        public SearchPackageType SearchCriterias { get; set; }
    }
    public class PackageTypeSearchResults
    {
        public int PackageTypeID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastAccessID { get; set; }
        public string LastUpdateDT { get; set; }
    }
    #endregion
    
    #region Program
        public class SearchViewProgramModel
        {
            public SearchViewProgramModel()
            {
                SearchCriterias = new SearchProgram();
            }
            public SearchProgram SearchCriterias { get; set; }
            public class SearchProgram
            {
                [Display(Name = "Program Code")]
                [MaxLength(20)]
                public string Code { get; set; }

                [Display(Name = "Program Name")]
                [MaxLength(100)]
                public string Name { get; set; }
            }
            public IEnumerable<ProgramSearchResults> SearchResults { get; set; }
        }
        public class UpdateViewProgramModel
        {
            public Program Program { get; set; }
            public List<PgmBdlPckComponentSearchResults> PgmBdlPckComponents { get; set; }
        }
        public class ProgramSearchResults
        {
            public int ProgramID { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
            public string CreateDT { get; set; }
            public string LastUpdateDT { get; set; }
            public string LastAccessID { get; set; }
            public string ActiveCheckBox { get; set; }
        }
        #endregion

    #region PgmBdlPkgComponent
        public class SearchViewPgmBdlPckComponentModel
        {
            public SearchViewPgmBdlPckComponentModel()
            {
                SearchCriterias = new SearchPgmBdlPckComponents();
            }
            public SearchPgmBdlPckComponents SearchCriterias { get; set; }
            public IEnumerable<PgmBdlPckComponent> SearchResults { get; set; }
        }
        public class SearchPgmBdlPckComponents
        {
            [Display(Name = "Program")]
            public string ProgramID { get; set; }
            [Display(Name = "Bundle")]
            public string BundleID { get; set; }
            [Display(Name = "Package")]
            public string PackageID { get; set; }
            [Display(Name = "Component")]
            public string ComponentID { get; set; }
        }
        public class PgmBdlPckComponentSearchResults
        {
            public int PgmBdlPckComponentID { get; set; }
            public int ProgramID { get; set; }
            public string Program { get; set; }
            public int BundleID { get; set; }
            public string Bundle { get; set; }
            public int PackageID { get; set; }
            public string Package { get; set; }
            public int ComponentID { get; set; }
            public string Component { get; set; }
            public string ActiveCheckBox { get; set; }
            public string LastUpdateDT { get; set; }
            public string LastAccessID { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public string CreateDT { get; set; }
            public RefType Type { get; set; }
        }
        public class PgmBdlPkgComponentViewModel
        {
            public int ProgramID { get; set; }
            public int BundleID { get; set; }
            public int PackageID { get; set; }
            public int ComponentID { get; set; }
            public RefType Type { get; set; }
            public PgmBdlPckComponent PgmBdlPckComponent { get; set; }
        }

        public class EditViewPgmBdlPckComponentModel
        {
            public RefType Type { get; set; }
            public PgmBdlPkgComponentViewModel PgmBdlPckComponent { get; set; }
            public IEnumerable<ItemPriceSearchResults> ItemPrices { get; set; }
        }
        #endregion
    
    #region Brand
        public class SearchViewModelBrand
        {
            public SearchViewModelBrand()
            {
                SearchCriterias = new SearchBrands();
            }
            public SearchBrands SearchCriterias { get; set; }
            public IEnumerable<BrandSearchResults> SearchResults { get; set; }
        }
        public class BrandSearchResults
        {
            public int BrandID { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string ImagePath { get; set; }
            public string Category { get; set; }
            public string ActiveCheckBox { get; set; }
            public string LastUpdateDT { get; set; }
            public string CreateDT { get; set; }
            public string LastAccessID { get; set; }
        }
        public class SearchBrands
        {
            [Display(Name = "Code")]
            public string Code { get; set; }
            [Display(Name = "Name")]
            public string Name { get; set; }
        }
        #endregion

    #region Model Group Model
        public class SearchViewModelModelGroupModel
        {
            public SearchViewModelModelGroupModel()
            {
                SearchCriterias = new SearchModelGroupModel();
            }
            public SearchModelGroupModel SearchCriterias { get; set; }
            public IEnumerable<ModelGroupModelSearchResults> SearchResults { get; set; }
        }
        public class ModelGroupModelSearchResults
        {
            public int ModelGroupModelID { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
            public string Model { get; set; }
            public string ModelGroup { get; set; }
            public string ActiveCheckBox { get; set; }
            public string CreateDT { get; set; }
            public string LastUpdateDT { get; set; }
            public string LastAccessID { get; set; }
            public RefType Type { get; set; }
        }
        public class SearchModelGroupModel
        {
            [Display(Name = "Code")]
            public int Code { get; set; }
            [Display(Name = "Name")]
            public int Name { get; set; }
        }
        public class ModelGroupModelViewModel
        {
            public int ModelID { get; set; }
            public int ModelGroupID { get; set; }
            public RefType Type { get; set; }
            public ModelGroupModel ModelGroupModel { get; set; }
        }
    #endregion

    #region ModelImage
        public class SearchViewModelModelImage
        {
            public SearchViewModelModelImage()
            {
                SearchCriterias = new SearchModelImage();
            }
            public SearchModelImage SearchCriterias { get; set; }
            public IEnumerable<ModelImageSearchResults> SearchResults { get; set; }
        }
        public class ModelImageSearchResults
        {
            public int ModelImageID { get; set; }
            public string Model { get; set; }
            public string Colour { get; set; }
            public string ImagePath { get; set; }
            public string ActiveCheckBox { get; set; }
            public string CreateDT { get; set; }
            public string LastUpdateDT { get; set; }
            public string LastAccessID { get; set; }
            public RefType Type { get; set; }
        }
        public class SearchModelImage
        {
            [Display(Name = "Model")]
            public int ModelID { get; set; }
            [Display(Name = "Colour")]
            public int ColourID { get; set; }
        }
        public class ModelImageViewModel
        {
            public int ModelID { get; set; }
            public int ColourID { get; set; }
            public RefType Type { get; set; }
            public ModelImage ModelImage { get; set; }
        }
    #endregion
}
