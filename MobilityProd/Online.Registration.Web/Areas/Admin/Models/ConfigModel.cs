﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Web.Security;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using System.ComponentModel.DataAnnotations;

namespace Online.Registration.Admin.Models
{
    #region Status Type

    public class SearchViewModelStatusType
    {
        public SearchViewModelStatusType()
        {
            SearchCriterias = new SearchStatusTypes();
        }
        public SearchStatusTypes SearchCriterias { get; set; }
        public IEnumerable<StatusTypeSearchResults> SearchResults { get; set; }
    }
    public class StatusTypeSearchResults
    {
        public int StatusTypeID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchStatusTypes
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
    }

    #endregion

    #region Status

    public class SearchViewModelStatus
    {
        public SearchViewModelStatus()
        {
            SearchCriterias = new SearchStatuses();
        }
        public SearchStatuses SearchCriterias { get; set; }
        public IEnumerable<StatusSearchResults> SearchResults { get; set; }
    }
    public class StatusSearchResults
    {
        public int StatusID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string StatusType { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchStatuses
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Display(Name = "Status Type")]
        public int StatusTypeID { get; set; }
    }
    public class UpdateViewStatusModel
    {
        public Status Status { get; set; }
        public List<StatusReasonCodeSearchResults> StatusReasonCode { get; set; }
        public List<StatusChangeSearchResults> StatusChange { get; set; }
    }

    #endregion

    #region Account Category
    public class SearchViewModelAccountCategory
    {
        public SearchViewModelAccountCategory()
        {
            SearchCriterias = new SearchAccountCategory();
        }
        public SearchAccountCategory SearchCriterias { get; set; }
        public IEnumerable<AccountCategorySearchResults> SearchResults { get; set; }
    }
    public class AccountCategorySearchResults
    {
        public int AccountCategoryID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string KenanCode { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchAccountCategory
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
    #endregion

    #region Status Reason
    public class SearchViewModelStatusReason
    {
        public SearchViewModelStatusReason()
        {
            SearchCriterias = new SearchStatusReason();
        }
        public SearchStatusReason SearchCriterias { get; set; }
        public IEnumerable<StatusReasonSearchResults> SearchResults { get; set; }
    }
    public class StatusReasonSearchResults
    {
        public int StatusReasonID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }

    public class UpdateViewStatusReasonModel
    {
        public StatusReason StatusReason { get; set; }
        public List<StatusReasonCodeSearchResults> StatusReasonCode { get; set; }
    }

    public class SearchStatusReason
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
    #endregion

    #region Status Change
    public class SearchViewModelStatusChange
    {
        public SearchViewModelStatusChange()
        {
            SearchCriterias = new SearchStatusChange();
        }
        public SearchStatusChange SearchCriterias { get; set; }
        public IEnumerable<StatusChangeSearchResults> SearchResults { get; set; }
    }
    public class StatusChangeSearchResults
    {
        public int StatusChangeID { get; set; }
        public string FromStatus { get; set; }
        public string ToStatus { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
        public string Type { get; set; }
    }
    public class SearchStatusChange
    {
        [Display(Name = "From Status")]
        public int FromStatusID { get; set; }
        [Display(Name = "To Status")]
        public int ToStatusID { get; set; }
    }
    public class StatusChangeViewModel
    {
        public int FromStatusID { get; set; }
        public int ToStatusID { get; set; }
        public string Type { get; set; }
        public StatusChange StatusChange { get; set; }
    }
    public class UpdateViewStatusChangeModel
    {
        public int FromStatusID { get; set; }
        public int ToStatusID { get; set; }
        public RefType Type { get; set; }
        public StatusChange StatusChange { get; set; }
    }

    #endregion

    #region Status Reason Code
    public class SearchViewModelStatusReasonCode
    {
        public SearchViewModelStatusReasonCode()
        {
            SearchCriterias = new SearchStatusReasonCode();
        }
        public SearchStatusReasonCode SearchCriterias { get; set; }
        public IEnumerable<StatusReasonCodeSearchResults> SearchResults { get; set; }
    }
    public class StatusReasonCodeSearchResults
    {
        public int StatusReasonCodeID { get; set; }
        public string Status { get; set; }
        public string ReasonCode { get; set; }
        public string Remark { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
        public RefType Type { get; set; }
    }
    public class SearchStatusReasonCode
    {
        [Display(Name = "Status")]
        public int StatusID { get; set; }
        [Display(Name = "Reason Code")]
        public int ReasonCodeID { get; set; }
    }
    public class StatusReasonCodeViewModel
    {
        public int StatusID { get; set; }
        public int StatusReasonID { get; set; }
        public RefType Type { get; set; }
        public StatusReasonCode StatusReasonCode { get; set; }
    }
    public class UpdateViewStatusReasonCodeModel
    {
        public RefType Type { get; set; }
        public StatusReasonCode StatusReasonCode { get; set; }
    }
    #endregion

    #region Organization
    public class SearchViewModelOrganization
    {
        public SearchViewModelOrganization()
        {
            SearchCriterias = new SearchOrganization();
        }
        public SearchOrganization SearchCriterias { get; set; }
        public IEnumerable<OrganizationSearchResults> SearchResults { get; set; }
    }
    public class OrganizationSearchResults
    {
        public int OrganizationID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchOrganization
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
    #endregion
}
