﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Web.Security;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using System.ComponentModel.DataAnnotations;

namespace Online.Registration.Admin.Models
{
    #region Address Type
    public class SearchViewModelAddressType
    {
        public SearchViewModelAddressType()
        {
            SearchCriterias = new SearchAddressType();
        }
        public SearchAddressType SearchCriterias { get; set; }
        public IEnumerable<AddressTypeSearchResults> SearchResults { get; set; }
    }
    public class AddressTypeSearchResults
    {
        public int AddressTypeID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ActiveCheckbox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchAddressType
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
    }

    #endregion 
    
    #region Bank
    public class SearchViewModelBank
    {
        public SearchViewModelBank()
        {
            SearchCriterias = new SearchBank();
        }
        public SearchBank SearchCriterias { get; set; }
        public IEnumerable<BankSearchResults> SearchResults { get; set; }
    }
    public class BankSearchResults
    {
        public int BankID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchBank
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Code")]
        public string Code { get; set; }
    }
    #endregion

    #region Card Type
    public class SearchViewModelCardType
    {
        public SearchViewModelCardType()
        {
            SearchCriterias = new SearchCardType();
        }
        public SearchCardType SearchCriterias { get; set; }
        public IEnumerable<CardTypeSearchResults> SearchResults { get; set; }
    }
    public class CardTypeSearchResults
    {
        public int CardTypeID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
        public string KenanCode { get; set; }
    }
    public class SearchCardType
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Code")]
        public string Code { get; set; }
    }
    #endregion

    #region Complaint Issue
    public class SearchViewModelComplaintIssue
    {
        public SearchViewModelComplaintIssue()
        {
            SearchCriterias = new SearchComplaintIssues();
        }
        public SearchComplaintIssues SearchCriterias { get; set; }
        public IEnumerable<ComplaintIssueSearchResults> SearchResults { get; set; }
    }
    public class ComplaintIssueSearchResults
    {
        public int ComplaintIssueID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchComplaintIssues
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Type")]
        public string Type { get; set; }
    }
    public class UpdateViewComplaintIssues
    {
        public ComplaintIssues ComplaintIssues { get; set; }
        public List<ComplaintIssuesGroupSearchResults> ComplaintIssuesGroup { get; set; }
        
    }
    #endregion

    #region Country
    public class SearchViewModelCountry
    {
        public SearchViewModelCountry()
        {
            SearchCriterias = new SearchCountries();
        }
        public SearchCountries SearchCriterias { get; set; }
        public IEnumerable<CountrySearchResults> SearchResults { get; set; }
    }
    public class CountrySearchResults
    {
        public int CountryID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchCountries
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Code")]
        public string Code { get; set; }
    }
    #endregion

    #region Customer Title
    public class SearchViewModelCustomerTitle
    {
        public SearchViewModelCustomerTitle()
        {
            SearchCriterias = new SearchCustomerTitles();
        }
        public SearchCustomerTitles SearchCriterias { get; set; }
        public IEnumerable<CustomerTitleSearchResults> SearchResults { get; set; }
    }
    public class CustomerTitleSearchResults
    {
        public int CustomerTitleID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ForGender { get; set; }
        public string KenanCode { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchCustomerTitles
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Code")]
        public string Code { get; set; }
    }
    #endregion

    #region ID Card Type
    public class SearchViewModelIDCardType
    {
        public SearchViewModelIDCardType()
        {
            SearchCriterias = new SearchIDCardTypes();
        }
        public SearchIDCardTypes SearchCriterias { get; set; }
        public IEnumerable<IDCardTypeSearchResults> SearchResults { get; set; }
    }
    public class IDCardTypeSearchResults
    {
        public int IDCardTypeID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
        public string KenanCode { get; set; }
    }
    public class SearchIDCardTypes
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
    }
    #endregion

    #region Language
    public class SearchViewModelLanguage
    {
        public SearchViewModelLanguage()
        {
            SearchCriterias = new SearchLanguages();
        }
        public SearchLanguages SearchCriterias { get; set; }
        public IEnumerable<LanguageSearchResults> SearchResults { get; set; }
    }
    public class LanguageSearchResults
    {
        public int LanguageID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchLanguages
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Code")]
        public string Code { get; set; }
    }
    #endregion

    #region Market
    public class SearchViewModelMarket
    {
        public SearchViewModelMarket()
        {
            SearchCriterias = new SearchMarkets();
        }
        public SearchMarkets SearchCriterias { get; set; }
        public IEnumerable<MarketSearchResults> SearchResults { get; set; }
    }
    public class MarketSearchResults
    {
        public int MarketID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string KenanCode { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchMarkets
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
    #endregion

    #region Nationality
    public class SearchViewModelNationality
    {
        public SearchViewModelNationality()
        {
            SearchCriterias = new SearchNationalities();
        }
        public SearchNationalities SearchCriterias { get; set; }
        public IEnumerable<NationalitySearchResults> SearchResults { get; set; }
    }
    public class NationalitySearchResults
    {
        public int NationalityID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string KenanCode { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchNationalities
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Code")]
        public string Code { get; set; }
    }
    #endregion

    #region Payment Mode
    public class SearchViewModelPaymentMode
    {
        public SearchViewModelPaymentMode()
        {
            SearchCriterias = new SearchPaymentModes();
        }
        public SearchPaymentModes SearchCriterias { get; set; }
        public IEnumerable<PaymentModeSearchResults> SearchResults { get; set; }
    }
    public class PaymentModeSearchResults
    {
        public int PaymentModeID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchPaymentModes
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Code")]
        public string Code { get; set; }
    }
    #endregion

    #region Race
    public class SearchViewModelRace
    {
        public SearchViewModelRace()
        {
            SearchCriterias = new SearchRaces();
        }
        public SearchRaces SearchCriterias { get; set; }
        public IEnumerable<RaceSearchResults> SearchResults { get; set; }
    }
    public class RaceSearchResults
    {
        public int RaceID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string KenanCode { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchRaces
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Code")]
        public string Code { get; set; }
    }
    #endregion

    #region RegType
    public class SearchViewModelRegType
    {
        public SearchViewModelRegType()
        {
            SearchCriterias = new SearchRegTypes();
        }
        public SearchRegTypes SearchCriterias { get; set; }
        public IEnumerable<RegTypeSearchResults> SearchResults { get; set; }
    }
    public class RegTypeSearchResults
    {
        public int RegTypeID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Message { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchRegTypes
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
    }
    #endregion

    #region State
    public class SearchViewModelState
    { 
        public SearchViewModelState()
        {
            SearchCriterias = new SearchStates();
        }
        public SearchStates SearchCriterias { get; set; }
        public IEnumerable<StateSearchResults> SearchResults { get; set; }
    }
    public class StateSearchResults
    {
        public int StateID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Country { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
        public string KenanStateID { get; set; }
        public string VOIPDefaultSearch { get; set; }
    }
    public class SearchStates
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Display(Name = "Country")]
        public int CountryID { get; set; }
    }
    #endregion
    
    #region ComplaintIssuesGroup
    public class ComplaintIssuesGroupSearchResults
    {
        public int ComplaintIssuesGroupID { get; set; }
        public string Product { get; set; }
        public string Reason1 { get; set; }
        public string Reason2 { get; set; }
        public string Reason3 { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
        public string Type { get; set; }
    }
    public class ComplaintIssuesGroupViewModel
    {
        public int ComplaintIssueID { get; set; }
        public string Type { get; set; }
        public ComplaintIssuesGroup ComplaintIssuesGroup { get; set; }
        public List<IssuesGroupDispatchQSearchResults> IssuesGroupDispatchQ { get; set; }
    }
    public class UpdateViewComplaintIssueGroupModel
    {
        public ComplaintIssuesGroup ComplaintIssuesGroup { get; set; }
        public string Type { get; set; }
    }
    public class UpdateViewComplaintIssuesGroup
    {
        public int ComplaintIssuesGroupID { get; set; }
        public string Type { get; set; }
        public ComplaintIssuesGroup ComplaintIssuesGroup { get; set; }
        public List<IssuesGroupDispatchQSearchResults> IssuesGroupDispatchQ { get; set; }

    }
    #endregion

    #region Issue Group Dispatch Queue
    public class IssuesGroupDispatchQSearchResults
    {
        public int IssueGroupDispatchQID { get; set; }
        public int IssuesGrpID { get; set; }
        public string CaseDispatchQ { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
        public string Type { get; set; }
    }
    public class IssuesGroupDispatchQViewModel
    {
        public int ComplaintIssueGroupID { get; set; }
        public string Type { get; set; }
        public IssueGrpDispatchQ IssueGrpDispatchQ { get; set; }
        public ComplaintIssuesGroup ComplaintIssuesGroup { get; set; }
    }
    #endregion
}