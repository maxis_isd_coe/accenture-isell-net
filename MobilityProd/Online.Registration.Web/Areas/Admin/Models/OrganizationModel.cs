﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Web.Security;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using System.ComponentModel.DataAnnotations;

namespace Online.Registration.Admin.Models
{
    #region Organization Type

    public class SearchViewModelOrgType
    {
        public SearchViewModelOrgType()
        {
            SearchCriterias = new SearchOrgTypes();
        }
        public SearchOrgTypes SearchCriterias { get; set; }
        public IEnumerable<OrgTypeSearchResults> SearchResults { get; set; }
    }
    public class OrgTypeSearchResults
    {
        public int OrgTypeID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ActiveCheckBox { get; set; }
        public string LastUpdateDT { get; set; }
        public string CreateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchOrgTypes
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
    }

    #endregion
}