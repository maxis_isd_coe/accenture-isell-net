﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Web.Security;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using System.ComponentModel.DataAnnotations;

namespace Online.Registration.Admin.Models
{
    #region User
    public class SearchViewModelUser
    {
        public SearchViewModelUser()
        {
            SearchCriterias = new SearchUsers();
        }
        public SearchUsers SearchCriterias { get; set; }
        public IEnumerable<UserSearchResults> SearchResults { get; set; }
    }
    public class UserSearchResults
    {
        public int UserID { get; set; }
        public string FullName { get; set; }
        public string IDCardType { get; set; }
        public string IDCardNo { get; set; }
        public string MobileNo { get; set; }
        public string EmailAddr { get; set; }
        public string Org { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchUsers
    {
        [Display(Name = "Full Name")]
        public string FullName { get; set; }
        [Display(Name = "Mobile Number")]
        public string MobileNo { get; set; }
        [Display(Name = "Email Address")]
        public string EmailAddr { get; set; }
    }
    #endregion

    #region User Group
    public class SearchViewModelUserGroup
    {
        public SearchViewModelUserGroup()
        {
            SearchCriterias = new SearchUserGroups();
        }
        public SearchUserGroups SearchCriterias { get; set; }
        public IEnumerable<UserGroupSearchResults> SearchResults { get; set; }
    }
    public class UserGroupSearchResults
    {
        public int UserGroupID { get; set; }
        public string Org { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchUserGroups
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "Code")]
        public string Code { get; set; }
    }
    public class UpdateViewUserGroupModel
    {
        public UserGroup UserGroup { get; set; }
        public List<UserGroupRoleSearchResults> UserGroupRole { get; set; }
        public List<UserGroupAccessSearchResults> UserGroupAccess { get; set; }
    }
    #endregion

    #region User Group Access
    public class SearchViewModelUserGroupAccess
    {
        public SearchViewModelUserGroupAccess()
        {
            SearchCriterias = new SearchUserGroupAccesses();
        }
        public SearchUserGroupAccesses SearchCriterias { get; set; }
        public IEnumerable<UserGroupAccessSearchResults> SearchResults { get; set; }
    }
    public class UserGroupAccessSearchResults
    {
        public int UserGroupAccessID { get; set; }
        public string UserGroup { get; set; }
        public string Access { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
        public RefType Type { get; set; }
    }
    public class SearchUserGroupAccesses
    {
        [Display(Name = "User Group")]
        public int UserGroupID { get; set; }
        [Display(Name = "Access")]
        public int AccessID { get; set; }
    }
    public class UserGroupAccessViewModel
    {
        public int UserGroupID { get; set; }
        public int AccessID { get; set; }
        public RefType Type { get; set; }
        public UserGrpAccess UserGroupAccess { get; set; }
    }
    #endregion

    #region User Role
    public class SearchViewModelUserRole
    {
        public SearchViewModelUserRole()
        {
            SearchCriterias = new SearchUserRoles();
        }
        public SearchUserRoles SearchCriterias { get; set; }
        public IEnumerable<UserRoleSearchResults> SearchResults { get; set; }
    }
    public class UserRoleSearchResults
    {
        public int UserRoleID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchUserRoles
    {
        [Display(Name = "Code")]
        public string Code { get; set; }
    }
    public class UpdateViewUserRoleModel
    {
        public UserRole UserRole { get; set; }
        public List<UserGroupRoleSearchResults> UserGroupRole { get; set; }
    }
    #endregion

    #region Access
    public class SearchViewModelAccess
    {
        public SearchViewModelAccess()
        {
            SearchCriterias = new SearchAccess();
        }
        public SearchAccess SearchCriterias { get; set; }
        public IEnumerable<AccessSearchResults> SearchResults { get; set; }
    }
    public class AccessSearchResults
    {
        public int AccessID { get; set; }
        public string UserFullName { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
    }
    public class SearchAccess
    {
        [Display(Name = "Username")]
        public string UserName { get; set; }
    }
    public class UpdateViewAccessModel
    {
        public Access Access { get; set; }
        public List<UserGroupAccessSearchResults> UserGroupAccess{ get; set; }
    }
    #endregion

#region User Group Role
    public class SearchViewModelUserGroupRole
    {
        public SearchViewModelUserGroupRole()
        {
            SearchCriterias = new SearchUserGroupRole();
        }
        public SearchUserGroupRole SearchCriterias { get; set; }
        public IEnumerable<UserGroupRoleSearchResults> SearchResults { get; set; }
    }
    public class UserGroupRoleSearchResults
    {
        public int UserGroupRoleID { get; set; }
        public string UserGroup { get; set; }
        public string UserRole { get; set; }
        public string ActiveCheckBox { get; set; }
        public string CreateDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string LastAccessID { get; set; }
        public RefType Type { get; set; }
    }
    public class SearchUserGroupRole
    {
        [Display(Name = "User Group")]
        public int UserGroupID { get; set; }
        [Display(Name = "User Role")]
        public int UserRoleID { get; set; }
    }
    public class UserGroupRoleViewModel
    {
        public int UserGroupID { get; set; }
        public int UserRoleID { get; set; }
        public RefType Type { get; set; }
        public UserGrpRole UserGroupRole { get; set; }
    }
#endregion
}