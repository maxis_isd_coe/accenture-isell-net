﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using System.Web.Mvc;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;

namespace Online.Registration.Web.SmartViewModels
{
    #region Mobile Registration
    [Serializable]
    public class MobileRegistrationVM
    {
        public MobileRegistrationVM()
        {


            Address = new Address();
            Customer = new Customer();
            OrderSummary = new OrderSummaryVM();
            PackageVM = new PackageVM();
            PhoneVM = new PhoneVM();
            VASesVM = new ValueAddedServicesVM();
            MobileNoVM = new MobileNoVM();

            //Packages = new List<PackageVM>();
            //VASes = new List<ValueAddedServicesVM>();
        }

        public int TabNumber { get; set; }

        public MobileNoVM MobileNoVM { get; set; }
        public PhoneVM PhoneVM { get; set; }
        public PackageVM PackageVM { get; set; }
        public ValueAddedServicesVM VASesVM { get; set; }
        public OrderSummaryVM OrderSummary { get; set; }
        public Customer Customer { get; set; }
        public Address Address { get; set; }
    }

    #endregion

    #region Device Options
    [Serializable]
    public class DeviceOptionVM
    {
        public DeviceOptionVM()
        {
            AvailableBrands = new List<Brand>();
        }

        public int TabNumber { get; set; }
        public string Type { get; set; } // Brand, Hot Seller, Promotion
        public List<Brand> AvailableBrands { get; set; }
        public bool IsBrandSelected { get; set; }
        [Display(Name = "Device Brand")]
        [Required(ErrorMessage = "Please select Device Brand.")]
        public string SelectedDeviceOption { get; set; }
    }
    [Serializable]
    public class DeviceOptionVMSmart
    {
        public DeviceOptionVMSmart()
        {
            AvailableBrands = new List<BrandSmart>();
        }

        public int TabNumber { get; set; }
        public string Type { get; set; } // Brand, Hot Seller, Promotion
        public List<BrandSmart> AvailableBrands { get; set; }
        public bool IsBrandSelected { get; set; }
        [Display(Name = "Device Brand")]
        [Required(ErrorMessage = "Please select Device Brand.")]
        public string SelectedDeviceOption { get; set; }
    }

    #endregion

    #region Device
    [Serializable]
    public class PhoneVM
    {
        public PhoneVM()
        {
            ModelImages = new List<AvailableModelImage>();
        }

        public int TabNumber { get; set; }
        public List<AvailableModelImage> ModelImages { get; set; }
        [Required(ErrorMessage = "Please choose a preferred device.")]
        [Display(Name = "Model")]
        public string SelectedModelImageID { get; set; }
        public int BrandID { get; set; }
        public int ModelID { get; set; }
        public int ColourID { get; set; }
    }
    [Serializable]
    public class PhoneVMSmart
    {
        public PhoneVMSmart()
        {
            ModelImages = new List<AvailableModelImageSmart>();
        }

        public int TabNumber { get; set; }
        public List<AvailableModelImageSmart> ModelImages { get; set; }
        [Required(ErrorMessage = "Please choose a preferred device.")]
        [Display(Name = "Model")]
        public string SelectedModelImageID { get; set; }
        public int BrandID { get; set; }
        public int ModelID { get; set; }
        public int ColourID { get; set; }
    }
    [Serializable]
    public class AvailableModelImage
    {
        public BrandArticle BrandArticle { get; set; }
        public ModelImage ModelImage { get; set; }
        public string ModelCode { get; set; }
        public int Count { get; set; }
    }
    [Serializable]
    public class AvailableModelImageSmart
    {
        public BrandArticleSmart BrandArticle { get; set; }
        public ModelImage ModelImage { get; set; }
        public string ModelCode { get; set; }
        public int Count { get; set; }
    }
    #endregion

    #region Value Added Services
    [Serializable]
    public class ValueAddedServicesVM
    {
        public ValueAddedServicesVM()
        {
            MandatoryVASes = new List<AvailableVAS>();
            AvailableVASes = new List<AvailableVAS>();
            ContractVASes = new List<AvailableVAS>();
            VASesName = new List<string>();
            SelectedPgmBdlPkgCompIDs = new List<int>();
            DataVASes = new List<AvailableVAS>();
            ExtraComponents = new List<AvailableVAS>();
            Offers = new List<MOCOfferDetails>();
        }

        public int OfferId { get; set; }
        public decimal Discount { get; set; }
        public int TabNumber { get; set; }
        public string SelectedVasIDs { get; set; }
        public List<int> SelectedPgmBdlPkgCompIDs { get; set; }

        public string uomCode { get; set; }
        public string price { get; set; }


        //[Required]
        //[Range(1, int.MaxValue, ErrorMessage = "Please Select a Contract")]
        //[Display(Name = "Contract")]
        public string SelectedContractID { get; set; }
        public string SelectedContractName { get; set; }

        public List<string> VASesName { get; set; }
        public List<AvailableVAS> AvailableVASes { get; set; }
        public List<AvailableVAS> ContractVASes { get; set; }
        public List<AvailableVAS> MandatoryVASes { get; set; }
        public List<AvailableVAS> DataVASes { get; set; }
        public List<AvailableVAS> ExtraComponents { get; set; }
        public List<MOCOfferDetails> Offers { get; set; }
    }
    [Serializable]
    public class AvailableVAS
    {
        public int PgmBdlPkgCompID { get; set; }
        public string ComponentName { get; set; }
        public string Value { get; set; }
        public decimal Price { get; set; }
        public int? GroupId { get; set; }
        public string GroupName { get; set; }
    }

    #endregion

    #region ComponentViewModel
    [Serializable]
    public class ComponentViewModel
    {
        //public IEnumerable<Online.Registration.Web.SubscriberICService.reassingVases> ReassingVases
        public IEnumerable<Online.Registration.Web.SubscriberICService.reassingVases> ReassingVases
        {
            set;
            get;
        }
        public int TabNumber { get; set; }

        public List<OldPackages> OldPackagesList { set; get; }
        public List<NewPackages> NewPackagesList { set; get; }
        public List<OldComponents> OldComponentsList { set; get; }
        public List<NewComponents> NewComponentsList { set; get; }
        public List<NewComponents> DependentVASList { set; get; }
		public string ShareSuppErrorMessage { get; set; }
    }
    [Serializable]
    public class OldComponents
    {
        public string OldComponentId { set; get; }
        public string OldComponentName { set; get; }
        public string OldPackageId { set; get; }

    }
    [Serializable]
    public class NewComponents
    {
        public string NewComponentId { set; get; }
        public string NewComponentKenanCode { set; get; }
        public string NewComponentName { set; get; }
        public string NewPackageId { set; get; }
        public string NewPackageKenanCode { set; get; }
        public bool IsChecked { set; get; }
        public bool ReassignIsmandatory { set; get; }
        public string Type { set; get; }
        public string ComponentGroupId { get; set; }
        public string ComponentGroupName { get; set; }
        public int isDefault { get; set; }
    }
    [Serializable]
    public class OldPackages
    {
        public string OldPackageId { set; get; }
        public string OldPackageName { set; get; }
    }
    [Serializable]
    public class NewPackages
    {
        public string NewPackageId { set; get; }
        public string NewPackageName { set; get; }
        public string Type { set; get; }
    }


    #endregion


    #region PackageVM
    [Serializable]
    public class PackageVM
    {
        public PackageVM()
        {
            AvailablePackages = new List<AvailablePackage>();
        }

        public int TabNumber { get; set; }
        public List<AvailablePackage> AvailablePackages { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select a Plan")]
        [Display(Name = "Plan")]
        public int SelectedPgmBdlPkgCompID { get; set; }
    }
    [Serializable]
    public class AvailablePackage
    {
        public int PgmBdlPkgCompID { get; set; }
        public string ProgramName { get; set; }
        public string BundleName { get; set; }
        public string PackageName { get; set; }
        public string PackageCode { get; set; }
        public decimal Price { get; set; }
        public decimal MonthlySubscription { get; set; }
        public string KenanCode { get; set; }
    }

    #endregion

    #region Mobile No
    [Serializable]
    public class MobileNoVM
    {
        public MobileNoVM()
        {
            SelectedMobileNumber = new List<string>();
            AvailableMobileNos = new List<string>();
        }

        public int TabNumber { get; set; }
        [Display(Name = "Mobile Number")]
        public List<string> SelectedMobileNumber { get; set; }
        public int NoOfDesireMobileNo { get; set; }
        public int NoOfSubline { get; set; }

        [StringLength(12)]
        [Display(Name = "Desire No")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Desire No must be a number.")]
        public string DesireNo { get; set; }

        public List<string> AvailableMobileNos { get; set; }

        [Required(ErrorMessage = "Please select a number.")]
        [Display(Name = "Mobile Number")]
        public string SelectedMobileNo { get; set; }
    }

    #endregion

    #region Mobile Registration Order Summary
    [Serializable]
    public class OrderSummaryVM
    {
        public OrderSummaryVM()
        {
            Sublines = new List<SublineVM>();
            VasVM = new ValueAddedServicesVM();
            MobileNumbers = new List<string>();
            Sublines = new List<SublineVM>();
            Offers = new List<MOCOfferDetails>();
            SelectedComponets = new List<RegSmartComponents>();
        }
        #region SUTAN ADDED CODE 19ThFeb2013
        public PersonalDetailsVM PersonalDetailsVM { get; set; }
        #endregion SUTAN ADDED CODE 19ThFeb2013
        public bool fromSubline { get; set; }

        // Device
        public int SelectedModelImageID { get; set; }
        public string ModelImageUrl { get; set; }
        [Display(Name = "Brand")]
        public int BrandID { get; set; }
        public string BrandName { get; set; }
        public string ArticleId { get; set; }
        [Display(Name = "Model")]
        public int ModelID { get; set; }
        public string ModelName { get; set; }
        [Display(Name = "Colour")]
        public int ColourID { get; set; }
        public string ColourName { get; set; }
        public decimal ModelPrice { get; set; }
        [Display(Name = "IMEI Number")]
        public string IMEINumber { get; set; }

        // Mobile No
        [Display(Name = "Mobile Number")]
        public List<string> MobileNumbers { get; set; }

        // Plan
        public int SelectedPgmBdlPkgCompID { get; set; }
        [Display(Name = "Plan Bundle")]
        public string PlanBundle { get; set; }
        [Display(Name = "Plan Name")]
        public string PlanName { get; set; }
        [Display(Name = "Phone Price")]
        public decimal Price { get; set; }
        [Display(Name = "Monthly Subscription")]
        public decimal MonthlySubscription { get; set; }
        /*Added by chetan for vasplan price adding*/
        public List<decimal> MonthlySubscription2 { get; set; }
        /*upto here*/
        [Display(Name = "Local Calls")]
        public int LocalCalls { get; set; }
        [Display(Name = "Local SMS")]
        public int LocalSMS { get; set; }
        [Display(Name = "Data")]
        public int Data { get; set; }
        [Display(Name = "SIM Serial")]
        public string SIMSerial { get; set; }

        // VAS
        public ValueAddedServicesVM VasVM { get; set; }
        public int VasesPrice { get; set; }
        public int ContractID { get; set; }
        public int DataPlanID { get; set; }

        // Subline
        public List<SublineVM> Sublines { get; set; }
        //Offers
        public List<MOCOfferDetails> Offers { get; set; }

        // Price
        //added by deepika
        public decimal MalayAdvDevPrice { get; set; }
        public decimal OthersAdvDevPrice { get; set; }
        public decimal SumPrice { get; set; }
        public decimal deposite { get; set; }
        public int RaceID { get; set; }
        //end

        /*Added by chetan for split adv & deposit*/
        public decimal MalyDevAdv { get; set; }
        public decimal MalayPlanAdv { get; set; }
        public decimal MalayDevDeposit { get; set; }
        public decimal MalyPlanDeposit { get; set; }

        public decimal OthDevAdv { get; set; }
        public decimal OthPlanAdv { get; set; }
        public decimal OthDevDeposit { get; set; }
        public decimal OthPlanDeposit { get; set; }

        //upto here
        public decimal Deposite { get; set; }
        public int ItemPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public DevicePriceType DevicePriceType { get; set; }
        public decimal PenaltyAmount { get; set; }

        #region Raj Added for to display Selected Components

        public List<RegSmartComponents> SelectedComponets { set; get; }

        #endregion
    }

    #endregion

    #region Personal Details
    [Serializable]
    public class PersonalDetailsVM
    {
        public PersonalDetailsVM()
        {
            Customer = new Customer()
            {
                LastAccessID = "SYSTEM"
            };
            Address = new Address()
            {
                LastAccessID = "SYSTEM"
            };
        }

        [Display(Name = "Registration ID")]
        public int RegID { get; set; }
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Queue must be a number.")]
        [Display(Name = "Queue No")]
        public string QueueNo { get; set; }
        [Display(Name = "Status")]
        public int StatusID { get; set; }
        public string StatusCode { get; set; }
        public int RegTypeID { get; set; }
        public bool IsRegClosed { get; set; }
        public bool IDCardNoValidate { get; set; }
        public int TabNumber { get; set; }
        public Customer Customer { get; set; }
        public Address Address { get; set; }
        public string idDocumentFront { get; set; }
        public PegaRecommendationsVM PegaVM { get; set; }

        [Required]
        [Display(Name = "Day of DOB")]
        public int DOBDay { get; set; }
        [Required]
        [Display(Name = "Month of DOB")]
        public int DOBMonth { get; set; }
        [Required]
        [Display(Name = "Year of DOB")]
        public int DOBYear { get; set; }

        [Display(Name = "Internal")]
        public bool BlacklistInternal { get; set; }
        [Display(Name = "External")]
        public bool BlacklistExternal { get; set; }
        [Display(Name = "Biometric Verification")]
        public bool BiometricVerify { get; set; }
        public string InputIDCardTypeID { get; set; }
        public string KenanAccountNo { get; set; }

        // Added by VLT on 09 Apr 2013 for Sim Model type
        [Required]
        [Display(Name = "SIM Model")]
        public string SIMModel { get; set; }
        // End by VLT

        #region VLT ADDED CODE
        [Display(Name = "Age Verification")]
        public bool AgeCheck { get; set; }
        [Display(Name = "DDMF Verification")]
        public bool DDMFCheck { get; set; }
        [Display(Name = "Address Verification")]
        public bool AddressCheck { get; set; }
        [Display(Name = "Outstanding Credit Verification")]
        public bool OutstandingCreditCheck { get; set; }
        [Display(Name = "Total Line Verification")]
        public bool TotalLineCheck { get; set; }
        [Display(Name = "Principal Line Verification")]
        public bool PrincipalLineCheck { get; set; }

        #endregion VLT ADDED CODE

        #region SUTAN ADDED CODE 19ThFeb2013
        [Display(Name = "Age Verification")]
        public string AgeCheckStatus { get; set; }
        [Display(Name = "DDMF Verification")]
        public string DDMFCheckStatus { get; set; }
        [Display(Name = "Address Verification")]
        public string AddressCheckStatus { get; set; }
        [Display(Name = "Outstanding Credit Verification")]
        public string OutStandingCheckStatus { get; set; }
        [Display(Name = "Total Line Verification")]
        public string TotalLineCheckStatus { get; set; }
        [Display(Name = "Principal Line Verification")]
        public string PrincipleLineCheckStatus { get; set; }
        #endregion SUTAN ADDED CODE 19ThFeb2013        

        #region Chetan added code
        [Display(Name = "Whitelist Status")]
        public string Whitelist_Status { get; set; }
        [Display(Name = "MOC Status")]
        public string MOC_Status { get; set; }
        [Display(Name = "Contract Check Verification")]
        public string ContractCheckStatus { get; set; }

        [Display(Name = "User Type")]
        public string UserType { get; set; }
        [Display(Name = "Plan Advance")]
        public decimal PlanAdvance { get; set; }
        [Display(Name = "Plan Deposit")]
        public decimal PlanDeposit { get; set; }
        [Display(Name = "Device Advance")]
        public decimal DeviceAdvance { get; set; }
        [Display(Name = "Device Deposit")]
        public decimal DeviceDeposit { get; set; }
        [Display(Name = "Discount")]
        public decimal Discount { get; set; }
        [Display(Name = "MNP ServiceId Check Status")]
        public string MNPServiceIdCheckStatus { get; set; }
        [Display(Name = "MNP PreportIn Check Status")]
        public string PrePortInCheckStatus { get; set; }
        [Display(Name = "Outstanding Acounts")]
        public string OutstandingAcs { get; set; }

        // Added for Smart Intergration
        public string SM_ContractType { get; set; }
        // Added for Smart Intergration

        #endregion

        #region Added by Patanjali to support Supp and New Line
        [Display(Name = "FxAcctNo")]
        public string fxAcctNo { get; set; }

        [Display(Name = "ExternalId")]
        public string externalId { get; set; }
        #endregion

        #region "Added by Patanjali to include Remarks"
        [Display(Name = "Remarks")]
        public string Remarks { get; set; }
        #endregion

        #region Added by Narayanareddy

        public string IMPOSFileName { get; set; }
        public int IMPOSStatus { get; set; }
        public string OrganisationId { get; set; }

        #endregion

        public string InputIDCardNo { get; set; }
        /*Added by Rajeswari on 24th March to disable nationality*/
        public int InputNationalityID { get; set; }

        /*Added by chetan for split adv & deposit*/
        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Advance must not less than 0.")]
        [Display(Name = "Device Advance for Malaysians")]
        public decimal MalyDevAdv { get; set; }


        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Advance must not less than 0.")]
        [Display(Name = "Plan Advance for Malaysians")]
        public decimal MalayPlanAdv { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Deposit must not less than 0.")]
        [Display(Name = "Device Deposit for Malaysians")]
        public decimal MalayDevDeposit { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Deposit must not less than 0.")]
        [Display(Name = "Plan Deposit for Malaysians")]
        public decimal MalyPlanDeposit { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Advance must not less than 0.")]
        [Display(Name = "Device Advance for Non Malaysians")]
        public decimal OthDevAdv { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Advance must not less than 0.")]
        [Display(Name = "Plan Advance for Non Malaysians")]
        public decimal OthPlanAdv { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Deposit must not less than 0.")]
        [Display(Name = "Device Deposit for Non Malaysians")]
        public decimal OthDevDeposit { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Deposit must not less than 0.")]
        [Display(Name = "Plan Deposit for Non Malaysians")]
        public decimal OthPlanDeposit { get; set; }
        /*upto here*/

        //added by Deepika       
        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Advance Payment & Deposit must not less than 0.")]
        [Display(Name = "Advance Payment/ Deposit for Non Malaysians *")]
        public decimal OtherAdvDeposit { get; set; }
        public decimal Deposite { get; set; }
        //end region
        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Advance Payment & Deposit must not less than 0.")]
        [Display(Name = "Advance Payment/ Deposit for Malaysians *")]
        public decimal MalayAdvDeposit { get; set; }
        [Display(Name = "Signature")]
        [AllowHtml]
        public string SignatureSVG { get; set; }
        [Display(Name = "Front")]
        [AllowHtml]
        public string CustomerPhoto { get; set; }
        [Display(Name = "Back")]
        [AllowHtml]
        public string AltCustomerPhoto { get; set; }
        [AllowHtml]
        public DateTime RFSalesDT { get; set; }
        [Display(Name = "Photo")]
        [AllowHtml]
        public string Photo { get; set; }

        //[Required]
        [Display(Name = "IMEI Number")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "IMEI Number must be a number.")]
        [StringLength(20)]
        public string CapturedIMEINumber { get; set; }
        public string RegIMEINumber { get; set; }

        [Required]
        [Display(Name = "SIM Serial")]
        [StringLength(30)]
        public string CapturedSIMSerial { get; set; }
        public string RegSIMSerial { get; set; }

        public string ServiceActivateErrMsg { get; set; }

        [Display(Name = "Donor")]
        [Required]
        public string DonorID { get; set; }

        //Added by Patanjali on 07-04-2013 to get payment status
        [Display(Name = "Payment Status")]
        public string PaymentStatus { get; set; }
        //Added by Patanjali on 07-04-2013 to get payment status ends here


        public WhiteListDetails _WhiteListDetails { get; set; }

        /*Chetan added for displaying the reson for failed transcation*/
        [Display(Name = "Message Code")]
        public int MessageCode { get; set; }

        [Display(Name = "Reason")]
        public string MessageDesc { get; set; }

        public decimal PenaltyAmount { get; set; }

        public decimal PenaltyWaivedAmount { get; set; }

        [Display(Name = "Reason for Cancellation")]
        public string CancelledReason { get; set; }

        [Required]
        [RegularExpression("^[0-9a-zA-Z ]+$", ErrorMessage = "Please enter Reason for cancellation.")]
        public string CancellationReason { get; set; }
        #region "Added by VLT ON May 24th 2013"
        [Display(Name = "IsSImRequired_Smart")]
        public bool IsSImRequired_Smart { get; set; }
        #endregion

        public bool IsVerified { get; set; }

        [Display(Name = "Liberlization")]
        public string Liberlization_Status { get; set; }

        [Display(Name = "MOC Status")]
        public string MOCStatus { get; set; }

        [Display(Name = "Printers")]
        public string Printers { get; set; }

        [AllowHtml]
        public string PrintMobileRegsummary { get; set; }

        public string PrintURL { get; set; }

        public string PrintName { get; set; }

        /*Changes by chetan related to PDPA implementation*/
        public string PDPDVersion { get; set; }

        public string DocumentUrl { get; set; }

        public string PDPAVersionStatus { get; set; }

        public int? ContractCheckCount { get; set; }

        public int? TotalLineCheckCount { get; set; }

		public string ArticleID { get; set; }

        [Required]
        [Display(Name = "SIM Type")]
        public string SIMCardType { get; set; }

        //Added for Removal of HardStop for Outstanding Account
        [Display(Name = "Justification")]
        public string Justification { get; set; }
        //Added for Removal of HardStop for Outstanding Account

        #region 07052015 - Anthony - [Paperless CR] Customer Order Acknowledgement
        public string frontDocumentName { get; set; }
        public string backDocumentName { get; set; }
        public string othDocumentName { get; set; }
        //public string hdnWaiverInfo { get; set; }
        #endregion

        //Drop-5 B18 - Dealer Code CR - Pavan - 2015.05.05 - Start
        public string OutletCode { get; set; }
        public string OutletChannelId { get; set; }
        public string SalesCode { get; set; }
        public string SalesChannelId { get; set; }
        public string SalesStaffCode { get; set; }
        public string SalesStaffChannelId { get; set; }
        //Drop-5 B18 - Dealer Code CR - Pavan - 2015.05.05 - End

		public string SurveyAnswer { get; set; }
        public string SurveyFeedBack { get; set; }
        public string SurveyAnswerRate { get; set; }
        public string SingleSignOnValue { get; set; }
        public string SingleSignOnEmailAddress { get; set; }
        public List<RegAttributes> RegAttributes { get; set; }//20150915 - Samuel - display sales code in order summary page
        public DAL.Models.Registration Registration { get; set; }//20150915 - Samuel - display sales code in order summary page
        public string BillingCycle { get; set; }
    }

    #endregion

    #region Print Registration Form
    [Serializable]
    public class RegistrationFormVM
    {
        public RegistrationFormVM()
        {
            Addresses = new List<Address>();
            BundlePackages = new List<PgmBdlPckComponent>();
            ModelGroupModels = new List<ModelGroupModel>();
            SuppLineForms = new List<SuppLineForm>();
            DeviceForms = new List<DeviceForm>();
            RegSmartComponents = new List<RegSmartComponents>();
        }

        public Online.Registration.DAL.Models.Registration Registration { get; set; }
        public Customer Customer { get; set; }
        public List<Address> Addresses { get; set; }
        public List<RegAttributes> RegAttributes { get; set; }
        public List<PgmBdlPckComponent> BundlePackages { get; set; }
        public List<PgmBdlPckComponent> PackageComponents { get; set; }
        public List<ModelGroupModel> ModelGroupModels { get; set; }
        public List<SuppLineForm> SuppLineForms { get; set; }
        public List<DeviceForm> DeviceForms { get; set; }
        public List<RegSmartComponents> RegSmartComponents { set; get; }

        public string OutletCode { get; set; }
        public string OutletChannelId { get; set; }
        public string SalesCode { get; set; }
        public string SalesStaffCode { get; set; }
    }
    [Serializable]
    public class RegistrationFormVM_Smart
    {
        public RegistrationFormVM_Smart()
        {
            Addresses = new List<Address>();
            BundlePackages = new List<PgmBdlPckComponentSmart>();//changed for one paln CR
            ModelGroupModels = new List<ModelGroupModel_Smart>();//changed for one paln CR
            SuppLineForms = new List<SuppLineForm>();
            DeviceForms = new List<DeviceForm>();
            DeviceFormsSec = new List<DeviceForm>();
        }

        public Online.Registration.DAL.Models.Registration Registration { get; set; }
        public DAL.Models.RegistrationSec RegistrationSec { get; set; }
        public Customer Customer { get; set; }
        public List<RegAttributes> RegAttributes { get; set; }
        public List<Address> Addresses { get; set; }
        public List<PgmBdlPckComponentSmart> BundlePackages { get; set; }
        public List<PgmBdlPckComponentSmart> PackageComponents { get; set; }
        public List<ModelGroupModel_Smart> ModelGroupModels { get; set; }
        public List<SuppLineForm> SuppLineForms { get; set; }
        public List<DeviceForm> DeviceForms { get; set; }
        public List<DeviceForm> DeviceFormsSec { get; set; }
        public List<RegSmartComponents> RegSmartComponents { get; set; } //Added for one paln CR
        public List<lnkExtraTenDetails> ExtraTenDetails { get; set; }
        public bool showInventorySection { get; set; }

        public string OutletCode { get; set; }
        public string OutletChannelId { get; set; }
        public string SalesCode { get; set; }
        public string SalesStaffCode { get; set; }
        public string BillingCycle { get; set; }
    }
    [Serializable]
    public class SuppLineForm
    {
        public int SuppLinePBPCID { get; set; }
        public List<int> SuppLineVASIDs { get; set; }
        public string SuppLineBdlPkgName { get; set; }
        public List<string> SuppLineVASNames { get; set; }
        public string MSISDN { get; set; }
    }
    [Serializable]
    public class DeviceForm
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public decimal Price { get; set; }
        public decimal RetailPrice { get; set; }
        public bool IsSuppLine { get; set; }
        public bool IsTradeUp { get; set; }//14012015 - Anthony - GST Trade Up
        public decimal TradeUpAmount { get; set; }//14012015 - Anthony - GST Trade Up
        public string Colour { get; set; }//20150909 - Samuel - Bugzilla 379
    }

    #endregion

    #region Search Registration
    [Serializable]
    public class SearchRegistrationVM
    {
        public SearchRegistrationVM()
        {
            RegistrationSearchResults = new List<RegistrationSearchResult>();
        }

        public RegistrationSearchCriteria RegistrationSearchCriteria { get; set; }
        public List<RegistrationSearchResult> RegistrationSearchResults { get; set; }
    }

    #endregion

    #region Blacklist Check

    [XmlRootAttribute(ElementName = "InternalCheckData")]
    [Serializable]
    public class InternalCheck
    {
        [XmlElement(ElementName = "ExternalIdValue")]
        public string ExternalIdValue { get; set; }
        [XmlElement(ElementName = "ExternalIdType")]
        public string ExternalIdType { get; set; }
    }

    [XmlRootAttribute(ElementName = "MxsDDMFCheckData")]
    [Serializable]
    public class ExternalCheck
    {
        [XmlElement(ElementName = "ExternalIdValue")]
        public string ExternalIdValue { get; set; }
        [XmlElement(ElementName = "ExternalIdType")]
        public string ExternalIdType { get; set; }
    }

    #endregion

    #region Subline Plan
    [Serializable]
    public class SublinePackageVM
    {
        public SublinePackageVM()
        {
            SelectedMobileNumber = new List<string>();
        }

        public PackageVM PackageVM { get; set; }
        [Display(Name = "Mobile Number")]
        public List<string> SelectedMobileNumber { get; set; }
        public int NoOfDesireMobileNo { get; set; }

        [StringLength(12)]
        [Display(Name = "Desire No")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Desire No must be a number.")]
        public string DesireNo { get; set; }
    }
    [Serializable]
    public class SublineVASVM
    {
        public ValueAddedServicesVM VasVM { get; set; }
    }
    [Serializable]
    public class SublineVM
    {
        public bool CanRemove { get; set; }
        public int SequenceNo { get; set; }
        public int PkgPgmBdlPkgCompID { get; set; }
        public string BundleName { get; set; }
        public string PackageName { get; set; }
        public List<int> SelectedVasIDs { get; set; }
        public List<string> SelectedVasNames { get; set; }
        public List<string> SelectedMobileNos { get; set; }
    }

    #endregion

    #region CustomerInformation
    [Serializable]
    public class CustomerPaymentDetailsInfo
    {
        public CustomerInfo CustomerInfo { get; set; }

    }

    #endregion

    #region Store Keeper
    [Serializable]
    public class StoreKeeperVM
    {
        public StoreKeeperVM()
        {
            RegistrationSearchResults = new List<RegistrationSearchResult>();
        }

        public RegistrationSearchCriteria RegistrationSearchCriteria { get; set; }
        public List<RegistrationSearchResult> RegistrationSearchResults { get; set; }
    }

    #endregion

    #region Cashier
    [Serializable]
    public class CashierVM
    {
        public CashierVM()
        {
            RegistrationSearchResults = new List<RegistrationSearchResult>();
        }

        public RegistrationSearchCriteria RegistrationSearchCriteria { get; set; }
        public List<RegistrationSearchResult> RegistrationSearchResults { get; set; }
    }

    #endregion

    #region Pega Recommendations Dashboard
    [Serializable]
    public class PegaRecommendationsVM
    {
        public PegaRecommendationSearchCriteria PegaRecommendationsSearchCriteria { get; set; }
        public List<PegaRecommendation> PegaRecommendationsSearchResult { get; set; }
    }

    #endregion


}
