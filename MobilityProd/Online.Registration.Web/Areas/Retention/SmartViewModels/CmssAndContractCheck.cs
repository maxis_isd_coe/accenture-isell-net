﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using System.Web.Mvc;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;

namespace Online.Registration.Web.SmartViewModels
{
    [Serializable]
    public class ContractModels
    {

        [Display(Name = "CMSS ID")]
        public string CMSSID { get; set; }

        public int TabNumber { get; set; }

        public string componentId { get; set; }
        public string componentInstId { get; set; }
        public string componentInstIdServ { get; set; }
        public string componentActiveDt { get; set; }
        public string componentInactiveDt { get; set; }
        public string componentDesc { get; set; }
        public string componentShortDisplay { get; set; }
        public string Contract12Months { get; set; }
        public string Contract24Months { get; set; }
        public List<ContractModels> itemList { get; set; }
    }
    [Serializable]
    public class SmartModel
    {
        public int TabNumber { get; set; }


        [Required(ErrorMessage = "Please select discount")]
        public string DiscountType { get; set; }

        [Display(Name = "Phone Model")]
        public string PhoneModel { get; set; }

        [Display(Name = "Phone Color")]
        public string PhoneColor { get; set; }

        [Display(Name = "Device RRP (RM)")]
        public string DevicePrice { get; set; }

        [Display(Name = "Net Price (RM)")]
        public string NetPrice { get; set; }

        [Display(Name = "Discount (RM)")]
        public string DiscountPrice { get; set; }

        [Display(Name = "Approval Username")]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        public string Password { get; set; }

        public bool isPenaltyChecked { get; set; }
    }
    [Serializable]
    public class PackageModel
    {
        public string PackageID { get; set; }
        public string PackageDesc { get; set; }

        public string packageInstId { get; set; }
        public string packageInstIdServ { get; set; }
        public string Contract12Months { get; set; }
        public string Contract24Months { get; set; }
        public string packageActiveDt { get; set; }
        public List<Components> compList { get; set; }
    }
    [Serializable]
    public class Components
    {
        public string componentId { get; set; }
        public string componentInstId { get; set; }
        public string componentInstIdServ { get; set; }
        public string componentActiveDt { get; set; }
        public string componentInactiveDt { get; set; }
        public string componentDesc { get; set; }
        public string componentShortDisplay { get; set; }
    }
    [Serializable]
    public class ContractDetails
    {
        [Required]
        [Display(Name = "CMSS ID")]
        [RegularExpression("^[0-9a-zA-Z ]+$", ErrorMessage = "CMSSID must be alphanumeric.")]
        public string CMSSID { get; set; }
        public string PackageID { get; set; }
        public string PackageDesc { get; set; }
        public string ContractRemaining { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime ActiveDate { get; set; }
        public int ContractDuration { get; set; }
        public string ContractExtend { get; set; }
        public string ContractExtendType { get; set; }
        public string ComponentDesc { get; set; }
        public string ContractTerimnate { get; set; }
        public string ContractStartDate { set; get; }
        public string ContractEndDate { set; get; }
        public string ContractDurationinMonths { get; set; }

        public decimal Penalty { get; set; }
        public decimal PenaltyWaiveOff { get; set; } //Discount
        public bool Waived { get; set; }
        public string ContractKenanCode { get; set; }
        public string ContractCustLib { get; set; }
		public string PenaltyType { get; set; }
    }
}