﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.ComponentModel.DataAnnotations;

using Online.Registration.Web.Helper;
using Online.Registration.DAL.ReportModels;

namespace Online.Registration.Web.SmartViewModels
{
    #region Aging Report

    public class SearchViewAgingReportModel
    {
        public SearchViewAgingReportModel()
        {
            SearchCriterias = new SearchAgingReports();
            CenterType = new List<SelectListItem>();
            CenterType.Add(new SelectListItem()
            {
                Text = "All",
                Value = "0"
            });
            CenterType.Add(new SelectListItem()
            {
                Text = "Center",
                Value = "1"
            });
            CenterType.Add(new SelectListItem()
            {
                Text = "Dealer",
                Value = "2"
            });
        }
        public SearchAgingReports SearchCriterias { get; set; }
        public IEnumerable<AgingReport> SearchResults { get; set; }
        public List<SelectListItem> CenterType { get; set; }
        public ReportFormat Format { get; set; }
    }
    public class SearchAgingReports
    {
        [Display(Name = "Registration ID")]
        public int? RegID { get; set; }
        [Display(Name = "MSISDN")]
        public string MSISDN { get; set; }
        [Display(Name = "IC No")]
        public string ICNo { get; set; }
        [Display(Name = "Status")]
        public int? StatusID { get; set; }
        [Display(Name = "Organization")]
        public int? CenterOrgID { get; set; }
        [Display(Name = "Center Type")]
        public int CenterTypeID { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Last Updated By")]
        public string LastUpdatedBy { get; set; }
        [Display(Name = "Registration Date From")]
        public DateTime? RegDateFrom { get; set; }
        [Display(Name = "To")]
        public DateTime? RegDateTo { get; set; }
    }

    #endregion

    #region Biometrics
    public class SearchViewBiometricsReportModel
    {
        public SearchViewBiometricsReportModel()
        {
            SearchCriterias = new SearchBiometricsReports();
            CenterType = new List<SelectListItem>();
        }
        public SearchBiometricsReports SearchCriterias { get; set; }
        public IEnumerable<BiometricsReport> SearchResult { get; set; }
        public List<SelectListItem> CenterType { get; set; }
        public ReportFormat Format { get; set; }
    }
    public class SearchBiometricsReports
    {
        [Display(Name = "ID Card No.")]
        public string IDCardNo { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
    #endregion
}