﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc; 
using System.Web.Routing;
using System.Web.Security;
using log4net;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.Models;
using Online.Registration.Web.Properties;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.SmartViewModels;
using Online.Registration.Web.SubscriberICService;
using SNT.Utility;
using Online.Registration.Web.ContentRepository;


namespace Online.Registration.Web.Areas.Retention.Controllers
{
    //[HandleError(View = "GenericError")]
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class RetentionController : Controller
    {
        int ID;
        Int32 discount = 0;
        string status = string.Empty;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(RetentionController));
        private string dateFormat = "d MMM yyyy";
        ContractModels ContractModel = new ContractModels();
        string MSISDN = string.Empty;
		private bool sharingQuotaFeature = ConfigurationManager.AppSettings["SharingSupplementaryFlag"].ToBool();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Accounts()
        {
            Session[SessionKey.ContractDetails.ToString()] = null;
            Session[SessionKey.AccountCategory.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session["_componentViewModel"] = null;
            Session["RegMobileReg_VasNames"] = null;
            Session["SelectedContractName"] = null;
            Session["RegMobileReg_SelectedContracts"] = null;
            Session["contractName"] = null;
            SubscriberICService.retrieveAcctListByICResponse AcctListByICResponse = null;
            SupplementaryListAccounts SuppListAccounts = null;

            #region Drop 5: BRE Check for non-Drop 4 Flow
            //09042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - Start
            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            var dropObj = new DropFourObj();
            dropObj.TrnType = "S";
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            //09042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - End
            #endregion

            if ((!ReferenceEquals(Session["PPID"], null) && "E".Equals(Convert.ToString(Session["PPID"]))) && !ReferenceEquals(Session["PPIDInfo"], null))
            {
                ///GET PPIDInfo FROM SESSION
                AcctListByICResponse = (SubscriberICService.retrieveAcctListByICResponse)Session["PPIDInfo"];

                ///LIST ONLY GSM A/C'S
                if (AcctListByICResponse.itemList.Where(c => c.Account != null && c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob == "POSTGSM").Count() > 0)
                {
                    SuppListAccounts = new SupplementaryListAccounts();

                    foreach (var v in AcctListByICResponse.itemList.Where(c => c.Account != null && c.ServiceInfoResponse != null && c.ServiceInfoResponse.prinSuppInd != "" && c.ServiceInfoResponse.lob == "POSTGSM" && c.EmfConfigId !=
                                        System.Configuration.ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2()).ToList())
                    {

                        SuppListAccounts.SuppListAccounts.Add(
                            new AddSuppInquiryAccount
                            {
                                AccountNumber = v.AcctExtId,
                                ActiveDate = v.Account.ActiveDate,
                                Address = v.Account.Address,
                                Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                                MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                                CompanyName = v.Account.CompanyName,
                                //Holder = v.Account.Holder,
                                Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                IDNumber = v.Account.IDNumber,
                                IDType = v.Account.IDType,
                                //MarketCode = v.Account.MarketCode,
                                Plan = v.Account.Plan,
                                SubscribeNo = v.SusbcrNo,
                                SubscribeNoResets = v.SusbcrNoResets,
                                AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                AccountName = v.ServiceInfoResponse.lob,
                                externalId = v.ExternalId,
                                accountExtId = v.AcctExtId,
                                accountIntId = v.AcctIntId,
                                IsMISM = v.IsMISM,
                                AccountType = v.ServiceInfoResponse.prinSuppInd.ToString2()
                            });


                    }
                }
            }
            Session["RegMobileReg_BiometricID1"] = Session["RegMobileReg_BiometricID"];
            ResetAllSession(1);
            Session["RegMobileReg_BiometricID"] = Session["RegMobileReg_BiometricID1"];
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();
            return View(SuppListAccounts);
        }

        [HttpPost]
        public ActionResult Accounts(FormCollection collection, string SelectAccountDtls = null)
        {
            if (collection["submit1"].ToString() == "next")
            {
                if (SelectAccountDtls != null)
                {
                    string[] strAccountDtls = SelectAccountDtls.Split(',');
					Session["SelectAccountDtls"] = SelectAccountDtls.ToString2();

                    Session["MSISDN"] = strAccountDtls[1].ToString();
                    Session["SimType"] = strAccountDtls[5].ToString();
                    Session["AccountHolder"] = strAccountDtls[7].ToString2();
                    Session[SessionKey.AccountCategory.ToString()] = strAccountDtls[8].ToString2();
                    Session[SessionKey.MarketCode.ToString()] = strAccountDtls[9].ToString2();
                    //Session["AccountType"] = strAccountDtls[6].ToString();    //commented to replace with below code
                    if (!string.IsNullOrEmpty(collection["AccountType"].ToString2()))
                    {
                        Session["AccountType"] = collection["AccountType"].ToString2();
                    }
                    else
                    {
                        Session["AccountType"] = strAccountDtls[6].ToString();
                    }
                    // to be used for CRPType in ConstructRegistration
                    Session["ActionType"] = collection["ActionType"].ToString2();

                    return RedirectToAction("CmssAndContractCheck", "Retention", new { Area = "Retention", KenanID = strAccountDtls[0].ToString(), MSISDN = strAccountDtls[1].ToString(), SubscriberNo = strAccountDtls[2].ToString(), SubscriberNoResets = strAccountDtls[3].ToString() });
                }
            }
            if (string.IsNullOrWhiteSpace(SelectAccountDtls))
            {
                TempData["Error"] = "Select an account";
                return RedirectToAction("Accounts");
            }
            return RedirectToAction("Accounts");
        }


        /// <summary>
        /// CMSS And Contract Check
        /// </summary>
        /// <returns></returns>
        public ActionResult CmssAndContractCheck()
        {
            Session["_componentViewModel"] = null;
            Session["PenaltyDeviceModel"] = null;
            Session["PenaltyDeviceIMEI"] = null;
            Session["PenaltyUpgradeFee"] = null;
            Session[SessionKey.PenaltyWaiveOff.ToString()] = null;
            Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
            ViewBag.EarlyTerminationFeeLabel = Settings.Default.EarlyTerminationFee;
            ContractDetails ContractDet = new ContractDetails();
            ContractDet = GetContractDetails();
            return View(ContractDet);
        }
        [HttpPost]
        public ActionResult CmssAndContractCheck(ContractDetails model, FormCollection Collection)
        {
            ContractDetails ContractDet = new ContractDetails();
            //List<SubscriberICService.retrievePenaltyByMSISDN> penaltybyMSISDN = new List<SubscriberICService.retrievePenaltyByMSISDN>();
            List<SubscriberICService.retrievePenalty> penaltybyMSISDN = new List<SubscriberICService.retrievePenalty>();




            decimal sumpenalty = 0;
            string strPenalty = string.Empty;
            Session["CMSSResponse"] = null;
            ViewBag.EarlyTerminationFeeLabel = Settings.Default.EarlyTerminationFee;
            string msisdn = Session[SessionKey.RegMobileReg_MSISDN.ToString()].ToString();
            string externalid = ConfigurationManager.AppSettings["KenanMSISDN"].ToString();
            string Subscriber = Session["SubscriberNo"].ToString();
            string SubscriberNoResets = Session["SubscriberNoResets"].ToString();


            if (Session[SessionKey.PPIDInfo.ToString()] != null)
            {
                //List<Online.Registration.Web.SubscriberICService.Items> itemList = ((Online.Registration.Web.SubscriberICService.retrieveAcctListByICResponse) Session[SessionKey.PPIDInfo.ToString()]).itemList.Where(a => a.ExternalId == (msisdn)).ToList();
                List<Online.Registration.Web.SubscriberICService.Items> itemList = ((Online.Registration.Web.SubscriberICService.retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()]).itemList.Where(a => a.ExternalId == (msisdn)).ToList();
                Session[SessionKey.AccountNumber.ToString()] = itemList[0].AcctIntId;
            }

            if (!ReferenceEquals(ModelState.IsValid, null))
            {
                if (ModelState.IsValid)
                {
                    Session["CMSSID"] = model.CMSSID;
                    if (Session["ContractDet"] != null)
                    {
                        ContractDet = (ContractDetails)Session["ContractDet"];
                    }
                    if (Collection["searchCustIDNumber"] != null)
                    {

                        CMSS CMSS = new CMSS();
                        //added by KD on 03/06/2013 for storing CMSS ID in DB for reconcilation report

                        CMSS = Util.retrieveCaseDetls(model.CMSSID, userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
                                password: Properties.Settings.Default.retrieveAcctListByICResponsePassword, postUrl: Properties.Settings.Default.retrieveCMSDetsPostUrl);

                        if (Convert.ToInt16(CMSS.msgCode) == 0)
                        {
                            Session["CMSSResponse"] = CMSS.msgDesc;
                        }
                        else
                        {
                            Session["CMSSResponse"] = CMSS.msgDesc;
                            ViewBag.ErrDesc = CMSS.msgCode;
                            return View(ContractDet);
                        }
                    }
                    else if (Collection["Continue"] != null)
                    {
                        WebHelper.Instance.ReCheckPenaltyWaiveOffEligibility();
                        if (Session["ContractType"] == "Extend")
                        {
                            //Penalty and Waive offs are calculated in Get method are calculated only by considering the pending contract length
                            // In case of Extend, there will be no penalty and waive off, so reset these values
                            if (Session[SessionKey.ContractDetails.ToString()] != null)
                            {
                                ((List<ContractDetails>)Session[SessionKey.ContractDetails.ToString()]).ForEach(c =>
                                {
                                    c.PenaltyWaiveOff = 0; c.Waived = false;
                                });
                            }
                        }

                        //added by KD on 03/06/2013 for storing CMSS ID in DB for reconcilation report
                        Session["CMSSID"] = Session["CMSSID"] == null ? string.Empty : Session["CMSSID"].ToString();
                        Session[SessionKey.CMSID.ToString()] = "Y";
                        if (Session["ContractType"] == null)
                        {
                            Session["ErrMsg"] = ConfigurationManager.AppSettings["ContractErrMsg"].ToString();
                            return View(ContractDet);
                        }
                        else
                        {
                            Session["ErrMsg"] = string.Empty;
                            if (Session[SessionKey.PPID.ToString()] != null)
                            {
                                if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                                {
                                    WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "E");
                                }
                                else
                                {
                                    WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");
                                }

                            }
                            return RedirectToAction("SelectDevice", "Retention", new { type = (int)MobileRegType.DevicePlan });
                        }
                    }
                    else if (Collection["Extend"] != null)
                    {
                        Session[SessionKey.CMSID.ToString()] = "N";
                        Session["ErrMsg"] = string.Empty;
                        Session["Penalty"] = null;
                        Session["ActualPenalty"] = null;

                        if (Session["ContractType"] != null && Session["ContractType"].ToString() == "Extend")
                        {
                            Session["ContractType"] = null;
                        }
                        else
                        {
                            Session["ContractType"] = "Extend";
                        }
                    }
                    else if (Collection["Termination"] != null)
                    {
                        Session["ErrMsg"] = string.Empty;
                        Session[SessionKey.CMSID.ToString()] = "N";
                        if (Session["ContractType"] != null && Session["ContractType"].ToString() == "Terimnate")
                        {
                            Session["Penalty"] = null;
                            Session["ContractType"] = null;
                        }
                        else
                        {
                            Session["ContractType"] = "Terimnate";
                            if (Collection["hasK2Contracts"].ToString() == "True")
                            {
                                decimal panalty = 0;
                                decimal penaltyWaiveOff = 0;
                                var proxy = new retrieveSmartServiceInfoProxy();
                                if (Session["penaltybyMSISDN"] != null)
                                {
                                    penaltybyMSISDN = (List<retrievePenalty>)Session["penaltybyMSISDN"];
                                }
                                // penaltybyMSISDN = proxy.retrievePenalty_Smart(msisdn, externalid, Subscriber, SubscriberNoResets);

                                ContractDet.PenaltyType = penaltybyMSISDN.FirstOrDefault() != null ? penaltybyMSISDN.Where(x => x.penaltyType == "DF").Any() ? "DF" : penaltybyMSISDN.FirstOrDefault().penaltyType : string.Empty;
                                foreach (var penalty in penaltybyMSISDN)
                                {

                                    sumpenalty = penalty.penalty.ToDecimal() + sumpenalty;
                                }
                                strPenalty = sumpenalty.ToString2();
                                if (penaltybyMSISDN != null)
                                {
                                    List<Online.Registration.Web.SmartViewModels.ContractDetails> ContractsList = new List<ContractDetails>();
                                    if (Session[SessionKey.ContractDetails.ToString()] != null)
                                    {
                                        ContractsList = (List<Online.Registration.Web.SmartViewModels.ContractDetails>)(Session[SessionKey.ContractDetails.ToString()]);
                                    }
                                    foreach (var item in penaltybyMSISDN)
                                    {
                                        Session["PenaltyAdminFee"] = item.adminFee;
                                        Session["PenaltyRRP"] = item.rrp;
                                        Session["PenaltyPhoneModel"] = item.phoneModel;
                                        Session["Upfrontpayment"] = item.UpfrontPayment;
                                        Session["PenaltyDeviceModel"] = item.DeviceModel;
                                        Session["PenaltyDeviceIMEI"] = item.DeviceIMEI;
                                        Session["PenaltyUpgradeFee"] = item.UpgradeFee;
                                        panalty += item.penalty.ToDecimal();

                                        penaltyWaiveOff += ContractsList.FirstOrDefault(c => c.ComponentDesc.Equals(item.ContractName)).PenaltyWaiveOff;

                                    }
                                    Session["Penalty"] = panalty;
                                    Session[SessionKey.PenaltyWaiveOff.ToString()] = penaltyWaiveOff;
                                }
                                else
                                {
                                    Session["Penalty"] = "0";
                                    Session[SessionKey.PenaltyWaiveOff.ToString()] = 0;
                                }
                                //Session["Penalty"] = penaltybyMSISDN.penalty;
                                if (!string.IsNullOrEmpty(strPenalty))
                                {
                                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AdminFee"]) && strPenalty.ToDecimal() > 0)
                                        Session["Penalty"] = Convert.ToString(Convert.ToDecimal(strPenalty) + Convert.ToDecimal(ConfigurationManager.AppSettings["AdminFee"]));
                                    else
                                        Session["Penalty"] = strPenalty;
                                }
                                else
                                    Session["Penalty"] = strPenalty;
                            }
                            else if (Collection["hascontracts"].ToString() == "True")
                            {
                                Session[SessionKey.AssignNew.ToString()] = "true";
                                if (Session[SessionKey.AssignNew.ToString()].ToString2().ToUpper() == "TRUE")
                                {
                                    var proxy = new retrieveSmartServiceInfoProxy();
                                    // penaltybyMSISDN = proxy.retrievePenalty_Smart(msisdn, externalid, Subscriber, SubscriberNoResets);
                                    if (Session["penaltybyMSISDN"] != null)
                                    {
                                        penaltybyMSISDN = (List<retrievePenalty>)Session["penaltybyMSISDN"];
                                    }
                                    ContractDet.PenaltyType = penaltybyMSISDN.FirstOrDefault() != null ? penaltybyMSISDN.Where(x => x.penaltyType == "DF").Any() ? "DF" : penaltybyMSISDN.FirstOrDefault().penaltyType : string.Empty;
                                
                                    foreach (var penalty in penaltybyMSISDN)
                                    {
                                        sumpenalty = penalty.penalty.ToDecimal() + sumpenalty;
                                    }
                                    decimal panalty = 0;
                                    strPenalty = sumpenalty.ToString2();
                                    if (penaltybyMSISDN != null)
                                    {
                                        foreach (var item in penaltybyMSISDN)
                                        {
                                            Session["PenaltyAdminFee"] = item.adminFee;
                                            Session["PenaltyRRP"] = item.rrp;
                                            Session["PenaltyPhoneModel"] = item.phoneModel;
                                            Session["Upfrontpayment"] = item.UpfrontPayment;
                                            Session["PenaltyDeviceModel"] = item.DeviceModel;
                                            Session["PenaltyDeviceIMEI"] = item.DeviceIMEI;
                                            Session["PenaltyUpgradeFee"] = item.UpgradeFee;
                                            panalty += item.penalty.ToDecimal();

                                        }
                                        Session["Penalty"] = panalty;
                                    }
                                    else
                                    {
                                        Session["Penalty"] = "0";
                                    }
                                    //Session["Penalty"] = penaltybyMSISDN.penalty;
                                    if (!string.IsNullOrEmpty(strPenalty))
                                    {
                                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AdminFee"]) && strPenalty.ToDecimal() > 0)
                                            Session["Penalty"] = Convert.ToString(Convert.ToDecimal(strPenalty) + Convert.ToDecimal(ConfigurationManager.AppSettings["AdminFee"]));
                                        else
                                            Session["Penalty"] = strPenalty;
                                    }
                                    else
                                        Session["Penalty"] = strPenalty;
                                }
                            }
                            else
                            { Session["Penalty"] = "0"; }
                            Session["ActualPenalty"] = Session["Penalty"].ToString2();
                        }
                    }
                }
            }
            return View(ContractDet);
        }
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectDevice(int? type)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session["DiscountStatus"] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session["NetPrice"] = null;
            Session["DPrice"] = null;
            Session["isPenaltyCheck"] = null;
            if (Session["Penalty"] == null)
                Session["Penalty"] = Session["ActualPenalty"].ToString2();
            Session["RegMobileReg_BiometricID1"] = Session["RegMobileReg_BiometricID"];
            ResetAllSession(type);
            Session["RegMobileReg_BiometricID"] = Session["RegMobileReg_BiometricID1"];
            SetRegStepNo(SmartRegistrationSteps.Device);
            bool isSubLine = false;
            if (Session["AccountType"].ToString2() == "S")
                isSubLine = true;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary(isSubLine);
            var deviceOptVM = GetAvailableDeviceOptions();
            //28042015 - Anthony - to validate the BRE check message in order summary if no contract added - Start
            string JustificationMessage = string.Empty;
            string HardStopMessage = string.Empty;
            string ApprovalMessage = string.Empty;
            bool isBREFail = false;
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            WebHelper.Instance.BRECheckMessage(out HardStopMessage, out JustificationMessage, out ApprovalMessage, out isBREFail);
            dropObj.SetAdditionalContract();
            //28042015 - Anthony - to validate the BRE check message in order summary if no contract added - End
            
            return View(deviceOptVM);
        }
        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectDevice(DeviceOptionVM devOptVM, FormCollection Collection)
        {
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = devOptVM.TabNumber;
            //return RedirectToAction("CmssAndContractCheck", "Retention", new { KenanID = par[0], MSISDN = par[1], SubscriberNo = par[2], SubscriberNoResets = par[3], Area = "Retention" });
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            dropObj.SetAdditionalContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            var options = (!string.IsNullOrEmpty(devOptVM.SelectedDeviceOption)) ? devOptVM.SelectedDeviceOption.Split('_').ToList() : null;
            if (Collection["submit"].ToString2().ToUpper() == "BACK")
            {
                return RedirectToAction("CmssAndContractCheck", "Retention", new { Area = "Retention" });
            }
            else
            {
                if (options != null)
                {
                    try
                    {
                        if (devOptVM.TabNumber == (int)SmartRegistrationSteps.DeviceCatalog)
                        {
                            // selected different Device Option
                            if (Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToInt() != options[1].ToInt() ||
                                Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToInt() != options[2].ToInt())
                            {
                                Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = options[1];
                                Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = options[2];

                                // reset session
                                Session["RegMobileReg_PhoneVM"] = null;
                                Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
                                Session["RegMobileReg_SelectedModelID"] = null;
                                Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
                                Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = null;
                                Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
                                Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
                                Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
                                Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
                                Session[SessionKey.RegMobileReg_MobileNo.ToString()] = null;
                                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //LogException(ex);
                        //return RedirectException(ex);
                        throw;
                    }
                    return RedirectToAction(GetRegActionStep(devOptVM.TabNumber));
                }
                else
                {
                    return View(GetRegActionStep(1));

                }
            }

        }
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectDeviceCatalog()
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session["SelectedModelID"] = null;
            Session["SelectedPlanID"] = null;
            SetRegStepNo(SmartRegistrationSteps.DeviceCatalog);
            if (Session["Penalty"] == null)
                Session["Penalty"] = Session["ActualPenalty"].ToString();
            bool isSubLine = false;
            if (Session["AccountType"].ToString2() == "S")
                isSubLine = true;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary(isSubLine);
            var phoneVM = GetAvailableModelImage();

            return View(phoneVM);
        }
        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectDeviceCatalog(PhoneVMSmart phoneVM)
        {
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = phoneVM.TabNumber;
            try
            {
                var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
                dropObj.SetAdditionalContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
                if (phoneVM.TabNumber == (int)SmartRegistrationSteps.Plan)
                {
                    // selected different Model Image
                    Session["SelectedModelID"] = phoneVM.ModelID;
                    if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToString2() != phoneVM.SelectedModelImageID)
                    {
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = phoneVM.SelectedModelImageID;

                        // reset session
                        Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
                        Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = null;
                        Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
                        Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
                        Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
                        Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
                    }
                }

                // Outright Sales
                if ((int)Session[SessionKey.RegMobileReg_Type.ToString()] == (int)MobileRegType.DeviceOnly)
                {
                    Session[SessionKey.RegMobileReg_TabIndex.ToString()] = (int)SmartRegistrationSteps.PersonalDetails;
                    phoneVM.TabNumber = (int)SmartRegistrationSteps.PersonalDetails;
                }
            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            return RedirectToAction(GetRegActionStep(phoneVM.TabNumber));
        }
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectPlan(int type = 0)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session["VasBox"] = false;
            Session["HidevasButton"] = true;
            Session["SelectedCompIdsForCheck"] = null;
          
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
            {
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            }

            if (type > 0)
            {
                ResetAllSession(type);
            }
            Session["isPenaltyCheck"] = null;
            if (Session["Penalty"] == null)
                Session["Penalty"] = Session["ActualPenalty"].ToString();

            #region Added by Chetan for hiding the contrats & vas in planonly
            if (type == 2)
            {
                Session[SessionKey.MobileRegType.ToString()] = "Planonly";
            }
            else
            {
                Session[SessionKey.MobileRegType.ToString()] = "Other";
            }
            #endregion

            SetRegStepNo(SmartRegistrationSteps.Plan);

            bool isSubLine = false;
            if (Session["AccountType"].ToString2() == "S")
                isSubLine = true;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary(isSubLine);
            GetExtendPlans();
            //var packageVM = GetAvailablePackages();

            var packageVM = GetAvailablePackages(isSubLine);

			if (sharingQuotaFeature)
			{
				packageVM = WebHelper.Instance.checkPrinSuppPlanEligibility_SMART(packageVM, null, isCRP: true);
			}

            return View(packageVM);
        }
        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectPlan(PackageVM packageVM)
        {
            //VLT ADDED CODE to check for PPID verification
            if (Session["_componentViewModel"] != null)
                Session["_componentViewModel"] = null;
            if (Session[SessionKey.PPID.ToString()].ToString() != "")
            {
                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null))
                {
                    Session.Contents.Remove("RedirectURL");
                }

                Session[SessionKey.RegMobileReg_TabIndex.ToString()] = packageVM.TabNumber;
                try
                {
                    //commented by KD
                    //if (packageVM.TabNumber == (int)SmartRegistrationSteps.Vas)
                    if (packageVM.TabNumber == (int)SmartRegistrationSteps.SelectComponent)
                    {
                        // selected different Plan
                        if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt() != packageVM.SelectedPgmBdlPkgCompID)
                        {
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = packageVM.SelectedPgmBdlPkgCompID;

                            Session["SelectedPlanID"] = packageVM.SelectedPgmBdlPkgCompID;

                            // Retrieve Plan Min Age
                            using (var proxy = new SmartCatelogServiceProxy())
                            {
                                var pbpc = proxy.PgmBdlPckComponentGetSmart(new int[] { packageVM.SelectedPgmBdlPkgCompID }).SingleOrDefault();

                                Session["ContractId"] = pbpc.KenanCode;

                                var bundleID = pbpc.ParentID;
                                var programID = proxy.PgmBdlPckComponentGetSmart(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                                {

                                    PgmBdlPckComponent = new Online.Registration.DAL.Models.PgmBdlPckComponent()
                                    {
                                        ChildID = bundleID,
                                        LinkType = Properties.Settings.Default.Program_Bundle
                                    },
                                    Active = true
                                })).SingleOrDefault().ParentID;

                                var minAge = proxy.ProgramGet(new int[] { programID }).SingleOrDefault().MinAge;
                                Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = minAge;
                            }

                            // reset session
                            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
                            Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
                            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
                            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
                        }
                    }

                }
                catch (Exception ex)
                {
                    //LogException(ex);
                    //return RedirectException(ex);
                    throw;
                }
                return RedirectToAction(GetRegActionStep(packageVM.TabNumber));
                //return RedirectToAction("SelectComponent");
            }
            else
            {
                Session[SessionKey.RedirectURL.ToString()] = "GuidedSales/PlanSearch";
                return RedirectToAction("Index", "Home");
            }
        }
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectVAS()
        {
            Session["VasBox"] = "True";
            Session["HidevasButton"] = null;
            Session["isPenaltyCheck"] = null;
            Session["contractName"] = null;
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            if (Session["Penalty"] == null)
                Session["Penalty"] = Session["ActualPenalty"].ToString();
            ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
            if (Session[SessionKey.PPID.ToString()].ToString() != "")
            {
                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null))
                {
                    Session.Contents.Remove("RedirectURL");
                    Session[SessionKey.MenuType.ToString()] = "1";
                }
                SetRegStepNo(SmartRegistrationSteps.Vas);
                if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] == null || (Convert.ToString(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]) == String.Empty))
                {
                    Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = Session["GuidedSales_PkgPgmBdlPkgCompID"];
                }

                //Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();
                bool isSubLine = false;
                if (Session["AccountType"].ToString2() == "S")
                    isSubLine = true;
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary(isSubLine);

                Lstvam = GetAvailablePackageComponents(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), isMandatory: false);

                string MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();

                Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);

                Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);

                var Ivalexists = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).PlanBundle;

                /* For Mandatory packages */
                Session["vasmandatoryids"] = "";
                using (var proxy = new SmartCatelogServiceProxy())
                {

                    BundlepackageResp respon = new BundlepackageResp();

                    respon = proxy.GetMandatoryVas(Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]));

                    for (int i = 0; i < respon.values.Count(); i++)
                    {

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Master_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            Session["intmandatoryidsval"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
                        }

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.CompType_DataCon)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == "0").Select(a => a.BdlDataPkgId).ToList();
                                if (Finalintvalues.Count() == 0)
                                {
                                    Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();
                                }

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value

                                                             };


                            Session["intdataidsval"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts);
                        }
                        /*Extra packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Extra_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value

                                                             };


                            Session["intextraidsval"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);

                            //Session["intextraidsval"] = respon.values[i].BdlDataPkgId;

                            //Lstvam2.ExtraComponents.AddRange(Lstvam.AvailableVASes);
                        }

                    }




                }


                if (MobileRegType == "Planonly")
                {
                    //vasVM.AvailableVASes.Clear();
                    Lstvam.ContractVASes.Clear();

                }

                return View(Lstvam2);

            }
            else
            {
                Session[SessionKey.RedirectURL.ToString()] = "Registration/SelectVAS";
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectVAS(ValueAddedServicesVM vasVM)
        {
            Session["isPenaltyCheck"] = null;
            if (Session["Penalty"] == null)
                Session["Penalty"] = Session["ActualPenalty"].ToString();
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = vasVM.SelectedContractID;
            Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID;
            //  K2ContractCheck();

            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = vasVM.TabNumber;
            //modefied by chetan
            var selectedVasIDs = "";
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            dropObj.SetAdditionalContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow

            if (vasVM.SelectedVasIDs != null)
            {
                if (vasVM.SelectedVasIDs != null && !Regex.IsMatch(vasVM.SelectedVasIDs, @"^,+$"))
                {
                    selectedVasIDs = vasVM.SelectedVasIDs;
                    Session["RegMobileReg_DataPkgID"] = selectedVasIDs.Remove(4);
                }
            }
            else
            {
                if (!ReferenceEquals(Session["RegMobileReg_DataPkgID"], null))
                {
                    Session.Contents.Remove("RegMobileReg_DataPkgID");
                }
                if (!ReferenceEquals(Session[SessionKey.RegMobileReg_VasIDs.ToString()], null))
                {
                    Session.Contents.Remove("RegMobileReg_VasIDs");
                }
            }

            try
            {
                if (vasVM.TabNumber == (int)SmartRegistrationSteps.Penalty && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) || !string.IsNullOrEmpty(vasVM.SelectedContractID)))
                {
                    if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                    {
                        Session["contractName"] = vasVM.SelectedContractName;
                        #region VLT ADDED CODE
                        //Contract check for existing users
                        //Retrieve data from session
                        #endregion VLT ADDED CODE
                        if (!string.IsNullOrEmpty(Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()))
                            if (vasVM.SelectedVasIDs != null)
                                selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()), "");

                        selectedVasIDs += vasVM.SelectedContractID + ",";
                    }

                    vasVM.SelectedVasIDs = selectedVasIDs;

                    Session[SessionKey.RegMobileReg_VasIDs.ToString()] = selectedVasIDs;
                    Session[SessionKey.RegMobileReg_ContractID.ToString()] = vasVM.SelectedContractID;

                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        Session[SessionKey.RegMobileReg_DataplanID.ToString()] = proxy.GettllnkContractDataPlan(Convert.ToInt32(Session[SessionKey.RegMobileReg_ContractID.ToString()]));
                    }

                }
            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == true)
            {
                return RedirectToAction(GetRegActionStep(vasVM.TabNumber + 1));
            }
            else
            {
                if (!string.IsNullOrEmpty(Session[SessionKey.MobileRegType.ToString()].ToString()))
                {
                    string MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();
                    if (MobileRegType == "Planonly")
                    {
                        return RedirectToAction(GetRegActionStep(SmartRegistrationSteps.PersonalDetails.ToInt()), new { type = 2 });
                    }
                    return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
                }
                else
                {
                    return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
                }
            }
        }
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult PersonalDetails()
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session["DiscountStatus"] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session["isPenaltyCheck"] = null;
            if (Session["Penalty"] == null && Session["FullPenalty"].ToString2() == null)
                Session["Penalty"] = Session["ActualPenalty"].ToString2();
            SetRegStepNo(SmartRegistrationSteps.PersonalDetails);
            //Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();
            bool isSubLine = false;
            if (Session["AccountType"].ToString2() == "S")
                isSubLine = true;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary(isSubLine);

            var idCardTypeID = Session[SessionKey.RegMobileReg_IDCardType.ToString()] != null ? Session[SessionKey.RegMobileReg_IDCardType.ToString()] : null;
            var personalDetailsVM = new PersonalDetailsVM();
            if ((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"] != null)
            {
                personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            }

            personalDetailsVM.Customer.IDCardTypeID = idCardTypeID.ToInt();
            personalDetailsVM.Customer.IDCardNo = Session[SessionKey.RegMobileReg_IDCardNo.ToString()] == null ? "" : Session[SessionKey.RegMobileReg_IDCardNo.ToString()].ToString2();

            return View(personalDetailsVM);
        }
        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult PersonalDetails(PersonalDetailsVM personalDetailsVM, FormCollection collection)
        {
            try
            {
                if (personalDetailsVM.TabNumber == (int)SmartRegistrationSteps.Summary)
                {
					// MyMaxis app survey 
					personalDetailsVM.SurveyFeedBack = collection["surveyFeedback"].ToString2();
                    //personalDetailsVM.SurveyAnswer = !string.IsNullOrWhiteSpace(personalDetailsVM.SurveyFeedBack) ? "Yes" : "No";
                    personalDetailsVM.SurveyAnswerRate = collection["surveyAnswerRate"].ToString2();
                    personalDetailsVM.SurveyAnswer = collection["surveyAnswer"].ToString2();

                    personalDetailsVM.SingleSignOnValue = collection["SingleSignOnValue"].ToString2();
                    personalDetailsVM.SingleSignOnEmailAddress = collection["SingleSignOnEmailAddress"].ToString2();
                    
					//clearing session if filename not exist 
					if (string.IsNullOrEmpty(personalDetailsVM.frontDocumentName)) Session["frontImage"] = null;
					if (string.IsNullOrEmpty(personalDetailsVM.backDocumentName)) Session["backImage"] = null;
					if (string.IsNullOrEmpty(personalDetailsVM.othDocumentName)) Session["otherImage"] = null;

                    #region Drop 5: BRE Check for non-Drop 4 Flow
                    //08042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - Start
                    var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                    var justificationInput = personalDetailsVM.Justification.ToString2().Trim();
                    string JustificationMessage = string.Empty;
                    string HardStopMessage = string.Empty;
                    string ApprovalMessage = string.Empty;
                    bool isBREFail = false;
                    WebHelper.Instance.BRECheckMessage(out HardStopMessage, out JustificationMessage, out ApprovalMessage, out isBREFail);

                    if (!string.IsNullOrEmpty(HardStopMessage) || (!string.IsNullOrEmpty(JustificationMessage) && string.IsNullOrEmpty(justificationInput)))
                    {
                        ViewBag.GSTMark = Settings.Default.GSTMark;
                        List<String> LabelReplace = new List<String>();
                        LabelReplace.Add("GSTMark");
                        ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
                    }

                    if (!string.IsNullOrEmpty(HardStopMessage))
                    {
                        ModelState.AddModelError(string.Empty, HardStopMessage);
                        return View(personalDetailsVM);
                    }
                    else if (!string.IsNullOrEmpty(JustificationMessage) && string.IsNullOrEmpty(justificationInput))
                    {
                        ModelState.AddModelError(string.Empty, "Please Enter Justification for - " + JustificationMessage);
                        return View(personalDetailsVM);
                    }
                    //08042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - End
                    #endregion
                    personalDetailsVM.Customer.CustomerTitleID = Session["cTitleID"].ToInt();
                    personalDetailsVM.Customer.Gender = "M";
                    personalDetailsVM.Customer.IDCardTypeID = personalDetailsVM.InputIDCardTypeID.ToInt();
                    personalDetailsVM.Customer.IDCardNo = personalDetailsVM.InputIDCardNo;
                    /*Added by Rajeswari on 24th March to disable nationality*/
                    personalDetailsVM.Customer.NationalityID = personalDetailsVM.InputNationalityID;
                    personalDetailsVM.Customer.EmailAddr = collection["CustEmailAddress"].ToString2();
					
                    Session[SessionKey.RegMobileReg_TabIndex.ToString()] = personalDetailsVM.TabNumber;
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;
                    if ((personalDetailsVM.DOBDay == 0 && personalDetailsVM.DOBMonth == 0 && personalDetailsVM.DOBYear == 0) || (personalDetailsVM.DOBDay == null && personalDetailsVM.DOBMonth == null && personalDetailsVM.DOBYear == null))
                    {
                        personalDetailsVM.Customer.DateOfBirth = DateTime.MinValue;
                    }

                    if (personalDetailsVM.Customer.PayModeID == Util.GetPaymentModeID(PAYMENTMODE.AutoBilling))
                    {
                        if (!ValidateCardDetails(personalDetailsVM.Customer))
                        {
                            return View(personalDetailsVM);
                        }
                    }
                }
                else
                {
                    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                    {
                        Session[SessionKey.RegMobileReg_TabIndex.ToString()] = (int)SmartRegistrationSteps.DeviceCatalog;
                        personalDetailsVM.TabNumber = (int)SmartRegistrationSteps.DeviceCatalog;
                    }
                }
            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            return RedirectToAction(GetRegActionStep(personalDetailsVM.TabNumber));
        }
        public ActionResult SelectDiscount()
        {
            Session["DiscountStatus"] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session["NetPrice"] = null;
            Session["DPrice"] = null;
            Session["isPenaltyCheck"] = null;
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            ViewBag.EarlyTerminationFeeLabel = Settings.Default.EarlyTerminationFee;
            if (Session["Penalty"] == null && Session["ActualPenalty"].ToString2() == null)
                Session["Penalty"] = Session["ActualPenalty"].ToString();
            Online.Registration.Web.SmartViewModels.SmartModel Smart = new Online.Registration.Web.SmartViewModels.SmartModel();
            //OrderSummaryVM OrderSummary = ConstructOrderSummary();
            bool isSubLine = false;
            if (Session["AccountType"].ToString2() == "S")
                isSubLine = true;
            OrderSummaryVM OrderSummary = ConstructOrderSummary(isSubLine);
            Smart.PhoneModel = OrderSummary.ModelName;
            Smart.PhoneColor = OrderSummary.ColourName;
            Smart.DevicePrice = OrderSummary.ModelPrice.ToString();
            Session["RegMobileReg_RRPPrice"] = Smart.DevicePrice.ToString();
            SetRegStepNo(SmartRegistrationSteps.Penalty);
            return View(Smart);
        }
        [HttpPost]
        public ActionResult SelectDiscount(FormCollection Collection, SmartModel smartmodelVM)
        {
            string discountID = string.Empty;
            SmartRebateReq oReq = new SmartRebateReq();
            SmartRebateRes oRp = new SmartRebateRes();
            SmartRebates smartRebate = new SmartRebates();
            var proxy = new SmartCatelogServiceProxy();
            ViewBag.EarlyTerminationFeeLabel = Settings.Default.EarlyTerminationFee;
            if (Collection["Button"] == null)
            {
                if (GetDiscount(Collection, smartmodelVM))
                {

                    Online.Registration.Web.SmartViewModels.SmartModel Smart = new Online.Registration.Web.SmartViewModels.SmartModel();
                    //OrderSummaryVM OrderSummary = ConstructOrderSummary();
                    bool isSubLine = false;
                    if (Session["AccountType"].ToString2() == "S")
                        isSubLine = true;
                    OrderSummaryVM OrderSummary = ConstructOrderSummary(isSubLine);
                    Smart.PhoneModel = OrderSummary.ModelName;
                    Smart.PhoneColor = OrderSummary.ColourName;
                    Smart.DevicePrice = OrderSummary.ModelPrice.ToString();
                    //ViewData["NetPrice"] = Convert.ToDouble(Convert.ToDouble(Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()].ToString()) - discount).ToString();
                    ViewData["NetPrice"] = Convert.ToDouble(Convert.ToDouble(Smart.DevicePrice) - discount).ToString();
                    Session["NetPrice"] = ViewData["NetPrice"];

                    discount = Session["DPrice"].ToInt();
                    if (Collection["chkRebatel1000"].ToString().ToLower() == "freedevice")
                    {
                        //As per POS specfications we need to pass 40 configued in settings and discount as 100 (percentage)
                        discountID = "0";
                        if (ConfigurationManager.AppSettings["FreeHandSetDiscountId"] != null)
                            discountID = ConfigurationManager.AppSettings["FreeHandSetDiscountId"].ToString();
                    }
                    else
                    {
                        smartRebate.Rebate = discount.ToString();
                        oReq.SmartRebates = smartRebate;
                        if (discount != 0)
                            discountID = proxy.GetDiscountID(oReq);
                        else
                            discountID = "40";
                    }
                    Session["discountID"] = discountID.ToString();
                    if (discount > Convert.ToInt32(ConfigurationManager.AppSettings["SmartDiscountLimit"]))
                    {
                        if (!Roles.IsUserInRole("MREG_SV") || !Roles.IsUserInRole("SMRT_MANAGER"))
                        {
                            Session["DiscountStatus"] = "R";
                            status = "P";
                        }
                        else
                        {
                            Session["DiscountStatus"] = "A";
                            status = "A";
                        }
                    }
                    else
                    {
                        Session["DiscountStatus"] = "A";
                        status = "A";
                    }
                    //Commented by Kota Durga Prasad to hide penalty waiver option
                    //if (Collection["penalty"] != null)
                    //{
                    //    smartmodelVM.isPenaltyChecked = true;
                    //    Session["isPenaltyCheck"] = true;
                    //    Session["FullPenalty"] = "N";
                    //    if (Collection["penalty"].ToString().ToLower() == "fullpenaltywaiver")
                    //    {
                    //        //Session["Penalty"] = null;
                    //        Session["FullPenalty"] = "Y";
                    //        Session["DiscountStatus"] = "R";
                    //        status = "P";
                    //    }
                    //}
                    if (status == "P")
                        return View(Smart);
                    else
                        return RedirectToAction(GetRegActionStep(smartmodelVM.TabNumber));
                }
                else
                {
                    if (smartmodelVM.TabNumber == 4)
                        return RedirectToAction(GetRegActionStep(smartmodelVM.TabNumber));
                    else
                        return View();

                    //if (Session[SessionKey.Discount.ToString()] != null)
                    //    return View();
                    //else
                    //    return RedirectToAction(GetRegActionStep(smartmodelVM.TabNumber));
                }
            }
            #region Commneted by sutan
            else
                //{
                //    bool isValidUser = isvalidSupervisor(Collection["Username"].ToString(), Collection["Password"].ToString());
                //    if (isValidUser)
                //    {
                //        smartmodelVM.TabNumber = smartmodelVM.TabNumber + 1;
                //        return RedirectToAction(GetRegActionStep(smartmodelVM.TabNumber));
                //    }
                //    else
                //        return View();
                //}
            #endregion Commneted by sutan
                return View();
        }
        public ActionResult MobileRegSummary(int? regID)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            ViewBag.EarlyTerminationFeeLabel = Settings.Default.EarlyTerminationFee;
            Session["VasBox"] = "True";
            if (regID > 0)
                Session["RegMobileReg_PersonalDetailsVM"] = null;
            if (regID == 0)
            {
                Session["isPenaltyCheck"] = null;
                if (Session["Penalty"] == null && Session["ActualPenalty"].ToString2() == null)
                    Session["Penalty"] = Session["ActualPenalty"].ToString2();
            }
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            var orderSummary = new OrderSummaryVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new Online.Registration.DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            int paymentstatus = -1;
            #region VLT ADDED CODE 21stFEB for Email functionality
            Session["RegID"] = regID;
            #endregion VLD ADDED CODE 21stFEB for Email functionality
            if (regID.ToInt() > 0)
            {
                ClearRegistrationSession();
                personalDetailsVM = new PersonalDetailsVM()
                {
                    RegID = regID.Value.ToInt(),
                };

                Session["RegMobileReg_PhoneVM"] = null;

                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    reg = proxy.RegistrationGet(regID.Value);
                    //Added by Patanjali to get the payment status on 07-04-2013
                    paymentstatus = proxy.PaymentStatusGet(regID.Value);
                    if (paymentstatus != -1)
                    {
                        if (paymentstatus == 0)
                        {
                            personalDetailsVM.PaymentStatus = "0";
                        }
                        else
                        {
                            personalDetailsVM.PaymentStatus = "1";
                        }
                    }
                    else
                    {
                        personalDetailsVM.PaymentStatus = "0";
                    }
                    //Added by Patanjali to get the payment status on 07-04-2013
                    personalDetailsVM.PenaltyWaivedAmount = reg.PenaltyWaivedAmt;
                    personalDetailsVM.QueueNo = reg.QueueNo;
                    //Added by Patanjali to support remarks on 30-03-2013
                    personalDetailsVM.Remarks = reg.Remarks;
                    //Added by Patanjali to support remarks on 30-03-2013 ends here
                    personalDetailsVM.BiometricVerify = reg.BiometricVerify;
                    personalDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
                    personalDetailsVM.BlacklistInternal = reg.InternalBlacklisted;
                    personalDetailsVM.SignatureSVG = reg.SignatureSVG;
                    personalDetailsVM.CustomerPhoto = reg.CustomerPhoto;
                    personalDetailsVM.AltCustomerPhoto = reg.AltCustomerPhoto;
                    personalDetailsVM.RFSalesDT = reg.RFSalesDT;
                    personalDetailsVM.Photo = reg.Photo;
                    personalDetailsVM.RegSIMSerial = reg.SIMSerial;
                    personalDetailsVM.RegIMEINumber = reg.IMEINumber;
                    personalDetailsVM.RegTypeID = reg.RegTypeID;
                    personalDetailsVM.AgeCheckStatus = reg.AgeCheckStatus;
                    personalDetailsVM.DDMFCheckStatus = reg.DDMFCheckStatus;
                    personalDetailsVM.AddressCheckStatus = reg.AddressCheckStatus;
                    personalDetailsVM.OutStandingCheckStatus = reg.OutStandingCheckStatus;
                    personalDetailsVM.TotalLineCheckStatus = reg.TotalLineCheckStatus;
                    personalDetailsVM.PrincipleLineCheckStatus = reg.PrincipleLineCheckStatus;
                    personalDetailsVM.ContractCheckStatus = reg.ContractCheckStatus;
                    personalDetailsVM.UserType = reg.UserType;
                    personalDetailsVM.PlanAdvance = reg.PlanAdvance;
                    personalDetailsVM.PlanDeposit = reg.PlanDeposit;
                    personalDetailsVM.DeviceAdvance = reg.DeviceAdvance;
                    personalDetailsVM.DeviceDeposit = reg.DeviceDeposit;
                    personalDetailsVM.Discount = reg.Discount;
                    personalDetailsVM.KenanAccountNo = reg.KenanAccountNo;
                    personalDetailsVM.OutstandingAcs = reg.OutstandingAcs;
                    personalDetailsVM.PrePortInCheckStatus = reg.PrePortInCheckStatus;
                    personalDetailsVM.MNPServiceIdCheckStatus = reg.MNPServiceIdCheckStatus;
                    personalDetailsVM.MOC_Status = reg.MOC_Status;
                    personalDetailsVM.Whitelist_Status = reg.Whitelist_Status;
                    personalDetailsVM.SM_ContractType = reg.SM_ContractType;
                    personalDetailsVM.IsVerified = reg.IsVerified;
                    personalDetailsVM.PenaltyWaivedAmount = reg.PenaltyWaivedAmt;


                    // RegAccount
                    var regAccount = proxy.RegAccountGet(new int[] { reg.ID }).SingleOrDefault();
                    if (regAccount != null)
                    {
                        personalDetailsVM.ServiceActivateErrMsg = regAccount.KenanActivateServiceErrMessage;
                    }

                    // Registration Status
                    var regStatusID = proxy.RegStatusFind(new RegStatusFind()
                    {
                        RegStatus = new RegStatus()
                        {
                            RegID = reg.ID
                        },
                        Active = true
                    }).SingleOrDefault();

                    personalDetailsVM.Deposite = reg.AdditionalCharges;
                    personalDetailsVM.StatusID = proxy.RegStatusGet(new int[] { regStatusID }).SingleOrDefault().StatusID;
                    personalDetailsVM.PenaltyAmount = reg.PenaltyAmount.ToDecimal();
                    using (var configProxy = new ConfigServiceProxy())
                    {
                        personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;
                    }

                    using (var regPro = new RegistrationServiceProxy())
                    {
                        var result = regPro.LnkRegistrationGetByRegId(regID.ToInt());
                        if (result != null && result.ContractCheckCount != null)
                        {
                            personalDetailsVM.ContractCheckCount = result.ContractCheckCount;
                            personalDetailsVM.TotalLineCheckCount = result.TotalLineCheckCount;
                            personalDetailsVM.Justification = result.Justification;//Added for justification CR
                        }
                    }

                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);
                    Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;

                    // Customer
                    personalDetailsVM.Customer = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    // Address
                    var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                    {
                        Active = true,
                        Address = new Address()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();
                    personalDetailsVM.Address = proxy.RegAddressGet(regAddrIDs).SingleOrDefault();

                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    // RegMdlGrpModels
                    var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                    {
                        RegMdlGrpModel = new RegMdlGrpModel()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();
                    if (regMdlGrpModelIDs.Count() > 0)
                        regMdlGrpModel = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).SingleOrDefault();

                    if (regMdlGrpModel.ID == 0)
                    {
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
                    }
                    else
                    {
                        Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = regMdlGrpModel.Price.ToDecimal();
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
                    }

                    // RegPgmBdlPkgComponent
                    var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                    {
                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();

                    // Main Line PgmBdlPkgComponent
                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    if (regPgmBdlPkgCompIDs.Count() > 0)
                        regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind()
                    {
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();

                    if (suppLineIDs.Count() > 0)
                    {
                        suppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    }
                }

                List<RegSmartComponents> listRegSmartComponents = new List<RegSmartComponents>();

                listRegSmartComponents = GetSelectedComponents(regID.Value.ToString2());
                if (listRegSmartComponents.Count > 0)
                {
                    List<string> Cmpnts = listRegSmartComponents.Select(a => a.ComponentDesc).ToList();
                    if (Cmpnts != null && Cmpnts.Any())
                        Session["RegMobileReg_SelectedContracts"] = Cmpnts.Distinct().ToList();
                    orderSummary.SelectedComponets = listRegSmartComponents;

                }

                if (suppLines.Count() > 0)
                    ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

                using (var proxy = new SmartCatelogServiceProxy())
                {
                    string userICNO = string.Empty;
                    if (Util.SessionAccess.User.IDCardNo != null)
                    {
                        userICNO = Util.SessionAccess.User.IDCardNo;
                    }

                    if (userICNO != null && userICNO.Length > 0)
                    {
                        objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                    }
                    if (objWhiteListDetailsResp != null)
                    {
                        if (objWhiteListDetailsResp.Description != null)
                        {
                            personalDetailsVM._WhiteListDetails.Description = objWhiteListDetailsResp.Description;
                        }
                    }

                    if (regMdlGrpModel.ID > 0)
                        //Session["RegMobileReg_SelectedModelID"] = proxy.ModelImageGet(new int[] { regMdlGrpModel.ModelImageID }).SingleOrDefault().ModelID;
                        Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;


                    var mainLinePBPCs = new List<PgmBdlPckComponentSmart>(); //modified for one plan cr
                    if (mainLinePBPCIDs.Count() > 0)
                        mainLinePBPCs = proxy.PgmBdlPckComponentGetSmart(mainLinePBPCIDs).ToList();//modified for one plan cr

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var DPCode = Properties.Settings.Default.RegType_DevicePlan;
                    var MAcode = Properties.Settings.Default.Master_Component;
                    var MPcode = Properties.Settings.Default.Main_package;
                    var CPCode = Properties.Settings.Default.ComplimentaryPlan;
                    var DCCode = Properties.Settings.Default.CompType_DataCon;
                    var ECCode = Properties.Settings.Default.Extra_Component;
                    var SPCode = Properties.Settings.Default.SupplimentaryPlan;



                    if (mainLinePBPCs.Count() > 0)
                    {
                        Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode || a.PlanType == SPCode)).SingleOrDefault().ID;
                        //Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode)).SingleOrDefault().ID;
                        // Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode)).FirstOrDefault();

                        var vasIDs = string.Empty;
                        var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode).Select(a => a.ID).ToList();

                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session[SessionKey.RegMobileReg_VasIDs.ToString()] = vasIDs;
                    }
                    //if (suppLinePBPCIDs.Count() > 0)
                    //{
                    //    var suppLinePBPCs = proxy.PgmBdlPckComponentGet(suppLinePBPCIDs).ToList();
                    //     Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode).SingleOrDefault().ID;
                    //}
                }

                //for tfs Bug id:2637
                using (var regProxy = new RegistrationServiceProxy())
                {
                    DAL.Models.LnkRegDetails lnkregdetails = regProxy.LnkRegistrationGetByRegId(regID.ToInt());
                    if (lnkregdetails != null)
                    {
                        personalDetailsVM.ContractCheckCount = lnkregdetails.ContractCheckCount.ToInt();
                    }

                    //<Start> - 20150914 - Samuel - display sales code in summary page
                    //personalDetailsVM.Registration = regProxy.RegistrationGet(personalDetailsVM.RegID);
                    personalDetailsVM.RegAttributes = regProxy.RegAttributesGetByRegID(personalDetailsVM.RegID);
                    /*using (var proxyOrg = new OrganizationServiceProxy())
                    {
                        if (!ReferenceEquals(personalDetailsVM.Registration, null) && !ReferenceEquals(personalDetailsVM.Registration.CenterOrgID, null) && personalDetailsVM.Registration.CenterOrgID != 0)
                        {
                            personalDetailsVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { personalDetailsVM.Registration.CenterOrgID }).FirstOrDefault();
                        }
                    }*/
                    var salesPerson = reg.SalesPerson;
                    var orgID = reg.CenterOrgID;
                    var outletCode = string.Empty;
                    var salesCode = string.Empty;

                    WebHelper.Instance.generateOutletAndSalesCode(orgID, salesPerson, personalDetailsVM.RegAttributes, out outletCode, out salesCode);
                    personalDetailsVM.OutletCode = outletCode;
                    personalDetailsVM.SalesCode = salesCode;
                    //<End> - 20150914 - Samuel - display sales code in summary page

                    //CR 29JAN2016 - display bill cycle
                    if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault() != null)
                    {
                        personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE).FirstOrDefault().ATT_Value;
                    }
                    else
                    {
                        personalDetailsVM.BillingCycle = "N/A";
                    }
                }
                //for tfs Bug id:2637

            }
            else
            {
                //CR 29JAN2016 - display bill cycle
                var selectedMsisdn = Session["MSISDN"].ToString2();
                retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                var selectedAccount = AcctListByICResponse.itemList.Where(c => c.ExternalId == selectedMsisdn).FirstOrDefault();
                var billCycle = (selectedAccount != null && selectedAccount.AccountDetails != null ? selectedAccount.AccountDetails.BillCycle : "N/A");
                personalDetailsVM.BillingCycle = (!string.IsNullOrWhiteSpace(billCycle) ? billCycle : "N/A");
            }
            bool isSubLine = false;
            if (Session["AccountType"].ToString2() == "S")
                isSubLine = true;
            var orderSummaryVM = ConstructOrderSummary(isSubLine);
            orderSummaryVM.IMEINumber = reg.IMEINumber;
            orderSummaryVM.SIMSerial = reg.SIMSerial;
            #region Commented by Durga Prasad Kota
            //if (reg.KenanAccountNo != null)
            //{ Session["KenanAccountNo"] = reg.KenanAccountNo; }
            //else
            //{ Session["KenanAccountNo"] = "0"; }
            #endregion
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderSummaryVM;

            #region MOC_Status and Liberization status
            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session["ExternalID"].ToString2()) ? string.Empty : Session["ExternalID"].ToString2(), regID.ToInt());
            if (Result != null && Result.Count() > 1)
            {
                personalDetailsVM.Liberlization_Status = Result[0];
                personalDetailsVM.MOCStatus = Result[1];
            }
            //28052015 - Anthony - to retrieve the liberalization based on the customer level - Start
            if (regID.ToInt() == 0)
            {
                var allCustomerDetails = SessionManager.Get("AllServiceDetails") as retrieveAcctListByICResponse;
                if (!ReferenceEquals(allCustomerDetails, null))
                {
                    personalDetailsVM.Liberlization_Status = Util.getMarketCodeLiberalisationMOC(allCustomerDetails, "LIBERALISATION");
                }
            }
            //28052015 - Anthony - to retrieve the liberalization based on the customer level - End
            #endregion

            if (personalDetailsVM.RegID == 0)
            {
                //Drop 5 B18 - Ricky - 2015.05.12 - Populate the default value for Outlet/Sales Code
                WebHelper.Instance.PopulateDealerInfo(personalDetailsVM);
            }

            #region Commented code for PDPA
            /*Changes by chetan related to PDPA implementation*/
            //if (reg.ID > 0)
            //{
            //    string documentversion = Result[3].ToString2();
            //    personalDetailsVM.PDPAVersionStatus = documentversion;
            //    if (!ReferenceEquals(documentversion, null))
            //    {
            //        using (var registrationProxy = new RegistrationServiceProxy())
            //        {
            //            var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", documentversion);
            //            personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
            //        }
            //    }
            //}
            //else
            //{
            //    using (var registrationProxy = new RegistrationServiceProxy())
            //    {
            //        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
            //        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
            //    }
            //}
            #endregion Commented code for PDPA
          
            //personalDetailsVM =
            return View(personalDetailsVM);
        }
        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,SMRT_USER,SMRT_MANAGER")]
        [DoNoTCache]
        public ActionResult MobileRegSummary(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            var resp = new RegistrationSvc.RegistrationCreateResp();
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = personalDetailsVM.TabNumber;
            try
            {
                #region PaymentRecived
                if (collection["submit1"].ToString() == "cancelReg")
                {
                    ClearRegistrationSession();
                    return RedirectToAction("Index", "../Home");
                }
                if (collection["submit1"].ToString() == "PaymentRecived")
                {
                    using (var proxy = new SmartRegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                        });
                    }
                    /*1.If device+plan genarate IMPOS file place in Shared path then redirect to new page with file name
                     */
                    if (Session[SessionKey.RegMobileReg_Type.ToString()] != null)
                    {
                        if (Convert.ToInt32(Session[SessionKey.RegMobileReg_Type.ToString()].ToString()) == (int)MobileRegType.DevicePlan)
                            using (var proxys = new SmartCatelogServiceProxy())
                            {
                                try
                                {
                                    IMPOSDetailsResp res = proxys.GetIMOPSDetails(personalDetailsVM.RegID);
                                    if (res != null)
                                    {
                                        //string fileName = Util.SessionAccess.User.Org.OrganisationId + "_" + res.ModelID + "_" + res.RegID + "_" + DateTime.Now.Date.ToString("yyyyMMdd");
                                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                                        //saveFile(fileName, ConfigurationManager.AppSettings["POSNetworkPath"].ToString(), personalDetailsVM.RegID, res);
                                        return RedirectToAction("PaymntReceived");

                                    }
                                }
                                catch (Exception)
                                {
                                    Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                                    return RedirectToAction("PaymntReceived");
                                }


                            }
                        else
                        {
                            //For Plan Only
                            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                            return RedirectToAction("PlanPaymntReceived");
                        }
                    }
                    else
                    {
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegCanceled", new { regID = personalDetailsVM.RegID });
                    }
                }
                #endregion

                if (collection["submit1"].ToString() == "close")
                {
                    #region close
                    if (personalDetailsVM.RegID == 0)
                    {
                        using (var proxy = new SmartRegistrationServiceProxy())
                        {

                            resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM,personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo),
                            ConstructCustomer(), ConstructRegMdlGroupModel(), ConstructRegAddress(),
                            ConstructRegPgmBdlPkgComponent(), ConstructRegStatus(), ConstructRegSuppLines(),
                            ConstructRegSuppLineVASes());
                            personalDetailsVM.RegID = resp.ID;
                        }
                        using (var proxy = new SmartRegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegBreFail)
                            });
                        }
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegBreFail", new { regID = personalDetailsVM.RegID });
                    }
                    else
                    {
                        using (var proxy = new SmartRegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                            });
                        }
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
                    }

                    #endregion
                }
                if (collection["submit1"].ToString() == "createpackage")
                {
                    #region createpackage
                    bool isCreated = false;
                    using (var proxy_createacc = new SmartKenanServiceProxy())
                    {
                        CenterOrderContractRequest Req = new CenterOrderContractRequest();
                        Req.orderId = personalDetailsVM.RegID.ToString();
                        isCreated = proxy_createacc.CenterOrderContractCreation(Req);

                    }
                    if (isCreated)
                    {
                        using (var proxy = new SmartRegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_SvcPendingActivate)
                            });
                        }

                    }
                    Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                    return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                    #endregion
                }
                if (collection["submit1"].ToString() == "cancel")
                {
                    #region cancel
                    using (var proxy = new SmartRegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)
                        });
                    }
                    Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                    return RedirectToAction("MobileRegCanceled", new { regID = personalDetailsVM.RegID });
                    #endregion
                }

                if (collection["submit1"].ToString() == "createAcc")
                {
                    #region createAcc
                    try
                    {
                        CreateKenanAccount(personalDetailsVM.RegID);
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });

                    }
                    catch (Exception ex)
                    {
                        LogException(ex);
                        using (var proxy = new SmartRegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                    }
                    #endregion
                }

                if (collection["submit1"].ToString() == "activateSvc")
                {
                    #region activateSvc
                    try
                    {
                        FulfillKenanAccount(personalDetailsVM.RegID);
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                    }
                    catch (Exception ex)
                    {
                        LogException(ex);
                        using (var proxy = new SmartRegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                    }
                    #endregion
                }

                //if (collection["submit1"].ToString() == "createAccWithExc")
                //{
                //    //CreateKenanAccount(personalDetailsVM.RegID);

                //    //return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
                //}

                if (personalDetailsVM.TabNumber == (int)SmartRegistrationSteps.Submit)
                {
                    #region submit
                    //if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == false)
                    //{
                    //    // Update Dealer Mobile No
                    //    var kenanResp = new MobileNoUpdateResponse();

                    //    using (var proxy = new SmartKenanServiceProxy())
                    //    {
                    //        kenanResp = proxy.MobileNoUpdate(ConstructMobileNoUpdate());
                    //    }

                    //    if (!kenanResp.Success)
                    //    {
                    //        personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

                    //        ModelState.AddModelError(string.Empty, string.Format("Failed to update Dealer Mobile Number ({0}).",
                    //            ((List<string>) Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2()));

                    //        return View("MobileRegSummary", personalDetailsVM);
                    //    }
                    //}
                    // submit registration

                    bool isValid = false;
                    string ErrorMessage = string.Empty;

                    using (var proxy = new SmartRegistrationServiceProxy())
                    {
                        //TODO: need to save ISSIMRequired to d/b
                        //collection["ISSIMRequired"]
                        //Included parameter to save SIMModelID to DB

                        //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - Start
                        WebHelper.Instance.ValidateDealerInfo(personalDetailsVM, out isValid, out ErrorMessage);
                        if (!isValid)
                        {
                            ModelState.AddModelError(string.Empty, ErrorMessage);
                            return View(personalDetailsVM);
                        }
                        //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - End

                        List<DAL.Models.WaiverComponents> objWaiverComp = new List<WaiverComponents>();
                        //Pradeep - WaiveOffs for individual contracts will be checked in KenanService. Its not required here anymore
                        //if (Session[SessionKey.ContractDetails.ToString()] != null)
                        //{
                        //    List<Online.Registration.Web.SmartViewModels.ContractDetails> ContractsList = (List<Online.Registration.Web.SmartViewModels.ContractDetails>)(Session[SessionKey.ContractDetails.ToString()]);
                        //    foreach (var contract in ContractsList)
                        //    {
                        //        DAL.Models.WaiverComponents curObj = new WaiverComponents();
                        //        curObj.ComponentID = contract.ContractKenanCode.ToInt();
                        //        curObj.ComponentName = contract.ComponentDesc.ToString2();
                        //        curObj.IsWaived = contract.Waived.ToBool();
                        //        curObj.price = contract.PenaltyWaiveOff.ToDecimal();
                        //        curObj.MismType = "c";
                        //        objWaiverComp.Add(curObj);
                        //    }

                        //}

                        resp = proxy.RegistrationCreate_Smart(ConstructRegistration(personalDetailsVM,personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, "", "", "", (collection["ISSIMRequired"] != null ? collection["ISSIMRequired"].ToString() : "false"), (!string.IsNullOrEmpty(collection["SimCardTypeSelected"]) ? collection["SimCardTypeSelected"].ToString() : "false")),
                                                        ConstructCustomer(), ConstructRegMdlGroupModel(), ConstructRegAddress(),
                                                        ConstructRegPgmBdlPkgComponent(), ConstructRegStatus(), ConstructRegSuppLines(),
                                                        ConstructRegSuppLineVASes(), ConstructPackage(), ConstructCMSSID(), ConstructSmartComponents(), objWaiverComp);


                        //resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo),
                        //                                ConstructCustomer(), ConstructRegMdlGroupModel(), ConstructRegAddress(),
                        //                                ConstructRegPgmBdlPkgComponent(), ConstructRegStatus(), ConstructRegSuppLines(),
                        //                                ConstructRegSuppLineVASes());//Added for Smart Intergration temp

						// for inserting photo to DB, must 1 by 1.

						if (resp.ID > 0)
						{
							using (var updateProxy = new UpdateServiceProxy())
							{
								personalDetailsVM.Photo = Session["otherImage"].ToString2();
								personalDetailsVM.CustomerPhoto = Session["frontImage"].ToString2();
								personalDetailsVM.AltCustomerPhoto = Session["backImage"].ToString2();

								if (!string.IsNullOrEmpty(personalDetailsVM.Photo))
								{
									updateProxy.SavePhotoToDB(personalDetailsVM.Photo, "", "", resp.ID);
								}
								if (!string.IsNullOrEmpty(personalDetailsVM.CustomerPhoto))
								{
									updateProxy.SavePhotoToDB("", personalDetailsVM.CustomerPhoto, "", resp.ID);
								}
								if (!string.IsNullOrEmpty(personalDetailsVM.AltCustomerPhoto))
								{
									updateProxy.SavePhotoToDB("", "", personalDetailsVM.AltCustomerPhoto, resp.ID);
								}
							}

                            //29042015 - Anthony - [Paperless CR] Save document to tblreguploaddoc for iContract - Start
                            bool iContractActive = ConfigurationManager.AppSettings["iContractActive"].ToBool();
                            if (iContractActive)
                            {
                                WebHelper.Instance.SaveDocumentToTableForSMART(personalDetailsVM, resp.ID);
                            }
                            //29042015 - Anthony - Save document to tblreguploaddoc for iContract - End
						}

                        var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
                        if (biometricID != 0)
                        {
                            var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
                            biometric.RegID = resp.ID;

                            proxy.BiometricUpdate(biometric);
                        }
                    }

                    #region ADDED BY Rajeswari on 15th March
                    if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
                    {
                        Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
                    }


                    //****Added to insert data in to lnkRegdetails for Addendum versioning - start****//
                    int regTypeID = 0;
                    if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
                    {
                        regTypeID = Util.GetRegTypeID(REGTYPE.MNPPlanOnly);
                    }
                    else
                    {
                        if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
                            regTypeID = Util.GetRegTypeID(REGTYPE.DevicePlan);
                        else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                            regTypeID = Util.GetRegTypeID(REGTYPE.PlanOnly);
                        else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                            regTypeID = Util.GetRegTypeID(REGTYPE.DeviceOnly);
                    }




                    #endregion
                    #region VLT ADDED CODE on 21stFEB for email functionality
                    if (resp.ID > 0)
                    {
                        #region Commented code for PDPA
                        //LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
                        //Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                        //objRegDetails.CreatedDate = DateTime.Now;
                        //objRegDetails.UserName = Util.SessionAccess.UserName;
                        //objRegDetails.RegId = resp.ID;
                        ///*Changes by chetan related to PDPA implementation*/
                        //using (var registrationProxy = new RegistrationServiceProxy())
                        //{
                        //    var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        //    objRegDetails.PdpaVersion = resultdata.Version;
                        //}
                        //objRegDetailsReq.LnkDetails = objRegDetails;
                        //using (var proxy1 = new RegistrationServiceProxy())
                        //{
                        //    proxy1.SaveLnkRegistrationDetails(objRegDetailsReq);
                        //}
                        #endregion Commented code for PDPA

                        #region Insert into lnkRegDetails
                        string PlanKenanCode = MasterDataCache.Instance.PackageComponents.Where(a => a.ID == Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"])).Select(a => a.KenanCode).SingleOrDefault().ToString2();
                        string latestPrintVersion = Util.GetPrintVersionNum(regTypeID, !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty, PlanKenanCode, "S");
                        LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
                        Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                        objRegDetails.CreatedDate = DateTime.Now;
                        objRegDetails.UserName = Util.SessionAccess.UserName;
                        objRegDetails.RegId = resp.ID;
                        objRegDetails.ContractCheckCount = Session["AccountswithContractCount"].ToInt();
                        //selected sim card type is assigned
                        if (!string.IsNullOrEmpty(collection["SimCardTypeSelected"]))
                            objRegDetails.SimCardType = collection["SimCardTypeSelected"].ToString2();
                        if (!string.IsNullOrEmpty(latestPrintVersion))
                        {
                            objRegDetails.PrintVersionNo = latestPrintVersion; //Code added by sindhu on 11/09/2013 for PrintVersion Changes
                        }
                        //objRegDetailsReq.LnkDetails = objRegDetails;//15042015 - Anthony - this code should be after assigning all value to objRegDetails
                        //for tfs Bug id:2637
                        //objRegDetails.ContractCheckCount = Session["AccountswithContractCount"].ToInt();//15042015 - Anthony - duplicate assignment
                        objRegDetails.Liberlization_Status = personalDetailsVM.Liberlization_Status;
                        objRegDetails.MOC_Status = personalDetailsVM.MOCStatus;
                        //added for removal of hard stop CR
                        //objRegDetails.Justification = collection["txtJustfication"].ToString2();
                        objRegDetails.Justification = personalDetailsVM.Justification.ToString2();
                        //for tfs Bug id:2637
                        objRegDetails.TotalLineCheckCount = Session["existingTotalLine"].ToInt();
                        bool IsWriteOff = false;
                        string acc = string.Empty;
                        WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
                        objRegDetails.WriteOffDetails = acc;
                        objRegDetailsReq.LnkDetails = objRegDetails;//15042015 - Anthony - move code to here

                        using (var proxy1 = new RegistrationServiceProxy())
                        {
                            proxy1.SaveLnkRegistrationDetails(objRegDetailsReq);
                        }
                        #endregion

                        using (var regProxy = new RegistrationServiceProxy())
                        using (var smartProxy = new SmartRegistrationServiceProxy())
                        {
                            /* BEGIN Added by VLT 5 Apr 2013 to add kenaninfo to database*/
                            if (Session["KenanCustomerInfo"] != null)
                            {
                                string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString().Split('╚');
                                KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
                                kenanInfo.FullName = strArrKenanInfo[0];
                                kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
                                kenanInfo.IDCardNo = strArrKenanInfo[2];
                                kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
                                kenanInfo.Address1 = strArrKenanInfo[4];
                                kenanInfo.Address2 = strArrKenanInfo[5];
                                kenanInfo.Address3 = strArrKenanInfo[6];
                                kenanInfo.Postcode = strArrKenanInfo[8];
                                kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
                                kenanInfo.RegID = resp.ID;

                                var kenanResp = new Online.Registration.Web.RegistrationSvc.KenanCustomerInfoCreateResp();
                                //kenanResp = smartProxy.KenanCustomerInfoCreate(kenanInfo);
                            }
                            Session["KenanCustomerInfo"] = null;
                            /* END Added by VLT 5 Apr 2013 to add kenaninfo to database*/

                            #region Insert into trnRegAttributes
                            var regAttribList = new List<RegAttributes>();
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletCode))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCODE, ATT_Value = personalDetailsVM.OutletCode });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletChannelId))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCHANNELID, ATT_Value = personalDetailsVM.OutletChannelId });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesCode))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCODE, ATT_Value = personalDetailsVM.SalesCode });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesChannelId))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCHANNELID, ATT_Value = personalDetailsVM.SalesChannelId });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffCode))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCODE, ATT_Value = personalDetailsVM.SalesStaffCode });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffChannelId))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCHANNELID, ATT_Value = personalDetailsVM.SalesStaffChannelId });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.BillingCycle))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_ACC_BILL_CYCLE, ATT_Value = personalDetailsVM.BillingCycle });

                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SingleSignOnValue))
                            {
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_SINGLESIGNON_STATUS, ATT_Value = personalDetailsVM.SingleSignOnValue });

                                if (personalDetailsVM.SingleSignOnValue.ToUpper() == "YES")
                                {
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_SINGLESIGNON_EMAILADDRESS, ATT_Value = personalDetailsVM.SingleSignOnEmailAddress });
                                }
                            }
                            
                            regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_PAYMENT_METHOD, ATT_Value = Util.getPaymentMethodName() });

                            //Samuel - KFC bucket phase 2
                            var reg = regProxy.RegistrationGet(resp.ID);
                            var pMSISDN = WebHelper.Instance.getPrincipalNoFromSuppNo(reg.MSISDN1);
                            if (!string.IsNullOrEmpty(pMSISDN))
                            {
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.PRIN_MSISDN, ATT_Value = pMSISDN });
                            }
                            regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_USER_DEVICE_AGENT, ATT_Value = Request.UserAgent });
                            regProxy.SaveListRegAttributes(regAttribList);
                            #endregion

							#region Insert into trnRegJustification
							WebHelper.Instance.saveTrnRegJustification(resp.ID);
                            #endregion

                            #region Insert into trnRegBreFailTreatment
                            WebHelper.Instance.saveTrnRegBreFailTreatment(resp.ID);
                            #endregion

							#region Insert into trnSurveyResponse
							WebHelper.Instance.SaveSurveyResult(personalDetailsVM, RegSurveyResponse.MAXIS_APP, resp.ID);
							#endregion
						}

                        ClearRegistrationSession();
                        return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });

                    }
                    #endregion VLT ADDED CODE on 21stFEB for email functionality
                    #endregion
                }
                else if (personalDetailsVM.TabNumber == (int)SmartRegistrationSteps.PersonalDetails)
                {
                    return RedirectToAction("PersonalDetails");
                }

            }
            catch (Exception ex)
            {
                Logger.Info("Throw Exception for Order ID:" + resp.ID + "Exception:" + ex);
                throw;
            }

            if (resp != null && resp.ID != null)
                return RedirectToAction("MobileRegFail", new { regID = resp.ID });

            else
                return RedirectToAction("MobileRegFail", new { regID = 0 });
        }
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER,SMRT_MANAGER")]
        [DoNoTCache]
        public ActionResult MobileRegSuccess(int regID)
        {
            var reg = new DAL.Models.Registration();
            PersonalDetailsVM personalDetails = new PersonalDetailsVM();
            var pegaOfferList = (Online.Registration.Web.ViewModels.PegaOfferVM)Session["PegaOfferList"];
            using (var regProxy = new RegistrationServiceProxy())
            {
                reg = regProxy.RegistrationGet(regID);
            }
            personalDetails.RegID = regID;
            personalDetails.SignatureSVG = reg.SignatureSVG;

            using (var proxy = new RegistrationServiceProxy())
            {
                personalDetails.PegaVM = new PegaRecommendationsVM();
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

                personalDetails.PegaVM.PegaRecommendationsSearchResult = proxy.GetPegaRecommendationbyMSISDN(personalDetails.PegaVM.PegaRecommendationsSearchCriteria);
                personalDetails.PegaVM.PegaRecommendationsSearchResult = personalDetails.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();
            }

            return View(personalDetails);
        }
        //[Authorize(Roles = "MREG_W,MREG_C,MREG_U,SMRT_USER,SMRT_MANAGER")]
        //public ActionResult PrintMobileRF(int id)
        //{
        //    var regFormVM = new RegistrationFormVM();
        //    var modelImageIDs = new List<int>();
        //    var pbpcIDs = new List<int>();
        //    var suppLineVASIDs = new List<int>();
        //    var suppLineForms = new List<SuppLineForm>();
        //    var regMdlGrpModels = new List<RegMdlGrpModel>();

        //    using (var proxy = new SmartRegistrationServiceProxy())
        //    {
        //        // Registration
        //        regFormVM.Registration = proxy.RegistrationGet(id);

        //        // Customer
        //        regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

        //        // Address
        //        var regAddrIDs = proxy.RegAddressFind(new AddressFind()
        //        {
        //            Active = true,
        //            Address = new Address()
        //            {
        //                RegID = id
        //            }
        //        }).ToList();
        //        regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

        //        // RegMdlGrpModels
        //        var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
        //        {
        //            RegMdlGrpModel = new RegMdlGrpModel()
        //            {
        //                RegID = id
        //            }
        //        }).ToList();
        //        if (regMdlGrpModelIDs.Count() > 0)
        //        {
        //            regMdlGrpModels = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
        //            modelImageIDs = regMdlGrpModels.Select(a => a.ModelImageID).ToList();
        //        }

        //        // RegPgmBdlPkgComponent
        //        var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
        //        {
        //            RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
        //            {
        //                RegID = id
        //            }
        //        }).ToList();

        //        var pbpc = new List<RegPgmBdlPkgComp>();
        //        if (regPgmBdlPkgCompIDs.Count() > 0)
        //            pbpc = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

        //        //main line
        //        if (pbpc.Count() > 0)
        //            pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();

        //        // Reg Supp Line
        //        var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = id } }).ToList();
        //        if (suppLineIDs.Count() > 0)
        //        {
        //            var regSuppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
        //            foreach (var regSuppLine in regSuppLines)
        //            {
        //                suppLineForms.Add(new SuppLineForm()
        //                {
        //                    // supp line pbpc ID
        //                    SuppLinePBPCID = regSuppLine.PgmBdlPckComponentID,
        //                    // supp line vas ID
        //                    SuppLineVASIDs = pbpc.Where(a => a.RegSuppLineID == regSuppLine.ID).Select(a => a.PgmBdlPckComponentID).ToList(),
        //                    MSISDN = regSuppLine.MSISDN1
        //                });
        //            }
        //        }

        //    }

        //    using (var proxy = new SmartCatelogServiceProxy())
        //    {

        //        // ModelGroupModel
        //        if (modelImageIDs.Count() > 0)
        //        {
        //            //regFormVM.ModelGroupModels = proxy.ModelGroupModelGet(mdlGrpModelIDs).ToList();
        //            var modelImages = proxy.ModelImageGet(modelImageIDs).ToList();
        //            var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());

        //            foreach (var regMdlGrpModel in regMdlGrpModels)
        //            {
        //                var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
        //                regFormVM.DeviceForms.Add(new DeviceForm()
        //                {
        //                    Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
        //                    Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
        //                    Price = regMdlGrpModel.Price.ToDecimal()
        //                });
        //            }
        //        }

        //        // BundlePackage, PackageComponents
        //        if (pbpcIDs.Count() > 0)
        //        {
        //            var pgmBdlPkgComps = proxy.PgmBdlPckComponentGet(pbpcIDs).Distinct().ToList();
        //            var bpCode = Properties.Settings.Default.Bundle_Package;
        //            var pcCode = Properties.Settings.Default.Package_Component;
        //            regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode).ToList();
        //            regFormVM.PackageComponents = pgmBdlPkgComps.Where(a => a.LinkType == pcCode).ToList();
        //        }

        //        foreach (var suppLineForm in suppLineForms)
        //        {
        //            //supp line package name
        //            suppLineForm.SuppLineBdlPkgName = proxy.PackageGet(proxy.PgmBdlPckComponentGet(new int[] { suppLineForm.SuppLinePBPCID })
        //                                                        .Select(a => a.ChildID)).SingleOrDefault().Name;
        //            //supp line vas names
        //            suppLineForm.SuppLineVASNames = proxy.ComponentGet(proxy.PgmBdlPckComponentGet(suppLineForm.SuppLineVASIDs).Select(a => a.ChildID).ToList()).Select(a => a.Name).ToList();
        //        }
        //        regFormVM.SuppLineForms = suppLineForms;
        //    }

        //    return View(regFormVM);
        //}

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MobileRegSuccess(FormCollection collection, PersonalDetailsVM personalDetails)
        {
            DropFourObj dropObj = new DropFourObj();
            dropObj.personalInformation = new ViewModels.PersonalDetailsVM();
            var pegaOfferList = (Online.Registration.Web.ViewModels.PegaOfferVM)Session["PegaOfferList"];
			dropObj.personalInformation.rfDocumentsFile = collection["frontPhoto"].ToString2() == "" ? Session["frontImage"].ToString2() : collection["frontPhoto"].ToString2();
			dropObj.personalInformation.rfDocumentsFileName = collection["frontPhotoFileName"];
            WebHelper.Instance.SaveDocumentToTable(dropObj, personalDetails.RegID);
            using (var proxy = new WebPOSCallBackSvc.WebPOSCallBackServiceClient())
            {
                proxy.sendDocsToIcontract(personalDetails.RegID);
            }
            ViewBag.documentUploaded = true;

            using (var proxy = new RegistrationServiceProxy())
            {
                personalDetails.PegaVM = new PegaRecommendationsVM();
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
                personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

                personalDetails.PegaVM.PegaRecommendationsSearchResult = proxy.GetPegaRecommendationbyMSISDN(personalDetails.PegaVM.PegaRecommendationsSearchCriteria);
                personalDetails.PegaVM.PegaRecommendationsSearchResult = personalDetails.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();
            }

            return View(personalDetails);
        }
        
        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,SMRT_USER,SMRT_MANAGER")]
        public ActionResult PrintMobileRF(int id)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            ViewBag.EarlyTerminationFeeLabel = Settings.Default.EarlyTerminationFee;
            return View(WebHelper.Instance.GetContractDetailsViewModel_SMART(id));
        }
        [HttpPost]
        public ActionResult UploadImage()
        {
            ///Get the upload and resize locations from settings
            ///PLEASE CREATE 'Uploads' AND 'Resized' FOLDERS IN THE PROJECT
            ///FOR EXAMPLE Online.Registration.Web/Uploads AND Online.Registration.Web/Resized
            //string imageUploadLocation = Server.MapPath(Properties.Settings.Default.imageUploadLocation);
            //string imageResizeLocation = Server.MapPath(Properties.Settings.Default.imageResizeLocation);

           ///Get the upload and resize locations from settings
			///PLEASE CREATE 'Uploads' AND 'Resized' FOLDERS IN THE PROJECT
			///FOR EXAMPLE Online.Registration.Web/Uploads AND Online.Registration.Web/Resized
			string imageUploadLocation = Server.MapPath(Properties.Settings.Default.imageUploadLocation);
			string imageResizeLocation = Server.MapPath(Properties.Settings.Default.imageResizeLocation);
			string fitImage = Properties.Settings.Default.fitImage;

			bool isResized = false;

			string SourceImageName, ActualImageName, onlyFileExtension, imageUrl, imageType = string.Empty;

			System.Drawing.Image srcimage = null;
			imageUrl = string.Empty;

			///CHECKS WHETHER THE Request.Files
			if (!ReferenceEquals(Request.Files, null) && Request.Files.Count > 0)
			{
				///RETRIEVES THE FIRST KEY IN CASE THERE WOULD BE MULTIPLE FOUND.
				string PostedFileName = Request.Files.AllKeys.FirstOrDefault();
				///TYPECASTING TO HttpPostedFileBase
				HttpPostedFileBase hpf = Request.Files[PostedFileName] as HttpPostedFileBase;

				///ONLY PROCESS IF LENGTH >0
				if (hpf.ContentLength > 0)
				{
					//Keep the actual imagename for future reference
					ActualImageName = SourceImageName = Path.GetFileName(hpf.FileName);
					imageType = hpf.ContentType;
					///Check whether there is already an image in the folder with the same name
					Util.checkFileExistance(imageUploadLocation, ref SourceImageName, out onlyFileExtension);
					imageUploadLocation = Path.Combine(imageUploadLocation + @"\", SourceImageName);
					///Save the image
					hpf.SaveAs(imageUploadLocation);

					srcimage = System.Drawing.Image.FromFile(imageUploadLocation);
					///CALL TO CHECK AND LOWER THE RESOLUTION OF HIGH RESOULTION IMAGE
					//isResized = Util.ResizeImage(srcimage, 73, imageResizeLocation + SourceImageName, 540, 365, fitImage);
					isResized = Util.AlwaysResizeImage(srcimage, 23, imageResizeLocation + SourceImageName, 540, 365, fitImage);

					///Return the processed or unprocessed image as url
					if (isResized)
					{
						imageResizeLocation = Path.Combine(imageResizeLocation + @"\", SourceImageName);
						imageUrl = string.Format(@"data:{0};base64,", imageType) + Convert.ToBase64String(System.IO.File.ReadAllBytes(imageResizeLocation));
					}
					else
					{
						imageUrl = string.Format(@"data:{0};base64,", imageType) + Convert.ToBase64String(System.IO.File.ReadAllBytes(imageUploadLocation));
					}
				}
			}
            ///RETURNS THE IMAGE IN BASE 64 ENCODED FORMAT
            var jsonData = new { imageUrl = imageUrl };

            return Json(jsonData);
        }

        [HttpPost]
        public ActionResult RefreshPegaRecord(string msisdn, string idCardNo, int type = 1)
        {
            var PegaVM = new PegaRecommendationsVM();

            using (var proxy = new RegistrationServiceProxy())
            {
                PegaRecommendationSearchCriteria criteria = new PegaRecommendationSearchCriteria();
                criteria.MSISDN = msisdn;
                criteria.IDCardNo = idCardNo;
                criteria.Response = Constants.ACCEPT;
                criteria.CapturedStartDt = DateTime.Now.Date;
                criteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

                PegaVM.PegaRecommendationsSearchResult = proxy.GetPegaRecommendationbyMSISDN(criteria);

                if (PegaVM.PegaRecommendationsSearchResult.Count() > 0)
                {
                    PegaVM.PegaRecommendationsSearchResult = PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();
                }
            }
            
            return View("_PegaRecommendationMiniDashboard_Retention", PegaVM);
        }

        [HttpPost]
        public JsonResult isvalidSupervisor(string Username, string Password)
        {
            int UserID = Util.SessionAccess == null ? 0 : Util.SessionAccess.UserID;
            bool isValidUser = false;
            int orgid = Util.SessionAccess.User.Org.ID;

            string status = string.Empty;
            if (Session["ID"] != null)
            {
                ID = Session["ID"].ToInt();
            }

            var access = UserServiceProxy.GetAccess(Username.ToString());
            var user = access != null ? UserServiceProxy.GetUser(access.UserID) : null;//08062015 - Anthony - fix for null reference exception
            if (access == null)
            {
                //ModelState.AddModelError("", "The user name does not exist.");
                status = "The user name does not exist.";
                isValidUser = false;
                //return isValidUser;

            }
            else if (access.IsLockedOut)
            {
                //ModelState.AddModelError("", "This account has been Locked out.");
                status = "This account has been Locked out.";
                isValidUser = false;
                //return isValidUser;
            }
            else if (user != null && user.OrgID != orgid)
            {
                status = "This account work for different Organization.";
                isValidUser = false;
            }

            else
            {
                //string sha1Pswd = Util.GetMD5Hash(Password.ToString());
                //var proxy1 = new UserServiceProxy();
                //access = proxy1.GetAccessByUsername(Username, sha1Pswd);
                if (!access.LDAPLogin)
                {
                    string sha1Pswd = Util.GetMD5Hash(Password.ToString());
                    var proxy1 = new UserServiceProxy();
                    access = proxy1.GetAccessByUsername(Username, sha1Pswd);
                }
                else
                {
                    using (var ldapProxy = new LDAPServiceProxy())
                    {
                        var resp = ldapProxy.ValidateCredential(new LDAPSvc.LDAPAccessRequest()
                        {
                            Username = Username,
                            Password = Password
                        },string.Empty);

                        if (!resp.IsCredentialValidated)
                        {
                            access = null;
                        }
                    }

                }
                if (access == null)
                {
                    //ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    status = "The user name or password provided is incorrect.";
                    isValidUser = false;
                }
                else
                {
                    if (Roles.IsUserInRole(Username, "SMRT_MANAGER"))
                    {
                        isValidUser = true;
                    }
                    else
                    {
                        status = "Not a supervisor user.";
                        isValidUser = false;
                    }
                }
            }
            var jsonData = new { status = status, isValidUser = isValidUser };
            return Json(jsonData);
        }
        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,SMRT_USER,SMRT_MANAGER,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegCanceled(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,SMRT_USER,SMRT_MANAGER,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegAccCreated(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,SMRT_USER,SMRT_MANAGER,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegSvcActivated(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,SMRT_USER,SMRT_MANAGER,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegClosed(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,SMRT_USER,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegBreFail(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,SMRT_USER,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegFail(int regID)
        {
            return View(regID);
        }
        #region private methods
        protected void SendEmail(int id)
        {
            #region VLT ADDED CODE

            //VLT ADDED CODE by Rajeswari on 2st Feb for sending Email to customer

            var pdVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            RequestContext reqCtx = new RequestContext();
            Util.CheckSessionAccess(reqCtx);
            string emailAddr = Util.SessionAccess.User.EmailAddr;

            MailMessage mail = new MailMessage();
            mail.IsBodyHtml = true;
            SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"].ToString());
            mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToString());
            //mail.To.Add(ConfigurationManager.AppSettings["ToEmail"].ToString());
            mail.To.Add(pdVM.Customer.EmailAddr);
            mail.Subject = ConfigurationManager.AppSettings["Subject"].ToString();

            string gender = string.Empty;
            if ((pdVM.Customer.Gender != "") || (pdVM.Customer.Gender != null))
            {
                if (pdVM.Customer.Gender == "M")
                    gender = "Male";
                else
                    gender = "Female";
            }
            string paymentMode = string.Empty;

            if (pdVM.Customer.PayModeID.ToString() != null)
                paymentMode = Util.GetNameByID(RefType.PaymentMode, pdVM.Customer.PayModeID);
            else
                paymentMode = null;

            string customerTitle = string.Empty;
            if (pdVM.Customer.CustomerTitle != null)
                customerTitle = Util.GetNameByID(RefType.CustomerTitle, pdVM.Customer.CustomerTitleID) + ". ";

            string idCardTypeId = string.Empty;

            if (!ReferenceEquals(pdVM.Customer.IDCardTypeID, null))
                idCardTypeId = Util.GetNameByID(RefType.IDCardType, pdVM.Customer.IDCardTypeID);

            string dob = string.Empty;
            if (pdVM.Customer.DateOfBirth != null)
                dob = Util.FormatDate(pdVM.Customer.DateOfBirth);

            string language = string.Empty;
            if (!ReferenceEquals(pdVM.Customer.LanguageID, null))
            {
                language = Util.GetNameByID(RefType.Language, pdVM.Customer.LanguageID);
            }

            string nationality = string.Empty;
            if (!ReferenceEquals(pdVM.Customer.NationalityID, null))
                nationality = Util.GetNameByID(RefType.Nationality, pdVM.Customer.NationalityID);

            string raceId = string.Empty;
            if (!ReferenceEquals(pdVM.Customer.RaceID, null))
                raceId = Util.GetNameByID(RefType.Race, pdVM.Customer.RaceID);

            string stateId = string.Empty;
            if (!ReferenceEquals(pdVM.Address.StateID, null))
                stateId = Util.GetNameByID(RefType.State, pdVM.Address.StateID);

            string cardTypeId = string.Empty;
            if (pdVM.Customer.CardTypeID != null)
                cardTypeId = Util.GetNameByID(RefType.CardType, pdVM.Customer.CardTypeID.Value);

            string bundlePkgId = string.Empty;




            StringBuilder sb = new StringBuilder();
            StringBuilder sbVas = new StringBuilder();
            var regFormVM = new RegistrationFormVM();
            var modelImageIDs = new List<int>();
            var pbpcIDs = new List<int>();
            var suppLineVASIDs = new List<int>();
            var suppLineForms = new List<SuppLineForm>();
            var regMdlGrpModels = new List<RegMdlGrpModel>();
            string additionalCharges = string.Empty;
            StringBuilder sbPlan = new StringBuilder();
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                regFormVM.Registration = proxy.RegistrationGet(id);

                // Customer
                regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

                // Address
                var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                {
                    Active = true,
                    Address = new Address()
                    {
                        RegID = id
                    }
                }).ToList();
                regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

                // RegMdlGrpModels
                var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                {
                    RegMdlGrpModel = new RegMdlGrpModel()
                    {
                        RegID = id
                    }
                }).ToList();
                if (regMdlGrpModelIDs.Count() > 0)
                {
                    regMdlGrpModels = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
                    modelImageIDs = regMdlGrpModels.Select(a => a.ModelImageID).ToList();
                }

                // RegPgmBdlPkgComponent
                var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                {
                    RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                    {
                        RegID = id
                    }
                }).ToList();

                var pbpc = new List<RegPgmBdlPkgComp>();

                if (regPgmBdlPkgCompIDs.Count() > 0)
                    pbpc = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                if (pbpc.Count() > 0)
                    pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();


                // Reg Supp Line                
                var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = id } }).ToList();
                if (suppLineIDs.Count() > 0)
                {
                    var regSuppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    foreach (var regSuppLine in regSuppLines)
                    {
                        suppLineForms.Add(new SuppLineForm()
                        {
                            // supp line pbpc ID  
                            SuppLineBdlPkgName = regSuppLine.PgmBdlPckComponent.PlanType,
                            SuppLinePBPCID = regSuppLine.PgmBdlPckComponentID,

                            // supp line vas ID
                            SuppLineVASIDs = pbpc.Where(a => a.RegSuppLineID == regSuppLine.ID).Select(a => a.PgmBdlPckComponentID).ToList(),
                            MSISDN = regSuppLine.MSISDN1,


                        });
                        additionalCharges = regSuppLine.Reg.AdditionalCharges.ToString();
                        sb.AppendFormat("<b>MSISDN</b>");
                        sb.AppendFormat("</td>");
                        sb.AppendFormat("<td valign='top'>");
                        sb.AppendFormat(regSuppLine.MSISDN1);
                        sb.AppendFormat("</td>");

                    }
                }
            }
            using (var proxy = new SmartCatelogServiceProxy())
            {
                if (modelImageIDs.Count() > 0)
                {
                    var modelImages = proxy.ModelImageGet(modelImageIDs).ToList();
                    var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());

                    foreach (var regMdlGrpModel in regMdlGrpModels)
                    {
                        var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
                        regFormVM.DeviceForms.Add(new DeviceForm()
                        {
                            Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
                            Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
                            Price = regMdlGrpModel.Price.ToDecimal()
                        });
                    }
                }

                if (pbpcIDs.Count() > 0)
                {
                    var pgmBdlPkgComps = proxy.PgmBdlPckComponentGet(pbpcIDs).Distinct().ToList();
                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode).ToList();
                    regFormVM.PackageComponents = pgmBdlPkgComps.Where(a => a.LinkType == pcCode).ToList();
                }


                StringBuilder sbPlanName = new StringBuilder();
                for (int i = 0; i < regFormVM.BundlePackages.Count; i++)
                {
                    sbPlanName.Append(regFormVM.BundlePackages[i].ChildID + ",");
                }
                string[] planName = sbPlanName.ToString().Split(',');
                if (planName.Length != 0)
                {
                    sbPlan.AppendFormat("<tr>" +
                                      "<td>" +
                                      "</td>" +
                                      "<td style='width: 200px' align='center'>" +
                                          "<b>PRINCIPAL LINE</b>" +
                                      "</td>" +
                                      "<td style='width: 200px' align='center'>" +
                                          "<b>SUPPLEMENTARY LINE (1)</b>" +
                                      "</td>" +
                                      "<td style='width: 200px' align='center'>" +
                                          "<b>SUPPLEMENTARY LINE (2)</b>" +
                                      "</td>" +
                                  "</tr>");
                }
                for (int j = 0; j < planName.Length - 1; j++)
                {
                    sbPlan.AppendFormat("<tr>" +
                                        "<td valign='top'>" +
                                            "<b>Plan</b>" +
                                        "</td>" +
                                        "<td valign='top'>" +
                                            "<div style='float: left; border-style: solid; border-width: 1px; border-radius: 5px;padding: 3px 3px 3px 3px; width: 200px;'>" +
                                                Util.GetNameByID(RefType.Package, Convert.ToInt32(planName[j].ToString())) +
                                            "</div>" +
                                        "</td></tr>");

                }
                sbPlan.AppendFormat("<tr>" +
                                    "<td valign='top'>" +
                                        "<b>VAS</b>" +
                                    "</td></tr>");
                StringBuilder sbVasData = new StringBuilder();

                for (int i = 0; i < regFormVM.PackageComponents.Count; i++)
                {
                    sbVasData.Append(regFormVM.PackageComponents[i].ChildID + ",");

                }
                string[] vasName = sbVasData.ToString().Split(',');
                for (int j = 0; j < vasName.Length - 2; j++)
                {
                    sbPlan.AppendFormat("<tr><td></td><td valign='top'>" +
                                            "<div style='float: left; border-style: solid; border-width: 1px; border-radius: 5px;padding: 3px 3px 3px 3px; width: 200px;'>" +
                                                Util.GetNameByID(RefType.Package, Convert.ToInt32(vasName[j].ToString())) +
                                            "</div>" +
                                        "</td></tr>");
                }
                sbPlan.AppendFormat("<tr><td></td><td></td></tr>");
            }
            if (sb.Length == 0)
                sb = sb.Append("Not Available");

            if (additionalCharges == "")
                additionalCharges = "Not Available";

            string simSerial = pdVM.RegSIMSerial;
            if (simSerial == null)
                simSerial = "Not Available";

            string imeiNo = pdVM.RegIMEINumber;
            if (imeiNo == null)
                imeiNo = "Not Available";


            StringBuilder mailBody = new StringBuilder();
            mailBody.AppendFormat("Dear " + customerTitle + pdVM.Customer.FullName);
            mailBody.AppendFormat("<table align='center' width='500px' style='font-size:smaller;font-family:Times New Roman;'>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<div style='font-size: large; font-weight: bolder; float: left;'>" +
                                               " MOBILE POSTPAID REGISTRATION FORM</div>" +
                                            "<div style='float: right'>" +
                                                "<img alt='' src=cid:corpLogo height='30px' /></div>" +
                                        "</td>" +
                                    "</tr>" +
                //"<br/>" +
                                    "<tr>" +
                                    "<td style='width: 100%'>" +
                        "<table class='rfTable' width='670px'>" +
                            "<tr>" +
                                "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                    "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<tr>" +
                                            "<td >" +
                                                "<div>A. Individual</div>" +
                                            "</td>" +
                                        "</tr>" +
                                    "</table>" +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>Fullname *</b>" +
                                "</td>" +
                                "<td>" + customerTitle + pdVM.Customer.FullName +
                                "</td>" +
                                "<td>" +
                                    "<b>Gender *</b>" +
                                "</td>" +
                                "<td>" + gender +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>IDCardType</b>" +
                                "</td>" +
                                "<td>" + idCardTypeId +
                                "</td>" +
                                "<td>" +
                                    "<b>IDCardNo</b>" +
                                "</td>" +
                                "<td>" + pdVM.Customer.IDCardNo +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>DOB *</b>" +
                                "</td>" +
                                "<td>" + dob +
                                "</td>" +
                                "<td>" +
                                    "<b>Language *</b>" +
                                "</td>" +
                                "<td>" + language +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>Mobile No *</b>" +
                                "</td>" +
                                "<td>" + pdVM.Customer.ContactNo +
                                "</td>" +
                                "<td>" +
                                    "<b>Nationality *</b>" +
                                "</td>" +
                                "<td>" + nationality +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>Alt Contact No</b>" +
                                "</td>" +
                                "<td>" + pdVM.Customer.AlternateContactNo +
                                "</td>" +
                                "<td>" +
                                    "<b>Race</b>" +
                                "</td>" +
                                "<td>" + raceId +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>Email * </b>" +
                                "</td>" +
                                "<td>" + pdVM.Customer.EmailAddr +
                                "</td>" +
                                "<td>" +
                                "</td>" +
                                "<td>" +
                                "</td>" +
                            "</tr>" +
                        "</table>" +

                    "<table class='rfTable' width='670px'>" +
                        "<tr>" +
                            "<td colspan='2' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                    "<tr>" +
                                        "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                            "<div>" +
                                                "B. Address</div>" +
                                        "</td>" +
                                    "</tr>" +
                                "</table>" +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td style='width: 120px'>" +
                                "<b>Addresses *</b>" +
                            "</td>" +
                            "<td>" + pdVM.Address.Line1 + pdVM.Address.Line2 + pdVM.Address.Line3 +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td style='width: 120px'>" +
                                "<b>Town/City Name * </b>" +
                            "</td>" +
                            "<td>" + pdVM.Address.Town +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td style='width: 120px'>" +
                                "<b>Postcode *</b>" +
                            "</td>" +
                            "<td>" + pdVM.Address.Postcode +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td style='width: 120px'>" +
                                "<b>State/Province *</b>" +
                            "</td>" +
                            "<td>" + stateId +
                            "</td>" +
                        "</tr>" +
                    "</table>" +

                "<br />" +
                "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<tr>" +
                                    "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<div>" +
                                            "C. Monthly Bill Settlement Preference</div>" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                    "<tr>" +
                       " <td style='width: 120px'>" +
                            "<b>Payment Mode *</b>" +
                        "</td>" +
                        "<td colspan='3'>" + paymentMode +
                        "</td>" +
                    "</tr>" +
                "</table>" +
                "<br />" +

                "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable'  style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<tr>" +
                                    "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<div>" +
                                           "D. Service Package</div>" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                    "<tr>" +
                       "<td valign='top' colspan='4'>" +
                            "<table>" + sbPlan.ToString() +
                                    "<tr>" +
                                    "<td valign='top' style='width: 100px'></td>" +
                                    "<td valign='top' style='width: 100px'></td>" +
                                     "</tr>" +
                                    "<tr>" +
                                    "<td valign='top' style='width: 100px'><b>MSISDN</b></td>" +
                                    "<td valign='top' style='width: 100px'  colspan='3'>" + sb.ToString() + "</td>" +
                                     "</tr>" +

                                "<tr>" +
                                    "<td valign='top' style='width: 100px'>" +
                                        "<b>Additional Charges</b>" +
                                    "</td>" +
                                    "<td valign='top' colspan='3'>" + additionalCharges +
                                    "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td valign='top' style='width: 100px'>" +
                                        "<b>IMEI Number</b>" +
                                    "</td>" +
                                    "<td valign='top' colspan='3'>" + imeiNo +
                                    "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td valign='top' style='width: 100px'>" +
                                        "<b>SIM Serial</b>" +
                                    "</td>" +
                                    "<td valign='top' colspan='3'>" + simSerial +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                "</table>" +
               "<br/>" +

               //Code Added by VLT to include customer photos on 4 mar 2013//

               "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<tr>" +
                                    "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<div>" +
                                            "E. CUSTOMER D PHOTO</div>" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                    "<tr>" +
                        "<td valign='top' colspan='4'>" +
                            "<table>" +
                                "<tr>" +
                                    "<td align='left'>" +
                                        "<label id='lblFrontImage12' style='font-weight: bold; color: Black;'>Photo (front)</label><br />" +
                                        "<img alt='' src='" + regFormVM.Registration.CustomerPhoto + "' width='230px' height='200px' />" +
                                    "</td>" +
                                    "<td align='left'>" +
                                        "<label id='lblFrontImage12' style='font-weight: bold; color: Black;'>Photo (back)</label><br />" +
                                        "<img alt='' src='" + regFormVM.Registration.AltCustomerPhoto + "' width='230px' height='200px' />" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                "</table>" +
                "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<tr>" +
                                    "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<div>" +
                                            "F. CUSTOMER PHOTO</div>" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                    "<tr>" +
                        "<td valign='top' colspan='4'>" +
                            "<table>" +
                                "<tr>" +
                                    "<td align='left'>" +
                                        "<img alt='' src='" + regFormVM.Registration.Photo + "' width='230px' height='200px' />" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                "</table>" +
                 "<br />" +


                 "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<tr>" +
                                    "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<div>" +
                                            "G. DECLARATION</div>" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                    "<tr>" +
                        "<td>" +
                            "<table>" +
                                "<tr>" +
                                    "<td>" +
                                        "I/We hereby declare: (a) that I/we wish to subscribe for the Services provided by " +
                                        "MMSSB; (b) that the above information provided is true and correct." +
                                        "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td>" +
                                       regFormVM.Registration.SignatureSVG +
                                    "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td>" +
                                        "____________________________________" +
                                        "<br />" +
                                        DateTime.Now.ToString("dd/MM/yyyy") +
                                    "</td>" +
                                "</tr>" +

                            "</table>" +
                        "</td>" +
                    "</tr>" +
                "</table>" +
                "<br/>" +
                //Code Added by VLT to include customer photos on 4 mar 2013//

                //Section-G code
                 "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +

                                " <tr>" +

                                    " <td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        " <div>" +

                                        " H. Terms & Conditions</div>" +

                                    " </td>" +

                                " </tr>" +

                                " </table>" +
                                "</td>" +
                                "</tr>" +

                    " <tr><td><table>" +

                    " <tr>" +

                    " <td style='width: 49%; float: left'>" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>1.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>DEFINITIONS</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Addendum(s)&quot;</strong> means any addendum(s) or supplement(s)" +

                    " executed by Customer and accepted by MMSSB for value added, supplemental or additional" +

                    " Services." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Agreement&quot;</strong> means the agreement for Services made between" +

                    " MMSSB and a Customer in accordance with these terms and conditions, the Registration" +

                    " Form, the Addendums, the terms of services, policies and procedures of the individual" +

                    " rate plans and all other documents which are expressly agreed to form part of the" +

                    " Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Bank&quot;</strong> means the authorised banks or financial institutions" +

                    " or such other entity which may be nominated by MMSSB from time to time." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Bursa Malaysia&quot;</strong> means Bursa Malaysia Securities Berhad." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Card&quot;</strong> means the credit or charge card (as applicable)" +

                    " nominated by the Customer as payment for the Services and accepted by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Cardholder&quot;</strong> means the lawful and authorised user of" +

                    " the Card whose name is embossed thereon and whose signature appears on the Card." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Card Issuer(s)&quot;</strong> refers to any bank or legal entity which" +

                    " is the issuer for the Card." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Customer&quot;</strong> means the individual, sole proprietorship," +

                    " partnership, company or entity named in the Registration Form overleaf whose application" +

                    " for Services or any part thereof has been accepted and approved by MMSSB. An individual" +

                    " applicant must be at least 18 years of age." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Direct Debit&quot;</strong> means the direct debit bill payment service" +

                    " offered by MMSSB whereby a Customer’s periodic official bill statement may be automatically" +

                    " billed into the Customer’s Card account for settlement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Donor Network Operator&quot;</strong> means a mobile service provider" +

                    " from which a Mobile Number has been or is to be ported out." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.11" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;International Mobile Equipment Identification (IMEI)&quot;</strong>" +

                    " is a unique electronic serial number sealed into the Mobile Equipment." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.12" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;m-Billing&quot;</strong> is a service offered whereby the Customer’s" +

                    " periodic official bill statement may be viewed, and the Customer has the option" +

                    " to pay the outstanding amount using the Customer’s Card account for settlement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.13" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;MMSSB&quot;</strong> means Maxis Mobile Services Sdn Bhd (formerly" +

                    " known as Malaysian Mobile Services Sdn Bhd (Company No. 73315-V)." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.14" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;MAXIS&quot;</strong> is the service name of the Global System for" +

                    " Mobile Communications (GSM) Service." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.15" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Mobile Equipment&quot;</strong> means equipment with a transmitter" +

                    " and receiver which does not contain any Personal Information." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.16" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Mobile Number&quot; </strong>means Mobile Station International Subscriber" +

                    " Directory Number (MSISDN)." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.17" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Mobile Number Portability&quot;/&quot;MNP&quot; </strong>means the" +

                    " ability for Customers to change from one mobile service provider to another and" +

                    " retain their Mobile Number." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.18" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Mobile Phone&quot;</strong> is composed of the Mobile Equipment and" +

                    " SIM Card which facilitates the use of the Services." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.19" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Pay-By-Phone&quot;</strong> means the Pay-By-Phone service whereby" +

                    " the Customer’s periodic official bill statement per account may be charged to the" +

                    " Customer’s Card account for settlement as advised by the Customer via telephone" +

                    " for each payment." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.20" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Personal Information&quot;</strong> means information collected by" +

                    " MMSSB from a Customer including all information and details in relation to the Services" +

                    " provided by MMSSB to the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.21" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Port or Porting&quot;</strong> means the transfer of Customer’s Mobile" +

                    " Number from one mobile service provider to another." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.22" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Recipient Network Operator&quot; </strong>means a mobile service provider" +

                    " to which a Mobile Number has been or is to be ported in." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.23" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Registration Form&quot;</strong> means Customer’s duly completed application" +

                    " form for registration to subscribe to the Services, which has been accepted and" +

                    " approved by MMSSB, the form and content of which are set out overleaf." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.24" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Related Corporations&quot;</strong> means the related corporations" +

                    " as defined under the Companies Act, 1965 and include their respective employees" +

                    " and directors." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.25" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Service(s)&quot;</strong> means the mobile telecommunication services" +

                    " to be provided by MMSSB to a Customer pursuant to the Agreement and any plans, value" +

                    " added, supplemental or additional Services as may be stated in the Agreement, Registration" +

                    " Form or Addendum(s)." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.26" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;SIM Card&quot;</strong> for GSM Services means either a card or plug-in" +

                    " module with a microchip which contains all necessary Customer information. The SIM" +

                    " Card has to be inserted into the Mobile Equipment in order for a call to be made." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.27" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;SKMM&quot; </strong>means the Suruhanjaya Komunikasi dan Multimedia" +

                    " Malaysia (SKMM), also known as Malaysian Communications and Multimedia Commission," +

                    " established under the Malaysian Communications and Multimedia Commission Act 1998" +

                    " (CMA 1998)." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.28" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Working Days&quot;</strong> means, save for the states of Kedah, Terengganu" +

                    " and Kelantan, Mondays to Fridays and Saturday (half day) excluding public holidays" +

                    " and Sundays. In relation to the states of Kedah, Terengganu and Kelantan, Saturdays" +

                    " to Wednesdays and Thursday (half day) excluding public holidays and Fridays." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>2.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>DURATION OF AGREEMENT</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 2.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Agreement between the Customer and MMSSB shall be in force from the date the" +

                    " Customer’s Registration Form for Services is approved by MMSSB, which is signified" +

                    " by availability of the Services to the Customer. Approval of the Customer’s application" +

                    " for Services shall be at MMSSB’s absolute discretion and shall be in force unless" +

                    " terminated in accordance with the Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 2.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Approval by MMSSB of the Customer’s application for one Service does not necessarily" +

                    " imply approval of nor oblige MMSSB to accept the Customer’s application for registration" +

                    " for other types of Services comprising the Services applied for. Approval by MMSSB" +

                    " of Customer’s application for registration for the other services comprising the" +

                    " Services is at MMSSB’s absolute discretion." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>2A.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>VALUE PLUS INTERNET PLAN</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 2A.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The principal and supplementary plans structure for the Value Plus Internet Plans" +

                    " (“Plans”) shall be as set out and published on MMSSB’s official website at <a href='http://www.maxis.com.my/Internet'>" +

                    " www.maxis.com.my/Internet </a>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 2A.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The principal and supplementary plans structure for the Plans are subject to change," +

                    " and the prevailing terms and conditions with respect to the Plans as updated on" +

                    " the official website shall apply and supercede any and all previous versions, including" +

                    " but not limited to, in any user guide or leaflets versions. The Customer is responsible" +

                    " to regularly review information relating to the Plans and Services posted on MMSSB’s" +

                    " official website which may include changes to the Agreement, Addendum(s), Services" +

                    " and/or Plans. The Customer’s continued use of the Services and/or Plans after the" +

                    " effective date of any change to the terms and conditions of Services and/or Plans" +

                    " shall constitute unconditional acceptance of such variations, additions or amendments" +

                    " by the Customer and the Customer shall be bound by the same. In the event the Customer" +

                    " does not accept such changes, the Customer shall be entitled to terminate Plan and/or" +

                    " the use of the Services." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 2A.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " With regard to the Plans, the Customer acknowledges and accepts that:<br />" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " Any usage exceeding the packaged voice minutes, SMS or data allocations will be" +

                    " charged based on the individual tariffs for exceeded usage;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " The packaged minutes and SMS applies to domestic on-net and off-net usage only;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " All packaged minutes, SMS and data allocated within the Plans are exclusively for" +

                    " domestic usage only;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " The Customer may purchase monthly internet quota upgrade passes before the Customer’s" +

                    " internet volume allocation is exceeded, whereby such upgrade is available through" +

                    " the Customer’s UMB menu accessible through the Customer’s Mobile Phone or other" +

                    " available devices or channels as may be informed by MMSSB as and when available" +

                    " or deemed necessary. Nevertheless, in the event the internet volume allocation tied" +

                    " to the Plans is exceeded and the Customer has not made an internet quota pass purchase," +

                    " the Customer will automatically be subject to extra charges (extra charges based" +

                    " on the Customer’s internet usage volume) as stipulated on MMSSB’s website with a" +

                    " maximum charge of up to RM250 per month or such other rate as stipulated by MMSSB" +

                    " as and when deemed necessary. While roaming overseas, the prevailing data roaming" +

                    " charges will apply." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " The supplementary plan does not allow sharing of unused packaged minutes, SMS’ or" +

                    " data allocation between family lines or the principal and supplementary lines." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " f." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " All monthly allocated Minutes, SMS’ and Internet quota will be pro-rated and therefore" +

                    " based on the Customer’s bill cycle date, whereby a customer might not enjoy the" +

                    " full allocation for the first month. From the second month onward the Customer will" +

                    " enjoy full allocation for Minutes, SMS’ and Internet quota.</li>" +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>3.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>CUSTOMER'S RESPONSIBILITY</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer must, throughout the duration of the Agreement:" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Promptly pay all amounts due to MMSSB as reflected in the official bill statement" +

                    " and for all charges whatsoever occasioned by the use of the Service, including all" +

                    " charges to supplementary lines, irrespective of whether such charges were authorised" +

                    " by the Customer, had exceeded the Customer’s credit limit or had arisen from any" +

                    " other causes whatsoever." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " CCease to utilise the Services or any part thereof for such period as may be required" +

                    " by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Continue to be liable for any applicable charges and fees during the period of interruption," +

                    " suspension or loss of Services or part thereof from any cause whatsoever." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Ensure that the Customer’s Mobile Equipment, SIM Card and Mobile Number are lawfully" +

                    " owned/used/possessed and that such ownership/usage/possession is not in contravention" +

                    " of any laws or regulations of Malaysia." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Ensure that the Service is used solely for the reception and transmission of messages" +

                    " (including without limitation picture, data and audio files) and other telecommunications" +

                    " by the Customer or other persons using the Customer’s Mobile Phone." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Not use the Service to cause embarrassment, distress, annoyance, irritation or nuisance" +

                    " to any person." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Comply with all notices or directions relating to the Customer’s use of the Service," +

                    " the Mobile Equipment, SIM Card and Mobile Number as MMSSB may see fit to issue from" +

                    " time to time or if MMSSB has reason or cause to suspect that the Customer is not" +

                    " complying with the Customer’s responsibilities and obligations under this Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Be fully responsible for any voice or data transmitted or broadcasted by the Customer" +

                    " or persons using the Customer’s Mobile Phone (whether authorised by the Customer" +

                    " or not)." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " At all times keep the personal identification number (PIN) of the Customer’s SIM" +

                    " Card confidential and not release the PIN to any other person." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Report immediately to MMSSB upon the discovery of any fraud, theft, loss, unauthorised" +

                    " usage or any other occurrence of unlawful acts in relation to the Mobile Phone and" +

                    " its use. In this respect, the Customer agrees to lodge a police report whenever" +

                    " instructed by MMSSB (if one has not already been lodged) and to give MMSSB a certified" +

                    " copy of such report." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.11" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Indemnify and shall keep indemnified MMSSB from any loss, damage, liability or expenses" +

                    " arising from any claims for libel, invasion of privacy, infringement of copyright," +

                    " patent, breach of confidence or privilege or breach of any law or regulation whatsoever" +

                    " arising from the material transmitted, received or stored via the Services or part" +

                    " thereof and from all other claims arising out of any act or omission of the Customer" +

                    " or any unauthorised use or exploitation of the Services or part thereof." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>4.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>OTHER PAYMENTS</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Upon submission and approval of the Registration Form, the Customer shall pay to" +

                    " MMSSB such payments (which may include without limitation, a refundable deposit)" +

                    " as may be required by MMSSB for the registration of the Services or part thereof." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Any refundable deposit shall be held to the Customer's credit and repaid to the" +

                    " Customer without interest after termination of the Agreement, and subject to the" +

                    " deduction of any amount due to MMSSB by the Customer. MMSSB reserves the right to" +

                    " deduct from the refundable deposit any amount due and payable to MMSSB at any time" +

                    " and may request the Customer to make a further refundable deposit payment towards" +

                    " maintaining the refundable deposit at the level determined by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer has 14 days from the date of the official bill statement to notify" +

                    " MMSSB in writing of any valid disputes or errors arising from the official bill" +

                    " statement, failing which the Customer will be deemed to have accepted the official" +

                    " bill statement rendered for any period as correct and final." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby acknowledges and agrees that its obligation to pay promptly" +

                    " all charges due and payable as stated in the official bill statement to MMSSB shall" +

                    " not be waived, absolved or diminished by virtue of:-<br />" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Customer not receiving any official bill statement from MMSSB for any particular" +

                    " billing period; and/or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Customer's failure or neglect to check, enquire, understand and ascertain the" +

                    " nature of the Services subscribed or used by the Customer and the applicable charges" +

                    " associated with such Services." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In addition to its obligations under Clause 3.1, it shall be the Customer's responsibility" +

                    " to:-<br />" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " request from MMSSB for the official bill statement which it has not received for" +

                    " any given billing period;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to check the applicable handbooks or brochures and/or addendums or supplemental" +

                    " thereto made available by MMSSB from time to time and make the necessary enquiries" +

                    " with MMSSB to understand and ascertain the nature of the Services subscribed or" +

                    " used by the Customer and the applicable charges associated with the Services." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In the event of any fees or charges remaining unpaid after becoming due, MMSSB may" +

                    " charge interest at the rate of 1.5% per month (such rate to be applicable before" +

                    " and after judgment) on the overdue amount, calculated on a daily basis commencing" +

                    " from the date after the due date for payment up to, and including the date of payment" +

                    " or RM10.00 per month whichever shall be the higher, and the Customer shall be liable" +

                    " to pay such amounts. MMSSB may also, at its sole discretion, waive any late payment" +

                    " charges or interest on any overdue amount." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB shall be entitled to vary the amount of deposit, fees and any other charges" +

                    " for the Services or<strong> </strong>part thereof at any time by notice to the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " At MMSSB's absolute discretion, any credit balance in the value of Ringgit Malaysia" +

                    " Ten (RM10.00) and below in a deactivated account, whereby such credit balance value" +

                    " may be changed from time to time, as and when deemed necessary by MMSSB, shall be" +

                    " absorbed as administration fees for the Service(s) and/or other services as may" +

                    " be provided by MMSSB and/or its Related Corporations with an outstanding amount," +

                    " as deemed necessary by MMSSB at any time." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer agrees to bear all legal costs and expenses incurred by MMSSB in recovering" +

                    " any moneys, charges, costs, and expenses payable by the Customer under the Agreement," +

                    " and the Customer also agrees to indemnify MMSSB against all costs, expenses and" +

                    " charges or legal fees incurred by MMSSB in enforcing the Agreement or in bringing" +

                    " any action or proceeding to recover all charges, costs and expenses payable by the" +

                    " Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer agrees that a certificate signed by an authorised personnel in MMSSB" +

                    " as to the monies for the time being due and owing to MMSSB from the Customer shall" +

                    " be conclusive evidence or proof that the amount appearing therein is due and owing" +

                    " and payable by the Customer to MMSSB in any legal proceedings. Any admission or" +

                    " acknowledgement in writing by the Customer or any person authorised by the Customer" +

                    " of the amount of indebtedness of the Customer to MMSSB and any judgement recovered" +

                    " by MMSSB against the Customer in respect of such indebtedness shall be binding and" +

                    " conclusive evidence in any courts within or outside Malaysia. For the avoidance" +

                    " of doubt, in the event of a conflict or inconsistency between the official bill" +

                    " statement and the certificate of indebtedness, the certificate of indebtedness shall" +

                    " prevail." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>5.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>MOBILE NUMBER PORTABILITY/MNP</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer acknowledges and accepts that: -<br />" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Mobile Numbers requested for Porting by the Customer must be in the range of" +

                    " Mobile Numbers as approved by SKMM from time to time;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " Mobile Number Porting is subject to existing geographic numbering requirements;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " only active Mobile Numbers are eligible for Porting. Mobile Numbers which have been" +

                    " suspended, terminated, blacklisted on the defaulters database and/or barred shall" +

                    " not be eligible for Porting; and" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " MMSSB may, upon receipt of a Port request from the Customer, notify the Customer" +

                    " by way of SMS the progress of the Customer's Port request" +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer is allowed to Port from prepaid Services to postpaid Services and vice" +

                    " versa. However, the Customer agrees and accepts that all Porting requests are subject" +

                    " to the respective MMSSB terms and conditions for new registration." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer acknowledges and accepts that all Customer's Services associated with" +

                    " the Mobile Number provided by the DNO, including but not limited to, value added" +

                    " services, rate plans, charges and fees will be terminated when the SIM card of the" +

                    " DNO is deactivated upon the Customer's successful Porting to the RNO and activation" +

                    " of MMSSB's SIM Card. MMSSB shall not be held liable or responsible to any Customer" +

                    " or any third party claiming through a Customer for any loss or damage whether direct," +

                    " indirect, special or consequential, or for loss of business, revenue or profits" +

                    " or of any nature suffered by any Customer, or any person authorised by any Customer," +

                    " or any injury caused to or suffered by a person or damage to property arising from" +

                    " or occasioned by termination of the DNO SIM Card and the Customer's services associated" +

                    " with the Mobile Number provided by the DNO.&nbsp;&nbsp;&nbsp;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer is responsible for identifying the supplementary Mobile Numbers (e.g." +

                    " voice, fax and data) that the Customer wishes to Port along with their primary Mobile" +

                    " Numbers and to provide all information necessary to satisfy MMSSB to proceed with" +

                    " the Porting request." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer expressly consents, acknowledges and agrees that the Customer's request" +

                    " to MMSSB to Port the Customer's Mobile Number represents a notice to terminate the" +

                    " Customer's subscription with the DNO. MMSSB shall not be held responsible or liable" +

                    " for any unsuccessful termination of the Customer's subscription with the DNO and" +

                    " resulting failure to Port with MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In the event of a Port to MMSSB, or Port withdrawal or Port reversal to the DNO," +

                    " MMSSB: -" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " is not responsible for any period of outage of the Customer's mobile service or" +

                    " any related ancillary services;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " is not liable to the Customer or any person claiming through the Customer for any" +

                    " damage, loss, costs or expenses or other liability in contract or tort or otherwise" +

                    " direct or indirect, for or in relation to the Port, or Port withdrawal or Port reversal," +

                    " for any reason whatsoever;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " will not refund the Porting fee." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall be responsible to fully settle all outstanding bills from the" +

                    " DNO.&nbsp; In the event of non-payment by the Customer of all outstanding bills" +

                    " from the DNO, the services with MMSSB may be disrupted." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Acceptance of these Services/this Agreement's terms and conditions will only be" +

                    " effective upon activation of the Ported number by MMSSB.&nbsp;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In addition to clause 8 of this Agreement and for purposes of the Porting activity," +

                    " the Customer expressly authorizes MMSSB to provide information regarding the Customer's" +

                    " Mobile Number to be disclosed to other telecommunication service providers to enable" +

                    " the transfer of the Customer's Mobile Number from one telecommunication service" +

                    " provider to another." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer acknowledges and accepts that any fees paid for Porting are non refundable." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>6.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>SUSPENSION AND TERMINATION</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 6.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer may at any time terminate the Agreement by giving MMSSB prior notice" +

                    " in writing. The Service shall be deemed terminated within four (4) Working Days" +

                    " from receipt of the termination notice by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 6.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB shall be entitled at its absolute discretion to immediately suspend/terminate" +

                    " the Services or Agreement, without liability, at any time, without any notice and" +

                    " may not be required to give any reason whatsoever, including but not limited to" +

                    " the following reasons:" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if any technical failure occurs in the Services or the Related Corporations' network;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " while the Services are being upgraded, modified or maintained;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if the Customer breaches any of the terms and conditions of the Agreement and/or" +

                    " any Addendum;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if the Customer does anything which may in MMSSB's opinion, lead to, including but" +

                    " not limited to, the damage or injury to the Services or MMSSB's and/or the Related" +

                    " Corporations network, reputation;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if MMSSB is required to do so by law, statute, enactment, code or by any relevant" +

                    " authorities;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " f." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if it is in MMSSB's opinion that the Services or the Related Corporations network" +

                    " is or may be used fraudulently, illegally or for unlawful purposes." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " MMSSB will endeavour to resume the Services as soon as possible if suspension or" +

                    " disconnection occurs for the reasons set out in Clause 6.2(a) and (b) above. The" +

                    " Customer shall be liable for all applicable charges during the period of interruption," +

                    " suspension or loss of the Services or part thereof from any cause whatsoever." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 6.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Upon termination of the Agreement by either party, the Customer shall be liable" +

                    " to MMSSB for:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the monthly subscription, access or other monthly fee for Services for the entire" +

                    " month in which the termination was effective," +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " any call or other usage charges incurred by the Customer up to and including the" +

                    " effective date of termination; and" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " any other outstanding amounts." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 6.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Upon the termination of the Agreement, all monies owing to MMSSB by the Customer" +

                    " in accordance with the Customer's account shall become immediately due and payable" +

                    " and the Customer shall upon the demand of MMSSB settle his account promptly." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 6.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Upon suspension, MMSSB may at its absolute discretion reconnect the Services, subject" +

                    " to the Customer paying a reconnection fee, all outstanding amounts due to MMSSB" +

                    " and a refundable deposit as may be required by MMSSB for the reconnection of the" +

                    " Services." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>7.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>MMSSB'S RIGHT</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 7.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB and/or its Related Corporations reserves the right to make any alteration" +

                    " or changes to the Services, or any part thereof, or suspend the Services or any" +

                    " part thereof without prior notice and MMSSB and/or its Related Corporations shall" +

                    " not be liable for any loss or inconvenience to the Customer resulting therefrom." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 7.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB reserves the right at its absolute discretion, from time to time, to vary," +

                    " add to or otherwise amend the terms and conditions of the Agreement or any part" +

                    " thereof. The Customer will be given written notice of such amendments. The Customer's" +

                    " continued use of the Services after the effective date of any variation, addition" +

                    " or amendments to the terms and conditions of the Agreement shall constitute unconditional" +

                    " acceptance of such variations, additions or amendments by the Customer. If the Customer" +

                    " does not accept such variation, addition or amendments, the Customer shall be entitled" +

                    " to terminate the Agreement by giving prior written notice to MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 7.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB reserves the right at any time to require the Customer to pay any outstanding" +

                    " amount within seven (7) days from such notice to the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 7.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Use of the Services is subject to the Customer's credit limit. Upon the Customer's" +

                    " request and/or when MMSSB deems fit in its absolute discretion, the credit limit" +

                    " may be restricted, limited, reduced or increased subject to further terms and conditions" +

                    " as MMSSB deems fit to impose. MMSSB may in its absolute discretion bar or suspend" +

                    " the Services or part thereof if the credit limit is exceeded but MMSSB is not obliged" +

                    " to ensure and makes no representation whatsoever that automatic suspension or barring" +

                    " of the Services or part thereof will occur upon the call and other usage charges" +

                    " reaching the Customer's credit limit. It is the Customer's responsibility to ensure" +

                    " that its calls and other usage charges do not exceed the credit limit. For the avoidance" +

                    " of doubt, the Customer shall be obliged to promptly pay all call and other usage" +

                    " charges notwithstanding that the credit limit has been exceeded." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>8.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>PERSONAL INFORMATION</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 8.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby acknowledges that he is aware his Personal Information will" +

                    " be used and/or disclosed for the purposes set out in Clauses 8.2 and 8.3 below." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 8.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby expressly consents that MMSSB may use his Personal Information" +

                    " for any purpose which is necessary or related to MMSSB's provision of the Services" +

                    " to the Customer. In this respect, the Customer also expressly consents that MMSSB" +

                    " may disclose his Personal Information to MMSSB's agents, contractors, business partners," +

                    " associates or such other parties as are necessary to facilitate the provision of" +

                    " the Services by MMSSB to the Customer." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " <td style='width: 49%; float: right'>" +

                    " <table>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 8.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In addition and without derogation to Clause 8.2 above, the Customer further expressly" +

                    " consents that MMSSB may use and/or disclose his Personal Information as follows:&shy;-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to MMSSB's shareholders, Related Corporations and affiliated companies for purposes" +

                    " of providing any goods or services to the Customer;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to MMSSB's agents, contractors, business partners or associates for purposes of" +

                    " marketing programs or providing any goods or services to its Customers;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to MMSSB's agents or contractors for the purposes of recovering any amount due to" +

                    " MMSSB;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to payment channels including without limitation, financial institutions for purposes" +

                    " of maintaining financial records, assessing or verifying credit and facilitating" +

                    " payments of any amount due to MMSSB pursuant to the Agreement;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to regulatory, governmental bodies or other authorities in compliance with requirements" +

                    " under law or towards the detection or prevention of crime, illegal/unlawful activities" +

                    " and/or fraud;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " f." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to any party involved in or related to a legal proceeding, for purposes of the legal" +

                    " proceedings;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " g." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to other service providers or to parties nominated or appointed by MMSSB either" +

                    " solely or jointly with other service providers, for purposes of establishing and" +

                    " maintaining a common database of customers;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " h." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " for any purpose which is necessary or related to MMSSB's provision of the Services" +

                    " to you and/or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " i." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to MMSSB's professional advisors on a need to know basis." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 8.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Save in accordance with Clauses 8.2 and 8.3 above and except as permitted or required" +

                    " under any enactment, law, statute or code, MMSSB will not use or disclose the Customer's" +

                    " Personal Information." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 8.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby acknowledges his awareness that failure to provide complete" +

                    " and correct information to MMSSB as required in the Agreement including the Registration" +

                    " Form or any Addendum, may result in his application for Services being rejected," +

                    " the Services or Agreement being terminated and/or correspondence from MMSSB including" +

                    " without limitation, bill statements failing to reach the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>9.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>DISCLAIMER</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " THE SERVICES ARE PROVIDED ON AN &quot;AS IS&quot; AND &quot;AS AVAILABLE&quot; BASIS." +

                    " MMSSB AND/OR ITS RELATED CORPORATIONS SHALL NOT BE LIABLE FOR AND MAKES NO EXPRESS" +

                    " OR IMPLIED REPRESENTATION OR WARRANTIES OF ANY KIND IN RELATION TO THE SERVICES" +

                    " INCLUDING BUT NOT LIMITED TO: -" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " AVAILABILITY, ACCESSIBILITY, TIMELINESS AND UNINTERRUPTED USE OF THE SERVICES;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " SEQUENCE, ACCURACY, COMPLETENESS, TIMELINESS OR THE SECURITY OF ANY DATA OR INFORMATION" +

                    " TRANSMITTED USING THE SERVICES;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " SEQUENCE. ACCURACY, COMPLETENESS, TIMELINESS OR THE SECURITY OF ANY DATA OR INFORMATION" +

                    " PROVIDED TO CUSTOMER AS PART OF THE SERVICES E.G. VIA MAXIS INTERACTIVE SERVICES" +

                    " (MIS) OR MOBILE INTERNET SERVICES." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>10.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>MMSSB'S AND/OR ITS RELATED CORPORATIONS' LIABILITY</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 10.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB AND/OR ITS RELATED CORPORATIONS SHALL NOT BE LIABLE TO ANY CUSTOMER, OR ANY" +

                    " THIRD PARTY authorised BY OR CLAIMING THROUGH A CUSTOMER FOR ANY LOSS OR DAMAGE," +

                    " WHETHER DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL, OR FOR LOSS OF BUSINESS, REVENUE" +

                    " OR PROFITS OR OF ANY NATURE SUFFERED BY ANY CUSTOMER, OR ANY PERSON authorised BY" +

                    " ANY CUSTOMER, OR ANY INJURY CAUSED TO OR SUFFERED BY A PERSON OR DAMAGE TO PROPERTY" +

                    " ARISING FROM OR OCCASIONED BY:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " THE USE OR INABILITY TO USE BY THE CUSTOMER OR ANY PERSONS authorised BY THE CUSTOMER," +

                    " OF THE SERVICES OR ANY PART THEREOF AND THE MOBILE PHONE OR SIM CARD;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " ANY MALFUNCTION, UNAUTHORISED USE, CLONING OF OR DEFECT IN THE MOBILE PHONE/ SIM" +

                    " CARD OR THE LOSS OF THE MOBILE PHONE, SIM CARD OR SERVICES OR ANY PART THEREOF FOR" +

                    " WHATEVER REASONS." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " ANY ACT, OMISSION, ERROR, DEFAULT OR DELAY BY MMSSB AND/OR ITS RELATED CORPORATIONS," +

                    " ITS OFFICERS, EMPLOYEES AND AGENTS IN RELATION TO THE SERVICE." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 10.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " WITHOUT LIMITING THE GENERALITY OF CLAUSE 10.1, MMSSB AND/OR ITS RELATED CORPORATIONS" +

                    " SHALL NOT BE LIABLE FOR:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " ANY CLAIM FOR LIBEL, SLANDER, INFRINGEMENT OF ANY INTELLECTUAL PROPERTY RIGHTS ARISING" +

                    " FROM THE TRANSMISSION AND RECEIPT OF MATERIAL IN CONNECTION WITH THE SERVICES AND" +

                    " ANY CLAIMS ARISING OUT OF ANY ACT OR OMISSION OF THE CUSTOMER IN RELATION TO THE" +

                    " SERVICES OR ANY PART THEREOF." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " ANY LOSS OR DAMAGE CAUSED TO THE CUSTOMER AS A RESULT OF THE SUSPENSION/BARRING/TERMINATION" +

                    " OF THE AGREEMENT AND THE INTERRUPTION/LOSS OF THE SERVICES OR ANY PART THEREOF FROM" +

                    " ANY CAUSE." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " ANY LOSS, DISTORTION OR CORRUPTION OF DATA ARISING FROM THE USE OF THE SERVICE TO" +

                    " TRANSMIT DATA OR FOR DATA COMMUNICATION PURPOSES AT ANY STAGE OF THE TRANSMISSION" +

                    " INCLUDING ANY UNLAWFUL OR UNAUTHORISED ACCESS TO THE CUSTOMER'S TRANSMISSION OR" +

                    " DATA." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " INTERRUPTION OR UNAVAILABILITY OF THE SERVICE AS A RESULT OF INCLUDING BUT NOT LIMITED" +

                    " TO ADVERSE WEATHER CONDITIONS, ELECTROMAGNETIC INTERFERENCE, EQUIPMENT FAILURE OR" +

                    " CONGESTION IN MMSSB AND/OR THE RELATED CORPORATIONS' NETWORK OR TELECOMMUNICATION" +

                    " SYSTEMS." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 10.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB and/or its Related Corporations shall not be liable for, and the Customer" +

                    " agrees to indemnify MMSSB and/or its Related Corporations against all claims, losses," +

                    " liabilities proceedin<em>g</em>s, demands, costs and expenses (including legal fees)" +

                    " which may result or which MMSSB and/or its Related Corporations may sustain in connection" +

                    " with or arising from the provision of the Services to the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 10.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Without prejudice to the foregoing, in the event a court or an arbitrator holds" +

                    " or finds MMSSB and/or its Related Corporations liable to the Customer for any breach" +

                    " or default by MMSSB and/or its Related Corporations, the Customer agrees that the" +

                    " amount of damages payable by MMSSB and/or its Related Corporations to the Customer" +

                    " shall not at any time exceed the sum of RM500.00 notwithstanding any order, decree" +

                    " or judgment to the contrary." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>11.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>SIM CARD</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The SIM Card shall remain the property of MMSSB at all times and the property of" +

                    " the SIM Card does not at any time pass to the Customer. MMSSB grants the Customer" +

                    " the right to use the SIM Card for the purposes of the Service. The SIM Card must" +

                    " be returned to MMSSB on demand. Risk passes to the Customer immediately upon the" +

                    " execution of this Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Services and/or features to be provided under the SIM Card will depend on the" +

                    " type of Mobile Equipment used." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " A Customer must not change or transfer the SIM Card to any other person unless prior" +

                    " written notice has been served to and written approval has been obtained from MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " A Customer must register with and obtain a prior written approval from MMSSB if" +

                    " he intends to obtain a second or further SIM Card. He must pay all fees and charges" +

                    " required for the new subscription. The Agreement shall similarly apply with respect" +

                    " to the additional SIM Card." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " A Customer shall bear full responsibility for the usage of the SIM Card and charges" +

                    " incurred through the usage of the SIM Card including the use by any other person" +

                    " whether authorised by him or not." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " A Customer shall use all precautions to prevent the loss, theft, cloning and/or" +

                    " unauthorised use of the SIM Card. In the event of loss, theft, cloning and/or unauthorised" +

                    " use of the SIM Card, the Customer shall immediately notify MMSSB. Replacement of" +

                    " a SIM Card is subject to such payments as may be prescribed by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB shall only replace a defective SIM Card at no cost if the defect is proven" +

                    " to MMSSB's satisfaction to be caused by the manufacturer within 12 months from the" +

                    " date of issuance of the SIM Card." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>12.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>FORCE MAJEURE</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 12.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Without limiting the generality of any provision in the Agreement, MMSSB and/or" +

                    " its Related Corporations shall not be liable for any failure to perform its obligations" +

                    " herein caused by an Act of God, insurrection or civil disorder, military operations" +

                    " or act of terrorism, all emergency, acts or omission of Government, or any competent" +

                    " authority, labour trouble or industrial disputes of any kind, fire, lightning, subsidence," +

                    " explosion, floods, acts or omission of persons or bodies for whom MMSSB and/or its" +

                    " Related Corporations has no control over or any cause outside MMSSB's and/or its" +

                    " Related Corporations' reasonable control." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 12.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Notwithstanding the event of force majeure, the Customer shall remain obliged to" +

                    " pay all fees and charges which are outstanding and/or due and payable to MMSSB in" +

                    " accordance with the Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>13.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>SEVERABILITY AND EFFECT OF TERMS AND CONDITIONS</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 13.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " If any of the provision herein contained should be invalid, illegal or unenforceable" +

                    " under any applicable law, the legality and enforceability of the remaining provisions" +

                    " shall not be affected or impaired in any way and such invalid, illegal or unenforceable" +

                    " provision shall be deemed deleted." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 13.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The terms and conditions contained in the Agreement shall have effect only to the" +

                    " extent not forbidden by law. For the avoidance of doubt, it is hereby agreed and" +

                    " declared in particular, but without limitation, that nothing herein shall be construed" +

                    " as an attempt to contract out of any provisions of the Consumer Protection Act 1999," +

                    " if and where the said Act is applicable." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>14.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>GOVERNING LAW</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 14.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Agreement shall be governed by and construed in accordance with the laws of" +

                    " Malaysia, excluding its conflict of law rules. Parties agree to submit to the exclusive" +

                    " jurisdiction of the Malaysian courts." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 14.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Where any claims, proceedings, actions, suits or disputes arising or in connection" +

                    " with this Agreement is to be commenced or adjudicated in the High Court of Malaya," +

                    " the parties agree that it shall be adjudicated in the High Court in Kuala Lumpur" +

                    " or Putrajaya, Malaysia, as the case may be." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>15.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>NOTICES</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 15.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " All official bill statements, notices, requests, notice of demands, writ of summons," +

                    " all other legal process and/or other communications/documents to be given by MMSSB" +

                    " to a Customer under the Agreement will be in writing and sent to his last known" +

                    " address and/or published in national newspapers in the main languages, published" +

                    " daily and circulating generally throughout Malaysia, as the case maybe." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 15.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " All notices, requests, notice of demands, writ of summons, all other legal process" +

                    " and/or other communications/documents to be given by the Customer to MMSSB under" +

                    " the Agreement must be in writing and sent to the following address: Maxis Mobile" +

                    " Services Sdn Bhd (formerly known as Malaysian Mobile Services Sdn Bhd), Level 18," +

                    " Menara Maxis, Kuala Lumpur City Centre 50088 Kuala Lumpur. Fax 03-2330 0008." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 15.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " All official bill statements, notices, requests, notice of demands, writ of summons," +

                    " all other legal process and/or other communications/documents given by MMSSB to" +

                    " the Customer pursuant to this clause shall be deemed to have been served if:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " sent by registered post, on the second Working Day after the date of posting irrespective" +

                    " of whether it is returned undelivered;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " sent by ordinary post, on the fifth Working Day after the date of posting irrespective" +

                    " of whether it is returned undelivered;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " hand delivered, upon delivery;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " sent by facsimile, upon successful completion of transmission as evidence by a transmission" +

                    " report and provided that notice shall in addition thereon be sent by post to the" +

                    " other party; or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " published in national newspapers in the main languages, published daily and circulating" +

                    " generally throughout Malaysia in respect of any change in the Services, terms of" +

                    " the Agreement, charges and/or tariffs." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>16.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>ASSIGNMENT</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 16.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall not be permitted to assign or novate any or part of their rights" +

                    " or obligations under the Agreement to any party, without the prior written consent" +

                    " of MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 16.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB may assign or novate all or part of the Agreement to any third party by notice" +

                    " to the Customer without the Customer's prior consent and the Customer agrees to" +

                    " make all subsequent payments as instructed in such or further notice." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>17.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>INDULGENCE</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " No delay or indulgence by MMSSB in enforcing any term or condition of the Agreement" +

                    " nor the granting of time by MMSSB to a Customer shall prejudice the rights or powers" +

                    " of MMSSB nor shall any waiver by MMSSB of any breach constitute a continuing waiver" +

                    " in respect of any subsequent or continuing breach." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>18.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>CUSTOMER'S OBLIGATIONS IN RELATION TO CONTENT</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 18.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall not disseminate or provide to any third party:&shy;-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " Bursa Malaysia Securities Berhad (&quot;Bursa Malaysia&quot;) stock information" +

                    " or any part thereof (whether in its original or adapted form) received as part of" +

                    " Mobile Interactive Services (MIS); or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " any other information or content (whether in its original or adapted form) received" +

                    " as part of other Services as MMSSB shall inform the Customer from time to time." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 18.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall not use any information or content or any parts thereof (whether" +

                    " in its original or adapted form) received as part of the Services (including without" +

                    " limitation, as part of Mobile Interactive Services (MIS)), forpurposes of creation" +

                    " of any commercial products, whether tradable or otherwise including but not limited" +

                    " to, any derivative products whether for the Customer's own purposes or for the purposes" +

                    " of any third party." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>19.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>MISCELLANEOUS</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " No rule of construction or interpretation shall apply to prejudice the interest" +

                    " of the party preparing the Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In the event of a conflict or inconsistency between the Registration Form, these" +

                    " terms and conditions and the Addendums, such inconsistency shall be resolved by" +

                    " giving precedence in the following order: Addendums, these terms and conditions" +

                    " and the Registration Form." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Agreement constitutes the entire agreement between the parties concerning the" +

                    " subject matter herein and supersedes all previous agreements, understanding, proposals," +

                    " representations and warranties relating to that subject matter." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Those clauses which by their nature would survive the termination of the Agreement" +

                    " shall so survive, including without limitation Clauses 3.1, 3.2, 3.3, 3.11, 4.3," +

                    " 4.5, 4.6, 4.8, 4.9, 6.3, 6.4, 9, 10, 14, 15 and 18." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Time wherever referred to in this Agreement shall be of the essence." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Agreement shall be binding on and shall inure for the benefit of each party's" +

                    " permitted assigns, successors in title, personal representatives, executors and" +

                    " administrators." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall bear all stamp duty, service tax charges, and any other cost" +

                    " or charge imposed by law in connection with the preparation of the Agreement and/or" +

                    " the provision of the Services." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall immediately inform MMSSB in writing of any change of address" +

                    " and/or employment or business." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Words importing the singular number include the plural number and vice versa." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Words importing the masculine gender include feminine." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.11" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " An expression importing a natural person includes any company, partnership, joint" +

                    " venture, association, corporation or other body and any governmental agency." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.12" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " All provisions contained herein shall be equally applicable to any and all supplementary" +

                    " lines subscribed by the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.13" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Notwithstanding anything to the contrary, the Customer hereby agrees to be bound" +

                    " by the terms of service, policies and procedures and/or any variations, additions" +

                    " or amendments made thereto, as may be determined by MMSSB at any time in respect" +

                    " of each and every individual rate plan subscribed by the Customer as provided overleaf." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>20.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>DIRECT DEBIT, PAY-BY-PHONE AND m-BILLING</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In addition to the foregoin<em>g </em>terms and conditions, the provisions set out" +

                    " in this Clause 20 shall at any time during the term of the Agreement apply to Customers" +

                    " whose application for Direct Debit and/or Pay-By-Phone and/or m-Billing has been" +

                    " accepted by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer declares and undertakes that:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the information supplied overleaf by the Customer is true and correct;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Card nominated overleaf for Direct Debit, Pay-By-Phone and/or m-Billing is in" +

                    " the name of the Customer. Where the Card so nominated is in the name of a third" +

                    " party, the Customer declares and undertakes that the Cardholder has authorised the" +

                    " Customer to use the Card for purposes of Direct Debit, Pay-By-Phone and/or m-Billing" +

                    " hereunder;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Customer is the lawful and authorised holder of the Card or where the Card belongs" +

                    " to a third party, that the Cardholder is the lawful and authorised holder of the" +

                    " Card;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Card is valid and has not expired and shall remain valid and unexpired throughout" +

                    " the duration of the Customer's use of Direct Debit, Pay-By-Phone and/or m-Billing;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Card has not been suspended nor terminated;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " f." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to promptly pay all amounts due to MMSSB as reflected in the official bill statement" +

                    " and for all charges whatsoever occasioned by the use of the Services irrespective" +

                    " of whether such charges were authorised by the Customer, had exceeded by the Customer's" +

                    " credit limit or had arisen from any other causes whatsoever;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " g." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Customer's primary obligation under the Agreement to settle his/her MMSSB Service" +

                    " bills and to settle the same in a timely manner shall continue and shall not be" +

                    " waived, extended nor suspended in any manner whatsoever by the mere approval or" +

                    " agreement of MMSSB to provide the Services to the Customer; and" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " h." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " save and except where the fault or delay is clearly attributable to circumstances" +

                    " within MMSSB's reasonable control, all overdue payments shall be subject to interest" +

                    " for late payment and/or such other consequences as provided under the&nbsp; Agreement." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall allow an interval of at least fourteen (14) days from receipt" +

                    " by MMSSB of the completed registration form for the processing of the application" +

                    " and activation of Direct Debit, Pay-By-Phone and/or m-Billing." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB reserves the right at its absolute discretion to approve or reject the Customer's" +

                    " application for Direct Debit, Pay-By-Phone and/or m-Billing without assigning any" +

                    " reason whatsoever. The Customer will be notified in the event that his/her application" +

                    " has been rejected." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby expressly authorises MMSSB to:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " verify that information supplied overleaf with the Card Issuer or any third party" +

                    " as may be necessary;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " forward the Customer's call transactions, billings and other details to the Bank," +

                    " the Card Issuer and other relevant parties for and in connection with the Direct" +

                    " Debit, Pay-By-Phone and/or m-Billing;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " share its database on the Customer with MMSSB's Related Corporations, corporate" +

                    " shareholders, third parties and/or relevant authorities for the provision of integrated" +

                    " or related services, marketing programmes, and/or towards the detection and prevention" +

                    " of crime." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB shall not be liable to the Customer:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if the Card is not honoured by the Bank or the Card Issuer;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if provision of or authorisation to the Cardholder for Direct Debit, Pay-By-Phone" +

                    " and/or m-Billing is denied/refused or suspended at any time by any party for any" +

                    " reason whatsoever; or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if MMSSB is unable to or delays in providing Direct Debit, Pay-By-Phone and/or m-Billing" +

                    " as a result of a power failure, failure of any computer or telecommunication system" +

                    " used in connection with Direct Debit, Pay-By-Phone and/or m-Billing, or any other" +

                    " circumstances beyond MMSSB's reasonable control." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby acknowledges that he/she understands that Direct Debit, Pay-By-Phone" +

                    " and/or m-Billing is only applicable for settlement of periodic MMSSB bills. All" +

                    " and any other payments outside of the periodic bill cycle shall be promptly settled" +

                    " in the ordinary manner by the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Notwithstanding that a transaction may have been duly completed via Direct Debit," +

                    " Pay-By-Phone and/or m-Billing, the Customer's particular MMSSB bill has been credited" +

                    " as paid, MMSSB reserves the right and shall be entitled without prior notice to" +

                    " the Customer to reverse any payment entry in the Customer's statement of account" +

                    " or charge back the transaction sum to the Customer's account with MMSSB in the event" +

                    " of any one or more of the following circumstances:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction is cancelled by the Bank or the Card Issuer for any reason whatsoever;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction is found to be incomplete, illegal or carried out by fraudulent" +

                    " means;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction is found to be one with a &quot;Declined Authorisation&quot; or" +

                    " a non-corresponding authorisation code;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction sum or part thereof was found to have exceeded the Cardholder's" +

                    " authorised credit limit;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Card concerned is found to have expired, terminated or is invalid for any reason" +

                    " whatsoever;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " f." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction was entered into without the authorisation of the Cardholder or" +

                    " the Cardholder disputes the transaction or denies liability for whatever reason;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " g." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction was carried out or credit was given to the Customer in circumstances" +

                    " constituting a breach of any express or implied term, condition, representation" +

                    " or duty of the Customer;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " h." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the performance of the Direct Debit, Pay-By-Phone and/or m-Billing transaction or" +

                    " the use of the Card involves a violation of the law, rules or regulations of any" +

                    " governmental body, notwithstanding that MMSSB may have received notice of the same" +

                    " at the time when the transaction was carried out; or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " i." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " at the absolute discretion of MMSSB, the Bank or the Card Issuer, without assigning" +

                    " any reason whatsoever." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Notwithstanding Clause 20.1, the Services shall be automatically terminated with" +

                    " immediate effect and without notice to the Customer in the event that the Card is" +

                    " cancelled by the Bank or the Card Issuer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Notwithstanding anything to the contrary, m-Billing is only applicable or available" +

                    " to Customers subscribing to the Services unless otherwise notified in writing by" +

                    " MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                //Section-G code
                    "</tr></td></tr></table>");
            mailBody.AppendFormat("Yours Sincerely,");
            mailBody.AppendFormat("<br/>Maxis Team");

            LinkedResource logo = new LinkedResource(Server.MapPath(@"~/Content/images/maxislogo.png"), "image/png");
            logo.ContentId = "corpLogo";
            logo.TransferEncoding = TransferEncoding.Base64;
            //Added by VLT to fix image issue in signature of mail on 5 Mar 2012//
            string body = "<html><body>" + mailBody.ToString() + "<br/><img alt='' src=cid:corpLogo height='30px' /><br/></body></html>";

            AlternateView av1 = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
            av1.LinkedResources.Add(logo);
            mail.AlternateViews.Add(av1);


            mail.Body = body;
            //Added by VLT to fix image issue in signature of mail on 5 Mar 2012//
            SmtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpServerPort"].ToString());
            SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            SmtpServer.UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"].ToString());
            SmtpServer.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"].ToString());
            SmtpServer.Send(mail);
            //VLT ADDED CODE by Rajeswari for the E-mail functionality
            #endregion VLT ADDED CODE
        }

        private Online.Registration.DAL.Models.Registration ConstructRegistration(PersonalDetailsVM personalDetailsVM, string queueNo, string remarks, string signatureSVG = "", string custPhoto = "", string altCustPhoto = "", string photo = "", string ISSIMRequired = "", string SimCardTypeSelected = "")
        {
            var regTypeID = 0;
            Online.Registration.DAL.Models.Registration registration = new Online.Registration.DAL.Models.Registration();
            //var isBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] : true;
            //we set this always TRUE why because based on this we are storing the ApproveBlacklistCheck  in trnregistration this we are sending to kenan as sales code
            var isBlacklisted = true;
           // var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            decimal penaltyAmount = 0;
            string penalty = Session["Penalty"].ToString2();
            ///Written by Kota Durga Prasad to handle decimals
            if (!string.IsNullOrEmpty(penalty))
            {
                //if (penalty.IndexOf('.') > 0)
                //{
                //    penaltyAmount = penalty.Substring(0, penalty.IndexOf('.')).ToDecimal();
                //}
                //else
                //{
                penaltyAmount = penalty.ToDecimal();
                //}
            }

            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
            {
                regTypeID = Util.GetRegTypeID(REGTYPE.MNPPlanOnly);
            }
            else
            {
                if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
                    regTypeID = Util.GetRegTypeID(REGTYPE.DevicePlan);
                else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                    regTypeID = Util.GetRegTypeID(REGTYPE.PlanOnly);
                else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                    regTypeID = Util.GetRegTypeID(REGTYPE.DeviceOnly);
            }

            //Added by VLT ON APR 08 2013
            registration.K2_Status = Session[SessionKey.RegK2_Status.ToString()].ToString2();
            //End by VLT ON APR 08 2013


            registration = new Online.Registration.DAL.Models.Registration();
            registration.CenterOrgID = Util.SessionAccess.User.OrgID;
            registration.RegTypeID = regTypeID;
            //Added by Patanjali to support remarks on 30-03-2013
            registration.Remarks = remarks;
            //Added by Patanjali to support remarks on 30-03-2013 ends her
            registration.QueueNo = queueNo;
            registration.SignatureSVG = signatureSVG;
            //CustomerPhoto = Uri.EscapeDataString(custPhoto);
            //registration.CustomerPhoto = custPhoto;
            //registration.AltCustomerPhoto = altCustPhoto;
            //registration.Photo = photo;
            registration.AdditionalCharges = personalDetailsVM.Deposite;
            registration.InternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;
            registration.ExternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;

            // w.loon - Is Justification/Supervisor Approval Required
            string ListOfRequiredBreValidation = WebHelper.Instance.ListOfRequiredBreValidation()["ListOfRequiredBreValidation"];

            bool IsWriteOff = false;
            string acc = string.Empty;
            WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
            registration.WriteOffCheckStatus = ListOfRequiredBreValidation.Contains("Writeoff Check") ?
                                                    !IsWriteOff ? "WOS" : "WOF"
                                                    : "WON";
            registration.AgeCheckStatus = ListOfRequiredBreValidation.Contains("AgeCheck") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]) : "AF"
                                                    : "AN";
            registration.DDMFCheckStatus = ListOfRequiredBreValidation.Contains("DDMF Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]) : "DF"
                                                    : "DN";
            registration.AddressCheckStatus = ListOfRequiredBreValidation.Contains("Address Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]) : "ADF"
                                                    : "ADN";
            registration.OutStandingCheckStatus = ListOfRequiredBreValidation.Contains("Outstanding Credit Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()]) : "OF"
                                                    : "ON";
            registration.TotalLineCheckStatus = ListOfRequiredBreValidation.Contains("Total Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()]) : "TLF"
                                                    : "TLN";
            registration.PrincipleLineCheckStatus = ListOfRequiredBreValidation.Contains("Principle Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()]) : "PLF"
                                                    : "PLN";
            registration.BiometricVerify = ListOfRequiredBreValidation.Contains("Biometric") ?
                                                    (!ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? Session[SessionKey.RegMobileReg_BiometricVerify.ToString()].ToBool() : false)
                                                    : false;

			if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
			{
				registration.SalesPerson = Request.Cookies["CookieUser"].Value;
			}
            //added by Nreddy for MyKad
            registration.IsVerified = !ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? true : false;
            registration.ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
            registration.RFSalesDT = DateTime.Now;
            //commented by KD for smart
            //ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
            //if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && !ReferenceEquals(Session[SessionKey.RegMobileReg_MSISDN.ToString()], null))
            //{
            //    //registration.MSISDN1 = Convert.ToString(Session[SessionKey.RegMobileReg_MSISDN.ToString()]);
            //}
            //else
            //{
            //    registration.MSISDN1 =  Session[SessionKey.RegMobileReg_MobileNo.ToString()] == null ? null : ((List<string>) Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2();
            //}
            //MSISDN2 = ((List<string>) Session[SessionKey.RegMobileReg_MobileNo.ToString()])[1].ToString2();
            registration.MSISDN1 = Session[SessionKey.ExternalID.ToString()] == null ? string.Empty : Session[SessionKey.ExternalID.ToString()].ToString();
            registration.CreateDT = DateTime.Now;
            registration.LastAccessID = Util.SessionAccess.UserName;
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "0";
            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
            registration.ContractCheckStatus = ListOfRequiredBreValidation.Contains("Contract Check") ?
                                                !ReferenceEquals(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()]) : "CF"
                                                : "CN";
            registration.UserType = Session[SessionKey.PPID.ToString()].ToString() == null ? "N" : Session[SessionKey.PPID.ToString()].ToString();
            registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()].ToString()) : 0);
            registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthDevAdv.ToString()].ToString()) : 0);
            registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalyPlanDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyPlanDeposit"].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()].ToString()) : 0);
            registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalayDevDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayDevDeposit"].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()].ToString()) : 0);
            registration.OutstandingAcs = Session[SessionKey.FailedAcctIds.ToString()] == null ? string.Empty : Session[SessionKey.FailedAcctIds.ToString()].ToString();
            registration.PrePortInCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()]) : "MPPF";
            registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()]) : "MSIF";
            registration.ArticleID = !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty;
           

            if ((!ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null)) && (!ReferenceEquals(Session["ContractId"], null)))
            {
                string ArticleID = Session[SessionKey.ArticleId.ToString()].ToString();
                //Session[SessionKey.RegMobileReg_ContractID.ToString()]
                //int ContractsDuration = GetContractDuration(Session["ContractId"].ToInt());
                int ContractsDuration = GetContractDuration(Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt());
                string ContractId = Session["ContractId"].ToString();
                registration.UOMCode = GetUomId(ArticleID, ContractsDuration, ContractId);
            }
            else
            {
                registration.UOMCode = string.Empty;
            }

            //if (Session[SessionKey.RegMobileReg_UOMCode.ToString()] != null)
            //{
            //    registration.UOMCode = Session[SessionKey.RegMobileReg_UOMCode.ToString()].ToString();
            //}
            //else
            //{
            //    registration.UOMCode = string.Empty;
            //}
            //// For plan setting UOM code and offerid to generic values -- By Patanjali on 29-04-2013
            if (registration.RegTypeID != 2)
            {
                registration.OfferID = 0;
                registration.UOMCode = "EA";
            }

            registration.Discount = Session["DPrice"] == null ? 0 : Session["DPrice"].ToDecimal();
            if (Session["ContractType"] == null)
                registration.SM_ContractType = string.Empty;
            else
            {
                if (Session["ContractType"].ToString().ToUpper() == "EXTEND")
                {
                    registration.SM_ContractType = "E";
                }
                //else if (Session["TerminatenAssNew"] != null && Session["TerminatenAssNew"].ToString().ToUpper() == "TRUE")
                else if (Session["TnA"].ToString2().ToUpper() == "TRUE")
                {
                    registration.SM_ContractType = "T";
                }
                else if (Session[SessionKey.AssignNew.ToString()] != null && Session[SessionKey.AssignNew.ToString()].ToString().ToUpper() == "TRUE")
                {
                    registration.SM_ContractType = "A";
                }


            }

            registration.KenanAccountNo = Session["KenanAccountNo"].ToString();
            registration.fxAcctNo = Session[SessionKey.AccountNumber.ToString()].ToString();
            registration.fxSubscrNo = Session["SubscriberNo"].ToString();
            registration.fxSubScrNoResets = Session["SubscriberNoResets"].ToString();
            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : "No";
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "No";
            registration.externalId = Session[SessionKey.ExternalID.ToString()].ToString();
            //TODO: important to revert this conversion and need to send it as decimal only
            registration.PenaltyAmount = penaltyAmount;
            registration.Trn_Type = "S";
            registration.Discount_ID = Session["discountID"] == null ? string.Empty : Session["discountID"].ToString();
            registration.Smart_ID = ConfigurationManager.AppSettings["SmartDiscountID"];
            //added by Nreddy
            registration.OrganisationId = Util.SessionAccess.User.Org.OrganisationId;
            //added by KD on 03/06/2013 for storing CMSS ID in DB for reconcilation report
            // registration.CMSSID = Session["CMSSID"] == null ? string.Empty : Session["CMSSID"].ToString();
            //VL
            if (ISSIMRequired.ToLower() == "true")
                registration.IsSImRequired_Smart = true;
            else
                registration.IsSImRequired_Smart = false;


            registration.IsK2 = ((!ReferenceEquals(Session["IsExistingK2"], null) && Session["IsExistingK2"].ToString() == "1") ? true : false);

            registration.PenaltyWaivedAmt = Session[SessionKey.PenaltyWaiveOff.ToString()].ToString2() == string.Empty ? 0 : Convert.ToDecimal(Session[SessionKey.PenaltyWaiveOff.ToString()]);
            //if (regTypeID == Util.GetRegTypeID(REGTYPE.DeviceCRP))
            //{
            // S - for supplines and other type is principle line
			if (Session["AccountType"].ToString2() == "P")
            {
                registration.CRPType = ((int)CrpType.SuplementaryToPrincipal).ToString2();
            }

            // PBI000000009222 - iSell SMART - Supplementary FnF Corridor Auto Disconnected
            // value derived from Accounts.cshtml
            if (Session["ActionType"].ToString2().ToLower() == "continue")
            {
                registration.CRPType = ((int)CrpType.PrincipalToPrincipal).ToString2();
            }

            //}
            return registration;
        }

        private List<RegSmartComponents> ConstructSmartComponents()
        {
            ComponentViewModel sessionVal = (ComponentViewModel)Session["_componentViewModel"];
            List<RegSmartComponents> regSmartComponents = new List<RegSmartComponents>();
            RegSmartComponents smartcomp = new RegSmartComponents();

            foreach (var c in sessionVal.NewComponentsList)
            {
                if (c.IsChecked)
                {
                    smartcomp = new RegSmartComponents();
                    smartcomp.PackageID = c.NewPackageKenanCode;
                    smartcomp.ComponentID = c.NewComponentKenanCode;
                    smartcomp.ComponentDesc = c.NewComponentName;
                    smartcomp.PackageDesc = string.Empty;                     // write package name here if require
                    regSmartComponents.Add(smartcomp);
                }
            }
            //foreach (var c in sessionVal.DependentVASList)
            //{
            //    if (c.IsChecked)
            //    {
            //        smartcomp = new RegSmartComponents();
            //        smartcomp.PackageID = c.NewPackageKenanCode;
            //        smartcomp.ComponentID = c.NewComponentKenanCode;
            //        smartcomp.ComponentDesc = c.NewComponentName;
            //        smartcomp.PackageDesc = string.Empty;
            //        regSmartComponents.Add(smartcomp);
            //    }
            //}
            if (sessionVal.DependentVASList != null)
            {
                foreach (var c in sessionVal.DependentVASList)
                {
                    if (c.IsChecked)
                    {
                        smartcomp = new RegSmartComponents();
                        smartcomp.PackageID = c.NewPackageKenanCode;
                        smartcomp.ComponentID = c.NewComponentKenanCode;
                        smartcomp.ComponentDesc = c.NewComponentName;
                        smartcomp.PackageDesc = string.Empty;
                        regSmartComponents.Add(smartcomp);
                    }
                }
            }

            if (Session[SessionKey.RegMobileReg_DataplanID.ToString()] != null && Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt() != 0)
            {

                //selectedCompIDs.Add(Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt());
                //TODO: need to fix whether we need to pass data planid to kenan or not
                //Urgent: 
                var bundlepackages = new List<PgmBdlPckComponentSmart>();
                using (var proxysmartcatelogserviceproxy = new SmartCatelogServiceProxy())
                {
                    bundlepackages = proxysmartcatelogserviceproxy.PgmBdlPckComponentGetSmart(new List<int>() { Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt() }).Where(a => a.Active == true).ToList();

                    if (bundlepackages != null && bundlepackages.Count > 0)
                    {
                        smartcomp = new RegSmartComponents();
                        smartcomp.PackageID = bundlepackages[0].KenanCode;
                        if (Session[SessionKey.RegMobileReg_ContractID.ToString()] != null && Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt() != 0)
                        {
                            var contractPkgID = proxysmartcatelogserviceproxy.PgmBdlPckComponentGet(new int[] { Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt() }).SingleOrDefault().AlternateKenanCode;
                            smartcomp.PackageID = contractPkgID;
                        }
                        smartcomp.ComponentID = bundlepackages[0].KenanCode;
                        smartcomp.ComponentDesc = bundlepackages[0].Name;
                        //To get Component Name & Price
                        var componentGet = proxysmartcatelogserviceproxy.ComponentGet(new int[] { bundlepackages[0].ChildID }).SingleOrDefault();
                        smartcomp.ComponentDesc = componentGet.Description + " - RM" + bundlepackages[0].Price.ToString();
                        regSmartComponents.Add(smartcomp);
                    }
                }

            }

            return regSmartComponents;
        }
        private Customer ConstructCustomer()
        {
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            personalDetailsVM.Customer.PayModeID = 1;
            personalDetailsVM.Customer.LanguageID = 1;
            

            //if (personalDetailsVM.Customer.DateOfBirth.Equals(DateTime.MinValue))
            //    personalDetailsVM.Customer.DateOfBirth = DateTime.MinValue;
            //else
            //    personalDetailsVM.Customer.DateOfBirth = Convert.ToDateTime(personalDetailsVM.DOBYear + "-" + personalDetailsVM.DOBMonth + "-" + personalDetailsVM.DOBDay);

            //if (Session["dobCusOrdSummary"] != null)
            //{
            //    personalDetailsVM.Customer.DateOfBirth = Convert.ToDateTime(Session["dobCusOrdSummary"].ToString());
            //}


            //personalDetailsVM.Customer.DateOfBirth = Convert.ToDateTime(personalDetailsVM.DOBYear + "-" + personalDetailsVM.DOBMonth + "-" + personalDetailsVM.DOBDay);

            DateTime dob = DateTime.MinValue;
            DateTime.TryParse(personalDetailsVM.DOBYear + "-" + personalDetailsVM.DOBMonth + "-" + personalDetailsVM.DOBDay, out dob);
            personalDetailsVM.Customer.DateOfBirth = dob;

            int CustomerTitleIDfromConfig = ConfigurationManager.AppSettings["CustomerTitleID"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["CustomerTitleID"]) : 0;
            if (personalDetailsVM.Customer.CustomerTitleID <= 0)
            {
                personalDetailsVM.Customer.CustomerTitleID = CustomerTitleIDfromConfig;
            }
            if (string.IsNullOrEmpty(personalDetailsVM.Customer.ContactNo))
            {
                personalDetailsVM.Customer.ContactNo = "60123456789";
            }
            //validation for email, 
            if (personalDetailsVM.Customer.EmailAddr != null)
            {

                bool validEmail = Util.isValidEmail(personalDetailsVM.Customer.EmailAddr);

                if (!validEmail)
                {
                    personalDetailsVM.Customer.EmailAddr = "";
                }
            }
            // fix contact number format for data inconsistency issue
			Util.FormatContactNumber(personalDetailsVM.Customer.ContactNo);

            var cust = new Customer()
            {
                FullName = personalDetailsVM.Customer.FullName,
                Gender = personalDetailsVM.Customer.Gender,
                DateOfBirth = personalDetailsVM.Customer.DateOfBirth,
                //DateOfBirth = Convert.ToDateTime(personalDetailsVM.DOBYear + "-" + personalDetailsVM.DOBMonth + "-" + personalDetailsVM.DOBDay),
                CustomerTitleID = personalDetailsVM.Customer.CustomerTitleID,
                RaceID = personalDetailsVM.Customer.RaceID,
                LanguageID = personalDetailsVM.Customer.LanguageID,
                NationalityID = personalDetailsVM.Customer.NationalityID,
                IDCardTypeID = personalDetailsVM.Customer.IDCardTypeID,
                IDCardNo = personalDetailsVM.Customer.IDCardNo,
                ContactNo = personalDetailsVM.Customer.ContactNo,
                AlternateContactNo = personalDetailsVM.Customer.AlternateContactNo,
                EmailAddr = personalDetailsVM.Customer.EmailAddr,
                PayModeID = personalDetailsVM.Customer.PayModeID,
                CardTypeID = personalDetailsVM.Customer.CardTypeID,
                NameOnCard = personalDetailsVM.Customer.NameOnCard,
                CardNo = personalDetailsVM.Customer.CardNo,
                CardExpiryDate = personalDetailsVM.Customer.CardExpiryDate,
                IsVIP = Session["isVIP_Masking"].ToBool() ? 1 : 0,
                Active = true,
                LastAccessID = Util.SessionAccess.UserName,
                CreateDT = DateTime.Now
            };

            return cust;
        }

        private RegMdlGrpModel ConstructRegMdlGroupModel()
        {
            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                return new RegMdlGrpModel();

            int? mdlGrpModelID = null;

            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
            {
                using (var proxy = new SmartCatelogServiceProxy())
                {
                    //var bundleID = proxy.PgmBdlPckComponentGet(new int[] {  Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt() }).SingleOrDefault().ParentID;
                    //var bpPgmBdlPkgCompIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    //{
                    //    PgmBdlPckComponent = new PgmBdlPckComponent()
                    //    {
                    //        ParentID = bundleID,
                    //        LinkType = Properties.Settings.Default.Bundle_Package
                    //    },
                    //    Active=true
                    //}).ToList();

                    var modelGroupIDs = proxy.ModelGroupFind(new ModelGroupFind()
                    {
                        ModelGroup = new ModelGroup()
                        {
                            PgmBdlPckComponentID = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt()
                        },
                        //PgmBdlPkgCompIDs = bpPgmBdlPkgCompIDs,
                        Active = true
                    });

                    var modelGroupModels_Smart = proxy.ModelGroupModelGet_Smart(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                    {
                        ModelGroupModel = new ModelGroupModel()
                        {
                            ModelID = Session["RegMobileReg_SelectedModelID"].ToInt()
                        }
                    })).ToList();

                    mdlGrpModelID = modelGroupModels_Smart.Where(a => modelGroupIDs.Contains(a.ModelGroupID)).Select(a => a.ID).FirstOrDefault();
                }
            }

            var regMdlGrpModel = new RegMdlGrpModel()
            {
                ModelImageID = Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt(),
                ModelGroupModelID = mdlGrpModelID != null ? mdlGrpModelID : 0,
                Price = Session["RegMobileReg_RRPPrice"] != null ? Session["RegMobileReg_RRPPrice"].ToDecimal() : 0,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            };

            return regMdlGrpModel;

        }

        private List<Address> ConstructRegAddress()
        {
            var addresses = new List<Address>();
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            //Added by KD for Language,email,Race
            personalDetailsVM.Customer.LanguageID = 1;
            personalDetailsVM.Customer.EmailAddr = string.Empty;
            personalDetailsVM.Customer.RaceID = 4;
            if (personalDetailsVM.Address.StateID <= 1)
            {
                personalDetailsVM.Address.StateID = 2;
            }
            addresses.Add(new Address()
            {
                AddressTypeID = 1,
                Line1 = personalDetailsVM.Address.Line1,
                Line2 = personalDetailsVM.Address.Line2,
                Line3 = personalDetailsVM.Address.Line3,
                Postcode = personalDetailsVM.Address.Postcode,
                StateID = personalDetailsVM.Address.StateID,
                Town = personalDetailsVM.Address.Town,
                Street = "",
                BuildingNo = "",
                CountryID = Util.GetIDByCode(RefType.Country, Properties.Settings.Default.Country_MY),
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });

            return addresses;
        }

        private List<RegPgmBdlPkgComp> ConstructRegPgmBdlPkgComponent()
        {
            var regPBPCs = new List<RegPgmBdlPkgComp>();

            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                return regPBPCs;

            // Plan PgmBdlPkgComponent
            regPBPCs.Add(new RegPgmBdlPkgComp()
            {
                RegSuppLineID = null,
                PgmBdlPckComponentID = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(),
                IsNewAccount = true,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });

            // Mandatory VAS PgmBdlPkgComponent
            var vasPBPCIDs = new List<int>();
            using (var proxy = new SmartCatelogServiceProxy())
            {
                var pkgID = proxy.PgmBdlPckComponentGetSmart(new int[] { Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt() }).SingleOrDefault().ChildID;

                vasPBPCIDs = proxy.PgmBdlPckComponentFindSmart(new PgmBdlPckComponentFindSmart()//modified for oneplan CR
                {
                    IsMandatory = true,
                    PgmBdlPckComponentSmart = new PgmBdlPckComponentSmart()
                    {
                        ParentID = pkgID,
                        LinkType = Properties.Settings.Default.Package_Component
                    },
                    Active = true
                }).ToList();

                foreach (var vasPBPCID in vasPBPCIDs)
                {
                    regPBPCs.Add(new RegPgmBdlPkgComp()
                    {
                        RegSuppLineID = null,
                        PgmBdlPckComponentID = vasPBPCID,
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }

            // Optional VAS PgmBdlPkgComponent
            var vasIDs = Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2().Split(',');
            // vasIDs[vasIDs.Length] = Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToString();
            // Session[SessionKey.RegMobileReg_DataplanID.ToString()]

            foreach (var vasID in vasIDs)
            {
                if (!string.IsNullOrEmpty(vasID.ToString()))
                {
                    regPBPCs.Add(new RegPgmBdlPkgComp()
                    {
                        PgmBdlPckComponentID = vasID.ToInt(),
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }
            //if (Session[SessionKey.RegMobileReg_DataplanID.ToString()] != null && Session[SessionKey.RegMobileReg_DataplanID.ToString()] != "" && Convert.ToInt32(Session[SessionKey.RegMobileReg_DataplanID.ToString()]) != 0)
            //{
            //    regPBPCs.Add(new RegPgmBdlPkgComp()
            //    {
            //        PgmBdlPckComponentID = Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt(),
            //        IsNewAccount = true,
            //        Active = true,
            //        CreateDT = DateTime.Now,
            //        LastAccessID = Util.SessionAccess.UserName
            //    });
            //}
            string depcompIds = GetDepenedencyComponents(Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToString(), false);
            List<string> depListCompIds = new List<string>();
            depListCompIds = depcompIds.Split(',').ToList();

            foreach (string compid in depListCompIds)
            {

                if (compid != string.Empty && Convert.ToInt32(compid) > 0)
                {
                    regPBPCs.Add(new RegPgmBdlPkgComp()
                    {
                        PgmBdlPckComponentID = compid.ToInt(),
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }

            }
            /*Mandatory package*/
            if (Session["intmandatoryidsval"] != null && Session["intmandatoryidsval"] != "" && Convert.ToInt32(Session["intmandatoryidsval"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session["intmandatoryidsval"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            /*data plan*/
            if (Session["intdataidsval"] != null && Session["intdataidsval"] != "" && Convert.ToInt32(Session["intdataidsval"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session["intdataidsval"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            /*extra plan*/
            if (Session["intextraidsval"] != null && Session["intextraidsval"] != "" && Convert.ToInt32(Session["intextraidsval"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session["intextraidsval"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            return regPBPCs;
        }

        private void ClearRegistrationSession()
        {
            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
            Session["intmandatoryidsval"] = null;
            Session["intdataidsval"] = null;
            Session["intextraidsval"] = null;
            Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
            Session["RegMobileReg_BiometricID"] = null;
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
            Session["RegMobileReg_SelectedModelID"] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
            Session[SessionKey.RegMobileReg_MobileNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session["RegMobileReg_PhoneVM"] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session[SessionKey.RegStepNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardType.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = null;
            Session[SessionKey.RegMobileReg_SublineVM.ToString()] = null;
            Session[SessionKey.ArticleId.ToString()] = null;
            Session["RegMobileReg_SelectedContracts"] = null;
            Session["_componentViewModel"] = null;
            Session["existingKenanIDs"] = null;
            ClearSubLineSession();
        }

        private RegStatus ConstructRegStatus()
        {
            var regStatus = new RegStatus()
            {
                Active = true,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName,
                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegNew)
            };

            return regStatus;
        }

        private List<RegSuppLine> ConstructRegSuppLines()
        {
            var regSuppLines = new List<RegSuppLine>();

            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];

            if (sublines != null)
            {
                foreach (var subline in sublines)
                {
                    // SuppLine Plan
                    regSuppLines.Add(new RegSuppLine()
                    {
                        SequenceNo = subline.SequenceNo,
                        PgmBdlPckComponentID = subline.PkgPgmBdlPkgCompID,
                        IsNewAccount = true,
                        MSISDN1 = subline.SelectedMobileNos[0].ToString2(),
                        //MSISDN2 = subline.SelectedMobileNos[1].ToString2(),
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }

            return regSuppLines;
        }

        private List<RegPgmBdlPkgComp> ConstructRegSuppLineVASes()
        {
            var regSuppLinePBPCs = new List<RegPgmBdlPkgComp>();
            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];

            if (sublines != null)
            {
                foreach (var subline in sublines)
                {
                    // Mandatory SuppLine VAS
                    var suppLineVasIDs = new List<int>();
                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        var pkgID = proxy.PgmBdlPckComponentGet(new int[] { subline.PkgPgmBdlPkgCompID }).SingleOrDefault().ChildID;

                        suppLineVasIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                        {
                            IsMandatory = true,
                            PgmBdlPckComponent = new PgmBdlPckComponent()
                            {
                                ParentID = pkgID,
                                LinkType = Properties.Settings.Default.Package_Component
                            }
                        }).ToList();
                    }

                    foreach (var suppLineVasID in suppLineVasIDs)
                    {
                        regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
                        {
                            SequenceNo = subline.SequenceNo,
                            PgmBdlPckComponentID = suppLineVasID,
                            IsNewAccount = true,
                            Active = true,
                            CreateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }

                    // Optional SuppLine VAS
                    foreach (var vasID in subline.SelectedVasIDs)
                    {
                        regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
                        {
                            SequenceNo = subline.SequenceNo,
                            PgmBdlPckComponentID = vasID.ToInt(),
                            IsNewAccount = true,
                            Active = true,
                            CreateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }
                }
            }
            return regSuppLinePBPCs;
        }
        private void LogException(Exception ex)
        {
            Logger.DebugFormat("Exception: {0}", ex.Message);
        }

        //Unused Methods

        //private RedirectToRouteResult RedirectException(Exception ex)
        //{
        //    Util.SetSessionErrMsg(ex);
        //    return RedirectToAction("Error", "Home");
        //}


        private void SetRegStepNo(SmartRegistrationSteps regStep)
        {
            switch (regStep)
            {
                case SmartRegistrationSteps.Device:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Device;
                    break;
                case SmartRegistrationSteps.DeviceCatalog:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.DeviceCatalog;
                    break;
                case SmartRegistrationSteps.Plan:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Plan;
                    break;
                case SmartRegistrationSteps.Vas:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Vas;
                    break;
                case SmartRegistrationSteps.Penalty:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.MobileNo;
                    break;
                case SmartRegistrationSteps.PersonalDetails:
                    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                    {
                        Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() + 1;
                    }
                    else
                    {
                        Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.PersonalDetails;
                    }
                    break;
                default:
                    break;
            }

            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan || Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
            {
                if (regStep == SmartRegistrationSteps.Device)
                    Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() + 1;
            }
            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
            {
                Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() - 1;
            }

            // set Personal Details as step 3 for Device Only
            if (regStep == SmartRegistrationSteps.PersonalDetails)
            {

            }
        }

        private string GetRegActionStep(int stepNo)
        {
            if ((int)SmartRegistrationSteps.Device == stepNo)
            {
                return "SelectDevice";
            }
            if ((int)SmartRegistrationSteps.DeviceCatalog == stepNo)
            {
                return "SelectDeviceCatalog";
            }
            else if ((int)SmartRegistrationSteps.Plan == stepNo)
            {
                return "SelectPlan";
            }
            else if ((int)SmartRegistrationSteps.SelectComponent == stepNo)
            {
                return "SelectComponent";
            }
            else if ((int)SmartRegistrationSteps.Vas == stepNo)
            {
                return "SelectVAS";
            }
            else if ((int)SmartRegistrationSteps.Penalty == stepNo)
            {
                //return "SelectMobileNo";
                return "SelectDiscount";
            }
            else if ((int)SmartRegistrationSteps.PersonalDetails == stepNo)
            {
                return "PersonalDetails";
            }
            else if ((int)SmartRegistrationSteps.Summary == stepNo)
            {
                return "MobileRegSummary";
            }
            else if ((int)SmartRegistrationSteps.Summary == stepNo)
            {
                return "MobileRegSummary";
            }

            return "";
        }

        private DeviceOptionVMSmart GetAvailableDeviceOptions()
        {
            var deviceOptVM = new DeviceOptionVMSmart();

            //using (var proxy = new SmartCatelogServiceProxy())
            //{
            //    var brandIDs = proxy.BrandFind(new BrandFindSmart()
            //    {
            //        Brand = new BrandSmart() { },
            //        Active = true
            //    });

            //    deviceOptVM.AvailableBrands = proxy.BrandGet(brandIDs).ToList();
            //}

            if (!string.IsNullOrEmpty(Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToString2()))
            {
                deviceOptVM.SelectedDeviceOption = "btnDvcOpt_" + Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToString2() + "_" + Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToString2();
            }
            deviceOptVM.Type = Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToString2();

            //if (Session["ContractType"] != null)
            //{
            //if (Session["ContractType"].ToString().ToUpper() == "EXTEND")
            //    GetExtendContracts();
            //}

            /////////////////
            if (Session["ContractType"] != null)
            {
                if (Session["ContractType"].ToString().ToUpper() == "ASSIGNNEW" || Session["ContractType"].ToString().ToUpper() == "TERIMNATE")
                {
                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        List<BrandSmart> brandSmarts = new List<BrandSmart>();

                        if (Session["AccountType"].ToString2() == "S")
                            brandSmarts = proxy.GetBrandSmartsForSuppLines();
                        else
                        {
                            var brandIDs = proxy.BrandFind(new BrandFindSmart()
                            {
                                Brand = new BrandSmart() { },
                                Active = true
                            });
                            //brandIDs = proxy.GetCRPBrands("E");
                            brandSmarts = proxy.BrandGet(brandIDs).ToList();
                        }


                        Session["list"] = brandSmarts.Select(c => c.Code).ToList();
                        deviceOptVM.AvailableBrands = brandSmarts.ToList();
                    }

                }
                else if (Session["ContractType"].ToString().ToUpper() == "EXTEND")
                {
                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        var brandIDs = proxy.BrandFind(new BrandFindSmart()
                        {
                            Brand = new BrandSmart() { },
                            Active = true
                        });

                        deviceOptVM.AvailableBrands = proxy.BrandGet(brandIDs).ToList();
                    }
                    GetExtendContracts();
                }

            }
            ///////////////



            return deviceOptVM;
        }

        public class ClassArticleId
        {
            public int id { get; set; }
            public string articleId { set; get; }
            public int modelId { set; get; }
            public int colourID { set; get; }
            public string imagePath { set; get; }
            public int count { set; get; }
        }

        //UNUSED METHODS
        //private int Count(string articleid)
        //{
        //    int articleCount = 0;
        //    //using (var proxy = new StoreServiceProxy())
        //    //{
        //    //    articleCount = proxy.GetStockCount(articleid);
        //    //}
        //    return articleCount;
        //}

        private PhoneVMSmart GetAvailableModelImage()
        {
            var modelIDs = new List<int>();
            var models = new List<ModelSmart>();
            var phoneVM = new PhoneVMSmart();
            var articleID = new List<BrandArticleSmart>();
            var availableModelImages = new List<AvailableModelImageSmart>();

            if ((PhoneVM)Session["RegMobileReg_PhoneVM"] != null)
            {
                phoneVM = (PhoneVMSmart)Session["RegMobileReg_PhoneVM"];
            }


            // select all device images
            using (var proxy = new SmartCatelogServiceProxy())
            {
                //commented to implement logic for both principal & supp

                //if (Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToInt() == (int)MobileRegDeviceOption.Brand)
                //{
                //    modelIDs = proxy.ModelFindSmart(new ModelFindSmart()
                //    {
                //        Model = new ModelSmart()
                //        {
                //            BrandID = Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToInt()
                //        },
                //        Active = true,
                //        IsSmart = true,
                //    }).ToList();

                //    models = proxy.ModelGetSmart(modelIDs).ToList();
                //}

                if (Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToInt() == (int)MobileRegDeviceOption.Brand)
                {

                    //if (Session["ContractType"].ToString2().ToUpper() == "TERIMNATE")
                    if (Session["ContractType"].ToString().ToUpper() == "ASSIGNNEW" || Session["ContractType"].ToString().ToUpper() == "TERIMNATE")
                    {
                        if (Session["AccountType"].ToString2() == "S")
                            models = proxy.GetSmartModelsForSupplines(Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToInt()).ToList();
                        else
                        {
                            //need to check contract type, if not terminate move to outer if block
                            modelIDs = proxy.ModelFindSmart(new ModelFindSmart()
                            {
                                Model = new ModelSmart()
                                {
                                    BrandID = Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToInt()
                                },
                                Active = true,
                                IsSmart = true,
                            }).ToList();

                            models = proxy.ModelGetSmart(modelIDs).ToList();
                        }

                        modelIDs = models.Select(c => c.ID).ToList();
                    }
                    //else
                    else if (Session["ContractType"].ToString().ToUpper() == "EXTEND")
                    {
                        modelIDs = proxy.ModelFindSmart(new ModelFindSmart()
                        {
                            Model = new ModelSmart()
                            {
                                BrandID = Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToInt()
                            },
                            Active = true,
                            IsSmart = true,
                        }).ToList();

                        models = proxy.ModelGetSmart(modelIDs).ToList();
                    }


                }

                #region Code for geting the count from the service by using dynamic proxy and geting the article id
                List<ClassArticleId> lstarticleId = new List<ClassArticleId>();
				Dictionary<string, int> stockCount = new Dictionary<string, int>();

                int counter = 0;
                using (var catalogProxy = new SmartCatelogServiceProxy())
                {
                    for (int i = 0; i < modelIDs.Count; i++)
                    {
                        articleID = proxy.GetArticleID(modelIDs[i].ToInt()).ArticleIDs.ToList();

                        if (articleID.Count > 0)
                        {

                            for (int j = 0; j < articleID.Count; j++)
                            {
                                ClassArticleId ClassArticleId = new ClassArticleId()
                                {
                                    id = articleID[j].ID,
                                    articleId = articleID[j].ArticleID.Trim(),
                                    modelId = articleID[j].ModelID,
                                    imagePath = ReferenceEquals(articleID[j].ImagePath, null) ? string.Empty : articleID[j].ImagePath.ToString().Trim(),
                                    colourID = articleID[j].ColourID,

                                };
                                lstarticleId.Add(ClassArticleId);
                            }
                            counter++;
                        }
                    }
                    if (counter <= 0)
                    {
                        lstarticleId = null;
                    }
                }

                //string strArgs = "100000061001";
                //int subscriptionReqFromVasResult;
                string wsdlUrl = string.Empty;

                //Added by Vahini to call new method to get stock counts for dealers
                int storeId = 0;
                string[] maxisStoreIds = ConfigurationManager.AppSettings["OrganizationsIds"].ToString().Split(",".ToCharArray());
                if (!maxisStoreIds.Contains(Util.SessionAccess.User.Org.OrganisationId))
                {
                    storeId = Convert.ToInt32(Util.SessionAccess.User.Org.OrganisationId);
                }
                if (!ReferenceEquals(Util.SessionAccess.User.Org.WSDLUrl, null))
                {
                    wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                }
                try
                {
                    using (var dsproxy = new DynamicServiceProxy())
                    {
						List<string> listArticleID = lstarticleId.Select(x => x.articleId).Distinct().ToList();
						stockCount = dsproxy.GetStockCounts(listArticleID, wsdlUrl, storeId);

                        //lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = (int)(dsproxy.GetStockCount(i.articleId, wsdlUrl,storeId)));
                    }
                }
                catch (Exception ex)
                {
                    //RedirectToAction("StoreError", "HOME");
                    throw;
                }
                /*end here*/



                if (!ReferenceEquals(lstarticleId, null))
                {
                    foreach (var item in lstarticleId)
                    {
                        availableModelImages.Add(new AvailableModelImageSmart()
                        {

                            BrandArticle = new BrandArticleSmart()
                            {
                                ID = item.id,
                                ColourID = item.colourID,
                                ImagePath = item.imagePath,
                                ModelID = item.modelId,
                                ArticleID = item.articleId,
                            },
                            ModelCode = models.Where(a => a.ID == item.modelId).SingleOrDefault().Code,
                            //Count = item.count
							Count = stockCount.ContainsKey(item.articleId) ? stockCount[item.articleId] : 0
                        });
                    }
                }
                #endregion

            }

            phoneVM.ModelImages = availableModelImages;
            phoneVM.TabNumber = Session[SessionKey.RegMobileReg_TabIndex.ToString()].ToInt();

            if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt() != 0)
                phoneVM.SelectedModelImageID = Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToString2();

            return phoneVM;
        }

        private PackageVM GetAvailablePackages(bool fromSubline = false)
        {
            List<int> lstExtendedPackages = new List<int>();
            var packageVM = new PackageVM();
            var availablePackages = new List<AvailablePackage>();
            var PgmBdlPckComponentSmart = new List<PgmBdlPckComponentSmart>();
            var programBundles = new List<PgmBdlPckComponentSmart>();
            var packages = new List<Package>();
            var bundles = new List<Bundle>();
            var bundleIDs = new List<int>();
            var programs = new List<Program>();
            var filterBdlIDs = new List<int>();
            //var bundlePackages = new List<PgmBdlPckComponentSmart>();
            var bundlePackages = new PgmBdlPckComponentGetResp();
            var packageIDs = new List<int>();
            var orderVM = new OrderSummaryVM();
            int plan = 0;


            using (var proxy = new SmartCatelogServiceProxy())
            {
                // retrieve bundles that assigned to current user
                var filterBdlPkgIDs = proxy.PgmBdlPckComponentFindSmart(new PgmBdlPckComponentFindSmart()
                {
                    PgmBdlPckComponentSmart = new PgmBdlPckComponentSmart()
                    {
                        LinkType = Properties.Settings.Default.Program_Bundle,
                        FilterOrgType = Util.SessionOrgTypeCode
                    },
                    Active = true
                }).ToList();

                //filterBdlIDs = proxy.PgmBdlPckComponentGet(filterBdlPkgIDs).Select(a => a.ChildID).ToList();
                filterBdlIDs = proxy.PgmBdlPckComponentGetSmart(filterBdlPkgIDs).Select(a => a.ChildID).ToList();
                bundleIDs = filterBdlIDs;
                // Main Line
                //if (!fromSubline)
                //{
                if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
                {   /*Commented for articleid*/
                    //var modelID = proxy.ModelImageGet(Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt()).ModelID;
                    var modelID = proxy.BrandArticleImageGet(Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt()).ModelID;

                    //var modelGroupIDs = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                    //{
                    //    ModelGroupIDs = new List<int>(),
                    //    ModelGroupModel = new ModelGroupModel()
                    //    {
                    //        ModelID = modelID
                    //    },
                    //    Active = true
                    //})).Select(a => a.ModelGroupID).Distinct().ToList();

                    var modelGroupIDs_Smart = proxy.ModelGroupModelGet_Smart(proxy.ModelGroupModelFind_Smart(new ModelGroupModelFind_Smart()
                    {
                        ModelGroupIDs = new List<int>(),
                        ModelGroupModel = new ModelGroupModel_Smart()
                        {
                            ModelID = modelID
                        },
                        Active = true
                    })).Select(a => a.ModelGroupID).Distinct().ToList();

                    if (Session["ContractType"] != null)
                    {
                        if (Session["ContractType"].ToString().ToUpper() == "EXTEND")
                        {
                            //lstExtendedPackages = proxy.GetExtendedPackages(modelID);
                            lstExtendedPackages = proxy.GetSmartExtendedPackages(modelID);
                        }

                        // will uncomment and impliment if terminate is not working properly
                        //else if (Session["ContractType"].ToString() == "Terimnate")
                        //{
                        //    List<ModelGroup> modelGroupIDs1 = new List<ModelGroup>();

                        //    if (fromSubline == true)
                        //        modelGroupIDs1 = proxy.GetModelGroupsForSupplines(modelID);
                        //    else
                        //        modelGroupIDs1 = proxy.GetCRPPackages(modelID, "");
                        //    if (modelGroupIDs1 != null)
                        //        modelGroupIDs = modelGroupIDs1.Select(c => c.ID).ToList();
                        //}

                    }

                    #region Commented BY Raj

                    //// Bundle - Package
                    //if (modelGroupIDs_Smart.Count > 0)
                    //{
                    //    var pbpcIDs = proxy.ModelGroupGet(modelGroupIDs_Smart).Select(a => a.PgmBdlPckComponentID).ToList();

                    //    bundlePackages = proxy.PgmBdlPckComponentGetSmart(pbpcIDs).Where(a => a.Active == true).ToList();
                    //    bundleIDs = filterBdlIDs.Intersect(bundlePackages.Select(a => a.ParentID)).ToList();
                    //    packageIDs = bundlePackages.Select(a => a.ChildID).ToList();
                    //}
                    //else
                    //{
                    //    return packageVM;
                    //}


                    #endregion
                    IEnumerable<int> modelGroupIDs = null;
                    if (modelGroupIDs_Smart != null)
                        modelGroupIDs = modelGroupIDs_Smart;//.Select(a => a.ModelGroupID).Distinct().ToList();


                    if (Session["ContractType"] != null)
                    {
                        if (Session["ContractType"].ToString().ToUpper() == "EXTEND")
                        {
                            //////  if (fromSubline == true)
                            ////// lstExtendedPackages = proxy.GetExtendedPackagesForSupplines(modelID);
                            //////else
                            //////  lstExtendedPackages = proxy.GetExtendedPackages(modelID);
                        }
                        else if (Session["ContractType"].ToString() == "Terimnate")
                        {
                            List<ModelGroup> modelGroupIDs1 = new List<ModelGroup>();

                            if (fromSubline == true)
                                modelGroupIDs1 = proxy.GetModelGroupSmartForSupplines(modelID);
                            else
                                modelGroupIDs1 = proxy.GetCRPSmartPackages(modelID, "");
                            if (modelGroupIDs1 != null)
                                modelGroupIDs = modelGroupIDs1.Select(c => c.ID).ToList();
                        }

                       
                    }


                    #region New Implimentation By Raj

                    #region New Implimentation By Raj


                    AvalablePackages _eligiblePackags = new AvalablePackages();
                    IList<SubscriberICService.PackageModel> Existingpackages;
                    Existingpackages = GetPackageDetails(Session[SessionKey.ExternalID.ToString()].ToString2(), ConfigurationManager.AppSettings["KenanMSISDN"]);
                    retrieveSmartServiceInfoProxy _retrieveServiceInfoProxy = new retrieveSmartServiceInfoProxy();
                    string tempString = string.Empty;
                    List<string> PkgIDs = new List<string>();
                    List<string> ExistingKenanIDs = new List<string>();
                    var resp_existing = new RegistrationSvc.SmartGetExistingPackageDetailsResp();
                    //Added for sending mandatory package id for getting eligible packages
                    foreach (var item in Existingpackages)
                    {
                        ExistingKenanIDs.Add(item.PackageID);
                        using (var proxy_kenan = new SmartRegistrationServiceProxy())
                        {
                            //principal pkg call
                            resp_existing = proxy_kenan.ExistingPackageDetails(item.PackageID.ToInt());
                        }
                        if ((resp_existing.ExistingPackageDetails.PlanType == "CP" && fromSubline == false) || (resp_existing.ExistingPackageDetails.PlanType == "SP"))
                        {
                            _eligiblePackags = _retrieveServiceInfoProxy.RetrieveEligiblePackages(item.PackageID.ToInt(), ConfigurationManager.AppSettings["mktCode"].ToInt(), ConfigurationManager.AppSettings["acctCategoryId"].ToInt(), ConfigurationManager.AppSettings["rateClass"].ToInt());

                            if (_eligiblePackags != null)
                            {
                                if (_eligiblePackags.EligiblePackagesList.Count > 0)
                                {
                                    foreach (var item1 in _eligiblePackags.EligiblePackagesList)
                                    {
                                        PkgIDs.Add(item1.PackageId);
                                    }
                                }
                            }
                        }
                        //PkgIDs.Add(_eligiblePackags.EligiblePackagesList.Select(a => a.PackageId));
                    }
                    Session["existingKenanIDs"] = ExistingKenanIDs;

                    //_eligiblePackags = _retrieveServiceInfoProxy.RetrieveEligiblePackages(4074, 32, 1, 1);   // Params values Hard coded for now 

                    //IEnumerable<string> PkgIDs = _eligiblePackags.EligiblePackagesList.Select(a => a.PackageId).ToList();
                    //TODO:need to remove
                    PkgIDs = PkgIDs.Take(1000).ToList();
                    //bundlePackages = proxy.PgmBdlPckComponentGetForSmartByKenanCode(PkgIDs).Where(a => a.Active == true).ToList();
                    bundlePackages = proxy.PgmBdlPckComponentGetForSmartByKenanCode(PkgIDs);
                    //bundlePackages = bundlePackages.Where(a => a.Active == true && a.PlanType == "BP" && (a.PlanType == "CP" || a.PlanType == "MP" || a.PlanType == "EP" || a.PlanType == "DP")).ToList();
                    if (fromSubline == false)
                    {
                        if (bundlePackages.PgmBdlPckComponents != null && bundlePackages.PgmBdlPckComponents.Any())
                        {
                            var PgmBdlPckComponentsList = bundlePackages.PgmBdlPckComponents.Where(a => a.Active == true && a.PlanType == "BP" && (a.PlanType == "CP" || a.PlanType == "MP" || a.PlanType == "EP" || a.PlanType == "DP")).ToList();
                            if (PgmBdlPckComponentsList.Any())
                            {
                                bundlePackages.PgmBdlPckComponents = PgmBdlPckComponentsList.ToArray();
                                bundleIDs = filterBdlIDs.Intersect(bundlePackages.PgmBdlPckComponents.Select(a => a.ParentID)).ToList();
                                packageIDs = bundlePackages.PgmBdlPckComponents.Select(a => a.ChildID).ToList();
                            }
                        }
                    }
                    else
                    {
                        if (bundlePackages.PgmBdlPckComponents != null && bundlePackages.PgmBdlPckComponents.Any())
                        {
                            var PgmBdlPckComponentsList1 = bundlePackages.PgmBdlPckComponents.Where(a => a.Active == true && a.PlanType == "BP" && (a.PlanType == "SP" || a.PlanType == "MP" || a.PlanType == "EP" || a.PlanType == "DP")).ToList().ToArray();
                            if (PgmBdlPckComponentsList1.Any())
                            {
                                bundlePackages.PgmBdlPckComponents = PgmBdlPckComponentsList1.ToArray();
                            }
                        }
                    }



                    //TestData:
                    if (packageIDs.Count < 1)
                    {
                        // Bundle - Package
                        if (modelGroupIDs_Smart.Count > 0)
                        {
                            var lpackages = new List<PgmBdlPckComponentSmart>();

                            var pbpcIDs = proxy.ModelGroupGet(modelGroupIDs_Smart).Select(a => a.PgmBdlPckComponentID).ToList();

                            lpackages = proxy.PgmBdlPckComponentGetSmart(pbpcIDs).ToList();
                            lpackages = lpackages.Where(a => a.Active == true).ToList();

                            //bundlePackages.PgmBdlPckComponents = lpackages.ToArray();

                            bundleIDs = filterBdlIDs.Intersect(lpackages.Select(a => a.ParentID)).ToList();
                            packageIDs = lpackages.Select(a => a.ChildID).ToList();
                        }
                        else
                        {
                            return packageVM;
                        }

                    }

                    #endregion

                }
                // }


                var query = proxy.PgmBdlPckComponentGetSmart(proxy.PgmBdlPckComponentFindSmart(new PgmBdlPckComponentFindSmart()
                {
                    PgmBdlPckComponentSmart = new PgmBdlPckComponentSmart()
                    {
                        LinkType = Properties.Settings.Default.Bundle_Package,
                        PlanType = fromSubline ? Properties.Settings.Default.SupplimentaryPlan : Properties.Settings.Default.ComplimentaryPlan,
                    },
                    ParentIDs = bundleIDs,
                    ChildIDs = packageIDs,
                    Active = true
                }));

                // Main Line
                if (!fromSubline)
                {
                    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
                    {
                        PgmBdlPckComponentSmart = query.Where(a => bundleIDs.Contains(a.ParentID)).ToList();
                    }
                    else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                    {
                        PgmBdlPckComponentSmart = query.ToList();
                    }
                }
                else //Sub Line
                {
                    PgmBdlPckComponentSmart = query.ToList();
                }

                if (PgmBdlPckComponentSmart.Count() > 0)
                {
                    packages = proxy.PackageGet(PgmBdlPckComponentSmart.Select(a => a.ChildID).Distinct()).ToList();
                    bundles = proxy.BundleGet(PgmBdlPckComponentSmart.Select(a => a.ParentID).Distinct()).ToList();
                }

                var pgmBdlIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.Program_Bundle,
                    },
                    ChildIDs = PgmBdlPckComponentSmart.Select(a => a.ParentID).ToList()
                });

                programBundles = proxy.PgmBdlPckComponentGetSmart(pgmBdlIDs).Distinct().ToList();

                var programIDs = programBundles.Select(a => a.ParentID).ToList();

                if (programIDs != null)
                {
                    if (programIDs.Count() > 0)
                    {
                        programs = proxy.ProgramGet(programIDs).ToList();
                    }
                }
            }
            if ((!ReferenceEquals(Session["ContractType"], null)) && Session["ContractType"].ToString().ToUpper() == "EXTEND")
            {

                ContractDetails ContractDet = new ContractDetails();
                if (Session["ContractDet"] != null)
                {
                    ContractDet = (SmartViewModels.ContractDetails)Session["ContractDet"];
                }
                foreach (var pbpc in PgmBdlPckComponentSmart)
                {
                    if (ContractDet.PackageDesc == packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name && plan == 0)
                    {
                        plan = 1;
                    }
                    if (plan == 1)
                    {
                        availablePackages.Add(new AvailablePackage()
                        {
                            PgmBdlPkgCompID = pbpc.ID,
                            ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
                            BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
                            PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                            PackageCode = pbpc.Code,
                            KenanCode = pbpc.KenanCode,
                        });
                    }

                }
                if (plan == 0)
                {

                    foreach (var pbpc in PgmBdlPckComponentSmart)
                    {

                        availablePackages.Add(new AvailablePackage()
                        {
                            PgmBdlPkgCompID = pbpc.ID,
                            ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
                            BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
                            PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                            PackageCode = pbpc.Code,
                            KenanCode = pbpc.KenanCode,
                        });
                    }
                }
            }

            else
            {
                foreach (var pbpc in PgmBdlPckComponentSmart)
                {

                    availablePackages.Add(new AvailablePackage()
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
                        BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
                        PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        PackageCode = pbpc.Code,
                        KenanCode = pbpc.KenanCode,
                    });
                }
            }

            //Commented by Vahini on 21\3\2014 as smart packages need not be filtered based on the offers available
            /*
            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
            {
                if (Session[SessionKey.ArticleId.ToString()] != null)
                {
                    string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
                    List<string> objPackages = new List<string>();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        objPackages = proxy.GetPackagesForArticleId(Session[SessionKey.ArticleId.ToString()].ToString(), MOCStatus);

                    }
                    if (objPackages != null && objPackages.Count > 0)
                        packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).Where(a => string.Join(",", objPackages).Contains(a.KenanCode)).ToList();
                    //else
                    //    packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();
                }
                else
                    packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();

            }
            else
                packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();
            */
            //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid

            //packageVM.AvailablePackages = availablePackages.OrderBy(a => a.PgmBdlPkgCompID).ThenBy(a => a.PackageName).ToList();

            packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();

            //if(lstExtendedPackages
            if (lstExtendedPackages.Count() > 0 && Session["ContractType"].ToString().ToUpper() == "EXTEND")
            {
                //packageVM.AvailablePackages =  availablePackages
                var query = (from order in availablePackages
                             join plan1 in lstExtendedPackages
                            on order.PgmBdlPkgCompID equals plan1
                             select order).ToList();
                packageVM.AvailablePackages = query;
            }

            //packageVM.AvailablePackages.Remove(packages.Where(a => a.Name==
            // Main Line
            if (!fromSubline)
                packageVM.SelectedPgmBdlPkgCompID = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt();
            else
                packageVM.SelectedPgmBdlPkgCompID = Session["RegMobileSub_PkgPgmBdlPkgCompID"].ToInt();

            return packageVM;
        }

        private ValueAddedServicesVM GetAvailablePackageComponents(int pbpcID, bool fromSubline = false, bool isMandatory = false, bool ExtendContractCheck = false)
        {
            var vasVM = new ValueAddedServicesVM();
            var retrievedVases = new List<AvailableVAS>();
            var contractVases = new List<AvailableVAS>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponentSmart>();
            var components = new List<Component>();
            var componentTypes = new List<ComponentType>();

            using (var proxy = new SmartCatelogServiceProxy())
            {
                var packageID = (pbpcID != null && pbpcID != 0) ? proxy.PgmBdlPckComponentGet(new int[] { pbpcID }).SingleOrDefault().ChildID : 0;
                if (packageID != 0)
                {
                    pgmBdlPckComponents = proxy.PgmBdlPckComponentGetSmart(proxy.PgmBdlPckComponentFindSmart(new PgmBdlPckComponentFindSmart()
                    {
                        PgmBdlPckComponentSmart = new PgmBdlPckComponentSmart()
                        {
                            LinkType = Properties.Settings.Default.Package_Component,
                            ParentID = packageID,
                        },
                        IsMandatory = isMandatory,
                        Active = true
                    })).ToList();
                }
                if (pgmBdlPckComponents.Count() > 0)
                {
                    if (Session["ContractType"] != null)
                    {
                        if (Session["ContractType"].ToString().ToUpper() == "EXTEND")
                        {
                            if (ExtendContractCheck == true)
                            {
                                pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.contract_type != "E").ToList();
                            }
                            else
                            {

                                pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.contract_type == "E").Distinct().ToList();
                                if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] != null)
                                {
                                    string[] mobileIDs = Session["ModelID"].ToString().Split(',');
                                    for (int mobIDCnt = 0; mobIDCnt < mobileIDs.Length; mobIDCnt++)
                                    {
                                        //pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.ModelId == mobileIDs[mobIDCnt]).ToList();
                                        pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.ModelId != null).ToList();
                                        pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.ModelId.Contains(mobileIDs[mobIDCnt])).ToList();
                                    }
                                }
                            }
                        }
                        else
                        {
                            pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.contract_type != "E").ToList();
                        }
                    }
                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();

                    componentTypes = proxy.ComponentTypeGet(components.Select(a => a.ComponentTypeID).Distinct()).ToList();
                }
            }
            var newpgmBdlPckComponents = new List<PgmBdlPckComponentSmart>();
            if (pgmBdlPckComponents != null)
            {
                // pgmBdlPckComponents = pgmBdlPckComponents.Select(a => a.ChildID).Distinct().ToList();
                //pgmBdlPckComponents = (List<PgmBdlPckComponentSmart>)pgmBdlPckComponents.Select(a => a.ChildID).Distinct().ToList();
                List<int> lstChildids = pgmBdlPckComponents.Select(a => a.ChildID).Distinct().ToList();
                foreach (var item in lstChildids)
                {
                    newpgmBdlPckComponents.Add(pgmBdlPckComponents.Where(a => a.ChildID == item).FirstOrDefault());
                }
            }

            foreach (var pbpc in newpgmBdlPckComponents)
            {

                var component = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault();

                var compTypeCode = componentTypes.Where(a => a.ID == component.ComponentTypeID).SingleOrDefault().Code;

                if (compTypeCode == Properties.Settings.Default.CompType_DataCon || compTypeCode == Properties.Settings.Default.CompType_VoiceCon)
                {
                    contractVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price
                    });
                }
                else
                {
                    retrievedVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price
                    });
                }


            }

            if (isMandatory)
            {
                vasVM.MandatoryVASes = retrievedVases;
            }
            else
            {
                vasVM.AvailableVASes = retrievedVases;
            }
            vasVM.ContractVASes = contractVases;

            vasVM.SelectedContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2();
            vasVM.SelectedVasIDs = fromSubline ? Session["RegMobileSub_VasIDs"].ToString2() : Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2();

            return vasVM;
        }

        private ValueAddedServicesVM GetAvailablePackageComponents_PC_MC_DC(int pbpcID, string listtype, bool fromSubline = false, bool isMandatory = false)
        {
            var vasVM = new ValueAddedServicesVM();
            var retrievedVases = new List<AvailableVAS>();
            var contractVases = new List<AvailableVAS>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var components = new List<Component>();
            var componentTypes = new List<ComponentType>();


            int packageID = 0;
            using (var proxy = new SmartCatelogServiceProxy())
            {

                packageID = (pbpcID != null) ? proxy.PgmBdlPckComponentGet(new int[] { pbpcID }).SingleOrDefault().ChildID : 0;

                if (packageID != 0)
                {

                    pgmBdlPckComponents.AddRange(proxy.PgmBdlPckComponentGet(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            LinkType = listtype,
                            ParentID = packageID,
                        },
                        IsMandatory = isMandatory,
                        Active = true
                    })).ToList());



                }
                if (pgmBdlPckComponents.Count() > 0)
                {
                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();

                    componentTypes = proxy.ComponentTypeGet(components.Select(a => a.ComponentTypeID).Distinct()).ToList();
                }
            }


            foreach (var pbpc in pgmBdlPckComponents)
            {
                var component = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault();
                var compTypeCode = componentTypes.Where(a => a.ID == component.ComponentTypeID).SingleOrDefault().Code;

                if (compTypeCode == Properties.Settings.Default.CompType_DataCon || compTypeCode == Properties.Settings.Default.CompType_VoiceCon)
                {
                    contractVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price
                    });
                }
                else
                {
                    retrievedVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price
                    });
                }
            }

            if (isMandatory)
            {
                vasVM.MandatoryVASes = retrievedVases;
            }
            else
            {
                vasVM.AvailableVASes = retrievedVases;
            }
            vasVM.ContractVASes = contractVases;

            vasVM.SelectedContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2();
            vasVM.SelectedVasIDs = fromSubline ? Session["RegMobileSub_VasIDs"].ToString2() : Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2();

            return vasVM;
        }

        private void ConstructSuppLineSummary(List<RegSuppLine> suppLines, List<RegPgmBdlPkgComp> regPBPCs)
        {
            var strVAS = "";
            var MSISDNs = new List<string>();
            var sublineVM = new List<SublineVM>();
            var suppLinePBPCs = new List<PgmBdlPckComponent>();

            using (var proxy = new SmartCatelogServiceProxy())
            {
                suppLinePBPCs = proxy.PgmBdlPckComponentGet(regPBPCs.Select(a => a.PgmBdlPckComponentID).ToList()).ToList();
            }

            foreach (var suppLine in suppLines)
            {
                ClearSubLineSession();

                var pbpcIDs = regPBPCs.Where(a => a.RegSuppLineID == suppLine.ID).Select(a => a.PgmBdlPckComponentID).ToList();
                var PBPCs = suppLinePBPCs.Where(a => pbpcIDs.Contains(a.ID)).ToList();

                var bpCode = Properties.Settings.Default.Bundle_Package;
                var pcCode = Properties.Settings.Default.Package_Component;

                Session["RegMobileSub_PkgPgmBdlPkgCompID"] = suppLine.PgmBdlPckComponentID;

                var vasIDs = PBPCs.Where(a => a.LinkType == pcCode).Select(a => a.ID).ToList();
                if (vasIDs.Count() > 0)
                {
                    strVAS = "";
                    foreach (var vasID in vasIDs)
                    {
                        strVAS = strVAS + vasID + ",";
                    }
                }
                Session["RegMobileSub_VasIDs"] = strVAS;

                if (!string.IsNullOrEmpty(suppLine.MSISDN1) || !string.IsNullOrEmpty(suppLine.MSISDN1))
                {
                    MSISDNs = new List<string>();
                    MSISDNs.Add(suppLine.MSISDN1);
                    MSISDNs.Add(suppLine.MSISDN2);
                }
                Session["RegMobileSub_MobileNo"] = MSISDNs;

                ConstructSubLineVM(false);
            }
        }

        private bool ValidateCardDetails(Customer customer)
        {
            if (!IsCardDetailsEntered(customer))
            {
                ModelState.AddModelError(string.Empty, "Please enter all Card details.");
                return false;
            }

            //if (customer.CardNo.Length != 16)
            //{
            //    ModelState.AddModelError(string.Empty, "Credit Card No must be 16 digit.");
            //    return false;
            //}

            //if (customer.CardExpiryDate.Length != 4)
            //{
            //    ModelState.AddModelError(string.Empty, "Credit Card Expiry Date must be 4 digit.");
            //    return false;
            //}

            return true;
        }

        private bool IsCardDetailsEntered(Customer customer)
        {
            if (!customer.CardTypeID.HasValue || customer.CardTypeID.Value <= 0)
                return false;

            if (string.IsNullOrEmpty(customer.CardNo))
                return false;

            if (string.IsNullOrEmpty(customer.CardExpiryDate))
                return false;

            if (string.IsNullOrEmpty(customer.NameOnCard))
                return false;

            return true;
        }

        //UNUSED METHODS
        //private creditCheckOperation ConstructInternalCheck(string idCardNo, string idCardTypeID)
        //{
        //    var userRoles = new List<string>();
        //    userRoles.Add(Properties.Settings.Default.IntChk_Role);

        //    var orgUnit = new List<string>();
        //    orgUnit.Add(Properties.Settings.Default.IntChk_Org);

        //    return new creditCheckOperation
        //    {
        //        creditCheckRequestField = new CreditCheck_RequestType
        //        {
        //            serviceHeaderField = new ServiceHeader
        //            {
        //                securityInfoField = new SecurityHeaderType
        //                {
        //                    orgUnitsField = orgUnit.ToArray(),
        //                    userRolesField = userRoles.ToArray(),
        //                    userNameTokenField = new UserNameTokenType()
        //                    {
        //                        idField = Properties.Settings.Default.IntChk_UserTokenID,
        //                        userNameField = Properties.Settings.Default.IntChk_UserTokenName,
        //                        userTypeField = Properties.Settings.Default.IntChk_UserTokenType
        //                    },
        //                },
        //                serviceInfoField = new ServiceRequestHeaderType
        //                {
        //                    operationNameField = Properties.Settings.Default.IntChk_Operation,
        //                    serviceNameField = new ServiceRequestHeaderTypeServiceName()
        //                    {
        //                        serviceIdField = Properties.Settings.Default.IntChk_ServiceID,
        //                        valueField = Properties.Settings.Default.IntChk_ServiceValue,
        //                        versionField = Properties.Settings.Default.IntChk_ServiceVersion
        //                    },
        //                    serviceRequesterInfoField = new ServiceRequesterInfoType()
        //                    {
        //                        applicationNameField = new ServiceRequesterInfoTypeApplicationName()
        //                        {
        //                            appIdField = Properties.Settings.Default.IntChk_AppID,
        //                            valueField = Properties.Settings.Default.IntChk_AppValue
        //                        },
        //                    },
        //                    transactionInfoField = new ServiceTransactionInfoType()
        //                    {
        //                        majorTransactionIdField = Properties.Settings.Default.IntChk_TransMajorID,
        //                        minorTransactionIdField = Properties.Settings.Default.IntChk_TransMinorID,
        //                        messageIdField = Properties.Settings.Default.IntChk_TransMsgID
        //                    }
        //                }
        //            },
        //            serviceRequestField = new _CreditCheckRequest
        //            {
        //                iD_TypeField = idCardTypeID, //Util.GetIDCardTypeID(IDCARDTYPE.NRIC).ToString2(),
        //                iD_ValueField = idCardNo
        //            }
        //        }
        //    };

        //    //String XmlizedString = null;
        //    //MemoryStream memoryStream = new MemoryStream();
        //    //XmlSerializer xs = new XmlSerializer(typeof(InternalCheck));
        //    //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, null);

        //    //xs.Serialize(xmlTextWriter, intCheck);
        //    //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
        //    //UTF8Encoding encode = new UTF8Encoding();
        //    //XmlizedString = encode.GetString(memoryStream.ToArray());

        //    //return XmlizedString;
        //}

        //UNUSED METHODS
        //private string ConstructExternalCheck(ExternalCheck extCheck)
        //{
        //    String XmlizedString = null;
        //    //MemoryStream memoryStream = new MemoryStream();
        //    //XmlSerializer xs = new XmlSerializer(typeof(ExternalCheck));
        //    //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, null);

        //    ////XmlWriterSettings settings = new XmlWriterSettings()
        //    ////{
        //    ////    OmitXmlDeclaration = true,
        //    ////    ConformanceLevel = ConformanceLevel.Fragment,
        //    ////    CloseOutput = false
        //    ////};



        //    //xs.Serialize(xmlTextWriter, extCheck);
        //    //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
        //    //UTF8Encoding encode = new UTF8Encoding();
        //    //XmlizedString = encode.GetString(memoryStream.ToArray());

        //    XmlizedString = StringXMLSerializer<ExternalCheck>.Convert2XML(extCheck);

        //    XmlDocument xmlDoc = new XmlDocument();
        //    xmlDoc.LoadXml(XmlizedString);

        //    xmlDoc.RemoveChild(xmlDoc.FirstChild);

        //    StringWriter sw = new StringWriter();
        //    XmlTextWriter tx = new XmlTextWriter(sw);
        //    xmlDoc.WriteTo(tx);

        //    XmlizedString = sw.ToString();

        //    //return "<![CDATA[" + XmlizedString + "]]>";
        //    return XmlizedString;

        //}

        private void ConstructSubLineVM(bool canRemove = true)
        {
            var package = new Package();
            var bundle = new Bundle();
            var sublineVMs = new List<SublineVM>();
            var vasNames = new List<string>();
            var selectedCompIDs = new List<int>();

            var vasIDs = Session["RegMobileSub_VasIDs"].ToString2().Split(',').ToList();
            foreach (var id in vasIDs)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    selectedCompIDs.Add(id.ToInt());
                }
            }

            using (var proxy = new SmartCatelogServiceProxy())
            {
                var bundlePackage = proxy.PgmBdlPckComponentGet(new int[] 
                    { 
                        (int)Session["RegMobileSub_PkgPgmBdlPkgCompID"] 
                    }).SingleOrDefault();

                package = proxy.PackageGet(new int[] 
                    { 
                        bundlePackage.ChildID 
                    }).SingleOrDefault();

                bundle = proxy.BundleGet(new int[] 
                    { 
                        bundlePackage.ParentID 
                    }).SingleOrDefault();

                vasNames = proxy.ComponentGet(
                    proxy.PgmBdlPckComponentGet
                    (
                        selectedCompIDs
                    ).Select(a => a.ChildID)
                ).Select(a => a.Name).ToList();
            }

            var sessionSublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
            if (sessionSublines != null)
            {
                if (sessionSublines.Count() > 0)
                    sublineVMs = sessionSublines;
            }

            sublineVMs.Add(new SublineVM()
            {
                CanRemove = canRemove,
                SequenceNo = sublineVMs.Count() + 1,
                PkgPgmBdlPkgCompID = (int)Session["RegMobileSub_PkgPgmBdlPkgCompID"],
                SelectedMobileNos = (List<string>)Session["RegMobileSub_MobileNo"],
                SelectedVasIDs = selectedCompIDs,
                SelectedVasNames = vasNames,
                BundleName = bundle.Name,
                PackageName = package.Name
            });

            Session[SessionKey.RegMobileReg_SublineVM.ToString()] = sublineVMs;
        }

        private void ClearSubLineSession()
        {
            Session["RegMobileSub_PkgPgmBdlPkgCompID"] = null;
            Session["RegMobileSub_MobileNo"] = null;
            Session["RegMobileSub_VasIDs"] = null;
            Session["RegMobileReg_SublineSummary"] = null;
        }

        //Unused Methods
        //private MobileNoUpdateRequest ConstructMobileNoUpdate()
        //{
        //    var msisdn = ((List<string>) Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2();
        //    var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
        //    //Changed by chetan for dynamicaly sending data for update mobile no depending on user details
        //    string state = Util.GetNameByID(RefType.State, Util.SessionAccess.User.Org.StateID);
        //    return new MobileNoUpdateRequest()
        //    {
        //        MSISDN = msisdn,
        //        //DealerCode = "2083.00001",
        //        DealerCode = Util.SessionAccess.User.Org.DealerCode,
        //        //DealerName = "Allied Marketing & Trading Company", 
        //        DealerName = Util.SessionAccess.User.Org.Name,
        //        DealerCode2 = "",
        //        //DealerName2 = "Allied Marketing & Trading Company",
        //        DealerName2 = Util.SessionAccess.User.Org.Name,
        //        Name = personalDetailsVM.Customer.FullName,
        //        ContactNo = personalDetailsVM.Customer.ContactNo,
        //        ICNo = personalDetailsVM.Customer.IDCardNo,
        //        RegType = "N", //mapping
        //        Account = "", //mapping
        //        //MaxisCenter = "Alor Setar", //mapping
        //        MaxisCenter = Util.SessionAccess.User.Org.Code,//mapping
        //        //State = "SARAWAK", //mapping
        //        State = state,//mapping
        //        SelectFlag = "Y",
        //        SelectDate = DateTime.Now,
        //    };
        //}

        private void ConstructGuidedSalesDetails()
        {
            Session[SessionKey.MenuType.ToString()] = MenuType.MobileRegistration.ToInt();
            Session[SessionKey.RegMobileReg_Type.ToString()] = Session["GuidedSales_Type"].ToInt();
            Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = MobileRegDeviceOption.Brand.ToInt();
            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = Session["GuidedSales_SelectedBrandID"].ToInt();
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = Session["GuidedSales_SelectedModelImageID"].ToInt();
            Session["RegMobileReg_SelectedModelID"] = Session["GuidedSales_SelectedModelID"].ToInt();
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = Session["GuidedSales_PkgPgmBdlPkgCompID"].ToInt();

            using (var proxy = new SmartCatelogServiceProxy())
            {
                var pbpc = proxy.PgmBdlPckComponentGet(new int[] { Session["GuidedSales_PkgPgmBdlPkgCompID"].ToInt() }).SingleOrDefault();

                var programID = proxy.PgmBdlPckComponentGet(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        ChildID = pbpc.ParentID,
                        LinkType = Properties.Settings.Default.Program_Bundle
                    },
                    Active = true
                })).SingleOrDefault().ParentID;

                var minAge = proxy.ProgramGet(new int[] { programID }).SingleOrDefault().MinAge;
                Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = minAge;
            }

            ClearGuidedSalesSession();
        }

        private void ClearGuidedSalesSession()
        {
            Session["GuidedSales_PkgPgmBdlPkgCompID"] = null;
            Session["GuidedSales_SelectedModelImageID"] = null;
            Session["GuidedSales_SelectedModelID"] = null;
            Session["GuidedSales_SelectedBrandID"] = null;
            Session["GuidedSales_Type"] = null;
        }

        private void ResetAllSession(int? type)
        {
            Session["RegMobileReg_FromSearch"] = null;
            Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
            ClearRegistrationSession();

            if (type == (int)MobileRegType.DevicePlan)
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
            else if (type == (int)MobileRegType.PlanOnly)
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
            else if (type == (int)MobileRegType.DeviceOnly)
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DeviceOnly;

            // From guided sales
            if (Session["GuidedSales_SelectedModelImageID"].ToInt() != 0 && Session["GuidedSales_PkgPgmBdlPkgCompID"].ToInt() != 0)
                ConstructGuidedSalesDetails();
            else
                ClearGuidedSalesSession();
        }
        private OrderSummaryVM ConstructOrderSummary(bool fromSubline = false, bool extendContractCheck = false)
        {
            string strDataPlanID;
            string strContractId;
            var components = new List<Component>();
            /*Commented for articleid*/
            //var modelImage = new ModelImage();
            var brandArticleImage = new BrandArticleSmart();
            var package = new Package();
            var bundle = new Bundle();
            var orderVM = new OrderSummaryVM();
            var phoneVM = (PhoneVM)Session["RegMobileReg_PhoneVM"];
            int varDataPlanId = 0;
            var personalDetailsVM = Session["RegMobileReg_PersonalDetailsVM"] == null ? new PersonalDetailsVM() : (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            // Main Line
            //if (!fromSubline)
            //{
            if ((OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()] != null)
                orderVM = (OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()];
            //added by deepika 10 march
            Session["ModelID"] = orderVM.ModelID;
            if (Session["RegMobileReg_DataPkgID"] != null)
            {

                strDataPlanID = Session["RegMobileReg_DataPkgID"].ToString();
                // if(strDataPlanID.

                if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] != null) && (Session[SessionKey.MobileRegType.ToString()].ToString() != "Planonly"))
                {
                    strContractId = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString();

                    using (var proxys = new SmartCatelogServiceProxy())

                        varDataPlanId = proxys.GettllnkContractDataPlan(Convert.ToInt32(strContractId));

                    Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = "0";
                    Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = "0";
                    Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = "0";
                    Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = "0";
                    Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = "0";
                    Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = "0";
                    Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = "0";
                    Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = "0";
                    ////added by deepika 10 march
                    //if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID != 0 && strContractId.ToInt() != 0 && varDataPlanId.ToInt() != 0 || varDataPlanId.ToInt() == 0)
                    //{
                    //    using (var proxy = new SmartCatelogServiceProxy())
                    //    {
                    //        List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
                    //        varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, strContractId.ToInt(), varDataPlanId.ToInt() }).ToList();
                    //        for (int i = 0; i < varAdvDeposit.Count; i++)
                    //            if (varAdvDeposit[i] != null && varAdvDeposit[i].modelId != 0)
                    //            {
                    //                var Malaytotalprice = varAdvDeposit[i].malyDevAdv + varAdvDeposit[i].malayPlanAdv + varAdvDeposit[i].malayDevDeposit + varAdvDeposit[i].malyPlanDeposit;
                    //                var Othertotalprice = varAdvDeposit[i].othDevAdv + varAdvDeposit[i].othPlanAdv + varAdvDeposit[i].othDevDeposit + varAdvDeposit[i].othPlanDeposit;
                    //                orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
                    //                orderVM.MalayAdvDevPrice = Malaytotalprice;
                    //                orderVM.OthersAdvDevPrice = Othertotalprice;
                    //                Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                    //                Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

                    //                /*Added by chetan for split adv & deposit*/
                    //                orderVM.MalyDevAdv = varAdvDeposit[i].malyDevAdv;
                    //                orderVM.MalayPlanAdv = varAdvDeposit[i].malayPlanAdv;
                    //                orderVM.MalayDevDeposit = varAdvDeposit[i].malayDevDeposit;
                    //                orderVM.MalyPlanDeposit = varAdvDeposit[i].malyPlanDeposit;
                    //                orderVM.OthDevAdv = varAdvDeposit[i].othDevAdv;
                    //                orderVM.OthPlanAdv = varAdvDeposit[i].othPlanAdv;
                    //                orderVM.OthDevDeposit = varAdvDeposit[i].othDevDeposit;
                    //                orderVM.OthPlanDeposit = varAdvDeposit[i].othPlanDeposit;

                    //                Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                    //                 Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                    //                Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                    //               Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                    //                Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                    //                Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                    //                Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                    //                Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                    //                /*up to here*/

                    //                if (personalDetailsVM.Customer.NationalityID != 0)
                    //                {
                    //                    if (personalDetailsVM.Customer.NationalityID == 1)
                    //                    {
                    //                        /*Added by chetan for split adv & deposit*/
                    //                        Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                    //                         Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                    //                        Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                    //                       Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                    //                        /*up to here*/
                    //                        Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                    //                        orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ModelPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
                    //                    }
                    //                    else
                    //                    {
                    //                        /*Added by chetan for split adv & deposit*/
                    //                        Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                    //                        Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                    //                        Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                    //                        Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                    //                        /*up to here*/
                    //                        Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
                    //                        orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ModelPrice != 0 ? orderVM.ModelPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
                    //                    }
                    //                }
                    //            }
                    //    }
                    //}

                }
                else if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] == null) && (Session[SessionKey.MobileRegType.ToString()].ToString2().ToUpper() == "PLANONLY"))
                {
                    //added by deepika 10 march
                    if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID == 0 && orderVM.ContractID == 0)
                    {
                        using (var proxy = new SmartCatelogServiceProxy())
                        {
                            Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = "0";
                        }
                    }

                }
            }
            else
            {
                strDataPlanID = "0";
                // if(strDataPlanID.

                if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] != null) && (Session[SessionKey.MobileRegType.ToString()] != "Planonly"))
                {
                    strContractId = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString();

                    using (var proxys = new SmartCatelogServiceProxy())

                        varDataPlanId = proxys.GettllnkContractDataPlan(Convert.ToInt32(strContractId));


                    //added by deepika 10 march
                    if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID != 0 && strContractId.ToInt() != 0 && varDataPlanId.ToInt() != 0 || varDataPlanId.ToInt() == 0)
                    {
                        using (var proxy = new SmartCatelogServiceProxy())
                        {
                            List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
                            Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = "0";
                            Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = "0";
                        }
                    }

                }
            }
            // END Deepika


            // Device Details
            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan || Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
            {
                //if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt()) // go to credit check
                //{
                //    Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = Session["GuidedSales_SelectedModelImageID"];
                //}
                if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt() != orderVM.SelectedModelImageID)
                {

                    orderVM.SelectedModelImageID = Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt();

                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        /*Commented for articleid*/
                        //modelImage = proxy.ModelImageGet(orderVM.SelectedModelImageID);
                        brandArticleImage = proxy.BrandArticleImageGet(orderVM.SelectedModelImageID);
                    }

                    /*Commented for articleid*/
                    //Session["RegMobileReg_SelectedModelID"] = modelImage.ModelID;
                    Session["RegMobileReg_SelectedModelID"] = brandArticleImage.ModelID;

                    var model = new ModelSmart();
                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        /*Commented for articleid*/
                        //model = proxy.ModelGet(modelImage.ModelID);
                        //model = proxy.ModelGet(brandArticleImage.ModelID);
                        List<int> modelids = new List<int>();
                        modelids.Add(brandArticleImage.ModelID);
                        model = proxy.ModelGetSmart(modelids).FirstOrDefault();
                    }

                    /*Commented for articleid*/
                    //orderVM.ModelImageUrl = modelImage.ImagePath;
                    orderVM.ModelImageUrl = brandArticleImage.ImagePath;
                    /*Commented for articleid*/
                    orderVM.ModelID = model.ID;
                    orderVM.ModelName = model.Name;
                    /*Commented for articleid*/
                    //orderVM.ColourName = modelImage.ColourID.HasValue ? Util.GetNameByID(RefType.Colour, modelImage.ColourID.Value) : "";
                    orderVM.ColourName = brandArticleImage.ColourID > 0 ? Util.GetNameByID(RefType.Colour, brandArticleImage.ColourID) : "";
                    orderVM.BrandID = model.BrandID;
                    orderVM.BrandName = Util.GetNameByID(RefType.Brand, model.BrandID);
                    orderVM.ArticleId = brandArticleImage.ArticleID.ToString().Trim();
                    Session[SessionKey.ArticleId.ToString()] = orderVM.ArticleId.ToString().Trim();
                    Session["RegMobileReg_RRPDevicePrice"] = model.RetailPrice.ToInt();
                    if (personalDetailsVM.RegID > 0)
                    {
                        orderVM.TotalPrice = Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()].ToDecimal();
                        orderVM.ModelPrice = Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()].ToDecimal();
                        orderVM.ItemPrice = Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()].ToInt();
                    }
                    else
                    {
                        orderVM.DevicePriceType = DevicePriceType.RetailPrice;
                        orderVM.TotalPrice -= orderVM.ModelPrice;
                        orderVM.TotalPrice += model.RetailPrice.ToInt();
                        orderVM.ModelPrice = model.RetailPrice.ToInt();

                        Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = orderVM.ModelPrice;
                    }
                }
            }
            //else //Sub Line
            //{
            //    if ((OrderSummaryVM)Session["RegMobileReg_SublineSummary"] != null)
            //        orderVM = (OrderSummaryVM)Session["RegMobileReg_SublineSummary"];
            //}

            //Package Details
            var sessionPkgPgmBdlPkgCompID = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(); //Main or Sub Line
            if (sessionPkgPgmBdlPkgCompID != orderVM.SelectedPgmBdlPkgCompID)
            {
                var bundlePackage = new PgmBdlPckComponentSmart();
                orderVM.SelectedPgmBdlPkgCompID = sessionPkgPgmBdlPkgCompID;

                // Package Details
                if (sessionPkgPgmBdlPkgCompID != 0)
                {
                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        bundlePackage = proxy.PgmBdlPckComponentGetSmart(new int[] 
                    { 
                        sessionPkgPgmBdlPkgCompID 
                    }).SingleOrDefault();

                        package = proxy.PackageGet(new int[] 
                    { 
                        bundlePackage.ChildID 
                    }).SingleOrDefault();

                        bundle = proxy.BundleGet(new int[] 
                    { 
                        bundlePackage.ParentID 
                    }).SingleOrDefault();
                    }
                }

                orderVM.PlanBundle = bundle.Name;
                orderVM.PlanName = package.Name;

                // Plan Monthly Subscription
                if (orderVM.MonthlySubscription != bundlePackage.Price)
                {
                    orderVM.MonthlySubscription = bundlePackage.Price;
                }

                // Mandatory Package Components
                if (sessionPkgPgmBdlPkgCompID > 0)
                    orderVM.VasVM.MandatoryVASes = GetAvailablePackageComponents(orderVM.SelectedPgmBdlPkgCompID, isMandatory: true, ExtendContractCheck: extendContractCheck).MandatoryVASes;

                else
                    orderVM.VasVM = new ValueAddedServicesVM();
            }

            //Start Contract Details added By Raj
            List<RegSmartComponents> listRegSmartComponents = new List<RegSmartComponents>();
            var selectedCompIDs = new List<int>();
            if (Session["_componentViewModel"] != null)
            {
                // 12345
                ComponentViewModel _component = (ComponentViewModel)Session["_componentViewModel"];
                RegSmartComponents objRegSmartComponents;
                if (_component.NewComponentsList.Count > 0)
                {
                    foreach (var item in _component.NewComponentsList)
                    {
                        if (item.IsChecked || item.ReassignIsmandatory)
                        {
                            objRegSmartComponents = new RegSmartComponents();
                            objRegSmartComponents.PackageID = item.NewPackageId;
                            objRegSmartComponents.ComponentID = item.NewComponentId;
                            objRegSmartComponents.ComponentDesc = item.NewComponentName;
                            listRegSmartComponents.Add(objRegSmartComponents);
                        }

                    }
                    orderVM.SelectedComponets = listRegSmartComponents;

                    #region "VAS Dependency Components"
                    if (_component.DependentVASList != null && _component.DependentVASList.Count > 0)
                    {
                        foreach (var item1 in _component.DependentVASList)
                        {
                            objRegSmartComponents = new RegSmartComponents();
                            objRegSmartComponents.PackageID = item1.NewPackageId;
                            objRegSmartComponents.ComponentID = item1.NewComponentId;
                            objRegSmartComponents.ComponentDesc = item1.NewComponentName;
                            listRegSmartComponents.Add(objRegSmartComponents);
                        }

                    }


                    List<int> depCompIds = new List<int>();
                    foreach (var id in orderVM.SelectedComponets)
                    {
                        //TODO:need to add instead of reset
                        //URgent: need to add instead of reset
                        //depCompIds = GetDepenedencyComponents(id.ComponentID);

                        //modified by KiranG for adding items into list previously its overriding.
                        List<int> ldepCompId = GetDepenedencyComponents(id.ComponentID);
                        foreach (int lid in ldepCompId)
                        {
                            depCompIds.Add(lid);
                        }
                    }
                    var pgmBdlPckComponents = new List<PgmBdlPckComponentSmart>();
                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        if (depCompIds != null && depCompIds.Count > 0)
                        {
                            foreach (var depComp in depCompIds)
                            {
                                List<Component> components1 = new List<Component>();
                                pgmBdlPckComponents = proxy.PgmBdlPckComponentGetSmart(depCompIds).ToList();
                                components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID)).ToList();
                                if (components != null && components.Count > 0)
                                {
                                    objRegSmartComponents = new RegSmartComponents();
                                    //TODO: need to add kenan code of package also
                                    objRegSmartComponents.PackageID = "";
                                    objRegSmartComponents.ComponentID = pgmBdlPckComponents[0].KenanCode;
                                    objRegSmartComponents.ComponentDesc = components[0].Description + " (RM" + pgmBdlPckComponents[0].Price.ToString() + ")";

                                    listRegSmartComponents.Add(objRegSmartComponents);

                                }
                            }

                        }
                    }
                    #endregion


                    List<string> Cmpnts = orderVM.SelectedComponets.Select(a => a.ComponentDesc).ToList();
                    if (Cmpnts != null && Cmpnts.Any())
                        Session["RegMobileReg_SelectedContracts"] = Cmpnts.Distinct().ToList();
                }
            }

            else
            {
                // 12345

                listRegSmartComponents = GetSelectedComponents(personalDetailsVM.RegID.ToString2());
                if (listRegSmartComponents.Count > 0)
                {
                    List<string> Cmpnts = listRegSmartComponents.Select(a => a.ComponentDesc).ToList();
                    if (Cmpnts != null && Cmpnts.Any())
                        Session["RegMobileReg_SelectedContracts"] = Cmpnts.Distinct().ToList();
                }
                orderVM.SelectedComponets = listRegSmartComponents;

                List<string> compkenancodes = listRegSmartComponents.Select(a => a.ComponentID).ToList();

                string strNewPackageId = orderVM.SelectedPgmBdlPkgCompID.ToString2();
                ComponentViewModel Componentresponse = GetSelectedPackageInfo_smart(0, strNewPackageId.ToInt(), "");

                if (Componentresponse.NewComponentsList.Count > 0)
                {
                    selectedCompIDs = new List<int>();
                    int cnt = 0;
                    for (cnt = 0; cnt < Componentresponse.NewComponentsList.Count; cnt++)
                    {
                        if (compkenancodes.Contains(Componentresponse.NewComponentsList[cnt].NewComponentKenanCode))
                            selectedCompIDs.Add(Componentresponse.NewComponentsList[cnt].NewComponentId.ToInt());
                    }
                }
            }
            //

            //End Contract Details added By Raj


            //VAS details
            //if (!fromSubline)
            //{
            var selectedVasIDs = Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2(); //Main or Sub Line
            // var selectedCompIDs = new List<int>();

            // Mandatory VASes
            if (orderVM.VasVM.MandatoryVASes.Count() > 0)
            {
                selectedCompIDs = orderVM.VasVM.MandatoryVASes.Select(a => a.PgmBdlPkgCompID).ToList();
            }
            //As per VAS retention we have a chance to get more VAS componensts based on selection
            if (orderVM.SelectedComponets.Count() > 0)
                selectedCompIDs = orderVM.SelectedComponets.Select(sc => Convert.ToInt32(sc.ComponentID)).ToList();

            if (orderVM.VasVM.SelectedVasIDs != selectedVasIDs || orderVM.VasVM.MandatoryVASes.Count() > 0)
            {
                //TODO: if we need to consider price maintaining @ plan level also then we need comment this line to reset
                orderVM.MonthlySubscription = 0;
                decimal vasPrice = 0;
                if ((Session[SessionKey.RegMobileReg_ContractID.ToString()]).ToString2() != string.Empty)
                    orderVM.ContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt();
                if ((Session[SessionKey.RegMobileReg_DataplanID.ToString()]).ToString2() != string.Empty)
                    orderVM.DataPlanID = Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt();
                orderVM.VasVM.SelectedVasIDs = selectedVasIDs;


                var vasIDs = orderVM.VasVM.SelectedVasIDs.Split(',').ToList();
                foreach (var id in vasIDs)
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        selectedCompIDs.Add(id.ToInt());
                    }
                }
                List<int> depCompIds = new List<int>();
                foreach (var id in selectedCompIDs)
                {
                    if (id > 0)
                    {

                        depCompIds.AddRange(GetDepenedencyComponents(id.ToString()));
                        // selectedCompIDs.Add(id.ToInt());
                    }
                }
                foreach (var id in depCompIds)
                {
                    if (id > 0)
                    {
                        selectedCompIDs.Add(id.ToInt());
                    }

                }
                if (Session[SessionKey.RegMobileReg_DataplanID.ToString()] != null && Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt() != 0)
                {

                    selectedCompIDs.Add(Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt());
                    //TODO: need to fix whether we need to pass data planid to kenan or not
                    //Urgent: 
                    //var bundlePackages = new List<PgmBdlPckComponentSmart>();
                    //using (var proxySmartCatelogServiceProxy = new SmartCatelogServiceProxy())
                    //{
                    //    bundlePackages = proxySmartCatelogServiceProxy.PgmBdlPckComponentGetForSmartByKenanCode(new List<string>() { Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToString() }).Where(a => a.Active == true).ToList();

                    //    if (bundlePackages != null && bundlePackages.Count > 0)
                    //    {
                    //        RegSmartComponents objRegSmartComponents = new RegSmartComponents();
                    //        objRegSmartComponents.PackageID = bundlePackages[0].KenanCode;
                    //        objRegSmartComponents.ComponentID = bundlePackages[0].KenanCode;
                    //        objRegSmartComponents.ComponentDesc = bundlePackages[0].Name;
                    //        listRegSmartComponents.Add(objRegSmartComponents);
                    //    }
                    //}

                    //orderVM.SelectedComponets = listRegSmartComponents;
                    //List<string> Cmpnts = orderVM.SelectedComponets.Select(a => a.ComponentDesc).ToList();
                    //Session["RegMobileReg_SelectedContracts"] = Cmpnts;
                }


                if (selectedCompIDs != null)
                {
                    if (selectedCompIDs.Count() > 0)
                    {
                        using (var proxy = new SmartCatelogServiceProxy())
                        {
                            var vases = proxy.PgmBdlPckComponentGetSmart
                               (
                                   selectedCompIDs
                               ).ToList();

                            components = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();


                            //orderVM.VasVM.VASesName =  vases.Select(a => a.Name + " (RM" + a.Price + ")").ToList();
                            var query = from selectedVases in vases
                                        join comps in components on
                                            selectedVases.ChildID equals comps.ID
                                        select new { name = comps.Name + " (RM" + selectedVases.Price + ")" };

                            orderVM.VasVM.VASesName = query.Select(a => a.name).ToList();

                            /*Added by chetan for vasplan price adding*/

                            if ((!string.IsNullOrEmpty(selectedVasIDs)) || (orderVM.VasVM.SelectedVasIDs != selectedVasIDs) || (orderVM.VasVM.VASesName.Count > 0))
                            {
                                var query1 = from selectedVases in vases
                                             join comps in components on
                                                selectedVases.ChildID equals comps.ID
                                             select new { selectedVases.Price };
                                if (query1 != null)
                                {
                                    orderVM.MonthlySubscription2 = query1.Select(b => b.Price).ToList();
                                    for (int i = 0; i < orderVM.MonthlySubscription2.Count; i++)
                                    {
                                        vasPrice += orderVM.MonthlySubscription2[i];
                                    }
                                }

                                //orderVM.MonthlySubscription += vasPrice;
                            }

                            /*Upto here*/
                        }
                    }
                }

                orderVM.MonthlySubscription += vasPrice;
                //Session[SessionKey.RegMobileReg_VasNames.ToString()] = orderVM.VasVM.VASesName;
                if (orderVM.VasVM != null && orderVM.VasVM.VASesName != null && orderVM.VasVM.VASesName.Any())
                {
                    Session["RegMobileReg_VasNames"] = orderVM.VasVM.VASesName.Distinct().ToList();
                }
            }
            //}

            // Advance Payment/ Deposite
            if (orderVM.Deposite != personalDetailsVM.Deposite)
            {
                orderVM.TotalPrice -= orderVM.Deposite;
                orderVM.TotalPrice += personalDetailsVM.Deposite;

                orderVM.Deposite = personalDetailsVM.Deposite;
            }


            // Device Price (cheaper)
            if (Session[SessionKey.RegMobileReg_ContractID.ToString()] != null && orderVM.SelectedPgmBdlPkgCompID.ToInt() != 0 && orderVM.ModelID != 0)
            {
                if (orderVM.ItemPrice != orderVM.ModelPrice)
                {
                    var itemPrice = new ItemPrice();
                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        var itemPriceID = proxy.ItemPriceFind(new ItemPriceFind()
                        {
                            ItemPrice = new ItemPrice()
                            {
                                BdlPkgID = orderVM.SelectedPgmBdlPkgCompID.ToInt(),
                                PkgCompID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt(),
                                ModelID = orderVM.ModelID,
                                pkgDataID = Session["RegMobileReg_DataPkgID"].ToInt()
                            },
                            Active = true
                        }).SingleOrDefault();

                        if (personalDetailsVM.RegID == 0)
                        {
                            if (itemPriceID.ToInt() != 0) // item price configured
                            {
                                itemPrice = proxy.ItemPriceGet(itemPriceID);

                                if (orderVM.DevicePriceType == DevicePriceType.RetailPrice)
                                {
                                    orderVM.TotalPrice -= orderVM.ModelPrice;
                                }
                                else if (orderVM.DevicePriceType == DevicePriceType.ItemPrice)
                                {
                                    orderVM.TotalPrice -= orderVM.ItemPrice;
                                }

                                if (itemPrice.Price == 0) // RM 0 price for device
                                {
                                    orderVM.DevicePriceType = DevicePriceType.NoPrice;
                                }
                                else // cheaper price for device
                                {
                                    orderVM.DevicePriceType = DevicePriceType.ItemPrice;
                                }

                                orderVM.TotalPrice += itemPrice.Price;
                                orderVM.ItemPrice = itemPrice.Price.ToInt();

                                Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = orderVM.ItemPrice.ToDecimal();
                            }
                            else // retail price
                            {
                                if (orderVM.DevicePriceType == DevicePriceType.ItemPrice)
                                {
                                    orderVM.TotalPrice -= orderVM.ItemPrice;
                                    //orderVM.ItemPrice = orderVM.ModelPrice.ToInt();
                                    orderVM.TotalPrice += orderVM.ModelPrice;
                                    orderVM.DevicePriceType = DevicePriceType.RetailPrice;

                                    Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = orderVM.ModelPrice;
                                }
                            }
                        }
                    }
                }
            }
            else //(retail price)
            {
                if (personalDetailsVM.RegID == 0)
                {
                    if (orderVM.DevicePriceType != DevicePriceType.RetailPrice)
                    {
                        orderVM.TotalPrice -= orderVM.ItemPrice;
                        orderVM.ItemPrice = 0;
                        orderVM.TotalPrice += orderVM.ItemPrice;
                    }
                }
            }
            //added by KD for penalty
            if (Session["Penalty"] != null)
                orderVM.PenaltyAmount = Session["Penalty"].ToDecimal();
            orderVM.TotalPrice = orderVM.TotalPrice + orderVM.PenaltyAmount;

            // MobileNo
            var mobileNos = fromSubline ? (List<string>)Session["RegMobileSub_MobileNo"] : (List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()]; // Main or Sub Line
            orderVM.MobileNumbers = mobileNos == null ? new List<string>() : mobileNos;

            if (!fromSubline) //main line
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderVM;
            else //sub line
            {
                Session["RegMobileReg_SublineSummary"] = orderVM;
                orderVM.fromSubline = true;
            }

            // Subline
            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
            if (sublines != null)
                orderVM.Sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];

            return orderVM;
        }


        private void GetPersonalInformationOnMSISDN(string MSISDN)
        {
            string ExceptionFromService = string.Empty;
            #region VLT ADDED CODE
            string IDCardTypeID = string.Empty;
            string IDCardNo = string.Empty;
            retrieveAcctListByICResponse AcctListByICResponse = null;
            string ExternalId_Msisdn = MSISDN;
            string SubscriberNo = string.Empty;
            string SubscriberNoResets = string.Empty;
            //retrieveServiceInfoResponse ServiceInfoResponse = null;
            SubscriberRetrieveServiceInfoResponse ServiceInfoResponse = null;
            CustomizedCustomer customer = null;
            //getPrinSuppRequest request = null;
            retrievegetPrinSuppResponse response = null;
            PrinSupEaiHeader header = null;
            CustomizedAccount IndividualAccount = null;
            CustomizedBillingInfo IndividualBillingInfo = null;
            #endregion VLT ADDED CODE


            #region Added by KDPrasad to support MSISDN in PPID

            using (var proxy = new retrieveSmartServiceInfoProxy())
            {
                //customer = Util.retrieveSubscriberDetls(MSISDN, userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
                //                password: Properties.Settings.Default.retrieveAcctListByICResponsePassword, postUrl: Properties.Settings.Default.retrieveAcctListByICPostUrl);
                customer = proxy.retrieveSubscriberDetls(Msisdn: ExternalId_Msisdn, loggedUserName: Request.Cookies["CookieUser"].Value, serviceException: ref ExceptionFromService);
            }
            if (customer != null)
            {
                Session[SessionKey.CustomerInformation.ToString()] = customer;
                if (!string.IsNullOrEmpty(customer.PassportNo))
                {
                    IDCardNo = customer.PassportNo;
                    IDCardTypeID = "PASSPORT";
                    Session[SessionKey.IDCardTypeID.ToString()] = 2;
                }
                else if (!string.IsNullOrEmpty(customer.NewIC))
                {
                    IDCardNo = customer.NewIC;
                    IDCardTypeID = "NEWIC";
                    Session[SessionKey.IDCardTypeID.ToString()] = 1;
                }
                else if (!string.IsNullOrEmpty(customer.OldIC))
                {
                    IDCardNo = customer.OldIC;
                    IDCardTypeID = "OLDIC";
                    Session[SessionKey.IDCardTypeID.ToString()] = 3;
                }
                else if (!string.IsNullOrEmpty(customer.OtherIC))
                {
                    IDCardNo = customer.OtherIC;
                    IDCardTypeID = "OTHERIC";
                    Session[SessionKey.IDCardTypeID.ToString()] = 4;
                }
                //else if (!string.IsNullOrEmpty(customer.Brn))
                //{

                //}

                Session[SessionKey.IDCardNo.ToString()] = IDCardNo;
            }


            #endregion
            var proxy_smart = new SubscriberICService.SubscriberICServiceClient();
            //AcctListByICResponse = proxy_smart.retrieveAcctListByIC(icType: IDCardTypeID, icValue: IDCardNo,"","");
            string exception = "";
            AcctListByICResponse = proxy_smart.retrieveAcctListByIC(IDCardTypeID, Session[SessionKey.IDCardNo.ToString()].ToString(), Request.Cookies["CookieUser"].Value, ref exception);

            ///If AcctListByICResponse is null then treat the user as new
            ///set the session to new
            ///added by chodey for out standing check
            if (AcctListByICResponse != null)
            {
                if (AcctListByICResponse.itemList[0].AcctExtId != null)
                {
                    Session[SessionKey.AcctExtId.ToString()] = AcctListByICResponse.itemList[0].AcctExtId.ToString();
                    //session added by Rajeswari
                    Session[SessionKey.AccountsExt.ToString()] = AcctListByICResponse;

                }
                else
                {
                    Session[SessionKey.AcctExtId.ToString()] = string.Empty;
                }
            }

            if (ReferenceEquals(AcctListByICResponse, null))
            {
                ///New user
                Session[SessionKey.PPID.ToString()] = "N";

                ///Return no further checking required

                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null) && !string.IsNullOrWhiteSpace(Convert.ToString(Session[SessionKey.RedirectURL.ToString()])))
                {
                    string redirectUrl, controllerName, actionName = string.Empty;
                    try
                    {
                        redirectUrl = Convert.ToString(Session[SessionKey.RedirectURL.ToString()]);
                        string[] urlParts = redirectUrl.Split('/');

                        controllerName = urlParts[0];
                        actionName = urlParts[1];

                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.MobileRegistration;

                        WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");

                    }
                    catch
                    {
                        throw;
                    }
                }

            }
            else
            {
                ///Proceed with remaining checks
                ///Loop through pass MSISDN and pass to retrieveServiceInfo which will provide a/c type for each msisdn.                                                
                retrieveServiceInfoProxy.counter = 0;//Applicable to Properties.Settings.Default.DevelopmentModeOnForPpidChecking=true mode only. Will not affect the live mode.
                using (var proxy = new retrieveSmartServiceInfoProxy())
                {
                    foreach (var v in AcctListByICResponse.itemList)
                    {
                        try
                        {
                            ExternalId_Msisdn = v.ExternalId;
                            //Added by Chodey
                            SubscriberNo = v.SusbcrNo;
                            SubscriberNoResets = v.SusbcrNoResets;
                            if (!string.IsNullOrWhiteSpace(ExternalId_Msisdn))
                            {
                                ServiceInfoResponse = proxy.retrieveServiceInfo(new SubscriberRetrieveServiceInfoRequest
                                {
                                    externalId = ExternalId_Msisdn,
                                    //added by chodey
                                    subscrNo = SubscriberNo,
                                    subscrNoResets = SubscriberNoResets
                                });

                                if (!ReferenceEquals(ServiceInfoResponse, null))
                                {
                                    if (ServiceInfoResponse.msgCode == "0")
                                    {
                                        ///Valid Subscriber so add the response with the current MSISDN
                                        v.ServiceInfoResponse = ServiceInfoResponse;
                                    }
                                    else
                                    {
                                        ///Invalid subscriber set it to null no a/c found for the MSISDN
                                        v.ServiceInfoResponse = null;
                                    }
                                }
                                else
                                {
                                    ///EXCEPTION OCCURED AT SERVICE LEVEL DUE TO TECHNICAL PROBLEM
                                    v.ServiceInfoResponse = null;
                                }

                                // Added by Patanjali on 12-3-2013 to support Logging
                                Logger.Info(string.Format(" Request: retrieveServiceInfo --> externalId :{0} , subscrNo :{1} , subscrNoResets: {2} ", ExternalId_Msisdn, SubscriberNo, SubscriberNoResets));
                                if (v.ServiceInfoResponse != null)
                                {

                                    Logger.Info(string.Format(" Response: retrieveServiceInfo --> Code :{0} , Message :{1}  ", ServiceInfoResponse.msgCode, ServiceInfoResponse.msgDesc));
                                }
                                else
                                {

                                    Logger.Info("Response: retrieveServiceInfo: No Response Recieved ");

                                }
                                // Added by Patanjali on 12-3-2013 to support Logging Ends here
                            }
                            else
                            {
                                ///Invalid subscriber set it to null no a/c found for the MSISDN
                                v.ServiceInfoResponse = null;
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }

                ///Check now whether ServiceInfoResponse corresponding to EACH MSISDN is null or not
                ///IF ALL NULL THEN TREAT AND SET THE USER TO NEW USER
                ///IF NOT NULL THEN CHECK FOR ANY GSM A/C FOUND FOR ANY MSISDN IF FOUND THEN TREAT THE USER 
                ///AS MAXIS USER AND UPDATE THE RESPECTIVE SESSION VALUES.

                //ApiMessages.tblAPIStatusMessageslst.SingleOrDefault(c => c.StatusCode == "AS").Description.ToString()
                //AcctListByICResponse.itemList.SingleOrDefault(c=>c

                ///1. Is all ServiceInfoResponse = null? that is there are no gsm nor any non gsm connections
                if (AcctListByICResponse.itemList.Exists(c => c.ServiceInfoResponse != null))
                {
                    ExternalId_Msisdn = string.Empty;
                    //int consumer_count = 0;
                    //int corp_count = 0;
                    string msisdn = string.Empty;
                    ///Yes there are 
                    ///As per patanjali
                    ///After this, if the user is having any accounts, then we will fetch the user information from the accounts. 
                    ///For this current phase, if any GSM account is available then we will use the first GSM account to fetch user information. 
                    ///Else we have to use first account to fetch the user information.
                    if (AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse != null).ToList().Exists(c => c.ServiceInfoResponse.lob == "POSTGSM"))
                    {
                        ///GET THE EXTERNAL ID OR MSISDN FOR THE FIRST GSM A/C IF AVAILABLE
                        if (AcctListByICResponse.itemList != null)
                        {

                            for (int i = 0; i < AcctListByICResponse.itemList.Count; i++)
                            {
                                if (AcctListByICResponse.itemList[i].ServiceInfoResponse != null)
                                {
                                    if (AcctListByICResponse.itemList[i].ServiceInfoResponse.lob == "POSTGSM")
                                    {
                                        //if (AcctListByICResponse.itemList[i].ServiceInfoResponse.prinSuppInd == "P")


                                        ExternalId_Msisdn = AcctListByICResponse.itemList[i].ExternalId;
                                        break;
                                        #region Commented by chetan on 08/03/2013
                                        //var Categorytype = GetAccountinfoCategory(ExternalId_Msisdn);
                                        //if (Categorytype == "Consumer")
                                        //{
                                        //    consumer_count++;
                                        //    //break;
                                        //    if (consumer_count == 1)
                                        //    {
                                        //        msisdn = ExternalId_Msisdn;
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    corp_count++;
                                        //    ExternalId_Msisdn = string.Empty;
                                        //}
                                        #endregion Commented by chetan on 08/03/2013
                                    }

                                }
                            }
                        }
                        #region Commented by chetan on 08/03/2013
                        //if (consumer_count == 0)
                        //{
                        //     Session[SessionKey.PPID.ToString()] = "N";
                        //}
                        //else
                        //{
                        //     Session[SessionKey.PPID.ToString()] = "E";
                        //    ExternalId_Msisdn = msisdn;
                        #endregion Commented by chetan on 08/03/2013

                        ///CALL SUPPLINE AND ADD TO COLLECTION BY MSISDN FOR GSM A/C'S                                
                        PrinSupServiceProxy.counter = 0;//Applicable to Properties.Settings.Default.DevelopmentModeOnForPpidChecking=true mode only. Will not affect the live mode.

                        if (ExternalId_Msisdn.Length > 0)
                        {
                            //Commented by chetan 
                            //foreach (var v in AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse.lob == "POSTGSM").ToList())
                            //Added by chetan
                            foreach (var v in AcctListByICResponse.itemList.Where(c => (c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob == "POSTGSM")).ToList())
                            {
                                header = null;
                                //request = null;
                                response = null;
                                using (var PrinSupproxy = new SmartPrinSupServiceProxy())
                                {
                                    response = PrinSupproxy.getPrinSupplimentarylines(new SubscribergetPrinSuppRequest
                                    {
                                        eaiHeader = header,
                                        msisdn = v.ExternalId
                                    });

                                    ///SET THE CORRESPONDING PrinSuppResponse OBJECT WITH RETURNED response
                                    if (response != null && response.itemList.Count > 0)
                                    {
                                        AcctListByICResponse.itemList.Where(i => i.ExternalId == v.ExternalId).ToList().ForEach(i => i.PrinSuppResponse = response);
                                    }
                                }
                            }
                        }
                        //Commented by chetan on 08/03/2013
                        //}
                        //upto here
                        ///EXISTING USER
                        Session[SessionKey.PPID.ToString()] = "E";
                    }
                    else
                    {
                        ///EVEN IF THE USER DOES NOT HAVE ANY GSM A/C AS PER REQUIREMENT ABOVE GET THE CUSTOMER INFORMATION BASED ON NON GSM A/C
                        ExternalId_Msisdn = AcctListByICResponse.itemList.FirstOrDefault(c => (c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob != "POSTGSM")).ExternalId;
                        ///New user
                        Session[SessionKey.PPID.ToString()] = "N";
                    }
                    //GET THE ACCOUNT DETAILS AND CUSTOMER INFORMATION NOW, BEING THE BELOW MSISDN BELONGING TO GSM OR NONGSM
                    if (!string.IsNullOrWhiteSpace(ExternalId_Msisdn))
                    {
                        #region CUSTOMER INFORMATION

                        customer = null;
                        ///GET THE CUSTOMER
                        using (var proxy = new SubscriberICServiceClient())
                        {
                            // customer = proxy.retrieveSubscriberDetls(Msisdn: ExternalId_Msisdn,);
                            customer = proxy.retrieveSubscriberDetls(Msisdn: Session[SessionKey.IDCardNo.ToString()].ToString(), loggedUserName: Request.Cookies["CookieUser"].Value, isSupAdmin: false);
                        }
                        ///ATTACH THE CUSTOMER INFORMATION                                
                        AcctListByICResponse.itemList.Where(i => i.ExternalId == ExternalId_Msisdn).ToList().ForEach(i => i.Customer = customer);
                        ///SET OTHER CUSTOMER INFORMATION TO NULL
                        AcctListByICResponse.itemList.Where(i => i.ExternalId != ExternalId_Msisdn).ToList().ForEach(i => i.Customer = null);

                        #endregion CUSTOMER INFORMATION
                        #region ACCOUNT INFORMATION

                        ///GET THE ACCOUNT DETAILS WITH PAYMENT DETAILS SAME RULE LIKE CUSTOMER AS EXPLAINED BY PATANJALI OVER PHONE
                        /*Commented by chetan */
                        //IndividualAccount = PopulateAccountInfo(msisdn: ExternalId_Msisdn);
                        /*upto here*/

                        if (!ReferenceEquals(AcctListByICResponse, null))
                        {
                            Session[SessionKey.PPIDInfo.ToString()] = AcctListByICResponse;
                        }
                        IndividualAccount = PopulateAccountInfo(msisdn: ExternalId_Msisdn);


                        ///ATTACH THE ACCOUNT INFORMATION                                
                        // AcctListByICResponse.itemList.Where(i => i.ExternalId == ExternalId_Msisdn).ToList().ForEach(i => i.Account = IndividualAccount);
                        AcctListByICResponse.itemList.ToList().ForEach(i => i.Account = IndividualAccount);
                        ///SET OTHER CUSTOMER INFORMATION TO NULL THOUGH THEY WERE NEVER INITIALIZED
                        //   AcctListByICResponse.itemList.Where(i => i.ExternalId != ExternalId_Msisdn).ToList().ForEach(i => i.Account = null);
                        #endregion ACCOUNT INFORMATION
                        #region billing information
                        ///GET THE ACCOUNT DETAILS WITH PAYMENT DETAILS SAME RULE LIKE CUSTOMER AS EXPLAINED BY PATANJALI OVER PHONE
                        IndividualBillingInfo = PopulateBillingInfo(msisdn: ExternalId_Msisdn);
                        ///ATTACH THE BILLING INFORMATION
                        if (IndividualBillingInfo != null)
                        {
                            AcctListByICResponse.itemList.Where(i => i.ExternalId == ExternalId_Msisdn).ToList().ForEach(i => i.BillingInfo = IndividualBillingInfo);
                        }
                        ///SET OTHER BILLING INFORMATION TO NULL THOUGH THEY WERE NEVER INITIALIZED
                        AcctListByICResponse.itemList.Where(i => i.ExternalId != ExternalId_Msisdn).ToList().ForEach(i => i.BillingInfo = null);
                        #endregion billing information
                    }
                }
                else
                {
                    ///AS ALL ServiceInfoResponse = null
                    ///New user
                    Session[SessionKey.PPID.ToString()] = "N";
                    #region VLT ADDED CODE by Rajeswari on Feb 25th to log user PPID result

                    #endregion

                    #region VLT ADDED CODE
                    if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null) && !string.IsNullOrWhiteSpace(Convert.ToString(Session[SessionKey.RedirectURL.ToString()])))
                    {
                        //return RedirectToAction("InviteLanding", "BookMate", new { guid = Session["RequestGUID"].ToString() });
                        //GuidedSales/PlanSearch
                        //GuidedSales/DeviceSearch
                        string redirectUrl, controllerName, actionName = string.Empty;
                        try
                        {
                            redirectUrl = Convert.ToString(Session[SessionKey.RedirectURL.ToString()]);
                            string[] urlParts = redirectUrl.Split('/');

                            controllerName = urlParts[0];
                            actionName = urlParts[1];

                            Session[SessionKey.MenuType.ToString()] = (int)MenuType.MobileRegistration;

                            WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");

                        }
                        catch
                        {
                            throw;
                        }
                    }
                    #endregion VLT ADDED CODE

                    ///Return no further checking required

                }
                ///STORE AcctListByICResponse TO  Session[SessionKey.PPIDInfo.ToString()]
                if (!ReferenceEquals(AcctListByICResponse, null))
                {
                    Session[SessionKey.PPIDInfo.ToString()] = AcctListByICResponse;
                }

                ViewBag.IDCardTypeID = IDCardTypeID;
                ViewBag.IDCardNo = IDCardNo;


                string stricno = "";
                string MOCIDs = "";
                using (var MOCandWhitelistcheck = new UserServiceProxy())
                {
                    foreach (var v in AcctListByICResponse.itemList.Where(c => (c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob == "POSTGSM")).ToList())
                    {
                        if (v.ServiceInfoResponse.mnpInd.Length > 0 && v.ServiceInfoResponse.mnpInd != "N")
                        {
                            if (MOCIDs != "")
                                MOCIDs = MOCIDs + ";" + v.ServiceInfoResponse.mnpInd;
                            else
                                MOCIDs = v.ServiceInfoResponse.mnpInd; ;

                        }

                    }

                    //stricno = customer.NewIC != null ? customer.NewIC : (customer.OldIC != null) ? customer.OldIC : (customer.OtherIC != null) ? customer.OtherIC : (customer.PassportNo != null) ? customer.PassportNo : null;
                    stricno = IDCardNo;
                    bool Mocandwhitelistcuststatus = MOCandWhitelistcheck.MocICcheckwithwhitelistcustomers(Convert.ToString(Properties.Settings.Default.retrieveAcctListByICResponseUserName), stricno);
                    Session[SessionKey.Mocandwhitelistcuststatus.ToString()] = Mocandwhitelistcuststatus;
                    Session[SessionKey.MocDefault.ToString()] = Properties.Settings.Default.MOCdiscount;
                    Session[SessionKey.MocandwhitelistSstatus.ToString()] = Mocandwhitelistcuststatus;
                    Session[SessionKey.MOCIDs.ToString()] = MOCIDs;
                }
            }

            #region VLT ADDED CODE
            if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null) && !string.IsNullOrWhiteSpace(Convert.ToString(Session[SessionKey.RedirectURL.ToString()])))
            {
                //return RedirectToAction("InviteLanding", "BookMate", new { guid = Session["RequestGUID"].ToString() });
                //GuidedSales/PlanSearch
                //GuidedSales/DeviceSearch
                string redirectUrl, controllerName, actionName = string.Empty;
                try
                {
                    redirectUrl = Convert.ToString(Session[SessionKey.RedirectURL.ToString()]);
                    string[] urlParts = redirectUrl.Split('/');

                    controllerName = urlParts[0];
                    actionName = urlParts[1];

                    Session[SessionKey.MenuType.ToString()] = (int)MenuType.MobileRegistration;

                    if (Session[SessionKey.PPID.ToString()] != null)
                    {
                        if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                        {
                            WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "E");
                        }
                        else
                        {
                            WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");
                        }

                    }

                }
                catch
                {
                    throw;
                }
            }
            #endregion VLT ADDED CODE

        }

        private CustomizedBillingInfo PopulateBillingInfo(string msisdn)
        {
            CustomizedBillingInfo BillingInfo = null;
            //SmartKenanService.RetrieveBillingDetailsResponse billingDetails = null;
            KenanSvc.RetrieveBillingDetailsResponse billingDetails = null;

            try
            {

                if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
                {
                    billingDetails = GetTestingBillingDetails();
                }
                else
                {
                    using (var proxy = new SmartKenanServiceProxy())
                    {
                        var request = new KenanSvc.RetrieveBillingDetailsRequest
                        {
                            MSISDN = msisdn,
                            TransactionType = KenanSvc.TransactionType.Msisdn,
                        };

                        billingDetails = proxy.RetrieveBillingDetails(request);
                        if (!billingDetails.Success)
                        {
                            throw new Exception("Transaction Unsuccessful");
                        }
                    }
                }
                BillingInfo = new CustomizedBillingInfo();
                BillingInfo.TotalOutstanding = billingDetails.TotalDueAmount.FormatCurrency();
                BillingInfo.Unbilled = billingDetails.UnbilledAmount.FormatCurrency();
                BillingInfo.CurrentBilled = billingDetails.CurrentBillAmount.FormatCurrency();
                BillingInfo.DueDate = billingDetails.CurrentBillDueDate == DateTime.MinValue ? "" : billingDetails.CurrentBillDueDate.ToString(dateFormat);
                BillingInfo.CreditLimit = billingDetails.CreditLimit.FormatCurrency();
                BillingInfo.BalanceForward = billingDetails.BalanceBringForwardAmount.FormatCurrency();
            }
            catch (Exception ex)
            {
                BillingInfo = null;
            }
            return BillingInfo;

        }

        //UNUSED METHODS
        //private M2KSvc.Account GetTestingAccount()
        //{
        //    var account = new M2KSvc.Account
        //    {
        //        AccountCategory = 4,
        //        MarketCode = 32,
        //        AccountNo = 146424614,
        //        LastName = "NOR AZRI BIN NOR AZIZAN",
        //        Address1 = "NO 22 JALAN CONGKAK 11/1D",
        //        Address2 = "SEKSYEN 11",
        //        City = "Shah Alam",
        //        State = "Selangor",
        //        Zip = "40100",
        //        CompanyName = "ValueLabs",
        //        extendedData = new M2KSvc.ExtendedData[] 
        //        { 
        //            new M2KSvc.ExtendedData
        //            {
        //                Id = "930826055237",
        //                DisVal = "New IC",
        //                Type = 9
        //            }
        //        }
        //    };

        //    return account;
        //}
        //private SmartKenanService.RetrieveBillingDetailsResponse GetTestingBillingDetails()
        private KenanSvc.RetrieveBillingDetailsResponse GetTestingBillingDetails()
        {
            var billingDetails = new KenanSvc.RetrieveBillingDetailsResponse
            {
                BalanceBringForwardAmount = 0.50,
                CreditLimit = 200,
                CurrentBillAmount = 160,
                CurrentBillDueDate = DateTime.ParseExact("20120901", "yyyyMMdd", null),
                LastPayments = new KenanSvc.PaymentDetail[] 
                { 
                    new KenanSvc.PaymentDetail
                    {
                        Date = DateTime.ParseExact("20120716", "yyyyMMdd", null),
                        Amount = 120.50
                    }, 
                    new KenanSvc.PaymentDetail
                    {
                        Date = DateTime.ParseExact("20120620", "yyyyMMdd", null),
                        Amount = 98.50
                    },
                    new KenanSvc.PaymentDetail
                    {
                        Date = DateTime.ParseExact("20120518", "yyyyMMdd", null),
                        Amount = 103.60
                    }
                },
                TotalDueAmount = 98.50,
                UnbilledAmount = 98.50
            };

            return billingDetails;
        }

        //UNUSED METHODS
        //private SmartKenanService.RetrieveBillingDetailsResponse GetKenanBillingDetails(string msisdn)
        //{
        //    SmartKenanService.RetrieveBillingDetailsResponse billingDetails = null;

        //    if (Settings.Default.DevelopmentModeOn)
        //    {
        //        billingDetails = GetTestingBillingDetails();
        //    }
        //    else
        //    {
        //        using (var proxy = new SmartKenanServiceProxy())
        //        {
        //            var request = new SmartKenanService.RetrieveBillingDetailsRequest
        //            {
        //                MSISDN = msisdn,
        //                TransactionType = SmartKenanService.TransactionType.Msisdn,
        //            };

        //            billingDetails = proxy.RetrieveBillingDetails(request);
        //            if (!billingDetails.Success)
        //                throw new ApplicationException(billingDetails.Message);
        //        }
        //    }

        //    return billingDetails;
        //}

        private CustomizedAccount PopulateAccountInfo(string msisdn)
        {
            retrieveAcctListByICResponse AcctListByICResponse = null;
            AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()];

            List<Items> itemList = AcctListByICResponse.itemList.Where(a => a.ExternalId == (msisdn)).ToList();

            //itemList.Where(a => a.ExternalId==(msisdn));

            #region commented by chetan
            //M2KSvc.Account M2KSvcAccount = null;
            //CustomizedAccount Account = null;


            //try
            //{
            //    if (Properties.Settings.Default.DevelopmentModeOnForPpidChecking)
            //        M2KSvcAccount = GetTestingAccount();
            //    else
            //        M2KSvcAccount = M2KServiceProxy.GetAccount(msisdn);

            //    //var modelAccountInfo = model.AccountInfo;
            //    var accountCategoryID = Util.GetIDByKenanCode(RefType.AccountCategory, M2KSvcAccount.AccountCategory.ToString());
            //    var marketID = Util.GetIDByKenanCode(RefType.Market, M2KSvcAccount.MarketCode.ToString());
            //    var addressDetails = Util.ConcatenateStrings(" ", M2KSvcAccount.City, M2KSvcAccount.Zip, M2KSvcAccount.State);

            //    if (M2KSvcAccount.Zip != null)
            //    {
            //        Session[SessionKey.PostalCode.ToString()] = M2KSvcAccount.Zip;
            //    }
            //    else
            //    {
            //        Session[SessionKey.PostalCode.ToString()] = string.Empty;
            //    }

            //    if (M2KSvcAccount.State != null)
            //    {
            //        Session[SessionKey.State.ToString()] = M2KSvcAccount.State;
            //    }
            //    else
            //    {
            //        Session[SessionKey.State.ToString()] = string.Empty;
            //    }

            //    Account = new CustomizedAccount();
            //    Account.AccountNumber = M2KSvcAccount.AccountNo.ToString();
            //    Account.Holder = M2KSvcAccount.LastName;
            //    Account.CompanyName = M2KSvcAccount.CompanyName;
            //    Account.Category = Util.GetNameByID(RefType.AccountCategory, accountCategoryID);
            //    Account.MarketCode = Util.GetNameByID(RefType.Market, marketID);
            //    Account.Address = Util.ConcatenateStrings(", ", M2KSvcAccount.Address1, M2KSvcAccount.Address2, M2KSvcAccount.Address3, addressDetails);
            //    Account.SubscribeNo = M2KSvcAccount.SubscribeNo.ToString();
            //    Account.SubscribeNoResets = M2KSvcAccount.SubscribeNoResets.ToString();
            //    Account.ActiveDate = ""; //TODO: Find out what api needed for this
            //    Account.Plan = ""; //TODO: Find out what api needed for this

            //    if (!M2KSvcAccount.extendedData.IsEmpty())
            //    {
            //        var extendedData = M2KSvcAccount.extendedData[0];
            //        Account.IDNumber = extendedData.Id;
            //        Account.IDType = extendedData.DisVal;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Account = null;
            //}
            #endregion

            CustomizedAccount Account = null;
            try
            {
                if (itemList[0].Customer.PostCode != null)
                {
                    Session[SessionKey.PostalCode.ToString()] = itemList[0].Customer.PostCode;
                }
                else
                {
                    Session[SessionKey.PostalCode.ToString()] = string.Empty;
                }

                if (itemList[0].Customer.State != null)
                {
                    Session[SessionKey.State.ToString()] = itemList[0].Customer.State;
                }
                else
                {
                    Session[SessionKey.State.ToString()] = string.Empty;
                }


                var addressDetails = Util.ConcatenateStrings(" ", itemList[0].Customer.City, itemList[0].Customer.PostCode, itemList[0].Customer.State);

                Account = new CustomizedAccount();
                Account.AccountNumber = itemList[0].AcctExtId;// itemList[0].Account.AccountNumber;
                Account.Holder = itemList[0].Customer.CustomerName;// itemList[0].Account.Holder;
                Account.CompanyName = "";// itemList[0].Account.CompanyName;
                Account.Category = "";// itemList[0].Account.Category;
                Account.MarketCode = "";// itemList[0].Account.MarketCode;
                Account.Address = Util.ConcatenateStrings(", ", itemList[0].Customer.Address1, itemList[0].Customer.Address2, itemList[0].Customer.Address3, addressDetails);
                Account.SubscribeNo = "";
                Account.SubscribeNoResets = "";
                Account.ActiveDate = ""; //TODO: Find out what api needed for this
                Account.Plan = ""; //TODO: Find out what api needed for this
                Account.IDNumber = Session[SessionKey.IDCardNo.ToString()].ToString();
                Account.IDType = Util.GetNameByID(RefType.IDCardType, Session[SessionKey.IDCardTypeID.ToString()].ToInt());

            }
            catch (Exception ex)
            {
                Account = null;
            }

            return Account;
        }
        private List<ContractModels> GetContractDetails(string MSISDN, string Kenancode)
        {
            //string str = "http://10.200.51.122:7030/ws/maxis.eai.process.common.ws:retrievePkgCompInfoService/maxis_eai_process_common_ws_retrievePkgCompInfoService_Port";
            string str = Settings.Default.retrievePkgCompInfoService.ToString();
            List<ContractModels> contractList = new List<ContractModels>();
            contractList = (List<ContractModels>)Util.retrieveCpntractDetls(MSISDN, Kenancode, userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
                password: Properties.Settings.Default.retrieveAcctListByICResponsePassword, postUrl: str.Trim());
            ContractModel.itemList = contractList;
            return contractList;
        }
        //private IList<SubscriberICService.PackageModel> GetPackageDetails(string MSISDN, string Kenancode)
        //private IList<SubscriberICService.PackageModel> GetPackageDetails(string MSISDN, string Kenancode)
        //{
        //    //var proxy = new retrieveSmartServiceInfoProxy();
        //    var proxy = new retrieveServiceInfoProxy();

        //    IList<SubscriberICService.PackageModel> packages = proxy.retrievePackageDetails(MSISDN, Kenancode, userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
        //        password: Properties.Settings.Default.retrieveAcctListByICResponsePassword);
        //    string xmlReq = XMLHelper.ConvertObjectToXml(packages);

        //    return packages;

        //}
        private IList<SubscriberICService.PackageModel> GetPackageDetails(string MSISDN, string Kenancode)
        {
            string str = Properties.Settings.Default.retrievePkgCompInfoService.ToString();


            Online.Registration.Web.SubscriberICService.SubscriberICServiceClient icserviceobj = new Online.Registration.Web.SubscriberICService.SubscriberICServiceClient();
            IList<Online.Registration.Web.SubscriberICService.PackageModel> packages =
                icserviceobj.retrievePackageDetls(MSISDN, Kenancode, userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName, password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword, postUrl: "");


            return packages;

        }
        private void GetExtendContracts()
        {
            var proxy = new SmartCatelogServiceProxy();
            ContractBundleModels model = new ContractBundleModels();
            IEnumerable<string> list = proxy.ExtendContract(model);
            Session["list"] = list;
        }
        private void GetExtendPlans()
        {
            var proxy = new SmartCatelogServiceProxy();
            ContractPlans model = new ContractPlans();
            IEnumerable<string> PlansList = proxy.ExtendPlanContract(model);
            Session["PlansList"] = PlansList;
        }
        private List<PackageComponents> ConstructPackage()
        {
            IList<SubscriberICService.PackageModel> packages = (IList<SubscriberICService.PackageModel>)Session["Package"];
            List<PackageComponents> pckg = new List<PackageComponents>();
            foreach (var contract in packages)
            {
                for (int i = 0; i < contract.compList.Count; i++)
                {
                    pckg.Add(new PackageComponents()
                    {
                        PackageId = contract.PackageID,
                        packageInstId = contract.packageInstId,
                        packageInstIdServ = contract.packageInstIdServ,
                        packageDesc = contract.PackageDesc,
                        componentId = contract.compList[i].componentId,
                        componentInstId = contract.compList[i].componentInstId,
                        componentInstIdServ = contract.compList[i].componentInstIdServ,
                        componentActiveDt = contract.compList[i].componentActiveDt,
                        componentInactiveDt = contract.compList[i].componentInactiveDt,
                        componentDesc = contract.compList[i].componentDesc,
                        componentShortDisplay = contract.compList[i].componentShortDisplay
                    });
                }
            }
            return pckg;

        }
        private CMSSComplain ConstructCMSSID()
        {
            CMSSComplain cms = new CMSSComplain();
            cms.CMSSID = Session["CMSSID"] == null ? string.Empty : Session["CMSSID"].ToString();
            return cms;
        }
        private bool GetDiscount(FormCollection form, SmartModel smartmodelVM)
        {
            string radiobuttonclicked = string.Empty;
            Session["DiscountPrice"] = null;
            if (form["chkRebatel1000"] != null)
            {
                radiobuttonclicked = form["chkRebatel1000"].ToString();
                if (radiobuttonclicked.ToUpper() == "REBATEL1000")
                {
                    if (!string.IsNullOrEmpty(form["ddlRebatel1000"]))
                    {
                        discount = Convert.ToInt32(form["ddlRebatel1000"].ToString());
                        Session["DiscountPrice"] = discount;
                    }
                }
                else if (radiobuttonclicked.ToUpper() == "REBATEG1000")
                {

                    if (!string.IsNullOrEmpty(form["ddlRebateg1000"]))
                    {
                        discount = Convert.ToInt32(form["ddlRebateg1000"].ToString());
                        Session["DiscountPrice"] = discount;
                    }
                }
                else if (radiobuttonclicked.ToUpper() == "FREEDEVICE")
                {
                    discount = Convert.ToInt32(Session["RegMobileReg_RRPDevicePrice"].ToString());
                    Session["DiscountPrice"] = discount;
                    //As Per POS specifications if Free handset then we need to pass discount as 100 means 100% discount then they take RRP in their end based on ArticleID
                    //in FreeHandset case we need to pass Discount_ID as 40 and manageable from Config setting
                }
                if (Session["DiscountPrice"] != null)
                {
                    Session["DPrice"] = Session["DiscountPrice"];
                    return true;
                }
                else
                {
                    smartmodelVM.DiscountType = "Please select discount";
                    Session[SessionKey.Discount.ToString()] = smartmodelVM.DiscountType;
                }
            }
            return false;
        }
        private void CreateKenanAccount(int regID)
        {
            //var result = "";
            var reg = new Online.Registration.DAL.Models.Registration();
            var cust = new Customer();
            var billAddr = new Address();

            try
            {
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    // ********** Update Registration Status **********
                    proxy.RegStatusCreate(ConstructRegStatus(regID, Properties.Settings.Default.Status_AccPendingCreate), null);

                    // ********** Construct Kenan object **********
                    reg = proxy.RegistrationGet(regID);
                    cust = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    var billAddrID = proxy.RegAddressFind(new AddressFind()
                    {
                        Address = new Address()
                        {
                            RegID = reg.ID,
                            AddressTypeID = Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Billing)
                        }
                    }).SingleOrDefault();

                    billAddr = proxy.RegAddressGet(new int[] { billAddrID }).SingleOrDefault();
                }

                using (var proxy = new SmartKenanServiceProxy())
                {
                    if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                    {

                        proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
                        {
                            orderId = regID.ToString(),
                            externalId = reg.MSISDN1,
                            extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
                            Customer = ConstructKenanCustomerMNP(cust, billAddr),
                            Account = ConstructKenanAccountMNP(cust),

                        });
                    }
                    else
                    {
                        if (reg.RegTypeID == (int)MobileRegType.MNPPlanOnly)
                        {

                            proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
                            {
                                orderId = regID.ToString(),
                                externalId = reg.MSISDN1,
                                extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
                                Customer = ConstructKenanCustomerMNP(cust, billAddr),
                                Account = ConstructKenanAccountMNP(cust),

                            });
                        }
                        else
                        {
                            proxy.KenanAccountCreate(new OrderCreationRequest()
                            {
                                OrderID = regID.ToString(),
                                MarketCodeID = Util.GetIDByCode(RefType.Market, Properties.Settings.Default.MarketCode_Individual),
                                AccCategoryID = Util.GetIDByCode(RefType.AccountCategory, Properties.Settings.Default.AccountCategory_Consumer),
								//VIPCodeID = Util.GetIDByCode(RefType.VIPCode, Properties.Settings.Default.VIPCode_Individal),
								// Add in Check For Wrong Collection Tagging
								VIPCodeID = Util.custVIPCheck(cust),
                                ExternalID = reg.MSISDN1,
                                ExtIDTypeID = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN),
                                Customer = ConstructKenanCustomer(cust),
                                BillingAddress = ConstructBillAddress(billAddr),


                            });
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                //result = ex.Message;
            }

            //return Json(result);
        }

        private KenanCustomer ConstructKenanCustomer(Customer cust)
        {
            return new KenanCustomer()
            {
                CustomerTitleID = cust.CustomerTitleID,
                DateOfBirth = cust.DateOfBirth,
                EmailAddr = cust.EmailAddr,
                FullName = cust.FullName,
                Gender = cust.Gender,
                IDCardNo = cust.IDCardNo,
                IDCardTypeID = cust.IDCardTypeID,
                NationalityID = cust.NationalityID,
                PayModeID = cust.PayModeID,
                RaceID = cust.RaceID,
                AlternateMSISDN = String.IsNullOrEmpty(cust.AlternateContactNo) ? String.Empty : cust.AlternateContactNo,
                CBR = cust.ContactNo
            };
        }

        private KenanAccountMNP ConstructKenanAccountMNP(Customer cust)
        {
            return new KenanAccountMNP()
            {

                mktCode = Util.GetIDByCode(RefType.Market, Properties.Settings.Default.MarketCode_Individual),
                acctCategory = Util.GetIDByCode(RefType.AccountCategory, Properties.Settings.Default.AccountCategory_Consumer),
                vipCode = Util.GetIDByCode(RefType.VIPCode, Properties.Settings.Default.VIPCode_Individal),
                custEmail = cust.EmailAddr,
                payMethod = cust.PayModeID
            };
        }

        private KenanCustomerMNP ConstructKenanCustomerMNP(Customer cust, Address billAddr)
        {
            return new KenanCustomerMNP()
            {
                CustomerTitleID = cust.CustomerTitleID,
                DateOfBirth = cust.DateOfBirth,
                EmailAddr = cust.EmailAddr,
                FullName = cust.FullName,
                Gender = cust.Gender,
                IDCardNo = cust.IDCardNo,
                IDCardTypeID = cust.IDCardTypeID,
                NationalityID = cust.NationalityID,
                PayModeID = cust.PayModeID,
                RaceID = cust.RaceID,
                CBR = cust.ContactNo,
                billAddress1 = billAddr.Line1,
                billAddress2 = billAddr.Line2,
                billAddress3 = billAddr.Line3,
                icType = Util.GetNameByID(RefType.IDCardType, cust.IDCardTypeID).ToString(),
                icNo = cust.IDCardNo,
                billLname = cust.FullName,
                billTitle = Util.GetNameByID(RefType.CustomerTitle, cust.CustomerTitleID).ToString(),
                billCity = billAddr.Town,
                billState = Util.GetNameByID(RefType.State, billAddr.StateID),
                billZip = billAddr.Postcode,
                billCounty = "",
                billCountryCode = "458",
            };
        }

        public BillAddress ConstructBillAddress(Address addr)
        {
            return new BillAddress()
            {
                BuildingNo = addr.BuildingNo,
                CountryID = addr.CountryID,
                Line1 = addr.Line1,
                Line2 = addr.Line2,
                Postcode = addr.Postcode,
                StateID = addr.StateID,
                Town = addr.Town,
                UnitNo = addr.UnitNo
            };
        }

        private RegStatus ConstructRegStatus(int regID, string statusCode)
        {
            return new Online.Registration.DAL.Models.RegStatus()
            {
                RegID = regID,
                StartDate = DateTime.Now,
                StatusID = Util.GetIDByCode(RefType.Status, statusCode),
                CreateDT = DateTime.Now,
                Active = true,
                LastAccessID = Util.SessionAccess.UserName
            };
        }

        private void FulfillKenanAccount(int regID)
        {
            //var result = "";
            var reg = new Online.Registration.DAL.Models.Registration();
            var cust = new Customer();
            var billAddr = new Address();

            try
            {
                //using (var proxy = new SmartRegistrationServiceProxy())
                //{
                //    // ********** Update Registration Status **********
                //    proxy.RegStatusCreate(ConstructRegStatus(regID, Properties.Settings.Default.Status_SvcPendingActivate), null);

                //    // ********** Construct Kenan object **********
                //    reg = proxy.RegistrationGet(regID);
                //    cust = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();
                //}

                var request = new OrderFulfillRequest()
                {
                    OrderID = regID.ToString(),
                    //AccountExternalID = reg.KenanAccountNo,
                    //Gender = cust.Gender,
                    //RaceID = cust.RaceID,
                    //NationalityID = cust.NationalityID,
                    //CountryID = Util.GetIDByCode(RefType.Country, Properties.Settings.Default.Country_MY),
                    //DateOfBirth = cust.DateOfBirth,
                    //ExternalIDList = ConstructExternalIDList(reg.SIMSerial, reg.MSISDN1).ToArray()
                };

                //// construct Package, Components, and Contract
                //ConstructKenanPackageComponent(request);

                using (var proxy = new SmartKenanServiceProxy())
                {
                    proxy.KenanAccountFulfill(request);
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                //result = ex.Message;
            }

            //return Json(result);
        }

        [HttpPost]
        public string SimModelsCount(string strArticleId)
        {
            int subscriptionReqFromVasResult = 0;
            string articleid = string.Empty;
            if (strArticleId != null && !string.IsNullOrEmpty(strArticleId) && !strArticleId.Equals("0"))
            {
                articleid = strArticleId.Split(',')[1];
                string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                int storeId = 0;
                string[] maxisStoreIds = ConfigurationManager.AppSettings["OrganizationsIds"].ToString().Split(",".ToCharArray());
                if (!maxisStoreIds.Contains(Util.SessionAccess.User.Org.OrganisationId))
                {
                    storeId = Convert.ToInt32(Util.SessionAccess.User.Org.OrganisationId);
                }
                Session["SimModelTypeId"] = strArticleId.Split(',')[0];

                try
                {
                    //Code by chetan moved the dynamic proxy call to integration service
                    using (var proxy = new DynamicServiceProxy())
                    {
                        subscriptionReqFromVasResult = proxy.GetStockCount(articleid, wsdlUrl,storeId);
                    }

                    if (subscriptionReqFromVasResult > 0)
                    {
                        Session["SimModelTypeId"] = strArticleId.Split(',')[0];
                    }
                }
                catch (Exception ex)
                {
                    //RedirectToAction("StoreError", "HOME");
                    WebHelper.Instance.LogExceptions(this.GetType(), ex);
                    throw ex;
                }
            }
            //END HERE

            return subscriptionReqFromVasResult.ToString();

        }

        public FileContentResult QRCodeGenerate(string value)
        {
            byte[] bitmapBytes = null;
            var bitmapImg = Util.GenerateQRCode("MECARD:N:Jamaludin b m yusof;ORG:12345;TEL:321451234512345;ADR:1234567890123456789 9999;NOTE:1.09876543;;", 150, 150);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                bitmapImg.Save(memoryStream, ImageFormat.Png);
                bitmapBytes = memoryStream.GetBuffer();
            }

            return new FileContentResult(bitmapBytes, "image/bmp");
        }
        private int GetContractDuration(int ContractId)
        {
            int ContractDuration = 0;
            using (var proxy = new SmartCatelogServiceProxy())
            {
                ContractDuration = proxy.GetContractDuration(ContractId);
            }
            return ContractDuration;
        }
        private string GetUomId(string ArticleId, int ContractDuration, string contractID)
        {
            string UomId = string.Empty;
            using (var proxy = new SmartCatelogServiceProxy())
            {
                //UomId = proxy.GetUomId(ArticleId, ContractDuration, contractID);
                UomId = proxy.GetUomId(ArticleId, ContractDuration, contractID, "SMART Program");
            }
            return UomId;
        }
                    #endregion

        public string GetDepenedencyComponents(string componentIds, bool planFlag = true)
        {
            if (componentIds != null)
            {
                string userName = string.Empty;
                if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                {
                    userName = Request.Cookies["CookieUser"].Value;
                }
                string selectedVasIDs = string.Empty;
                if (componentIds.Contains(",") && componentIds.Contains("_"))
                {
                    foreach (string vasID in componentIds.Split(','))
                    {
                        if (vasID.Contains("_"))
                        {
                            selectedVasIDs = selectedVasIDs + vasID.Split('_')[1] + ",";
                        }
                    }
                    componentIds = selectedVasIDs;
                }
                string selectcompIds = string.Empty;
                if (componentIds != string.Empty && componentIds.Contains(","))
                    componentIds = string.Join(",", componentIds.TrimEnd(',').Split(',').ToList<string>().Select(id => id.ToString()).Distinct());

                selectcompIds = componentIds;

                List<int> depCompIds = new List<int>();
                List<string> filteredCompIds = new List<string>();
                using (var proxy = new CatalogServiceProxy())
                {
                    if (componentIds != string.Empty)
                    {
                        foreach (var compId in componentIds.Split(','))
                        {
                            if (compId != "[object XMLDocument]")
                                depCompIds = proxy.GetAllDependentComponents(Convert.ToInt32(compId), 0);


                            foreach (var depCompId in depCompIds)
                            {

                                if (!componentIds.Contains(depCompId.ToString()))
                                {
                                    if (filteredCompIds != null && filteredCompIds.Count > 0 && (!string.Join(",", filteredCompIds.ToArray()).Contains(depCompId.ToString())))
                                        filteredCompIds.Add(depCompId.ToString());
                                    else if (filteredCompIds != null && filteredCompIds.Count == 0)
                                    {
                                        filteredCompIds.Add(depCompId.ToString());
                                    }
                                }


                            }
                        }
                    }

                }
                if (planFlag)
                {
                    int selectedPlan = 0;
                    if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] != null)
                    {
                        selectedPlan = Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]);

                    }
                    List<int> mandatoryVasIds = new List<int>();
                    if (selectedPlan > 0)
                    {
                        mandatoryVasIds = GetAvailablePackageComponents(selectedPlan, isMandatory: true).MandatoryVASes.Select(cas => cas.PgmBdlPkgCompID).ToList();
                        if (mandatoryVasIds != null && mandatoryVasIds.Count > 0)
                        {
                            using (var proxy = new CatalogServiceProxy())
                            {

                                foreach (var vasId in mandatoryVasIds)
                                {

                                    depCompIds = proxy.GetAllDependentComponents(Convert.ToInt32(vasId), selectedPlan);

                                    foreach (var depCompId in depCompIds)
                                    {

                                        if (filteredCompIds != null)
                                        {
                                            filteredCompIds.Add(depCompId.ToString());
                                        }



                                    }
                                }
                            }

                        }
                    }
                }

                if (filteredCompIds != null && filteredCompIds.Count > 0)
                {
                    if (selectcompIds != null && selectcompIds != string.Empty)
                    {
                        if (selectcompIds.Substring(selectcompIds.Length - 1) != ",")
                        {
                            selectcompIds = selectcompIds + "," + string.Join(",", filteredCompIds.ToArray()) + ",";
                        }
                        else
                            selectcompIds = selectcompIds + string.Join(",", filteredCompIds.ToArray()) + ",";
                    }
                    else if (selectcompIds == string.Empty)
                        selectcompIds = selectcompIds + string.Join(",", filteredCompIds.ToArray()) + ",";


                    if (selectcompIds != string.Empty && selectcompIds.Contains(","))
                        selectcompIds = string.Join(",", selectcompIds.TrimEnd(',').Split(',').ToList<string>().Select(id => id.ToString()).Distinct());

                }
                return selectcompIds;
            }
            else return componentIds;
        }

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        [DoNoTCache]
        public ActionResult SelectComponent()
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session["VasBox"] = "False";
            Session["HidevasButton"] = true;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();
            ComponentViewModel componentViewModel = new ComponentViewModel();
            if (Session["_componentViewModel"] == null)
            {
                GetOldEligiblePackagesFromKenan(componentViewModel);
                componentViewModel = GetPackagesAndItsComponents();
                Session["_componentViewModel"] = componentViewModel;
            }
            else
            {
                componentViewModel = (ComponentViewModel)Session["_componentViewModel"];
            }
            return View(componentViewModel);
        }

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        [HttpPost]
        public ActionResult SelectComponent(FormCollection collection, ComponentViewModel VM)
        {
            Session["RegMobileReg_ContractID"] = null;
            int nextStep = VM.TabNumber;
            if (nextStep == 4)
            {
                string[] selectedVals = collection["SelectedValues"].Split(',');
                string[] selectedCompIds = collection["SelectedCompIds"].Split(',');
                //string[] SelectedCkeckBoxIds = collection["SelectedCkeckBoxIds"].Split(',');
                
                //Session["SelectedCompIdsForCheck"] = SelectedCkeckBoxIds;
                Session["SelectedCompIdsForCheck"] = collection["SelectedCkeckBoxIds"];


                ComponentViewModel sessionVal = (ComponentViewModel)Session["_componentViewModel"];

				if (sharingQuotaFeature)
				{
					List<string> existingComponentList = sessionVal.OldComponentsList.Select(x => x.OldComponentId).ToList();
					var selectedComponentList = WebHelper.Instance.CommaSplitToListString(collection["SelectedValues"].ToString2());
					sessionVal.ShareSuppErrorMessage = WebHelper.Instance.MutualDependencyheckForARS(existingComponentList, selectedComponentList);

					if (!string.IsNullOrEmpty(sessionVal.ShareSuppErrorMessage))
					{
						return RedirectToAction("SelectComponent");
					}
				}

                #region perform to check All the Dependent Components

                List<int> depCompIds = new List<int>();
                List<int> depdendentCompIds = new List<int>();
                int PlanId = 0;
                if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] != null)
                {
                    PlanId = Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]);

                }

                if (selectedCompIds != null)
                {
                    using (var proxy = new CatalogServiceProxy())
                    {

                        foreach (var compId in selectedCompIds)
                        {
                            depCompIds = proxy.GetAllSmartDependentComponents(Convert.ToInt32(compId), 0);

                            foreach (var depCompId in depCompIds)
                            {
                                depdendentCompIds.Add(depCompId);

                            }
                        }
                    }
                }


                #endregion

                #region perform VAS dependency checking

                List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();
                Online.Registration.Web.SubscriberICService.componentList component;
                string datapackagkenacode = string.Empty;
                string datapackageId = string.Empty;

                foreach (NewComponents item in sessionVal.NewComponentsList)
                {
                    if (selectedVals.Contains(item.NewComponentKenanCode))
                    {
                        item.IsChecked = true;
                        component = new Online.Registration.Web.SubscriberICService.componentList();
                        component.componentIdField = item.NewComponentKenanCode;
                        component.packageIdField = item.NewPackageKenanCode;
                        //component.NewPackageKenanCode = item.NewPackageKenanCode;
                        componentList.Add(component);
                        if (item.ComponentGroupName == "DC")
                        {
                            datapackagkenacode = item.NewPackageKenanCode;
                            datapackageId = item.NewPackageId;
                        }
                    }
                    else
                    {
                        item.IsChecked = false;
                    }
                }
                if (datapackagkenacode == string.Empty)
                {// this conidation for surf more plans. because we dont have data components for surfmore plans.
                    datapackagkenacode = TempData["DataComponentPkgKenancode"].ToString2();
                    datapackageId = TempData["DataComponentPkgId"].ToString2();
                }
                if (datapackagkenacode == string.Empty)
                {
                    //datapackagkenacode = "41634";//
                    RegistrationSvc.RegistrationServiceClient proxy = new RegistrationServiceClient();

                    datapackagkenacode = proxy.GetDataKenaCode_Smart(PlanId.ToString2());

					if (string.IsNullOrEmpty(datapackagkenacode) || datapackagkenacode == "0")
					{
						using (var regProxy = new RegistrationServiceProxy())
						{
							var resp = regProxy.SelectedPackageDetail(PlanId);
							datapackagkenacode = resp.PackageDetails.DataPackageId;
						}
					}
                }
                if (!string.IsNullOrEmpty(datapackagkenacode))
                {
                    if (depdendentCompIds != null)
                    {
                        List<NewComponents> dependentVAses = new List<NewComponents>();
                        if (depdendentCompIds.Count() > 0)
                        {
                            using (var proxy = new SmartCatelogServiceProxy())
                            {
                                var vases = proxy.PgmBdlPckComponentGet
                                   (
                                       depdendentCompIds
                                   ).ToList();

                                List<Component> components = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();

                                var query = from selectedVases in vases
                                            join comps in components on
                                                selectedVases.ChildID equals comps.ID
                                            select new { name = comps.Name + " (RM" + selectedVases.Price + ")", id = comps.ID };


                                foreach (var vascomp in vases)
                                {
                                    var vasnames = (from cvas in query
                                                    where cvas.id.Equals(vascomp.ChildID)
                                                    select new { name = cvas.name }).ToList();
                                    var depvasname = string.Empty;
                                    if (vasnames.Count > 0)
                                        depvasname = vasnames[0].name;
                                    else
                                        depvasname = vascomp.Name;

                                    component = new Online.Registration.Web.SubscriberICService.componentList();
                                    component.componentIdField = vascomp.KenanCode;
                                    component.packageIdField = datapackagkenacode;

                                    componentList.Add(component);
                                    if (!string.IsNullOrEmpty(datapackagkenacode))
                                    {
                                        dependentVAses.Add(new NewComponents
                                        {
                                            ComponentGroupId = "13",
                                            ComponentGroupName = "DC",
                                            NewComponentId = vascomp.ID.ToString(),
                                            NewComponentKenanCode = vascomp.KenanCode,
                                            NewComponentName = depvasname,
                                            NewPackageId = datapackageId,
                                            NewPackageKenanCode = datapackagkenacode,
                                            IsChecked = true
                                        });
                                    }

                                }
                            }
                        }

                        sessionVal.DependentVASList = dependentVAses;
                    }
                }
                else
                {
                    if (depdendentCompIds != null)
                    {
                        List<NewComponents> dependentVAses = new List<NewComponents>();
                        if (depdendentCompIds.Count() > 0)
                        {
                            using (var proxy = new SmartCatelogServiceProxy())
                            {
                                var vases = proxy.PgmBdlPckComponentGet
                                   (
                                       depdendentCompIds
                                   ).ToList();

                                List<Component> components = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();

                                var query = from selectedVases in vases
                                            join comps in components on
                                                selectedVases.ChildID equals comps.ID
                                            select new { name = comps.Name + " (RM" + selectedVases.Price + ")", id = comps.ID };
                                string depdendentComps = string.Join(",", depdendentCompIds.ToArray());
                                List<FxDependChkComps> Comps = new List<FxDependChkComps>();
                                using (var Proxy = new RegistrationServiceProxy())
                                {
                                    Comps = Proxy.GetAllComponentsbyPlanId(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), collection["SelectedCompIds"], "BP");
                                }
                                if (Comps != null && Comps.Any())
                                {
                                    datapackagkenacode = Comps[0].KenanCode;
                                }
                                foreach (var vascomp in vases)
                                {
                                    var vasnames = (from cvas in query
                                                    where cvas.id.Equals(vascomp.ChildID)
                                                    select new { name = cvas.name }).ToList();
                                    var depvasname = string.Empty;
                                    if (vasnames.Count > 0)
                                        depvasname = vasnames[0].name;
                                    else
                                        depvasname = vascomp.Name;

                                    component = new Online.Registration.Web.SubscriberICService.componentList();
                                    component.componentIdField = vascomp.KenanCode;
                                    component.packageIdField = datapackagkenacode;

                                    componentList.Add(component);
                                    if (!string.IsNullOrEmpty(datapackagkenacode))
                                    {
                                        dependentVAses.Add(new NewComponents
                                        {
                                            ComponentGroupId = "13",
                                            ComponentGroupName = "DC",
                                            NewComponentId = vascomp.ID.ToString(),
                                            NewComponentKenanCode = vascomp.KenanCode,
                                            NewComponentName = depvasname,
                                            NewPackageId = datapackageId,
                                            NewPackageKenanCode = datapackagkenacode,
                                            IsChecked = true
                                        });
                                    }

                                }
                            }
                        }

                        sessionVal.DependentVASList = dependentVAses;
                    }

                }

                if (componentList.Count > 0)
                {
                    //component = new Online.Registration.Web.SubscriberICService.componentList();
                    //component.componentIdField = "40090";
                    //component.packageIdField = PlanId.ToString2();
                    //componentList.Add(component);
					string strPackageId = string.Empty, strPackageKenanCode = string.Empty;
					strPackageId = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToString();

					using (var proxy = new SmartCatelogServiceProxy())
					{
						var pbpc = proxy.PgmBdlPckComponentGetSmart(new int[] { strPackageId.ToInt() }).SingleOrDefault();
						if (pbpc != null)
							strPackageKenanCode = pbpc.KenanCode;
					}
					// drop 5
					retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
					typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, mandatoryPackageID: strPackageKenanCode);
                    
                    ViewBag.GSTMark = Settings.Default.GSTMark;
                    List<String> LabelReplace = new List<String>();
                    LabelReplace.Add("GSTMark");
                    ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);


                    typeChkFxVasDependencyResponse response = _retrieveServiceInfoProxy.ChkFxVasDependency(request);

                    if (Session["_componentViewModel"] != null)
                    {
                        VM = (ComponentViewModel)Session["_componentViewModel"];
                    }
                    if (response.mxsRuleMessagesField != null && response.mxsRuleMessagesField.Count > 0)
                    {
                        foreach (string errmsg in response.mxsRuleMessagesField)
                        {
                            ModelState.AddModelError(string.Empty, errmsg);
                        }
                        TempData["Error"] = "True";
                        return View(VM);
                    }
                    else if (response.msgCodeField == "3")
                    {
                        ModelState.AddModelError(string.Empty, "OPF system error - Unable to fetch data.");   // Need to change msg
                        return View(VM);
                    }
                    else if (response.msgCodeField != "0")
                    {
                        ModelState.AddModelError(string.Empty, "Selected components are not available.");   // Need to change msg
                        return View(VM);
                    }
                }

                #endregion
                Session["_componentViewModel"] = sessionVal;
            }
            return RedirectToAction(GetRegActionStep(nextStep));
        }

        private List<packagePairs> ReAssignVASList()
        {
            string strNewPackageId = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToString();
            List<string> lst = (List<string>)Session["existingKenanIDs"];
            if (lst != null && lst.Count > 0)
            {
                lst = lst.Distinct().ToList();
            }
            //var resp = new RegistrationSvc.SmartGetPackageDetailsResp();
            var resp = new RegistrationSvc.GetPackageDetailsResp();
            var resp_existing = new RegistrationSvc.SmartGetExistingPackageDetailsResp();
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                //TODO: need to save ISSIMRequired to d/b
                //collection["ISSIMRequired"]
                resp = proxy.SelectedPackageDetail(strNewPackageId.ToInt());
            }
            string masterpackageid = resp.PackageDetails.MasterPackageId;
            string datapackagid = resp.PackageDetails.DataPackageId;
            string extrapackageid = resp.PackageDetails.ExtraPackageId;
            strNewPackageId = resp.PackageDetails.packageKenanId.ToString2();
            //CP,MP, DP, EP
            string existingpackage = string.Empty;
            string masterpackageid_existing = string.Empty;
            string datapackagid_existing = string.Empty;
            string extrapackageid_existing = string.Empty;

            List<packagePairs> listpackagePairs = new List<packagePairs>();
            packagePairs pkg;
            for (int i = 0; i <= lst.Count - 1; i++)
            {
                pkg = new packagePairs();
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    //TODO: need to save ISSIMRequired to d/b
                    //collection["ISSIMRequired"]
                    resp_existing = proxy.ExistingPackageDetails(lst[i].ToInt());
                    if (resp_existing.ExistingPackageDetails.PlanType == "CP")
                    {
                        existingpackage = lst[i].ToString();
                        pkg.newPackageId = strNewPackageId;
                        pkg.oldPackageId = existingpackage;

                    }
                    else if (resp_existing.ExistingPackageDetails.PlanType == "MP")
                    {
                        masterpackageid_existing = lst[i].ToString();
                        pkg.newPackageId = masterpackageid;
                        pkg.oldPackageId = masterpackageid_existing;

                    }
                    else if (resp_existing.ExistingPackageDetails.PlanType == "DP")
                    {
                        datapackagid_existing = lst[i].ToString();
                        pkg.newPackageId = datapackagid;
                        pkg.oldPackageId = datapackagid_existing;
                    }
                    else if (resp_existing.ExistingPackageDetails.PlanType == "EP")
                    {
                        extrapackageid_existing = lst[i].ToString();
                        pkg.newPackageId = extrapackageid;
                        pkg.oldPackageId = extrapackageid_existing;
                    }
                }
                if (pkg.newPackageId != null && pkg.oldPackageId != null)
                    listpackagePairs.Add(pkg);
            }

            // Preparing other than mandatory,main,data and extra package ids as Other components group    
            if (lst != null && lst.Count > 0)
            {
                List<packagePairs> otherpkgsPairs = new List<packagePairs>();
                for (int i = 0; i <= lst.Count - 1; i++)
                {

                    var existingpkgs = from p in listpackagePairs where p.oldPackageId == lst[i] select p;
                    if (existingpkgs.Count() <= 0)
                    {
                        pkg = new packagePairs();
                        pkg.newPackageId = lst[i];
                        pkg.oldPackageId = lst[i];
                        otherpkgsPairs.Add(pkg);
                    }
                }

                if (otherpkgsPairs.Count() > 0)
                {
                    listpackagePairs.AddRange(otherpkgsPairs);
                }
            }
            // End

            return listpackagePairs;
        }

        private ComponentViewModel GetPackagesAndItsComponents()
        {
            //Session["Kenan_componentViewModel"] = null;

            ComponentViewModel response = new ComponentViewModel();
            Online.Registration.Web.SubscriberICService.SubscriberICServiceClient icserviceobj = new Online.Registration.Web.SubscriberICService.SubscriberICServiceClient();

            IList<Online.Registration.Web.SubscriberICService.PackageModel>
            packages = icserviceobj.retrievePackageDetls(Session[SessionKey.ExternalID.ToString()].ToString2(), System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"], userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName, password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword, postUrl: "");
            //{
            //    packages = packages.GroupBy(x => x.PackageID).Select(x => x.First()).ToList();
            //}

            SmartRegistrationServiceProxy proxy = new SmartRegistrationServiceProxy();
            IEnumerable<string> lstContracts = proxy.GetContractsList().ToList().Where(cd => cd.IsDeviceContract == 1).Select(c => c.KenanCode);
            OldPackages oldPackages;
            OldComponents oldComponents;
            List<OldPackages> oldPackagesList = new List<OldPackages>();
            List<OldComponents> oldComponentsList = new List<OldComponents>();
            //string OldPCkenanCode = string.Empty;

            // added for smart supplimentary
            List<RestrictedComponent> restrictcomps = new List<RestrictedComponent>();
            if (Session["AccountType"].ToString2() == "S")
            {
                using (CatalogServiceProxy prox = new CatalogServiceProxy())
                {
                    restrictcomps = prox.GetAllRestrictedComponents();
                }
            }
            //*****************************//

            if (packages != null)
            {
                foreach (var item in packages)
                {
                    if (!lstContracts.Contains(item.compList[0].componentId))
                    {

                        oldPackages = new OldPackages();
                        oldPackages.OldPackageId = item.PackageID;
                        //OldPCkenanCode = item.PackageID;          // need to check is it correct or worong again
                        oldPackages.OldPackageName = item.PackageDesc;
                        if (item.compList != null)
                        {
                            foreach (var comp in item.compList)
                            {
                                oldComponents = new OldComponents();
                                oldComponents.OldComponentId = comp.componentId;
                                oldComponents.OldComponentName = comp.componentDesc;
                                oldComponents.OldPackageId = item.PackageID;
                                oldComponentsList.Add(oldComponents);
                                // added for smart supplimentary
                                if (Session["AccountType"].ToString2() == "S")
                                {
                                    List<RestrictedComponent> oldrestrictedcomp = restrictcomps.Where(rc => rc.KenanCode == comp.componentId).ToList();
                                    if (oldrestrictedcomp.Count == 0)
                                    {
                                        if (Session["OCCmpntList"].ToString2().Length > 0)
                                        {
                                            Session["OCCmpntList"] = Session["OCCmpntList"].ToString2() + "," + comp.componentId.ToString();
                                        }
                                        else
                                        {
                                            Session["OCCmpntList"] = comp.componentId.ToString();
                                        }
                                    }
                                }
                                //**************************//
                            }
                        }
                        oldPackagesList.Add(oldPackages);
                    }

                }
            }

            //response.OldComponentsList = oldComponentsList;
            //response.OldPackagesList = oldPackagesList.GroupBy(a => a.OldPackageId).Select(a => a.Last()).ToList();
            response.OldComponentsList = oldComponentsList.GroupBy(a => a.OldComponentId).Select(a => a.Last()).ToList();
            response.OldPackagesList = oldPackagesList.GroupBy(b => b.OldPackageId).Select(b => b.Last()).ToList();


            string strNewPackageId = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToString();
            int ModelId = 0;

            using (var _proxy = new SmartCatelogServiceProxy())
            {
                ModelId = _proxy.BrandArticleImageGet(Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt()).ModelID;
            }
            //string ModelId = Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToString2();
            ComponentViewModel Componentresponse = GetSelectedPackageInfo_smart(ModelId, strNewPackageId.ToInt(), "");

            if (((ComponentViewModel)Session["Kenan_componentViewModel"]) != null)
            {
                ComponentViewModel kenanComps = (ComponentViewModel)Session["Kenan_componentViewModel"];
                if (Componentresponse.NewPackagesList != null && kenanComps.NewPackagesList != null)
                {
                    Componentresponse.NewComponentsList = CheckAndUncheckBasedOnVasRetention(response.OldComponentsList, kenanComps.NewComponentsList, Componentresponse.NewComponentsList);
                }

            }

            response.NewPackagesList = Componentresponse.NewPackagesList;
            response.NewComponentsList = Componentresponse.NewComponentsList;

            return response;
        }

        private void GetOldEligiblePackagesFromKenan(ComponentViewModel req)
        {
            ComponentViewModel componentViewModel = new ComponentViewModel();
            retrieveSmartServiceInfoProxy _retrieveServiceInfoProxy = new retrieveSmartServiceInfoProxy();
            //Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary(extendContractCheck: true);

            //bool isSubLine = false;
            //if (Session["AccountType"].ToString2() == "S")
            //    isSubLine = true;
            //Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary(extendContractCheck: true, fromSubline: isSubLine);

            string strNewPackageId = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToString();
            List<reassingVases> objReassingVases = null;
            List<packagePairs> objpackagePairs = new List<packagePairs>();
            List<string> lst = (List<string>)Session["existingKenanIDs"];
            foreach (string kenanCode in lst)
            {
                packagePairs objpackagePair = new packagePairs();
                objpackagePair.newPackageId = strNewPackageId;
                objpackagePair.oldPackageId = kenanCode;
                objpackagePairs.Add(objpackagePair);
            }
            objReassingVases = _retrieveServiceInfoProxy.ReassignVas(Session[SessionKey.AccountNumber.ToString()].ToString(), Session["SubscriberNo"].ToString(), Session["SubscriberNoResets"].ToString(), ReAssignVASList());
            componentViewModel.ReassingVases = objReassingVases;

            SmartCatelogServiceProxy Proxy = new SmartCatelogServiceProxy();
            //var bundlePackages = new List<PgmBdlPckComponentSmart>();
            var bundlePackages = new PgmBdlPckComponentGetResp();
            List<string> PkgIDs = new List<string>();
            OldPackages oldPackages;
            List<OldPackages> oldPackagesList = new List<OldPackages>();
            NewPackages newPackages;
            List<NewPackages> newPackagesList = new List<NewPackages>();

            OldComponents oldComponents;
            List<OldComponents> oldComponentsList = new List<OldComponents>();
            NewComponents newComponents;
            List<NewComponents> newComponentsList = new List<NewComponents>();
            if (componentViewModel.ReassingVases != null)
            {
                foreach (var item in componentViewModel.ReassingVases)
                {
                    PkgIDs.Add(item.NewComponentId);
                    PkgIDs.Add(item.NewPackageId);
                    PkgIDs.Add(item.OldComponentId);
                    PkgIDs.Add(item.OldPackageId);
                }
                PkgIDs = PkgIDs.Distinct().ToList();
                bundlePackages = Proxy.PgmBdlPckComponentGetForSmartByKenanCode(PkgIDs);


                if (bundlePackages != null && bundlePackages.PgmBdlPckComponents!=null)

                    foreach (var item in componentViewModel.ReassingVases)
                    {
                        oldPackages = new OldPackages();
                        oldPackages.OldPackageId = item.OldPackageId;
                        //oldPackages.OldPackageName = bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(item.OldPackageId)) == null ? "" : bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(item.OldPackageId)).Description;
                        oldPackages.OldPackageName = bundlePackages.PgmBdlPckComponents.FirstOrDefault(a => a.KenanCode.Contains(item.OldPackageId)) == null ? "" : bundlePackages.PgmBdlPckComponents.FirstOrDefault(a => a.KenanCode.Contains(item.OldPackageId)).Description;
                        if (oldPackagesList.Where(a => a.OldPackageId.Equals(item.OldPackageId)).Count() == 0)
                            oldPackagesList.Add(oldPackages);

                        newPackages = new NewPackages();
                        newPackages.NewPackageId = item.NewPackageId;
                        //newPackages.NewPackageName = bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(item.NewPackageId)) == null ? "" : bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(item.NewPackageId)).Description;
                        newPackages.NewPackageName = bundlePackages.PgmBdlPckComponents.FirstOrDefault(a => a.KenanCode.Contains(item.NewPackageId)) == null ? "" : bundlePackages.PgmBdlPckComponents.FirstOrDefault(a => a.KenanCode.Contains(item.NewPackageId)).Description;
                        if (newPackagesList.Where(a => a.NewPackageId.Equals(item.NewPackageId)).Count() == 0)
                            newPackagesList.Add(newPackages);

                        oldComponents = new OldComponents();
                        oldComponents.OldComponentId = item.OldComponentId;
                        oldComponents.OldPackageId = item.OldPackageId;
                        //oldComponents.OldComponentName = bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(item.OldComponentId)) == null ? "" : bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(item.OldComponentId)).Description;
                        oldComponents.OldComponentName = bundlePackages.PgmBdlPckComponents.FirstOrDefault(a => a.KenanCode.Contains(item.OldComponentId)) == null ? "" : bundlePackages.PgmBdlPckComponents.FirstOrDefault(a => a.KenanCode.Contains(item.OldComponentId)).Description;
                        oldComponentsList.Add(oldComponents);

                        newComponents = new NewComponents();
                        newComponents.NewComponentId = item.NewComponentId;
                        newComponents.NewPackageId = item.NewPackageId;
                        //newComponents.NewComponentName = bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(item.NewComponentId)) == null ? "" : bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(item.NewComponentId)).Description;
                        newComponents.NewComponentName = bundlePackages.PgmBdlPckComponents.FirstOrDefault(a => a.KenanCode.Contains(item.NewComponentId)) == null ? "" : bundlePackages.PgmBdlPckComponents.FirstOrDefault(a => a.KenanCode.Contains(item.NewComponentId)).Description;

                        //newComponents.Type = bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(item.NewComponentId)) == null ? "" : bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(item.NewComponentId)).LinkType
                        newComponentsList.Add(newComponents);
                    }
            }

            componentViewModel.NewPackagesList = newPackagesList;
            componentViewModel.NewComponentsList = newComponentsList;
            componentViewModel.OldPackagesList = oldPackagesList;
            componentViewModel.OldComponentsList = oldComponentsList;

            //response.OldComponentsList = oldComponentsList;
            //response.OldPackagesList = oldPackagesList;
            //var resp = new SmartRegistrationService.SmartGetPackageDetailsResp();

            //using (var _proxy = new SmartRegistrationServiceProxy())
            //{
            //    //TODO: need to save ISSIMRequired to d/b
            //    //collection["ISSIMRequired"]
            //    resp = _proxy.SelectedPackageDetail(strNewPackageId.ToInt());


            //}                
            //List<string> pkgids = new List<string>();
            //using (var _Proxy = new SmartCatelogServiceProxy())
            //{
            //    if (!string.IsNullOrEmpty(resp.PackageDetails.MasterPackageId) && !string.IsNullOrEmpty(resp.PackageDetails.ExtraPackageId))
            //    {
            //        pkgids.Add(resp.PackageDetails.MasterPackageId);
            //        pkgids.Add(resp.PackageDetails.ExtraPackageId);
            //        bundlePackages = _Proxy.PgmBdlPckComponentGetForSmartByKenanCode(pkgids).ToList();

            //    }
            //}

            //if (bundlePackages.Count > 0)
            //{
            //    NewPackages objNewPackages;
            //    List<NewPackages> listNewPackages = new List<NewPackages>();

            //    foreach (var item in bundlePackages)
            //    {
            //        objNewPackages = new NewPackages();
            //        objNewPackages.NewPackageId = bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(pkgids[0].ToString2())) == null ? "" : bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(pkgids[0].ToString2())).
            //        ComponentViewModel Model = GetOldPackagesAndItsComponents();

            //        //ExtraPckgId == bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(pkgids[1].ToString2())) == null ? "" : bundlePackages.FirstOrDefault(a => a.KenanCode.Contains(pkgids[1].ToString2())).ID.ToString();
            //    }

            //}  
            req = componentViewModel;
            Session["Kenan_componentViewModel"] = componentViewModel;
        }

        //ComponentViewModel ViewModel = GetPackagesAndItsComponents();
        //Session["componentViewModel"] = ViewModel;
        ////BundlepackageResp respon = new BundlepackageResp();
        ////using (var proxy = new SmartCatelogServiceProxy())
        ////{
        ////    respon = proxy.GetMandatoryVas(Convert.ToInt32( Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]));
        ////}

        //return ViewModel;

        private ComponentViewModel GetSelectedPackageInfo_smart(int modelId, int packageId, string kenanCode)
        {
            ComponentViewModel pkgResults = new ComponentViewModel();
            List<ModelPackageInfo> listRes = null;
            List<int> Finalintvalues = null;
            using (var proxy = new SmartCatelogServiceProxy())
            {
                listRes = proxy.GetModelPackageInfo_smart(modelId, packageId, kenanCode, true);
            }

            #region add step to get Device Protection Component based on RRP price
            using (var proxy2 = new CatalogServiceProxy())
            {
                List<PkgDataPlanId> values = proxy2.getFilterplanvalues(3).values.ToList();
                //find the correct model id 

                string modelid = modelId.ToString2();
                if ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"]) != null)
                {
                    modelid = modelid == "0" ? ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString() : modelid;

                    Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();
                }
            }

            #endregion

            List<NewPackages> listNewPackages = new List<NewPackages>();
            NewComponents objNewComponents;
            List<NewComponents> listNewComponents = new List<NewComponents>();

            NewPackages objNewPackagesPC = new NewPackages();
            NewPackages objNewPackagesMA = new NewPackages();
            NewPackages objNewPackagesEC = new NewPackages();
            NewPackages objNewPackagesDC = new NewPackages();
            NewPackages objNewPackagesIC = new NewPackages();
            if (listRes != null)
            {
                foreach (var item in listRes)
                {

                    if (item.packageType == "PC")
                    {
                        objNewPackagesPC.NewPackageId = item.PackageId.ToString2();
                        objNewPackagesPC.NewPackageName = item.PackageName;
                        objNewPackagesPC.Type = item.packageType;

                        objNewComponents = new NewComponents();
                        objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        objNewComponents.NewComponentName = item.ComponentName;
                        objNewComponents.NewPackageId = item.PackageId.ToString2();
                        objNewComponents.NewPackageKenanCode = item.KenanCode;
                        objNewComponents.ReassignIsmandatory = item.IsMandatory;
                        objNewComponents.ComponentGroupId = item.ComponentGroupId;
                        objNewComponents.ComponentGroupName = item.ComponentGroupName;
                        listNewComponents.Add(objNewComponents);

                    }
                    if (item.packageType == "MA")
                    {
                        objNewPackagesMA.NewPackageId = item.PackageId.ToString2();
                        objNewPackagesMA.NewPackageName = item.PackageName;
                        objNewPackagesMA.Type = item.packageType;

                        objNewComponents = new NewComponents();
                        objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        objNewComponents.NewComponentName = item.ComponentName;
                        objNewComponents.NewPackageId = item.PackageId.ToString2();
                        objNewComponents.NewPackageKenanCode = item.KenanCode;
                        objNewComponents.IsChecked = item.IsMandatory;
                        objNewComponents.ComponentGroupId = item.ComponentGroupId;
                        objNewComponents.ComponentGroupName = item.ComponentGroupName;
                        listNewComponents.Add(objNewComponents);

                    }
                    if (item.packageType == "EC")
                    {
                        objNewPackagesEC.NewPackageId = item.PackageId.ToString2();
                        objNewPackagesEC.NewPackageName = item.PackageName;
                        objNewPackagesEC.Type = item.packageType;

                        objNewComponents = new NewComponents();
                        objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        objNewComponents.NewComponentName = item.ComponentName;
                        objNewComponents.NewPackageId = item.PackageId.ToString2();
                        objNewComponents.NewPackageKenanCode = item.KenanCode;
                        objNewComponents.IsChecked = item.IsMandatory;
                        objNewComponents.ComponentGroupId = item.ComponentGroupId;
                        objNewComponents.ComponentGroupName = item.ComponentGroupName;
                        listNewComponents.Add(objNewComponents);

                    }
                    //**************************************************************//
                    if (item.packageType == "DC" || item.packageType == "MD") 
                    {
                        if (item.packageType == "DC")
                        {
                            TempData["DataComponentPkgKenancode"] = item.KenanCode;
                            TempData["DataComponentPkgId"] = item.PackageId.ToString2();
                        }
                        else if (item.packageType == "MD")
                        {
                            //getting data package id for Manadatory Data components
                            using (var proxy = new CatalogServiceProxy())
                            {
                                BundlepackageResp respon = new BundlepackageResp();
                                respon = proxy.GetMandatoryVas(Convert.ToInt32(item.PackageId));
                                Session["DataComponentPkgKenancode"] = MasterDataCache.Instance.PackageComponents.Where(a => a.ID == respon.values.Where(b => b.Plan_Type == "DC").ToList()[0].BdlDataPkgId.ToInt()).Select(a => a.KenanCode).FirstOrDefault();
                            }
                        }
                        //**************************************************************//
                        objNewPackagesDC.NewPackageId = item.PackageId.ToString2();
                        objNewPackagesDC.NewPackageName = item.PackageName;
                        objNewPackagesDC.Type = item.packageType;

                        objNewComponents = new NewComponents();
                        //objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        //objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        //objNewComponents.NewComponentName = item.ComponentName;
                        //objNewComponents.NewPackageId = item.PackageId.ToString2();
                        //objNewComponents.NewPackageKenanCode = item.KenanCode;
                        //objNewComponents.IsChecked = item.IsMandatory;
                        //objNewComponents.ComponentGroupId = item.ComponentGroupId;
                        //objNewComponents.ComponentGroupName = item.ComponentGroupName;
                        //listNewComponents.Add(objNewComponents);

                        objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        objNewComponents.NewComponentName = item.ComponentName;
                        objNewComponents.NewPackageId = item.PackageId.ToString2();
                        objNewComponents.NewPackageKenanCode = item.KenanCode;
                        objNewComponents.ReassignIsmandatory = item.IsMandatory;
                        objNewComponents.ComponentGroupId = item.ComponentGroupId.ToString2();
                        objNewComponents.ComponentGroupName = item.ComponentGroupName;
                        objNewComponents.IsChecked = item.IsMandatory;
                        objNewComponents.Type = item.packageType;
                        objNewComponents.isDefault = item.isDefault;
                        listNewComponents.Add(objNewComponents);

                    }
                    if (item.packageType == "IC")
                    {
                        if (!ReferenceEquals(item.ComponentId, null) && !ReferenceEquals(Finalintvalues, null) && Finalintvalues.Contains(item.ComponentId.ToInt()))
                        {
                            objNewPackagesIC.NewPackageId = item.PackageId.ToString2();
                            objNewPackagesIC.NewPackageName = item.PackageName;
                            objNewPackagesIC.Type = item.packageType;
                            objNewComponents = new NewComponents();
                            objNewComponents.NewComponentId = item.ComponentId.ToString2();
                            objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                            objNewComponents.NewComponentName = item.ComponentName;
                            objNewComponents.NewPackageId = item.PackageId.ToString2();
                            objNewComponents.NewPackageKenanCode = item.KenanCode;
                            objNewComponents.ReassignIsmandatory = item.IsMandatory;
                            objNewComponents.Type = item.packageType;
                            objNewComponents.ComponentGroupId = item.ComponentGroupId.ToString2();
                            objNewComponents.ComponentGroupName = item.ComponentGroupName;
                            objNewComponents.IsChecked = item.isDefault > 0 ? true : false;
                            objNewComponents.isDefault = item.isDefault > 0 ? item.isDefault : 0;
                            listNewComponents.Add(objNewComponents);
                        }
                    }
                }


                listNewPackages.Add(objNewPackagesPC);
                listNewPackages.Add(objNewPackagesMA);
                listNewPackages.Add(objNewPackagesEC);
                listNewPackages.Add(objNewPackagesDC);
                listNewPackages.Add(objNewPackagesIC);

                pkgResults.NewComponentsList = listNewComponents;
                pkgResults.NewPackagesList = listNewPackages;
            }

            return pkgResults;

        }

        private List<NewComponents> CheckAndUncheckBasedOnVasRetention(List<OldComponents> oldComponents, List<NewComponents> VasResults, List<NewComponents> DBResults)
        {
            string[] oldComponentsKenanList = oldComponents.Select(a => a.OldComponentId).ToArray();
            string[] VASRetainKenanList = VasResults.Select(a => a.NewComponentId).ToArray();
            bool reainAllOldComponents = false;
            if (ConfigurationManager.AppSettings["ReainAllOldComponents"] != null && ConfigurationManager.AppSettings["ReainAllOldComponents"].ToString().ToUpper() == "TRUE")
                reainAllOldComponents = true;

            foreach (NewComponents item in DBResults)
            {
                if (VASRetainKenanList.Contains(item.NewComponentKenanCode))
                    item.IsChecked = true;
                else
                    item.IsChecked = false;
                if (reainAllOldComponents && oldComponentsKenanList.Contains(item.NewComponentKenanCode))
                    item.IsChecked = true;
            }
            return DBResults;
        }

        private int getCPPackegeIdByKenanCode(string KenanCode)
        {
            int pkgId = 0;
            //var resp = new RegistrationSvc.SmartGetPackageDetailsResp();
            var resp = new RegistrationSvc.GetPackageDetailsResp();
            string strNewPackageId = KenanCode;
            //using (var _proxy = new SmartRegistrationServiceProxy())
            //{                
            //    //TODO: need to save ISSIMRequired to d/b
            //    //collection["ISSIMRequired"]
            //    resp = _proxy.SelectedPackageDetail(strNewPackageId.ToInt());


            //}
            //var bundlePackages = new List<PgmBdlPckComponentSmart>();
            //List<string> pkgids = new List<string>();
            //using(var _Proxy=new SmartCatelogServiceProxy())
            //{
            //     if (!string.IsNullOrEmpty(resp.PackageDetails.MasterPackageId) && !string.IsNullOrEmpty(resp.PackageDetails.ExtraPackageId))
            //    {                     
            //         pkgids.Add(resp.PackageDetails.MasterPackageId);
            //         pkgids.Add(resp.PackageDetails.ExtraPackageId);
            //         bundlePackages= _Proxy.PgmBdlPckComponentGetForSmartByKenanCode(pkgids).ToList();
            //    }              
            //}


            //var bundlePackages = new List<PgmBdlPckComponentSmart>();
            var bundlePackages = new PgmBdlPckComponentGetResp();
            List<string> pkgids = new List<string>();
            using (var _Proxy = new SmartCatelogServiceProxy())
            {
                pkgids.Add(KenanCode);
                bundlePackages = _Proxy.PgmBdlPckComponentGetForSmartByKenanCode(pkgids);
            }

            if (bundlePackages.PgmBdlPckComponents.Count() > 0)
            {
                pkgId = bundlePackages.PgmBdlPckComponents.FirstOrDefault(a => a.PlanType == "CP").ID;
            }
            return pkgId;
        }

        private List<RegSmartComponents> GetSelectedComponents(string regId)
        {
            List<RegSmartComponents> listRegSmartComponents = new List<RegSmartComponents>();

            using (var proxy = new SmartCatelogServiceProxy())
            {

                listRegSmartComponents = proxy.GetSelectedComponents(regId);
            }

            return listRegSmartComponents;

        }

        #endregion
        private ContractDetails GetContractDetails()
        {
            SubscriberICService.PackageModel package = new SubscriberICService.PackageModel();
            IList<SubscriberICService.PackageModel> packages;
            ContractDetails ContractDet = new ContractDetails();



            Online.Registration.Web.SmartViewModels.DateDifference datediff;

            Session["CMSSResponse"] = null;
            Session["ErrMsg"] = string.Empty;
            Session["ContractType"] = null;
            Session["Penalty"] = null;
            Session[SessionKey.CMSID.ToString()] = null;
            Session["isExpired"] = null;
            Session["IsExistingK2"] = null;

            if (!ReferenceEquals(Request.QueryString["KenanID"], null) && !ReferenceEquals(Request.QueryString["MSISDN"], null))
            {
                MSISDN = Request.QueryString["MSISDN"].ToString();
                Session["KenanAccountNo"] = Request.QueryString["KenanID"].ToString();
                Session[SessionKey.RegMobileReg_MSISDN.ToString()] = Request.QueryString["MSISDN"].ToString();
                Session["SubscriberNo"] = Request.QueryString["SubscriberNo"].ToString();
                Session["SubscriberNoResets"] = Request.QueryString["SubscriberNoResets"].ToString();
                Session[SessionKey.ExternalID.ToString()] = MSISDN;
            }
            else
            {
                if (Session[SessionKey.ExternalID.ToString()] != null)
                    MSISDN = Session[SessionKey.ExternalID.ToString()].ToString();
            }

            //  GetPersonalInformationOnMSISDN(MSISDN);
            packages = GetPackageDetails(MSISDN, ConfigurationManager.AppSettings["KenanMSISDN"]);
            GetExtendPlans();
            SmartRegistrationServiceProxy proxy = new SmartRegistrationServiceProxy();
            IEnumerable<Registration.DAL.Models.Contracts> lstContracts = proxy.GetContractsList().ToList();
            IEnumerable<string> lstContractsKenan = lstContracts.Select(c => c.KenanCode);
            int contractMonths = 0;
            bool isExpired = false;
            List<ContractDetails> ContractsList = new List<ContractDetails>();

            //foreach (var contract in packages)            
            //{
            //    ContractDetails Contract = new ContractDetails();
            //    ContractDet.PackageID = contract.PackageID;
            //    ContractDet.PackageDesc = contract.PackageDesc;
            //    for (int i = 0; i < contract.compList.Count; i++)
            //    {

            //        if (lstContractsKenan.Contains(contract.compList[i].componentId))
            //        {
            //            contractMonths = 0;
            //            ContractDet.ActiveDate = DateTime.ParseExact(contract.compList[i].componentActiveDt.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            //            if (ContractDet.ActiveDate != null)
            //                ContractDet.ContractStartDate = ContractDet.ActiveDate.ToString("dd-MMM-yyyy");
            //            contractMonths = lstContracts.Where(c => c.KenanCode == contract.compList[i].componentId).Select(cd => cd.Duration).Single();
            //            ContractDet.EndDate = ContractDet.ActiveDate.AddMonths(contractMonths);
            //            ContractDet.EndDate = DateTime.ParseExact(ContractDet.EndDate.ToString("yyyyMMdd"), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            //            ContractDet.ContractEndDate = ContractDet.EndDate.ToString("dd-MMM-yyyy");
            //            datediff = new Online.Registration.Web.SmartViewModels.DateDifference(DateTime.Now, ContractDet.EndDate);
            //            //ContractDet.ContractRemaining = "Contract Remaining for: " + datediff.Years + " Years " + datediff.Months + " Months " + datediff.Days + " Days ";
            //            ContractDet.ContractDuration = Convert.ToInt16(datediff.Years) * 12 + Convert.ToInt16(datediff.Months);
            //            if (datediff.Days > 15)
            //            {
            //                ContractDet.ContractDuration = ContractDet.ContractDuration + 1;
            //            }
            //            ContractDet.ContractRemaining = ContractDet.ContractDuration.ToString2() + " Months";
            //            ContractDet.ComponentDesc = contract.compList[i].componentDesc;
            //            if (DateTime.Compare(ContractDet.EndDate, DateTime.Now) < 0)
            //            {
            //                isExpired = true;
            //            }
            //            else
            //            {
            //                // added by Raj for to show All Active Contracts
            //                Contract.ComponentDesc = ContractDet.ComponentDesc;
            //                Contract.ActiveDate = ContractDet.ActiveDate;
            //                if (ContractDet.ActiveDate != null)
            //                    Contract.ContractStartDate = ContractDet.ActiveDate.ToString("dd-MMM-yyyy");
            //                Contract.EndDate = ContractDet.EndDate;
            //                if (ContractDet.EndDate != null)
            //                    Contract.ContractEndDate = ContractDet.EndDate.ToString("dd-MMM-yyyy");
            //                Contract.ContractRemaining = ContractDet.ContractRemaining;
            //                Contract.ContractDuration = ContractDet.ContractDuration;
            //                Contract.ContractDurationinMonths = Contract.ContractDuration.ToString2() + " Months";
            //                ContractsList.Add(Contract);
            //            }

            //        }

            //    }
            //}

            ///////////////
            int ContractRemainMonths = 0;
            List<SubscriberICService.retrievePenalty> penaltybyMSISDN = new List<SubscriberICService.retrievePenalty>();
            List<string> lstIDs = new List<string>();
            using (RegistrationServiceProxy registrationServiceProxy = new RegistrationServiceProxy())
                lstIDs = registrationServiceProxy.getRebateContractIDs();
            using (var Smartproxy = new retrieveSmartServiceInfoProxy())
            {
                Session["penaltybyMSISDN"] = penaltybyMSISDN = Smartproxy.retrievePenalty(MSISDN, ConfigurationManager.AppSettings["KenanMSISDN"], Session["SubscriberNo"].ToString2(), Session["SubscriberNoResets"].ToString2(), userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
                       password: Properties.Settings.Default.retrieveAcctListByICResponsePassword).Where(p => !lstIDs.Contains(p.ContractName)).ToList();
            }

            Items Item = WebHelper.Instance.PPIDInfo(new WebHelper.PPIDFilter() { ExternalId = MSISDN, EliminateDStatusLines = true }).FirstOrDefault();
            String libforwaiver = Util.getMarketCodeLiberalisationMOC(((retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails")), "LIBERALISATION");
            
            List<Online.Registration.DAL.Models.Contracts> lstContractsAll = new List<Contracts>();

            using (SmartRegistrationServiceProxy Smartproxy = new SmartRegistrationServiceProxy())
            {
                lstContractsAll = Smartproxy.GetContractsList().ToList();
            }
            foreach (var item in penaltybyMSISDN)
            {
                ContractDetails Contract = new ContractDetails();
                ContractDet.ActiveDate = DateTime.ParseExact(item.startDate.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                ContractDet.EndDate = DateTime.ParseExact(item.endDate.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                datediff = new Online.Registration.Web.SmartViewModels.DateDifference(DateTime.Now, ContractDet.EndDate);
                if (ContractDet.ActiveDate > DateTime.Now)
                {

                    datediff = new Online.Registration.Web.SmartViewModels.DateDifference(ContractDet.ActiveDate, ContractDet.EndDate);
                }
                else
                {
                    datediff = new Online.Registration.Web.SmartViewModels.DateDifference(DateTime.Now, ContractDet.EndDate);
                }

                ContractDet.ContractDuration = Convert.ToInt16(datediff.Years) * 12 + Convert.ToInt16(datediff.Months);

                if (datediff.Days >= 15)
                {
                    ContractDet.ContractDuration = ContractDet.ContractDuration + 1;
                }

                //Evan-150323 Change for ContractCheck sync with Kenan - Start
                //ContractDet.ContractRemaining = ContractDet.ContractDuration.ToString2() + " Months";
                ContractDet.ContractRemaining = Util.getDiffDate(ContractDet.EndDate, ContractDet.ActiveDate).ToString2();
                //Evan-150323 Change for ContractCheck sync with Kenan - End
                ContractDet.ComponentDesc = item.ContractName;
                if (DateTime.Compare(ContractDet.EndDate, DateTime.Now) > 0)
                {
                    Contract.ComponentDesc = ContractDet.ComponentDesc;
                    Contract.ActiveDate = ContractDet.ActiveDate;
                    if (ContractDet.ActiveDate != null)
                        Contract.ContractStartDate = ContractDet.ActiveDate.ToString("dd-MMM-yyyy");
                    Contract.EndDate = ContractDet.EndDate;
                    if (ContractDet.EndDate != null)
                        Contract.ContractEndDate = ContractDet.EndDate.ToString("dd-MMM-yyyy");
                    Contract.ContractRemaining = ContractDet.ContractRemaining;
                    Contract.ContractDuration = (ContractDet.EndDate.Subtract(ContractDet.ActiveDate).TotalDays / 30).ToInt();

                    //var pendingDays = ContractDet.EndDate.Subtract(DateTime.Now).TotalDays;
                    Regex digitsOnly = new Regex(@"[^\d]");
                    var pendingDays = digitsOnly.Replace(Contract.ContractRemaining, "").ToInt();
                    //Evan-150323 Change for ContractCheck sync with Kenan - Start
                    //Contract.ContractRemaining = Math.Round(pendingDays / 30.0, 0) + " Months";
                    //Evan-150323 Change for ContractCheck sync with Kenan - End
                    Contract.Penalty = item.penalty.ToDecimal();
                    //var pendingMonths = (Math.Round(pendingDays / 30.0, 0));

                    int gpFor12 = 2;
                    int gpFor24 = 4;
                    if (ConfigurationManager.AppSettings["gpFor12"] != null)
                    {
                        int.TryParse(System.Configuration.ConfigurationManager.AppSettings["gpFor12"], out gpFor12);
                    }
                    if (ConfigurationManager.AppSettings["gpFor24"] != null)
                    {
                        int.TryParse(ConfigurationManager.AppSettings["gpFor24"], out gpFor24);
                    }
                    //Contract.ContractCustLib = Item != null && Item.Customer != null && !String.IsNullOrEmpty(Item.Customer.RiskCategory.ToString2()) ? Item.Customer.RiskCategory.ToString2() : (!String.IsNullOrEmpty(libforwaiver) ? libforwaiver : "");
                    Contract.ContractCustLib = !String.IsNullOrEmpty(libforwaiver) ? libforwaiver : "";
                    bool bPenaltyWaiverForLib = WebHelper.Instance.GetPenaltyWaiveOffForLiberalization(Contract.ContractCustLib,Item.Customer.NationalityID,false);
                    if (!string.IsNullOrWhiteSpace(Contract.ContractCustLib) && bPenaltyWaiverForLib)
                    {
                        if ((ContractDet.EndDate.Subtract(ContractDet.ActiveDate).TotalDays / 30).ToInt() == 12)
                        {
                            Contract.Waived = pendingDays <= gpFor12;
                        }
                        else if ((ContractDet.EndDate.Subtract(ContractDet.ActiveDate).TotalDays / 30).ToInt() == 24)
                        {
                            Contract.Waived = pendingDays <= gpFor24;
                        }
                    }
                    Contract.PenaltyWaiveOff = Contract.Waived ? Contract.Penalty : 0;

                    ContractsList.Add(Contract);
                    // Support for UAT PN contract Extension for should not exeed 30 minths above(Raj)
                    ContractRemainMonths += ContractDet.ContractDuration;
                    //End Support for UAT PN contract Extension for should not exeed 30 minths above(Raj)
                }
            }

            ///////////
            //}

            if (isExpired)
            {
                ContractDet = null;
                Session["Penalty"] = null;
            }

            if (ContractDet != null && !isExpired)
            {
                if (ContractDet.ComponentDesc != null)
                {

                    Session["ErrMsg"] = string.Empty;
                    if (ContractRemainMonths > 18)
                        ContractDet.ContractExtend = "N";
                    else if (ContractRemainMonths <= 6)
                    {
                        ContractDet.ContractExtend = "Y";
                        ContractDet.ContractExtendType = "B";
                    }
                    else if (ContractRemainMonths <= 18 && ContractRemainMonths > 6)
                    {
                        ContractDet.ContractExtend = "Y";
                        ContractDet.ContractExtendType = "12";
                    }
                    else if (ContractRemainMonths > ConfigurationManager.AppSettings["TotalContractDurationinMonths"].ToInt())
                    {
                        ContractDet.ContractExtend = "N";
                    }

                }
                else
                {
                    Session["ErrMsg"] = ConfigurationManager.AppSettings["NoContracts"];
                    Session["ErrMsg"] = Session["ErrMsg"].ToString().Replace("[MSISDN]", MSISDN);
                }
            }

            else
            {
                Session["ErrMsg"] = ConfigurationManager.AppSettings["NoContracts"];
                Session["ErrMsg"] = Session["ErrMsg"].ToString().Replace("[MSISDN]", MSISDN);
            }
            bool hasivalueplan = false;
            if (ContractDet != null)
            {
                if (ContractDet.ContractExtend != null)
                {
                    if (ContractDet.ContractExtend.ToUpper() == "Y")
                    {
                        foreach (var contract in packages)
                        {
                            if (contract.PackageDesc.ToLower().IndexOf("ivalue") > 0 && contract.PackageDesc.ToLower().IndexOf("mandatory") > 0)
                            {
                                hasivalueplan = true;
                            }
                        }
                    }
                }

                if (!hasivalueplan)
                    ContractDet.ContractExtend = "N";

				ContractDet.PenaltyType = penaltybyMSISDN.FirstOrDefault() != null ? penaltybyMSISDN.FirstOrDefault().penaltyType : string.Empty ;

            }
            #region Extend Based on MOC status PN By Raj
            int ActiveContractsCount = 0;
            if (ContractsList != null && ContractsList.Count() > 0)
            {
                ActiveContractsCount = ContractsList.Count;
            }
            //retrieveAcctListByICResponse AcctListByICResponse = null;
            // if (!ReferenceEquals(SessionManager.Get("PPIDInfo"), null))
            //{
            // AcctListByICResponse = (retrieveAcctListByICResponse)SessionManager.Get("PPIDInfo");
            // var Item = AcctListByICResponse.itemList.Where(a => a.ExternalId == MSISDN).FirstOrDefault();
            if (Item != null && Item.Customer != null && Item.Customer.Moc != string.Empty && !ReferenceEquals(Item.Customer.Moc, null))
            {
                if (ActiveContractsCount >= 3)
                {
                    if (ContractDet != null)
                        ContractDet.ContractExtend = "N";
                }
            }
            else
            {
                if (ActiveContractsCount >= 2)
                {
                    if (ContractDet != null)
                        ContractDet.ContractExtend = "N";
                }
            }

            //}

            #endregion
            if (isExpired)
            {
                ContractDet = null;
                Session["isExpired"] = "true";
            }
            Session["ContractDet"] = ContractDet;
            Session[SessionKey.ContractDetails.ToString()] = ContractsList;
            Session["Package"] = packages;

            return ContractDet;

        }
        public List<int> GetDepenedencyComponents(string componentIds)
        {
            List<int> depCompIds = new List<int>();

            using (var proxy = new SmartCatelogServiceProxy())
            {
                depCompIds = proxy.GetAllDependentComponents(Convert.ToInt32(componentIds), 0);
            }

            return depCompIds;
        }

        public ActionResult DevicePlanDetails(string articleId, int modelId)
        {

            Online.Registration.Web.CatalogSvc.GetDeviceImageDetailsRespSmart deviceDetails = new GetDeviceImageDetailsRespSmart();
            using (var catalogProxy = new CatalogServiceClient())
            {
                deviceDetails = catalogProxy.GetDeviceImageDetailsForSmart(articleId, modelId);
            }

            return View(deviceDetails);
        }

        [HttpPost]
        public string GetSuppLineTenure(string strexternalId, string strsubscrNo, string strsubscrNoResets)
        {
            int suppLineTenure = 0;

            SubscriberRetrieveServiceInfoResponse ServiceInfoResponse = null;
            var proxy = new retrieveSmartServiceInfoProxy();
            ServiceInfoResponse = proxy.retrieveServiceInfo(new SubscriberRetrieveServiceInfoRequest
            {
                externalId = strexternalId,
                subscrNo = strsubscrNo,
                subscrNoResets = strsubscrNoResets
            });

            if (!ReferenceEquals(ServiceInfoResponse, null))
            {
                if (!ServiceInfoResponse.serviceActiveDt.Equals(DateTime.MinValue))
                {
                    Online.Registration.Web.SmartViewModels.DateDifference datediff;
                    //datediff = new Online.Registration.Web.SmartViewModels.DateDifference(DateTime.Now, ServiceInfoResponse.serviceActiveDt);
                    DateTime Date1 = DateTime.Now;
                    DateTime Date2 = ServiceInfoResponse.serviceActiveDt;
                    suppLineTenure = (Date1.Year * 12 + Date1.Month) - (Date2.Year * 12 + Date2.Month);

                    //if (!string.IsNullOrEmpty(ServiceInfoResponse.packageId))
                    //{
                    //    var svcproxy = new SmartCatelogServiceProxy();
                    //    int contractDuration = svcproxy.GetContractDurationByKenanCode(ServiceInfoResponse.packageId);
                    //    suppLineTenure = contractDuration - completedPeriod;
                    //}
                }
            }
            return suppLineTenure.ToString();
        }

        [HttpPost]
        public string CheckContract(string ExternalID, string KenanID, string SubscriberNo, string SubscriberNoResets)
        {
            MSISDN = ExternalID;
            Session["KenanAccountNo"] = KenanID;
            Session[SessionKey.RegMobileReg_MSISDN.ToString()] = ExternalID;
            Session["SubscriberNo"] = SubscriberNo;
            Session["SubscriberNoResets"] = SubscriberNoResets;
            Session[SessionKey.ExternalID.ToString()] = MSISDN;

            ContractDetails ContractDet = new ContractDetails();
            ContractDet = GetContractDetails();
            bool result = false;
            if (ContractDet != null)
            {
                result = (ContractDet.ComponentDesc != null) ? true : false;
            }
            return result.ToString();
        }

    }


}



