﻿using System.Web.Mvc;

namespace Online.Registration.Web.Areas.Retention
{
    public class RetentionAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Retention";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Retention_default",
                "Retention/{controller}/{action}/{id}",
                new { action = "CmssAndContractCheck", id = UrlParameter.Optional }
            );
        }
    }
}
