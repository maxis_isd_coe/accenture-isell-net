﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using log4net;
//using Online.Registration.DAL.Models; 
using Online.Registration.DAL.Models;
//using Online.Registration.Web.SmartCatelogService;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.Controllers;
//using Online.Registration.Web.ViewModels;
using Online.Registration.Web.Helper;
//using Online.Registration.Web.SmartKenanService;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.Models;
//using Online.Registration.Web.RetrieveServiceInfo;
//using Online.Registration.Web.PrinSuppSvc;
using Online.Registration.Web.Properties;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.SmartViewModels;
//using Online.Registration.Web.StoreServiceSvc;
//using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.SubscriberICService;
using SNT.Utility;
namespace Online.Registration.Web.Areas.Storekeeper.Controllers
{
    [Authorize]
    //[HandleError(View = "GenericError")]
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class StorekeeperController : Controller
    {
        int ID;
        Int32 discount = 0;
        string status = string.Empty;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(RegistrationController));
        private string dateFormat = "d MMM yyyy";
        ContractModels ContractModel = new ContractModels();

        protected override void Initialize(RequestContext requestContext)
        {
            Util.CheckSessionAccess(requestContext);
            base.Initialize(requestContext);
        }

        private void LogException(Exception ex)
        {
            Logger.DebugFormat("Exception: {0}", ex.Message);
        }

        //UNUSED METHODS
        //private RedirectToRouteResult RedirectException(Exception ex)
        //{
        //    Util.SetSessionErrMsg(ex);
        //    return RedirectToAction("Error", "Home");
        //}

        #region code uncommented by VLT
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]


        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]

        public ActionResult SelectDevice(int? type)
        {
            Session["DiscountStatus"] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session["NetPrice"] = null;
            Session["DPrice"] = null;
            Session["isPenaltyCheck"] = null;
            ResetAllSession(type);

            SetRegStepNo(MobileRegistrationSteps.Device);

            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();
            var deviceOptVM = GetAvailableDeviceOptions();

            return View(deviceOptVM);
        }
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectDeviceCatalog()
        {
            SetRegStepNo(MobileRegistrationSteps.DeviceCatalog);

            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();
            var phoneVM = GetAvailableModelImage();

            return View(phoneVM);
        }
        # endregion code commented by VLT ends here

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectPlan(int type = 0)
        {
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
            {
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            }

            if (type > 0)
            {
                ResetAllSession(type);
            }
            Session["isPenaltyCheck"] = null;
            #region Added by Chetan for hiding the contrats & vas in planonly
            if (type == 2)
            {
                Session[SessionKey.MobileRegType.ToString()] = "Planonly";
            }
            else
            {
                Session[SessionKey.MobileRegType.ToString()] = "Other";
            }
            #endregion

            SetRegStepNo(MobileRegistrationSteps.Plan);

            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();
            GetExtendPlans();
            var packageVM = GetAvailablePackages();

            return View(packageVM);
        }

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectVAS()
        {
            Session["isPenaltyCheck"] = null;
            ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
            if (Session[SessionKey.PPID.ToString()].ToString() != "")
            {
                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null))
                {
                    Session.Contents.Remove("RedirectURL");
                    Session[SessionKey.MenuType.ToString()] = "1";
                }
                SetRegStepNo(MobileRegistrationSteps.Vas);
                if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] == null || (Convert.ToString(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]) == String.Empty))
                {
                    Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = Session["GuidedSales_PkgPgmBdlPkgCompID"];
                }
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();


                Lstvam = GetAvailablePackageComponents(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), isMandatory: false);

                string MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();

                Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);

                Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);

                var Ivalexists = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).PlanBundle;

                /* For Mandatory packages */
                Session["vasmandatoryids"] = "";
                using (var proxy = new SmartCatelogServiceProxy())
                {

                    BundlepackageResp respon = new BundlepackageResp();

                    respon = proxy.GetMandatoryVas(Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]));

                    for (int i = 0; i < respon.values.Count(); i++)
                    {

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Master_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            Session["intmandatoryidsval"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
                        }

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.CompType_DataCon)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value

                                                             };


                            Session["intdataidsval"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts);
                        }
                        /*Extra packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Extra_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value

                                                             };


                            Session["intextraidsval"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);

                            //Session["intextraidsval"] = respon.values[i].BdlDataPkgId;

                            //Lstvam2.ExtraComponents.AddRange(Lstvam.AvailableVASes);
                        }

                    }




                }


                if (MobileRegType == "Planonly")
                {
                    //vasVM.AvailableVASes.Clear();
                    Lstvam.ContractVASes.Clear();

                }


                return View(Lstvam2);

            }
            else
            {
                Session[SessionKey.RedirectURL.ToString()] = "Registration/SelectVAS";
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        [HandleError]
        [ValidateInput(false)]
        public ActionResult SelectMobileNo(string desireNo = "")
        {
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();
            SetRegStepNo(MobileRegistrationSteps.MobileNo);
            var mobileNos = new MobileNoSelectionResponse();

            // **************** Dealer Number ******************
            using (var proxy = new SmartKenanServiceProxy())
            {
                mobileNos = proxy.MobileNoSelection(new MobileNoSelectionRequest()
                {
                    DesireNo = desireNo
                });
            }
            // **************************************************

            var mobileNoVM = new MobileNoVM()
            {
                AvailableMobileNos = mobileNos.MobileNos.ToList(),
                NoOfDesireMobileNo = Properties.Settings.Default.NoOfDesireMobileNo
            };

            if (((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()]) != null)
            {
                mobileNoVM.SelectedMobileNumber = ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()]);
            }

            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
            if (sublines != null)
                mobileNoVM.NoOfSubline = sublines.Count();

            return View(mobileNoVM);
        }

        private OrderSummaryVM ConstructOrderSummary(bool fromSubline = false)
        {
            string strDataPlanID;
            string strContractId;
            var components = new List<Component>();
            /*Commented for articleid*/
            //var modelImage = new ModelImage();
            var brandArticleImage = new BrandArticleSmart();
            var package = new Package();
            var bundle = new Bundle();
            var orderVM = new OrderSummaryVM();
            var phoneVM = (PhoneVM)Session["RegMobileReg_PhoneVM"];
            int varDataPlanId = 0;
            var personalDetailsVM = Session["RegMobileReg_PersonalDetailsVM"] == null ? new PersonalDetailsVM() : (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            // Main Line
            if (!fromSubline)
            {
                if ((OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()] != null)
                    orderVM = (OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()];
                //added by deepika 10 march
                Session["ModelID"] = orderVM.ModelID;
                if (Session["RegMobileReg_DataPkgID"] != null)
                {

                    strDataPlanID = Session["RegMobileReg_DataPkgID"].ToString();
                    // if(strDataPlanID.

                    if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] != null) && (Session[SessionKey.MobileRegType.ToString()].ToString() != "Planonly"))
                    {
                        strContractId = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString();

                        using (var proxys = new SmartCatelogServiceProxy())

                            varDataPlanId = proxys.GettllnkContractDataPlan(Convert.ToInt32(strContractId));


                        //added by deepika 10 march
                        if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID != 0 && strContractId.ToInt() != 0 && varDataPlanId.ToInt() != 0 || varDataPlanId.ToInt() == 0)
                        {
                            using (var proxy = new SmartCatelogServiceProxy())
                            {
                                List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
                                varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, strContractId.ToInt(), varDataPlanId.ToInt() }).ToList();
                                for (int i = 0; i < varAdvDeposit.Count; i++)
                                    if (varAdvDeposit[i] != null && varAdvDeposit[i].modelId != 0)
                                    {
                                        var Malaytotalprice = varAdvDeposit[i].malyDevAdv + varAdvDeposit[i].malayPlanAdv + varAdvDeposit[i].malayDevDeposit + varAdvDeposit[i].malyPlanDeposit;
                                        var Othertotalprice = varAdvDeposit[i].othDevAdv + varAdvDeposit[i].othPlanAdv + varAdvDeposit[i].othDevDeposit + varAdvDeposit[i].othPlanDeposit;
                                        orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
                                        orderVM.MalayAdvDevPrice = Malaytotalprice;
                                        orderVM.OthersAdvDevPrice = Othertotalprice;
                                        Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                        Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

                                        /*Added by chetan for split adv & deposit*/
                                        orderVM.MalyDevAdv = varAdvDeposit[i].malyDevAdv;
                                        orderVM.MalayPlanAdv = varAdvDeposit[i].malayPlanAdv;
                                        orderVM.MalayDevDeposit = varAdvDeposit[i].malayDevDeposit;
                                        orderVM.MalyPlanDeposit = varAdvDeposit[i].malyPlanDeposit;
                                        orderVM.OthDevAdv = varAdvDeposit[i].othDevAdv;
                                        orderVM.OthPlanAdv = varAdvDeposit[i].othPlanAdv;
                                        orderVM.OthDevDeposit = varAdvDeposit[i].othDevDeposit;
                                        orderVM.OthPlanDeposit = varAdvDeposit[i].othPlanDeposit;

                                        Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                        Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                        Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                        Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                        Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                        Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                        Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                        Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                        /*up to here*/

                                        if (personalDetailsVM.Customer.NationalityID != 0)
                                        {
                                            if (personalDetailsVM.Customer.NationalityID == 1)
                                            {
                                                /*Added by chetan for split adv & deposit*/
                                                Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                                Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                                Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                                Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                                /*up to here*/
                                                Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                                orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ModelPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
                                            }
                                            else
                                            {
                                                /*Added by chetan for split adv & deposit*/
                                                Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                                Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                                Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                                Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                                /*up to here*/
                                                Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
                                                orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ModelPrice != 0 ? orderVM.ModelPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
                                            }
                                        }
                                    }
                            }
                        }

                    }
                    else if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] == null) && (Session[SessionKey.MobileRegType.ToString()].ToString2().ToUpper() == "PLANONLY"))
                    {
                        //added by deepika 10 march
                        if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID == 0 && orderVM.ContractID == 0)
                        {
                            using (var proxy = new SmartCatelogServiceProxy())
                            {
                                var varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, orderVM.ContractID, 0 }).SingleOrDefault();
                                if (varAdvDeposit != null)
                                {
                                    var Malaytotalprice = varAdvDeposit.malyDevAdv + varAdvDeposit.malayPlanAdv + varAdvDeposit.malayDevDeposit + varAdvDeposit.malyPlanDeposit;
                                    var Othertotalprice = varAdvDeposit.othDevAdv + varAdvDeposit.othPlanAdv + varAdvDeposit.othDevDeposit + varAdvDeposit.othPlanDeposit;
                                    orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
                                    orderVM.MalayAdvDevPrice = Malaytotalprice;
                                    orderVM.OthersAdvDevPrice = Othertotalprice;
                                    Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                    Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

                                    /*Added by chetan for split adv & deposit*/
                                    orderVM.MalyDevAdv = varAdvDeposit.malyDevAdv;
                                    orderVM.MalayPlanAdv = varAdvDeposit.malayPlanAdv;
                                    orderVM.MalayDevDeposit = varAdvDeposit.malayDevDeposit;
                                    orderVM.MalyPlanDeposit = varAdvDeposit.malyPlanDeposit;
                                    orderVM.OthDevAdv = varAdvDeposit.othDevAdv;
                                    orderVM.OthPlanAdv = varAdvDeposit.othPlanAdv;
                                    orderVM.OthDevDeposit = varAdvDeposit.othDevDeposit;
                                    orderVM.OthPlanDeposit = varAdvDeposit.othPlanDeposit;

                                    Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                    Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                    Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                    Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                    Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                    Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                    Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                    Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                    /*up to here*/

                                    if (personalDetailsVM.Customer.NationalityID != 0)
                                    {
                                        if (personalDetailsVM.Customer.NationalityID == 1)
                                        {
                                            /*Added by chetan for split adv & deposit*/
                                            Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                            Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                            Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                            Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                            /*up to here*/
                                            Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                            orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ModelPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
                                        }
                                        else
                                        {
                                            /*Added by chetan for split adv & deposit*/
                                            Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                            Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                            Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                            Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                            /*up to here*/
                                            Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
                                            orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ModelPrice != 0 ? orderVM.ModelPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                else
                {
                    strDataPlanID = "0";
                    // if(strDataPlanID.

                    if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] != null) && (Session[SessionKey.MobileRegType.ToString()] != "Planonly"))
                    {
                        strContractId = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString();

                        using (var proxys = new SmartCatelogServiceProxy())

                            varDataPlanId = proxys.GettllnkContractDataPlan(Convert.ToInt32(strContractId));


                        //added by deepika 10 march
                        if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID != 0 && strContractId.ToInt() != 0 && varDataPlanId.ToInt() != 0 || varDataPlanId.ToInt() == 0)
                        {
                            using (var proxy = new SmartCatelogServiceProxy())
                            {
                                List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
                                varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, strContractId.ToInt(), varDataPlanId.ToInt() }).ToList();
                                for (int i = 0; i < varAdvDeposit.Count; i++)
                                    if (varAdvDeposit[i] != null && varAdvDeposit[i].modelId != 0)
                                    {
                                        var Malaytotalprice = varAdvDeposit[i].malyDevAdv + varAdvDeposit[i].malayPlanAdv + varAdvDeposit[i].malayDevDeposit + varAdvDeposit[i].malyPlanDeposit;
                                        var Othertotalprice = varAdvDeposit[i].othDevAdv + varAdvDeposit[i].othPlanAdv + varAdvDeposit[i].othDevDeposit + varAdvDeposit[i].othPlanDeposit;
                                        orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
                                        orderVM.MalayAdvDevPrice = Malaytotalprice;
                                        orderVM.OthersAdvDevPrice = Othertotalprice;
                                        Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                        Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

                                        /*Added by chetan for split adv & deposit*/
                                        orderVM.MalyDevAdv = varAdvDeposit[i].malyDevAdv;
                                        orderVM.MalayPlanAdv = varAdvDeposit[i].malayPlanAdv;
                                        orderVM.MalayDevDeposit = varAdvDeposit[i].malayDevDeposit;
                                        orderVM.MalyPlanDeposit = varAdvDeposit[i].malyPlanDeposit;
                                        orderVM.OthDevAdv = varAdvDeposit[i].othDevAdv;
                                        orderVM.OthPlanAdv = varAdvDeposit[i].othPlanAdv;
                                        orderVM.OthDevDeposit = varAdvDeposit[i].othDevDeposit;
                                        orderVM.OthPlanDeposit = varAdvDeposit[i].othPlanDeposit;

                                        Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                        Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                        Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                        Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                        Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                        Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                        Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                        Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                        /*up to here*/

                                        if (personalDetailsVM.Customer.NationalityID != 0)
                                        {
                                            if (personalDetailsVM.Customer.NationalityID == 1)
                                            {
                                                /*Added by chetan for split adv & deposit*/
                                                Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                                Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                                Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                                Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                                /*up to here*/
                                                Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                                orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ModelPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
                                            }
                                            else
                                            {
                                                /*Added by chetan for split adv & deposit*/
                                                Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                                Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                                Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                                Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                                /*up to here*/
                                                Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
                                                orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ModelPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
                                            }
                                        }
                                    }
                            }
                        }

                    }
                    else if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] == null) && (Session[SessionKey.MobileRegType.ToString()].ToString2().ToUpper() == "PLANONLY"))
                    {
                        //added by deepika 10 march
                        if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID == 0 && orderVM.ContractID == 0)
                        {
                            using (var proxy = new SmartCatelogServiceProxy())
                            {
                                var varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, orderVM.ContractID, 0 }).SingleOrDefault();
                                if (varAdvDeposit != null)
                                {
                                    var Malaytotalprice = varAdvDeposit.malyDevAdv + varAdvDeposit.malayPlanAdv + varAdvDeposit.malayDevDeposit + varAdvDeposit.malyPlanDeposit;
                                    var Othertotalprice = varAdvDeposit.othDevAdv + varAdvDeposit.othPlanAdv + varAdvDeposit.othDevDeposit + varAdvDeposit.othPlanDeposit;
                                    orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
                                    orderVM.MalayAdvDevPrice = Malaytotalprice;
                                    orderVM.OthersAdvDevPrice = Othertotalprice;
                                    Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                    Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

                                    /*Added by chetan for split adv & deposit*/
                                    orderVM.MalyDevAdv = varAdvDeposit.malyDevAdv;
                                    orderVM.MalayPlanAdv = varAdvDeposit.malayPlanAdv;
                                    orderVM.MalayDevDeposit = varAdvDeposit.malayDevDeposit;
                                    orderVM.MalyPlanDeposit = varAdvDeposit.malyPlanDeposit;
                                    orderVM.OthDevAdv = varAdvDeposit.othDevAdv;
                                    orderVM.OthPlanAdv = varAdvDeposit.othPlanAdv;
                                    orderVM.OthDevDeposit = varAdvDeposit.othDevDeposit;
                                    orderVM.OthPlanDeposit = varAdvDeposit.othPlanDeposit;

                                    Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                    Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                    Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                    Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                    Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                    Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                    Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                    Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                    /*up to here*/

                                    if (personalDetailsVM.Customer.NationalityID != 0)
                                    {
                                        if (personalDetailsVM.Customer.NationalityID == 1)
                                        {
                                            /*Added by chetan for split adv & deposit*/
                                            Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                            Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                            Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                            Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                            /*up to here*/
                                            Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                            orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ModelPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
                                        }
                                        else
                                        {
                                            /*Added by chetan for split adv & deposit*/
                                            Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                            Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                            Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                            Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                            /*up to here*/
                                            Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
                                            orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ModelPrice != 0 ? orderVM.ModelPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                // END Deepika


                // Device Details
                if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan || Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                {
                    if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt()) // go to credit check
                    {
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = Session["GuidedSales_SelectedModelImageID"];
                    }
                    if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt() != orderVM.SelectedModelImageID)
                    {

                        orderVM.SelectedModelImageID = Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt();

                        using (var proxy = new SmartCatelogServiceProxy())
                        {
                            /*Commented for articleid*/
                            //modelImage = proxy.ModelImageGet(orderVM.SelectedModelImageID);
                            brandArticleImage = proxy.BrandArticleImageGet(orderVM.SelectedModelImageID);
                        }

                        /*Commented for articleid*/
                        //Session["RegMobileReg_SelectedModelID"] = modelImage.ModelID;
                        Session["RegMobileReg_SelectedModelID"] = brandArticleImage.ModelID;

                        var model = new ModelSmart();
                        using (var proxy = new SmartCatelogServiceProxy())
                        {
                            /*Commented for articleid*/
                            //model = proxy.ModelGet(modelImage.ModelID);
                            //model = proxy.ModelGet(brandArticleImage.ModelID);

                            List<int> modelids = new List<int>();
                            modelids.Add(brandArticleImage.ModelID);
                            model = proxy.ModelGetSmart(modelids).FirstOrDefault();
                        }

                        /*Commented for articleid*/
                        //orderVM.ModelImageUrl = modelImage.ImagePath;
                        orderVM.ModelImageUrl = brandArticleImage.ImagePath;
                        /*Commented for articleid*/
                        orderVM.ModelID = model.ID;
                        orderVM.ModelName = model.Name;
                        /*Commented for articleid*/
                        //orderVM.ColourName = modelImage.ColourID.HasValue ? Util.GetNameByID(RefType.Colour, modelImage.ColourID.Value) : "";
                        orderVM.ColourName = brandArticleImage.ColourID > 0 ? Util.GetNameByID(RefType.Colour, brandArticleImage.ColourID) : "";
                        orderVM.BrandID = model.BrandID;
                        orderVM.BrandName = Util.GetNameByID(RefType.Brand, model.BrandID);
                        orderVM.ArticleId = brandArticleImage.ArticleID.ToString().Trim();
                        Session[SessionKey.ArticleId.ToString()] = orderVM.ArticleId.ToString().Trim();
                        Session["RegMobileReg_RRPDevicePrice"] = model.RetailPrice.ToInt();
                        if (personalDetailsVM.RegID > 0)
                        {
                            orderVM.TotalPrice = Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()].ToDecimal();
                            orderVM.ModelPrice = Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()].ToDecimal();
                            orderVM.ItemPrice = Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()].ToInt();
                        }
                        else
                        {
                            orderVM.DevicePriceType = DevicePriceType.RetailPrice;
                            orderVM.TotalPrice -= orderVM.ModelPrice;
                            orderVM.TotalPrice += model.RetailPrice.ToInt();
                            orderVM.ModelPrice = model.RetailPrice.ToInt();

                            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = orderVM.ModelPrice;
                        }
                    }
                }
            }
            else //Sub Line
            {
                if ((OrderSummaryVM)Session["RegMobileReg_SublineSummary"] != null)
                    orderVM = (OrderSummaryVM)Session["RegMobileReg_SublineSummary"];
            }

            //Package Details
            var sessionPkgPgmBdlPkgCompID = fromSubline ? Session["RegMobileSub_PkgPgmBdlPkgCompID"].ToInt() : Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(); //Main or Sub Line
            if (sessionPkgPgmBdlPkgCompID != orderVM.SelectedPgmBdlPkgCompID)
            {
                var bundlePackage = new PgmBdlPckComponentSmart();
                orderVM.SelectedPgmBdlPkgCompID = sessionPkgPgmBdlPkgCompID;

                // Package Details
                if (sessionPkgPgmBdlPkgCompID != 0)
                {
                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        bundlePackage = proxy.PgmBdlPckComponentGetSmart(new int[] 
                    { 
                        sessionPkgPgmBdlPkgCompID 
                    }).SingleOrDefault();

                        package = proxy.PackageGet(new int[] 
                    { 
                        bundlePackage.ChildID 
                    }).SingleOrDefault();

                        bundle = proxy.BundleGet(new int[] 
                    { 
                        bundlePackage.ParentID 
                    }).SingleOrDefault();
                    }
                }

                orderVM.PlanBundle = bundle.Name;
                orderVM.PlanName = package.Name;

                // Plan Monthly Subscription
                if (orderVM.MonthlySubscription != bundlePackage.Price)
                {
                    orderVM.MonthlySubscription = bundlePackage.Price;
                }

                // Mandatory Package Components
                if (sessionPkgPgmBdlPkgCompID > 0)
                    orderVM.VasVM.MandatoryVASes = GetAvailablePackageComponents(orderVM.SelectedPgmBdlPkgCompID, isMandatory: true).MandatoryVASes;

                else
                    orderVM.VasVM = new ValueAddedServicesVM();
            }
            List<RegSmartComponents> listRegSmartComponents = new List<RegSmartComponents>();
            //Start Componets Details added By Raj
            if (Session["_componentViewModel"] != null)
            {
                // 12345
                ComponentViewModel _component = (ComponentViewModel)Session["_componentViewModel"];
                RegSmartComponents objRegSmartComponents;
                if (_component.NewComponentsList.Count > 0)
                {
                    foreach (var item in _component.NewComponentsList)
                    {
                        if (item.IsChecked || item.ReassignIsmandatory)
                        {
                            objRegSmartComponents = new RegSmartComponents();
                            objRegSmartComponents.PackageID = item.NewPackageId;
                            objRegSmartComponents.ComponentID = item.NewComponentId;
                            objRegSmartComponents.ComponentDesc = item.NewComponentName;
                            listRegSmartComponents.Add(objRegSmartComponents);
                        }
                        orderVM.SelectedComponets = listRegSmartComponents;

                    }
                    List<string> Cmpnts = orderVM.SelectedComponets.Select(a => a.ComponentDesc).ToList();
                    Session["RegMobileReg_SelectedContracts"] = Cmpnts;
                }
            }
            else
            {
                // 12345
                listRegSmartComponents = GetSelectedComponents(personalDetailsVM.RegID.ToString2());
                if (listRegSmartComponents.Count > 0)
                {
                    List<string> Cmpnts = listRegSmartComponents.Select(a => a.ComponentDesc).ToList();
                    Session["RegMobileReg_SelectedContracts"] = Cmpnts;
                    orderVM.SelectedComponets = listRegSmartComponents;
                }
                //////////////////////************//////////////////////
                List<DAL.Models.Component> componentsList = Util.GetContractsBYRegId(personalDetailsVM.RegID, true);
                if (componentsList != null && componentsList.Count > 0)
                {
                    Session["SelectedContractName"] = componentsList.Select(vas => vas.Name).ToList();

                }
                ////////////////////////*************////////////////////////
            }

            // End Componets Details added By Raj

            //VAS details
            //if (!fromSubline)
            //{
            var selectedVasIDs = Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2(); //Main or Sub Line
            var selectedCompIDs = new List<int>();

            // Mandatory VASes
            if (orderVM.VasVM.MandatoryVASes.Count() > 0)
            {
                selectedCompIDs = orderVM.VasVM.MandatoryVASes.Select(a => a.PgmBdlPkgCompID).ToList();
            }
            //As per VAS retention we have a chance to get more VAS componensts based on selection
            if (orderVM.SelectedComponets.Count() > 0)
            {
                List<string> compkenancodes = listRegSmartComponents.Select(a => a.ComponentID).ToList();

                string strNewPackageId = orderVM.SelectedPgmBdlPkgCompID.ToString2();
                ComponentViewModel Componentresponse = GetSelectedPackageInfo_smart(0, strNewPackageId.ToInt(), "");

                if (Componentresponse.NewComponentsList.Count > 0)
                {
                    selectedCompIDs = new List<int>();
                    int cnt = 0;
                    for (cnt = 0; cnt < Componentresponse.NewComponentsList.Count; cnt++)
                    {
                        if (compkenancodes.Contains(Componentresponse.NewComponentsList[cnt].NewComponentKenanCode))
                            selectedCompIDs.Add(Componentresponse.NewComponentsList[cnt].NewComponentId.ToInt());
                    }
                }
                //selectedCompIDs = orderVM.SelectedComponets.Select(sc => Convert.ToInt32(sc.ComponentID)).ToList();
            }

            if (orderVM.VasVM.SelectedVasIDs != selectedVasIDs || orderVM.VasVM.MandatoryVASes.Count() > 0)
            {
                //TODO: if we need to consider price maintaining @ plan level also then we need comment this line to reset
                orderVM.MonthlySubscription = 0;
                decimal vasPrice = 0;
                orderVM.ContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt();
                orderVM.DataPlanID = Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt();
                orderVM.VasVM.SelectedVasIDs = selectedVasIDs.Trim(',');


                var vasIDs = orderVM.VasVM.SelectedVasIDs.Split(',').ToList();
                foreach (var id in vasIDs)
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        //selectedCompIDs.Add(id.ToInt());
                        if (selectedCompIDs.Count == 0 || !selectedCompIDs.Contains(id.ToInt()))
                        {
                            selectedCompIDs.Add(id.ToInt());
                            ////if it's  a Component & not in Smart Component list then we need to add, to include VAS dependency, contract data plan 
                            //using (var proxy = new SmartCatelogServiceProxy())
                            //{
                            //    var pgmBdlPkgComp1 = proxy.PgmBdlPckComponentGet(new int[] { id.ToInt() }).ToList();
                            //    if (pgmBdlPkgComp1 != null && pgmBdlPkgComp1.Count() > 0 && pgmBdlPkgComp1[0].LinkType != "BP")
                            //    {

                            //    }
                            //}
                        }
                    }
                }

                if (Session[SessionKey.RegMobileReg_DataplanID.ToString()] != null && Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt() != 0)
                {

                    selectedCompIDs.Add(Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt());
                }


                if (selectedCompIDs != null)
                {
                    if (selectedCompIDs.Count() > 0)
                    {
                        using (var proxy = new SmartCatelogServiceProxy())
                        {
                            var vases = proxy.PgmBdlPckComponentGetSmart
                               (
                                   selectedCompIDs
                               ).ToList();

                            components = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();


                            //orderVM.VasVM.VASesName =  vases.Select(a => a.Name + " (RM" + a.Price + ")").ToList();
                            var query = from selectedVases in vases
                                        join comps in components on
                                            selectedVases.ChildID equals comps.ID
                                        select new { name = comps.Name + " (RM" + selectedVases.Price + ")" };

                            orderVM.VasVM.VASesName = query.Select(a => a.name).ToList();

                            /*Added by chetan for vasplan price adding*/

                            if ((!string.IsNullOrEmpty(selectedVasIDs)) || (orderVM.VasVM.SelectedVasIDs != selectedVasIDs) || (orderVM.VasVM.VASesName.Count > 0))
                            {
                                var query1 = from selectedVases in vases
                                             join comps in components on
                                                selectedVases.ChildID equals comps.ID
                                             select new { selectedVases.Price };
                                if (query1 != null)
                                {
                                    orderVM.MonthlySubscription2 = query1.Select(b => b.Price).ToList();
                                    for (int i = 0; i < orderVM.MonthlySubscription2.Count; i++)
                                    {
                                        vasPrice += orderVM.MonthlySubscription2[i];
                                    }
                                }

                                //orderVM.MonthlySubscription += vasPrice;
                            }

                            /*Upto here*/
                        }
                    }
                }

                orderVM.MonthlySubscription += vasPrice;
                Session[SessionKey.RegMobileReg_VasNames.ToString()] = orderVM.VasVM.VASesName;
            }
            //}

            // Advance Payment/ Deposite
            if (orderVM.Deposite != personalDetailsVM.Deposite)
            {
                orderVM.TotalPrice -= orderVM.Deposite;
                orderVM.TotalPrice += personalDetailsVM.Deposite;

                orderVM.Deposite = personalDetailsVM.Deposite;
            }


            // Device Price (cheaper)
            if (Session[SessionKey.RegMobileReg_ContractID.ToString()] != null && orderVM.SelectedPgmBdlPkgCompID.ToInt() != 0 && orderVM.ModelID != 0)
            {
                if (orderVM.ItemPrice != orderVM.ModelPrice)
                {
                    var itemPrice = new ItemPrice();
                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        var itemPriceID = proxy.ItemPriceFind(new ItemPriceFind()
                        {
                            ItemPrice = new ItemPrice()
                            {
                                BdlPkgID = orderVM.SelectedPgmBdlPkgCompID.ToInt(),
                                PkgCompID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt(),
                                ModelID = orderVM.ModelID,
                                pkgDataID = Session["RegMobileReg_DataPkgID"].ToInt()
                            },
                            Active = true
                        }).SingleOrDefault();

                        if (personalDetailsVM.RegID == 0)
                        {
                            if (itemPriceID.ToInt() != 0) // item price configured
                            {
                                itemPrice = proxy.ItemPriceGet(itemPriceID);

                                if (orderVM.DevicePriceType == DevicePriceType.RetailPrice)
                                {
                                    orderVM.TotalPrice -= orderVM.ModelPrice;
                                }
                                else if (orderVM.DevicePriceType == DevicePriceType.ItemPrice)
                                {
                                    orderVM.TotalPrice -= orderVM.ItemPrice;
                                }

                                if (itemPrice.Price == 0) // RM 0 price for device
                                {
                                    orderVM.DevicePriceType = DevicePriceType.NoPrice;
                                }
                                else // cheaper price for device
                                {
                                    orderVM.DevicePriceType = DevicePriceType.ItemPrice;
                                }

                                orderVM.TotalPrice += itemPrice.Price;
                                orderVM.ItemPrice = itemPrice.Price.ToInt();

                                Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = orderVM.ItemPrice.ToDecimal();
                            }
                            else // retail price
                            {
                                if (orderVM.DevicePriceType == DevicePriceType.ItemPrice)
                                {
                                    orderVM.TotalPrice -= orderVM.ItemPrice;
                                    //orderVM.ItemPrice = orderVM.ModelPrice.ToInt();
                                    orderVM.TotalPrice += orderVM.ModelPrice;
                                    orderVM.DevicePriceType = DevicePriceType.RetailPrice;

                                    Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = orderVM.ModelPrice;
                                }
                            }
                        }
                    }
                }
            }
            else //(retail price)
            {
                if (personalDetailsVM.RegID == 0)
                {
                    if (orderVM.DevicePriceType != DevicePriceType.RetailPrice)
                    {
                        orderVM.TotalPrice -= orderVM.ItemPrice;
                        orderVM.ItemPrice = 0;
                        orderVM.TotalPrice += orderVM.ItemPrice;
                    }
                }
            }
            //added by KD for penalty
            //TODO: important to revert this conversion and need to send it as decimal only
            if (Session["Penalty"] != null)
                orderVM.PenaltyAmount = Session["Penalty"].ToDecimal().ToInt();
            orderVM.TotalPrice = orderVM.TotalPrice + orderVM.PenaltyAmount;

            // MobileNo
            var mobileNos = fromSubline ? (List<string>)Session["RegMobileSub_MobileNo"] : (List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()]; // Main or Sub Line
            orderVM.MobileNumbers = mobileNos == null ? new List<string>() : mobileNos;

            if (!fromSubline) //main line
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderVM;
            else //sub line
            {
                Session["RegMobileReg_SublineSummary"] = orderVM;
                orderVM.fromSubline = true;
            }

            // Subline
            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
            if (sublines != null)
                orderVM.Sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];

            return orderVM;
        }

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult PersonalDetails()
        {
            Session["DiscountStatus"] = null;
            Session[SessionKey.Discount.ToString()] = null;
            SetRegStepNo(MobileRegistrationSteps.PersonalDetails);
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();
            Session["isPenaltyCheck"] = null;
            var idCardTypeID = Session[SessionKey.RegMobileReg_IDCardType.ToString()] != null ? Session[SessionKey.RegMobileReg_IDCardType.ToString()] : null;
            var personalDetailsVM = new PersonalDetailsVM();
            if ((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"] != null)
            {
                personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            }

            personalDetailsVM.Customer.IDCardTypeID = idCardTypeID.ToInt();
            personalDetailsVM.Customer.IDCardNo = Session[SessionKey.RegMobileReg_IDCardNo.ToString()] == null ? "" : Session[SessionKey.RegMobileReg_IDCardNo.ToString()].ToString2();

            return View(personalDetailsVM);
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,SMRT_USER,SMRT_MANAGER,SMRT_SK")]
        [DoNoTCache]
        public ActionResult MobileRegSummary(int? regID)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            ViewBag.EarlyTerminationFeeLabel = Settings.Default.EarlyTerminationFee;
            Session["TsCs"] = null;
            Session["ConTsCs"] = null;
            Session["isPenaltyCheck"] = null;
            if (regID.ToInt() > 0)
            {
                ClearRegistrationSession();
            }
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            var orderSummary = new OrderSummaryVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new Online.Registration.DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            int paymentstatus = -1;
            var allRegDetails = new Online.Registration.DAL.RegistrationDetails();
            #region VLT ADDED CODE 21stFEB for Email functionality
            Session["RegID"] = regID;
            #endregion VLD ADDED CODE 21stFEB for Email functionality
            if (regID.ToInt() > 0)
            {
                ClearRegistrationSession();
                personalDetailsVM = new PersonalDetailsVM()
                {
                    RegID = regID.Value.ToInt(),
                };

                Session["RegMobileReg_PhoneVM"] = null;
                var KenanLogDetails = new DAL.Models.KenanaLogDetails();
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    
                    reg = proxy.RegistrationGet(regID.Value);
                    //Added by Patanjali to get the payment status on 07-04-2013

                    // Customer
                    personalDetailsVM.Customer = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    //02092015 - CR [Assessment] Bug 1153 - Lus
                    if (personalDetailsVM.Customer.IsVIP == 1 && !Roles.IsUserInRole("MREG_SV"))
                    {
                        Session["VIPFailMsg"] = true;
                        //return RedirectToAction("StoreKeeper", "Registration");
                        return Redirect("~/Registration/StoreKeeper");
                    }

                    paymentstatus = proxy.PaymentStatusGet(regID.Value);
                    var proxy_kenan = new RegistrationServiceProxy();
                    KenanLogDetails = proxy_kenan.KenanLogDetailsGet(regID.Value);
                    if (!ReferenceEquals(KenanLogDetails, null))
                    {
                        personalDetailsVM.MessageCode = KenanLogDetails.MessageCode.ToInt();
                        personalDetailsVM.MessageDesc = KenanLogDetails.MessageDesc.ToString();
                    }
                    if (paymentstatus != -1)
                    {
                        if (paymentstatus == 0)
                        {
                            personalDetailsVM.PaymentStatus = "0";
                        }
                        else
                        {
                            personalDetailsVM.PaymentStatus = "1";
                        }
                    }
                    else
                    {
                        personalDetailsVM.PaymentStatus = "0";
                    }
                    //Added by Patanjali to get the payment status on 07-04-2013
                    personalDetailsVM.PenaltyWaivedAmount = reg.PenaltyWaivedAmt;
                    personalDetailsVM.QueueNo = reg.QueueNo;
                    //Added by Patanjali to support remarks on 30-03-2013
                    personalDetailsVM.Remarks = reg.Remarks;
                    //Added by Patanjali to support remarks on 30-03-2013 ends here
                    personalDetailsVM.BiometricVerify = reg.BiometricVerify;
                    personalDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
                    personalDetailsVM.BlacklistInternal = reg.InternalBlacklisted;
                    personalDetailsVM.SignatureSVG = reg.SignatureSVG;
                    personalDetailsVM.CustomerPhoto = reg.CustomerPhoto;
                    personalDetailsVM.AltCustomerPhoto = reg.AltCustomerPhoto;
                    personalDetailsVM.RFSalesDT = reg.RFSalesDT;
                    personalDetailsVM.Photo = reg.Photo;
                    personalDetailsVM.RegSIMSerial = reg.SIMSerial;
                    personalDetailsVM.RegIMEINumber = reg.IMEINumber;
                    personalDetailsVM.RegTypeID = reg.RegTypeID;
                    personalDetailsVM.AgeCheckStatus = reg.AgeCheckStatus;
                    personalDetailsVM.DDMFCheckStatus = reg.DDMFCheckStatus;
                    personalDetailsVM.AddressCheckStatus = reg.AddressCheckStatus;
                    personalDetailsVM.OutStandingCheckStatus = reg.OutStandingCheckStatus;
                    personalDetailsVM.TotalLineCheckStatus = reg.TotalLineCheckStatus;
                    personalDetailsVM.PrincipleLineCheckStatus = reg.PrincipleLineCheckStatus;
                    personalDetailsVM.ContractCheckStatus = reg.ContractCheckStatus;
                    personalDetailsVM.UserType = reg.UserType;
                    personalDetailsVM.PlanAdvance = reg.PlanAdvance;
                    personalDetailsVM.PlanDeposit = reg.PlanDeposit;
                    personalDetailsVM.DeviceAdvance = reg.DeviceAdvance;
                    personalDetailsVM.DeviceDeposit = reg.DeviceDeposit;
                    personalDetailsVM.Discount = reg.Discount;
                    personalDetailsVM.KenanAccountNo = reg.KenanAccountNo;
                    personalDetailsVM.OutstandingAcs = reg.OutstandingAcs;
                    personalDetailsVM.PrePortInCheckStatus = reg.PrePortInCheckStatus;
                    personalDetailsVM.MNPServiceIdCheckStatus = reg.MNPServiceIdCheckStatus;
                    personalDetailsVM.MOC_Status = reg.MOC_Status;
                    personalDetailsVM.Whitelist_Status = reg.Whitelist_Status;
                    personalDetailsVM.SM_ContractType = reg.SM_ContractType;
                    personalDetailsVM.PenaltyAmount = reg.PenaltyAmount.ToDecimal();
                    personalDetailsVM.PenaltyWaivedAmount = reg.PenaltyWaivedAmt.ToDecimal();
                    personalDetailsVM.IsSImRequired_Smart = reg.IsSImRequired_Smart;
                    //Added to assign sim model
                    personalDetailsVM.SIMModel = reg.SimModelId.ToString2() == "" ? "0" : reg.SimModelId.ToString();
                    personalDetailsVM.IsVerified = reg.IsVerified;
					personalDetailsVM.ArticleID = reg.ArticleID;
                    // RegAccount
                    var regAccount = proxy.RegAccountGet(new int[] { reg.ID }).SingleOrDefault();
                    if (regAccount != null)
                    {
                        personalDetailsVM.ServiceActivateErrMsg = regAccount.KenanActivateServiceErrMessage;
                    }
                    //contract check count
                    using (var regPro = new RegistrationServiceProxy())
                    {
                        var result = regPro.LnkRegistrationGetByRegId(regID.ToInt());
                        if (result != null)
                        {
                            personalDetailsVM.ContractCheckCount = result.ContractCheckCount != null ? result.ContractCheckCount : 0;
                            personalDetailsVM.TotalLineCheckCount = result.TotalLineCheckCount != null ? result.TotalLineCheckCount : 0;
                        }
                    }
                    // Registration Status
                    var regStatusID = proxy.RegStatusFind(new RegStatusFind()
                    {
                        RegStatus = new RegStatus()
                        {
                            RegID = reg.ID
                        },
                        Active = true
                    }).SingleOrDefault();

                    personalDetailsVM.Deposite = reg.AdditionalCharges;
                    personalDetailsVM.StatusID = proxy.RegStatusGet(new int[] { regStatusID }).SingleOrDefault().StatusID;

                    using (var configProxy = new ConfigServiceProxy())
                    {
                        personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;

                    }
                    //if (personalDetailsVM.StatusID == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed) ||
                    //    personalDetailsVM.StatusID == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan))
                    //{
                    //    personalDetailsVM.IsRegClosed = true;
                    //}
                    //if (personalDetailsVM.StatusID == 21)
                    if (personalDetailsVM.StatusID == 554 || personalDetailsVM.StatusID == 555)
                    {

                        using (var regProxy = new RegistrationServiceProxy())
                        {
                            string reason = regProxy.CancelReasonGet(personalDetailsVM.RegID);
                            personalDetailsVM.MessageDesc = reason;

                        }
                    }
                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);
                    Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;

                    // Address
                    var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                    {
                        Active = true,
                        Address = new Address()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();
                    personalDetailsVM.Address = proxy.RegAddressGet(regAddrIDs).SingleOrDefault();

                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    // RegMdlGrpModels
                    var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                    {
                        RegMdlGrpModel = new RegMdlGrpModel()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();
                    if (regMdlGrpModelIDs.Count() > 0)
                        regMdlGrpModel = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).SingleOrDefault();

                    if (regMdlGrpModel.ID == 0)
                    {
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
                    }
                    else
                    {
                        Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = regMdlGrpModel.Price.ToDecimal();
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
                    }

                    // RegPgmBdlPkgComponent
                    var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                    {
                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();

                    // Main Line PgmBdlPkgComponent
                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    if (regPgmBdlPkgCompIDs.Count() > 0)
                        regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind()
                    {
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();

                    if (suppLineIDs.Count() > 0)
                    {
                        suppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    }
                }

                if (suppLines.Count() > 0)
                    ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

                using (var proxy = new SmartCatelogServiceProxy())
                {
                    string userICNO = Util.SessionAccess.User.IDCardNo;

                    if (userICNO != null && userICNO.Length > 0)
                    {
                        objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                    }
                    if (objWhiteListDetailsResp != null)
                    {
                        if (objWhiteListDetailsResp.Description != null)
                        {
                            personalDetailsVM._WhiteListDetails.Description = objWhiteListDetailsResp.Description;
                        }
                    }

                    if (regMdlGrpModel.ID > 0)
                        //Session["RegMobileReg_SelectedModelID"] = proxy.ModelImageGet(new int[] { regMdlGrpModel.ModelImageID }).SingleOrDefault().ModelID;
                        Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;


                    //var mainLinePBPCs = new List<PgmBdlPckComponent>();
                    var mainLinePBPCs = new List<PgmBdlPckComponentSmart>();
                    if (mainLinePBPCIDs.Count() > 0)
                        mainLinePBPCs = proxy.PgmBdlPckComponentGetSmart(mainLinePBPCIDs).ToList();

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var DPCode = Properties.Settings.Default.RegType_DevicePlan;
                    var MAcode = Properties.Settings.Default.Master_Component;
                    var MPcode = Properties.Settings.Default.Main_package;
                    var CPCode = Properties.Settings.Default.ComplimentaryPlan;
                    var DCCode = Properties.Settings.Default.CompType_DataCon;
                    var ECCode = Properties.Settings.Default.Extra_Component;
                    var SPcode = Properties.Settings.Default.SupplimentaryPlan;


                    if (mainLinePBPCs.Count() > 0)
                    {
                        if (mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode || a.PlanType == SPcode)).SingleOrDefault() != null)
                        {
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode || a.PlanType == SPcode)).SingleOrDefault().ID;
                        }
                        // Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode)).FirstOrDefault();

                        var vasIDs = "";
                        var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode).Select(a => a.ID).ToList();

                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session[SessionKey.RegMobileReg_VasIDs.ToString()] = vasIDs;
                    }
                    //if (suppLinePBPCIDs.Count() > 0)
                    //{
                    //    var suppLinePBPCs = proxy.PgmBdlPckComponentGet(suppLinePBPCIDs).ToList();
                    //     Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode).SingleOrDefault().ID;
                    //}
                }

                //for tfs Bug id:2637
                using (var regProxy = new RegistrationServiceProxy())
                {
                    DAL.Models.LnkRegDetails lnkregdetails = regProxy.LnkRegistrationGetByRegId(regID.ToInt());
                    if (lnkregdetails != null)
                    {
                        personalDetailsVM.ContractCheckCount = lnkregdetails.ContractCheckCount.ToInt();
                        personalDetailsVM.Justification = lnkregdetails.Justification; //Added for justification CR
                    }

                    //<Start> - 20150914 - Samuel - display sales code in summary page
                    //personalDetailsVM.Registration = regProxy.RegistrationGet(personalDetailsVM.RegID);
                    personalDetailsVM.RegAttributes = regProxy.RegAttributesGetByRegID(personalDetailsVM.RegID);
                    /*using (var proxyOrg = new OrganizationServiceProxy())
                    {
                        if (!ReferenceEquals(personalDetailsVM.Registration, null) && !ReferenceEquals(personalDetailsVM.Registration.CenterOrgID, null) && personalDetailsVM.Registration.CenterOrgID != 0)
                        {
                            personalDetailsVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { personalDetailsVM.Registration.CenterOrgID }).FirstOrDefault();
                        }
                    }*/
                    var salesPerson = reg.SalesPerson;
                    var orgID = reg.CenterOrgID;
                    var outletCode = string.Empty;
                    var salesCode = string.Empty;

                    WebHelper.Instance.generateOutletAndSalesCode(orgID, salesPerson, personalDetailsVM.RegAttributes, out outletCode, out salesCode);
                    personalDetailsVM.OutletCode = outletCode;
                    personalDetailsVM.SalesCode = salesCode;
                    //<End> - 20150914 - Samuel - display sales code in summary page

                    //CR 29JAN2016 - display bill cycle
                    if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE).FirstOrDefault() != null)
                    {
                        personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE).FirstOrDefault().ATT_Value;
                    }
                    else
                    {
                        personalDetailsVM.BillingCycle = "N/A";
                    }
                }
                //for tfs Bug id:2637

            }
            var orderSummaryVM = ConstructOrderSummary();
            orderSummaryVM.IMEINumber = reg.IMEINumber;
            orderSummaryVM.SIMSerial = reg.SIMSerial;

            #region get SIM Card Type
            using (var proxy = new RegistrationServiceProxy())
            {
                if (regID.ToInt() > 0)
                {
                    DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(regID.ToInt());
                    if (lnkregdetails != null)
                    {
                        if (lnkregdetails.SimCardType != null)
                            personalDetailsVM.SIMCardType = lnkregdetails.SimCardType;
                        //else
                        //    personalDetailsVM.SIMCardType = "Normal";
                    }
                    else
                    {
                        //personalDetailsVM.SIMCardType = "Normal";
                    }
                    Session["SIMCardType"] = personalDetailsVM.SIMCardType;
                }
            }
            #endregion

            #region Commented by Durga Prasad Kota
            //if (reg.KenanAccountNo != null)
            //{ Session["KenanAccountNo"] = reg.KenanAccountNo; }
            //else
            //{ Session["KenanAccountNo"] = "0"; }
            #endregion
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderSummaryVM;

            if (!ReferenceEquals(((List<string>)TempData[SessionKey.errorValidationList.ToString()]), null) && ((List<string>)TempData[SessionKey.errorValidationList.ToString()]).Any())
            {
                string inventoryValidation = "Please re-insert the ";

                if (((List<string>)TempData[SessionKey.errorValidationList.ToString()]).Count() > 1)
                {
                    foreach (var errorMsg in ((List<string>)TempData[SessionKey.errorValidationList.ToString()]))
                    {
                        switch (errorMsg)
                        {
                            case "Sim serial": ModelState.Remove("RegSIMSerial"); break;
                            case "IMEI number": ModelState.Remove("RegIMEINumber"); break;
                        }
                        inventoryValidation += errorMsg + " and ";
                    }
                }
                else
                { // only 1 error
                    inventoryValidation += ((List<string>)TempData[SessionKey.errorValidationList.ToString()]).FirstOrDefault();
                }
                if (inventoryValidation.EndsWith(" and "))
                {
                    inventoryValidation = inventoryValidation.Substring(0, inventoryValidation.Length - 5);
                }
                ModelState.AddModelError(string.Empty, inventoryValidation);
            }

            #region MOC_Status and Liberization status
            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session["ExternalID"].ToString2()) ? string.Empty : Session["ExternalID"].ToString2(), regID.ToInt());
            if (Result != null && Result.Count() > 1)
            {
                personalDetailsVM.Liberlization_Status = Result[0];
                personalDetailsVM.MOCStatus = Result[1];
            }
            #endregion
            //personalDetailsVM =
            return View(personalDetailsVM);
        }

        //[Authorize(Roles = "MREG_W,MREG_C,MREG_U,SMRT_USER,SMRT_MANAGER,SMRT_SK")]
        //public ActionResult PrintMobileRF(int id)
        //{
        //    var regFormVM = new RegistrationFormVM();
        //    var modelImageIDs = new List<int>();
        //    var pbpcIDs = new List<int>();
        //    var suppLineVASIDs = new List<int>();
        //    var suppLineForms = new List<SuppLineForm>();
        //    var regMdlGrpModels = new List<RegMdlGrpModel>();

        //    using (var proxy = new SmartRegistrationServiceProxy())
        //    {
        //        // Registration
        //        regFormVM.Registration = proxy.RegistrationGet(id);

        //        // Customer
        //        regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

        //        // Address
        //        var regAddrIDs = proxy.RegAddressFind(new AddressFind()
        //        {
        //            Active = true,
        //            Address = new Address()
        //            {
        //                RegID = id
        //            }
        //        }).ToList();
        //        regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

        //        // RegMdlGrpModels
        //        var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
        //        {
        //            RegMdlGrpModel = new RegMdlGrpModel()
        //            {
        //                RegID = id
        //            }
        //        }).ToList();
        //        if (regMdlGrpModelIDs.Count() > 0)
        //        {
        //            regMdlGrpModels = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
        //            modelImageIDs = regMdlGrpModels.Select(a => a.ModelImageID).ToList();
        //        }

        //        // RegPgmBdlPkgComponent
        //        var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
        //        {
        //            RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
        //            {
        //                RegID = id
        //            }
        //        }).ToList();

        //        var pbpc = new List<RegPgmBdlPkgComp>();
        //        if (regPgmBdlPkgCompIDs.Count() > 0)
        //            pbpc = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

        //        //main line
        //        if (pbpc.Count() > 0)
        //            pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();

        //        // Reg Supp Line
        //        var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = id } }).ToList();
        //        if (suppLineIDs.Count() > 0)
        //        {
        //            var regSuppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
        //            foreach (var regSuppLine in regSuppLines)
        //            {
        //                suppLineForms.Add(new SuppLineForm()
        //                {
        //                    // supp line pbpc ID
        //                    SuppLinePBPCID = regSuppLine.PgmBdlPckComponentID,
        //                    // supp line vas ID
        //                    SuppLineVASIDs = pbpc.Where(a => a.RegSuppLineID == regSuppLine.ID).Select(a => a.PgmBdlPckComponentID).ToList(),
        //                    MSISDN = regSuppLine.MSISDN1
        //                });
        //            }
        //        }

        //    }

        //    using (var proxy = new SmartCatelogServiceProxy())
        //    {

        //        // ModelGroupModel
        //        if (modelImageIDs.Count() > 0)
        //        {
        //            //regFormVM.ModelGroupModels = proxy.ModelGroupModelGet(mdlGrpModelIDs).ToList();
        //            var modelImages = proxy.ModelImageGet(modelImageIDs).ToList();
        //            var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());

        //            foreach (var regMdlGrpModel in regMdlGrpModels)
        //            {
        //                var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
        //                regFormVM.DeviceForms.Add(new DeviceForm()
        //                {
        //                    Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
        //                    Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
        //                    Price = regMdlGrpModel.Price.ToDecimal()
        //                });
        //            }
        //        }

        //        // BundlePackage, PackageComponents
        //        if (pbpcIDs.Count() > 0)
        //        {
        //            var pgmBdlPkgComps = proxy.PgmBdlPckComponentGet(pbpcIDs).Distinct().ToList();
        //            var bpCode = Properties.Settings.Default.Bundle_Package;
        //            var pcCode = Properties.Settings.Default.Package_Component;
        //            regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode).ToList();
        //            regFormVM.PackageComponents = pgmBdlPkgComps.Where(a => a.LinkType == pcCode).ToList();
        //        }

        //        foreach (var suppLineForm in suppLineForms)
        //        {
        //            //supp line package name
        //            suppLineForm.SuppLineBdlPkgName = proxy.PackageGet(proxy.PgmBdlPckComponentGet(new int[] { suppLineForm.SuppLinePBPCID })
        //                                                        .Select(a => a.ChildID)).SingleOrDefault().Name;
        //            //supp line vas names
        //            suppLineForm.SuppLineVASNames = proxy.ComponentGet(proxy.PgmBdlPckComponentGet(suppLineForm.SuppLineVASIDs).Select(a => a.ChildID).ToList()).Select(a => a.Name).ToList();
        //        }
        //        regFormVM.SuppLineForms = suppLineForms;
        //    }

        //    return View(regFormVM);
        //}
        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,SMRT_USER,SMRT_MANAGER,SMRT_SK,MREG_DAB,MREG_HQ")]
        public ActionResult PrintMobileRF(int id)
        {
            var regFormVM = new RegistrationFormVM();
            var modelImageIDs = new List<int>();
            var pbpcIDs = new List<int>();
            var suppLineVASIDs = new List<int>();
            var suppLineForms = new List<SuppLineForm>();
            var regMdlGrpModels = new List<RegMdlGrpModel>();
            ViewBag.EarlyTerminationFeeLabel = Settings.Default.EarlyTerminationFee;

            using (var proxy = new SmartRegistrationServiceProxy())
            {
                // Registration
                regFormVM.Registration = proxy.RegistrationGet(id);

                // Customer
                regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

                // Address
                var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                {
                    Active = true,
                    Address = new Address()
                    {
                        RegID = id
                    }
                }).ToList();
                regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

                // RegMdlGrpModels
                var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                {
                    RegMdlGrpModel = new RegMdlGrpModel()
                    {
                        RegID = id
                    }
                }).ToList();
                if (regMdlGrpModelIDs.Count() > 0)
                {
                    regMdlGrpModels = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
                    modelImageIDs = regMdlGrpModels.Select(a => a.ModelImageID).ToList();
                }
                //Commented by VLT on 11 Apr 2013
                // RegPgmBdlPkgComponent
                var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                {
                    RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                    {
                        RegID = id,
                    }
                }).ToList();
                //Commented by VLT on 11 Apr 2013

                //var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgComponentFind(new RegPgmBdlPkgCompFind()
                //{
                //    RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                //    {
                //        RegID = id,
                //        RegSuppLine = null
                //    }
                //}).ToList();

                var pbpc = new List<RegPgmBdlPkgComp>();
                if (regPgmBdlPkgCompIDs.Count() > 0)
                    pbpc = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();
                //var pbpc = new List<PgmBdlPckComponent>();
                //if (regPgmBdlPkgCompIDs.Count() > 0)
                //    pbpc = proxy.RegPgmBdlPkgComponentGet(regPgmBdlPkgCompIDs).ToList();
                //Commented by VLT on 11 Apr 2013

                //main line
                if (pbpc.Count() > 0)
                    pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();
                //Commented by VLT on 11 Apr 2013

                //if (pbpc.Count() > 0)
                //pbpcIDs = pbpc.Select(a => a.ID).ToList();

                // Reg Supp Line



                var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = id } }).ToList();
                if (suppLineIDs.Count() > 0)
                {
                    var regSuppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    foreach (var regSuppLine in regSuppLines)
                    {
                        suppLineForms.Add(new SuppLineForm()
                        {
                            // supp line pbpc ID
                            SuppLinePBPCID = regSuppLine.PgmBdlPckComponentID,

                            // supp line vas ID
                            //Commented by VLT on 11 Apr 2013
                            SuppLineVASIDs = pbpc.Where(a => a.RegSuppLineID == regSuppLine.ID).Select(a => a.PgmBdlPckComponentID).ToList(),

                            //Commented by VLT on 11 Apr 2013
                            //Added by VLT on 11 Apr 2013
                            // SuppLineVASIDs = GetPbpcForSuppLines(id,regSuppLine.ID).Select(a => a.ID).ToList(),
                            //Added by VLT on 11 Apr 2013
                            MSISDN = regSuppLine.MSISDN1
                        });
                    }
                }

            }

            using (var proxy = new SmartCatelogServiceProxy())
            {

                // ModelGroupModel
                if (modelImageIDs.Count() > 0)
                {
                    //regFormVM.ModelGroupModels = proxy.ModelGroupModelGet(mdlGrpModelIDs).ToList();
                    //var modelImages = proxy.ModelImageGet(modelImageIDs).ToList();
                    var modelImages = proxy.BrandArticleModelImageGet(modelImageIDs).ToList();
                    var models = proxy.ModelGetSmart(modelImages.Select(a => a.ModelID).ToList());

                    foreach (var regMdlGrpModel in regMdlGrpModels)
                    {
                        var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
                        regFormVM.DeviceForms.Add(new DeviceForm()
                        {
                            Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
                            Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
                            Price = regMdlGrpModel.Price.ToDecimal()
                        });
                    }
                }
                var vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
                // BundlePackage, PackageComponents
                if (pbpcIDs.Count() > 0)
                {
                    var pgmBdlPkgComps = proxy.PgmBdlPckComponentGet(pbpcIDs).Distinct().ToList();
                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;

                    regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode).ToList();
                    regFormVM.PackageComponents = pgmBdlPkgComps.Where((a => a.LinkType.ToLower() == "pc" || a.LinkType.ToLower() == "ec" || a.LinkType.ToLower() == "dc" || a.LinkType.ToLower() == "ma")).ToList();
                }

                List<RegSmartComponents> listRegSmartComponents = new List<RegSmartComponents>();

                listRegSmartComponents = GetSelectedComponents(id.ToString2());
                if (listRegSmartComponents.Count > 0)
                {
                    //List<string> Cmpnts = listRegSmartComponents.Select(a => a.ComponentDesc).ToList();
                    //Session["RegMobileReg_SelectedContracts"] = Cmpnts;
                    regFormVM.RegSmartComponents = listRegSmartComponents;
                }




                foreach (var suppLineForm in suppLineForms)
                {
                    //supp line package name
                    suppLineForm.SuppLineBdlPkgName = proxy.PackageGet(proxy.PgmBdlPckComponentGet(new int[] { suppLineForm.SuppLinePBPCID })
                                                                .Select(a => a.ChildID)).SingleOrDefault().Name;
                    //supp line vas names
                    suppLineForm.SuppLineVASNames = proxy.ComponentGet(proxy.PgmBdlPckComponentGet(suppLineForm.SuppLineVASIDs).Select(a => a.ChildID).ToList()).Select(a => a.Name).ToList();
                }
                regFormVM.SuppLineForms = suppLineForms;



                //Contract Name Dispaly
                if (regFormVM.Registration.RegTypeID == Util.GetIDByCode(RefType.RegType, Settings.Default.RegType_DevicePlan))
                {
                    List<DAL.Models.Component> componentsList = Util.GetContractsBYRegId(id, true);
                    if (componentsList != null && componentsList.Count > 0)
                    {
                        Session["SelectedContractName"] = componentsList.Select(vas => vas.Name).ToList();
                        int Duration = 0;
                        foreach (var item in componentsList)
                        {
                            using (var RSproxy = new CatalogServiceProxy())
                            {
                                string strValue = RSproxy.GetContractByCode(componentsList[0].Code);
                                if (Duration < strValue.ToInt())
                                {
                                    Duration = strValue.ToInt();
                                }
                            }

                        }
                        Session["SelectedContractValue"] = Duration;
                    }
                }
            }
            using (var proxyOrg = new OrganizationServiceProxy())
            {
                if (!ReferenceEquals(regFormVM.Registration, null) && !ReferenceEquals(regFormVM.Registration.CenterOrgID, null) && regFormVM.Registration.CenterOrgID != 0)
                {
                    regFormVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { regFormVM.Registration.CenterOrgID }).FirstOrDefault();
                }
            }

            WebHelper.Instance.MapRegAttributesByRegID(regFormVM, !ReferenceEquals(regFormVM.Registration, null) ? regFormVM.Registration.ID : id);

            return View(regFormVM);
        }
        [Authorize(Roles = "MREG_W,MREG_U,MREG_R,MREG_RA,SMRT_MANAGER,SMRT_SK")]
        public ActionResult PrintContractDetails(int id)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            return View(WebHelper.Instance.GetContractDetailsViewModel_SMART(id));
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PrintMobileRegSummary(FormCollection collection, RegistrationFormVM personalDetailsVM)
        {
            Util.CommonPrintMobileRegSummary(collection);

            //int success = 0;

            //// RenderRazorViewToString(this, "PrintContractDetails", new {id=93473,status=28});

            //string HTML = collection[3].ToString();


            //string sDate = string.Format("{0:MMddyyyyhmmtt}", System.DateTime.Now);

            //string sPath = Server.MapPath("~/PrintBill");

            //if (!System.IO.Directory.Exists(sPath))
            //{
            //    System.IO.Directory.CreateDirectory(sPath);
            //}


            //string sFileName = "myfile" + sDate + "P-" + collection[0] + ".htm";

            //sPath = sPath + "/" + sFileName;

            //StreamWriter swXLS = new StreamWriter(sPath);

            //swXLS.Write(HTML.ToString());

            //swXLS.Close();

            ////After Hosting
            ////string Output = Request.Url.Host;

            ////Testing Local
            //string Output = Util.URLPath();

            //string uri = "http://" + Output + "/PrintBill/" + sFileName; ;


            //string Apppath = Server.MapPath("~/App/wp.exe");

            //int printstatus = Util.PrintThroughExe(collection[0].ToString(), uri, Apppath, collection[5].ToString(), collection[1].ToString());

            //if (printstatus == 1)
            //{
            //    Session["printsuccess"] = "PrintSuccessfullyCompleted";
            //}
            //else
            //{
            //    if (printstatus == (int)PrintingIssues.Internalproblem)
            //    {
            //        Session["printsuccess"] = "Internal problem while posting request to printer";
            //    }
            //    else if (printstatus == (int)PrintingIssues.Networkproblem)
            //    {
            //        Session["printsuccess"] = "Network problem";
            //    }
            //    else if (printstatus == (int)PrintingIssues.Printernotdetected)
            //    {
            //        Session["printsuccess"] = "Printer not detected";
            //    }
            //    else
            //    {
            //        Session["printsuccess"] = "Printing Failed";
            //    }


            //}
            //Session["selectedPrinter"] = collection[5].ToString();
            //System.IO.File.Delete(sPath);

            return RedirectToAction("MobileRegSummary", new { id = collection[1].ToString2() });

        }


        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER,SMRT_MANAGER,SMRT_SK,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegSuccess(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,SMRT_USER,SMRT_MANAGER,SMRT_SK,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegCanceled(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,SMRT_USER,SMRT_MANAGER,SMRT_SK,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegAccCreated(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,SMRT_USER,SMRT_MANAGER,SMRT_SK,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegSvcActivated(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER,SMRT_MANAGER,SMRT_SK,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegClosed(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegBreFail(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER,SMRT_MANAGER,SMRT_SK,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegFail(int regID)
        {
            return View(regID);
        }
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER,SMRT_MANAGER,SMRT_SK,MREG_DC,MREG_DN,MREG_DSA,MREG_DIC")]
        [HttpPost]
        public ActionResult MobileRegFail(FormCollection collection)
        {
            return RedirectToAction(GetRegActionStep(collection["TabNumber"].ToInt()));
        }

        [Authorize(Roles = "MREG_W,MREG_U,MREG_R,MREG_RA,SMRT_USER,SMRT_MANAGER,SMRT_SK")]
        public ActionResult SearchRegistration()
        {
            var searchRegVM = new SearchRegistrationVM();

            using (var proxy = new SmartRegistrationServiceProxy())
            {
                searchRegVM.RegistrationSearchResults = proxy.RegistrationSearch(new RegistrationSearchCriteria()
                {
                    StatusID = 0,//Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegNew),
                    IsMobilePlan = true,
                    UserID = Util.SessionAccess == null ? 0 : Util.SessionAccess.UserID,
                    PageSize = Properties.Settings.Default.PageSize
                });
            }

            Session["RegMobileReg_FromSearch"] = true;

            return View(searchRegVM);
        }

        #region Added by patanjali on 30-03-2013 to support Store Keeper and Cashier

        [Authorize(Roles = "MREG_W,MREG_U,MREG_R,MREG_RA,SMRT_MANAGER,SMRT_SK")]
        public ActionResult StoreKeeper()
        {
            var storeKeeperVM = new StoreKeeperVM();

            using (var proxy = new SmartRegistrationServiceProxy())
            {
                storeKeeperVM.RegistrationSearchResults = proxy.StoreKeeperSearch(new RegistrationSearchCriteria()
                {
                    StatusID = 0,//Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegNew),
                    IsMobilePlan = true,
                    UserID = Util.SessionAccess == null ? 0 : Util.SessionAccess.UserID,
                    PageSize = Properties.Settings.Default.PageSize
                });
            }

            Session["RegMobileReg_FromSearch"] = true;

            return View(storeKeeperVM);
        }

        [Authorize(Roles = "MREG_W,MREG_U,MREG_R,MREG_RA,SMRT_MANAGER")]
        public ActionResult Cashier()
        {
            var cashierVM = new CashierVM();

            using (var proxy = new SmartRegistrationServiceProxy())
            {
                cashierVM.RegistrationSearchResults = proxy.CashierSearch(new RegistrationSearchCriteria()
                {
                    StatusID = 0,//Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegNew),
                    IsMobilePlan = true,
                    UserID = Util.SessionAccess == null ? 0 : Util.SessionAccess.UserID,
                    PageSize = Properties.Settings.Default.PageSize
                });
            }

            Session["RegMobileReg_FromSearch"] = true;

            return View(cashierVM);
        }

        #endregion

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult BlacklistChecking(int type)
        {
            ResetAllSession(type);

            return View(new PersonalDetailsVM());
        }

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult BlacklistResult(bool result)
        {
            ViewBag.isBlacklisted = result;

            var resultMsg = Session[SessionKey.RegMobileReg_ResultMessage.ToString()];

            return View();
        }

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SubLinePlan(bool isBack = false)
        {
            if (!isBack)
                ClearSubLineSession();

            var sublinePkgVM = new SublinePackageVM();
            sublinePkgVM.PackageVM = GetAvailablePackages(true);
            sublinePkgVM.SelectedMobileNumber = Session["RegMobileSub_MobileNo"] == null ? new List<string>() : (List<string>)Session["RegMobileSub_MobileNo"];
            sublinePkgVM.NoOfDesireMobileNo = Properties.Settings.Default.NoOfDesireMobileNo;

            Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(true);
            return View(sublinePkgVM);
        }

        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SubLineVAS()
        {
            var sublineVasVM = new SublineVASVM();
            Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(true);
            sublineVasVM.VasVM = GetAvailablePackageComponents(Session["RegMobileSub_PkgPgmBdlPkgCompID"].ToInt(), true);

            return View(sublineVasVM);
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_R,MREG_RA")]
        public ActionResult MediaGallery()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectDevice(DeviceOptionVM devOptVM)
        {
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = devOptVM.TabNumber;

            var options = (!string.IsNullOrEmpty(devOptVM.SelectedDeviceOption)) ? devOptVM.SelectedDeviceOption.Split('_').ToList() : null;
            if (options != null)
            {
                try
                {
                    if (devOptVM.TabNumber == (int)MobileRegistrationSteps.DeviceCatalog)
                    {
                        // selected different Device Option
                        if (Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToInt() != options[1].ToInt() ||
                            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToInt() != options[2].ToInt())
                        {
                            Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = options[1];
                            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = options[2];

                            // reset session
                            Session["RegMobileReg_PhoneVM"] = null;
                            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
                            Session["RegMobileReg_SelectedModelID"] = null;
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
                            Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = null;
                            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
                            Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
                            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
                            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
                            Session[SessionKey.RegMobileReg_MobileNo.ToString()] = null;
                            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //LogException(ex);
                    //return RedirectException(ex);
                    throw;
                }
                return RedirectToAction(GetRegActionStep(devOptVM.TabNumber));
            }
            else
            {
                return View(GetRegActionStep(1));

            }

        }


        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectDeviceCatalog(PhoneVM phoneVM)
        {
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = phoneVM.TabNumber;
            try
            {
                if (phoneVM.TabNumber == (int)MobileRegistrationSteps.Plan)
                {
                    // selected different Model Image
                    if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToString2() != phoneVM.SelectedModelImageID)
                    {
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = phoneVM.SelectedModelImageID;

                        // reset session
                        Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
                        Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = null;
                        Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
                        Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
                        Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
                        Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
                    }
                }

                // Outright Sales
                if ((int)Session[SessionKey.RegMobileReg_Type.ToString()] == (int)MobileRegType.DeviceOnly)
                {
                    Session[SessionKey.RegMobileReg_TabIndex.ToString()] = (int)MobileRegistrationSteps.PersonalDetails;
                    phoneVM.TabNumber = (int)MobileRegistrationSteps.PersonalDetails;
                }
            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            return RedirectToAction(GetRegActionStep(phoneVM.TabNumber));
        }
        #region "Added by VLT"

        //Added by VLT ON APR 08 2013
        /// <summary>
        /// K2ContractCheck
        /// </summary>
        /// <returns>Bool</returns>
        public bool K2ContractCheck()
        {
            // return true;
            int principalCount = 0;
            int principalK2CheckCount = 0;
            bool K2Check = false;
            if (Session[SessionKey.PPID.ToString()] != null)
            {
                if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                {

                    if (Session[SessionKey.ArticleId.ToString()] != null)
                    {
                        if (ConfigurationManager.AppSettings["k2articleids"] != null)
                        {
                            if (ConfigurationManager.AppSettings["k2articleids"].ToString().Contains(Session[SessionKey.ArticleId.ToString()].ToString()))
                            {

                                using (var proxys = new SmartKenanServiceProxy())
                                {
                                    SubscriberICService.retrieveAcctListByICResponse AcctListByICResponse = new SubscriberICService.retrieveAcctListByICResponse();
                                    AcctListByICResponse = (SubscriberICService.retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()];
                                    principalCount = AcctListByICResponse.itemList.Count;
                                    foreach (var item in AcctListByICResponse.itemList)
                                    {
                                        int contractCount = 0;
                                        int months = 0;
                                        if (item.ServiceInfoResponse != null)
                                        {
                                            if (item.ServiceInfoResponse.prinSuppInd != "S")
                                            {
                                                var kenanReq = new RetrieveComponentInfoRequest();
                                                kenanReq.ExternalID = item.ExternalId;
                                                var kenanResp = new RetrieveComponentInfoResponse();

                                                kenanResp = proxys.RetrieveComponentInfo(kenanReq);


                                                if (ConfigurationManager.AppSettings["k2contacttypes"] != null)
                                                {
                                                    string[] strArrContractTypes = ConfigurationManager.AppSettings["k2contacttypes"].ToString().Split(',');

                                                    foreach (string contractType in strArrContractTypes)
                                                    {
                                                        var contract = kenanResp.Contracts.Where(c => c.ContractType == contractType).SingleOrDefault();
                                                        if (contract != null)
                                                        {
                                                            if (DateTime.Now < contract.EndDate)
                                                            {
                                                                contractCount++;
                                                                //TimeSpan t = (contract.EndDate - DateTime.Now);
                                                                months = months + (contract.EndDate.Year - DateTime.Now.Year) * 12 + contract.EndDate.Month - DateTime.Now.Month;

                                                            }
                                                        }
                                                    }

                                                }
                                            }

                                            if (contractCount >= 2)
                                            {
                                                Session[SessionKey.RegK2_Status.ToString()] = false;
                                                return false;
                                            }
                                            if (months > 6)
                                            {
                                                Session[SessionKey.RegK2_Status.ToString()] = false;
                                                return false;

                                            }


                                            if (contractCount < 2 || months <= 6)
                                            {
                                                principalK2CheckCount++;
                                            }

                                            if (Session[SessionKey.RegMobileReg_ContractID.ToString()] != null)
                                            {
                                                int ContractsDuration = GetContractDuration(Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt());

                                                if (months > 6 && months < 18)
                                                    if (ContractsDuration == 12)
                                                    {
                                                        Session[SessionKey.RegK2_Status.ToString()] = false;
                                                        return false;
                                                    }
                                                if (months > 18) { Session[SessionKey.RegK2_Status.ToString()] = false; return false; }
                                            }

                                        }
                                    }
                                }

                            }
                        }

                    }




                }

            }

            if (principalCount == principalK2CheckCount)
            {
                Session[SessionKey.RegK2_Status.ToString()] = true;
                return true;
            }
            else
            {
                Session[SessionKey.RegK2_Status.ToString()] = false;
                return false;
            }

        }
        //End by VLT ON APR 08 2013

        #endregion
        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectPlan(PackageVM packageVM)
        {
            //VLT ADDED CODE to check for PPID verification
            if (Session[SessionKey.PPID.ToString()].ToString() != "")
            {
                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null))
                {
                    Session.Contents.Remove("RedirectURL");
                }

                Session[SessionKey.RegMobileReg_TabIndex.ToString()] = packageVM.TabNumber;
                try
                {
                    if (packageVM.TabNumber == (int)MobileRegistrationSteps.Vas)
                    {
                        // selected different Plan
                        if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt() != packageVM.SelectedPgmBdlPkgCompID)
                        {
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = packageVM.SelectedPgmBdlPkgCompID;

                            // Retrieve Plan Min Age
                            using (var proxy = new SmartCatelogServiceProxy())
                            {
                                var pbpc = proxy.PgmBdlPckComponentGet(new int[] { packageVM.SelectedPgmBdlPkgCompID }).SingleOrDefault();

                                Session["ContractId"] = pbpc.KenanCode;

                                var bundleID = pbpc.ParentID;
                                var programID = proxy.PgmBdlPckComponentGet(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                                {
                                    PgmBdlPckComponent = new PgmBdlPckComponent()
                                    {
                                        ChildID = bundleID,
                                        LinkType = Properties.Settings.Default.Program_Bundle
                                    },
                                    Active = true
                                })).SingleOrDefault().ParentID;

                                var minAge = proxy.ProgramGet(new int[] { programID }).SingleOrDefault().MinAge;
                                Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = minAge;
                            }

                            // reset session
                            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
                            Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
                            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
                            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
                        }
                    }

                }
                catch (Exception ex)
                {
                    //LogException(ex);
                    //return RedirectException(ex);
                    throw;
                }
                return RedirectToAction(GetRegActionStep(packageVM.TabNumber));
            }
            else
            {
                Session[SessionKey.RedirectURL.ToString()] = "GuidedSales/PlanSearch";
                return RedirectToAction("Index", "Home");
            }
        }



        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult SelectVAS(ValueAddedServicesVM vasVM)
        {
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = vasVM.SelectedContractID;
            //  K2ContractCheck();

            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = vasVM.TabNumber;
            //modefied by chetan
            var selectedVasIDs = "";


            if (vasVM.SelectedVasIDs != null)
            {
                if (vasVM.SelectedVasIDs != null && !Regex.IsMatch(vasVM.SelectedVasIDs, @"^,+$"))
                {
                    selectedVasIDs = vasVM.SelectedVasIDs;
                    Session["RegMobileReg_DataPkgID"] = selectedVasIDs.Remove(4);
                }
            }
            else
            {
                if (!ReferenceEquals(Session["RegMobileReg_DataPkgID"], null))
                {
                    Session.Contents.Remove("RegMobileReg_DataPkgID");
                }
                if (!ReferenceEquals(Session[SessionKey.RegMobileReg_VasIDs.ToString()], null))
                {
                    Session.Contents.Remove("RegMobileReg_VasIDs");
                }
            }

            try
            {
                if (vasVM.TabNumber == (int)MobileRegistrationSteps.MobileNo && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) || !string.IsNullOrEmpty(vasVM.SelectedContractID)))
                {
                    if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                    {
                        #region VLT ADDED CODE
                        //Contract check for existing users
                        //Retrieve data from session
                        #endregion VLT ADDED CODE
                        if (!string.IsNullOrEmpty(Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()))
                            if (vasVM.SelectedVasIDs != null)
                                selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()), "");

                        selectedVasIDs += vasVM.SelectedContractID + ",";
                    }

                    vasVM.SelectedVasIDs = selectedVasIDs;

                    Session[SessionKey.RegMobileReg_VasIDs.ToString()] = selectedVasIDs;
                    Session[SessionKey.RegMobileReg_ContractID.ToString()] = vasVM.SelectedContractID;

                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        Session[SessionKey.RegMobileReg_DataplanID.ToString()] = proxy.GettllnkContractDataPlan(Convert.ToInt32(Session[SessionKey.RegMobileReg_ContractID.ToString()]));
                    }

                }
            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == true)
            {
                return RedirectToAction(GetRegActionStep(vasVM.TabNumber + 1));
            }
            else
            {
                if (!string.IsNullOrEmpty(Session[SessionKey.MobileRegType.ToString()].ToString()))
                {
                    string MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();
                    if (MobileRegType == "Planonly")
                    {
                        return RedirectToAction(GetRegActionStep(MobileRegistrationSteps.PersonalDetails.ToInt()), new { type = 2 });
                    }
                    return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
                }
                else
                {
                    return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
                }
            }
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C")]
        [HandleError]
        [ValidateInput(false)]
        public ActionResult SelectMobileNo(FormCollection collection, MobileNoVM mobileNoVM)
        {
            try
            {
                if (collection["submit1"].ToString() == "subline")
                {
                    ClearSubLineSession();
                    return RedirectToAction("SubLinePlan");
                }
                else if (collection["submit1"].ToString() == "search")
                {
                    return RedirectToAction("SelectMobileNo", new { desireNo = mobileNoVM.DesireNo });
                }
                else if (collection["submit1"].ToInt() > 0)
                {
                    var squeenceNo = collection["submit1"].ToInt();
                    var subLines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
                    var removeItem = subLines.Where(a => a.SequenceNo == squeenceNo).SingleOrDefault();
                    subLines.Remove(removeItem);

                    for (int i = 1; i <= subLines.Count(); i++)
                    {
                        subLines[i - 1].SequenceNo = i;
                    }

                    Session[SessionKey.RegMobileReg_SublineVM.ToString()] = subLines;

                    return RedirectToAction(GetRegActionStep((int)Session[SessionKey.RegMobileReg_TabIndex.ToString()]));
                }
                else
                {
                    Session[SessionKey.RegMobileReg_TabIndex.ToString()] = mobileNoVM.TabNumber;

                    var mobileNos = new List<string>();
                    #region VLT ADDED CODE by Rajeswari on 7thMar to resolve back button issue
                    if (mobileNoVM != null && mobileNoVM.SelectedMobileNo != null)
                    #endregion VLT ADDED CODE ends here by Rajeswari on 7thMar

                    //if (mobileNoVM != null)
                    {
                        if (!mobileNoVM.SelectedMobileNo.StartsWith("60"))
                        {
                            mobileNoVM.SelectedMobileNo = "60" + mobileNoVM.SelectedMobileNo;
                        }


                        mobileNos.Add(mobileNoVM.SelectedMobileNo);
                    }
                    //for (int i = 0; i < Properties.Settings.Default.NoOfDesireMobileNo; i++)
                    //{
                    //    mobileNos.Add(collection["txtMobileNo_" + i].ToString2());
                    //}

                    if ((int)MobileRegistrationSteps.PersonalDetails == mobileNoVM.TabNumber)
                    {
                        Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;
                    }
                }

            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            return RedirectToAction(GetRegActionStep(mobileNoVM.TabNumber));
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult PersonalDetails(PersonalDetailsVM personalDetailsVM)
        {
            try
            {
                if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.Summary)
                {
                    personalDetailsVM.Customer.Gender = "M";
                    personalDetailsVM.Customer.IDCardTypeID = personalDetailsVM.InputIDCardTypeID.ToInt();
                    personalDetailsVM.Customer.IDCardNo = personalDetailsVM.InputIDCardNo;
                    /*Added by Rajeswari on 24th March to disable nationality*/
                    personalDetailsVM.Customer.NationalityID = personalDetailsVM.InputNationalityID;

                    Session[SessionKey.RegMobileReg_TabIndex.ToString()] = personalDetailsVM.TabNumber;
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    if (personalDetailsVM.Customer.PayModeID == Util.GetPaymentModeID(PAYMENTMODE.AutoBilling))
                    {
                        if (!ValidateCardDetails(personalDetailsVM.Customer))
                        {
                            return View(personalDetailsVM);
                        }
                    }
                }
                else
                {
                    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                    {
                        Session[SessionKey.RegMobileReg_TabIndex.ToString()] = (int)MobileRegistrationSteps.DeviceCatalog;
                        personalDetailsVM.TabNumber = (int)MobileRegistrationSteps.DeviceCatalog;
                    }
                }
            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            return RedirectToAction(GetRegActionStep(personalDetailsVM.TabNumber));
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_U,MREG_R,MREG_RA,SMRT_USER,SMRT_MANAGER,SMRT_SK")]
        public ActionResult SearchRegistration(SearchRegistrationVM searchRegVM)
        {
            try
            {
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    searchRegVM.RegistrationSearchResults = proxy.RegistrationSearch(new RegistrationSearchCriteria()
                    {
                        RegID = searchRegVM.RegistrationSearchCriteria.RegID,
                        IDCardNo = searchRegVM.RegistrationSearchCriteria.IDCardNo,
                        StatusID = searchRegVM.RegistrationSearchCriteria.StatusID,
                        QueueNo = searchRegVM.RegistrationSearchCriteria.QueueNo,
                        RegTypeID = searchRegVM.RegistrationSearchCriteria.RegTypeID,
                        IsMobilePlan = true,
                        UserID = Util.SessionAccess.UserID,
                        PageSize = Properties.Settings.Default.PageSize
                    });
                }

            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            return View(searchRegVM);
        }

        #region Added by Patanjali on 30-03-2013, Store Keeper and Cashier flow

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_U,MREG_R,MREG_RA,SMRT_MANAGER,SMRT_SK")]
        public ActionResult StoreKeeper(StoreKeeperVM storeKeeperVM)
        {
            try
            {
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    storeKeeperVM.RegistrationSearchResults = proxy.StoreKeeperSearch(new RegistrationSearchCriteria()
                    {
                        RegID = storeKeeperVM.RegistrationSearchCriteria.RegID,
                        IDCardNo = storeKeeperVM.RegistrationSearchCriteria.IDCardNo,
                        StatusID = storeKeeperVM.RegistrationSearchCriteria.StatusID,
                        QueueNo = storeKeeperVM.RegistrationSearchCriteria.QueueNo,
                        RegTypeID = storeKeeperVM.RegistrationSearchCriteria.RegTypeID,
                        Trn_Type = storeKeeperVM.RegistrationSearchCriteria.Trn_Type,
                        //Trans_Type = storeKeeperVM.RegistrationSearchCriteria.Trans_Type,
                        IsMobilePlan = true,
                        UserID = Util.SessionAccess.UserID,
                        PageSize = Properties.Settings.Default.PageSize
                    });
                }

            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            return View(storeKeeperVM);
        }

        [HttpPost]
        //[Authorize(Roles = "MREG_W,MREG_U,MREG_R,MREG_RA,SMRT_MANAGER")]
        public ActionResult Cashier(CashierVM cashierVM)
        {
            try
            {
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    cashierVM.RegistrationSearchResults = proxy.CashierSearch(new RegistrationSearchCriteria()
                    {
                        RegID = cashierVM.RegistrationSearchCriteria.RegID,
                        IDCardNo = cashierVM.RegistrationSearchCriteria.IDCardNo,
                        StatusID = cashierVM.RegistrationSearchCriteria.StatusID,
                        QueueNo = cashierVM.RegistrationSearchCriteria.QueueNo,
                        RegTypeID = cashierVM.RegistrationSearchCriteria.RegTypeID,
                        IsMobilePlan = true,
                        UserID = Util.SessionAccess.UserID,
                        PageSize = Properties.Settings.Default.PageSize
                    });
                }

            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            return View(cashierVM);
        }

        #endregion

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult BlacklistChecking(string idCardNo, string idCardTypeID)
        {
            try
            {
                Session[SessionKey.RegMobileReg_IDCardNo.ToString()] = idCardNo;
                Session[SessionKey.RegMobileReg_IDCardType.ToString()] = idCardTypeID;

                var idCardType = new IDCardType();
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    idCardType = proxy.IDCardTypeGet(idCardTypeID.ToInt());
                }

                using (var proxy = new SmartKenanServiceProxy())
                {
                    var resp = proxy.BusinessRuleCheck(new BusinessRuleRequest()
                    {
                        RuleNames = Properties.Settings.Default.BusinessRule_MOBILERuleNames.Split('|'),
                        IDCardNo = idCardNo,
                        IDCardType = idCardType.KenanCode
                    });

                    Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] = resp.IsBlacklisted;
                    Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = resp.ResultMessage;
                }

                #region BlacklistChecking Commented on 20121205 by Ben

                //var blacklistCheckReq = new BlacklistCheckRequest()
                //{
                //    CreditCheckRequest = ConstructInternalCheck(idCardNo, idCardType.KenanCode),
                //    ExternalCheckXML = ConstructExternalCheck(new ExternalCheck()
                //    {
                //        ExternalIdValue = idCardNo,
                //        ExternalIdType = idCardType.KenanCode
                //    })
                //};

                //using (var proxy = new SmartKenanServiceProxy())
                //{
                //    var resp = proxy.BlacklistCheck(blacklistCheckReq);

                //    if (resp.IsExternalBlacklisted || resp.IsInternalBlacklisted)
                //    {
                //        isBlacklisted = true;
                //        Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = resp.ResultMessage;
                //    }

                //    Session["RegMobileReg_ExternalBlacklist"] = resp.IsExternalBlacklisted;
                //    Session["RegMobileReg_InternalBlacklist"] = resp.IsInternalBlacklisted;

                //    // For testing purposes
                //    //isBlacklisted = true;
                //    //Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = "IC number is blacklisted";
                //}

                #endregion
            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }

            return Json(Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()].ToBool());
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult BlacklistResult(FormCollection collection)
        {
            try
            {
                var isBlacklist = collection["isBlacklisted"].ToString();

                if (collection["submit"].ToString2() == "back")
                {
                    return View("BlacklistChecking");
                }
                //else if (collection["submit"].ToString() == "skip")
                //{


                //    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
                //        return RedirectToAction("SelectDevice");
                //    else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                //        return RedirectToAction("SelectPlan");
                //}
                else if (collection["submit"].ToString2() == "next" || collection["submit"].ToString() == "skip")
                {
                    if (collection["submit"].ToString() == "skip")
                        Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] = (isBlacklist == "True") ? true : false;

                    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan || Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                        return RedirectToAction("SelectDevice");
                    else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                        return RedirectToAction("SelectPlan");
                }

                Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;

            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,SMRT_USER,SMRT_MANAGER,SMRT_SK")]
        [DoNoTCache]
        public ActionResult MobileRegSummary(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            var resp = new RegistrationSvc.RegistrationCreateResp();
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = personalDetailsVM.TabNumber;
            try
            {
                #region PaymentRecived
                if (collection["submit1"].ToString() == "PaymentRecived")
                {
                    using (var proxy = new SmartRegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                        });
                    }
                    /*1.If device+plan genarate IMPOS file place in Shared path then redirect to new page with file name
                     */
                    if (Session[SessionKey.RegMobileReg_Type.ToString()] != null)
                    {
                        if (Convert.ToInt32(Session[SessionKey.RegMobileReg_Type.ToString()].ToString()) == (int)MobileRegType.DevicePlan)
                            using (var proxys = new SmartCatelogServiceProxy())
                            {
                                try
                                {
                                    IMPOSDetailsResp res = proxys.GetIMOPSDetails(personalDetailsVM.RegID);
                                    if (res != null)
                                    {
                                        //string fileName = Util.SessionAccess.User.Org.OrganisationId + "_" + res.ModelID + "_" + res.RegID + "_" + DateTime.Now.Date.ToString("yyyyMMdd");

                                        //saveFile(fileName, ConfigurationManager.AppSettings["POSNetworkPath"].ToString(), personalDetailsVM.RegID, res);
                                        return RedirectToAction("PaymntReceived");

                                    }
                                }
                                catch (Exception)
                                {
                                    return RedirectToAction("PaymntReceived");
                                }


                            }
                        else
                        {
                            //For Plan Only
                            return RedirectToAction("PlanPaymntReceived");
                        }
                    }
                    else
                    {
                        return RedirectToAction("MobileRegCanceled", new { regID = personalDetailsVM.RegID });
                    }
                }
                #endregion

                if (collection["submit1"].ToString() == "close")
                {
                    if (personalDetailsVM.RegID == 0)
                    {
                        using (var proxy = new SmartRegistrationServiceProxy())
                        {
                            resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo),
                            ConstructCustomer(), ConstructRegMdlGroupModel(), ConstructRegAddress(),
                            ConstructRegPgmBdlPkgComponent(), ConstructRegStatus(), ConstructRegSuppLines(),
                            ConstructRegSuppLineVASes());

                            //resp = proxy.RegistrationCreate_Smart(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo),
                            //ConstructCustomer(), ConstructRegMdlGroupModel(), ConstructRegAddress(),
                            //ConstructRegPgmBdlPkgComponent(), ConstructRegStatus(), ConstructRegSuppLines(),
                            //ConstructRegSuppLineVASes(), ConstructPackage()); //Added for Smart Intergration temp







                            personalDetailsVM.RegID = resp.ID;
                        }
                        using (var proxy = new SmartRegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegBreFail)
                            });
                        }
                        return RedirectToAction("MobileRegBreFail", new { regID = personalDetailsVM.RegID });
                    }
                    else
                    {
                        using (var proxy = new SmartRegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                            });
                        }
                        return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
                    }

                }
                if (collection["submit1"].ToString() == "createpackage")
                {
                    //bool isCreated = false;
                    //using (var proxy_createacc = new SmartKenanServiceProxy())
                    //{
                    //    CenterOrderContractRequest Req = new CenterOrderContractRequest();
                    //    Req.orderId = personalDetailsVM.RegID.ToString();
                    //    isCreated = proxy_createacc.CenterOrderContractCreation(Req);

                    //}
                    //if (isCreated)
                    //{
                    //    using (var proxy = new SmartRegistrationServiceProxy())
                    //    {
                    //        proxy.RegistrationCancel(new RegStatus()
                    //        {
                    //            RegID = personalDetailsVM.RegID,
                    //            Active = true,
                    //            CreateDT = DateTime.Now,
                    //            StartDate = DateTime.Now,
                    //            LastAccessID = Util.SessionAccess.UserName,
                    //            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_SvcPendingActivate)
                    //        });
                    //    }
                    //    string username = string.Empty;
                    //    if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                    //    {
                    //        username = Request.Cookies["CookieUser"].Value;
                    //    }
                    //    using (var proxy = new SmartRegistrationServiceProxy())
                    //    {
                    //        proxy.RegistrationUpdate(new Online.Registration.DAL.Models.Registration()
                    //        {
                    //            ID = personalDetailsVM.RegID,
                    //            SalesPerson = username,
                    //            LastAccessID = Util.SessionAccess.UserName,

                    //            //SimModelId = ReferenceEquals(Session["SimModelTypeId"].ToString2(), null) ? 0 : Convert.ToInt32(Session["SimModelTypeId"].ToString2()),

                    //        });
                    //    }


                    //}

                    //using (var proxy = new SmartRegistrationServiceProxy())
                    //{
                    //    proxy.RegistrationClose(new RegStatus()
                    //    {
                    //        RegID = personalDetailsVM.RegID,
                    //        Active = true,
                    //        CreateDT = DateTime.Now,
                    //        StartDate = DateTime.Now,
                    //        LastAccessID = Util.SessionAccess.UserName,
                    //        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RFP)
                    //    });
                    //}

                    var reg = new DAL.Models.Registration();
                    //var regMdlGrpModels = new List<RegMdlGrpModel>();
                    OrderSummaryVM orderSummaryVM = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                    }

                    List<string> errorValidationList = new List<string>();

                    if (string.IsNullOrEmpty(reg.SIMSerial) && (reg.IsSImRequired_Smart || personalDetailsVM.IsSImRequired_Smart))
                    {
                        // fill error msg
                        errorValidationList.Add("Sim serial");
                    }
                    if (string.IsNullOrEmpty(reg.IMEINumber) && !string.IsNullOrEmpty(orderSummaryVM.ModelImageUrl))
                    {
                        // fill error msg
                        errorValidationList.Add("IMEI number");
                    }
                    if (errorValidationList.Any())
                    {
                        TempData[SessionKey.errorValidationList.ToString()] = errorValidationList;
                        using (var proxy = new UserServiceProxy())
                        {
                            String errorTrace = "fail insert inventory" + (String)(errorValidationList.Contains("Sim serial") ? " SIM(" + personalDetailsVM.RegSIMSerial + ") " : "") + (String)(errorValidationList.Contains("IMEI number") ? " IMEI(" + personalDetailsVM.RegIMEINumber + ") " : "") + "for regID: " + reg.ID;
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.PlanOnly), errorTrace, "", "");
                        }
                        return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                    }

                    #region 11052015 - Anthony - Generate RF for iContract
                    try
                    {
                        using (var updateProxy = new UpdateServiceProxy())
                        {
                            if (!updateProxy.FindDocument(Constants.CONTRACT, personalDetailsVM.RegID))
                            {
                                String contractHTMLSource = string.Empty;
                                contractHTMLSource = Util.RenderPartialViewToString(this, "PrintContractDetails", WebHelper.Instance.GetContractDetailsViewModel_SMART(personalDetailsVM.RegID, false));
                                contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
                                contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
                                contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide");
                                if (!string.IsNullOrEmpty(contractHTMLSource))
                                {
                                    Byte[] PDFBytes = Util.convertHTMLStringToPDFByteArray(contractHTMLSource, personalDetailsVM.RegID);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //Logger.Error("ICONTRACT EXCEPTION = " + ex);
                        WebHelper.Instance.LogExceptions(this.GetType(), ex);
                    }
                    #endregion

                    using (var proxy = new SmartRegistrationServiceProxy())
                    {
                        string username = string.Empty;
                        if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                        {
                            username = Request.Cookies["CookieUser"].Value;
                        }
                        proxy.RegistrationUpdate(new DAL.Models.Registration()
                        {
                            ID = personalDetailsVM.RegID,
                            SalesPerson = username,
                            LastAccessID = Util.SessionAccess.UserName,

                            //SimModelId = ReferenceEquals(Session["SimModelTypeId"].ToString2(), null) ? 0 : Convert.ToInt32(Session["SimModelTypeId"].ToString2()),

                        });
                    }

                    using (var proxy = new KenanServiceProxy())
                    {
                        String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                        if (!ReferenceEquals(Action, String.Empty))
                            return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                        //proxy.CreateIMPOSFile(personalDetailsVM.RegID);
                    }

                    return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                }
                if (collection["submit1"].ToString() == "cancel")
                {
                    string reason = string.Empty;
                    int prevStatus = WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID);
                    reason = ReferenceEquals(reason, null) ? "No reason provided." : collection["hdnReasonCancel"].ToString();

                    using (var proxy = new SmartRegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan_SK)
                        });


                        string username = string.Empty;
                        if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                        {
                            username = Request.Cookies["CookieUser"].Value;
                        }
                        proxy.RegistrationUpdate(new DAL.Models.Registration()
                        {
                            ID = personalDetailsVM.RegID,
                            SalesPerson = username,
                            LastAccessID = Util.SessionAccess.UserName,

                            //SimModelId = ReferenceEquals(Session["SimModelTypeId"].ToString2(), null) ? 0 : Convert.ToInt32(Session["SimModelTypeId"].ToString2()),

                        });


                        proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                        {
                            RegID = personalDetailsVM.RegID,
                            CancelReason = reason,
                            CreateDT = DateTime.Now,
                            LastUpdateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });


                        var totalPrice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID);
                        if (prevStatus == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment) && WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID) == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan_SK) && totalPrice > 0)
                        {
                            WebHelper.Instance.SendDetailsToWebPos(personalDetailsVM.RegID, Constants.StatusWebPosToCancel);
                        }
                    }
                    return RedirectToAction("MobileRegCanceled", new { regID = personalDetailsVM.RegID });
                }

                if (collection["submit1"].ToString() == "createAcc")
                {
                    try
                    {
                        CreateKenanAccount(personalDetailsVM.RegID);

                        return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });

                    }
                    catch (Exception ex)
                    {
                        LogException(ex);
                        using (var proxy = new SmartRegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                    }
                }

                if (collection["submit1"].ToString() == "activateSvc")
                {
                    try
                    {
                        FulfillKenanAccount(personalDetailsVM.RegID);
                        return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                    }
                    catch (Exception ex)
                    {
                        LogException(ex);
                        using (var proxy = new SmartRegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                    }
                }

                //if (collection["submit1"].ToString() == "createAccWithExc")
                //{
                //    //CreateKenanAccount(personalDetailsVM.RegID);

                //    //return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
                //}

                if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.Submit)
                {
                    //if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == false)
                    //{
                    //    // Update Dealer Mobile No
                    //    var kenanResp = new MobileNoUpdateResponse();

                    //    using (var proxy = new SmartKenanServiceProxy())
                    //    {
                    //        kenanResp = proxy.MobileNoUpdate(ConstructMobileNoUpdate());
                    //    }

                    //    if (!kenanResp.Success)
                    //    {
                    //        personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

                    //        ModelState.AddModelError(string.Empty, string.Format("Failed to update Dealer Mobile Number ({0}).",
                    //            ((List<string>) Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2()));

                    //        return View("MobileRegSummary", personalDetailsVM);
                    //    }
                    //}
                    // submit registration

                    using (var proxy = new SmartRegistrationServiceProxy())
                    {
                        resp = proxy.RegistrationCreate_Smart(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo),
                                                        ConstructCustomer(), ConstructRegMdlGroupModel(), ConstructRegAddress(),
                                                        ConstructRegPgmBdlPkgComponent(), ConstructRegStatus(), ConstructRegSuppLines(),
                                                        ConstructRegSuppLineVASes(), ConstructPackage(), ConstructCMSSID(), null, null);


                        //resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo),
                        //                                ConstructCustomer(), ConstructRegMdlGroupModel(), ConstructRegAddress(),
                        //                                ConstructRegPgmBdlPkgComponent(), ConstructRegStatus(), ConstructRegSuppLines(),
                        //                                ConstructRegSuppLineVASes());//Added for Smart Intergration temp



                        var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
                        if (biometricID != 0)
                        {
                            var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
                            biometric.RegID = resp.ID;

                            proxy.BiometricUpdate(biometric);
                        }
                    }

                    #region ADDED BY Rajeswari on 15th March
                    if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
                    {
                        Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
                    }
                    #endregion
                    #region VLT ADDED CODE on 21stFEB for email functionality
                    if (resp.ID > 0)
                    {

                        LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
                        Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                        objRegDetails.CreatedDate = DateTime.Now;
                        objRegDetails.UserName = Util.SessionAccess.UserName;
                        objRegDetails.RegId = resp.ID;

                        //selected sim card type is assigned
                        if (!string.IsNullOrEmpty(collection["SimCardTypeSelected"]))
                            objRegDetails.SimCardType = collection["SimCardTypeSelected"].ToString2();

                        objRegDetailsReq.LnkDetails = objRegDetails;

                        /* BEGIN Added by VLT 5 Apr 2013 to add kenaninfo to database*/
                        if (Session["KenanCustomerInfo"] != null)
                        {
                            string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString().Split('╚');
                            KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
                            kenanInfo.FullName = strArrKenanInfo[0];
                            kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
                            kenanInfo.IDCardNo = strArrKenanInfo[2];
                            kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
                            kenanInfo.Address1 = strArrKenanInfo[4];
                            kenanInfo.Address2 = strArrKenanInfo[5];
                            kenanInfo.Address3 = strArrKenanInfo[6];
                            kenanInfo.Postcode = strArrKenanInfo[8];
                            kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
                            kenanInfo.RegID = resp.ID;
                            using (var proxys = new SmartRegistrationServiceProxy())
                            {
                                var kenanResp = new KenanCustomerInfoCreateResp();

                                //kenanResp = proxys.KenanCustomerInfoCreate(kenanInfo);
                            }
                        }
                        Session["KenanCustomerInfo"] = null;
                        /* END Added by VLT 5 Apr 2013 to add kenaninfo to database*/

                        ClearRegistrationSession();
                        return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                    }
                    #endregion VLT ADDED CODE on 21stFEB for email functionality
                }
                else if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.PersonalDetails)
                {
                    return RedirectToAction("PersonalDetails");
                }

            }
            catch (Exception ex)
            {
                Logger.Info("Throw Exception for Order ID:" + resp.ID + "Exception:" + ex);
                throw;
            }

            return RedirectToAction("MobileRegFail", new { regID = resp.ID });
        }

        protected void SendEmail(int id)
        {
            #region VLT ADDED CODE

            //VLT ADDED CODE by Rajeswari on 2st Feb for sending Email to customer

            var pdVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            RequestContext reqCtx = new RequestContext();
            Util.CheckSessionAccess(reqCtx);
            string emailAddr = Util.SessionAccess.User.EmailAddr;

            MailMessage mail = new MailMessage();
            mail.IsBodyHtml = true;
            SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"].ToString());
            mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToString());
            //mail.To.Add(ConfigurationManager.AppSettings["ToEmail"].ToString());
            mail.To.Add(pdVM.Customer.EmailAddr);
            mail.Subject = ConfigurationManager.AppSettings["Subject"].ToString();

            string gender = string.Empty;
            if ((pdVM.Customer.Gender != "") || (pdVM.Customer.Gender != null))
            {
                if (pdVM.Customer.Gender == "M")
                    gender = "Male";
                else
                    gender = "Female";
            }
            string paymentMode = string.Empty;

            if (pdVM.Customer.PayModeID.ToString() != null)
                paymentMode = Util.GetNameByID(RefType.PaymentMode, pdVM.Customer.PayModeID);
            else
                paymentMode = null;

            string customerTitle = string.Empty;
            if (pdVM.Customer.CustomerTitle != null)
                customerTitle = Util.GetNameByID(RefType.CustomerTitle, pdVM.Customer.CustomerTitleID) + ". ";

            string idCardTypeId = string.Empty;

            if (!ReferenceEquals(pdVM.Customer.IDCardTypeID, null))
                idCardTypeId = Util.GetNameByID(RefType.IDCardType, pdVM.Customer.IDCardTypeID);

            string dob = string.Empty;
            if (pdVM.Customer.DateOfBirth != null)
                dob = Util.FormatDate(pdVM.Customer.DateOfBirth);

            string language = string.Empty;
            if (!ReferenceEquals(pdVM.Customer.LanguageID, null))
            {
                language = Util.GetNameByID(RefType.Language, pdVM.Customer.LanguageID);
            }

            string nationality = string.Empty;
            if (!ReferenceEquals(pdVM.Customer.NationalityID, null))
                nationality = Util.GetNameByID(RefType.Nationality, pdVM.Customer.NationalityID);

            string raceId = string.Empty;
            if (!ReferenceEquals(pdVM.Customer.RaceID, null))
                raceId = Util.GetNameByID(RefType.Race, pdVM.Customer.RaceID);

            string stateId = string.Empty;
            if (!ReferenceEquals(pdVM.Address.StateID, null))
                stateId = Util.GetNameByID(RefType.State, pdVM.Address.StateID);

            string cardTypeId = string.Empty;
            if (pdVM.Customer.CardTypeID != null)
                cardTypeId = Util.GetNameByID(RefType.CardType, pdVM.Customer.CardTypeID.Value);

            string bundlePkgId = string.Empty;




            StringBuilder sb = new StringBuilder();
            StringBuilder sbVas = new StringBuilder();
            var regFormVM = new RegistrationFormVM();
            var modelImageIDs = new List<int>();
            var pbpcIDs = new List<int>();
            var suppLineVASIDs = new List<int>();
            var suppLineForms = new List<SuppLineForm>();
            var regMdlGrpModels = new List<RegMdlGrpModel>();
            string additionalCharges = string.Empty;
            StringBuilder sbPlan = new StringBuilder();
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                regFormVM.Registration = proxy.RegistrationGet(id);

                // Customer
                regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

                // Address
                var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                {
                    Active = true,
                    Address = new Address()
                    {
                        RegID = id
                    }
                }).ToList();
                regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

                // RegMdlGrpModels
                var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                {
                    RegMdlGrpModel = new RegMdlGrpModel()
                    {
                        RegID = id
                    }
                }).ToList();
                if (regMdlGrpModelIDs.Count() > 0)
                {
                    regMdlGrpModels = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
                    modelImageIDs = regMdlGrpModels.Select(a => a.ModelImageID).ToList();
                }

                // RegPgmBdlPkgComponent
                var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                {
                    RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                    {
                        RegID = id
                    }
                }).ToList();

                var pbpc = new List<RegPgmBdlPkgComp>();

                if (regPgmBdlPkgCompIDs.Count() > 0)
                    pbpc = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                if (pbpc.Count() > 0)
                    pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();


                // Reg Supp Line                
                var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = id } }).ToList();
                if (suppLineIDs.Count() > 0)
                {
                    var regSuppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    foreach (var regSuppLine in regSuppLines)
                    {
                        suppLineForms.Add(new SuppLineForm()
                        {
                            // supp line pbpc ID  
                            SuppLineBdlPkgName = regSuppLine.PgmBdlPckComponent.PlanType,
                            SuppLinePBPCID = regSuppLine.PgmBdlPckComponentID,

                            // supp line vas ID
                            SuppLineVASIDs = pbpc.Where(a => a.RegSuppLineID == regSuppLine.ID).Select(a => a.PgmBdlPckComponentID).ToList(),
                            MSISDN = regSuppLine.MSISDN1,


                        });
                        additionalCharges = regSuppLine.Reg.AdditionalCharges.ToString();
                        sb.AppendFormat("<b>MSISDN</b>");
                        sb.AppendFormat("</td>");
                        sb.AppendFormat("<td valign='top'>");
                        sb.AppendFormat(regSuppLine.MSISDN1);
                        sb.AppendFormat("</td>");

                    }
                }
            }
            using (var proxy = new SmartCatelogServiceProxy())
            {
                if (modelImageIDs.Count() > 0)
                {
                    var modelImages = proxy.ModelImageGet(modelImageIDs).ToList();
                    var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());

                    foreach (var regMdlGrpModel in regMdlGrpModels)
                    {
                        var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
                        regFormVM.DeviceForms.Add(new DeviceForm()
                        {
                            Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
                            Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
                            Price = regMdlGrpModel.Price.ToDecimal()
                        });
                    }
                }

                if (pbpcIDs.Count() > 0)
                {
                    var pgmBdlPkgComps = proxy.PgmBdlPckComponentGet(pbpcIDs).Distinct().ToList();
                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode).ToList();
                    regFormVM.PackageComponents = pgmBdlPkgComps.Where(a => a.LinkType == pcCode).ToList();
                }


                StringBuilder sbPlanName = new StringBuilder();
                for (int i = 0; i < regFormVM.BundlePackages.Count; i++)
                {
                    sbPlanName.Append(regFormVM.BundlePackages[i].ChildID + ",");
                }
                string[] planName = sbPlanName.ToString().Split(',');
                if (planName.Length != 0)
                {
                    sbPlan.AppendFormat("<tr>" +
                                      "<td>" +
                                      "</td>" +
                                      "<td style='width: 200px' align='center'>" +
                                          "<b>PRINCIPAL LINE</b>" +
                                      "</td>" +
                                      "<td style='width: 200px' align='center'>" +
                                          "<b>SUPPLEMENTARY LINE (1)</b>" +
                                      "</td>" +
                                      "<td style='width: 200px' align='center'>" +
                                          "<b>SUPPLEMENTARY LINE (2)</b>" +
                                      "</td>" +
                                  "</tr>");
                }
                for (int j = 0; j < planName.Length - 1; j++)
                {
                    sbPlan.AppendFormat("<tr>" +
                                        "<td valign='top'>" +
                                            "<b>Plan</b>" +
                                        "</td>" +
                                        "<td valign='top'>" +
                                            "<div style='float: left; border-style: solid; border-width: 1px; border-radius: 5px;padding: 3px 3px 3px 3px; width: 200px;'>" +
                                                Util.GetNameByID(RefType.Package, Convert.ToInt32(planName[j].ToString())) +
                                            "</div>" +
                                        "</td></tr>");

                }
                sbPlan.AppendFormat("<tr>" +
                                    "<td valign='top'>" +
                                        "<b>VAS</b>" +
                                    "</td></tr>");
                StringBuilder sbVasData = new StringBuilder();

                for (int i = 0; i < regFormVM.PackageComponents.Count; i++)
                {
                    sbVasData.Append(regFormVM.PackageComponents[i].ChildID + ",");

                }
                string[] vasName = sbVasData.ToString().Split(',');
                for (int j = 0; j < vasName.Length - 2; j++)
                {
                    sbPlan.AppendFormat("<tr><td></td><td valign='top'>" +
                                            "<div style='float: left; border-style: solid; border-width: 1px; border-radius: 5px;padding: 3px 3px 3px 3px; width: 200px;'>" +
                                                Util.GetNameByID(RefType.Package, Convert.ToInt32(vasName[j].ToString())) +
                                            "</div>" +
                                        "</td></tr>");
                }
                sbPlan.AppendFormat("<tr><td></td><td></td></tr>");
            }
            if (sb.Length == 0)
                sb = sb.Append("Not Available");

            if (additionalCharges == "")
                additionalCharges = "Not Available";

            string simSerial = pdVM.RegSIMSerial;
            if (simSerial == null)
                simSerial = "Not Available";

            string imeiNo = pdVM.RegIMEINumber;
            if (imeiNo == null)
                imeiNo = "Not Available";


            StringBuilder mailBody = new StringBuilder();
            mailBody.AppendFormat("Dear " + customerTitle + pdVM.Customer.FullName);
            mailBody.AppendFormat("<table align='center' width='500px' style='font-size:smaller;font-family:Times New Roman;'>" +
                                    "<tr>" +
                                        "<td>" +
                                            "<div style='font-size: large; font-weight: bolder; float: left;'>" +
                                               " Mobile Postpaid Retention Form</div>" +
                                            "<div style='float: right'>" +
                                                "<img alt='' src=cid:corpLogo height='30px' /></div>" +
                                        "</td>" +
                                    "</tr>" +
                //"<br/>" +
                                    "<tr>" +
                                    "<td style='width: 100%'>" +
                        "<table class='rfTable' width='670px'>" +
                            "<tr>" +
                                "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                    "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<tr>" +
                                            "<td >" +
                                                "<div>A. Individual</div>" +
                                            "</td>" +
                                        "</tr>" +
                                    "</table>" +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>Fullname *</b>" +
                                "</td>" +
                                "<td>" + customerTitle + pdVM.Customer.FullName +
                                "</td>" +
                                "<td>" +
                                    "<b>Gender *</b>" +
                                "</td>" +
                                "<td>" + gender +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>IDCardType</b>" +
                                "</td>" +
                                "<td>" + idCardTypeId +
                                "</td>" +
                                "<td>" +
                                    "<b>IDCardNo</b>" +
                                "</td>" +
                                "<td>" + pdVM.Customer.IDCardNo +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>DOB *</b>" +
                                "</td>" +
                                "<td>" + dob +
                                "</td>" +
                                "<td>" +
                                    "<b>Language *</b>" +
                                "</td>" +
                                "<td>" + language +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>Mobile No *</b>" +
                                "</td>" +
                                "<td>" + pdVM.Customer.ContactNo +
                                "</td>" +
                                "<td>" +
                                    "<b>Nationality *</b>" +
                                "</td>" +
                                "<td>" + nationality +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>Alt Contact No</b>" +
                                "</td>" +
                                "<td>" + pdVM.Customer.AlternateContactNo +
                                "</td>" +
                                "<td>" +
                                    "<b>Race</b>" +
                                "</td>" +
                                "<td>" + raceId +
                                "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>" +
                                    "<b>Email * </b>" +
                                "</td>" +
                                "<td>" + pdVM.Customer.EmailAddr +
                                "</td>" +
                                "<td>" +
                                "</td>" +
                                "<td>" +
                                "</td>" +
                            "</tr>" +
                        "</table>" +

                    "<table class='rfTable' width='670px'>" +
                        "<tr>" +
                            "<td colspan='2' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                    "<tr>" +
                                        "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                            "<div>" +
                                                "B. Address</div>" +
                                        "</td>" +
                                    "</tr>" +
                                "</table>" +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td style='width: 120px'>" +
                                "<b>Addresses *</b>" +
                            "</td>" +
                            "<td>" + pdVM.Address.Line1 + pdVM.Address.Line2 + pdVM.Address.Line3 +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td style='width: 120px'>" +
                                "<b>Town/City Name * </b>" +
                            "</td>" +
                            "<td>" + pdVM.Address.Town +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td style='width: 120px'>" +
                                "<b>Postcode *</b>" +
                            "</td>" +
                            "<td>" + pdVM.Address.Postcode +
                            "</td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td style='width: 120px'>" +
                                "<b>State/Province *</b>" +
                            "</td>" +
                            "<td>" + stateId +
                            "</td>" +
                        "</tr>" +
                    "</table>" +

                "<br />" +
                "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<tr>" +
                                    "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<div>" +
                                            "C. Monthly Bill Settlement Preference</div>" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                    "<tr>" +
                       " <td style='width: 120px'>" +
                            "<b>Payment Mode *</b>" +
                        "</td>" +
                        "<td colspan='3'>" + paymentMode +
                        "</td>" +
                    "</tr>" +
                "</table>" +
                "<br />" +

                "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable'  style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<tr>" +
                                    "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<div>" +
                                           "D. Service Package</div>" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                    "<tr>" +
                       "<td valign='top' colspan='4'>" +
                            "<table>" + sbPlan.ToString() +
                                    "<tr>" +
                                    "<td valign='top' style='width: 100px'></td>" +
                                    "<td valign='top' style='width: 100px'></td>" +
                                     "</tr>" +
                                    "<tr>" +
                                    "<td valign='top' style='width: 100px'><b>MSISDN</b></td>" +
                                    "<td valign='top' style='width: 100px'  colspan='3'>" + sb.ToString() + "</td>" +
                                     "</tr>" +

                                "<tr>" +
                                    "<td valign='top' style='width: 100px'>" +
                                        "<b>Additional Charges</b>" +
                                    "</td>" +
                                    "<td valign='top' colspan='3'>" + additionalCharges +
                                    "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td valign='top' style='width: 100px'>" +
                                        "<b>IMEI Number</b>" +
                                    "</td>" +
                                    "<td valign='top' colspan='3'>" + imeiNo +
                                    "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td valign='top' style='width: 100px'>" +
                                        "<b>SIM Serial</b>" +
                                    "</td>" +
                                    "<td valign='top' colspan='3'>" + simSerial +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                "</table>" +
               "<br/>" +

               //Code Added by VLT to include customer photos on 4 mar 2013//

               "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<tr>" +
                                    "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<div>" +
                                            "E. CUSTOMER D PHOTO</div>" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                    "<tr>" +
                        "<td valign='top' colspan='4'>" +
                            "<table>" +
                                "<tr>" +
                                    "<td align='left'>" +
                                        "<label id='lblFrontImage12' style='font-weight: bold; color: Black;'>Photo (front)</label><br />" +
                                        "<img alt='' src='" + regFormVM.Registration.CustomerPhoto + "' width='230px' height='200px' />" +
                                    "</td>" +
                                    "<td align='left'>" +
                                        "<label id='lblFrontImage12' style='font-weight: bold; color: Black;'>Photo (back)</label><br />" +
                                        "<img alt='' src='" + regFormVM.Registration.AltCustomerPhoto + "' width='230px' height='200px' />" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                "</table>" +
                "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<tr>" +
                                    "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<div>" +
                                            "F. CUSTOMER PHOTO</div>" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                    "<tr>" +
                        "<td valign='top' colspan='4'>" +
                            "<table>" +
                                "<tr>" +
                                    "<td align='left'>" +
                                        "<img alt='' src='" + regFormVM.Registration.Photo + "' width='230px' height='200px' />" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                "</table>" +
                 "<br />" +


                 "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td colspan='4' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                "<tr>" +
                                    "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        "<div>" +
                                            "G. DECLARATION</div>" +
                                    "</td>" +
                                "</tr>" +
                            "</table>" +
                        "</td>" +
                    "</tr>" +
                    "<tr>" +
                        "<td>" +
                            "<table>" +
                                "<tr>" +
                                    "<td>" +
                                        "I/We hereby declare: (a) that I/we wish to subscribe for the Services provided by " +
                                        "MMSSB; (b) that the above information provided is true and correct." +
                                        "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td>" +
                                       regFormVM.Registration.SignatureSVG +
                                    "</td>" +
                                "</tr>" +
                                "<tr>" +
                                    "<td>" +
                                        "____________________________________" +
                                        "<br />" +
                                        DateTime.Now.ToString("dd/MM/yyyy") +
                                    "</td>" +
                                "</tr>" +

                            "</table>" +
                        "</td>" +
                    "</tr>" +
                "</table>" +
                "<br/>" +
                //Code Added by VLT to include customer photos on 4 mar 2013//

                //Section-G code
                 "<table class='rfTable' width='670px'>" +
                    "<tr>" +
                        "<td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                            "<table class='rfTable' style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +

                                " <tr>" +

                                    " <td style='background-color: Black; color: White; text-transform: uppercase; font-weight: bolder;font-size: small;'>" +
                                        " <div>" +

                                        " H. Terms & Conditions</div>" +

                                    " </td>" +

                                " </tr>" +

                                " </table>" +
                                "</td>" +
                                "</tr>" +

                    " <tr><td><table>" +

                    " <tr>" +

                    " <td style='width: 49%; float: left'>" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>1.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>DEFINITIONS</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Addendum(s)&quot;</strong> means any addendum(s) or supplement(s)" +

                    " executed by Customer and accepted by MMSSB for value added, supplemental or additional" +

                    " Services." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Agreement&quot;</strong> means the agreement for Services made between" +

                    " MMSSB and a Customer in accordance with these terms and conditions, the Registration" +

                    " Form, the Addendums, the terms of services, policies and procedures of the individual" +

                    " rate plans and all other documents which are expressly agreed to form part of the" +

                    " Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Bank&quot;</strong> means the authorised banks or financial institutions" +

                    " or such other entity which may be nominated by MMSSB from time to time." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Bursa Malaysia&quot;</strong> means Bursa Malaysia Securities Berhad." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Card&quot;</strong> means the credit or charge card (as applicable)" +

                    " nominated by the Customer as payment for the Services and accepted by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Cardholder&quot;</strong> means the lawful and authorised user of" +

                    " the Card whose name is embossed thereon and whose signature appears on the Card." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Card Issuer(s)&quot;</strong> refers to any bank or legal entity which" +

                    " is the issuer for the Card." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Customer&quot;</strong> means the individual, sole proprietorship," +

                    " partnership, company or entity named in the Registration Form overleaf whose application" +

                    " for Services or any part thereof has been accepted and approved by MMSSB. An individual" +

                    " applicant must be at least 18 years of age." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Direct Debit&quot;</strong> means the direct debit bill payment service" +

                    " offered by MMSSB whereby a Customer’s periodic official bill statement may be automatically" +

                    " billed into the Customer’s Card account for settlement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Donor Network Operator&quot;</strong> means a mobile service provider" +

                    " from which a Mobile Number has been or is to be ported out." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.11" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;International Mobile Equipment Identification (IMEI)&quot;</strong>" +

                    " is a unique electronic serial number sealed into the Mobile Equipment." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.12" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;m-Billing&quot;</strong> is a service offered whereby the Customer’s" +

                    " periodic official bill statement may be viewed, and the Customer has the option" +

                    " to pay the outstanding amount using the Customer’s Card account for settlement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.13" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;MMSSB&quot;</strong> means Maxis Mobile Services Sdn Bhd (formerly" +

                    " known as Malaysian Mobile Services Sdn Bhd (Company No. 73315-V)." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.14" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;MAXIS&quot;</strong> is the service name of the Global System for" +

                    " Mobile Communications (GSM) Service." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.15" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Mobile Equipment&quot;</strong> means equipment with a transmitter" +

                    " and receiver which does not contain any Personal Information." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.16" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Mobile Number&quot; </strong>means Mobile Station International Subscriber" +

                    " Directory Number (MSISDN)." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.17" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Mobile Number Portability&quot;/&quot;MNP&quot; </strong>means the" +

                    " ability for Customers to change from one mobile service provider to another and" +

                    " retain their Mobile Number." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.18" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Mobile Phone&quot;</strong> is composed of the Mobile Equipment and" +

                    " SIM Card which facilitates the use of the Services." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.19" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Pay-By-Phone&quot;</strong> means the Pay-By-Phone service whereby" +

                    " the Customer’s periodic official bill statement per account may be charged to the" +

                    " Customer’s Card account for settlement as advised by the Customer via telephone" +

                    " for each payment." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.20" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Personal Information&quot;</strong> means information collected by" +

                    " MMSSB from a Customer including all information and details in relation to the Services" +

                    " provided by MMSSB to the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.21" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Port or Porting&quot;</strong> means the transfer of Customer’s Mobile" +

                    " Number from one mobile service provider to another." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.22" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Recipient Network Operator&quot; </strong>means a mobile service provider" +

                    " to which a Mobile Number has been or is to be ported in." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.23" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Registration Form&quot;</strong> means Customer’s duly completed application" +

                    " form for registration to subscribe to the Services, which has been accepted and" +

                    " approved by MMSSB, the form and content of which are set out overleaf." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.24" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Related Corporations&quot;</strong> means the related corporations" +

                    " as defined under the Companies Act, 1965 and include their respective employees" +

                    " and directors." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.25" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Service(s)&quot;</strong> means the mobile telecommunication services" +

                    " to be provided by MMSSB to a Customer pursuant to the Agreement and any plans, value" +

                    " added, supplemental or additional Services as may be stated in the Agreement, Registration" +

                    " Form or Addendum(s)." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.26" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;SIM Card&quot;</strong> for GSM Services means either a card or plug-in" +

                    " module with a microchip which contains all necessary Customer information. The SIM" +

                    " Card has to be inserted into the Mobile Equipment in order for a call to be made." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.27" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;SKMM&quot; </strong>means the Suruhanjaya Komunikasi dan Multimedia" +

                    " Malaysia (SKMM), also known as Malaysian Communications and Multimedia Commission," +

                    " established under the Malaysian Communications and Multimedia Commission Act 1998" +

                    " (CMA 1998)." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 1.28" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " <strong>&quot;Working Days&quot;</strong> means, save for the states of Kedah, Terengganu" +

                    " and Kelantan, Mondays to Fridays and Saturday (half day) excluding public holidays" +

                    " and Sundays. In relation to the states of Kedah, Terengganu and Kelantan, Saturdays" +

                    " to Wednesdays and Thursday (half day) excluding public holidays and Fridays." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>2.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>DURATION OF AGREEMENT</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 2.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Agreement between the Customer and MMSSB shall be in force from the date the" +

                    " Customer’s Registration Form for Services is approved by MMSSB, which is signified" +

                    " by availability of the Services to the Customer. Approval of the Customer’s application" +

                    " for Services shall be at MMSSB’s absolute discretion and shall be in force unless" +

                    " terminated in accordance with the Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 2.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Approval by MMSSB of the Customer’s application for one Service does not necessarily" +

                    " imply approval of nor oblige MMSSB to accept the Customer’s application for registration" +

                    " for other types of Services comprising the Services applied for. Approval by MMSSB" +

                    " of Customer’s application for registration for the other services comprising the" +

                    " Services is at MMSSB’s absolute discretion." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>2A.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>VALUE PLUS INTERNET PLAN</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 2A.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The principal and supplementary plans structure for the Value Plus Internet Plans" +

                    " (“Plans”) shall be as set out and published on MMSSB’s official website at <a href='http://www.maxis.com.my/Internet'>" +

                    " www.maxis.com.my/Internet </a>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 2A.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The principal and supplementary plans structure for the Plans are subject to change," +

                    " and the prevailing terms and conditions with respect to the Plans as updated on" +

                    " the official website shall apply and supercede any and all previous versions, including" +

                    " but not limited to, in any user guide or leaflets versions. The Customer is responsible" +

                    " to regularly review information relating to the Plans and Services posted on MMSSB’s" +

                    " official website which may include changes to the Agreement, Addendum(s), Services" +

                    " and/or Plans. The Customer’s continued use of the Services and/or Plans after the" +

                    " effective date of any change to the terms and conditions of Services and/or Plans" +

                    " shall constitute unconditional acceptance of such variations, additions or amendments" +

                    " by the Customer and the Customer shall be bound by the same. In the event the Customer" +

                    " does not accept such changes, the Customer shall be entitled to terminate Plan and/or" +

                    " the use of the Services." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 2A.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " With regard to the Plans, the Customer acknowledges and accepts that:<br />" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " Any usage exceeding the packaged voice minutes, SMS or data allocations will be" +

                    " charged based on the individual tariffs for exceeded usage;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " The packaged minutes and SMS applies to domestic on-net and off-net usage only;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " All packaged minutes, SMS and data allocated within the Plans are exclusively for" +

                    " domestic usage only;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " The Customer may purchase monthly internet quota upgrade passes before the Customer’s" +

                    " internet volume allocation is exceeded, whereby such upgrade is available through" +

                    " the Customer’s UMB menu accessible through the Customer’s Mobile Phone or other" +

                    " available devices or channels as may be informed by MMSSB as and when available" +

                    " or deemed necessary. Nevertheless, in the event the internet volume allocation tied" +

                    " to the Plans is exceeded and the Customer has not made an internet quota pass purchase," +

                    " the Customer will automatically be subject to extra charges (extra charges based" +

                    " on the Customer’s internet usage volume) as stipulated on MMSSB’s website with a" +

                    " maximum charge of up to RM250 per month or such other rate as stipulated by MMSSB" +

                    " as and when deemed necessary. While roaming overseas, the prevailing data roaming" +

                    " charges will apply." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " The supplementary plan does not allow sharing of unused packaged minutes, SMS’ or" +

                    " data allocation between family lines or the principal and supplementary lines." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " f." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " All monthly allocated Minutes, SMS’ and Internet quota will be pro-rated and therefore" +

                    " based on the Customer’s bill cycle date, whereby a customer might not enjoy the" +

                    " full allocation for the first month. From the second month onward the Customer will" +

                    " enjoy full allocation for Minutes, SMS’ and Internet quota.</li>" +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>3.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>CUSTOMER'S RESPONSIBILITY</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer must, throughout the duration of the Agreement:" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Promptly pay all amounts due to MMSSB as reflected in the official bill statement" +

                    " and for all charges whatsoever occasioned by the use of the Service, including all" +

                    " charges to supplementary lines, irrespective of whether such charges were authorised" +

                    " by the Customer, had exceeded the Customer’s credit limit or had arisen from any" +

                    " other causes whatsoever." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " CCease to utilise the Services or any part thereof for such period as may be required" +

                    " by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Continue to be liable for any applicable charges and fees during the period of interruption," +

                    " suspension or loss of Services or part thereof from any cause whatsoever." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Ensure that the Customer’s Mobile Equipment, SIM Card and Mobile Number are lawfully" +

                    " owned/used/possessed and that such ownership/usage/possession is not in contravention" +

                    " of any laws or regulations of Malaysia." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Ensure that the Service is used solely for the reception and transmission of messages" +

                    " (including without limitation picture, data and audio files) and other telecommunications" +

                    " by the Customer or other persons using the Customer’s Mobile Phone." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Not use the Service to cause embarrassment, distress, annoyance, irritation or nuisance" +

                    " to any person." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Comply with all notices or directions relating to the Customer’s use of the Service," +

                    " the Mobile Equipment, SIM Card and Mobile Number as MMSSB may see fit to issue from" +

                    " time to time or if MMSSB has reason or cause to suspect that the Customer is not" +

                    " complying with the Customer’s responsibilities and obligations under this Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Be fully responsible for any voice or data transmitted or broadcasted by the Customer" +

                    " or persons using the Customer’s Mobile Phone (whether authorised by the Customer" +

                    " or not)." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " At all times keep the personal identification number (PIN) of the Customer’s SIM" +

                    " Card confidential and not release the PIN to any other person." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Report immediately to MMSSB upon the discovery of any fraud, theft, loss, unauthorised" +

                    " usage or any other occurrence of unlawful acts in relation to the Mobile Phone and" +

                    " its use. In this respect, the Customer agrees to lodge a police report whenever" +

                    " instructed by MMSSB (if one has not already been lodged) and to give MMSSB a certified" +

                    " copy of such report." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 3.11" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Indemnify and shall keep indemnified MMSSB from any loss, damage, liability or expenses" +

                    " arising from any claims for libel, invasion of privacy, infringement of copyright," +

                    " patent, breach of confidence or privilege or breach of any law or regulation whatsoever" +

                    " arising from the material transmitted, received or stored via the Services or part" +

                    " thereof and from all other claims arising out of any act or omission of the Customer" +

                    " or any unauthorised use or exploitation of the Services or part thereof." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>4.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>OTHER PAYMENTS</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Upon submission and approval of the Registration Form, the Customer shall pay to" +

                    " MMSSB such payments (which may include without limitation, a refundable deposit)" +

                    " as may be required by MMSSB for the registration of the Services or part thereof." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Any refundable deposit shall be held to the Customer's credit and repaid to the" +

                    " Customer without interest after termination of the Agreement, and subject to the" +

                    " deduction of any amount due to MMSSB by the Customer. MMSSB reserves the right to" +

                    " deduct from the refundable deposit any amount due and payable to MMSSB at any time" +

                    " and may request the Customer to make a further refundable deposit payment towards" +

                    " maintaining the refundable deposit at the level determined by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer has 14 days from the date of the official bill statement to notify" +

                    " MMSSB in writing of any valid disputes or errors arising from the official bill" +

                    " statement, failing which the Customer will be deemed to have accepted the official" +

                    " bill statement rendered for any period as correct and final." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby acknowledges and agrees that its obligation to pay promptly" +

                    " all charges due and payable as stated in the official bill statement to MMSSB shall" +

                    " not be waived, absolved or diminished by virtue of:-<br />" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Customer not receiving any official bill statement from MMSSB for any particular" +

                    " billing period; and/or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Customer's failure or neglect to check, enquire, understand and ascertain the" +

                    " nature of the Services subscribed or used by the Customer and the applicable charges" +

                    " associated with such Services." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In addition to its obligations under Clause 3.1, it shall be the Customer's responsibility" +

                    " to:-<br />" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " request from MMSSB for the official bill statement which it has not received for" +

                    " any given billing period;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to check the applicable handbooks or brochures and/or addendums or supplemental" +

                    " thereto made available by MMSSB from time to time and make the necessary enquiries" +

                    " with MMSSB to understand and ascertain the nature of the Services subscribed or" +

                    " used by the Customer and the applicable charges associated with the Services." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In the event of any fees or charges remaining unpaid after becoming due, MMSSB may" +

                    " charge interest at the rate of 1.5% per month (such rate to be applicable before" +

                    " and after judgment) on the overdue amount, calculated on a daily basis commencing" +

                    " from the date after the due date for payment up to, and including the date of payment" +

                    " or RM10.00 per month whichever shall be the higher, and the Customer shall be liable" +

                    " to pay such amounts. MMSSB may also, at its sole discretion, waive any late payment" +

                    " charges or interest on any overdue amount." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB shall be entitled to vary the amount of deposit, fees and any other charges" +

                    " for the Services or<strong> </strong>part thereof at any time by notice to the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " At MMSSB's absolute discretion, any credit balance in the value of Ringgit Malaysia" +

                    " Ten (RM10.00) and below in a deactivated account, whereby such credit balance value" +

                    " may be changed from time to time, as and when deemed necessary by MMSSB, shall be" +

                    " absorbed as administration fees for the Service(s) and/or other services as may" +

                    " be provided by MMSSB and/or its Related Corporations with an outstanding amount," +

                    " as deemed necessary by MMSSB at any time." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer agrees to bear all legal costs and expenses incurred by MMSSB in recovering" +

                    " any moneys, charges, costs, and expenses payable by the Customer under the Agreement," +

                    " and the Customer also agrees to indemnify MMSSB against all costs, expenses and" +

                    " charges or legal fees incurred by MMSSB in enforcing the Agreement or in bringing" +

                    " any action or proceeding to recover all charges, costs and expenses payable by the" +

                    " Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 4.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer agrees that a certificate signed by an authorised personnel in MMSSB" +

                    " as to the monies for the time being due and owing to MMSSB from the Customer shall" +

                    " be conclusive evidence or proof that the amount appearing therein is due and owing" +

                    " and payable by the Customer to MMSSB in any legal proceedings. Any admission or" +

                    " acknowledgement in writing by the Customer or any person authorised by the Customer" +

                    " of the amount of indebtedness of the Customer to MMSSB and any judgement recovered" +

                    " by MMSSB against the Customer in respect of such indebtedness shall be binding and" +

                    " conclusive evidence in any courts within or outside Malaysia. For the avoidance" +

                    " of doubt, in the event of a conflict or inconsistency between the official bill" +

                    " statement and the certificate of indebtedness, the certificate of indebtedness shall" +

                    " prevail." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>5.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>MOBILE NUMBER PORTABILITY/MNP</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer acknowledges and accepts that: -<br />" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Mobile Numbers requested for Porting by the Customer must be in the range of" +

                    " Mobile Numbers as approved by SKMM from time to time;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " Mobile Number Porting is subject to existing geographic numbering requirements;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " only active Mobile Numbers are eligible for Porting. Mobile Numbers which have been" +

                    " suspended, terminated, blacklisted on the defaulters database and/or barred shall" +

                    " not be eligible for Porting; and" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " MMSSB may, upon receipt of a Port request from the Customer, notify the Customer" +

                    " by way of SMS the progress of the Customer's Port request" +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer is allowed to Port from prepaid Services to postpaid Services and vice" +

                    " versa. However, the Customer agrees and accepts that all Porting requests are subject" +

                    " to the respective MMSSB terms and conditions for new registration." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer acknowledges and accepts that all Customer's Services associated with" +

                    " the Mobile Number provided by the DNO, including but not limited to, value added" +

                    " services, rate plans, charges and fees will be terminated when the SIM card of the" +

                    " DNO is deactivated upon the Customer's successful Porting to the RNO and activation" +

                    " of MMSSB's SIM Card. MMSSB shall not be held liable or responsible to any Customer" +

                    " or any third party claiming through a Customer for any loss or damage whether direct," +

                    " indirect, special or consequential, or for loss of business, revenue or profits" +

                    " or of any nature suffered by any Customer, or any person authorised by any Customer," +

                    " or any injury caused to or suffered by a person or damage to property arising from" +

                    " or occasioned by termination of the DNO SIM Card and the Customer's services associated" +

                    " with the Mobile Number provided by the DNO.&nbsp;&nbsp;&nbsp;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer is responsible for identifying the supplementary Mobile Numbers (e.g." +

                    " voice, fax and data) that the Customer wishes to Port along with their primary Mobile" +

                    " Numbers and to provide all information necessary to satisfy MMSSB to proceed with" +

                    " the Porting request." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer expressly consents, acknowledges and agrees that the Customer's request" +

                    " to MMSSB to Port the Customer's Mobile Number represents a notice to terminate the" +

                    " Customer's subscription with the DNO. MMSSB shall not be held responsible or liable" +

                    " for any unsuccessful termination of the Customer's subscription with the DNO and" +

                    " resulting failure to Port with MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In the event of a Port to MMSSB, or Port withdrawal or Port reversal to the DNO," +

                    " MMSSB: -" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " is not responsible for any period of outage of the Customer's mobile service or" +

                    " any related ancillary services;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " is not liable to the Customer or any person claiming through the Customer for any" +

                    " damage, loss, costs or expenses or other liability in contract or tort or otherwise" +

                    " direct or indirect, for or in relation to the Port, or Port withdrawal or Port reversal," +

                    " for any reason whatsoever;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " will not refund the Porting fee." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall be responsible to fully settle all outstanding bills from the" +

                    " DNO.&nbsp; In the event of non-payment by the Customer of all outstanding bills" +

                    " from the DNO, the services with MMSSB may be disrupted." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Acceptance of these Services/this Agreement's terms and conditions will only be" +

                    " effective upon activation of the Ported number by MMSSB.&nbsp;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In addition to clause 8 of this Agreement and for purposes of the Porting activity," +

                    " the Customer expressly authorizes MMSSB to provide information regarding the Customer's" +

                    " Mobile Number to be disclosed to other telecommunication service providers to enable" +

                    " the transfer of the Customer's Mobile Number from one telecommunication service" +

                    " provider to another." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 5.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer acknowledges and accepts that any fees paid for Porting are non refundable." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>6.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>SUSPENSION AND TERMINATION</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 6.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer may at any time terminate the Agreement by giving MMSSB prior notice" +

                    " in writing. The Service shall be deemed terminated within four (4) Working Days" +

                    " from receipt of the termination notice by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 6.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB shall be entitled at its absolute discretion to immediately suspend/terminate" +

                    " the Services or Agreement, without liability, at any time, without any notice and" +

                    " may not be required to give any reason whatsoever, including but not limited to" +

                    " the following reasons:" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if any technical failure occurs in the Services or the Related Corporations' network;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " while the Services are being upgraded, modified or maintained;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if the Customer breaches any of the terms and conditions of the Agreement and/or" +

                    " any Addendum;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if the Customer does anything which may in MMSSB's opinion, lead to, including but" +

                    " not limited to, the damage or injury to the Services or MMSSB's and/or the Related" +

                    " Corporations network, reputation;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if MMSSB is required to do so by law, statute, enactment, code or by any relevant" +

                    " authorities;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " f." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if it is in MMSSB's opinion that the Services or the Related Corporations network" +

                    " is or may be used fraudulently, illegally or for unlawful purposes." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " MMSSB will endeavour to resume the Services as soon as possible if suspension or" +

                    " disconnection occurs for the reasons set out in Clause 6.2(a) and (b) above. The" +

                    " Customer shall be liable for all applicable charges during the period of interruption," +

                    " suspension or loss of the Services or part thereof from any cause whatsoever." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 6.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Upon termination of the Agreement by either party, the Customer shall be liable" +

                    " to MMSSB for:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the monthly subscription, access or other monthly fee for Services for the entire" +

                    " month in which the termination was effective," +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " any call or other usage charges incurred by the Customer up to and including the" +

                    " effective date of termination; and" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " any other outstanding amounts." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 6.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Upon the termination of the Agreement, all monies owing to MMSSB by the Customer" +

                    " in accordance with the Customer's account shall become immediately due and payable" +

                    " and the Customer shall upon the demand of MMSSB settle his account promptly." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 6.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Upon suspension, MMSSB may at its absolute discretion reconnect the Services, subject" +

                    " to the Customer paying a reconnection fee, all outstanding amounts due to MMSSB" +

                    " and a refundable deposit as may be required by MMSSB for the reconnection of the" +

                    " Services." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>7.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>MMSSB'S RIGHT</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 7.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB and/or its Related Corporations reserves the right to make any alteration" +

                    " or changes to the Services, or any part thereof, or suspend the Services or any" +

                    " part thereof without prior notice and MMSSB and/or its Related Corporations shall" +

                    " not be liable for any loss or inconvenience to the Customer resulting therefrom." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 7.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB reserves the right at its absolute discretion, from time to time, to vary," +

                    " add to or otherwise amend the terms and conditions of the Agreement or any part" +

                    " thereof. The Customer will be given written notice of such amendments. The Customer's" +

                    " continued use of the Services after the effective date of any variation, addition" +

                    " or amendments to the terms and conditions of the Agreement shall constitute unconditional" +

                    " acceptance of such variations, additions or amendments by the Customer. If the Customer" +

                    " does not accept such variation, addition or amendments, the Customer shall be entitled" +

                    " to terminate the Agreement by giving prior written notice to MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 7.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB reserves the right at any time to require the Customer to pay any outstanding" +

                    " amount within seven (7) days from such notice to the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 7.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Use of the Services is subject to the Customer's credit limit. Upon the Customer's" +

                    " request and/or when MMSSB deems fit in its absolute discretion, the credit limit" +

                    " may be restricted, limited, reduced or increased subject to further terms and conditions" +

                    " as MMSSB deems fit to impose. MMSSB may in its absolute discretion bar or suspend" +

                    " the Services or part thereof if the credit limit is exceeded but MMSSB is not obliged" +

                    " to ensure and makes no representation whatsoever that automatic suspension or barring" +

                    " of the Services or part thereof will occur upon the call and other usage charges" +

                    " reaching the Customer's credit limit. It is the Customer's responsibility to ensure" +

                    " that its calls and other usage charges do not exceed the credit limit. For the avoidance" +

                    " of doubt, the Customer shall be obliged to promptly pay all call and other usage" +

                    " charges notwithstanding that the credit limit has been exceeded." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>8.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>PERSONAL INFORMATION</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 8.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby acknowledges that he is aware his Personal Information will" +

                    " be used and/or disclosed for the purposes set out in Clauses 8.2 and 8.3 below." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 8.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby expressly consents that MMSSB may use his Personal Information" +

                    " for any purpose which is necessary or related to MMSSB's provision of the Services" +

                    " to the Customer. In this respect, the Customer also expressly consents that MMSSB" +

                    " may disclose his Personal Information to MMSSB's agents, contractors, business partners," +

                    " associates or such other parties as are necessary to facilitate the provision of" +

                    " the Services by MMSSB to the Customer." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " <td style='width: 49%; float: right'>" +

                    " <table>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 8.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In addition and without derogation to Clause 8.2 above, the Customer further expressly" +

                    " consents that MMSSB may use and/or disclose his Personal Information as follows:&shy;-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to MMSSB's shareholders, Related Corporations and affiliated companies for purposes" +

                    " of providing any goods or services to the Customer;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to MMSSB's agents, contractors, business partners or associates for purposes of" +

                    " marketing programs or providing any goods or services to its Customers;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to MMSSB's agents or contractors for the purposes of recovering any amount due to" +

                    " MMSSB;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to payment channels including without limitation, financial institutions for purposes" +

                    " of maintaining financial records, assessing or verifying credit and facilitating" +

                    " payments of any amount due to MMSSB pursuant to the Agreement;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to regulatory, governmental bodies or other authorities in compliance with requirements" +

                    " under law or towards the detection or prevention of crime, illegal/unlawful activities" +

                    " and/or fraud;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " f." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to any party involved in or related to a legal proceeding, for purposes of the legal" +

                    " proceedings;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " g." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to other service providers or to parties nominated or appointed by MMSSB either" +

                    " solely or jointly with other service providers, for purposes of establishing and" +

                    " maintaining a common database of customers;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " h." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " for any purpose which is necessary or related to MMSSB's provision of the Services" +

                    " to you and/or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " i." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to MMSSB's professional advisors on a need to know basis." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 8.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Save in accordance with Clauses 8.2 and 8.3 above and except as permitted or required" +

                    " under any enactment, law, statute or code, MMSSB will not use or disclose the Customer's" +

                    " Personal Information." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 8.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby acknowledges his awareness that failure to provide complete" +

                    " and correct information to MMSSB as required in the Agreement including the Registration" +

                    " Form or any Addendum, may result in his application for Services being rejected," +

                    " the Services or Agreement being terminated and/or correspondence from MMSSB including" +

                    " without limitation, bill statements failing to reach the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>9.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>DISCLAIMER</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " THE SERVICES ARE PROVIDED ON AN &quot;AS IS&quot; AND &quot;AS AVAILABLE&quot; BASIS." +

                    " MMSSB AND/OR ITS RELATED CORPORATIONS SHALL NOT BE LIABLE FOR AND MAKES NO EXPRESS" +

                    " OR IMPLIED REPRESENTATION OR WARRANTIES OF ANY KIND IN RELATION TO THE SERVICES" +

                    " INCLUDING BUT NOT LIMITED TO: -" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " AVAILABILITY, ACCESSIBILITY, TIMELINESS AND UNINTERRUPTED USE OF THE SERVICES;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " SEQUENCE, ACCURACY, COMPLETENESS, TIMELINESS OR THE SECURITY OF ANY DATA OR INFORMATION" +

                    " TRANSMITTED USING THE SERVICES;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " SEQUENCE. ACCURACY, COMPLETENESS, TIMELINESS OR THE SECURITY OF ANY DATA OR INFORMATION" +

                    " PROVIDED TO CUSTOMER AS PART OF THE SERVICES E.G. VIA MAXIS INTERACTIVE SERVICES" +

                    " (MIS) OR MOBILE INTERNET SERVICES." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>10.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>MMSSB'S AND/OR ITS RELATED CORPORATIONS' LIABILITY</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 10.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB AND/OR ITS RELATED CORPORATIONS SHALL NOT BE LIABLE TO ANY CUSTOMER, OR ANY" +

                    " THIRD PARTY authorised BY OR CLAIMING THROUGH A CUSTOMER FOR ANY LOSS OR DAMAGE," +

                    " WHETHER DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL, OR FOR LOSS OF BUSINESS, REVENUE" +

                    " OR PROFITS OR OF ANY NATURE SUFFERED BY ANY CUSTOMER, OR ANY PERSON authorised BY" +

                    " ANY CUSTOMER, OR ANY INJURY CAUSED TO OR SUFFERED BY A PERSON OR DAMAGE TO PROPERTY" +

                    " ARISING FROM OR OCCASIONED BY:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " THE USE OR INABILITY TO USE BY THE CUSTOMER OR ANY PERSONS authorised BY THE CUSTOMER," +

                    " OF THE SERVICES OR ANY PART THEREOF AND THE MOBILE PHONE OR SIM CARD;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " ANY MALFUNCTION, UNAUTHORISED USE, CLONING OF OR DEFECT IN THE MOBILE PHONE/ SIM" +

                    " CARD OR THE LOSS OF THE MOBILE PHONE, SIM CARD OR SERVICES OR ANY PART THEREOF FOR" +

                    " WHATEVER REASONS." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " ANY ACT, OMISSION, ERROR, DEFAULT OR DELAY BY MMSSB AND/OR ITS RELATED CORPORATIONS," +

                    " ITS OFFICERS, EMPLOYEES AND AGENTS IN RELATION TO THE SERVICE." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 10.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " WITHOUT LIMITING THE GENERALITY OF CLAUSE 10.1, MMSSB AND/OR ITS RELATED CORPORATIONS" +

                    " SHALL NOT BE LIABLE FOR:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " ANY CLAIM FOR LIBEL, SLANDER, INFRINGEMENT OF ANY INTELLECTUAL PROPERTY RIGHTS ARISING" +

                    " FROM THE TRANSMISSION AND RECEIPT OF MATERIAL IN CONNECTION WITH THE SERVICES AND" +

                    " ANY CLAIMS ARISING OUT OF ANY ACT OR OMISSION OF THE CUSTOMER IN RELATION TO THE" +

                    " SERVICES OR ANY PART THEREOF." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " ANY LOSS OR DAMAGE CAUSED TO THE CUSTOMER AS A RESULT OF THE SUSPENSION/BARRING/TERMINATION" +

                    " OF THE AGREEMENT AND THE INTERRUPTION/LOSS OF THE SERVICES OR ANY PART THEREOF FROM" +

                    " ANY CAUSE." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " ANY LOSS, DISTORTION OR CORRUPTION OF DATA ARISING FROM THE USE OF THE SERVICE TO" +

                    " TRANSMIT DATA OR FOR DATA COMMUNICATION PURPOSES AT ANY STAGE OF THE TRANSMISSION" +

                    " INCLUDING ANY UNLAWFUL OR UNAUTHORISED ACCESS TO THE CUSTOMER'S TRANSMISSION OR" +

                    " DATA." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " INTERRUPTION OR UNAVAILABILITY OF THE SERVICE AS A RESULT OF INCLUDING BUT NOT LIMITED" +

                    " TO ADVERSE WEATHER CONDITIONS, ELECTROMAGNETIC INTERFERENCE, EQUIPMENT FAILURE OR" +

                    " CONGESTION IN MMSSB AND/OR THE RELATED CORPORATIONS' NETWORK OR TELECOMMUNICATION" +

                    " SYSTEMS." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 10.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB and/or its Related Corporations shall not be liable for, and the Customer" +

                    " agrees to indemnify MMSSB and/or its Related Corporations against all claims, losses," +

                    " liabilities proceedin<em>g</em>s, demands, costs and expenses (including legal fees)" +

                    " which may result or which MMSSB and/or its Related Corporations may sustain in connection" +

                    " with or arising from the provision of the Services to the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 10.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Without prejudice to the foregoing, in the event a court or an arbitrator holds" +

                    " or finds MMSSB and/or its Related Corporations liable to the Customer for any breach" +

                    " or default by MMSSB and/or its Related Corporations, the Customer agrees that the" +

                    " amount of damages payable by MMSSB and/or its Related Corporations to the Customer" +

                    " shall not at any time exceed the sum of RM500.00 notwithstanding any order, decree" +

                    " or judgment to the contrary." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>11.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>SIM CARD</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The SIM Card shall remain the property of MMSSB at all times and the property of" +

                    " the SIM Card does not at any time pass to the Customer. MMSSB grants the Customer" +

                    " the right to use the SIM Card for the purposes of the Service. The SIM Card must" +

                    " be returned to MMSSB on demand. Risk passes to the Customer immediately upon the" +

                    " execution of this Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Services and/or features to be provided under the SIM Card will depend on the" +

                    " type of Mobile Equipment used." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " A Customer must not change or transfer the SIM Card to any other person unless prior" +

                    " written notice has been served to and written approval has been obtained from MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " A Customer must register with and obtain a prior written approval from MMSSB if" +

                    " he intends to obtain a second or further SIM Card. He must pay all fees and charges" +

                    " required for the new subscription. The Agreement shall similarly apply with respect" +

                    " to the additional SIM Card." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " A Customer shall bear full responsibility for the usage of the SIM Card and charges" +

                    " incurred through the usage of the SIM Card including the use by any other person" +

                    " whether authorised by him or not." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " A Customer shall use all precautions to prevent the loss, theft, cloning and/or" +

                    " unauthorised use of the SIM Card. In the event of loss, theft, cloning and/or unauthorised" +

                    " use of the SIM Card, the Customer shall immediately notify MMSSB. Replacement of" +

                    " a SIM Card is subject to such payments as may be prescribed by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 11.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB shall only replace a defective SIM Card at no cost if the defect is proven" +

                    " to MMSSB's satisfaction to be caused by the manufacturer within 12 months from the" +

                    " date of issuance of the SIM Card." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>12.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>FORCE MAJEURE</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 12.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Without limiting the generality of any provision in the Agreement, MMSSB and/or" +

                    " its Related Corporations shall not be liable for any failure to perform its obligations" +

                    " herein caused by an Act of God, insurrection or civil disorder, military operations" +

                    " or act of terrorism, all emergency, acts or omission of Government, or any competent" +

                    " authority, labour trouble or industrial disputes of any kind, fire, lightning, subsidence," +

                    " explosion, floods, acts or omission of persons or bodies for whom MMSSB and/or its" +

                    " Related Corporations has no control over or any cause outside MMSSB's and/or its" +

                    " Related Corporations' reasonable control." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 12.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Notwithstanding the event of force majeure, the Customer shall remain obliged to" +

                    " pay all fees and charges which are outstanding and/or due and payable to MMSSB in" +

                    " accordance with the Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>13.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>SEVERABILITY AND EFFECT OF TERMS AND CONDITIONS</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 13.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " If any of the provision herein contained should be invalid, illegal or unenforceable" +

                    " under any applicable law, the legality and enforceability of the remaining provisions" +

                    " shall not be affected or impaired in any way and such invalid, illegal or unenforceable" +

                    " provision shall be deemed deleted." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 13.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The terms and conditions contained in the Agreement shall have effect only to the" +

                    " extent not forbidden by law. For the avoidance of doubt, it is hereby agreed and" +

                    " declared in particular, but without limitation, that nothing herein shall be construed" +

                    " as an attempt to contract out of any provisions of the Consumer Protection Act 1999," +

                    " if and where the said Act is applicable." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>14.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>GOVERNING LAW</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 14.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Agreement shall be governed by and construed in accordance with the laws of" +

                    " Malaysia, excluding its conflict of law rules. Parties agree to submit to the exclusive" +

                    " jurisdiction of the Malaysian courts." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 14.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Where any claims, proceedings, actions, suits or disputes arising or in connection" +

                    " with this Agreement is to be commenced or adjudicated in the High Court of Malaya," +

                    " the parties agree that it shall be adjudicated in the High Court in Kuala Lumpur" +

                    " or Putrajaya, Malaysia, as the case may be." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>15.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>NOTICES</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 15.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " All official bill statements, notices, requests, notice of demands, writ of summons," +

                    " all other legal process and/or other communications/documents to be given by MMSSB" +

                    " to a Customer under the Agreement will be in writing and sent to his last known" +

                    " address and/or published in national newspapers in the main languages, published" +

                    " daily and circulating generally throughout Malaysia, as the case maybe." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 15.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " All notices, requests, notice of demands, writ of summons, all other legal process" +

                    " and/or other communications/documents to be given by the Customer to MMSSB under" +

                    " the Agreement must be in writing and sent to the following address: Maxis Mobile" +

                    " Services Sdn Bhd (formerly known as Malaysian Mobile Services Sdn Bhd), Level 18," +

                    " Menara Maxis, Kuala Lumpur City Centre 50088 Kuala Lumpur. Fax 03-2330 0008." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 15.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " All official bill statements, notices, requests, notice of demands, writ of summons," +

                    " all other legal process and/or other communications/documents given by MMSSB to" +

                    " the Customer pursuant to this clause shall be deemed to have been served if:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " sent by registered post, on the second Working Day after the date of posting irrespective" +

                    " of whether it is returned undelivered;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " sent by ordinary post, on the fifth Working Day after the date of posting irrespective" +

                    " of whether it is returned undelivered;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " hand delivered, upon delivery;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " sent by facsimile, upon successful completion of transmission as evidence by a transmission" +

                    " report and provided that notice shall in addition thereon be sent by post to the" +

                    " other party; or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " published in national newspapers in the main languages, published daily and circulating" +

                    " generally throughout Malaysia in respect of any change in the Services, terms of" +

                    " the Agreement, charges and/or tariffs." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>16.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>ASSIGNMENT</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 16.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall not be permitted to assign or novate any or part of their rights" +

                    " or obligations under the Agreement to any party, without the prior written consent" +

                    " of MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 16.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB may assign or novate all or part of the Agreement to any third party by notice" +

                    " to the Customer without the Customer's prior consent and the Customer agrees to" +

                    " make all subsequent payments as instructed in such or further notice." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>17.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>INDULGENCE</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " No delay or indulgence by MMSSB in enforcing any term or condition of the Agreement" +

                    " nor the granting of time by MMSSB to a Customer shall prejudice the rights or powers" +

                    " of MMSSB nor shall any waiver by MMSSB of any breach constitute a continuing waiver" +

                    " in respect of any subsequent or continuing breach." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>18.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>CUSTOMER'S OBLIGATIONS IN RELATION TO CONTENT</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 18.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall not disseminate or provide to any third party:&shy;-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " Bursa Malaysia Securities Berhad (&quot;Bursa Malaysia&quot;) stock information" +

                    " or any part thereof (whether in its original or adapted form) received as part of" +

                    " Mobile Interactive Services (MIS); or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " any other information or content (whether in its original or adapted form) received" +

                    " as part of other Services as MMSSB shall inform the Customer from time to time." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 18.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall not use any information or content or any parts thereof (whether" +

                    " in its original or adapted form) received as part of the Services (including without" +

                    " limitation, as part of Mobile Interactive Services (MIS)), forpurposes of creation" +

                    " of any commercial products, whether tradable or otherwise including but not limited" +

                    " to, any derivative products whether for the Customer's own purposes or for the purposes" +

                    " of any third party." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>19.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>MISCELLANEOUS</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " No rule of construction or interpretation shall apply to prejudice the interest" +

                    " of the party preparing the Agreement." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In the event of a conflict or inconsistency between the Registration Form, these" +

                    " terms and conditions and the Addendums, such inconsistency shall be resolved by" +

                    " giving precedence in the following order: Addendums, these terms and conditions" +

                    " and the Registration Form." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Agreement constitutes the entire agreement between the parties concerning the" +

                    " subject matter herein and supersedes all previous agreements, understanding, proposals," +

                    " representations and warranties relating to that subject matter." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Those clauses which by their nature would survive the termination of the Agreement" +

                    " shall so survive, including without limitation Clauses 3.1, 3.2, 3.3, 3.11, 4.3," +

                    " 4.5, 4.6, 4.8, 4.9, 6.3, 6.4, 9, 10, 14, 15 and 18." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Time wherever referred to in this Agreement shall be of the essence." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Agreement shall be binding on and shall inure for the benefit of each party's" +

                    " permitted assigns, successors in title, personal representatives, executors and" +

                    " administrators." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall bear all stamp duty, service tax charges, and any other cost" +

                    " or charge imposed by law in connection with the preparation of the Agreement and/or" +

                    " the provision of the Services." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall immediately inform MMSSB in writing of any change of address" +

                    " and/or employment or business." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Words importing the singular number include the plural number and vice versa." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Words importing the masculine gender include feminine." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.11" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " An expression importing a natural person includes any company, partnership, joint" +

                    " venture, association, corporation or other body and any governmental agency." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.12" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " All provisions contained herein shall be equally applicable to any and all supplementary" +

                    " lines subscribed by the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 19.13" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Notwithstanding anything to the contrary, the Customer hereby agrees to be bound" +

                    " by the terms of service, policies and procedures and/or any variations, additions" +

                    " or amendments made thereto, as may be determined by MMSSB at any time in respect" +

                    " of each and every individual rate plan subscribed by the Customer as provided overleaf." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>20.</strong>" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.4em'>" +

                    " <strong>DIRECT DEBIT, PAY-BY-PHONE AND m-BILLING</strong>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.1" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " In addition to the foregoin<em>g </em>terms and conditions, the provisions set out" +

                    " in this Clause 20 shall at any time during the term of the Agreement apply to Customers" +

                    " whose application for Direct Debit and/or Pay-By-Phone and/or m-Billing has been" +

                    " accepted by MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.2" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer declares and undertakes that:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the information supplied overleaf by the Customer is true and correct;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Card nominated overleaf for Direct Debit, Pay-By-Phone and/or m-Billing is in" +

                    " the name of the Customer. Where the Card so nominated is in the name of a third" +

                    " party, the Customer declares and undertakes that the Cardholder has authorised the" +

                    " Customer to use the Card for purposes of Direct Debit, Pay-By-Phone and/or m-Billing" +

                    " hereunder;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Customer is the lawful and authorised holder of the Card or where the Card belongs" +

                    " to a third party, that the Cardholder is the lawful and authorised holder of the" +

                    " Card;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Card is valid and has not expired and shall remain valid and unexpired throughout" +

                    " the duration of the Customer's use of Direct Debit, Pay-By-Phone and/or m-Billing;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Card has not been suspended nor terminated;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " f." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " to promptly pay all amounts due to MMSSB as reflected in the official bill statement" +

                    " and for all charges whatsoever occasioned by the use of the Services irrespective" +

                    " of whether such charges were authorised by the Customer, had exceeded by the Customer's" +

                    " credit limit or had arisen from any other causes whatsoever;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " g." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Customer's primary obligation under the Agreement to settle his/her MMSSB Service" +

                    " bills and to settle the same in a timely manner shall continue and shall not be" +

                    " waived, extended nor suspended in any manner whatsoever by the mere approval or" +

                    " agreement of MMSSB to provide the Services to the Customer; and" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " h." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " save and except where the fault or delay is clearly attributable to circumstances" +

                    " within MMSSB's reasonable control, all overdue payments shall be subject to interest" +

                    " for late payment and/or such other consequences as provided under the&nbsp; Agreement." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.3" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer shall allow an interval of at least fourteen (14) days from receipt" +

                    " by MMSSB of the completed registration form for the processing of the application" +

                    " and activation of Direct Debit, Pay-By-Phone and/or m-Billing." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.4" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB reserves the right at its absolute discretion to approve or reject the Customer's" +

                    " application for Direct Debit, Pay-By-Phone and/or m-Billing without assigning any" +

                    " reason whatsoever. The Customer will be notified in the event that his/her application" +

                    " has been rejected." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.5" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby expressly authorises MMSSB to:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " verify that information supplied overleaf with the Card Issuer or any third party" +

                    " as may be necessary;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " forward the Customer's call transactions, billings and other details to the Bank," +

                    " the Card Issuer and other relevant parties for and in connection with the Direct" +

                    " Debit, Pay-By-Phone and/or m-Billing;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " share its database on the Customer with MMSSB's Related Corporations, corporate" +

                    " shareholders, third parties and/or relevant authorities for the provision of integrated" +

                    " or related services, marketing programmes, and/or towards the detection and prevention" +

                    " of crime." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.6" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " MMSSB shall not be liable to the Customer:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if the Card is not honoured by the Bank or the Card Issuer;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if provision of or authorisation to the Cardholder for Direct Debit, Pay-By-Phone" +

                    " and/or m-Billing is denied/refused or suspended at any time by any party for any" +

                    " reason whatsoever; or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " if MMSSB is unable to or delays in providing Direct Debit, Pay-By-Phone and/or m-Billing" +

                    " as a result of a power failure, failure of any computer or telecommunication system" +

                    " used in connection with Direct Debit, Pay-By-Phone and/or m-Billing, or any other" +

                    " circumstances beyond MMSSB's reasonable control." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.7" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " The Customer hereby acknowledges that he/she understands that Direct Debit, Pay-By-Phone" +

                    " and/or m-Billing is only applicable for settlement of periodic MMSSB bills. All" +

                    " and any other payments outside of the periodic bill cycle shall be promptly settled" +

                    " in the ordinary manner by the Customer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.8" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Notwithstanding that a transaction may have been duly completed via Direct Debit," +

                    " Pay-By-Phone and/or m-Billing, the Customer's particular MMSSB bill has been credited" +

                    " as paid, MMSSB reserves the right and shall be entitled without prior notice to" +

                    " the Customer to reverse any payment entry in the Customer's statement of account" +

                    " or charge back the transaction sum to the Customer's account with MMSSB in the event" +

                    " of any one or more of the following circumstances:-" +

                    " <table cellspacing='0px'>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " a." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction is cancelled by the Bank or the Card Issuer for any reason whatsoever;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " b." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction is found to be incomplete, illegal or carried out by fraudulent" +

                    " means;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " c." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction is found to be one with a &quot;Declined Authorisation&quot; or" +

                    " a non-corresponding authorisation code;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " d." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction sum or part thereof was found to have exceeded the Cardholder's" +

                    " authorised credit limit;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " e." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the Card concerned is found to have expired, terminated or is invalid for any reason" +

                    " whatsoever;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " f." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction was entered into without the authorisation of the Cardholder or" +

                    " the Cardholder disputes the transaction or denies liability for whatever reason;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " g." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the transaction was carried out or credit was given to the Customer in circumstances" +

                    " constituting a breach of any express or implied term, condition, representation" +

                    " or duty of the Customer;" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " h." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " the performance of the Direct Debit, Pay-By-Phone and/or m-Billing transaction or" +

                    " the use of the Card involves a violation of the law, rules or regulations of any" +

                    " governmental body, notwithstanding that MMSSB may have received notice of the same" +

                    " at the time when the transaction was carried out; or" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " i." +

                    " </td>" +

                    " <td valign='top' style='font-size: 1em'>" +

                    " at the absolute discretion of MMSSB, the Bank or the Card Issuer, without assigning" +

                    " any reason whatsoever." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.9" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Notwithstanding Clause 20.1, the Services shall be automatically terminated with" +

                    " immediate effect and without notice to the Customer in the event that the Card is" +

                    " cancelled by the Bank or the Card Issuer." +

                    " </td>" +

                    " </tr>" +

                    " <tr>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " 20.10" +

                    " </td>" +

                    " <td valign='top' style='font-size: 0.3em'>" +

                    " Notwithstanding anything to the contrary, m-Billing is only applicable or available" +

                    " to Customers subscribing to the Services unless otherwise notified in writing by" +

                    " MMSSB." +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                    " </td>" +

                    " </tr>" +

                    " </table>" +

                //Section-G code
                    "</tr></td></tr></table>");
            mailBody.AppendFormat("Yours Sincerely,");
            mailBody.AppendFormat("<br/>Maxis Team");

            LinkedResource logo = new LinkedResource(Server.MapPath(@"~/Content/images/maxislogo.png"), "image/png");
            logo.ContentId = "corpLogo";
            logo.TransferEncoding = TransferEncoding.Base64;
            //Added by VLT to fix image issue in signature of mail on 5 Mar 2012//
            string body = "<html><body>" + mailBody.ToString() + "<br/><img alt='' src=cid:corpLogo height='30px' /><br/></body></html>";

            AlternateView av1 = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
            av1.LinkedResources.Add(logo);
            mail.AlternateViews.Add(av1);


            mail.Body = body;
            //Added by VLT to fix image issue in signature of mail on 5 Mar 2012//
            SmtpServer.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpServerPort"].ToString());
            SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            SmtpServer.UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"].ToString());
            SmtpServer.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"].ToString());
            SmtpServer.Send(mail);
            //VLT ADDED CODE by Rajeswari for the E-mail functionality
            #endregion VLT ADDED CODE
        }

        private Online.Registration.DAL.Models.Registration ConstructRegistration(string queueNo, string remarks, string signatureSVG = "", string custPhoto = "", string altCustPhoto = "", string photo = "")
        {
            var regTypeID = 0;
            Online.Registration.DAL.Models.Registration registration = new Online.Registration.DAL.Models.Registration();
            var isBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] : true;
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
            {
                regTypeID = Util.GetRegTypeID(REGTYPE.MNPPlanOnly);
            }
            else
            {
                if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
                    regTypeID = Util.GetRegTypeID(REGTYPE.DevicePlan);
                else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                    regTypeID = Util.GetRegTypeID(REGTYPE.PlanOnly);
                else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                    regTypeID = Util.GetRegTypeID(REGTYPE.DeviceOnly);
            }

            //Added by VLT ON APR 08 2013
            registration.K2_Status = Session[SessionKey.RegK2_Status.ToString()].ToString2();
            //End by VLT ON APR 08 2013


            registration = new Online.Registration.DAL.Models.Registration();
            registration.CenterOrgID = Util.SessionAccess.User.OrgID;
            registration.RegTypeID = regTypeID;
            //Added by Patanjali to support remarks on 30-03-2013
            registration.Remarks = remarks;
            //Added by Patanjali to support remarks on 30-03-2013 ends her
            registration.QueueNo = queueNo;
            registration.SignatureSVG = signatureSVG;
            //CustomerPhoto = Uri.EscapeDataString(custPhoto);
            registration.CustomerPhoto = custPhoto;
            registration.AltCustomerPhoto = altCustPhoto;
            registration.Photo = photo;
            registration.AdditionalCharges = personalDetailsVM.Deposite;
            registration.InternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;
            registration.ExternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;
            #region SUTAN ADDED CODE 20thFeb2013
            registration.AgeCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]) : "AF";
            registration.DDMFCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]) : "DF";
            registration.AddressCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]) : "ADF";
            registration.OutStandingCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()]) : "OF";
            registration.TotalLineCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()]) : "TLF";
            registration.PrincipleLineCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()]) : "PLF";
            #endregion SUTAN ADDED CODE 20thFeb2013
            registration.BiometricVerify = ((Session["RegMobileReg_BiometricVerify"].ToBool() != null) ? (Session["RegMobileReg_BiometricVerify"].ToBool()) : false);
            //added by Nreddy
            registration.IsVerified = (!ReferenceEquals(Session["RegMobileReg_BiometricVerify"], null) ? true : false);
            registration.ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
            registration.RFSalesDT = DateTime.Now;
            //commented by KD for smart
            //ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
            //if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && !ReferenceEquals(Session[SessionKey.RegMobileReg_MSISDN.ToString()], null))
            //{
            //    //registration.MSISDN1 = Convert.ToString(Session[SessionKey.RegMobileReg_MSISDN.ToString()]);
            //}
            //else
            //{
            //    registration.MSISDN1 =  Session[SessionKey.RegMobileReg_MobileNo.ToString()] == null ? null : ((List<string>) Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2();
            //}
            //MSISDN2 = ((List<string>) Session[SessionKey.RegMobileReg_MobileNo.ToString()])[1].ToString2();
            registration.MSISDN1 = Session[SessionKey.ExternalID.ToString()] == null ? string.Empty : Session[SessionKey.ExternalID.ToString()].ToString();
            registration.CreateDT = DateTime.Now;
            registration.LastAccessID = Util.SessionAccess.UserName;
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "0";
            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
            registration.ContractCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()]) : "CF";
            registration.UserType = Session[SessionKey.PPID.ToString()].ToString() == null ? "N" : Session[SessionKey.PPID.ToString()].ToString();
            registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()].ToString()) : 0);
            registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthDevAdv.ToString()].ToString()) : 0);
            registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalyPlanDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyPlanDeposit"].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()].ToString()) : 0);
            registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalayDevDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayDevDeposit"].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()].ToString()) : 0);
            registration.OutstandingAcs = Session[SessionKey.FailedAcctIds.ToString()] == null ? string.Empty : Session[SessionKey.FailedAcctIds.ToString()].ToString();
            registration.PrePortInCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()]) : "MPPF";
            registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()]) : "MSIF";
            registration.ArticleID = !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty;
            if ((!ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null)) && (!ReferenceEquals(Session["ContractId"], null)))
            {
                string ArticleID = Session[SessionKey.ArticleId.ToString()].ToString();
                int ContractsDuration = GetContractDuration(Session["ContractId"].ToInt());
                string ContractId = Session["ContractId"].ToString();
                registration.UOMCode = GetUomId(ArticleID, ContractsDuration, ContractId);
            }
            else
            {
                registration.UOMCode = string.Empty;
            }

            registration.Discount = Session["DPrice"] == null ? 0 : Session["DPrice"].ToDecimal();
            if (Session["ContractType"] == null)
                registration.SM_ContractType = string.Empty;
            else
            {
                if (Session["ContractType"].ToString().ToUpper() == "EXTEND")
                {
                    registration.SM_ContractType = "E";
                }
                else
                {
                    registration.SM_ContractType = "T";
                }

            }

            registration.KenanAccountNo = Session["KenanAccountNo"].ToString();
            registration.fxAcctNo = Session[SessionKey.AccountNumber.ToString()].ToString();
            registration.fxSubscrNo = Session["SubscriberNo"].ToString();
            registration.fxSubScrNoResets = Session["SubscriberNoResets"].ToString();
            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : "No";
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "No";
            registration.externalId = Session[SessionKey.ExternalID.ToString()].ToString();
            registration.PenaltyAmount = Session["Penalty"] == null ? 0 : Session["Penalty"].ToString() == "" ? 0 : Convert.ToDecimal(Convert.ToDouble(Session["Penalty"]));
            registration.Trn_Type = "S";
            registration.Discount_ID = Session["discountID"] == null ? string.Empty : Session["discountID"].ToString();
            registration.Smart_ID = ConfigurationManager.AppSettings["SmartDiscountID"];
            //added by Nreddy
            registration.OrganisationId = Util.SessionAccess.User.Org.OrganisationId;
            return registration;
        }

        private Customer ConstructCustomer()
        {
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            personalDetailsVM.Customer.PayModeID = 1;
            personalDetailsVM.Customer.LanguageID = 1;
            personalDetailsVM.Customer.EmailAddr = string.Empty;
            personalDetailsVM.Customer.RaceID = 4;
            //if (personalDetailsVM.DOBYear == 0)
            //{
            //    personalDetailsVM.DOBYear = DateTime.Now.Year;
            //    personalDetailsVM.DOBMonth = DateTime.Now.Month;
            //    personalDetailsVM.DOBDay = DateTime.Now.Day;
            //}
            if (personalDetailsVM.Customer.CustomerTitleID <= 0)
            {
                personalDetailsVM.Customer.CustomerTitleID = 1;
            }
            if (string.IsNullOrEmpty(personalDetailsVM.Customer.ContactNo))
            {
                personalDetailsVM.Customer.ContactNo = "60123456789";
            }
            else if (personalDetailsVM.Customer.ContactNo.Length == 10)
                personalDetailsVM.Customer.ContactNo = "6" + personalDetailsVM.Customer.ContactNo;
            var cust = new Customer()
            {
                FullName = personalDetailsVM.Customer.FullName,
                Gender = personalDetailsVM.Customer.Gender,
                //DateOfBirth = personalDetailsVM.Customer.DateOfBirth,
                DateOfBirth = Convert.ToDateTime(personalDetailsVM.DOBYear + "-" + personalDetailsVM.DOBMonth + "-" + personalDetailsVM.DOBDay),
                CustomerTitleID = personalDetailsVM.Customer.CustomerTitleID,
                RaceID = personalDetailsVM.Customer.RaceID,
                LanguageID = personalDetailsVM.Customer.LanguageID,
                NationalityID = personalDetailsVM.Customer.NationalityID,
                IDCardTypeID = personalDetailsVM.Customer.IDCardTypeID,
                IDCardNo = personalDetailsVM.Customer.IDCardNo,
                ContactNo = personalDetailsVM.Customer.ContactNo,
                AlternateContactNo = personalDetailsVM.Customer.AlternateContactNo,
                EmailAddr = personalDetailsVM.Customer.EmailAddr,
                PayModeID = personalDetailsVM.Customer.PayModeID,
                CardTypeID = personalDetailsVM.Customer.CardTypeID,
                NameOnCard = personalDetailsVM.Customer.NameOnCard,
                CardNo = personalDetailsVM.Customer.CardNo,
                CardExpiryDate = personalDetailsVM.Customer.CardExpiryDate,
                IsVIP = Session["isVIP_Masking"].ToBool() ? 1 : 0,
                Active = true,
                LastAccessID = Util.SessionAccess.UserName,
                CreateDT = DateTime.Now
            };

            return cust;
        }

        private RegMdlGrpModel ConstructRegMdlGroupModel()
        {
            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                return new RegMdlGrpModel();

            int? mdlGrpModelID = null;

            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
            {
                using (var proxy = new SmartCatelogServiceProxy())
                {
                    //var bundleID = proxy.PgmBdlPckComponentGet(new int[] {  Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt() }).SingleOrDefault().ParentID;
                    //var bpPgmBdlPkgCompIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    //{
                    //    PgmBdlPckComponent = new PgmBdlPckComponent()
                    //    {
                    //        ParentID = bundleID,
                    //        LinkType = Properties.Settings.Default.Bundle_Package
                    //    },
                    //    Active=true
                    //}).ToList();

                    var modelGroupIDs = proxy.ModelGroupFind(new ModelGroupFind()
                    {
                        ModelGroup = new ModelGroup()
                        {
                            PgmBdlPckComponentID = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt()
                        },
                        //PgmBdlPkgCompIDs = bpPgmBdlPkgCompIDs,
                        Active = true
                    });

                    var modelGroupModels = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                    {
                        ModelGroupModel = new ModelGroupModel()
                        {
                            ModelID = Session["RegMobileReg_SelectedModelID"].ToInt()
                        }
                    })).ToList();

                    mdlGrpModelID = modelGroupModels.Where(a => modelGroupIDs.Contains(a.ModelGroupID)).Select(a => a.ID).FirstOrDefault();
                }
            }

            var regMdlGrpModel = new RegMdlGrpModel()
            {
                ModelImageID = Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt(),
                ModelGroupModelID = mdlGrpModelID != null ? mdlGrpModelID : 0,
                Price = Session["RegMobileReg_RRPPrice"] != null ? Session["RegMobileReg_RRPPrice"].ToDecimal() : 0,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            };

            return regMdlGrpModel;

        }

        private List<Address> ConstructRegAddress()
        {
            var addresses = new List<Address>();
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            //Added by KD for Language,email,Race
            personalDetailsVM.Customer.LanguageID = 1;
            personalDetailsVM.Customer.EmailAddr = string.Empty;
            personalDetailsVM.Customer.RaceID = 4;
            if (personalDetailsVM.Address.StateID <= 1)
            {
                personalDetailsVM.Address.StateID = 2;
            }
            addresses.Add(new Address()
            {
                AddressTypeID = 1,
                Line1 = personalDetailsVM.Address.Line1,
                Line2 = personalDetailsVM.Address.Line2,
                Line3 = personalDetailsVM.Address.Line3,
                Postcode = personalDetailsVM.Address.Postcode,
                StateID = personalDetailsVM.Address.StateID,
                Town = personalDetailsVM.Address.Town,
                Street = "",
                BuildingNo = "",
                CountryID = Util.GetIDByCode(RefType.Country, Properties.Settings.Default.Country_MY),
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });

            return addresses;
        }

        private List<RegPgmBdlPkgComp> ConstructRegPgmBdlPkgComponent()
        {
            var regPBPCs = new List<RegPgmBdlPkgComp>();

            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                return regPBPCs;

            // Plan PgmBdlPkgComponent
            regPBPCs.Add(new RegPgmBdlPkgComp()
            {
                RegSuppLineID = null,
                PgmBdlPckComponentID = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(),
                IsNewAccount = true,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });

            // Mandatory VAS PgmBdlPkgComponent
            var vasPBPCIDs = new List<int>();
            using (var proxy = new SmartCatelogServiceProxy())
            {
                var pkgID = proxy.PgmBdlPckComponentGet(new int[] { Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt() }).SingleOrDefault().ChildID;

                vasPBPCIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    IsMandatory = true,
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        ParentID = pkgID,
                        LinkType = Properties.Settings.Default.Package_Component
                    },
                    Active = true
                }).ToList();

                foreach (var vasPBPCID in vasPBPCIDs)
                {
                    regPBPCs.Add(new RegPgmBdlPkgComp()
                    {
                        RegSuppLineID = null,
                        PgmBdlPckComponentID = vasPBPCID,
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }

            // Optional VAS PgmBdlPkgComponent
            var vasIDs = Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2().Split(',');
            // vasIDs[vasIDs.Length] = Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToString();
            // Session[SessionKey.RegMobileReg_DataplanID.ToString()]

            foreach (var vasID in vasIDs)
            {
                if (!string.IsNullOrEmpty(vasID.ToString()))
                {
                    regPBPCs.Add(new RegPgmBdlPkgComp()
                    {
                        PgmBdlPckComponentID = vasID.ToInt(),
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }
            if (Session[SessionKey.RegMobileReg_DataplanID.ToString()] != null && Session[SessionKey.RegMobileReg_DataplanID.ToString()] != "" && Convert.ToInt32(Session[SessionKey.RegMobileReg_DataplanID.ToString()]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }

            /*Mandatory package*/
            if (Session["intmandatoryidsval"] != null && Session["intmandatoryidsval"] != "" && Convert.ToInt32(Session["intmandatoryidsval"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session["intmandatoryidsval"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            /*data plan*/
            if (Session["intdataidsval"] != null && Session["intdataidsval"] != "" && Convert.ToInt32(Session["intdataidsval"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session["intdataidsval"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            /*extra plan*/
            if (Session["intextraidsval"] != null && Session["intextraidsval"] != "" && Convert.ToInt32(Session["intextraidsval"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session["intextraidsval"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            return regPBPCs;
        }

        private void ClearRegistrationSession()
        {
            Session["intmandatoryidsval"] = null;
            Session["intdataidsval"] = null;
            Session["intextraidsval"] = null;
            Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
            Session["RegMobileReg_BiometricID"] = null;
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
            Session["RegMobileReg_SelectedModelID"] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
            Session[SessionKey.RegMobileReg_MobileNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session["RegMobileReg_PhoneVM"] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session[SessionKey.RegStepNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardType.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = null;
            Session[SessionKey.RegMobileReg_SublineVM.ToString()] = null;
            Session[SessionKey.ArticleId.ToString()] = null;
            Session["RegMobileReg_SelectedContracts"] = null;
            Session["_componentViewModel"] = null;
            ClearSubLineSession();
        }

        private RegStatus ConstructRegStatus()
        {
            var regStatus = new RegStatus()
            {
                Active = true,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName,
                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegNew)
            };

            return regStatus;
        }

        private List<RegSuppLine> ConstructRegSuppLines()
        {
            var regSuppLines = new List<RegSuppLine>();

            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];

            if (sublines != null)
            {
                foreach (var subline in sublines)
                {
                    // SuppLine Plan
                    regSuppLines.Add(new RegSuppLine()
                    {
                        SequenceNo = subline.SequenceNo,
                        PgmBdlPckComponentID = subline.PkgPgmBdlPkgCompID,
                        IsNewAccount = true,
                        MSISDN1 = subline.SelectedMobileNos[0].ToString2(),
                        //MSISDN2 = subline.SelectedMobileNos[1].ToString2(),
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }

            return regSuppLines;
        }

        private List<RegPgmBdlPkgComp> ConstructRegSuppLineVASes()
        {
            var regSuppLinePBPCs = new List<RegPgmBdlPkgComp>();
            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];

            if (sublines != null)
            {
                foreach (var subline in sublines)
                {
                    // Mandatory SuppLine VAS
                    var suppLineVasIDs = new List<int>();
                    using (var proxy = new SmartCatelogServiceProxy())
                    {
                        var pkgID = proxy.PgmBdlPckComponentGet(new int[] { subline.PkgPgmBdlPkgCompID }).SingleOrDefault().ChildID;

                        suppLineVasIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                        {
                            IsMandatory = true,
                            PgmBdlPckComponent = new PgmBdlPckComponent()
                            {
                                ParentID = pkgID,
                                LinkType = Properties.Settings.Default.Package_Component
                            }
                        }).ToList();
                    }

                    foreach (var suppLineVasID in suppLineVasIDs)
                    {
                        regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
                        {
                            SequenceNo = subline.SequenceNo,
                            PgmBdlPckComponentID = suppLineVasID,
                            IsNewAccount = true,
                            Active = true,
                            CreateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }

                    // Optional SuppLine VAS
                    foreach (var vasID in subline.SelectedVasIDs)
                    {
                        regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
                        {
                            SequenceNo = subline.SequenceNo,
                            PgmBdlPckComponentID = vasID.ToInt(),
                            IsNewAccount = true,
                            Active = true,
                            CreateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }
                }
            }
            return regSuppLinePBPCs;
        }

        [HttpPost]
        public ActionResult SubLinePlan(FormCollection collection, SublinePackageVM sublinePkgVM)
        {
            try
            {
                if (collection["submit1"].ToString2() == "Next")
                {
                    // selected different Sub Plan
                    if (Session["RegMobileSub_PkgPgmBdlPkgCompID"].ToInt() != sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID)
                    {
                        Session["RegMobileSub_PkgPgmBdlPkgCompID"] = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
                    }

                    var mobileNos = new List<string>();
                    //mobileNos.Add(sublinePkgVM.DesireNo);

                    for (int i = 0; i < Properties.Settings.Default.NoOfDesireMobileNo; i++)
                    {
                        mobileNos.Add(collection["txtMobileNo_" + i].ToString2());
                    }

                    Session["RegMobileSub_MobileNo"] = mobileNos;
                }
                else
                {
                    return RedirectToAction("SelectMobileNo");
                }
            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
            return RedirectToAction("SubLineVAS");
        }
        [HttpPost]
        public ActionResult SubLineVAS(FormCollection collection, SublineVASVM sublineVasVM)
        {
            try
            {
                if (collection["submit1"].ToString2() == "Next")
                {
                    if (!string.IsNullOrEmpty(sublineVasVM.VasVM.SelectedVasIDs))
                    {
                        Session["RegMobileSub_VasIDs"] = sublineVasVM.VasVM.SelectedVasIDs;
                    }

                    //construct List<SublineVM> session
                    ConstructSubLineVM();

                    return RedirectToAction(GetRegActionStep((int)Session[SessionKey.RegMobileReg_TabIndex.ToString()]));
                }
                else
                {
                    // back to subline plan
                    return RedirectToAction("SubLinePlan", new { isBack = true });
                }

            }
            catch (Exception ex)
            {
                //LogException(ex);
                //return RedirectException(ex);
                throw;
            }
        }

        private void SetRegStepNo(MobileRegistrationSteps regStep)
        {
            switch (regStep)
            {
                case MobileRegistrationSteps.Device:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Device;
                    break;
                case MobileRegistrationSteps.DeviceCatalog:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.DeviceCatalog;
                    break;
                case MobileRegistrationSteps.Plan:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Plan;
                    break;
                case MobileRegistrationSteps.Vas:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Vas;
                    break;
                case MobileRegistrationSteps.MobileNo:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.MobileNo;
                    break;
                case MobileRegistrationSteps.PersonalDetails:
                    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                    {
                        Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() + 1;
                    }
                    else
                    {
                        Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.PersonalDetails;
                    }
                    break;
                default:
                    break;
            }

            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan || Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
            {
                if (regStep == MobileRegistrationSteps.Device)
                    Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() + 1;
            }
            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
            {
                Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() - 1;
            }

            // set Personal Details as step 3 for Device Only
            if (regStep == MobileRegistrationSteps.PersonalDetails)
            {

            }
        }

        private string GetRegActionStep(int stepNo)
        {
            if ((int)MobileRegistrationSteps.Device == stepNo)
            {
                return "SelectDevice";
            }
            if ((int)MobileRegistrationSteps.DeviceCatalog == stepNo)
            {
                return "SelectDeviceCatalog";
            }
            else if ((int)MobileRegistrationSteps.Plan == stepNo)
            {
                return "SelectPlan";
            }
            else if ((int)MobileRegistrationSteps.Vas == stepNo)
            {
                return "SelectVAS";
            }
            else if ((int)MobileRegistrationSteps.MobileNo == stepNo)
            {
                //return "SelectMobileNo";
                return "SelectDiscount";
            }
            else if ((int)MobileRegistrationSteps.PersonalDetails == stepNo)
            {
                return "PersonalDetails";
            }
            else if ((int)MobileRegistrationSteps.Summary == stepNo)
            {
                return "MobileRegSummary";
            }

            return "";
        }

        private DeviceOptionVMSmart GetAvailableDeviceOptions()
        {
            var deviceOptVM = new DeviceOptionVMSmart();

            using (var proxy = new SmartCatelogServiceProxy())
            {
                var brandIDs = proxy.BrandFind(new BrandFindSmart()
                {
                    Brand = new BrandSmart() { },
                    Active = true
                });

                deviceOptVM.AvailableBrands = proxy.BrandGet(brandIDs).ToList();
            }

            if (!string.IsNullOrEmpty(Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToString2()))
            {
                deviceOptVM.SelectedDeviceOption = "btnDvcOpt_" + Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToString2() + "_" + Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToString2();
            }

            deviceOptVM.Type = Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToString2();
            if (Session["ContractType"] != null)
            {
                if (Session["ContractType"].ToString().ToUpper() == "EXTEND")
                    GetExtendContracts();
            }
            return deviceOptVM;
        }

        public class ClassArticleId
        {
            public int id { get; set; }
            public string articleId { set; get; }
            public int modelId { set; get; }
            public int colourID { set; get; }
            public string imagePath { set; get; }
            public int count { set; get; }
        }

        private PhoneVM GetAvailableModelImage()
        {
            var modelIDs = new List<int>();
            var models = new List<Model>();
            var phoneVM = new PhoneVM();
            var articleID = new List<BrandArticleSmart>();
            var availableModelImages = new List<AvailableModelImage>();

            if ((PhoneVM)Session["RegMobileReg_PhoneVM"] != null)
            {
                phoneVM = (PhoneVM)Session["RegMobileReg_PhoneVM"];
            }


            // select all device images
            using (var proxy = new SmartCatelogServiceProxy())
            {
                if (Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToInt() == (int)MobileRegDeviceOption.Brand)
                {
                    modelIDs = proxy.ModelFind(new ModelFind()
                    {
                        Model = new Model()
                        {
                            BrandID = Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToInt()
                        },
                        Active = true
                    }).ToList();

                    models = proxy.ModelGet(modelIDs).ToList();
                }

                #region Code for geting the count from the service by using dynamic proxy and geting the article id
                List<ClassArticleId> lstarticleId = new List<ClassArticleId>();

                int counter = 0;
                using (var catalogProxy = new CatalogServiceClient())
                {
                    for (int i = 0; i < modelIDs.Count; i++)
                    {
                        articleID = proxy.GetArticleID(modelIDs[i].ToInt()).ArticleIDs.ToList();

                        if (articleID.Count > 0)
                        {

                            for (int j = 0; j < articleID.Count; j++)
                            {
                                ClassArticleId ClassArticleId = new ClassArticleId()
                                {
                                    id = articleID[j].ID,
                                    articleId = articleID[j].ArticleID.Trim(),
                                    modelId = articleID[j].ModelID,
                                    imagePath = ReferenceEquals(articleID[j].ImagePath, null) ? string.Empty : articleID[j].ImagePath.ToString().Trim(),
                                    colourID = articleID[j].ColourID,

                                };
                                lstarticleId.Add(ClassArticleId);
                            }
                            counter++;
                        }
                    }
                    if (counter <= 0)
                    {
                        lstarticleId = null;
                    }
                }

                //string strArgs = "100000061001";
                //int subscriptionReqFromVasResult;
                string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                int storeId = 0;
                string[] maxisStoreIds = ConfigurationManager.AppSettings["OrganizationsIds"].ToString().Split(",".ToCharArray());
                if (!maxisStoreIds.Contains(Util.SessionAccess.User.Org.OrganisationId))
                {
                    storeId = Convert.ToInt32(Util.SessionAccess.User.Org.OrganisationId);
                }
                try
                {
                    using (var dsproxy = new DynamicServiceProxy())
                    {
                        lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = (int)(dsproxy.GetStockCount(i.articleId, wsdlUrl,storeId)));
                    }
                }
                catch (Exception ex)
                {
                    //RedirectToAction("StoreError", "HOME");
                    throw;
                }
                /*end here*/



                if (!ReferenceEquals(lstarticleId, null))
                {
                    foreach (var item in lstarticleId)
                    {
                        availableModelImages.Add(new AvailableModelImage()
                        {

                            BrandArticle = new BrandArticle()
                            {
                                ID = item.id,
                                ColourID = item.colourID,
                                ImagePath = item.imagePath,
                                ModelID = item.modelId,
                                ArticleID = item.articleId,
                            },
                            ModelCode = models.Where(a => a.ID == item.modelId).SingleOrDefault().Code,
                            Count = item.count

                        });
                    }
                }
                #endregion

                #region Commented code by chetan for implementing the above logic
                //var modelImages = proxy.ModelImageGet(
                //                        proxy.ModelImageFind(
                //                            new ModelImageFind()
                //                            {
                //                                ModelImage = new ModelImage(),
                //                                Active = true,
                //                                ModelIDs = modelIDs
                //                            })
                //                        ).OrderByDescending(a => a.CreateDT).ToList();



                //foreach (var item in modelImages)
                //{
                //    availableModelImages.Add(new AvailableModelImage()
                //        {
                //            ModelImage = new ModelImage()
                //            {
                //                ID = item.ID,
                //                ColourID = item.ColourID,
                //                ImagePath = item.ImagePath,
                //                ModelID = item.ModelID
                //            },
                //            ModelCode = models.Where(a => a.ID == item.ModelID).SingleOrDefault().Code
                //        });
                //}
                #endregion

            }

            phoneVM.ModelImages = availableModelImages;
            phoneVM.TabNumber = Session[SessionKey.RegMobileReg_TabIndex.ToString()].ToInt();

            if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt() != 0)
                phoneVM.SelectedModelImageID = Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToString2();

            return phoneVM;
        }

        private PackageVM GetAvailablePackages(bool fromSubline = false)
        {
            var packageVM = new PackageVM();
            var availablePackages = new List<AvailablePackage>();
            var PgmBdlPckComponentSmart = new List<PgmBdlPckComponentSmart>();
            var programBundles = new List<PgmBdlPckComponent>();
            var packages = new List<Package>();
            var bundles = new List<Bundle>();
            var bundleIDs = new List<int>();
            var programs = new List<Program>();
            var filterBdlIDs = new List<int>();
            var bundlePackages = new List<PgmBdlPckComponent>();
            var packageIDs = new List<int>();
            var orderVM = new OrderSummaryVM();
            int plan = 0;

            using (var proxy = new SmartCatelogServiceProxy())
            {
                // retrieve bundles that assigned to current user
                var filterBdlPkgIDs = proxy.PgmBdlPckComponentFindSmart(new PgmBdlPckComponentFindSmart()
                {
                    PgmBdlPckComponentSmart = new PgmBdlPckComponentSmart()
                    {
                        LinkType = Properties.Settings.Default.Program_Bundle,
                        FilterOrgType = Util.SessionOrgTypeCode
                    },
                    Active = true
                }).ToList();

                //filterBdlIDs = proxy.PgmBdlPckComponentGet(filterBdlPkgIDs).Select(a => a.ChildID).ToList();

                filterBdlIDs = proxy.PgmBdlPckComponentGetSmart(filterBdlPkgIDs).Select(a => a.ChildID).ToList();

                bundleIDs = filterBdlIDs;

                // Main Line
                if (!fromSubline)
                {
                    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
                    {   /*Commented for articleid*/
                        //var modelID = proxy.ModelImageGet(Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt()).ModelID;
                        var modelID = proxy.BrandArticleImageGet(Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt()).ModelID;

                        //var modelGroupIDs = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                        //{
                        //    ModelGroupIDs = new List<int>(),
                        //    ModelGroupModel = new ModelGroupModel()
                        //    {
                        //        ModelID = modelID
                        //    },
                        //    Active = true
                        //})).Select(a => a.ModelGroupID).Distinct().ToList();

                        var modelGroupIDs_Smart = proxy.ModelGroupModelGet_Smart(proxy.ModelGroupModelFind_Smart(new ModelGroupModelFind_Smart()
                        {
                            ModelGroupIDs = new List<int>(),
                            ModelGroupModel = new ModelGroupModel_Smart()
                            {
                                ModelID = modelID
                            },
                            Active = true
                        })).Select(a => a.ModelGroupID).Distinct().ToList();

                        // Bundle - Package
                        var pbpcIDs = proxy.ModelGroupGet(modelGroupIDs_Smart).Select(a => a.PgmBdlPckComponentID).ToList();

                        bundlePackages = proxy.PgmBdlPckComponentGet(pbpcIDs).Where(a => a.Active == true).ToList();
                        bundleIDs = filterBdlIDs.Intersect(bundlePackages.Select(a => a.ParentID)).ToList();
                        packageIDs = bundlePackages.Select(a => a.ChildID).ToList();
                    }
                }



                var query = proxy.PgmBdlPckComponentGetSmart(proxy.PgmBdlPckComponentFindSmart(new PgmBdlPckComponentFindSmart()
                {
                    PgmBdlPckComponentSmart = new PgmBdlPckComponentSmart()
                    {
                        LinkType = Properties.Settings.Default.Bundle_Package,
                        PlanType = fromSubline ? Properties.Settings.Default.SupplimentaryPlan : Properties.Settings.Default.ComplimentaryPlan,
                    },
                    ParentIDs = bundleIDs,
                    ChildIDs = packageIDs,
                    Active = true
                }));

                // Main Line
                if (!fromSubline)
                {
                    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
                    {
                        PgmBdlPckComponentSmart = query.Where(a => bundleIDs.Contains(a.ParentID)).ToList();
                    }
                    else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                    {
                        PgmBdlPckComponentSmart = query.ToList();
                    }
                }
                else //Sub Line
                {
                    PgmBdlPckComponentSmart = query.ToList();
                }

                if (PgmBdlPckComponentSmart.Count() > 0)
                {
                    packages = proxy.PackageGet(PgmBdlPckComponentSmart.Select(a => a.ChildID).Distinct()).ToList();
                    bundles = proxy.BundleGet(PgmBdlPckComponentSmart.Select(a => a.ParentID).Distinct()).ToList();
                }

                var pgmBdlIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.Program_Bundle,
                    },
                    ChildIDs = PgmBdlPckComponentSmart.Select(a => a.ParentID).ToList()
                });

                programBundles = proxy.PgmBdlPckComponentGet(pgmBdlIDs).Distinct().ToList();

                var programIDs = programBundles.Select(a => a.ParentID).ToList();

                if (programIDs != null)
                {
                    if (programIDs.Count() > 0)
                    {
                        programs = proxy.ProgramGet(programIDs).ToList();
                    }
                }
            }
            if ((!ReferenceEquals(Session["ContractType"], null)) && Session["ContractType"].ToString().ToUpper() == "EXTEND")
            {

                ContractDetails ContractDet = new ContractDetails();
                if (Session["ContractDet"] != null)
                {
                    ContractDet = (SmartViewModels.ContractDetails)Session["ContractDet"];
                }
                foreach (var pbpc in PgmBdlPckComponentSmart)
                {
                    if (ContractDet.PackageDesc == packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name && plan == 0)
                    {
                        plan = 1;
                    }
                    if (plan == 1)
                    {
                        availablePackages.Add(new AvailablePackage()
                        {
                            PgmBdlPkgCompID = pbpc.ID,
                            ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
                            BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
                            PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                            PackageCode = pbpc.Code
                        });
                    }

                }
                if (plan == 0)
                {

                    foreach (var pbpc in PgmBdlPckComponentSmart)
                    {

                        availablePackages.Add(new AvailablePackage()
                        {
                            PgmBdlPkgCompID = pbpc.ID,
                            ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
                            BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
                            PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                            PackageCode = pbpc.Code
                        });
                    }
                }
            }

            else
            {
                foreach (var pbpc in PgmBdlPckComponentSmart)
                {

                    availablePackages.Add(new AvailablePackage()
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
                        BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
                        PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        PackageCode = pbpc.Code
                    });
                }
            }
            packageVM.AvailablePackages = availablePackages.OrderBy(a => a.PgmBdlPkgCompID).ThenBy(a => a.PackageName).ToList();
            //packageVM.AvailablePackages.Remove(packages.Where(a => a.Name==
            // Main Line
            if (!fromSubline)
                packageVM.SelectedPgmBdlPkgCompID = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt();
            else
                packageVM.SelectedPgmBdlPkgCompID = Session["RegMobileSub_PkgPgmBdlPkgCompID"].ToInt();

            return packageVM;
        }

        private ValueAddedServicesVM GetAvailablePackageComponents(int pbpcID, bool fromSubline = false, bool isMandatory = false)
        {
            var vasVM = new ValueAddedServicesVM();
            var retrievedVases = new List<AvailableVAS>();
            var contractVases = new List<AvailableVAS>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponentSmart>();
            var components = new List<Component>();
            var componentTypes = new List<ComponentType>();

            using (var proxy = new SmartCatelogServiceProxy())
            {
                var packageID = (pbpcID != null && pbpcID != 0) ? proxy.PgmBdlPckComponentGet(new int[] { pbpcID }).SingleOrDefault().ChildID : 0;
                if (packageID != 0)
                {
                    pgmBdlPckComponents = proxy.PgmBdlPckComponentGetSmart(proxy.PgmBdlPckComponentFindSmart(new PgmBdlPckComponentFindSmart()
                    {
                        PgmBdlPckComponentSmart = new PgmBdlPckComponentSmart()
                        {
                            LinkType = Properties.Settings.Default.Package_Component,
                            ParentID = packageID,
                        },
                        IsMandatory = isMandatory,
                        Active = true
                    })).ToList();
                }
                if (pgmBdlPckComponents.Count() > 0)
                {
                    if (Session["ContractType"] != null)
                    {
                        if (Session["ContractType"].ToString().ToUpper() == "EXTEND")
                        {
                            pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.contract_type == "E").ToList();
                            if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] != null)
                            {
                                string[] mobileIDs = Session["ModelID"].ToString().Split(',');
                                for (int mobIDCnt = 0; mobIDCnt < mobileIDs.Length; mobIDCnt++)
                                {
                                    //pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.ModelId == mobileIDs[mobIDCnt]).ToList();
                                    pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.ModelId != null).ToList();
                                    pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.ModelId.Contains(mobileIDs[mobIDCnt])).ToList();
                                }
                            }
                        }
                        else
                        {
                            pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.contract_type != "E").ToList();
                        }
                    }
                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();

                    componentTypes = proxy.ComponentTypeGet(components.Select(a => a.ComponentTypeID).Distinct()).ToList();
                }
            }

            foreach (var pbpc in pgmBdlPckComponents)
            {

                var component = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault();

                var compTypeCode = componentTypes.Where(a => a.ID == component.ComponentTypeID).SingleOrDefault().Code;

                if (compTypeCode == Properties.Settings.Default.CompType_DataCon || compTypeCode == Properties.Settings.Default.CompType_VoiceCon)
                {
                    contractVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price
                    });
                }
                else
                {
                    retrievedVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price
                    });
                }


            }

            if (isMandatory)
            {
                vasVM.MandatoryVASes = retrievedVases;
            }
            else
            {
                vasVM.AvailableVASes = retrievedVases;
            }
            vasVM.ContractVASes = contractVases;

            vasVM.SelectedContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2();
            vasVM.SelectedVasIDs = fromSubline ? Session["RegMobileSub_VasIDs"].ToString2() : Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2();

            return vasVM;
        }

        private ValueAddedServicesVM GetAvailablePackageComponents_PC_MC_DC(int pbpcID, string listtype, bool fromSubline = false, bool isMandatory = false)
        {
            var vasVM = new ValueAddedServicesVM();
            var retrievedVases = new List<AvailableVAS>();
            var contractVases = new List<AvailableVAS>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var components = new List<Component>();
            var componentTypes = new List<ComponentType>();


            int packageID = 0;
            using (var proxy = new SmartCatelogServiceProxy())
            {

                packageID = (pbpcID != null) ? proxy.PgmBdlPckComponentGet(new int[] { pbpcID }).SingleOrDefault().ChildID : 0;

                if (packageID != 0)
                {

                    pgmBdlPckComponents.AddRange(proxy.PgmBdlPckComponentGet(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            LinkType = listtype,
                            ParentID = packageID,
                        },
                        IsMandatory = isMandatory,
                        Active = true
                    })).ToList());



                }
                if (pgmBdlPckComponents.Count() > 0)
                {
                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();

                    componentTypes = proxy.ComponentTypeGet(components.Select(a => a.ComponentTypeID).Distinct()).ToList();
                }
            }


            foreach (var pbpc in pgmBdlPckComponents)
            {
                var component = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault();
                var compTypeCode = componentTypes.Where(a => a.ID == component.ComponentTypeID).SingleOrDefault().Code;

                if (compTypeCode == Properties.Settings.Default.CompType_DataCon || compTypeCode == Properties.Settings.Default.CompType_VoiceCon)
                {
                    contractVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price
                    });
                }
                else
                {
                    retrievedVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price
                    });
                }
            }

            if (isMandatory)
            {
                vasVM.MandatoryVASes = retrievedVases;
            }
            else
            {
                vasVM.AvailableVASes = retrievedVases;
            }
            vasVM.ContractVASes = contractVases;

            vasVM.SelectedContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2();
            vasVM.SelectedVasIDs = fromSubline ? Session["RegMobileSub_VasIDs"].ToString2() : Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2();

            return vasVM;
        }

        private void ConstructSuppLineSummary(List<RegSuppLine> suppLines, List<RegPgmBdlPkgComp> regPBPCs)
        {
            var strVAS = "";
            var MSISDNs = new List<string>();
            var sublineVM = new List<SublineVM>();
            var suppLinePBPCs = new List<PgmBdlPckComponent>();

            using (var proxy = new SmartCatelogServiceProxy())
            {
                suppLinePBPCs = proxy.PgmBdlPckComponentGet(regPBPCs.Select(a => a.PgmBdlPckComponentID).ToList()).ToList();
            }

            foreach (var suppLine in suppLines)
            {
                ClearSubLineSession();

                var pbpcIDs = regPBPCs.Where(a => a.RegSuppLineID == suppLine.ID).Select(a => a.PgmBdlPckComponentID).ToList();
                var PBPCs = suppLinePBPCs.Where(a => pbpcIDs.Contains(a.ID)).ToList();

                var bpCode = Properties.Settings.Default.Bundle_Package;
                var pcCode = Properties.Settings.Default.Package_Component;

                Session["RegMobileSub_PkgPgmBdlPkgCompID"] = suppLine.PgmBdlPckComponentID;

                var vasIDs = PBPCs.Where(a => a.LinkType == pcCode).Select(a => a.ID).ToList();
                if (vasIDs.Count() > 0)
                {
                    strVAS = "";
                    foreach (var vasID in vasIDs)
                    {
                        strVAS = strVAS + vasID + ",";
                    }
                }
                Session["RegMobileSub_VasIDs"] = strVAS;

                if (!string.IsNullOrEmpty(suppLine.MSISDN1) || !string.IsNullOrEmpty(suppLine.MSISDN1))
                {
                    MSISDNs = new List<string>();
                    MSISDNs.Add(suppLine.MSISDN1);
                    MSISDNs.Add(suppLine.MSISDN2);
                }
                Session["RegMobileSub_MobileNo"] = MSISDNs;

                ConstructSubLineVM(false);
            }
        }

        private bool ValidateCardDetails(Customer customer)
        {
            if (!IsCardDetailsEntered(customer))
            {
                ModelState.AddModelError(string.Empty, "Please enter all Card details.");
                return false;
            }

            //if (customer.CardNo.Length != 16)
            //{
            //    ModelState.AddModelError(string.Empty, "Credit Card No must be 16 digit.");
            //    return false;
            //}

            //if (customer.CardExpiryDate.Length != 4)
            //{
            //    ModelState.AddModelError(string.Empty, "Credit Card Expiry Date must be 4 digit.");
            //    return false;
            //}

            return true;
        }

        private bool IsCardDetailsEntered(Customer customer)
        {
            if (!customer.CardTypeID.HasValue || customer.CardTypeID.Value <= 0)
                return false;

            if (string.IsNullOrEmpty(customer.CardNo))
                return false;

            if (string.IsNullOrEmpty(customer.CardExpiryDate))
                return false;

            if (string.IsNullOrEmpty(customer.NameOnCard))
                return false;

            return true;
        }

        //UNUSED METHODS
        //private creditCheckOperation ConstructInternalCheck(string idCardNo, string idCardTypeID)
        //{
        //    var userRoles = new List<string>();
        //    userRoles.Add(Properties.Settings.Default.IntChk_Role);

        //    var orgUnit = new List<string>();
        //    orgUnit.Add(Properties.Settings.Default.IntChk_Org);

        //    return new creditCheckOperation
        //    {
        //        creditCheckRequestField = new CreditCheck_RequestType
        //        {
        //            serviceHeaderField = new ServiceHeader
        //            {
        //                securityInfoField = new SecurityHeaderType
        //                {
        //                    orgUnitsField = orgUnit.ToArray(),
        //                    userRolesField = userRoles.ToArray(),
        //                    userNameTokenField = new UserNameTokenType()
        //                    {
        //                        idField = Properties.Settings.Default.IntChk_UserTokenID,
        //                        userNameField = Properties.Settings.Default.IntChk_UserTokenName,
        //                        userTypeField = Properties.Settings.Default.IntChk_UserTokenType
        //                    },
        //                },
        //                serviceInfoField = new ServiceRequestHeaderType
        //                {
        //                    operationNameField = Properties.Settings.Default.IntChk_Operation,
        //                    serviceNameField = new ServiceRequestHeaderTypeServiceName()
        //                    {
        //                        serviceIdField = Properties.Settings.Default.IntChk_ServiceID,
        //                        valueField = Properties.Settings.Default.IntChk_ServiceValue,
        //                        versionField = Properties.Settings.Default.IntChk_ServiceVersion
        //                    },
        //                    serviceRequesterInfoField = new ServiceRequesterInfoType()
        //                    {
        //                        applicationNameField = new ServiceRequesterInfoTypeApplicationName()
        //                        {
        //                            appIdField = Properties.Settings.Default.IntChk_AppID,
        //                            valueField = Properties.Settings.Default.IntChk_AppValue
        //                        },
        //                    },
        //                    transactionInfoField = new ServiceTransactionInfoType()
        //                    {
        //                        majorTransactionIdField = Properties.Settings.Default.IntChk_TransMajorID,
        //                        minorTransactionIdField = Properties.Settings.Default.IntChk_TransMinorID,
        //                        messageIdField = Properties.Settings.Default.IntChk_TransMsgID
        //                    }
        //                }
        //            },
        //            serviceRequestField = new _CreditCheckRequest
        //            {
        //                iD_TypeField = idCardTypeID, //Util.GetIDCardTypeID(IDCARDTYPE.NRIC).ToString2(),
        //                iD_ValueField = idCardNo
        //            }
        //        }
        //    };

        //    //String XmlizedString = null;
        //    //MemoryStream memoryStream = new MemoryStream();
        //    //XmlSerializer xs = new XmlSerializer(typeof(InternalCheck));
        //    //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, null);

        //    //xs.Serialize(xmlTextWriter, intCheck);
        //    //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
        //    //UTF8Encoding encode = new UTF8Encoding();
        //    //XmlizedString = encode.GetString(memoryStream.ToArray());

        //    //return XmlizedString;
        //}

        //UNUSED METHODS
        //private string ConstructExternalCheck(ExternalCheck extCheck)
        //{
        //    String XmlizedString = null;
        //    //MemoryStream memoryStream = new MemoryStream();
        //    //XmlSerializer xs = new XmlSerializer(typeof(ExternalCheck));
        //    //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, null);

        //    ////XmlWriterSettings settings = new XmlWriterSettings()
        //    ////{
        //    ////    OmitXmlDeclaration = true,
        //    ////    ConformanceLevel = ConformanceLevel.Fragment,
        //    ////    CloseOutput = false
        //    ////};



        //    //xs.Serialize(xmlTextWriter, extCheck);
        //    //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
        //    //UTF8Encoding encode = new UTF8Encoding();
        //    //XmlizedString = encode.GetString(memoryStream.ToArray());

        //    XmlizedString = StringXMLSerializer<ExternalCheck>.Convert2XML(extCheck);

        //    XmlDocument xmlDoc = new XmlDocument();
        //    xmlDoc.LoadXml(XmlizedString);

        //    xmlDoc.RemoveChild(xmlDoc.FirstChild);

        //    StringWriter sw = new StringWriter();
        //    XmlTextWriter tx = new XmlTextWriter(sw);
        //    xmlDoc.WriteTo(tx);

        //    XmlizedString = sw.ToString();

        //    //return "<![CDATA[" + XmlizedString + "]]>";
        //    return XmlizedString;

        //}

        private void ConstructSubLineVM(bool canRemove = true)
        {
            var package = new Package();
            var bundle = new Bundle();
            var sublineVMs = new List<SublineVM>();
            var vasNames = new List<string>();
            var selectedCompIDs = new List<int>();

            var vasIDs = Session["RegMobileSub_VasIDs"].ToString2().Split(',').ToList();
            foreach (var id in vasIDs)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    selectedCompIDs.Add(id.ToInt());
                }
            }

            using (var proxy = new SmartCatelogServiceProxy())
            {
                var bundlePackage = proxy.PgmBdlPckComponentGet(new int[] 
                    { 
                        (int)Session["RegMobileSub_PkgPgmBdlPkgCompID"] 
                    }).SingleOrDefault();

                package = proxy.PackageGet(new int[] 
                    { 
                        bundlePackage.ChildID 
                    }).SingleOrDefault();

                bundle = proxy.BundleGet(new int[] 
                    { 
                        bundlePackage.ParentID 
                    }).SingleOrDefault();

                vasNames = proxy.ComponentGet(
                    proxy.PgmBdlPckComponentGet
                    (
                        selectedCompIDs
                    ).Select(a => a.ChildID)
                ).Select(a => a.Name).ToList();
            }

            var sessionSublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
            if (sessionSublines != null)
            {
                if (sessionSublines.Count() > 0)
                    sublineVMs = sessionSublines;
            }

            sublineVMs.Add(new SublineVM()
            {
                CanRemove = canRemove,
                SequenceNo = sublineVMs.Count() + 1,
                PkgPgmBdlPkgCompID = (int)Session["RegMobileSub_PkgPgmBdlPkgCompID"],
                SelectedMobileNos = (List<string>)Session["RegMobileSub_MobileNo"],
                SelectedVasIDs = selectedCompIDs,
                SelectedVasNames = vasNames,
                BundleName = bundle.Name,
                PackageName = package.Name
            });

            Session[SessionKey.RegMobileReg_SublineVM.ToString()] = sublineVMs;
        }

        private void ClearSubLineSession()
        {
            Session["RegMobileSub_PkgPgmBdlPkgCompID"] = null;
            Session["RegMobileSub_MobileNo"] = null;
            Session["RegMobileSub_VasIDs"] = null;
            Session["RegMobileReg_SublineSummary"] = null;
        }

        private MobileNoUpdateRequest ConstructMobileNoUpdate()
        {
            var msisdn = ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2();
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            //Changed by chetan for dynamicaly sending data for update mobile no depending on user details
            string state = Util.GetNameByID(RefType.State, Util.SessionAccess.User.Org.StateID);
            return new MobileNoUpdateRequest()
            {
                MSISDN = msisdn,
                //DealerCode = "2083.00001",
                DealerCode = Util.SessionAccess.User.Org.DealerCode,
                //DealerName = "Allied Marketing & Trading Company", 
                DealerName = Util.SessionAccess.User.Org.Name,
                DealerCode2 = "",
                //DealerName2 = "Allied Marketing & Trading Company",
                DealerName2 = Util.SessionAccess.User.Org.Name,
                Name = personalDetailsVM.Customer.FullName,
                ContactNo = personalDetailsVM.Customer.ContactNo,
                ICNo = personalDetailsVM.Customer.IDCardNo,
                RegType = "N", //mapping
                Account = "", //mapping
                //MaxisCenter = "Alor Setar", //mapping
                MaxisCenter = Util.SessionAccess.User.Org.Code,//mapping
                //State = "SARAWAK", //mapping
                State = state,//mapping
                SelectFlag = "Y",
                SelectDate = DateTime.Now,
            };
        }

        private void ConstructGuidedSalesDetails()
        {
            Session[SessionKey.MenuType.ToString()] = MenuType.MobileRegistration.ToInt();
            Session[SessionKey.RegMobileReg_Type.ToString()] = Session["GuidedSales_Type"].ToInt();
            Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = MobileRegDeviceOption.Brand.ToInt();
            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = Session["GuidedSales_SelectedBrandID"].ToInt();
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = Session["GuidedSales_SelectedModelImageID"].ToInt();
            Session["RegMobileReg_SelectedModelID"] = Session["GuidedSales_SelectedModelID"].ToInt();
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = Session["GuidedSales_PkgPgmBdlPkgCompID"].ToInt();

            using (var proxy = new SmartCatelogServiceProxy())
            {
                var pbpc = proxy.PgmBdlPckComponentGet(new int[] { Session["GuidedSales_PkgPgmBdlPkgCompID"].ToInt() }).SingleOrDefault();

                var programID = proxy.PgmBdlPckComponentGet(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        ChildID = pbpc.ParentID,
                        LinkType = Properties.Settings.Default.Program_Bundle
                    },
                    Active = true
                })).SingleOrDefault().ParentID;

                var minAge = proxy.ProgramGet(new int[] { programID }).SingleOrDefault().MinAge;
                Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = minAge;
            }

            ClearGuidedSalesSession();
        }

        private void ClearGuidedSalesSession()
        {
            Session["GuidedSales_PkgPgmBdlPkgCompID"] = null;
            Session["GuidedSales_SelectedModelImageID"] = null;
            Session["GuidedSales_SelectedModelID"] = null;
            Session["GuidedSales_SelectedBrandID"] = null;
            Session["GuidedSales_Type"] = null;
        }

        private void ResetAllSession(int? type)
        {
            Session["RegMobileReg_FromSearch"] = null;
            Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
            ClearRegistrationSession();

            if (type == (int)MobileRegType.DevicePlan)
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
            else if (type == (int)MobileRegType.PlanOnly)
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
            else if (type == (int)MobileRegType.DeviceOnly)
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DeviceOnly;

            // From guided sales
            if (Session["GuidedSales_SelectedModelImageID"].ToInt() != 0 && Session["GuidedSales_PkgPgmBdlPkgCompID"].ToInt() != 0)
                ConstructGuidedSalesDetails();
            else
                ClearGuidedSalesSession();
        }

        [HttpGet]
        public JsonResult GetPackages()
        {
            var packageList = GetAvailablePackages();

            return Json(packageList, JsonRequestBehavior.AllowGet);
        }



        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        public ActionResult CustomerImage()
        {
            return View();
        }

        // Kenan Update Order Status testing method 
        [Authorize(Roles = "MREG_W,MREG_C,SMRT_USER")]
        [HttpPost]
        public ActionResult KenanUpdateOrderStatusTesting(int regID)
        {
            var result = "";
            var reg = new Online.Registration.DAL.Models.Registration();
            var cust = new Customer();
            var billAddr = new Address();

            //using (var proxy = new SmartRegistrationServiceProxy())
            //{
            //    reg = proxy.RegistrationGet(regID);
            //    cust = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

            //    var billAddrID = proxy.RegAddressFind(new AddressFind()
            //    {
            //        Address = new Address()
            //        {
            //            RegID = reg.ID,
            //            AddressTypeID = Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Billing)
            //        }
            //    }).SingleOrDefault();

            //    billAddr = proxy.RegAddressGet(new int[] { billAddrID }).SingleOrDefault();
            //}

            try
            {
                using (var proxy = new SmartKenanServiceProxy())
                {
                    var resp = proxy.UpdateKenanOrderStatus(new UpdateOrderStatusRequest()
                    {
                        MsgCode = "0",
                        MsgDesc = "",
                        OrderID = "ISELL" + regID.ToString(),
                        EventType = "CREATEORD",
                        FxAcctExtID = "12345",
                        FxAccIntID = "54321",
                        FxOrderID = "",
                        FxOrderIDResets = "",
                        FxSubscrNo = "",
                        FxSubscrNoResets = "",
                        ExternalList = new List<FxExternal>().ToArray(),

                    });

                    //var resp = proxy.KenanAccountCreate(new OrderCreationRequest()
                    //{
                    //    OrderID = regID.ToString(),
                    //    MarketCodeID = Util.GetIDByCode(RefType.Market, Properties.Settings.Default.MarketCode_Individual),
                    //    AccCategoryID = Util.GetIDByCode(RefType.AccountCategory, Properties.Settings.Default.AccountCategory_Consumer),
                    //    VIPCodeID = Util.GetIDByCode(RefType.VIPCode, Properties.Settings.Default.VIPCode_Individal),
                    //    ExternalID = reg.MSISDN1,
                    //    ExtIDTypeID = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN),
                    //    Customer = cust,
                    //    BillingAddress = billAddr,
                    //});

                    result = string.Format("Code:{0}, Message:{1}", resp.MsgCode, resp.MsgDesc);
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                result = ex.Message;
            }

            return Json(result);
        }

        [HttpPost]
        //[Authorize(Roles = "MREG_W,MREG_U")]
        public ActionResult UpdateIMEI(int regID, string imei)
        {
            var result = "";
            try
            {
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    proxy.RegistrationUpdate(new Online.Registration.DAL.Models.Registration()
                    {
                        ID = regID,
                        IMEINumber = imei,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                result = ex.Message;
            }

            return Json(result);
        }

        [HttpPost]
        //[Authorize(Roles = "MREG_W,MREG_U")]
        public ActionResult UpdateSIM(int regID, string sim)
        {
            var result = "";
            try
            {
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    proxy.RegistrationUpdate(new Online.Registration.DAL.Models.Registration()
                    {
                        ID = regID,
                        SIMSerial = sim,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                result = ex.Message;
            }

            return Json(result);
        }

        #region Sutan Added code 6Th March 2013
        /// <summary>        
        /// 1.Extracts the file information passed from Request collection. 
        /// 2.Uploads the actual image and resized image in temporary location.
        /// 3.Returns the image in base 64 encoded format.
        /// </summary>
        /// <Author>Sutan Dan</Author>
        /// <returns>TypeOf(Json)</returns>
        [HttpPost]
        public ActionResult UploadImage()
        {
            ///Get the upload and resize locations from settings
            ///PLEASE CREATE 'Uploads' AND 'Resized' FOLDERS IN THE PROJECT
            ///FOR EXAMPLE Online.Registration.Web/Uploads AND Online.Registration.Web/Resized
            string imageUploadLocation = Server.MapPath(Properties.Settings.Default.imageUploadLocation);
            string imageResizeLocation = Server.MapPath(Properties.Settings.Default.imageResizeLocation);
            string fitImage = Properties.Settings.Default.fitImage;

            bool isResized = false;

            string SourceImageName, ActualImageName, onlyFileExtension, imageUrl, imageType = string.Empty;

            System.Drawing.Image srcimage = null;
            imageUrl = string.Empty;

            ///CHECKS WHETHER THE Request.Files
            if (!ReferenceEquals(Request.Files, null) && Request.Files.Count > 0)
            {
                ///RETRIEVES THE FIRST KEY IN CASE THERE WOULD BE MULTIPLE FOUND.
                string PostedFileName = Request.Files.AllKeys.FirstOrDefault();
                ///TYPECASTING TO HttpPostedFileBase
                HttpPostedFileBase hpf = Request.Files[PostedFileName] as HttpPostedFileBase;

                ///ONLY PROCESS IF LENGTH >0
                if (hpf.ContentLength > 0)
                {
					//Keep the actual imagename for future reference
					ActualImageName = SourceImageName = Path.GetFileName(hpf.FileName);
					imageType = hpf.ContentType;
					///Check whether there is already an image in the folder with the same name
					Util.checkFileExistance(imageUploadLocation, ref SourceImageName, out onlyFileExtension);
					imageUploadLocation = Path.Combine(imageUploadLocation + @"\", SourceImageName);
					///Save the image
					hpf.SaveAs(imageUploadLocation);

					srcimage = System.Drawing.Image.FromFile(imageUploadLocation);
					///CALL TO CHECK AND LOWER THE RESOLUTION OF HIGH RESOULTION IMAGE
					//isResized = Util.ResizeImage(srcimage, 73, imageResizeLocation + SourceImageName, 540, 365, fitImage);
					isResized = Util.AlwaysResizeImage(srcimage, 23, imageResizeLocation + SourceImageName, 540, 365, fitImage);

					///Return the processed or unprocessed image as url
					if (isResized)
					{
						imageResizeLocation = Path.Combine(imageResizeLocation + @"\", SourceImageName);
						imageUrl = string.Format(@"data:{0};base64,", imageType) + Convert.ToBase64String(System.IO.File.ReadAllBytes(imageResizeLocation));
					}
					else
					{
						imageUrl = string.Format(@"data:{0};base64,", imageType) + Convert.ToBase64String(System.IO.File.ReadAllBytes(imageUploadLocation));
					}
                }
            }
            ///RETURNS THE IMAGE IN BASE 64 ENCODED FORMAT
            var jsonData = new { imageUrl = imageUrl };

            return Json(jsonData);
        }
        #endregion Sutan Added code 6Th March 2013

        [HttpPost]
        public JsonResult CreditCheck(string idCardNo, string idCardTypeID)
        {
            var resp = new BusinessRuleResponse();

            try
            {
                var idCardType = new IDCardType();
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    idCardType = proxy.IDCardTypeGet(idCardTypeID.ToInt());
                }

                using (var proxy = new SmartKenanServiceProxy())
                {
                    resp = proxy.BusinessRuleCheck(new BusinessRuleRequest()
                    {
                        RuleNames = Properties.Settings.Default.BusinessRule_MOBILERuleNames.Split('|'),
                        IDCardNo = idCardNo,
                        IDCardType = idCardType.KenanCode
                    });
                }

                Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] = resp.IsBlacklisted;
                Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = resp.ResultMessage;
            }
            catch (Exception ex)
            {
                LogException(ex);
                resp.ResultMessage = ex.Message;
            }

            return new JsonResult() { Data = new { isBlacklisted = resp.IsBlacklisted, msg = resp.ResultMessage } };
        }

        //[HttpPost]
        //public ActionResult CreateKenanAccount
        private void CreateKenanAccount(int regID)
        {
            //var result = "";
            var reg = new Online.Registration.DAL.Models.Registration();
            var cust = new Customer();
            var billAddr = new Address();

            try
            {
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    // ********** Update Registration Status **********
                    proxy.RegStatusCreate(ConstructRegStatus(regID, Properties.Settings.Default.Status_AccPendingCreate), null);

                    // ********** Construct Kenan object **********
                    reg = proxy.RegistrationGet(regID);
                    cust = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    var billAddrID = proxy.RegAddressFind(new AddressFind()
                    {
                        Address = new Address()
                        {
                            RegID = reg.ID,
                            AddressTypeID = Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Billing)
                        }
                    }).SingleOrDefault();

                    billAddr = proxy.RegAddressGet(new int[] { billAddrID }).SingleOrDefault();
                }

                using (var proxy = new SmartKenanServiceProxy())
                {
                    if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                    {

                        proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
                        {
                            orderId = regID.ToString(),
                            externalId = reg.MSISDN1,
                            extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
                            Customer = ConstructKenanCustomerMNP(cust, billAddr),
                            Account = ConstructKenanAccountMNP(cust),

                        });
                    }
                    else
                    {
                        if (reg.RegTypeID == (int)MobileRegType.MNPPlanOnly)
                        {

                            proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
                            {
                                orderId = regID.ToString(),
                                externalId = reg.MSISDN1,
                                extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
                                Customer = ConstructKenanCustomerMNP(cust, billAddr),
                                Account = ConstructKenanAccountMNP(cust),

                            });
                        }
                        else
                        {
                            proxy.KenanAccountCreate(new OrderCreationRequest()
                            {
                                OrderID = regID.ToString(),
                                MarketCodeID = Util.GetIDByCode(RefType.Market, Properties.Settings.Default.MarketCode_Individual),
                                AccCategoryID = Util.GetIDByCode(RefType.AccountCategory, Properties.Settings.Default.AccountCategory_Consumer),
								//VIPCodeID = Util.GetIDByCode(RefType.VIPCode, Properties.Settings.Default.VIPCode_Individal),
								// Add in Check For Wrong Collection Tagging
								VIPCodeID = Util.custVIPCheck(cust),
                                ExternalID = reg.MSISDN1,
                                ExtIDTypeID = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN),
                                Customer = ConstructKenanCustomer(cust),
                                BillingAddress = ConstructBillAddress(billAddr),


                            });
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                //result = ex.Message;
            }

            //return Json(result);
        }

        private KenanCustomer ConstructKenanCustomer(Customer cust)
        {
            return new KenanCustomer()
            {
                CustomerTitleID = cust.CustomerTitleID,
                DateOfBirth = cust.DateOfBirth,
                EmailAddr = cust.EmailAddr,
                FullName = cust.FullName,
                Gender = cust.Gender,
                IDCardNo = cust.IDCardNo,
                IDCardTypeID = cust.IDCardTypeID,
                NationalityID = cust.NationalityID,
                PayModeID = cust.PayModeID,
                RaceID = cust.RaceID,
                AlternateMSISDN = String.IsNullOrEmpty(cust.AlternateContactNo) ? String.Empty : cust.AlternateContactNo,
                CBR = cust.ContactNo
            };
        }

        private KenanAccountMNP ConstructKenanAccountMNP(Customer cust)
        {
            return new KenanAccountMNP()
            {

                mktCode = Util.GetIDByCode(RefType.Market, Properties.Settings.Default.MarketCode_Individual),
                acctCategory = Util.GetIDByCode(RefType.AccountCategory, Properties.Settings.Default.AccountCategory_Consumer),
                vipCode = Util.GetIDByCode(RefType.VIPCode, Properties.Settings.Default.VIPCode_Individal),
                custEmail = cust.EmailAddr,
                payMethod = cust.PayModeID
            };
        }

        private KenanCustomerMNP ConstructKenanCustomerMNP(Customer cust, Address billAddr)
        {
            return new KenanCustomerMNP()
            {
                CustomerTitleID = cust.CustomerTitleID,
                DateOfBirth = cust.DateOfBirth,
                EmailAddr = cust.EmailAddr,
                FullName = cust.FullName,
                Gender = cust.Gender,
                IDCardNo = cust.IDCardNo,
                IDCardTypeID = cust.IDCardTypeID,
                NationalityID = cust.NationalityID,
                PayModeID = cust.PayModeID,
                RaceID = cust.RaceID,
                CBR = cust.ContactNo,
                billAddress1 = billAddr.Line1,
                billAddress2 = billAddr.Line2,
                billAddress3 = billAddr.Line3,
                icType = Util.GetNameByID(RefType.IDCardType, cust.IDCardTypeID).ToString(),
                icNo = cust.IDCardNo,
                billLname = cust.FullName,
                billTitle = Util.GetNameByID(RefType.CustomerTitle, cust.CustomerTitleID).ToString(),
                billCity = billAddr.Town,
                billState = Util.GetNameByID(RefType.State, billAddr.StateID),
                billZip = billAddr.Postcode,
                billCounty = "",
                billCountryCode = "458",
            };
        }

        public BillAddress ConstructBillAddress(Address addr)
        {
            return new BillAddress()
            {
                BuildingNo = addr.BuildingNo,
                CountryID = addr.CountryID,
                Line1 = addr.Line1,
                Line2 = addr.Line2,
                Postcode = addr.Postcode,
                StateID = addr.StateID,
                Town = addr.Town,
                UnitNo = addr.UnitNo
            };
        }

        private RegStatus ConstructRegStatus(int regID, string statusCode)
        {
            return new Online.Registration.DAL.Models.RegStatus()
            {
                RegID = regID,
                StartDate = DateTime.Now,
                StatusID = Util.GetIDByCode(RefType.Status, statusCode),
                CreateDT = DateTime.Now,
                Active = true,
                LastAccessID = Util.SessionAccess.UserName
            };
        }

        private void FulfillKenanAccount(int regID)
        {
            //var result = "";
            var reg = new Online.Registration.DAL.Models.Registration();
            var cust = new Customer();
            var billAddr = new Address();

            try
            {
                //using (var proxy = new SmartRegistrationServiceProxy())
                //{
                //    // ********** Update Registration Status **********
                //    proxy.RegStatusCreate(ConstructRegStatus(regID, Properties.Settings.Default.Status_SvcPendingActivate), null);

                //    // ********** Construct Kenan object **********
                //    reg = proxy.RegistrationGet(regID);
                //    cust = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();
                //}

                var request = new OrderFulfillRequest()
                {
                    OrderID = regID.ToString(),
                    //AccountExternalID = reg.KenanAccountNo,
                    //Gender = cust.Gender,
                    //RaceID = cust.RaceID,
                    //NationalityID = cust.NationalityID,
                    //CountryID = Util.GetIDByCode(RefType.Country, Properties.Settings.Default.Country_MY),
                    //DateOfBirth = cust.DateOfBirth,
                    //ExternalIDList = ConstructExternalIDList(reg.SIMSerial, reg.MSISDN1).ToArray()
                };

                //// construct Package, Components, and Contract
                //ConstructKenanPackageComponent(request);

                using (var proxy = new SmartKenanServiceProxy())
                {
                    proxy.KenanAccountFulfill(request);
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                //result = ex.Message;
            }

            //return Json(result);
        }

        public FileContentResult QRCodeGenerate(string value)
        {
            byte[] bitmapBytes = null;
            var bitmapImg = Util.GenerateQRCode("MECARD:N:Jamaludin b m yusof;ORG:12345;TEL:321451234512345;ADR:1234567890123456789 9999;NOTE:1.09876543;;", 150, 150);

            using (MemoryStream memoryStream = new MemoryStream())
            {
                bitmapImg.Save(memoryStream, ImageFormat.Png);
                bitmapBytes = memoryStream.GetBuffer();
            }

            return new FileContentResult(bitmapBytes, "image/bmp");
        }

        public ActionResult ComingSoon(string text)
        {
            ViewBag.Text = text;

            return View();
        }

        public ActionResult PaymentDetails(int id)
        {
            Online.Registration.Web.SmartViewModels.OrderSummaryVM MobileRegOrderSummary = (OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()];
            string Amount = (MobileRegOrderSummary.PersonalDetailsVM.PlanAdvance + MobileRegOrderSummary.PersonalDetailsVM.PlanDeposit + MobileRegOrderSummary.PersonalDetailsVM.DeviceAdvance + MobileRegOrderSummary.PersonalDetailsVM.DeviceDeposit + ((MobileRegOrderSummary.ItemPrice != 0) ? MobileRegOrderSummary.ItemPrice : MobileRegOrderSummary.ModelPrice)).ToString();
            Session["OrderId"] = null;
            Session["Amount"] = null;
            ViewBag.Amount = Amount;
            ViewBag.OrderId = id;
            return View();
        }

        [HttpPost]
        public ActionResult PaymentDetails(FormCollection frm)
        {
            #region Commented by chetan
            /*Logic for inserting the data card info to db*/
            //using (var proxy = new SmartRegistrationServiceProxy())
            //{

            //    proxy.CustomerInfoCreate(new CustomerInfo()
            //    {
            //        PayModeID = Convert.ToInt32(frm["CustomerInfo.PayModeID"]),
            //        CardTypeID = Convert.ToInt32(frm["CustomerInfo.PayModeID"]),
            //        NameOnCard = frm["CustomerInfo.NameOnCard"],
            //        CardNo = CalculateMD5Hash(frm["CustomerInfo.CardNo"]),
            //        CardExpiryDate = frm["CustomerInfo.CardExpiryDate"],
            //        CVV = frm["CustomerInfo.CVV"],
            //        OrderID = Session["OrderId"].ToString(),
            //        Amount = Amount,
            //        Active = true,
            //        CreateDT = DateTime.Now,
            //        LastAccessID = Util.SessionAccess.UserName,

            //    });
            //}

            //return RedirectToAction("PaymentDetailsSuccess");
            #endregion

            string OrderId = Session["OrderId"].ToString();
            string Amount = Session["Amount"].ToString();
            string HashingSaltValue = "1234567890";
            string HashingSalt = CalculateMD5Hash(HashingSaltValue + Amount + OrderId + OrderId);

            if (frm["CustomerInfo.PayModeID"] == "1")
            {
                return Redirect("PaymentDetailsSuccess?id=" + OrderId + "&paymentstatus=Success");
            }
            else if (frm["CustomerInfo.PayModeID"] == "2")
            {
                //return Redirect("https://merchantstore/payment.aspx?MerchTxnRef=" + OrderId + "&OrderInfo=" + OrderId + "&Amount=" + Amount + "&Hash=" + HashingSalt);
                return Redirect("http://203.121.86.30/MaxisPaymentISELL/PGGP/doCardPayment.aspx?MerchTxnRef=" + OrderId + "&OrderInfo=" + OrderId + "&Amount=" + Amount + "&Hash=" + HashingSalt);
            }
            return Redirect("PaymentDetailsSuccess?id=" + OrderId + "&paymentstatus=Failed");
        }

        public ActionResult PaymntReceived()
        {
            return View();
        }

        public ActionResult PlanPaymntReceived()
        {
            return View();
        }

        public ActionResult PaymentDetailsSuccess(int id, string paymentstatus)
        {

            ViewBag.Paymentstatus = paymentstatus;
            return View();

        }

        [HttpPost]
        public ActionResult PaymentDetailsSuccess(FormCollection frm)
        {
            string OrderId = Request.QueryString["id"].ToString();
            string Paymentstatus = Request.QueryString["paymentstatus"].ToString();

            if (Paymentstatus == "Success")
            {
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    proxy.RegistrationPaymentSuccess(new RegStatus()
                    {
                        RegID = Convert.ToInt32(OrderId),
                        Active = true,
                        CreateDT = DateTime.Now,
                        StartDate = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName,
                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegPaymentSuccess)
                    });
                }
            }
            else if (Paymentstatus == "Failed")
            {
                using (var proxy = new SmartRegistrationServiceProxy())
                {
                    proxy.RegistrationPaymentFailed(new RegStatus()
                    {
                        RegID = Convert.ToInt32(OrderId),
                        Active = true,
                        CreateDT = DateTime.Now,
                        StartDate = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName,
                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegPaymentFailed)
                    });
                }
            }

            return RedirectToAction("MobileRegSummary", new { id = OrderId });
        }
        public string CalculateMD5Hash(string input)
        {

            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
        #region IMPOS File creation code

        #region imports

        //UNUSED METHODS
        //[DllImport("advapi32.dll", SetLastError = true)]
        //private static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        //[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        //private static extern bool CloseHandle(IntPtr handle);

        //[DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        //public extern static bool DuplicateToken(IntPtr existingTokenHandle,
        //int SECURITY_IMPERSONATION_LEVEL, ref IntPtr duplicateTokenHandle);
        #endregion

        #region logon consts
        // logon types
        const int LOGON32_LOGON_INTERACTIVE = 2;
        const int LOGON32_LOGON_NETWORK = 3;
        const int LOGON32_LOGON_NEW_CREDENTIALS = 9;

        // logon providers
        const int LOGON32_PROVIDER_DEFAULT = 0;
        const int LOGON32_PROVIDER_WINNT50 = 3;
        const int LOGON32_PROVIDER_WINNT40 = 2;
        const int LOGON32_PROVIDER_WINNT35 = 1;
        #endregion



        #endregion
        private int GetContractDuration(int ContractId)
        {
            int ContractDuration = 0;
            using (var proxy = new SmartCatelogServiceProxy())
            {
                ContractDuration = proxy.GetContractDuration(ContractId);
            }
            return ContractDuration;
        }
        private string GetUomId(string ArticleId, int ContractDuration, string contractID)
        {
            string UomId = string.Empty;
            using (var proxy = new SmartCatelogServiceProxy())
            {
                //UomId = proxy.GetUomId(ArticleId, ContractDuration, contractID);
                UomId = proxy.GetUomId(ArticleId, ContractDuration, contractID, "");
            }
            return UomId;
        }


        #region Smart Methods
        public ActionResult CmssAndContractCheck()
        {
            if (Session[SessionKey.PPID.ToString()] != null)
            {
                if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                {
                    WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "E");
                }
                else
                {
                    WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");
                }

            }

            bool isValidPackage = false;
            Session["CMSSResponse"] = null;
            Session["ErrMsg"] = string.Empty;
            Session["ContractType"] = null;
            Session["Penalty"] = null;
            IList<SubscriberICService.PackageModel> packages;
            //PackageModel package = new PackageModel();
            SubscriberICService.PackageModel package = new SubscriberICService.PackageModel();
            string MSISDN = string.Empty;
            Session[SessionKey.CMSID.ToString()] = null;
            package.Contract12Months = ConfigurationManager.AppSettings["ContractID12Months"];
            package.Contract24Months = ConfigurationManager.AppSettings["ContractID24Months"];
            ContractDetails ContractDet = new ContractDetails();
            Online.Registration.Web.SmartViewModels.DateDifference datediff;
            if (!ReferenceEquals(Request.QueryString["KenanID"], null) && !ReferenceEquals(Request.QueryString["MSISDN"], null))
            {
                MSISDN = Request.QueryString["MSISDN"].ToString();
                Session["KenanAccountNo"] = Request.QueryString["KenanID"].ToString();
                Session[SessionKey.RegMobileReg_MSISDN.ToString()] = Request.QueryString["MSISDN"].ToString();
                Session["SubscriberNo"] = Request.QueryString["SubscriberNo"].ToString();
                Session["SubscriberNoResets"] = Request.QueryString["SubscriberNoResets"].ToString();
                Session[SessionKey.ExternalID.ToString()] = MSISDN;
            }
            else
            {
                if (Session[SessionKey.ExternalID.ToString()] != null)
                    MSISDN = Session[SessionKey.ExternalID.ToString()].ToString();
            }

            packages = GetPackageDetails(MSISDN, ConfigurationManager.AppSettings["KenanMSISDN"]);
            GetExtendPlans();
            var pkgs = (IEnumerable<string>)Session["PlansList"];
            foreach (var pkg in pkgs)
            {
                foreach (var ctrt in packages)
                {
                    if (pkg == ctrt.PackageDesc)
                    {
                        isValidPackage = true;
                    }
                }
            }
            //if (isValidPackage)
            //{
            foreach (var contract in packages)
            {
                ContractDet.PackageID = contract.PackageID;
                ContractDet.PackageDesc = contract.PackageDesc;
                for (int i = 0; i < contract.compList.Count; i++)
                {
                    if (contract.compList[i].componentId == package.Contract12Months)
                    {
                        ContractDet.ActiveDate = DateTime.ParseExact(contract.compList[i].componentActiveDt.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        ContractDet.EndDate = ContractDet.ActiveDate.AddYears(1);
                        ContractDet.EndDate = DateTime.ParseExact(ContractDet.EndDate.ToString("yyyyMMdd"), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        datediff = new Online.Registration.Web.SmartViewModels.DateDifference(DateTime.Now, ContractDet.EndDate);
                        ContractDet.ContractRemaining = "Contract Remaining for: " + datediff.Years + " Years " + datediff.Months + " Months " + datediff.Days + " Days ";
                        ContractDet.ContractDuration = Convert.ToInt16(datediff.Years) * 12 + Convert.ToInt16(datediff.Months);
                        ContractDet.ComponentDesc = contract.compList[i].componentDesc;

                    }
                    else if (contract.compList[i].componentId == package.Contract24Months)
                    {
                        ContractDet.ActiveDate = DateTime.ParseExact(contract.compList[i].componentActiveDt.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        ContractDet.EndDate = ContractDet.ActiveDate.AddYears(2);
                        ContractDet.EndDate = DateTime.ParseExact(ContractDet.EndDate.ToString("yyyyMMdd"), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        datediff = new Online.Registration.Web.SmartViewModels.DateDifference(DateTime.Now, ContractDet.EndDate);
                        ContractDet.ContractRemaining = "Contract Remaining for: " + datediff.Years + " Years " + datediff.Months + " Months " + datediff.Days + " Days ";
                        ContractDet.ContractDuration = Convert.ToInt16(datediff.Years) * 12 + Convert.ToInt16(datediff.Months);
                        ContractDet.ComponentDesc = contract.compList[i].componentDesc;
                    }
                }
            }
            //}
            if (ContractDet.ComponentDesc != null)
            {

                Session["ErrMsg"] = string.Empty;
                if (ContractDet.ContractDuration > 18)
                    ContractDet.ContractExtend = "N";
                else if (ContractDet.ContractDuration <= 6)
                {
                    ContractDet.ContractExtend = "Y";
                    ContractDet.ContractExtendType = "B";
                }
                else if (ContractDet.ContractDuration <= 18 && ContractDet.ContractDuration > 6)
                {
                    ContractDet.ContractExtend = "Y";
                    ContractDet.ContractExtendType = "12";
                }
                else if (ContractDet.ContractDuration > ConfigurationManager.AppSettings["TotalContractDurationinMonths"].ToInt())
                {
                    ContractDet.ContractExtend = "N";
                }

                //if (ContractDet.ContractDuration > ConfigurationManager.AppSettings["TotalContractDurationinMonths"].ToInt())
                //    ContractDet.ContractTerimnate = "N";
                //else
                //    ContractDet.ContractTerimnate = "Y";
            }

            else
            {
                Session["ErrMsg"] = ConfigurationManager.AppSettings["NoContracts"];
                Session["ErrMsg"] = Session["ErrMsg"].ToString().Replace("[MSISDN]", MSISDN);
            }
            bool hasivalueplan = false;

            if (ContractDet.ContractExtend != null)

            //if (ContractDet.ContractExtend != null && ContractDet.ContractExtend.ToUpper() == "Y")
            {
                if (ContractDet.ContractExtend.ToUpper() == "Y")
                {
                    foreach (var contract in packages)
                    {
                        if (contract.PackageDesc.ToLower().IndexOf("ivalue") > 0)
                        {
                            hasivalueplan = true;
                        }
                    }
                }
            }
            if (!hasivalueplan)
                ContractDet.ContractExtend = "N";

            Session["ContractDet"] = ContractDet;
            Session["Package"] = packages;
            return View(ContractDet);
        }
        [HttpPost]
        public ActionResult CmssAndContractCheck(ContractDetails model, FormCollection Collection)
        {
            Session["CMSSResponse"] = null;


            ContractDetails ContractDet = new ContractDetails();
            Online.Registration.Web.Helper.Util.retrievePenaltyByMSISDN penaltybyMSISDN = new Online.Registration.Web.Helper.Util.retrievePenaltyByMSISDN();
            string msisdn = Session[SessionKey.RegMobileReg_MSISDN.ToString()].ToString();
            string externalid = ConfigurationManager.AppSettings["KenanMSISDN"].ToString();
            string Subscriber = Session["SubscriberNo"].ToString();
            string SubscriberNoResets = Session["SubscriberNoResets"].ToString();
            if (Session[SessionKey.PPIDInfo.ToString()] != null)
            {
                List<Items> itemList = ((retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()]).itemList.Where(a => a.ExternalId == (msisdn)).ToList();
                Session[SessionKey.AccountNumber.ToString()] = itemList[0].AcctIntId;
            }

            if (ModelState.IsValid)
            {

                if (Session["ContractDet"] != null)
                    ContractDet = (ContractDetails)Session["ContractDet"];
                if (Collection["searchCustIDNumber"] != null)
                {

                    CMSS CMSS = new CMSS();
                    CMSS = Util.retrieveCaseDetls(model.CMSSID, userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
                            password: Properties.Settings.Default.retrieveAcctListByICResponsePassword, postUrl: Properties.Settings.Default.retrieveCMSDetsPostUrl);

                    if (Convert.ToInt16(CMSS.msgCode) == 0)
                    {
                        Session["CMSSResponse"] = CMSS.msgDesc;
                    }
                    else
                    {
                        Session["CMSSResponse"] = CMSS.msgDesc;
                        ViewBag.ErrDesc = CMSS.msgCode;
                        return View(ContractDet);
                    }
                }
                else if (Collection["Continue"] != null)
                {

                    Session[SessionKey.CMSID.ToString()] = "Y";
                    if (Session["ContractType"] == null)
                    {
                        Session["ErrMsg"] = ConfigurationManager.AppSettings["ContractErrMsg"].ToString();
                        return View(ContractDet);
                    }
                    else
                    {
                        Session["ErrMsg"] = string.Empty;
                        if (Session[SessionKey.PPID.ToString()] != null)
                        {
                            if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                            {
                                WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "E");
                            }
                            else
                            {
                                WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");
                            }

                        }
                        return RedirectToAction("SelectDevice", "SmartRegistration", new { type = (int)MobileRegType.DevicePlan });
                    }
                }
                else if (Collection["Extend"] != null)
                {
                    Session[SessionKey.CMSID.ToString()] = "N";
                    Session["ErrMsg"] = string.Empty;
                    Session["Penalty"] = null;
                    if (Session["ContractType"] != null && Session["ContractType"].ToString() == "Extend")
                    {
                        Session["ContractType"] = null;
                    }
                    else
                    {
                        Session["ContractType"] = "Extend";
                    }
                }
                else if (Collection["Termination"] != null)
                {
                    Session["ErrMsg"] = string.Empty;
                    Session[SessionKey.CMSID.ToString()] = "N";
                    if (Session["ContractType"] != null && Session["ContractType"].ToString() == "Terimnate")
                    {
                        Session["Penalty"] = null;
                        Session["ContractType"] = null;
                    }
                    else
                    {
                        Session["ContractType"] = "Terimnate";
                        penaltybyMSISDN = Util.retrievePenalty(msisdn, externalid, Subscriber, SubscriberNoResets, userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
                            password: Properties.Settings.Default.retrieveAcctListByICResponsePassword, postUrl: Properties.Settings.Default.retrievePenaltyAmount);
                        Session["Penalty"] = penaltybyMSISDN.penalty;

                    }
                }
            }
            return View(ContractDet);
        }

        private IList<SubscriberICService.PackageModel> GetPackageDetails(string MSISDN, string Kenancode)
        {
            var proxy = new retrieveSmartServiceInfoProxy();

            IList<SubscriberICService.PackageModel> packages = proxy.retrievePackageDetails(MSISDN, Kenancode, userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
                password: Properties.Settings.Default.retrieveAcctListByICResponsePassword);
            return packages;

        }
        public ActionResult SelectDiscount()
        {
            Session["DiscountStatus"] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session["NetPrice"] = null;
            Session["DPrice"] = null;
            Online.Registration.Web.SmartViewModels.SmartModel Smart = new Online.Registration.Web.SmartViewModels.SmartModel();
            OrderSummaryVM OrderSummary = ConstructOrderSummary();
            Smart.PhoneModel = OrderSummary.ModelName;
            Smart.PhoneColor = OrderSummary.ColourName;
            Smart.DevicePrice = OrderSummary.ModelPrice.ToString();
            Session["RegMobileReg_RRPPrice"] = Smart.DevicePrice.ToString();
            SetRegStepNo(MobileRegistrationSteps.MobileNo);
            return View(Smart);
        }
        [HttpPost]
        public ActionResult SelectDiscount(FormCollection Collection, SmartModel smartmodelVM)
        {
            string discountID = string.Empty;
            SmartRebateReq oReq = new SmartRebateReq();
            SmartRebateRes oRp = new SmartRebateRes();
            SmartRebates smartRebate = new SmartRebates();
            var proxy = new SmartCatelogServiceProxy();
            if (Collection["Button"] == null)
            {
                if (GetDiscount(Collection, smartmodelVM))
                {

                    Online.Registration.Web.SmartViewModels.SmartModel Smart = new Online.Registration.Web.SmartViewModels.SmartModel();
                    OrderSummaryVM OrderSummary = ConstructOrderSummary();
                    Smart.PhoneModel = OrderSummary.ModelName;
                    Smart.PhoneColor = OrderSummary.ColourName;
                    Smart.DevicePrice = OrderSummary.ModelPrice.ToString();
                    //ViewData["NetPrice"] = Convert.ToDouble(Convert.ToDouble(Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()].ToString()) - discount).ToString();
                    ViewData["NetPrice"] = Convert.ToDouble(Convert.ToDouble(Smart.DevicePrice) - discount).ToString();
                    Session["NetPrice"] = ViewData["NetPrice"];

                    discount = Session["DPrice"].ToInt();
                    if (Collection["chkRebatel1000"].ToString().ToLower() == "freedevice")
                        discountID = "0";
                    else
                    {
                        smartRebate.Rebate = discount.ToString();
                        oReq.SmartRebates = smartRebate;
                        discountID = proxy.GetDiscountID(oReq);
                    }
                    Session["discountID"] = discountID.ToString();
                    if (discount > Convert.ToInt32(ConfigurationManager.AppSettings["SmartDiscountLimit"]))
                    {
                        Session["DiscountStatus"] = "R";
                        status = "P";
                    }
                    else
                    {
                        Session["DiscountStatus"] = "A";
                        status = "A";
                    }
                    if (Collection["penalty"] != null)
                    {
                        smartmodelVM.isPenaltyChecked = true;
                        Session["isPenaltyCheck"] = true;
                        if (Collection["penalty"].ToString().ToLower() == "fullpenaltywaiver")
                        {
                            Session["Penalty"] = null;
                            Session["DiscountStatus"] = "R";
                            status = "P";
                        }
                    }
                    if (status == "P")
                        return View(Smart);
                    else
                        return RedirectToAction(GetRegActionStep(smartmodelVM.TabNumber));
                }
                else
                {
                    if (smartmodelVM.TabNumber == 3)
                        return RedirectToAction(GetRegActionStep(smartmodelVM.TabNumber));
                    else
                        return View();

                    //if (Session[SessionKey.Discount.ToString()] != null)
                    //    return View();
                    //else
                    //    return RedirectToAction(GetRegActionStep(smartmodelVM.TabNumber));
                }
            }
            #region Commneted by sutan
            else
                //{
                //    bool isValidUser = isvalidSupervisor(Collection["Username"].ToString(), Collection["Password"].ToString());
                //    if (isValidUser)
                //    {
                //        smartmodelVM.TabNumber = smartmodelVM.TabNumber + 1;
                //        return RedirectToAction(GetRegActionStep(smartmodelVM.TabNumber));
                //    }
                //    else
                //        return View();
                //}
            #endregion Commneted by sutan
                return View();
        }
        private bool GetDiscount(FormCollection form, SmartModel smartmodelVM)
        {
            string radiobuttonclicked = string.Empty;
            Session["DiscountPrice"] = null;
            if (form["chkRebatel1000"] != null)
            {
                radiobuttonclicked = form["chkRebatel1000"].ToString();
                if (radiobuttonclicked.ToUpper() == "REBATEL1000")
                {
                    if (!string.IsNullOrEmpty(form["ddlRebatel1000"]))
                    {
                        discount = Convert.ToInt32(form["ddlRebatel1000"].ToString());
                        Session["DiscountPrice"] = discount;
                    }
                }
                else if (radiobuttonclicked.ToUpper() == "REBATEG1000")
                {

                    if (!string.IsNullOrEmpty(form["ddlRebateg1000"]))
                    {
                        discount = Convert.ToInt32(form["ddlRebateg1000"].ToString());
                        Session["DiscountPrice"] = discount;
                    }
                }
                else if (radiobuttonclicked.ToUpper() == "FREEDEVICE")
                {
                    discount = Convert.ToInt32(Session["RegMobileReg_RRPDevicePrice"].ToString());
                    Session["DiscountPrice"] = discount;
                }
                if (Session["DiscountPrice"] != null)
                {
                    Session["DPrice"] = Session["DiscountPrice"];
                    return true;
                }
                else
                {
                    smartmodelVM.DiscountType = "Please select discount";
                    Session[SessionKey.Discount.ToString()] = smartmodelVM.DiscountType;
                }
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult isvalidSupervisor(string Username, string Password)
        {
            int UserID = Util.SessionAccess == null ? 0 : Util.SessionAccess.UserID;
            bool isValidUser = false;
            int orgid = Util.SessionAccess.User.Org.ID;

            string status = string.Empty;
            if (Session["ID"] != null)
            {
                ID = Session["ID"].ToInt();
            }

            var access = UserServiceProxy.GetAccess(Username.ToString());
            var user = UserServiceProxy.GetUser(access.UserID);
            if (access == null)
            {
                //ModelState.AddModelError("", "The user name does not exist.");
                status = "The user name does not exist.";
                isValidUser = false;
                //return isValidUser;

            }
            else if (access.IsLockedOut)
            {
                //ModelState.AddModelError("", "This account has been Locked out.");
                status = "This account has been Locked out.";
                isValidUser = false;
                //return isValidUser;
            }
            else if (user.OrgID != orgid)
            {
                status = "This account work for different Organization.";
                isValidUser = false;
            }

            else
            {
                string sha1Pswd = Util.GetMD5Hash(Password.ToString());
                var proxy1 = new UserServiceProxy();
                access = proxy1.GetAccessByUsername(Username, sha1Pswd);
                if (access == null)
                {
                    //ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    status = "The user name or password provided is incorrect.";
                    isValidUser = false;
                }
                else
                {
                    if (Roles.IsUserInRole(Username, "SMRT_MANAGER"))
                    {
                        isValidUser = true;
                    }
                    else
                    {
                        status = "Not a supervisor user.";
                        isValidUser = false;
                    }
                }
            }
            var jsonData = new { status = status, isValidUser = isValidUser };
            return Json(jsonData);
        }
        //private bool isvalidSupervisor(string Username, string Password)
        //{
        //    int UserID = Util.SessionAccess == null ? 0 : Util.SessionAccess.UserID;
        //    bool isValidUser = true;

        //    string status = string.Empty;
        //    if (Session["ID"] != null)
        //    {
        //        ID = Session["ID"].ToInt();
        //    }

        //    var access = UserServiceProxy.GetAccess(Username.ToString());
        //    if (access == null)
        //    {
        //        ModelState.AddModelError("", "The user name does not exist.");
        //        isValidUser = false;
        //        return isValidUser;

        //    }
        //    else if (access.IsLockedOut)
        //    {
        //        ModelState.AddModelError("", "This account has been Locked out.");
        //        isValidUser = false;
        //        return isValidUser;
        //    }

        //    else
        //    {
        //        string sha1Pswd = Util.GetMD5Hash(Password.ToString());
        //        var proxy1 = new UserServiceProxy();
        //        access = proxy1.GetAccessByUsername(Username, sha1Pswd);
        //        if (access == null)
        //        {
        //            ModelState.AddModelError("", "The user name or password provided is incorrect.");
        //            isValidUser = false;
        //            return isValidUser;
        //        }
        //    }
        //    isValidUser = Roles.IsUserInRole(Username, "SMRT_MANAGER");
        //    return isValidUser;
        //}
        private void GetExtendContracts()
        {
            var proxy = new SmartCatelogServiceProxy();
            ContractBundleModels model = new ContractBundleModels();
            IEnumerable<string> list = proxy.ExtendContract(model);
            Session["list"] = list;
        }
        private void GetExtendPlans()
        {
            var proxy = new SmartCatelogServiceProxy();
            ContractPlans model = new ContractPlans();
            IEnumerable<string> PlansList = proxy.ExtendPlanContract(model);
            Session["PlansList"] = PlansList;
        }
        private List<PackageComponents> ConstructPackage()
        {
            IList<Online.Registration.Web.SmartViewModels.PackageModel> packages = (IList<Online.Registration.Web.SmartViewModels.PackageModel>)Session["Package"];
            List<PackageComponents> pckg = new List<PackageComponents>();
            foreach (var contract in packages)
            {
                for (int i = 0; i < contract.compList.Count; i++)
                {
                    pckg.Add(new PackageComponents()
                    {
                        PackageId = contract.PackageID,
                        packageInstId = contract.packageInstId,
                        packageInstIdServ = contract.packageInstIdServ,
                        packageDesc = contract.PackageDesc,
                        componentId = contract.compList[i].componentId,
                        componentInstId = contract.compList[i].componentInstId,
                        componentInstIdServ = contract.compList[i].componentInstIdServ,
                        componentActiveDt = contract.compList[i].componentActiveDt,
                        componentInactiveDt = contract.compList[i].componentInactiveDt,
                        componentDesc = contract.compList[i].componentDesc,
                        componentShortDisplay = contract.compList[i].componentShortDisplay
                    });
                }
            }
            return pckg;

        }

        /// <summary>
        /// Added by KD
        /// </summary>
        /// <returns></returns>
        private CMSSComplain ConstructCMSSID()
        {
            CMSSComplain cms = new CMSSComplain();
            cms.CMSSID = Session["CMSSID"] == null ? string.Empty : Session["CMSSID"].ToString();
            return cms;
        }

        private List<RegSmartComponents> GetSelectedComponents(string regId)
        {
            List<RegSmartComponents> listRegSmartComponents = new List<RegSmartComponents>();

            using (var proxy = new SmartCatelogServiceProxy())
            {

                listRegSmartComponents = proxy.GetSelectedComponents(regId);
            }

            return listRegSmartComponents;

        }

        //[HttpPost]
        //public string CheckSimModel(string sim, string simCardType)
        //{
        //    #region Variable declaretion
        //    string resStatus = string.Empty;
        //    string regszArticleID = string.Empty;
        //    string regszStatus = string.Empty;
        //    string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
        //    bool kenanStatus = false;
        //    #endregion
        //    #region ValidationLogic for sim model
        //    using (var proxy = new DynamicServiceProxy())
        //    {
        //        resStatus = proxy.GetSerialArtStatus(sim.Trim().Length > 18 ? sim.Trim().Substring(0, 18) : sim.Trim(), wsdlUrl);
        //        //Should Uncomment after POS updation in production
        //        if (!string.IsNullOrEmpty(resStatus) && resStatus.Contains("|"))
        //        {
        //            regszArticleID = resStatus.Split('|')[0];
        //            regszStatus = resStatus.Split('|')[1];
        //        }
        //    }

        //    #region Kenan Inventry Validation
        //    using (var proxy = new KenanServiceProxy())
        //    {
        //        kenanStatus = proxy.validateInventoryExtIdService(new TypeValidateInventoryExtIdRequest
        //        {
        //            externalId = sim
        //        });

        //        if (!kenanStatus)
        //            return string.Empty;
        //    }
        //    #endregion
        //    return CheckSimModelName(regszArticleID, simCardType);
        //    #endregion
        //}

        //private string CheckSimModelName(string articleId, string simCardType)
        //{          

        //    string modelName = string.Empty;
        //    using (var proxy = new CatalogServiceProxy())
        //    {
        //        List<SimModels> simModels = proxy.GetModelName(Convert.ToInt32(articleId), simCardType);
        //        if (simModels.Count > 0)
        //        {
        //            modelName = simModels[0].SimTypeDescription;
        //            Session["SimModelTypeId"] = simModels[0].ID;
        //        }
        //    }
        //    return modelName;
        //}

        #endregion Smart Methods

        [HttpPost]
        public string CheckSimModel(string sim, string simCardType, string SIMType)
        {
            #region Variable declaretion
            string resStatus = string.Empty;
            string regszArticleID = string.Empty;
            string regszStatus = string.Empty;
            string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
            string storeId = Util.SessionAccess.User.Org.OrganisationId;
            bool kenanStatus = false;
            #endregion
            #region ValidationLogic for sim model
            if (Session["IsDealer"].ToBool() == false)
            {
                using (var proxy = new DynamicServiceProxy())
                {
                    resStatus = proxy.GetSerialNumberStatus(storeId, sim.Trim().Length > 18 ? sim.Trim().Substring(0, 18) : sim.Trim(), wsdlUrl);
                    //Should Uncomment after POS updation in production
                    if (!string.IsNullOrEmpty(resStatus) && resStatus.Contains("|"))
                    {
                        regszArticleID = resStatus.Split('|')[0];
                        if (Session["IsDealer"].ToBool())
                        {
                            regszStatus = ConfigurationManager.AppSettings["IMEIStatus"];
                        }
                        else
                        {
                            regszStatus = resStatus.Split('|')[1];
							if (!regszStatus.Equals("ONSTOCK"))
							{
								return string.Empty;
							}
                        }
                    }
                }
            }

            #region Kenan Inventry Validation
            using (var proxy = new KenanServiceProxy())
            {
                kenanStatus = proxy.validateInventoryExtIdService(new TypeValidateInventoryExtIdRequest
                {
                    externalId = sim
                });


                if (!kenanStatus)
                {

                    return string.Empty;
                }

            }
            #endregion
            if (Session["IsDealer"].ToBool() == false)
            {
                return CheckSimModelName(regszArticleID, simCardType, SIMType);
            }
            else
            {
                if (!kenanStatus)
                {
                    return string.Empty;
                }
                else
                {
                    return "Success";
                }
            }
            #endregion
        }

        private string CheckSimModelName(string articleId, string simCardType, string SIMType)
        {
            string modelName = string.Empty;
            using (var proxy = new CatalogServiceProxy())
            {
                List<SimModels> simModels = new List<SimModels>();
                if (!string.IsNullOrEmpty(articleId))
                    simModels = proxy.GetModelName(Convert.ToInt32(articleId), simCardType);

                if (simModels.Count > 0)
                {
                    switch (SIMType.ToLower())
                    {
                        case "mism":

                            if (simModels.FirstOrDefault().SimTypeDescription.ToLower().Contains(SIMType.ToLower()))
                            {
                                modelName = simModels[0].SimTypeDescription;
                                Session["SimModelTypeId"] = simModels[0].ID;

                            }
                            break;
                        case "normal":
                            if (simModels.FirstOrDefault().SimTypeDescription.ToLower().Contains("mism") == false)
                            {
                                modelName = simModels[0].SimTypeDescription;
                                Session["SimModelTypeId"] = simModels[0].ID;
                            }
                            break;
						
                        default:
                            //default should be avoided, implemented for otherthan new regostration flows
                            modelName = simModels[0].SimTypeDescription;
                            Session["SimModelTypeId"] = simModels[0].ID;
                            break;
                    }
                }
            }
            return modelName;
        }

        [HttpPost]
        public ActionResult UpdateSIMData(int regID, string sim, string simCardType = "")
        {
            var result = "";
            try
            {
                if (Session["IsDealer"].ToBool() && !Roles.IsUserInRole("MREG_DSK"))
                {
                    Session["SimSerialNumber"] = sim;

                }
                if(regID.ToInt()>0)
                {
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationUpdate(new DAL.Models.Registration()
                        {
                            ID = regID,
                            SIMSerial = sim,
                            LastAccessID = Util.SessionAccess.UserName,
                            PenaltyAmount = -1
                        });
                        if (Session["SimModelTypeId"] != null)
                        {
                            proxy.RegistrationUpdate(new DAL.Models.Registration()
                            {
                                ID = regID,
                                SimModelId = ReferenceEquals(Session["SimModelTypeId"].ToString2(), null) ? 0 : Convert.ToInt32(Session["SimModelTypeId"].ToString2()),
                                LastAccessID = Util.SessionAccess.UserName,
                                PenaltyAmount = -1
                            });
                        }
                       
                    }
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

            return Json(result);
        }


        [HttpPost]
        public ActionResult UpdateMismSIMCardType(int regID, string sim, string MismSimCardType = "")
        {
            var result = "";
            try
            {
                //using (var proxy = new RegistrationServiceProxy())
                //{
                //    proxy.RegistrationUpdate(new DAL.Models.Registration()
                //    {
                //        ID = regID,
                //        SIMSerial = sim,
                //        LastAccessID = Util.SessionAccess.UserName,
                //        PenaltyAmount = -1
                //    });
                //    if (Session["SimModelTypeId"] != null)
                //    {
                //        proxy.RegistrationUpdate(new DAL.Models.Registration()
                //        {
                //            ID = regID,
                //            SimModelId = ReferenceEquals(Session["SimModelTypeId"].ToString2(), null) ? 0 : Convert.ToInt32(Session["SimModelTypeId"].ToString2()),
                //            LastAccessID = Util.SessionAccess.UserName,
                //            PenaltyAmount = -1
                //        });
                //    }
                //    if (!string.IsNullOrEmpty(MismSimCardType) && regID > 0)
                //    {
                //        proxy.UpdateMismSimCardType(regID, MismSimCardType);
                //        proxy.UpdateSecondarySimSerial(regID, sim);
                //    }
                //}
                using (var proxy = new RegistrationServiceProxy())
                {
                    proxy.RegistrationUpdateMISM(new DAL.Models.RegistrationSec()
                    {
                        RegistrationID = regID,
                        SIMSerial = sim,
                        LastAccessID = Util.SessionAccess.UserName,
                        PenaltyAmount = -1
                    });
                    if (Session["SimModelTypeId"] != null)
                    {
                        proxy.RegistrationUpdateMISM(new DAL.Models.RegistrationSec()
                        {
                            RegistrationID = regID,
                            SimModelId = ReferenceEquals(Session["SimModelTypeId"].ToString2(), null) ? 0 : Convert.ToInt32(Session["SimModelTypeId"].ToString2()),
                            LastAccessID = Util.SessionAccess.UserName,
                            PenaltyAmount = -1
                        });
                    }
                    if (!string.IsNullOrEmpty(MismSimCardType) && regID > 0)
                    {
                        proxy.UpdateMismSimCardType(regID, MismSimCardType);
                        proxy.UpdateSecondarySimSerial(regID, sim);
                    }
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

            return Json(result);
        }

        private ComponentViewModel GetSelectedPackageInfo_smart(int modelId, int packageId, string kenanCode)
        {
            ComponentViewModel pkgResults = new ComponentViewModel();
            List<ModelPackageInfo> listRes = null;
            using (var proxy = new CatalogServiceProxy())
            {
                listRes = proxy.GetModelPackageInfo_smart(modelId, packageId, kenanCode, true);
            }

            List<NewPackages> listNewPackages = new List<NewPackages>();
            NewComponents objNewComponents;
            List<NewComponents> listNewComponents = new List<NewComponents>();

            NewPackages objNewPackagesPC = new NewPackages();
            NewPackages objNewPackagesMA = new NewPackages();
            NewPackages objNewPackagesEC = new NewPackages();
            NewPackages objNewPackagesDC = new NewPackages();
            NewPackages objPackagesOC = new NewPackages();
            NewPackages objNewPackagesRC = new NewPackages();
            NewPackages objNewPackagesCR = new NewPackages();
            List<NewPackages> ListNewPackagesOC = new List<NewPackages>();

            if (listRes != null)
            {
                foreach (var item in listRes)
                {
                    if (item.packageType == "PC")
                    {
                        objNewPackagesPC.NewPackageId = item.PackageId.ToString2();
                        objNewPackagesPC.NewPackageName = item.PackageName;
                        objNewPackagesPC.Type = item.packageType;
                        objNewComponents = new NewComponents();
                        objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        objNewComponents.NewComponentName = item.ComponentName;
                        objNewComponents.NewPackageId = item.PackageId.ToString2();
                        objNewComponents.NewPackageKenanCode = item.KenanCode;
                        objNewComponents.ReassignIsmandatory = item.IsMandatory;
                        //objNewComponents.ComponentGroupId = item.ComponentGroupId.ToString2();
                        objNewComponents.ComponentGroupName = item.ComponentGroupName;
                        listNewComponents.Add(objNewComponents);
                    }
                    if (item.packageType == "MA")
                    {
                        objNewPackagesMA.NewPackageId = item.PackageId.ToString2();
                        objNewPackagesMA.NewPackageName = item.PackageName;
                        objNewPackagesMA.Type = item.packageType;
                        objNewComponents = new NewComponents();
                        objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        objNewComponents.NewComponentName = item.ComponentName;
                        objNewComponents.NewPackageId = item.PackageId.ToString2();
                        objNewComponents.NewPackageKenanCode = item.KenanCode;
                        objNewComponents.ReassignIsmandatory = item.IsMandatory;
                        //objNewComponents.ComponentGroupId = item.ComponentGroupId.ToString2();
                        objNewComponents.ComponentGroupName = item.ComponentGroupName;
                        listNewComponents.Add(objNewComponents);

                    }
                    if (item.packageType == "EC")
                    {
                        objNewPackagesEC.NewPackageId = item.PackageId.ToString2();
                        objNewPackagesEC.NewPackageName = item.PackageName;
                        objNewPackagesEC.Type = item.packageType;
                        objNewComponents = new NewComponents();
                        objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        objNewComponents.NewComponentName = item.ComponentName;
                        objNewComponents.NewPackageId = item.PackageId.ToString2();
                        objNewComponents.NewPackageKenanCode = item.KenanCode;
                        objNewComponents.ReassignIsmandatory = item.IsMandatory;
                        //objNewComponents.ComponentGroupId = item.ComponentGroupId.ToString2();
                        objNewComponents.ComponentGroupName = item.ComponentGroupName;
                        listNewComponents.Add(objNewComponents);

                    }
                    if (item.packageType == "DC")
                    {
                        Session["DataComponentPkgKenancode"] = item.KenanCode;
                        Session["DataComponentPkgId"] = item.PackageId.ToString2();

                        objNewPackagesDC.NewPackageId = item.PackageId.ToString2();
                        objNewPackagesDC.NewPackageName = item.PackageName;
                        objNewPackagesDC.Type = item.packageType;
                        objNewComponents = new NewComponents();
                        objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        objNewComponents.NewComponentName = item.ComponentName;
                        objNewComponents.NewPackageId = item.PackageId.ToString2();
                        objNewComponents.NewPackageKenanCode = item.KenanCode;
                        objNewComponents.ReassignIsmandatory = item.IsMandatory;
                        objNewComponents.ComponentGroupId = item.ComponentGroupId.ToString2();
                        objNewComponents.ComponentGroupName = item.ComponentGroupName;
                        listNewComponents.Add(objNewComponents);
                    }

                    if (item.packageType == "OC")
                    {
                        if (string.IsNullOrEmpty(objPackagesOC.NewPackageId) || objPackagesOC.NewPackageId.ToInt() != item.PackageId)
                        {
                            objPackagesOC = new NewPackages();
                            objPackagesOC.NewPackageId = item.PackageId.ToString2();
                            objPackagesOC.NewPackageName = item.PackageName;
                            objPackagesOC.Type = item.packageType;
                            ListNewPackagesOC.Add(objPackagesOC);
                        }

                        objNewComponents = new NewComponents();
                        objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        objNewComponents.NewComponentName = item.ComponentName;
                        objNewComponents.NewPackageId = item.PackageId.ToString2();
                        objNewComponents.NewPackageKenanCode = item.KenanCode;
                        objNewComponents.ReassignIsmandatory = item.IsMandatory;
                        //objNewComponents.ComponentGroupId = item.ComponentGroupId.ToString2();
                        objNewComponents.ComponentGroupName = item.ComponentGroupName;
                        listNewComponents.Add(objNewComponents);

                        //objNewComponents = new NewComponents();
                        //objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        //objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        //objNewComponents.NewComponentName = item.ComponentName;
                        //objNewComponents.NewPackageId = item.PackageId.ToString2();
                        //objNewComponents.NewPackageKenanCode = item.KenanCode;
                        //if (mandatoryKenanCodes.Contains(item.ComponentKenanCode.ToString2()))
                        //    objNewComponents.ReassignIsmandatory = item.IsMandatory;
                        //else
                        //    objNewComponents.IsChecked = item.IsMandatory;
                        //listNewComponents.Add(objNewComponents);


                    }
                    if (item.packageType == "RC")
                    {
                        objNewPackagesRC.NewPackageId = item.PackageId.ToString2();
                        objNewPackagesRC.NewPackageName = item.PackageName;
                        objNewPackagesRC.Type = item.packageType;
                        objNewComponents = new NewComponents();
                        objNewComponents.NewComponentId = item.ComponentId.ToString2();
                        objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                        objNewComponents.NewComponentName = item.ComponentName;
                        objNewComponents.NewPackageId = item.PackageId.ToString2();
                        objNewComponents.NewPackageKenanCode = item.KenanCode;
                        objNewComponents.ReassignIsmandatory = item.IsMandatory;
                        objNewComponents.Type = item.packageType;
                        //objNewComponents.ComponentGroupId = item.ComponentGroupId.ToString2();
                        objNewComponents.ComponentGroupName = item.ComponentGroupName;
                        listNewComponents.Add(objNewComponents);
                    }
                    if (item.packageType == "CR")
                    {
                        objNewPackagesCR.NewPackageId = item.PackageId.ToString2();
                        objNewPackagesCR.NewPackageName = item.PackageName;
                        objNewPackagesCR.Type = item.packageType;
                        if (!item.isCRP)
                        {
                            objNewComponents = new NewComponents();
                            objNewComponents.NewComponentId = item.ComponentId.ToString2();
                            objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                            objNewComponents.NewComponentName = item.ComponentName;
                            objNewComponents.NewPackageId = item.PackageId.ToString2();
                            objNewComponents.NewPackageKenanCode = item.KenanCode;
                            objNewComponents.ReassignIsmandatory = item.IsMandatory;
                            objNewComponents.Type = item.packageType;
                            objNewComponents.ComponentGroupId = item.ComponentGroupId;

                            listNewComponents.Add(objNewComponents);
                        }
                        else
                        {
                            if (Session["Package"] != null)
                            {
                                IList<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel> oldCompPackage = (IList<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel>)Session["Package"];
                                foreach (var pkg in oldCompPackage)
                                {
                                    foreach (var comp in pkg.compList)
                                    {
                                        //checking MI bundle components
                                        if ((comp.componentId == "45310" || comp.componentId == "45308"))
                                        {
                                            if (comp.componentId == item.ComponentKenanCode.ToString())
                                            {
                                                objNewComponents = new NewComponents();
                                                objNewComponents.NewComponentId = item.ComponentId.ToString2();
                                                objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
                                                objNewComponents.NewComponentName = item.ComponentName;
                                                objNewComponents.NewPackageId = item.PackageId.ToString2();
                                                objNewComponents.NewPackageKenanCode = item.KenanCode;
                                                objNewComponents.ReassignIsmandatory = item.IsMandatory;
                                                objNewComponents.IsChecked = true;
                                                objNewComponents.Type = item.packageType;
                                                objNewComponents.ComponentGroupId = item.ComponentGroupId;
                                                listNewComponents.Add(objNewComponents);
                                            }

                                        }
                                    }
                                }
                            }

                        }
                    }
                }

                listNewPackages.Add(objNewPackagesPC);
                listNewPackages.Add(objNewPackagesMA);
                listNewPackages.Add(objNewPackagesDC);
                listNewPackages.Add(objNewPackagesEC);
                listNewPackages.Add(objNewPackagesRC);
                listNewPackages.Add(objNewPackagesCR);
                if (ListNewPackagesOC != null && ListNewPackagesOC.Count > 0)
                {
                    foreach (var item in ListNewPackagesOC)
                    {
                        listNewPackages.Add(item);
                    }
                }

                pkgResults.NewComponentsList = listNewComponents;
                pkgResults.NewPackagesList = listNewPackages;
            }

            return pkgResults;

        }
    }
}
