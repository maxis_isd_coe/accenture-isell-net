﻿using System.Web.Mvc;

namespace Online.Registration.Web.Areas.Storekeeper
{
    public class StorekeeperAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Storekeeper";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Storekeeper_default",
                "Storekeeper/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
