﻿using System.Web.Mvc;

namespace Online.Registration.Web.Areas.Storekeeper
{
    public class ServiceRequestAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ServiceRequest";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ServiceRequest_default",
                "ServiceRequest/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new []{ "Online.Registration.Web.Areas.ServiceRequest.Controllers" }
            );
        }
    }
}
