﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.ViewModels.Common;
using Online.Registration.Web.CustomExtension;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable()]
    public class PrepaidToPostpaidPrintOrderVM : SRPrintOrderWithCustVM
    {
        #region Service Information
        [Required]
        [StringLength(10)]
        [Display(Name = "Postpaid Account No")]
        public override string NewAccountNo { get; set; }
        public string Service_PostpaidSIMSerialNumber { get; set; }
        public string Service_PostpaidPlan { get; set; }
        #endregion
    }
}