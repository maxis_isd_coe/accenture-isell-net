﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
//-----------------------------------------------------------------------------------
//    Developer   : Ashley Ow
//    File Name   : PostpaidToPrepaidSmVM.cshtml
//    Purpose     : View Model for Postpaid To Prepaid Summary Service Request
//    Version Control:
//    Version     Date        Change Made
//    1.0         11 Oct 2015  View Model created
//-----------------------------------------------------------------------------------

    [Serializable()]
    public class PostpaidToPrepaidNewOrderSmVM : SRSummaryVM
    {
        public override string HeaderCustName
        {
            get
            {
                return NewOrder.CustName;
            }
        }

        public new PostpaidToPrepaidNewOrderVM NewOrder
        {
            get
            {
                return (PostpaidToPrepaidNewOrderVM)base.NewOrder;
            }
            set
            {
                base.NewOrder = value;
            }
        }
    }
}