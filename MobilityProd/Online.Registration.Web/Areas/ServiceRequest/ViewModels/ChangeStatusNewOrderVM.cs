﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Helper;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.DAL.Models.ServiceRequest.Enums;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    //-----------------------------------------------------------------------------------
    //    Developer   : Ashley Ow
    //    File Name   : ChangeStatusNewOrderVM
    //    Purpose     : View Model for Change Status Service Request
    //    Version Control:
    //    Version     Date        Change Made
    //    1.0         11 Oct 2015  View Model created
    //-----------------------------------------------------------------------------------
    [Serializable()]
    public class ChangeStatusNewOrderVM : SRNewOrderWthCustVM
    {
        #region "Basic Input"
        public int ChangeStatusTypeId { get; set; }

        public int ContractPendingMonths { get; set; }

        public decimal EarlyTerminationFee { get; set; }

        [UploadDocRequireIf("BiometricPass", false, ErrorMessage = "Biometric has failed, please upload ID documents")]
        [UploadDocRequireIf("OutstandingCheckPass", false, "Justification", ErrorMessage = "Outstanding check failed, justification require")]
        public UploadDocVM UploadDoc { get; set; }
        #endregion

        public ChangeStatusNewOrderVM()
        {
            ChangeStatusTypeId = (int)ChangeStatusTypes.Suspension;
            //form configuration
            AllowPrint = false;
            RequireSignature = false;
        }

        protected override void AddRule()
        {
            base.AddRule();
            //Only customer with valid msisdn can proceed
            AddEligibleRule("Customer do not have any valid MSISDN to proceed.", n => n.MSISDNList != null && n.MSISDNList.Count() > 0);
        }

        internal string GetPenaltyWarning()
        {
            return string.Format(
                "The account has a contract with {0} months remaining with an early termination fee of RM {1}",
                ContractPendingMonths,
                EarlyTerminationFee.ToString("F"));
        }
    }
}