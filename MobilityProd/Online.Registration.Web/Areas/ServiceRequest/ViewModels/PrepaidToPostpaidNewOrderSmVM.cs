﻿using Online.Registration.DAL.Interfaces;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.ServiceProxy;
using Online.Registration.Web.SRService;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    //-----------------------------------------------------------------------------------
    //    Developer   : Ashley Ow
    //    File Name   : PrepaidToPostpaidNewOrderSmVM.cshtml
    //    Purpose     : View Model for Prepauid To Postpaid Summary Service Request
    //    Version Control:
    //    Version     Date        Change Made
    //    1.0         11 Oct 2015  View Model created
    //-----------------------------------------------------------------------------------

    [Serializable()]
    public class PrepaidToPostpaidNewOrderSmVM : SRSummaryVM
    {
        public override string HeaderCustName
        {
            get
            {
                return NewOrder.CustName;
            }
        }

        public new PrepaidToPostpaidNewOrderVM NewOrder
        {
            get
            {
                return (PrepaidToPostpaidNewOrderVM)base.NewOrder;
            }
            set
            {
                base.NewOrder = value;
            }
        }

        public string PostplanName { 
            get{
                return NewOrder.PostpaidPlanId.ToPostpaidPlanName();
            }
        }
    }
}