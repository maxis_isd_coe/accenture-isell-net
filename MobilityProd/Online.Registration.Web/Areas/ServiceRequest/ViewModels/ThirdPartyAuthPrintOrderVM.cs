﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.ViewModels.Common;
using Online.Registration.Web.CustomExtension;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable()]
    public class ThirdPartyAuthPrintOrderVM : SRPrintOrderWithCustVM
    {
        #region Third Party Information
        public string ThirdParty_Name { get; set; }
        public int ThirdParty_IDTypeId { get; set; }
        public string ThirdParty_IDCardNumber { get; set; }
        public int ThirdParty_AuthorizationTypeId { get; set; }
        public bool ThirdParty_BiometricPass { get; internal set; }
        #endregion
    }
}