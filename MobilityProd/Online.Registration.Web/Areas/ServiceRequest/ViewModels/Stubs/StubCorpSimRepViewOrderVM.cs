﻿using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Stubs
{
    [Serializable] 
    public class StubCorpSimRepViewOrderVM:CorpSimRepViewOrderVM
    {
        public StubCorpSimRepViewOrderVM()
        {
            PicName = "LIM HEE GUAN";
            Status = SRStatus.New;
            Justification = "Customer fail biometric check";
            Remarks = "Done in Kenan, Approved by Zahid"; 
            QueueNo = "1058";
            OrderId = "SR100262";
            PicName = "Mr. LIM HEE GUAN";
            CompName = "ABC Sn Bhd";
            IdType = 1;
            IdNo = "590117135105";
            BrnNum = "300900-W";
            Msisdn = "60120009999"; 
            NewAccountNo = "17009100";
            NewSimSerial = "89600000112000111";
            LockBy = "Ccklcc1";
            TermAndCondition = new TermAndConditionVMBase(){
                Agreed=true
            };
            Signature = new SignatureVM(){
                SignDate=DateTime.Now,
                SignSvg = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMTQ2IiBoZWlnaHQ9IjUxIj48cGF0aCBmaWxsPSJub25lIiBzdHJva2U9IiMwMDAwMDAiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIiBkPSJNIDg1IDEwIGMgLTAuMDcgLTAuMDIgLTIuNyAtMSAtNCAtMSBjIC00LjQyIDAgLTEwIC0wLjA5IC0xNCAxIGMgLTIuNjkgMC43MyAtNS44OCAzLjA0IC04IDUgYyAtMS45NCAxLjc5IC0zLjYxIDQuNTcgLTUgNyBjIC0xLjIzIDIuMTYgLTIuNTEgNC43MyAtMyA3IGMgLTAuNDUgMi4wOSAtMS4wNCA1LjUyIDAgNyBjIDIuODMgNC4wNCA5LjUxIDEwLjAxIDE0IDEzIGMgMS42NiAxLjExIDQuNjQgMC45IDcgMSBjIDUuNjggMC4yNCAxMS40IDAuNTYgMTcgMCBjIDcuNjggLTAuNzcgMTUuNjcgLTEuOTggMjMgLTQgYyA1Ljc3IC0xLjU5IDExLjQ2IC00LjMyIDE3IC03IGMgNC4xOSAtMi4wMiA4LjU4IC00LjQ5IDEyIC03IGMgMS4yMyAtMC45IDIuNDUgLTIuNjIgMyAtNCBjIDAuNjcgLTEuNjggMS4yOCAtNC4zMiAxIC02IGMgLTAuMzEgLTEuODUgLTEuNzQgLTQuNDIgLTMgLTYgYyAtMS4yMSAtMS41MSAtMy4xOCAtMy4wNSAtNSAtNCBjIC00Ljk2IC0yLjYgLTEwLjU5IC01Ljc5IC0xNiAtNyBjIC0xMC4zIC0yLjMxIC0yMS44MyAtMy40MyAtMzMgLTQgYyAtMTUuMTMgLTAuNzggLTMwLjE2IC0wLjUzIC00NSAwIGMgLTMuNjkgMC4xMyAtNy42MiAwLjczIC0xMSAyIGMgLTkuNjEgMy42MSAtMjEuMzggOC44MiAtMjkgMTMgYyAtMS4wNyAwLjU5IC0yLjI3IDMuMDEgLTIgNCBsIDUgNyIvPjwvc3ZnPg=="
            };
        }
    }
}