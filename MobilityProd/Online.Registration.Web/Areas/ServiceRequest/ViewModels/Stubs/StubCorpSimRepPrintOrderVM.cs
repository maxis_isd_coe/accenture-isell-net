﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Stubs
{
    [Serializable]
    public class StubCorpSimRepPrintOrderVM:CorpSimRepPrintOrderVM
    {
        public StubCorpSimRepPrintOrderVM()
        {
            PicName = "Encik YUSOFF BIN OTHMAN";
            IdTypeId = 1;
            IdNo = "520517075267";
            CompName = "ABC Sdn Bhd";
            CompBrn = "300900-W";
            BiometricPass = false;
            Msisdn = "60120009999";
            AccNo = "17009100";
            NewSimSerial = "89600000112000111";
            BranchCode = "CNTRL.00010";
            BranchName = "Maxis Centre KLCC i-Centre";
            StaffCode = "ccklcc1";
            SignDt = new DateTime(2015,10,20,6,42,51);
        }
    }
}