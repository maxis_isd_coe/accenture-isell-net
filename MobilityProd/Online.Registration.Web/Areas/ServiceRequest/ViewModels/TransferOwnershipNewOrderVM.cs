﻿//-----------------------------------------------------------------------------------
//    Developer   : Ashley Ow
//    File Name   : TransferOwnershipNewOrderVM.cshtml
//    Purpose     : View Model for Transfer Ownership Service Request
//    Version Control:
//    Version     Date        Change Made
//    1.0         11 Oct 2015  View Model created
//-----------------------------------------------------------------------------------

using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Helper;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.DAL.Helpers;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable()]
    public class TransferOwnershipNewOrderVM : SRNewOrderWthCustVM, IExistingCustOnlyOrder
    {
        public bool IsTransferNewAcc { get; set; }

        #region Existing Acc Info
        [CustomRequiredAttribute(false, "IsTransferNewAcc")]
        [Display(Name = "New Owner Name")]
        public string Exist_CustName { get; set; }

        [CustomRequiredAttribute(false, "IsTransferNewAcc")]
        [Display(Name = "New Owner Identification Type")]
        public int Exist_CustIDTypeId { get; set; }

        [CustomRequiredAttribute(false, "IsTransferNewAcc")]
        [StringLength(14)]
        [Display(Name = "New Owner Identification Number")]
        [RegularExpression(@"^[a-zA-Z0-9]*$")]
        [NotEqualTo("IdNum", ErrorMessage = "Customer and Transferee’s Identification Number cannot be the same.")]
        [ValidIdNum("Exist_CustIDTypeId")]
        public string Exist_CustIDNumber { get; set; }

        [CustomRequiredAttribute(false, "IsTransferNewAcc")]
        [StringLength(15)]
        [Display(Name = "New Owner MSISDN")]
        public string Exist_CustMSISDN { get; set; }

        public string Exist_CustSalutation { get; set; }
        public DateTime? Exist_CustDOB { get; set; }
        public string Exist_CustGender { get; set; }
        public int Exist_CustLanguageID { get; set; }
        public int Exist_CustNationalityID { get; set; }
        public int Exist_CustRaceID { get; set; }
        public string Exist_CustEmail { get; set; }
        public string Exist_CustContactNumber { get; set; }

        [NotEqualToIf("AccNum", "IsTransferNewAcc", false, ErrorMessage = " Customer and Transferee’s Identification Number cannot be the same.")]
        [Display(Name="New Owner Account No")]
        public string Exist_CustAccountNumber { get; set; }
        public string Exist_CustAlternateNumber { get; set; }
        public string Exist_CustAddressLine1 { get; set; }
        public string Exist_CustAddressLine2 { get; set; }
        public string Exist_CustAddressLine3 { get; set; }
        public string Exist_CustPostCode { get; set; }
        public string Exist_CustCity { get; set; }
        public string Exist_CustState { get; set; }
        public int Exist_CustStateID { get; set; }

        [MustBeTrueIf("IsTransferNewAcc", false, ErrorMessage = "Transferee's Identification Number does not have valid Kenan account for transfer")]
        public bool ValidMaxisCustomer { get; set; }
        #endregion

        #region New Acc Info
        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Name")]
        [StringLength(100, ErrorMessage = "Name is too long")]
        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Name is not valid")]
        public string New_CustName { get; set; }

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Identification Type")]
        public int New_CustIDTypeId { get; set; }

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Identification Number")]
        [RegularExpression(@"^[a-zA-Z0-9]*$")]
        [NotEqualTo("IdNum", ErrorMessage = "Customer and Transferee’s Identification Number cannot be the same.")]
        [ValidIdNum("New_CustIDTypeId")]
        public string New_CustIDNumber { get; set; }

        [NotEqualToIf("AccNum", "IsTransferNewAcc", false, ErrorMessage = " Customer and Transferee’s Identification Number cannot be the same.")]
        [Display(Name = "Account Number")]
        public string New_CustAccountNumber { get; set; }

        [Display(Name = "Salutation")]
        public string New_CustSalutation { get; set; }

        [Display(Name = "Date Of Birth")]
        public DateTime? New_CustDOB { get; set; }

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Year")]
        public string New_DOBYear{get;set;}

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Month")]
        public string New_DOBMonth{get;set;}

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Day")]
        public string New_DOBDay {get;set;}

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Gender")]
        public string New_CustGender { get; set; }

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Preferred Language")]
        public int? New_CustLanguageID { get; set; }


        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Nationality")]
        public int? New_CustNationalityID { get; set; }

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Race")]
        public int? New_CustRaceID { get; set; }

        [Display(Name = "Email")]
        [StringLength(100, ErrorMessage ="Email address is too long.")]
        [RegularExpression("^[a-z0-9_\\+-]+(\\.[a-z0-9_\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Email is not valid")]
        public string New_CustEmail { get; set; }

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [CustomStringLength(11, 11, "{0} is too long", "{0} must be 11 characters")]
        [RegularExpression(@"^\d+$", ErrorMessage = "{0} not a valid format")]
        [Display(Name = "Contact Number")]
        public string New_CustContactNumber { get; set; }

        [Display(Name = "Alternate Contact Number")]
        public string New_CustAlternateNumber { get; set; }

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Primary Address")]
        public string New_CustAddressLine1 { get; set; }
        public string New_CustAddressLine2 { get; set; }
        public string New_CustAddressLine3 { get; set; }

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "Postcode")]
        [StringLength(5, MinimumLength = 5, ErrorMessage= "Postcode is too long")]
        public string New_CustPostCode { get; set; }

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "City")]
        public string New_CustCity { get; set; }

        [CustomRequiredAttribute(true, "IsTransferNewAcc")]
        [Display(Name = "State")]
        public string New_CustState { get; set; }

        [Display(Name = "State ID")]
        public int? New_CustStateID { get; set; }
        #endregion

        public Dictionary<BreType, string> TransfereeBreErrors { get; set; }

        public bool TransfereeBiometricPass { get; set; }

        #region UploadDocRequireIfParam
        public UploadDocRequireIfParam[] NewOwnerUploadDocRequireIfJustificationParams { get; set; }
        #endregion

        [UploadDocRequireIf("BiometricPass", false, ErrorMessage = "1=>Biometric has failed, please upload ID documents")]
        [UploadDocRequireIf("UploadDocRequireIfJustificationParams", "Justification", ErrorMessage = "2=>{0}, justification require")]
        [UploadDocRequireIf("NewOwnerBiometricPass", false, "TransfereeFrontIdFile", "TransfereeBackIdFile", ErrorMessage = "3=>Biometric has failed, please upload ID documents For New Owner")]
        [UploadDocRequireIf("NewOwnerUploadDocRequireIfJustificationParams", "Justification", ErrorMessage = "4=>Please Enter Justification For New Owner - {0}")]
        public TransferOwnershipUploadDocVM UploadDoc { get; set; }

        public string DisplayTransferName
        {
            get
            {
                if (Exist_CustName != null)
                {
                    return Exist_CustName;
                }
                else
                {
                    return New_CustName;
                }
            }
        }

        public TransferOwnershipNewOrderVM()
        {
            TransfereeMsisdnList = new List<string>();
            TransfereeBreErrors = new Dictionary<BreType, string>();
            NewCustBreErrors = new Dictionary<BreType, string>();
            //define bre validation condition
            UploadDocRequireIfJustificationParams = new[] {
                new UploadDocRequireIfParam(this.Name(n=>n.OutstandingCheckPass), false, "Outstanding check failed"),
                new UploadDocRequireIfParam(this.Name(n=>n.WriteoffCheckPass), false, "Write off check failed"),
            };
            NewOwnerUploadDocRequireIfJustificationParams = new[] {
                new UploadDocRequireIfParam(this.Name(n=>n.NewOwnerOutstandingCheckPass), false, "Outstanding Check"),
                new UploadDocRequireIfParam(this.Name(n=>n.NewOwnerTtlLineCheckPass), false, "Total Line Check"),
                new UploadDocRequireIfParam(this.Name(n=>n.NewOwnerTtlContractCheckPass), false, "Total Contract Check"),
                new UploadDocRequireIfParam(this.Name(n=>n.NewOwnerWriteoffCheckPass), false, "Write Off Check"),
            };
        }

        /// <summary>
        /// To store the service info map for transferee
        /// </summary>
        public Dictionary<string, ServiceInfoVM> TransfereeMsisdnServiceInfoMap { get; set; }

        public List<string> TransfereeMsisdnList { get; set; }

        public Dictionary<BreType, string> NewCustBreErrors { get; set; }
        
        /// <summary>
        /// Indicate is new cust is exist cust
        /// </summary>
        public bool NewCustIsExistCust { get; set; }
        /// <summary>
        /// Flag to know is already perform customer search for existing acc
        /// </summary>
        [MustBeTrueIf("IsTransferNewAcc", false, ErrorMessage = "Please perform customer search for Transferee.")]
        public bool ExistQueryCustomer { get; set; }
        /// <summary>
        /// Flag to know is already perform customer search for new account
        /// </summary>
        [MustBeTrueIf("IsTransferNewAcc", true, ErrorMessage = "Please perform customer search for Transferee.")]
        public bool NewQueryCustomer { get; set; }

        public bool NewOwnerTtlLineCheckPass { get; set; }

        public bool NewOwnerTtlContractCheckPass { get; set; }

        public bool NewOwnerWriteoffCheckPass { get; set; }

        public bool NewOwnerOutstandingCheckPass { get; set; }

        public bool NewOwnerBiometricPass { get; set; }
    }
}