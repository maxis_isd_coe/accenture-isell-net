﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Helper;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    //-----------------------------------------------------------------------------------
    //    Developer   : Ashley Ow
    //    File Name   : ChangeMSISDNNewOrderVM
    //    Purpose     : View Model for Change MSISDN Service Request
    //    Version Control:
    //    Version     Date        Change Made
    //    1.0         11 Jan 2015  View Model created
    //-----------------------------------------------------------------------------------
    [Serializable()]
    public class ChangeMSISDNNewOrderVM : SRNewOrderWthCustVM
    {
        #region "Basic Input"

        [Required]
        [Display(Name = "New MSISDN")]
        [CustomStringLength(15, 11, "{0} is too long.", "{0} must be 11 characters.")]
        [RegularExpression(@"^-?\d+$", ErrorMessage = "Invalid {0} format.")]
        public string NewMSISDN { get; set; }

        [UploadDocRequireIf("BiometricPass", false, ErrorMessage = "Biometric has failed, please upload ID documents")]
        [UploadDocRequireIf("OutstandingCheckPass", false, "Justification", ErrorMessage = "Outstanding check failed, justification require")]
        public UploadDocVM UploadDoc { get; set; }
        #endregion

        /// <summary>
        /// Allow edit only eligible for Name for Personal data, other should get from Kenan(existing) or ic(new)
        /// </summary>
        public bool AllowEditPersonalInfo { get; internal set; }

        public ChangeMSISDNNewOrderVM()
        {
            //form configuration
            AllowPrint = false;
            RequireSignature = false;
        }

        protected override void AddRule()
        {
            base.AddRule();
            //Only customer with valid msisdn can proceed
            AddEligibleRule("Customer do not have any valid MSISDN to proceed.", n => n.MSISDNList != null && n.MSISDNList.Count() > 0);
        }
    }
}