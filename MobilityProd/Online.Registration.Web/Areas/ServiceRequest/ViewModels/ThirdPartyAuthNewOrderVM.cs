﻿//-----------------------------------------------------------------------------------
//    Developer   : Ashley Ow
//    File Name   : ThirdPartyAuthNewOrderVM.cs
//    Purpose     : Model for 3rd Party Authorization Service Request
//    Version Control:
//    Version     Date         Change Made
//    1.0         10 Oct 2015  Model created
//    -----------------------------------------------------------------------------------

using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Helper;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.DAL.Helpers;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable()]
    public class ThirdPartyAuthNewOrderVM : SRNewOrderWthCustVM, IExistingCustOnlyOrder
    {
        #region "Basic Input"
        [Required]
        [MaxLength(100)]
        [Display(Name="Third Party Name")]
        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Third Party Name is not valid")]
        public string ThirdPartyName { get; set; }

        [Required]
        [Display(Name="Third Party ID Type")]
        [Range(1,int.MaxValue,ErrorMessage="{0} require.")]
        public string ThirdPartyIDType { get; set; }

        [Required]
        [Display(Name = "Identification Number")]
        [RegularExpression(@"^[a-zA-Z0-9]*$", ErrorMessage = "Identification Number not a valid format")]
        [NotEqualTo("IdNum",ErrorMessage = "Third party id cannot be the same as customer id.")]
        [NotEqualTo("Msisdn", ErrorMessage = "Third party MSISDN cannot be the same as customer MSISDN.")]
        [ValidIdNum("ThirdPartyIDType")]
        public string ThirdPartyIDNumber { get; set; }

        [Required]
        [Display(Name = "Account Number")]
        public override string AccNum { get; set; }

        [Required]
        [Display(Name = "Third Party Authorization Type")]
        public int ThirdPartyAuthTypeId { get; set; }

        public List<string> ThirdPartyAuthTypeList { get; set; }

        #endregion

        public bool ThirdPartyInfoReadOnly { get; set; }

        public bool ThirdPartyBiometricPass { get; set; }

        /// <summary>
        /// Save the sate if already perfrom customer search
        /// </summary>
        [MustBeTrue(ErrorMessage="Please perform customer search for 3rd Party")]
        public bool QueryCustomer {get;set;}

        [UploadDocRequireIf("BiometricPass", false, ErrorMessage = "Biometric has failed, please upload ID documents")]
        public ThirdPartyAuthUploadDocVM UploadDoc { get; set; }

        /// <summary>
        /// To store bre error perform on Third party
        /// </summary>
        public Dictionary<BreType, string> ThirdPartyBreErrors { get; set; }

        public ThirdPartyAuthNewOrderVM()
        {
            ThirdPartyBreErrors = new Dictionary<BreType, string>();
        }

        protected override void AddRule()
        {
            base.AddRule();
            //Only customer with valid postpoid can proceed
            AddEligibleRule("Customer do not have any valid msisdn.", n => n.MSISDNList != null && n.MSISDNList.Count() > 0);
        }
    }
}