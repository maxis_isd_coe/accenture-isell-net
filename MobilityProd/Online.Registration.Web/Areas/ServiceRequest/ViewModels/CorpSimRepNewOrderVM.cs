﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Helper;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    //-----------------------------------------------------------------------------------
    //    Developer   : Jazz Tong
    //    File Name   : CorpSimRepNewOrderVM
    //    Purpose     : View Model for Corp sim replacement Service Request
    //    Version Control:
    //    Version     Date        Change Made
    //    1.0         11 Oct 2015  View Model created
    //-----------------------------------------------------------------------------------
    [Serializable()]
    public class CorpSimRepNewOrderVM : SRNewOrderWthCustVM
    {
        #region "Basic Input"


        [Required]
        [StringLength(100, ErrorMessage="Company Name is too long")]
        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Company Name is not valid")]
        [Display(Name = "Company Name")]
        public string CompName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage="Company BRN is too long")]
        [RegularExpression(@"^[A-Za-z\d_-]+$", ErrorMessage = "Invalid {0} format.")]
        [Display(Name = "BRN Number")]
        public string BrnNum { get; set; }

        [Required]
        [Display(Name = "New SIM Type")]
        public string NewSimTypeCode { get; set; }

        [Required]
        [CustomStringLength(19, 19, "{0} is too long.", "{0} must be 19 characters.")]
        [RegularExpression(@"^-?\d+$", ErrorMessage = "Invalid {0} format.")]
        [ValidSimSerial("NewSimTypeCode","New SIM Type and {0} not match or {0} is not a valid sim serial in inventory")]
        [Display(Name = "New SIM Serial Number")]
        public string NewSimSerial { get; set; }

        [UploadDocRequireIf("BiometricPass", false, ErrorMessage = "Biometric has failed, please upload ID documents")]
        public UploadDocVM UploadDoc { get; set; }
        #endregion

        /// <summary>
        /// Allow edit only eligible for Name for Personal data, other should get from Kenan(existing) or ic(new)
        /// </summary>
        public bool AllowEditPersonalInfo { get; internal set; }
    }
}