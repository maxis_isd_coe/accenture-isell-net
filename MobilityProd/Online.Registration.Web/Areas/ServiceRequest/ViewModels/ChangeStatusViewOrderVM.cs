﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using System.ComponentModel.DataAnnotations;
using Online.Registration.Web.CustomDataAnnotation;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable]
    public class ChangeStatusViewOrderVM:SRViewOrderWithCustVM
    {
        public int ChangeStatusTypeId { get; set; }
        public ChangeStatusViewOrderVM()
        {
            SaveContract = false;
        }
    }
}