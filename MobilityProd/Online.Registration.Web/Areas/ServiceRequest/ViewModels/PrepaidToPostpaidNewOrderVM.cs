﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Helper;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.DAL.Helpers;
//-----------------------------------------------------------------------------------
//    Developer   : Ashley Ow
//    File Name   : PrepaidToPostpaidNewOrderVM.cshtml
//    Purpose     : View Model for Prepaid To Postpaid Service Request
//    Version Control:
//    Version     Date        Change Made
//    1.0         11 Oct 2015  View Model created
//-----------------------------------------------------------------------------------

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable()]
    public class PrepaidToPostpaidNewOrderVM : SRNewOrderWthCustVM, IExistingCustOnlyOrder
    {
        #region "Basic Input"

        public bool HasPostpaid { get; set; }

        [Required]
        [Display(Name = "Prepaid Account Number")]
        public override string AccNum { get; set; }

        [Display(Name = "Postpaid Account Number")]
        public string PostpaidAccNumber { get; set; }

        [Required]
        [CustomStringLength(19, 19, "Postpaid SIM Serial is too long.", "Postpaid SIM Serial must be 19 characters.")]
        [Display(Name = "Postpaid SIM Serial Number")]
        [RegularExpression(@"^[0-9]{0,19}$", ErrorMessage = "Invalid Postpaid SIM Serial format.")]
        public string PostpaidSIMNumber { get; set; }

        [Display(Name = "Postpaid Plan")]
        public string PostpaidPlanId { get; set; }

        [Display(Name = "Prepaid Plan")]
        public string PrepaidPlan { get; set; }
        #endregion

        [UploadDocRequireIf("BiometricPass", false, ErrorMessage = "Biometric has failed, please upload ID documents")]
        [UploadDocRequireIf("UploadDocRequireIfJustificationParams", "Justification", ErrorMessage = "{0}, justification require")]
        public UploadDocVM UploadDoc { get; set; }

        protected override void AddRule()
        {
            base.AddRule();
            //Only customer with valid prepaid can proceed
            AddEligibleRule("Customer do not have any valid prepaid info.", n => n.MSISDNList != null && n.MSISDNList.Count() > 0);
        }

        public PrepaidToPostpaidNewOrderVM()
        {
            //Default setting
            PostpaidPlanId = StaticListVM.Instance.PostpaidPlanList.First().Key;
            PrepaidPlan = "Hotlink Plan";
            UploadDocRequireIfJustificationParams = new[] {
                new UploadDocRequireIfParam(this.Name(n=>n.DDMFCheckPass), false, "DDMF check failed"),
                new UploadDocRequireIfParam(this.Name(n=>n.TtlLineCheckPass), false, "Total line check failed"),
                new UploadDocRequireIfParam(this.Name(n=>n.TtlContractCheckPass), false, "Contract check failed"),
                new UploadDocRequireIfParam(this.Name(n=>n.OutstandingCheckPass), false, "Outstanding check failed"),
                new UploadDocRequireIfParam(this.Name(n=>n.WriteoffCheckPass), false, "Write off check failed"),
            };
        }
    }
}