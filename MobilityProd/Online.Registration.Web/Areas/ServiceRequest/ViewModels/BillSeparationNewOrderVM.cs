﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Helper;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    //-----------------------------------------------------------------------------------
    //    Developer   : Ashley Ow
    //    File Name   : BillSeparationNewOrderVM.cshtml
    //    Purpose     : View Model for Bill Separation Service Request
    //    Version Control:
    //    Version     Date        Change Made
    //    1.0         11 Oct 2015  View Model created
    //-----------------------------------------------------------------------------------
    [Serializable()]
    public class BillSeparationNewOrderVM : SRNewOrderWthCustVM, IExistingCustOnlyOrder
    {
        #region "Basic Input"        
        [Required]
        [Display(Name = "Current Plan")]
        public override string Plan { get; set; }

        public bool IsSplitNewAcc { get; set; }

        [CustomRequiredAttribute(false,"IsSplitNewAcc")]
        [Display(Name = "Existing Account MSISDN")]
        public string ExistMSISDN { get; set; }

        [NotEqualTo("AccNum", ErrorMessage = "Bill separation must be done to a different Kenan Account no")]
        [CustomRequiredAttribute(false, "IsSplitNewAcc")]
        [StringLength(15)]
        [Display(Name = "Existing Account Number")]
        [RegularExpression(@"^[0-9]{0,10}$", ErrorMessage = "Existing Account Number must be a number.")]
        public string ExistAccountNumber { get; set; }

        [CustomRequiredAttribute(true, "IsSplitNewAcc")]
        [Display(Name = "Billing Address")]
        public string NewAddrLine1 { get; set; }
        public string NewAddrLine2 { get; set; }
        public string NewAddrLine3 { get; set; }

        [CustomRequiredAttribute(true, "IsSplitNewAcc")]
        [Display(Name = "Postcode")]
        [StringLength(5, MinimumLength = 5, ErrorMessage = "Postcode is too long")]
        public string NewAddrPostCode { get; set; }

        [CustomRequiredAttribute(true, "IsSplitNewAcc")]
        [Display(Name = "City")]
        public string NewAddrCity { get; set; }

        [CustomRequiredAttribute(true, "IsSplitNewAcc")]
        [Display(Name = "State")]
        public string NewAddrState { 
            get{
                return _newAddrState;
            } set{
                _newAddrState = value;
                NewAddrStateID = _newAddrState.ToStateTypeValue();
            }
        }
        private string _newAddrState;

        [CustomRequiredAttribute(true, "IsSplitNewAcc")]
        [Display(Name = "StateID")]
        public int NewAddrStateID { get; set; }

        public List<string> ExistMSISDNList { get; set; }

        #endregion

        [UploadDocRequireIf("BiometricPass", false, ErrorMessage = "Biometric has failed, please upload ID documents")]
        [UploadDocRequireIf("OutstandingCheckPass", false,"Justification", ErrorMessage = "Outstanding check failed, justification require")]
        public UploadDocVM UploadDoc { get; set; }

        protected override void AddRule()
        {
            base.AddRule();
            //Only customer with valid postpoid can proceed
            AddEligibleRule("Customer do not have any valid MSISDN to proceed.", n => n.MSISDNList!=null && n.MSISDNList.Count() > 0);
        }

        /// <summary>
        /// Flag if customer only one account num
        /// </summary>
        public bool OnlyOneAccNum { get; set; }
    }
}