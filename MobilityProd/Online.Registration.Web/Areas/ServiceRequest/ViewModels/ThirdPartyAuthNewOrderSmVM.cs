﻿using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.ServiceProxy;
using Online.Registration.Web.SRService;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.Areas.ServiceRequest.Interface;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
//-----------------------------------------------------------------------------------
//    Developer   : Ashley Ow
//    File Name   : ThirdPartyAuthSmVM.cshtml
//    Purpose     : View Model for Transfer Ownership Summary Service Request
//    Version Control:
//    Version     Date         Change Made
//    1.0         10 Oct 2015  View Model created
//-----------------------------------------------------------------------------------

    [Serializable()]
    public class ThirdPartyAuthNewOrderSmVM : SRSummaryVM
    {
        public override string HeaderCustName
        {
            get
            {
                return NewOrder.CustName;
            }
        }

        public new ThirdPartyAuthNewOrderVM NewOrder
        {
            get
            {
                return (ThirdPartyAuthNewOrderVM)base.NewOrder;
            }
            set
            {
                base.NewOrder = value;
            }
        }

        public ThirdPartyAuthNewOrderSmVM(ThirdPartyAuthNewOrderVM newOrder)
        {
            this.NewOrder = newOrder;
        }

        public ThirdPartyAuthNewOrderSmVM()
        {
        }
    }
}