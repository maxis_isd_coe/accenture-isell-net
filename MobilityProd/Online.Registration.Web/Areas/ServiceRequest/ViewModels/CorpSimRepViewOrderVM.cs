﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using System.ComponentModel.DataAnnotations;
using Online.Registration.Web.CustomDataAnnotation;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable]
    public class CorpSimRepViewOrderVM:SRViewOrderVM
    {
        public string PicName { get; set; }

        public string CompName { get; set; }

        public string BrnNum { get; set; }

        public string Msisdn { get; set; }

        public string NewSimTypeCode { get; set; }

        public string NewSimSerial { get; set; }

        public int IdType { get; set; }

        public override string HeaderCustName
        {
            get{
                return PicName;//Maybe need title here
            }
        }

        [Display(Name = "New Account No")]
        public override string NewAccountNo { get; set; }

        public string IdNo { get; set; }
    }
}