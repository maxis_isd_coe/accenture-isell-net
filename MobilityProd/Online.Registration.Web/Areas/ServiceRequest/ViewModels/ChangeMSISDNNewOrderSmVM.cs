﻿using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.Factories;
using Online.Registration.Web.ServiceProxy;
using Online.Registration.Web.SRService;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common;
using Online.Registration.Web.Areas.ServiceRequest.Interface;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    //-----------------------------------------------------------------------------------
    //    Developer   : Ashley Ow
    //    File Name   : ChangeMSISDNNewOrderSmVM.cshtml
    //    Purpose     : View Model Summary for Change MSISDN Service Request
    //    Version Control:
    //    Version     Date        Change Made
    //    1.0         11 Jan 2015  View Model created
    //-----------------------------------------------------------------------------------
    [Serializable()]
    public class ChangeMSISDNNewOrderSmVM : SRSummaryVM
    {

        public override string HeaderCustName {
            get {
                return NewOrder.CustName;
            } 
        }

        public string IdTypeDisplay{
            get{
                return int.Parse(NewOrder.IdType).ToIdCardTypeDesc();
            }
        }

        public new ChangeMSISDNNewOrderVM NewOrder {
            get{
                return (ChangeMSISDNNewOrderVM)base.NewOrder;
            }
            set{
                base.NewOrder = value;
            }
        }

        public string Identication { 
            get{
                if (NewOrder == null) return "";
                var find = StaticListVM.Instance.IdTypList.Where(n => n.Value ==  NewOrder.IdType.ToString());
                var text = find.Count() > 0 ? find.First().Text : "";
                return string.Format("{0} {1}", text, NewOrder.IdNum);
            }
        }

        public ChangeMSISDNNewOrderSmVM(ChangeMSISDNNewOrderVM newOrder):base()
        {
            this.NewOrder = newOrder;
        }

        public ChangeMSISDNNewOrderSmVM()
        {
        }
    }
}