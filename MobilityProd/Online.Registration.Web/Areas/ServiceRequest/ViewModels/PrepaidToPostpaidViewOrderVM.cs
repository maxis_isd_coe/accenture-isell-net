﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.CustomDataAnnotation;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable]
    public class PrepaidToPostpaidViewOrderVM : SRViewOrderWithCustVM
    {
       
        #region Service Information
        [Display(Name = "Postpaid Account No")]
        public override string NewAccountNo { get; set; }
        public string Service_PostpaidSIMSerialNumber { get; set; }
        public string Service_PostpaidPlan { get; set; }
        #endregion

    }
}
