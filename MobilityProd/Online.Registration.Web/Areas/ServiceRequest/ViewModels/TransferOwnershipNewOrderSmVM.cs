﻿using Online.Registration.DAL.Interfaces;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.ServiceProxy;
using Online.Registration.Web.SRService;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    //-----------------------------------------------------------------------------------
    //    Developer   : Ashley Ow
    //    File Name   : TransferOwnershipSmVM.cshtml
    //    Purpose     : View Model for Transfer Ownership Summary Service Request
    //    Version Control:
    //    Version     Date        Change Made
    //    1.0         11 Oct 2015  View Model created
    //-----------------------------------------------------------------------------------

    [Serializable()]
    public class TransferOwnershipNewOrderSmVM : SRSummaryVM
    {
        public SignatureVM Signature_ForNewOwner { get; set; }

        public TransferOwnershipNewOrderSmVM()
        {
            TermAndCondition_ForNewOwner = new TermAndConditionVM();
            Signature_ForNewOwner = new SignatureVM();
            Signature_ForNewOwner = new SignatureVM();
        }

        public override string HeaderCustName
        {
            get
            {
                return NewOrder.CustName;
            }
        }

        public new TransferOwnershipNewOrderVM NewOrder
        {
            get
            {
                return (TransferOwnershipNewOrderVM)base.NewOrder;
            }
            set
            {
                base.NewOrder = value;
            }
        }

        #region New Acc Info
        public string New_CustName { get; set; }
        public int New_CustIDTypeId { get; set; }
        public string New_CustIDNumber { get; set; }
        public string New_CustAccountNumber { get; set; }
        public string New_CustSalutation { get; set; }
        public DateTime? New_CustDOB { get; set; }
        public string New_CustGender { get; set; }
        public int New_CustLanguageID { get; set; }
        public int New_CustNationalityID { get; set; }
        public int New_CustRaceID { get; set; }
        public string New_CustEmail { get; set; }
        public string New_CustContactNumber { get; set; }
        public string New_CustAlternateNumber { get; set; }
        public string New_CustAddressLine1 { get; set; }
        public string New_CustAddressLine2 { get; set; }
        public string New_CustAddressLine3 { get; set; }
        public string New_CustPostCode { get; set; }
        public string New_CustCity { get; set; }
        public string New_CustState { get; set; }
        public int New_CustStateID { get; set; }
        #endregion

        /// <summary>
        /// Consolidate data from existing acc or new owner info
        /// </summary>
        internal void ConsolidateNewOwner()
        {
            //reset plan and vas base on msisdn
            NewOrder.Plan = NewOrder.MsisdnServiceInfoMap[NewOrder.Msisdn].Plan;
            NewOrder.VasList = NewOrder.MsisdnServiceInfoMap[NewOrder.Msisdn].VasList;
            if(NewOrder.IsTransferNewAcc){
                CopyNewAccInfo(); 
            }else{
                CopyExistingAccInfo();
            }
        }

        private void CopyExistingAccInfo()
        {
            New_CustName =NewOrder.Exist_CustName;
            New_CustIDTypeId =NewOrder.Exist_CustIDTypeId;
            New_CustIDNumber=NewOrder.Exist_CustIDNumber;
            New_CustAccountNumber =NewOrder.Exist_CustAccountNumber;
            New_CustSalutation = NewOrder.Exist_CustSalutation;
            New_CustDOB = NewOrder.Exist_CustDOB;
            New_CustGender = NewOrder.Exist_CustGender;
            New_CustLanguageID = NewOrder.Exist_CustLanguageID;
            New_CustNationalityID = NewOrder.Exist_CustNationalityID;
            New_CustRaceID = NewOrder.Exist_CustRaceID;
            New_CustEmail = NewOrder.Exist_CustEmail;
            New_CustContactNumber = NewOrder.Exist_CustContactNumber;
            New_CustAlternateNumber = NewOrder.Exist_CustAlternateNumber;
            New_CustAddressLine1 = NewOrder.Exist_CustAddressLine1;
            New_CustAddressLine2 = NewOrder.Exist_CustAddressLine2;
            New_CustAddressLine3 = NewOrder.Exist_CustAddressLine3;
            New_CustPostCode = NewOrder.Exist_CustPostCode;
            New_CustCity = NewOrder.Exist_CustCity;
            New_CustState = NewOrder.Exist_CustState;
            New_CustStateID = NewOrder.Exist_CustStateID;
        }

        private void CopyNewAccInfo()
        {
            New_CustName = NewOrder.New_CustName;
            New_CustIDTypeId = NewOrder.New_CustIDTypeId;
            New_CustIDNumber = NewOrder.New_CustIDNumber;
            New_CustAccountNumber = NewOrder.New_CustAccountNumber;
            New_CustSalutation = NewOrder.New_CustSalutation;
            New_CustDOB = new DateTime(int.Parse(NewOrder.New_DOBYear),int.Parse(NewOrder.New_DOBMonth),int.Parse(NewOrder.New_DOBDay));
            New_CustGender = NewOrder.New_CustGender;
            New_CustLanguageID = NewOrder.New_CustLanguageID.HasValue?NewOrder.New_CustLanguageID.Value:0;
            New_CustNationalityID = NewOrder.New_CustNationalityID.HasValue?NewOrder.New_CustNationalityID.Value:0;
            New_CustRaceID = NewOrder.New_CustRaceID.HasValue?NewOrder.New_CustRaceID.Value:0;
            New_CustEmail = NewOrder.New_CustEmail;
            New_CustContactNumber = NewOrder.New_CustContactNumber;
            New_CustAlternateNumber = NewOrder.New_CustAlternateNumber;
            New_CustAddressLine1 = NewOrder.New_CustAddressLine1;
            New_CustAddressLine2 = NewOrder.New_CustAddressLine2;
            New_CustAddressLine3 = NewOrder.New_CustAddressLine3;
            New_CustPostCode = NewOrder.New_CustPostCode;
            New_CustCity = NewOrder.New_CustCity;
            New_CustState = NewOrder.New_CustState;
            New_CustStateID = NewOrder.New_CustStateID.HasValue?NewOrder.New_CustStateID.Value:0;
        }
    }
}