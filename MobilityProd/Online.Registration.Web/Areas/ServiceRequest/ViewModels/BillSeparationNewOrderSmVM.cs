﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    //-----------------------------------------------------------------------------------  
    //Developer   : Ashley Ow
    //File Name   : BillSeparation.cshtml
    //Purpose     : View Model for Bill Separation Summary Service Request
    //Version Control:
    //Version     Date        Change Made
    //1.0         11 Oct 2015  View Model created
    //-----------------------------------------------------------------------------------
    [Serializable()]
    public class BillSeparationNewOrderSmVM:SRSummaryVM
    {
        public BillSeparationNewOrderSmVM()
        {
        }

        public override string  HeaderCustName
        {
            get
            {
                return NewOrder.CustName;
            }
        }

        public new BillSeparationNewOrderVM NewOrder{
            get{
                return (BillSeparationNewOrderVM)base.NewOrder;
            }
            set{
                base.NewOrder = value;
            }
        }

        public BillSeparationNewOrderSmVM(BillSeparationNewOrderVM newOrder)
        {
            this.NewOrder = newOrder;
        }

    }
}