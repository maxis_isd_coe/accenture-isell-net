﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable()]
    public class ThirdPartyAuthUploadDocVM : UploadDocVM
    {
        [Display(Name = "Letter Of Authorization Id File")]
        [DataUnique("File", ErrorMessage = "Same document cannot be uploaded more than once")]
        public string LOAIdFile { get; set; }

        [Display(Name = "Letter Of Authorization Id File Name")]
        [StringLength(50)]
        public string LOAIdFileName { get; set; }

        [Display(Name = "3rd Party Front Id File")]
        [Required]
        [DataUnique("File", ErrorMessage = "Same document cannot be uploaded more than once")]
        public string ThirdPartyFrontIdFile { get; set; }

        [Display(Name = "3rd Party Front Id File Name")]
        [StringLength(50)]
        public string ThirdPartyFrontIdFileName { get; set; }

        [Display(Name = "3rd Party Back Id File")]
        [Required]
        [DataUnique("File", ErrorMessage = "Same document cannot be uploaded more than once")]
        public string ThirdPartyBackIdFile { get; set; }

        [Display(Name = "3rd Party Front Id Name")]
        [StringLength(50)]
        public string ThirdPartyBackIdFileName { get; set; }
    }
}