﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable]
    public class ThirdPartyAuthViewOrderVM : SRViewOrderWithCustVM
    {
        #region Third Party Information
        public string ThirdParty_Name { get; set; }
        public int ThirdParty_IDTypeId { get; set; }
        public string ThirdParty_IDCardNumber { get; set; }
        public int ThirdParty_AuthorizationTypeId { get; set; }
        #endregion
    }
}
