﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable()]
    public class PostpaidToPrepaidPrintOrderVM : SRPrintOrderWithCustVM
    {
        #region Service Information
        [Required]
        [StringLength(10)]
        [Display(Name = "Prepaid Account No")]
        public override string NewAccountNo { get; set; }
        public string Service_PrepaidSIMTypeCode { get; set; }
        public string Service_PrepaidSIMSerialNumber { get; set; }
        public string Service_PrepaidPlan { get; set; }
        public string Service_OutstandingAmount { get; set; }
        public string Service_EarlyTerminateFee { get; set; }
        #endregion
    }
}