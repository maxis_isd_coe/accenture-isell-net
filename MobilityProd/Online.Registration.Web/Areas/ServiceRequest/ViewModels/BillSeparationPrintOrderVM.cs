﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable()]
    public class BillSeparationPrintOrderVM:SRPrintOrderWithCustVM
    {
        #region Service Information
        public string Service_BillAddressLine1 { get; set; }
        public string Service_BillAddressLine2 { get; set; }
        public string Service_BillAddressLine3 { get; set; }
        public string Service_BillAddressPostCode { get; set; }
        public string Service_BillAddressCity { get; set; }
        public int Service_BillAddressStateId { get; set; }
        public string Service_BillAddressState { get; set; }
        #endregion
    }
}