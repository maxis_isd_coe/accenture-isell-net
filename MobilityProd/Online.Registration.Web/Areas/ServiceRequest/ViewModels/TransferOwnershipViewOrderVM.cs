﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable]
    public class TransferOwnershipViewOrderVM : SRViewOrderWithCustVM
    {
        #region NewOwner Information
        public string NewOwnerName { get; set; }
        public int NewOwnerIDTypeId { get; set; }
        public string NewOwnerIDCardNumber { get; set; }
        public DateTime NewOwnerDateOfBirth { get; set; }
        public string NewOwnerEmail { get; set; }
        public string NewOwnerGender { get; set; }
        public int NewOwnerLanguageId { get; set; }
        public int NewOwnerNationalityId { get; set; }
        public int NewOwnerRaceId { get; set; }
        public string NewOwnerContactNumber { get; set; }
        public string NewOwnerAltContactNumber { get; set; }
        public string NewOwnerAddressLine1 { get; set; }
        public string NewOwnerAddressLine2 { get; set; }
        public string NewOwnerAddressLine3 { get; set; }
        public string NewOwnerAddressPostCode { get; set; }
        public string NewOwnerAddressCity { get; set; }
        public int NewOwnerAddressStateId { get; set; }
        public SignatureVM NewOwnerSignature { get; set; }
        #endregion

        [Display(Name = "New Account No")]
        public override string NewAccountNo { get; set; }
    }
}
