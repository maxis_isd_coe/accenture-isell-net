﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable()]
    public class TransferOwnershipUploadDocVM:UploadDocVM
    {
        #region Override properties
        [Display(Name = "Transferor Front Id File")]
        [DataUnique("File", ErrorMessage = "Same document cannot be uploaded more than once")]
        public override string FrontIdFile { get; set; }

        [Display(Name = "Transferor Back Id File")]
        [DataUnique("File", ErrorMessage = "Same document cannot be uploaded more than once")]
        public override string BackIdFile { get; set; }
        #endregion

        [Display(Name = "Transferee Front Id File")]
        [DataUnique("File", ErrorMessage = "Same document cannot be uploaded more than once")]
        public string TransfereeFrontIdFile { get; set; }

        [Display(Name = "Transferee Front Id File Name")]
        [StringLength(50)]
        public string TransfereeFrontIdFileName { get; set; }

        [Display(Name = "Transferee Back Id File")]
        [DataUnique("File", ErrorMessage = "Same document cannot be uploaded more than once")]
        public string TransfereeBackIdFile { get; set; }

        [Display(Name = "Transferee Back Id File Name")]
        [StringLength(50)]
        public string TransfereeBackIdFileName { get; set; }
    }
}