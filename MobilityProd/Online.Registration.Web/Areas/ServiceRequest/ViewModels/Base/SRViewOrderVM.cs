﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Web.ViewModels.Common;
using Online.Registration.Web.ViewModels.Inteface;
using System.Web.Mvc;
using Online.Registration.Web.Helper;
using System.ComponentModel.DataAnnotations;
using Online.Registration.Web.CustomDataAnnotation;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base
{
    [Serializable()]
    public abstract class SRViewOrderVM:SRVMBase,INetworkPrint
    {
        #region INetworkPrint Implementation
        public string PrinterName { get; set; }

        public string PrinterId { get; set; }

        public string PrintOrderId{
            get{
                return OrderId;
            }
        }

        public IEnumerable<SelectListItem> GetPrinters{
            get{
                return Util.GetPrinters();
            }
        }

        public string PreviewPrintUrl{
            get{
                return _urlHelper.Action("PrintOrder", new { id = PrintOrderId });
            }
        }

        public string NetworkPrintUrl{
            get{
                return _urlHelper.Action("NetworkPrint");
            }
        }
        #endregion

        public abstract string HeaderCustName { get;}

        public RequestTypes RequestType { get; set; }

        public string CreateBy { get; set; }

        public string RequestTypeDisplay{
            get{
                return RequestType.GetLabel();
            }
        }

        public string OrderId { get; set; }

        public string QueueNo { get; set; }

        public string StatusDisplay {
            get{
                return Status.GetLabel();
            }
        }

        public SRStatus Status { get; set; }

        public string Remarks { get; set; }

        public string Justification { get; set; }

        public TermAndConditionVMBase TermAndCondition { get; set; }

        public SignatureVM Signature { get; set; }

        public String LockStatusDisplay {
            get{
                if (string.IsNullOrEmpty(LockBy))
                    return "Order is not locked.";
                if (LockBy == LoginId)
                    return "Lock by you.";
                return string.Format("Lock by {0}.",LockBy);
            }
        }

        private string _lockBy;
        public string LockBy {
            get{
                return _lockBy;
            }
            set{
                _lockBy = value;
                if (_lockBy == LoginId && !string.IsNullOrEmpty(LoginId))
                    IsLocked = true;
                else
                    IsLocked = false;
            }
        }

        public bool IsLocked{get;set;}

        public bool LockByOther {
            get{
                return !string.IsNullOrEmpty(LockBy);
            }
        }

        public string IsLockedLabel{
            get
            {
                if (string.IsNullOrEmpty(LockBy))
                    return "Lock";
                if (LockBy == LoginId)
                    return "Release Lock";
                return "Take Lock";
            }
        }

        /// <summary>
        /// Is order can edit, only in new status and lock by you
        /// </summary>
        public bool CanEdit {
            get{
                return LockBy == LoginId && 
                    !string.IsNullOrEmpty(LoginId) && 
                    Status==SRStatus.New;
            }
        }

        public bool IsNew {
            get{
                return Status == SRStatus.New;
            }
        }

        [CustomRequired("2", "Status")]
        [RegularExpression(@"^-?\d+$", ErrorMessage = "Invalid {0} format.")]
        [StringLength(10)]
        public virtual String NewAccountNo { get; set; }

        public bool HasDocument { get; set; }

        private UrlHelper _urlHelper{
            get{
                return new UrlHelper(HttpContext.Current.Request.RequestContext);
            }
        }

        public bool SaveContract { get;protected set; }

        public SRViewOrderVM()
        {
            //default when complete form will generate contract to pdf
            SaveContract = true;
        }

        /// <summary>
        /// Form config to show print button
        /// </summary>
        public bool CanPrint { 
            get{
                //Use save contract as taging now, enhance if future if need
                return SaveContract;
            }
        }
    }
}