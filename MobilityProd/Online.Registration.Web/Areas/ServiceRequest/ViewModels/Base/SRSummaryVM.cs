﻿using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base
{
    /// <summary>
    /// Abstract summary view model to share same properties
    /// </summary>
    [Serializable()]
    public abstract class SRSummaryVM : SRVMBase
    {
        public TermAndConditionVM TermAndCondition { get; set; }
        public TermAndConditionVM TermAndCondition_ForNewOwner { get; set; }

        public SRSummaryVM()
        {
            TermAndCondition = new TermAndConditionVM();
        }

        public virtual SRNewOrderVM NewOrder { get; set; }

        #region User Input
        public bool? AgreeTnC { get; set; }
        public SignatureVM Signature { get; set; }
        #endregion

        public abstract string HeaderCustName { get;}

        public RequestTypes RequestType { get { return this.ToRequestType(); } }

        public string RequestTypeDisplay { get { return RequestType.GetLabel(); } }
    }
}