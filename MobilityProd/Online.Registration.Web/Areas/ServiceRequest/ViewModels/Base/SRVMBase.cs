﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Repositories;
using Online.Registration.Web.Areas.ServiceRequest.Factories;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using System.Web;
using Online.Registration.Web.ViewModels.Common;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base
{
    [Serializable]
    public abstract class SRVMBase :VMBase
    {
        public string WarningMessage { get; set; }
    }
}
