﻿using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base
{
    [Serializable()]
    public class SRPrintOrderVM
    {
        public string StaffCodeDisplay
        {
            get
            {
                return string.Format("{0} ({1})", StaffCode, BranchCode);
            }
        }
        public string BranchNameDisplay
        {
            get
            {
                return string.Format("{0} ({1})", BranchName, BranchCode);
            }
        }
        public string SignSvg { get; set; }
        public DateTime? SignDt { get; set; }
        public string BranchName { get; set; }
        public string StaffCode { get; set; }
        public string BranchCode { get; set; }

        public bool BiometricPass { get; internal set; }
        public virtual String NewAccountNo { get; set; }

        public TermAndConditionVM TermAndCondition { get; set; }

        public RequestTypes RequestType { get; set; }

        public string OrderId { get; set; }
    }
}
