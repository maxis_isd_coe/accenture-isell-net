﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.CommonEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using System.ComponentModel.DataAnnotations;
using Online.Registration.Web.CustomDataAnnotation;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base
{
    [Serializable()]
    public abstract class SRNewOrderVM : SRVMBase
    {
        public Dictionary<BreType, string> BreErrors { get; set; }

        public abstract string HeaderCustName { get;}

        public RequestTypes RequestType { get { return this.ToRequestType(); } }

        public string RequestTypeDisplay { get { return RequestType.GetLabel(); } }

        public bool BiometricPass { get; internal set; }

        public bool OutstandingCheckPass { get; internal set; }

        public bool WriteoffCheckPass { get; internal set; }

        public bool DDMFCheckPass { get; internal set; }

        public bool TtlLineCheckPass { get; internal set; }

        public bool TtlContractCheckPass { get; internal set; }

        public bool IsExistingCust { get; set; }

        #region "Form configuration Settings"
        /// <summary>
        /// Is this request require signature and will display upload contract if no signature, 
        /// if false will totally skip signature and no upload if no signature
        /// </summary>
        public bool RequireSignature { get;protected set; }
        /// <summary>
        /// Allow print in the end of order summary, show or not show print button
        /// </summary>
        public bool AllowPrint { get; protected set; }
        #endregion

        #region Eligible check process
        /// <summary>
        /// Run eligible check to get condition is this form can proceed request
        /// </summary>
        public void RunEligibleCheck()
        {
            AddRule();
            if (EligibleRules != null)
            {
                EligibleRules.ToList().ForEach(
                    n => RunEligibleCheck(n.Key, n.Value)
                    );
            }
        }

        private void RunEligibleCheck(string errMsg, Func<SRNewOrderWthCustVM, bool> rule)
        {
            if (!rule.Invoke((dynamic)this))
                EligibleErrors.Add(errMsg);
        }

        public List<string> EligibleErrors { get; private set; }

        protected Dictionary<string, Func<SRNewOrderWthCustVM, bool>> EligibleRules { get; private set; }

        /// <summary>
        /// Add rule for eligible check
        /// </summary>
        /// <param name="errMsg"></param>
        /// <param name="rule"></param>
        public void AddEligibleRule(string errMsg, Func<SRNewOrderWthCustVM, bool> rule)
        {
            EligibleRules.Add(errMsg, rule);
        }

        /// <summary>
        /// Is form pass eligible check
        /// </summary>
        public bool EligibleCheckPass
        {
            get
            {
                return EligibleErrors == null || EligibleErrors.Count() == 0;
            }
        }
        #endregion

        public SRNewOrderVM()
        {
            RequireSignature = true;
            AllowPrint = true;
            EligibleRules = new Dictionary<string, Func<SRNewOrderWthCustVM, bool>>();
            EligibleErrors = new List<string>();
        }

        protected virtual void AddRule(){
            //add default rule
            //Rule pass when form is IExistingCustOnlyOrder and form customer is existing or form is not IExistingCustOnlyOrder
            AddEligibleRule("This request only allow for existing customer", n => 
                (n is IExistingCustOnlyOrder && n.IsExistingCust) || 
                !(n is IExistingCustOnlyOrder)
                );
        }

        public UploadDocRequireIfParam[] UploadDocRequireIfJustificationParams { get; set; }
    }
}