﻿using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomDataAnnotation;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base
{
    [Serializable()]
    public abstract class SRNewOrderWthCustVM:SRNewOrderVM
    {
        #region Service Info Property
        [Required]
        [Display(Name = "MSISDN")]
        [CustomStringLength(15, 11, "{0} is too long.", "{0} must be 11 characters.")]
        [RegularExpression(@"^-?\d+$", ErrorMessage = "Invalid {0} format.")]
        public string Msisdn { get; set; }

        [Display(Name = "Account No")]
        [CustomStringLength(10, 9, "{0} is too long.", "{0} must be 10 characters.")]
        [RegularExpression(@"^-?\d+$", ErrorMessage = "Invalid {0} format.")]
        public virtual string AccNum { get; set; }

        [Display(Name = "Plan Name")]
        public virtual string Plan { get; set; }

        public List<string> MSISDNList { get; set; }

        public List<string> VasList { get;set; }
        #endregion

        #region Cust Info Property
        [Required]
        [Display(Name = "Identification Type")]
        public string IdType { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Identification Number")]
        [RegularExpression(@"^[a-zA-Z0-9]*$")] //Allow alphanumeric
        public string IdNum { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Name")]
        public string CustName { get; set; }

        [Display(Name = "Language ID")]
        public int CustLanguageID { get; set; }

        [Display(Name = "Nationality ID")]
        public int CustNationalityID { get; set; }

        [Display(Name = "Race ID")]
        public int CustRaceID { get; set; }

        [Display(Name = "Gender")]
        public string CustGender { get; set; }

        [Display(Name = "Email")]
        public string CustEmail { get; set; }

        [Display(Name = "Contact Number")]
        public string CustContactNumber { get; set; }

        [Display(Name = "Alternate Contact Number")]
        public string CustAltContactNumber { get; set; }

        [Display(Name = "Date Of Birth")]
        public DateTime CustDob { get; set; }

        [Display(Name = "Address Line 1")]
        public string CustBillAddLine1 { get; set; }

        [Display(Name = "Address Line 2")]
        public string CustBillAddLine2 { get; set; }

        [Display(Name = "Address Line 3")]
        public string CustBillAddLine3 { get; set; }

        [Display(Name = "City")]
        public string CustBillCity { get; set; }

        [Display(Name = "State")]
        public string CustBillState { get; set; }

        [Display(Name = "State ID")]
        public int CustBillStateID { get; set; }

        [Display(Name = "Postcode")]
        public string CustBillPostCode { get; set; }
        #endregion

        public override string HeaderCustName
        {
            get
            {
                return CustName ?? "New Customer";
            }
        }
        /// <summary>
        /// A dictionary list of msisdn key and service info data
        /// </summary>
        public Dictionary<string,ServiceInfoVM> MsisdnServiceInfoMap { get; set; }
    }
}