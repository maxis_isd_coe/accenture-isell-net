﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base
{
    [Serializable()]
    public abstract class SRPrintOrderWithCustVM:SRPrintOrderVM
    {
        #region Customer Information
        public string CustName { get; set; }
        public int CustIDTypeId { get; set; }
        public string CustIDCardNumber { get; set; }
        public DateTime CustDateOfBirth { get; set; }
        public string CustGender { get; set; }
        public int CustLanguageId { get; set; }
        public int CustNationalityId { get; set; }
        public int CustRaceId { get; set; }
        public string CustEmail { get; set; }
        public string CustContactNumber { get; set; }
        public string CustAltContactNumber { get; set; }
        public string CustAddressLine1 { get; set; }
        public string CustAddressLine2 { get; set; }
        public string CustAddressLine3 { get; set; }
        public string CustAddressPostCode { get; set; }
        public string CustAddressCity { get; set; }
        public int CustAddressStateId { get; set; }
        public string CustAddressState { get; set; }
        #endregion

        #region Service Information "Current Info"
        public string Service_MSISDN { get; set; }
        public string Service_Plan { get; set; }
        public List<string> Service_VAS { get; set; }
        public virtual string Service_AccountNumber { get; set; }
        #endregion
    }
}