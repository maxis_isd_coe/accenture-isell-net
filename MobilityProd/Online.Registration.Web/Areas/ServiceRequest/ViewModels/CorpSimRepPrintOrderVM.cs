﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable()]
    public class CorpSimRepPrintOrderVM:SRPrintOrderVM
    {
        public string PicName { get; set; }
        public string IdNo { get; set; }
        public string CompName { get; set; }
        public string CompBrn { get; set; }
        public string Msisdn { get; set; }
        public string AccNo { get; set; }
        public string NewSimTypeCode { get; set; }
        public string NewSimSerial { get; set; }        
        
        public int IdTypeId { get; set; }
        

        public string BiometricDisplay{
            get{
                return BiometricPass.ToBiometricResult();
            }
        }

        public string IdType {
            get{
                return IdTypeId.ToIdCardTypeDesc();
            }
        }
    }
}