﻿//-----------------------------------------------------------------------------------
//    Developer   : Ashley Ow
//    File Name   : PostpaidToPrepaidNewOrderVM.cshtml
//    Purpose     : View Model for Postpaid To Prepaid Service Request
//    Version Control:
//    Version     Date        Change Made
//    1.0         11 Oct 2015  View Model created
//-----------------------------------------------------------------------------------

using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Helper;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.DAL.Helpers;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable()]
    public class PostpaidToPrepaidNewOrderVM : SRNewOrderWthCustVM, IExistingCustOnlyOrder
    {
        #region "Basic Input"
        [Required]
        [Display(Name = "Postpaid Account Number")]
        public override string AccNum { get; set; }

        [Display(Name = "Prepaid Account Number")]
        public string PrepaidAccNumber { get; set; }

        [Required]
        [Display(Name = "Prepaid SIM type")]
        public string PrepaidSimTypeCode { get; set; }

        [Required]
        [Display(Name = "Prepaid SIM Serial Number")]
        [CustomStringLength(19, 19, "Prepaid SIM Serial is too long.", "Prepaid SIM Serial must be 19 characters.")]
        [RegularExpression(@"^-?\d+$", ErrorMessage = "Invalid Prepaid SIM Serial format.")]
        [ValidSimSerial("PrepaidSimTypeCode", "Prepaid SIM Type and {0} not match or {0} is not a valid sim serial in inventory")]
        public string PrepaidSIMNumber { get; set; }

        [Display(Name = "Postpaid Plan")]
        public override string Plan { get; set; }

        [Display(Name = "Prepaid Plan")]
        public string PrepaidPlan { get; set; }

        [Display(Name = "Outstanding Bill Amount")]
        public Dictionary<string, decimal> OutstandingBillAmount { get; set; }
        public decimal GetOutstandingBillAmount { 
            get{
                if (OutstandingBillAmount.Keys.Contains(AccNum))
                {
                    return OutstandingBillAmount[AccNum];
                }
                else {
                    return 0.00M;
                }
            }
        }

        [Display(Name = "Early Termination Fee")]
        public decimal EarlyTerminationFee { get; set; }
        #endregion

        [UploadDocRequireIf("BiometricPass", false, ErrorMessage = "Biometric has failed, please upload ID documents")]
        [UploadDocRequireIf("UploadDocRequireIfJustificationParams", "Justification", ErrorMessage = "{0}, justification require")]
        public UploadDocVM UploadDoc { get; set; }

        public PostpaidToPrepaidNewOrderVM()
        {
            //Default plan
            PrepaidPlan="Hotlink Plan";
            UploadDocRequireIfJustificationParams = new[] {
                new UploadDocRequireIfParam(this.Name(n=>n.OutstandingCheckPass), false, "Outstanding check failed"),
                new UploadDocRequireIfParam(this.Name(n=>n.WriteoffCheckPass), false, "Write off check failed"),
            };
        }

        protected override void AddRule()
        {
            base.AddRule();
            //Only customer with valid postpoid can proceed
            AddEligibleRule("Customer do not have any valid postpaid info.", n => n.MSISDNList != null && n.MSISDNList.Count() > 0);
        }
    } 
}