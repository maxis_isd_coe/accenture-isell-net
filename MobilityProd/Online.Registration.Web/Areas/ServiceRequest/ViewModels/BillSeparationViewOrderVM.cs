﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels
{
    [Serializable]
    public class BillSeparationViewOrderVM:SRViewOrderWithCustVM
    {

        #region Service Information
        [Display(Name = "New Account No")]
        public override string NewAccountNo { get; set; }

        public string Service_BillAddressLine1 { get; set; }
        public string Service_BillAddressLine2 { get; set; }
        public string Service_BillAddressLine3 { get; set; }
        public string Service_BillAddressPostCode { get; set; }
        public string Service_BillAddressCity { get; set; }
        public int Service_BillAddressStateId { get; set; }
        public string Service_BillAddressState { get; set; }
        #endregion
    }
}
