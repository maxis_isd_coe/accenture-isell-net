﻿//-----------------------------------------------------------------------------------
//  Developer   : Jazz Tong
//  File Name   : SRSubmitSuccessVM.cs
//  Purpose     : View model class for success page
//  Version Control:
//  Version     Date         Change Made
//  1.0         20 Oct 2015  New
//-----------------------------------------------------------------------------------
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Online.Registration.Web.CustomExtension;
using System.ComponentModel.DataAnnotations;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common
{
    /// <summary>
    /// View model class to provide display need for success page
    /// </summary>
    [Serializable]
    public class SRSubmitResultVM:SRVMBase
    {
        public string OrderId { get; set; }
        public string ErrorMessage { get; set; }
        public bool Success { get; set; }
        public string QueueNo { get; set; }

        public string HeaderName { 
            get{
                return RequestType.GetLabel();
            }
        }
        public RequestTypes RequestType {get;set;}

        public SRSubmitResultVM()
        {
            Success = true;
        }

        /// <summary>
        /// Hide or show print button
        /// </summary>
        public bool AllowPrint { get; set; }
    }
}
