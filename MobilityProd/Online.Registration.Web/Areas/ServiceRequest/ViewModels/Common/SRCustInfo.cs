﻿using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common
{
    [Serializable()]
    public class SRCustInfo
    {
        public string Name { get; set; }

        public string IdType { get; set; }

        public string IdNum { get; set; }

        public string Gender { get; set; }

        public int RaceId { get; set; }

        public int LanguageId { get; set; }

        public int NationalityId { get; set; }

        public string Email { get; set; }

        public string ContactNum { get; set; }

        public string AltContactNum { get; set; }

        public Dictionary<string, ServiceInfoVM> MsisdnServiceInfoMap { get; set; }

        public string BillAddLine1 { get; set; }

        public string BillAddLine2 { get; set; }

        public string BillPostCode { get; set; }

        public string BillCity { get; set; }

        public string BillState { get; set; }

        public string BillAddLine3 { get; set; }

        public string Salutation { get; set; }

        public DateTime? Dob { get; set; }

        public int BillStateId { get; set; }

        public int DobY {
            get { return Dob.HasValue?Dob.Value.Year:0; }
        }

        public int DobM {
            get { return Dob.HasValue?Dob.Value.Month:0; }
        }

        public int DobD {
            get { return Dob.HasValue?Dob.Value.Day:0; }
        }

        public bool IsExistingCust { get; set; }
    }
}