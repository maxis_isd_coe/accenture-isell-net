﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common
{
    [Serializable()]
    public class SRSubmitResultUploadVM :SRSubmitResultVM
	{
        [Display(Name = "Scan form")]
        [StringLength(50)]
        [Required]
        public string RegFormFileName { get; set; }

        [Display(Name = "Scan form file name")]
        [Required]
        public string RegFormFile { get; set; }
    }
}