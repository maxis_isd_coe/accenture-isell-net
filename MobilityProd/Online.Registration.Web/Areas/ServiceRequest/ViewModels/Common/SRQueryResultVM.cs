﻿using System;
using System.Collections.Generic;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common
{
    [Serializable()]
    public class SRQueryResultVM
    {
        public List<SRQueryResultItemVM> Results { get; set; }
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public string SortBy { get; set; }
        public bool? SortDesc { get; set; }
    }
}