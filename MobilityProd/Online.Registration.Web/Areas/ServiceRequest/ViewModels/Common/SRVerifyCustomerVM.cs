﻿using Online.Registration.Web.CustomDataAnnotation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common
{
    [Serializable()]
    public class SRVerifyCustomerVM
    {
        [Range(typeof(int), "1", "999", ErrorMessage = "Identification Type Require")]
        [Required]
        public string IdType { get; set; }
        [Required]
        [ValidIdNum("IdType")]
        [Display(Name="Identification Number")]
        public string IdNo { get; set; }

        public Boolean? IsNew {get;set;}

        public SRVerifyCustomerVM()
        {
            IsNew = false;//default false
        }
    }
}