﻿using Online.Registration.DAL.Models.FilterCriterias;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.DAL.Helpers;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common
{
    [Serializable()]
    public class SRViewOrderListVM:SRVMBase,IDtPostVM
    {
        public string IdNo { get; set; }

        public string IdNo1 { get; set; }

        public string OrderId { get; set; }

        public RequestTypes? RequestType { get; set; }

        public SRStatus? Status { get; set; }

        public String CreatedBy { get; set; }

        public string MSISDN { get; set; }

        public DateTime? CreateDateStart { get; set; }

        public DateTime? CreateDateEnd { get; set; }

        public bool IsToday { get; set; }

        public List<string> CreateByList { get; set; }

        public SRViewOrderListVM()
        {
            Status = SRStatus.New;
            CreateByList = new List<string>();
        }

        #region IDtPostVM Implementation
        public List<DtPostColumnVM> Columns { get; set; }
        public int Draw { get; set; }
        public int Length { get; set; }
        public List<DtPostOrderVM> Order { get; set; }
        public int Start { get; set; }
        public DtPostSearchVM Search { get; set; }
        #endregion

        public List<string> GetColumnNames(){
            return ResultModelVMMapper.Keys.ToList();
        }

        private Dictionary<string, string> _resultModelVMMapper = null;
        //This mapper control what display in front-end and what column in backend associate with when perfrom sorting
        public Dictionary<string,string> ResultModelVMMapper{
            get
            {
                if (_resultModelVMMapper == null)
                {
                    var vm = new SRQueryResultItemVM();
                    var model = new SRQueryMaster();
                    _resultModelVMMapper = new Dictionary<string, string>()
                    {
                        {vm.Name(n=>n.OrderId),model.Name(n=>n.OrderId)},
                        {vm.Name(n=>n.RequestTypeDisplay),model.Name(n=>n.RequestTypeId)},
                        {vm.Name(n=>n.IdCardNo),model.Name(n=>n.IdCardNo)},
                        {vm.Name(n=>n.MSISDN),model.Name(n=>n.MSISDN)},
                        {vm.Name(n=>n.FullName),model.Name(n=>n.FullName)},
                        {vm.Name(n=>n.CreateDtDisplay),model.Name(n=>n.CreateDt)},
                        {vm.Name(n=>n.CreateBy),model.Name(n=>n.CreateBy)},
                        {vm.Name(n=>n.UpdateDtDisplay),model.Name(n=>n.UpdateDt)},
                        {vm.Name(n=>n.UpdateBy),model.Name(n=>n.UpdateBy)},
                        {vm.Name(n=>n.QueueNo),model.Name(n=>n.QueueNo)},
                        {vm.Name(n=>n.StatusDisplay),model.Name(n=>n.StatusId)},
                        {vm.Name(n=>n.LockBy),model.Name(n=>n.LockBy)}
                    };
                }
                return _resultModelVMMapper;
            }
        }
        internal string GetColumnName(int column)
        {
            return ResultModelVMMapper[GetColumnNames()[column]];
        }
    }
}