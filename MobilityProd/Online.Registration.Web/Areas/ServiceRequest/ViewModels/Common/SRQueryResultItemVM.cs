﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Web.CustomExtension;

namespace Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common
{
    [Serializable()]
    public class SRQueryResultItemVM:SRVMBase
    {
        const string DATEFORMAT= "d/M/yyyy h:m:s tt";
        public string OrderId { get; set; }
        public RequestTypes RequestType { get; set; }

        public string RequestTypeDisplay {
            get
            {
                return RequestType.GetLabel();
            }
        }
        public string IdCardNo { get; set; }
        public string MSISDN { get; set; }
        public string FullName { get; set; }
        public DateTime CreateDt { get; set; }

        public string CreateDtDisplay
        {
            get {
                return CreateDt.ToString(DATEFORMAT);
            }
        }
        public string CreateBy { get; set; }
        public DateTime? UpdateDt { get; set; }

        public string UpdateDtDisplay
        {
            get
            {
                if (UpdateDt.HasValue)
                    return UpdateDt.Value.ToString(DATEFORMAT);
                return "";
            }
        }
        public string UpdateBy { get; set; }
        public string QueueNo { get; set; }
        public SRStatus Status { get; set; }

        public string StatusDisplay {
            get{
                return Status.GetLabel();
            }
        }
        public string LockBy { get; set; }
    }
}