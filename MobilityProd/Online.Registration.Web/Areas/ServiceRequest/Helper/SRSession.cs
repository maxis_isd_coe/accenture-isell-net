﻿using Online.Registration.DAL.Models;
using Online.Registration.Web.AMRSvc;
using Online.Registration.Web.Areas.ServiceRequest.DataAdapter;
using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.Helper;
using Online.Registration.Web.Models;
using Online.Registration.Web.SRService;
using Online.Registration.Web.SubscriberHistoryService;
using Online.Registration.Web.SubscriberICService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Online.Registration.Web.Areas.ServiceRequest.Helper
{
    public class SRSession
    {
        public static retrieveAcctListByICResponse CurrentPPIDInfo
        {
            get {
                return (retrieveAcctListByICResponse)Session["PPIDInfo"];
            }
        }

        private static HttpSessionState Session
        {
            get
            {
                return HttpContext.Current.Session;
            }
        }

        public static string CurrentPPIDFlag { 
            get {
                return (string)Session["PPID"];
            } 
        }

        public static RetrieveMyKadInfoRp CurrentCustBiometricData {
            get{
                return (RetrieveMyKadInfoRp)Session[SessionKey.BiometricResponse.ToString()];
            }
        }

        public static string CurrentFraudMessage{
            get{
                return Session["Fraudmessage"].ToString();
            }
        }
        /// <summary>
        /// Search by value from Search Page
        /// </summary>
        public static string SearchByIdTypeValue { 
            get{
                return (String)Session["SearchBy"];
            }
        }

        /// <summary>
        /// Id num value from Search Page
        /// </summary>
        public static string SearchByIdNumValue { 
            get {
                return (String)Session["cardNumber"];
            }
        }

        /// <summary>
        /// Bre status detail for current customer
        /// </summary>
        public static List<BreStatusDetails> CurrentBreStatusDtls { 
            get {
                return (List<BreStatusDetails>)Session["BreStatusDetails"];
            }
        }

        /// <summary>
        /// Outstanding Bill Amount (if any) for current customer
        /// </summary>
        public static string CurrentOutstandingBillAmountDtls
        {
            get
            {
                return (String)Session[SessionKey.FailedAcctIds.ToString()];
            }
        }

        /// <summary>
        /// Biometric status desc from customer search screen
        /// </summary>
        public static string CurrentBiometricMsg { 
            get { 
                return (string)Session["BiometricDesc"]; 
            } 
        }

        /// <summary>
        /// Current oustanding msg from customer search
        /// </summary>
        public static string CurrentOutstanding { 
            get {
                return (string)Session[SessionKey.FailedAcctIds.ToString()];
            } 
        }
        /// <summary>
        /// Drop 4 object, need to restructure
        /// </summary>
        public static DropFourObj DropFourObj {
            get{
                return (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            }
        }
        /// <summary>
        /// Current account contract count
        /// </summary>
        public static int CurrentContractCount{
            get{
                return (int)Session["AccountswithContractCount"];
            }
        }

        public static List<IndividualResponse> CurrentTelcoHistory {
            get{
                return (List<IndividualResponse>)Session["HistCompany"];
            }
        }

        //
        public static retrieveAcctListByICResponse CurrentAllCustomerInfo {
            get {
                return (retrieveAcctListByICResponse)Session["AllCustomerDetails"];
            }
        }

        /// <summary>
        /// Current login user
        /// </summary>
        public static Access CurrentLoginUser{
            get{
                return Util.SessionAccess;
            }
        }

        public static SRSummaryVM CurrentNewOrderSummary{
            get{
                return (dynamic)Session["OrderSummary"];
            }
            set{
                Session["OrderSummary"] = value;
            }
        }

        public static SRNewOrderVM CurrentNewOrder{
            get{
                return (dynamic)Session["Order"];
            }
            set{
                Session["Order"] = value;
            }
        }

        public static SRViewOrderVM CurrentViewOrder
        {
            get
            {
                return (dynamic)Session["ViewOrder"];
            }
            set
            {
                Session["ViewOrder"] = value;
            }
        }

        public static SRViewOrderListVM CurrentViewOrderList
        {
            get
            {
                return (dynamic)Session["ViewOrderList"];
            }
            set
            {
                Session["ViewOrderList"] = value;
            }
        }

        public static SRQueryOrderReq CurrentQueryOrderReq { 
            get{
                return (dynamic)Session["QueryOrderReq"];
            }
            set{
                Session["QueryOrderReq"] = value;
            }
        }

        public static ICustInfoProvider CurrentCustInfo
        {
            get
            {
                return (dynamic)Session["CustInfoAdapter"];
            }
            set
            {
                Session["CustInfoAdapter"] = value;
            }
        }
    }
}