﻿//-----------------------------------------------------------------------------------
//  Developer   : Ashley Ow
//  View Name   : PostpaidToPrepaidController.cs
//  Purpose     : Controller for Postpaid To Prepaid Service Request
//  Version Control:
//  Version     Date         Change Made
//  1.0         12 Oct 2015  Controller created
//-----------------------------------------------------------------------------------

using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.Areas.ServiceRequest.Controllers
{
    public class PostpaidToPrepaidController : CommonController
    {
    }
}