﻿//-----------------------------------------------------------------------------------
//  Developer   : Ashley Ow
//  File Name   : TransferOwnershipController.cs
//  Purpose     : Controller for Transfer Ownership Service Request
//  Version Control:
//  Version     Date         Change Made
//  1.0         12 Oct 2015  Controller created
//-----------------------------------------------------------------------------------

using Online.Registration.Web.Areas.ServiceRequest.DataAdapter;
using Online.Registration.Web.Areas.ServiceRequest.Enums;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.CustomExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.Areas.ServiceRequest.Controllers
{
    public class TransferOwnershipController : CommonController
    {
        [HttpGet]
        public ActionResult AjaxVerifyCustomer(SRVerifyCustomerVM vm)
        {
            if (!ModelState.IsValid){
                return Json(ModelState.ToErrorJson(), JsonRequestBehavior.AllowGet);
            }
            var idNo = vm.IdNo;
            var idType = vm.IdType;
            var isNew = vm.IsNew;
            //Get cust info
            CustInfoSource = new CustInfoByICAdapter(idType.ToIdTypeKenanEnumStr(), idNo);
            var custInfoVM = TheVMFactory.CreateCustInfoVM(CustInfoSource,ServiceTypes.PostpaidOnly);
            //Get bre info
            var breChecker = GetBreChecker(idType,idNo,CustInfoSource);
            var breErrors = breChecker.GetBreErrors();
            var newOrder = NewOrderVM as TransferOwnershipNewOrderVM;
            //set bre check status
            SetNewOwnerbreStatus(newOrder, breErrors);
            //Set in session to keep data sync
            if (isNew == false){
                newOrder.TransfereeBreErrors = breErrors;
                newOrder.NewCustIsExistCust = false;
                newOrder.ExistQueryCustomer = true;
            }
            else{
                newOrder.NewQueryCustomer = true;
                newOrder.NewCustIsExistCust = custInfoVM!=null && custInfoVM.IsExistingCust;
                newOrder.NewCustBreErrors = breErrors;
            }

            newOrder.TransfereeBiometricPass = breChecker.BiometricPass;

            if(custInfoVM!=null){
                if(isNew==false){
                    SetCustInfoToOrder(newOrder,custInfoVM);
                }
                else
                {
                    //let form posting to collect data
                }
            }
            if (custInfoVM != null && custInfoVM.MsisdnServiceInfoMap != null && custInfoVM.MsisdnServiceInfoMap.Count()>0)
            {
                newOrder.TransfereeMsisdnServiceInfoMap = custInfoVM.MsisdnServiceInfoMap;
                newOrder.TransfereeMsisdnList = custInfoVM.MsisdnServiceInfoMap.Keys.ToList();
            }
            //Output array of object with custinfo and breerrors
            return Json(new object[]{
                custInfoVM,
                breErrors.ToDictionary(n => n.Key.ToString(), n => n.Value)
            },
            JsonRequestBehavior.AllowGet);
        }

        private void SetNewOwnerbreStatus(TransferOwnershipNewOrderVM vm, Dictionary<BreType, string> breErrors)
        {
            vm.NewOwnerOutstandingCheckPass = !breErrors.ContainsKey(BreType.OutstandingCreditCheck);
            vm.NewOwnerTtlContractCheckPass = !breErrors.ContainsKey(BreType.ContractCheck);
            vm.NewOwnerTtlLineCheckPass = !breErrors.ContainsKey(BreType.TotalLineCheck);
            vm.NewOwnerWriteoffCheckPass = !breErrors.ContainsKey(BreType.WriteoffCheck);
            vm.NewOwnerBiometricPass = !breErrors.ContainsKey(BreType.Biometric);
        }

        private void SetCustInfoToOrder(TransferOwnershipNewOrderVM vm, SRCustInfo custInfoVM)
        {
            vm.Exist_CustName=custInfoVM.Name;
            vm.Exist_CustIDTypeId=int.Parse(custInfoVM.IdType.ToIDCardTypeValue());
            vm.Exist_CustIDNumber=custInfoVM.IdNum;
            vm.Exist_CustSalutation=custInfoVM.Salutation;
            vm.Exist_CustDOB=custInfoVM.Dob;
            vm.Exist_CustGender=custInfoVM.Gender;
            vm.Exist_CustLanguageID=custInfoVM.LanguageId;
            vm.Exist_CustNationalityID=custInfoVM.NationalityId;
            vm.Exist_CustRaceID=custInfoVM.RaceId;
            vm.Exist_CustEmail=custInfoVM.Email;
            vm.Exist_CustContactNumber=custInfoVM.ContactNum;
            vm.Exist_CustAlternateNumber=custInfoVM.AltContactNum;
            vm.Exist_CustAddressLine1=custInfoVM.BillAddLine1;
            vm.Exist_CustAddressLine2=custInfoVM.BillAddLine2;
            vm.Exist_CustAddressLine3=custInfoVM.BillAddLine3;
            vm.Exist_CustPostCode=custInfoVM.BillPostCode;
            vm.Exist_CustCity=custInfoVM.BillCity;
            vm.Exist_CustState=custInfoVM.BillState;
            vm.Exist_CustStateID=custInfoVM.BillStateId;
        }

        private IBreInfoProvider GetBreChecker(string idType, string idNo,ICustInfoProvider CustInfoSource)
        {
            //Only require ttl check, contract check, outstanding check, writeoff and biometric for thirdparty Auth
            var breChecker = new BreCheckerAdapter(
                idType,idNo,
                new[] {
                    BreType.TotalLineCheck,
                    BreType.ContractCheck,
                    BreType.OutstandingCreditCheck,
                    BreType.WriteoffCheck,
                    BreType.Biometric
                });
            //Init parameter to start breChecker
            breChecker.InitServices((CustInfoSource as CustInfoByICAdapter).GetAllServices());
            breChecker.InitBiometricParam((CustInfoSource as CustInfoByICAdapter).GetBiometricData());
            return breChecker;
        }
    }
}