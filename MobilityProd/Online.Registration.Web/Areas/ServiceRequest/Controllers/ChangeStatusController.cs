﻿using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.Areas.ServiceRequest.Controllers
{
    public class ChangeStatusController : CommonController
    {
        [HttpGet]
        public ActionResult AjaxVerifyPenaltyInfo(string msisdn){
            var result = "";
            if(!string.IsNullOrEmpty(msisdn)){
                var newOrder = NewOrderVM as ChangeStatusNewOrderVM;
                if (newOrder.MsisdnServiceInfoMap.ContainsKey(msisdn))
                {
                    var serviceInfo = newOrder.MsisdnServiceInfoMap[msisdn];
                    var penalties = ContractUtil.GetPenaltyInfo(serviceInfo);
                    if (penalties != null && penalties.Count() > 0){
                        //Set to insync with object
                        newOrder.ContractPendingMonths = penalties.First().RemainingMonth;
                        newOrder.EarlyTerminationFee = penalties.First().Penalty;
                        result = newOrder.GetPenaltyWarning();
                    }
                }
            }
            return Json(new object[]{result},JsonRequestBehavior.AllowGet);
        }
    }
}