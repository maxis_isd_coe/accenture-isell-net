﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Repositories;
using Online.Registration.Web.ServiceProxy;
using Online.Registration.Web.SRService;
using Online.Registration.Web.Areas.ServiceRequest.Factories;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.Web.ViewModels.Common;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.DAL.Helpers;
using System.Reflection;
using System.Linq.Expressions;
using System.Web.Mvc;
using System;
using System.Linq;

namespace Online.Registration.Web.Areas.ServiceRequest.Controllers
{
	public partial class CommonController
	{
        /// <summary>
        /// Display new order page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NewOrder()
        {
            try
            {
                if (!IsFromSummary)
                {
                    NewOrderVM = null;
                    NewOrderVM = TheVMFactory.CreateNewOrderVM(this);
                }
                //Add eligile error to model
                if (!NewOrderVM.EligibleCheckPass)
                    ModelState.AddModelError("EligibleCheck", NewOrderVM.EligibleErrors.First());
                //Handle popup
                PopupInfo = TheVMFactory.CreatePopupInfoVM(NewOrderVM);
                return View(NewOrderVM);
            }
            catch(Exception ex)
            {
                //Did not perform customer search
                return RedirectToAction("IndexNew", "Home", new { area = "" });
            }
        }

        /// <summary>
        /// Post action to new order page
        /// </summary>
        /// <param name="newOrder"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("NewOrder")]
        public ActionResult NewOrder_Post(FormCollection form)
        {
            if (NewOrderVM==null){
                ModelState.AddModelError(string.Empty, "You got no data to post");
                return RedirectToAction(this.MethodName(n => n.NewOrder()));
            }

            TryUpdateModel((dynamic)NewOrderVM, form);

            if (!ModelState.IsValid)
            {
                NewOrderSmVM = null;
                return View(NewOrderVM);
            }

            NewOrderSmVM = TheVMFactory.CreateNewOrderSMVM(this);
            return RedirectToAction(this.MethodName(n => n.NewOrderSummary()));
        }

        /// <summary>
        /// Display New Order Summary page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NewOrderSummary()
        {
            if (NewOrderSmVM == null || NewOrderSmVM.NewOrder == null)
            {
                ModelState.AddModelError(string.Empty, "New order info need to filled.");
                return RedirectToAction(this.MethodName(n => n.NewOrder()));
            }
            return View(NewOrderSmVM);
        }

        /// <summary>
        /// Click submit in summary page
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("NewOrderSummary")]
        public ActionResult NewOrderSummary_Post(FormCollection form)
        {
            if(NewOrderSmVM ==null){
                //Enter here for no previous session data, so return to new order screen
                ModelState.AddModelError(string.Empty, "New order info need to filled.");
                return RedirectToAction(this.MethodName(n => n.NewOrder()));
            }

            TryUpdateModel((dynamic)NewOrderSmVM, form);

            if (!ModelState.IsValid)
                return View(NewOrderSmVM);
            var result = Submit(NewOrderSmVM);
            if (!result.Success)
            {
                ModelState.AddModelError("", result.ErrorMessage);
                return View(NewOrderSmVM);
            }

            //Set in temp to pass in next action
            SubmitInfo = result;
            //Clean data
            NewOrderSmVM = null;
            NewOrderVM = null;

            if (SubmitInfo is SRSubmitResultUploadVM)
                return RedirectToAction(this.MethodName(n => n.NewOrderSuccessUpload()));
            return RedirectToAction(this.MethodName(n => n.NewOrderSuccess()));
        }

        /// <summary>
        /// New order page click back
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NewOrderBack()
        {
            NewOrderVM = null;
            NewOrderSmVM = null;
            return RedirectToActionPermanent("IndexNew", "Home", new { area = "" });
        }

        /// <summary>
        /// Summary click back to new order page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NewOrderSummaryBack()
        {
            IsFromSummary = true;
            return RedirectToAction(this.MethodName(n => n.NewOrder()));
        }

        /// <summary>
        /// Cancel current order go home page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NewOrderSummaryCancel()
        {
            NewOrderVM = null;
            NewOrderSmVM = null;
            return RedirectToActionPermanent("IndexNew", "Home", new { area = "" });
        }

        /// <summary>
        /// Go new order success page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NewOrderSuccess()
        {
            try
            {
                if(SubmitInfo==null)
                    return RedirectToAction(this.MethodName(n => n.NewOrder()));
                return View(SubmitInfo);
            }
            catch (System.Exception)
            {
                return RedirectToAction(this.MethodName(n => n.NewOrder()));
            }
        }

        /// <summary>
        /// Go new order success page with no print button
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NewOrderSuccessNoPrint()
        {
            try
            {
                if (SubmitInfo == null)
                    return RedirectToAction(this.MethodName(n => n.NewOrder()));
                return View(SubmitInfo);
            }
            catch (System.Exception)
            {
                return RedirectToAction(this.MethodName(n => n.NewOrder()));
            }
        }

        /// <summary>
        /// Go new order success page with upload feature
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NewOrderSuccessUpload()
        {
            try
            {
                if(SubmitInfo==null)
                    return RedirectToAction(this.MethodName(n => n.NewOrder()));
                return View(SubmitInfo);
            }
            catch (System.Exception)
            {
                return RedirectToAction(this.MethodName(n => n.NewOrder()));
            }
        }

        [HttpPost]
        [ActionName("NewOrderSuccessUpload")]
        public ActionResult NewOrderSuccessUpload_Post(FormCollection form)
        {
            var vm = TheVMFactory.CreateSubmitResultUploadVM();
            TryUpdateModel((dynamic)vm, form);
            if (!ModelState.IsValid)
                return View(vm);

            SubmitInfo = SubmitRegistrationForm(vm);

            return RedirectToAction(this.MethodName(n => n.NewOrderSuccess()));
        }
	}
}