﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Repositories;
using Online.Registration.Web.ServiceProxy;
using Online.Registration.Web.SRService;
using Online.Registration.Web.Areas.ServiceRequest.Factories;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.Web.ViewModels.Common;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.DAL.Helpers;
using System.Reflection;
using System.Linq.Expressions;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Stubs;
using Online.Registration.Web.CustomExtension;
using System.Text;
using System.IO;
using System.Web.Routing;
using Online.Registration.Web.Helper;
using Online.Registration.Web.ViewModels.Inteface;
using System.Net;
using Online.Registration.Web.CommonEnum;
using log4net;
using Online.Registration.Web.NetworkPrintSvc;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.DAL.Models.Common;
using Online.Registration.Web.CustomAuthorize;

namespace Online.Registration.Web.Areas.ServiceRequest.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    [AuthorizeNonDealer]
    public partial class CommonController : Controller
    {
        #region "The Things"
        private SRVMFactory _theVMFactory = null;
        public SRVMFactory TheVMFactory
        {
            get
            {
                if (_theVMFactory == null)
                    _theVMFactory = new SRVMFactory();
                return _theVMFactory;
            }
        }

        private ISRRepository _theSRRepository = null;
        public ISRRepository TheSRRepository
        {
            get
            {
                if (_theSRRepository == null)
                    _theSRRepository = new SRRepository();
                return _theSRRepository;
            }
        }

        private SRModelFactory _theSRModelFactory;
        public SRModelFactory TheSRModelFactory
        {
            get
            {
                if (_theSRModelFactory == null)
                    _theSRModelFactory = new SRModelFactory();
                return _theSRModelFactory;
            }
        }

        private static readonly ILog Logger = LogManager.GetLogger(typeof(CommonController));
        #endregion

        #region "Temp Data"
        public PopupInfoVM PopupInfo
        {
            get { return TempData["PopupInfo"] == null ? null : (dynamic)TempData["PopupInfo"]; }
            set { TempData["PopupInfo"] = value; }
        }
        public SRSubmitResultVM SubmitInfo
        {
            get { return TempData["SubmitInfo"] == null ? null : (dynamic)TempData["SubmitInfo"]; }
            set { TempData["SubmitInfo"] = value; }
        }
        /// <summary>
        /// Flag to check is from summary page
        /// </summary>
        public bool IsFromSummary
        {
            get { return TempData["IsFromSummary"] == null ? false : (Boolean)TempData["IsFromSummary"]; }
            set { TempData["IsFromSummary"] = value; }
        }
        /// <summary>
        /// New Model Error object to pass around between action, where existing model state cannot do this
        /// </summary>
        public string ModelError
        {
            get { return TempData["err"] == null ? string.Empty : (string)TempData["err"]; }
            set { TempData["err"] = value; }
        }
        #endregion

        #region "Shorthand Property"
        public ICustInfoProvider CustInfoSource
        {
            get
            {
                return SRSession.CurrentCustInfo;
            }
            set
            {
                SRSession.CurrentCustInfo = value;
            }
        }
        public SRNewOrderVM NewOrderVM
        {
            get
            {
                return SRSession.CurrentNewOrder;
            }
            set
            {
                SRSession.CurrentNewOrder = value;
            }
        }
        public SRSummaryVM NewOrderSmVM
        {
            get
            {
                return SRSession.CurrentNewOrderSummary;
            }
            set
            {
                SRSession.CurrentNewOrderSummary = value;
            }
        }
        public SRViewOrderVM ViewOrderVM
        {
            get
            {
                return SRSession.CurrentViewOrder;
            }
            set
            {
                SRSession.CurrentViewOrder = value;
            }
        }
        public SRViewOrderListVM ViewOrderListVM
        {
            get
            {
                return SRSession.CurrentViewOrderList;
            }
            set
            {
                SRSession.CurrentViewOrderList = value;
            }
        }
        public SRQueryOrderReq SRViewOrderListReq
        {
            get
            {
                return SRSession.CurrentQueryOrderReq;
            }
            set
            {
                SRSession.CurrentQueryOrderReq = value;
            }
        }
        #endregion

        #region "Private Methods"
        [NonAction]
        private void AbjustControllerNameByFormType(RequestTypes requestTypes)
        {
            //This method will abjust the common controller name to correct form controller name to loop up correct view
            if (this.RouteData.Values["controller"].ToString().ToLower() == "common")
                this.RouteData.Values["controller"] = requestTypes.ToControllerName();
        }

        [NonAction]
        private SRSubmitResultVM Submit(SRSummaryVM NewOrderSmVM)
        {
            using (var proxy = new SRServiceProxy())
            {
                var order = TheSRModelFactory.CreateOrder(NewOrderSmVM);
                order.Detail = TheSRModelFactory.CreateOrderDetail((dynamic)NewOrderSmVM);
                var req = new SRSaveOrderReq()
                {
                    Order = order
                };
                var result = proxy.SaveOrder(req);
                return TheVMFactory.CreateSubmitResultVM(NewOrderSmVM,order, result);
            }
        }

        [NonAction]
        private SRSubmitResultVM SubmitRegistrationForm(SRSubmitResultUploadVM vm)
        {
            using (var proxy = new SRServiceProxy())
            {
                var scanForm = TheSRModelFactory.CreateVerificationDoc(vm);
                var req = new SRSaveDocsReq()
                {
                    OrderId = vm.OrderId,
                    Docs = new[] { scanForm }
                };
                var result = proxy.SaveDocs(req);
                return TheVMFactory.CreateSubmitResultVM(vm, result);
            }
        }

        [NonAction]
        private SROrderMaster RetrieveOrder(string id)
        {
            SROrderMaster order = null;
            using (var proxy = new SRServiceProxy())
            {
                var resp = proxy.FindOrderById(new SRFindOrderReq()
                {
                    OrderId = id
                });
                if (resp.Content != null)
                    order = resp.Content as SROrderMaster;
                else
                    ModelState.AddModelError(string.Empty, string.Format("Fail to retrieve order : {0}", resp.Message));
            }
            return order;
        }

        [NonAction]
        private Boolean UpdateLock(SRViewOrderVM vm)
        {
            bool success = false;
            using (var proxy = new SRServiceProxy())
            {
                var resp = proxy.ChangeOrderLock(new SRChangeLockReq()
                {
                    OrderId = vm.OrderId,
                    LockBy = vm.IsLocked ? vm.LoginId : null
                });
                if (resp.Content != null)
                    success = Convert.ToBoolean(resp.Content);
                else
                    ModelState.AddModelError(string.Empty, string.Format("Fail to update order lock : {0}", resp.Message));
            }
            return success;
        }

        [NonAction]
        private Boolean UpdateOrderStatus(SRViewOrderVM vm)
        {
            bool success = false;
            //Save status
            using (var proxy = new SRServiceProxy())
            {
                var resp = proxy.UpdateOrderStatus(new SRUpdateStatusReq()
                {
                    UpdateContent = TheSRModelFactory.CreateUpdateStatusInfo((dynamic)vm)
                });
                if (resp.Content != null)
                    success = Convert.ToBoolean(resp.Content);
                else
                {
                    success = false;
                    ModelState.AddModelError(string.Empty, string.Format("Fail to update order status : {0}", resp.Message));
                }
            }
            if (!success) return success;
            if (vm.Status == SRStatus.Completed)
            {
                if(vm.SaveContract){
                    //Save pdf
                    success = SaveContractPdf(vm);
                }
                if (!success) return success;
                //Upload to icontract
                success = UploadToIContract(vm);
            }
            
            return success;
        }

        [NonAction]
        private Boolean SaveContractPdf(SRViewOrderVM vm)
        {
            bool success = false;
            var pdf = GetPrintFormPdf(vm.OrderId);
            using (var proxy = new SRServiceProxy())
            {
                var resp = proxy.SaveDocs(new SRSaveDocsReq()
                {
                    OrderId = vm.OrderId,
                    Docs = new[] { TheSRModelFactory.CreateVerificationDoc(vm, pdf, DocTypes.Contract) }
                });
                var result = Convert.ToBoolean(resp.Content);
                if (!result)
                    ModelState.AddModelError(string.Empty, string.Format("Fail to save contract {0} to iContract : {1}", vm.OrderId, resp.Message));
                else
                    success = true;
            }
            return success;
        }

        [NonAction]
        public byte[] GetPrintFormPdf(string orderId)
        {
            var html = this.GetActionResultHtml("PrintOrder", new { id = orderId });
            return html.ToPdf();
        }

        [NonAction]
        private bool UploadToIContract(SRViewOrderVM vm)
        {
            var order = RetrieveOrder(vm.OrderId);
            bool success = false;
            var reqs = TheSRModelFactory.CreateIContractReqs(order);
            using (var proxy = new iContractProxy())
            {
                foreach (var req in reqs)
                {
                    var resp = proxy.UploadFiletoIcontract(req);
                    if (!string.IsNullOrEmpty(resp))
                        success = true;
                    else
                        ModelState.AddModelError(string.Empty, string.Format("Fail to upload {0} to iContract : {1}", order.OrderId, resp));
                }
            }
            return success;
        }
        #endregion

        /// <summary>
        /// Receive order id and query from server to get know what form type
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ViewOrder(string id)
        {
            var order = RetrieveOrder(id);
            if (order == null)
            {
                //Cannot get order, something wrong
                var ctrl = new DashboardController();
                ModelError = string.Format("Cannot find order for {0}, could be record deleted", id);
                return RedirectToAction(ctrl.MethodName(n => n.ViewRequestList()), ctrl.GetName());
            }
            ViewOrderVM = TheVMFactory.CreateViewOrderVM(this, order);
            AbjustControllerNameByFormType(ViewOrderVM.RequestType);
            return View(ViewOrderVM);
        }

        /// <summary>
        /// Update status for order
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("ViewOrder")]
        public ActionResult ViewOrder_Post(FormCollection form)
        {
            var orVM = ViewOrderVM.CloneObj();
            if (ViewOrderVM == null)
            {
                ModelState.AddModelError(string.Empty, "You cannot change order status before open it");
                var ctrl = new DashboardController();
                return RedirectToAction(ctrl.MethodName(n => n.ViewRequestList()), ctrl.GetName());
            }
            AbjustControllerNameByFormType(ViewOrderVM.RequestType);
            TryUpdateModel((dynamic)ViewOrderVM, form);
            //check lock before edit
            var latestOrder = RetrieveOrder(ViewOrderVM.OrderId);
            if (latestOrder.LockBy != ViewOrderVM.LockBy)
            {
                //Error
                ModelError = "Conflicting lock detected. Please try again";
                //Refresh page
                return RedirectToAction(this.MethodName(n => n.ViewOrder("")), new { id = ViewOrderVM.OrderId });
            }
            if (!ModelState.IsValid)
            {
                //Reset status
                ViewOrderVM.Status = SRStatus.New;
                return View(ViewOrderVM);
            }

            //Save to db
            var result = false;
            try
            {
                result = UpdateOrderStatus(ViewOrderVM);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, string.Format("Update status fail. {0}", ex.Message));
            }
            if (!result)
            {
                //Restore
                ViewOrderVM = orVM;
                return View(ViewOrderVM);
            }
            //Refresh page
            var dashCtrl = new DashboardController();
            return RedirectToAction(dashCtrl.MethodName(n => n.ViewRequestList()), dashCtrl.GetName());
        }

        [HttpPost]
        public ActionResult ChangeLock(FormCollection form)
        {
            if (ViewOrderVM == null)
            {
                ModelState.AddModelError(string.Empty, "You cannot change lock without open it");
                var ctrl = new DashboardController();
                return RedirectToAction(ctrl.MethodName(n => n.ViewRequestList()), ctrl.GetName());
            }
            TryUpdateModel((dynamic)ViewOrderVM, form);
            var result = UpdateLock(ViewOrderVM);
            //Refresh page
            return RedirectToAction(this.MethodName(n => n.ViewOrder("")), new { id = ViewOrderVM.OrderId });
        }

        [HttpPost]
        public ActionResult ChangeLockFromList(string id)
        {
            if (string.IsNullOrEmpty(id))
                return Json(new { ErrorMsg = "Id is null." });

            try
            {
                var success = false;
                using (var proxy = new SRServiceProxy())
                {
                    var resp = proxy.ChangeOrderLock(new SRChangeLockReq()
                    {
                        OrderId = id,
                        LockBy = ViewOrderListVM.LoginId
                    });
                    if (resp.Content != null)
                        success = Convert.ToBoolean(resp.Content);
                    else
                        return Json(new { ErrorMsg = string.Format("Fail to update order lock : {0}", resp.Message) });
                }
                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return Json(new { ErrorMsg = ex.Message });
            }
        }

        [HttpPost]
        public ActionResult NetworkPrint(FormCollection form)
        {
            try
            {
                TryUpdateModel((dynamic)ViewOrderVM, form);
                //Convert print form to html file
                var html = this.GetActionResultHtml("PrintOrder", new { id = ViewOrderVM.OrderId });
                
                //send to networkprint
                var result = ViewOrderVM.PrintFile(html);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(TheVMFactory.CreateNetworkPrintResultVM(ex));
            }
        }

        public ActionResult DownloadDocuments(string id)
        {
            var order = RetrieveOrder(id);
            var fileVM = TheVMFactory.CreateFileResultVM(order);
            return File(fileVM.File, fileVM.ContentType, fileVM.FileName);
        }

        /// <summary>
        /// Print order
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PrintOrder(string id)
        {
            SROrderMaster order = null;
            using (var proxy = new SRServiceProxy())
            {
                var resp = proxy.FindOrderById(new SRFindOrderReq()
                {
                    OrderId = id
                });
                if (resp.Content == null)
                    return View();//Return Default view to display error
                order = resp.Content as SROrderMaster;
            }
            var printOrderVM = TheVMFactory.CreatePrintOrderVM(order);
            AbjustControllerNameByFormType(printOrderVM.RequestType);
            return View(printOrderVM);
        }
    }
}