﻿//-----------------------------------------------------------------------------------
//  Developer   :Jazz Tong
//  View Name   : Dashboard Controller.cs
//  Purpose     : Controller for Dashboard related
//  Version Control:
//  Version     Date         Change Made
//  1.0         12 Oct 2015  Controller created
//-----------------------------------------------------------------------------------

using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common;
using Online.Registration.Web.ServiceProxy;
using Online.Registration.Web.SRService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.DAL.Helpers;
using Online.Registration.Web.ViewModels.Common;
using Newtonsoft.Json;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.Web.Helper;
using Online.Registration.Web.CustomExtension;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace Online.Registration.Web.Areas.ServiceRequest.Controllers
{
    public class DashboardController : CommonController
    {
        [HttpGet]
        public ActionResult ViewRequestList()
        {
            ViewOrderListVM = TheVMFactory.CreateViewOrderListVM();
            if(ViewOrderListVM.CreateByList==null ||ViewOrderListVM.CreateByList.Count==0)
            {
                ViewOrderListVM.CreateByList = GetCreateByList();
            }
            return View(ViewOrderListVM);
        }

        [HttpPost]
        [ActionName("ViewRequestList")]
        public ActionResult ViewRequestList_Post(FormCollection form)
        {
            if (ViewOrderListVM == null)
            {
                ModelState.AddModelError(string.Empty, "You cannot query order before go to search page");
                RedirectToAction(this.MethodName(n => n.ViewRequestList()));
            }
            TryUpdateModel((dynamic)ViewOrderListVM, form);
            
            return View(new SRViewOrderListVM());
        }

        public ActionResult AjaxGetJsonData(string json)
        {
            if(string.IsNullOrEmpty(json))
                return Json(new {ErrMsg="Wrong json data."});
            json = json.Replace("[\"true\",\"false\"]", "true")
                        .Replace("[\"false\",\"false\"]", "false");
            try
            {
                ViewOrderListVM = (SRViewOrderListVM)JsonConvert.DeserializeObject(json, typeof(SRViewOrderListVM),new JsonSerializerSettings(){
                    Error = HandleDeserializationError,
                    Converters = new List<JsonConverter>() { 
                        new IsoDateTimeConverter { DateTimeFormat = ViewOrderListVM.DateFormatServer } 
                    }
                });
            }
            catch (Exception ex)
            {
                return Json(new { ErrMsg = "Cannot deserialize data." +ex.Message });
            }

            var result = QueryResult(ViewOrderListVM);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public void HandleDeserializationError(object sender, ErrorEventArgs errorArgs)
        {
            var currentError = errorArgs.ErrorContext.Error.Message;
            errorArgs.ErrorContext.Handled = true;
        }

        public ActionResult AjaxDownloadQueryResult(){
            if(SRViewOrderListReq==null)
                return Json(new { ErrMsg = "Fail to download query result"});
            //set full record
            SRViewOrderListReq.QueryDto.FullRecord = true;
            var result = QueryResultDownload(SRViewOrderListReq);
            var jsonData = JsonConvert.SerializeObject(ToExcelList(result));
            var path=ExcelHelper.Export(jsonData);
            return Json(path);
        }

        private object ToExcelList(List<SRQueryResultItemVM> result)
        {
            return result.Select(n=>new {
                SR_No=n.OrderId,
                SR_Type=n.RequestTypeDisplay,
                ID_Card_No=n.IdCardNo,
                MSISDN=n.MSISDN,
                Customer_Name=n.FullName,
                Created_Date=n.CreateDt,
                Created_By=n.CreateBy,
                Last_Updated_Date=n.UpdateDtDisplay,
                Last_Updated_By=n.UpdateBy,
                Queue_No=n.QueueNo,
                Status=n.StatusDisplay,
                Lock_By=n.LockBy
            }).ToList();
        }

        #region Private Methods
        private List<SRQueryResultItemVM> QueryResultDownload(SRQueryOrderReq req)
        {
            using (var proxy = new SRServiceProxy())
            {
                var resp = proxy.QueryOrder(req);
                if (resp.Content != null)
                {
                    var lst = (resp.Content as SRQueryMaster[]).ToList();
                    return TheVMFactory.CreateQueryResultItemListVM(lst);

                }
                return new List<SRQueryResultItemVM>();
            }
        }

        private DtResultVM<SRQueryResultItemVM> QueryResult(SRViewOrderListVM vm)
        {
            var req = new SRQueryOrderReq() {
                page=vm.Start/vm.Length,
                pageSize=vm.Length,
                QueryDto=TheSRModelFactory.CreateQueryDto(vm)
            };
            SRViewOrderListReq = req.CloneObj();
            using(var proxy=new SRServiceProxy()){
                var resp = proxy.QueryOrder(req);
                if (resp.Content != null)
                {
                    var lst = (resp.Content as SRQueryMaster[]).ToList();
                    var result = TheVMFactory.CreateDtResultVM(lst, resp.TotalCount, vm);
                    return result;
                }
                return TheVMFactory.CreateDtResultVM(vm);
            }
        }

        private List<string> GetCreateByList()
        {
            var result = new List<string>();
            using(var proxy=new SRServiceProxy()){
                var resp = proxy.GetDistinctCreateByList(new SRGetCreateByReq(){
                    CenterOrgId=SRSession.CurrentLoginUser.User.OrgID,
                    LoginId=SRSession.CurrentLoginUser.UserName
                });
                if (resp.Content != null)
                    result = (resp.Content as CollectionContract).Cast<string>().ToList();
            }
            return result;
        }
        #endregion
    }
}