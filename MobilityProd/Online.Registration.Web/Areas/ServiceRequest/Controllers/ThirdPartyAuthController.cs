﻿//-----------------------------------------------------------------------------------
//  Developer   : Ashley Ow
//  File Name   : ThirdPartyAuthController.cs
//  Purpose     : Controller for Third Party Authorization Service Request
//  Version Control:
//  Version     Date         Change Made
//  1.0         10 Oct 2015  Controller created
//-----------------------------------------------------------------------------------

using Online.Registration.Web.Areas.ServiceRequest.DataAdapter;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.Web.CommonEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.Interface;
using System.ComponentModel.DataAnnotations;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common;

namespace Online.Registration.Web.Areas.ServiceRequest.Controllers
{
    public class ThirdPartyAuthController : CommonController
    {
        [HttpGet]
        public ActionResult AjaxVerifyCustomer(SRVerifyCustomerVM vm)
        {
            if (!ModelState.IsValid){
                return Json(ModelState.ToErrorJson(), JsonRequestBehavior.AllowGet);
            }
            var idType = vm.IdType;
            var idNo = vm.IdNo;
            //Get cust info
            CustInfoSource = new CustInfoByICAdapter(idType.ToIdTypeKenanEnumStr(), idNo);
            var custInfoVM = TheVMFactory.CreateCustInfoVM(CustInfoSource);
            //Get bre info
            var breChecker = GetBreChecker(idType,idNo,CustInfoSource);
            var breErrors = breChecker.GetBreErrors();
            var newOrder = NewOrderVM as ThirdPartyAuthNewOrderVM;
            newOrder.ThirdPartyBreErrors = breErrors;
            newOrder.ThirdPartyBiometricPass = breChecker.BiometricPass;
            newOrder.ThirdPartyInfoReadOnly = custInfoVM != null && !string.IsNullOrEmpty(custInfoVM.Name);
            newOrder.QueryCustomer = true;
            //Output array of object with custinfo and breerrors
            return Json(new object[]{
                custInfoVM,
                newOrder.ThirdPartyBreErrors.ToDictionary(n => n.Key.ToString(), n => n.Value)
            },
            JsonRequestBehavior.AllowGet);
        }

        private IBreInfoProvider GetBreChecker(string idType, string idNo,ICustInfoProvider CustInfoSource)
        {
            //Only require biometric check for thirdparty Auth
            var breChecker= new BreCheckerAdapter(idType,idNo,new[] { BreType.Biometric });
            breChecker.InitBiometricParam((CustInfoSource as CustInfoByICAdapter).GetBiometricData());
            return breChecker;
        }
    }
}
