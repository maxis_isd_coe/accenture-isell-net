﻿using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Online.Registration.Web.Areas.ServiceRequest.Interface
{
    public interface ICustInfoProvider
    {
        string CustomerName { get; }

        string IdType { get; }
          
        string IdNum { get; }

        bool IsExistingCust { get; }

        bool IsBioMetricMatched { get; }

        DateTime? DateOfBirth { get; }

        string Gender { get; }

        int LanguageID { get; }

        int RaceID { get; }

        string RaceFrmIc { get; }

        int NationalityID { get; }

        string Email { get; }

        string ContactNumber { get; }

        string AlternateContactNumber { get; }

        string AddressLine1 { get; }

        string AddressLine1FrmIc { get; }

        string AddressLine2 { get; }

        string AddressLine2FrmIc { get; }

        string AddressLine3 { get; }

        string AddressLine3FrmIc { get; }

        string PostCode { get; }

        string PostCodeFrmIc { get; }

        string City { get; }

        string CityFrmIc { get; }

        string State { get; }

        string StateFrmIc { get; }

        string Msisdn { get; }

        string CustomerCurrentPlan { get; }

        string AccountNumber { get; }

        string CustomerNameFrmIc { get;}

        string IdNumFrmIc { get; }
        /// <summary>
        /// Extract gender from Ic number last digit
        /// </summary>
        string GenderFromId { get;}
        /// <summary>
        /// Extract dob from ic number
        /// </summary>
        DateTime? DateOfBirthFrmIc {get;}
        /// <summary>
        /// Extract nationality field from biometric
        /// </summary>
        string NationalityFromIC { get;}
        /// <summary>
        /// Extract nationality id from IC Number pattern
        /// </summary>
        int NationalityIDFromIC { get;}
        /// <summary>
        /// List of service info for postpaid
        /// </summary>
        List<ServiceInfoVM> PostServiceInfoList { get;}
        /// <summary>
        /// List of service info for prepaid
        /// </summary>
        List<ServiceInfoVM> PreServiceInfoList {get;}
        /// <summary>
        /// List of service info for all mobile (pre,post)
        /// </summary>
        List<ServiceInfoVM> MobileServiceInfoList {get;}

        string Salutation { get; }
    }
}
