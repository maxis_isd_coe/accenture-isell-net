﻿using Online.Registration.Web.CommonEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.Interface
{
    public interface IBreInfoProvider
    {
        Dictionary<BreType, string> GetBreErrors();

        bool BiometricPass { get;}

        bool OutstandingCheckPass { get; }

        Dictionary<string, decimal> OutstandingBillAccountsAmount { get; }

        bool WriteoffCheckPass { get;}

        bool DDMFCheckPass { get;}

        bool TtlContractCheckPass { get; }

        bool TtlLineCheckPass { get; }
    }
}