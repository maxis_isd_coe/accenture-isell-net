﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.Enums
{
    public enum ServiceTypes
    {
        PrepaidOnly = 1,
        PostpaidOnly = 2,
        All = 3
    }
}