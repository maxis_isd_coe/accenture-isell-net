﻿using Online.Registration.DAL.Models;
using Online.Registration.Web.AMRSvc;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Helper;
using Online.Registration.Web.Models;
using Online.Registration.Web.SubscriberHistoryService;
using Online.Registration.Web.SubscriberICService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.SessionState;

namespace Online.Registration.Web.Areas.ServiceRequest.DataAdapter
{
    [Serializable()]
    public class BaseDataAdapter
    {
        protected List<IndividualResponse> CurrentTelcoHistory{
            get{
                return SRSession.CurrentTelcoHistory;
            }
        }
        protected int TotalAllowedContact{
            get{
                return int.Parse(WebHelper.Instance.checkTotalContract());
            }
        }
        protected int CurrentExistingContractCount{
            get{
                return SRSession.CurrentContractCount;
            }
        }
        protected int CurrentExistingLineCount{
            get{
                return DropFourObj.getExistingLineCount();
            }
        }
        protected int MaxLineAllowed{
            get{
                return int.Parse(ConfigurationManager.AppSettings["MaxOrderTotalline"]);
            }
        }
        protected DropFourObj DropFourObj{
            get{
                return SRSession.DropFourObj;
            }
        }
        protected string CurrentOutstanding{
            get{
                return SRSession.CurrentOutstanding;
            }
        }
        protected string CurrentBiometricMsg{
            get{
                return SRSession.CurrentBiometricMsg;
            }
        }
        protected List<BreStatusDetails> CurrentBreStatusDtls{
            get{
                return SRSession.CurrentBreStatusDtls;
            }
        }

        protected virtual string OutstandingAmountDtls{
            get
            {
                return SRSession.CurrentOutstandingBillAmountDtls;
            }
        }

        protected virtual retrieveAcctListByICResponse CurrentPPIDInfo {
            get {
                return SRSession.CurrentPPIDInfo;
            }
        }

        protected virtual retrieveAcctListByICResponse AllCustomerInfo 
        {
            get
            {
                return SRSession.CurrentAllCustomerInfo;
            }
        }
        
        protected string CurrentPPIDFlag{
            get{
                return SRSession.CurrentPPIDFlag;
            }
        }
        protected virtual RetrieveMyKadInfoRp CurrentCustBiometricData
        {
            get{
                return SRSession.CurrentCustBiometricData;
            }
        }
        protected string CurrentFraudMessage{
            get{
                return SRSession.CurrentFraudMessage;
            }
        }
        protected Items FirstPPIDInfoActiveItem
        {
            get
            {
                var result=CurrentPPIDInfo==null?null:CurrentPPIDInfo.itemList
                    .Where(x =>x.IsActive)
                    .FirstOrDefault();
                return result;
            }
        }
        protected CustomizedCustomer FirstActiveCust
        {
            get
            {
                var cust=FirstPPIDInfoActiveItem==null?null:FirstPPIDInfoActiveItem.Customer;
                if (cust == null)
                    cust = AllCustomerInfo==null?null:AllCustomerInfo.itemList.First().Customer;
                return cust;
            }
        }
        protected CustomizedAccount FirstActiveAcc
        {
            get
            {
                return FirstPPIDInfoActiveItem==null?null:FirstPPIDInfoActiveItem.Account;
            }
        }
        /// <summary>
        /// List of Item that is active, and has principle and supplimental indicator
        /// </summary>
        protected IEnumerable<Items> MobileActiveItem
        {
            get
            {
                return CurrentPPIDInfo == null ? null : CurrentPPIDInfo.itemList
                    .Where(x =>
                    x.IsActive && x.ServiceInfoResponse.prinSuppInd != "")
                    .ToList();
            }
        }
        /// <summary>
        /// List of item that is active, and is Post plan
        /// </summary>
        protected IEnumerable<Items> PostplanActiveItem{
            get{
                return CurrentPPIDInfo == null ? null : CurrentPPIDInfo.itemList
                    .Where(x =>
                        x.IsActive && x.ServiceInfoResponse.lob == "POSTGSM" && x.ServiceInfoResponse.prinSuppInd!="")
                        .ToList();
            }
        }
        /// <summary>
        /// List of item that is active and is prepaid
        /// </summary>
        protected IEnumerable<Items> PreplanActiveItem
        {
            get{
                return CurrentPPIDInfo == null ? null : CurrentPPIDInfo.itemList
                    .Where(x =>
                        x.IsActive && x.ServiceInfoResponse.lob == "PREGSM")
                        .ToList();
            }
        }
        
    }
}
