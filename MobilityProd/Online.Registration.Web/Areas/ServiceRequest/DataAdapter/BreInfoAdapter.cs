﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Areas.ServiceRequest.DataAdapter
{
    [Serializable()]
    public class BreInfoAdapter:BaseDataAdapter,IBreInfoProvider
    {
        private Dictionary<BreType,string> _breErrors=new Dictionary<BreType, string>();

        public Dictionary<BreType, string> GetBreErrors()
        {
            _breErrors = new Dictionary<BreType, string>();
            foreach (BreType bre in BreCheckTypes){
                string msg = GetErrorsMsg(bre);
                if (!string.IsNullOrEmpty(msg))
                    _breErrors.Add(bre, msg);
            }
            return _breErrors;
        }

        private string GetErrorsMsg(BreType breType)
        {
            var method = this.GetType().GetMethod(string.Format("Get{0}Msg", breType.ToString()));
            if (method == null)
                throw new ApplicationException(string.Format("method Get{0}Msg not found, please implement",breType.ToString()));
            return (string)method.Invoke(this,null);
        }

        public virtual string GetBiometricMsg()
        {
            var find = CurrentBreStatusDtls==null?null:CurrentBreStatusDtls.Where(n => n.Code == "Biometric" && !n.KenanStatus).FirstOrDefault();
            if (find == null) return string.Empty;
            return string.Format("Biometric : {0}",CurrentBiometricMsg);
        }

        public virtual string GetOutstandingCreditCheckMsg()
        {
            var find = CurrentBreStatusDtls == null ? null : CurrentBreStatusDtls.Where(n => n.Code == "OutstandingCheck" && !n.KenanStatus).FirstOrDefault();
            if (find == null) return string.Empty;
            return string.Format("Outstanding Check : {0}", CurrentOutstanding.Replace("(", "(RM ").Replace(",", " , ").TrimEnd(','));
        }

        public virtual string GetTotalLineCheckMsg()
        {
            var find = CurrentBreStatusDtls == null ? null : CurrentBreStatusDtls.Where(n => n.Code == "TotalLineCheck" && !n.KenanStatus).FirstOrDefault();
            if (find == null)
                return string.Empty;
            if(CurrentExistingLineCount < MaxLineAllowed)
                return string.Empty;
            return string.Format("Total Line Check : {0}", CurrentExistingLineCount);
        }

        public virtual string GetContractCheckMsg()
        {
            var find = CurrentBreStatusDtls == null ? null : CurrentBreStatusDtls.Where(n => n.Code == "ContractCheck" && !n.KenanStatus).FirstOrDefault();
            if (find == null)
                return string.Empty;
            if (CurrentExistingContractCount < TotalAllowedContact)
                return string.Empty;
            return string.Format("Contract Check : {0}", CurrentExistingContractCount);
        }

        public virtual string GetDDMFCheckMsg()
        {
            var find = CurrentBreStatusDtls == null ? null : CurrentBreStatusDtls.Where(n => n.Code == "DDMFCheck" && !n.KenanStatus).FirstOrDefault();
            if (find == null || CurrentTelcoHistory==null || CurrentTelcoHistory.Count==0)
                return string.Empty;
            var msg = "";
            foreach (var ddmf in CurrentTelcoHistory)
                msg = msg + "Submit by : " + ddmf.Company + "<br/>Acct No : " + ddmf.dfAccount + "<br/>";
            return string.Format("DDMF Check : {0}", msg);
        }

        public virtual string GetWriteoffCheckMsg()
        {
            if (CurrentFraudMessage == Constants.WRITTEN_OFF)
                return string.Format("Writeoff Check : {0}", CurrentFraudMessage.Split(new char[]{'.'}).First());
            return string.Empty;
        }

        private IEnumerable<BreType> _breCheckTypes;
        public virtual IEnumerable<BreType> BreCheckTypes { 
            get {
                if(_breCheckTypes==null)
                    _breCheckTypes = new List<BreType>() { 
                        BreType.Biometric,
                        BreType.OutstandingCreditCheck,
                        BreType.TotalLineCheck,
                        BreType.ContractCheck,
                        BreType.DDMFCheck,
                        BreType.WriteoffCheck
                    };
                return _breCheckTypes;
            } 
        }

        public virtual bool BiometricPass {
            get { return !_breErrors.Keys.Contains(BreType.Biometric); }
        }

        public virtual bool OutstandingCheckPass{
            get { return !_breErrors.Keys.Contains(BreType.OutstandingCreditCheck); }
        }

        public virtual Dictionary<string, decimal> OutstandingBillAccountsAmount{
            get
            {
                var dic = new Dictionary<string, decimal>();
                string OutstandingDataFromSession;

                OutstandingDataFromSession = OutstandingAmountDtls;

                if (OutstandingDataFromSession != "")
                {
                    //split by comma
                    string[] OutstandingBillAmountArray = OutstandingDataFromSession.Split(',');
                    foreach (var field in OutstandingBillAmountArray)
                    {
                        //split by ( and )
                        var detail = field.Split('(', ')');
                        dic.Add(detail[0], decimal.Parse(detail[1]));
                    }
                }
                return dic;
            }
        }

        public virtual bool WriteoffCheckPass{
            get { return !_breErrors.Keys.Contains(BreType.WriteoffCheck); }
        }

        public virtual bool DDMFCheckPass{
            get { return !_breErrors.Keys.Contains(BreType.DDMFCheck); }
        }

        public virtual bool TtlContractCheckPass{
            get { return !_breErrors.Keys.Contains(BreType.ContractCheck); }
        }
        
        public virtual bool TtlLineCheckPass{
            get { return !_breErrors.Keys.Contains(BreType.TotalLineCheck); }
        }
    }
}