﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.Helper;
using Org.BouncyCastle.Asn1.Ocsp;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.ViewModels.Common;
using Online.Registration.Web.Areas.ServiceRequest.Factories;

namespace Online.Registration.Web.Areas.ServiceRequest.DataAdapter
{
    /// <summary>
    /// Customer Info Adapter, will provide information from various data source
    /// </summary>
    [Serializable()]
    public class CustInfoAdapter:BaseDataAdapter,ICustInfoProvider
    {
        public string CustomerName
        {
            get {return FirstActiveCust!=null?FirstActiveCust.CustomerName:"";}
        }

        public string IdType
        {
            get {
                var _idtype= FirstActiveAcc!=null?FirstActiveAcc.IDType:"";
                if (string.IsNullOrEmpty(_idtype))
                    _idtype = FirstActiveCust.NewIC != "" ? "NEWIC" : "OLDIC";
                return _idtype;
            }
        }

        public string IdNum
        {
            get {
                var _idNum= FirstActiveAcc!=null?FirstActiveAcc.IDNumber:"";
                if (string.IsNullOrEmpty(_idNum))
                    _idNum = FirstActiveCust.NewIC != "" ? FirstActiveCust.NewIC : FirstActiveCust.OldIC;
                return _idNum;
            }
        }

        public DateTime? DateOfBirth 
        {
            get { 
                var dob=FirstActiveCust != null ? FirstActiveCust.Dob : "";
                if(string.IsNullOrEmpty(dob)){
                    //get dob from ic
                    if(!string.IsNullOrEmpty(FirstActiveCust.NewIC)){
                        var ic = FirstActiveCust.NewIC;
                        return new DateTime(int.Parse("19" + ic.Substring(0, 2)), int.Parse(ic.Substring(2, 2)), int.Parse(ic.Substring(4, 2)));
                    }
                    return null;
                }
                return new DateTime(
                    int.Parse(dob.Substring(0, 4)), 
                    int.Parse(dob.Substring(4, 2)), 
                    int.Parse(dob.Substring(6, 2)));
            }
        }

        public string CustomerCurrentPlan
        {
            get { return CurrentPPIDInfo != null ? CurrentPPIDInfo.itemList[0].Packages[0].PackageDesc : ""; }
        }
        public string Gender
        {
            get { return FirstActiveCust != null ? FirstActiveCust.Genders : ""; }
        }

        public int LanguageID
        {
            get { return FirstActiveCust != null ? FirstActiveCust.LanguageID : 0; }
        }

        public int NationalityID
        {
            get { return FirstActiveCust != null ? FirstActiveCust.NationalityID : 0; }
        }

        public int RaceID
        {
            get { 
                var _raceid= FirstActiveCust != null ? FirstActiveCust.RaceID : 0;
                if (_raceid == 0 && !string.IsNullOrEmpty(FirstActiveCust.Races))
                    _raceid = int.Parse(FirstActiveCust.Races);
                return _raceid;
            }
        }

        public string RaceFrmIc
        {
            get { return CurrentCustBiometricData != null ? CurrentCustBiometricData.Race : ""; }
        }

        public DateTime? DateOfBirthFrmIc
        {
            get{
                //biometric dob format DD/MM/YYYY
                var dob=CurrentCustBiometricData.DOB.Replace("/","");
                DateTime? dobDt=new DateTime(
                    int.Parse(dob.Substring(0, 4)),
                    int.Parse(dob.Substring(4, 2)),
                    int.Parse(dob.Substring(6, 2)));
                return CurrentCustBiometricData != null && !string.IsNullOrEmpty(dob)? dobDt : null;
            }
        }

        public string NationalityFromIC{
            get{return CurrentCustBiometricData!=null ?CurrentCustBiometricData.Nationality:"";}
        }

        public string Email
        {
            get { return FirstActiveCust != null ? FirstActiveCust.EmailAddrs : ""; }
        }

        public string ContactNumber
        {
            get { return FirstActiveCust != null ? Util.FormatContactNumber(FirstActiveCust.Cbr) : ""; }
        }

        public string AlternateContactNumber
        {
            get { return FirstActiveCust != null ? FirstActiveCust.AlternateContactNo : ""; }
        }

        public string AddressLine1
        {
            get { return FirstActiveCust != null ? FirstActiveCust.Address1 : ""; }
        }

        public string AddressLine1FrmIc
        {
            get { return CurrentCustBiometricData != null ? CurrentCustBiometricData.Address1 : ""; }
        }

        public string AddressLine2
        {
            get { return FirstActiveCust != null ? FirstActiveCust.Address2 : ""; }
        }
        
        public string AddressLine2FrmIc
        {
            get { return CurrentCustBiometricData != null ? CurrentCustBiometricData.Address2 : ""; }
        }

        public string AddressLine3
        {
            get { return FirstActiveCust != null ? FirstActiveCust.Address3 : ""; }
        }

        public string AddressLine3FrmIc
        {
            get { return CurrentCustBiometricData != null ? CurrentCustBiometricData.Address3 : ""; }
        }

        public string PostCode
        {
            get { return FirstActiveCust != null ? FirstActiveCust.PostCode : ""; }
        }

        public string PostCodeFrmIc
        {
            get { return CurrentCustBiometricData != null ? CurrentCustBiometricData.Postcode : ""; }
        }

        public string City
        {
            get { return FirstActiveCust != null ? FirstActiveCust.City : ""; }
        }

        public string CityFrmIc
        {
            get { return CurrentCustBiometricData != null ? CurrentCustBiometricData.City : ""; }
        }

        public string State
        {
            get { return FirstActiveCust != null ? FirstActiveCust.State : ""; }
        }

        public string StateFrmIc
        {
            get { return CurrentCustBiometricData != null ? CurrentCustBiometricData.State : ""; }
        }

        public string Msisdn
        {
            get { return FirstActiveAcc != null ? FirstActiveAcc.Holder : ""; }
        }

        public string AccountNumber
        {
            get { return FirstActiveAcc != null ? FirstActiveAcc.AccountNumber : ""; }
        }

        public virtual bool IsExistingCust
        {
            get {
                //at least one active account
                return AllCustomerInfo != null && AllCustomerInfo.itemList.Where(n => n.IsActive).Count() > 0;
            }
        }

        public bool IsBioMetricMatched
        {
            get { return CurrentCustBiometricData!=null?CurrentCustBiometricData.FingerMatch:false; }
        }

        public string CustomerNameFrmIc
        {
            get { return CurrentCustBiometricData!=null?CurrentCustBiometricData.Name:""; }
        }

        public string IdNumFrmIc
        {
            get{
                if (CurrentCustBiometricData == null) return "";
                return !String.IsNullOrEmpty(CurrentCustBiometricData.NewIC) ? CurrentCustBiometricData.NewIC : CurrentCustBiometricData.OldIC;
            }
        }      

        public string GenderFromId
        {
            get
            {
                if(IdType.ToIDCardTypeValue ()== "1" && !string.IsNullOrEmpty(IdNum))
                {
                    var genderDigit = int.Parse(IdNum.Last().ToString());
                    if (genderDigit % 2 == 0)
                        return "F";
                    return "M";
                }
                return Gender;
            }
        }

        public int NationalityIDFromIC
        {
            get
            {
                //Malaysian
                if(!string.IsNullOrEmpty(FirstActiveCust.NewIC) || !string.IsNullOrEmpty(FirstActiveCust.OldIC))
                    return 1;
                //Other country
                return 132;
            }
        }

        public List<ServiceInfoVM> PostServiceInfoList
        {
            get{
                return GetVMFactory().CreateServiceInfoVMList(PostplanActiveItem);
            }
        }

        public List<ServiceInfoVM> PreServiceInfoList
        {
            get{
                return GetVMFactory().CreateServiceInfoVMList(PreplanActiveItem);
            }
        }

        public List<ServiceInfoVM> MobileServiceInfoList
        {
            get {
                return GetVMFactory().CreateServiceInfoVMList(MobileActiveItem);
            }
        }

        private SRVMFactory GetVMFactory(){
            return new SRVMFactory();
        }

        public string Salutation
        {
            get{return FirstActiveCust != null ? FirstActiveCust.Title : "";}
        }
    }
}