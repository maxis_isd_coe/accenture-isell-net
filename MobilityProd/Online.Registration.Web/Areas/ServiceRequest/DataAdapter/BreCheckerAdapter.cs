﻿using Online.Registration.DAL.Models;
using Online.Registration.Web.AMRSvc;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.SubscriberICService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using SNT.Utility;

namespace Online.Registration.Web.Areas.ServiceRequest.DataAdapter
{
    public class BreCheckerAdapter:BreInfoAdapter
    {
        
        private IEnumerable<BreType> _breCheckTypes;
        private string _idType;
        private string _idNo;
        private retrieveAcctListByICResponse _services;
        private Dictionary<BreType,List<BusinessRuleResponse>> _breResponse;
        private Dictionary<BreType, List<BusinessRuleRequest>> _breRequest;
        public BreCheckerAdapter(string idType, string idNo,BreType[] breTypes)
        {
            _idType = idType;
            _idNo=idNo;
            _breCheckTypes = breTypes.ToList();
        }

        public override IEnumerable<BreType> BreCheckTypes
        {
            get
            {
                return _breCheckTypes;
            }
        }

        /// <summary>
        /// Set services parameter for checking
        /// </summary>
        /// <param name="services"></param>
        public void InitServices(retrieveAcctListByICResponse services){
            _services = services;
        }

        private Dictionary<BreType, List<BusinessRuleResponse>> BreResponse
        {
            get
            {
                if (_breResponse == null)
                    _breResponse = PerformBreCheckFromServer();
                return _breResponse;
            }
        }

        private Dictionary<BreType, List<BusinessRuleRequest>> BreRequest
        {
            get
            {
                if (_breRequest == null)
                    _breRequest = GenerateBreResponse();
                return _breRequest;
            }
        }

        private Dictionary<BreType, List<BusinessRuleRequest>> GenerateBreResponse()
        {
            Dictionary<BreType, List<BusinessRuleRequest>> dic = new Dictionary<BreType, List<BusinessRuleRequest>>();
            BreCheckTypes.ToList().ForEach(n => {
                var lst = new List<BusinessRuleRequest>();
                var reqs = GetRuleReq(n);
                if(reqs!=null && reqs.Count()>0)
                    lst.AddRange(reqs);
                dic.Add(n, lst);
            });
            return dic;
        }

        private IEnumerable<BusinessRuleRequest> GetRuleReq(BreType breType)
        {
            //use private method loopup
            var method = this.GetType().GetMethod(string.Format("Get{0}RuleReq", breType.ToString()),BindingFlags.NonPublic | BindingFlags.Instance);
            if (method == null)
                throw new ApplicationException(string.Format("method Get{0}RuleReq not found, please implement", breType.ToString()));
            return (IEnumerable<BusinessRuleRequest>)method.Invoke(this, null);
        }

        private IDCardType _kenanIdType;
        private IDCardType KenanIdType{
            get{
                if(_kenanIdType==null)
                {
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        _kenanIdType = proxy.IDCardTypeGet(int.Parse(_idType));
                    }
                }
                return _kenanIdType;
            }
        }

        #region BusinessRuleRequest
        private IEnumerable<BusinessRuleRequest> GetContractCheckRuleReq()
        {
            return new[]{new BusinessRuleRequest
            {
                RuleNames = new []{"ContractCheck"},
                totalContractsAllowed = TotalAllowedContact.ToString(),
                IDCardNo = _idNo,
                IDCardType = KenanIdType.KenanCode,
                businessRuleType = BusinessRuleType.ContractCheck
            }};
        }

        private IEnumerable<BusinessRuleRequest> GetOutstandingCreditCheckRuleReq()
        {
            var lst = new List<BusinessRuleRequest>();
            if (AllServices == null) return lst;
            AllServices.itemList.Where(x => x.IsBillable == "Y").Select(x => x.AcctExtId).Distinct().ToList().ForEach(n =>
            {
                lst.Add(new BusinessRuleRequest
                {
                    RuleNames = new []{"OutstandingCreditCheck"},
                    kenanExtAcctId = n,
                    IDCardNo = _idNo,
                    IDCardType = KenanIdType.KenanCode,
                    businessRuleType = BusinessRuleType.OutstandingCreditCheck
                });
            });
            return lst;
        }

        private IEnumerable<BusinessRuleRequest> GetBiometricRuleReq()
        {
            //Do in diff manner
            return null;
        }

        private IEnumerable<BusinessRuleRequest> GetWriteoffCheckRuleReq()
        {
            return new[]{new BusinessRuleRequest(){
                RuleNames = new []{"FxInternalCheck"},
                IDCardNo = _idNo,
                IDCardType = KenanIdType.KenanCode,
            }};
        }

        private IEnumerable<BusinessRuleRequest> GetDDMFCheckRuleReq()
        {
            return null;
        }

        private IEnumerable<BusinessRuleRequest> GetTotalLineCheckRuleReq()
        {
            //Do in diff manner
            return null;
        }
        #endregion

        private Dictionary<BreType, List<BusinessRuleResponse>> PerformBreCheckFromServer()
        {
            var dic = new Dictionary<BreType, List<BusinessRuleResponse>>();
            BreRequest.ToList().ForEach(n => {
                using (var proxy = new KenanServiceProxy())
                {
                    var lst=new List<BusinessRuleResponse>();
                    dic.Add(n.Key, proxy.BusinessRuleOneTimeCheck(n.Value));
                }
            });
            //filter req with data
            return dic.Where(n=>n.Value.Count>0).ToDictionary(n=>n.Key,n=>n.Value);
        }

        public retrieveAcctListByICResponse AllServices{
            get{
                if (_services == null){
                    //need retrieve again by ic
                }
                //if (_services == null)
                //    throw new Exception("BreCheckerAdapter missing require paramter [Services]");
                return _services;
            }
        }

        private List<BusinessRuleResponse> GetRuleResp(BreType breType)
        {
            if (BreResponse.ContainsKey(breType))
                return BreResponse[breType];
            return null;
        }

        #region Outstanding Credit check
        public override string GetOutstandingCreditCheckMsg()
        {
            var breResps = GetRuleResp(BreType.OutstandingCreditCheck);
            var msgs = new List<string>();
            if(breResps!=null)
            {
                breResps.Where(n => n.IsOutstandingCreditCheckFailed).ToList().ForEach(n=> {
                    msgs.Add(string.Format("Outstanding Check : {0}(RM{1})",n.KenanAccountId,n.OutStandingAmount));
                });
            }
            if (msgs.Count > 0)
                return string.Join(Environment.NewLine, msgs);
            return string.Empty;
        }
        #endregion

        #region Contract Check
        public override string GetContractCheckMsg()
        {
            var breResps= GetRuleResp(BreType.ContractCheck);
            if(breResps!=null)
            {
                if (breResps.FirstOrDefault().IsContractCheckFailed)
                {
                    var exitContractCount = CountExistContract();
                    var totalAllowedContract = CountTotalAllowContract();
                    if (exitContractCount < totalAllowedContract)
                        return string.Empty;
                    else
                        return string.Format("Contract Check : {0}", exitContractCount);
                }
            }
            return string.Empty;
        }

        private int CountTotalAllowContract()
        {
            int totalContractAllowed = 0;
            var session = HttpContext.Current.Session;
            totalContractAllowed = ConfigurationManager.AppSettings["totalContractsAllowed"].ToInt();
            retrieveAcctListByICResponse allCustomerDetails = AllCustomerInfo;
            List<DateTime> ListDate = new List<DateTime>();
            List<DateTime> ListDateSorted = new List<DateTime>();

            allCustomerDetails.itemList.Where(n => n.IsActive && n.ServiceInfoResponse != null && n.ServiceInfoResponse.serviceStatus=="A").ToList().ForEach(n => {
                ListDate.Add(n.ServiceInfoResponse.serviceActiveDt);
            });
            if (ListDate.Count > 0)
            {
                ListDateSorted = ListDate.OrderByDescending(x => x).ToList();
                int ListDataIndex = ListDateSorted.Count();
                if (ListDataIndex > 0)
                {
                    if ((DateTime.Now - ListDateSorted[ListDataIndex - 1]).TotalDays > 365)
                        totalContractAllowed = int.Parse(ConfigurationManager.AppSettings["totalContractsAllowed_customerTenure1yearmore"]);
                    else
                        totalContractAllowed = int.Parse(ConfigurationManager.AppSettings["totalContractsAllowed_customerTenure1yearless"]);
                }
                else
                    totalContractAllowed = int.Parse(ConfigurationManager.AppSettings["totalContractsAllowed_customerTenure1yearless"]);
            }else
                totalContractAllowed = int.Parse(ConfigurationManager.AppSettings["totalContractsAllowed_customerTenure1yearless"]);
            return totalContractAllowed;
        }
        private int CountExistContract()
        {
            if (AllServices == null) return 0;
            int contractCheckCount = 0;
            int suppContractCount = 0;
            bool contractcheckflag = false;
            int totalContract = 0;
            SmartRegistrationServiceProxy proxy = new SmartRegistrationServiceProxy();
            IEnumerable<Online.Registration.DAL.Models.Contracts> lstContractsAll = proxy.GetContractsList().Where(a => a.Active == true).ToList();
            IEnumerable<string> lstContracts = lstContractsAll.Select(c => c.KenanCode);
            IEnumerable<string> objAllContracts = lstContractsAll.Select(c => c.KenanCode);
            retrieveAcctListByICResponse AcctListByICResponse = AllServices;
            if (!ReferenceEquals(AcctListByICResponse, null))
            {
                foreach (var v in AcctListByICResponse.itemList)
                {
                    IList<Online.Registration.Web.SubscriberICService.PackageModel> packages = v.Packages;
                    bool hascontracts = false;
                    bool othercontracts = false;
                    int contractMonths = 0;

                    foreach (var contract in packages)
                    {
                        for (int i = 0; i < contract.compList.Count; i++)
                        {
                            if (lstContracts.Contains(contract.compList[i].componentId))
                            {
                                DateTime InactDate;
                                if (string.IsNullOrEmpty(contract.compList[i].componentInactiveDt))
                                {
                                    DateTime ActiveDate = DateTime.ParseExact(contract.compList[i].componentActiveDt.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                    contractMonths = lstContractsAll.Where(c => c.KenanCode == contract.compList[i].componentId).Select(cd => cd.Duration).FirstOrDefault();
                                    InactDate = ActiveDate.AddMonths(contractMonths);
                                }
                                else
                                {
                                    InactDate = DateTime.ParseExact(contract.compList[i].componentInactiveDt.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                }
                                hascontracts = (DateTime.Compare(InactDate, DateTime.Now) > 0) ? true : hascontracts;
                            }
                            else
                            {
                                othercontracts = true;
                            }
                            if (objAllContracts.Contains(contract.compList[i].componentId))
                            {
                                DateTime InactDate;
                                if (string.IsNullOrEmpty(contract.compList[i].componentInactiveDt))
                                {
                                    DateTime ActiveDate = DateTime.ParseExact(contract.compList[i].componentActiveDt.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                    contractMonths = lstContractsAll.Where(c => c.KenanCode == contract.compList[i].componentId).Select(cd => cd.Duration).FirstOrDefault();
                                    InactDate = ActiveDate.AddMonths(contractMonths);
                                }
                                else
                                {
                                    InactDate = DateTime.ParseExact(contract.compList[i].componentInactiveDt.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                }
                                if (DateTime.Compare(InactDate, DateTime.Now) > 0)
                                {
                                    contractcheckflag = true;
                                }
                            }
                            if (contractcheckflag == true)
                            {
                                contractcheckflag = false;
                                contractCheckCount++;
                            }
                        }
                    }
                }
            }
            totalContract = contractCheckCount + suppContractCount;
            return totalContract;
        }

        #endregion

        #region DDMF Check
        public override string GetDDMFCheckMsg()
        {
            //TODO: Not implement at this monet
            return string.Empty;
        }
        #endregion

        #region Writeoff Check
        public override string GetWriteoffCheckMsg()
        {
            var breRep = BreResponse.ContainsKey(BreType.WriteoffCheck) ? BreResponse[BreType.WriteoffCheck].FirstOrDefault() : null;
            var _fraudMsg = "";
            if (breRep != null)
                _fraudMsg = breRep.ResultMessage;
            if (_fraudMsg.Contains(Constants.WRITTEN_OFF))
                return string.Format("Writeoff Check : {0}", Constants.WRITTEN_OFF.Split('.').First());
            return string.Empty;
        }
        #endregion

        #region Total Line Check
        public override string GetTotalLineCheckMsg()
        {
            var ttlLineCount = CalculateLine();
            if (ttlLineCount < MaxLineAllowed)
                return string.Empty;
            return string.Format("Total Line Check : {0}", ttlLineCount);
        }

        private int CalculateLine()
        {
            var lineCount = 0;
            if (AllServices == null) return 0;
            retrieveAcctListByICResponse _allServices = AllServices;
            //active only
            _allServices.itemList = _allServices.itemList.Where(n => n.IsActive).ToList();
            //suppCount
            var suppCount = _allServices.itemList.Where(n => n.PrinSupplimentaryResponse != null)
                .SelectMany(n => n.PrinSupplimentaryResponse.itemList).Count();
            //principalCount
            var principalCount = _allServices.itemList.Where(n => n.ServiceInfoResponse!=null 
                && n.ServiceInfoResponse.prinSuppInd == "P" && n.ServiceInfoResponse.postPreInd != "PRE").Count();
            //msimCount
            var msimCount = _allServices.itemList.Where(n => n.SecondarySimList != null && n.SecondarySimList.Count > 0).Count();
            //broadband
            var broadbandCount = _allServices.itemList.Where(n => n.ServiceInfoResponse != null 
                && n.ServiceInfoResponse.lob == "HSDPA").Count();
            //voipCount
            var voipCount = _allServices.itemList.Where(n => n.ServiceInfoResponse != null 
                && n.ServiceInfoResponse.lob == "POSTGSM" && n.ServiceInfoResponse.prinSuppInd=="").Count();
            //FTTHCount
            var ftthCount = _allServices.itemList.Where(n => n.ServiceInfoResponse != null
                && n.ServiceInfoResponse.lob == "FTTH" && n.ServiceInfoResponse.prinSuppInd == "").Count();
            //Not include msimcount as it is part of principle
            lineCount = suppCount + principalCount  + broadbandCount + voipCount + ftthCount;
            return lineCount;
        }
        #endregion

        #region Biometric Check
        public override string GetBiometricMsg()
        {
            //Only check for ic
            if (_idType != "1") return string.Empty;
            var msg = "";
            RetrieveMyKadInfoRp resp;
            if(biometricRes!=null){
                resp = biometricRes;
            }
            else{
                if (biometricReq == null)
                    throw new Exception("No biometric parameter setup.");
                using (var proxy = new AMRServiceProxy())
                {
                    resp = proxy.RetrieveMyKadInfo(biometricReq);
                }
            }
            //Match and no error
            if (resp.FingerMatch) return string.Empty;
            msg = processBiometricCheck(resp);
            return string.Format("Biometric : {0}", msg);
        }

        private string processBiometricCheck(RetrieveMyKadInfoRp resp)
        {
            if (resp.Code == "-1")
                return "Cannot connect to Biometric server";
            if (string.IsNullOrEmpty(resp.FingerThumb))
                return "No Biometric Data found";
            else
                return "Biometric check is performed, but thumb impression not matched!";
        }

        private RetrieveMyKadInfoRq biometricReq;
        private RetrieveMyKadInfoRp biometricRes;
        /// <summary>
        /// Setup biometric param using req parameter
        /// </summary>
        /// <param name="icNo"></param>
        public void InitBiometricParam(string icNo)
        {
            biometricReq = new RetrieveMyKadInfoRq()
            {
                ApplicationID = 3,
                DealerCode = SRSession.CurrentLoginUser.User.Org.DealerCode,
                NewIC = icNo
            };
        }
        /// <summary>
        /// Setup biometric param using response info
        /// </summary>
        /// <param name="retrieveMyKadInfoRp"></param>
        public void InitBiometricParam(RetrieveMyKadInfoRp retrieveMyKadInfoRp)
        {
            biometricRes = retrieveMyKadInfoRp;
        }
        #endregion
    }
}