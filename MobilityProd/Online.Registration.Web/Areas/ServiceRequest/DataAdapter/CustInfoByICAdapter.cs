﻿using Online.Registration.Web.AMRSvc;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Helper;
using Online.Registration.Web.Properties;
using Online.Registration.Web.SubscriberICService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;

namespace Online.Registration.Web.Areas.ServiceRequest.DataAdapter
{
    [Serializable()]
    public class CustInfoByICAdapter : CustInfoAdapter
    {
        private string _idType;
        private string _idNum;
        public CustInfoByICAdapter(string IdType, string IdNum)
        {
            _idNum = IdNum;
            _idType = IdType;
        }

        private retrieveAcctListByICResponse _allCustomerInfo;
        protected override retrieveAcctListByICResponse AllCustomerInfo
        {
            get{
                if (_allCustomerInfo == null){
                    //If search by MSISDN it will use another service to retrieve ic or passport to query actuall record
                    if(_idType.ToUpper().Equals("MSISDN")){
                        AbjustIdTypeFromMSISDN(_idNum);
                    }
                    _allCustomerInfo = GetAllCustInfoFmServer();
                }
                return _allCustomerInfo;
            }
        }

        private retrieveAcctListByICResponse _currentPPIDInfo;
        protected override retrieveAcctListByICResponse CurrentPPIDInfo
        {
            get
            {
                if (_currentPPIDInfo == null){
                    //get with serviceinfo responeconly and active
                    _currentPPIDInfo = AllCustomerInfo.CloneObj();
                    _currentPPIDInfo.itemList = _currentPPIDInfo.itemList.Where(n => n.IsActive && n.ServiceInfoResponse != null).ToList();
                }
                return _currentPPIDInfo;
            }
        }

        private RetrieveMyKadInfoRp _currentCustBiometricData;
        protected override RetrieveMyKadInfoRp CurrentCustBiometricData
        {
            get
            {
                if (_currentCustBiometricData == null)
                    _currentCustBiometricData = GetBiometricDataFrmServer();
                return _currentCustBiometricData;
            }
        }

        private RetrieveMyKadInfoRp GetBiometricDataFrmServer()
        {
            using (var proxy = new AMRServiceProxy())
            {
                return proxy.RetrieveMyKadInfo(new RetrieveMyKadInfoRq()
                {
                    ApplicationID = 3,
                    DealerCode = SRSession.CurrentLoginUser.User.Org.DealerCode,
                    NewIC = _idNum
                });
            }
        }

        private void AbjustIdTypeFromMSISDN(string _msisdn)
        {
            using (var proxy = new retrieveServiceInfoProxy())
            {
                var customer = proxy.retrieveSubscriberDetls(_msisdn, loggedUserName: SRSession.CurrentLoginUser.UserName, isSupAdmin: true);
                //Do nothing if customer is null
                if (customer == null) return;
                if (!string.IsNullOrEmpty(customer.NewIC))
                {
                    _idNum = customer.NewIC;
                    _idType = "NEWIC";
                }
                else if (!string.IsNullOrEmpty(customer.PassportNo))
                {
                    _idNum = customer.PassportNo;
                    _idType = "PASSPORT";
                }
                else if (!string.IsNullOrEmpty(customer.OldIC))
                {
                    _idNum = customer.OldIC;
                    _idType = "OLDIC";
                }
                else if (!string.IsNullOrEmpty(customer.OtherIC))
                {
                    _idNum = customer.OtherIC;
                    _idType = "OTHERIC";
                }
            }
        }

        private retrieveAcctListByICResponse GetAllCustInfoFmServer()
        {
            using (var proxy = new retrieveServiceInfoProxy())
            {
                string exception = "";
                var result = proxy.retrieveAllDetails(exception, SRSession.CurrentLoginUser.UserName, _idType, _idNum, 
                    ConfigurationManager.AppSettings["KenanMSISDN"], 
                    Settings.Default.retrieveAcctListByICResponseUserName, 
                    Settings.Default.retrieveAcctListByICResponsePassword, "", false);
                if (result == null || result.itemList.Count==0) return result;
                //only active and have serviceinfo
                var firstActiveAc=result.itemList.Where(n => n.IsActive && n.ServiceInfoResponse != null).FirstOrDefault();
                if(firstActiveAc!=null){
                    //Populate account if got active account
                    var externalId = firstActiveAc.ExternalId;
                    var account = PopulateAc(result, externalId, _idNum, _idType);
                    result.itemList.ForEach(n => n.Account = account);
                }
                //TODO :Spaghetti code here follow existing isell, need to relook
                RunCustomeOverriding(result);
                return result;
            }
        }

        private void RunCustomeOverriding(retrieveAcctListByICResponse result)
        {
            //change all service with emfConfig to supplimentatly tag
            var excludeEmfConfigCode = ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString();
            result.itemList.Where(i =>i.ServiceInfoResponse!=null && i.ServiceInfoResponse.emf_config_id == excludeEmfConfigCode)
                .ToList().ForEach(i => i.ServiceInfoResponse.prinSuppInd = "S");
            //add multisim..super messey
            result.itemList.Where(i => i.ServiceInfoResponse != null && i.ServiceInfoResponse.emf_config_id == excludeEmfConfigCode)
                .ToList().ForEach(i => {
                    using (var proxy = new PrinSupServiceProxy())
                    {
                        var resp = proxy.getPrimSecondaryines(i.ExternalId);
                        i.SecondarySimList = resp==null?null:resp.itemList.Where(n => n.PrimSecInd != "P").ToList();
                    }
                });
            //Set isBillable by account type
            result.itemList.Where(i => i.IsActive && i.AccountDetails.AcctType == "1").ToList().ForEach(i => i.IsBillable="Y");
        }

        private CustomizedAccount PopulateAc(retrieveAcctListByICResponse AcctListByICResponse, string msisdn,string IdNum, string IdType)
        {
            //Need to relook this
            List<Items> itemList = AcctListByICResponse.itemList.Where(a => a.ExternalId == (msisdn)).ToList();

            if (itemList.Count > 1 && itemList.Exists(a => a.ServiceInfoResponse != null))
                itemList = AcctListByICResponse.itemList.Where(a => a.ExternalId == (msisdn) && a.ServiceInfoResponse != null).ToList();

            CustomizedAccount Account = null;
            if (msisdn != string.Empty && itemList.Any())
            {
                try
                {
                    if (!ReferenceEquals(itemList[0].Customer, null))
                    {

                        var addressDetails = Util.ConcatenateStrings(" ", itemList[0].Customer.City, itemList[0].Customer.PostCode, itemList[0].Customer.State);

                        Account = new CustomizedAccount();
                        Account.AccountNumber = itemList[0].AcctExtId;
                        Account.Holder = itemList[0].Customer.CustomerName;
                        Account.CompanyName = "";
                        Account.Category = "";
                        Account.MarketCode = "";

                        var address1 = itemList[0].Customer.Address1.Trim();
                        var address2 = itemList[0].Customer.Address2.Trim();
                        var address3 = itemList[0].Customer.Address3.Trim();

                        if (!string.IsNullOrEmpty(address1) && address1.Length > 0)
                        {
                            var lastChar = address1[address1.Length - 1];
                            if (lastChar == ',')
                            {
                                address1 = address1.Substring(0, address1.Length - 1);
                            }
                        }

                        if (!string.IsNullOrEmpty(address2) && address2.Length > 0)
                        {
                            var lastChar = address2[address2.Length - 1];
                            if (lastChar == ',')
                            {
                                address2 = address2.Substring(0, address2.Length - 1);
                            }
                        }

                        if (!string.IsNullOrEmpty(address3) && address3.Length > 0)
                        {
                            var lastChar = address3[address3.Length - 1];
                            if (lastChar == ',')
                            {
                                address3 = address3.Substring(0, address3.Length - 1);
                            }
                        }

                        Account.Address = Util.ConcatenateStrings(", ", address1, address2, address3, addressDetails);
                        Account.SubscribeNo = "";
                        Account.SubscribeNoResets = "";
                        Account.ActiveDate = "";
                        Account.Plan = "";
                        Account.IDNumber = IdNum;
                        Account.IDType = IdType;
                    }
                    else
                    {
                        Account = null;
                    }
                }
                catch (Exception ex)
                {
                    Account = null;
                }
            }
            return Account;
        }

        public RetrieveMyKadInfoRp GetBiometricData(){
            return CurrentCustBiometricData;
        }

        public retrieveAcctListByICResponse GetAllServices()
        {
            return AllCustomerInfo;
        }
    }
}