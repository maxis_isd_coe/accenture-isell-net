﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.DAL.Models.Common;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels.Common;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common;
using Online.Registration.DAL.Models.ServiceRequest.Dto;
using Online.Registration.Web.Helper;
using Online.Registration.Web.CustomExtension;
using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Helpers;
using Online.Registration.DAL.Models.FilterCriterias;

namespace Online.Registration.Web.Areas.ServiceRequest.Factories
{
    public class SRModelFactory
    {
        //mapping the fields in Bill Separation VM to database table fields
        internal SROrderBillSeparationDtl CreateOrderDetail(BillSeparationNewOrderSmVM vm)
        {
            //Customer
            var detail = new SROrderBillSeparationDtl()
            {
                CustBiometricPass = vm.NewOrder.BiometricPass,
                CustMSISDN = vm.NewOrder.Msisdn,
                CustAcctNumber = vm.NewOrder.AccNum,
                SignatureSvg = vm.Signature.SignSvg,
                SignDt = vm.Signature.SignDate,
                ExistMSISDN = vm.NewOrder.ExistMSISDN,
                ExistAcctNumber = vm.NewOrder.ExistAccountNumber,
                CustCurrentPlan = vm.NewOrder.Plan,
                IsSplitNewAcc=vm.NewOrder.IsSplitNewAcc
            };
            detail.Customer = new Customer()
            {
                FullName = vm.NewOrder.CustName.Trim(),
                CreateDT = DateTime.Now,
                LastAccessID = vm.LoginId,
                IDCardNo = vm.NewOrder.IdNum,
                IDCardTypeID = int.Parse(vm.NewOrder.IdType),
                RaceID = vm.NewOrder.CustRaceID,
                LanguageID = vm.NewOrder.CustLanguageID,
                NationalityID = vm.NewOrder.CustNationalityID,
                EmailAddr = vm.NewOrder.CustEmail,
                Gender = vm.NewOrder.CustGender,
                ContactNo = vm.NewOrder.CustContactNumber,
                AlternateContactNo = vm.NewOrder.CustAltContactNumber,
                DateOfBirth = vm.NewOrder.CustDob
            };

            //New Customer Address
            var newAddress = new CommonAddress()
            {
                CreateDt = DateTime.Now,
                CreateBy = vm.LoginId,
                Line1 = vm.NewOrder.NewAddrLine1,
                Line2 = vm.NewOrder.NewAddrLine2,
                Line3 = vm.NewOrder.NewAddrLine3,
                Postcode = vm.NewOrder.NewAddrPostCode,
                Town = vm.NewOrder.NewAddrCity,
                StateID = vm.NewOrder.NewAddrStateID
            };
            detail.NewAddress = newAddress;
            
            //Current Customer Address
            detail.CurrAddress = CreateAddress(vm.NewOrder);

            //Upload doc
            if (vm.NewOrder.UploadDoc != null)
            {
                CreateOrderDetail(detail as SROrderDtl, vm.NewOrder.UploadDoc);
            }
            return detail;
        }

        internal SRQueryOrderDto CreateQueryDto(SRViewOrderListVM vm)
        {
            return new SRQueryOrderDto() {
                SortDesc=vm.Order.First().Dir=="asc"?true:false,
                SortyBy = vm.GetColumnName(vm.Order.First().Column),
                CriteriaList=CreateFilterCriteria(vm),
                LoginId=vm.LoginId,
                CenterOrgId=vm.CenterOrgId
            };
        }

        private Dictionary<string, FilterHelper> _filterMapping = null;
        public Dictionary<string, FilterHelper> FilterMapping
        {
            get
            {
                if (_filterMapping == null)
                {
                    var vm = new SRViewOrderListVM();
                    var tmpItem = new SRQueryMaster();
                    _filterMapping = new Dictionary<string, FilterHelper> {
                        { vm.Name(n=>n.CreateDateEnd),new FilterHelper(typeof(EndDateFilterCriteria),tmpItem.Name(n=>n.CreateDt))},
                        { vm.Name(n=>n.CreateDateStart),new FilterHelper(typeof(StartDateFilterCriteria),tmpItem.Name(n=>n.CreateDt))} ,
                        { vm.Name(n=>n.CreatedBy),new FilterHelper(typeof(FilterCriteria),tmpItem.Name(n=>n.CreateBy))} ,
                        { vm.Name(n=>n.IdNo),new FilterHelper(typeof(FilterCriteria),tmpItem.Name(n=>n.IdCardNo))} ,
                        { vm.Name(n=>n.IdNo1),new FilterHelper(typeof(FilterCriteria),tmpItem.Name(n=>n.IdCardNo))} ,
                        { vm.Name(n=>n.OrderId),new FilterHelper(typeof(FilterCriteria),tmpItem.Name(n=>n.OrderId))} ,
                        { vm.Name(n=>n.RequestType),new FilterHelper(typeof(FilterCriteria),tmpItem.Name(n=>n.RequestTypeId))} ,
                        { vm.Name(n=>n.Status),new FilterHelper(typeof(FilterCriteria),tmpItem.Name(n=>n.StatusId))} ,
                        { vm.Name(n=>n.MSISDN),new FilterHelper(typeof(FilterCriteria),tmpItem.Name(n=>n.MSISDN))} ,
                        { vm.Name(n=>n.IsToday),new FilterHelper(typeof(TodayFilterCriteria),tmpItem.Name(n=>n.CreateDt))} 
                    };
                }
                return _filterMapping;
            }
        }
        private List<IFilterCriteria> CreateFilterCriteria(SRViewOrderListVM vm)
        {
            var filters = new List<IFilterCriteria>();
            foreach(var mapping in FilterMapping)
            {
                var value = vm.GetType().GetProperty(mapping.Key).GetValue(vm,null);
                if (value != null)
                {
                    if (value is string && string.IsNullOrEmpty((string)value))
                        continue;
                    if (value is bool && (bool)value == false)
                        continue;
                    if (value is Enum)
                        value = (int)value;
                    try 
                    {
                        var param = new List<object>() { mapping.Value.Target };
                        var cParamCount = mapping.Value.Type.GetConstructors().First().GetParameters().Count();
                        if (cParamCount > 1)
                            param.Add(value);
                        var filter = (IFilterCriteria)Activator.CreateInstance(
                            mapping.Value.Type, param.ToArray());
                        filters.Add(filter);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Cannot create filter for {0}.{1}",mapping.Key,ex.Message));
                    }
                }
            }
            return filters;
        }

        internal SROrderMaster CreateOrder(SRSummaryVM vm)
        {
            SROrderMaster order = new SROrderMaster() {
                RequestType = vm.ToRequestType(),
                Status = SRStatus.New,
                CenterOrgId = vm.CenterOrgId,
                CreateBy = vm.LoginId,
                CreateDt = DateTime.Now,
            };
            try
            {
                order.VerificationDocs = CreateVerificationDocs(((dynamic)vm.NewOrder).UploadDoc);
            }
            catch (Exception)
            {
            }
            return order;
        }

        internal VerificationDoc CreateVerificationDoc(SRViewOrderVM vm, byte[] file, DocTypes docType)
        {
            return new VerificationDoc(){
                DocType=docType,
                File=Convert.ToBase64String(file),
                FileName=string.Format("{0}.pdf",vm.OrderId),
                CreateBy=vm.LoginId,
                CreateDt=DateTime.Now
            };
        }

        //Map SRSubmitResultUploadVM to VerificationDoc
        internal VerificationDoc CreateVerificationDoc(SRSubmitResultUploadVM vm){
            //Scan form submit to server
            return new VerificationDoc(){
                DocType=DocTypes.ScanContract,
                File=vm.RegFormFile,
                FileName= GetFileName(DocTypes.ScanContract, vm.RegFormFileName),
                CreateBy=vm.LoginId,
                CreateDt=DateTime.Now
            };
        }
        //Map upload thirdparty auth upload doc to verificationDoc
        internal List<VerificationDoc> CreateVerificationDocs(ThirdPartyAuthUploadDocVM vm)
        {
            var list = new List<VerificationDoc>();
            if (vm != null)
            {
                //Leter of authorization
                if (!string.IsNullOrEmpty(vm.LOAIdFile))
                {
                    list.Add(new VerificationDoc()
                    {
                        CreateBy = vm.LoginId,
                        CreateDt = DateTime.Now,
                        File = vm.LOAIdFile,
                        FileName = GetFileName(DocTypes.LetterOfAuth, vm.LOAIdFileName),
                        DocType = DocTypes.LetterOfAuth
                    });
                }
                //Thirdparty Id front
                if (!string.IsNullOrEmpty(vm.ThirdPartyFrontIdFile))
                {
                    list.Add(new VerificationDoc()
                    {
                        CreateBy = vm.LoginId,
                        CreateDt = DateTime.Now,
                        File = vm.ThirdPartyFrontIdFile,
                        FileName = GetFileName(DocTypes.ThirdPartyIdFront, vm.ThirdPartyFrontIdFileName),
                        DocType = DocTypes.ThirdPartyIdFront
                    });
                }
                //Thirdparty Id back
                if (!string.IsNullOrEmpty(vm.ThirdPartyBackIdFile))
                {
                    list.Add(new VerificationDoc()
                    {
                        CreateBy = vm.LoginId,
                        CreateDt = DateTime.Now,
                        File = vm.ThirdPartyBackIdFile,
                        FileName = GetFileName(DocTypes.ThirdPartyIdBack,vm.ThirdPartyBackIdFileName),
                        DocType = DocTypes.ThirdPartyIdBack
                    });
                }
            }
            //Collect from UploadDocVm parameter
            list.AddRange(CreateVerificationDocs((UploadDocVM)vm));
            return list;
        }

        //Map upload transfer ownership upload doc to verificationDoc
        internal List<VerificationDoc> CreateVerificationDocs(TransferOwnershipUploadDocVM vm)
        {
            var list = new List<VerificationDoc>();
            if (vm != null)
            {
                //Transferee Id front
                if (!string.IsNullOrEmpty(vm.TransfereeFrontIdFile))
                {
                    list.Add(new VerificationDoc()
                    {
                        CreateBy = vm.LoginId,
                        CreateDt = DateTime.Now,
                        File = vm.TransfereeFrontIdFile,
                        FileName = GetFileName(DocTypes.TransfereeIdFront, vm.TransfereeFrontIdFileName),
                        DocType = DocTypes.TransfereeIdFront
                    });
                }
                //Transferee Id back
                if (!string.IsNullOrEmpty(vm.TransfereeBackIdFile))
                {
                    list.Add(new VerificationDoc()
                    {
                        CreateBy = vm.LoginId,
                        CreateDt = DateTime.Now,
                        File = vm.TransfereeBackIdFile,
                        FileName = GetFileName(DocTypes.TransfereeIdBack, vm.TransfereeBackIdFileName),
                        DocType = DocTypes.TransfereeIdBack
                    });
                }
            }
            //Collect from UploadDocVm parameter
            list.AddRange(CreateVerificationDocs((UploadDocVM)vm));
            return list;
        }

        private string GetFileName(DocTypes docType, string fileName)
        {
            return docType.ToFileName() + fileName.GetExtension();
        }

        //Map upload doc VM to List of VerificationDoc
        internal List<VerificationDoc> CreateVerificationDocs(UploadDocVM vm)
        {
            var list = new List<VerificationDoc>();
            if(vm != null)
            {
                if (!string.IsNullOrEmpty(vm.FrontIdFile))
                {
                    list.Add(new VerificationDoc() {
                        CreateBy=vm.LoginId,
                        CreateDt=DateTime.Now,
                        File=vm.FrontIdFile,
                        FileName = GetFileName(DocTypes.IdFront, vm.FrontIdFileName),
                        DocType =DocTypes.IdFront
                    });
                }
                if (!string.IsNullOrEmpty(vm.BackIdFile))
                {
                    list.Add(new VerificationDoc()
                    {
                        CreateBy = vm.LoginId,
                        CreateDt = DateTime.Now,
                        File = vm.BackIdFile,
                        FileName = GetFileName(DocTypes.IdBack, vm.BackIdFileName),
                        DocType = DocTypes.IdBack
                    });
                }
                if (!string.IsNullOrEmpty(vm.OtherIdFile))
                {
                    list.Add(new VerificationDoc()
                    {
                        CreateBy = vm.LoginId,
                        CreateDt = DateTime.Now,
                        File = vm.OtherIdFile,
                        FileName = GetFileName(DocTypes.OtherDoc, vm.OtherIdFileName),
                        DocType = DocTypes.OtherDoc
                    });
                }
            }
            return list;
        }

        //mapping the fields in Corp SIM Rep VM to database table fields
        internal SROrderCorpSimRepDtl CreateOrderDetail(CorpSimRepNewOrderSmVM vm)
        {
            var detail= new SROrderCorpSimRepDtl() {
                CustBiometricPass=vm.NewOrder.BiometricPass,
                MSISDN=vm.NewOrder.Msisdn,
                AccountNo=vm.NewOrder.AccNum,
                NewSimTypeCode=vm.NewOrder.NewSimTypeCode,
                NewSimSerial=vm.NewOrder.NewSimSerial,
                SignatureSvg=vm.Signature.SignSvg,
                SignDt=vm.Signature.SignDate
            };
            //Customer
            detail.Customer = new Customer() {
                BizRegNo=vm.NewOrder.BrnNum,
                BizRegName=vm.NewOrder.CompName,
                PersonInCharge=vm.NewOrder.CustName.Trim(),
                FullName=vm.NewOrder.CustName.Trim(),
                CreateDT=DateTime.Now,
                LastAccessID=vm.LoginId,
                IDCardNo=vm.NewOrder.IdNum,
                IDCardTypeID=int.Parse(vm.NewOrder.IdType)
            };
            //Upload doc
            if (vm.NewOrder.UploadDoc != null)
            {
                CreateOrderDetail(detail as SROrderDtl,vm.NewOrder.UploadDoc);
            }
            return detail;
        }

        private void CreateOrderDetail(SROrderDtl detail, UploadDocVM vm)
        {
            //Remark in master
            detail.Justification = vm.Justification;
            detail.Remarks = vm.Remarks;
            detail.QueueNo = vm.QueueNum;
        }

        //mapping the fields in Postpaid To Prepaid VM to database table fields
        internal SROrderPostToPreDtl CreateOrderDetail(PostpaidToPrepaidNewOrderSmVM vm)
        {
            var detail = new SROrderPostToPreDtl()
            {
                CustBiometricPass = false,
                CustMSISDN = vm.NewOrder.Msisdn,
                CustName = vm.NewOrder.CustName.Trim(),
                CustIDType = vm.NewOrder.IdType,
                CustIDNumber = vm.NewOrder.IdNum,
                SignatureSvg = vm.Signature.SignSvg,
                SignDt = vm.Signature.SignDate,
                PostpaidPlan = vm.NewOrder.Plan,
                PrepaidPlan = vm.NewOrder.PrepaidPlan,
                PrepaidSIMTypeCode=vm.NewOrder.PrepaidSimTypeCode,
                PrepaidSIMSerial = vm.NewOrder.PrepaidSIMNumber,
                PostpaidAccNumber = vm.NewOrder.AccNum,
                PrepaidAccNumber = vm.NewOrder.PrepaidAccNumber,
                OutstandingAmount = vm.NewOrder.GetOutstandingBillAmount,
                EarlyTerminateFee = vm.NewOrder.EarlyTerminationFee
            };
            //Customer
            detail.Customer = new Customer()
            {
                FullName = vm.NewOrder.CustName.Trim(),
                CreateDT = DateTime.Now,
                LastAccessID = vm.LoginId,
                IDCardNo = vm.NewOrder.IdNum,
                IDCardTypeID = int.Parse(vm.NewOrder.IdType),
                RaceID = vm.NewOrder.CustRaceID,
                LanguageID = vm.NewOrder.CustLanguageID,
                NationalityID = vm.NewOrder.CustNationalityID,
                EmailAddr = vm.NewOrder.CustEmail,
                Gender = vm.NewOrder.CustGender,
                ContactNo = vm.NewOrder.CustContactNumber,
                AlternateContactNo = vm.NewOrder.CustAltContactNumber,
                DateOfBirth = vm.NewOrder.CustDob
            };
            //Address
            detail.CustAddress = CreateAddress(vm.NewOrder);

            //Upload doc
            if (vm.NewOrder.UploadDoc != null)
            {
                CreateOrderDetail(detail as SROrderDtl, vm.NewOrder.UploadDoc);
            }
            return detail;
        }

        //mapping the fields in Prepaid To Postpaid VM to database table fields
        internal SROrderPreToPostDtl CreateOrderDetail(PrepaidToPostpaidNewOrderSmVM vm)
        {
            var detail = new SROrderPreToPostDtl()
            {
                CustBiometricPass = false,
                CustMSISDN = vm.NewOrder.Msisdn,
                CustName = vm.NewOrder.CustName.Trim(),
                CustIDType = vm.NewOrder.IdType,
                CustIDNumber = vm.NewOrder.IdNum,
                SignatureSvg = vm.Signature.SignSvg,
                SignDt = vm.Signature.SignDate,
                PostpaidPlan = vm.PostplanName,
                PostpaidPlanId=vm.NewOrder.PostpaidPlanId,
                PrepaidPlan = vm.NewOrder.PrepaidPlan,
                PostpaidSIMSerial = vm.NewOrder.PostpaidSIMNumber,
                PostpaidAccNumber = vm.NewOrder.PostpaidAccNumber,
                PrepaidAccNumber = vm.NewOrder.AccNum
            };
            //Customer
            detail.Customer = new Customer()
            {
                FullName = vm.NewOrder.CustName.Trim(),
                CreateDT = DateTime.Now,
                LastAccessID = vm.LoginId,
                IDCardNo = vm.NewOrder.IdNum,
                IDCardTypeID = int.Parse(vm.NewOrder.IdType),
                RaceID = vm.NewOrder.CustRaceID,
                LanguageID = vm.NewOrder.CustLanguageID,
                NationalityID = vm.NewOrder.CustNationalityID,
                EmailAddr = vm.NewOrder.CustEmail,
                Gender = vm.NewOrder.CustGender,
                ContactNo = vm.NewOrder.CustContactNumber,
                AlternateContactNo = vm.NewOrder.CustAltContactNumber,
                DateOfBirth = vm.NewOrder.CustDob
            };
            //Address
            detail.CustAddress = CreateAddress(vm.NewOrder);

            //Upload doc
            if (vm.NewOrder.UploadDoc != null)
            {
                CreateOrderDetail(detail as SROrderDtl, vm.NewOrder.UploadDoc);
            }
            return detail;
        }

        //mapping the fields in Transfer Ownership VM to database table fields
        internal SROrderTransferOwnerDtl CreateOrderDetail(TransferOwnershipNewOrderSmVM vm)
        {
            //setting the VAS List and customer plan
            vm.NewOrder.VasList = vm.NewOrder.MsisdnServiceInfoMap[vm.NewOrder.Msisdn].VasList;
            vm.NewOrder.Plan = vm.NewOrder.MsisdnServiceInfoMap[vm.NewOrder.Msisdn].Plan;

            var detail = new SROrderTransferOwnerDtl()
            {
                Transferor_CustBiometricPass = false,
                Transferee_CustBiometricPass = false,
                TransfereeAcctNumber = vm.New_CustAccountNumber,
                TransferorAcctNumber = vm.NewOrder.AccNum,
                TransferorPlan = vm.NewOrder.Plan,
                TransferorVAS = string.Join(";",vm.NewOrder.VasList),
                TransferorMSISDN = vm.NewOrder.Msisdn,
                TransfereeMSISDN = vm.NewOrder.Exist_CustMSISDN,
                TransfereeSignature = vm.Signature_ForNewOwner.SignSvg,
                SignatureSvg = vm.Signature.SignSvg,
                SignDt = vm.Signature.SignDate,
                TransfereeSignDt=vm.Signature_ForNewOwner.SignDate,
                IsTransferNewAcc = vm.NewOrder.IsTransferNewAcc
            };
            //Customer - Current/Transferor
            detail.TransferorCustomer = new Customer()
            {
                FullName = vm.NewOrder.CustName.Trim(),
                CreateDT = DateTime.Now,
                LastAccessID = vm.LoginId,
                IDCardNo = vm.NewOrder.IdNum,
                IDCardTypeID = int.Parse(vm.NewOrder.IdType),
                RaceID = vm.NewOrder.CustRaceID,
                LanguageID = vm.NewOrder.CustLanguageID,
                NationalityID = vm.NewOrder.CustNationalityID,
                EmailAddr = vm.NewOrder.CustEmail ,
                Gender = vm.NewOrder.CustGender,
                ContactNo = vm.NewOrder.CustContactNumber,
                AlternateContactNo = vm.NewOrder.CustAltContactNumber,
                DateOfBirth = vm.NewOrder.CustDob
            };
            //Customer - Transferee
            detail.TransfereeCustomer = new Customer()
            {
                FullName = vm.New_CustName.Trim(),
                CreateDT = DateTime.Now,
                LastAccessID = vm.LoginId,
                IDCardNo = vm.NewOrder.IdNum,
                IDCardTypeID = vm.New_CustIDTypeId,
                RaceID = vm.New_CustRaceID,
                LanguageID = vm.New_CustLanguageID,
                NationalityID = vm.New_CustNationalityID,
                EmailAddr = vm.New_CustEmail,
                Gender = vm.New_CustGender,
                ContactNo = vm.New_CustContactNumber,
                AlternateContactNo = vm.New_CustAlternateNumber,
                DateOfBirth = vm.New_CustDOB.HasValue?vm.New_CustDOB.Value:DateTime.MinValue
            };

            //Address - Current/Transferor
            detail.TransferorAddress = CreateAddress(vm.NewOrder);

            //Address - Transferee
            var transfereeAddress = new CommonAddress()
            {
                CreateDt = DateTime.Now,
                CreateBy = vm.LoginId,
                Line1 = vm.New_CustAddressLine1,
                Line2 = vm.New_CustAddressLine2,
                Line3 = vm.New_CustAddressLine3,
                Postcode = vm.New_CustPostCode,
                Town = vm.New_CustCity,
                StateID = vm.New_CustStateID
            };
            detail.TransfereeAddress = transfereeAddress;
            //Upload doc
            if (vm.NewOrder.UploadDoc != null)
            {
                CreateOrderDetail(detail as SROrderDtl, vm.NewOrder.UploadDoc);
            }
            return detail;
        }

        //mapping the fields in Third Party Authorization VM to database table fields
        internal SROrderThirdPartyAuthDtl CreateOrderDetail(ThirdPartyAuthNewOrderSmVM vm)
        {
            var detail = new SROrderThirdPartyAuthDtl()
            {
                CustBiometricPass = vm.NewOrder.BiometricPass,
                MSISDN = vm.NewOrder.Msisdn,
                AccountNo = vm.NewOrder.AccNum,
                ThirdPartyName = vm.NewOrder.ThirdPartyName,
                ThirdPartyIdNumber = vm.NewOrder.ThirdPartyIDNumber,
                ThirdPartyIdType = vm.NewOrder.ThirdPartyIDType,
                ThirdPartyAuthTypeId = vm.NewOrder.ThirdPartyAuthTypeId,
                SignatureSvg = vm.Signature.SignSvg,
                SignDt = vm.Signature.SignDate,
                ThirdPartyBiometricPass=vm.NewOrder.ThirdPartyBiometricPass
            };
            //Customer
            detail.Customer = new Customer()
            {
                FullName = vm.NewOrder.CustName.Trim(),
                CreateDT = DateTime.Now,
                LastAccessID = vm.LoginId,
                IDCardNo = vm.NewOrder.IdNum,
                IDCardTypeID = int.Parse(vm.NewOrder.IdType),
                RaceID = vm.NewOrder.CustRaceID,
                LanguageID = vm.NewOrder.CustLanguageID,
                NationalityID = vm.NewOrder.CustNationalityID,
                EmailAddr = vm.NewOrder.CustEmail,
                Gender = vm.NewOrder.CustGender,
                ContactNo = vm.NewOrder.CustContactNumber,
                AlternateContactNo = vm.NewOrder.CustAltContactNumber,
                DateOfBirth = vm.NewOrder.CustDob
            };
            //Address
            detail.CustAddress = CreateAddress(vm.NewOrder);

            //Upload doc
            if (vm.NewOrder.UploadDoc != null)
            {
                CreateOrderDetail(detail as SROrderDtl, vm.NewOrder.UploadDoc);
            }
            return detail;
        }

        //mapping the fields in Change MSISDN VM to database table fields
        internal SROrderChangeMSISDNDtl CreateOrderDetail(ChangeMSISDNNewOrderSmVM vm)
        {
            var detail = new SROrderChangeMSISDNDtl()
            {
                CustBiometricPass = vm.NewOrder.BiometricPass,
                MSISDN = vm.NewOrder.Msisdn,
                AccountNo = vm.NewOrder.AccNum,
                NewMSISDN = vm.NewOrder.NewMSISDN
            };
            //Customer
            detail.Customer = CreateCustomer(vm.NewOrder);
            //Address
            detail.Address = CreateAddress(vm.NewOrder);
            //Upload doc
            if (vm.NewOrder.UploadDoc != null)
            {
                CreateOrderDetail(detail as SROrderDtl, vm.NewOrder.UploadDoc);
            }
            return detail;
        }

        //mapping the fields in Change Status VM to database table fields
        internal SROrderChangeStatusDtl CreateOrderDetail(ChangeStatusNewOrderSmVM vm)
        {
            var detail = new SROrderChangeStatusDtl()
            {
                CustBiometricPass = vm.NewOrder.BiometricPass,
                MSISDN = vm.NewOrder.Msisdn,
                AccountNo = vm.NewOrder.AccNum,
                ChangeStatusTypeId = vm.NewOrder.ChangeStatusTypeId,
                EarlyTerminationFee=vm.NewOrder.EarlyTerminationFee,
                ContractRemainingMonths=vm.NewOrder.ContractPendingMonths
            };
            //Customer
            detail.Customer = CreateCustomer(vm.NewOrder);
            //Address
            detail.Address = CreateAddress(vm.NewOrder);
            //Upload doc
            if (vm.NewOrder.UploadDoc != null)
            {
                CreateOrderDetail(detail as SROrderDtl, vm.NewOrder.UploadDoc);
            }
            return detail;
        }

        private Customer CreateCustomer(SRNewOrderWthCustVM vm)
        {
            return new Customer()
            {
                FullName = vm.CustName.Trim(),
                CreateDT = DateTime.Now,
                LastAccessID = vm.LoginId,
                IDCardNo = vm.IdNum,
                IDCardTypeID = int.Parse(vm.IdType)
            };
        }

        private CommonAddress CreateAddress(SRNewOrderWthCustVM vm)
        {
            return new CommonAddress()
            {
                CreateDt = DateTime.Now,
                CreateBy = vm.LoginId,
                Line1 = vm.CustBillAddLine1,
                Line2 = vm.CustBillAddLine2,
                Line3 = vm.CustBillAddLine3,
                Postcode = vm.CustBillPostCode,
                Town = vm.CustBillCity,
                StateID = vm.CustBillStateID
            };
        }

        //Map SRView order vm to SRupdateStatus DTO to update the order status
        internal SRUpdateStatusDto CreateUpdateStatusInfo(SRViewOrderVM vm)
        {
            var model= new SRUpdateStatusDto(){
                OrderId=vm.OrderId,
                UpdateBy=vm.LoginId,
                Remarks=vm.Remarks,
                Status=vm.Status,
                AccountNo=vm.NewAccountNo
            };

            return model;
        }

        internal List<iContractProxy.iContractReqDto> CreateIContractReqs(SROrderMaster order)
        {
            var list=new List<iContractProxy.iContractReqDto>();
            foreach(var doc in order.VerificationDocs){
                list.Add(CreateIContractReq(order,order.Detail, doc));
            }
            return list;
        }
        
        private iContractProxy.iContractReqDto CreateIContractReq(SROrderMaster order, SROrderDtl dtl, VerificationDoc doc)
        {
            //TODO : Need think of way of better manage subclass here
            iContractProxy.iContractReqDto model = CreateIContractReq(order, (dynamic)dtl, doc);//expecting dispatch by subclass
            model.DocType = doc.DocType.ToIContractDocType();
            model.RegId = order.OrderId;
            model.TrnType = "SR";
            model.StoreID = string.Format("{0}({1})", order.CenterOrg.Name, order.CenterOrg.DealerCode);
            model.File = Convert.FromBase64String(doc.File);
            model.AgentCode = order.CreateBy;
            model.FileName = doc.FileName;
            return model;
        }

        private iContractProxy.iContractReqDto CreateIContractReq(SROrderMaster order, SROrderChangeStatusDtl dtl, VerificationDoc doc)
        {
            return new iContractProxy.iContractReqDto()
            {
                IDcardValue = dtl.Customer.IDCardNo,
                MSISDN = dtl.MSISDN,
                AccNo = dtl.AccountNo
            };
        }

        private iContractProxy.iContractReqDto CreateIContractReq(SROrderMaster order, SROrderChangeMSISDNDtl dtl, VerificationDoc doc)
        {
            return new iContractProxy.iContractReqDto()
            {
                IDcardValue = dtl.Customer.IDCardNo,
                MSISDN = dtl.MSISDN,
                AccNo = dtl.AccountNo
            };
        }

        private iContractProxy.iContractReqDto CreateIContractReq(SROrderMaster order,SROrderCorpSimRepDtl dtl, VerificationDoc doc)
        {
            return new iContractProxy.iContractReqDto(){
                IDcardValue = dtl.Customer.IDCardNo,
                MSISDN=dtl.MSISDN,
                AccNo=dtl.AccountNo
            };
        }

        private iContractProxy.iContractReqDto CreateIContractReq(SROrderMaster order, SROrderBillSeparationDtl dtl, VerificationDoc doc)
        {
            return new iContractProxy.iContractReqDto(){
                IDcardValue = dtl.Customer.IDCardNo,
                MSISDN = dtl.CustMSISDN,
                AccNo = dtl.CustAcctNumber
            };
        }

        private iContractProxy.iContractReqDto CreateIContractReq(SROrderMaster order, SROrderPostToPreDtl dtl, VerificationDoc doc)
        {
            return new iContractProxy.iContractReqDto(){
                IDcardValue = dtl.Customer.IDCardNo,
                MSISDN = dtl.CustMSISDN,
                AccNo = dtl.PostpaidAccNumber
            };
        }

        private iContractProxy.iContractReqDto CreateIContractReq(SROrderMaster order, SROrderPreToPostDtl dtl, VerificationDoc doc)
        {
            return new iContractProxy.iContractReqDto(){
                IDcardValue = dtl.Customer.IDCardNo,
                MSISDN = dtl.CustMSISDN,
                AccNo = dtl.PrepaidAccNumber
            };
        }

        private iContractProxy.iContractReqDto CreateIContractReq(SROrderMaster order, SROrderThirdPartyAuthDtl dtl, VerificationDoc doc)
        {
            return new iContractProxy.iContractReqDto(){
                IDcardValue = dtl.Customer.IDCardNo,
                MSISDN = dtl.MSISDN,
                AccNo = dtl.AccountNo
            };
        }

        private iContractProxy.iContractReqDto CreateIContractReq(SROrderMaster order, SROrderTransferOwnerDtl dtl, VerificationDoc doc)
        {
            return new iContractProxy.iContractReqDto(){
                IDcardValue = dtl.TransfereeCustomer.IDCardNo,
                MSISDN = dtl.TransferorMSISDN,
                AccNo = dtl.TransferorAcctNumber
            };
        }
    }

    public class FilterHelper
    {
        public FilterHelper(Type type,string target)
        {
            _type = type;
            _target = target;
        }
        private Type _type;
        public Type Type
        {
            get
            {
                return _type;
            }
        }
        private string _target;
        public string Target
        {
            get
            {
                return _target;
            }
        }
    }
}