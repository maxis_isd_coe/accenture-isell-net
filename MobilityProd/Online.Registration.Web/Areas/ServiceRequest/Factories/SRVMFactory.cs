﻿using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.Web.Areas.ServiceRequest.DataAdapter;
using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Common;
using Online.Registration.Web.Areas.ServiceRequest.Controllers;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System.Web.Mvc;
using Online.Registration.Web.SRService;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Stubs;
using Online.Registration.Web.ViewModels.Common;
using Online.Registration.Web.ViewModels.Inteface;
using Online.Registration.Web.NetworkPrintSvc;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.Areas.ServiceRequest.DataLoader;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.Areas.ServiceRequest.Enums;
using Online.Registration.Web.Helper;
using System.Configuration;
using Online.Registration.DAL.Models.Common;
using Online.Registration.DAL.Models;

namespace Online.Registration.Web.Areas.ServiceRequest.Factories
{
    /// <summary>
    /// This Factory class focus in generate View Model object, all view model generation should call from this factory
    /// </summary>
    public class SRVMFactory
    {
        #region "The Things"
        private ICustInfoProvider _custInfoProvider;
        protected ICustInfoProvider TheCustInfoProvider
        {
            get
            {
                if (_custInfoProvider == null)
                    _custInfoProvider = new CustInfoAdapter();
                return _custInfoProvider;
            }
        }

        private IBreInfoProvider _breInfoProvider;
        public IBreInfoProvider TheBreInfoProvider{
            get{
                if (_breInfoProvider == null)
                    _breInfoProvider = new BreInfoAdapter();
                return _breInfoProvider;
            }
        }
        
        private SRVMDataLoader _theVMDataLoader;
        public SRVMDataLoader TheVMDataLoader{
            get{
                if (_theVMDataLoader == null)
                    _theVMDataLoader = new SRVMDataLoader(TheCustInfoProvider,TheBreInfoProvider);
                return _theVMDataLoader;
            }
        }
        #endregion

        internal SRPrintOrderVM CreatePrintOrderVM(SROrderMaster order)
        {
            return CreatePrintOrderVM(order,(dynamic)order.Detail);//Use dynamic dispatch
        }

        private PrepaidToPostpaidPrintOrderVM CreatePrintOrderVM(SROrderMaster order, SROrderPreToPostDtl dtl)
        {
            var vm = new PrepaidToPostpaidPrintOrderVM()
            {
                //service information
                Service_MSISDN = dtl.CustMSISDN,
                NewAccountNo = dtl.PostpaidAccNumber,
                Service_PostpaidPlan = dtl.PostpaidPlan,
                Service_PostpaidSIMSerialNumber = dtl.PostpaidSIMSerial,
                Service_AccountNumber = dtl.PrepaidAccNumber,
            };
            CreatePrintOrderVMForCustInfo(dtl.Customer, vm);
            CreatePrintOrderVMForAddress(dtl.CustAddress, vm);
            CreatePrintOrderVM(vm, order, dtl);
            return vm;
        }

        private void CreatePrintOrderVMForAddress(CommonAddress add, SRPrintOrderWithCustVM vm)
        {
            vm.CustAddressLine1 = add.Line1;
            vm.CustAddressLine2 = add.Line2;
            vm.CustAddressLine3 = add.Line3;
            vm.CustAddressCity = add.Town;
            vm.CustAddressPostCode = add.Postcode;
            vm.CustAddressStateId = add.StateID;
        }

        internal DtResultVM<SRQueryResultItemVM> CreateDtResultVM(IDtPostVM postVm)
        {
            return new DtResultVM<SRQueryResultItemVM>() {
                draw = postVm.Draw,
                data = new List<SRQueryResultItemVM>()
            };
        }

        internal DtResultVM<SRQueryResultItemVM> CreateDtResultVM(List<SRQueryMaster> content, int totalCount,IDtPostVM postVm)
        {
            var vm = new DtResultVM<SRQueryResultItemVM>() {
                draw=postVm.Draw,
                recordsTotal=totalCount,
                //recordsFiltered=content.Count(),
                recordsFiltered = totalCount,
                data =content.Select(n=>CreateQueryResultItemVM(n)).ToList()
            };
            return vm;
        }

        private SRQueryResultItemVM CreateQueryResultItemVM(SRQueryMaster n)
        {
            return new SRQueryResultItemVM()
            {
                CreateBy = n.CreateBy,
                FullName = n.FullName,
                CreateDt = n.CreateDt,
                IdCardNo = n.IdCardNo,
                OrderId = n.OrderId,
                LockBy = n.LockBy,
                MSISDN = n.MSISDN,
                QueueNo = n.QueueNo,
                RequestType = n.RequestType,
                Status = n.Status,
                UpdateBy = n.UpdateBy,
                UpdateDt = n.UpdateDt
            };
        }
        //Set property for PrintOrderVM
        private void CreatePrintOrderVM(SRPrintOrderVM vm, SROrderMaster order, SROrderDtl dtl)
        {
            vm.OrderId = order.OrderId;
            vm.RequestType = order.RequestType;
            vm.SignDt = dtl.SignDt;
            vm.SignSvg = dtl.SignatureSvg;
            vm.BranchName = order.CenterOrg.Name;
            vm.StaffCode = order.CreateBy;
            vm.BranchCode = order.CenterOrg.DealerCode;
            vm.TermAndCondition = new TermAndConditionVM();
        }
        private PostpaidToPrepaidPrintOrderVM CreatePrintOrderVM(SROrderMaster order, SROrderPostToPreDtl dtl)
        {
            var vm = new PostpaidToPrepaidPrintOrderVM()
            {
                //service information
                Service_MSISDN = dtl.CustMSISDN,
                Service_AccountNumber = dtl.PostpaidAccNumber,
                NewAccountNo = dtl.PrepaidAccNumber,
                Service_PrepaidPlan = dtl.PrepaidPlan,
                Service_PrepaidSIMTypeCode=dtl.PrepaidSIMTypeCode,
                Service_PrepaidSIMSerialNumber = dtl.PrepaidSIMSerial,
                Service_OutstandingAmount = dtl.OutstandingAmount.ToString(),
                Service_EarlyTerminateFee = dtl.EarlyTerminateFee.ToString()
            };
            CreatePrintOrderVMForCustInfo(dtl.Customer, vm);
            CreatePrintOrderVMForAddress(dtl.CustAddress, vm);
            CreatePrintOrderVM(vm, order, dtl);
            return vm;
        }

        private ThirdPartyAuthPrintOrderVM CreatePrintOrderVM(SROrderMaster order, SROrderThirdPartyAuthDtl dtl)
        {
            var vm = new ThirdPartyAuthPrintOrderVM()
            {
                //service information
                Service_AccountNumber = dtl.AccountNo,
                Service_MSISDN = dtl.MSISDN,

                //third party information
                ThirdParty_Name = dtl.ThirdPartyName,
                ThirdParty_IDTypeId = int.Parse(dtl.ThirdPartyIdType),
                ThirdParty_IDCardNumber = dtl.ThirdPartyIdNumber,
                ThirdParty_AuthorizationTypeId = dtl.ThirdPartyAuthTypeId,
                ThirdParty_BiometricPass = dtl.ThirdPartyBiometricPass,
            };
            CreatePrintOrderVMForCustInfo(dtl.Customer, vm);
            CreatePrintOrderVMForAddress(dtl.CustAddress, vm);
            CreatePrintOrderVM(vm, order, dtl);
            return vm;
        }

        private BillSeparationPrintOrderVM CreatePrintOrderVM(SROrderMaster order, SROrderBillSeparationDtl dtl)
        {
            var vm = new BillSeparationPrintOrderVM()
            {
                //service information
                Service_MSISDN = dtl.CustMSISDN,
                Service_BillAddressLine1 = dtl.NewAddress.Line1,
                Service_BillAddressLine2 = dtl.NewAddress.Line2,
                Service_BillAddressLine3 = dtl.NewAddress.Line3,
                Service_BillAddressPostCode = dtl.NewAddress.Postcode,
                Service_BillAddressCity = dtl.NewAddress.Town,
                Service_BillAddressStateId = dtl.NewAddress.StateID,
                Service_AccountNumber = dtl.CustAcctNumber,
                NewAccountNo = dtl.ExistAcctNumber,
                BiometricPass = dtl.CustBiometricPass
            };
            CreatePrintOrderVMForCustInfo(dtl.Customer, vm);
            CreatePrintOrderVMForAddress(dtl.CurrAddress, vm);
            CreatePrintOrderVM(vm, order, dtl);
            return vm;
        }

        private CorpSimRepPrintOrderVM CreatePrintOrderVM(SROrderMaster order, SROrderCorpSimRepDtl dtl)
        {
            var vm = new CorpSimRepPrintOrderVM(){
                PicName=dtl.Customer.FullName,
                IdTypeId=dtl.Customer.IDCardTypeID,
                IdNo=dtl.Customer.IDCardNo,
                CompName=dtl.Customer.BizRegName,
                CompBrn=dtl.Customer.BizRegNo,
                AccNo=dtl.AccountNo,
                BiometricPass=dtl.CustBiometricPass,
                Msisdn=dtl.MSISDN,
                NewSimTypeCode=dtl.NewSimTypeCode,
                NewSimSerial=dtl.NewSimSerial
            };
            CreatePrintOrderVM(vm,order, dtl);
            return vm;
        }

        private TransferOwnershipPrintOrderVM CreatePrintOrderVM(SROrderMaster order, SROrderTransferOwnerDtl dtl)
        {
            var vm = new TransferOwnershipPrintOrderVM()
            {
                CustSignature = new SignatureVM()
                {
                    SignSvg = dtl.SignatureSvg,
                    SignDate = dtl.SignDt
                },

                //Transferee information
                NewOwnerName = dtl.TransfereeCustomer.FullName,
                NewOwnerIDTypeId = dtl.TransfereeCustomer.IDCardTypeID,
                NewOwnerIDCardNumber = dtl.TransfereeCustomer.IDCardNo,
                NewOwnerContactNumber = dtl.TransfereeCustomer.ContactNo,
                NewOwnerAltContactNumber = dtl.TransfereeCustomer.AlternateContactNo,
                NewOwnerDateOfBirth = dtl.TransfereeCustomer.DateOfBirth,
                NewOwnerEmail = dtl.TransfereeCustomer.EmailAddr,
                NewOwnerGender = dtl.TransfereeCustomer.Gender,
                NewOwnerLanguageId = dtl.TransfereeCustomer.LanguageID,
                NewOwnerRaceId = dtl.TransfereeCustomer.RaceID,
                NewOwnerNationalityId = dtl.TransfereeCustomer.NationalityID,
                NewOwnerAddressLine1 = dtl.TransfereeAddress.Line1,
                NewOwnerAddressLine2 = dtl.TransfereeAddress.Line2,
                NewOwnerAddressLine3 = dtl.TransfereeAddress.Line3,
                NewOwnerAddressPostCode = dtl.TransfereeAddress.Postcode,
                NewOwnerAddressCity = dtl.TransfereeAddress.Town,
                NewOwnerAddressStateId = dtl.TransfereeAddress.StateID,
                NewOwnerSignature = new SignatureVM()
                {
                    SignSvg = dtl.TransfereeSignature,
                    SignDate = dtl.SignDt
                },

                //service information
                Service_MSISDN = dtl.TransferorMSISDN,
                Service_AccountNumber = dtl.TransferorAcctNumber,
                NewAccountNo = dtl.TransfereeAcctNumber,
                Service_Plan = dtl.TransferorPlan,
                Service_VAS = dtl.TransferorVAS.Split(';').ToList(),

            };
            CreatePrintOrderVMForCustInfo(dtl.TransferorCustomer, vm);
            CreatePrintOrderVMForAddress(dtl.TransferorAddress, vm);
            CreatePrintOrderVM(vm, order, dtl);
            return vm;
        }

        private void CreatePrintOrderVMForCustInfo(Customer cust, SRPrintOrderWithCustVM vm)
        {
            vm.CustName = cust.FullName;
            vm.CustIDTypeId = cust.IDCardTypeID;
            vm.CustIDCardNumber = cust.IDCardNo;
            vm.CustDateOfBirth = cust.DateOfBirth;
            vm.CustContactNumber = cust.ContactNo;
            vm.CustAltContactNumber = cust.AlternateContactNo;
            vm.CustEmail = cust.EmailAddr;
            vm.CustGender = cust.Gender;
            vm.CustLanguageId = cust.LanguageID;
            vm.CustNationalityId = cust.NationalityID;
            vm.CustRaceId = cust.RaceID;
        }

        private SRViewOrderVM CreateViewOrderVM(CommonController ctrl, SROrderMaster order, SROrderChangeMSISDNDtl dtl)
        {
            var cust = dtl.Customer;
            var address = dtl.Address;
            var vm = new ChangeMSISDNViewOrderVM()
            {
                Service_AccountNumber=dtl.AccountNo,
                Service_MSISDN=dtl.MSISDN,
                NewMSISDN = dtl.NewMSISDN,
            };
            CreateViewOrderVMForCustInfo(cust, vm);
            CreateViewOrderVMForAddress(address, vm);
            return vm;
        }

        private SRViewOrderVM CreateViewOrderVM(CommonController ctrl, SROrderMaster order, SROrderChangeStatusDtl dtl)
        {
            var cust = dtl.Customer;
            var address = dtl.Address;
            var vm = new ChangeStatusViewOrderVM()
            {
                Service_MSISDN = dtl.MSISDN,
                Service_AccountNumber = dtl.AccountNo,
                ChangeStatusTypeId = dtl.ChangeStatusTypeId
            };
            CreateViewOrderVMForCustInfo(cust, vm);
            CreateViewOrderVMForAddress(address, vm);
            return vm;
        }

        private void CreateViewOrderVMForCustInfo(Customer cust, SRViewOrderWithCustVM vm)
        {
            vm.CustName = cust.FullName;
            vm.CustContactNumber = cust.ContactNo;
            vm.CustAltContactNumber = cust.AlternateContactNo;
            vm.CustDateOfBirth = cust.DateOfBirth;
            vm.CustEmail = cust.EmailAddr;
            vm.CustIDTypeId = cust.IDCardTypeID;
            vm.CustIDCardNumber = cust.IDCardNo;
            vm.CustLanguageId = cust.LanguageID;
            vm.CustNationalityId = cust.NationalityID;
            vm.CustRaceId = cust.RaceID;
            vm.CustGender = cust.Gender;
        }

        private void CreateViewOrderVMForAddress(CommonAddress address, SRViewOrderWithCustVM vm)
        {
            vm.CustAddressLine1 = address.Line1;
            vm.CustAddressLine2 = address.Line2;
            vm.CustAddressLine3 = address.Line3;
            vm.CustAddressCity = address.Town;
            vm.CustAddressPostCode = address.Postcode;
            vm.CustAddressStateId = address.StateID;
        }

        internal SRSubmitResultVM CreateSubmitResultVM(SRSubmitResultUploadVM vm, SRServiceResp result)
        {
            var submitVM = new SRSubmitResultVM();
            if (!string.IsNullOrEmpty(result.StackTrace))
            {
                submitVM.Success = false;
                submitVM.ErrorMessage = result.Message;
            }
            submitVM.RequestType = vm.RequestType;
            submitVM.OrderId = vm.OrderId;
            return submitVM;
        }

        internal SRSubmitResultVM CreateSubmitResultVM(SRSummaryVM smVM, SROrderMaster order, SRServiceResp result)
        {
            var submitVM = new SRSubmitResultVM() { 
                RequestType = order.RequestType
            };
            
            //No signature need upload registration form
            //Exclude no signature summary order
            if (String.IsNullOrEmpty(order.Detail.SignatureSvg) && smVM.NewOrder.RequireSignature)
                submitVM = new SRSubmitResultUploadVM() {
                    RequestType=order.RequestType
                };
            //Tag for print
            submitVM.AllowPrint = smVM.NewOrder.AllowPrint;
            if (!string.IsNullOrEmpty(result.StackTrace))
            {
                submitVM.Success = false;
                submitVM.ErrorMessage = result.Message;
            }else{
                submitVM.OrderId = (string)result.Content;
                submitVM.QueueNo = order.Detail.QueueNo;
            }

            return submitVM;
        }

        /// <summary>
        /// Create New Order object base on different controller
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="newObjectOnly">if true only generate new order object, else go individual new order generator method : default false</param>
        /// <returns>New Order object</returns>
        internal SRNewOrderVM CreateNewOrderVM(CommonController ctrl, bool newObjectOnly=false)
        {
            SRNewOrderVM neworderVM = null;
            if (ctrl is BillSeparationController)
                neworderVM = new BillSeparationNewOrderVM();
            else if (ctrl is CorpSimRepController)
                neworderVM = new CorpSimRepNewOrderVM();
            else if (ctrl is PostpaidToPrepaidController)
                neworderVM = new PostpaidToPrepaidNewOrderVM();
            else if (ctrl is PrepaidToPostpaidController)
                neworderVM = new PrepaidToPostpaidNewOrderVM();
            else if (ctrl is ThirdPartyAuthController)
                neworderVM = new ThirdPartyAuthNewOrderVM();
            else if (ctrl is TransferOwnershipController)
                neworderVM = new TransferOwnershipNewOrderVM();
            else if (ctrl is ChangeMSISDNController)
                neworderVM = new ChangeMSISDNNewOrderVM();
            else if (ctrl is ChangeStatusController)
                neworderVM = new ChangeStatusNewOrderVM();
            else
            {
                throw new Exception(
                    string.Format("Sorry there is no new order creation for your controller : {0}",
                    ctrl.GetType().Name));
            }
            if (!newObjectOnly)
            {
                TheVMDataLoader.LoadData(neworderVM);
                neworderVM.RunEligibleCheck();
            }
            return neworderVM;
        }

        /// <summary>
        /// Create new order summary object base on different controller
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="newObjectOnly"></param>
        /// <returns></returns>
        internal SRSummaryVM CreateNewOrderSMVM(CommonController ctrl, bool newObjectOnly = false)
        {
            SRNewOrderVM newOrderVM = ctrl.NewOrderVM;
            SRSummaryVM newSummaryVM=null;
            //Create new object
            if (ctrl is BillSeparationController)
                newSummaryVM = new BillSeparationNewOrderSmVM();
            else if (ctrl is CorpSimRepController)
                newSummaryVM = new CorpSimRepNewOrderSmVM();
            else if (ctrl is PostpaidToPrepaidController)
                newSummaryVM = new PostpaidToPrepaidNewOrderSmVM();
            else if (ctrl is PrepaidToPostpaidController)
                newSummaryVM = new PrepaidToPostpaidNewOrderSmVM();
            else if (ctrl is ThirdPartyAuthController)
                newSummaryVM = new ThirdPartyAuthNewOrderSmVM();
            else if (ctrl is TransferOwnershipController)
                newSummaryVM = new TransferOwnershipNewOrderSmVM();
            else if (ctrl is ChangeMSISDNController)
                newSummaryVM = new ChangeMSISDNNewOrderSmVM();
            else if (ctrl is ChangeStatusController)
                newSummaryVM = new ChangeStatusNewOrderSmVM();
            else
            {
                throw new Exception(
                    string.Format("Sorry there is no new summary order creation for your controller : {0}",
                    ctrl.GetType().Name));
            }
            
            if (!newObjectOnly){
                //Store new order in summary for some information reference
                newSummaryVM.NewOrder = newOrderVM;
                OtherProcess((dynamic)newSummaryVM,ctrl);
            }
            return newSummaryVM;
        }

        private void OtherProcess(SRSummaryVM vm, CommonController ctrl)
        {
            //do nothing
        }

        private void OtherProcess(TransferOwnershipNewOrderSmVM vm, CommonController ctrl)
        {
            vm.ConsolidateNewOwner();
        }

        private void OtherProcess(PostpaidToPrepaidNewOrderSmVM vm, CommonController ctrl)
        {
            var penaltyInfos = ContractUtil.GetPenaltyInfo(vm.NewOrder.MsisdnServiceInfoMap[vm.NewOrder.Msisdn]);
            if (penaltyInfos != null && penaltyInfos.Count > 0)
                vm.NewOrder.EarlyTerminationFee = penaltyInfos.First().Penalty;
        }

        internal SRViewOrderVM CreateNewViewOrderVM(CommonController ctrl, SROrderMaster order)
        {
            SRViewOrderVM vm = null;
            var detail = order.Detail;
            if (detail is SROrderBillSeparationDtl)
                vm = new BillSeparationViewOrderVM();
            else if (detail is SROrderCorpSimRepDtl)
                vm = new CorpSimRepViewOrderVM();//use stub
            else if (detail is SROrderPostToPreDtl)
                vm = new PostpaidToPrepaidViewOrderVM();
            else if (detail is SROrderPreToPostDtl)
                vm = new PrepaidToPostpaidViewOrderVM();
            else if (detail is SROrderThirdPartyAuthDtl)
                vm = new ThirdPartyAuthViewOrderVM();
            else if (detail is SROrderTransferOwnerDtl)
                vm = new TransferOwnershipViewOrderVM();
            else if (detail is SROrderChangeMSISDNDtl)
                vm = new ChangeStatusViewOrderVM();
            else if (detail is SROrderChangeStatusDtl)
                vm = new ChangeStatusViewOrderVM();
            else
            {
                throw new Exception(
                    string.Format("Sorry there is no new view order creation for your controller : {0}",
                    ctrl.GetType().Name));
            }
            return vm;
        }

        internal SRSubmitResultUploadVM CreateSubmitResultUploadVM()
        {
            return new SRSubmitResultUploadVM();
        }

        internal SRViewOrderVM CreateViewOrderVM(CommonController ctrl, SROrderMaster order)
        {
            SRViewOrderVM vm = CreateViewOrderVM(ctrl, order,(dynamic) order.Detail);
            //Update for common properties
            CreateViewOrderVM(vm, order, order.Detail);
            return vm;
        }

        private void CreateViewOrderVM(SRViewOrderVM vm, SROrderMaster order, SROrderDtl dtl)
        {
            vm.Justification = dtl.Justification;
            vm.Remarks = dtl.Remarks;
            vm.Status = order.Status;
            vm.RequestType = dtl.ToRequestType();
            vm.OrderId = order.OrderId;
            vm.LockBy = order.LockBy;
            vm.QueueNo = dtl.QueueNo;
            vm.CreateBy = order.CreateBy;
            vm.TermAndCondition = new TermAndConditionVMBase()
            {
                Agreed = true//always true
            };
            vm.Signature = new SignatureVM()
            {
                SignDate = dtl.SignDt,
                SignSvg = dtl.SignatureSvg
            };
            vm.HasDocument = order.VerificationDocs == null ? false : order.VerificationDocs.Count > 0;
            
        }

        private SRViewOrderVM CreateViewOrderVM(CommonController ctrl, SROrderMaster order, SROrderDtl sROrderDtl)
        {
            throw new NotImplementedException("Cannot use SROrderDtl to create view order VM");
        }

        private SRViewOrderVM CreateViewOrderVM(CommonController ctrl, SROrderMaster order, SROrderCorpSimRepDtl dtl)
        {
            var cust = dtl.Customer;
            var vm=new CorpSimRepViewOrderVM() {
                PicName=cust.PersonInCharge,
                BrnNum=cust.BizRegNo,
                CompName=cust.BizRegName,
                IdNo=cust.IDCardNo,
                IdType=cust.IDCardTypeID,
                Msisdn=dtl.MSISDN,
                NewAccountNo=dtl.AccountNo,
                NewSimTypeCode=dtl.NewSimTypeCode,
                NewSimSerial=dtl.NewSimSerial
            };
            return vm;
        }

        private SRViewOrderVM CreateViewOrderVM(CommonController ctrl, SROrderMaster order, SROrderBillSeparationDtl dtl)
        {
            var cust = dtl.Customer;
            var address = dtl.CurrAddress;
            var vm = new BillSeparationViewOrderVM()
            {
                //customer information
                CustName = cust.FullName,
                CustContactNumber = cust.ContactNo,
                CustAltContactNumber = cust.AlternateContactNo,
                CustDateOfBirth = cust.DateOfBirth,
                CustEmail = cust.EmailAddr,
                CustIDTypeId = cust.IDCardTypeID,
                CustIDCardNumber = cust.IDCardNo,
                CustLanguageId = cust.LanguageID,
                CustNationalityId = cust.NationalityID,
                CustRaceId = cust.RaceID,
                CustGender = cust.Gender,

                //service information
                Service_MSISDN = dtl.CustMSISDN,
                Service_BillAddressLine1 = dtl.NewAddress.Line1,
                Service_BillAddressLine2 = dtl.NewAddress.Line2,
                Service_BillAddressLine3 = dtl.NewAddress.Line3,
                Service_BillAddressPostCode = dtl.NewAddress.Postcode,
                Service_BillAddressCity = dtl.NewAddress.Town,
                Service_BillAddressStateId = dtl.NewAddress.StateID,
                Service_AccountNumber = dtl.CustAcctNumber,
                NewAccountNo = dtl.ExistAcctNumber,
            };
            CreateViewOrderVMForAddress(address, vm);
            return vm;
        }

        private SRViewOrderVM CreateViewOrderVM(CommonController ctrl, SROrderMaster order, SROrderPostToPreDtl dtl)
        {
            var cust = dtl.Customer;
            var address = dtl.CustAddress;
            var vm = new PostpaidToPrepaidViewOrderVM()
            {
                //customer information
                CustName = cust.FullName,
                CustContactNumber = cust.ContactNo,
                CustAltContactNumber = cust.AlternateContactNo,
                CustDateOfBirth = cust.DateOfBirth,
                CustEmail = cust.EmailAddr,
                CustIDTypeId = cust.IDCardTypeID,
                CustIDCardNumber = cust.IDCardNo,
                CustLanguageId = cust.LanguageID,
                CustNationalityId = cust.NationalityID,
                CustRaceId = cust.RaceID,
                CustGender = cust.Gender,

                //service information
                Service_MSISDN = dtl.CustMSISDN,
                Service_AccountNumber = dtl.PostpaidAccNumber,
                NewAccountNo = dtl.PrepaidAccNumber,
                Service_PrepaidPlan = dtl.PrepaidPlan,
                Service_PrepaidSIMTypeCode=dtl.PrepaidSIMTypeCode,
                Service_PrepaidSIMSerialNumber = dtl.PrepaidSIMSerial,
                Service_OutstandingAmount = dtl.OutstandingAmount.ToString(),
                Service_EarlyTerminateFee = dtl.EarlyTerminateFee.ToString()
            };
            CreateViewOrderVMForAddress(address, vm);
            return vm;
        }

        private SRViewOrderVM CreateViewOrderVM(CommonController ctrl, SROrderMaster order, SROrderPreToPostDtl dtl)
        {
            var cust = dtl.Customer;
            var address = dtl.CustAddress;
            var vm = new PrepaidToPostpaidViewOrderVM()
            {
                //service information
                Service_MSISDN = dtl.CustMSISDN,
                NewAccountNo = dtl.PostpaidAccNumber,
                Service_PostpaidPlan = dtl.PostpaidPlan,
                Service_PostpaidSIMSerialNumber = dtl.PostpaidSIMSerial,
                Service_AccountNumber = dtl.PrepaidAccNumber,
            };
            CreateViewOrderVMForCustInfo(cust, vm);
            CreateViewOrderVMForAddress(address, vm);
            return vm;
        }

        private SRViewOrderVM CreateViewOrderVM(CommonController ctrl, SROrderMaster order, SROrderThirdPartyAuthDtl dtl)
        {
            var cust = dtl.Customer;
            var address = dtl.CustAddress;
            var vm = new ThirdPartyAuthViewOrderVM()
            {
                //customer information
                CustName = cust.FullName,
                CustContactNumber = cust.ContactNo,
                CustAltContactNumber = cust.AlternateContactNo,
                CustDateOfBirth = cust.DateOfBirth,
                CustEmail = cust.EmailAddr,
                CustGender = cust.Gender,
                CustIDTypeId = cust.IDCardTypeID,
                CustIDCardNumber = cust.IDCardNo,
                CustLanguageId = cust.LanguageID,
                CustNationalityId = cust.NationalityID,
                CustRaceId = cust.RaceID,

                //service information
                Service_AccountNumber = dtl.AccountNo,
                Service_MSISDN = dtl.MSISDN,

                //third party information
                ThirdParty_Name = dtl.ThirdPartyName,
                ThirdParty_IDTypeId = int.Parse(dtl.ThirdPartyIdType),
                ThirdParty_IDCardNumber = dtl.ThirdPartyIdNumber,
                ThirdParty_AuthorizationTypeId = dtl.ThirdPartyAuthTypeId,
            };
            CreateViewOrderVMForAddress(address, vm);
            return vm;
        }

        private SRViewOrderVM CreateViewOrderVM(CommonController ctrl, SROrderMaster order, SROrderTransferOwnerDtl dtl)
        {
            var transferee_cust = dtl.TransfereeCustomer;
            var transferee_addr = dtl.TransfereeAddress;
            var transferor_cust = dtl.TransferorCustomer;
            var transferor_addr = dtl.TransferorAddress;
            var vm = new TransferOwnershipViewOrderVM()
            {
                //Transferor information
                CustName = transferor_cust.FullName,
                CustIDTypeId = transferor_cust.IDCardTypeID,
                CustIDCardNumber = transferor_cust.IDCardNo,
                CustDateOfBirth = transferor_cust.DateOfBirth,
                CustContactNumber = transferor_cust.ContactNo,
                CustAltContactNumber = transferor_cust.AlternateContactNo,
                CustEmail = transferor_cust.EmailAddr,
                CustGender = transferor_cust.Gender,
                CustLanguageId = transferor_cust.LanguageID,
                CustNationalityId = transferor_cust.NationalityID,
                CustRaceId = transferor_cust.RaceID,
                Signature = new SignatureVM()
                {
                    SignSvg = dtl.SignatureSvg,
                    SignDate = dtl.SignDt
                },

                //Transferee information
                NewOwnerName = transferee_cust.FullName,
                NewOwnerIDTypeId = transferee_cust.IDCardTypeID,
                NewOwnerIDCardNumber = transferee_cust.IDCardNo,
                NewOwnerContactNumber = transferee_cust.ContactNo,
                NewOwnerAltContactNumber = transferee_cust.AlternateContactNo,
                NewOwnerDateOfBirth = transferee_cust.DateOfBirth,
                NewOwnerEmail = transferee_cust.EmailAddr,
                NewOwnerGender = transferee_cust.Gender,
                NewOwnerLanguageId = transferee_cust.LanguageID,
                NewOwnerRaceId = transferee_cust.RaceID,
                NewOwnerNationalityId = transferee_cust.NationalityID,
                NewOwnerAddressLine1 = transferee_addr.Line1,
                NewOwnerAddressLine2 = transferee_addr.Line2,
                NewOwnerAddressLine3 = transferee_addr.Line3,
                NewOwnerAddressPostCode = transferee_addr.Postcode,
                NewOwnerAddressCity = transferee_addr.Town,
                NewOwnerAddressStateId = transferee_addr.StateID,
                NewOwnerSignature = new SignatureVM()
                {
                    SignSvg = dtl.TransfereeSignature,
                    SignDate = dtl.SignDt
                },

                //service information
                Service_MSISDN = dtl.TransferorMSISDN,
                Service_AccountNumber = dtl.TransferorAcctNumber,
                NewAccountNo = dtl.TransfereeAcctNumber,
                Service_Plan = dtl.TransferorPlan,
                Service_VAS = dtl.TransferorVAS.Split(';').ToList(),
            };
            CreateViewOrderVMForAddress(transferee_addr, vm);
            return vm;
        }

        internal NetworkPrintResultVM CreateNetworkPrintResultVM(Exception ex)
        {
            return new NetworkPrintResultVM() {Success=false,ErrorMessage=ex.Message};
        }

        internal FileResultVM CreateFileResultVM(SROrderMaster order)
        {
            var vm = new FileResultVM(order.OrderId);
            foreach (var doc in order.VerificationDocs)
                vm.AddBase64File(doc.FileName, doc.File);
            return vm;
        }

        internal SRViewOrderListVM CreateViewOrderListVM()
        {
            var vm = new SRViewOrderListVM();
            return vm;
        }

        internal List<SRQueryResultItemVM> CreateQueryResultItemListVM(List<SRQueryMaster> lst)
        {
            return lst.Select(n => CreateQueryResultItemVM(n)).ToList();
        }

        internal SRCustInfo CreateCustInfoVM(ICustInfoProvider provider, ServiceTypes serviceType = ServiceTypes.All)
        {
            if(provider.IsExistingCust){
                var vm=new SRCustInfo()
                {
                    Name = provider.CustomerName,
                    IdType = provider.IdType,
                    IdNum = provider.IdNum,
                    Gender = provider.GenderFromId,
                    RaceId = provider.RaceID,
                    Dob=provider.DateOfBirth,
                    Salutation=provider.Salutation,
                    LanguageId = 1,
                    NationalityId = provider.NationalityIDFromIC,
                    Email = provider.Email,
                    ContactNum = provider.ContactNumber,
                    AltContactNum = provider.AlternateContactNumber,
                    BillAddLine1 = provider.AddressLine1,
                    BillAddLine2 = provider.AddressLine2,
                    BillAddLine3 = provider.AddressLine3,
                    BillPostCode = provider.PostCode,
                    BillCity = provider.City,
                    BillState = provider.State,
                    BillStateId=provider.State.ToStateTypeValue(),
                    IsExistingCust=true
                };
                switch(serviceType){
                    case ServiceTypes.All:
                        vm.MsisdnServiceInfoMap = provider.MobileServiceInfoList.ToDictionary(n => n.Msisdn, n => n);
                        break;
                    case ServiceTypes.PostpaidOnly:
                        vm.MsisdnServiceInfoMap = provider.PostServiceInfoList.ToDictionary(n => n.Msisdn, n => n);
                        break;
                    case ServiceTypes.PrepaidOnly:
                        vm.MsisdnServiceInfoMap = provider.PreServiceInfoList.ToDictionary(n => n.Msisdn, n => n);
                        break;
                }
                return vm;
            }
            if(provider.IsBioMetricMatched){
                return new SRCustInfo() {
                    Name=provider.CustomerNameFrmIc,
                    IdType=provider.IdType,
                    IdNum=provider.IdNumFrmIc,
                    Gender=provider.GenderFromId,
                    Dob=provider.DateOfBirthFrmIc,
                    RaceId=provider.RaceFrmIc.ToRaceValue(),
                    NationalityId=provider.NationalityFromIC.ToNationalityValue(),
                    BillAddLine1 = provider.AddressLine1FrmIc,
                    BillAddLine2 = provider.AddressLine2FrmIc,
                    BillAddLine3 = provider.AddressLine3FrmIc,
                    BillPostCode = provider.PostCodeFrmIc,
                    BillCity = provider.CityFrmIc,
                    BillState = provider.StateFrmIc,
                    IsExistingCust = false
                };
            }
            return null;
        }

        internal List<ServiceInfoVM> CreateServiceInfoVMList(IEnumerable<Items> items)
        {
            var lst= items
                .OrderBy(n=>n.ServiceInfoResponse.lob)
                .ThenBy(n=>n.ServiceInfoResponse.prinSuppInd)
                .Select(n=>new ServiceInfoVM(){
                Msisdn=n.ExternalId,
                AccNum=n.AcctExtId,
                Plan=n.ServiceInfoResponse.packageName,
                BillAddLine1=n.Customer.Address1,
                BillAddLine2=n.Customer.Address2,
                BillAddLine3=n.Customer.Address3,
                BillAddCity=n.Customer.City,
                BillAddPostCode=n.Customer.PostCode,
                BillAddState=n.Customer.State.Trim(),
                BillAddStateId=n.Customer.State.ToStateTypeValue(),
                ExternalID=n.AcctExtId,
                SubscriberNo=n.SusbcrNo,
                SubscriberNoReset=n.SusbcrNoResets,
                VasList=n.Packages.Count==0?
                    new List<string>():
                    n.Packages.SelectMany(x=>x.compList).Select(x=>x.componentDesc).ToList()
            }).ToList();
            //add in missing state by postcode
            lst.Where(n => string.IsNullOrEmpty(n.BillAddState) && !string.IsNullOrEmpty(n.BillAddPostCode)).ToList().ForEach(n => {
                n.BillAddState = n.BillAddPostCode.PostCodeToStateDesc();
                n.BillAddStateId = n.BillAddState.ToStateTypeValue();
            });
            return lst;
        }

        internal PopupInfoVM CreatePopupInfoVM(SRVMBase vm)
        {
            if (!string.IsNullOrEmpty(vm.WarningMessage))
                return new PopupInfoVM("Service Request") { 
                    PopupType= PopupInfoVM.PopupTypes.Error,
                    Message=vm.WarningMessage,
                    CustomHeader="Service Request Message",
                    Show=true
                };
            return null;
        }
    }
}