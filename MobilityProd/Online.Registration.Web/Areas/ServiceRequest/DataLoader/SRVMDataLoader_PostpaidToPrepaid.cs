﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;

namespace Online.Registration.Web.Areas.ServiceRequest.DataLoader
{
    public partial class SRVMDataLoader
    {
        private void LoadExistingCustData(PostpaidToPrepaidNewOrderVM vm)
        {
            LoadExistingCustData(vm as SRNewOrderWthCustVM);
            LoadPostpaidServiceInfo(vm as SRNewOrderWthCustVM);
            LoadBreInfo(vm as PostpaidToPrepaidNewOrderVM);
        }

        private void LoadBreInfo(PostpaidToPrepaidNewOrderVM vm)
        {
            vm.EarlyTerminationFee = 0;
            vm.OutstandingBillAmount = _breInfo.OutstandingBillAccountsAmount;
        }
    }
}