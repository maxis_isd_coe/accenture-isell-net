﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;

namespace Online.Registration.Web.Areas.ServiceRequest.DataLoader
{
    public partial class SRVMDataLoader
    {
        private void LoadExistingCustData(BillSeparationNewOrderVM vm)
        {
            LoadExistingCustData(vm as SRNewOrderWthCustVM);
            LoadPostpaidServiceInfo(vm as SRNewOrderWthCustVM);
            vm.ExistMSISDN = vm.MsisdnServiceInfoMap.First().Key;
            vm.ExistAccountNumber = vm.MsisdnServiceInfoMap[vm.ExistMSISDN].AccNum;
            //if customer only one account num will default to split new acc
            if(vm.MsisdnServiceInfoMap.Select(n=>n.Value.AccNum).Distinct().Count()<=1){
                vm.IsSplitNewAcc = true;
                vm.OnlyOneAccNum = true;
            }
        }
    }
}