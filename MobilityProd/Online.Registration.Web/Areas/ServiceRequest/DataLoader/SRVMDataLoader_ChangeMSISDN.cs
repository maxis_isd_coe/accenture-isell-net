﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;

namespace Online.Registration.Web.Areas.ServiceRequest.DataLoader
{
    public partial class SRVMDataLoader
    {
        private void LoadExistingCustData(ChangeMSISDNNewOrderVM vm)
        {
            LoadExistingCustData(vm as SRNewOrderWthCustVM);
            LoadPostpaidServiceInfo(vm as SRNewOrderWthCustVM);
        }
    }
}