﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.Web.ViewModels.Common;

namespace Online.Registration.Web.Areas.ServiceRequest.DataLoader
{
    public partial class SRVMDataLoader
    {
        private void LoadExistingCustData(PrepaidToPostpaidNewOrderVM vm)
        {
            LoadExistingCustData(vm as SRNewOrderWthCustVM);
            LoadPrepaidServiceInfo(vm as SRNewOrderWthCustVM);
            vm.HasPostpaid = _custInfo.PostServiceInfoList!=null && _custInfo.PostServiceInfoList.Count > 0;
        }
    }
}