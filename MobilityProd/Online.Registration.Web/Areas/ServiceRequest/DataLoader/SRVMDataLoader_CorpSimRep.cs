﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;

namespace Online.Registration.Web.Areas.ServiceRequest.DataLoader
{
    public partial class SRVMDataLoader
    {
        private void LoadExistingCustData(CorpSimRepNewOrderVM vm)
        {
            LoadExistingCustData(vm as SRNewOrderWthCustVM);
        }

        private void LoadNewCustWithBiometricPass(CorpSimRepNewOrderVM vm)
        {
            LoadNewCustWithBiometricPass(vm as SRNewOrderWthCustVM);
        }

        private void LoadNewCustWithBiometricFail(CorpSimRepNewOrderVM vm)
        {
            LoadNewCustWithBiometricFail(vm as SRNewOrderWthCustVM);
            vm.AllowEditPersonalInfo = true;
        }
    }
}