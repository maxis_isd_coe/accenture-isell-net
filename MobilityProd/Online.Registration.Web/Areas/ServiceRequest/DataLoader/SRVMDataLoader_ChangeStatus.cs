﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.Web.Helper;

namespace Online.Registration.Web.Areas.ServiceRequest.DataLoader
{
    public partial class SRVMDataLoader
    {
        private void LoadExistingCustData(ChangeStatusNewOrderVM vm)
        {
            LoadExistingCustData(vm as SRNewOrderWthCustVM);
            LoadPostpaidServiceInfo(vm as SRNewOrderWthCustVM);
            //Check for penalty for first msisdn
            if(!string.IsNullOrEmpty(vm.Msisdn)){
                var penalties = ContractUtil.GetPenaltyInfo(vm.MsisdnServiceInfoMap[vm.Msisdn]);
                if (penalties != null && penalties.Count() > 0){
                    vm.ContractPendingMonths = penalties.First().RemainingMonth;
                    vm.EarlyTerminationFee = penalties.First().Penalty;
                    vm.WarningMessage = vm.GetPenaltyWarning();
                }
            }
        }
    }
}