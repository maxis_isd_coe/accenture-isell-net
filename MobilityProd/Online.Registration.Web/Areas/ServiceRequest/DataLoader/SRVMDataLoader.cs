﻿using Online.Registration.Web.Areas.ServiceRequest.Interface;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.Web.Areas.ServiceRequest.Helper;
using Online.Registration.Web.ViewModels.Common;

namespace Online.Registration.Web.Areas.ServiceRequest.DataLoader
{
    public partial class SRVMDataLoader
    {
        private ICustInfoProvider _custInfo;
        private IBreInfoProvider _breInfo;

        public SRVMDataLoader(ICustInfoProvider custInfoProvider, IBreInfoProvider breInfoProvider)
        {
            _custInfo = custInfoProvider;
            _breInfo = breInfoProvider;
        }

        internal void LoadData(SRNewOrderVM vm)
        {
            //Get the bre error for this customer
            vm.BreErrors = _breInfo.GetBreErrors();
            vm.IsExistingCust = _custInfo.IsExistingCust;
            vm.BiometricPass = _custInfo.IsBioMetricMatched;
            vm.OutstandingCheckPass = _breInfo.OutstandingCheckPass;
            vm.WriteoffCheckPass = _breInfo.WriteoffCheckPass;
            vm.DDMFCheckPass = _breInfo.DDMFCheckPass;
            vm.TtlContractCheckPass = _breInfo.TtlContractCheckPass;
            vm.TtlLineCheckPass = _breInfo.TtlLineCheckPass;
            //1 ) when existing customer=>Display data from record
            if(_custInfo.IsExistingCust)
            {
                LoadExistingCustData((dynamic)vm);
            }
            //2) when new customer with biometric match=>Display data from biometric record
            else if(_custInfo.IsBioMetricMatched)
            {
                LoadNewCustWithBiometricPass((dynamic)vm);
            }
            //3) when new customer with biomertic fail=>Allow modified
            else{
                LoadNewCustWithBiometricFail((dynamic)vm);
            }
        }

        private void LoadExistingCustData(SRNewOrderVM vm)
        {

        }

        private void LoadExistingCustData(SRNewOrderWthCustVM vm)
        {
            vm.IdNum = _custInfo.IdNum;
            vm.IdType = _custInfo.IdType.ToIDCardTypeValue();
            vm.CustName = _custInfo.CustomerName;
            //vm.AccNum = _custInfo.AccountNumber;//Not require
            vm.CustBillAddLine1 = _custInfo.AddressLine1;
            vm.CustBillAddLine2 = _custInfo.AddressLine2;
            vm.CustBillAddLine3 = _custInfo.AddressLine3;
            vm.CustBillPostCode = _custInfo.PostCode;
            vm.CustBillCity = _custInfo.City;
            vm.CustBillState = _custInfo.State;
            vm.CustBillStateID = _custInfo.State.ToStateTypeValue();
            if (_custInfo.DateOfBirth!=null && _custInfo.DateOfBirth.Value != null)
                vm.CustDob = _custInfo.DateOfBirth.Value;
            vm.CustEmail = _custInfo.Email;
            vm.CustGender = _custInfo.GenderFromId;
            //Hardcode to english only,follow existing
            vm.CustLanguageID = 1;
            vm.CustNationalityID = _custInfo.NationalityIDFromIC;
            vm.CustRaceID = _custInfo.RaceID;
            vm.CustContactNumber = _custInfo.ContactNumber;
            vm.CustAltContactNumber = _custInfo.AlternateContactNumber;
        }

        private void LoadNewCustWithBiometricPass(SRNewOrderVM vm)
        {

        }

        private void LoadNewCustWithBiometricPass(SRNewOrderWthCustVM vm)
        {
            vm.IdType = SRSession.SearchByIdTypeValue.ToIDCardTypeValue();
            vm.IdNum = SRSession.SearchByIdNumValue;
            vm.CustName = _custInfo.CustomerNameFrmIc;
            vm.CustBillAddLine1 = _custInfo.AddressLine1FrmIc;
            vm.CustBillAddLine2 = _custInfo.AddressLine2FrmIc;
            vm.CustBillAddLine3 = _custInfo.AddressLine3FrmIc;
            vm.CustBillPostCode = _custInfo.PostCodeFrmIc;
            vm.CustBillCity = _custInfo.CityFrmIc;
            vm.CustBillState = _custInfo.StateFrmIc;
            //Should add other
        }

        private void LoadNewCustWithBiometricFail(SRNewOrderWthCustVM vm)
        {
            vm.IdType = SRSession.SearchByIdTypeValue.ToIDCardTypeValue();
            vm.IdNum = SRSession.SearchByIdNumValue;
        }

        private void LoadPostpaidServiceInfo(SRNewOrderWthCustVM vm)
        {
            //Only post msisdn only
            LoadServiceInfo(vm, _custInfo.PostServiceInfoList);
        }

        private void LoadPrepaidServiceInfo(SRNewOrderWthCustVM vm)
        {
            //Only prepaid msisdn only
            LoadServiceInfo(vm, _custInfo.PreServiceInfoList);
        }

        private void LoadAllMobileServiceInfo(SRNewOrderWthCustVM vm)
        {
            //All mobile msisdn
            LoadServiceInfo(vm, _custInfo.MobileServiceInfoList);
        }

        private void LoadServiceInfo(SRNewOrderWthCustVM vm, List<ServiceInfoVM> list)
        {
            vm.MsisdnServiceInfoMap = list.ToDictionary(n => n.Msisdn, n => n);
            if (vm.MsisdnServiceInfoMap.Count > 0)
            {
                vm.MSISDNList = vm.MsisdnServiceInfoMap.Keys.ToList();
                vm.Msisdn = vm.MSISDNList.First();
                vm.AccNum = vm.MsisdnServiceInfoMap[vm.Msisdn].AccNum;
                vm.Plan = vm.MsisdnServiceInfoMap[vm.Msisdn].Plan;
                vm.VasList = vm.MsisdnServiceInfoMap[vm.Msisdn].VasList;
            }
        }
    }
}