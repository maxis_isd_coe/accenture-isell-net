﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using System.Web.Mvc;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;

namespace Online.Registration.Web.Areas.CRP.CRPViewModels
{
    public class DateDifference
    {
        /// <summary>
        /// defining Number of days in month; index 0=> january and 11=> December
        /// february contain either 28 or 29 days, that's why here value is -1
        /// which wil be calculate later.
        /// </summary>
        private int[] monthDay = new int[12] { 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        /// <summary>
        /// contain from date
        /// </summary>
        private DateTime fromDate;

        /// <summary>
        /// contain To Date
        /// </summary>
        private DateTime toDate;

        /// <summary>
        /// this three variable for output representation..
        /// </summary>
        private int year;
        private int month;
        private int day;

        public DateDifference(DateTime d1, DateTime d2)
        {
            int increment;

            if (d1 > d2)
            {
                this.fromDate = d2;
                this.toDate = d1;
            }
            else
            {
                this.fromDate = d1;
                this.toDate = d2;
            }

            /// 
            /// Day Calculation
            /// 
            increment = 0;

            if (this.fromDate.Day > this.toDate.Day)
            {
                increment = this.monthDay[this.fromDate.Month - 1];

            }
            /// if it is february month
            /// if it's to day is less then from day
            if (increment == -1)
            {
                if (DateTime.IsLeapYear(this.fromDate.Year))
                {
                    // leap year february contain 29 days
                    increment = 29;
                }
                else
                {
                    increment = 28;
                }
            }
            if (increment != 0)
            {
                day = (this.toDate.Day + increment) - this.fromDate.Day;
                increment = 1;
            }
            else
            {
                day = this.toDate.Day - this.fromDate.Day;
            }

            ///
            ///month calculation
            ///
            if ((this.fromDate.Month + increment) > this.toDate.Month)
            {
                this.month = (this.toDate.Month + 12) - (this.fromDate.Month + increment);
                increment = 1;
            }
            else
            {
                this.month = (this.toDate.Month) - (this.fromDate.Month + increment);
                increment = 0;
            }

            ///
            /// year calculation
            ///
            this.year = this.toDate.Year - (this.fromDate.Year + increment);

        }

        public override string ToString()
        {
            //return base.ToString();
            return this.year + " Year(s), " + this.month + " month(s), " + this.day + " day(s)";
        }

        public int Years
        {
            get
            {
                return this.year;
            }
        }

        public int Months
        {
            get
            {
                return this.month;
            }
        }

        public int Days
        {
            get
            {
                return this.day;
            }
        }

    }
   
    public class Components
    {
        public string componentId { get; set; }
        public string componentInstId { get; set; }
        public string componentInstIdServ { get; set; }
        public string componentActiveDt { get; set; }
        public string componentInactiveDt { get; set; }
        public string componentDesc { get; set; }
        public string componentShortDisplay { get; set; }
    }
    public class PackageModel
    {
        public string PackageID { get; set; }
        public string PackageDesc { get; set; }

        public string packageInstId { get; set; }
        public string packageInstIdServ { get; set; }
        public string Contract12Months { get; set; }
        public string Contract24Months { get; set; }
        public string packageActiveDt { get; set; }
        public List<Components> compList { get; set; }
    }

    //[Serializable]
    //public class ContractDetails
    //{
    //    //[Required]
    //    [Display(Name = "CMSS ID")]
    //    //[RegularExpression("^[0-9a-zA-Z ]+$", ErrorMessage = "CMSSID must be alpha numeric.")]
    //    public string CMSSID { get; set; }
    //    public string PackageID { get; set; }
    //    public string PackageDesc { get; set; }
    //    public string ContractRemaining { get; set; }
    //    public DateTime EndDate { get; set; }
    //    public DateTime ActiveDate { get; set; }
    //    public int ContractDuration { get; set; }
    //    public string ContractExtend { get; set; }
    //    public string ContractExtendType { get; set; }
    //    public string ComponentDesc { get; set; }
    //    public string ContractTerimnate { get; set; }
    //    public string ContractStartDate { set; get; }
    //    public string ContractEndDate { set; get; }
    //}

    public class CRPModelPenalty
    {
        public int TabNumber { get; set; }


        [Required(ErrorMessage = "Please select discount")]
        public string DiscountType { get; set; }

        [Display(Name = "Phone Model")]
        public string PhoneModel { get; set; }

        [Display(Name = "Phone Color")]
        public string PhoneColor { get; set; }

        [Display(Name = "Device RRP (RM)")]
        public string DevicePrice { get; set; }

        [Display(Name = "Net Price (RM)")]
        public string NetPrice { get; set; }

        [Display(Name = "Discount (RM)")]
        public string DiscountPrice { get; set; }

        [Display(Name = "Approval Username")]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        public string Password { get; set; }

        public bool isPenaltyChecked { get; set; }
    }
    public class ContractDataPlanV
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
   
}