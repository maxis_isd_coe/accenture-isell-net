﻿using log4net;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.NetworkPrintSvc;
using Online.Registration.Web.ViewModels.Common;
using Online.Registration.Web.ViewModels.Inteface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;

namespace Online.Registration.Web.CustomExtension
{
    public static class UtilExtension
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(UtilExtension));

        /// <summary>
        /// Extension method to print file from INetworkPrint object with input html
        /// </summary>
        /// <param name="vm"></param>
        /// <param name="html"></param>
        /// <returns></returns>
        public static NetworkPrintResultVM PrintFile(this INetworkPrint vm,string html){
            if (string.IsNullOrEmpty(html))
                throw new Exception("Cannot print emtpy content.");
            string fileUrl = "";
            NetworkPrintResultVM result = new NetworkPrintResultVM();
            try
            {
                var printer = WebUtility.HtmlDecode(vm.PrinterName);
                string printFolder = System.Configuration.ConfigurationManager.AppSettings["Folderpath"];
                string phyPath = HttpContext.Current.Server.MapPath("~" + printFolder);
                if (!System.IO.Directory.Exists(phyPath))
                    System.IO.Directory.CreateDirectory(phyPath);

                string strPart = string.Format("{0:yyyyMMddhhmmssfff}", System.DateTime.Now) + ".htm";

                using (StreamWriter swXLS = new StreamWriter(phyPath + strPart))
                {
                    swXLS.Write(html.ToString());
                }

                var urlsList = System.Configuration.ConfigurationManager.AppSettings["PrintFilePaths"];
                fileUrl = urlsList + printFolder + "/" + strPart;

                using (var nwPrintClient = new NetworkPrintSvc.NetworkPrintServiceClient())
                {
                    var response = nwPrintClient.Print(new PrintRequest()
                    {
                        FileUrl = fileUrl,
                        PrinterName = printer,
                        RegID = vm.PrintOrderId
                    });
                    HttpContext.Current.Session[SessionKey.PrintErrorMsg.ToString()] = response.Message;
                    result = new NetworkPrintResultVM() {Success=response.Status,ErrorMessage=response.Message};
                }
            }
            catch (Exception ex)
            {
                HttpContext.Current.Session[SessionKey.PrintErrorMsg.ToString()] = "Server exception occured while printing.";
                Logger.Error("Exception in PrintFile : " + fileUrl, ex);
                result.Success = false;
                result.ErrorMessage += ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Extension method to validate sim serial and simtype code from server
        /// </summary>
        /// <param name="sim">sim serial</param>
        /// <param name="simTypeCode">sim type code</param>
        /// <returns></returns>
        public static bool IsValidSimSerial(this string sim,string simTypeCode){
            bool isDealer = Util.SessionAccess.User.isDealer;
            //Get article id
            var articleId = GetSimArticleId(sim);
            if (string.IsNullOrEmpty(articleId)) return false;
            //check in kenan
            if (!IsValidInventoryItem(sim)) return false;
            //Skip further check if is dealer
            if (isDealer) return true;
            //check sim model in inventory
            return IsValidSimModel(articleId, simTypeCode);
        }

        /// <summary>
        /// Check is valid sim model by article id and sim type code
        /// </summary>
        /// <param name="articleId">article id from SAP</param>
        /// <param name="simTypeCode">sim type code</param>
        /// <returns></returns>
        public static bool IsValidSimModel(this string articleId, string simTypeCode)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                if (!string.IsNullOrEmpty(articleId)){
                    var models = proxy.GetModelName(Convert.ToInt32(articleId), simTypeCode);
                    return models.Count() > 0;
                }
            }
            return false;
        }

        /// <summary>
        /// Check is valid inventory item by checking external id
        /// </summary>
        /// <param name="extId">external id</param>
        /// <returns>is valid or false</returns>
        public static bool IsValidInventoryItem(this string extId)
        {
            bool status = false;
            using (var proxy = new KenanServiceProxy())
            {
                status = proxy.validateInventoryExtIdService(new TypeValidateInventoryExtIdRequest{externalId = extId});
            }
            return status;
        }

        /// <summary>
        /// Get article id by sim serial
        /// </summary>
        /// <param name="sim"></param>
        /// <returns></returns>
        public static string GetSimArticleId(this string sim)
        {
            if (string.IsNullOrEmpty(sim) || Util.SessionAccess == null || Util.SessionAccess.User == null) return string.Empty;
            using (var proxy = new DynamicServiceProxy())
            {
                //only need 18 digit to check article
                var response = proxy.GetSerialNumberStatus(Util.SessionAccess.User.Org.OrganisationId, sim.Substring(0,18), Util.SessionAccess.User.Org.WSDLUrl);
                if (!string.IsNullOrEmpty(response) && response.Contains("|"))
                    return response.Split('|')[0];
            }
            return string.Empty;
        }
        /// <summary>
        /// Clone an Object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T CloneObj<T>(this T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        public static string GetExtension(this string fileName)
        {
            return Path.GetExtension(fileName);
        }

        /// <summary>
        /// Get total month diff, exclude date
        /// </summary>
        /// <param name="d1"></param>
        /// <param name="d2"></param>
        /// <returns></returns>
        public static int GetMonthDiff(this DateTime d1,DateTime d2){
            var nD1 = new DateTime(d1.Year, d1.Month, d1.Day);
            var nD2 = new DateTime(d2.Year, d2.Month, d2.Day);
            var diff = DateTime.MinValue + nD1.Subtract(nD2);
            return (diff.Year - 1) * 12 + (diff.Month - 1);
        }
    }
}