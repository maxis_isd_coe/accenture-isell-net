﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Online.Registration.Web.CustomExtension
{
    //-----------------------------------------------------------------------------------
    //  Developer   : Jazz Tong
    //  File Name   : CustomCtrlHtmlExtension.cs
    //  Purpose     : Extension class to provide helper feature for razor page
    //  Version Control:
    //  Version     Date         Change Made
    //  1.0         20 Oct 2015  New
    //-----------------------------------------------------------------------------------
    public static class CustomCtrlHtmlExtension
    {
        public static MvcHtmlString CustomDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, 
            Expression<Func<TModel, TProperty>> expression, 
            IEnumerable<SelectListItem> selectList,
            object htmlAttributes = null)
        {
            var attributesHtml = new StringBuilder();
            if (htmlAttributes != null)
            {
                var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes) as IDictionary<string, object>;
                foreach (var attr in attributes)
                {
                    attributesHtml.Append(string.Format("{0}=\"{1}\"",attr.Key,attr.Value));
                }
            }
            var name = ExpressionHelper.GetExpressionText(expression);
            string value = GetModelValue(htmlHelper,name);
            var findItems=selectList.Where(n => n.Value == value);
            var displayText = "";
            if (findItems.Count() > 0)
                displayText = findItems.First().Text;
            else
                displayText = selectList.Count()>0?selectList.First().Text:"";

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<button data-toggle=\"dropdown\" type=\"button\" id=\"{0}Button\" style=\"height:40px;\" {1} class=\"btn btn-default dropdown-toggle selectby form-dropdown\">", name, attributesHtml);
            sb.AppendFormat("<span id=\"{0}Txt\">{1}</span><span class=\"drop-arrow\"></span>", name, displayText);
            sb.AppendFormat("</button>");
            sb.AppendFormat("<ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dLabel\" display-id=\"{0}Txt\" setval-id=\"{0}\">",name);
            foreach (var selectItem in selectList)
            {
                sb.AppendFormat("<li><a class=\"{0}TypeItem\" href=\"#\" data-value=\"{1}\" onclick=\"select{0}Value($(this))\">{2}</a></li>",name,selectItem.Value,selectItem.Text);
            }
            sb.AppendFormat("</ul>");
            sb.AppendFormat("<script type=\"text/javascript\">");
            sb.AppendFormat("function select{0}Value(obj){{",name);
            sb.AppendFormat("$('#{0}Txt').html(obj.html());$('#{0}Button ~ #{0}').val(obj.attr('data-value')).trigger('change');", name);
            sb.AppendFormat("}}");
            sb.AppendFormat("</script>");
            sb.Append(htmlHelper.HiddenFor(expression).ToHtmlString());
            return new MvcHtmlString(sb.ToString());
        }

        public static MvcHtmlString CustomRadioButtonGroupFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, 
            Expression<Func<TModel, TProperty>> expression, 
            IEnumerable<SelectListItem> selectList,
            object htmlAttributes = null)
        {
            var name = ExpressionHelper.GetExpressionText(expression);
            string value = GetModelValue(htmlHelper, name);
            //set select list to select first
            selectList.First().Selected = true;
            var findItems = selectList.Where(n => n.Value == value);
            if(findItems.Count()>0){
                selectList.ToList().ForEach(n => {
                    if (n.Value == value)
                        n.Selected = true;
                    else
                        n.Selected = false;
                });
            }
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<div>");
            foreach(var selectItem in selectList){
                if(selectItem.Selected)
                    sb.AppendFormat("<input onchange=\"selectRb{1}Value($(this))\" checked=\"{0}\" class=\"form-radio\" data-val=\"{0}\" id=\"rb{1}_{2}\" name=\"rb{1}\" type=\"radio\" value=\"{2}\">", selectItem.Selected, name, selectItem.Value);
                else
                    sb.AppendFormat("<input onchange=\"selectRb{0}Value($(this))\" class=\"form-radio\" id=\"rb{0}_{1}\" name=\"rb{0}\" type=\"radio\" value=\"{1}\">", name, selectItem.Value);
                sb.AppendFormat("<label for=\"rb{0}_{1}\" class=\"stock-label\">{2}</label>", name, selectItem.Value,selectItem.Text);
            }
            sb.Append(htmlHelper.HiddenFor(expression).ToHtmlString());
            sb.AppendFormat("</div>");
            sb.AppendFormat("<script type=\"text/javascript\">");
            sb.AppendFormat("function selectRb{0}Value(obj){{", name);
            sb.AppendFormat("$('#{0}').val(obj.val()).trigger('change');", name);
            sb.AppendFormat("}}");
            sb.AppendFormat("</script>");
            return new MvcHtmlString(sb.ToString());
        }
        


        private static string GetModelValue<TModel>(HtmlHelper<TModel> htmlHelper,string name)
        {
            var valueModel = htmlHelper.ViewData.Model == null ? null : htmlHelper.ViewData.Model.GetType().GetProperties().Where(n => n.Name.Equals(name)).FirstOrDefault();
            string value = "";
            if (valueModel != null)
            {
                var objValue = valueModel.GetValue(htmlHelper.ViewData.Model, null);
                if (objValue!=null && objValue.GetType().IsEnum)
                    value = Convert.ToString((int)(objValue));
                else if (objValue != null)
                    value = Convert.ToString(objValue);
            }
            return value;
        }

        /// <summary>
        /// Generate Name for model property use in razor view ,e.g: abc.c.d
        /// </summary>
        /// <typeparam name="TModel">Model</typeparam>
        /// <typeparam name="TProperty">Model property</typeparam>
        /// <param name="htmlHelper">html helper object</param>
        /// <param name="expression">Linq expression</param>
        /// <returns>Name of model property</returns>
        public static string NameFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));
        }

        /// <summary>
        /// Generate unique ide for model property use in razor view, e.g: abc_c_d
        /// </summary>
        /// <typeparam name="TModel">Model</typeparam>
        /// <typeparam name="TProperty">Model property</typeparam>
        /// <param name="htmlHelper">html helper object</param>
        /// <param name="expression">Linq expression</param>
        /// <returns>Id of model property</returns>
        public static string IdFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(ExpressionHelper.GetExpressionText(expression));
        }
    }
}