﻿using Online.Registration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels;
using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using Online.Registration.DAL.Models.ServiceRequest;
using log4net;
using System.IO;
using System.Web.Hosting;
using Online.Registration.Web.Areas.ServiceRequest.Controllers;
using Online.Registration.Web.ContentRepository;
using Online.Registration.Web.ViewModels.Common;
using System.Globalization;

namespace Online.Registration.Web.CustomExtension
{
    //-----------------------------------------------------------------------------------
    //  Developer   : Jazz Tong
    //  File Name   : TypeConversionExtension.cs
    //  Purpose     : Extension class to provide type conversion
    //  Version Control:
    //  Version     Date         Change Made
    //  1.0         20 Oct 2015  Type Conversion Extension created
    //-----------------------------------------------------------------------------------
    public static class TypeConversionExtension
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TypeConversionExtension));

        private static Dictionary<RequestTypes,string> _srRequestTypeCtrlNameMapping = null;
        public static string ToControllerName(this RequestTypes requestType){
            if (_srRequestTypeCtrlNameMapping == null)
                _srRequestTypeCtrlNameMapping = new Dictionary<RequestTypes,string>(){
                {RequestTypes.CorpSimReplace,(new CorpSimRepController()).GetName()},
                {RequestTypes.BillSeparation,(new BillSeparationController()).GetName()},
                {RequestTypes.Postpaid2Prepaid,(new PostpaidToPrepaidController()).GetName()},
                {RequestTypes.Prepaid2Postpaid,(new PrepaidToPostpaidController()).GetName()},
                {RequestTypes.ThridPartyAuthorization,(new ThirdPartyAuthController()).GetName()},
                {RequestTypes.TransferOwnerShip,(new TransferOwnershipController()).GetName()},
                {RequestTypes.ChangeMSISDN,(new ChangeMSISDNController()).GetName() },
                {RequestTypes.ChangeStatus,(new ChangeStatusController()).GetName() }
            };
            return _srRequestTypeCtrlNameMapping[requestType];
        }

        private static Dictionary<Type, RequestTypes> _srSMTypeMapping = null;
        public static RequestTypes ToRequestType(this SRSummaryVM vm)
        {
            if (_srSMTypeMapping == null)
                _srSMTypeMapping = new Dictionary<Type, RequestTypes>(){
                {typeof(CorpSimRepNewOrderSmVM),RequestTypes.CorpSimReplace},
                {typeof(BillSeparationNewOrderSmVM),RequestTypes.BillSeparation},
                {typeof(PostpaidToPrepaidNewOrderSmVM),RequestTypes.Postpaid2Prepaid},
                {typeof(PrepaidToPostpaidNewOrderSmVM),RequestTypes.Prepaid2Postpaid},
                {typeof(ThirdPartyAuthNewOrderSmVM),RequestTypes.ThridPartyAuthorization},
                {typeof(TransferOwnershipNewOrderSmVM),RequestTypes.TransferOwnerShip},
                {typeof(ChangeMSISDNNewOrderSmVM), RequestTypes.ChangeMSISDN },
                {typeof(ChangeStatusNewOrderSmVM), RequestTypes.ChangeStatus }
            };
            return _srSMTypeMapping[vm.GetType()];
        }

        private static Dictionary<Type, RequestTypes> _srNOTypeMapping = null;
        public static RequestTypes ToRequestType(this SRNewOrderVM vm)
        {
            if (_srNOTypeMapping == null)
                _srNOTypeMapping = new Dictionary<Type, RequestTypes>(){
                {typeof(CorpSimRepNewOrderVM),RequestTypes.CorpSimReplace},
                {typeof(BillSeparationNewOrderVM),RequestTypes.BillSeparation},
                {typeof(PostpaidToPrepaidNewOrderVM),RequestTypes.Postpaid2Prepaid},
                {typeof(PrepaidToPostpaidNewOrderVM),RequestTypes.Prepaid2Postpaid},
                {typeof(ThirdPartyAuthNewOrderVM),RequestTypes.ThridPartyAuthorization},
                {typeof(TransferOwnershipNewOrderVM),RequestTypes.TransferOwnerShip},
                {typeof(ChangeMSISDNNewOrderVM), RequestTypes.ChangeMSISDN },
                {typeof(ChangeStatusNewOrderVM), RequestTypes.ChangeStatus }
            };
            return _srNOTypeMapping[vm.GetType()];
        }

        private static Dictionary<Type, RequestTypes> _srDtlTypeMapping = null;
        public static RequestTypes ToRequestType(this SROrderDtl vm)
        {
            if (_srDtlTypeMapping == null)
                _srDtlTypeMapping = new Dictionary<Type, RequestTypes>(){
                {typeof(SROrderCorpSimRepDtl),RequestTypes.CorpSimReplace},
                {typeof(SROrderBillSeparationDtl),RequestTypes.BillSeparation},
                {typeof(SROrderPostToPreDtl),RequestTypes.Postpaid2Prepaid},
                {typeof(SROrderPreToPostDtl),RequestTypes.Prepaid2Postpaid},
                {typeof(SROrderThirdPartyAuthDtl),RequestTypes.ThridPartyAuthorization},
                {typeof(SROrderTransferOwnerDtl),RequestTypes.TransferOwnerShip},
                {typeof(SROrderChangeMSISDNDtl), RequestTypes.ChangeMSISDN },
                {typeof(SROrderChangeStatusDtl), RequestTypes.ChangeStatus }
            };
            return _srDtlTypeMapping[vm.GetType()];
        }

        /// <summary>
        /// Convert list of string to list of select items
        /// </summary>
        /// <param name="list"></param>
        /// <param name="defaultText"></param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> ToSelectList(this IEnumerable<string> list)
        {
            var selectLst = list.Select(
                n => new SelectListItem()
                {
                    Text = n,
                    Value = n
                }).ToList();
            return selectLst;
        }

        /// <summary>
        /// Convert list of string to list of selectitems with default selection
        /// </summary>
        /// <param name="list"></param>
        /// <param name="defaultText"></param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> ToSelectList2(this IEnumerable<string> list,
            string defaultText = "Please Select")
        {
            var selectLst = list.Select(
                n => new SelectListItem()
                {
                    Text = n,
                    Value = n
                }).ToList();
            selectLst.Insert(0, new SelectListItem() { Text = defaultText });
            return selectLst;
        }

        public static string ToIdTypeKenanEnumStr(this string value){
            var str = "";
            switch (value)
            {
                case "1":
                    str = "NEWIC";
                    break;
                case "2":
                    str = "PASSPORT";
                    break;
                case "3":
                    str = "OLDIC";
                    break;
                case "4":
                    str = "OTHERIC";
                    break;
                case "6":
                    str = "MSISDN";
                    break;
                case "1001":
                    str = "ACCT";
                    break;
                default: str = "NEWIC";
                    break;
            }
            return str;
        }

        public static string ToIDCardTypeValue(this string display)
        {
            if (string.IsNullOrEmpty(display)) return string.Empty;
            // TODO : Should find a place to centalize it
            var value = "";
            switch (display.ToUpper())
            {
                case "NEWIC":
                    value = "1";
                    break;
                case "PASSPORT":
                    value = "2";
                    break;
                case "OLDIC":
                    value = "3";
                    break;
                case "OTHERIC":
                    value = "4";
                    break;
                case "MSISDN":
                    value = "6";
                    break;
                case "ACCT":
                    value = "1001";
                    break;
            }
            if (!string.IsNullOrEmpty(value))
            {
                return value;
            }

            var first = MasterDataCache.Instance.IDCardType.IDCardType.Where(n => n.Description.ToUpper() == display.ToUpper()).FirstOrDefault();
            return first==null?"":first.ID.ToString();
        }

        /// <summary>
        /// Return html attribute for checked="Checked" attribute in use in page
        /// </summary>
        /// <param name="value">boolean value</param>
        /// <returns>html value : string</returns>
        public static string ToCheckboxHtmlAttr(this bool value){
            if (value)
                return string.Format("checked = \"{0}\"", "checked");
            return "";
        }

        /// <summary>
        /// Convert result boolean to biometric display string
        /// </summary>
        /// <param name="result">biometric result : bool</param>
        /// <returns>display string</returns>
        public static string ToBiometricResult(this bool result){
            if (!result)
                return "Not Performed / Failed";
            return "Matched";
        }

        /// <summary>
        /// Get plan name by plan id
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public static string ToPostpaidPlanName(this string planId)
        {
            return StaticListVM.Instance.PostpaidPlanList.Where(n => n.Key == planId).FirstOrDefault().Value;
        }
        /// <summary>
        /// Shorthand method to get id card name
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToIdCardTypeDesc(this int value)
        {
            return Util.GetNameByID(RefType.IDCardType, value);
        }
        /// <summary>
        /// to get id desc
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToIdCardTypeDesc(this string value)
        {
            if (string.IsNullOrEmpty(value)) return "";
            return Util.GetNameByID(RefType.IDCardType, int.Parse(value));
        }
        /// <summary>
        /// To gender description from value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToGenderDesc(this string value){
            switch (value)
            {
                case "M":
                    return "Male";
                case "F":
                    return "Female";
                default:
                    return "";
            }
        }

        /// <summary>
        /// To language desc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ToLangaugeDesc(this int id)
        {
            return Util.GetNameByID(RefType.Language, id);
        }
        /// <summary>
        /// Get Third party type desc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ToThirdPartyTypeDesc(this int id)
        {
            var find = MasterDataCache.Instance.ThirdPartyAuthType.ThirdPartyAuthTypes.Where(n => n.ID == id).FirstOrDefault();
            return find==null?"":find.Name;
        }
        /// <summary>
        /// Get nationality desc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ToNationalityDesc(this int id){
            return Util.GetNameByID(RefType.Nationality, id);
        }
        /// <summary>
        /// Get race desction by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ToRaceDesc(this int id){
            return Util.GetNameByID(RefType.Race, id);
        }
        /// <summary>
        /// Get Race id from name
        /// </summary>
        /// <param name="desc"></param>
        /// <returns></returns>
        public static int ToRaceValue(this string name){
            var newName=name;
            //find by type from biometric,name from biometric is diff
            switch(name.ToUpper()){
                case "MELAYU":
                    newName = "MALAY";
                    break;
                case "CINA":
                    newName = "CHINESE";
                    break;
                case "INDIA":
                    newName = "INDIAN";
                    break;
                default:
                    newName = "OTHERS";
                    break;
            }
            var find = MasterDataCache.Instance.Race.Races.Where(n => n.Name.ToUpper().Equals(newName)).FirstOrDefault();
            if (find != null)
                return find.ID;
            return 0;
        }
        /// <summary>
        /// Get nationality id from name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int ToNationalityValue(this string name){
            var newName = name;
            switch(name.ToUpper()){
                case "WARGANEGARA":
                    newName = "MALAYSIAN";
                    break;
            }
            var find = MasterDataCache.Instance.Nationality.Nationalities.Where(n => n.Name.ToUpper().Equals(newName)).FirstOrDefault();
            return find != null ? find.ID : 0;
        }
        /// <summary>
        /// Get state desc by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ToStateDesc(this int id){
            return Util.GetNameByID(RefType.State, id);
        }
        /// <summary>
        /// Convert error json from Model State dictionary with new object with Error property name
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        public static Dictionary<string,object> ToErrorJson(this ModelStateDictionary  modelState){
            var errorList = modelState.Where(n => n.Value.Errors.Count > 0).ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray());
            return new Dictionary<string, object>() { { "Error", errorList } };
        }
        /// <summary>
        /// Convert state name to id
        /// </summary>
        /// <param name="desc">state name or desc</param>
        /// <returns>int of state id</returns>
        public static int ToStateTypeValue(this string desc)
        {
            if (String.IsNullOrEmpty(desc.Trim()))
                return 0;
            var vState = MasterDataCache.Instance.State;
            if (vState != null && vState.State.Any()){
                var find=vState.State.Where(a => a.Name.ToUpper() == desc.ToUpper()).FirstOrDefault();
                return find!=null?find.ID:0;
            }
            return 0;
        }
        /// <summary>
        /// Get state id from postcode
        /// </summary>
        /// <param name="postCode"></param>
        /// <returns></returns>
        public static string PostCodeToStateDesc(this string postCode){
            var postCodeList = MasterDataCache.Instance.GetPostCodeCityState;
            var find= postCodeList.Where(n => n.PostCode == postCode && n.Active).FirstOrDefault();
            if (find != null)
                return find.State;
            return string.Empty;
        }
        /// <summary>
        /// Convert SR doctypes to file name to avoid duplicate
        /// </summary>
        /// <param name="docType"></param>
        /// <returns></returns>
        public static string ToFileName(this DocTypes docType)
        {
            return docType.ToString();
        }
        /// <summary>
        /// Convert SR doctypes to iContract doc type for folder categories
        /// </summary>
        /// <param name="docType"></param>
        /// <returns></returns>
        public static string ToIContractDocType(this DocTypes docType){
            switch(docType){
                case DocTypes.IdBack:
                case DocTypes.IdFront:
                case DocTypes.ThirdPartyIdBack:
                case DocTypes.ThirdPartyIdFront:
                case DocTypes.TransfereeIdBack:
                case DocTypes.TransfereeIdFront:
                    return "IC";
                case DocTypes.OtherDoc:
                case DocTypes.LetterOfAuth:
                    return "other_ID";
                case DocTypes.Contract:
                case DocTypes.ScanContract:
                    return "Contract";

            }
            throw new Exception(string.Format("Not support doc type - {0}",docType.ToString()));
        }

        /// <summary>
        /// Convert html to pdf
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static byte[] ToPdf(this string html){
            //TODO : Need enhance using regular expression to more generic convertion
            String imageFileLocation = HttpContext.Current.Server.MapPath("/");
            if (imageFileLocation != null)
            {
                imageFileLocation = imageFileLocation.Replace("\\", "/");
            }
            Logger.Info("imageFileLocation /: " + imageFileLocation);

            if (!File.Exists(imageFileLocation + "/Content/images/maxis_use_only_s.png"))
            {
                imageFileLocation = HttpContext.Current.Server.MapPath("~");

                if (imageFileLocation != null)
                {
                    imageFileLocation = imageFileLocation.Replace("\\", "/");
                }
                Logger.Info("imageFileLocation ~: " + imageFileLocation);
            }


            String applicationVirtualPath = HostingEnvironment.ApplicationVirtualPath;
            Logger.Info("System.Web.Hosting.HostingEnvironment.SiteName: " + HostingEnvironment.SiteName);
            Logger.Info("System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath: " + HostingEnvironment.ApplicationVirtualPath);

            html = html.Replace("<div class=\"pageBreak3\">&nbsp;</div>", "<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />");
            html = html.Replace("<div class=\"pageBreak2\"></div> ", "<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />");

            if (applicationVirtualPath.EndsWith("/"))
            {
                html = html.Replace(applicationVirtualPath + "Content/images/maxislogo.png", imageFileLocation + "/Content/images/maxislogo.png");
                html = html.Replace(applicationVirtualPath + "Content/images/maxis_use_only_s.png", imageFileLocation + "/Content/images/maxis_use_only_s.png");
            }
            else
            {
                html = html.Replace(applicationVirtualPath + "/Content/images/maxislogo.png", imageFileLocation + "/Content/images/maxislogo.png");
                html = html.Replace(applicationVirtualPath + "/Content/images/maxis_use_only_s.png", imageFileLocation + "/Content/images/maxis_use_only_s.png");
            }

            Logger.Info("html to PDF: " + html);
            //File.WriteAllText(@"c:\debug.htm", html);
            byte[] PDFBytes = (new NReco.PdfGenerator.HtmlToPdfConverter()).GeneratePdf(html);
            //File.WriteAllBytes(@"c:\debug.pdf",PDFBytes);
            return PDFBytes;
        }

        /// <summary>
        /// Convert id to string for change status type
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string ToChangeStatusTypeDesc(this int id){
            return ((ChangeStatusTypes)id).ToString();
        }

        /// <summary>
        /// Convert string with format yyyyMMdd to date
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime ToDate(this string dt){
            return DateTime.ParseExact(dt, "yyyyMMdd", CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Convert string to decimal
        /// </summary>
        /// <param name="dc"></param>
        /// <returns></returns>
        public static Decimal ToDecimal(this string dc){
            decimal v;
            Decimal.TryParse(dc,out v);
            return v;
        }
    }
}