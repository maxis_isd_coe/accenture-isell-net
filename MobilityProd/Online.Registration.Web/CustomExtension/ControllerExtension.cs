﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace Online.Registration.Web.CustomExtension
{
    public static class ControllerExtension
    {
        /// <summary>
        /// Get the name of controller, without controller posfix
        /// </summary>
        /// <param name="ctrl">obj of controller type</param>
        /// <returns>String of controller name</returns>
        public static string GetName(this Controller ctrl)
        {
            return ctrl.GetType().Name.Replace("Controller", "");
        }

        /// <summary>
        /// Get html result from action
        /// </summary>
        /// <param name="ctrl">Controller</param>
        /// <param name="action">action name</param>
        /// <param name="routeValues">route value with dictionary format</param>
        /// <returns>Html string</returns>
        public static string GetActionResultHtml(this Controller ctrl, string action, object routeValues)
        {
            return GetActionResultHtml(ctrl, null,action, routeValues);
        }

        /// <summary>
        /// Get html result from action
        /// </summary>
        /// <param name="ctrl">Controller</param>
        /// <param name="controllerName">Target controller name</param>
        /// <param name="action">action name</param>
        /// <param name="routeValues">route paramter</param>
        /// <returns>html result</returns>
        public static string GetActionResultHtml(this Controller ctrl,Controller targetCtrl, string action, object routeValues)
        {
            // Create memory writer.
            var sb = new StringBuilder();
            var memWriter = new StringWriter(sb);
            var oriRouteDataDic = ctrl.ControllerContext.RouteData==null? 
                null : 
                ctrl.ControllerContext.RouteData.Values.ToDictionary(n=>n.Key,n=>n.Value);
            // Create fake http context to render the view.
            var fakeResponse = new HttpResponse(memWriter);
            var fakeContext = new HttpContext(System.Web.HttpContext.Current.Request,
                fakeResponse);
            
            var fakeControllerContext = new ControllerContext(
                new HttpContextWrapper(fakeContext),
                ctrl.ControllerContext.RouteData,
                ctrl.ControllerContext.Controller);
            var oldContext = System.Web.HttpContext.Current;
            //reuse session
            fakeControllerContext.HttpContext.Items.Add("AspSession", System.Web.HttpContext.Current.Session);
            //reuse authenticationCookies
            var authenticationCookies = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            fakeControllerContext.HttpContext.Request.Cookies.Add(authenticationCookies);
            //set user
            fakeControllerContext.HttpContext.User = HttpContext.Current.User;
            System.Web.HttpContext.Current = fakeContext;
            var controllerName = targetCtrl == null ? ctrl.GetName() : targetCtrl.GetName();
            fakeControllerContext.RouteData.Values["controller"] = controllerName;
            fakeControllerContext.RouteData.Values["action"] = action;
            var routes = HtmlHelper.AnonymousObjectToHtmlAttributes(routeValues) as IDictionary<string, object>;
            foreach (var attr in routes)
                fakeControllerContext.RouteData.Values[attr.Key] = attr.Value;
            // Render the view.
            var ac = ctrl.GetType().GetMethod(action).Invoke(ctrl, routes.Values.ToArray()) as ActionResult;
            ac.ExecuteResult(fakeControllerContext);
            // Restore old context.
            System.Web.HttpContext.Current = oldContext;
            if (oriRouteDataDic != null)
            {
                foreach (var v in oriRouteDataDic)
                    ctrl.ControllerContext.RouteData.Values[v.Key] = v.Value;
            }
            // Flush memory and return output.
            memWriter.Flush();
            return sb.ToString();
        }
    }
}