﻿using Online.Registration.DAL.Models.ServiceRequest.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.CustomExtension
{
    //-----------------------------------------------------------------------------------
    //  Developer   : Jazz Tong
    //  File Name   : LabelMsgExtension.cs
    //  Purpose     : Extension class to provide type conversion
    //  Version Control:
    //  Version     Date         Change Made
    //  1.0         20 Oct 2015  Extension class to manage label display in mvc project
    //-----------------------------------------------------------------------------------
    public static class LabelMsgExtension
    {
        public static string GetLabel(this SRStatus status)
        {
            switch (status)
            {
                case SRStatus.New:
                    return "New";
                case SRStatus.Cancelled:
                    return "Cancelled";
                case SRStatus.Completed:
                    return "Completed";
                case SRStatus.Rejected:
                    return "Rejected";
                default:
                    return "<<Default SRStatus Label>>";
            }
        }

        public static string GetLabel(this RequestTypes srType)
        {
            switch (srType)
            {
                case RequestTypes.BillSeparation:
                    return "Bill Separation";
                case RequestTypes.CorpSimReplace:
                    return "Corporate SIM Replacement";
                case RequestTypes.Postpaid2Prepaid:
                    return "Postpaid to Prepaid";
                case RequestTypes.Prepaid2Postpaid:
                    return "Prepaid to Postpaid";
                case RequestTypes.ThridPartyAuthorization:
                    return "Third Party Authorization";
                case RequestTypes.TransferOwnerShip:
                    return "Transfer of Ownership";
                case RequestTypes.ChangeMSISDN:
                    return "Change MSISDN";
                case RequestTypes.ChangeStatus:
                    return "Change Status";
                default:
                    return "<<Default Label>>";
            }
        }
    }
}