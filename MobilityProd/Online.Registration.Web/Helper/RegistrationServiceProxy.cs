﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.DAL.Models;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.DAL;
using System.Xml;

namespace Online.Registration.Web.Helper
{
    public class RegistrationServiceProxy : ServiceProxyBase<RegistrationServiceClient> 
    {
        public string LastErrorMessage { get; set; }

        public RegistrationServiceProxy()
        {
            _client = new RegistrationServiceClient();
        }

        #region Private Methods

        private void LogResponse(string code, string message, string stackTrace = "")
        {
            LastErrorMessage = "";

            if (!Util.IsSuccessCode(code))
            {
                LastErrorMessage = message;

                // Log Error
                //Logger.ErrorFormat("Code: {0}|Message: {1}|StackTrace: {2}", code, message, stackTrace);
            }
        }

        private void LogResponse(WCFResponse response)
        {
            LogResponse(response.Code, response.Message, response.StackTrace);
        }

        #endregion

        #region Public Methods

        public bool IsLastCallFailed()
        {
            return !string.IsNullOrEmpty(LastErrorMessage);
        }

        #endregion

        #region Admin

        #region AddressType
        public AddressTypeCreateResp AddressTypeCreate(AddressType addressType)
        {
            var addressTypeCreateRq = new AddressTypeCreateReq()
            {
                AddressType = addressType
            };

            var resp = _client.AddressTypeCreate(addressTypeCreateRq);
            return resp;
        }
        public IEnumerable<int> AddressTypeFind(AddressTypeFind AddressType)
        {
            var request = new AddressTypeFindReq()
            {
                AddressTypeFind = AddressType
            };

            var response = _client.AddressTypeFind(request);
            return response.IDs;
        }
        public DAL.Models.AddressType AddressTypeGet(int AddressTypeID)
        {
            var AddressTypes = AddressTypeGet(new int[] { AddressTypeID });
            return AddressTypes == null ? null : AddressTypes.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.AddressType> AddressTypeGet(IEnumerable<int> AddressTypes)
        {
            if (AddressTypes == null)
                return new List<AddressType>(); ;

            var request = new AddressTypeGetReq()
            {
                IDs = AddressTypes.ToArray()
            };

            var response = _client.AddressTypeGet(request);

            return response.AddressTypes;
        }

        public AddressTypeGetResp AddressTypeList()
        {
            var response = _client.AddressTypeList();
            return response;
        }

        public IEnumerable<DAL.Models.AddressType> FindAddressType(AddressTypeFind AddressType)
        {
            return AddressTypeGet(AddressTypeFind(AddressType));
        }
        public void AddressTypeUpdate(AddressType addressType)
        {
            var addressTypeUpdateRq = new AddressTypeUpdateReq()
            {
                AddressType = addressType
            };

            var resp = _client.AddressTypeUpdate(addressTypeUpdateRq);
        }
        #endregion

        #region Bank
        public BankCreateResp BankCreate(Bank bank)
        {
            var bankCreateRq = new BankCreateReq()
            {
                Bank = bank
            };

            var resp = _client.BankCreate(bankCreateRq);
            return resp;
        }
        public IEnumerable<int> BankFind(BankFind bank)
        {
            var request = new BankFindReq()
            {
                BankFind = bank
            };

            var response = _client.BankFind(request);
            return response.IDs;
        }
        public Bank BankGet(int bankID)
        {
            var banks = BankGet(new int[] { bankID });
            return banks == null ? null : banks.SingleOrDefault();
        }
        public IEnumerable<Bank> BankGet(IEnumerable<int> banks)
        {
            if (banks == null)
                return new List<Bank>();

            var request = new BankGetReq()
            {
                IDs = banks.ToArray()
            };

            var response = _client.BankGet(request);

            return response.Bank;
        }
        public static IEnumerable<DAL.Models.Bank> FindBank(BankFind Bank)
        {
            using (var proxy = new RegistrationServiceProxy())
            {
                return proxy.BankGet(proxy.BankFind(Bank));
            }
        }
        public void BankUpdate(Bank bank)
        {
            var bankUpdateReq = new BankUpdateReq()
            {
                Bank = bank
            };

            var resp = _client.BankUpdate(bankUpdateReq);
        }
        #endregion

        #region CardType
        public CardTypeCreateResp CardTypeCreate(CardType cardType)
        {
            var cardTypeCreateRq = new CardTypeCreateReq()
            {
                CardType = cardType
            };

            var resp = _client.CardTypeCreate(cardTypeCreateRq);
            return resp;
        }
        public IEnumerable<int> CardTypeFind(CardTypeFind CardType)
        {
            var request = new CardTypeFindReq()
            {
                CardTypeFind = CardType
            };

            var response = _client.CardTypeFind(request);
            return response.IDs;
        }
        public DAL.Models.CardType CardTypeGet(int CardTypeID)
        {
            var CardTypes = CardTypeGet(new int[] { CardTypeID });
            return CardTypes == null ? null : CardTypes.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.CardType> CardTypeGet(IEnumerable<int> CardTypes)
        {
            if (CardTypes == null)
                return new List<CardType>(); ;

            var request = new CardTypeGetReq()
            {
                IDs = CardTypes.ToArray()
            };

            var response = _client.CardTypeGet(request);

            return response.CardType;
        }

        public CardTypeGetResp CardTypeList()
        {
            var response = _client.CardTypeList();
            return response;
        }

        public IEnumerable<DAL.Models.CardType> FindCardType(CardTypeFind CardType)
        {

            return CardTypeGet(CardTypeFind(CardType));
        }
        public void CardTypeUpdate(CardType cardType)
        {
            var cardTypeUpdateRq = new CardTypeUpdateReq()
            {
                CardType = cardType
            };

            var resp = _client.CardTypeUpdate(cardTypeUpdateRq);
        }
        #endregion

        #region CustomerTitle
        public IEnumerable<int> CustomerTitleFind(CustomerTitleFind CustomerTitle)
        {
            var request = new CustomerTitleFindReq()
            {
                CustomerTitleFind = CustomerTitle
            };

            var response = _client.CustomerTitleFind(request);
            return response.IDs;
        }
        public DAL.Models.CustomerTitle CustomerTitleGet(int CustomerTitleID)
        {
            var CustomerTitles = CustomerTitleGet(new int[] { CustomerTitleID });
            return CustomerTitles == null ? null : CustomerTitles.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.CustomerTitle> CustomerTitleGet(IEnumerable<int> CustomerTitles)
        {
            if (CustomerTitles == null)
                return new List<CustomerTitle>();

            var request = new CustomerTitleGetReq()
            {
                IDs = CustomerTitles.ToArray()
            };

            var response = _client.CustomerTitleGet(request);

            return response.CustomerTitles;
        }

        public CustomerTitleGetResp CustomerTitleList()
        {
            var response = _client.CustomerTitleList();
            return response;
        }

        public IEnumerable<DAL.Models.CustomerTitle> FindCustomerTitle(CustomerTitleFind CustomerTitle)
        {
            return CustomerTitleGet(CustomerTitleFind(CustomerTitle));
        }
        public void CustomerTitleUpdate(CustomerTitle customerTitle)
        {
            var customerTitleUpdateRq = new CustomerTitleUpdateReq()
            {
                CustomerTitle = customerTitle
            };

            var resp = _client.CustomerTitleUpdate(customerTitleUpdateRq);
        }
        public CustomerTitleCreateResp CustomerTitleCreate(CustomerTitle customerTitle)
        {
            var customerTitleCreateRq = new CustomerTitleCreateReq()
            {
                CustomerTitle = customerTitle
            };

            var resp = _client.CustomerTitleCreate(customerTitleCreateRq);
            return resp;
        }
        #endregion

        #region IDCardType
        public IDCardTypeCreateResp IDCardTypeCreate(IDCardType idCardType)
        {
            var idCardTypeCreateRq = new IDCardTypeCreateReq()
            {
                IDCardType = idCardType
            };

            var resp = _client.IDCardTypeCreate(idCardTypeCreateRq);
            return resp;
        }
        public IEnumerable<int> IDCardTypeFind(IDCardTypeFind IDCardType)
        {
            var request = new IDCardTypeFindReq()
            {
                IDCardTypeFind = IDCardType
            };

            var response = _client.IDCardTypeFind(request);
            return response.IDs;
        }
        public DAL.Models.IDCardType IDCardTypeGet(int IDCardTypeID)
        {
            var IDCardTypes = IDCardTypeGet(new int[] { IDCardTypeID });
            return IDCardTypes == null ? null : IDCardTypes.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.IDCardType> IDCardTypeGet(IEnumerable<int> IDCardTypes)
        {
            if (IDCardTypes == null)
                return new List<IDCardType>(); ;

            var request = new IDCardTypeGetReq()
            {
                IDs = IDCardTypes.ToArray()
            };

            var response = _client.IDCardTypeGet(request);

            return response.IDCardType;
        }

        public IDCardTypeGetResp IDCardTypeList()
        {
            var response = _client.IDCardTypeList();
            return response;
        }

        public IEnumerable<DAL.Models.IDCardType> FindIDCardType(IDCardTypeFind IDCardType)
        {
            return IDCardTypeGet(IDCardTypeFind(IDCardType));
        }
        public void IDCardTypeUpdate(IDCardType idCardType)
        {
            var idCardTypeUpdateRq = new IDCardTypeUpdateReq
            {
                IDCardType = idCardType
            };
            var resp = _client.IDCardTypeUpdate(idCardTypeUpdateRq);
        }
        #endregion

        #region State
        public StateCreateResp StateCreate(State state)
        {
            var stateCreateRq = new StateCreateReq()
            {
                State = state
            };

            var resp = _client.StateCreate(stateCreateRq);
            return resp;
        }
        public IEnumerable<int> StateFind(StateFind state)
        {
            var request = new StateFindReq()
            {
                StateFind = state
            };

            var response = _client.StateFind(request);
            return response.IDs;
        }
        public void StateUpdate(State state)
        {
            var stateUpdateRq = new StateUpdateReq()
            {
                State = state
            };

            var resp = _client.StateUpdate(stateUpdateRq);
        }
        public DAL.Models.State StateGet(int StateID)
        {
            var States = StateGet(new int[] { StateID });
            return States == null ? null : States.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.State> StateGet(IEnumerable<int> States)
        {
            if (States == null)
                return new List<State>(); ;

            var request = new StateGetReq()
            {
                IDs = States.ToArray()
            };

            var response = _client.StateGet(request);

            return response.State;
        }

        public StateGetResp StateList()
        {
            var response = _client.StateList();
            return response;
        }
        public IEnumerable<DAL.Models.State> FindState(StateFind State)
        {
            //using (var proxy = new RegistrationServiceProxy())
            //{
            //    return proxy.StateGet(proxy.StateFind(State));
            //}

            return StateGet(StateFind(State));
        }
        #endregion

        #region PaymentMode
        public PaymentModeCreateResp PaymentModeCreate(PaymentMode PaymentMode)
        {
            var PaymentModeCreateReq = new PaymentModeCreateReq()
            {
                PaymentMode = PaymentMode
            };

            var resp = _client.PaymentModeCreate(PaymentModeCreateReq);
            return resp;
        }
        public IEnumerable<int> PaymentModeFind(PaymentModeFind PaymentMode)
        {
            var request = new PaymentModeFindReq()
            {
                PaymentModeFind = PaymentMode
            };

            var response = _client.PaymentModeFind(request);
            return response.IDs;
        }
        public DAL.Models.PaymentMode PaymentModeGet(int PaymentModeID)
        {
            var PaymentModes = PaymentModeGet(new int[] { PaymentModeID });
            return PaymentModes == null ? null : PaymentModes.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.PaymentMode> PaymentModeGet(IEnumerable<int> PaymentModes)
        {
            if (PaymentModes == null)
                return new List<PaymentMode>(); ;

            var request = new PaymentModeGetReq()
            {
                IDs = PaymentModes.ToArray()
            };

            var response = _client.PaymentModeGet(request);

            return response.PaymentMode;
        }

        public PaymentModeGetResp PaymentModeList()
        {
            var response = _client.PaymentModeList();
            return response;
        }

        public IEnumerable<DAL.Models.PaymentMode> FindPaymentMode(PaymentModeFind PaymentMode)
        {
            return PaymentModeGet(PaymentModeFind(PaymentMode));
        }
        public void PaymentModeUpdate(PaymentMode PaymentMode)
        {
            var PaymentModeUpdateReq = new PaymentModeUpdateReq
            {
                PaymentMode = PaymentMode
            };
            var resp = _client.PaymentModeUpdate(PaymentModeUpdateReq);
        }
        #endregion

        #region Payment Mode Payment Details
        public PaymentModePaymentDetailsCreateResp PaymentModePaymentDetailsCreate(PaymentModePaymentDetails PaymentModePaymentDetails)
        {
            var PaymentModePaymentDetailsCreateReq = new PaymentModePaymentDetailsCreateReq()
            {
                PaymentModePaymentDetails = PaymentModePaymentDetails
            };

            var resp = _client.PaymentModePaymentDetailsCreate(PaymentModePaymentDetailsCreateReq);
            return resp;
        }
        public IEnumerable<int> PaymentModePaymentDetailsFind(PaymentModePaymentDetailsFind PaymentModePaymentDetails)
        {
            var request = new PaymentModePaymentDetailsFindReq()
            {
                PaymentModePaymentDetailsFind = PaymentModePaymentDetails
            };

            var response = _client.PaymentModePaymentDetailsFind(request);
            return response.IDs;
        }
        public DAL.Models.PaymentModePaymentDetails PaymentModePaymentDetailsGet(int PaymentModePaymentDetailsID)
        {
            var PaymentDetailsPaymentModes = PaymentModePaymentDetailsGet(new int[] { PaymentModePaymentDetailsID });
            return PaymentDetailsPaymentModes == null ? null : PaymentDetailsPaymentModes.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.PaymentModePaymentDetails> PaymentModePaymentDetailsGet(IEnumerable<int> PaymentDetailsPaymentModes)
        {
            if (PaymentDetailsPaymentModes == null)
                return new List<PaymentModePaymentDetails>(); ;

            var request = new PaymentModePaymentDetailsGetReq()
            {
                IDs = PaymentDetailsPaymentModes.ToArray()
            };

            var response = _client.PaymentModePaymentDetailsGet(request);

            return response.PaymentModePaymentDetails;
        }
        public IEnumerable<DAL.Models.PaymentModePaymentDetails> FindPaymentModePaymentDetails(PaymentModePaymentDetailsFind PaymentModePaymentDetails)
        {
            return PaymentModePaymentDetailsGet(PaymentModePaymentDetailsFind(PaymentModePaymentDetails));
        }
        public void PaymentModePaymentDetailsUpdate(PaymentModePaymentDetails PaymentModePaymentDetails)
        {
            var PaymentModePaymentDetailsUpdateReq = new PaymentModePaymentDetailsUpdateReq
            {
                PaymentModePaymentDetails = PaymentModePaymentDetails
            };
            var resp = _client.PaymentModePaymentDetailsUpdate(PaymentModePaymentDetailsUpdateReq);
        }
        #endregion

        #region CustomerInfo
        public CustomerInfoCreateResp CustomerInfoCreate(CustomerInfo CustomerInfo)
        {
            var CustomerInfoCreateReq = new CustomerInfoCreateReq()
            {
                CustomerInfo = CustomerInfo
            };

            var resp = _client.CustomerInfoCreate(CustomerInfoCreateReq);
            return resp;
        }
        public void CustomerInfoUpdate(CustomerInfo CustomerInfo)
        {
            var CustomerInfoUpdateReq = new CustomerInfoUpdateReq
            {
                CustomerInfo = CustomerInfo
            };
            var resp = _client.CustomerInfoUpdate(CustomerInfoUpdateReq);
        }
        #endregion

        #region Card Type Payment Details
        public CardTypePaymentDetailsCreateResp CardTypePaymentDetailsCreate(CardTypePaymentDetails cardTypePaymentDetails)
        {
            var cardTypePaymentDetailsCreateRq = new CardTypePaymentDetailsCreateReq()
            {
                CardTypePaymentDetails = cardTypePaymentDetails
            };

            var resp = _client.CardTypePaymentDetailsCreate(cardTypePaymentDetailsCreateRq);
            return resp;
        }
        public IEnumerable<int> CardTypePaymentDetailsFind(CardTypePaymentDetailsFind CardTypePaymentDetails)
        {
            var request = new CardTypePaymentDetailsFindReq()
            {
                CardTypePaymentDetailsFind = CardTypePaymentDetails
            };

            var response = _client.CardTypePaymentDetailsFind(request);
            return response.IDs;
        }
        public DAL.Models.CardTypePaymentDetails CardTypePaymentDetailsGet(int CardTypePaymentDetailsID)
        {
            var PaymentDetailsCardTypes = CardTypePaymentDetailsGet(new int[] { CardTypePaymentDetailsID });
            return PaymentDetailsCardTypes == null ? null : PaymentDetailsCardTypes.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.CardTypePaymentDetails> CardTypePaymentDetailsGet(IEnumerable<int> PaymentDetailsCardTypes)
        {
            if (PaymentDetailsCardTypes == null)
                return new List<CardTypePaymentDetails>(); ;

            var request = new CardTypePaymentDetailsGetReq()
            {
                IDs = PaymentDetailsCardTypes.ToArray()
            };

            var response = _client.CardTypePaymentDetailsGet(request);

            return response.CardTypePaymentDetails;
        }

        public CardTypePaymentDetailsGetResp CardTypePaymentList()
        {
            var response = _client.CardTypePaymentList();
            return response;
        }
        public IEnumerable<DAL.Models.CardTypePaymentDetails> FindCardTypePaymentDetails(CardTypePaymentDetailsFind CardTypePaymentDetails)
        {

            return CardTypePaymentDetailsGet(CardTypePaymentDetailsFind(CardTypePaymentDetails));
        }
        public void CardTypeUpdate(CardTypePaymentDetails cardTypePaymentDetails)
        {
            var cardTypePaymentDetailsUpdateRq = new CardTypePaymentDetailsUpdateReq()
            {
                CardTypePaymentDetails = cardTypePaymentDetails
            };

            var resp = _client.CardTypePaymentDetailsUpdate(cardTypePaymentDetailsUpdateRq);
        }
        #endregion

        #region RegType
        public RegTypeCreateResp RegTypeCreate(RegType RegType)
        {
            var RegTypeCreateReq = new RegTypeCreateReq()
            {
                RegType = RegType
            };

            var resp = _client.RegTypeCreate(RegTypeCreateReq);
            return resp;
        }
        public IEnumerable<int> RegTypeFind(RegTypeFind RegType)
        {
            var request = new RegTypeFindReq()
            {
                RegTypeFind = RegType
            };

            var response = _client.RegTypeFind(request);
            return response.IDs;
        }
        public DAL.Models.RegType RegTypeGet(int RegTypeID)
        {
            var RegTypes = RegTypeGet(new int[] { RegTypeID });
            return RegTypes == null ? null : RegTypes.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.RegType> RegTypeGet(IEnumerable<int> RegTypes)
        {
            if (RegTypes == null)
                return new List<RegType>(); ;

            var request = new RegTypeGetReq()
            {
                IDs = RegTypes.ToArray()
            };

            var response = _client.RegTypeGet(request);

            return response.RegTypes;
        }

        public RegTypeGetResp RegTypeList()
        {
            var response = _client.RegTypeList();
            return response;
        }

        public static IEnumerable<DAL.Models.RegType> FindRegType(RegTypeFind RegType)
        {
            using (var proxy = new RegistrationServiceProxy())
            {
                return proxy.RegTypeGet(proxy.RegTypeFind(RegType));
            }
        }
        public void RegTypeUpdate(RegType RegType)
        {
            var RegTypeUpdateReq = new RegTypeUpdateReq
            {
                RegType = RegType
            };
            var resp = _client.RegTypeUpdate(RegTypeUpdateReq);
        }
        #endregion

        #region Race
        public RaceCreateResp RaceCreate(Race Race)
        {
            var RaceCreateReq = new RaceCreateReq()
            {
                Race = Race
            };

            var resp = _client.RaceCreate(RaceCreateReq);
            return resp;
        }
        public IEnumerable<int> RaceFind(RaceFind Race)
        {
            var request = new RaceFindReq()
            {
                RaceFind = Race
            };

            var response = _client.RaceFind(request);
            return response.IDs;
        }
        public DAL.Models.Race RaceGet(int RaceID)
        {
            var Races = RaceGet(new int[] { RaceID });
            return Races == null ? null : Races.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.Race> RaceGet(IEnumerable<int> Races)
        {
            if (Races == null)
                return new List<Race>(); ;

            var request = new RaceGetReq()
            {
                IDs = Races.ToArray()
            };

            var response = _client.RaceGet(request);

            return response.Races;
        }

        public RaceGetResp RaceList()
        {
            var response = _client.RaceList();
            return response;
        }

        public IEnumerable<DAL.Models.Race> FindRace(RaceFind Race)
        {
            return RaceGet(RaceFind(Race));
        }
        public void RaceUpdate(Race Race)
        {
            var RaceUpdateReq = new RaceUpdateReq
            {
                Race = Race
            };
            var resp = _client.RaceUpdate(RaceUpdateReq);
        }
        #endregion

        #region ThirdPartyAuthType
        public ThirdPartyAuthTypeCreateResp ThirdPartyAuthTypeCreate(ThirdPartyAuthType ThirdPartyAuthType)
        {
            var ThirdPartyAuthTypeCreateRq = new ThirdPartyAuthTypeCreateReq()
            {
                ThirdPartyAuthType = ThirdPartyAuthType
            };

            var resp = _client.ThirdPartyAuthTypeCreate(ThirdPartyAuthTypeCreateRq);
            return resp;
        }
        public IEnumerable<int> ThirdPartyAuthTypeFind(ThirdPartyAuthTypeFind ThirdPartyAuthType)
        {
            var request = new ThirdPartyAuthTypeFindReq()
            {
                ThirdPartyAuthTypeFind = ThirdPartyAuthType
            };

            var response = _client.ThirdPartyAuthTypeFind(request);
            return response.IDs;
        }
        public DAL.Models.ThirdPartyAuthType ThirdPartyAuthTypeGet(int ThirdPartyAuthTypeID)
        {
            var ThirdPartyAuthTypes = ThirdPartyAuthTypeGet(new int[] { ThirdPartyAuthTypeID });
            return ThirdPartyAuthTypes == null ? null : ThirdPartyAuthTypes.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.ThirdPartyAuthType> ThirdPartyAuthTypeGet(IEnumerable<int> ThirdPartyAuthTypes)
        {
            if (ThirdPartyAuthTypes == null)
                return new List<ThirdPartyAuthType>(); ;

            var request = new ThirdPartyAuthTypeGetReq()
            {
                IDs = ThirdPartyAuthTypes.ToArray()
            };

            var response = _client.ThirdPartyAuthTypeGet(request);

            return response.ThirdPartyAuthTypes;
        }

        public ThirdPartyAuthTypeGetResp ThirdPartyAuthTypeGetList()
        {
            var response = _client.ThirdPartyAuthTypeGetList();
            return response;
        }

        public IEnumerable<DAL.Models.ThirdPartyAuthType> FindLanguage(ThirdPartyAuthTypeFind ThirdPartyAuthType)
        {
            return ThirdPartyAuthTypeGet(ThirdPartyAuthTypeFind(ThirdPartyAuthType));
        }
        public void ThirdPartyAuthTypeUpdate(ThirdPartyAuthType ThirdPartyAuthType)
        {
            var ThirdPartyAuthTypeUpdateReq = new ThirdPartyAuthTypeUpdateReq
            {
                ThirdPartyAuthType = ThirdPartyAuthType
            };
            var resp = _client.ThirdPartyAuthTypeUpdate(ThirdPartyAuthTypeUpdateReq);
        }
        #endregion

        #region Language
        public LanguageCreateResp LanguageCreate(Language Language)
        {
            var LangaugeCreateRq = new LanguageCreateReq()
            {
                Language = Language
            };

            var resp = _client.LanguageCreate(LangaugeCreateRq);
            return resp;
        }
        public IEnumerable<int> LanguageFind(LanguageFind Language)
        {
            var request = new LanguageFindReq()
            {
                LanguageFind = Language
            };

            var response = _client.LanguageFind(request);
            return response.IDs;
        }
        public DAL.Models.Language LanguageGet(int LanguageID)
        {
            var Languages = LanguageGet(new int[] { LanguageID });
            return Languages == null ? null : Languages.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.Language> LanguageGet(IEnumerable<int> Languages)
        {
            if (Languages == null)
                return new List<Language>(); ;

            var request = new LanguageGetReq()
            {
                IDs = Languages.ToArray()
            };

            var response = _client.LanguageGet(request);

            return response.Languages;
        }

        public LanguageGetResp LanguageGetList()
        {
            var response = _client.LanguageGetList();
            return response;
        }

        public IEnumerable<DAL.Models.Language> FindLanguage(LanguageFind Language)
        {
            return LanguageGet(LanguageFind(Language));
        }
        public void LanguageUpdate(Language Language)
        {
            var LanguageUpdateReq = new LanguageUpdateReq
            {
                Language = Language
            };
            var resp = _client.LanguageUpdate(LanguageUpdateReq);
        }

        #endregion

        #region Nationality
        public NationalityCreateResp NationalityCreate(Nationality Nationality)
        {
            var NationalityCreateReq = new NationalityCreateReq()
            {
                Nationality = Nationality
            };

            var resp = _client.NationalityCreate(NationalityCreateReq);
            return resp;
        }
        public IEnumerable<int> NationalityFind(NationalityFind Nationality)
        {
            var request = new NationalityFindReq()
            {
                NationalityFind = Nationality
            };

            var response = _client.NationalityFind(request);
            return response.IDs;
        }
        public DAL.Models.Nationality NationalityGet(int NationalityID)
        {
            var Nationalitys = NationalityGet(new int[] { NationalityID });
            return Nationalitys == null ? null : Nationalitys.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.Nationality> NationalityGet(IEnumerable<int> Nationalitys)
        {
            if (Nationalitys == null)
                return new List<Nationality>(); ;

            var request = new NationalityGetReq()
            {
                IDs = Nationalitys.ToArray()
            };

            var response = _client.NationalityGet(request);

            return response.Nationalities;
        }


        public NationalityGetResp NationalityList()
        {
            var response = _client.NationalityList();
            return response;
        }

        public IEnumerable<DAL.Models.Nationality> FindNationality(NationalityFind Nationality)
        {
            //using (var proxy = new RegistrationServiceProxy())
            //{
            //    return proxy.NationalityGet(proxy.NationalityFind(Nationality));
            //}
            return NationalityGet(NationalityFind(Nationality));
        }
        public void NationalityUpdate(Nationality Nationality)
        {
            var NationalityUpdateReq = new NationalityUpdateReq
            {
                Nationality = Nationality
            };
            var resp = _client.NationalityUpdate(NationalityUpdateReq);
        }
        #endregion

        #region Country
        public CountryCreateResp CountryCreate(Country country)
        {
            var countryCreateRq = new CountryCreateReq()
            {
                Country = country
            };

            var resp = _client.CountryCreate(countryCreateRq);

            return resp;
        }
        public void CountryUpdate(Country country)
        {
            var countryUpdateReq = new CountryUpdateReq()
            {
                Country = country
            };

            var resp = _client.CountryUpdate(countryUpdateReq);
        }
        public IEnumerable<int> CountryFind(CountryFind country)
        {
            var request = new CountryFindReq()
            {
                CountryFind = country
            };

            var response = _client.CountryFind(request);

            return response.IDs;
        }
        public Country CountryGet(int countryID)
        {
            var countries = CountryGet(new int[] { countryID });
            return countries == null ? null : countries.SingleOrDefault();
        }
        public IEnumerable<Country> CountryGet(IEnumerable<int> countries)
        {
            if (countries == null)
                return new List<Country>();

            var request = new CountryGetReq()
            {
                IDs = countries.ToArray()
            };

            var response = _client.CountryGet(request);

            return response.Country;
        }

        public CountryGetResp CountryList()
        {
            var response = _client.CountryList();
            return response;
        }

        public IEnumerable<DAL.Models.Country> FindCountry(CountryFind Country)
        {
            return CountryGet(CountryFind(Country));
        }
        #endregion

        #region ExtIDType
        public ExtIDTypeCreateResp ExtIDTypeCreate(ExtIDType ExtIDType)
        {
            var ExtIDTypeCreateRq = new ExtIDTypeCreateReq()
            {
                ExtIDType = ExtIDType
            };

            var resp = _client.ExtIDTypeCreate(ExtIDTypeCreateRq);
            return resp;
        }
        public IEnumerable<int> ExtIDTypeFind(ExtIDTypeFind ExtIDType)
        {
            var request = new ExtIDTypeFindReq()
            {
                ExtIDTypeFind = ExtIDType
            };

            var response = _client.ExtIDTypeFind(request);
            return response.IDs;
        }
        public ExtIDType ExtIDTypeGet(int ExtIDTypeID)
        {
            var ExtIDTypes = ExtIDTypeGet(new int[] { ExtIDTypeID });
            return ExtIDTypes == null ? null : ExtIDTypes.SingleOrDefault();
        }
        public IEnumerable<ExtIDType> ExtIDTypeGet(IEnumerable<int> ExtIDTypes)
        {
            if (ExtIDTypes == null)
                return new List<ExtIDType>();

            var request = new ExtIDTypeGetReq()
            {
                IDs = ExtIDTypes.ToArray()
            };

            var response = _client.ExtIDTypeGet(request);

            return response.ExtIDType;
        }

        public ExtIDTypeGetResp ExtIDTypeList()
        {
            var response = _client.ExtIDTypeList();
            return response;
        }
        public static IEnumerable<DAL.Models.ExtIDType> FindExtIDType(ExtIDTypeFind ExtIDType)
        {
            using (var proxy = new RegistrationServiceProxy())
            {
                return proxy.ExtIDTypeGet(proxy.ExtIDTypeFind(ExtIDType));
            }
        }
        public void ExtIDTypeUpdate(ExtIDType ExtIDType)
        {
            var ExtIDTypeUpdateReq = new ExtIDTypeUpdateReq()
            {
                ExtIDType = ExtIDType
            };

            var resp = _client.ExtIDTypeUpdate(ExtIDTypeUpdateReq);
        }
        #endregion

        #region VIPCode
        public VIPCodeCreateResp VIPCodeCreate(VIPCode VIPCode)
        {
            var VIPCodeCreateRq = new VIPCodeCreateReq()
            {
                VIPCode = VIPCode
            };

            var resp = _client.VIPCodeCreate(VIPCodeCreateRq);
            return resp;
        }
        public IEnumerable<int> VIPCodeFind(VIPCodeFind VIPCode)
        {
            var request = new VIPCodeFindReq()
            {
                VIPCodeFind = VIPCode
            };

            var response = _client.VIPCodeFind(request);
            return response.IDs;
        }
        public VIPCode VIPCodeGet(int VIPCodeID)
        {
            var VIPCodes = VIPCodeGet(new int[] { VIPCodeID });
            return VIPCodes == null ? null : VIPCodes.SingleOrDefault();
        }


        public IEnumerable<VIPCode> VIPCodeGet(IEnumerable<int> VIPCodes)
        {
            if (VIPCodes == null)
                return new List<VIPCode>();

            var request = new VIPCodeGetReq()
            {
                IDs = VIPCodes.ToArray()
            };

            var response = _client.VIPCodeGet(request);

            return response.VIPCode;
        }

        public VIPCodeGetResp VIPCodeList()
        {
            var response = _client.VIPCodeList();

            return response;
        }
        public static IEnumerable<DAL.Models.VIPCode> FindVIPCode(VIPCodeFind VIPCode)
        {
            using (var proxy = new RegistrationServiceProxy())
            {
                return proxy.VIPCodeGet(proxy.VIPCodeFind(VIPCode));
            }
        }
        public void VIPCodeUpdate(VIPCode VIPCode)
        {
            var VIPCodeUpdateReq = new VIPCodeUpdateReq()
            {
                VIPCode = VIPCode
            };

            var resp = _client.VIPCodeUpdate(VIPCodeUpdateReq);
        }
        #endregion

        #endregion

        #region Registration

        public RegistrationCreateResp RegistrationCreate(DAL.Models.Registration reg, Customer customer, RegMdlGrpModel regMdlGrpModel,
                                                        List<Address> address, List<RegPgmBdlPkgComp> regPBPCs, RegStatus regStatus,
                                                        List<RegSuppLine> RegSuppLines, List<RegPgmBdlPkgComp> RegSuppLineVASes, List<RegSmartComponents> RegSmartcomps, List<WaiverComponents> RegWaiverComponents, bool isBREFail = false, int approverID = 0, DateTime? TimeApproval = null)
        {
            var request = new RegistrationCreateReq()
            {
                Registration = reg,
                Customer = customer,
                RegMdlGrpModel = regMdlGrpModel == null ? null : regMdlGrpModel,
                Addresses = address == null ? null : address.ToArray(),
                RegPgmBdlPkgComps = regPBPCs == null ? null : regPBPCs.ToArray(),
                RegStatus = regStatus == null ? null : regStatus,
                RegSuppLines = RegSuppLines == null ? null : RegSuppLines.ToArray(),
                RegSuppLineVASes = RegSuppLineVASes == null ? null : RegSuppLineVASes.ToArray(),
                RegSmartComponents = RegSmartcomps == null ? null : RegSmartcomps.ToArray(),
                RegWaiverComponents = RegWaiverComponents == null ? null : RegWaiverComponents.ToArray(),
                isBREFail = isBREFail,
                approverID = approverID,
                TimeApproval = TimeApproval
                //CustPhotos = SplitStringIntoBlocks(reg.CustomerPhoto,100000).ToArray()
            };

            var response = _client.RegistrationCreate(request);           

            return response;
        }

        public RegistrationCreateResp SecondaryLineRegistrationCreate(DAL.Models.RegistrationSec registrationSec, List<RegPgmBdlPkgCompSec> regPgmBdlPkgCompSecs)
        {
            var request = new RegistrationCreateWithSecReq()
            {

                RegistrationSec = registrationSec == null ? null : registrationSec,
                RegPgmBdlPkgCompSecs = regPgmBdlPkgCompSecs == null ? null : regPgmBdlPkgCompSecs.ToArray(),
            };

            var response = _client.SecondaryLineRegistrationCreate(request);

            return response;

        }



        public RegistrationCreateResp RegistrationCreateWithSec2(
                                                        DAL.Models.Registration reg, 
                                                        Customer customer, 
                                                        RegMdlGrpModel regMdlGrpModel,
                                                        List<Address> address, 
                                                        List<RegPgmBdlPkgComp> regPBPCs, 
                                                        RegStatus regStatus,
                                                        List<RegSuppLine> RegSuppLines, 
                                                        List<RegPgmBdlPkgComp> RegSuppLineVASes, 
                                                        DAL.Models.RegistrationSec registrationSec, 
                                                        List<RegPgmBdlPkgCompSec> regPgmBdlPkgCompSecs, 
                                                        RegMdlGrpModelSec regMdlGrpModelSec, 
                                                        List<RegSmartComponents> RegSmartcomps, 
                                                        List<WaiverComponents> RegWaiverComponents,
                                                        bool isBREFail=false,
                                                        List<string> abc = null)
        {
            //wenhao todo
            var request = new RegistrationCreateWithSecReq()
            {
                Registration = reg,
                Customer = customer,
                RegMdlGrpModel = regMdlGrpModel == null ? null : regMdlGrpModel,
                RegMdlGrpModelSec = regMdlGrpModelSec == null ? null : regMdlGrpModelSec,//Device Plan
                Addresses = address == null ? null : address.ToArray(),
                RegPgmBdlPkgComps = regPBPCs == null ? null : regPBPCs.ToArray(),
                RegStatus = regStatus == null ? null : regStatus,
                RegSuppLines = RegSuppLines == null ? null : RegSuppLines.ToArray(),
                RegSuppLineVASes = RegSuppLineVASes == null ? null : RegSuppLineVASes.ToArray(),
                RegistrationSec = registrationSec == null ? null : registrationSec,
                RegPgmBdlPkgCompSecs = regPgmBdlPkgCompSecs == null ? null : regPgmBdlPkgCompSecs.ToArray(),
                RegSmartComponents = RegSmartcomps == null ? null : RegSmartcomps.ToArray(),
                RegWaiverComponents = RegWaiverComponents == null ? null : RegWaiverComponents.ToArray(),
                isBREFail = isBREFail,
            };

            string compressedRequest = SNT.Utility.Compress.Zip(Util.ConvertObjectToXml(request));

            var response = _client.RegistrationCreateWithSec(compressedRequest);
            return response;
        }

        public RegistrationCreateResp RegistrationCreateWithSec(DAL.Models.Registration reg, Customer customer, RegMdlGrpModel regMdlGrpModel,
                                                        List<Address> address, List<RegPgmBdlPkgComp> regPBPCs, RegStatus regStatus,
                                                        List<RegSuppLine> RegSuppLines, List<RegPgmBdlPkgComp> RegSuppLineVASes, DAL.Models.RegistrationSec registrationSec
                                                        , List<RegPgmBdlPkgCompSec> regPgmBdlPkgCompSecs, RegMdlGrpModelSec regMdlGrpModelSec, List<RegSmartComponents> RegSmartcomps, List<WaiverComponents> RegWaiverComponents, bool isBREFail = false, int approverID = 0, DateTime? TimeApproval = null)
        {
            var request = new RegistrationCreateWithSecReq()
            {
                Registration = reg,
                Customer = customer,
                RegMdlGrpModel = regMdlGrpModel == null ? null : regMdlGrpModel,
                RegMdlGrpModelSec = regMdlGrpModelSec == null ? null : regMdlGrpModelSec,//Device Plan
                Addresses = address == null ? null : address.ToArray(),
                RegPgmBdlPkgComps = regPBPCs == null ? null : regPBPCs.ToArray(),
                RegStatus = regStatus == null ? null : regStatus,
                RegSuppLines = RegSuppLines == null ? null : RegSuppLines.ToArray(),
                RegSuppLineVASes = RegSuppLineVASes == null ? null : RegSuppLineVASes.ToArray(),
                RegistrationSec = registrationSec == null ? null : registrationSec,
                RegPgmBdlPkgCompSecs = regPgmBdlPkgCompSecs == null ? null : regPgmBdlPkgCompSecs.ToArray(),
                RegSmartComponents = RegSmartcomps == null ? null : RegSmartcomps.ToArray(),
                RegWaiverComponents = RegWaiverComponents == null ? null : RegWaiverComponents.ToArray(),
                isBREFail = isBREFail,
                approverID = approverID,
                TimeApproval = TimeApproval
            };

            string compressedRequest = SNT.Utility.Compress.Zip(Util.ConvertObjectToXml(request));

            var response = _client.RegistrationCreateWithSec(compressedRequest);
            return response;
        }


        //Home registration create
        public HomeRegistrationCreateResp HomeRegistrationCreate(DAL.Models.Registration reg, Customer customer, List<Address> address,
                                                            List<RegPgmBdlPkgComp> regPBPCs, RegStatus regStatus)
        {
            var request = new RegistrationCreateReq()
            {
                Registration = reg,
                Customer = customer,
                //RegMdlGrpModel = regMdlGrpModel,
                Addresses = address.ToArray(),
                RegPgmBdlPkgComps = regPBPCs.ToArray(),
                RegStatus = regStatus
            };

            var response = _client.HomeRegistrationCreate(request);

            return response;
        }
        public IEnumerable<int> RegistrationFind(RegistrationFind Registration)
        {
            var request = new RegistrationFindReq()
            {
                RegistrationFind = Registration
            };

            var response = _client.RegistrationFind(request);
            return response.IDs;
        }
        public DAL.Models.Registration RegistrationGet(int RegistrationID)
        {
            var Registrations = RegistrationGet(new int[] { RegistrationID });
            return Registrations == null ? null : Registrations.SingleOrDefault();
        }

        public DAL.Models.RegistrationSec RegistrationSecGet(int RegistrationID)
        {
            var Registrations = RegistrationSecGet(new int[] { RegistrationID });
            return Registrations == null ? null : Registrations.SingleOrDefault();

        }

        public IEnumerable<DAL.Models.Registration> RegistrationGet(IEnumerable<int> RegistrationIDs)
        {
            if (RegistrationIDs == null)
                return new List<DAL.Models.Registration>(); ;

            var request = new RegistrationGetReq()
            {
                IDs = RegistrationIDs.ToArray()
            };

            var response = _client.RegistrationGet(request);

            return response.Registrations;
        }

        public IEnumerable<DAL.Models.RegistrationSec> RegistrationSecGet(IEnumerable<int> RegistrationIDs)
        {
            if (RegistrationIDs == null)
                return new List<DAL.Models.RegistrationSec>(); ;

            var request = new RegistrationSecGetReq()
            {
                IDs = RegistrationIDs.ToArray()
            };

            var response = _client.RegistrationSecGet(request);

            return response.Registrations;
        }

        public static IEnumerable<DAL.Models.Registration> FindRegistration(RegistrationFind Registration)
        {
            using (var proxy = new RegistrationServiceProxy())
            {
                return proxy.RegistrationGet(proxy.RegistrationFind(Registration));
            }
        }
        public List<RegistrationSearchResult> RegistrationSearch(RegistrationSearchCriteria criteria)
        {
            var request = new RegistrationSearchReq()
            {
                Criteria = criteria
            };

            var response = _client.RegistrationSearch(request);

            return response.RegistrationSearchResults.ToList();
        }

        #region Added by Patanjali on 30-3-2013 to support Storekeeper and Cashier flow

        public List<RegistrationSearchResult> StoreKeeperSearch(RegistrationSearchCriteria criteria)
        {
            var request = new RegistrationSearchReq()
            {
                Criteria = criteria
            };

            var response = _client.StoreKeeperSearch(request);
            if (response != null && response.RegistrationSearchResults != null)
                return response.RegistrationSearchResults.ToList();

            return new List<RegistrationSearchResult>();
        }
        public AgentCodeByUserNameResult GetAgentCodeByUserName(string userName)
        {

            var response = _client.GetAgentCodeByUserName(userName);

            if (response != null && response.AgentCodeByUserNameResult != null)
                return response.AgentCodeByUserNameResult;

            return new AgentCodeByUserNameResult();
        }

        public List<RegistrationSearchResult> CashierSearch(RegistrationSearchCriteria criteria)
        {
            var request = new RegistrationSearchReq()
            {
                Criteria = criteria
            };

            var response = _client.CashierSearch(request);
            if (response != null && response.RegistrationSearchResults != null)
                return response.RegistrationSearchResults.ToList();

            return new List<RegistrationSearchResult>();
        }

        public List<RegistrationSearchResult> OrderStatusReportSearch(RegistrationSearchCriteria criteria)
        {
            var request = new RegistrationSearchReq()
            {
                Criteria = criteria
            };

            var response = _client.OrderStatusReportSearch(request);
            if (response != null && response.RegistrationSearchResults != null)
                return response.RegistrationSearchResults.ToList();

            return new List<RegistrationSearchResult>();
        }

        #endregion

        #region Added by Patanjali on 07-04-2013 to get Payment status
        public int PaymentStatusGet(int RegID)
        {
            int returnstatus = -1;

            returnstatus = _client.PaymentStatusGet(RegID);

            return returnstatus;
        }
        #endregion


        public void RegistrationClose(RegStatus regStatus)
        {
            var request = new RegistrationCloseReq()
            {
                RegStatus = regStatus
            };

            var response = _client.RegistrationClose(request);
        }
        public void RegistrationCancel(RegStatus regStatus)
        {
            var request = new RegistrationCancelReq()
            {
                RegStatus = regStatus
            };

            var response = _client.RegistrationCancel(request);
        }
        public void RegistrationPaymentSuccess(RegStatus regStatus)
        {
            var request = new RegistrationPaymentSuccessReq()
            {
                RegStatus = regStatus
            };

            var response = _client.RegistrationPaymentSuccess(request);
        }

        public void RegistrationPaymentFailed(RegStatus regStatus)
        {
            var request = new RegistrationPaymentFailedReq()
            {
                RegStatus = regStatus
            };

            var response = _client.RegistrationPaymentFailed(request);
        }
        public void RegistrationUpdate(DAL.Models.Registration reg)
        {
            var regUpdateRq = new RegistrationUpdateReq()
            {
                Registration = reg
            };

            var resp = _client.RegistrationUpdate(regUpdateRq);

        }

        /// <summary>
        /// MNP multiport in changes.
        /// </summary>
        /// <param name="oReqs">The o reqs.</param>
        public Int32 RegSuppLineUpdate(List<DAL.Models.RegSuppLine> oReqs)
        {
            var regUpdateRq = new RegSuppLineMultiUpdateReq()
            {
                RegSuppLines = oReqs.ToArray()
            };

            var resp = _client.RegSuppLineUpdate(regUpdateRq);
            return Convert.ToInt32(resp.Code);
        }

        public void RegistrationUpdateMISM(DAL.Models.RegistrationSec reg)
        {
            var regUpdateMISMRq = new RegistrationUpdateMISMReq()
            {
                RegistrationSec = reg
            };

            var resp = _client.RegistrationUpdateMISM(regUpdateMISMRq);

        }


        public KenanCustomerInfoCreateResp KenanCustomerInfoCreate(DAL.Models.KenamCustomerInfo reg)
        {
            var kencustInfoRq = new KenanCustomerInfoCreateReq()
            {
                KenanCustomerInfo = reg
            };

            var resp = _client.KenanCustomerInfoCreate(kencustInfoRq);
            return resp;
        }

        public List<FxDependChkComps> GetAllComponentsbyPlanId(int PlanID, string mandaotryComps,string linkType)
        {
            return _client.GetAllComponentsbyPlanId(PlanID, mandaotryComps,linkType).ToList();
        }

        public List<FxDependChkComps> GetAllComponentsbySecondaryPlanId(int PlanID, string mandaotryComps)
        {
            return _client.GetAllComponentsbySecondaryPlanId(PlanID, mandaotryComps).ToList();
        }

        public int BlackBerryDiscount(int deviceID, int planID)
        {
            return _client.BlackBerryDiscount(deviceID, planID);
        }

        //Method added by ravi to check whether the customer is whitelist or not
        public int CheckWhiteListCustomer(string icNo)
        {
            return _client.CheckWhiteListCustomer(icNo);
        }

        public List<Online.Registration.DAL.Models.lnkofferuomarticleprice> GetPromotionalOffers(int whiteListId)
        {
            return _client.GetPromotionalOffers(whiteListId).ToList();
        }

        public Online.Registration.DAL.Models.PlanDetailsandContract GetPlanDetailsbyOfferId(int offerId)
        {
            return _client.GetPlanDetailsbyOfferId(offerId);
        }

        //end of new method
        #endregion

        #region Customer

        public IEnumerable<int> CustomerFind(CustomerFind Customer)
        {
            var request = new CustomerFindReq()
            {
                CustomerFind = Customer
            };

            var response = _client.CustomerFind(request);
            return response.IDs;
        }
        public IEnumerable<Customer> CustomerGet(IEnumerable<int> CustomerIDs)
        {
            if (CustomerIDs == null)
                return new List<Customer>(); ;

            var request = new CustomerGetReq()
            {
                IDs = CustomerIDs.ToArray()
            };

            var response = _client.CustomerGet(request);

            return response.Customers;
        }
        public IEnumerable<Customer> CustomerGet1(CustomerFind Customer)
        {
            var request = new CustomerFindReq()
            {
                CustomerFind = Customer
            };

            var response = _client.CustomerGet1(request);
            return response.Customers;
        }

        #endregion

        #region Reg Model Group Model

        public IEnumerable<int> RegMdlGrpModelFind(RegMdlGrpModelFind regMdlGrpModel)
        {
            var request = new RegMdlGrpModelFindReq()
            {
                RegMdlGrpModelFind = regMdlGrpModel
            };

            var response = _client.RegMdlGrpModelFind(request);
            return response.IDs;
        }

        public IEnumerable<int> RegMdlGrpModelSecFind(RegMdlGrpModelSecFind regMdlGrpModel)
        {
            var request = new RegMdlGrpModelSecFindReq()
            {
                RegMdlGrpModelSecFind = regMdlGrpModel
            };

            var response = _client.RegMdlGrpModelSecFind(request);
            return response.IDs;
        }
        public IEnumerable<RegMdlGrpModel> RegMdlGrpModelGet1(RegMdlGrpModelFind regMdlGrpModel)
        {
            var request = new RegMdlGrpModelFindReq()
            {
                RegMdlGrpModelFind = regMdlGrpModel
            };

            var response = _client.RegMdlGrpModelGet1(request);
            return response.RegMdlGrpModels;
        }
        public IEnumerable<RegMdlGrpModel> RegMdlGrpModelGet(IEnumerable<int> regMdlGrpModelIDs)
        {
            if (regMdlGrpModelIDs == null)
                return new List<RegMdlGrpModel>(); ;

            var request = new RegMdlGrpModelGetReq()
            {
                IDs = regMdlGrpModelIDs.ToArray()
            };

            var response = _client.RegMdlGrpModelGet(request);

            return response.RegMdlGrpModels;
        }

        public IEnumerable<RegMdlGrpModelSec> RegMdlGrpModelSecGet(IEnumerable<int> regMdlGrpModelIDs)
        {
            if (regMdlGrpModelIDs == null)
                return new List<RegMdlGrpModelSec>(); ;

            var request = new RegMdlGrpModelSecGetReq()
            {
                IDs = regMdlGrpModelIDs.ToArray()
            };

            var response = _client.RegMdlGrpModelSecGet(request);

            return response.RegMdlGrpModels;
        }

        #endregion

        #region Reg Address

        public IEnumerable<int> RegAddressFind(AddressFind address)
        {
            var request = new AddressFindReq()
            {
                AddressFind = address
            };

            var response = _client.AddressFind(request);
            return response.IDs;
        }
        public IEnumerable<Address> RegAddressGet(IEnumerable<int> addressIDs)
        {
            if (addressIDs == null)
                return new List<Address>(); ;

            var request = new AddressGetReq()
            {
                IDs = addressIDs.ToArray()
            };

            var response = _client.AddressGet(request);

            return response.Addresses;
        }
        public IEnumerable<Address> RegAddressGet1(AddressFind address)
        {
            var request = new AddressFindReq()
            {
                AddressFind = address
            };

            var response = _client.AddressGet1(request);
            return response.Addresses;
        }
        #endregion

        #region Reg Program Bundle Package Component

        public IEnumerable<int> RegPgmBdlPkgCompFind(RegPgmBdlPkgCompFind regPgmBdlPkgComp)
        {
            var request = new RegPgmBdlPkgCompFindReq()
            {
                RegPgmBdlPkgCompFind = regPgmBdlPkgComp
            };

            var response = _client.RegPgmBdlPkgCompFind(request);
            return response.IDs;
        }
        public IEnumerable<int> RegPgmBdlPkgCompSecFind(RegPgmBdlPkgCompSecFind regPgmBdlPkgComp)
        {
            var request = new RegPgmBdlPkgCompSecFindReq()
            {
                RegPgmBdlPkgCompSecFind = regPgmBdlPkgComp
            };

            var response = _client.RegPgmBdlPkgCompSecFind(request);
            return response.IDs;
        }
        public IEnumerable<int> RegPgmBdlPkgComponentFind(RegPgmBdlPkgCompFind regPgmBdlPkgComp)
        {
            var request = new RegPgmBdlPkgCompFindReq()
            {
                RegPgmBdlPkgCompFind = regPgmBdlPkgComp
            };

            var response = _client.RegPgmBdlPkgComponentFind(request);
            return response.IDs;
        }
        public IEnumerable<RegPgmBdlPkgComp> RegPgmBdlPkgCompGet(IEnumerable<int> regPgmBdlPkgCompIDs)
        {
            if (regPgmBdlPkgCompIDs == null)
                return new List<RegPgmBdlPkgComp>(); ;

            var request = new RegPgmBdlPkgCompGetReq()
            {
                IDs = regPgmBdlPkgCompIDs.ToArray()
            };

            var response = _client.RegPgmBdlPkgCompGet(request);

            return response.RegPgmBdlPkgComps;
        }
        public IEnumerable<RegPgmBdlPkgComp> RegPgmBdlPkgCompGet1(RegPgmBdlPkgCompFind regPgmBdlPkgComp)
        {
            var request = new RegPgmBdlPkgCompFindReq()
            {
                RegPgmBdlPkgCompFind = regPgmBdlPkgComp
            };

            var response = _client.RegPgmBdlPkgCompGet1(request);
            return response.RegPgmBdlPkgComps;
        }
        public IEnumerable<RegPgmBdlPkgCompSec> RegPgmBdlPkgCompSecGet(IEnumerable<int> regPgmBdlPkgCompIDs)
        {
            if (regPgmBdlPkgCompIDs == null)
                return new List<RegPgmBdlPkgCompSec>(); ;

            var request = new RegPgmBdlPkgCompSecGetReq()
            {
                IDs = regPgmBdlPkgCompIDs.ToArray()
            };

            var response = _client.RegPgmBdlPkgCompSecGet(request);

            return response.RegPgmBdlPkgCompsSec;
        }
        public IEnumerable<PgmBdlPckComponent> RegPgmBdlPkgComponentGet(IEnumerable<int> regPgmBdlPkgCompIDs)
        {
            if (regPgmBdlPkgCompIDs == null)
                return new List<PgmBdlPckComponent>(); ;

            var request = new RegPgmBdlPkgCompGetReq()
            {
                IDs = regPgmBdlPkgCompIDs.ToArray()
            };

            var response = _client.RegPgmBdlPkgComponentGet(request);

            return response.RegPgmBdlPkgComps;
        }
        #endregion

        #region Reg Supplimentary Line

        public IEnumerable<int> RegSuppLineFind(RegSuppLineFind regSuppLine)
        {
            var request = new RegSuppLineFindReq()
            {
                RegSuppLineFind = regSuppLine
            };

            var response = _client.RegSuppLineFind(request);
            return response.IDs;
        }
        public IEnumerable<RegSuppLine> RegSuppLineGet(IEnumerable<int> regSuppLineIDs)
        {
            if (regSuppLineIDs == null)
                return new List<RegSuppLine>(); ;

            var request = new RegSuppLineGetReq()
            {
                IDs = regSuppLineIDs.ToArray()
            };

            var response = _client.RegSuppLineGet(request);

            return response.RegSuppLines;
        }
        public IEnumerable<RegSuppLine> RegSuppLineGet1(RegSuppLineFind regSuppLine)
        {
            var request = new RegSuppLineFindReq()
            {
                RegSuppLineFind = regSuppLine
            };

            var response = _client.RegSuppLineGet1(request);
            return response.RegSuppLines;
        }

        #endregion

        #region Reg Status

        public int RegStatusCreate(RegStatus regStatus, DAL.Models.Registration reg)
        {
            var request = new RegStatusCreateReq() { RegStatuses = regStatus, Registrations = reg };
            var response = _client.RegStatusCreate(request);
            LogResponse(response);

            return response.ID;
        }
        public int RegStatusUpdate(int Regid, RegStatus regStatus)
        {
            var response = _client.RegStatusUpdate(Regid,regStatus);
            return response;
        }
        public IEnumerable<int> RegStatusFind(RegStatusFind regStatus)
        {
            var request = new RegStatusFindReq()
            {
                RegStatusFind = regStatus
            };

            var response = _client.RegStatusFind(request);
            return response.IDs;
        }
        public IEnumerable<RegStatus> RegStatusGet(IEnumerable<int> regStatusIDs)
        {
            if (regStatusIDs == null)
                return new List<RegStatus>(); ;

            var request = new RegStatusGetReq()
            {
                IDs = regStatusIDs.ToArray()
            };

            var response = _client.RegStatusGet(request);

            return response.RegStatuses;
        }
        public IEnumerable<RegStatus> RegStatusGet1(RegStatusFind regStatus)
        {
            var request = new RegStatusFindReq()
            {
                RegStatusFind = regStatus
            };

            var response = _client.RegStatusGet1(request);
            return response.RegStatuses;
        }
        public IEnumerable<RegStatusResult> RegStatusLogGet(IEnumerable<int> regStatusIDs)
        {
            if (regStatusIDs == null)
                return new List<RegStatusResult>(); ;

            var request = new RegStatusLogGetReq()
            {
                IDs = regStatusIDs.ToArray()
            };

            var response = _client.RegStatusLogGet(request);

            return response.RegStatusResult;
        }
        public RegistrationDetails GetRegistrationFullDetails(int regId)
        {

            return _client.GetRegistrationFullDetails(regId);
        }

        #endregion

        #region Biometirc

        public int BiometricCreate(Biometrics Biometric)
        {
            var request = new BiometricCreateReq() { Biometric = Biometric };
            var response = _client.BiometricCreate(request);
            LogResponse(response);

            return response.ID;
        }
        public void BiometricUpdate(Biometrics Biometric)
        {
            var request = new BiometricUpdateReq() { Biometric = Biometric };
            var response = _client.BiometricUpdate(request);
            LogResponse(response);
        }
        public IEnumerable<int> BiometricFind(BiometricFind Biometric)
        {
            var request = new BiometricFindReq()
            {
                BiometricFind = Biometric
            };

            var response = _client.BiometricFind(request);
            return response.IDs;
        }
        public IEnumerable<Biometrics> BiometricGet(IEnumerable<int> BiometricIDs)
        {
            if (BiometricIDs == null)
                return new List<Biometrics>(); ;

            var request = new BiometricGetReq()
            {
                IDs = BiometricIDs.ToArray()
            };

            var response = _client.BiometricGet(request);

            return response.Biometrics;
        }

        #endregion

        #region Reg Account

        public IEnumerable<RegAccount> RegAccountGet(IEnumerable<int> regIDs)
        {
            if (regIDs == null)
                return new List<RegAccount>(); ;

            var request = new RegAccountGetReq()
            {
                RegIDs = regIDs.ToArray()
            };

            var response = _client.RegAccountGet(request);

            return response.RegAccounts;
        }

        #endregion


        /*Chetan added for displaying the reson for failed transcation*/
        public DAL.Models.KenanaLogDetails KenanLogDetailsGet(int RegistrationID)
        {
            var KenanLogDetails = KenanLogDetailsGet(new int[] { RegistrationID });
            // return KenanLogDetails == null ? null : KenanLogDetails.SingleOrDefault();

            return KenanLogDetails == null ? null : KenanLogDetails.FirstOrDefault();
        }

        public List<DAL.Models.KenanaLogDetails> KenanLogDetailsGet1(int RegistrationID)
        {
            var KenanLogDetails = KenanLogDetailsGet(new int[] { RegistrationID });
            // return KenanLogDetails == null ? null : KenanLogDetails.SingleOrDefault();

            foreach (Online.Registration.DAL.Models.KenanaLogDetails objlogDetails in KenanLogDetails)
            {
                if (objlogDetails.MethodName.ToUpper() == "UPDATEORDERSTATUS" && objlogDetails.MessageCode != "0")
                {
                    //var obj = objlogDetails.KenanXmlReq;

                    XmlDocument _doc = new XmlDocument();
                    _doc.LoadXml(objlogDetails.KenanXmlReq);

                    XmlNodeList eventType = _doc.GetElementsByTagName("EventType");

                    for (int i = 0; i < eventType.Count; ++i)
                    {
                        string methodname = eventType[i].InnerText;
                        objlogDetails.MethodName = methodname;
                    }

                }
            }

            return KenanLogDetails == null ? null : KenanLogDetails.ToList();
        }

        public IEnumerable<DAL.Models.KenanaLogDetails> KenanLogDetailsGet(IEnumerable<int> RegistrationIDs)
        {
            if (RegistrationIDs == null)
                return new List<DAL.Models.KenanaLogDetails>(); ;

            var request = new KenanLogDetailsGetReq()
            {
                IDs = RegistrationIDs.ToArray()
            };

            var response = _client.KenanLogDetailsGet(request);

            return response.KenanaLogDetails;
        }
        public IEnumerable<DAL.Models.KenanaLogDetails> KenanLogHistoryDetailsGet(IEnumerable<int> RegistrationIDs)
        {
            if (RegistrationIDs == null)
                return new List<DAL.Models.KenanaLogDetails>(); ;

            var request = new KenanLogHistoryDetailsGetReq()
            {
                IDs = RegistrationIDs.ToArray()
            };

            var response = _client.KenanLogHistoryDetailsGet(request);

            return response.KenanaLogDetails;
        }

        public int SaveRegSuppLines(RegSupplinesMapReq regSupplinesMapReq)
        {
            return _client.SaveRegSuppLines(regSupplinesMapReq);
        }

        //Spend Limit commented by Ravi as per new flow on june 15 2013
        #region "Spend Limit"
        public int SaveSpendLimit(LnkRegSpendLimitReq oReq)
        {
            var response = _client.SaveSpendLimit(oReq);

            return response;
        }

        public DAL.Models.lnkRegSpendLimit SpendLimitGet(int RegistrationID)
        {
            var response = _client.SpendLimitGet(RegistrationID);

            return response.lnkRegSpendLimitDetails;
        }
        #endregion



        #region ExtraTen

        public bool SaveExtraTen(ExtraTenReq oReq)
        {
            int id = 0;
            id = _client.SaveExtraTen(oReq);

            if (id > 0)
                return true;
            else
                return false;

        }

        public bool UpdateExtraTen(ExtraTenReq oReq)
        {
            int id = 0;
            id = _client.UpdateExtraTen(oReq);

            if (id > 0)
                return true;
            else
                return false;
        }

        public bool DeleteExtraTen(int extraTenId)
        {
            int id = 0;
            id = _client.DeleteExtraTen(extraTenId);

            if (id > 0)
                return true;
            else
                return false;
        }


        public bool DeleteExtraTenDetails(int regId)
        {
            int id = 0;
            id = _client.DeleteExtraTenDetails(regId);

            if (id > 0)
                return true;
            else
                return false;
        }



        public ExtraTenGetResp GetExtraTenDetails(int regId)
        {
            ExtraTenGetResp response = new ExtraTenGetResp();
            response = _client.GetExtraTenDetails(regId);

            return response;
        }

        public bool SaveExtraTenLogs(ExtraTenLogReq oReq)
        {
            int id = 0;
            id = _client.SaveExtraTenLogs(oReq);

            if (id > 0)
                return true;
            else
                return false;
        }

        public int SaveExtraTenAddEditDetails(ExtraTenAddEditReq oreq)
        {
            int id = 0;
            id = _client.SaveExtraTenAddEditDetails(oreq);
            return id;
        }
        #endregion


        #region Cancel Reason
        public int RegistrationCancelCreate(RegistrationCancelReason oReq)
        {
            RegistrationCancelReasonResp resp = null;
            RegistrationCancelReasonReq CancelRegistrationReq = new RegistrationCancelReasonReq();
            CancelRegistrationReq.RegistrationCancelReason = oReq;
            resp = _client.RegistrationCancelCreate(CancelRegistrationReq);
            return resp.ID;
        }
        #endregion

        public string CancelReasonGet(int regID)
        {
            string resp = string.Empty;
            resp = _client.CancelReasonGet(regID);
            return resp;
        }

        /// <summary>
        /// Used to Save the Registration Details
        /// </summary>
        /// <param name="oReq"></param>
        /// <returns></returns>
        public int SaveLnkRegistrationDetails(LnkRegDetailsReq oReq)
        {
            var response = _client.SaveLnkRegistrationDetails(oReq);

            return response;
        }

        /// <summary>
        /// Used to Get the Registration Details
        /// </summary>
        /// <param name="oReq"></param>
        /// <returns></returns>
        public DAL.Models.LnkRegDetails LnkRegistrationGetByRegId(int regId)
        {
            var response = _client.LnkRegistrationGetByRegId(regId);

            return response;
        }

        /// <summary>
        /// Used to Update the Registration Details
        /// </summary>
        /// <param name="oReq"></param>
        /// <returns></returns>
        public int UpdateLnkRegistrationDetails(LnkRegDetailsReq oReq)
        {
            var response = _client.UpdateLnkRegistrationDetails(oReq);

            return response;
        }

        /// <summary>
        /// Used to Get the Registration Details
        /// </summary>
        /// <param name="oReq"></param>
        /// <returns></returns>
        public LnkRegDetailsReq GetLnkRegistrationDetails(int regId)
        {
            var response = _client.GetLnkRegistrationDetails(regId);

            return response;
        }

        public Dictionary<string, string> GetlnkpgmbdlpkgcompidForKenancomponentAll(int planId, List<string> kenancodes)
        {
            return _client.GetlnkpgmbdlpkgcompidForKenancomponentAll(planId, kenancodes.ToArray());
        }

        public Dictionary<string, string> GetlnkpgmbdlpkgcompidsForKenancomponents(string plan_type, int planId, string[] kenancodes)
        {
            var response = new Dictionary<string, string>();
            try
            {
                response = _client.PgmBdlCompIdForKenanComponentsAll(plan_type, planId, kenancodes);
            }
            catch (Exception ex)
            {
                response = null;
            }
            return response;
        }


        public SmartGetExistingPackageDetailsResp ExistingPackageDetails(int packageId)
        {
            var request = new SmartGetExistingPackageDetailsReq()
            {
                IDs = packageId
            };
            var response = _client.ExistingPackageDetails(request);

            return response;
        }
        public Dictionary<int, SmartGetExistingPackageDetailsResp> ExistingPackagesDetails(List<int> packageIds)
        {
            var oReq = new List<SmartGetExistingPackageDetailsReq>();
            foreach (int packageId in packageIds)
            {
                oReq.Add(new SmartGetExistingPackageDetailsReq() { IDs = packageId });
            }
            var response = _client.ExistingPackagesDetails(oReq.ToArray());

            return response;
        }

        public string GetlnkpgmbdlpkgcompidForKenancomponent(string plan_type, int planId, string kenancode)
        {
            string response = "";
            try
            {
                response = _client.GetlnkpgmbdlpkgcompidForKenancomponent(plan_type, planId, kenancode);
            }
            catch (Exception ex)
            {
                response = "";
            }

            return response;
        }
        public long GetLnkPgmBdlPkgCompIdByKenanCode(string kenanCode, string planType)
        {
            long response = 0;
            try
            {
                response = _client.GetLnkPgmBdlPkgCompIdByKenanCode(kenanCode, planType);
            }
            catch (Exception ex)
            {
                response = 0;
            }

            return response;
        }
        /// <summary>
        /// Used to get Suppline details whether as new or existing
        /// </summary>
        /// <param name="RegID"></param>
        /// <returns>bool</returns>
        public bool getSupplineDetails(int regID)
        {
            var response = _client.getSupplineDetails(regID);


            return response;
        }


        // Added BY Nagaraju       

        public RegistrationCreateResp RegistrationCreateNew(DAL.Models.Registration reg, Customer customer, List<Address> address, RegStatus regStatus, List<WaiverComponents> RegWaiverComponents, bool isBREFail=false)
        {
            var request = new RegistrationCreateReq()
            {
                Registration = reg,
                Customer = customer,
                Addresses = address.ToArray(),
                RegStatus = regStatus,
                RegWaiverComponents = RegWaiverComponents.ToArray(),
                isBREFail = isBREFail,

            };

            var response = _client.RegistrationCreateNew(request);

            return response;
        }

        public GetPackageDetailsResp SelectedPackageDetail(int PackageID)
        {

            var request = new GetPackageDetailsReq()
            {
                IDs = PackageID
            };
            var response = _client.SelectedPackageDetail(request);
            return response;
        }

        public List<Online.Registration.DAL.Models.Contracts> GetContractsList()
        {
            var resp = _client.GetActiveContractsList();
            return resp.Contracts.ToList();
        }


        public List<Online.Registration.DAL.Models.UserSpecificPrintersInfo> GetUserSpecificPrinters(int userID, int OrgID)
        {

            var resp = _client.GetUserSpecificPrintersInfo(userID, OrgID);
            if (resp == null)
                return new List<Online.Registration.DAL.Models.UserSpecificPrintersInfo>();

            return resp.ToList();
        }

        public IEnumerable<long> selectusedmobilenos()
        {
            var response = _client.selectusedmobilenos();
            return response;
        }

        public int Insertusedmobilenos(int regid, long mobileno, int active)
        {
            var response = _client.Insertusedmobilenos(regid, mobileno, active);
            return response;
        }
        #region Added by Sindhu on 11th Aug 2013
        public bool IsIMPOSFileExists(int regID, int regType, out string status)
        {
            var response = _client.IsIMPOSFileExists(out status, regID, regType);
            return response;
        }
        #endregion

        #region CRP



        public RegistrationCreateResp RegistrationCreate_CRP(Online.Registration.DAL.Models.Registration reg, Customer customer, RegMdlGrpModel regMdlGrpModel,
                                                     List<Address> address, List<RegPgmBdlPkgComp> regPBPCs, RegStatus regStatus,
                                                     List<RegSuppLine> RegSuppLines, List<RegPgmBdlPkgComp> RegSuppLineVASes, List<PackageComponents> PackageComponents, CMSSComplain cms, List<RegSmartComponents> RegSmartComponents, List<WaiverComponents> RegWaiverComponents,bool isBREFail=false)
        {
            var request = new RegistrationCreateReq_CRP()
            {
                Registration = reg,
                Customer = customer,
                RegMdlGrpModel = regMdlGrpModel,
                Addresses = address.ToArray(),
                RegPgmBdlPkgComps = regPBPCs.ToArray(),
                RegStatus = regStatus,
                RegSuppLines = RegSuppLines.ToArray(),
                RegSuppLineVASes = RegSuppLineVASes.ToArray(),
                Packages = PackageComponents.ToArray(),
                CMSSID = cms,
                RegSmartComponents = RegSmartComponents.ToArray(),
                RegWaiverComponents = RegWaiverComponents.ToArray(),
                isBREFail = isBREFail
                //CustPhotos = SplitStringIntoBlocks(reg.CustomerPhoto,100000).ToArray()
            };

            var response = _client.RegistrationCreate_CRP(request);
            return response;
        }

        public bool isCRPaccount(int regid)
        {
            bool isCRPreg = _client.isCRPregistration(regid);

            return isCRPreg;
        }
        #endregion

        /// <summary>
        /// Registrations the get MNP supplementary lines.
        /// </summary>
        /// <Author>Sutan Dan</Author>
        /// <param name="RegID">The reg ID.</param>
        /// <returns></returns>
        public List<Online.Registration.DAL.Models.RegSuppLine> RegistrationGetMnpSupple(Int32 RegID)
        {
            try
            {
                var resp = _client.RegistrationGetMnpSupple(RegID: RegID);
                return resp.Registrations.ToList();
            }
            catch
            {
                throw;
            }
        }

        public int GetBBAdvancePrice(string uomCode, int planID, int modelId)
        {
            var response = _client.GetBBAdvancePrice(uomCode, planID, modelId);
            return response;
        }



        public List<string> GetCompDescByRegid(int regID)
        {
            var response = _client.GetCompDescByRegid(regID);
            return response.ToList();
        }



        public List<string> GetDataPlanComponentsForMISM()
        {
            var response = _client.GetDataPlanComponentsForMISM();
            return response.ToList();
        }

        #region WhitleList

        public List<SMECIWhiteList> WhiteListSearchByIC(SMECIWhiteList whiteList)
        {

            return _client.WhiteListSearchByIC(whiteList).ToList();
        }

        public bool DeleteWhiteListCustomer(Online.Registration.DAL.Models.SMECIWhiteList whiteList)
        {
            return _client.DeleteWhiteListCustomer(whiteList);
        }

        public bool AddWhiteListCustomer(List<SMECIWhiteList> whiteList)
        {
            return _client.AddWhiteListCustomer(whiteList.ToArray());
        }
        public List<int> GetDependentComp(string planId)
        {
            return _client.GetDependentComp(planId).ToList();
        }

        #endregion

        /// <summary>
        ///  Added by sindhu on 11/09/2013 for print version changes
        /// </summary>
        /// <returns></returns>
        public string GetPrintVersionNum(int regType, string brand, string PlanKenanCode, string Trn_Type = null)
        {
            return _client.GetPrintVersionNum(regType, brand, PlanKenanCode, Trn_Type);
        }

        /// <summary>
        /// Added by sindhu on 11/09/2013 for print version changes
        /// </summary>
        /// <param name="versionNo"></param>
        /// <param name="regType"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public Online.Registration.DAL.Models.PrintVersion GetPrintTsCsInfo(string versionNo, int regType, string brand, string PlanKenanCode, string Trn_Type)
        {
            var resp = _client.GetPrintTsCsInfo(versionNo, regType, brand, PlanKenanCode, Trn_Type);

            return resp;
        }

        public string GetPrintVersionNoByRegid(int regId)
        {
            var resp = _client.GetPrintVersionNoByRegid(regId);
            return resp;
        }

        public int GetMarketCodeIdByRegid(int regid)
        {
            return _client.GetMarketCodeIdByRegid(regid);
        }

        #region Get Waiver Components
        public List<DAL.Models.WaiverComponents> GetWaiverComponents(int packageID)
        {
            var WaiverComponents = _client.GetWaiverComponents(packageID);
            return WaiverComponents.ToList();
        }
        public List<DAL.Models.WaiverComponents> GetWaiverComponentsbyRegID(int RegID)
        {
            var WaiverComponents = _client.GetWaiverComponentsbyRegID(RegID);
            return WaiverComponents.ToList();
        }
        #endregion

        #region supervisor waive off

        public List<string> GetCreatedUsersForWaiveOff(int OrgId)
        {
            var response = _client.GetCreatedUsersForWaiveOff(OrgId);
            return response.ToList();
        }

        public List<string> GetAcknowledgedUsersForWaiveOff(int OrgId, bool isCDPU)
        {
            var response = _client.GetAcknowledgedUsersForWaiveOff(OrgId, isCDPU);
            return response.ToList();
        }

        public List<SearchResultForSuperVisorWaieverOff> SupervisorWaiverOffSearch(RegistrationSearchCriteriaForWaiverOff criteria)
        {
            List<SearchResultForSuperVisorWaieverOff> response = new List<SearchResultForSuperVisorWaieverOff>();
            try
            {
                var request = new SupervisorWaiveOffSearchReq()
                {
                    Criteria = criteria
                };

                 var serviceResponse = _client.SupervisorWaiveOffSearch(request);

                 if (serviceResponse != null)
                     response = serviceResponse.RegistrationSearchResults.ToList();
            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;
        }

        public int AcknowledgedUsersForWaiveOffUpdate(List<RegDetails> details)
        {
            var request = new AcknowledgedUsersReq()
            {
                AcknowledgedUsers = details.ToArray()
            };

            int response = _client.AcknowledgedUsersForWaiveOffUpdate(request);

            return response;
        }

        #endregion

        public List<RegistrationSearchResult> TLProfileSearch(RegistrationSearchCriteria criteria)
        {
            var request = new RegistrationSearchReq()
            {
                Criteria = criteria
            };

            var response = _client.TLProfileSearch(request);

            return response.RegistrationSearchResults.ToList();
        }


        public int SaveUserPrintLog(UserPrintLog objUserPrintLog)
        {
            var resp = _client.SaveUserPrintLog(objUserPrintLog);

            return resp;
        }

        public List<DAL.Models.UserPrintLog> GetPrintersLog(string File)
        {
            var resp = _client.GetPrintersLog(File).ToList();

            return resp;

        }
        public string GetBrandModelIDbyArticleID(string ArticleID)
        {
            var resp = _client.GetBrandModelIDbyArticleID(ArticleID);
            return resp;
        }
        public List<int> GetSpotifyCopmByPkgKenancode(string kenanCode, int Regid)
        {
            List<int> response = new List<int>();
            try
            {
                response = _client.GetSpotifyCopmByPkgKenancode(kenanCode, Regid).ToList();
            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;
        }
        public List<int> GetSpotifyCopmByPkgKenancode(string kenanCode)
        {
            List<int> response = new List<int>();
            try
            {
                response = _client.GetSpotifyCopmIDByCompKenancode(kenanCode).ToList();
            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;
        }
        public long GetLnkPgmBdlPkgCompIdByKenanCode(string kenanCode, string planType, string linkType)
        {
            long response = 0;
            try
            {
                response = _client.GetLnkPgmBdlPkgCompIdByKenanCodeandTypes(kenanCode, planType, linkType);
            }
            catch (Exception ex)
            {
                response = 0;
            }

            return response;
        }
        public int SaveDocumentToDB(string photoData, string custPhoto, string altCustPhoto, int regid)
        {
            int response = 0;
            try
            {
                response = _client.SaveDocumentToDB(photoData, custPhoto,altCustPhoto, regid);
            }
            catch (Exception ex)
            {
                response = 0;
            }
            return response;
        }

        /*Changes by chetan related to PDPA implementation*/
        public Online.Registration.DAL.Models.PDPAData GetPDPADataInfo(string DocType, string TrnType)
        {
            var resp = _client.GetPDPADataInfo(DocType, TrnType);

            return resp;
        }

        public Online.Registration.DAL.Models.PDPAData GetPDPADataInfoWithVersion(string DocType, string TrnType, string pdpdversion)
        {
            var resp = _client.GetPDPADataInfoWithVersion(DocType, TrnType, pdpdversion);

            return resp;
        }


        public int RegAccountsUpdate(RegAccount oReq)
        {
            var response = _client.RegAccountsUpdate(oReq);
            return response;
        }

        public RegAccount RegAccountGetByRegID(int regID)
        {
            var resp = _client.RegAccountGetByRegID(regID);
            return resp;
        }

        public int UpdateSecondaryRequestSent(int RegID)
        {
            int resp = _client.UpdateSecondaryRequestSent(RegID);
            return resp;
        }

        /*Code for saving CMSS case details by chetan*/
        public int SaveCMSSDetails(CMSSComplain oReq)
        {
            var response = _client.SaveCMSSDetails(oReq);

            return response;
        }

        public DateTime GetCMSSCaseDateForAcct(string AccountNumber)
        {
            var resp = _client.GetCMSSCaseDateForAcct(AccountNumber);
            return resp;
        }

        public CMSSComplain GetCMSSCaseDetails(int regId)
        {
            var resp = _client.GetCMSSCaseDetails(regId);
            return resp;
        }

        public int GetSIMModelIDByArticleID(string strAticleID)
        {

            var response = _client.GetSIMModelIDByArticleID(strAticleID);

            return response;
        }
        public WaiverRulesResp GetWaiverRules()
        {
            return _client.GetWaiverRules();
        }


        public int SaveUserDMEPrint(UserDMEPrint objUserDMEPrint)
        {
            var resp = _client.SaveUserDMEPrint(objUserDMEPrint);
            return resp;
        }

        public string GetCDPUStatus(int regId)
        {
            var resp = _client.GetCDPUStatus(regId);
            return resp;
        }

        // Drop 5
        public RegBREStatus GetCDPUDetails(RegBREStatus req)
        {
            var resp = _client.GetCDPUDetails(req);
            return resp;
        }

        public bool UpsertBRE (RegBREStatus objBre)
        {
            var resp = _client.UpsertBRE(objBre);
            return resp;
        }


        public int UpdateBREApproveStatus(int RegId, string Status, string LastAccessID)
        {
            var resp = _client.UpdateBREApproveStatus(RegId, Status, LastAccessID);
            return resp;
        }

        public int UpdateBRECancelReason(int RegId, string CReason, string LastAccessID)
        {
            var resp = _client.UpdateBRECancelReason(RegId, CReason, LastAccessID);
            return resp;
        }

        public List<DealerPlanIds> GetDealerSpecificPlans(int userId)
        {
            var resp = _client.GetDealerSpecificPlans(userId);
            return resp.ToList();
        }
        public bool IsCallConferenceWaived(int RegID)
        {
            var resp = _client.IsCallConferenceWaived(RegID);
            return resp;
        }

        public bool IsInternationalRoamingWaived(int RegID)
        {
            var resp = _client.IsInternationalRoamingWaived(RegID);
            return resp;
        }
        public string GetArticelIdByModelId(int modelID)
        {
            string  resp = _client.ArticelIdByModelId(modelID);
            return resp;
        }

        
        public int RegSecondaryAccountsForSimRplc(List<lnkSecondaryAcctDetailsSimRplc> SecAccountLines)
        {
            var response = _client.RegSecondaryAccountsForSimRplc(SecAccountLines: SecAccountLines.ToArray());
            return response;
        }

        //public string GetAgentCodecByUserName_OrgID(string userName, int orgID)
        //{
        //    var response = _client.GetAgentCodecByUserName_OrgID(userName, orgID);
        //    return response;
        //}

        public List<lnkSecondaryAcctDetailsSimRplc> GetSecondaryAccountLinesForSimRplc(int Regid)
        {
            var response = _client.GetSecondaryAccountLinesForSimRplc(Regid);
            return response.ToList();
        }
        public List<string> getRebateContractIDbyComponentID(string ComponentID)
        {
            
            var resp = _client.getRebateContractIDbyComponentID(ComponentID);
            return resp.ToList();
        }
        public List<string> getRebateContractIDs()
        {
            var resp = _client.getRebateContractIDs();
            return resp.ToList();
        }
        public List<DAL.Models.RebateDataContractComponents> getRebateDataContractComponents()
        {
            var response = _client.getRebateDataContractComponents().ToList();
            return response;
        }

        public List<DAL.Models.BreCheckTreatment> getBRECheckTreatment()
        {
            var response = _client.getBreCheckTreatment().ToList();
            return response;
        }

        public List<DAL.Models.PostCodeCityState> getPostCodeCityState()
        {
            var response = _client.getPostCodeCityState().ToList();
            return response;
        }

        public List<DAL.Models.AutoWaiverRules> getAutoWaiverRules()
        {
            var response = _client.getAutoWaiverRules().ToList();
            return response;
        }

        public List<DAL.Models.RegRebateDatacontractPenalty> getRebateDatacontractPenalty(int RegID)
        {
            var response = _client.getRebateDatacontractPenalty(RegID).ToList();
            return response;
        }

        //25052015 Method to get Plan Name query by RegId - Lus
        public MnpRegDetailsVM getPlanNamebyRegId(int RegId)
        {
            var response = _client.getPlanNamebyRegId(RegId);
            return response;
        }

		public List<DAL.Models.PackageComponents> getDisconnectComponents(int regID)
		{
			var response = _client.getDisconnectComponents(regID).ToList();

			return response;
		}

    public int SaveLnkRegRebateDatacontractPenalty(LnkLnkRegRebateDatacontractPenaltyReq objRebatePenaltyReq)
    {
        var response = _client.SaveLnkRegRebateDatacontractPenalty(objRebatePenaltyReq);
        return response;
    }

        public int SaveLnkPackageComponet(int Regid, List<PackageComponents> PackageComponents, int GroupID)
        {
            int response = 0;
            response = _client.SaveLnkPackageComponet(Regid: Regid, PackageComponents: PackageComponents.ToArray(), GroupID: GroupID);
            return response;
        }
	 public void UpdateSimCardType(int RegID, string SimCardType)
    {
        var response = _client.UpdateSimCardType(RegID, SimCardType);
    }

    public void UpdateMismSimCardType(int RegID, string MismSimCardType)
    {
        var response = _client.UpdateMismSimCardType(RegID, MismSimCardType);
    }

    public void UpdateSecondarySimSerial(int RegID, string sim)
    {
       _client.UpdateSecondarySimSerial(RegID, sim);
    }

    public Online.Registration.DAL.Models.lnkofferuomarticleprice GetOffersById(int OfferId)
    {
        return _client.GetOfferDetailsByID(OfferId);
    }

    public void LogToDB(WebPosLog posLog)
    {
        _client.InsertLog(posLog);
    }


	public int SaveListRegAttributes(List<RegAttributes> objListRegAtributes)
	{
		return _client.SaveListRegAttributes(objListRegAtributes.ToArray());
	}

	public int SaveRegAttributes(RegAttributes objListRegAtributes)
	{
		return _client.SaveRegAttributes(objListRegAtributes);
	}

	public List<RegAttributes> RegAttributesGetAll(RegAttributeReq oReq)
	{
		List<RegAttributes> result = new List<RegAttributes>();
		try
		{
			result = _client.RegAttributesGetAll(oReq).ToList();
		}
		catch (Exception ex){
			result = null;
		}
		return result;
	}

	public List<RegAttributes> RegAttributesGetByRegID(int _regID)
	{
		List<RegAttributes> result = new List<RegAttributes>();
		try
		{
			result = _client.RegAttributesGetByRegID(_regID).ToList();
		}
		catch (Exception ex){
			result = null;
		}
		return result;
	}

	public List<RegAttributes> RegAttributesGetBySuppLineID(int _suppLineID)
	{
		List<RegAttributes> result = new List<RegAttributes>();
		try
		{
			result = _client.RegAttributesGetBySuppLineID(_suppLineID).ToList();
		}
		catch (Exception ex)
		{
			result = null;
		}
		return result;
	}

    //12052015 Add for Supervisor Writeoff approval - Lus
    public int SaveJustificationDetails(RegJustification objListRegJustification)
    {
        return _client.SaveJustificationDetails(objListRegJustification);
    }

    // 20160120 - w.loon - Write to TrnRegBreFailTreatment - for display in Supervisor Dashboard
    public int SaveBreFailTreatmentDetails(RegBreFailTreatment objRegBreFailTreatment)
    {
        return _client.SaveBreFailTreatmentDetails(objRegBreFailTreatment);
    }

	public int SaveSurveyResponse(RegSurveyResponse objRegSurveyResponse)
	{
		return _client.SaveSurveyResponse(objRegSurveyResponse);
	}

        public List<int> SaveTrnRegAccessory(List<RegAccessory> objRegAccessories)
        {
            var result = new List<int>();
            try
            {
                result = _client.SaveTrnRegAccessory(objRegAccessories.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                result = null;
            }
            return result;
        }

        public List<RegAccessory> GetRegAccessoryByRegID(int regID)
        {
            var result = new List<RegAccessory>();
            try
            {
                result = _client.GetRegAccessoryByRegID(regID).ToList();
            }
            catch (Exception ex)
            {
                result = null;
            }
            return result;
        }

        public bool SaveTrnRegAccessoryDetails(List<RegAccessoryDetails> objRegAccessoryDetailsList)
        {
            var result = false;

            try
            {
                result = _client.SaveTrnRegAccessoryDetails(objRegAccessoryDetailsList.ToArray());
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public List<RegAccessoryDetails> GetRegAccessoryDetailsByRegAccessoryID(List<int> regAccessoryID)
        {
            var result = new List<RegAccessoryDetails>();
            try
            {
                result = _client.GetRegAccessoryDetailsByRegAccessoryID(regAccessoryID.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                result = null;
            }
            return result;
        }

        public int SaveAccessorySerialNumber(List<RegAccessoryDetails> regAccessoryDetailsList)
        {
            var result = 0;
            try
            {
                result = _client.SaveAccessorySerialNumber(regAccessoryDetailsList.ToArray());
            }
            catch (Exception ex)
            {
                result = -1;
            }
            return result;
        }

        public List<PopUpMessage> GetPopUpMessageByIdCard(string IDCardNo)
        {
            var result = new List<PopUpMessage>();
            try { result = _client.GetPopUpMessageByIdCard(IDCardNo).ToList(); }
            catch (Exception ex) { result = null; }
            return result;
        }

        public int UpdatePopUpMessageAcceptReject(int ID, string Action)
        {
            return _client.UpdatePopUpMessageAcceptReject(ID, Action);
        }

        public List<PopUpMessageAuditLog> SavePopUpMessageAuditLog(List<PopUpMessageAuditLog> objPopUpMessageAuditLogList)
        {
            var result = new List<PopUpMessageAuditLog>();
            try
            {
                result = _client.SavePopUpMessageAuditLog(objPopUpMessageAuditLogList.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                result = null;
            }
            return result;
        }

		public List<PegaRecommendation> SavePegaRecommendation(List<PegaRecommendation> objListPegaRecommendation)
		{
			var result = new List<PegaRecommendation>();
			try
			{
				result = _client.SavePegaRecommendation(objListPegaRecommendation.ToArray()).ToList();
			}
			catch (Exception ex)
			{
				result = null;
			}
			return result;
		}

        public List<PegaRecommendation> GetPegaRecommendationbyMSISDN(PegaRecommendationSearchCriteria criteria)
        {
            var result = new List<PegaRecommendation>();
            try
            {
                result = _client.GetPegaRecommendationbyMSISDN(criteria).ToList();
            }
            catch (Exception ex)
            {
                result = null;
            }
            return result;
        }

        public int UpdatePegaRecommendation(List<PegaRecommendationVM> objPegaVM)
        {
            var result = new int();
            try
            {
                result = _client.UpdatePegaRecommendation(objPegaVM.ToArray());
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }

        public int InsertTacAuditLog(TacAuditLog TacAuditLog)
        {
            var request = new TacAuditLogReq() { TacAuditLog = TacAuditLog };
            var response = _client.InsertTacAuditLog(request);
            LogResponse(response);

            return response.ID;
        }

		public List<TacAuditLog> GetAllTacAuditLog(int rowCount)
		{
			var result = new List<TacAuditLog>();
			try
			{
				result = _client.GetAllTacAuditLog(rowCount).ToList();
			}
			catch (Exception ex)
			{
				result = null;
			}

			return result;
		}

    }
}
