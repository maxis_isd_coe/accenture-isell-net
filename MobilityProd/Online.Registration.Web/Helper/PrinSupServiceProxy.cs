﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.DAL.Models;
//using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.SubscriberICService;
using System.Net;
using System.Xml.Linq;
using log4net;



namespace Online.Registration.Web.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public class PrinSupServiceProxy : ServiceProxyBase<SubscriberICServiceClient>
    {
        public static int counter = 0;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PrinSupServiceProxy));
        public PrinSupServiceProxy()
        {
            _client = new SubscriberICServiceClient();
        }

        #region Private Methods

        #endregion

        #region Public Methods

        public retrievegetPrinSuppResponse getPrinSupplimentarylines(SubscribergetPrinSuppRequest request,bool prinFlag=false)
        {
            retrievegetPrinSuppResponse prinsupp = null;

            try
            {
                prinsupp = _client.getPrinSupplimentarylines(request, prinFlag);
            }
            catch (Exception ex)
            {
                prinsupp = null;
            }

            return prinsupp;

        }


        public retrieveMismDetlsResponse getPrimSecondaryines(string msISDN)
        {
            retrieveMismDetlsResponse primseco = null;

            try
            {
                primseco = _client.retrieveMsimDetails(msISDN);
            }
            catch (Exception ex)
            {
                primseco = null;
            }

            return primseco;

        }

        #endregion
    }
}