﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace Online.Registration.Web.Helper
{
    public abstract class ServiceProxyBase<TClient> : IDisposable where TClient : class, ICommunicationObject
    {
        protected TClient _client;

        #region IDisposable Members

        public void Dispose()
        {
            if (_client == null)
                return;

            switch (_client.State)
            {
                case CommunicationState.Opened:
                    _client.Close();
                    break;

                case CommunicationState.Faulted:
                    _client.Abort();
                    break;
            }

            _client = null;
        }

        #endregion
    }
}