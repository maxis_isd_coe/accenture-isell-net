﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.ComplainSvc;
using Online.Registration.DAL.Models;

using log4net;

namespace Online.Registration.Web.Helper
{
    public class ComplainServiceProxy : ServiceProxyBase<ComplainServiceClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(ComplainServiceProxy));
        public string LastErrorMessage { get; set; }

        public ComplainServiceProxy()
        {
            _client = new ComplainServiceClient();
        }

        public bool IsLastCallFailed()
        {
            return !string.IsNullOrEmpty(LastErrorMessage);
        }

        #region Private Methods

        private void LogResponse(string code, string message, string stackTrace = "")
        {
            LastErrorMessage = "";

            if (!Util.IsSuccessCode(code))
            {
                LastErrorMessage = message;

                // Log Error
                Logger.ErrorFormat("Code: {0}|Message: {1}|StackTrace: {2}", code, message, stackTrace);
            }
        }

        private void LogResponse(WCFResponse response)
        {
            LogResponse(response.Code, response.Message, response.StackTrace);
        }

        #endregion

        #region Complain

        public void ComplainCreate(Complain complain)
        {
            var request = new ComplainCreateReq { Complain = complain };
            var response = _client.ComplainCreate(request);
            LogResponse(response);

            complain.ID = response.ID;
        }

        public void ComplainUpdate(Complain complain)
        {
            var request = new ComplainUpdateReq { Complain = complain };
            var response = _client.ComplainUpdate(request);
            LogResponse(response);
        }

        public IEnumerable<int> ComplainFind(ComplainFind findCriteria)
        {
            var request = new ComplainFindReq { FindCriteria = findCriteria };
            var response = _client.ComplainFind(request);
            LogResponse(response);

            return response.IDs;
        }

        public IEnumerable<Complain> ComplainGet(IEnumerable<int> ids)
        {
            var request = new ComplainGetReq { IDs = ids.ToArray() };
            var response = _client.ComplainGet(request);
            LogResponse(response);

            return response.Complains;
        }

        public static void CreateComplain(Complain complain)
        {
            using (var proxy = new ComplainServiceProxy())
            {
                proxy.ComplainCreate(complain);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);
            }
        }

        public static void UpdateComplain(Complain complain)
        {
            using (var proxy = new ComplainServiceProxy())
            {
                proxy.ComplainUpdate(complain);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);
            }
        }



        #endregion

        #region Complaint Issues
        public ComplaintIssuesCreateResp ComplaintIssuesCreate(ComplaintIssues complaintIssues)
        {
            var complaintIssuesCreateRq = new ComplaintIssuesCreateReq()
            {
                ComplaintIssues = complaintIssues
            };

            var resp = _client.ComplaintIssuesCreate(complaintIssuesCreateRq);
            return resp;
        }
        public IEnumerable<int> ComplaintIssuesFind(ComplaintIssuesFind complaintIssues)
        {
            var request = new ComplaintIssuesFindReq()
            {
                FindCriteria = complaintIssues
            };

            var response = _client.ComplaintIssuesFind(request);
            return response.IDs;
        }
        public ComplaintIssues ComplaintIssuesGet(int complaintIssuesID)
        {
            var complaintIssues = ComplaintIssuesGet(new int[] { complaintIssuesID });
            return complaintIssues == null ? null : complaintIssues.SingleOrDefault();
        }
        public IEnumerable<ComplaintIssues> ComplaintIssuesGet(IEnumerable<int> complaintIssues)
        {
            if (complaintIssues == null)
                return new List<ComplaintIssues>();

            var request = new ComplaintIssuesGetReq()
            {
                IDs = complaintIssues.ToArray()
            };

            var response = _client.ComplaintIssuesGet(request);

            return response.ComplaintIssuess;
        }

        public ComplaintIssuesGetResp ComplaintIssuesList()
        {

            var response = _client.ComplaintIssuesList();

            return response;
        }

        public IEnumerable<DAL.Models.ComplaintIssues> FindComplaintIssues(ComplaintIssuesFind ComplaintIssues)
        {
            return ComplaintIssuesGet(ComplaintIssuesFind(ComplaintIssues));
        }
        public void ComplaintIssuesUpdate(ComplaintIssues complaintIssues)
        {
            var complaintIssuesUpdateReq = new ComplaintIssuesUpdateReq()
            {
                ComplaintIssues = complaintIssues
            };

            var resp = _client.ComplaintIssuesUpdate(complaintIssuesUpdateReq);
        }
        /*
        public void ComplaintIssuesCreate(ComplaintIssues complaintIssues)
        {
            var request = new ComplaintIssuesCreateReq { ComplaintIssues = complaintIssues };
            var response = _client.ComplaintIssuesCreate(request);
            LogResponse(response);

            complaintIssues.ID = response.ID;
        }

        public void ComplaintIssuesUpdate(ComplaintIssues complaintIssues)
        {
            var request = new ComplaintIssuesUpdateReq { ComplaintIssues = complaintIssues };
            var response = _client.ComplaintIssuesUpdate(request);
            LogResponse(response);
        }

        public IEnumerable<int> ComplaintIssuesFind(ComplaintIssuesFind findCriteria)
        {
            var request = new ComplaintIssuesFindReq { FindCriteria = findCriteria };
            var response = _client.ComplaintIssuesFind(request);
            //LogResponse(response);

            return response.IDs;
        }

        public IEnumerable<ComplaintIssues> ComplaintIssuesGet(IEnumerable<int> ids)
        {
            //var request = new ComplaintIssuesGetReq { IDs = ids.ToArray() };
            //var response = _client.ComplaintIssuesGet(request);
           // LogResponse(response);

            return response.ComplaintIssuess;

            if (ids == null)
                return new List<ComplaintIssues>();

            var request = new ComplaintIssuesGetReq()
            {
                IDs = ids.ToArray()
            };

            var response = _client.ComplaintIssuesGet(request);

            return response.ComplaintIssuess;
        }

        public ComplaintIssues ComplaintIssuesGet(int complaintIssuesID)
        {
            var issues = ComplaintIssuesGet(new int[] { complaintIssuesID });
            return issues == null ? null : issues.SingleOrDefault();
        }

        public static IEnumerable<ComplaintIssues> FindComplaintIssues(ComplaintIssuesFind findCriteria)
        {
            using (var proxy = new ComplainServiceProxy())
            {
                return proxy.ComplaintIssuesGet(proxy.ComplaintIssuesFind(findCriteria));
            }
        }*/

        #endregion

        #region Complaint Issues Group

        public void ComplaintIssuesGroupCreate(ComplaintIssuesGroup complaintIssuesGrp)
        {
            var request = new ComplaintIssuesGroupCreateReq { ComplaintIssuesGroup = complaintIssuesGrp };
            var response = _client.ComplaintIssuesGroupCreate(request);
            LogResponse(response);

            complaintIssuesGrp.ID = response.ID;
        }

        public void ComplaintIssuesGroupUpdate(ComplaintIssuesGroup complaintIssuesGrp)
        {
            var request = new ComplaintIssuesGroupUpdateReq { ComplaintIssuesGroup = complaintIssuesGrp };
            var response = _client.ComplaintIssuesGroupUpdate(request);
            LogResponse(response);
        }

        public IEnumerable<int> ComplaintIssuesGroupFind(ComplaintIssuesGroupFind findCriteria)
        {
            var request = new ComplaintIssuesGroupFindReq { FindCriteria = findCriteria };
            var response = _client.ComplaintIssuesGroupFind(request);
            LogResponse(response);

            return response.IDs;
        }
        public IEnumerable<ComplaintIssues> GetRequredXMLInputs(ComplaintIssuesGroupFind findCriteria)
        {
            var request = new ComplaintIssuesGroupFindReq { FindCriteria = findCriteria };
            var response = _client.GetRequredXMLInputs(request);
            LogResponse(response);

            return response.ComplaintIssuess;
        }
        public IEnumerable<ComplaintIssuesGroup> ComplaintIssuesGroupGet(IEnumerable<int> ids)
        {
            var request = new ComplaintIssuesGroupGetReq { IDs = ids.ToArray() };
            var response = _client.ComplaintIssuesGroupGet(request);
            LogResponse(response);

            return response.ComplaintIssuesGroups;
        }

        public static IEnumerable<ComplaintIssuesGroup> FindComplaintIssuesGroup(ComplaintIssuesGroupFind findCriteria)
        {
            using (var proxy = new ComplainServiceProxy())
            {
                return proxy.ComplaintIssuesGroupGet(proxy.ComplaintIssuesGroupFind(findCriteria));
            }
        }

        #endregion

        #region Issue Group Dispatch Queue

        public void IssueGrpDispatchQCreate(IssueGrpDispatchQ issueGrpDispatchQ)
        {
            var request = new IssueGrpDispatchQCreateReq { IssueGrpDispatchQ = issueGrpDispatchQ };
            var response = _client.IssueGrpDispatchQCreate(request);
            LogResponse(response);

            issueGrpDispatchQ.ID = response.ID;
        }

        public void IssueGrpDispatchQUpdate(IssueGrpDispatchQ issueGrpDispatchQ)
        {
            var request = new IssueGrpDispatchQUpdateReq { IssueGrpDispatchQ = issueGrpDispatchQ };
            var response = _client.IssueGrpDispatchQUpdate(request);
            LogResponse(response);
        }

        public IEnumerable<int> IssueGrpDispatchQFind(IssueGrpDispatchQFind findCriteria)
        {
            var request = new IssueGrpDispatchQFindReq { FindCriteria = findCriteria };
            var response = _client.IssueGrpDispatchQFind(request);
            LogResponse(response);

            return response.IDs;
        }

        public IEnumerable<IssueGrpDispatchQ> IssueGrpDispatchQGet(IEnumerable<int> ids)
        {
            var request = new IssueGrpDispatchQGetReq { IDs = ids.ToArray() };
            var response = _client.IssueGrpDispatchQGet(request);
            LogResponse(response);

            return response.IssueGrpDispatchQs;
        }

        public static IEnumerable<IssueGrpDispatchQ> FindIssueGrpDispatchQ(IssueGrpDispatchQFind findCriteria)
        {
            using (var proxy = new ComplainServiceProxy())
            {
                return proxy.IssueGrpDispatchQGet(proxy.IssueGrpDispatchQFind(findCriteria));
            }
        }

        #endregion

        public Online.Registration.Web.ComplainSvc.ComplaintGetResp GetCmssList(Online.Registration.DAL.Models.CMSSComplainGet objComplaint)
        {
            return _client.GetCmssList(objComplaint);
        }
    }
}