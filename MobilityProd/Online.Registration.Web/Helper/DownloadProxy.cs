﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.iContractSearchSvc;

using log4net;
using Online.Registration.DAL.Models;

namespace Online.Registration.Web.Helper
{
	public class DownloadProxy : ServiceProxyBase<IContractSearchClient>
	{
		private static ILog Logger = LogManager.GetLogger(typeof(DownloadProxy));
		public string LastErrorMessage { get; set; }

		public DownloadProxy()
		{
			_client = new IContractSearchClient();
		}

		public SearchResponse downloadDocumentIContract(IContractSearchRequest request) 
		{
			SearchResponse response = _client.DownloadFromIContract(request);

			return response;
		}

	}
}