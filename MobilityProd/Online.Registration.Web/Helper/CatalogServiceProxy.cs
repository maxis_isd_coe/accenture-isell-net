﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.Models;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ContentRepository;

namespace Online.Registration.Web.Helper
{
    public class CatalogServiceProxy : ServiceProxyBase<CatalogServiceClient>
    {
        public string LastErrorMessage { get; set; }

        public CatalogServiceProxy()
        {
            _client = new CatalogServiceClient();
        }

        #region Private Methods

        //unused methods
        //private void LogResponse(string code, string message, string stackTrace = "")
        //{
        //    LastErrorMessage = "";

        //    if (!Util.IsSuccessCode(code))
        //    {
        //        LastErrorMessage = message;

        //        // Log Error
        //        //Logger.ErrorFormat("Code: {0}|Message: {1}|StackTrace: {2}", code, message, stackTrace);
        //    }
        //}

        //unused methods
        //private void LogResponse(WCFResponse response)
        //{
        //    LogResponse(response.Code, response.Message, response.StackTrace);
        //}

        #endregion

        #region Public Methods

        public bool IsLastCallFailed()
        {
            return !string.IsNullOrEmpty(LastErrorMessage);
        }

        public List<PgmBdlPckComponent> GetLnkPgmBdlPkgComp(Online.Registration.DAL.Models.PgmBdlPckComponentFind oReq)
        {
            var dataObj = _client.GetLnkPgmBdlPkgComp(new PgmBdlPckComponentFindReq() { PgmBdlPckComponentFind = oReq });
            if (string.IsNullOrEmpty(dataObj.ComponentsZippedData))
            {
                return new List<PgmBdlPckComponent>();
            }

            var unzippedData = SNT.Utility.Compress.Unzip(dataObj.ComponentsZippedData);

            List<PgmBdlPckComponent> lstItems = Util.Deserialize<List<PgmBdlPckComponent>>(unzippedData);

            return lstItems;
            // var dataObj = _client.Get
        }
        #endregion

        #region Brand
        public BrandCreateResp BrandCreate(Brand Brand)
        {
            var brandCreateRq = new BrandCreateReq()
            {
                Brand = Brand
            };

            var resp = _client.BrandCreate(brandCreateRq);

            return resp;
        }
        public void BrandUpdate(Brand Brand)
        {
            var brandUpdateRq = new BrandUpdateReq()
            {
                Brand = Brand
            };

            var resp = _client.BrandUpdate(brandUpdateRq);

        }
        public IEnumerable<int> BrandFind(BrandFind Brand)
        {
            var request = new BrandFindReq()
            {
                BrandFind = Brand
            };

            var response = _client.BrandFind(request);
            return response.IDs;
        }
        public DAL.Models.Brand BrandGet(int brandID)
        {
            var brands = BrandGet(new int[] { brandID });
            return brands == null ? null : brands.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.Brand> BrandGet(IEnumerable<int> Brands)
        {
            if (Brands == null)
                return new List<Brand>(); ;

            var request = new BrandGetReq()
            {
                IDs = Brands.ToArray()
            };

            var response = _client.BrandGet(request);

            return response.Brands;
        }

        public BrandGetResp BrandsList()
        {
            var response = _client.BrandsList();
            return response;
        }

        public IEnumerable<DAL.Models.Brand> FindBrand(BrandFind Brand)
        {

            return BrandGet(BrandFind(Brand));
        }
        #endregion

        #region Bundle
        public BundleCreateResp BundleCreate(Bundle bundle)
        {
            var bundleCreateRq = new BundleCreateReq()
            {
                Bundle = bundle
            };

            var resp = _client.BundleCreate(bundleCreateRq);

            return resp;
        }
        public void BundleUpdate(Bundle bundle)
        {
            var bundleUpdateRq = new BundleUpdateReq()
            {
                Bundle = bundle
            };

            var resp = _client.BundleUpdate(bundleUpdateRq);

        }
        public IEnumerable<int> BundleFind(BundleFind bundle)
        {
            var request = new BundleFindReq()
            {
                BundleFind = bundle,
            };

            var response = _client.BundleFind(request);

            return response.IDs;
        }
        public IEnumerable<DAL.Models.Bundle> BundleGet(IEnumerable<int> bundles)
        {
            if (bundles == null)
                return new List<Bundle>(); ;

            var request = new BundleGetReq()
            {
                IDs = bundles.ToArray()
            };

            var response = _client.BundleGet(request);

            return response.Bundles;
        }

        public BundleGetResp BundlesList()
        {
            var response = _client.BundlesList();

            return response;
        }

        public IEnumerable<Bundle> FindBundle(BundleFind bundle)
        {

            return BundleGet(BundleFind(bundle));
        }
        #endregion

        #region Category

        public int GettllnkContractDataPlan(int selectedid)
        {
            var resp = _client.GettllnkContractDataPlan(selectedid);

            return resp;
        }

        public int[] GettllnkContractDataPlan(List<int> selectedids)
        {
            var resp = _client.GettllnkContractDataPlanList(selectedids.ToArray());
            return resp;
        }

        public int[] GetDependentComponents(int componentId)
        {
            var resp = _client.GetDependentComponents(componentId);

            return resp;
        }

        public int[] GetDependentComponentList()
        {
            var resp = _client.GetDependentComponentList();
            return resp;
        }
        public List<Online.Registration.DAL.Models.dependentContract> GetContractsOnOfferId(string OfferIds)
        {
            var resp = _client.GetContractsOnOfferId(OfferIds);
            return resp.ToList();
        }

        public DataPlanIdsResp getFilterplanvalues(int plantypeid)
        {

            var resp = _client.getFilterplanvalues(plantypeid);

            return resp;
        }
        public CategoryCreateResp CategoryCreate(Category category)
        {
            var categoryCreateRq = new CategoryCreateReq()
            {
                Category = category
            };

            var resp = _client.CategoryCreate(categoryCreateRq);

            return resp;
        }
        public void CategoryUpdate(Category category)
        {
            var categoryUpdateRq = new CategoryUpdateReq()
            {
                Category = category
            };

            var resp = _client.CategoryUpdate(categoryUpdateRq);

        }
        public IEnumerable<int> CategoryFind(CategoryFind Category)
        {
            var request = new CategoryFindReq()
            {
                CategoryFind = Category
            };

            var response = _client.CategoryFind(request);

            return response.IDs;
        }
        public DAL.Models.Category CategoryGet(int categoryID)
        {
            var categories = CategoryGet(new int[] { categoryID });
            return categories == null ? null : categories.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.Category> CategoryGet(IEnumerable<int> Categorys)
        {
            if (Categorys == null)
                return new List<Category>(); ;

            var request = new CategoryGetReq()
            {
                IDs = Categorys.ToArray()
            };

            var response = _client.CategoryGet(request);

            return response.Categorys;
        }

        public CategoryGetResp CategoriesList()
        {
            var response = _client.CategoriesList();
            return response;
        }

        public IEnumerable<DAL.Models.Category> FindCategory(CategoryFind oRq)
        {
            return CategoryGet(CategoryFind(oRq));
        }
        public static IEnumerable<Category> GetCategory(IEnumerable<int> ids)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.CategoryGet(ids);
            }
        }
        public static Category GetCategory(int id)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.CategoryGet(id);
            }
        }

        #endregion

        #region Colour

        public ColourCreateResp ColourCreate(Colour colour)
        {
            var colourCreateRq = new ColourCreateReq()
            {
                Colour = colour
            };

            var resp = _client.ColourCreate(colourCreateRq);

            return resp;
        }
        public void ColourUpdate(Colour colour)
        {
            var colourUpdateRq = new ColourUpdateReq()
            {
                Colour = colour
            };

            var resp = _client.ColourUpdate(colourUpdateRq);

        }
        public IEnumerable<Colour> FindColour(ColourFind colour)
        {
            return ColourGet(ColourFind(colour));
        }
        public static IEnumerable<Colour> GetColour(IEnumerable<int> ids)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.ColourGet(ids);
            }
        }
        public static Colour GetColour(int id)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.ColourGet(id);
            }
        }
        public IEnumerable<int> ColourFind(ColourFind colour)
        {
            var request = new ColourFindReq()
            {
                ColourFind = colour
            };

            var response = _client.ColourFind(request);

            return response.IDs;
        }
        public Colour ColourGet(int colourID)
        {
            var colours = ColourGet(new int[] { colourID });
            return colours == null ? null : colours.SingleOrDefault();
        }
        public IEnumerable<Colour> ColourGet(IEnumerable<int> colours)
        {
            if (colours == null)
                return new List<Colour>();

            var request = new ColourGetReq()
            {
                IDs = colours.ToArray()
            };

            var response = _client.ColourGet(request);

            return response.Colours;
        }

        public ColourGetResp ColoursList()
        {
            var response = _client.ColoursList();

            return response;
        }


        #endregion

        #region Component
        public ComponentCreateResp ComponentCreate(Component component)
        {
            var componentCreateRq = new ComponentCreateReq()
            {
                Component = component
            };

            var resp = _client.ComponentCreate(componentCreateRq);

            return resp;
        }
        public void ComponentUpdate(Component component)
        {
            var componentUpdateRq = new ComponentUpdateReq()
            {
                Component = component
            };

            var resp = _client.ComponentUpdate(componentUpdateRq);

        }
        public IEnumerable<int> ComponentFind(ComponentFind component)
        {
            var request = new ComponentFindReq()
            {
                ComponentFind = component,
            };

            var response = _client.ComponentFind(request);

            return response.IDs;
        }

        public IEnumerable<Component> ComponentGet(IEnumerable<int> components)
        {
            if (components == null)
                return new List<Component>(); ;

            var request = new ComponentGetReq()
            {
                IDs = components.ToArray()
            };

            var response = _client.ComponentGet(request);

            return response.Components;
        }

        public IEnumerable<VasesWithPkg> ComponentGetWithPackage(IEnumerable<ComponentWithPkg> components)
        {
            if (components == null)
                return new List<VasesWithPkg>(); ;


            var request = new ComponentGetReq()
            {
                IDs = (from cmp in components select cmp.ComponentId).ToArray()
            };

            var response = _client.ComponentGet(request);

            var vases = response.Components.Where(a => a.RestricttoKenan == false).ToList();

            var compts = components.ToList();

            var result = (from vas in vases join comps in compts on vas.ID equals comps.ComponentId select new VasesWithPkg(vas.Name, comps.LinkType)).ToList();

            return result;
        }

        public IEnumerable<Component> ContractComponentGet()
        {
            var response = _client.ContractComponentGet();

            return response.Components;
        }
        public IEnumerable<DAL.Models.Component> FindComponent(ComponentFind component)
        {
            return ComponentGet(ComponentFind(component));
        }
        #endregion

        #region ComponentType
        public ComponentTypeCreateResp ComponentTypeCreate(ComponentType componentType)
        {
            var componentTypeCreateRq = new ComponentTypeCreateReq()
            {
                ComponentType = componentType
            };

            var resp = _client.ComponentTypeCreate(componentTypeCreateRq);

            return resp;
        }
        public void ComponentTypeUpdate(ComponentType componentType)
        {
            var componentTypeUpdateRq = new ComponentTypeUpdateReq()
            {
                ComponentType = componentType
            };

            var resp = _client.ComponentTypeUpdate(componentTypeUpdateRq);

        }
        public IEnumerable<int> ComponentTypeFind(ComponentTypeFind componentType)
        {
            var request = new ComponentTypeFindReq()
            {
                ComponentTypeFind = componentType
            };

            var response = _client.ComponentTypeFind(request);

            return response.IDs;
        }
        public IEnumerable<ComponentType> ComponentTypeGet(IEnumerable<int> componentTypes)
        {
            if (componentTypes == null)
                return new List<ComponentType>(); ;

            var request = new ComponentTypeGetReq()
            {
                IDs = componentTypes.ToArray()
            };

            var response = _client.ComponentTypeGet(request);

            return response.ComponentTypes;
        }
        public ComponentTypeGetResp ComponentTypeList()
        {
            var response = _client.ComponentTypeList();
            return response;
        }
        public IEnumerable<ComponentType> FindComponentType(ComponentTypeFind componentType)
        {
            return ComponentTypeGet(ComponentTypeFind(componentType));
        }
        #endregion

        #region ComponentRelation
        public List<string> ComponentRelationGetMutualExclusive(int planBundle)
        {
            var resp = _client.ComponentRelationGet(planBundle, Constants.COMPONENT_RELATION_MUTUAL_EXCLUSIVE);
            List<string> result = new List<string>();

            foreach (ComponentRelation comp in resp)
            {
                if (comp.ComponentPgm != null && !string.IsNullOrEmpty(comp.KenanCode))
                {
                    result.Add(comp.ComponentPgm + "_" + comp.KenanCode);
                }
            }
            return result;
        }
        #endregion

		public List<string> ComponentRelationGetDependencyExclusiveByComponent(int componentPgmID)
		{
			var resp = _client.ComponentRelationGet(componentPgmID, Constants.COMPONENT_RELATION_MUTUAL_DEPENDENCY);
			List<string> result = new List<string>();

			foreach (ComponentRelation comp in resp)
			{
				if (comp.ComponentPgm != null && !string.IsNullOrEmpty(comp.KenanCode))
				{
					result.Add(comp.ComponentPgm + "_" + comp.KenanCode);
				}
			}
			return result;
		}

		public List<ComponentRelation> GetAllComponentRelationByRule(string rules)
		{
			var resp = _client.GetAllComponentRelationByRule(rules);

			return resp.ToList();
		}

		#region planDeviceValidationRules
		public PlanDeviceValidationRulesGetResp PlanDeviceValidationRulesGet(int planBundle, string deviceArticle, string VAS)
		{
			var PlanDeviceValidation = new PlanDeviceValidationRules()
			{
				existingPlanId = planBundle,
				existingDeviceArticleId = deviceArticle,
				existingVASId = VAS
			};
			var PlanDeviceValidationRequest = new PlanDeviceValidationRulesGetReq()
			{
				PlanDeviceValidationRules = PlanDeviceValidation
			};

			var resp = _client.PlanDeviceValidationRulesGet(PlanDeviceValidationRequest);

			return resp;
		}
		#endregion

        #region Model
        public ModelCreateResp ModelCreate(Model model)
        {
            var modelCreateRq = new ModelCreateReq()
            {
                Model = model
            };

            var resp = _client.ModelCreate(modelCreateRq);

            return resp;
        }
        public void ModelUpdate(Model model)
        {
            var modelUpdateRq = new ModelUpdateReq()
            {
                Model = model
            };

            var resp = _client.ModelUpdate(modelUpdateRq);

        }
        public IEnumerable<Model> FindModel(ModelFind model)
        {
            return ModelGet(ModelFind(model));
        }
        public static IEnumerable<Model> GetModel(IEnumerable<int> ids)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.ModelGet(ids);
            }
        }
        public static Model GetModel(int id)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.ModelGet(id);
            }
        }
        public IEnumerable<int> ModelFind(ModelFind model)
        {
            var request = new ModelFindReq()
            {
                ModelFind = model
            };

            var response = _client.ModelFind(request);

            return response.IDs;
        }
        public Model ModelGet(int modelID)
        {
            var models = ModelGet(new int[] { modelID });
            return models == null ? null : models.SingleOrDefault();
        }
        public IEnumerable<Model> ModelGet(IEnumerable<int> models)
        {
            if (models == null)
                return new List<Model>(); ;

            var request = new ModelGetReq()
            {
                IDs = models.ToArray()
            };

            var response = _client.ModelGet(request);

            return response.Models;
        }

        public ModelGetResp ModelsList()
        {
            var response = _client.ModelsList();
            return response;
        }

        #endregion

        #region ModelImage
        public void ModelImageCreate(ModelImage modelImage)
        {
            var modelImageRq = new ModelImageCreateReq()
            {
                ModelImage = modelImage
            };

            var resp = _client.ModelImageCreate(modelImageRq);
        }
        public void ModelImageUpdate(ModelImage modelImage)
        {
            var modelImageUpdateRq = new ModelImageUpdateReq()
            {
                ModelImage = modelImage
            };

            var resp = _client.ModelImageUpdate(modelImageUpdateRq);
        }
        public IEnumerable<int> ModelImageFind(ModelImageFind ModelImage)
        {
            var request = new ModelImageFindReq()
            {
                ModelImageFind = ModelImage
            };

            var response = _client.ModelImageFind(request);
            return response.IDs;
        }
        public DAL.Models.ModelImage ModelImageGet(int ModelImageID)
        {
            var ModelImages = ModelImageGet(new int[] { ModelImageID });
            return ModelImages == null ? null : ModelImages.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.ModelImage> ModelImageGet(IEnumerable<int> ModelImages)
        {
            if (ModelImages == null)
                return new List<ModelImage>(); ;

            var request = new ModelImageGetReq()
            {
                IDs = ModelImages.ToArray()
            };

            var response = _client.ModelImageGet(request);

            return response.ModelImages;
        }
        public static IEnumerable<DAL.Models.ModelImage> FindModelImage(ModelImageFind ModelImage)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.ModelImageGet(proxy.ModelImageFind(ModelImage));
            }
        }
        #endregion

        #region Model Group
        public ModelGroupCreateResp ModelGroupCreate(ModelGroup modelGroup)
        {
            var modelGroupCreateRq = new ModelGroupCreateReq()
            {
                ModelGroup = modelGroup
            };

            var resp = _client.ModelGroupCreate(modelGroupCreateRq);

            return resp;
        }
        public void ModelGroupUpdate(ModelGroup modelGroup)
        {
            var modelGroupUpdateRq = new ModelGroupUpdateReq()
            {
                ModelGroup = modelGroup
            };

            var resp = _client.ModelGroupUpdate(modelGroupUpdateRq);

        }
        public IEnumerable<int> ModelGroupFind(ModelGroupFind mgp)
        {
            var request = new ModelGroupFindReq()
            {
                ModelGroupFind = mgp
            };

            var response = _client.ModelGroupFind(request);

            return response.IDs;
        }
        public IEnumerable<ModelGroup> ModelGroupGet(IEnumerable<int> mgcs)
        {
            if (mgcs == null)
                return new List<ModelGroup>(); ;

            var request = new ModelGroupGetReq()
            {
                IDs = mgcs.ToArray()
            };

            var response = _client.ModelGroupGet(request);

            return response.ModelGroups;
        }
        public ModelGroupGetResp ModelGroupList()
        {
            var response = _client.ModelGroupList();
            return response;
        }

        public IEnumerable<ModelGroup> ModelGroupGetByPBPCIDs(IEnumerable<int> pdpcIDs)
        {
            if (pdpcIDs == null)
                return new List<ModelGroup>(); ;

            var request = new ModelGroupGetReq()
            {
                IDs = pdpcIDs.ToArray()
            };

            var response = _client.ModelGroupGetByPBPCIDs(request);

            return response.ModelGroups;
        }
        public IEnumerable<ModelGroup> FindModelGroup(ModelGroupFind mgp)
        {
            return ModelGroupGet(ModelGroupFind(mgp));
        }
        #endregion

        #region Program
        public ProgramCreateResp ProgramCreate(Program program)
        {
            var programCreateRq = new ProgramCreateReq()
            {
                Program = program
            };

            var resp = _client.ProgramCreate(programCreateRq);

            return resp;
        }
        public void ProgramUpdate(Program program)
        {
            var programUpdateRq = new ProgramUpdateReq()
            {
                Program = program
            };

            var resp = _client.ProgramUpdate(programUpdateRq);

        }
        public IEnumerable<int> ProgramFind(ProgramFind program)
        {
            var request = new ProgramFindReq()
            {
                ProgramFind = program
            };

            var response = _client.ProgramFind(request);

            return response.IDs;
        }
        /*public IEnumerable<Program> ProgramGet(IEnumerable<int> programs)
        {
            if (programs == null)
                return new List<Program>(); ;

            var request = new ProgramGetReq()
            {
                IDs = programs.ToArray()
            };

            var response = _client.ProgramGet(request);

            return response.Programs;
        }*/
        public IEnumerable<DAL.Models.Program> ProgramGet(IEnumerable<int> programs)
        {
            if (programs == null)
                return new List<Program>(); ;

            var request = new ProgramGetReq()
            {
                IDs = programs.ToArray()
            };

            var response = _client.ProgramGet(request);

            return response.Programs;
        }

        public ProgramGetResp ProgramsList()
        {
            var response = _client.ProgramsList();
            return response;
        }

        public DAL.Models.Program ProgramGet(int programID)
        {
            var programs = ProgramGet(new int[] { programID });
            return programs == null ? null : programs.SingleOrDefault();
        }
        public IEnumerable<Program> FindProgram(ProgramFind program)
        {
            return ProgramGet(ProgramFind(program));
        }
        #endregion

        #region Package
        public BundlepackageResp GetMandatoryVas(int selectedid)
        {

            var resp = _client.GetMandatoryVas(selectedid);

            return resp;
        }
        public PackageCreateResp PackageCreate(Package package)
        {
            var packageCreateRq = new PackageCreateReq()
            {
                Package = package
            };

            var resp = _client.PackageCreate(packageCreateRq);

            return resp;
        }
        public void PackageUpdate(Package package)
        {
            var packageUpdateRq = new PackageUpdateReq()
            {
                Package = package
            };

            var resp = _client.PackageUpdate(packageUpdateRq);

        }
        public IEnumerable<int> PackageFind(PackageFind package)
        {
            var request = new PackageFindReq()
            {
                PackageFind = package
            };

            var response = _client.PackageFind(request);

            return response.IDs;
        }
        public IEnumerable<Package> PackageGet(IEnumerable<int> packages)
        {
            if (packages == null)
                return new List<Package>(); ;

            var request = new PackageGetReq()
            {
                IDs = packages.ToArray()
            };

            var response = _client.PackageGet(request);

            return response.Packages;
        }

        public PackageGetResp PackagesList()
        {
            var response = _client.PackagesList();
            return response;
        }

        public IEnumerable<Package> FindPackage(PackageFind package)
        {
            return PackageGet(PackageFind(package));
        }
        #endregion

        #region PackageType
        public PackageTypeCreateResp PackageTypeCreate(PackageType package)
        {
            var packageCreateRq = new PackageTypeCreateReq()
            {
                PackageType = package
            };

            var resp = _client.PackageTypeCreate(packageCreateRq);

            return resp;
        }
        public void PackageTypeUpdate(PackageType package)
        {
            var packageUpdateRq = new PackageTypeUpdateReq()
            {
                PackageType = package
            };

            var resp = _client.PackageTypeUpdate(packageUpdateRq);

        }
        public IEnumerable<int> PackageTypeFind(PackageTypeFind PackageType)
        {
            var request = new PackageTypeFindReq()
            {
                PackageTypeFind = PackageType
            };

            var response = _client.PackageTypeFind(request);

            return response.IDs;
        }
        public IEnumerable<PackageType> PackageTypeGet(IEnumerable<int> PackageTypes)
        {
            if (PackageTypes == null)
                return new List<PackageType>(); ;

            var request = new PackageTypeGetReq()
            {
                IDs = PackageTypes.ToArray()
            };

            var response = _client.PackageTypeGet(request);

            return response.PackageTypes;
        }
        public IEnumerable<PackageType> FindPackageType(PackageTypeFind PackageType)
        {
            return PackageTypeGet(PackageTypeFind(PackageType));
        }

        public PackageTypeGetResp PackageTypesList()
        {
            var response = _client.PackageTypesList();
            return response;
        }

        #endregion

        #region PgmBdlPckComponent
        public PgmBdlPckComponentCreateResp PgmBdlPckComponentCreate(PgmBdlPckComponent pgmBdl)
        {
            var pgmBdlCreateRq = new PgmBdlPckComponentCreateReq()
            {
                PgmBdlPckComponent = pgmBdl
            };

            var resp = _client.PgmBdlPckComponentCreate(pgmBdlCreateRq);

            return resp;
        }
        public IEnumerable<int> PgmBdlPckComponentFind(PgmBdlPckComponentFind PgmBdlPckComponent)
        {
            var request = new PgmBdlPckComponentFindReq()
            {
                PgmBdlPckComponentFind = PgmBdlPckComponent
            };

            var response = _client.PgmBdlPckComponentFind(request);

            return response.IDs;
        }
        public IEnumerable<PgmBdlPckComponent> PgmBdlPckComponentGet(IEnumerable<int> PgmBdlPckComponents)
        {
            if (PgmBdlPckComponents == null || PgmBdlPckComponents.ToList().Count == 0)
                return new List<PgmBdlPckComponent>();

            var request = new PgmBdlPckComponentGetReq()
            {
                IDs = PgmBdlPckComponents.ToArray()
            };

            var response = _client.PgmBdlPckComponentGet(request);

            return response.PgmBdlPckComponents;
        }
        public IEnumerable<PgmBdlPckComponent> PgmBdlPckComponentGet1(PgmBdlPckComponentFind PgmBdlPckComponent)
        {
            try
            {
                if (PgmBdlPckComponent == null)
                    return new List<PgmBdlPckComponent>();

                var request = new PgmBdlPckComponentFindReq()
                {
                    PgmBdlPckComponentFind = PgmBdlPckComponent
                };

                var response = _client.PgmBdlPckComponentGet1(request);


                return response.PgmBdlPckComponents;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static IEnumerable<PgmBdlPckComponent> FindPgmBdlPckComponent(PgmBdlPckComponentFind PgmBdlPckComponent)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.PgmBdlPckComponentGet(proxy.PgmBdlPckComponentFind(PgmBdlPckComponent));
            }
        }
        public void PgmBdlPckComponentUpdate(PgmBdlPckComponent pgmBdlPckComponent)
        {
            var pgmBdlPckComponentUpdateRq = new PgmBdlPckComponentUpdateReq()
            {
                PgmBdlPckComponent = pgmBdlPckComponent
            };

            var resp = _client.PgmBdlPckComponentUpdate(pgmBdlPckComponentUpdateRq);

        }
        public Dictionary<string, string> PkgCompContractGet()
        {
            var response = _client.PkgCompContractGet();

            return response.PgmBdlPckComponents;
        }
        public Dictionary<string, string> BdlPkgGet()
        {
            var response = _client.BdlPkgGet();

            return response.PgmBdlPckComponents;
        }
        public IEnumerable<MISMMandatoryComponents> MISMMandatoryComponentGet()
        {
            var response = _client.MISMMandatoryComponentGet();

            return response.MISMComps;
        }
        #endregion

        #region Model Group Model
        public void ModelGroupModelCreate(ModelGroupModel modelGroupModel)
        {
            var modelGroupModelRq = new ModelGroupModelCreateReq()
            {
                ModelGroupModel = modelGroupModel
            };

            var resp = _client.ModelGroupModelCreate(modelGroupModelRq);
        }
        public void ModelGroupModelUpdate(ModelGroupModel modelGroupModel)
        {
            var modelGroupModelUpdateRq = new ModelGroupModelUpdateReq()
            {
                ModelGroupModel = modelGroupModel
            };

            var resp = _client.ModelGroupModelUpdate(modelGroupModelUpdateRq);
        }
        public IEnumerable<int> ModelGroupModelFind(ModelGroupModelFind mgpm)
        {
            var request = new ModelGroupModelFindReq()
            {
                ModelGroupModelFind = mgpm
            };

            var response = _client.ModelGroupModelFind(request);

            return response.IDs;
        }
        public IEnumerable<ModelGroupModel> ModelGroupModelGet(IEnumerable<int> mgpmIDs)
        {
            if (mgpmIDs == null)
                return new List<ModelGroupModel>();

            var request = new ModelGroupModelGetReq()
            {
                IDs = mgpmIDs.ToArray()
            };

            var response = _client.ModelGroupModelGet(request);

            return response.ModelGroupModels;
        }
        public static IEnumerable<ModelGroupModel> FindModelGroupModel(ModelGroupModelFind mgpm)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(mgpm));
            }
        }
        #endregion

        #region Item Price
        public ItemPriceCreateResp ItemPriceCreate(ItemPrice itemPrice)
        {
            var itemPriceCreateRq = new ItemPriceCreateReq()
            {
                ItemPrice = itemPrice
            };

            var resp = _client.ItemPriceCreate(itemPriceCreateRq);

            return resp;
        }
        public IEnumerable<int> ItemPriceFind(ItemPriceFind ItemPrice)
        {
            var request = new ItemPriceFindReq()
            {
                ItemPriceFind = ItemPrice
            };

            var response = _client.ItemPriceFind(request);
            return response.IDs;
        }

        public IEnumerable<int> ItemPriceFindForMultipleContratsAndPlans(ItemPriceFind ItemPrice)
        {
            var request = new ItemPriceFindReq()
            {
                ItemPriceFind = ItemPrice
            };
            var response = _client.ItemPriceFindForMultipleContratsAndPlans(request);
            return response.IDs;
        }

        public DAL.Models.ItemPrice ItemPriceGet(int ItemPriceID)
        {
            var ItemPrices = ItemPriceGet(new int[] { ItemPriceID });
            return ItemPrices == null ? null : ItemPrices.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.ItemPrice> ItemPriceGet(IEnumerable<int> ItemPrices)
        {
            if (ItemPrices == null)
                return new List<ItemPrice>(); ;

            var request = new ItemPriceGetReq()
            {
                IDs = ItemPrices.ToArray()
            };

            var response = _client.ItemPriceGet(request);

            return response.ItemPrices;
        }
        public static IEnumerable<DAL.Models.ItemPrice> FindItemPrice(ItemPriceFind ItemPrice)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.ItemPriceGet(proxy.ItemPriceFind(ItemPrice));
            }
        }
        public void ItemPriceUpdate(ItemPrice itemPrice)
        {
            var itemPriceUpdateRq = new ItemPriceUpdateReq()
            {
                ItemPrice = itemPrice
            };

            var resp = _client.ItemPriceUpdate(itemPriceUpdateRq);

        }
        #endregion

        #region Advance & Deposit Price for Device
        //added Deepika
        public DAL.Models.AdvDepositPrice AdvDepositPricesGet(int AdvDepositPriceID)
        {
            var AdvDepositPrices = AdvDepositPricesGet(new int[] { AdvDepositPriceID });
            return AdvDepositPrices == null ? null : AdvDepositPrices.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.AdvDepositPrice> AdvDepositPricesGet(IEnumerable<int> AdvDepositPrices)
        {
            if (AdvDepositPrices == null)
                return new List<AdvDepositPrice>(); ;

            var request = new AdvDepositPriceGetReq()
            {
                IDs = AdvDepositPrices.ToArray()
            };

            var response = _client.AdvDepositPriceGet(request);

            return response.AdvDepositPrices;
        }

        public IEnumerable<DAL.Models.AdvDepositPrice> AdvDepositPrices1(IEnumerable<string> advDepositPriceRequest)
        {
            if (advDepositPriceRequest == null)
                return new List<AdvDepositPrice>();
            var requetFrame = new AdvDepositPriceGetReqStr()
            {
                IDs = advDepositPriceRequest.ToArray()
            };
            var resp = _client.AdvDepositPriceGetBySP(requetFrame);
            return resp.AdvDepositPrices;
        }

        //end region
        #endregion

        #region Property
        //public PropertyCreateResp PropertyCreate(Property Property)
        //{
        //    var PropertyCreateRq = new PropertyCreateReq()
        //    {
        //        Property = Property
        //    };

        //    var resp = _client.PropertyCreate(PropertyCreateRq);

        //    return resp;
        //}
        //public void PropertyUpdate(Property Property)
        //{
        //    var PropertyUpdateRq = new PropertyUpdateReq()
        //    {
        //        Property = Property
        //    };

        //    var resp = _client.PropertyUpdate(PropertyUpdateRq);

        //}
        public IEnumerable<int> PropertyFind(PropertyFind Property)
        {
            var request = new PropertyFindReq()
            {
                PropertyFind = Property
            };

            var response = _client.PropertyFind(request);
            return response.IDs;
        }
        public DAL.Models.Property PropertyGet(int PropertyID)
        {
            var Properties = PropertyGet(new int[] { PropertyID });
            return Properties == null ? null : Properties.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.Property> PropertyGet(IEnumerable<int> Properties)
        {
            if (Properties == null)
                return new List<Property>(); ;

            var request = new PropertyGetReq()
            {
                IDs = Properties.ToArray()
            };

            var response = _client.PropertyGet(request);

            return response.Properties;
        }

        public PropertyGetResp PropertyList()
        {

            var response = _client.PropertyList();

            return response;
        }
        public static IEnumerable<DAL.Models.Property> FindProperty(PropertyFind Property)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.PropertyGet(proxy.PropertyFind(Property));
            }
        }
        #endregion

        #region Property Value
        //public PropertyValueCreateResp PropertyValueCreate(PropertyValue PropertyValue)
        //{
        //    var PropertyValueCreateRq = new PropertyValueCreateReq()
        //    {
        //        PropertyValue = PropertyValue
        //    };

        //    var resp = _client.PropertyValueCreate(PropertyValueCreateRq);

        //    return resp;
        //}
        //public void PropertyValueUpdate(PropertyValue PropertyValue)
        //{
        //    var PropertyValueUpdateRq = new PropertyValueUpdateReq()
        //    {
        //        PropertyValue = PropertyValue
        //    };

        //    var resp = _client.PropertyValueUpdate(PropertyValueUpdateRq);

        //}
        public IEnumerable<int> PropertyValueFind(PropertyValueFind PropertyValue)
        {
            var request = new PropertyValueFindReq()
            {
                PropertyValueFind = PropertyValue
            };

            var response = _client.PropertyValueFind(request);
            return response.IDs;
        }
        public DAL.Models.PropertyValue PropertyValueGet(int PropertyValueID)
        {
            var PropertyValues = PropertyValueGet(new int[] { PropertyValueID });
            return PropertyValues == null ? null : PropertyValues.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.PropertyValue> PropertyValueGet(IEnumerable<int> PropertyValues)
        {
            if (PropertyValues == null)
                return new List<PropertyValue>(); ;

            var request = new PropertyValueGetReq()
            {
                IDs = PropertyValues.ToArray()
            };

            var response = _client.PropertyValueGet(request);

            return response.PropertyValues;
        }
        public static IEnumerable<DAL.Models.PropertyValue> FindPropertyValue(PropertyValueFind PropertyValue)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.PropertyValueGet(proxy.PropertyValueFind(PropertyValue));
            }
        }
        #endregion

        #region Plan Property
        //public PlanPropertyCreateResp PlanPropertyCreate(PlanProperty PlanProperty)
        //{
        //    var PlanPropertyCreateRq = new PlanPropertyCreateReq()
        //    {
        //        PlanProperty = PlanProperty
        //    };

        //    var resp = _client.PlanPropertyCreate(PlanPropertyCreateRq);

        //    return resp;
        //}
        //public void PlanPropertyUpdate(PlanProperty PlanProperty)
        //{
        //    var PlanPropertyUpdateRq = new PlanPropertyUpdateReq()
        //    {
        //        PlanProperty = PlanProperty
        //    };

        //    var resp = _client.PlanPropertyUpdate(PlanPropertyUpdateRq);

        //}
        public IEnumerable<int> PlanPropertyFind(PlanPropertyFind PlanProperty)
        {
            var request = new PlanPropertyFindReq()
            {
                PlanPropertyFind = PlanProperty
            };

            var response = _client.PlanPropertyFind(request);
            return response.IDs;
        }
        public DAL.Models.PlanProperty PlanPropertyGet(int PlanPropertyID)
        {
            var PlanProperties = PlanPropertyGet(new int[] { PlanPropertyID });
            return PlanProperties == null ? null : PlanProperties.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.PlanProperty> PlanPropertyGet(IEnumerable<int> PlanProperties)
        {
            if (PlanProperties == null)
                return new List<PlanProperty>(); ;

            var request = new PlanPropertyGetReq()
            {
                IDs = PlanProperties.ToArray()
            };

            var response = _client.PlanPropertyGet(request);

            return response.PlanProperties;
        }
        public static IEnumerable<DAL.Models.PlanProperty> FindPlanProperty(PlanPropertyFind PlanProperty)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.PlanPropertyGet(proxy.PlanPropertyFind(PlanProperty));
            }
        }
        #endregion

        #region Device Property
        //public DevicePropertyCreateResp DevicePropertyCreate(DeviceProperty DeviceProperty)
        //{
        //    var DevicePropertyCreateRq = new DevicePropertyCreateReq()
        //    {
        //        DeviceProperty = DeviceProperty
        //    };

        //    var resp = _client.DevicePropertyCreate(DevicePropertyCreateRq);

        //    return resp;
        //}
        //public void DevicePropertyUpdate(DeviceProperty DeviceProperty)
        //{
        //    var DevicePropertyUpdateRq = new DevicePropertyUpdateReq()
        //    {
        //        DeviceProperty = DeviceProperty
        //    };

        //    var resp = _client.DevicePropertyUpdate(DevicePropertyUpdateRq);

        //}
        public IEnumerable<int> DevicePropertyFind(DevicePropertyFind DeviceProperty)
        {
            var request = new DevicePropertyFindReq()
            {
                DevicePropertyFind = DeviceProperty
            };

            var response = _client.DevicePropertyFind(request);
            return response.IDs;
        }
        public DAL.Models.DeviceProperty DevicePropertyGet(int DevicePropertyID)
        {
            var DeviceProperties = DevicePropertyGet(new int[] { DevicePropertyID });
            return DeviceProperties == null ? null : DeviceProperties.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.DeviceProperty> DevicePropertyGet(IEnumerable<int> DeviceProperties)
        {
            if (DeviceProperties == null)
                return new List<DeviceProperty>(); ;

            var request = new DevicePropertyGetReq()
            {
                IDs = DeviceProperties.ToArray()
            };

            var response = _client.DevicePropertyGet(request);

            return response.DeviceProperties;
        }
        public static IEnumerable<DAL.Models.DeviceProperty> FindDeviceProperty(DevicePropertyFind DeviceProperty)
        {
            using (var proxy = new CatalogServiceProxy())
            {
                return proxy.DevicePropertyGet(proxy.DevicePropertyFind(DeviceProperty));
            }
        }
        #endregion

        #region BrandArticle

        public ArticleIDGetResp GetArticleID(int p)
        {
            var resp = _client.GetArticleID(p);
            return resp;
        }
        public ArticleIDGetResp GetArticleIDs(List<int> p)
        {
            var resp = _client.GetArticleIDs(p.ToArray());
            return resp;
        }
        public ArticleIDGetResp GetAllArticleIDs()
        {
            var resp = _client.GetAllArticleIDs();
            return resp;
        }


        public DAL.Models.BrandArticle BrandArticleImageGet(int ModelImageID)
        {
            var ModelImages = BrandArticleModelImageGet(new int[] { ModelImageID });
            return ModelImages == null ? null : ModelImages.SingleOrDefault();
        }

        public IEnumerable<DAL.Models.BrandArticle> BrandArticleModelImageGet(IEnumerable<int> ModelImages)
        {
            if (ModelImages == null)
                return new List<BrandArticle>(); ;

            var request = new BrandArticleModelImageGetReq()
            {
                IDs = ModelImages.ToArray()
            };

            var response = _client.BrandArticleModelImageGet(request);

            return response.BarndArticleImages;
        }

        public int GetContractDuration(int Id)
        {
            var resp = _client.GetContractDuration(Id);
            return resp;
        }

        public string GetUomId(string ArticleId, int ContractDuration, string contractID, string offerName)
        {
            var resp = _client.GetUomId(ArticleId, ContractDuration, contractID, offerName);
            return resp;
        }
        #endregion

        //public IMPOSDetailsResp GetIMOPSDetails(int RegID)
        //{
        //    IMPOSDetailsResp objIMPOSDetailsResp = _client.GetIMOPSDetails(RegID);
        //    return objIMPOSDetailsResp;
        //}

        public WhiteListDetailsResp GetWhilteListDetails(string UserICNO)
        {
            WhiteListDetailsResp objIWhiteListDetailsResp = _client.getWhilstDescreption(UserICNO);
            return objIWhiteListDetailsResp;
        }

        #region  Added by VLT for SimModels on 09 Apr 2013
        public SimModelTypeGetResp GetSIMModels()
        {
            SimModelTypeGetResp objSimModelsResp = _client.getSIMModels();
            return objSimModelsResp;
        }
        #endregion

        #region  Added by VLT for SimReplacement on 17 June 2013
        public SimReplacementReasonGetResp GetSimReplacementReasons(bool accType)
        {
            SimReplacementReasonGetResp objSimReplacementResp = _client.GetSimReplacementReason(accType);
            return objSimReplacementResp;
        }

        public SimReplacementReasonResp GetSimReplacementReasonDetails(int ReasonId)
        {
            SimReplacementReasonResp objSimReplacementResp = _client.GetSimReplacementReasonDetails(ReasonId);
            return objSimReplacementResp;
        }


        public SimModels GetSimModelDetails(int id, int articalId)
        {
            SimModels objSimModels = _client.GetSimModelDetails(id, articalId);
            return objSimModels;
        }

        #endregion


        public List<int> GETOfferID(string articleId)
        {

            List<int> offerId = _client.GetOfferID(articleId).ToList();

            return offerId;
        }

        public MOCOfferDetailsRes GETMOCOfferDetails(int offerId)
        {

            var res = _client.GetMOCOfferDetails(offerId);

            return res;
        }

        #region"Get offer Name by Id by VLT"
        /// <summary>
        /// Method to Get Offer Name
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>string</returns>
        public string GetOfferNameById(int offerID)
        {
            string offerName = _client.GetOfferNameById(offerID);

            return offerName;
        }
        #endregion

        public List<int> GetAllDependentComponents(int componentId, int planId)
        {
            var resp = _client.GetAllDependentComponents(componentId, planId).ToList();
            return resp;
        }

        public List<int> GetAllSmartDependentComponents(int componentId, int planId)
        {
            var resp = _client.GetAllSmartDependentComponents(componentId, planId).ToList();
            return resp;
        }

        public List<int> GetAllDependentComponents(List<int> componentId, int planId)
        {
            var resp = _client.GetAllDependentComponentsList(componentId.ToArray(), planId).ToList();
            return resp;
        }

        public lnkofferuomarticlepriceResp GetOffersfromUOMArticle(string ArticleId, string planID, string contractID, string MOCStatus)
        {
            //var resp = _client.GetOffersfromUOMArticle(ArticleId, planID, contractID, MOCStatus);
            var resp = WebHelper.Instance.GetOffersfromUOMArticleNew(ArticleId, planID, contractID, MOCStatus);
            return resp;
        }

        public lnkofferuomarticlepriceResp GetOffersfromUOMArticleNew(string ArticleId, string PkgKenanID, string kenanIds, string MOCStatus, int AcctCtg, int MktCode)
        {
            var resp = _client.GetOffersfromUOMArticleNew(ArticleId, PkgKenanID, kenanIds, MOCStatus, AcctCtg, MktCode);
            return resp;
        }

        public List<lnkofferuomarticleprice> GetOffersfromUOMArticle2(string ArticleId, string PackageKenanId, string MOCStatus, string contractKenanIds)
        {
            //var resp = _client.GetOffersfromUOMArticle2(ArticleId, PackageKenanId, MOCStatus, contractKenanIds);
            //List<lnkofferuomarticleprice> resp1 = new List<lnkofferuomarticleprice>();
            //if (!ReferenceEquals(resp, null))
            //{
            //    if (resp.values.Count() > 0)
            //    {
            //        for (int i = 0; i < resp.values.Count(); i++)
            //        {
            //            lnkofferuomarticleprice item = new lnkofferuomarticleprice();
            //            item.Id = resp.values[i].Id;
            //            item.ArticleId = resp.values[i].ArticleId;
            //            item.KenanComponent1Id = resp.values[i].KenanComponent1Id;
            //            item.KenanPackageId = resp.values[i].KenanPackageId;
            //            item.OfferName = resp.values[i].OfferName;
            //            item.Price = resp.values[i].Price;
            //            item.ToDateTime = resp.values[i].ToDateTime;
            //            item.UOMCode = resp.values[i].UOMCode;
            //            resp1.Add(item);
            //        }
            //    }
            //}
            //return resp1;

            var offers = WebHelper.Instance.GetOffersfromUOMArticleNew(ArticleId, PackageKenanId, contractKenanIds, MOCStatus);

            //bool ok = Util.ConvertObjectToXml(resp1) == Util.ConvertObjectToXml(offers.values.ToList());
            return offers.values.ToList();
        }

        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
        public List<string> GetPackagesForArticleId(string articleId, string mocStatus)
        {
            List<string> objPackages = new List<string>();
            objPackages = _client.GetPackagesForArticleId(articleId, mocStatus).ToList();
            return objPackages;
        }

        public List<string> GetContractsForArticleId(string articleId, string planID, string mocStatus)
        {
            List<string> objContracts = new List<string>();
            objContracts = _client.GetContractsForArticleId(articleId, planID, mocStatus).ToList();
            return objContracts;
        }
        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid

        //Added by VLT on 17 May 2013 to get device image details dynamically

        public GetDeviceImageDetailsResp GetDeviceImageDetails(string articleId, int modelId)
        {
            GetDeviceImageDetailsResp objGetDeviceImageDetailsResp = new GetDeviceImageDetailsResp();
            objGetDeviceImageDetailsResp = _client.GetDeviceImageDetails(articleId, modelId);
            return objGetDeviceImageDetailsResp;
        }
        //Pavan

        public DiscountPricesRes GetDiscountPriceDetails(int Modelid, int planid, int contractid, int Dataplanid)
        {
            DiscountPricesRes objGetDeviceImageDetailsResp = new DiscountPricesRes();
            objGetDeviceImageDetailsResp = _client.GetDiscountPrices(Modelid, planid, contractid.ToString(), Dataplanid);
            return objGetDeviceImageDetailsResp;
        }
        //Pavan

        //Added by VLT on 17 May 2013 to get device image details dynamically


        #region"Get CONTRACT Id by VLT"
        /// <summary>
        /// Method CONTRACT ID
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>INT    </returns>
        public List<int> GetContractIdByRegID(int regID)
        {
            List<int> contractID = _client.GetContractIdByRegID(regID).ToList();

            return contractID;
        }
        public int GetContractIdByRegIDSeco(int regID)
        {
            int contractID = _client.GetContractIdByRegIDSeco(regID);

            return contractID;
        }
        public int GetContractIdByRegSuppLineID(int regSuppLineID)
        {
            int contractID = _client.GetContractIdByRegSuppLineID(regSuppLineID);

            return contractID;
        }


        #endregion

        #region"Get CONTRACT value by VLT" Added by VLT 06 May 2013
        /// <summary>
        /// Method CONTRACT Value
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>String    </returns>
        public string GetContractByCode(string compCode)
        {
            string contractValue = _client.GetContractByCode(compCode);

            return contractValue;
        }
        #endregion


        #region ExtraTen

        public bool SaveAddRemoveVAS(AddRemoveVASReq oReq)
        {
            long id = 0;
            id = _client.SaveAddRemoveVAS(oReq);

            if (id > 0)
                return true;
            else
                return false;

        }

        public bool UpdateAddRemoveVAS(AddRemoveVASReq oReq)
        {
            int id = 0;
            id = _client.UpdateAddRemoveVAS(oReq);

            if (id > 0)
                return true;
            else
                return false;
        }

        public bool DeleteAddRemoveVasComponent(int Id)
        {
            int id = 0;
            id = _client.DeleteAddRemoveVasComponent(Id);

            if (id > 0)
                return true;
            else
                return false;
        }


        public bool DeleteAddRemoveVasComponents(int regId)
        {
            int id = 0;
            id = _client.DeleteAddRemoveVasComponents(regId);

            if (id > 0)
                return true;
            else
                return false;
        }



        public AddRemoveVASResp GetAddRemoveVASComponents(int regId)
        {
            AddRemoveVASResp response = new AddRemoveVASResp();
            response = _client.GetAddRemoveVASComponents(regId);

            return response;
        }

        #endregion


        #region CRP

        public IEnumerable<PgmBdlPckComponent> PgmBdlPckComponentGetForSmartByKenanCode(IEnumerable<string> PgmBdlPckComponents)
        {
            if (PgmBdlPckComponents == null || PgmBdlPckComponents.ToList().Count == 0)
                return new List<PgmBdlPckComponent>();

            var PgmBdlPckComponentSmartList = MasterDataCache.Instance.PackageComponents.Where(e => PgmBdlPckComponents.Contains(e.KenanCode) && e.Active == true).ToList();


            var listComponent = this.ContractComponentGet();

            if (PgmBdlPckComponentSmartList != null && PgmBdlPckComponentSmartList.Count > 0)
            {
                foreach (var item in PgmBdlPckComponentSmartList)
                {
                    item.Description = listComponent.FirstOrDefault(a => a.Code == item.Code) == null ? item.Description : listComponent.FirstOrDefault(a => a.Code == item.Code).Description;
                }
            }

            return PgmBdlPckComponentSmartList;
        }

        public List<ModelPackageInfo> GetModelPackageInfo_smart(int ModelId, int packageId = 0, string KenanCode = "", bool isSmart = false)
        {

            List<ModelPackageInfo> results = _client.GetModelPackageInfo_smart(ModelId, packageId, KenanCode, isSmart).ToList();

            return results;
        }

        //GetSelectedComponentsByRegId
        public List<RegSmartComponents> GetSelectedComponents(string regId)
        {
            List<RegSmartComponents> listRegSmartComponents = new List<RegSmartComponents>();

            try
            {
                listRegSmartComponents = _client.GetSelectedComponents(regId).ToList(); // _client.GetSelectedComponents(regId).ToList();

            }
            catch (Exception ex)
            {

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }

            return listRegSmartComponents;

        }

        //GetSelectedComponentsByRegId
        public List<RegSmartComponents> GetSelectedComponentsForCRPPlan(string regId)
        {
            List<RegSmartComponents> listRegSmartComponents = new List<RegSmartComponents>();

            try
            {
                listRegSmartComponents = _client.GetSelectedComponentsForCRPPlan(regId).ToList();

            }
            catch (Exception ex)
            {

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }

            return listRegSmartComponents;

        }

        public List<Brand> GetCRPBrands(string type)
        {
            List<Brand> objBrands = new List<Brand>();
            objBrands = _client.GetCRPBrands(type).ToList();
            return objBrands;
        }


        public List<Model> GetCRPModels(int Brandid, string type)
        {
            List<Model> objModelExtend = new List<Model>();
            objModelExtend = _client.GetCrpModels(Brandid, type).ToList();
            return objModelExtend;
        }

        public List<ModelGroup> GetCRPPackages(int Modelid, string type)
        {
            List<ModelGroup> objPackagesExtend = new List<ModelGroup>();
            objPackagesExtend = _client.GetCrpPackages(Modelid, type).ToList();
            return objPackagesExtend;
        }
        public IEnumerable<string> ExtendPlanContract(ContractPlans Extend)
        {
            var request = new ExtendPlanFindReq()
            {
                ContractPlan = Extend
            };
            var response = _client.ExtendPlan(request);
            return response.IDs;
        }

        public List<int> GetExtendedPackages(int Modelid)
        {
            List<int> objPackagesExtend = new List<int>();
            objPackagesExtend = _client.GetExtendedPackages(Modelid).ToList();
            return objPackagesExtend;
        }

        public List<Model> GetExtendedModelsDevice(int brandid, string type)
        {
            List<Model> objPackagesExtend = new List<Model>();
            objPackagesExtend = _client.GetExtendedModelsDevice(brandid, type).ToList();
            return objPackagesExtend;
        }


        public List<DAL.Models.ContractDataplan> GetContractDataPlan(int DataPlanID)
        {
            List<DAL.Models.ContractDataplan> objPackagesExtend = new List<DAL.Models.ContractDataplan>();
            objPackagesExtend = _client.GetContractDataPlan(DataPlanID).ToList();
            //objPackagesExtend = _client.getcon(brandid, type).ToList();
            return objPackagesExtend;
        }

        public IEnumerable<string> ExtendContract(ContractBundleModels Extend)
        {
            var request = new ExtendBrandFindReq()
            {

                ContractBundleModels = Extend
            };
            var response = _client.ExtendBrand(request);
            return response.IDs;

        }
        public CRPIMPOSDetailsResp GetIMOPSDetails(int RegID)
        {
            CRPIMPOSDetailsResp objIMPOSDetailsResp = _client.GetIMOPSDetails(RegID);
            return objIMPOSDetailsResp;
        }

        public List<IMPOSDetails> GetPosKeyandOrgIDNew(int RegID)
        {
            List<IMPOSDetails> lst = new List<IMPOSDetails>();
            lst =   _client.GetPosKeyandOrgIDNew(RegID).ToList();
          return lst;
        }
        
        public IMPOSSpecDetails GetIMPOSSpecDetails(string PosKey)
        {
            IMPOSSpecDetails objIMPOSSpecDetails = new IMPOSSpecDetails();
            objIMPOSSpecDetails = _client.GetIMPOSSpecDetails(PosKey);

            return objIMPOSSpecDetails;
        }

        public IMPOSDetails getIMPOSDetails_Smart(int RegID, string OrganisationId, string strSIMType)
        {
            IMPOSDetails objIMPOSDetails = new IMPOSDetails();
            objIMPOSDetails = _client.getIMPOSDetails_Smart(RegID, OrganisationId, strSIMType);

            return objIMPOSDetails;
        }

        public int GetArticleIdById(int SIMId)
        {
            int value = 0;
            value = _client.GetArticleIdById(SIMId);

            return value;
        }

        public int UpdateIMPOSDetails(int RegID, string IMPOSFileName, int IMPOSStatus, string strSIMType = "P")
        {
            int value = 0;
            value = _client.UpdateIMPOSDetails(RegID, IMPOSFileName, IMPOSStatus, strSIMType);

            return value;
        }

        public IMPOSDetails GetIMPOSDetails(int RegID, string OrganisationId, string strSIMType)
        {
            IMPOSDetails objIMPOSDetails = new IMPOSDetails();
            objIMPOSDetails = _client.GetIMPOSDetails(RegID, OrganisationId, strSIMType);

            return objIMPOSDetails;
        }

        public string GetUOMCodeBySIMModelID(int SIMId)
        {
            string value = string.Empty;
            value = _client.GetUOMCodeBySIMModelID(SIMId);

            return value;
        }

        public IMPOSDetails getIMPOSDetailsForPlanOnly(int RegID, string OrganisationId, string strSIMType = "P")
        {
            var resp = _client.getIMPOSDetailsForPlanOnly(RegID, OrganisationId, strSIMType);

            return resp;
        }
        //public IMPOSDetails GetIMPOSDetails(int RegID, string OrganisationId, string strSIMType)
        //{
        //    IMPOSDetails objIMPOSDetails = new IMPOSDetails();
        //    objIMPOSDetails = _client.getIMPOSDetails1
        //}

        #endregion

        #region AddContract
        /*
        public List<Brand> GetBrandsForPackage(string packageId, string planType)
        {
            List<Brand> objBrands = new List<Brand>();
            Brand[] brands = _client.GetBrandsForPackage(packageId, planType);
            if (brands != null)
                objBrands = _client.GetBrandsForPackage(packageId, planType).ToList();
            return objBrands;
        }

        public List<Model> GetModelsForPackage(int Brandid, string packageId, string planType)
        {
            List<Model> objModelExtend = new List<Model>();
            objModelExtend = _client.GetModelsForPackage(Brandid, packageId, planType).ToList();
            return objModelExtend;
        }
        */
        public List<BrandArticle> GetDeviceListForPackage(string packageId, string mktCode, string acctCategory)
        {
            List<BrandArticle> objModelExtend = new List<BrandArticle>();
            objModelExtend = _client.GetDeviceListForPackage(packageId, mktCode, acctCategory).ToList();
            return objModelExtend;
        }

        public List<Model> GetModelsForExtenstionForPackage(int Brandid, string packageId, string planType)
        {
            List<Model> objModelExtend = new List<Model>();
            objModelExtend = _client.GetModelsForExtenstionForPackage(Brandid, packageId, planType).ToList();
            return objModelExtend;
        }

        #endregion AddContract

        #region Devices for supplines

        public List<Brand> GetBrandsForSuppLines()
        {
            List<Brand> objBrands = new List<Brand>();
            objBrands = _client.GetBrandsForSuppLines().ToList();
            return objBrands;
        }

        public List<Model> GetModelsForSupplines(int Brandid)
        {
            List<Model> objModelExtend = new List<Model>();
            objModelExtend = _client.GetModelsForSupplines(Brandid).ToList();
            return objModelExtend;
        }

        public List<Model> GetModelsForExtenstionForSupplines(int Brandid)
        {
            List<Model> objModelExtend = new List<Model>();
            objModelExtend = _client.GetModelsForExtenstionForSupplines(Brandid).ToList();
            return objModelExtend;
        }

        public List<ModelGroup> GetModelGroupsForSupplines(int Modelid)
        {
            List<ModelGroup> objPackagesExtend = new List<ModelGroup>();
            objPackagesExtend = _client.GetModelGroupsForSupplines(Modelid).ToList();
            return objPackagesExtend;
        }

        public List<int> GetExtendedPackagesForSupplines(int Modelid)
        {
            List<int> objPackagesExtend = new List<int>();
            objPackagesExtend = _client.GetExtendedPackagesForSupplines(Modelid).ToList();
            return objPackagesExtend;
        }
        #endregion

        #region Restricted kenan codes

        public List<RestrictedComponent> GetAllRestrictedComponents()
        {
            var resp = _client.GetAllRestrictedComponents();
            List<RestrictedComponent> restrictComps = new List<RestrictedComponent>();

            if (resp != null)
                restrictComps = resp.RestrictedComponents.ToList();

            return restrictComps;
        }

        #endregion

        //Changes related to Multiple Contracts CR changed by code--Chetan/ db--Pavan
        public List<PlanPrices> GetPlanPrice(int _modelID, string _uom)
        {
            List<PlanPrices> PlanPrices = _client.GetPlanPrice(_modelID, _uom).ToList();
            return PlanPrices;
        }

        public IEnumerable<PgmBdlPckComponent> PgmBdlPckComponentGet2(int componentId, int planId)
        {

            var response = _client.PgmBdlPckComponentGet2(componentId, planId);

            return response.PgmBdlPckComponents;
        }

        public IEnumerable<VoiceContractDetails> GetVoiceAddendums(IEnumerable<string> contractKenanCodes)
        {
            if (contractKenanCodes == null)
                return new List<VoiceContractDetails>();
            var requetFrame = new VoiceContractDetailsReqStr()
            {
                IDs = contractKenanCodes.ToArray()
            };
            var resp = _client.GetVoiceAddedendums(requetFrame);
            return resp.lstVoiceContractDetails;
        }
        public List<string> GetDataContractByRegId(int regID)
        {
            List<string> contractID = _client.GetDataContractByRegId(regID).ToList();

            return contractID;
        }

        //CR for auto selection for Discount/Promotion component implementation by chetan
        public string[] GetKenanCodeForReassign()
        {
            var resp = _client.GetKenanCodeForReassign();
            return resp;
        }

        public Dictionary<string, string> GetDCGreater2GB()
        {
            Dictionary<string, string> dataComponents = new Dictionary<string, string>();
            dataComponents = _client.RetriveDCGreater2GB();
            return dataComponents;
        }

        public Dictionary<string, string> FindPackageOnKenanCode(string Kenancode)
        {
            var response = _client.FindPackageOnKenanCode(Kenancode);
            return response;
        }
        #region  get sim types
        public SimCardTypesGetResp GetSIMCardTypes()
        {
            SimCardTypesGetResp objSimModelsResp = _client.GetSIMCardTypes();
            return objSimModelsResp;
        }
        #endregion

        #region  get Capacity
        public DeviceCapacityGetResp getDeviceCapacity()
        { 
        DeviceCapacityGetResp objDeviceCapacityResp = _client.GetDeviceCapacity();
            return objDeviceCapacityResp;
        }
        #endregion

        #region  get Device Type
        public DeviceTypesGetResp getDeviceType()
        {
            DeviceTypesGetResp objDeviceTypeResp = _client.GetDeviceType();
            return objDeviceTypeResp;
        }
        #endregion

        #region  get Price Range
        public PriceRangeGetResp getPriceRange()
        {
            PriceRangeGetResp objPriceRangeGetResp = _client.GetPriceRange();
            return objPriceRangeGetResp;
        }
        #endregion



        #region  get sim model based on article & sim type
        public List<SimModels> GetModelName(int ArticleId, string simCardType)
        {
            List<SimModels> simModels = _client.GetModelName(ArticleId, simCardType).ToList();
            return simModels;
        }
        #endregion

        public List<lnkAcctMktCodePackages> GetPlansbyMarketandAccountCategory(int AcctCategory, int MarketCode)
        {
            return _client.GetPlansbyMarketandAccountCategory(AcctCategory, MarketCode).ToList();
        }

		public List<lnkAcctMktCodeOffers> GetOffersbyMarketandAccountCategory(int AcctCategory, int MarketCode)
		{
			return _client.GetOffersbyMarketandAccountCategory(AcctCategory, MarketCode).ToList();
		}

        public List<Region> GetRegion()
        {
            return _client.GetRegion().ToList();
        }

        public List<refLovList> GetLovList(string lovtype)
        {
            var response = _client.GetLovList(lovtype).ToList();
            return response;
        }

		public List<refLovList> GetAllLovList()
		{
			var response = _client.GetAllLovList().ToList();
			return response;
		}

		public List<DeviceFinancingInfo> GetAllDeviceFinancingInfo()
		{
			var response = _client.GetAllDeviceFinancingInfo().ToList();
			return response;
		}

		public List<DeviceFinancingInfo> GetDeviceFinancingInfoByArticleIDs(List<string> _ArticleID)
		{
			var response = _client.GetDeviceFinancingInfoByArticleIDs(_ArticleID.ToArray()).ToList();
			return response;
		}

		public List<DeviceFinancingInfo> GetDeviceFinancingInfoByArticleID(string _ArticleID, int _PlanID)
		{
			var response = _client.GetDeviceFinancingInfoByArticleID(_ArticleID, _PlanID).ToList();
			return response;
		}

        #region 23072015 - Anthony - Reference Table [Accessory CR]

        public List<Accessory> GetAccessoryDetailsByEANKey(string eanKey)
        {
            return _client.GetAccessoryDetailsByEANKey(eanKey).ToList();
        }

        public List<Accessory> GetAccessoryDetailsByArticleID(string articleID)
        {
            return _client.GetAccessoryDetailsByArticleID(articleID).ToList();
        }

        public List<Accessory> GetAccessoryDetailsByName(string accessoryName)
        {
            return _client.GetAccessoryDetailsByName(accessoryName).ToList();
        }

        public string GetEANKeyByArticleID(string articleID)
        {
            return _client.GetEANKeyByArticleID(articleID);
        }

        public List<OfferAccessory> GetOfferAccessoryDetails(List<string> articleIDs)
        {
            return _client.GetOfferAccessoryDetails(articleIDs.ToArray()).ToList();
        }

        public List<OfferAccessory> GetAccessoryAvailability(List<OfferAccessory> offerAccessories, int orgID)
        {
            return _client.GetAccessoryAvailability(offerAccessories.ToArray(), orgID).ToList();
        }

		public List<AccFinancingInfo> GetAllAccFinancingInfo()
		{
			var response = _client.GetAllAccessoriesFinancingInfo();

			if (response != null)
				return _client.GetAllAccessoriesFinancingInfo().ToList();
			else
				return new List<AccFinancingInfo>();
		}
        #endregion
    }
}
