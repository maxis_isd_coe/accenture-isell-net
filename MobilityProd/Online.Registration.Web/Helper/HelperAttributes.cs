﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.Helper
{
    /// <summary>
    /// New filter attribute for not storing information in cache
    /// <Author>
    /// Sutan Dan
    /// </Author>
    /// </summary>
    [Serializable]    
    public class DoNoTCache : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            context.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        }
    }
}