﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.M2KSvc;

using log4net;

namespace Online.Registration.Web.Helper
{
    public class M2KServiceProxy : ServiceProxyBase<ServiceSoapClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(M2KServiceProxy));
        public bool IsLastCallFailed { get; set; }
        public string LastErrorMessage { get; set; }

        public M2KServiceProxy()
        {
            _client = new ServiceSoapClient();
        }

        #region Private Methods

        private void LogResponse(WSResponse response)
        {
            IsLastCallFailed = false;
            LastErrorMessage = "";

            if (response.Code == "1")
                return;

            // Log Error
            Logger.ErrorFormat("Code: {0}|Message: {1}|ErrorID: {2}", response.Code, response.Message, response.ErrorID);

            IsLastCallFailed = true;
            LastErrorMessage = response.Message;
        }

        #endregion

        public AccountGetResp AccountGet(string msisdn)
        {
            var response = _client.AccountGet(msisdn);
            LogResponse(response.Response);

            return response;
        }

        public static Account GetAccount(string msisdn)
        {
            Logger.InfoFormat("GetAccount MSISDN: {0}", msisdn);

            using (var proxy = new M2KServiceProxy())
            {
                var response = proxy.AccountGet(msisdn);
                if (response != null)
                {
                    //if (proxy.IsLastCallFailed)
                    //    throw new ApplicationException(proxy.LastErrorMessage);

                    return response.Account;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}