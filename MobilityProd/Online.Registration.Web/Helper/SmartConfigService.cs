﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Online.Registration.Web.SmartConfigService;
using Online.Registration.Web.ConfigSvc;
using Online.Registration.DAL.Models;

namespace Online.Registration.Web.Helper
{
    public class SmartConfigService : ServiceProxyBase<ConfigSvc.ConfigServiceClient>
    {
        public SmartConfigService()
        {
            _client = new ConfigSvc.ConfigServiceClient();
        }
       
        public IEnumerable<DAL.Models.TransactionTypes> TransactionTypeGet()
        {
            var response = _client.TransactionTypeGet();
            return response.Statuss;
        }
        public IEnumerable<int> StoreKeeperFind(StatusFind Status)
        {
            var request = new StatusFindReq()
            {
                StatusFind = Status
            };

            var response = _client.StoreKeeperFind(request);
            return response.IDs;
        }
        public static IEnumerable<DAL.Models.Status> FindStoreKeeperStatus(StatusFind Status)
        {
            using (var proxy = new SmartConfigService())
            {
                return proxy.StatusGet(proxy.StoreKeeperFind(Status));
            }
        }
        public IEnumerable<DAL.Models.Status> StatusGet(IEnumerable<int> Statuss)
        {
            if (Statuss == null)
                return new List<Status>(); ;

            var request = new StatusGetReq()
            {
                IDs = Statuss.ToArray()
            };

            var response = _client.StatusGet(request);

            return response.Statuss;
        }
        public static IEnumerable<DAL.Models.Status> FindCashierStatus(StatusFind Status)
        {
            using (var proxy = new SmartConfigService())
            {
                return proxy.StatusGet(proxy.CashierFind(Status));
            }
        }
        public IEnumerable<int> CashierFind(StatusFind Status)
        {
            var request = new StatusFindReq()
            {
                StatusFind = Status
            };

            var response = _client.CashierFind(request);
            return response.IDs;
        }
    }
}