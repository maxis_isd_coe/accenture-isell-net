﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Security.Cryptography;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using Online.Registration.Web.Properties;
using Online.Registration.DAL.Models;
using com.google.zxing.common;
using com.google.zxing.qrcode;
using System.Xml.Linq;
using System.Net;
using System.Drawing.Drawing2D;
using Online.Registration.Web.SubscriberICService;
using log4net;
using SNT.Utility;
using System.Data;
using System.Web.Security;
using Online.Registration.Web.Providers;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using System.Runtime.Serialization;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.ContentRepository;
using System.Xml;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.Helper;
using Online.Registration.Web.Models;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Online.Registration.Web.Helper
{

    public class Response<T>
    {
        public T Data { get; set; }
        public string Message { get; set; }
        public bool Status { get; set; }
        public bool isReserved { get; set; }
    }

    #region Public Enum

    public enum CorpType
    {
        BRN,
        NAME,
        MASTERACCT,
        PARENTACCT
    }

    public enum AccessoryDropDownList
    {
        PleaseSelect = 0,
        EANID = 1,
        ArticleID = 2,
        Name = 3,
        Serial = 4
    }

    public enum CrpType
    {
        PrincipalToPrincipal = 1,
        PrincipalToSuplementary = 2,
        SuplementaryToSuplementary = 3,
        SuplementaryToPrincipal = 4

    }
    public enum ContractType
    {
        Terimnate,
        Extend,
        TerimnateOnly,
        PtoSupline,
        StoPrinciple,
        CRP
    }
    public enum CRPDeviceRegSteps
    {
        Device = 0,
        DeviceCatalog = 1,
        Plan = 2,
        SelectComponent = 3,
        Vas = 4,
        Penalty = 5,
        PersonalDetails = 6,
        Summary = 7,
        Submit = 8,
		//SelectOfferVas = 9,
		Accesories = 10,
		DevicePlan = 11,
		SummaryNew = 12

    }

    public enum AddContractRegSteps
    {
        AccountList = 1,
        ContractCheck = 2,
        DevicePlan = 3,
        Accessory = 4,
        VAS = 5,
        PersonalDetails = 6,
        Summary = 7,
        Submit = 8

    }

    public enum SmartRegistrationSteps
    {
        Device = 0,
        DeviceCatalog = 1,
        Plan = 2,
        SelectComponent = 3,
        Vas = 4,
        Penalty = 5,
        PersonalDetails = 6,
        Summary = 7,
        Submit = 8

    }

    public enum MenuType
    {
        MobileRegistration = 1,
        HomeRegistration = 2,
        CustomerService = 3,
        SearchOrder = 4,
        MNPRegistration = 5,
        StoreKeeper = 6,
        Cashier = 7,
        Reports = 8, // Added by VLT 21-05-2013
        MNPStatusTrack = 9, ///Added by Sutan Dan for MNP related changes
        TeamLeader = 10,
        DashBoard = 11,
        MNPPlanWithMultiSuppline = 12,
        OrderStatusReport = 13,
        CDPUDashBoard = 14,
        PegaRecommendationDashboard = 15
    }

    public enum CRPMobileRegistrationSteps
    {
        Plan = 1,
        Vas = 2,
        PersonalDetails = 3,
        Summary = 4,
        Submit = 5
    }

    public enum DevicePriceType
    {
        NoPrice,
        RetailPrice,
        ItemPrice
    }

    public enum NATIONALITY
    {
        Malaysian,
        NonMalaysian
    }

    public enum IDCARDTYPE
    {
        NRIC,
        Passport
    }

    public enum MobileRegDeviceOption
    {
        Brand = 1,
        HotSeller = 2,
        Promotion = 3
    }

    public enum RegistrationSteps
    {
        Device = 0,
        DeviceCatalog = 1,
        Plan = 2,
        Vas = 3,
        SecoPlan = 4,
        SecoVas = 5,
        MobileNo = 6,
        PersonalDetails = 7,
        Summary = 8,
        Submit = 9,
        SelectComponent = 10,
        ReplacementReasons = 11,
        CustomerPersonalDetails = 12,
        DeviceSeco = 13,
        DeviceCatalogSeco = 14,
        DevicePlan = 15
    }

    public enum DeviceSaleSteps
    {
        Brand = 1,
        Device = 2,
        Accessory = 3,
        PersonalDetails = 4,
        CustomerSummary = 5
    }

    public enum MobileRegistrationSteps
    {
        Device = 0,
        DeviceCatalog = 1,
        Plan = 2,
        Vas = 3,
        MobileNo = 4,
        PersonalDetails = 5,
        Summary = 6,
        Submit = 7,
        SelectComponent = 8,
        ReplacementReasons = 9,
        CustomerPersonalDetails = 10,
        MNPSelectSuppLine = 11,  //Please check the below enum value and correct it.
        DevicePlan = 12,
        Accessory = 13,
        Accounts = 14
    }

    public enum SIMReplacementStep
    {
        AccountDetails = 0,
        VAS = 1,
        CustomerSummary = 2
        
    }

    public enum SecondaryLineRegistrationSteps
    {


        Plan = 1,
        Vas = 2,
        PersonalDetails = 3,
        Summary = 4,
        Submit = 5
    }

    public enum HomeRegistrationSteps
    {
        InstallationAddr = 0,
        CustDetails = 1,
        Plan = 2,
        WBBPlan = 3,
        //NumberSelection = 4,
        Summary = 4,
        Submit = 5
    }

    public enum MobileRegType
    {
        DevicePlan = 1,
        PlanOnly = 2,
        DeviceOnly = 6,
        SuppPlan = 8,
        NewLine = 9,
        //Added by Patanjali on 30-03-2013 to support MNP
        MNPPlanOnly = 10,
        MNPSuppPlan = 11,
        MNPNewLine = 12,
        AddRemoveVAS = 13,
        SimReplacement = 14,
        CRP = 19,
        DeviceCRP = 24,
        RegType_MNPPlanWithMultiSuppline = 26,
        SecPlan = 27,
        SupplementaryPlanwithDevice = 37,
        //Added by Patanjali on 30-03-2013 to support MNP Ends here
        Contract = 36,
        DeviceSales = 38,
        ManageExtraTen = 39,
        //GTM e-Billing CR - Ricky - 2014.09.25
        BillDelivery = 40,
        Accessories = 41
    }

    public enum PrintingIssues
    {

        Networkproblem = 2,
        Printernotdetected = 3,
        Internalproblem = 4
    }

    public enum PAYMENTMODE
    {
        Cash,
        AutoBilling

    }

    public enum RefType
    {
        AddressType,
        AccountCategory,
        Bank,
        Brand,
        Bundle,
        CardType,
        Category,
        Colour,
        ComplaintIssues,
        Component,
        ComponentType,
        Country,
        CustomerTitle,
        DeviceProperty,
        ExternalIDType,
        IDCardType,
        IssueGrpDispatchQ,
        Language,
        Nationality,
        Market,
        Model,
        Organization,
        OrgType,
        Package,
        PackageType,
        PaymentMode,
        PgmBdlPkgComp,
        PlanProperty,
        Program,
        Property,
        PropertyValues,
        Race,
        RegType,
        State,
        Status,
        StatusType,
        StatusReason,
        UserGroup,
        User,
        VIPCode,
        Access,
        AccessUserByOrg,
        UserRole,
        ModelGroup,
        CustomerInfoPaymentMode,
        CustomerInfoCardType,
        Doners,
        SimModelType, // Added by VLT on 09 Apr 2013
        SimReplacementReason, //  Added by VLT on 17 June 2013
        SimReplacementprePaidReason, //  Added by VLT on 17 June 2013
        Msisdn,
        RestrictKeananComponent,
        RestrictKeananComponentGetAlias,
        IdCardTypes,
        SIMCardType, //  Added by VLT on 28 March 2014
        AllSimModelType,
        OrganizationWithWsdlURL,
        Region,
        Capacity,
        DeviceType,
        PriceRange,
        ModelArticle,
        ModelID,
		NBAOthRecommendations,
        RegTypeWaiveOff,
        ThirdPartyAuthType, // Added by Ashley Ow on 30 October 2015
        TransferOwnershipIDType, //Added by Ashley Ow on 23 December 2015
        PegaResponse,
        PegaStatus,
        PegaStatusRule,
		NBA_GroupID,
		NBA_DeptID,
		NBA_RoleID
    }

    public enum REGTYPE
    {
        DevicePlan,
        PlanOnly,
        DeviceOnly,
        Home,
        NewLine,
        SuppPlan,
        MNPPlanOnly,
        MNPSuppPlan,
        MNPNewLine,
        SimReplacement,
        CRP,
        DeviceCRP,
        SecPlan,
        RegType_MNPPlanWithMultiSuppline = 26,
        AddContract = 28,
        ManageExtraTen = 29,
        //GTM e-Billing CR - Ricky - 2014.09.25
        BillDelivery
    }

    public enum STATUS
    {

    }

    public enum STATUSTYPE
    {
        Registration,
        Home
    }

    public enum ORGTYPE
    {
        Warehouse, Installer, Distributor, Supplier, Incumbent, Dealer, Branch
    }

    public enum ReportFormat { Excel, Web }

    public enum SimReplacementType
    {
        N2N = 1,
        N2M = 2,
        M2M = 3,
        M2N = 4,
        PREPAID = 5,
        HSDPA = 6,
        FWBB = 7
    }

    #endregion
    public class CMSS
    {
        public string msgCode { get; set; }
        public string msgDesc { get; set; }
    }

    public class Util
    {
        #region Common Print method

        public static void CommonPrintMobileRegSummary(FormCollection collection)
        {
            HttpContext.Current.Session["selectedPrinter"] = collection[5].ToString();
            int printstatus = Util.PrintFile(collection[3].ToString2(), collection[5].ToString2(),collection[1].ToInt());

            if (printstatus == 1)
            {
                HttpContext.Current.Session["printsuccess"] = "PrintSuccessfullyCompleted";
            }
            else
            {
                HttpContext.Current.Session["printsuccess"] = "Printing Failed";
            }
        }
        #endregion
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Util));

        public static CMSS retrieveCaseDetls(string caseId, string userName = "iselluser", string password = "iselluser", string postUrl = "")
        {
            postUrl = "http://10.200.51.126:7030/ws/maxis.eai.process.crm.ws:retrieveCrmCaseInfoService/maxis_eai_process_crm_ws_retrieveCrmCaseInfoService_Port";
            string postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:v11=""http://services.maxis.com.my/EAI/CRM/enquiryService/v1/"" xmlns:v12=""http://schemas.maxis.com.my/EAI/CRM/enquiry/v1/"">
<soapenv:Header>
<v1:eaiHeader>
<v1:from>ISELL</v1:from>
<v1:to>ESB</v1:to>
<v1:appId>OPF</v1:appId>
<v1:msgType>Request</v1:msgType>
<v1:msgId>ISELL12345</v1:msgId>
<!--Optional:-->
<v1:correlationId>?</v1:correlationId>
<v1:timestamp>20130316</v1:timestamp>
</v1:eaiHeader>
</soapenv:Header>
<soapenv:Body>
<v11:retrieveCrmCaseInfoRequest>
<v12:retrieveCrmCaseInfoRequestDetails>
<v12:caseId>{0}</v12:caseId>
</v12:retrieveCrmCaseInfoRequestDetails>
</v11:retrieveCrmCaseInfoRequest>
</soapenv:Body>
</soapenv:Envelope>", caseId);
            WebClient retrieveAcctListWebClient = new WebClient();
            Uri postUri = new Uri(postUrl);
            string postResult = string.Empty;
            CMSS cms = new CMSS();
            retrieveAcctListWebClient.Credentials = new NetworkCredential(userName, password);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");

            try
            {
                //Depending upon settings if true then use dummy data

                postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                postResult = RemoveAllNamespaces(postResult);
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", postResult));

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here

                XDocument doc = XDocument.Parse(postResult);
                foreach (var r in doc.Descendants("retrieveCrmCaseInfoResponseDetails"))
                {
                    cms.msgCode = r.Element("msgCode") == null ? "" : r.Element("msgCode").Value.ToString();
                    cms.msgDesc = r.Element("msgDesc") == null ? "" : r.Element("msgDesc").Value.ToString();
                }
            }
            catch (Exception ex)
            {
                cms = null;
            }
            return cms;
        }

        public static string RenderViewToString(string controllerName, string viewName, object viewData)
        {
            using (var writer = new StringWriter())
            {
                var routeData = new RouteData();
                routeData.Values.Add("controller", controllerName);
                var fakeControllerContext = new ControllerContext(new HttpContextWrapper(new HttpContext(new HttpRequest(null, "http://google.com", null), new HttpResponse(null))), routeData, new Online.Registration.Web.Controllers.RegistrationController());
                var razorViewEngine = new RazorViewEngine();
                var razorViewResult = razorViewEngine.FindView(fakeControllerContext, viewName, "", false);

                var viewContext = new ViewContext(fakeControllerContext, razorViewResult.View, new ViewDataDictionary(viewData), new TempDataDictionary(), writer);
                razorViewResult.View.Render(viewContext, writer);
                return writer.ToString();
            }
        }

        #region Method by hadi, method to create String HTML from view
        public static string RenderPartialViewToString(Controller controller, string viewName, object model)
        {
            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    controller.ViewData.Model = model;

                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        #endregion

        public static byte[] convertHTMLStringToPDFByteArray(String html, int regID)
        {

            //20141126 - Habs - Ensure that the images are written in the PDF - start
            //String imageFileLocation = "C:/N/MobilityDrop4/Online.Registration.Web";
            String imageFileLocation = HttpContext.Current.Server.MapPath("/");

            if (imageFileLocation != null)
            {
                imageFileLocation = imageFileLocation.Replace("\\", "/");
            }
            Logger.Info("imageFileLocation /: " + imageFileLocation);

            // 20141202 - investigate missing image file - start
            if (!File.Exists(imageFileLocation + "/Content/images/maxis_use_only_s.png"))
            {
                imageFileLocation = HttpContext.Current.Server.MapPath("~");

                if (imageFileLocation != null)
                {
                    imageFileLocation = imageFileLocation.Replace("\\", "/");
                }
                Logger.Info("imageFileLocation ~: " + imageFileLocation);
            }



            if (!File.Exists(imageFileLocation + "/Content/images/maxis_use_only_s.png"))
            {
                imageFileLocation = "C:/Accenture/iSELL_Mobility_UT_Test/Content/images";
                Logger.Info("imageFileLocation hardcoded: " + imageFileLocation);
            }
            // 20141202 - investigate missing image file - end

            String applicationVirtualPath = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
            Logger.Info("System.Web.Hosting.HostingEnvironment.SiteName: " + System.Web.Hosting.HostingEnvironment.SiteName);
            Logger.Info("System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath: " + System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);



            html = html.Replace("<div class=\"pageBreak3\">&nbsp;</div>", "<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />");
            html = html.Replace("<div class=\"pageBreak2\"></div> ", "<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />");

            if (applicationVirtualPath.EndsWith("/"))
            {
                html = html.Replace(applicationVirtualPath + "Content/images/maxislogo.png", imageFileLocation + "/Content/images/maxislogo.png");
                html = html.Replace(applicationVirtualPath + "Content/images/maxis_use_only_s.png", imageFileLocation + "/Content/images/maxis_use_only_s.png");
            }
            else
            {
                html = html.Replace(applicationVirtualPath + "/Content/images/maxislogo.png", imageFileLocation + "/Content/images/maxislogo.png");
                html = html.Replace(applicationVirtualPath + "/Content/images/maxis_use_only_s.png", imageFileLocation + "/Content/images/maxis_use_only_s.png");
            }

            //20141126 - Habs - Ensure that the images are written in the PDF - end

            Logger.Info("html to PDF: " + html);

            byte[] PDFBytes = (new NReco.PdfGenerator.HtmlToPdfConverter()).GeneratePdf(html);


            // habs - this is to debug the PDF creation - start
            Boolean debugPdfFileCreation = false; // set this to true to test to view the PDF file created locally

            if (debugPdfFileCreation)
            {
                String path = @"C:\Data\MyTest.pdf";

                // Delete the file if it exists. 
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                // Create the file. 
                using (FileStream fs = File.Create(path))
                {
                    //Byte[] info = new UTF8Encoding(true).GetBytes("This is some text in the file.");
                    // Add some information to the file.
                    fs.Write(PDFBytes, 0, PDFBytes.Length);
                }
            }
            // habs - this is to debug the PDF creation - end

            // Gry - move pdf save function here, so directly save to db after pdf created.
            using (var service = new UpdateServiceProxy())
            {
				Logger.Info(string.Format("PDF GENERATION {0}-{1}", regID, "PDF"));
				int? GeneratedID = null;
                RegUploadDoc objRegUploadDoc = new RegUploadDoc();
                objRegUploadDoc.FileName = regID + ".pdf";
                objRegUploadDoc.FileType = "Contract";
                objRegUploadDoc.RegID = regID;
                objRegUploadDoc.FileStream = PDFBytes;

                // habs - add a try catch here in-order for other process to continue below - start
                //service.SaveDocument(objRegUploadDoc);
                try
                {
                    service.SaveDocument(objRegUploadDoc);
					Logger.Info(string.Format("PDF GENERATION SUCCESS {0}-{1}", regID, "PDF"));
                }
                catch (Exception ex)
                {
                    Logger.Error("Failed to save contract HTML source:" + ex);
					WebHelper.Instance.LogService(APIname: "ICONTRACT", LastGeneratedID: out GeneratedID, MethodName: "generatePDF", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);
                }
                // habs - add a try catch here in-order for other process to continue below - end

            }

            return PDFBytes;
        }

        public static List<SelectListItem> GetTransactionTypes(string defaultValue = "0", string defaultText = "default")
        {
            SmartConfigService proxy = new SmartConfigService();
            var resultList = new List<SelectListItem>();
            if (defaultText == "default")
                defaultText = "-- Please Select --";
            var statusList = proxy.TransactionTypeGet();
            if (statusList != null)
            {
                resultList = statusList.Select(a =>
                    new SelectListItem()
                    {
                        Text = a.Trn_Type_Name,
                        Value = a.Trn_Type_ID.ToString()
                    }).ToList();
            }
            resultList.Insert(0, new SelectListItem()
            {
                Text = defaultText,
                Value = defaultValue,
                Selected = true
            });

            return resultList;
        }

        public static List<SelectListItem> GetCancellationReasons(string defaultValue = "0", string defaultText = "default")
        {
            var proxy = new SmartRegistrationServiceProxy();
            var resultList = new List<SelectListItem>();
            if (defaultText == "default")
                defaultText = "-- Please Select --";
            var statusList = proxy.RegistrationCancellationReason();
            if (statusList != null)
            {
                resultList = statusList.Select(a =>
                    new SelectListItem()
                    {
                        Text = a.CancellationReason,
                        Value = a.ID.ToString()
                    }).ToList();
            }

            resultList.Insert(0, new SelectListItem()
            {
                Text = defaultText,
                Value = defaultValue,
                Selected = true
            });

            return resultList;

        }

        public static retrievePenaltyByMSISDN retrievePenalty(string MSISDN, string ExternalID, string SubscriberNo, string SubscriberNoResets, string userName = "iselluser", string password = "iselluser", string postUrl = "")
        {
            //postUrl = "http://10.200.51.126:7030/ws/maxis.eai.process.common.ws.retrieveContractInfoService/maxis_eai_process_common_ws_retrieveContractInfoService_Port";
            postUrl = System.Configuration.ConfigurationManager.AppSettings["RetrieveContractInfoService"].ToString();
            retrievePenaltyByMSISDN penaltybyMSISDN = new retrievePenaltyByMSISDN();
            string postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:max=""http://maxis71/maxis.eai.process.common.ws:retrieveContractInfoService"" xmlns:v11=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
   <soapenv:Header>
      <v1:eaiHeader>
         <v1:from>ISELL</v1:from>
         <v1:to>ESB</v1:to>
         <v1:appId>OPF</v1:appId>
         <v1:msgType>Request</v1:msgType>
         <v1:msgId>12345</v1:msgId>
         <!--Optional:-->
         <v1:correlationId>?</v1:correlationId>
         <v1:timestamp>20130316</v1:timestamp>
      </v1:eaiHeader>
   </soapenv:Header>
   <soapenv:Body>
      <max:retrieveContractInfoRequest>
         <v11:retrieveContractInfoRequestDetails>
            <v11:externalId>{0}</v11:externalId>
            <v11:extenalIdType>{1}</v11:extenalIdType>
            <!--Optional:-->
            <v11:subscrNo>{2}</v11:subscrNo>
            <!--Optional:-->
            <v11:subscrNoResets>{3}</v11:subscrNoResets>
         </v11:retrieveContractInfoRequestDetails>
      </max:retrieveContractInfoRequest>
   </soapenv:Body>
</soapenv:Envelope>", MSISDN, ExternalID, SubscriberNo, SubscriberNoResets);
            WebClient retrieveAcctListWebClient = new WebClient();
            Uri postUri = new Uri(postUrl);
            string postResult = string.Empty;

            retrieveAcctListWebClient.Credentials = new NetworkCredential(userName, password);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");

            try
            {
                //Depending upon settings if true then use dummy data

                postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                postResult = RemoveAllNamespaces(postResult);
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", postResult));

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here
                XDocument doc = XDocument.Parse(postResult);
                string penalty = string.Empty;
                foreach (var p in doc.Descendants("retrieveContractInfoResponseDetails"))
                {

                    penaltybyMSISDN.msgCode = p.Element("msgCode") == null ? "" : p.Element("msgCode").Value.ToString();
                    penaltybyMSISDN.msgDesc = p.Element("msgDesc") == null ? "" : p.Element("msgDesc").Value.ToString();
                    foreach (var r in doc.Descendants("contractInfoList"))
                    {
                        penaltybyMSISDN.startDate = r.Element("startDate") == null ? "" : r.Element("startDate").Value.ToString();
                        penaltybyMSISDN.endDate = r.Element("endDate") == null ? "" : r.Element("endDate").Value.ToString();
                        penaltybyMSISDN.contractType = r.Element("contractType") == null ? "" : r.Element("contractType").Value.ToString();
                        penaltybyMSISDN.penalty = r.Element("penalty") == null ? "" : r.Element("penalty").Value.ToString();
                        if (penalty.Length == 0)
                            penalty = penaltybyMSISDN.penalty;
                    }

                }
                penaltybyMSISDN.penalty = penalty;
            }
            catch (Exception ex)
            {

            }
            return penaltybyMSISDN;
        }

        public static IList<Online.Registration.Web.SmartViewModels.ContractModels> retrieveCpntractDetls(string MSISDN, string kenancode, string userName = "iselluser", string password = "iselluser", string postUrl = "")
        {
            postUrl = Settings.Default.retrievePkgCompInfoService.ToString();
            string postData = string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:v1=""http://schemas.maxis.com.my/EAI/common/soap/v1/"" xmlns:v11=""http://services.maxis.com.my/EAI/COMMON01/enquiryService/v1/"" xmlns:v12=""http://schemas.maxis.com.my/EAI/COMMON01/enquiry/v1/"">
            <soapenv:Header>
      <v1:eaiHeader>
         <v1:from>ISELL</v1:from>
         <v1:to>ESB</v1:to>
         <v1:appId>OPF</v1:appId>
         <v1:msgType>Request</v1:msgType>
         <v1:msgId>ISELL12345</v1:msgId>
         <!--Optional:-->
         <v1:correlationId>?</v1:correlationId>
         <v1:timestamp>20130316</v1:timestamp>
      </v1:eaiHeader>
   </soapenv:Header>
   <soapenv:Body>
      <v11:retrievePkgCompInfoRequest>
         <v12:retrievePkgCompInfoRequestDetails>
            <v12:externalId>{0}</v12:externalId>
            <v12:extenalIdType>{1}</v12:extenalIdType>
         </v12:retrievePkgCompInfoRequestDetails>
      </v11:retrievePkgCompInfoRequest>
   </soapenv:Body>
</soapenv:Envelope>", MSISDN, kenancode);
            WebClient retrieveAcctListWebClient = new WebClient();
            Uri postUri = new Uri(postUrl);
            string postResult = string.Empty;
            IList<Online.Registration.Web.SmartViewModels.ContractModels> listOfThings = new List<Online.Registration.Web.SmartViewModels.ContractModels>();
            retrieveAcctListWebClient.Credentials = new NetworkCredential(userName, password);
            retrieveAcctListWebClient.Headers.Add("content-type", "text/xml");

            try
            {
                //Depending upon settings if true then use dummy data

                postResult = retrieveAcctListWebClient.UploadString(postUri, postData);
                postResult = RemoveAllNamespaces(postResult);
                // Log the request and Response -- Added by Patanjali on 12-3-2013
                Logger.Info(String.Format(" retrieveSubscriberDetls Request : {0} ", postData));
                Logger.Info(String.Format(" retrieveSubscriberDetls Response: {0} ", postResult));

                // Log the request and Response -- Added by Patanjali on 12-3-2013 Ends here

                XDocument doc = XDocument.Parse(postResult);
                foreach (var r in doc.Descendants("components"))
                {
                    Online.Registration.Web.SmartViewModels.ContractModels cms = new Online.Registration.Web.SmartViewModels.ContractModels();
                    cms.componentId = r.Element("componentId") == null ? "" : r.Element("componentId").Value.ToString();
                    cms.componentInstId = r.Element("componentInstId") == null ? "" : r.Element("componentInstId").Value.ToString();
                    cms.componentInstIdServ = r.Element("componentInstIdServ") == null ? "" : r.Element("componentInstIdServ").Value.ToString();
                    cms.componentShortDisplay = r.Element("componentShortDisplay") == null ? "" : r.Element("componentShortDisplay").Value.ToString();
                    cms.componentActiveDt = r.Element("componentActiveDt") == null ? "" : r.Element("componentActiveDt").Value.ToString();
                    cms.componentDesc = r.Element("componentDesc") == null ? "" : r.Element("componentDesc").Value.ToString();
                    cms.componentInactiveDt = r.Element("componentInactiveDt") == null ? "" : r.Element("componentInactiveDt").Value.ToString();
                    listOfThings.Add(cms);
                }
            }
            catch (Exception ex)
            {

            }
            return listOfThings;
        }


        /// <summary>
        /// return list number
        /// </summary>
        /// <param name="maxNumber"></param>
        /// <returns></returns>
        public static List<SelectListItem> getListOfNumber(int maxNumber)
        {
            var numberList = new List<SelectListItem>();
            for (int i = 1; i <= Convert.ToInt32(maxNumber); i++)
            {
                numberList.Add(new SelectListItem()
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }
            return numberList;
        }

        public static List<SelectListItem> GetDiscountsLessThan1000(string Maxvalue, string selectedValue = "")
        {
            var dayList = new List<SelectListItem>();
            for (int i = 0; i <= Convert.ToInt32(Maxvalue); i += 50)
            {
                dayList.Add(new SelectListItem()
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }
            //Discounts using in Smart only and as per old system we shouldn't show 0, 50 in discount selection
            if (System.Configuration.ConfigurationManager.AppSettings["ZeroDiscountRequired"] != null && System.Configuration.ConfigurationManager.AppSettings["ZeroDiscountRequired"].ToString().ToUpper() == "TRUE")
                dayList = dayList.Where(d => d.Text != "50").ToList();
            else
                dayList = dayList.Where(d => d.Text != "50" && d.Text != "0").ToList();
            List<SelectListItem> list = new List<SelectListItem>();




            for (int i = 0; i < dayList.Count; i++)
            {
                if (dayList[i].Text != selectedValue)
                {
                    list.Add(new SelectListItem() { Text = dayList[i].Text, Value = dayList[i].Value, Selected = false });
                }
                else
                {
                    list.Add(new SelectListItem() { Text = dayList[i].Text, Value = dayList[i].Value, Selected = true });
                }
            }



            list.Insert(0, new SelectListItem()
            {

                Text = "Please Select",
                Value = ""
            });

            return list;
        }

        public static List<SelectListItem> GetDiscountsGreaterThan1000(string Maxvalue, string selectedValue = "")
        {
            var dayList = new List<SelectListItem>();
            int MaxDvalue = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MaxRebate"]);
            //CODE COMMENTED BY SUTAN AS NO FILTERATION IS PRESENT
            //for (int i = 1100, j = 1100; j < Convert.ToInt32(MaxDvalue); i += 100, j += 100)
            int MaxValueOrMaxDvalue = Convert.ToInt32(Maxvalue) <= MaxDvalue ? Convert.ToInt32(Maxvalue) : MaxDvalue;

            for (int i = 1100, j = 1100; j < Convert.ToInt32(MaxValueOrMaxDvalue); i += 100, j += 100)
            {
                dayList.Add(new SelectListItem()
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            List<SelectListItem> list = new List<SelectListItem>();

            for (int i = 0; i < dayList.Count; i++)
            {
                if (dayList[i].Text != selectedValue)
                {
                    list.Add(new SelectListItem() { Text = dayList[i].Text, Value = dayList[i].Value, Selected = false });
                }
                else
                {
                    list.Add(new SelectListItem() { Text = dayList[i].Text, Value = dayList[i].Value, Selected = true });
                }
            }



            list.Insert(0, new SelectListItem()
            {

                Text = "Please Select",
                Value = ""
            });

            return list;
        }

        //Spend Limit Commented by Ravi As per New Flow on June 15 2013
        //private static string spendLimit;
        private static string _supplementaryMSISDNs;

        public static string SupplementaryMSISDNs
        {
            get { return _supplementaryMSISDNs; }
            set { _supplementaryMSISDNs = value; }
        }

        //Added by VLT For Supple New Account Creation
        private static bool _isSuppleNewAccount;

        public static bool IsSuppleNewAccount
        {
            get { return _isSuppleNewAccount; }
            set { _isSuppleNewAccount = value; }
        }

        private static bool _isSecondaryNewAccount;
        public static bool IsSecLineNewAccount
        {
            get { return _isSecondaryNewAccount; }
            set { _isSecondaryNewAccount = value; }
        }


        #region "Extra Ten Common Functionality"

        //Extra Ten Changes 
        public static string validateExtraTenMSISDN(string arrMSISDNs)
        {

            string strInvalidMSISDNs = string.Empty;
            using (var proxy = new retrieveServiceInfoProxy())
            {

                foreach (var msisdn in arrMSISDNs.Split(','))
                {
                    if (msisdn != string.Empty)
                    {
                        SubscriberRetrieveServiceInfoRequest objRequest = new SubscriberRetrieveServiceInfoRequest();
                        objRequest.externalId = msisdn;
                        SubscriberRetrieveServiceInfoResponse objResponse = new SubscriberRetrieveServiceInfoResponse();
                        objResponse = proxy.retrieveServiceInfo(objRequest);
                        if (objResponse != null)
                        {
                            if ((objResponse.lob == "POSTGSM" || objResponse.lob == "PREGSM" || objResponse.lob == "HSDPA") && (objResponse.packageName.ToUpper().Contains("VOIP") == false))
                            { }
                            else
                                //return "false";
                                strInvalidMSISDNs = strInvalidMSISDNs + msisdn + ",";
                        }
                        else
                            strInvalidMSISDNs = strInvalidMSISDNs + msisdn + ",";


                    }

                }
            }
            return strInvalidMSISDNs.TrimEnd(',');

        }

        public static string validateExtraTenMSISDNAgainstAccount(string arrMSISDNs)
        {
            string strInvalidMSISDNs = string.Empty;
            if (HttpContext.Current.Session[SessionKey.SuppleMsisdns.ToString()].ToString2().Length > 0)
            {
                foreach (var msisdn in arrMSISDNs.Split(','))
                {
                    if (msisdn != string.Empty)
                    {
                        if (HttpContext.Current.Session[SessionKey.SuppleMsisdns.ToString()].ToString2().Contains(msisdn) == true)
                        {
                            strInvalidMSISDNs = strInvalidMSISDNs + msisdn + ",";
                        }

                    }
                }
            }

            return strInvalidMSISDNs.TrimEnd(',');
        }

        public static int updateLnkMdlGrpMdlforSupp(int regPrimeID)
        {
            Logger.Debug("Entering - updateLnkMdlGrpMdlforSupp = " + regPrimeID);
            var session = HttpContext.Current.Session;
            var dropObj = (DropFourObj)session[SessionKey.DropFourObj.ToString()];
            int result = 0;

            try
            {
                using (var proxy = new RegistrationServiceProxy())
                {
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = regPrimeID } }).ToList();
                    int? mdlGrpModelID = null;
                    #region Update mdlGrpMdl when supplementary have device
                    if (suppLineIDs.Any())
                    {
                        var regSuppLinesList = proxy.RegSuppLineGet(suppLineIDs).ToList();
                        if (regSuppLinesList.Any())
                        {
                            foreach (var suppline in regSuppLinesList)
                            {
                                if (!string.IsNullOrEmpty(suppline.ArticleID) && suppline.ArticleID != "0")
                                {
									int index = dropObj.suppFlow.FindIndex(x => x.mobile.SelectedMobileNo.Equals(suppline.MSISDN1));
                                    Logger.Info("updateLnkMdlGrpMdlforSupp index = " + index);
                                    if (index >= 0)
                                    {
                                        var isTradeUp = false;
                                        decimal tradeUpAmount = 0;

                                        using (var cat_proxy = new CatalogServiceProxy())
                                        {
                                            var modelGroupIDs = cat_proxy.ModelGroupFind(new ModelGroupFind()
                                            {
                                                ModelGroup = new ModelGroup()
                                                {
                                                    PgmBdlPckComponentID = dropObj.suppFlow[index].PkgPgmBdlPkgCompID
                                                },
                                                Active = true
                                            });

                                            var modelGroupModels = cat_proxy.ModelGroupModelGet(cat_proxy.ModelGroupModelFind(new ModelGroupModelFind()
                                            {
                                                ModelGroupModel = new ModelGroupModel()
                                                {
                                                    ModelID = dropObj.suppFlow[index].orderSummary.SelectedModelImageID
                                                }
                                            })).ToList();

                                            mdlGrpModelID = modelGroupModels.Where(a => modelGroupIDs.Contains(a.ModelGroupID)).Select(a => a.ID).FirstOrDefault();
                                        }

                                        //14012015 - Anthony - GST Trade Up - Start
                                        if (dropObj.suppFlow[index].isTradeUp.ToBool())
                                        {
                                            isTradeUp = dropObj.suppFlow[index].isTradeUp.ToBool();
                                            tradeUpAmount = dropObj.suppFlow[index].orderSummary.SelectedTradeUpComp.Price;
                                        }
                                        //14012015 - Anthony - GST Trade Up - End

                                        var regMdlGrpModel = new RegMdlGrpModel()
                                        {
                                            ModelImageID = dropObj.suppFlow[index].orderSummary.SelectedModelImageID, //wen hao
                                            ModelGroupModelID = mdlGrpModelID != null ? mdlGrpModelID : 0,
                                            Price = dropObj.suppFlow[index].vas != null && !string.IsNullOrEmpty(dropObj.suppFlow[index].vas.price) ? Convert.ToDecimal(dropObj.suppFlow[index].vas.price) : 0,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            SuppLineID = suppline.ID,
                                            RegID = regPrimeID,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            IsTradeUp = isTradeUp,
                                            TradeUpAmount = tradeUpAmount
                                        };
                                        using (var update = new UpdateServiceProxy())
                                        {
                                            result = update.SaveRegMdlGrpModel(regMdlGrpModel);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                string errMsg = LogException(ex);
                Logger.Error(errMsg);
            }
            Logger.Debug("Entering - updateLnkMdlGrpMdlforSupp = " + regPrimeID + " + " + result);
            return result;
        }

        public static decimal getPrinSuppTotalPrice(int regPrimeID, bool IMEInumberStored = true)
        {
            decimal totalPrice = 0;
            decimal totalSuppPrice = 0;
            decimal totalPrinPrice = 0;
			bool _prinHaveDevice = false; bool _prinHaveAF = false;
			bool _suppHaveDevice = false; bool _suppHaveAF = false;


            try
            {
                using (var proxy = new RegistrationServiceProxy())
                {
					// AF info
					var afOrderInformation = WebHelper.Instance.getAccessoryDetails(regPrimeID);
                    // for principal
                    var _prinLine = proxy.RegistrationGet(regPrimeID);
                    totalPrinPrice = _prinLine.DeviceAdvance + _prinLine.DeviceDeposit + _prinLine.PlanDeposit + _prinLine.PlanAdvance;
					// if have device, do not do fullfill, need to go to pos
					if (IMEInumberStored){
						_prinHaveDevice = !string.IsNullOrEmpty(_prinLine.IMEINumber) ? true : false;
					}
					else{
						_prinHaveDevice = !string.IsNullOrEmpty(_prinLine.ArticleID) ? true : false;
					}
                    if (_prinHaveDevice)
                        totalPrinPrice += 1;

					if (afOrderInformation != null && afOrderInformation.Where(x => x.isFinancing == true).Any())
						totalPrinPrice += 1;


                    // for suppLine
                    List<DAL.Models.RegSuppLine> RegSupplines = new List<RegSuppLine>();
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = regPrimeID } }).ToList();
                    string result = string.Empty;
                    if (suppLineIDs.Any())
                    {
                        var regSuppLinesList = proxy.RegSuppLineGet(suppLineIDs).ToList();
                        if (regSuppLinesList.Any())
                        {
                            foreach (var _suppline in regSuppLinesList)
                            {
                                var eachSuppPrice = _suppline.deviceadvance + _suppline.devicedeposit + _suppline.planadvance + _suppline.plandeposit;
                                totalSuppPrice += eachSuppPrice;

                                // if have device, do not do fullfill, need to go to pos
								if (IMEInumberStored)
								{
									_suppHaveDevice = !string.IsNullOrEmpty(_suppline.IMEINumber) ? true : false;
								}
								else {
									_suppHaveDevice = !string.IsNullOrEmpty(_suppline.ArticleID) ? true : false;
								}

                                if (_suppHaveDevice)
                                    totalSuppPrice += 1;
                            }
                        }
                    }

                    if (afOrderInformation != null && afOrderInformation.Where(x => x.isFinancing == true && x.supplineIndex != null).Any())
					    totalSuppPrice += 1;

                    bool isCDPUorder = WebHelper.Instance.isCDPUorder(regPrimeID);

                    totalPrinPrice = isCDPUorder ? 0 : totalPrinPrice;
                    totalSuppPrice = isCDPUorder ? 0 : totalSuppPrice;

                    return totalPrinPrice + totalSuppPrice;
                }
            }
            catch (Exception ex)
            {
                string errMsg = LogException(ex);
                Logger.Error(errMsg);
            }
            return totalPrinPrice + totalSuppPrice;
        }


        public static bool updateSuppLineExtOrderID(int regPrimeID)
        {
            try
            {
                using (var proxy = new RegistrationServiceProxy())
                {
                    int ReturnValue = 0;
                    List<DAL.Models.RegSuppLine> RegSupplines = new List<RegSuppLine>();
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = regPrimeID } }).ToList();
                    string result = string.Empty;

                    if (suppLineIDs.Any())
                    {
                        var regSuppLinesList = proxy.RegSuppLineGet(suppLineIDs).ToList();
                        if (regSuppLinesList.Any())
                        {
                            foreach (var suppline in regSuppLinesList)
                            {
                                string ext_order_id = regPrimeID + "-" + suppline.ID;
                                RegSupplines.Add(new RegSuppLine()
                                {
                                    ID = suppline.ID,
                                    EXT_ORDER_ID = ext_order_id,
                                    Order_Status = Constants.STATUS_NEW,
                                    LastAccessID = Util.SessionAccess.UserName
                                });
                                ReturnValue = proxy.RegSuppLineUpdate(RegSupplines);
                            }
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errMsg = LogException(ex);
                Logger.Error(errMsg);
            }
            return false;
        }

        public static bool SaveExtraTen(string extraTenMobiles, int regId)
        {
            bool result = false;
            try
            {
                using (var proxy = new RegistrationServiceProxy())
                {
                    if (!ReferenceEquals(extraTenMobiles, null))
                    {
                        foreach (var extratenMSISDN in extraTenMobiles.Split(','))
                        {
                            if (extratenMSISDN != string.Empty)
                            {
                                result = proxy.SaveExtraTen(new Online.Registration.Web.RegistrationSvc.ExtraTenReq
                                {
                                    ExtraTenDetails = new Online.Registration.DAL.Models.lnkExtraTenDetails { CreatedBy = Util.SessionAccess.UserName, CreatedDate = System.DateTime.Now, Msisdn = extratenMSISDN, RegId = regId, Status = "A" }


                                });
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                string errMsg = LogException(ex);
                Logger.Error(errMsg);
            }
            return result;
        }
        #region "This is for enable / disable fields in personal details Customer screen"
        // compare if there's value before go to Personal details screen, disable the field.
        public static Online.Registration.Web.ViewModels.PersonalDetailsVM CheckDisablePersonalDetailsPage(Online.Registration.Web.ViewModels.PersonalDetailsVM _personalDetailsVM)
        {
            if (ReferenceEquals(_personalDetailsVM, null))
                return _personalDetailsVM;

            if (ReferenceEquals(_personalDetailsVM.Customer, null))
                return _personalDetailsVM;

            // check for DOB
			CustomizedCustomer CustomerPersonalInfo = null;
			int quantity;
			if (!ReferenceEquals(HttpContext.Current.Session["AllServiceDetails"], null))
			{
				string processingMSISDN = HttpContext.Current.Session["ExternalID"].ToString2();
				CustomerPersonalInfo = WebHelper.Instance.constructPersonalInformationForView(processingMSISDN);
			}

			if (_personalDetailsVM.DOBDay != 0 && _personalDetailsVM.DOBMonth != 0 && _personalDetailsVM.DOBYear != 0)
			{
				_personalDetailsVM.disablePersonalDetails.disableDOB = true;
			}
			else
			{
				if ((CustomerPersonalInfo.Dob != null) && (CustomerPersonalInfo.Dob != string.Empty) && (CustomerPersonalInfo.Dob.Length >= 4) && (int.TryParse(CustomerPersonalInfo.Dob.Substring(0, 4), out quantity) == true))
				{
					if ((CustomerPersonalInfo.Dob != null) && (CustomerPersonalInfo.Dob != string.Empty) && (CustomerPersonalInfo.Dob.Length >= 6) && (int.TryParse(CustomerPersonalInfo.Dob.Substring(4, 2), out quantity) == true))
					{
						if ((CustomerPersonalInfo.Dob != null) && (CustomerPersonalInfo.Dob != string.Empty) && (CustomerPersonalInfo.Dob.Length >= 8) && (int.TryParse(CustomerPersonalInfo.Dob.Substring(6, 2), out quantity) == true))
						{
							_personalDetailsVM.disablePersonalDetails.disableDOB = true;
						}
					}
				}
			}
            // check & disable for gender, race, contactNo, salutation, nation, email, alternateNumber
			_personalDetailsVM.disablePersonalDetails.disableRace = _personalDetailsVM.Customer.RaceID != 0 ? true : false;

			if (!string.IsNullOrEmpty(_personalDetailsVM.Customer.Gender))
				_personalDetailsVM.disablePersonalDetails.disableGender = _personalDetailsVM.Customer.Gender.Equals("F") || _personalDetailsVM.Customer.Gender.Equals("M") ? true : false;

			if (!_personalDetailsVM.disablePersonalDetails.disableGender)
				_personalDetailsVM.disablePersonalDetails.disableGender = !string.IsNullOrEmpty(CustomerPersonalInfo.Genders);
			
			if (!_personalDetailsVM.disablePersonalDetails.disableRace)
				_personalDetailsVM.disablePersonalDetails.disableRace = !string.IsNullOrEmpty(CustomerPersonalInfo.Races);

            _personalDetailsVM.disablePersonalDetails.disableSalutaion = _personalDetailsVM.Customer.CustomerTitleID > 0 ? true : false;
            _personalDetailsVM.disablePersonalDetails.disableNationality = _personalDetailsVM.Customer.NationalityID > 0 ? true : false;
            _personalDetailsVM.disablePersonalDetails.disableEmail = !string.IsNullOrEmpty(_personalDetailsVM.Customer.EmailAddr) ? true : false;
            _personalDetailsVM.disablePersonalDetails.disableAlternativeNumber = !string.IsNullOrEmpty(_personalDetailsVM.Customer.AlternateContactNo) ? true : false;
            _personalDetailsVM.disablePersonalDetails.disableContactNumber = !string.IsNullOrEmpty(_personalDetailsVM.Customer.ContactNo) ? true : false;



			return _personalDetailsVM;
        }

        #endregion

		public static PersonalDetailsVM mapCustomerInfoToVM(PersonalDetailsVM _personalDetailsVM ,CustomizedCustomer _customerInfo)
		{
			_personalDetailsVM.Address.Line1 = !string.IsNullOrEmpty(_personalDetailsVM.Address.Line1) ? _personalDetailsVM.Address.Line1 :_customerInfo.Address1;
			_personalDetailsVM.Address.Line2 = !string.IsNullOrEmpty(_personalDetailsVM.Address.Line2) ? _personalDetailsVM.Address.Line2 : _customerInfo.Address2;
			_personalDetailsVM.Address.Line3 = !string.IsNullOrEmpty(_personalDetailsVM.Address.Line3) ? _personalDetailsVM.Address.Line3 : _customerInfo.Address3;
			_personalDetailsVM.Address.Postcode = !string.IsNullOrEmpty(_personalDetailsVM.Address.Postcode) ? _personalDetailsVM.Address.Postcode : _customerInfo.PostCode;
			_personalDetailsVM.Address.StateID = _customerInfo.State.ToInt();

			return _personalDetailsVM;
		}


        #region "Forming Personal Details and Customer by ravi"
        /// <summary>
        ///Common Method for forming personal details and customer by ppidinfo object 
        /// </summary>
        /// <param name="personalDetailsVM">PersonalDetailsVM object</param>
        /// <returns>PersonalDetailsVM object</returns>
        public static Online.Registration.Web.ViewModels.PersonalDetailsVM FormPersonalDetailsandCustomer(Online.Registration.Web.ViewModels.PersonalDetailsVM personalDetailsVM, bool _fromSuppLine = false, bool _editDisabled = false, bool _isStandalone = false)
        {
            var session = HttpContext.Current.Session;
            #region "Personal Details Formation and customer for the registration"
            retrieveAcctListByICResponse AcctListByICResponse = null;
            CustomizedCustomer CustomerPersonalInfo = null;
            if (!ReferenceEquals(session["PPIDInfo"], null))
            {
                List<SelectListItem> lstNationality = Util.GetList(RefType.Nationality, defaultValue: string.Empty);

                ///CHECK IF EXISTING USER THEN ONLY RETRIEVE CUSTOMER INFORMATION
                AcctListByICResponse = (retrieveAcctListByICResponse)session["PPIDInfo"];
                ///CHECK IF CUSTOMER INFORMATION IS THERE OR NOT
                if (!ReferenceEquals(AcctListByICResponse, null) && !ReferenceEquals(AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList(), null))
                {
                    if (AcctListByICResponse.itemList.Count > 0)
                    {
                        if (session["ExternalID"] != null)
                        {
                            CustomerPersonalInfo = Util.GetCustomerList(AcctListByICResponse, session["ExternalID"] != null ? session["ExternalID"].ToString() : "0");
                        }
                        else
                        {
                            CustomerPersonalInfo = AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList().FirstOrDefault().Customer;
                        }
                    }
                    session["CustPersonalInfo"] = CustomerPersonalInfo;
                    if (CustomerPersonalInfo != null)
                    {
                        if (AcctListByICResponse.itemList.Any(c => c.Customer != null))
                        {
                            CustomerPersonalInfo = AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList().FirstOrDefault().Customer;
                        }
                    }
                    if (CustomerPersonalInfo != null)
                    {
                        if (!String.IsNullOrEmpty(CustomerPersonalInfo.NewIC))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 1;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.NewIC;
                            personalDetailsVM.Customer.NationalityID = lstNationality.Where(n => n.Text.ToUpper() == NATIONALITY.Malaysian.ToString().ToUpper()).SingleOrDefault().Value.ToInt();
                        }
                        else if (!String.IsNullOrEmpty(CustomerPersonalInfo.PassportNo))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 2;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.PassportNo;
                            personalDetailsVM.Customer.NationalityID = lstNationality.Where(n => n.Text.ToUpper() == NATIONALITY.NonMalaysian.ToString().ToUpper()).SingleOrDefault().Value.ToInt();
                        }
                        else if (!String.IsNullOrEmpty(CustomerPersonalInfo.OldIC))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 3;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.OldIC;
                            //personalDetailsVM.Customer.NationalityID = CustomerPersonalInfo.NationalityID;
                            personalDetailsVM.Customer.NationalityID = lstNationality.Where(n => n.Text.ToUpper() == NATIONALITY.Malaysian.ToString().ToUpper()).SingleOrDefault().Value.ToInt();
                        }
                        else if (!String.IsNullOrEmpty(CustomerPersonalInfo.OtherIC))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 4;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.OtherIC;
                            personalDetailsVM.Customer.NationalityID = CustomerPersonalInfo.NationalityID;
                        }
                    }
                }
                if (CustomerPersonalInfo != null)
                {

                    var raceList = MasterDataCache.Instance.Race;
                    var raceID = 0;
                    if (!string.IsNullOrEmpty(CustomerPersonalInfo.Races) && !CustomerPersonalInfo.Races.Equals("0"))
                    {
                        if (raceList.Races.Where(x => x.KenanCode == CustomerPersonalInfo.Races).Any())
                            raceID = raceList.Races.Where(x => x.KenanCode == CustomerPersonalInfo.Races).FirstOrDefault().ID;//20032015 - Anthony - Handle customer dirty data where CustomerPersonalInfo.Races = 5
                    }
                    var customerTitleList = MasterDataCache.Instance.CustomerTitle;
                    var customerTitleID = 0;

                    // 20141125 - Handle customer title dirty data (e.g. "Miss." instead of "Miss") - habs - start
                    if (!string.IsNullOrEmpty(CustomerPersonalInfo.Title))
                    {
                        CustomerTitle customerTitle = new CustomerTitle();
                        try
                        {
                            if (!CustomerPersonalInfo.Title.EndsWith(".") && customerTitleList.CustomerTitles.Where(x => x.Name.ToLower().Equals(CustomerPersonalInfo.Title.ToLower())).Any())
                            {
                                customerTitle = customerTitleList.CustomerTitles.Where(x => x.Name.ToLower().Equals(CustomerPersonalInfo.Title.ToLower())).FirstOrDefault();
                            }
                            else
                            {
								if ( customerTitleList.CustomerTitles.Where(x => x.Name.ToLower().Equals(CustomerPersonalInfo.Title.ToLower())).Any() )
									customerTitle = customerTitleList.CustomerTitles.Where(x => x.Name.ToLower().Equals(CustomerPersonalInfo.Title.ToLower())).FirstOrDefault();
                            }

                            if (customerTitle != null)
                            {
                                customerTitleID = customerTitle.ID;
                            }
                        }
                        catch
                        {
                            // if 0 means it's failed the validation , then need to not disable the salutation dropdown.
                            customerTitleID = 0;
                        }
                        //customerTitleID = customerTitleList.CustomerTitles.Where(x => x.Name.ToLower().Equals(CustomerPersonalInfo.Title.ToLower())).SingleOrDefault().ID;

                    }
                    // 20141125 - Handle customer title dirty data (e.g. "Miss." instead of "Miss") - habs - start

                    personalDetailsVM.Customer.CustomerTitleID = customerTitleID;

                    personalDetailsVM.Customer.Gender = !string.IsNullOrEmpty(CustomerPersonalInfo.Genders) ? CustomerPersonalInfo.Genders.Equals("1") ? "M" : "F" : string.Empty;// string.Empty;
                    personalDetailsVM.Customer.RaceID = raceID > 0 ? raceID : 0; //0;
                    //personalDetailsVM.Customer.Race = CustomerPersonalInfo.Race;
                    personalDetailsVM.Customer.NationalityID = !string.IsNullOrEmpty(CustomerPersonalInfo.Nation) ? CustomerPersonalInfo.Nation.ToInt() : CustomerPersonalInfo.NationalityID;
                    personalDetailsVM.Customer.LanguageID = CustomerPersonalInfo.LanguageID == 0 ? 1 : CustomerPersonalInfo.LanguageID;// 0; //1 is for English, refer to table refLanguage
                    personalDetailsVM.Customer.Language = CustomerPersonalInfo.Language;
                    personalDetailsVM.Customer.AlternateContactNo = string.Empty;
                    personalDetailsVM.Customer.EmailAddr = CustomerPersonalInfo.EmailAddrs;// string.Empty;
                    //if ((HttpContext.Current.Session["FromMNP"].ToBool() && !string.IsNullOrEmpty(HttpContext.Current.Session[SessionKey.AccExternalID.ToString()].ToString2()))
                    //    || HttpContext.Current.Session["FromMNP"].ToBool() == false)
                    if (String.IsNullOrEmpty(personalDetailsVM.Customer.PayModeID.ToString2()) || personalDetailsVM.Customer.PayModeID == 0 || personalDetailsVM.Customer.PayModeID == -100)
                    {
                        if ((HttpContext.Current.Session["FromMNP"].ToBool() && !string.IsNullOrEmpty(HttpContext.Current.Session[SessionKey.AccExternalID.ToString()].ToString2()))
                            || HttpContext.Current.Session["FromMNP"].ToBool() == false)
                            personalDetailsVM.Customer.PayModeID = Util.GetPaymentModeID(PAYMENTMODE.Cash);//10072015 - Anthony - to enhance the pre-default Direct Debit button for MNP new registration
                    }
                    //personalDetailsVM.Customer.PayModeID = Util.GetPaymentModeID(PAYMENTMODE.AutoBilling);
                    //personalDetailsVM.Customer.CardTypeID = 0;
                    //personalDetailsVM.Customer.NameOnCard = string.Empty;
                    //personalDetailsVM.Customer.CardNo = string.Empty;
                    if (String.IsNullOrEmpty(personalDetailsVM.Customer.CardExpiryDate.ToString2()))
                        personalDetailsVM.Customer.CardExpiryDate = null;
                    List<SelectListItem> lstStates = new List<SelectListItem>();
                    //19012015 - Anthony - retrieve the edited Address value instead of default Address value (back button issue from MobileRegSummaryNew to PersonalDetailsNew) - Start
                    personalDetailsVM.Address.Postcode = !ReferenceEquals(personalDetailsVM.Address,null) 
                                                                ? !string.IsNullOrEmpty(personalDetailsVM.Address.Postcode)
                                                                        ? personalDetailsVM.Address.Postcode
                                                                        : _isStandalone ? "" : CustomerPersonalInfo.PostCode.ToString2()
                                                                : _isStandalone ? "" : CustomerPersonalInfo.PostCode.ToString2();
                    personalDetailsVM.Address.Line1 = !ReferenceEquals(personalDetailsVM.Address, null)
                                                                ? !string.IsNullOrEmpty(personalDetailsVM.Address.Line1)
                                                                        ? personalDetailsVM.Address.Line1
                                                                        : _isStandalone ? "" : CustomerPersonalInfo.Address1.ToString2()
                                                                : _isStandalone ? "" : CustomerPersonalInfo.Address1.ToString2();
                    personalDetailsVM.Address.Line2 = !ReferenceEquals(personalDetailsVM.Address, null)
                                                                ? !string.IsNullOrEmpty(personalDetailsVM.Address.Line2)
                                                                        ? personalDetailsVM.Address.Line2
                                                                        : _isStandalone ? "" : CustomerPersonalInfo.Address2.ToString2()
                                                                : _isStandalone ? "" : CustomerPersonalInfo.Address2.ToString2();
                    personalDetailsVM.Address.Line3 = !ReferenceEquals(personalDetailsVM.Address, null)
                                                                ? !string.IsNullOrEmpty(personalDetailsVM.Address.Line3)
                                                                        ? personalDetailsVM.Address.Line3
                                                                        : _isStandalone ? "" : CustomerPersonalInfo.Address3.ToString2()
                                                                : _isStandalone ? "" : CustomerPersonalInfo.Address3.ToString2();
                    personalDetailsVM.Address.Town = !ReferenceEquals(personalDetailsVM.Address, null)
                                                                ? !string.IsNullOrEmpty(personalDetailsVM.Address.Town)
                                                                        ? personalDetailsVM.Address.Town
                                                                        : _isStandalone ? "" : CustomerPersonalInfo.City.ToString2()
                                                                : _isStandalone ? "" : CustomerPersonalInfo.City.ToString2();

                    //personalDetailsVM.Address.Postcode = CustomerPersonalInfo.PostCode.ToString2();
                    //personalDetailsVM.Address.Line1 = CustomerPersonalInfo.Address1.ToString2();
                    //personalDetailsVM.Address.Line2 = CustomerPersonalInfo.Address2.ToString2();
                    //personalDetailsVM.Address.Line3 = CustomerPersonalInfo.Address3.ToString2();
                    //personalDetailsVM.Address.Town = CustomerPersonalInfo.City.ToString2();

                    //19012015 - Anthony - retrieve the edited Address value instead of default Address value (back button issue from MobileRegSummaryNew to PersonalDetailsNew) - End

                    personalDetailsVM.Customer.FullName = CustomerPersonalInfo.CustomerName.ToString2();

                    lstStates = Util.GetList(RefType.State);
                    if (CustomerPersonalInfo.State.ToString2().Trim().Length > 0)
                    {
                        if (lstStates.Any(s => s.Text.ToUpper() == CustomerPersonalInfo.State.ToString2().Trim().ToUpper()))
                        {
                            //19012015 - Anthony - retrieve the edited Address value instead of default Address value (back button issue from MobileRegSummaryNew to PersonalDetailsNew) - Start
                            var stateId = lstStates.Where(s => s.Text.ToUpper() == CustomerPersonalInfo.State.ToUpper()).SingleOrDefault().Value.ToInt();
                            personalDetailsVM.Address.StateID = !ReferenceEquals(personalDetailsVM.Address, null)
                                                                ? personalDetailsVM.Address.StateID > 0
                                                                        ? personalDetailsVM.Address.StateID
                                                                        : _isStandalone ? 0 : stateId
                                                                : _isStandalone ? 0 : stateId;
                            //personalDetailsVM.Address.StateID = lstStates.Where(s => s.Text.ToUpper() == CustomerPersonalInfo.State.ToUpper()).SingleOrDefault().Value.ToInt();

                            //19012015 - Anthony - retrieve the edited Address value instead of default Address value (back button issue from MobileRegSummaryNew to PersonalDetailsNew) - End
                        }
                    }

                    if ((CustomerPersonalInfo.Dob != null) && CustomerPersonalInfo.Dob.Length >= 8)
                    {
                        personalDetailsVM.DOBDay = (CustomerPersonalInfo.Dob.Substring(6, 2)).ToInt();
                        personalDetailsVM.DOBMonth = (CustomerPersonalInfo.Dob.Substring(4, 2)).ToInt();
                        personalDetailsVM.DOBYear = (CustomerPersonalInfo.Dob.Substring(0, 4)).ToInt();
                    }

					#region Gerry - PBI000000008658 To enable the field that is empty from Kenan
					// not defaulting to the minimum date
					// also passport is not mandatory to have birth date in Kenan.
					/*
                    else
                    {
                        // given fix for min date if no date is avail.
                        if (session["IDCardNo"] != null)
                        {
                            int OutPut;
                            if (int.TryParse(session["IDCardNo"].ToString2().Substring(0, 2), out OutPut))
                            {
                                CustomerPersonalInfo.Dob = session["IDCardNo"].ToString2();
                                int year = session["IDCardNo"].ToString2().Substring(0, 2).ToInt();
                                int CurrYear = DateTime.Now.Year;
                                string PreYear = "19" + year.ToString2();
                                if (CurrYear - PreYear.ToInt() <= 100)
                                {
                                    personalDetailsVM.DOBYear = PreYear.ToInt();
                                }
                                else
                                {
                                    PreYear = "20" + year.ToString2();
                                    personalDetailsVM.DOBYear = PreYear.ToInt();
                                }
                                personalDetailsVM.DOBDay = (CustomerPersonalInfo.Dob.Substring(4, 2)).ToInt();
                                personalDetailsVM.DOBMonth = (CustomerPersonalInfo.Dob.Substring(2, 2)).ToInt();

                                //19820805
                                CustomerPersonalInfo.Dob = personalDetailsVM.DOBYear + "" + CustomerPersonalInfo.Dob.Substring(2, 2) + "" + CustomerPersonalInfo.Dob.Substring(4, 2);


                            }
                            else
                            {

                                personalDetailsVM.Customer.DateOfBirth = DateTime.MinValue;
                            }


                        }
					}
					*/
					#endregion

					//personalDetailsVM.Customer.CustomerTitleID = CustomerPersonalInfo.CustomerTitleID;
                }

                if (CustomerPersonalInfo != null && string.IsNullOrEmpty(CustomerPersonalInfo.Cbr) == false)
                {
					CustomerPersonalInfo.Cbr = Util.FormatContactNumber(CustomerPersonalInfo.Cbr);
					personalDetailsVM.Customer.ContactNo = CustomerPersonalInfo.Cbr;
                }
                else
                {
                    personalDetailsVM.Customer.ContactNo = string.Empty;
                }

                //Feb Drop UAT Bug 1671 - Wrong state being displayed
                //if (personalDetailsVM.Address.StateID == 0)
                //{
                //    personalDetailsVM.Address.StateID = 19;
                //}

                personalDetailsVM.fromSuppLine = _fromSuppLine;
                personalDetailsVM.editDisabled = _editDisabled;
            }
            /*
            else if (personalDetailsVM != null && personalDetailsVM.Customer != null)
            {
                personalDetailsVM.Customer.CardTypeID = 0;
                personalDetailsVM.Customer.NameOnCard = string.Empty;
                personalDetailsVM.Customer.CardNo = string.Empty;
                personalDetailsVM.Customer.CardExpiryDate = null;
            }
            */
            return personalDetailsVM;

            #endregion "Personal Details Formation and customer for the registration"
        }
        #endregion "Forming Personal Details and Customer by ravi"

        /// <summary>
        /// Method used to return sim Type by ravi
        /// </summary>
        /// <returns>string</returns>
        public static string SIMReplacementSIMType()
        {
            string SimType = "Normal";
            SimReplacementType objSimType = (SimReplacementType)HttpContext.Current.Session["SimReplacementType"].ToInt();
            switch (objSimType)
            {
                case SimReplacementType.N2M:
                case SimReplacementType.M2M:
                    SimType = "MISM";
                    break;
                case SimReplacementType.PREPAID:
                    SimType = "Prepaid";
                    break;
                case SimReplacementType.HSDPA:
                    SimType = "HSDPA";
                    break;
                case SimReplacementType.FWBB:
                    SimType = "FWBB";
                    break;
                default: break;

            }
            return SimType;
        }

        public static bool IsK2COntract(string componentid)
        {
            bool result = false;


            SmartRegistrationServiceProxy proxy = new SmartRegistrationServiceProxy();
            IEnumerable<Online.Registration.DAL.Models.Contracts> lstContractsAll = proxy.GetContractsList().ToList();
            int isdevicecontract = proxy.GetContractsList().Where(d => d.KenanCode == componentid).FirstOrDefault().IsDeviceContract;

            if (isdevicecontract == 1)
            {
                result = true;
            }

            return result;
        }
        #endregion


        /// <summary>
        /// This method is used to log the exceptions in text file
        /// </summary>
        /// <param name="objException"></param>
        public static string LogException(Exception objException)
        {
            StringBuilder strException = new StringBuilder();
            strException.AppendFormat("Exception Found:\n{0}", objException.GetType().FullName);
            strException.AppendFormat("\n{0}", objException.Message);
            strException.AppendFormat("\n{0}", objException.Source);
            strException.AppendFormat("\n{0}", objException.StackTrace);
            if (objException.InnerException != null)
            {
                strException.AppendFormat("Inner Exception Found:\n{0}", objException.InnerException.GetType().FullName);
                strException.AppendFormat("\n{0}", objException.InnerException.Message);
                strException.AppendFormat("\n{0}", objException.InnerException.Source);
                strException.AppendFormat("\n{0}", objException.InnerException.StackTrace);
            }
            return strException.ToString();

        }

        #region "Waiver Table Info"

        private static string FormWaiverTableROW(List<DAL.Models.WaiverComponents> objWaiverComponents)
        {
            System.Text.StringBuilder strWaiverTableRow = new System.Text.StringBuilder();
            foreach (var waiverComp in objWaiverComponents)
            {
                waiverComp.ComponentName = (waiverComp.ComponentName).ToUpper().Contains("PENALTY")?waiverComp.ComponentName.Replace("Penalty",Settings.Default.EarlyTerminationFee):waiverComp.ComponentName;
                strWaiverTableRow.Append("<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + waiverComp.ComponentName + "</td>");

                string checkboxHtml = "<input type='checkbox' disabled='disabled' class='clsWaiver' value='" + waiverComp.ComponentName + "~" + waiverComp.ComponentID + "~" + waiverComp.price.ToString() + "~" + waiverComp.MismType.ToString() + "' />";
                string strDealerWaiver = "Not Waived";//09072015 - Anthony - Show the indicator whether the advance payment is waived or not waived
                //string strDealerWaiver = "";
                if (waiverComp.IsWaived == true)
                {
                    checkboxHtml = "<input type='checkbox' disabled='disabled' class='clsWaiver' checked value='" + waiverComp.ComponentName + "~" + waiverComp.ComponentID + "~" + waiverComp.price.ToString() + "~" + waiverComp.MismType.ToString() + "' />";
                    strDealerWaiver = "Waived";
                }
                if (HttpContext.Current.Session["IsDealer"].ToString() == "False")
                {
                    //strWaiverTableRow.Append("<td id='summary-data' style='white-space:nowrap;'>RM" + waiverComp.price.ToString() + "</td ><td style='white-space:nowrap;'>" + checkboxHtml + "</td></tr>");
                    //strWaiverTableRow.Append("<td id='summary-data' style='white-space:nowrap;'>" + "" + "</td ><td style='white-space:nowrap;'>" + checkboxHtml + "</td></tr>");
                    strWaiverTableRow.Append("<td id='summary-data' style='white-space:nowrap;'>RM" + waiverComp.price.ToString() + "</td ><td style='white-space:nowrap;'>" + checkboxHtml + "</td></tr>");//12032015 - Anthony - Re-display the waiver stuff with the correct view (as per Production behaviour) for CRP & Add Contract
                }
                else
                {
                    //strWaiverTableRow.Append("<td id='summary-data' style='white-space:nowrap;'>RM" + waiverComp.price.ToString() + "</td><td id='summary-data' style='white-space:nowrap;color:red;'>" + strDealerWavier + "</td><td style='white-space:nowrap;display:none;'>" + checkboxHtml + "</td></tr>");
                    //strWaiverTableRow.Append("<td id='summary-data' style='white-space:nowrap;'>" + "" + "</td><td id='summary-data' style='white-space:nowrap;color:red;'>" + "" + "</td><td style='white-space:nowrap;display:none;'>" + "" + "</td></tr>");
                    strWaiverTableRow.Append("<td id='summary-data' style='white-space:nowrap;'>RM" + waiverComp.price.ToString() + "</td><td id='summary-data' style='white-space:nowrap;color:red;'>" + strDealerWaiver + "</td><td style='white-space:nowrap;display:none;'>" + checkboxHtml + "</td></tr>");//12032015 - Anthony - Re-display the waiver stuff with the correct view (as per Production behaviour) for CRP & Add Contract
                }
            }
            return strWaiverTableRow.ToString();
        }


        private static string FormWaiverTableROWNew(List<DAL.Models.WaiverComponents> objWaiverComponents)
        {
            System.Text.StringBuilder strWaiverTableRow = new System.Text.StringBuilder();
            foreach (var waiverComp in objWaiverComponents)
            {
                // Habs - code clean-up - start
                String checkboxHtml = Util.FormTableRowNewWaiver(
                    waiverComp.ComponentName,
                    waiverComp.price.ToString(),
                    waiverComp.ComponentID.ToString(),
                    waiverComp.MismType,
                    "CBWaiver",
                    waiverComp.MismType);

                strWaiverTableRow.Append(checkboxHtml);

                /*
                if (HttpContext.Current.Session["IsDealer"].ToString() == "False")
                {
                    checkboxHtml = "<input type='checkbox' class='clsWaiver' checked value='" + waiverComp.ComponentName + "~" + waiverComp.ComponentID + "~" + waiverComp.price.ToString() + "~" + waiverComp.MismType.ToString() + "' />";
                }
                else
                {
                    checkboxHtml = "<input type='checkbox' disabled='disabled' class='clsWaiver' checked value='" + waiverComp.ComponentName + "~" + waiverComp.ComponentID + "~" + waiverComp.price.ToString() + "~" + waiverComp.MismType.ToString() + "' />";
                }
                strWaiverTableRow.Append("<span>" + checkboxHtml + "</span><span class='waiverSpan paddingLeft' data-compname='" + waiverComp.ComponentName + "' data-price='" + waiverComp.price + "' data-compid='" + waiverComp.ComponentID + "' data-msismtype='" + waiverComp.MismType + "'>" + waiverComp.ComponentName + " - RM" + waiverComp.price + "</span>");
                 */
                // Habs - code clean-up - end
            }
            return strWaiverTableRow.ToString();

        }

        public static string FormWaiverTable(int regType, int packageId, int planAdvance = 0, int regDeposit = 0, int deviceAdvance = 0, int deviceDeposit = 0, int regId = 0, bool isMnpMultiCheck = false, int upfrontpayment = 0, int isWaiverReq = 1, string penaltyWaiver = "", OrderSummaryVM orderSummary = null)
        {
            System.Text.StringBuilder strWaiverTabl = new System.Text.StringBuilder();
            if (regId > 0)
            {
                using (var proxy = new Registration.Web.Helper.RegistrationServiceProxy())
                {
                    List<DAL.Models.WaiverComponents> objWaiverComponents = proxy.GetWaiverComponentsbyRegID(regId);

                    objWaiverComponents = objWaiverComponents.Where(c => c.MismType != "c").ToList();

                    if (isMnpMultiCheck == true)
                    {

                        var objPrimaryList = objWaiverComponents.Where(e => e.MismType.ToUpper() == "MP").ToList();
                        var objSecondaryList = objWaiverComponents.Where(e => e.MismType.ToUpper() != "MP").ToList();
                        strWaiverTabl.Append("<tr class='title'><td>Primary MSISDN</td></tr>");

                        //Primary
                        strWaiverTabl.Append(FormWaiverTableROW(objPrimaryList));

                        //Supplementary Lines
                        for (int supleMsisdnsCnt = 1; supleMsisdnsCnt <= objSecondaryList.Count; supleMsisdnsCnt++)
                        {
                            string curMsisdn = "ms" + supleMsisdnsCnt.ToString();
                            var curSecondaryList = objSecondaryList.Where(e => e.MismType.ToLower() == curMsisdn).ToList();

                            if (curSecondaryList.Count > 0)
                            {
                                strWaiverTabl.Append("<tr class='title'><td>Supp MSISDN " + supleMsisdnsCnt.ToString() + "</td></tr>");
                                //Supple Lines
                                strWaiverTabl.Append(FormWaiverTableROW(curSecondaryList));
                            }
                        }

                        return "<table>" + strWaiverTabl.ToString() + "</table>";

                    }
                    else
                    {

                        //MSIM OR NORMAL ONE
                        bool isMsim = objWaiverComponents.Count > 0 ? (objWaiverComponents[0].MismType.ToLower() == "n" ? false : true) : false;
                        if (isMsim == true)
                        {
                            System.Text.StringBuilder strWaiverTablSecondary = new System.Text.StringBuilder();
                            var objPrimaryList = objWaiverComponents.Where(e => e.MismType.ToLower() == "p").ToList();
                            var objSecondaryList = objWaiverComponents.Where(e => e.MismType.ToLower() == "s").ToList();

                            strWaiverTabl.Append(FormWaiverTableROW(objPrimaryList));
                            strWaiverTablSecondary.Append(FormWaiverTableROW(objSecondaryList));

                            string FinalTable = "<table><tr><td>";
                            if (strWaiverTabl.Length > 0)
                            {
                                strWaiverTabl.Append("</table>");
                                FinalTable += "<table><tr class='title'><td>Primary</td></tr>" + strWaiverTabl.ToString();

                            }
                            if (strWaiverTablSecondary.Length > 0)
                            {
                                strWaiverTablSecondary.Append("</table>");
                                FinalTable += "<table><tr class='title'><td>Secondary</td></tr>" + strWaiverTablSecondary.ToString();
                            }
                            FinalTable += "</td></tr></table>";
                            return FinalTable;

                        }
                        else
                        {
                            strWaiverTabl.Append(FormWaiverTableROW(objWaiverComponents));
                        }
                    }

                }
            }
            else
            {
                if (regType != (int)MobileRegType.AddRemoveVAS)
                {
                    WaiverRules waiverRule = HttpContext.Current.Session[SessionKey.WaiverRules.ToString()] != null ? (WaiverRules)HttpContext.Current.Session["WaiverRules"] : new WaiverRules();
                    //21042015 - Anthony - Display the correct waiver information - Start
                    orderSummary = orderSummary ?? new OrderSummaryVM();
                    //var hasDevice = WebHelper.Instance.determineIfGotDevice(orderSummary);
                    var hasDevice = ((orderSummary.ContractID != null && orderSummary.ContractID != 0) || (orderSummary.ModelID != null && orderSummary.ModelID != 0)) ? true : false;
                    bool isMalaysian = WebHelper.Instance.determineNationality() == "1"
                                            ? true
                                            : HttpContext.Current.Session[SessionKey.IDCardTypeID.ToString()].ToInt() != 2
                                                    ? true
                                                    : false;
                    //21042015 - Anthony - Display the correct waiver information - End
                    var isPlanAdvanceAutoWaived = orderSummary.isPlanAdvanceAutoWaived;
                    var isPlanDepositAutoWaived = orderSummary.isPlanDepositAutoWaived;
                    var isDeviceAdvanceAutoWaived = orderSummary.isDeviceAdvanceAutoWaived;
                    var isDeviceDepositAutoWaived = orderSummary.isDeviceDepositAutoWaived;
                    var unwaivedPrice = orderSummary.unwaivedAdvancePaymentDeposit;
                    decimal unwaivedPlanAdvance = 0;
                    decimal unwaivedPlanDeposit = 0;
                    decimal unwaivedDeviceAdvance = 0;
                    decimal unwaivedDeviceDeposit = 0;

                    //06052015 - Anthony - to display either device advance or ivalue advance - Start
                    if (unwaivedPrice != null)
                    {
                        if (isMalaysian == true)
                        {
                            unwaivedPlanAdvance = unwaivedPrice.malayPlanAdv;
                            unwaivedPlanDeposit = unwaivedPrice.malyPlanDeposit;
                            unwaivedDeviceAdvance = unwaivedPrice.malyDevAdv;
                            unwaivedDeviceDeposit = unwaivedPrice.malayDevDeposit;
                        }
                        else
                        {
                            unwaivedPlanAdvance = unwaivedPrice.othPlanAdv;
                            unwaivedPlanDeposit = unwaivedPrice.othPlanDeposit;
                            unwaivedDeviceAdvance = unwaivedPrice.othDevAdv;
                            unwaivedDeviceDeposit = unwaivedPrice.othDevDeposit;
                        }
                    }
                    //06052015 - Anthony - to display either device advance or ivalue advance - End

                    //if (planAdvance > 0)//12032015 - Anthony - Re-display the waiver stuff with the correct view (as per Production behaviour) for CRP & Add Contract
                    if (isMalaysian == true && hasDevice == false)//21042015 - Anthony - Display the correct waiver information
                    {
                        strWaiverTabl.Append(FormTableRow("Plan Advance", planAdvance.ToString("F2"), "0", "n", isPlanAdvanceAutoWaived, unwaivedPlanAdvance));
                    }
                    //if (regDeposit > 0)//12032015 - Anthony - Re-display the waiver stuff with the correct view (as per Production behaviour) for CRP & Add Contract
                    if (isMalaysian == false && regDeposit > 0)//21042015 - Anthony - Display the correct waiver information
                    {
                        strWaiverTabl.Append(FormTableRow("Registration Deposit", regDeposit.ToString("F2"), "0", "n", isPlanDepositAutoWaived, unwaivedPlanDeposit));
                    }
                    if (regType == (int)MobileRegType.DevicePlan || regType == (int)MobileRegType.DeviceCRP || regType == (int)MobileRegType.Contract)
                    {
                        //if (deviceAdvance > 0)//12032015 - Anthony - Re-display the waiver stuff with the correct view (as per Production behaviour) for CRP & Add Contract
                        if (hasDevice == true)//21042015 - Anthony - Display the correct waiver information
                        {
                            if (unwaivedDeviceAdvance > 0)
                                strWaiverTabl.Append(FormTableRow("Device Advance", deviceAdvance.ToString("F2"), "0", "n", isDeviceAdvanceAutoWaived, unwaivedDeviceAdvance));
                            //}
                            //if (deviceDeposit > 0)//12032015 - Anthony - Re-display the waiver stuff with the correct view (as per Production behaviour) for CRP & Add Contract
                            //{
                            if (unwaivedDeviceDeposit > 0)
                                strWaiverTabl.Append(FormTableRow(Constants.IVALUE_ADVANCE, deviceDeposit.ToString("F2"), "0", "n", isDeviceDepositAutoWaived, unwaivedDeviceDeposit));
                        }
                        if (upfrontpayment > 0 && isWaiverReq != 1)//12032015 - Anthony - Re-display the waiver stuff with the correct view (as per Production behaviour) for CRP & Add Contract
                        {
                            strWaiverTabl.Append(FormTableRow("Upfront Payment", upfrontpayment.ToString("F2"), "0", "n"));
                        }
                        if (penaltyWaiver != null && penaltyWaiver != "" && Convert.ToDecimal(penaltyWaiver) == 0)
                        {
                            strWaiverTabl.Append(FormTableRow(Settings.Default.EarlyTerminationFee + " Waiver", penaltyWaiver, "0", "n", true));
                        }

                    }
                }


                if (packageId > 0)
                {
                    strWaiverTabl.Append(FormWaiverComponents(packageId, (List<string>)HttpContext.Current.Session[SessionKey.RegMobileReg_VasNames.ToString()], "n", regType));

                }
            }
            if (strWaiverTabl.Length > 0)
            {
                strWaiverTabl.Append("</table>");
                return "<table>" + strWaiverTabl.ToString();
            }
            else
            {
                return "Not Available";
            }

        }

        public static string FormWaiverTableNew(int regType, string indicator, int packageId, int planAdvance = 0, int regDeposit = 0, int deviceAdvance = 0, int deviceDeposit = 0, int regId = 0, bool isMnpMultiCheck = false, int upfrontpayment = 0, int isWaiverReq = 1, string penaltyWaiver = "")
        {
            System.Text.StringBuilder strWaiverTabl = new System.Text.StringBuilder();
            if (regId > 0)
            {
                using (var proxy = new Registration.Web.Helper.RegistrationServiceProxy())
                {
                    List<DAL.Models.WaiverComponents> objWaiverComponents = proxy.GetWaiverComponentsbyRegID(regId);

                    objWaiverComponents = objWaiverComponents.Where(c => c.MismType != "c").ToList();

                    if (isMnpMultiCheck == true)
                    {
                        var objPrimaryList = objWaiverComponents.Where(e => e.MismType.ToUpper() == "MP").ToList();
                        var objSecondaryList = objWaiverComponents.Where(e => e.MismType.ToUpper() != "MP").ToList();

                        strWaiverTabl.Append(FormWaiverTableROWNew(objPrimaryList));
                        for (int supleMsisdnsCnt = 1; supleMsisdnsCnt <= objSecondaryList.Count; supleMsisdnsCnt++)
                        {
                            string curMsisdn = "ms" + supleMsisdnsCnt.ToString();
                            var curSecondaryList = objSecondaryList.Where(e => e.MismType.ToLower() == curMsisdn).ToList();
                            if (curSecondaryList.Count > 0)
                            {
                                strWaiverTabl.Append(FormWaiverTableROWNew(curSecondaryList));
                            }
                        }
                        return strWaiverTabl.ToString();
                    }
                    else
                    {
                        bool isMsim = objWaiverComponents.Count > 0 ? (objWaiverComponents[0].MismType.ToLower() == "n" ? false : true) : false;
                        if (isMsim == true)
                        {
                            System.Text.StringBuilder strWaiverTablSecondary = new System.Text.StringBuilder();
                            var objPrimaryList = objWaiverComponents.Where(e => e.MismType.ToLower() == "p").ToList();
                            var objSecondaryList = objWaiverComponents.Where(e => e.MismType.ToLower() == "s").ToList();



                            strWaiverTabl.Append(FormWaiverTableROWNew(objPrimaryList));
                            strWaiverTablSecondary.Append(FormWaiverTableROWNew(objSecondaryList));

                            string FinalTable = "";
                            if (strWaiverTabl.Length > 0)
                            {
                                FinalTable += strWaiverTabl.ToString();
                            }
                            if (strWaiverTablSecondary.Length > 0)
                            {
                                FinalTable += strWaiverTablSecondary.ToString();
                            }
                            return FinalTable;
                        }
                        else
                        {
                            strWaiverTabl.Append(FormWaiverTableROWNew(objWaiverComponents));
                        }
                    }

                }
            }
            else
            {
                if (regType != (int)MobileRegType.AddRemoveVAS)
                {
                    WaiverRules waiverRule = HttpContext.Current.Session[SessionKey.WaiverRules.ToString()] != null ? (WaiverRules)HttpContext.Current.Session["WaiverRules"] : new WaiverRules();
                    string checkLine = "n";
                    if (indicator.Contains("S"))
                    {
                        checkLine = indicator;
                    }
                    if (regDeposit > 0)
                    {
                        strWaiverTabl.Append(FormTableRowNew("Registration Deposit", regDeposit.ToString("F2"), "0", checkLine, waiverRule.PlanDeposit));
                    }
                    if (planAdvance > 0)
                    {
                        strWaiverTabl.Append(FormTableRowNew("Plan Advance", planAdvance.ToString("F2"), "0", checkLine, waiverRule.PlanAdv));
                    }
                    if (regType == (int)MobileRegType.DevicePlan || regType == (int)MobileRegType.DeviceCRP || regType == (int)MobileRegType.Contract)
                    {
                        if (deviceDeposit > 0)
                        {
                            strWaiverTabl.Append(FormTableRowNew(Constants.IVALUE_ADVANCE, deviceDeposit.ToString("F2"), "0", checkLine, waiverRule.DeviceDeposit));

                        }
                        if (deviceAdvance > 0)
                        {
                            strWaiverTabl.Append(FormTableRowNew("Device Advance", deviceAdvance.ToString("F2"), "0", checkLine, waiverRule.DeviceAdv));

                        }
                        if (upfrontpayment > 0 && isWaiverReq != 1)
                        {
                            strWaiverTabl.Append(FormTableRowNew("Upfront Payment", upfrontpayment.ToString("F2"), "0", checkLine));
                        }
                        if (penaltyWaiver != null && penaltyWaiver != "" && Convert.ToDecimal(penaltyWaiver) > 0)
                        {
                            strWaiverTabl.Append(FormTableRowNew(Settings.Default.EarlyTerminationFee + " Waiver", penaltyWaiver, "0", checkLine, true));
                        }

                    }
                }


                if (packageId > 0)
                {
                    strWaiverTabl.Append(FormWaiverComponentsNew(packageId, (List<string>)HttpContext.Current.Session[SessionKey.RegMobileReg_VasNames.ToString()], "n", regType));

                }
            }

            if (strWaiverTabl.Length > 0)
            {
                return strWaiverTabl.ToString();
            }
            else
            {
                return "Not Available";
            }

        }

        //Evan add before unused parameter (checked only seven Parameters used / all defaults)
        //public static string FormWaiverTableNewCB(int regType, string indicator, int packageId, int planAdvance = 0, int regDeposit = 0, int deviceAdvance = 0, int deviceDeposit = 0, int regId = 0, bool isMnpMultiCheck = false, int upfrontpayment = 0, int isWaiverReq = 1, string penaltyWaiver = "")
        public static string FormWaiverTableNewCB(int regType, string indicator, int packageId, int planAdvance = 0, int regDeposit = 0, int deviceAdvance = 0, int deviceDeposit = 0, string ivalIndicator = "plan", int regId = 0, bool isMnpMultiCheck = false, int upfrontpayment = 0, int isWaiverReq = 1, string penaltyWaiver = "", OrderSummaryVM orderSummaryVM = null)
        {//Anthony-[#2046]
            System.Text.StringBuilder strWaiverTabl = new System.Text.StringBuilder();
            //var ivalIndicator = indicator.Split(',')[1];
            if (regId > 0)
            {
                using (var proxy = new Registration.Web.Helper.RegistrationServiceProxy())
                {
                    List<DAL.Models.WaiverComponents> objWaiverComponents = proxy.GetWaiverComponentsbyRegID(regId);

                    objWaiverComponents = objWaiverComponents.Where(c => c.MismType != "c").ToList();

                    if (isMnpMultiCheck == true)
                    {
                        var objPrimaryList = objWaiverComponents.Where(e => e.MismType.ToUpper() == "MP").ToList();
                        var objSecondaryList = objWaiverComponents.Where(e => e.MismType.ToUpper() != "MP").ToList();

                        strWaiverTabl.Append(FormWaiverTableROWNew(objPrimaryList));
                        for (int supleMsisdnsCnt = 1; supleMsisdnsCnt <= objSecondaryList.Count; supleMsisdnsCnt++)
                        {
                            string curMsisdn = "ms" + supleMsisdnsCnt.ToString();
                            var curSecondaryList = objSecondaryList.Where(e => e.MismType.ToLower() == curMsisdn).ToList();
                            if (curSecondaryList.Count > 0)
                            {
                                strWaiverTabl.Append(FormWaiverTableROWNew(curSecondaryList));
                            }
                        }
                        return strWaiverTabl.ToString();
                    }
                    else
                    {
                        bool isMsim = objWaiverComponents.Count > 0 ? (objWaiverComponents[0].MismType.ToLower() == "n" ? false : true) : false;
                        if (isMsim == true)
                        {
                            System.Text.StringBuilder strWaiverTablSecondary = new System.Text.StringBuilder();
                            var objPrimaryList = objWaiverComponents.Where(e => e.MismType.ToLower() == "p").ToList();
                            var objSecondaryList = objWaiverComponents.Where(e => e.MismType.ToLower() == "s").ToList();



                            strWaiverTabl.Append(FormWaiverTableROWNew(objPrimaryList));
                            strWaiverTablSecondary.Append(FormWaiverTableROWNew(objSecondaryList));

                            string FinalTable = "";
                            if (strWaiverTabl.Length > 0)
                            {
                                FinalTable += strWaiverTabl.ToString();
                            }
                            if (strWaiverTablSecondary.Length > 0)
                            {
                                FinalTable += strWaiverTablSecondary.ToString();
                            }
                            return FinalTable;
                        }
                        else
                        {
                            strWaiverTabl.Append(FormWaiverTableROWNew(objWaiverComponents));
                        }
                    }

                }
            }
            else
            {
                if (regType != (int)MobileRegType.AddRemoveVAS)
                {
                    WaiverRules waiverRule = HttpContext.Current.Session[SessionKey.WaiverRules.ToString()] != null ? (WaiverRules)HttpContext.Current.Session["WaiverRules"] : new WaiverRules();
                    string checkLine = "n";
                    orderSummaryVM = orderSummaryVM ?? new OrderSummaryVM();
                    var fromAddContractOrCRP = HttpContext.Current.Session["NoRegistrationForCRP"].ToInt() == MobileRegType.CRP.ToInt() || HttpContext.Current.Session["NoRegistrationForCRP"].ToInt() == MobileRegType.Contract.ToInt();
                    var hasDevice = (regType == (int)MobileRegType.DevicePlan || regType == (int)MobileRegType.DeviceCRP || regType == (int)MobileRegType.Contract);
                    bool isMalaysian = WebHelper.Instance.determineNationality() == "1"
                                            ? true
                                            : HttpContext.Current.Session[SessionKey.IDCardTypeID.ToString()].ToInt() != 2
                                                    ? true
                                                    : false;
                    var isPlanAdvanceAutoWaived = orderSummaryVM.isPlanAdvanceAutoWaived;
                    var isPlanDepositAutoWaived = orderSummaryVM.isPlanDepositAutoWaived;
                    var isDeviceAdvanceAutoWaived = orderSummaryVM.isDeviceAdvanceAutoWaived;
                    var isDeviceDepositAutoWaived = orderSummaryVM.isDeviceDepositAutoWaived;
                    var unwaivedPrice = orderSummaryVM.unwaivedAdvancePaymentDeposit;
                    decimal unwaivedPlanAdvance = 0;
                    decimal unwaivedPlanDeposit = 0;
                    decimal unwaivedDeviceAdvance = 0;
                    decimal unwaivedDeviceDeposit = 0;

                    if (unwaivedPrice != null)
                    {
                        if (isMalaysian == true)
                        {
                            unwaivedPlanAdvance = unwaivedPrice.malayPlanAdv;
                            unwaivedPlanDeposit = unwaivedPrice.malyPlanDeposit;
                            unwaivedDeviceAdvance = unwaivedPrice.malyDevAdv;
                            unwaivedDeviceDeposit = unwaivedPrice.malayDevDeposit;
                        }
                        else
                        {
                            unwaivedPlanAdvance = unwaivedPrice.othPlanAdv;
                            unwaivedPlanDeposit = unwaivedPrice.othPlanDeposit;
                            unwaivedDeviceAdvance = unwaivedPrice.othDevAdv;
                            unwaivedDeviceDeposit = unwaivedPrice.othDevDeposit;
                        }
                    }

                    if (indicator.Contains("S"))
                    {
                        checkLine = indicator;
                    }

                    // habs - always display reg deposit - start
                    //if (regDeposit > 0)
                    //{
                    if (isMalaysian == false && fromAddContractOrCRP == false)
                    {
                        strWaiverTabl.Append(FormTableRowNewWaiver("Registration Deposit", regDeposit.ToString("F2"), "0", checkLine, "CBRegDeposit", indicator, isPlanDepositAutoWaived, unwaivedPlanDeposit));
                    }
                    //}
                    // habs - always display reg deposit - end

                    // habs - always display plan advanced - start
                    //if (planAdvance > 0)
                    //{
                    if (hasDevice == false && isMalaysian == true)
                    {
                        strWaiverTabl.Append(FormTableRowNewWaiver("Plan Advance", planAdvance.ToString("F2"), "0", checkLine, "CBPlanAdv", indicator, isPlanAdvanceAutoWaived, unwaivedPlanAdvance, fromAddContractOrCRP: fromAddContractOrCRP));
                    }
                    //}
                    // habs - always display plan advanced - end

                    if (regType == (int)MobileRegType.DevicePlan || regType == (int)MobileRegType.DeviceCRP || regType == (int)MobileRegType.Contract)
                    {
                        //23022015 - Anthony - Only Show the iValue Advance for iValue plan (Apple)
                        //15052015 - Anthony - to display either device advance or ivalue advance
                        //if (ivalIndicator.Equals("ivalue"))
                        //{
                            //20141210 - Evan Change iValue Deposit ? why display Advance Pymt (iValue), (not "device advance")
                            //strWaiverTabl.Append(FormTableRowNewWaiver(ivalIndicator == "ivalue" ? "iValue Advance" : Constants.IVALUE_ADVANCE, deviceDeposit.ToString("F2"), "0", checkLine, "CBAdvPymt", indicator, waiverRule.DeviceDeposit));
                            //20141126 - Habs - Why the label is Advance Pymt (iValue)? - start
                            //strWaiverTabl.Append(FormTableRowNewWaiver("Advance Pymt (iValue)", deviceDeposit.ToString("F2"), "0", checkLine, "CBAdvPymt", indicator, waiverRule.DeviceDeposit));
                            //strWaiverTabl.Append(FormTableRowNewWaiver(ivalIndicator == "ivalue" ? "iValue Deposit" : "Device Deposit", deviceDeposit.ToString("F2"), "0", checkLine, "CBAdvPymt", indicator, waiverRule.DeviceDeposit));
                            //20141126 - Habs - Why the label is Advance Pymt (iValue)? -end
                        //}
                        //15052015 - Anthony - to display either device advance or ivalue advance - Start
                        if (orderSummaryVM.unwaivedAdvancePaymentDeposit != null)
                        {
                            if (orderSummaryVM.unwaivedAdvancePaymentDeposit.malayDevDeposit > 0 || orderSummaryVM.unwaivedAdvancePaymentDeposit.othDevDeposit > 0)
                            {
                                strWaiverTabl.Append(FormTableRowNewWaiver(Constants.IVALUE_ADVANCE, deviceDeposit.ToString("F2"), "0", checkLine, "CBAdvPymt", indicator, isDeviceDepositAutoWaived, unwaivedDeviceDeposit, fromAddContractOrCRP: fromAddContractOrCRP));
                            }

                            if (orderSummaryVM.unwaivedAdvancePaymentDeposit.malyDevAdv > 0 || orderSummaryVM.unwaivedAdvancePaymentDeposit.othDevAdv > 0)
                            {
                                strWaiverTabl.Append(FormTableRowNewWaiver("Device Advance", deviceAdvance.ToString("F2"), "0", checkLine, "CBDevAdv", indicator, isDeviceAdvanceAutoWaived, unwaivedDeviceAdvance, fromAddContractOrCRP: fromAddContractOrCRP));
                            }
                        }
                        else
                        {
                            if (deviceDeposit > 0)
                            {
                                strWaiverTabl.Append(FormTableRowNewWaiver(Constants.IVALUE_ADVANCE, deviceDeposit.ToString("F2"), "0", checkLine, "CBAdvPymt", indicator, isDeviceDepositAutoWaived, unwaivedDeviceDeposit, fromAddContractOrCRP: fromAddContractOrCRP));
                            }

                            if (deviceAdvance > 0)
                            {
                                strWaiverTabl.Append(FormTableRowNewWaiver("Device Advance", deviceAdvance.ToString("F2"), "0", checkLine, "CBDevAdv", indicator, isDeviceAdvanceAutoWaived, unwaivedDeviceAdvance, fromAddContractOrCRP: fromAddContractOrCRP));
                            }
                        }
                        //15052015 - Anthony - to display either device advance or ivalue advance - End
                        // habs - always display device advance even if the price is zero - start
                        //if (deviceAdvance > 0)
                        //{
                        //15052015 - Anthony - to display either device advance or ivalue advance
                        //strWaiverTabl.Append(FormTableRowNewWaiver("Device Advance", deviceAdvance.ToString("F2"), "0", checkLine, "CBDevAdv", indicator, waiverRule.DeviceAdv));
                        //}
                        // habs - always display plan advanced - end


                        if (upfrontpayment > 0 && isWaiverReq != 1)
                        {
                            strWaiverTabl.Append(FormTableRowNewWaiver("Upfront Payment", upfrontpayment.ToString("F2"), "0", checkLine, "CBUpfrontPymt", indicator, false));
                        }
                        if (penaltyWaiver != null && penaltyWaiver != "" && Convert.ToDecimal(penaltyWaiver) > 0)
                        {
                            strWaiverTabl.Append(FormTableRowNewWaiver(Settings.Default.EarlyTerminationFee + " Waiver", penaltyWaiver, "0", checkLine, "CBPenaltyWaiver", indicator, true));
                        }

                    }
                }


                if (packageId > 0)
                {
                    strWaiverTabl.Append(FormWaiverComponentsNew(packageId, (List<string>)HttpContext.Current.Session[SessionKey.RegMobileReg_VasNames.ToString()], "n", regType));

                }
            }

            if (strWaiverTabl.Length > 0)
            {
                return strWaiverTabl.ToString();
            }
            else
            {
                return "Not Available";
            }

        }

        private static string FormTableRow(string compName, string price, string compId, string msismType, bool isAutoWaived = false, decimal originalPrice = 0)
        {
            var isDealer = HttpContext.Current.Session["IsDealer"].ToString2() != "False";
            String classString = "clsWaiver";
            String check = "";
            String autoWaivedText = "Not Waived";
            string displayedPrice = price;

            if ((price == "0.00" || price == "0") && originalPrice > 0)
            {
                displayedPrice = originalPrice.ToString("F2");
            }

            if (price == "0.00" || price == "0")
            {
                classString = "";
                autoWaivedText = "Waived";
                check = "checked='checked' disabled='true'";
            }
            //11032015 - Anthony - Re-display the waiver stuff with the correct view for CRP & Add Contract - End
            /*
            if (tobeChecked)
            {
                if (HttpContext.Current.Session["IsDealer"].ToString() == "False")
                {
                    //return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>RM" + price + "</td><td style='white-space:nowrap;'><input type='checkbox' style='display:none;' checked='checked' disabled='true' class='clsWaiver' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";
                    //return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>" + "" + "</td><td style='white-space:nowrap;'><input type='checkbox' style='display:none;' checked='checked' disabled='true' class='clsWaiver' value='" + compName + "~" + compId + "~" + "" + "~" + msismType + "' /></td></tr>";
                    return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>RM" + price + "</td><td style='white-space:nowrap;'><input type='checkbox' checked='checked' disabled='true' class='clsWaiver' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";//11032015 - Anthony - Re-display the waiver stuff with the correct view for CRP & Add Contract
                }
                else
                {
                    //return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>RM" + price + "</td><td id='summary-data' style='color:red;'>Waived" + "</td><td style='white-space:nowrap;display:none;'><input type='checkbox' style='display:none;' checked='checked' disabled='true' class='clsWaiver' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";
                    //return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>" + "" + "</td><td id='summary-data' style='color:red;'>" + "</td><td style='white-space:nowrap;display:none;'><input type='checkbox' style='display:none;' checked='checked' disabled='true' class='clsWaiver' value='" + compName + "~" + compId + "~" + "" + "~" + msismType + "' /></td></tr>";
                    return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>RM" + price + "<td style='white-space:nowrap;display:none;'><input type='checkbox' checked='checked' disabled='true' class='clsWaiver' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";//11032015 - Anthony - Re-display the waiver stuff with the correct view for CRP & Add Contract
                }
            }
            else
            {
                if (HttpContext.Current.Session["IsDealer"].ToString() == "False")
                {
                    //return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>RM" + price + "</td><td style='white-space:nowrap;'><input type='checkbox' style='display:none;' class='clsWaiver' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";
                    //return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>" + "" + "</td><td style='white-space:nowrap;'><input type='checkbox' style='display:none;' class='clsWaiver' value='" + compName + "~" + compId + "~" + "" + "~" + msismType + "' /></td></tr>";
                    return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>RM" + price + "</td><td style='white-space:nowrap;'><input type='checkbox' class='clsWaiver' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";//11032015 - Anthony - Re-display the waiver stuff with the correct view for CRP & Add Contract
                }
                else
                {
                    //return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>RM" + price + "</td><td id='summary-data' style='color:red;'>Not Waived" + "</td><td style='white-space:nowrap;display:none;'><input type='checkbox' style='display:none;' class='clsWaiver' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";
                    //return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>" + "" + "</td><td id='summary-data' style='color:red;'>" + "</td><td style='white-space:nowrap;display:none;'><input type='checkbox' style='display:none;' class='clsWaiver' value='" + compName + "~" + compId + "~" + "" + "~" + msismType + "' /></td></tr>";
                    return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td  id='summary-data' style='white-space:nowrap;'>RM" + price + "</td><td id='summary-data' style='color:red;'>Not Waived" + "</td><td style='white-space:nowrap;display:none;'><input type='checkbox' style='display:none;' class='clsWaiver' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";//11032015 - Anthony - Re-display the waiver stuff with the correct view for CRP & Add Contract
                }
            }
            */
            if (!isDealer)
            {   /* if (price != "0.00")
                {
                    return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td id='summary-data' style='white-space:nowrap;'>RM" + displayedPrice + "</td><td style='white-space:nowrap;'><input type='checkbox' class='" + classString + "' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";
                }
                else
                {
                    return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td id='summary-data' style='white-space:nowrap;'>RM" + price + "</td><td style='white-space:nowrap;'><input type='checkbox' " + check + " value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";
                }*/
                if (price != "0.00")
                {
                    return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td id='summary-data' style='white-space:nowrap;'>RM" + displayedPrice + "</td><td style='white-space:nowrap;'><input type='checkbox' class='" + classString + "' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";
                }
                else
                {
                    return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td id='summary-data' style='white-space:nowrap;'>RM" + price + "</td><td style='white-space:nowrap;'><input type='checkbox'" + check + " value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";
                }
            }
            else
            {
                if (price != "0.00")
                {
                    return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td id='summary-data' style='white-space:nowrap;'>RM" + displayedPrice + "</td><td id='summary-data' style='color:red;'>" + autoWaivedText + "</td><td style='white-space:nowrap;display:none;'><input type='checkbox' style='display:none;' class='" + classString + "' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";
                }
                else
                {
                    return "<tr><td id='summary-label' style='white-space:nowrap;width:50%;' >" + compName + "</td><td id='summary-data' style='white-space:nowrap;'>RM" + price + "</td><td id='summary-data' style='color:red;'>" + autoWaivedText + "</td><td style='white-space:nowrap;display:none;'><input type='checkbox' style='display:none;' " + check + " value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></td></tr>";
                }
            }
        }

        private static string FormTableRowNew(string compName, string price, string compId, string msismType, bool tobeChecked = false)
        {

            // Habs - Implement Auto Waiver for AutoDebit - start
            String classString = "";
            if ("CBPlanAdv" == compName)
            {
                classString = "clsWaiver";
            }

            if ("Mobile - Access International Roaming" == compName || "Mobile - Call Conferencing" == compName)
            {
                classString = "clsCompWaiver";
            }
            // Habs - Implement Auto Waiver for AutoDebit - end

            if (HttpContext.Current.Session["IsDealer"].ToString() == "False")
            {
                return "<span><input type='checkbox' class='" + classString + "' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></span><span class='waiverSpan paddingLeft' data-compname='" + compName + "' data-price='" + price + "' data-compid='" + compId + "' data-msismtype='" + msismType + "'>" + compName + " - RM" + price + "</span><br/>";
            }
            else
            {
                return "<span><input type='checkbox' disabled='true' class='" + classString + "' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></span><span class='waiverSpan paddingLeft' data-compname='" + compName + "' data-price='" + price + "' data-compid='" + compId + "' data-msismtype='" + msismType + "'>" + compName + " - RM" + price + "</span><br/>";
            }
        }


        public static string FormTableRowNewWaiverDisplay(string compName, decimal origPrice, bool waived = false, bool ivalue = false)
        {
            bool isDealer = Util.SessionAccess.User.isDealer;
            String checkboxChecked = "";
            String autoWaivedText = "";

            if (waived)
            {
                checkboxChecked = "checked='checked'";
                autoWaivedText = " (Waived)";
            }
            else
            {
                autoWaivedText = " (Not Waived)";
            }

            //return "<span></span><span class='waiverSpan paddingLeft'>" + compName + " - RM" + origPrice + autoWaivedText + "</span><br/>";
            //09122014 - Anthony - Modify autowaiver display for Dealer credential
            if (!isDealer)
            {
                return "<div class='row'>"
                            + "<div class='col-xs-6'><b>" + compName + "</b></div>"
                            + "<div class='col-xs-3'>RM" + origPrice + autoWaivedText + "</div>"
                            + "<div class='col-xs-3'><input type='checkbox' disabled='true' " + checkboxChecked + "/></div>"
                     + "</div>";
            }
            else
            {
                return "<div class='row'>"
                            + "<div class='col-xs-6'><b>" + compName + "</b></div>"
                            + "<div class='col-xs-3'>RM" + origPrice + autoWaivedText + "</div>"
                     + "</div>";
            }
        }

        private static string FormTableRowNewWaiver(string compName, string price, string compId, string msismType, string checkBoxName, string id, bool isAutoWaived = false, decimal originalPrice = 0, bool fromAddContractOrCRP = false)
        {//Anthony-[#2046]

            // Habs - Implement Auto Waiver for AutoDebit - start
            String classString = "clsWaiver";
            String hiddenInput = "";

            //30012015 - Anthony - Waiver CR: Direct Debit is only for local (Malaysian) customer - Start
            retrieveAcctListByICResponse allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
            var customerNationality = string.Empty;
            var idCardTypeID = HttpContext.Current.Session[SessionKey.IDCardTypeID.ToString()].ToInt();//"2: PASSPORT"
            var isMNPNewRegistration = (HttpContext.Current.Session["FromMNP"].ToBool() && HttpContext.Current.Session[SessionKey.KenanACNumber.ToString()].ToInt() == 0);//04032015 - Anthony - MNP New Registration for Foreigner will be treated as New Customer

            //12052015 - Anthony - Add an additional flag to determine the MNP Newline with Supplementary line - Start
            if (isMNPNewRegistration == true)
            {
                var isMNPNewlineWithSupplementary = HttpContext.Current.Session[SessionKey.isMNPNewlineWithSupplementary.ToString()].ToBool();

                if (isMNPNewlineWithSupplementary == true)
                {
                    isMNPNewRegistration = false;
                }
            }
            //12052015 - Anthony - Add an additional flag to determine the MNP Newline with Supplementary line - End

            if (!ReferenceEquals(allCustomerDetails, null))
            {
                foreach (var customerDetail in allCustomerDetails.itemList)
                {
                    if (!ReferenceEquals(customerDetail.Customer, null))
                    {
                        if (!string.IsNullOrEmpty(customerDetail.Customer.Nation))
                        {
                            customerNationality = customerDetail.Customer.Nation;
                            break;
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(customerNationality) && !ReferenceEquals(allCustomerDetails, null))
            {
                customerNationality = WebHelper.Instance.determineNationality();
            }

            if (customerNationality.Equals("1") || idCardTypeID != 2)
            {
                if ("Plan Advance" == compName)
                {
                    classString = "clsDirectDebitWaiver";
                    hiddenInput = "<input type='text' style='display: none' readonly='true' disabled='true' class='clsDirectDebitWaiverHidden' name='CBWaiver' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' />";
                }
            }//30012015 - Anthony - Waiver CR: Direct Debit is only for local (Malaysian) customer - End
            else if (customerNationality.Equals("2") && !isMNPNewRegistration)//03032015 - Anthony - Add auto waiver rule for Foreigner Customer - Start
            {
                if ("Registration Deposit" == compName)
                {
                    classString = "clsDirectDebitWaiver";
                    hiddenInput = "<input type='text' style='display: none' readonly='true' disabled='true' class='clsDirectDebitWaiverHidden' name='CBWaiver' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' />";
                }
            }
            //03032015 - Anthony - Add auto waiver rule for Foreigner Customer - End
            // Habs - Implement Auto Waiver for AutoDebit - end

            // habs - Disable checkbox if the price is 0, because there is nothing to wave - start
            String check = "";
            String autoWaivedText = "";
            string displayedPrice = price;

            if ("0.00" == price && originalPrice > 0)
            {
                displayedPrice = originalPrice.ToString("F2");
            }

            if ("0.00" == price)
            {
                if (fromAddContractOrCRP == false)
                {
                    classString = "";
                }
                check = "checked='checked'";
                autoWaivedText = " (Waived)";
            }
            // habs - Hide checkbox if the price is 0, because there is nothing to wave - end

            //if (HttpContext.Current.Session["IsDealer"].ToString() == "False" && "0.00" != price)
            if ("0.00" != price)
            {
                return hiddenInput + "<span><input type='checkbox' disabled='true' class='" + classString + "' name='CBWaiver' id='" + id + "' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></span><span class='waiverSpan paddingLeft' data-compname='" + compName + "' data-price='" + price + "' data-compid='" + compId + "' data-msismtype='" + msismType + "'>" + compName + " - RM" + displayedPrice + autoWaivedText + "</span><br/>";
            }
            else
            {
                return hiddenInput + "<span><input type='checkbox' disabled='true' class='" + classString + "' " + check + " name='CBWaiver' id='" + id + "' value='" + compName + "~" + compId + "~" + price + "~" + msismType + "' /></span><span class='waiverSpan paddingLeft' data-compname='" + compName + "' data-price='" + price + "' data-compid='" + compId + "' data-msismtype='" + msismType + "'>" + compName + " - RM" + displayedPrice + autoWaivedText + "</span><br/>";
            }
        }

        public static string FormWaiverTableMSIM(int nationalityId, int regType)
        {
            System.Text.StringBuilder strWaiverTablPrimary = new System.Text.StringBuilder();
            System.Text.StringBuilder strWaiverTablSecondary = new System.Text.StringBuilder();
            System.Text.StringBuilder strWaiverTablUpfront = new System.Text.StringBuilder();

            WaiverRules waiverRule = HttpContext.Current.Session[SessionKey.WaiverRules.ToString()] != null ? (WaiverRules)HttpContext.Current.Session["WaiverRules"] : new WaiverRules();


            if (nationalityId == 1)
            {
                if (HttpContext.Current.Session["RegMobileReg_MalyPlanDeposit"].ToInt() > 0)
                {
                    strWaiverTablPrimary.Append(FormTableRow("Primary Plan Registration Deposit", HttpContext.Current.Session["RegMobileReg_MalyPlanDeposit"].ToString(), "0", "P", waiverRule.PlanDeposit));
                }
                if (HttpContext.Current.Session["RegMobileReg_MalyPlanDeposit_Seco"].ToInt() > 0)
                {
                    strWaiverTablSecondary.Append(FormTableRow("Secondary Plan Registration Deposit", HttpContext.Current.Session["RegMobileReg_MalyPlanDeposit_Seco"].ToString(), "0", "s", waiverRule.PlanDeposit));
                }
                if (HttpContext.Current.Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()].ToInt() > 0)
                {
                    strWaiverTablPrimary.Append(FormTableRow("Primary Plan Advance", HttpContext.Current.Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()].ToString(), "0", "p", waiverRule.PlanAdv));

                }
                if (HttpContext.Current.Session["RegMobileReg_MalayPlanAdv_Seco"].ToInt() > 0)
                {
                    strWaiverTablSecondary.Append(FormTableRow("Secondary Plan Advance", HttpContext.Current.Session["RegMobileReg_MalayPlanAdv_Seco"].ToString(), "0", "s", waiverRule.PlanAdv));
                }
                if (regType == (int)MobileRegType.DevicePlan || regType == (int)MobileRegType.DeviceCRP)
                {
                    if (HttpContext.Current.Session["RegMobileReg_MalayDevDeposit"].ToInt() > 0)
                    {
                        strWaiverTablPrimary.Append(FormTableRow("Primary Advance Pymt (iValue)", HttpContext.Current.Session["RegMobileReg_MalayDevDeposit"].ToString(), "0", "p", waiverRule.DeviceDeposit));
                    }
                    if (HttpContext.Current.Session["RegMobileReg_MalayDevDeposit_Seco"].ToInt() > 0)
                    {
                        strWaiverTablSecondary.Append(FormTableRow("Secondary Device Deposit", HttpContext.Current.Session["RegMobileReg_MalayDevDeposit_Seco"].ToString(), "0", "s", waiverRule.DeviceDeposit));
                    }
                    if (HttpContext.Current.Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()].ToInt() > 0)
                    {
                        strWaiverTablPrimary.Append(FormTableRow("Primary Device Advance", HttpContext.Current.Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()].ToString(), "0", "p", waiverRule.DeviceDeposit));
                    }
                    if (HttpContext.Current.Session["RegMobileReg_MalyDevAdv_Seco"].ToInt() > 0)
                    {
                        strWaiverTablSecondary.Append(FormTableRow("Secondary Device Advance", HttpContext.Current.Session["RegMobileReg_MalyDevAdv_Seco"].ToString(), "0", "s", waiverRule.DeviceAdv));
                    }
                }


            }
            else
            {
                if (HttpContext.Current.Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()].ToInt() > 0)
                {
                    strWaiverTablPrimary.Append(FormTableRow("Primary Plan Registration Deposit", HttpContext.Current.Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()].ToString(), "0", "p", waiverRule.PlanDeposit));
                }
                if (HttpContext.Current.Session["RegMobileReg_OthPlanDeposit_Seco"].ToInt() > 0)
                {
                    strWaiverTablSecondary.Append(FormTableRow("Secondary Plan Registration Deposit", HttpContext.Current.Session["RegMobileReg_OthPlanDeposit_Seco"].ToString(), "0", "s", waiverRule.PlanDeposit));
                }
                if (HttpContext.Current.Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()].ToInt() > 0)
                {
                    strWaiverTablPrimary.Append(FormTableRow("Primary Plan Advance", HttpContext.Current.Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()].ToString(), "0", "p", waiverRule.PlanAdv));
                }
                if (HttpContext.Current.Session["RegMobileReg_OthPlanAdv_Seco"].ToInt() > 0)
                {
                    strWaiverTablSecondary.Append(FormTableRow("Secondary Plan Advance", HttpContext.Current.Session["RegMobileReg_OthPlanAdv_Seco"].ToString(), "0", "s", waiverRule.PlanAdv));
                }
                if (regType == (int)MobileRegType.DevicePlan)
                {
                    if (HttpContext.Current.Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()].ToInt() > 0)
                    {
                        strWaiverTablPrimary.Append(FormTableRow("Primary Advance Pymt (iValue)", HttpContext.Current.Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()].ToString(), "0", "p", waiverRule.DeviceDeposit));
                    }
                    if (HttpContext.Current.Session["RegMobileReg_OthDevDeposit_Seco"].ToInt() > 0)
                    {
                        strWaiverTablSecondary.Append(FormTableRow("Secondary Device Deposit", HttpContext.Current.Session["RegMobileReg_OthDevDeposit_Seco"].ToString(), "0", "s", waiverRule.DeviceDeposit));
                    }
                    if (HttpContext.Current.Session[SessionKey.RegMobileReg_OthDevAdv.ToString()].ToInt() > 0)
                    {
                        strWaiverTablPrimary.Append(FormTableRow("Primary Device Advance", HttpContext.Current.Session[SessionKey.RegMobileReg_OthDevAdv.ToString()].ToString(), "0", "p", waiverRule.DeviceAdv));
                    }
                    if (HttpContext.Current.Session["RegMobileReg_OthDevAdv_Seco"].ToInt() > 0)
                    {
                        strWaiverTablSecondary.Append(FormTableRow("Secondary Device Advance", HttpContext.Current.Session["RegMobileReg_OthDevAdv_Seco"].ToString(), "0", "s", waiverRule.DeviceAdv));
                    }
                }
            }
            //Priamry
            if (HttpContext.Current.Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt() > 0)
            {
                strWaiverTablPrimary.Append(FormWaiverComponents(HttpContext.Current.Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), (List<string>)HttpContext.Current.Session[SessionKey.RegMobileReg_VasNames.ToString()], "p", regType));

            }

            //Secondary
            if (HttpContext.Current.Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt() > 0)
            {
                strWaiverTablSecondary.Append(FormWaiverComponents(HttpContext.Current.Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(), (List<string>)HttpContext.Current.Session[SessionKey.RegMobileReg_VasNames_Seco.ToString()], "s", regType));

            }

            //UpfrontPayment
            if (HttpContext.Current.Session["RegMobileReg_UpfrontPayment"].ToInt() > 0 && HttpContext.Current.Session["RegMobileReg_UpfrontPayment_isWaiverReq"].ToInt() != 1)
            {
                strWaiverTablUpfront.Append(FormTableRow("Upfront Payment", HttpContext.Current.Session["RegMobileReg_UpfrontPayment"].ToString(), "0", "p"));
            }

            string FinalTable = "<table><tr><td>";
            if (strWaiverTablUpfront.Length > 0)
            {
                strWaiverTablUpfront.Append("</table>");
                FinalTable += "<table><tr class='title'><td>Primary</td></tr>" + strWaiverTablUpfront.ToString();
            }
            if (strWaiverTablPrimary.Length > 0)
            {
                strWaiverTablPrimary.Append("</table>");
                FinalTable += "<table><tr class='title'><td>Primary</td></tr>" + strWaiverTablPrimary.ToString();

            }
            if (strWaiverTablSecondary.Length > 0)
            {
                strWaiverTablSecondary.Append("</table>");
                FinalTable += "<table><tr class='title'><td>Secondary</td></tr>" + strWaiverTablSecondary.ToString();
            }
            FinalTable += "</td></tr></table>";
            return FinalTable;
        }

        public static string FormWaiverTableMSIMNew(int nationalityId, int regType)
        {
            System.Text.StringBuilder strWaiverTablPrimary = new System.Text.StringBuilder();
            System.Text.StringBuilder strWaiverTablSecondary = new System.Text.StringBuilder();
            System.Text.StringBuilder strWaiverTablUpfront = new System.Text.StringBuilder();

            WaiverRules waiverRule = HttpContext.Current.Session[SessionKey.WaiverRules.ToString()] != null ? (WaiverRules)HttpContext.Current.Session["WaiverRules"] : new WaiverRules();

            if (nationalityId == 1)
            {
                if (HttpContext.Current.Session["RegMobileReg_MalyPlanDeposit"].ToInt() > 0)
                {
                    strWaiverTablPrimary.Append(FormTableRowNew("Primary Plan Registration Deposit", HttpContext.Current.Session["RegMobileReg_MalyPlanDeposit"].ToString(), "0", "P", waiverRule.PlanDeposit));
                }
                if (HttpContext.Current.Session["RegMobileReg_MalyPlanDeposit_Seco"].ToInt() > 0)
                {
                    strWaiverTablSecondary.Append(FormTableRowNew("Secondary Plan Registration Deposit", HttpContext.Current.Session["RegMobileReg_MalyPlanDeposit_Seco"].ToString(), "0", "s", waiverRule.PlanDeposit));
                }
                if (HttpContext.Current.Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()].ToInt() > 0)
                {
                    strWaiverTablPrimary.Append(FormTableRowNew("Primary Plan Advance", HttpContext.Current.Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()].ToString(), "0", "p", waiverRule.PlanAdv));
                }
                if (HttpContext.Current.Session["RegMobileReg_MalayPlanAdv_Seco"].ToInt() > 0)
                {
                    strWaiverTablSecondary.Append(FormTableRowNew("Secondary Plan Advance", HttpContext.Current.Session["RegMobileReg_MalayPlanAdv_Seco"].ToString(), "0", "s", waiverRule.PlanAdv));
                }
                if (regType == (int)MobileRegType.DevicePlan || regType == (int)MobileRegType.DeviceCRP)
                {
                    if (HttpContext.Current.Session["RegMobileReg_MalayDevDeposit"].ToInt() > 0)
                    {
                        strWaiverTablPrimary.Append(FormTableRowNew("Primary Advance Pymt (iValue)", HttpContext.Current.Session["RegMobileReg_MalayDevDeposit"].ToString(), "0", "p", waiverRule.DeviceDeposit));
                    }
                    if (HttpContext.Current.Session["RegMobileReg_MalayDevDeposit_Seco"].ToInt() > 0)
                    {
                        strWaiverTablSecondary.Append(FormTableRowNew("Secondary Device Deposit", HttpContext.Current.Session["RegMobileReg_MalayDevDeposit_Seco"].ToString(), "0", "s", waiverRule.DeviceDeposit));
                    }
                    if (HttpContext.Current.Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()].ToInt() > 0)
                    {
                        strWaiverTablPrimary.Append(FormTableRowNew("Primary Device Advance", HttpContext.Current.Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()].ToString(), "0", "p", waiverRule.DeviceDeposit));
                    }
                    if (HttpContext.Current.Session["RegMobileReg_MalyDevAdv_Seco"].ToInt() > 0)
                    {
                        strWaiverTablSecondary.Append(FormTableRowNew("Secondary Device Advance", HttpContext.Current.Session["RegMobileReg_MalyDevAdv_Seco"].ToString(), "0", "s", waiverRule.DeviceAdv));
                    }
                }
            }
            else
            {
                if (HttpContext.Current.Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()].ToInt() > 0)
                {
                    strWaiverTablPrimary.Append(FormTableRowNew("Primary Plan Registration Deposit", HttpContext.Current.Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()].ToString(), "0", "p", waiverRule.PlanDeposit));
                }
                if (HttpContext.Current.Session["RegMobileReg_OthPlanDeposit_Seco"].ToInt() > 0)
                {
                    strWaiverTablSecondary.Append(FormTableRowNew("Secondary Plan Registration Deposit", HttpContext.Current.Session["RegMobileReg_OthPlanDeposit_Seco"].ToString(), "0", "s", waiverRule.PlanDeposit));
                }
                if (HttpContext.Current.Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()].ToInt() > 0)
                {
                    strWaiverTablPrimary.Append(FormTableRowNew("Primary Plan Advance", HttpContext.Current.Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()].ToString(), "0", "p", waiverRule.PlanAdv));
                }
                if (HttpContext.Current.Session["RegMobileReg_OthPlanAdv_Seco"].ToInt() > 0)
                {
                    strWaiverTablSecondary.Append(FormTableRowNew("Secondary Plan Advance", HttpContext.Current.Session["RegMobileReg_OthPlanAdv_Seco"].ToString(), "0", "s", waiverRule.PlanAdv));
                }
                if (regType == (int)MobileRegType.DevicePlan)
                {
                    if (HttpContext.Current.Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()].ToInt() > 0)
                    {
                        strWaiverTablPrimary.Append(FormTableRowNew("Primary Advance Pymt (iValue)", HttpContext.Current.Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()].ToString(), "0", "p", waiverRule.DeviceDeposit));
                    }
                    if (HttpContext.Current.Session["RegMobileReg_OthDevDeposit_Seco"].ToInt() > 0)
                    {
                        strWaiverTablSecondary.Append(FormTableRowNew("Secondary Device Deposit", HttpContext.Current.Session["RegMobileReg_OthDevDeposit_Seco"].ToString(), "0", "s", waiverRule.DeviceDeposit));
                    }
                    if (HttpContext.Current.Session[SessionKey.RegMobileReg_OthDevAdv.ToString()].ToInt() > 0)
                    {
                        strWaiverTablPrimary.Append(FormTableRowNew("Primary Device Advance", HttpContext.Current.Session[SessionKey.RegMobileReg_OthDevAdv.ToString()].ToString(), "0", "p", waiverRule.DeviceAdv));
                    }
                    if (HttpContext.Current.Session["RegMobileReg_OthDevAdv_Seco"].ToInt() > 0)
                    {
                        strWaiverTablSecondary.Append(FormTableRowNew("Secondary Device Advance", HttpContext.Current.Session["RegMobileReg_OthDevAdv_Seco"].ToString(), "0", "s", waiverRule.DeviceAdv));
                    }
                }
            }
            //Priamry
            if (HttpContext.Current.Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt() > 0)
            {
                strWaiverTablPrimary.Append(FormWaiverComponentsNew(HttpContext.Current.Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), (List<string>)HttpContext.Current.Session[SessionKey.RegMobileReg_VasNames.ToString()], "p", regType));

            }

            //Secondary
            if (HttpContext.Current.Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt() > 0)
            {
                strWaiverTablSecondary.Append(FormWaiverComponentsNew(HttpContext.Current.Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(), (List<string>)HttpContext.Current.Session[SessionKey.RegMobileReg_VasNames_Seco.ToString()], "s", regType));

            }

            //UpfrontPayment
            if (HttpContext.Current.Session["RegMobileReg_UpfrontPayment"].ToInt() > 0 && HttpContext.Current.Session["RegMobileReg_UpfrontPayment_isWaiverReq"].ToInt() != 1)
            {
                strWaiverTablUpfront.Append(FormTableRowNew("Upfront Payment", HttpContext.Current.Session["RegMobileReg_UpfrontPayment"].ToString(), "0", "p"));
            }

            string FinalTable = "";
            if (strWaiverTablUpfront.Length > 0)
            {
                FinalTable += strWaiverTablUpfront.ToString();
            }
            if (strWaiverTablPrimary.Length > 0)
            {
                FinalTable += strWaiverTablPrimary.ToString();

            }
            if (strWaiverTablSecondary.Length > 0)
            {
                FinalTable += strWaiverTablSecondary.ToString();
            }
            return FinalTable;
        }

        private static string FormWaiverComponents(int packageId, List<string> objVasNames, string compType, int regType = 0, string _liberalization = "")
        {
            string[] libs = {"ELITE_A", "ELITE_B", "ELITE_C", "SEL500_A", "SEL400_A", "SEL300_A",
                                    "SEL500_B", "SEL400_B", "SEL300_B" ,"SEL500_C", "SEL400_C", "SEL300_C",
                                    "MASS_A","TEN7_A","MASS_B","TEN7_B","MASS_C","TEN7_C" };
            System.Text.StringBuilder strWaiverTabl = new System.Text.StringBuilder();
            string[] stringSeparators = new string[] { " (RM" };
            using (var proxy = new Registration.Web.Helper.RegistrationServiceProxy())
            {
                List<string> objExistCompIds = new List<string>();
                if (regType == (int)MobileRegType.CRP || regType == (int)MobileRegType.DeviceCRP || regType == (int)MobileRegType.AddRemoveVAS || regType == (int)MobileRegType.Contract)
                {
                    objExistCompIds = FormExistingComponents(HttpContext.Current.Session[SessionKey.ExternalID.ToString()].ToString2());
                }

                List<DAL.Models.WaiverComponents> objWaiverComponents = proxy.GetWaiverComponents(packageId);
                WaiverRules waiverRule = (WaiverRules)HttpContext.Current.Session[SessionKey.WaiverRules.ToString()] != null ? (WaiverRules)HttpContext.Current.Session["WaiverRules"] : new WaiverRules();
                bool isCallConf = waiverRule.CallConf.ToBool();
                bool isfromMNP = !ReferenceEquals(HttpContext.Current.Session["FromMNP"], null) && Convert.ToBoolean(HttpContext.Current.Session["FromMNP"]);
                bool isIntRoaming = false;
                if (isfromMNP)
                {
                    if (HttpContext.Current.Session["IsOlo"].ToInt() == 1)
                    {
                        isCallConf = true;
                    }
                    //libs = new string[] { "ELITE_A", "ELITE_B", "ELITE_C", "SEL500_A", "SEL400_A", "SEL300_A",
                    //                "SEL500_B", "SEL400_B", "SEL300_B" ,"SEL500_C", "SEL400_C", "SEL300_C",
                    //                "MASS_A","TEN7_A","MASS_B","TEN7_B","MASS_C","TEN7_C" };
                    isIntRoaming = Array.IndexOf(libs, waiverRule.Liberization) > -1;
                }


                foreach (var waiverComp in objWaiverComponents)
                {
                    // comps.Name + " (RM" + selectedVases.Price + ")"
                    //var vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()]; 
                    //string waiverCompName =waiverComp.ComponentName.Trim().Replace(" ",string.Empty) + "(RM" + waiverComp.price.ToString() + ")";

                    string waiverCompName = waiverComp.ComponentName.Trim();
                    bool isCompExist = false;
                    if (regType == (int)MobileRegType.CRP || regType == (int)MobileRegType.DeviceCRP || regType == (int)MobileRegType.AddRemoveVAS || regType == (int)MobileRegType.Contract)
                    {
                        foreach (string curKenanCode in objExistCompIds)
                        {
                            if (curKenanCode == waiverComp.ComponentID.ToString())
                            {
                                isCompExist = true;
                                break;
                            }
                        }
                    }
                    if (isCompExist == false)
                    {
                        if (!ReferenceEquals(objVasNames, null))
                        {
                            for (int vasCnt = 0; vasCnt < objVasNames.Count; vasCnt++)
                            {
                                string curVasName = objVasNames[vasCnt].Trim().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries)[0];

                                if (curVasName == waiverCompName)
                                {
                                    switch (waiverCompName)
                                    {
                                        //Not Available
                                        case "Mobile - Access International Roaming":
                                            {

                                                if (isfromMNP)
                                                    strWaiverTabl.Append(FormTableRow(waiverComp.ComponentName, waiverComp.price.ToString2(), waiverComp.ComponentID.ToString(), compType, isIntRoaming));
                                                else
                                                    strWaiverTabl.Append(FormTableRow(waiverComp.ComponentName, waiverComp.price.ToString2(), waiverComp.ComponentID.ToString(), compType, waiverRule.IntRoaming));

                                                break;
                                            }
                                        case "Mobile - Call Conferencing":
                                            strWaiverTabl.Append(FormTableRow(waiverComp.ComponentName, waiverComp.price.ToString2(), waiverComp.ComponentID.ToString(), compType, isCallConf));
                                            break;
                                        default:
                                            strWaiverTabl.Append(FormTableRow(waiverComp.ComponentName, waiverComp.price.ToString2(), waiverComp.ComponentID.ToString(), compType));
                                            break;
                                    }
                                    break;

                                }
                            }
                        }
                    }
                }
            }
            return strWaiverTabl.ToString2();
        }

        private static string FormWaiverComponentsNew(int packageId, List<string> objVasNames, string compType, int regType = 0, string _liberalization = "")
        {
            string[] libs = {"ELITE_A", "ELITE_B", "ELITE_C", "SEL500_A", "SEL400_A", "SEL300_A",
                                    "SEL500_B", "SEL400_B", "SEL300_B" ,"SEL500_C", "SEL400_C", "SEL300_C",
                                    "MASS_A","TEN7_A","MASS_B","TEN7_B","MASS_C","TEN7_C" };
            System.Text.StringBuilder strWaiverTabl = new System.Text.StringBuilder();
            string[] stringSeparators = new string[] { " (RM" };
            using (var proxy = new Registration.Web.Helper.RegistrationServiceProxy())
            {
                List<string> objExistCompIds = new List<string>();
                if (regType == (int)MobileRegType.CRP || regType == (int)MobileRegType.DeviceCRP || regType == (int)MobileRegType.AddRemoveVAS || regType == (int)MobileRegType.Contract)
                {
                    objExistCompIds = FormExistingComponents(HttpContext.Current.Session[SessionKey.ExternalID.ToString()].ToString2());
                }

                List<DAL.Models.WaiverComponents> objWaiverComponents = proxy.GetWaiverComponents(packageId);
                WaiverRules waiverRule = (WaiverRules)HttpContext.Current.Session[SessionKey.WaiverRules.ToString()] != null ? (WaiverRules)HttpContext.Current.Session["WaiverRules"] : new WaiverRules();
                bool isCallConf = waiverRule.CallConf.ToBool();
                bool isfromMNP = !ReferenceEquals(HttpContext.Current.Session["FromMNP"], null) && Convert.ToBoolean(HttpContext.Current.Session["FromMNP"]);
                bool isIntRoaming = false;
                if (isfromMNP)
                {
                    if (HttpContext.Current.Session["IsOlo"].ToInt() == 1)
                    {
                        isCallConf = true;
                    }
                    isIntRoaming = Array.IndexOf(libs, waiverRule.Liberization) > -1;
                }


                foreach (var waiverComp in objWaiverComponents)
                {
                    string waiverCompName = waiverComp.ComponentName.Trim();
                    bool isCompExist = false;
                    if (regType == (int)MobileRegType.CRP || regType == (int)MobileRegType.DeviceCRP || regType == (int)MobileRegType.AddRemoveVAS || regType == (int)MobileRegType.Contract)
                    {
                        foreach (string curKenanCode in objExistCompIds)
                        {
                            if (curKenanCode == waiverComp.ComponentID.ToString())
                            {
                                isCompExist = true;
                                break;
                            }
                        }
                    }
                    if (isCompExist == false)
                    {
                        for (int vasCnt = 0; vasCnt < objVasNames.Count; vasCnt++)
                        {
                            string curVasName = objVasNames[vasCnt].Trim().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries)[0];

                            if (curVasName == waiverCompName)
                            {
                                switch (waiverCompName)
                                {
                                    case "Mobile - Access International Roaming":
                                        {

                                            if (isfromMNP)
                                                strWaiverTabl.Append(FormTableRowNew(waiverComp.ComponentName, waiverComp.price.ToString2(), waiverComp.ComponentID.ToString(), compType, isIntRoaming));
                                            else
                                                strWaiverTabl.Append(FormTableRowNew(waiverComp.ComponentName, waiverComp.price.ToString2(), waiverComp.ComponentID.ToString(), compType, waiverRule.IntRoaming));

                                            break;
                                        }
                                    case "Mobile - Call Conferencing":
                                        strWaiverTabl.Append(FormTableRowNew(waiverComp.ComponentName, waiverComp.price.ToString2(), waiverComp.ComponentID.ToString(), compType, isCallConf));
                                        break;
                                    default:
                                        strWaiverTabl.Append(FormTableRowNew(waiverComp.ComponentName, waiverComp.price.ToString2(), waiverComp.ComponentID.ToString(), compType));
                                        break;
                                }
                                break;

                            }
                        }
                    }
                }
            }
            return strWaiverTabl.ToString2();
        }

        /*
         * This method generates a list of waiver components based on dropFourObj
         * 20141204 - Habs
         * */
        public static List<WaiverComponents> constructWaiverComponentList(DropFourObj dropFourObj)
        {
            List<DAL.Models.WaiverComponents> resultList = new List<WaiverComponents>();
            List<DAL.Models.WaiverComponents> waiverComponentList = null;

            Boolean isMNP = Convert.ToBoolean(HttpContext.Current.Session["FromMNP"]);

            if (dropFourObj == null)
            {
                return resultList;
            }

            int idCardType = dropFourObj.personalInformation.Customer.NationalityID;

            if (dropFourObj.mainFlow != null && dropFourObj.mainFlow.orderSummary != null)
            {
                dropFourObj.mainFlow.mismType = (isMNP ? "MP" : "P");

                waiverComponentList = constructWaiverComponentList(
                        dropFourObj.mainFlow.orderSummary,
                        dropFourObj.mainFlow.PkgPgmBdlPkgCompID,
                        idCardType,
                        dropFourObj.mainFlow.mismType);

                resultList.AddRange(waiverComponentList);
            }

            if (dropFourObj.suppIndex > 0)
            {
                //04022015 - Anthony - Change MISM Type from "S1" to "n" for Add Supplementary flow - Start
                var isDedicatedSuppFlow = HttpContext.Current.Session[SessionKey.DedicatedSuppFlow.ToString()].ToBool();

                if (isDedicatedSuppFlow)
                {
                    dropFourObj.suppFlow[0].mismType = "n";

                    waiverComponentList = constructWaiverComponentList(
                        dropFourObj.suppFlow[0].orderSummary,
                        dropFourObj.suppFlow[0].PkgPgmBdlPkgCompID,
                        idCardType,
                        dropFourObj.suppFlow[0].mismType);

                    resultList.AddRange(waiverComponentList);
                }
                else
                {
                    int i = 1;

                    foreach (var supp in dropFourObj.suppFlow)
                    {
                        supp.mismType = (isMNP ? "MS" + i : "S" + i);
                        i++;

                        waiverComponentList = constructWaiverComponentList(
                            supp.orderSummary,
                            supp.PkgPgmBdlPkgCompID,
                            idCardType,
                            supp.mismType);

                        resultList.AddRange(waiverComponentList);
                    }
                }
                //04022015 - Anthony - Change MISM Type from "S1" to "n" for Add Supplementary flow - End
            }

            return resultList;
        }


        /*
         * This method generates a list of waiver components based on orderSummary
         * 20141204 - Habs
         * */
        public static List<WaiverComponents> constructWaiverComponentList(OrderSummaryVM orderSummary, int componentId, int idCardType, String lineType)
        //public static List<WaiverComponents> constructWaiverComponentList(OrderSummaryVM orderSummary)
        {
            Boolean isMalaysian = (idCardType == 1 || idCardType == 3);

            List<DAL.Models.WaiverComponents> waiverComponentList = new List<WaiverComponents>();
            DAL.Models.WaiverComponents waiverComponent = null;

            Decimal origPlanAdvance = 0;
            Decimal origPlanDeposit = 0;
            Decimal origDeviceDeposit = 0;
            Decimal origDeviceAdvance = 0;

            if (orderSummary.unwaivedAdvancePaymentDeposit != null)
            {
                origPlanAdvance = isMalaysian
                    ? orderSummary.unwaivedAdvancePaymentDeposit.malayPlanAdv
                    : orderSummary.unwaivedAdvancePaymentDeposit.othPlanAdv;

                origPlanDeposit = isMalaysian
                    ? orderSummary.unwaivedAdvancePaymentDeposit.malyPlanDeposit
                    : orderSummary.unwaivedAdvancePaymentDeposit.othPlanDeposit;

                origDeviceDeposit = isMalaysian
                    ? orderSummary.unwaivedAdvancePaymentDeposit.malayDevDeposit
                    : orderSummary.unwaivedAdvancePaymentDeposit.othDevDeposit;

                origDeviceAdvance = isMalaysian
                    ? orderSummary.unwaivedAdvancePaymentDeposit.malyDevAdv
                    : orderSummary.unwaivedAdvancePaymentDeposit.othDevAdv;
            }



            // 1. Add Plan Advance
            if (origPlanAdvance > 0)
            {
                Decimal waivedPlanAdvance = isMalaysian
                    ? orderSummary.MalayPlanAdv
                    : orderSummary.OthPlanAdv;

                waiverComponent = constructWaiverComponent(
                    "Plan Advance",
                    componentId,
                    waivedPlanAdvance,
                    origPlanAdvance,
                    lineType,
                    orderSummary.isPlanAdvanceAutoWaived);//21042015 - Anthony - Store the correct information to DB

                waiverComponentList.Add(waiverComponent);
            }


            // 2. Registration Deposit
            if (origPlanDeposit > 0)
            {
                Decimal waivedPlanDeposit = isMalaysian
                    ? orderSummary.MalyPlanDeposit
                    : orderSummary.OthPlanDeposit;

                waiverComponent = constructWaiverComponent(
                    "Registration Deposit",
                    componentId,
                    waivedPlanDeposit,
                    origPlanDeposit,
                    lineType,
                    orderSummary.isPlanDepositAutoWaived);//21042015 - Anthony - Store the correct information to DB

                waiverComponentList.Add(waiverComponent);
            }


            // 3. Device Deposit
            if (origDeviceDeposit > 0)
            {
                Decimal waivedDeviceDeposit = isMalaysian
                    ? orderSummary.MalayDevDeposit
                    : orderSummary.OthDevDeposit;

                waiverComponent = constructWaiverComponent(
                    orderSummary.PlanName.Contains("iValue") ? "iValue Advance" : "Device Deposit",
                    componentId,
                    waivedDeviceDeposit,
                    origDeviceDeposit,
                    lineType,
                    orderSummary.isDeviceAdvanceAutoWaived);//21042015 - Anthony - Store the correct information to DB

                waiverComponentList.Add(waiverComponent);
            }


            // 4. Device Advance            
            if (origDeviceAdvance > 0)
            {
                Decimal waivedDeviceAdvance = isMalaysian
                    ? orderSummary.MalyDevAdv
                    : orderSummary.OthDevAdv;

                waiverComponent = constructWaiverComponent(
                    "Device Advance",
                    componentId,
                    waivedDeviceAdvance,
                    origDeviceAdvance,
                    lineType,
                    orderSummary.isDeviceDepositAutoWaived);//21042015 - Anthony - Store the correct information to DB

                waiverComponentList.Add(waiverComponent);
            }

            return waiverComponentList;
        }

        public static WaiverComponents constructWaiverComponent(String componentName, int componentId, Decimal waivedPrice, Decimal origPrice, String lineType, bool isAutoWaived = false)
        {
            DAL.Models.WaiverComponents waiverComponent = new WaiverComponents();

            if (componentName == "Plan Advance"
                || componentName == "Registration Deposit"
                || componentName == "Device Deposit"
                || componentName == "Device Advance"
                || componentName == "iValue Advance")
            {
                waiverComponent.ComponentID = 0;
            }
            else
            {
                waiverComponent.ComponentID = componentId;
            }

            waiverComponent.ComponentName = componentName;
            waiverComponent.IsWaived = (waivedPrice == 0 && waivedPrice < origPrice);
            waiverComponent.price = origPrice;
            waiverComponent.MismType = lineType;
            waiverComponent.IsAutoWaived = isAutoWaived;//21042015 - Anthony - Store the correct information to DB
            return waiverComponent;
        }


        /// <summary>
        /// Waiver information List for Mobile Regsummary by ravikiran 
        /// </summary>
        /// <param name="waiverInfo"></param>
        /// <returns></returns>
        public static List<WaiverComponents> ConstructWaiverComponents(string waiverInfo, bool isMultiMNP = false, OrderSummaryVM orderSummary = null)
        {
            #region "Waiver Options"
            List<DAL.Models.WaiverComponents> obj = new List<WaiverComponents>();
            //21042015 - Anthony - Store the correct information to DB - Start
            decimal origPricePlanAdvance = 0;
            decimal origPricePlanDeposit = 0;
            decimal origPriceDeviceAdvance = 0;
            decimal origPriceDeviceDeposit = 0;
            orderSummary = orderSummary ?? new OrderSummaryVM();
            bool isMalaysian = WebHelper.Instance.determineNationality() == "1"
                                    ? true
                                    : HttpContext.Current.Session[SessionKey.IDCardTypeID.ToString()].ToInt() != 2
                                            ? true
                                            : false;
            if (orderSummary.unwaivedAdvancePaymentDeposit != null)
            {
                if (isMalaysian == true)
                {
                    origPricePlanAdvance = orderSummary.unwaivedAdvancePaymentDeposit.malayPlanAdv;
                    origPricePlanDeposit = orderSummary.unwaivedAdvancePaymentDeposit.malyPlanDeposit;
                    origPriceDeviceAdvance = orderSummary.unwaivedAdvancePaymentDeposit.malyDevAdv;
                    origPriceDeviceDeposit = orderSummary.unwaivedAdvancePaymentDeposit.malayDevDeposit;
                }
                else
                {
                    origPricePlanAdvance = orderSummary.unwaivedAdvancePaymentDeposit.othPlanAdv;
                    origPricePlanDeposit = orderSummary.unwaivedAdvancePaymentDeposit.othPlanDeposit;
                    origPriceDeviceAdvance = orderSummary.unwaivedAdvancePaymentDeposit.othDevAdv;
                    origPriceDeviceDeposit = orderSummary.unwaivedAdvancePaymentDeposit.othDevDeposit;
                }
            }
            //21042015 - Anthony - Store the correct information to DB - End
            if (!string.IsNullOrEmpty(waiverInfo))
            {
                Online.Registration.Web.ViewModels.MNPPrimaryPlanStorage objMnp = (Online.Registration.Web.ViewModels.MNPPrimaryPlanStorage)HttpContext.Current.Session["mNPPrimaryPlanStorage"];
                foreach (string waiverItem in waiverInfo.ToString().Split('+'))
                {
                    string[] curComponet = waiverItem.Split('~');
                    bool isAutoWaived = false;//21042015 - Anthony - To differentiate the autowaived component
                    decimal originalPrice = 0;//21042015 - Anthony - Store the correct information to DB

                    if (curComponet[0] != "")//11032015 - Anthony - This is to cater the last empty string after split the variable waiverItem (for CRP scenario)
                    {
                        if (isMultiMNP == true)
                        {
                            if (curComponet[0] == "Registration Deposit" && curComponet[4].ToString2() == "1" && curComponet[1].ToInt() == 0)
                            {
                                isAutoWaived = orderSummary.isPlanDepositAutoWaived;//21042015 - Anthony - To differentiate the autowaived component
                                originalPrice = origPricePlanDeposit;//21042015 - Anthony - Store the correct information to DB
                                if (objMnp != null)
                                {
                                    if (curComponet[3].ToString2().Contains("MP") == true)
                                    {
                                        objMnp.RegMobileReg_MalyPlanDeposit = 0;
                                        objMnp.RegMobileReg_OthPlanDeposit = 0;
                                    }
                                    else
                                    {
                                        //Supplementary Lines
                                        for (int supleMsisdnsCnt = 0; supleMsisdnsCnt < objMnp.MNPSupplinePlanStorageList.Count; supleMsisdnsCnt++)
                                        {
                                            string curMsisdn = ("MS" + (supleMsisdnsCnt + 1)).ToString();
                                            if (curComponet[3].ToString2().Contains(curMsisdn) == true)
                                            {
                                                objMnp.MNPSupplinePlanStorageList[supleMsisdnsCnt].RegMobileReg_MalyPlanDeposit = 0;
                                                objMnp.MNPSupplinePlanStorageList[supleMsisdnsCnt].RegMobileReg_OthPlanDeposit = 0;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                            if (curComponet[0] == "Plan Advance" && curComponet[4].ToString2() == "1" && curComponet[1].ToInt() == 0)
                            {
                                isAutoWaived = orderSummary.isPlanAdvanceAutoWaived;//21042015 - Anthony - To differentiate the autowaived component
                                originalPrice = origPricePlanAdvance;//21042015 - Anthony - Store the correct information to DB
                                if (objMnp != null)
                                {
                                    if (curComponet[3].ToString2().Contains("MP") == true)
                                    {
                                        objMnp.RegMobileReg_MalayPlanAdv = 0;
                                        objMnp.RegMobileReg_OthPlanAdv = 0;
                                    }
                                    else
                                    {
                                        //Supplementary Lines
                                        for (int supleMsisdnsCnt = 0; supleMsisdnsCnt < objMnp.MNPSupplinePlanStorageList.Count; supleMsisdnsCnt++)
                                        {
                                            string curMsisdn = ("MS" + (supleMsisdnsCnt + 1)).ToString();
                                            if (curComponet[3].ToString2().Contains(curMsisdn) == true)
                                            {
                                                objMnp.MNPSupplinePlanStorageList[supleMsisdnsCnt].RegMobileReg_MalayPlanAdv = 0;
                                                objMnp.MNPSupplinePlanStorageList[supleMsisdnsCnt].RegMobileReg_OthPlanAdv = 0;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        else
                        {
                            if ((curComponet[0] == "Registration Deposit" || curComponet[0] == "Primary Plan Registration Deposit") && curComponet[4].ToString2() == "1" && curComponet[1].ToInt() == 0)
                            {
                                HttpContext.Current.Session["RegMobileReg_MalyPlanDeposit"] = null;
                                HttpContext.Current.Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = null;
                                isAutoWaived = orderSummary.isPlanDepositAutoWaived;//21042015 - Anthony - To differentiate the autowaived component
                                originalPrice = origPricePlanDeposit;//21042015 - Anthony - Store the correct information to DB
                            }
                            if ((curComponet[0] == "Plan Advance" || curComponet[0] == "Primary Plan Advance") && curComponet[4].ToString2() == "1" && curComponet[1].ToInt() == 0)
                            {
                                HttpContext.Current.Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = null;
                                HttpContext.Current.Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = null;
                                isAutoWaived = orderSummary.isPlanAdvanceAutoWaived;//21042015 - Anthony - To differentiate the autowaived component
                                originalPrice = origPricePlanAdvance;//21042015 - Anthony - Store the correct information to DB
                            }
                            if (curComponet[0] == "Secondary Plan Registration Deposit" && curComponet[4].ToString2() == "1" && curComponet[1].ToInt() == 0)
                            {
                                HttpContext.Current.Session["RegMobileReg_MalyPlanDeposit_Seco"] = null;
                                HttpContext.Current.Session["RegMobileReg_OthPlanDeposit_Seco"] = null;
                            }
                            if (curComponet[0] == "Secondary Plan Advance" && curComponet[4].ToString2() == "1" && curComponet[1].ToInt() == 0)
                            {
                                HttpContext.Current.Session["RegMobileReg_OthPlanAdv_Seco"] = null;
                                HttpContext.Current.Session["RegMobileReg_MalayPlanAdv_Seco"] = null;
                            }
                            if ((curComponet[0] == Constants.IVALUE_ADVANCE || curComponet[0] == "Primary Advance Pymt (iValue)") && curComponet[4].ToString2() == "1" && curComponet[1].ToInt() == 0)
                            {
                                HttpContext.Current.Session["RegMobileReg_MalayDevDeposit"] = null;
                                HttpContext.Current.Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = null;
                                isAutoWaived = orderSummary.isDeviceDepositAutoWaived;//21042015 - Anthony - To differentiate the autowaived component
                                originalPrice = origPriceDeviceDeposit;//21042015 - Anthony - Store the correct information to DB
                            }
                            if ((curComponet[0] == "Device Advance" || curComponet[0] == "Primary Device Advance") && curComponet[4].ToString2() == "1" && curComponet[1].ToInt() == 0)
                            {
                                HttpContext.Current.Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = null;
                                HttpContext.Current.Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = null;
                                isAutoWaived = orderSummary.isDeviceAdvanceAutoWaived;//21042015 - Anthony - To differentiate the autowaived component
                                originalPrice = origPriceDeviceAdvance;//21042015 - Anthony - Store the correct information to DB
                            }
                            if (curComponet[0] == "Upfront Payment" && curComponet[4].ToString2() == "1" && curComponet[1].ToInt() == 0)
                            {
                                HttpContext.Current.Session["RegMobileReg_UpfrontPayment"] = null;
                            }
                        }

                        if (curComponet[0] == "Plan Advance") originalPrice = origPricePlanAdvance;
                        if (curComponet[0] == "Registration Deposit") originalPrice = origPricePlanDeposit;
                        if (curComponet[0] == "Device Advance") originalPrice = origPriceDeviceAdvance;
                        if (curComponet[0] == Constants.IVALUE_ADVANCE) originalPrice = origPriceDeviceDeposit;

                        if (originalPrice > 0)//21042015 - Anthony - Store the correct information to DB
                        {
                            DAL.Models.WaiverComponents curObj = new WaiverComponents();
                            curObj.ComponentID = curComponet[1].ToInt();
                            curObj.ComponentName = curComponet[0];
                            curObj.IsWaived = curComponet[4].ToString2() == "1" ? true : false;
                            //curObj.price = curComponet[2].ToString2() != "" ? Convert.ToDecimal(curComponet[2]) : 0;
                            curObj.price = originalPrice;//21042015 - Anthony - Store the correct information to DB
                            curObj.MismType = curComponet[3].ToString2();
                            curObj.IsAutoWaived = isAutoWaived;//21042015 - Anthony - To differentiate the autowaived component
                            obj.Add(curObj);
                        }
                    }
                }

            }
            return obj;

            #endregion
        }


        private static List<string> FormExistingComponents(string msisdn = "")
        {

            List<string> compids = new List<string>();
            if (msisdn != string.Empty)
            {
                Online.Registration.Web.SubscriberICService.SubscriberICServiceClient icserviceobj = new Online.Registration.Web.SubscriberICService.SubscriberICServiceClient();

                IList<Online.Registration.Web.SubscriberICService.PackageModel>
                packages = icserviceobj.retrievePackageDetls(msisdn, System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"], userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName, password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword, postUrl: "");

                foreach (var pkg in packages)
                {
                    compids.AddRange(pkg.compList.Select(c => c.componentId).ToList());

                }
            }

            return compids;
        }

        public static PersonalDetailsVM ConstructWaiverPrice(PersonalDetailsVM _inputPV, DropFourObj dropObj)
        {
            var waiveComponent = new List<string>();
            List<string> suppElement = new List<string>();
            Dictionary<string, List<string>> suppWaiveComponent = new Dictionary<string, List<string>>();
            string suppIndex = "";
            int index = 0;

            // habs - fix waiver checkboxes - start
            /*
            if (!string.IsNullOrEmpty(personalDetailsVM.hdnWaiverInfo))
            {
                var waiveObject = (personalDetailsVM.hdnWaiverInfo.Split('+'));
            */


            if (!ReferenceEquals(_inputPV, null))
            {
                if (!ReferenceEquals((object)_inputPV.cbWaiver, null))
                {
                    var waiveObject = _inputPV.cbWaiver;
                    // habs - fix waiver checkboxes - end

                    foreach (var item in waiveObject)
                    {
                        var split = item.ToString().Split('~');
                        if (!split[3].Contains("S"))
                        {
                            waiveComponent.Add(split[index]);
                        }
                        else
                        {

                            suppIndex = split[3].Substring(split[3].Count() - 1);
                            if (suppWaiveComponent.ContainsKey(suppIndex))
                            {
                                // habs fix waiver checkbox - start
                                suppWaiveComponent.TryGetValue(suppIndex, out suppElement);
                                // habs fix waiver checkbox - end
                                suppElement.Add(split[index]);
                                //suppWaiveComponent.Clear();
                                //suppWaiveComponent.Add(suppIndex, suppElement);
                            }
                            else
                            {
                                // habs fix waiver checkbox - start
                                /*
                                if(null != suppElement.Count())
                                {
                                    suppElement.Clear();
                                }
                                */
                                suppElement = new List<string>();
                                // habs fix waiver checkbox - end

                                suppElement.Add(split[index]);
                                suppWaiveComponent.Add(suppIndex, suppElement);
                            }
                        }
                    }
                }
            }
            try
            {
                if (!ReferenceEquals(_inputPV.Customer.NationalityID, null))
                {
                    if (_inputPV.Customer.NationalityID.ToInt() == 1)
                    {
                        if (!ReferenceEquals(waiveComponent, null))
                        {
                            if (waiveComponent.Contains("Plan Advance"))
                            {
                                dropObj.mainFlow.orderSummary.MalayPlanAdv = 0;
                            }
                            if (waiveComponent.Contains("Device Advance"))
                            {
                                dropObj.mainFlow.orderSummary.MalyDevAdv = 0;
                            }
                            if (waiveComponent.Contains("Device Deposit") || waiveComponent.Contains("iValue Advance"))//12022015 - Anthony - Fix manual waive for iValue Deposit
                            {
                                dropObj.mainFlow.orderSummary.MalayDevDeposit = 0;
                            }
                            if (waiveComponent.Contains("Registration Deposit"))
                            {

                                dropObj.mainFlow.orderSummary.MalyPlanDeposit = 0;
                            }
                        }
                        else
                        {
                            throw new Exception("waiveComponent cannot be null");
                        }
                        if (null != suppWaiveComponent)
                        {

                            for (int i = 0; i < dropObj.suppFlow.Count(); i++)
                            {
                                string check = (i + 1).ToString();
                                if (suppWaiveComponent.ContainsKey(check))
                                {

                                    // Habs - start
                                    List<String> suppWaiveComponentList = new List<String>();
                                    suppWaiveComponent.TryGetValue(check, out suppWaiveComponentList);

                                    //if (waiveComponent.Contains("Plan Advance"))
                                    if (suppWaiveComponentList.Contains("Plan Advance"))
                                    // Habs - end
                                    {
                                        dropObj.suppFlow[i].orderSummary.MalayPlanAdv = 0;
                                    }
                                    if (suppWaiveComponentList.Contains("Device Advance"))
                                    {
                                        dropObj.suppFlow[i].orderSummary.MalyDevAdv = 0;
                                    }
                                    if (suppWaiveComponentList.Contains("Device Deposit") || suppWaiveComponentList.Contains("iValue Advance"))//12022015 - Anthony - Fix manual waive for iValue Deposit
                                    {
                                        dropObj.suppFlow[i].orderSummary.MalayDevDeposit = 0;
                                    }
                                    if (suppWaiveComponentList.Contains("Registration Deposit"))
                                    {
                                        dropObj.suppFlow[i].orderSummary.MalyPlanDeposit = 0;
                                    }

                                }
                            }
                        }

                    }
                    else
                    {
                        if (!ReferenceEquals(waiveComponent, null))
                        {
                            if (waiveComponent.Contains("Plan Advance"))
                            {
                                dropObj.mainFlow.orderSummary.OthPlanAdv = 0;
                            }
                            if (waiveComponent.Contains("Device Advance"))
                            {

                                dropObj.mainFlow.orderSummary.OthDevAdv = 0;

                            }
                            if (waiveComponent.Contains("Device Deposit") || waiveComponent.Contains("iValue Advance"))//12022015 - Anthony - Fix manual waive for iValue Deposit
                            {

                                dropObj.mainFlow.orderSummary.OthDevDeposit = 0;
                            }
                            if (waiveComponent.Contains("Registration Deposit"))
                            {
                                dropObj.mainFlow.orderSummary.OthPlanDeposit = 0;
                            }
                        }
                        else
                        {
                            throw new Exception("waiveComponent cannot be null");
                        }


                        if (null != suppWaiveComponent)
                        {

                            for (int i = 0; i < dropObj.suppFlow.Count(); i++)
                            {
                                string check = (i + 1).ToString();
                                if (suppWaiveComponent.ContainsKey(check))
                                {

                                    // Habs - start
                                    List<String> suppWaiveComponentList = new List<String>();
                                    suppWaiveComponent.TryGetValue(check, out suppWaiveComponentList);

                                    //if (waiveComponent.Contains("Plan Advance"))
                                    if (suppWaiveComponentList.Contains("Plan Advance"))
                                    // Habs - end
                                    {
                                        dropObj.suppFlow[i].orderSummary.OthPlanAdv = 0;
                                    }
                                    if (suppWaiveComponentList.Contains("Device Advance"))
                                    {
                                        dropObj.suppFlow[i].orderSummary.OthDevAdv = 0;
                                    }
                                    if (suppWaiveComponentList.Contains("Device Deposit"))
                                    {
                                        dropObj.suppFlow[i].orderSummary.OthDevDeposit = 0;
                                    }
                                    if (suppWaiveComponentList.Contains("Registration Deposit"))
                                    {

                                        dropObj.suppFlow[i].orderSummary.OthPlanDeposit = 0;
                                    }

                                }
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("NationalityID Cannot be null");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.Message + " (" + e.StackTrace + ")");
            }

            return _inputPV;
        }


        public static string FormWaiverTableForMNP(int nationality)
        {
            System.Text.StringBuilder strWaiverTabl = new StringBuilder();
            Online.Registration.Web.ViewModels.MNPPrimaryPlanStorage objMNP = (Online.Registration.Web.ViewModels.MNPPrimaryPlanStorage)HttpContext.Current.Session["mNPPrimaryPlanStorage"];
            if (objMNP != null)
            {
                strWaiverTabl.Append("<tr class='title'><td>Primary MSISDN</td></tr>");
                if (nationality == 1)
                {
                    if (objMNP.RegMobileReg_MalyPlanDeposit.ToInt() > 0)
                    {
                        strWaiverTabl.Append(FormTableRow("Registration Deposit", objMNP.RegMobileReg_MalyPlanDeposit.ToString(), "0", "MP"));
                    }
                    if (objMNP.RegMobileReg_MalayPlanAdv.ToInt() > 0)
                    {
                        strWaiverTabl.Append(FormTableRow("Plan Advance", objMNP.RegMobileReg_MalayPlanAdv.ToString(), "0", "MP"));
                    }
                }
                else
                {
                    if (objMNP.RegMobileReg_OthPlanDeposit.ToInt() > 0)
                    {
                        strWaiverTabl.Append(FormTableRow("Registration Deposit", objMNP.RegMobileReg_OthPlanDeposit.ToString(), "0", "MP"));
                    }
                    if (objMNP.RegMobileReg_OthPlanAdv.ToInt() > 0)
                    {
                        strWaiverTabl.Append(FormTableRow("Plan Advance", objMNP.RegMobileReg_OthPlanAdv.ToString(), "0", "MP"));
                    }
                }
                if (objMNP.RegMobileReg_PkgPgmBdlPkgCompID.ToInt() > 0)
                {
                    strWaiverTabl.Append(FormWaiverComponents(objMNP.RegMobileReg_PkgPgmBdlPkgCompID, (List<string>)objMNP.RegMobileReg_VasNames, "MP"));

                }

                string suppMsidn = "1";
                foreach (var item in objMNP.MNPSupplinePlanStorageList)
                {

                    strWaiverTabl.Append("<tr class='title'><td>Supp MSISDN " + suppMsidn + "</td></tr>");
                    if (nationality == 1)
                    {
                        if (item.RegMobileReg_MalyPlanDeposit.ToInt() > 0)
                        {
                            //strWaiverTabl.Append(FormTableRow("Registration Deposit", item.RegMobileReg_MalyPlanDeposit.ToString(), "0", "MS" + suppMsidn));
                        }
                        if (item.RegMobileReg_MalayPlanAdv.ToInt() > 0)
                        {
                            strWaiverTabl.Append(FormTableRow("Plan Advance", item.RegMobileReg_MalayPlanAdv.ToString(), "0", "MS" + suppMsidn));
                        }
                    }
                    else
                    {
                        if (item.RegMobileReg_OthPlanDeposit.ToInt() > 0)
                        {
                            strWaiverTabl.Append(FormTableRow("Registration Deposit", item.RegMobileReg_OthPlanDeposit.ToString(), "0", "MS" + suppMsidn));
                        }
                        if (item.RegMobileReg_OthPlanAdv.ToInt() > 0)
                        {
                            strWaiverTabl.Append(FormTableRow("Plan Advance", item.RegMobileReg_OthPlanAdv.ToString(), "0", "MS" + suppMsidn));
                        }
                    }
                    if (item.RegMobileReg_PkgPgmBdlPkgCompID.ToInt() > 0)
                    {
                        strWaiverTabl.Append(FormWaiverComponents(item.RegMobileReg_PkgPgmBdlPkgCompID, item.RegMobileReg_OrderSummary.VasVM.VASesName, "MS" + suppMsidn));
                    }
                    suppMsidn = ((int.Parse(suppMsidn)) + 1).ToString();
                }

                return "<table>" + strWaiverTabl.ToString() + "</table>";
            }
            return "";
        }

        public static string FormWaiverTableForMNPNew(int nationality)
        {
            System.Text.StringBuilder strWaiverTabl = new StringBuilder();
            Online.Registration.Web.ViewModels.MNPPrimaryPlanStorage objMNP = (Online.Registration.Web.ViewModels.MNPPrimaryPlanStorage)HttpContext.Current.Session["mNPPrimaryPlanStorage"];
            if (objMNP != null)
            {
                if (nationality == 1)
                {
                    if (objMNP.RegMobileReg_MalyPlanDeposit.ToInt() > 0)
                    {
                        //strWaiverTabl.Append(FormTableRowNew("Registration Deposit", objMNP.RegMobileReg_MalyPlanDeposit.ToString(), "0", "MP"));
                    }
                    if (objMNP.RegMobileReg_MalayPlanAdv.ToInt() > 0)
                    {
                        strWaiverTabl.Append(FormTableRowNew("Plan Advance", objMNP.RegMobileReg_MalayPlanAdv.ToString(), "0", "MP"));
                    }
                }
                else
                {
                    if (objMNP.RegMobileReg_OthPlanDeposit.ToInt() > 0)
                    {
                        strWaiverTabl.Append(FormTableRowNew("Registration Deposit", objMNP.RegMobileReg_OthPlanDeposit.ToString(), "0", "MP"));
                    }
                    if (objMNP.RegMobileReg_OthPlanAdv.ToInt() > 0)
                    {
                        strWaiverTabl.Append(FormTableRowNew("Plan Advance", objMNP.RegMobileReg_OthPlanAdv.ToString(), "0", "MP"));
                    }
                }
                if (objMNP.RegMobileReg_PkgPgmBdlPkgCompID.ToInt() > 0)
                {
                    strWaiverTabl.Append(FormWaiverComponentsNew(objMNP.RegMobileReg_PkgPgmBdlPkgCompID, (List<string>)objMNP.RegMobileReg_VasNames, "MP"));

                }

                string suppMsidn = "1";
                int index = 1;
                foreach (var item in objMNP.MNPSupplinePlanStorageList)
                {

                    if (nationality == 1)
                    {
                        if (item.RegMobileReg_MalyPlanDeposit.ToInt() > 0)
                        {
                            //strWaiverTabl.Append(FormTableRowNew("Registration Deposit", item.RegMobileReg_MalyPlanDeposit.ToString(), "0", "MS" + suppMsidn));
                        }
                        if (item.RegMobileReg_MalayPlanAdv.ToInt() > 0)
                        {
                            strWaiverTabl.Append(FormTableRowNew("Plan Advance", item.RegMobileReg_MalayPlanAdv.ToString(), "0", "MS" + suppMsidn));
                        }
                    }
                    else
                    {
                        if (item.RegMobileReg_OthPlanDeposit.ToInt() > 0)
                        {
                            strWaiverTabl.Append(FormTableRowNew("Registration Deposit", item.RegMobileReg_OthPlanDeposit.ToString(), "0", "MS" + suppMsidn));
                        }
                        if (item.RegMobileReg_OthPlanAdv.ToInt() > 0)
                        {
                            strWaiverTabl.Append(FormTableRowNew("Plan Advance", item.RegMobileReg_OthPlanAdv.ToString(), "0", "MS" + suppMsidn));
                        }
                    }
                    index++;
                    if (item.RegMobileReg_PkgPgmBdlPkgCompID.ToInt() > 0)
                    {
                        strWaiverTabl.Append(FormWaiverComponentsNew(item.RegMobileReg_PkgPgmBdlPkgCompID, item.RegMobileReg_OrderSummary.VasVM.VASesName, "MS" + suppMsidn));
                    }
                    suppMsidn = ((int.Parse(suppMsidn)) + 1).ToString();
                }

                return strWaiverTabl.ToString();
            }
            return "";
        }

        #endregion

        public class retrievePenaltyByMSISDN
        {
            public string msgCode { get; set; }
            public string msgDesc { get; set; }

            public string startDate { get; set; }
            public string endDate { get; set; }
            public string contractType { get; set; }
            public string penalty { get; set; }
        }

        #region Properties Setting Members

        public static int GridRowsPerPage
        {
            get
            {
                return Settings.Default.GridRowsPerPage;
            }
        }

        #endregion

        #region Get ID

        public static int GetIDCardTypeID(IDCARDTYPE type)
        {
            switch (type)
            {
                case IDCARDTYPE.NRIC:
                    return GetIDByCode(RefType.IDCardType, Settings.Default.IDCardType_NRIC);
                default:
                    return 0;
            }
        }

        public static int GetOrgTypeID(ORGTYPE type)
        {
            int ID = 0;
            switch (type)
            {
                case ORGTYPE.Warehouse:
                    ID = GetIDByCode(RefType.OrgType, "WH");
                    break;
                case ORGTYPE.Dealer:
                    ID = GetIDByCode(RefType.OrgType, Properties.Settings.Default.OrgType_Dealer);
                    break;
            }
            return ID;
        }

        public static int GetPaymentModeID(PAYMENTMODE type)
        {
            switch (type)
            {
                case PAYMENTMODE.AutoBilling:
                    return GetIDByCode(RefType.PaymentMode, Settings.Default.PaymentMode_AutoBilling);
                case PAYMENTMODE.Cash:
                    return GetIDByCode(RefType.PaymentMode, Settings.Default.PaymentMode_Cash);
                default:
                    return 0;
            }
        }

        public static int GetStatusTypeID(STATUSTYPE type)
        {
            switch (type)
            {
                case STATUSTYPE.Registration:
                    return GetIDByCode(RefType.StatusType, Settings.Default.StatusType_Registration);
                case STATUSTYPE.Home:
                    return GetIDByCode(RefType.StatusType, Settings.Default.StatusType_Home);
                default:
                    return 0;
            }
        }

        public static int GetRegTypeID(REGTYPE type)
        {
            switch (type)
            {
                case REGTYPE.DevicePlan:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_DevicePlan);
                case REGTYPE.PlanOnly:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_PlanOnly);
                case REGTYPE.DeviceOnly:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_PlanOnly);
                case REGTYPE.Home:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_HomePersonal);
                case REGTYPE.NewLine:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_Newline);
                case REGTYPE.SuppPlan:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_Supp);
                case REGTYPE.SecPlan:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_Secp);
                case REGTYPE.MNPPlanOnly:
                    return GetIDByCode(RefType.RegType, Settings.Default.MNPPlanOnly);
                case REGTYPE.MNPSuppPlan:
                    return GetIDByCode(RefType.RegType, Settings.Default.MNPSuppPlan);
                case REGTYPE.MNPNewLine:
                    return GetIDByCode(RefType.RegType, Settings.Default.MNPNewLine);
                case REGTYPE.CRP:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_CRP);
                case REGTYPE.DeviceCRP:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_DeviceCRP);
                case REGTYPE.RegType_MNPPlanWithMultiSuppline:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_MNPPlanWithMultiSuppline);
                case REGTYPE.AddContract:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_AddContract);
                //GTM e-Billing CR - Ricky - 2014.09.25
                case REGTYPE.BillDelivery:
                    return GetIDByCode(RefType.RegType, Settings.Default.RegType_BillDelivery);
                default:
                    return 0;
            }
        }

        public static int GetNationalityID(NATIONALITY type)
        {
            switch (type)
            {
                case NATIONALITY.Malaysian:
                    return GetIDByCode(RefType.Nationality, Settings.Default.Nationality_Malaysian);
                default:
                    return 0;
            }
        }

        #endregion

        #region Get List Items
        public static List<SelectListItem> GetListDetails(RefType type, bool active = true, string defaultValue = "0", string defaultText = "default", int orgID = 0, string complaintIssueCode = "", string selectedValue = "", string simType = "")
        {
            List<SelectListItem> resultList = new List<SelectListItem>();
            if (defaultText == "default")
                defaultText = "-- Please Select --";// Settings.Default.SelectListHeader;

            switch (type)
            {
                case RefType.AddressType:
                    var listAddressType = MasterDataCache.Instance.Address;
                    if (listAddressType != null && listAddressType.AddressTypes.Any())
                    {
                        resultList = listAddressType.AddressTypes.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Description,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.AccountCategory:
                    var listAccountCategory = MasterDataCache.Instance.AccountCategory;
                    if (listAccountCategory != null && listAccountCategory.AccountCategories.Any())
                    {
                        resultList = listAccountCategory.AccountCategories.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Brand:
                    var listBrand = MasterDataCache.Instance.Brand;
                    if (listBrand != null && listBrand.Brands.Any())
                    {
                        resultList = listBrand.Brands.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Bundle:
                    var listBundle = MasterDataCache.Instance.Bundle;
                    if (listBundle != null && listBundle.Bundles.Any())
                    {
                        resultList = listBundle.Bundles.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.CardType:
                    var listCardType = MasterDataCache.Instance.CardType;
                    if (listCardType != null && listCardType.CardType.Any())
                    {
                        resultList = listCardType.CardType.Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.CustomerInfoCardType:
                    var listCustomerInfoCardType = MasterDataCache.Instance.CustomerInfoCardType;
                    if (listCustomerInfoCardType != null && listCustomerInfoCardType.CardTypePaymentDetails.Any())
                    {
                        resultList = listCustomerInfoCardType.CardTypePaymentDetails.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;
                case RefType.Category:
                    var listCategory = MasterDataCache.Instance.Category;
                    if (listCategory != null && listCategory.Categorys.Any())
                    {
                        resultList = listCategory.Categorys.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Colour:
                    var listColour = MasterDataCache.Instance.Colour;
                    if (listColour != null && listColour.Colours.Any())
                    {
                        resultList = listColour.Colours.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.ComplaintIssues:
                    var listComplainIssue = MasterDataCache.Instance.ComplaintIssues;
                    if (listComplainIssue != null && listComplainIssue.ComplaintIssuess.Any())
                    {
                        resultList = listComplainIssue.ComplaintIssuess.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Component: /***/
                    List<Component> ids = new List<Component>();
                    var listComponent = new CatalogServiceProxy().FindComponent(new ComponentFind() { Component = new Component() { }, Active = true });
                    if (listComponent != null)
                    {
                        resultList = listComponent.Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.ComponentType:
                    var listComponentType = MasterDataCache.Instance.ComponentType;
                    if (listComponentType != null && listComponentType.ComponentTypes.Any())
                    {
                        resultList = listComponentType.ComponentTypes.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Country:
                    var listCountry = MasterDataCache.Instance.Country;
                    if (listCountry != null && listCountry.Country.Any())
                    {
                        resultList = listCountry.Country.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.CustomerTitle:
                    var listCustomerTitle = MasterDataCache.Instance.CustomerTitle;
                    if (listCustomerTitle != null && listCustomerTitle.CustomerTitles.Any())
                    {
                        resultList = listCustomerTitle.CustomerTitles.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.IDCardType:
                    var listIDCardType = MasterDataCache.Instance.IDCardType;
                    if (listIDCardType != null && listIDCardType.IDCardType.Any())
                    {
                        resultList = listIDCardType.IDCardType.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Description,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Language:
                    var listLanguage = MasterDataCache.Instance.Language;
                    if (listLanguage != null && listLanguage.Languages.Any())
                    {
                        resultList = listLanguage.Languages.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Market:
                    var listMarket = MasterDataCache.Instance.Market;
                    if (listMarket != null && listMarket.Markets.Any())
                    {
                        resultList = listMarket.Markets.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Model:
                    var listModel = MasterDataCache.Instance.Model;
                    if (listModel != null && listModel.Models.Any())
                    {
                        resultList = listModel.Models.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Nationality:
                    var listNationality = MasterDataCache.Instance.Nationality;
                    if (listNationality != null && listNationality.Nationalities.Any())
                    {
                        resultList = listNationality.Nationalities.Where(a => a.Active == true).OrderBy(a => a.Name).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Organization:
                    var listOrganization = MasterDataCache.Instance.Organization;
                    if (listOrganization != null && listOrganization.Organizations.Any())
                    {
                        resultList = listOrganization.Organizations.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).OrderBy(a => a.Text).ToList();
                    }
                    break;

                case RefType.OrgType:
                    var listOrgType = MasterDataCache.Instance.OrgType;
                    if (listOrgType != null && listOrgType.OrgTypes.Any())
                    {
                        resultList = listOrgType.OrgTypes.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Package:
                    var listPackage = MasterDataCache.Instance.Package;
                    if (listPackage != null && listPackage.Packages.Any())
                    {
                        resultList = listPackage.Packages.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.PackageType:
                    var listPackageType = MasterDataCache.Instance.PackageType;
                    if (listPackageType != null && listPackageType.PackageTypes.Any())
                    {
                        resultList = listPackageType.PackageTypes.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.PaymentMode:
                    var listPaymentMode = MasterDataCache.Instance.PaymentMode;
                    if (listPaymentMode != null && listPaymentMode.PaymentMode.Any())
                    {
                        resultList = listPaymentMode.PaymentMode.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.CustomerInfoPaymentMode: /****/
                    var listPaymentModePaymentDetails = new RegistrationServiceProxy().FindPaymentModePaymentDetails(new PaymentModePaymentDetailsFind() { PaymentModePaymentDetails = new PaymentModePaymentDetails() { }, Active = true });
                    if (listPaymentModePaymentDetails != null)
                    {
                        resultList = listPaymentModePaymentDetails.Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;


                case RefType.Program:
                    var listProgram = MasterDataCache.Instance.Program;
                    if (listProgram != null && listProgram.Programs.Any())
                    {
                        resultList = listProgram.Programs.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Race:
                    var listRace = MasterDataCache.Instance.Race;
                    if (listRace != null && listRace.Races.Any())
                    {
                        resultList = listRace.Races.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.State:
                    var listState = MasterDataCache.Instance.State;
                    if (listState != null && listState.State.Any())
                    {
                        resultList = listState.State.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.StatusReason:
                    var listStatusReason = MasterDataCache.Instance.StatusReason;
                    if (listStatusReason != null && listStatusReason.StatusReasons.Any())
                    {
                        resultList = listStatusReason.StatusReasons.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.StatusType:
                    var listStatusType = MasterDataCache.Instance.StatusType;
                    if (listStatusType != null && listStatusType.StatusTypes.Any())
                    {
                        resultList = listStatusType.StatusTypes.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Description,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Status:
                    var listStatus = MasterDataCache.Instance.Status;
                    if (listStatus != null && listStatus.Statuss.Any())
                    {
                        resultList = listStatus.Statuss.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Description,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.Access:
                    var listAccess = MasterDataCache.Instance.Access;
                    if (listAccess != null && listAccess.Accesss.Any())
                    {
                        resultList = listAccess.Accesss.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Description,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.UserGroup:
                    var listUserGroup = MasterDataCache.Instance.UserGroup;
                    if (listUserGroup != null && listUserGroup.UserGroups.Any())
                    {
                        resultList = listUserGroup.UserGroups.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.User:
                    var listUser = MasterDataCache.Instance.User;
                    if (listUser != null && listUser.Users.Any())
                    {
                        resultList = listUser.Users.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.FullName,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.UserRole:
                    var lisUserRole = MasterDataCache.Instance.UserRole;
                    if (lisUserRole != null && lisUserRole.UserRoles.Any())
                    {
                        resultList = lisUserRole.UserRoles.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Description,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.ModelGroup:
                    var listModelGroup = MasterDataCache.Instance.ModelGroup;
                    if (listModelGroup != null && listModelGroup.ModelGroups.Any())
                    {
                        resultList = listModelGroup.ModelGroups.Where(a => a.Active == true).Select(a => new SelectListItem()
                        {
                            Text = a.Name,
                            Value = a.ID.ToString()
                        }).ToList();
                    }
                    break;
                case RefType.Doners:
                    var listDoners = MasterDataCache.Instance.Doners;

                    if (listDoners != null && listDoners.tblDonorlst.Any())
                    {
                        resultList = listDoners.tblDonorlst.Select(a => new SelectListItem()
                        {
                            Text = a.Donor,
                            Value = a.DonorID
                        }).ToList();
                    }
                    break;

                case RefType.SimModelType:
                    var listSIMModels = MasterDataCache.Instance.SimModelType;

                    if (listSIMModels != null && listSIMModels.SimModels.Any())
                    {
                        if (simType == "Normal")
                            resultList = listSIMModels.SimModels.Where(a => !(a.SimTypeDescription.Contains("MISM"))).Select(a => new SelectListItem()
                            {
                                Text = a.SimTypeDescription,
                                Value = a.ID.ToString() + "," + a.ArticleId.ToString()
                            }).ToList();
                        else
                            resultList = listSIMModels.SimModels.Where(a => a.SimTypeDescription.Contains("MISM")).Select(a => new SelectListItem()
                            {
                                Text = a.SimTypeDescription,
                                Value = a.ID.ToString() + "," + a.ArticleId.ToString()
                            }).ToList();
                    }
                    break;

                case RefType.SimReplacementReason:
                    var listSIMReplacementResons = MasterDataCache.Instance.SimReplacementReason;
                    if (listSIMReplacementResons != null && listSIMReplacementResons.ReplacementReasons.Any())
                    {
                        resultList = listSIMReplacementResons.ReplacementReasons.Select(a => new SelectListItem()
                        {
                            Text = a.DISPLAY_VALUE,
                            Value = a.Id.ToString()
                        }).ToList();
                    }
                    break;

                case RefType.SimReplacementprePaidReason:
                    var listSIMReplacementPrepaidResons = MasterDataCache.Instance.SimReplacementprePaidReason;
                    if (listSIMReplacementPrepaidResons != null && listSIMReplacementPrepaidResons.ReplacementReasons.Any())
                    {
                        resultList = listSIMReplacementPrepaidResons.ReplacementReasons.Select(a => new SelectListItem()
                        {
                            Text = a.DISPLAY_VALUE,
                            Value = a.Id.ToString()
                        }).ToList();
                    }
                    break;
                //End by VLT
                default:
                    break;
            }
            List<SelectListItem> list = new List<SelectListItem>();
            #region VLT ADDED CODE
            int i = 0;
            if (!string.IsNullOrWhiteSpace(selectedValue) && ((resultList.Exists(c => c.Text == selectedValue) || (resultList.Exists(c => c.Value == selectedValue)))))
            {
                for (i = 0; i < resultList.Count; i++)
                {
                    if (resultList[i].Text != selectedValue)
                        list.Add(new SelectListItem() { Text = resultList[i].Text, Value = resultList[i].Value, Selected = false });
                    else
                        list.Add(new SelectListItem() { Text = resultList[i].Text, Value = resultList[i].Value, Selected = true });
                }
                list.Insert(resultList.Count, new SelectListItem()
                {
                    Text = defaultText,
                    Value = defaultValue
                });
            }
            else
            {
                for (i = 0; i < resultList.Count; i++)
                {
                    list.Add(new SelectListItem() { Text = resultList[i].Text, Value = resultList[i].Value, Selected = true });
                }
            }
            #endregion VLT ADDED CODE
            return list;
        }

        public static List<SelectListItem> GetList(RefType type, bool active = true, string defaultValue = "0", string defaultText = "default", int orgID = 0, string complaintIssueCode = "", string selectedValue = "", string simType = "", int simreplaceType = 0, string kenanRaceId = "-1", bool defaultSelected = true, string uomCode ="")
        {
            // List<SelectListItem> resultList = new List<SelectListItem>();
            var resultList = new List<SelectListItem>();
            var list = new List<SelectListItem>();
            try
            {
                if (defaultText == "default")
                    defaultText = "-- Please Select --";

                //   defaultText = "-- Please Select --";// Settings.Default.SelectListHeader;
                string msisdnType = string.Empty;

                switch (type)
                {
                    case RefType.AddressType:
                        var listAddressType = MasterDataCache.Instance.Address;
                        if (listAddressType != null && listAddressType.AddressTypes.Any())
                        {
                            resultList = listAddressType.AddressTypes.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.AccountCategory:
                        var listAccountCategory = MasterDataCache.Instance.AccountCategory;
                        if (listAccountCategory != null && listAccountCategory.AccountCategories.Any())
                        {
                            resultList = listAccountCategory.AccountCategories.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Brand:
                        var listBrand = MasterDataCache.Instance.Brand;
                        if (listBrand != null && listBrand.Brands.Any())
                        {
                            resultList = listBrand.Brands.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Bundle:
                        var listBundle = MasterDataCache.Instance.Bundle;
                        if (listBundle != null && listBundle.Bundles.Any())
                        {
                            resultList = listBundle.Bundles.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.CardType:
                        var listCardType = MasterDataCache.Instance.CardType;
                        if (listCardType != null && listCardType.CardType.Any())
                        {
                            resultList = listCardType.CardType.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.CustomerInfoCardType:
                        var listCustomerInfoCardType = MasterDataCache.Instance.CustomerInfoCardType;
                        if (listCustomerInfoCardType != null && listCustomerInfoCardType.CardTypePaymentDetails.Any())
                        {
                            resultList = listCustomerInfoCardType.CardTypePaymentDetails.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;
                    case RefType.Category:
                        var listCategory = MasterDataCache.Instance.Category;
                        if (listCategory != null && listCategory.Categorys.Any())
                        {
                            resultList = listCategory.Categorys.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Colour:
                        var listColour = MasterDataCache.Instance.Colour;
                        if (listColour != null && listColour.Colours.Any())
                        {
                            resultList = listColour.Colours.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.ComplaintIssues:
                        if (complaintIssueCode == "PR")
                        {
                            var listComplainIssue = MasterDataCache.Instance.ComplaintIssues;
                            if (listComplainIssue != null && listComplainIssue.ComplaintIssuess.Any())
                            {
                                resultList = listComplainIssue.ComplaintIssuess.Where(a => a.Active == true && a.Type.Trim().ToUpper() == complaintIssueCode.Trim().ToUpper()).Select(a => new SelectListItem()
                                {
                                    Text = a.Name,
                                    Value = a.ID.ToString()
                                }).OrderBy(o => o.Text).ToList();
                            }
                        }
                        break;

                    case RefType.Component: //***//
                        List<Component> ids = new List<Component>();
                        var listComponent = new CatalogServiceProxy().FindComponent(new ComponentFind() { Component = new Component() { }, Active = true });
                        if (listComponent != null)
                        {
                            resultList = listComponent.Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.ComponentType: //***//
                        var listComponentType = new CatalogServiceProxy().FindComponentType(new ComponentTypeFind() { ComponentType = new ComponentType() { }, Active = true });
                        if (listComponentType != null)
                        {
                            resultList = listComponentType.Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Country:
                        var listCountry = MasterDataCache.Instance.Country;
                        if (listCountry != null && listCountry.Country.Any())
                        {
                            resultList = listCountry.Country.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.CustomerTitle:
                        var listCustomerTitle = MasterDataCache.Instance.CustomerTitle;
                        if (listCustomerTitle != null && listCustomerTitle.CustomerTitles.Any())
                        {
                            var titleListResult = new List<CustomerTitle>();
                            var commonUsedTitleList = listCustomerTitle.CustomerTitles.Where(a => a.Active == true && a.DisplaySequence != null).OrderBy(x => x.DisplaySequence).ToList();
                            var uncommonUsedTitleList = listCustomerTitle.CustomerTitles.Where(a => a.Active == true && a.DisplaySequence == null).OrderBy(x => x.Name).ToList();
                            titleListResult.AddRange(commonUsedTitleList);
                            titleListResult.AddRange(uncommonUsedTitleList);

                            resultList = titleListResult.Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.IDCardType:
                        var listIDCardType = MasterDataCache.Instance.IDCardType;
                        if (listIDCardType != null && listIDCardType.IDCardType.Any())
                        {
                            resultList = listIDCardType.IDCardType.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.IdCardTypes:
                        var listIDCardTypes = MasterDataCache.Instance.IDCardType;
                        if (listIDCardTypes != null && listIDCardTypes.IDCardType.Any())
                        {
                            resultList = listIDCardTypes.IDCardType.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.Code.ToString() + "," + a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Language:
                        var listLanguage = MasterDataCache.Instance.Language;
                        if (listLanguage != null && listLanguage.Languages.Any())
                        {
                            resultList = listLanguage.Languages.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Market:
                        var listMarket = MasterDataCache.Instance.Market;
                        if (listMarket != null && listMarket.Markets.Any())
                        {
                            resultList = listMarket.Markets.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Model:
                        var listModel = MasterDataCache.Instance.Model;
                        if (listModel != null && listModel.Models.Any())
                        {
                            resultList = listModel.Models.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Nationality:
                        var listNationality = MasterDataCache.Instance.Nationality;
                        if (listNationality != null && listNationality.Nationalities.Any())
                        {
                            resultList = listNationality.Nationalities.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Organization:
                        var listOrganization = MasterDataCache.Instance.Organization;
                        if (listOrganization != null && listOrganization.Organizations.Any())
                        {
                            resultList = listOrganization.Organizations.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).OrderBy(a => a.Text).ToList();
                        }
                        break;

                    case RefType.OrgType: //****//
                        var listOrgType = new OrganizationServiceProxy().FindOrgType(new OrgTypeFind() { OrgType = new OrgType() { }, Active = true });
                        if (listOrgType != null)
                        {
                            resultList = listOrgType.Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Package:
                        var listPackage = MasterDataCache.Instance.Package;
                        if (listPackage != null && listPackage.Packages.Any())
                        {
                            resultList = listPackage.Packages.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.PackageType: //****//
                        var listPackageType = new CatalogServiceProxy().FindPackageType(new PackageTypeFind() { PackageType = new PackageType() { }, Active = true });
                        if (listPackageType != null)
                        {
                            resultList = listPackageType.Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.PaymentMode:
                        var listPaymentMode = MasterDataCache.Instance.PaymentMode;
                        if (listPaymentMode != null && listPaymentMode.PaymentMode.Any())
                        {
                            resultList = listPaymentMode.PaymentMode.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.CustomerInfoPaymentMode: //****//
                        var listPaymentModePaymentDetails = new RegistrationServiceProxy().FindPaymentModePaymentDetails(new PaymentModePaymentDetailsFind() { PaymentModePaymentDetails = new PaymentModePaymentDetails() { }, Active = true });
                        if (listPaymentModePaymentDetails != null)
                        {
                            resultList = listPaymentModePaymentDetails.Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;


                    case RefType.Program:
                        var listProgram = MasterDataCache.Instance.Program;
                        if (listProgram != null && listProgram.Programs.Any())
                        {
                            resultList = listProgram.Programs.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Race:
                        var listRace = MasterDataCache.Instance.Race;
                        if (listRace != null && listRace.Races.Any())
                        {
                            Race existingCustRace = null;
                            if (!string.IsNullOrEmpty(kenanRaceId) && kenanRaceId != "-1")
                            {
                                existingCustRace = listRace.Races.Where(a => a.Active == true && a.KenanCode == kenanRaceId).FirstOrDefault();
                            }

                            resultList = listRace.Races.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();

                            try
                            {
                                if (!string.IsNullOrEmpty(kenanRaceId) && kenanRaceId != "-1" && existingCustRace != null)
                                {
                                    foreach (var rl in resultList)
                                    {
                                        if (rl.Value.ToInt() == existingCustRace.ID)
                                        {
                                            rl.Selected = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                ;
                            }
                        }
                        break;

                    case RefType.State:
                        var listState = MasterDataCache.Instance.State;
                        if (listState != null && listState.State.Any())
                        {
                            resultList = listState.State.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.StatusReason:
                        var listStatusReason = MasterDataCache.Instance.StatusReason;
                        if (listStatusReason != null && listStatusReason.StatusReasons.Any())
                        {
                            resultList = listStatusReason.StatusReasons.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.StatusType:
                        var listStatusType = MasterDataCache.Instance.StatusType;
                        if (listStatusType != null && listStatusType.StatusTypes.Any())
                        {
                            resultList = listStatusType.StatusTypes.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Status:
                        var listStatus = MasterDataCache.Instance.Status;
                        if (listStatus != null && listStatus.Statuss.Any())
                        {
                            resultList = listStatus.Statuss.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.Access:
                        var listAccess = MasterDataCache.Instance.Access;
                        if (listAccess != null && listAccess.Accesss.Any())
                        {
                            resultList = listAccess.Accesss.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.AccessUserByOrg:
                        var listAccessOrg = MasterDataCache.Instance.Access;
                        var listUserMaster = MasterDataCache.Instance.User;
                        int OrgId;

                        if (Roles.IsUserInRole("MREG_HQ"))
                            // sending 0 for Head Quarter supervisor
                            OrgId = 0;
                        else
                            // sending orginal orgid for current store/branch/organization
                            OrgId = Util.SessionAccess.User.OrgID;

                        var listUserOrg = listUserMaster.Users.Where(x => x.OrgID == OrgId);

                        if(listUserOrg != null && listUserOrg.Any())
                        {
                            foreach (var itm in listUserOrg)
                            {
                                var result = listAccessOrg.Accesss.Where(a => a.Active == true && a.UserID == itm.ID).Select(a => new SelectListItem()
                                {
                                    Text = a.UserName,
                                    Value = a.UserName
                                });
                                resultList.AddRange(result);
                            }
                            resultList = resultList.OrderBy(a => a.Value).ToList();
                        }
                        break;

                    case RefType.UserGroup:
                        var listUserGroup = MasterDataCache.Instance.UserGroup;
                        if (listUserGroup != null && listUserGroup.UserGroups.Any())
                        {
                            resultList = listUserGroup.UserGroups.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.User:
                        var listUser = MasterDataCache.Instance.User;
                        if (listUser != null && listUser.Users.Any())
                        {
                            resultList = listUser.Users.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.FullName,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.UserRole:
                        var lisUserRole = MasterDataCache.Instance.UserRole;
                        if (lisUserRole != null && lisUserRole.UserRoles.Any())
                        {
                            resultList = lisUserRole.UserRoles.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;

                    case RefType.ModelGroup:
                        var listModelGroup = MasterDataCache.Instance.ModelGroup;
                        if (listModelGroup != null && listModelGroup.ModelGroups.Any())
                        {
                            resultList = listModelGroup.ModelGroups.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;
                    case RefType.Doners: //****//
                        var listDoners = new UserServiceProxy().GetDonors();
                        if (listDoners != null)
                        {
                            resultList = listDoners.tblDonorlst.Select(a => new SelectListItem()
                            {
                                Text = a.Donor,
                                Value = a.DonorID
                            }).ToList();
                        }
                        break;

                    case RefType.SimModelType: //****//
                        var listSIMModels = new CatalogServiceProxy().GetSIMModels();

                        if (listSIMModels != null)
                        {
                           // if (simType == "Normal" || simType == "FWBB") //10-12-2015 Karyne:FWBB uses HSDPA sim
                            if (simType == "Normal")
                            {
                                if (!string.IsNullOrEmpty(uomCode))
                                {
                                    resultList = listSIMModels.SimModels.Where(a => !(a.SimTypeDescription.Contains("MISM")) && !(a.SimTypeDescription.Contains("Prepaid")) && !(a.SimTypeDescription.Contains("HSDPA")) && a.UOM == uomCode).Select(a => new SelectListItem()
                                    {
                                        Text = a.SimTypeDescription,
                                        Value = a.ID.ToString() + "," + a.ArticleId.ToString()
                                    }).ToList();
                                }
                                else
                                {
                                    resultList = listSIMModels.SimModels.Where(a => !(a.SimTypeDescription.Contains("MISM")) && !(a.SimTypeDescription.Contains("Prepaid")) && !(a.SimTypeDescription.Contains("HSDPA"))).Select(a => new SelectListItem()
                                    {
                                        Text = a.SimTypeDescription,
                                        Value = a.ID.ToString() + "," + a.ArticleId.ToString()
                                    }).ToList();
                                }
                            }
                            else if (simType == "Prepaid")
                            {
                                if (!string.IsNullOrEmpty(uomCode))
                                {
                                    resultList = listSIMModels.SimModels.Where(a => a.SimTypeDescription.Contains("Prepaid") && a.UOM == uomCode).Select(a => new SelectListItem()
                                    {
                                        Text = a.SimTypeDescription,
                                        Value = a.ID.ToString() + "," + a.ArticleId.ToString()
                                    }).ToList();
                                }
                                else
                                {
                                    resultList = listSIMModels.SimModels.Where(a => a.SimTypeDescription.Contains("Prepaid")).Select(a => new SelectListItem()
                                    {
                                        Text = a.SimTypeDescription,
                                        Value = a.ID.ToString() + "," + a.ArticleId.ToString()
                                    }).ToList();
                                }
                            }
                            else if (simType == "HSDPA" || simType == "FWBB")
                            {
                                if (!string.IsNullOrEmpty(uomCode))
                                {
                                    resultList = listSIMModels.SimModels.Where(a => a.SimTypeDescription.Contains("HSDPA") && a.UOM == uomCode).Select(a => new SelectListItem()
                                    {
                                        Text = a.SimTypeDescription,
                                        Value = a.ID.ToString() + "," + a.ArticleId.ToString()
                                    }).ToList();
                                }
                            else
                            {
                                    resultList = listSIMModels.SimModels.Where(a => a.SimTypeDescription.Contains("HSDPA")).Select(a => new SelectListItem()
                                    {
                                        Text = a.SimTypeDescription,
                                        Value = a.ID.ToString() + "," + a.ArticleId.ToString()
                                    }).ToList();
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(uomCode))
                                {
                                    resultList = listSIMModels.SimModels.Where(a => a.SimTypeDescription.Contains("MISM") && a.UOM == uomCode ).Select(a => new SelectListItem()
                                    {
                                        Text = a.SimTypeDescription,
                                        Value = a.ID.ToString() + "," + a.ArticleId.ToString()
                                    }).ToList();
                                }
                                else
                                {
                                    resultList = listSIMModels.SimModels.Where(a => a.SimTypeDescription.Contains("MISM")).Select(a => new SelectListItem()
                                    {
                                        Text = a.SimTypeDescription,
                                        Value = a.ID.ToString() + "," + a.ArticleId.ToString()
                                    }).ToList();
                                }
                            }
                        }

                        break;


                    case RefType.AllSimModelType: //****//
                        var listAllSIMModels = new CatalogServiceProxy().GetSIMModels();

                        if (listAllSIMModels != null)
                        {

                            resultList = listAllSIMModels.SimModels.Select(a => new SelectListItem()
                            {
                                Text = a.SimTypeDescription,
                                Value = a.ID.ToString() + "," + a.ArticleId.ToString()
                            }).ToList();
                        }

                        break;


                    case RefType.SimReplacementReason: //****//
                        var listSIMReplacementResons = new CatalogServiceProxy().GetSimReplacementReasons(true);
                        var array = new string[] { "personal/customerrequest", "lost/stolen/damagedhandset/card", "replacement" };
                        if (listSIMReplacementResons != null)
                        {
                            resultList = listSIMReplacementResons.ReplacementReasons.Select(a => new SelectListItem()
                            {
                                Text = a.DISPLAY_VALUE,
                                Value = a.Id.ToString()
                            }).ToList();
                        }

                        break;
                    case RefType.SimReplacementprePaidReason: //****//
                        var listSIMReplacementPrepaidResons = new CatalogServiceProxy().GetSimReplacementReasons(false);

                        if (listSIMReplacementPrepaidResons != null)
                        {
                            resultList = listSIMReplacementPrepaidResons.ReplacementReasons.Select(a => new SelectListItem()
                            {
                                Text = a.DISPLAY_VALUE,
                                Value = a.Id.ToString()
                            }).ToList();

                        }
                        break;

                    case RefType.Msisdn: //****//
                        retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)SessionManager.Get("PPIDInfo");
                        resultList = AcctListByICResponse.itemList.Where(a => a.ExternalId != null).ToList().Select(a => new SelectListItem()
                        {
                            Text = a.ExternalId,
                            Value = a.ExternalId
                        }).ToList();

                        break;

                    case RefType.SIMCardType: //****//
                        var listSIMTypes = new CatalogServiceProxy().GetSIMCardTypes();
                        if (listSIMTypes != null)
                        {
                            resultList = listSIMTypes.SIMCardTypes.Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.Code
                            }).ToList();
                        }
                        break;
                    case RefType.Capacity: //****//
                        var listDeviceCapacity = MasterDataCache.Instance.DeviceCapacity;
                        if (listDeviceCapacity != null)
                        {
                            resultList = listDeviceCapacity.DeviceCapacity.Select(a => new SelectListItem()
                            {
                                Text = a.Capacity,
                                Value = a.ID.ToString2()
                            }).ToList();
                        }
                        break;

                    case RefType.DeviceType: //****//
                        var listDeviceType = MasterDataCache.Instance.DeviceType;
                        if (listDeviceType != null)
                        {
                            resultList = listDeviceType.DeviceTypes.Select(a => new SelectListItem()
                            {
                                Text = a.DeviceType,
                                Value = a.ID.ToString2()
                            }).ToList();
                        }
                        break;
                    case RefType.Region:
                        var listRegion = new CatalogServiceProxy().GetRegion();
                        if (listRegion != null)
                        {
                            resultList = listRegion.Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString2()
                            }).ToList();
                        }
                        break;
                    case RefType.PriceRange: //****//
                        var listPriceRange = MasterDataCache.Instance.PriceRange;
                        if (listPriceRange != null)
                        {
                            resultList = listPriceRange.PriceRange.Select(a => new SelectListItem()
                            {
                                Text = a.DisplayText,
                                Value = a.Value
                            }).ToList();
                        }
                        break;
                    case RefType.RegTypeWaiveOff:
                        var listRegTypeWO = new List<SelectListItem>();
                        using (var proxy = new CatalogServiceProxy())
                        {
                            var lovList = proxy.GetLovList("REG_TYPE");
                            
                            foreach (var lov in lovList)
                            {
                                resultList.Add(new SelectListItem()
                                {
                                    Text = lov.Source,
                                    Value = lov.Value
                                });
                            }
                        }
                        break;
					case RefType.NBAOthRecommendations:
						using (var proxy = new CatalogServiceProxy())
						{
							var lovList = MasterDataCache.Instance.GetAllLovList.Where(x=> x.Type == Constants.NBA_RECOMMENDATION);

							foreach (var lov in lovList)
							{
								resultList.Add(new SelectListItem()
								{
									Text = lov.Source,
									Value = lov.Value
								});
							}

						}
						break;
                    case RefType.PegaResponse:
                        using (var proxy = new CatalogServiceProxy())
                        {
                            var lovList = proxy.GetLovList("NBA_RESPONSE");

                            foreach (var lov in lovList)
                        {
                                resultList.Add(new SelectListItem()
                            {
                                    Text = lov.Source,
                                    Value = lov.Value
                                });
                            }
                        }
                        break;

                    case RefType.PegaStatus:
                        using (var proxy = new CatalogServiceProxy())
                        {
                            var lovList = proxy.GetLovList("NBA_STATUS");

                            foreach (var lov in lovList)
                            {
                                resultList.Add(new SelectListItem()
                                {
                                    Text = lov.Source,
                                    Value = lov.Value
                                });
                            }
                        }
                        break;
                    case RefType.PegaStatusRule:
                        using (var proxy = new CatalogServiceProxy())
                        {
                            var lovList = proxy.GetLovList("NBA_STATUS_RULE");

                            foreach (var lov in lovList)
                            {
                                if (lov.Value == defaultText)
                        {
                                    resultList.Add(new SelectListItem()
                            {
                                        Text = lov.Source,
                                        Value = lov.Value
                                    });
                                }
                            }
                        }
                        break;
                    case RefType.ThirdPartyAuthType:
                        var listThirdPartyAuthType = MasterDataCache.Instance.ThirdPartyAuthType;
                        if (listThirdPartyAuthType != null && listThirdPartyAuthType.ThirdPartyAuthTypes.Any())
                        {
                            resultList = listThirdPartyAuthType.ThirdPartyAuthTypes.Select(a => new SelectListItem()
                            {
                                Text = a.Name,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                        break;
                    case RefType.TransferOwnershipIDType:
                        var listTransferOwnershipIdType = MasterDataCache.Instance.IDCardType;
                        if (listTransferOwnershipIdType != null && listTransferOwnershipIdType.IDCardType.Any())
                        {
                            resultList = listTransferOwnershipIdType.IDCardType.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                            resultList[3].Text = "";
                        }
                        break;
                    default:
                        break;
                }

                int selValue = 0;
                #region VLT ADDED CODE
                int i = 0;
                if (!string.IsNullOrWhiteSpace(selectedValue) && ((resultList.Exists(c => c.Text.ToLower() == selectedValue.ToLower()) || (resultList.Exists(c => c.Value == selectedValue)))))
                {


                    for (i = 0; i < resultList.Count; i++)
                    {
                        if (resultList[i].Text.ToLower() != selectedValue.ToLower() && resultList[i].Value.ToLower() != selectedValue.ToLower())
                        {
                            list.Add(new SelectListItem() { Text = resultList[i].Text, Value = resultList[i].Value, Selected = false });
                        }
                        else
                        {
                            list.Add(new SelectListItem() { Text = resultList[i].Text, Value = resultList[i].Value, Selected = true });
                        }
                    }
                    // Fix - 2554 - Chokks - State Johor not populated in the drop down - Start
                    if (type != RefType.PaymentMode && type != RefType.SIMCardType)
                    {
                        list.Insert(0, new SelectListItem()
                        {
                            Text = defaultText,
                            Value = defaultValue,
                            Selected = false
                        });
                    }
                    // Fix - 2554 - Chokks - State Johor not populated in the drop down - End

                }
                else if (!string.IsNullOrWhiteSpace(selectedValue) && msisdnType != "msisdn" && Int32.TryParse(selectedValue, out selValue) ? selectedValue.ToInt() > 0 : false)
                {
                    for (i = 0; i < resultList.Count; i++)
                    {

                        list.Add(new SelectListItem() { Text = resultList[i].Text, Value = resultList[i].Value, Selected = false });

                    }
                    list.Insert(0, new SelectListItem()
                    {
                        Text = defaultText,
                        Value = defaultValue,
                        Selected = false
                    });
                    var selectedItem = list.Where(a => a.Value.Split(',')[0] == selectedValue).FirstOrDefault();
                    if (selectedItem != null)
                    {
                        selectedItem.Selected = true;
                    }
                }
                else
                {
                    for (i = 0; i < resultList.Count; i++)
                    {

                        list.Add(new SelectListItem() { Text = resultList[i].Text, Value = resultList[i].Value, Selected = resultList[i].Selected });

                    }
                    if (type != RefType.Msisdn)
                    {
                        if (defaultSelected)
                        {
                            list.Insert(0, new SelectListItem()
                            {
                                Text = defaultText,
                                Value = defaultValue,
                                Selected = true
                            });
                        }
                        else
                        {
                            list.Insert(0, new SelectListItem()
                            {
                                Text = defaultText,
                                Value = defaultValue,
                                Selected = false
                            });
                        }
                    }
                }
                #endregion VLT ADDED CODE

				if(type == RefType.NBAOthRecommendations)
				{
					if (defaultValue != "0")
					{
						var pleaseSelectIndex = list.FindIndex(x => x.Text.ToUpper().Contains("PLEASE SELECT"));
						if (pleaseSelectIndex > -1)
						{
							list.RemoveAt(pleaseSelectIndex);
							var defaultValueIndex = list.FindIndex(x => x.Value == defaultValue);
							list = swap(list, 0, defaultValueIndex);
						}
						else {
							var defaultValueIndex = list.FindIndex(x => x.Value == defaultValue);
							list = swap(list, 0, defaultValueIndex);
						}
						
					}
				}

            }
            catch (Exception ex)
            {
                string strErrorMsg = Util.LogException(ex);
                string inputParams = "RefType : " + type + " , active : " + active + ", defaultValue : " + defaultValue + ", defaultText : " + defaultText + ", orgID : " + orgID + " , complaintIssueCode  : " + complaintIssueCode + ", selectedValue : " + selectedValue + " , simType : " + simType;
                Logger.Error("Exception in Helper\\Util.cs:GetList(): " + "\n" + "Input Parameters : - " + inputParams + "\n" + " Error:" + strErrorMsg, ex);


            }

            if( type != RefType.PegaStatusRule )
                list = list.GroupBy(a => a.Text).Select(x => x.First()).ToList();
            
            return list;
        }

		public static List<SelectListItem> swap(List<SelectListItem> obj, int oldIndex, int newIndex)
		{
			var tempObj = obj[oldIndex];
			obj[oldIndex] = obj[newIndex];
			obj[newIndex] = tempObj;

			return obj;
		}

        public static List<SelectListItem> GetStatusList(STATUSTYPE type, string defaultValue = "0", string defaultText = "default")
        {
            var resultList = new List<SelectListItem>();
            int statusTypeID = GetStatusTypeID(type);

            if (defaultText == "default")
                defaultText = "-- Please Select --";

            switch (type)
            {
                case STATUSTYPE.Registration:
                case STATUSTYPE.Home:
                    var status = MasterDataCache.Instance.Status;
                    if (status != null && status.Statuss.Any())
                    {
                        var statusList = status.Statuss.Where(a => a.StatusTypeID == statusTypeID);
                        if (statusList != null && statusList.Any())
                        {
                            resultList = statusList.Where(a => a.Active == true).Select(a => new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                        }
                    }
                    break;

                default:
                    break;
            }

            resultList.Insert(0, new SelectListItem()
            {
                Text = defaultText,
                Value = defaultValue,
                Selected = true
            });

            return resultList;
        }

        public static CustomizedCustomer GetCustomerList(retrieveAcctListByICResponse objretrieveAcctListByICResponse, string msisdn)
        {
            CustomizedCustomer CustomerPersonalInfo = null;
            var Session = HttpContext.Current.Session;
            if (Session["AccountType"].ToString2() != "MISMSec")
            {
                if (Session["AccountType"].ToString2().ToUpper() == "S")
                {
                    if (objretrieveAcctListByICResponse.itemList.Any(i => (i.Customer != null && i.PrinSupplimentaryResponse != null && i.PrinSupplimentaryResponse.itemList.Any(s => s.msisdnField.Equals(Session[SessionKey.ExternalID.ToString()].ToString2())))))
                        CustomerPersonalInfo = objretrieveAcctListByICResponse.itemList.FirstOrDefault(i => (i.Customer != null && i.PrinSupplimentaryResponse != null && i.PrinSupplimentaryResponse.itemList.Any(s => s.msisdnField.Equals(Session[SessionKey.ExternalID.ToString()].ToString2())))).Customer;

                    if (CustomerPersonalInfo == null)
                    {
                        if (objretrieveAcctListByICResponse.itemList.Any(i => (i.Customer != null && i.SecondarySimList != null && i.SecondarySimList.Any(s => s.Msisdn.Equals(Session[SessionKey.ExternalID.ToString()].ToString2())))))
                            CustomerPersonalInfo = objretrieveAcctListByICResponse.itemList.FirstOrDefault(i => (i.Customer != null && i.SecondarySimList != null && i.SecondarySimList.Any(s => s.Msisdn.Equals(Session[SessionKey.ExternalID.ToString()].ToString2())))).Customer;
                    }
                }
                else
                {
                    if (objretrieveAcctListByICResponse.itemList.Where(c => c.Customer != null && c.ExternalId.ToString2() == msisdn.ToString2()).ToList().FirstOrDefault() != null)
                    {
                        // PBI000000008573 - Gerry, put Active Flag
                        CustomerPersonalInfo = objretrieveAcctListByICResponse.itemList.Where(c => c.Customer != null && c.ExternalId.ToString2() == msisdn.ToString2() && c.IsActive).ToList().FirstOrDefault().Customer;
                    }

                }

                if (CustomerPersonalInfo == null)
                {
                    if (objretrieveAcctListByICResponse.itemList.Where(c => c.Customer != null && c.ExternalId.ToString2() == msisdn.ToString2()).ToList().FirstOrDefault() != null)
                    {
                        CustomerPersonalInfo = objretrieveAcctListByICResponse.itemList.Where(c => c.Customer != null && c.ExternalId.ToString2() == msisdn.ToString2()).ToList().FirstOrDefault().Customer;
                    }
                }
            }
            else //"MISMSec"
            {
                if (objretrieveAcctListByICResponse.itemList.Any(i => (i.Customer != null && i.SecondarySimList != null && i.SecondarySimList.Any(s => s.Msisdn.Equals(Session[SessionKey.ExternalID.ToString()].ToString2())))))
                    CustomerPersonalInfo = objretrieveAcctListByICResponse.itemList.FirstOrDefault(i => (i.Customer != null && i.SecondarySimList != null && i.SecondarySimList.Any(s => s.Msisdn.Equals(Session[SessionKey.ExternalID.ToString()].ToString2())))).Customer;
            }

			if (!ReferenceEquals(CustomerPersonalInfo, null))
			{
				if (!string.IsNullOrEmpty(CustomerPersonalInfo.Cbr))
				{
					CustomerPersonalInfo.Cbr = Util.FormatContactNumber(CustomerPersonalInfo.Cbr);
				}
			}


            return (CustomerPersonalInfo == null ? new CustomizedCustomer() : CustomerPersonalInfo);
        }


        #region Added by Patanjali to add find for Store Keeper and Cashier
        public static List<SelectListItem> GetStoreKeeperList(STATUSTYPE type, string defaultValue = "0", string defaultText = "default")
        {
            var resultList = new List<SelectListItem>();
            var Session = HttpContext.Current.Session;
            int statusTypeID = GetStatusTypeID(type);

            if (defaultText == "default")
                defaultText = "-- Please Select --";

            switch (type)
            {
                case STATUSTYPE.Registration:
                    string Status = string.Empty;
                    CustomRoleProvider objCustomRoleProvider = new CustomRoleProvider();
                    if (objCustomRoleProvider.IsUserInRole(Util.SessionAccess.UserName, "MREG_TL"))
                    {
                        Status = "TL";
                    }
                    if (Session["IsDealer"].ToBool())
                    {
                        Status = "Dealer";
                    }
                    var statusList = ConfigServiceProxy.FindStoreKeeperStatus(new StatusFind()
                    {
                        Status = new Status()
                        {
                            StatusTypeID = statusTypeID
                        },
                        Role = Status
                    });

                    if (statusList != null)
                    {
                        resultList = statusList.Select(a =>
                            new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                    }
                    break;

                case STATUSTYPE.Home:
                    var homeStatusList = new ConfigServiceProxy().FindStatus(new StatusFind()
                    {
                        Status = new Status()
                        {
                            StatusTypeID = statusTypeID
                        },
                        Active = true
                    });

                    if (homeStatusList != null)
                    {
                        resultList = homeStatusList.Select(a =>
                            new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                    }
                    break;

                default:
                    break;
            }

            resultList.Insert(0, new SelectListItem()
            {
                Text = defaultText,
                Value = defaultValue,
                Selected = true
            });

            return resultList;
        }
        public static List<SelectListItem> GetStoreKeeperList_Smart(STATUSTYPE type, string defaultValue = "0", string defaultText = "default")
        {
            var resultList = new List<SelectListItem>();
            int statusTypeID = GetStatusTypeID(type);

            if (defaultText == "default")
                defaultText = "-- Please Select --";

            switch (type)
            {
                case STATUSTYPE.Registration:
                    var statusList = Helper.SmartConfigService.FindStoreKeeperStatus(new DAL.Models.StatusFind()
                    {
                        Status = new DAL.Models.Status()
                        {
                            StatusTypeID = statusTypeID
                        }
                    });

                    if (statusList != null)
                    {
                        resultList = statusList.Select(a =>
                            new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                    }
                    break;

                case STATUSTYPE.Home:
                    var homeStatusList = new ConfigServiceProxy().FindStatus(new StatusFind()
                    {
                        Status = new Status()
                        {
                            StatusTypeID = statusTypeID
                        },
                        Active = true
                    });

                    if (homeStatusList != null)
                    {
                        resultList = homeStatusList.Select(a =>
                            new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                    }
                    break;

                default:
                    break;
            }

            resultList.Insert(0, new SelectListItem()
            {
                Text = defaultText,
                Value = defaultValue,
                Selected = true
            });

            return resultList;
        }
        #region "Added by Ranjeeth"

        public static List<SelectListItem> GetCriteriaList(bool isDealer, string defaultValue = "0", string defaultText = "default")
        {
            var resultList = new List<SelectListItem>();
            if (defaultText == "default")
                defaultText = "-- Please Select --";
            resultList.Add(new SelectListItem() { Text = "Retail", Value = "R" });
            if (!isDealer)
            {
                resultList.Add(new SelectListItem() { Text = "Smart", Value = "S" });
                resultList.Add(new SelectListItem() { Text = "Both", Value = "RS", Selected = true });
            }
            return resultList;
        }

        public static string GetCriteriaTypeName(string CriteriaType)
        {
            string _cname = string.Empty;
            if (CriteriaType == "R")
            {
                _cname = "Retail";
            }
            else if (CriteriaType == "S")
            {
                _cname = "Smart";
            }
            else
            {
                _cname = "undefined";
            }
            return _cname;
        }
        #endregion


        public static List<SelectListItem> GetCashierList(STATUSTYPE type, string defaultValue = "0", string defaultText = "default")
        {
            var resultList = new List<SelectListItem>();
            int statusTypeID = GetStatusTypeID(type);
            var Session = HttpContext.Current.Session;

            if (defaultText == "default")
                defaultText = "-- Please Select --";

            switch (type)
            {
                case STATUSTYPE.Registration:
                    string role = string.Empty;

                    if (Session["IsDealer"].ToBool())
                    {
                        role = "Dealer";
                    }

                    var statusList = ConfigServiceProxy.FindCashierStatus(new StatusFind()
                    {
                        Status = new Status()
                        {
                            StatusTypeID = statusTypeID
                        },
                        Role = role
                    });

                    if (statusList != null)
                    {
                        resultList = statusList.Select(a =>
                            new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                    }
                    break;

                case STATUSTYPE.Home:
                    var homeStatusList = new ConfigServiceProxy().FindStatus(new StatusFind()
                    {
                        Status = new Status()
                        {
                            StatusTypeID = statusTypeID
                        },
                        Active = true
                    });

                    if (homeStatusList != null)
                    {
                        resultList = homeStatusList.Select(a =>
                            new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                    }
                    break;

                default:
                    break;
            }

            resultList.Insert(0, new SelectListItem()
            {
                Text = defaultText,
                Value = defaultValue,
                Selected = true
            });

            return resultList;
        }
        public static List<SelectListItem> GetCashierList_Smart(STATUSTYPE type, string defaultValue = "0", string defaultText = "default")
        {
            var resultList = new List<SelectListItem>();
            int statusTypeID = GetStatusTypeID(type);

            if (defaultText == "default")
                defaultText = "-- Please Select --";

            switch (type)
            {
                case STATUSTYPE.Registration:
                    var statusList = Helper.SmartConfigService.FindCashierStatus(new DAL.Models.StatusFind()
                    {
                        Status = new DAL.Models.Status()
                        {
                            StatusTypeID = statusTypeID
                        }
                    });

                    if (statusList != null)
                    {
                        resultList = statusList.Select(a =>
                            new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                    }
                    break;

                case STATUSTYPE.Home:
                    var homeStatusList = new ConfigServiceProxy().FindStatus(new StatusFind()
                    {
                        Status = new Status()
                        {
                            StatusTypeID = statusTypeID
                        },
                        Active = true
                    });

                    if (homeStatusList != null)
                    {
                        resultList = homeStatusList.Select(a =>
                            new SelectListItem()
                            {
                                Text = a.Description,
                                Value = a.ID.ToString()
                            }).ToList();
                    }
                    break;

                default:
                    break;
            }

            resultList.Insert(0, new SelectListItem()
            {
                Text = defaultText,
                Value = defaultValue,
                Selected = true
            });

            return resultList;
        }
        #endregion

        public static List<SelectListItem> GetLogStatusList(int statusID, string defaultText = "default")
        {
            var resultList = new List<SelectListItem>();
            //int statusTypeID = GetStatusTypeID(type);

            if (defaultText == "default")
                defaultText = "-- Please Select --";

            var statusGet = new Status();
            using (var proxy = new ConfigServiceProxy())
            {
                statusGet = proxy.StatusGet(statusID);
            }

            if (statusGet.Code == Properties.Settings.Default.Status_HomeReject || statusGet.Code == Properties.Settings.Default.Status_HomeCancelled || statusGet.Code == Properties.Settings.Default.Status_HomeComplete)
                return resultList;


            var toStatusIDs = ConfigServiceProxy.FindStatusChange(new StatusChangeFind()
            {
                StatusChange = new StatusChange()
                {
                    FromStatusID = statusID
                }
            }).Select(a => a.ToStatusID).ToList();

            var statusList = new List<Status>();
            using (var proxy = new ConfigServiceProxy())
            {
                statusList = proxy.StatusGet(toStatusIDs).ToList();
            }

            if (statusList != null)
            {
                resultList = statusList.Select(a =>
                    new SelectListItem()
                    {
                        Text = a.Description,
                        Value = a.ID.ToString()
                    }).ToList();
            }

            resultList.Insert(0, new SelectListItem()
            {
                Text = defaultText,
                Value = "",
                Selected = true
            });

            return resultList;
        }

        public static List<SelectListItem> GetDayList(string selectedValue = "")
        {
            var dayList = new List<SelectListItem>();

            for (int i = 1; i <= 31; i++)
            {
                dayList.Add(new SelectListItem()
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            List<SelectListItem> list = new List<SelectListItem>();




            for (int i = 0; i < dayList.Count; i++)
            {
                if (dayList[i].Text != selectedValue)
                {
                    list.Add(new SelectListItem() { Text = dayList[i].Text, Value = dayList[i].Value, Selected = false });
                }
                else
                {
                    list.Add(new SelectListItem() { Text = dayList[i].Text, Value = dayList[i].Value, Selected = true });
                }
            }

            list.Insert(0, new SelectListItem()
            {

                Text = "Day",
                Value = ""
            });

            return list;
        }

        public static List<SelectListItem> GetMonthList(string defaultText = "default", string selectedValue = "")
        {
            var monthList = new List<SelectListItem>();

            if (defaultText == "default")
                defaultText = "-- Please Select --";

            monthList.Add(new SelectListItem()
            {
                Text = "Jan",
                Value = "1"
            });
            monthList.Add(new SelectListItem()
            {
                Text = "Feb",
                Value = "2"
            });
            monthList.Add(new SelectListItem()
            {
                Text = "Mar",
                Value = "3"
            });
            monthList.Add(new SelectListItem()
            {
                Text = "Apr",
                Value = "4"
            });
            monthList.Add(new SelectListItem()
            {
                Text = "May",
                Value = "5"
            });
            monthList.Add(new SelectListItem()
            {
                Text = "June",
                Value = "6"
            });
            monthList.Add(new SelectListItem()
            {
                Text = "July",
                Value = "7"
            });
            monthList.Add(new SelectListItem()
            {
                Text = "Aug",
                Value = "8"
            });
            monthList.Add(new SelectListItem()
            {
                Text = "Sept",
                Value = "9"
            });
            monthList.Add(new SelectListItem()
            {
                Text = "Oct",
                Value = "10"
            });
            monthList.Add(new SelectListItem()
            {
                Text = "Nov",
                Value = "11"
            });
            monthList.Add(new SelectListItem()
            {
                Text = "Dec",
                Value = "12"
            });

            List<SelectListItem> list = new List<SelectListItem>();

            for (int i = 0; i < monthList.Count; i++)
            {
                if (monthList[i].Value != selectedValue)
                {
                    list.Add(new SelectListItem() { Text = monthList[i].Text, Value = monthList[i].Value, Selected = false });
                }
                else
                {
                    list.Add(new SelectListItem() { Text = monthList[i].Text, Value = monthList[i].Value, Selected = true });
                }
            }

            list.Insert(0, new SelectListItem()
            {
                Text = "Month",
                Value = ""
            });

            return list;
        }

        public static List<SelectListItem> GetYearList(int maxYear = 0, string selectedValue = "")
        {
            var yearList = new List<SelectListItem>();

            for (int i = DateTime.Now.Year - maxYear; i >= DateTime.Now.Year - 100; i--)
            {
                yearList.Add(new SelectListItem()
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            List<SelectListItem> list = new List<SelectListItem>();




            for (int i = 0; i < yearList.Count; i++)
            {
                if (yearList[i].Text != selectedValue)
                {
                    list.Add(new SelectListItem() { Text = yearList[i].Text, Value = yearList[i].Value, Selected = false });
                }
                else
                {
                    list.Add(new SelectListItem() { Text = yearList[i].Text, Value = yearList[i].Value, Selected = true });
                }
            }




            list.Insert(0, new SelectListItem()
            {

                Text = "Year",
                Value = ""
            });

            return list;
        }

        public static List<SelectListItem> GetComplaintIssuesList(string defaultValue = "", string defaultText = "default")
        {
            var resultList = new List<SelectListItem>();

            if (defaultText == "default")
                defaultText = "-- Please Select --";

            resultList.Insert(0, new SelectListItem()
            {
                Text = defaultText,
                Value = defaultValue,
                Selected = true
            });
            resultList.Insert(1, new SelectListItem()
            {
                Text = "Product",
                Value = Properties.Settings.Default.ComplaintIssues_Product
            });
            resultList.Insert(2, new SelectListItem()
            {
                Text = "Reason 1",
                Value = Properties.Settings.Default.ComplaintIssues_Reason1
            });
            resultList.Insert(3, new SelectListItem()
            {
                Text = "Reason 2",
                Value = Properties.Settings.Default.ComplaintIssues_Reason2
            });
            resultList.Insert(4, new SelectListItem()
            {
                Text = "Reason 3",
                Value = Properties.Settings.Default.ComplaintIssues_Reason3
            });
            resultList.Insert(5, new SelectListItem()
            {
                Text = "Dispatch Queue",
                Value = Properties.Settings.Default.ComplaintIssues_DispatchQ
            });

            return resultList;
        }

        public static List<SelectListItem> GetPkgCompList(string type = "", string defaultValue = "0", string defaultText = "default")
        {
            List<SelectListItem> resultList = new List<SelectListItem>();

            if (defaultText == "default")
                defaultText = "-- Please Select --";

            switch (type)
            {
                case "PC":
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var list = proxy.PkgCompContractGet();
                        if (list != null)
                        {
                            resultList = list.Select(a => new SelectListItem()
                            {
                                Text = a.Value,
                                Value = a.Key
                            }).ToList();
                        }
                    }
                    break;

                case "BP":
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var list = proxy.BdlPkgGet();
                        if (list != null)
                        {
                            resultList = list.Select(a => new SelectListItem()
                            {
                                Text = a.Value,
                                Value = a.Key
                            }).ToList();
                        }
                    }
                    break;
            }

            resultList.Insert(0, new SelectListItem()
            {
                Text = defaultText,
                Value = defaultValue,
                Selected = true
            });

            return resultList;
        }

        #endregion

        #region Session

        public static string SessionErrMsg
        {
            get
            {
                return HttpContext.Current.Session["ErrMsg"] as string;
            }
            set
            {
                HttpContext.Current.Session["ErrMsg"] = value;
            }
        }

        public static void SetSessionErrMsg(Exception ex)
        {
            Util.SessionErrMsg = ex.Message + Environment.NewLine + ex.StackTrace;
        }

        public static Access SessionAccess
        {
            get
            {
                if (HttpContext.Current.Session["Access"] == null)
                    return new Access();
                return HttpContext.Current.Session["Access"] as Access;
            }
            set
            {
                HttpContext.Current.Session["Access"] = value;
            }
        }

        public static string SessionOrgTypeCode
        {
            get
            {
                return HttpContext.Current.Session["OrgTypeCode"] as string;
            }
            set
            {
                HttpContext.Current.Session["OrgTypeCode"] = value;
            }
        }



        public static void CheckSessionAccess(RequestContext reqCtx)
        {
            if (Util.SessionAccess != null) return;

            var request = reqCtx.HttpContext.Request;
            if (request.Cookies["CookieUser"] != null)
            {
                Access access = null;
                string userName = request.Cookies["CookieUser"].Value;

                using (var proxy = new UserServiceProxy())
                {
                    access = proxy.GetAccessByUsername(userName);
                    if (access != null)
                    {
                        access.User = proxy.UserGet(new int[] { access.UserID }).Single();
                        Util.SessionAccess = access;
                    }
                }

                using (var orgProxy = new OrganizationServiceProxy())
                {
                    var org = orgProxy.OrganizationGet(new int[] { access.User.OrgID }).Single();
                    access.User.Org = new DAL.Models.Organization()
                    {
                        Active = org.Active,
                        AddrLine1 = org.AddrLine1,
                        AddrLine2 = org.AddrLine2,
                        Code = org.Code,
                        DealerCode = org.DealerCode,
                        ContactNo = org.ContactNo,
                        CountryID = org.CountryID,
                        CreateDT = org.CreateDT,
                        Description = org.Description,
                        EmailAddr = org.EmailAddr,
                        //EntityKey = org.EntityKey,
                        FaxNo = org.FaxNo,
                        ID = org.ID,
                        IsHQ = org.IsHQ,
                        KenanRegCode = org.KenanRegCode,
                        KenanServiceProviderID = org.KenanServiceProviderID,
                        LastAccessID = org.LastAccessID,
                        LastUpdateDT = org.LastUpdateDT,
                        MobileNo = org.MobileNo,
                        Name = org.Name,
                        OrgTypeID = org.OrgTypeID,
                        PersonInCharge = org.PersonInCharge,
                        Postcode = org.Postcode,
                        //RegionID = org.RegionID,
                        SAPBranchCode = org.SAPBranchCode,
                        StateID = org.StateID,
                        TownCity = org.TownCity,
                        WSDLUrl = org.WSDLUrl,

                        #region Added by NarayanaReddy
                        OrganisationId = org.OrganisationId,
                        Inventry_OutOfStockLower = org.Inventry_OutOfStockLower,
                        Inventry_OutOfStockHigher = org.Inventry_OutOfStockHigher,
                        Inventry_LimitedStockLower = org.Inventry_LimitedStockLower,
                        Inventry_LimitedStockHigher = org.Inventry_LimitedStockHigher,
                        Inventry_AvailableStockLower = org.Inventry_AvailableStockLower,
                        Inventry_AvailableStockHigher = org.Inventry_AvailableStockHigher,
                        #endregion
						// added by Gerry
						SMARTTeamID = org.SMARTTeamID

                    };

                    Util.SessionOrgTypeCode = orgProxy.OrgTypeGet(new int[] { access.User.Org.OrgTypeID }).Single().Code;
                }
            }
            return;
        }

        #endregion

        #region Settings

        public static string GetDateFormatString()
        {
            return Settings.Default.DateFormatString;
        }

        #endregion

        public static string GetAbsoluteAppPath(string path)
        {
            var request = HttpContext.Current.Request;
            var appPath = request.ApplicationPath == "/" ? "" : request.ApplicationPath;

            path = request.Url.ToString().Replace(request.Url.AbsolutePath, "") + appPath + path;

            return path;
        }

        #region supervisor waive off

		public static List<SelectListItem> GetRegistrationTypesForWaiveOff()
		{
			var resultList = new List<SelectListItem>();

			using (var proxy = new CatalogServiceProxy())
			{
				var lovList = proxy.GetLovList("REG_TYPE");
				resultList.Add(new SelectListItem()
				{
					Text = "Please Select",
					Value = ""
				});
				foreach (var lov in lovList)
				{
					resultList.Add(new SelectListItem()
					{
						Text = lov.Source,
						Value = lov.Value
					});
				}
			}
			
			return resultList;
		}

		#region change the list to refLOV
		/*
		public static List<SelectListItem> GetRegistrationTypesForWaiveOff()
        {
            var resultList = new List<SelectListItem>();

            using (var proxy = new RegistrationServiceProxy())
            {
                List<int> regids = new List<int>();
                regids.Add(MobileRegType.NewLine.ToInt());
                regids.Add(MobileRegType.SuppPlan.ToInt());
                regids.Add(MobileRegType.SecPlan.ToInt());
                regids.Add(MobileRegType.AddRemoveVAS.ToInt());
                regids.Add(MobileRegType.SimReplacement.ToInt());
                regids.Add(MobileRegType.CRP.ToInt());
                regids.Add(MobileRegType.DeviceCRP.ToInt());
                regids.Add(MobileRegType.MNPPlanOnly.ToInt());
                regids.Add(MobileRegType.MNPSuppPlan.ToInt());
                regids.Add(MobileRegType.MNPNewLine.ToInt());
                regids.Add(MobileRegType.RegType_MNPPlanWithMultiSuppline.ToInt());
                regids.Add(3);
                regids.Add(2);
                var results = proxy.RegTypeGet(regids);

                foreach (var regtype in results)
                {
                    resultList.Add(new SelectListItem()
                    {
                        Text = regtype.Description,
                        Value = regtype.ID.ToString()
                    });
                }
                resultList.Add(new SelectListItem()
                {
                    Text = "Device Supplementary Plan",
                    Value = "28"
                });
                resultList.Insert(0, new SelectListItem()
                {

                    Text = "Please Select",
                    Value = ""
                });
            }

            return resultList;
        }
		*/
		#endregion

        public static List<SelectListItem> GetCreatedByUsersForWiveOff()
        {
            var resultList = new List<SelectListItem>();
            int OrgId;

            using (var proxy = new RegistrationServiceProxy())
            {

                if (Roles.IsUserInRole("MREG_HQ"))
                    // sending 0 for Head Quarter supervisor
                    OrgId = 0;
                else if (Roles.IsUserInRole("MREG_DAPP"))
                    // sending dealer org id
                    OrgId = 8;
                else
                    // sending orginal orgid for current store/branch/organization
                    OrgId = Util.SessionAccess.User.OrgID;

                var results = proxy.GetCreatedUsersForWaiveOff(OrgId);


                foreach (var user in results)
                {
                    resultList.Add(new SelectListItem()
                    {
                        Text = user,
                        Value = user
                    });
                }
            }

            resultList.Insert(0, new SelectListItem()
            {

                Text = "Please Select",
                Value = ""
            });

            return resultList;
        }

        public static List<SelectListItem> GetAcknowledgedUsersForWaiveOff()
        {
            var resultList = new List<SelectListItem>();
            int OrgId;
            using (var proxy = new RegistrationServiceProxy())
            {
                // CDPU
                bool isCDPU = Roles.IsUserInRole("MREG_DAPP") ? true : false;
                
                if (Roles.IsUserInRole("MREG_HQ"))
                    // sending 0 for Head Quarter supervisor
                    OrgId = 0;
                else
                    // sending orginal orgid for current store/branch/organization
                    OrgId = Util.SessionAccess.User.OrgID;

                var results = proxy.GetAcknowledgedUsersForWaiveOff(OrgId, isCDPU);

                foreach (var user in results)
                {
                    resultList.Add(new SelectListItem()
                    {
                        Text = user,
                        Value = user
                    });
                }
            }

            resultList.Insert(0, new SelectListItem()
            {

                Text = "Please Select",
                Value = ""
            });

            return resultList;
        }

        public static List<SelectListItem> GetTranTypesForWaiveOff()
        {
            var resultList = new List<SelectListItem>();

            resultList.Add(new SelectListItem()
            {
                Text = "ALL",
                Value = "0"
            });

            resultList.Add(new SelectListItem()
            {
                Text = "Waiver",
                Value = "1"
            });

            resultList.Add(new SelectListItem()
            {
                Text = "BRE Fail",
                Value = "2"
            });

            resultList.Add(new SelectListItem()
            {
                Text = "Voided",
                Value = "3"
            });

            resultList.Add(new SelectListItem()
            {
                Text = "Returned",
                Value = "4"
            });

            return resultList;
        }

        public static List<SelectListItem> GetCDPUApprovalStatus()
        {
            var resultList = new List<SelectListItem>();

            resultList.Add(new SelectListItem()
            {
                Text = "Please Select",
                Value = "0"
            });
            
            resultList.Add(new SelectListItem()
            {
                Text = Constants.StatusCDPUPending,
                Value = Constants.StatusCDPUPending
            });

            resultList.Add(new SelectListItem()
            {
                Text = Constants.StatusCDPUApprove,
                Value = Constants.StatusCDPUApprove
            });

            resultList.Add(new SelectListItem()
            {
                Text = Constants.StatusCDPURejected,
                Value = Constants.StatusCDPURejected
            });

            return resultList;
        }

        public static List<SelectListItem> GetAllRegistrationTypes()
        {
            var resultList = new List<SelectListItem>();

            var regtypes = Enum.GetValues(typeof(MobileRegType));

            using (var proxy = new RegistrationServiceProxy())
            {
                List<int> regids = new List<int>();
                regids.Add(MobileRegType.NewLine.ToInt());
                regids.Add(MobileRegType.SuppPlan.ToInt());
                regids.Add(MobileRegType.SecPlan.ToInt());
                regids.Add(MobileRegType.AddRemoveVAS.ToInt());
                regids.Add(MobileRegType.SimReplacement.ToInt());
                regids.Add(MobileRegType.CRP.ToInt());
                regids.Add(MobileRegType.DeviceCRP.ToInt());
                regids.Add(MobileRegType.MNPPlanOnly.ToInt());
                regids.Add(MobileRegType.MNPSuppPlan.ToInt());
                regids.Add(MobileRegType.MNPNewLine.ToInt());
                regids.Add(MobileRegType.RegType_MNPPlanWithMultiSuppline.ToInt());
                regids.Add(3);
                regids.Add(2);
                var results = proxy.RegTypeGet(regids);

                foreach (var regtype in results)
                {
                    resultList.Add(new SelectListItem()
                    {
                        Text = regtype.Description,
                        Value = regtype.ID.ToString()
                    });
                }
            }
            resultList.Add(new SelectListItem()
            {
                Text = "Device Supplementary Plan",
                Value = "28"
            });
            resultList.Insert(0, new SelectListItem()
            {

                Text = "Please Select",
                Value = ""
            });
            return resultList;
        }

        public static List<SelectListItem> GetAllOrganizationsForWaiveOff()
        {
            var resultList = new List<SelectListItem>();
            int OrgId;
            using (var proxy = new OrganizationServiceProxy())
            {
                if (Roles.IsUserInRole("MREG_HQ"))
                    // sending 0 for Head Quarter supervisor
                    OrgId = 0;
                else
                    // sending orginal orgid for current store/branch/organization
                    OrgId = Util.SessionAccess.User.OrgID;

                var results = proxy.OrganizationGetAll();

                foreach (var org in results)
                {
                    resultList.Add(new SelectListItem()
                    {
                        Text = org.Name,
                        Value = org.ID.ToString()
                    });
                }
            }

            resultList.Insert(0, new SelectListItem()
            {

                Text = "Please Select",
                Value = ""
            });

            return resultList;
        }

        #endregion

        public static string GetNameByString(RefType type, string name)
        {
            string result = "";

            if (string.IsNullOrEmpty(name))
                return result;

            switch (type)
            {
                case RefType.ModelArticle:
                    #region "BrandArticle"
                    var allArticle = MasterDataCache.Instance.Article;
                    int modelID = allArticle.Where(x => x.ArticleID == name).Select(y => y.ModelID).SingleOrDefault();
                    result = Util.GetNameByID(RefType.Model, modelID);
                    #endregion
                    break;

                //20151606 - Samuel add this for get color device
                case RefType.Colour:
                    #region "BrandColour"
                    var allColourArticle = MasterDataCache.Instance.Article;
                    int colourID = allColourArticle.Where(x => x.ArticleID == name).Select(y => y.ColourID).SingleOrDefault();
                    result = Util.GetNameByID(RefType.Colour, colourID);
                    #endregion
                    break;

                case RefType.ModelID:
                    #region "Model ID"
                    var allArticles = MasterDataCache.Instance.Article;
                    if (string.IsNullOrEmpty(name))
                    {
                        result = string.Empty;
                    }
                    else
                    {
                        result = allArticles.Where(x => x.ArticleID == name).Select(y => y.ModelID.ToString2()).SingleOrDefault();
                    }
                    #endregion
                    break;

                case RefType.PegaStatus:
                    #region "PegaStatus"
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var lovList = proxy.GetLovList("NBA_STATUS");
                        if (string.IsNullOrEmpty(name))
                        {
                            result = string.Empty;
                        }
                        else
                        {
                            result = lovList.Where(x => x.Source == name).Select(y => y.Value.ToString2()).SingleOrDefault();
                        }
                    }
                    #endregion
                    break;

            }

            return result;
        }

        public static string GetNameByID(RefType type, int id)
        {
            string name = "";
            if (id == 0)
                return name;

            switch (type)
            {
                case RefType.AccountCategory:
                    #region "AccountCategory"
                    var acctCategoryList = MasterDataCache.Instance.AccountCategory;
                    if (acctCategoryList != null && acctCategoryList.AccountCategories.Any())
                        name = acctCategoryList.AccountCategories.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.AddressType:
                    #region "AddressType"
                    var addressType = MasterDataCache.Instance.Address;
                    if (addressType != null && addressType.AddressTypes.Any())
                        name = addressType.AddressTypes.Where(a => a.ID == id).Select(a => a.Description).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Brand:
                    #region "Brand"
                    var vbrand = MasterDataCache.Instance.Brand;
                    if (vbrand != null && vbrand.Brands.Any())
                        name = vbrand.Brands.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Bundle:
                    #region "Bundle"
                    var vbundle = MasterDataCache.Instance.Bundle;
                    if (vbundle != null && vbundle.Bundles.Any())
                        name = vbundle == null ? "" : vbundle.Bundles.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Category:
                    #region "Category"
                    var vCategory = MasterDataCache.Instance.Category;
                    if (vCategory != null && vCategory.Categorys.Any())
                        name = vCategory.Categorys.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.CardType:
                    #region "CardType"
                    var CardType = MasterDataCache.Instance.CardType;
                    if (CardType != null && CardType.CardType.Any())
                        name = CardType == null ? "" : CardType.CardType.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Colour:
                    #region "Colour"
                    var vColor = MasterDataCache.Instance.Colour;
                    if (vColor != null && vColor.Colours.Any())
                        name = vColor.Colours.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.ComplaintIssues:
                    #region "ComplaintIssues"
                    var vComplaint = MasterDataCache.Instance.ComplaintIssues;
                    if (vComplaint != null && vComplaint.ComplaintIssuess.Any())
                        name = vComplaint.ComplaintIssuess.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Component:
                    #region "Component"
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var Component = proxy.ComponentGet(new int[] { id }).SingleOrDefault();
                        // due to no name field, use Code as temporary replacement
                        name = Component == null ? "" : Component.Name;
                    }
                    break;
                    #endregion

                case RefType.RestrictKeananComponent:
                    #region "RestrictKeananComponent"
                    using (var proxy = new CatalogServiceProxy())
                    {
                        List<Component> components = new List<Component>();
                        components = proxy.ComponentGet(new int[] { id }).ToList();
                        var component = components.Where(a => a.RestricttoKenan == false).SingleOrDefault();
                        // due to no name field, use Code as temporary replacement
                        name = component == null ? string.Empty : component.Name;
                    }
                    #endregion
                    break;

                case RefType.RestrictKeananComponentGetAlias:
                    #region "RestrictKeananComponentGetAlias"
                    using (var proxy = new CatalogServiceProxy())
                    {
                        List<Component> components = new List<Component>();
                        components = proxy.ComponentGet(new int[] { id }).ToList();
                        var component = components.Where(a => a.RestricttoKenan == false).SingleOrDefault();
                        name = component == null ? string.Empty : component.AliasName;
                    }
                    #endregion
                    break;

                case RefType.Country:
                    #region  "Country"
                    var vCountry = MasterDataCache.Instance.Country;
                    if (vCountry != null && vCountry.Country.Any())
                        name = vCountry.Country.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.CustomerTitle:
                    #region "CustomerTitle"
                    var vCustomerTitle = MasterDataCache.Instance.CustomerTitle;
                    if (vCustomerTitle != null && vCustomerTitle.CustomerTitles.Any())
                        name = vCustomerTitle.CustomerTitles.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.IDCardType:
                    #region "IDCardType"
                    var vIdCardType = MasterDataCache.Instance.IDCardType;
                    if (vIdCardType != null && vIdCardType.IDCardType.Any())
                        name = vIdCardType.IDCardType.Where(a => a.ID == id).Select(a => a.Description).SingleOrDefault();

                    #endregion
                    break;

                case RefType.Language:
                    #region "Language"
                    var vLang = MasterDataCache.Instance.Language;
                    if (vLang != null && vLang.Languages.Any())
                        name = vLang.Languages.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Market:
                    #region "Market"
                    var vMarket = MasterDataCache.Instance.Market;
                    if (vMarket != null && vMarket.Markets.Any())
                        name = vMarket.Markets.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Model:
                    #region "Model"
                    var vmodel = MasterDataCache.Instance.Model;
                    if (vmodel != null && vmodel.Models.Any())
                        name = vmodel.Models.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Nationality:
                    #region "Nationality"
                    var vNation = MasterDataCache.Instance.Nationality;
                    if (vNation != null && vNation.Nationalities.Any())
                        name = vNation.Nationalities.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Organization:
                    #region "Organization"
                    var vOrg = MasterDataCache.Instance.Organization;
                    if (vOrg != null && vOrg.Organizations.Any())
                        name = vOrg.Organizations.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Package:
                    #region "Package"
                    var vPack = MasterDataCache.Instance.Package;
                    if (vPack != null && vPack.Packages.Any())
                        name = vPack.Packages.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.PaymentMode:
                    #region "PaymentMode"
                    var vPayMode = MasterDataCache.Instance.PaymentMode;
                    if (vPayMode != null && vPayMode.PaymentMode.Any())
                        name = vPayMode.PaymentMode.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.PgmBdlPkgComp:
                    #region "PgmBdlPkgComp"

                    var PgmBdlPkgComp = MasterDataCache.Instance.FilterComponents(new int[] { id }).SingleOrDefault();
                    // due to no name field, use Code as temporary replacement
                    name = PgmBdlPkgComp == null ? "" : PgmBdlPkgComp.Name;

                    #endregion
                    break;

                case RefType.Program:
                    #region "Program"
                    var vPgm = MasterDataCache.Instance.Program;
                    if (vPgm != null && vPgm.Programs.Any())
                        name = vPgm.Programs.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Race:
                    #region "Race"

                    var vRace = MasterDataCache.Instance.Race;
                    if (vRace != null && vRace.Races.Any())
                        name = vRace.Races.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();

                    #endregion
                    break;

                case RefType.State:
                    #region "State"
                    var vState = MasterDataCache.Instance.State;
                    if (vState != null && vState.State.Any())
                        name = vState.State.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Status:
                    #region "Status"
                    var vStatus = MasterDataCache.Instance.Status;
                    if (vStatus != null && vStatus.Statuss.Any())
                        name = vStatus.Statuss.Where(a => a.ID == id).Select(a => a.Description).SingleOrDefault();
                    #endregion
                    break;

                case RefType.StatusType:
                    #region "StatusType"
                    var vStatusType = MasterDataCache.Instance.StatusType;
                    if (vStatusType != null && vStatusType.StatusTypes.Any())
                        name = vStatusType.StatusTypes.Where(a => a.ID == id).Select(a => a.Description).SingleOrDefault();
                    #endregion
                    break;

                case RefType.StatusReason:
                    #region "StatusReason"
                    var vStatusReason = MasterDataCache.Instance.StatusReason;
                    if (vStatusReason != null && vStatusReason.StatusReasons.Any())
                        name = vStatusReason.StatusReasons.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();
                    #endregion
                    break;

                case RefType.UserGroup:
                    #region "UserGroup"
                    var vUserGrp = MasterDataCache.Instance.UserGroup;
                    if (vUserGrp != null && vUserGrp.UserGroups.Any())
                        name = vUserGrp.UserGroups.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();

                    #endregion
                    break;

                case RefType.User:
                    #region "User"
                    var vUsers = MasterDataCache.Instance.User;
                    if (vUsers != null && vUsers.Users.Any())
                        name = vUsers.Users.Where(a => a.ID == id).Select(a => a.FullName).SingleOrDefault();
                    #endregion
                    break;

                case RefType.Access:
                    #region "Access"
                    var vAccess = MasterDataCache.Instance.Access;
                    if (vAccess != null && vAccess.Accesss.Any())
                        name = vAccess.Accesss.Where(a => a.ID == id).Select(a => a.Description).SingleOrDefault();
                    #endregion
                    break;

                case RefType.UserRole:
                    #region "UserRole"
                    var vUserRole = MasterDataCache.Instance.UserRole;
                    if (vUserRole != null && vUserRole.UserRoles.Any())
                        name = vUserRole.UserRoles.Where(a => a.ID == id).Select(a => a.Description).SingleOrDefault();

                    #endregion
                    break;

                case RefType.ModelGroup:
                    #region "ModelGroup"
                    var vModelGroup = MasterDataCache.Instance.ModelGroup;
                    if (vModelGroup != null && vModelGroup.ModelGroups.Any())
                        name = vModelGroup.ModelGroups.Where(a => a.ID == id).Select(a => a.Name).SingleOrDefault();

                    #endregion

                    break;
                case RefType.RegType:
                    #region "RegType"
                    var vRegType = MasterDataCache.Instance.RegType;
                    if (vRegType != null && vRegType.RegTypes.Any())
                        name = vRegType.RegTypes.Where(a => a.ID == id).Select(a => a.Description).SingleOrDefault();
                    #endregion
                    break;
            }
            return name;
        }

        public static List<string> GetNameByIDs(RefType type, List<int> ids)
        {
            List<string> names = new List<string>();

            if (ids.Count == 0)
                return names;
            switch (type)
            {
                case RefType.Model:
                    #region "Model"
                    var vModel = MasterDataCache.Instance.Model;
                    if (vModel != null && vModel.Models.Any())
                    {
                        names = vModel.Models.Where(a => ids.Contains(a.ID)).Select(a => a.Name).ToList();
                    }
                    #endregion
                    break;
                case RefType.Colour:
                    #region "Colour"
                    var vColor = MasterDataCache.Instance.Colour;
                    if (vColor != null && vColor.Colours.Any())
                    {
                        names = vColor.Colours.Where(a => ids.Contains(a.ID)).Select(a => a.Name).ToList();
                    }
                    #endregion
                    break;
            }
            return names;
        }

        public static Dictionary<RefType, string> GetNameByIDsList(Dictionary<RefType, int> values)
        {
            Dictionary<RefType, string> names = new Dictionary<RefType, string>();
            string name = "";
            if (values.Count == 0)
                return names;
            foreach (KeyValuePair<RefType, int> item in values)
            {

                switch (item.Key)
                {
                    case RefType.AccountCategory:

                        var AcctCat = MasterDataCache.Instance.AccountCategory;
                        if (AcctCat != null && AcctCat.AccountCategories.Any())
                            name = AcctCat.AccountCategories.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault();
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.AddressType:
                        var AddrType = MasterDataCache.Instance.Address;
                        name = AddrType != null && AddrType.AddressTypes.Any() ? AddrType.AddressTypes.Where(a => a.ID == item.Value).Select(a => a.Description).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.Brand:

                        var vBrand = MasterDataCache.Instance.Brand;
                        name = vBrand != null && vBrand.Brands.Any() ? vBrand.Brands.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.Bundle:
                        var vBundle = MasterDataCache.Instance.Bundle;
                        name = vBundle != null && vBundle.Bundles.Any() ? vBundle.Bundles.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.Category:
                        var vCategory = MasterDataCache.Instance.Category;
                        name = vCategory != null && vCategory.Categorys.Any() ? vCategory.Categorys.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.CardType:
                        var vCardType = MasterDataCache.Instance.CardType;
                        name = vCardType != null && vCardType.CardType.Any() ? vCardType.CardType.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;


                    case RefType.Colour:
                        var vColour = MasterDataCache.Instance.Colour;
                        name = vColour != null && vColour.Colours.Any() ? vColour.Colours.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.ComplaintIssues:
                        var vComplaintIssues = MasterDataCache.Instance.ComplaintIssues;
                        name = vComplaintIssues != null && vComplaintIssues.ComplaintIssuess.Any() ? vComplaintIssues.ComplaintIssuess.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.Component:
                        if (!string.IsNullOrEmpty(name))
                            using (var proxy = new CatalogServiceProxy())
                            {
                                var Component = proxy.ComponentGet(new int[] { item.Value }).SingleOrDefault();
                                // due to no name field, use Code as temporary replacement
                                name = Component == null ? "" : Component.Name;
                            }
                        names.Add(item.Key, name);
                        break;


                    case RefType.RestrictKeananComponent:
                        using (var proxy = new CatalogServiceProxy())
                        {
                            List<Component> components = new List<Component>();
                            components = proxy.ComponentGet(new int[] { item.Value }).ToList();

                            var component = components.Where(a => a.RestricttoKenan == false).SingleOrDefault();

                            // due to no name field, use Code as temporary replacement
                            name = component == null ? string.Empty : component.Name;
                        }
                        names.Add(item.Key, name);
                        break;


                    case RefType.Country:
                        var vCountry = MasterDataCache.Instance.Country;
                        name = vCountry != null && vCountry.Country.Any() ? vCountry.Country.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.CustomerTitle:
                        var vCustomerTitle = MasterDataCache.Instance.CustomerTitle;
                        name = vCustomerTitle != null && vCustomerTitle.CustomerTitles.Any() ? vCustomerTitle.CustomerTitles.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        // if (!string.IsNullOrEmpty(name))
                        names.Add(item.Key, name);
                        break;

                    case RefType.IDCardType:
                        var vIDCardType = MasterDataCache.Instance.IDCardType;
                        name = vIDCardType != null && vIDCardType.IDCardType.Any() ? vIDCardType.IDCardType.Where(a => a.ID == item.Value).Select(a => a.Description).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.Language:
                        var vLanguage = MasterDataCache.Instance.Language;
                        name = vLanguage != null && vLanguage.Languages.Any() ? vLanguage.Languages.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.Market:
                        var vMarket = MasterDataCache.Instance.Market;
                        name = vMarket != null && vMarket.Markets.Any() ? vMarket.Markets.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.Model:
                        var vModel = MasterDataCache.Instance.Model;
                        name = vModel != null && vModel.Models.Any() ? vModel.Models.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.Nationality:
                        var vNationality = MasterDataCache.Instance.Nationality;
                        name = vNationality != null && vNationality.Nationalities.Any() ? vNationality.Nationalities.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.Organization:
                        var vOrganization = MasterDataCache.Instance.Organization;
                        name = vOrganization != null && vOrganization.Organizations.Any() ? vOrganization.Organizations.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.Package:
                        var vPackage = MasterDataCache.Instance.Package;
                        name = vPackage != null && vPackage.Packages.Any() ? vPackage.Packages.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.PaymentMode:
                        var vPaymentMode = MasterDataCache.Instance.PaymentMode;
                        name = vPaymentMode != null && vPaymentMode.PaymentMode.Any() ? vPaymentMode.PaymentMode.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.PgmBdlPkgComp:
                        using (var proxy = new CatalogServiceProxy())
                        {
                            var PgmBdlPkgComp = MasterDataCache.Instance.FilterComponents(new int[] { item.Value }).SingleOrDefault();
                            // due to no name field, use Code as temporary replacement
                            name = PgmBdlPkgComp == null ? "" : PgmBdlPkgComp.Name;
                        }
                        names.Add(item.Key, name);
                        break;

                    case RefType.Program:
                        var vProgram = MasterDataCache.Instance.Program;
                        name = vProgram != null && vProgram.Programs.Any() ? vProgram.Programs.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.Race:
                        var vRace = MasterDataCache.Instance.Race;
                        name = vRace != null && vRace.Races.Any() ? vRace.Races.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.State:
                        var vState = MasterDataCache.Instance.State;
                        name = vState != null && vState.State.Any() ? vState.State.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);

                        break;

                    case RefType.Status:
                        var vStatus = MasterDataCache.Instance.Status;
                        name = vStatus != null && vStatus.Statuss.Any() ? vStatus.Statuss.Where(a => a.ID == item.Value).Select(a => a.Description).SingleOrDefault() : string.Empty;
                        // if (!string.IsNullOrEmpty(name))
                        names.Add(item.Key, name);

                        break;

                    case RefType.StatusType:
                        var vStatusType = MasterDataCache.Instance.StatusType;
                        name = vStatusType != null && vStatusType.StatusTypes.Any() ? vStatusType.StatusTypes.Where(a => a.ID == item.Value).Select(a => a.Description).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.StatusReason:
                        var vStatusReason = MasterDataCache.Instance.StatusReason;
                        name = vStatusReason != null && vStatusReason.StatusReasons.Any() ? vStatusReason.StatusReasons.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);

                        break;

                    case RefType.UserGroup:
                        var vUserGroup = MasterDataCache.Instance.UserGroup;
                        name = vUserGroup != null && vUserGroup.UserGroups.Any() ? vUserGroup.UserGroups.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);

                        break;

                    case RefType.User:
                        var vUser = MasterDataCache.Instance.User;
                        name = vUser != null && vUser.Users.Any() ? vUser.Users.Where(a => a.ID == item.Value).Select(a => a.FullName).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            using (var proxy = new UserServiceProxy())
                            {
                                var User = proxy.UserGet(new int[] { item.Value }).SingleOrDefault();
                                // due to no name field, use Code as temporary replacement
                                name = User == null ? "" : User.FullName;
                            }
                        names.Add(item.Key, name);

                        break;

                    case RefType.Access:
                        var vAccess = MasterDataCache.Instance.Access;
                        name = vAccess != null && vAccess.Accesss.Any() ? vAccess.Accesss.Where(a => a.ID == item.Value).Select(a => a.Description).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;

                    case RefType.UserRole:
                        var vUserRole = MasterDataCache.Instance.UserRole;
                        name = vUserRole != null && vUserRole.UserRoles.Any() ? vUserRole.UserRoles.Where(a => a.ID == item.Value).Select(a => a.Description).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);

                        break;

                    case RefType.ModelGroup:
                        var vModelGroup = MasterDataCache.Instance.ModelGroup;
                        name = vModelGroup != null && vModelGroup.ModelGroups.Any() ? vModelGroup.ModelGroups.Where(a => a.ID == item.Value).Select(a => a.Name).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);

                        break;
                    case RefType.RegType:
                        var vRegType = MasterDataCache.Instance.RegType;
                        name = vRegType != null && vRegType.RegTypes.Any() ? vRegType.RegTypes.Where(a => a.ID == item.Value).Select(a => a.Description).SingleOrDefault() : string.Empty;
                        if (!string.IsNullOrEmpty(name))
                            names.Add(item.Key, name);
                        break;
                }
            }

            return names;
        }

		public static string processBase64(string _input)
		{
			var firstBase64index = 0;
			var lastBase64index = 0;

			firstBase64index = _input.IndexOf("data:");
			lastBase64index = _input.IndexOf(',');

			if (firstBase64index > -1)
				return _input.Remove(firstBase64index, lastBase64index + 1);
			else
				return _input;
		}

		public static bool isCDPUUser()
		{ 
			try {
				return (Roles.IsUserInRole("MREG_CUSR"));
			}
			catch{
				return false;
			}
		}


		/// <summary>
		/// to change thefilename
		/// </summary>
		/// <param name="inputFileName">Gerry.jpg</param>
		/// <param name="outputFileName">Front</param>
		/// <returns></returns>
		public static string changefileName(string inputFileName, string outputFileName)
		{
			if (string.IsNullOrEmpty(inputFileName))
				return string.Empty;

			string[] fileNameArray = inputFileName.Trim().Split('.');
			string fileName = outputFileName;
			string extension = fileNameArray[1].ToString2();

			return string.Format("{0}.{1}", fileName, extension);
		}

        /// <summary>
        /// PN-3965 
        /// Will format contact number from incorrect format that retrieved from kenan
        /// </summary>
        /// <param name="contactNumber"></param>
        /// <returns>International number format</returns>
        public static string FormatContactNumber(string contactNumber)
        {
            Logger.Info("Input string = " + contactNumber);
            // posibble value
            // 60|6xxx
            // 6|0xxx
            // 60|xxx
            // 60xxxx
            if (!string.IsNullOrEmpty(contactNumber))
            {
				if (contactNumber.Contains("-"))
					contactNumber.Replace("-", "");

                if (contactNumber.Length >= 2)
                {
                    bool firstIs60 = string.Equals(contactNumber.Substring(0, 2), "60");
                    bool firstIs6 = string.Equals(contactNumber.Substring(0, 1), "6");
                    bool secondIs0 = string.Equals(contactNumber.Substring(1, 1), "0");
                    bool firstIs0 = string.Equals(contactNumber.Substring(0, 1), "0");

                    if (firstIs0)
                    {
                        contactNumber = "6" + contactNumber;
                    }
                    else if (!firstIs60 || (firstIs6 && !secondIs0))
                    {
                        contactNumber = "60" + contactNumber;
                    }
                }
            }
            Logger.Info("Output string = " + contactNumber);
            return contactNumber;
        }

		/// <summary>
		/// this is to make the first character of the string is on capital.
		/// </summary>
		/// <param name="_input"></param>
		/// <returns></returns>
		public static string capitalizeFirstChar(string value)
		{
			if (string.IsNullOrWhiteSpace(value))
				return string.Empty;

			char[] array = value.ToLower().ToCharArray();
			// Handle the first letter in the string.
			if (array.Length >= 1)
			{
				if (char.IsLower(array[0]))
				{
					array[0] = char.ToUpper(array[0]);
				}
			}
			// Scan through the letters, checking for spaces.
			// ... Uppercase the lowercase letters following spaces.
			for (int i = 1; i < array.Length; i++)
			{
				if (array[i - 1] == ' ')
				{
					if (char.IsLower(array[i]))
					{
						array[i] = char.ToUpper(array[i]);
					}
				}
			}

			return new string(array);
		}

		public static List<string> convertListIntToString(List<int> intList)
		{
			List<string> result = new List<string>();
			foreach (int i in intList)
			{
				result.Add(i.ToString2());
			}

			return result;
		}

        #region Added by Sindhu on 11th Aug 2013
        public static bool IsImposFile(int regId, int regType, out string status)
        {
            string posFileStatus = "";
            bool isIMPOSFileGenerated = false;
            using (var proxy = new RegistrationServiceProxy())
            {
                isIMPOSFileGenerated = proxy.IsIMPOSFileExists(regId, regType, out status);
                posFileStatus = status;

                if (posFileStatus == "A")
                {
                    proxy.RegistrationCancel(new RegStatus()
                    {
                        RegID = regId,
                        Active = true,
                        CreateDT = DateTime.Now,
                        StartDate = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName,
                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)
                    });

                }

            }
            return isIMPOSFileGenerated;
            // return false;
        }
        #endregion

		public static string GetValueFromSource(string lovType, string source)
		{
			List<refLovList> responseList = new List<refLovList>();
			
			var allLovList = MasterDataCache.Instance.GetAllLovList;
			var filteredLOV = allLovList.Where(x => x.Type.ToUpper() == lovType.ToUpper());
			responseList = filteredLOV.Where(x => x.Source.ToUpper() == source.ToUpper()).ToList();
					
			return responseList.FirstOrDefault().Value;
		}

        public static int GetIDByCode(RefType type, string code)
        {
            IEnumerable<int> ids = null;

            switch (type)
            {
                case RefType.AccountCategory:

                    var acctCat = MasterDataCache.Instance.AccountCategory;
                    if (acctCat != null && acctCat.AccountCategories.Any())
                        ids = acctCat.AccountCategories.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    break;

                case RefType.AddressType:
                    var addrType = MasterDataCache.Instance.Address;
                    if (addrType != null && addrType.AddressTypes.Any())
                    {
                        ids = addrType.AddressTypes.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.Country:
                    var cntry = MasterDataCache.Instance.Country;
                    if (cntry != null && cntry.Country.Any())
                    {
                        ids = cntry.Country.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.ExternalIDType:

                    var extIdType = MasterDataCache.Instance.ExternalIDType;
                    if (extIdType != null && extIdType.ExtIDType.Any())
                    {
                        ids = extIdType.ExtIDType.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.IDCardType:

                    var idCardType = MasterDataCache.Instance.IDCardType;
                    if (idCardType != null && idCardType.IDCardType.Any())
                    {
                        ids = idCardType.IDCardType.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.Market:
                    var market = MasterDataCache.Instance.Market;
                    if (market != null && market.Markets.Any())
                    {
                        ids = market.Markets.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.Nationality:
                    var nationality = MasterDataCache.Instance.Nationality;
                    if (nationality != null && nationality.Nationalities.Any())
                    {
                        ids = nationality.Nationalities.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.OrgType:
                    var orgType = MasterDataCache.Instance.Organization;
                    if (orgType != null && orgType.Organizations.Any())
                    {
                        ids = orgType.Organizations.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.PaymentMode:

                    var paymentMode = MasterDataCache.Instance.PaymentMode;
                    if (paymentMode != null && paymentMode.PaymentMode.Any())
                    {
                        ids = paymentMode.PaymentMode.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.Program:
                    var program = MasterDataCache.Instance.Program;
                    if (program != null && program.Programs.Any())
                    {
                        ids = program.Programs.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.Property:
                    var property = MasterDataCache.Instance.Property;
                    if (property != null && property.Properties.Any())
                    {
                        ids = property.Properties.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.RegType:
                    var regType = MasterDataCache.Instance.RegType;
                    if (regType != null && regType.RegTypes.Any())
                    {
                        ids = regType.RegTypes.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.Status:
                    var status = MasterDataCache.Instance.Status;
                    if (status != null && status.Statuss.Any())
                    {
                        ids = status.Statuss.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.StatusType:
                    var statusType = MasterDataCache.Instance.StatusType;
                    if (statusType != null && statusType.StatusTypes.Any())
                    {
                        ids = statusType.StatusTypes.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.VIPCode:
                    var vipCode = MasterDataCache.Instance.VIPCode;
                    if (vipCode != null && vipCode.VIPCode.Any())
                    {
                        ids = vipCode.VIPCode.Where(a => a.Code == code).Select(a => a.ID).ToList();
                    }
                    break;

                default:
                    throw new ArgumentException("Invalid RefType!");
            }

            if (ids == null || ids.Count() == 0)
                throw new ArgumentException(string.Format("Invalid Code! No records found! Type: {0} | Code: {1}", type.ToString(), code));
            if (ids.Count() > 1)
                throw new ArgumentException(string.Format("Invalid Code! More than 1 record found! Type: {0} | Code: {1}", type.ToString(), code));

            return ids.Single();
        }

        public static int custVIPCheck(Customer cust)
        {
            if (cust != null)
            {
                if (cust.NationalityID != 1)
                {
                    if (cust.IsVIP == 1)
                    {
                        return Util.GetIDByCode(RefType.VIPCode, Properties.Settings.Default.VIPCode_ForeignVIP);
                    }
                    else
                    {
                        return Util.GetIDByCode(RefType.VIPCode, Properties.Settings.Default.VIPCode_Foreign);
                    }
                }
                else
                {
                    if (cust.IsVIP == 1)
                    {
                        return Util.GetIDByCode(RefType.VIPCode, Properties.Settings.Default.VIPCode_IndividalVIP);
                    }
                    else
                    {
                        return Util.GetIDByCode(RefType.VIPCode, Properties.Settings.Default.VIPCode_Individal);
                    }
                }
            }
            else
                return 0;

        }



        public static int GetIDByKenanCode(RefType type, string kenanCode)
        {
            IEnumerable<int> ids = null;

            switch (type)
            {
                case RefType.AccountCategory:
                    var accountCategory = MasterDataCache.Instance.AccountCategory;
                    if (accountCategory != null && accountCategory.AccountCategories.Any())
                    {
                        ids = accountCategory.AccountCategories.Where(a => a.KenanCode == kenanCode).Select(a => a.ID).ToList();
                    }
                    break;

                case RefType.Market:
                    var market = MasterDataCache.Instance.Market;
                    if (market != null && market.Markets.Any())
                    {
                        ids = market.Markets.Where(a => a.KenanCode == kenanCode).Select(a => a.ID).ToList();
                    }
                    break;

                default:
                    throw new ArgumentException("Invalid RefType!");
            }

            if (ids == null || ids.Count() == 0)
                throw new ArgumentException(string.Format("Invalid Kenan Code! No records found! Type: {0} | KenanCode: {1}", type.ToString(), kenanCode));
            if (ids.Count() > 1)
                throw new ArgumentException(string.Format("Invalid Kenan Code! More than 1 record found! Type: {0} | KenanCode: {1}", type.ToString(), kenanCode));

            return ids.Single();
        }

        public static string FormatDate(DateTime? date)
        {
            if (!date.HasValue)
                return "";
            //Support non nullable default date value
            if (date.Value == DateTime.MinValue)
                return "";
            return date.Value.ToString(GetDateFormatString());
        }

        public static string FormatDateTime(DateTime? dt)
        {
            if (!dt.HasValue)
                return string.Empty;
            return dt.Value.ToString(Settings.Default.DateTimeFormatString);
        }

        public static bool IsSuccessCode(string code)
        {
            return code == Properties.Settings.Default.SuccessCode;
        }

        public static string GetCheckBox(bool active)
        {
            string stringCheckBox = "<input type=\"checkbox\" ";
            if (active)
                stringCheckBox += "checked = \"yes\" ";
            stringCheckBox += " disabled />";
            return stringCheckBox;
        }

        public static string ConcatenateStrings(string delimiter, params string[] list)
        {
            string result = "";

            foreach (var item in list)
            {
                if (string.IsNullOrEmpty(item))
                    continue;

                if (!string.IsNullOrEmpty(result))
                    result += delimiter;

                result += item.Trim();
            }

            return result;
        }

        
        /// <summary>
        /// this will return string, e.g : 1 Years 2 Months
        /// if failed return empty string
        /// if 0 month && less than 15 days, then return 0 months
        /// if 0 month && more than 15 days, return 1 months
        /// </summary>
        /// <param name="givenDate"></param>
        /// <returns></returns>
        //Evan-150323 Change for ContractCheck sync with Kenan - Start
        //public static string getDiffDate(DateTime givenDate, bool showYears = true)
        //{
        //    string result = "";
        //    var datetimeNow = DateTime.Now;

        //    //if (givenDate > datetimeNow)
        //    //    return string.Empty;

        //    DateDifference datediff;
        //    datediff = new DateDifference(DateTime.Now, givenDate);

        //    if (showYears)
        //    {
        //        if (datediff.Years >= 1)
        //        {
        //            result = datediff.Years + " Years ";
        //        }

        //        if (datediff.Months >= 0)
        //        {
        //            int? months = datediff.Months;
        //            if (datediff.Days >= 15)
        //            {
        //                months += 1;
        //            }
        //            result += months + " Months";
        //        }
        //    }
        //    else
        //    {
        //        int tempYears = 0;
        //        if (datediff.Years >= 1)
        //        {
        //            tempYears = datediff.Years;
        //        }

        //        if (datediff.Months >= 0)
        //        {
        //            int? months = datediff.Months;
        //            if (datediff.Days >= 15)
        //            {
        //                months += 1;
        //            }
        //            result += (tempYears * 12) + months + " Months";
        //        }
        //    }


        //    return result;
        //}

        public static string getDiffDate(DateTime givenDate, DateTime? givenStartDate = null)
        {
            string result = "";
            //Drop5 UAT - Bugzilla 961 to remove the time information
            var startDate = givenStartDate != null && givenStartDate > DateTime.Now ? Convert.ToDateTime(givenStartDate).Date : DateTime.Now.Date;//03092015 - Anthony - To cater the start date whether it's future start date (extended contract) or not

            int datediffdays;
            
            datediffdays = ((
                (givenDate > startDate) ? (givenDate - startDate) : (startDate - givenDate)
                ).TotalMilliseconds / 86400000).ToInt();
            int calcDuration = (int)Math.Round((datediffdays * 12.0) / 365.0);
            
            //datediffdays = ((
            //    (givenDate > DateTime.Now) ? (givenDate - DateTime.Now) : (DateTime.Now - givenDate)
            //    ).TotalDays + 1).ToDecimal();
            //decimal calcDuration = Math.Round(Math.Truncate(100 * ((datediffdays * 12) / 365)) / 100);
            
            result = calcDuration.ToString2() + " Months";

            return result;
        }
        //Evan-150323 Change for ContractCheck sync with Kenan - end


        public static string getDiffBetweenDate(DateTime fromDate, DateTime toDate, bool showYears = true)
        {
            string result = "";

            DateDifference datediff;
            datediff = new DateDifference(fromDate, toDate);

            if (showYears)
            {
                if (datediff.Years >= 1)
                {
                    result = datediff.Years + " Years ";
                }

                if (datediff.Months >= 0)
                {
                    int? months = datediff.Months;
                    if (datediff.Days >= 15)
                    {
                        months += 1;
                    }
                    result += months + " Months";
                }
            }
            else
            {
                int tempYears = 0;
                if (datediff.Years >= 1)
                {
                    tempYears = datediff.Years;
                }

                if (datediff.Months >= 0)
                {
                    int? months = datediff.Months;
                    if (datediff.Days >= 15)
                    {
                        months += 1;
                    }
                    result += (tempYears * 12) + months + " Months";
                }
            }


            return result;
        }


        public static string GetMD5Hash(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        public static string GetLoginDateTime()
        {
            var datetime = DateTime.Now;

            return datetime.ToString("dddd d MMMM yyyy h:mm tt");
        }

        public static string Barcodes(string strTextToEncode)
        {
            int intBarcodeHeight = 40, intBarcodeWidthMultiplier = 1;
            string strDataToEncode = strTextToEncode.ToUpper();
            string strEncodedData = "", strBarcodeImgs = "";

            Dictionary<string, string> dictEncoding = new Dictionary<string, string>();
            #region Barcode Dictionary

            dictEncoding.Add("0", "101001101101");
            dictEncoding.Add("1", "110100101011");
            dictEncoding.Add("2", "101100101011");
            dictEncoding.Add("3", "110110010101");
            dictEncoding.Add("4", "101001101011");
            dictEncoding.Add("5", "110100110101");
            dictEncoding.Add("6", "101100110101");
            dictEncoding.Add("7", "101001011011");
            dictEncoding.Add("8", "110100101101");
            dictEncoding.Add("9", "101100101101");
            dictEncoding.Add("A", "110101001011");
            dictEncoding.Add("B", "101101001011");
            dictEncoding.Add("C", "110110100101");
            dictEncoding.Add("D", "101011001011");
            dictEncoding.Add("E", "110101100101");
            dictEncoding.Add("F", "101101100101");
            dictEncoding.Add("G", "101010011011");
            dictEncoding.Add("H", "110101001101");
            dictEncoding.Add("I", "101101001101");
            dictEncoding.Add("J", "101011001101");
            dictEncoding.Add("K", "110101010011");
            dictEncoding.Add("L", "101101010011");
            dictEncoding.Add("M", "110110101001");
            dictEncoding.Add("N", "101011010011");
            dictEncoding.Add("O", "110101101001");
            dictEncoding.Add("P", "101101101001");
            dictEncoding.Add("Q", "101010110011");
            dictEncoding.Add("R", "110101011001");
            dictEncoding.Add("S", "101101011001");
            dictEncoding.Add("T", "101011011001");
            dictEncoding.Add("U", "110010101011");
            dictEncoding.Add("V", "100110101011");
            dictEncoding.Add("W", "110011010101");
            dictEncoding.Add("X", "100101101011");
            dictEncoding.Add("Y", "110010110101");
            dictEncoding.Add("Z", "100110110101");
            dictEncoding.Add("-", "100101011011");
            dictEncoding.Add(":", "110010101101");
            dictEncoding.Add(" ", "100110101101");
            dictEncoding.Add("$", "100100100101");
            dictEncoding.Add("/", "100100101001");
            dictEncoding.Add("+", "100101001001");
            dictEncoding.Add("%", "101001001001");
            dictEncoding.Add("*", "100101101101");
            dictEncoding.Add("_", "100101011011");

            #endregion

            strEncodedData = dictEncoding["*"] + "0";
            for (int i = 0; i < strDataToEncode.Length; i++)
            {
                strEncodedData += dictEncoding[strDataToEncode.Substring(i, 1)] + "0";
            }
            strEncodedData += dictEncoding["*"];

            var blackImgUrl = VirtualPathUtility.ToAbsolute("~/Content/images/bar_blk.gif");
            var whiteImgUrl = VirtualPathUtility.ToAbsolute("~/Content/images/bar_wht.gif");

            for (int i = 0; i < strEncodedData.Length; i++)
            {
                if (strEncodedData.Substring(i, 1) == "1")
                {
                    strBarcodeImgs += "<img src=\"" + blackImgUrl + "\"" +
                                    " width=\"" + intBarcodeWidthMultiplier + "\"" +
                                    " height=\"" + intBarcodeHeight + "\" />";
                }
                else
                {
                    strBarcodeImgs += "<img src=\"" + whiteImgUrl + "\"" +
                                    " width=\"" + intBarcodeWidthMultiplier + "\"" +
                                    " height=\"" + intBarcodeHeight + "\" />";
                }
            }
            return strBarcodeImgs;
        }

        public static Bitmap GenerateQRCode(string code, int width, int height)
        {
            QRCodeWriter qrCode = new QRCodeWriter();
            ByteMatrix byteIMG = qrCode.encode(code, com.google.zxing.BarcodeFormat.QR_CODE, width, height);
            sbyte[][] img = byteIMG.Array;
            Bitmap bmp = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(bmp);
            g.Clear(Color.White);

            for (int i = 0; i <= img.Length - 1; i++)
            {
                for (int j = 0; j <= img[i].Length - 1; j++)
                {
                    if (img[j][i] == 0)
                        g.FillRectangle(Brushes.Black, i, j, 1, 1);
                    else
                        g.FillRectangle(Brushes.White, i, j, 1, 1);
                }
            }

            return bmp;
        }

        public static string GetPrintVersionNum(int regType, string brand, string PlanKenanCode, String Trn_Type = null)
        {
            string value = "";
            using (var proxy = new RegistrationServiceProxy())
            {
                value = proxy.GetPrintVersionNum(regType, brand, PlanKenanCode, Trn_Type);
            }
            return value;
        }

        // COV
        public static string ConvertTrendToIndicator(double input)
        {
            return input > 100.00 ? "UP" : input < 100.00 ? "DOWN" : "NORMAL";
        }

        public static bool IsNumeric(string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

		public static string checkIsellContractGroup(string kenanCode)
		{
			var contractGroupList = MasterDataCache.Instance.GetAllLovList.Where(x => x.Type == "CONTRACT_GROUP").ToList();

			if (contractGroupList.Where(x => x.Source == kenanCode).Any())
				return contractGroupList.FirstOrDefault(x => x.Source == kenanCode).Value;
			else
				return string.Empty;
		}

        public static string getMarketCodeLiberalisationMOC(retrieveAcctListByICResponse accountList, string criteria)
        {
            string result = string.Empty;

			if (accountList == null)
				return result;

            switch (criteria)
            {
				case "DF_MOCWAIVER":
				foreach (var itemList in accountList.itemList)
				{
					if (itemList.Customer != null && !string.IsNullOrEmpty(itemList.Customer.Moc))
					{
						var _MOCWaiverEligibleList = MasterDataCache.Instance.GetAllLovList.Where(x => x.Type == "DF_MOC").Select(x => x.Value);
						if (_MOCWaiverEligibleList != null && _MOCWaiverEligibleList.Any())
						{
							if (itemList.Customer.Moc.Contains("-"))
							{
								string[] curMOC = itemList.Customer.Moc.Trim().Split('-');
								string mocLevel = curMOC[0];
								if (_MOCWaiverEligibleList.Contains(mocLevel.ToUpper()))
								{
									result = "YES";
									break;
								}
									
							}
							else {
								if (_MOCWaiverEligibleList.Contains(itemList.Customer.Moc.ToUpper()))
								{
									result = "YES";
									break;
								}
									
							}					
						}			
						//break;
					}
				}
					break;

                case "MARKETCODE":
                    foreach (var itemList in accountList.itemList)
                    {
                        if (itemList.AccountDetails != null && !string.IsNullOrEmpty(itemList.AccountDetails.CustomerType))
                        {
                            result = itemList.AccountDetails.CustomerType;
                            break;
                        }
                    }
                    break;

                case "MOC":
                    foreach (var itemList in accountList.itemList)
                    {
                        if (itemList.Customer != null && !string.IsNullOrEmpty(itemList.Customer.Moc))
                        {
                            result = "YES";
                            break;
                        }
                    }
                    break;

                case "LIBERALISATION":

                    Logger.Debug("################ LIBERALISATION ###############");
                    Logger.Debug(XMLHelper.ConvertObjectToXml(accountList));

                    #region
                    Dictionary<string, int> classRating = new Dictionary<string, int>();
                    classRating.Add("ELITE_A", 1);
                    classRating.Add("SEL500_A", 2);
                    classRating.Add("SEL400_A", 3);
                    classRating.Add("SEL300_A", 4);
                    classRating.Add("TEN7_A", 5);
                    classRating.Add("MASS_A", 6);

                    classRating.Add("ELITE_B", 11);
                    classRating.Add("SEL500_B", 12);
                    classRating.Add("SEL400_B", 13);
                    classRating.Add("SEL300_B", 14);
                    classRating.Add("TEN7_B", 15);
                    classRating.Add("MASS_B", 16);

                    classRating.Add("ELITE_C", 21);
                    classRating.Add("SEL500_C", 22);
                    classRating.Add("SEL400_C", 23);
                    classRating.Add("SEL300_C", 24);
                    classRating.Add("TEN7_C", 25);
                    classRating.Add("MASS_C", 26);

                    classRating.Add("ELITE_D", 27);
                    classRating.Add("SEL500_D", 28);
                    classRating.Add("SEL400_D", 29);
                    classRating.Add("SEL300_D", 30);
                    classRating.Add("TEN7_D", 31);
                    classRating.Add("MASS_D", 32);
                    #endregion
                    string lowestShow = System.Configuration.ConfigurationManager.AppSettings["LiberalisationLowest"];

                    int tempResult = lowestShow.ToUpper().Equals("TRUE") ? 0 : 100;

                    List<string> riskCategoryList = new List<string>();
                    foreach (var itemList in accountList.itemList)
                    {
                        if (itemList.Customer != null && !string.IsNullOrEmpty(itemList.Customer.RiskCategory))
                        {
                            if (!riskCategoryList.Contains(itemList.Customer.RiskCategory.ToUpper()))
                            {
                                riskCategoryList.Add(itemList.Customer.RiskCategory.ToUpper());
                            }
                        }
                    }
                    foreach (string risk in riskCategoryList)
                    {
                        if (lowestShow.ToUpper().Equals("TRUE"))
                        {
                            if (classRating.ContainsKey(risk) && tempResult < classRating[risk])
                            {
                                tempResult = classRating[risk];
                            }
                        }
                        else
                        {
                            if (classRating.ContainsKey(risk) && tempResult > classRating[risk])
                            {
                                tempResult = classRating[risk];
                            }
                        }

                    }
                    result = tempResult > 0 ? classRating.FirstOrDefault(x => x.Value == tempResult).Key : string.Empty;
                    //result = "MASS_A";
                    break;

                default:
                    break;
            }

            result = !string.IsNullOrEmpty(result) ? result : Constants.NOT_AVAILABLE;

            return result;
        }

        public static bool isValidNewNRIC(String newNRIC)
        {
            int lengthIC = newNRIC.Length;
            if (lengthIC == 12)
            {
                //mmddyyaaxxxx
                //012345678901
                String prefix = newNRIC.Substring(0, 6);
                String suffix = newNRIC.Substring(8);

                if (IsNumeric(prefix) && isValidNRICPrefix(prefix) && IsNumeric(suffix))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private static bool isValidNRICPrefix(String prefix)
        {
            String mm = prefix.Substring(2, 2);
            String day = prefix.Substring(4, 2);

            int dd = int.Parse(day);

            if (mm.Equals("01") || mm.Equals("02") || mm.Equals("03") || mm.Equals("04") || mm.Equals("05") || mm.Equals("06")
                       || mm.Equals("07") || mm.Equals("08") || mm.Equals("09") || mm.Equals("10") || mm.Equals("11") || mm.Equals("12"))
            {
                if (mm.Equals("01") || mm.Equals("03") || mm.Equals("05") || mm.Equals("07") || mm.Equals("08") || mm.Equals("10") || mm.Equals("12"))
                {
                    if (dd >= 1 && dd <= 31)
                    {
                        return true;
                    }
                }
                else if (mm.Equals("04") || mm.Equals("06") || mm.Equals("09") || mm.Equals("11"))
                {
                    if (dd >= 1 && dd <= 30)
                    {
                        return true;
                    }
                }
                else
                {
                    if (dd >= 1 && dd <= 29)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
		
		// this method for determine which store that can be use iSell drop 4
		// and will impact iContract & email capabilities.
		// this method is exist on Integration Service layer, so if there's any changes, change it both
		public static bool rollOutCheck()
		{
			bool resp = false;

			List<string> storeIDs = new List<string>();
			var currentStoreID = Util.SessionAccess.User.Org.ID.ToString2();

			if (!ReferenceEquals(ConfigurationManager.AppSettings["StoreListRollOut"], null))
			{
				storeIDs = System.Configuration.ConfigurationManager.AppSettings["StoreListRollOut"].ToString2().Split(',').ToList();
				if (storeIDs.Count > 0)
				{
					if (storeIDs.Contains(currentStoreID))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			else
			{
				// if it's null, means this is full rollout, so return true
				return true;
			}

			return resp;
		}




        #region printing code

        public static string URLPath()
        {
            return System.Configuration.ConfigurationManager.AppSettings["PrintHTTPpath"];
        }
        public static int PrintFile(string HTML, string printer, int regID)
        {
            return PrintFile(HTML, printer, regID.ToString());
        }
        public static int PrintFile(string HTML, string printer, string regID)
        {
            string fileUrl = "";
            try
            {
                printer = WebUtility.HtmlDecode(printer);
                string printFolder = System.Configuration.ConfigurationManager.AppSettings["Folderpath"];
                string phyPath = HttpContext.Current.Server.MapPath("~" + printFolder);
                if (!System.IO.Directory.Exists(phyPath))
                {
                    System.IO.Directory.CreateDirectory(phyPath);
                }
                string strPart = string.Format("{0:yyyyMMddhhmmssfff}", System.DateTime.Now) + ".htm";

                using (StreamWriter swXLS = new StreamWriter(phyPath + strPart))
                {
                    swXLS.Write(HTML.ToString());
                }

                var urlsList = System.Configuration.ConfigurationManager.AppSettings["PrintFilePaths"];
                fileUrl = urlsList + printFolder + "/" + strPart;

                using (var nwPrintClient = new NetworkPrintSvc.NetworkPrintServiceClient())
                {
                    var response = nwPrintClient.Print(new NetworkPrintSvc.PrintRequest()
                    {
                        FileUrl = fileUrl,
                        PrinterName = printer,
						RegID = regID
                    });
                    HttpContext.Current.Session[SessionKey.PrintErrorMsg.ToString()] = response.Message;
                    return response.Status ? 1 : 0;
                }
                return 1;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Session[SessionKey.PrintErrorMsg.ToString()] = "Server exception occured while printing.";
                Logger.Error("Exception in PrintFile : " + fileUrl, ex);
                return 0;
            }
        }
        public static List<SelectListItem> GetPrinters(string defaultValue = "0", string defaultText = "default")
        {
            var proxy = new RegistrationServiceProxy();
            var resultList = new List<SelectListItem>();
            if (defaultText == "default")
                defaultText = "-- Please Select --";

            var statusList = proxy.GetUserSpecificPrinters(SessionAccess.UserID, SessionAccess.User.OrgID);
            if (statusList != null && statusList.Any())
            {
                resultList = statusList.Select(a =>
                    new SelectListItem()
                    {
                        Text = a.PrinterName,
                        Value = a.id.ToString()
                    }).ToList();

            }
            else
                resultList = new List<SelectListItem>();

            resultList.Insert(0, new SelectListItem()
            {
                Text = defaultText,
                Value = defaultValue,
                Selected = true
            });
            return resultList;
        }
        public static System.Threading.ManualResetEvent printEvntTimer = null;

        public static string _printStatus = "";

        public static List<DAL.Models.UserPrintLog> getPrintStatus(string uri)
        {
            int i = 0;
            List<DAL.Models.UserPrintLog> obj = null;

            using (var proxy = new RegistrationServiceProxy())
            {
                obj = proxy.GetPrintersLog(uri);

                if (obj.Count > 0)
                {

                    if (obj[0].status > 0)
                    {
                        i = 100001;

                    }
                }

                _printStatus = "Printed";

                i = i + 1;
            }

            return obj;

        }

        public static int PrintThroughExe(string printerPath, string FilePath, string AppPath, string PrinterName, string RegID)
        {
            int success = 0;

            Process myProcess = new Process();
            List<DAL.Models.UserPrintLog> obj = null;

            try
            {

                SaveUserPrintLog(printerPath, FilePath, "Print Sent to app", 1, PrinterName, RegID);
                obj = getPrintStatus(FilePath);

                success = obj[0].status;
            }

            catch (Exception ex)
            {
                SaveUserPrintLog(printerPath, FilePath, "Print Failed", 2, PrinterName, RegID);
            }
            finally
            {
            }

            return success;
        }
        public static CorpType getSearchCorporateType(string searchType)
        {
            CorpType corpType = CorpType.BRN;
            if (searchType.ToUpper() == "NAME")
            {
                corpType = CorpType.NAME;
            }
            else if (searchType.ToUpper() == "MASTERACCT")
            {
                corpType = CorpType.MASTERACCT;
            }
            else if (searchType.ToUpper() == "PARENTACCT")
            {
                corpType = CorpType.PARENTACCT;
            }
            return corpType;
        }

        public static void SaveUserPrintLog(string PrinterPath, string Printfile, string remarks, int Status, string PrinterName, string RegID)
        {
            UserPrintLog objUserPrintLog = new UserPrintLog();

            objUserPrintLog.Orgid = Util.SessionAccess.User.OrgID;
            objUserPrintLog.PrinterName = PrinterName;
            objUserPrintLog.Printfile = Printfile;
            objUserPrintLog.PrinterNetworkPath = PrinterPath;
            objUserPrintLog.RegID = Convert.ToInt32(RegID);
            objUserPrintLog.remarks = remarks;
            objUserPrintLog.status = Status;
            objUserPrintLog.UserID = Util.SessionAccess.UserID;

            using (var Printlogproxy = new RegistrationServiceProxy())
            {
                Printlogproxy.SaveUserPrintLog(objUserPrintLog);
            }
        }

        #endregion printing code
        #region "VLT ADDED CODE"
        public static string ConvertObjectToXml(object objInput)
        {
            try
            {
                #region "testing purpose to test the performance"

                string serializedContent = string.Empty;
                DataContractSerializer serializer = new DataContractSerializer(objInput.GetType());
                using (MemoryStream ms = new MemoryStream())
                {
                    serializer.WriteObject(ms, objInput);
                    ms.Position = 0;

                    using (StreamReader sr = new StreamReader(ms))
                    {
                        serializedContent = sr.ReadToEnd();
                    }
                }
                serializer = null;
                return serializedContent;

                #endregion


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //objStringWriter.Close();
            }
        }


        public static T Deserialize<T>(string rawXml)
        {
            using (XmlReader reader = XmlReader.Create(new StringReader(rawXml)))
            {
                DataContractSerializer formatter0 =
                    new DataContractSerializer(typeof(T));
                return (T)formatter0.ReadObject(reader);
            }
        }
        #endregion

        #region SUTAN ADDED CODE 5Th March 2013
        /// <summary>
        /// Resizes the image passed.
        /// </summary>        
        /// <param name="srcImage">The SRC image.</param>
        /// <param name="imageDpi">The image dpi.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="imagePlaceHolderWidth">Width of the image place holder.</param>
        /// <param name="imagePlaceHolderHeight">Height of the image place holder.</param>
        /// <param name="fitImage">The fit image.</param>
        /// <returns>TypeOf(Bool) returns true if resized</returns>
        public static bool ResizeImage(System.Drawing.Image srcImage, int imageDpi, string filePath, int imagePlaceHolderWidth, int imagePlaceHolderHeight, string fitImage)
        {
            try
            {
                if (imageDpi - 1 > 72)
                {
                    imagePlaceHolderWidth = ((imagePlaceHolderWidth) * (imageDpi - 1)) / 72;
                    imagePlaceHolderHeight = ((imagePlaceHolderHeight) * (imageDpi - 1)) / 72;
                }
                ImageResize objResize = new ImageResize();

                if ((float)srcImage.Width < (float)imagePlaceHolderWidth || (float)srcImage.Height < (float)imagePlaceHolderHeight)
                {
                    if (imageDpi - 1 <= 72)
                    {
                        return false;
                    }
                }
                float AspectRatiobtSourceDest = 0;
                AspectRatiobtSourceDest = (float)(((float)srcImage.Width / (float)srcImage.Height) - ((float)imagePlaceHolderWidth / (float)imagePlaceHolderHeight));


                if (AspectRatiobtSourceDest == 0)
                {
                    //resize the image
                    objResize.FixedSize(srcImage, imagePlaceHolderWidth, imagePlaceHolderHeight, imageDpi).Save(filePath, ImageFormat.Jpeg);

                }
                else if (AspectRatiobtSourceDest < 0)//Portait
                {

                    //Crop the image
                    objResize.Crop(srcImage, imagePlaceHolderWidth, imagePlaceHolderHeight, ImageResize.AnchorPosition.Center, imageDpi).Save(filePath, ImageFormat.Jpeg);

                }
                else if (AspectRatiobtSourceDest > 0)//Landscape
                {
                    //Crop the image
                    objResize.Crop(srcImage, imagePlaceHolderWidth, imagePlaceHolderHeight, ImageResize.AnchorPosition.Center, imageDpi).Save(filePath, ImageFormat.Jpeg);

                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

		public static string AddWaterMark(Image imgPhoto, string filePath)
		{
			string result = "";
			try
			{
				int sourceWidth = imgPhoto.Width;
				int sourceHeight = imgPhoto.Height;
				int sourceX = 0;
				int sourceY = 0;
				int destX = 0;
				int destY = 0;
				float nPercent = 0;
				float nPercentW = 0;
				float nPercentH = 0;
				int Width = imgPhoto.Width;
				int Height = imgPhoto.Height;

				nPercentW = ((float)Width / (float)sourceWidth);
				nPercentH = ((float)Height / (float)sourceHeight);
				if (nPercentH < nPercentW)
				{
					nPercent = nPercentH;
					destX = (int)((Width - (sourceWidth * nPercent)) / 2);
				}
				else
				{
					nPercent = nPercentW;
					destY = (int)((Height - (sourceHeight * nPercent)) / 2);
				}
				int destWidth = (int)(sourceWidth * nPercent);
				int destHeight = (int)(sourceHeight * nPercent);

				Bitmap bmPhoto = new Bitmap(imgPhoto);

				Graphics grPhoto = Graphics.FromImage(bmPhoto);
				grPhoto.Clear(Color.White);
				grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
				grPhoto.DrawImage(imgPhoto,
				new Rectangle(destX, destY, destWidth, destHeight),
				new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
				GraphicsUnit.Pixel);
				Matrix matrix = new Matrix();

				//WaterMark starts from here
				String watermarkText = "For Maxis use only";
				StringFormat stringFormat = new StringFormat();
				stringFormat.Alignment = StringAlignment.Center;
				stringFormat.LineAlignment = StringAlignment.Center;
				matrix.Translate(bmPhoto.Width / 2, bmPhoto.Height / 2);
				matrix.Rotate(-40.0f);
				grPhoto.Transform = matrix;
				Font font = new Font("Verdana", 42, FontStyle.Bold, GraphicsUnit.Pixel);
				Color color = Color.FromArgb(100, 0, 0, 0); //Adds a transparent watermark with an 100 alpha value.
				SolidBrush sbrush = new SolidBrush(color);
				grPhoto.DrawString(watermarkText, font, new SolidBrush(color), 0, 0, stringFormat);

				// over Watermark till here
				grPhoto.Dispose();
				bmPhoto.Save(filePath);
			}
			catch (Exception ex)
			{
				result = "FAILED";
				Logger.Fatal("FAILED TO ADD WATERMARK" + Util.LogException(ex));
			}
			
			return result;
		}

        public static bool AlwaysResizeImage(System.Drawing.Image srcImage, int imageDpi, string filePath, int imagePlaceHolderWidth, int imagePlaceHolderHeight, string fitImage)
        {
            try
            {
                /* RST failed resize png
				if ((float)srcImage.Width < (float)imagePlaceHolderWidth || (float)srcImage.Height < (float)imagePlaceHolderHeight)
				{
					if (imageDpi - 1 < 72)
					{
						return false;
					}
				}*/
                var ratioX = (double)imagePlaceHolderWidth / srcImage.Width;
                var ratioY = (double)imagePlaceHolderHeight / srcImage.Height;
                var ratio = Math.Min(ratioX, ratioY);

                var newWidth = (int)(srcImage.Width * ratio);
                var newHeight = (int)(srcImage.Height * ratio);


                ImageResize objResize = new ImageResize();

                objResize.FixedSize(srcImage, newWidth, newHeight, imageDpi).Save(filePath, ImageFormat.Jpeg);

                return true;

            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }

        #region uploadDocument

        public static string uploadFile(string imageResizeLocation, string imageUploadLocation, HttpPostedFileBase file, bool needResize = true)
        {
            string SourceImageName, ActualImageName, onlyFileExtension, imageUrl, imageType = string.Empty;
            System.Drawing.Image srcimage = null;
            string fitImage = Properties.Settings.Default.fitImage;

            ActualImageName = SourceImageName = Path.GetFileName(file.FileName);
            Util.checkFileExistance(imageUploadLocation, ref SourceImageName, out onlyFileExtension);
            if (needResize)
            {
                imageUploadLocation = Path.Combine(imageUploadLocation + @"\", SourceImageName);
                file.SaveAs(imageUploadLocation);
                srcimage = System.Drawing.Image.FromFile(imageUploadLocation);
                bool isResized = Util.AlwaysResizeImage(srcimage, 23, imageResizeLocation + SourceImageName, 540, 365, fitImage);

                imageResizeLocation = Path.Combine(imageResizeLocation + @"\", SourceImageName);
                return imageResizeLocation;
            }
            else
            {

                imageUploadLocation = Path.Combine(imageUploadLocation + @"\", SourceImageName);
                file.SaveAs(imageUploadLocation);

            }
            //file.SaveAs(imageResizeLocation);
            return imageUploadLocation;
        }


        /*
        public static bool uploadFile(string type, string regid, HttpPostedFileBase[] files)
        {
            bool uploadSuccess = true;
            if (files != null)
            {
                int fileCount = files.Count();

                for (int i = 0; i < fileCount; i++)
                {
                    var file = files[i];
                    if (file != null)
                    {
						

                        string fileExtension, fullFilePath = string.Empty;
                        // get the extension
                        fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();

                        string networkPath = "@\\10.21.3.71\\Users\\acn_GERRY\\upload";
                        string filename = string.Format("{0}-{1}{2}", regid, type, fileExtension);
                        string folderName = regid;

                        IntPtr token = IntPtr.Zero;
                        Logger.Info("The File Network Path is :+" + networkPath );
                        //bool isSuccess = LogonUser(res[0].POSUserName, res[0].POSDomain, res[0].POSPassword,
                        //LOGON32_LOGON_NEW_CREDENTIALS,
                        //LOGON32_PROVIDER_DEFAULT, ref token);
                        //using (WindowsImpersonationContext filecopy = new WindowsIdentity(token).Impersonate())
                        //{

                        //}

                        if (System.IO.Directory.Exists(@"\\10.21.3.71\Users\acn_GERRY\upload"))
                        {
                            // if exist
                            // create the folder in iContract named with id

                            System.IO.Directory.CreateDirectory(networkPath + @"\" + folderName);
                            string filePath = networkPath + @"\" + folderName;

                            ///ONLY PROCESS IF LENGTH >0
                            if (file.ContentLength > 0)
                            {
                                ///Check whether there is already an image in the folder with the same name
                                fullFilePath = Path.Combine(filePath + @"\", filename);
                                ///Save the image

                                file.SaveAs(fullFilePath);
                                uploadSuccess = true;
                            }
                        }
                    }
                }
            }
            return uploadSuccess;
        }
        */
        #endregion

        /// <summary>
        /// Checks the file existance.
        /// </summary>
        /// <Author>Sutan Dan</Author>
        /// <param name="path">The path.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="onlyFileExtension">The only file extension.</param>
        public static void checkFileExistance(string path, ref string fileName, out string onlyFileExtension)
        {
            string onlyFileName = Path.GetFileNameWithoutExtension(path + @"\" + fileName);
            int startPointOfExtension = 0;
            onlyFileExtension = string.Empty;
            startPointOfExtension = fileName.LastIndexOf(".", StringComparison.OrdinalIgnoreCase);
            int uniqueNumber = 2;

            onlyFileExtension = startPointOfExtension >= 0 ? fileName.Substring(startPointOfExtension) : string.Empty;

            string directoryName = Path.GetDirectoryName(path + @"\");
            string[] files = Directory.GetFiles(directoryName);

            string tempFileName = fileName;

            while (true)
            {
                if (System.IO.File.Exists(Path.Combine(path, fileName)))
                {
                    fileName = onlyFileName + "(" + (uniqueNumber++).ToString() + ")" + onlyFileExtension;
                }
                else
                {
                    break;
                }
            }
        }
        #endregion SUTAN ADDED CODE 5Th March 2013

        public static string RemoveAllNamespaces(string xmlDocument)
        {
            XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

            return xmlDocumentWithoutNs.ToString();
        }

        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;
                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }

        public static DataSet ConvertObjectToDataSet(object obj)
        {
            DataSet ds = new DataSet();

            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            System.IO.StringWriter writer = new System.IO.StringWriter();
            xmlSerializer.Serialize(writer, obj);
            System.IO.StringReader reader = new System.IO.StringReader(writer.ToString());
            ds.ReadXml(reader);
            return ds;
        }

        public static string[] GetMocAndLibStatus(string MSISDN, int regID)
        {
            string[] MOC_Lib = new string[4];
            string NotAvailable = "Not Available"; // later move it to Web.config as Key
            if (regID == 0)
            {
                if (!ReferenceEquals(SessionManager.Get("AllServiceDetails"), null))
                {
					retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");

                    if (AcctListByICResponse != null && AcctListByICResponse.itemList != null && AcctListByICResponse.itemList.Where(a => a.ExternalId == MSISDN && a.IsActive == true && a.ServiceInfoResponse != null).ToList().Count() > 0)
                    {
                        var item = AcctListByICResponse.itemList.Where(a => a.ExternalId == MSISDN && a.IsActive == true && a.ServiceInfoResponse != null).FirstOrDefault();
                        if (item != null && item.Customer != null)
                        {
							MOC_Lib[0] = Util.getMarketCodeLiberalisationMOC(AcctListByICResponse, "LIBERALISATION");
                            MOC_Lib[1] = item.Customer.Moc;
                        }
                    }
                    else if (AcctListByICResponse != null && AcctListByICResponse.itemList != null)
                    {
                        foreach (var item in AcctListByICResponse.itemList)
                        {
                            if (item.Customer != null && !string.IsNullOrEmpty(item.Customer.RiskCategory))
                            {
								MOC_Lib[0] = Util.getMarketCodeLiberalisationMOC(AcctListByICResponse, "LIBERALISATION");
                                break;
                            }
                            else
                            {
                                MOC_Lib[0] = NotAvailable;
                            }

                        }

                        foreach (var item in AcctListByICResponse.itemList)
                        {
                            if (item.Customer != null && !string.IsNullOrEmpty(item.Customer.Moc))
                            {
                                MOC_Lib[1] = item.Customer.Moc;
                                break;
                            }
                            else
                            {
                                MOC_Lib[1] = NotAvailable;
                            }

                        }

                    }
                    else
                    {
                        MOC_Lib[0] = NotAvailable;
                        MOC_Lib[1] = NotAvailable;
                    }
                }
                else
                {
                    MOC_Lib[0] = NotAvailable;
                    MOC_Lib[1] = NotAvailable;
                }
            }
            else
            {
                if (regID > 0)
                {
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(regID.ToInt());
                        if (lnkregdetails != null)
                        {

                            if (lnkregdetails.MOC_Status != null)
                                MOC_Lib[0] = lnkregdetails.Liberlization_Status;
                            if (lnkregdetails.Liberlization_Status != null)
                                MOC_Lib[1] = lnkregdetails.MOC_Status;

                            //for justification
                            MOC_Lib[2] = lnkregdetails.Justification.ToString2();

                            /*Changes by chetan related to PDPA implementation*/
                            MOC_Lib[3] = lnkregdetails.PdpaVersion.ToString2();

                        }

                    }
                }
            }
            if (string.IsNullOrEmpty(MOC_Lib[0]))
                MOC_Lib[0] = NotAvailable;
            if (string.IsNullOrEmpty(MOC_Lib[1]))
                MOC_Lib[1] = NotAvailable;

            return MOC_Lib;
        }

        public static List<Component> GetContractsBYRegId(int regID, bool isSmart = false)
        {
            List<Component> componentsList = new List<Component>();

            using (var proxy = new CatalogServiceProxy())
            {
                List<int> contractId = null;
                if (isSmart)
                {
                    var lProxy = new CatalogSvc.CatalogServiceClient();
                    contractId = lProxy.GetContractByRegIdForSmart(regID).ToList();
                    if (contractId.Count > 0)
                    {
                        var vases = lProxy.PgmBdlPckComponentGetForSmart(
                            new CatalogSvc.PgmBdlPckComponentGetReqSmart()
                            {
                                IDs = contractId.ToArray(),
                            });

                        componentsList = proxy.ComponentGet(vases.PgmBdlPckComponents.Select(a => a.ChildID)).ToList();

                    }
                }
                else
                {
                    contractId = proxy.GetContractIdByRegID(regID);
                    if (contractId.Count > 0)
                    {
                        var vases = MasterDataCache.Instance.FilterComponents
                        (
                        (IEnumerable<int>)contractId
                        ).ToList();

                        componentsList = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();

                    }
                }

            }
            return componentsList;
        }

        public static List<string> GetContractsKenanCode_ByRegID(int regID, bool isSmart = false)
        {
            List<string> componentsList = new List<string>();
            List<Contract> Contractlist = new List<Contract>();
            using (var proxy = new CatalogServiceProxy())
            {
                List<int> contractId = null;
                if (isSmart)
                {
                    var lProxy = new CatalogSvc.CatalogServiceClient();
                    contractId = lProxy.GetContractByRegIdForSmart(regID).ToList();
                    if (contractId.Count > 0)
                    {
                        var vases = lProxy.PgmBdlPckComponentGetForSmart(
                            new CatalogSvc.PgmBdlPckComponentGetReqSmart()
                            {
                                IDs = contractId.ToArray(),
                            });

                        componentsList = vases.PgmBdlPckComponents.Select(a => a.KenanCode).ToList();

                    }
                }
                else
                {
                    contractId = proxy.GetContractIdByRegID(regID);
                    if (contractId.Count > 0)
                    {
                        var vases = MasterDataCache.Instance.FilterComponents
                        (
                        (IEnumerable<int>)contractId
                        ).ToList();

                        componentsList = vases.Select(a => a.KenanCode).ToList();

                    }
                }

            }
            return componentsList;
        }

        public static List<VoiceContractDetails> GetVoiceContractsBYRegId(int regID, bool isSmart = false)
        {
            List<VoiceContractDetails> VoiceContractDetails = new List<VoiceContractDetails>();
            using (var proxy = new CatalogServiceProxy())
            {
                List<int> contractId = null;
                if (isSmart)
                {
                    var lProxy = new CatalogSvc.CatalogServiceClient();
                    contractId = lProxy.GetContractByRegIdForSmart(regID).ToList();
                }
                else
                {
                    contractId = proxy.GetContractIdByRegID(regID);
                }
                if (contractId.Count > 0)
                {
                    var vases = MasterDataCache.Instance.FilterComponents
                    (
                    (IEnumerable<int>)contractId
                    ).ToList();

                    VoiceContractDetails = proxy.GetVoiceAddendums(vases.Select(a => a.KenanCode)).ToList();

                }
            }
            return VoiceContractDetails;
        }

        public static PgmBdlPckComponent GetContractPbpcByRegSuppLineId(int regSuppLineID)
        {
            var contractPbpc = new PgmBdlPckComponent();
            List<Component> componentsList = new List<Component>();

            using (var proxy = new CatalogServiceProxy())
            {
                var contractId = proxy.GetContractIdByRegSuppLineID(regSuppLineID);
                contractPbpc = proxy.PgmBdlPckComponentGet(new List<int> { contractId }).FirstOrDefault();
            }
            return contractPbpc != null ? contractPbpc : new PgmBdlPckComponent();
        }

        public static Component GetContractByRegSuppLineId(int regSuppLineID)
        {
            Component component = new Component();

            using (var proxy = new CatalogServiceProxy())
            {
                int contractId = proxy.GetContractIdByRegSuppLineID(regSuppLineID);
                if (contractId > 0)
                {
                    var vases = MasterDataCache.Instance.FilterComponents(new List<int> { contractId });
                    component = proxy.ComponentGet(vases.Select(a => a.ChildID)).FirstOrDefault();
                }
            }
            return component;
        }

        public static int SaveUserDMEPrint(UserDMEPrint objUserDMEPrint)
        {
            var _client = new Online.Registration.Web.RegistrationSvc.RegistrationServiceClient();
            var resp = _client.SaveUserDMEPrint(objUserDMEPrint);
            return resp;
        }

        public static List<DAL.Models.UserDMEPrint> GetUserDMEPrinters(int userId)
        {
            var _client = new Online.Registration.Web.RegistrationSvc.RegistrationServiceClient();
            var resp = _client.GetUserDMEPrinters(userId).ToList();
            return resp;
        }

        public static List<DAL.Models.PendingRegBREStatus> PendingOrders()
        {
            var _client = new Online.Registration.Web.RegistrationSvc.RegistrationServiceClient();
            var resp = _client.PendingOrders().ToList();
            return null;//return resp;
        }

        public static int UpdateBREApproveStatus(int RegId, string Status, string LastAccessID)
        {
            var _client = new Online.Registration.Web.RegistrationSvc.RegistrationServiceClient();
            var resp = _client.UpdateBREApproveStatus(RegId, Status, LastAccessID);
            return resp;
        }

        public static List<DAL.Models.BRECheck> GetBRECheckDetails(List<int> userGroupId)
        {
            var _client = new Online.Registration.Web.ConfigSvc.ConfigServiceClient();
            var resp = _client.GetBRECheckDetails(userGroupId.ToArray()).ToList();
            return resp;
        }
        public static string GetBREActionByCode(string code)
        {

            var userIds = (IEnumerable<int>)HttpContext.Current.Session["UserGroupId"];

            if (HttpContext.Current.Session["BreChecksbyUserID"] == null)
            {
                HttpContext.Current.Session["BreChecksbyUserID"] = GetBreChecksByUserIds(userIds.ToList());
            }

            var lstBreCheck = (List<BRECheck>)HttpContext.Current.Session["BreChecksbyUserID"];

            foreach (DAL.Models.BRECheck row in lstBreCheck)
            {
                if (row.BRECheckCode == code)
                {
                    return row.BRECheckCondition;
                }
            }
            return "";
        }

        private static List<BRECheck> GetBreChecksByUserIds(List<int> userIds)
        {
            var _client = new Online.Registration.Web.ConfigSvc.ConfigServiceClient();
            var resp = _client.GetBRECheckDetails(userIds.ToArray()).ToList();
            var distinctCheckStatus = resp.Select(x => x.BRECheckCode).Distinct();
            var lstBreCheck = new List<BRECheck>();

            foreach (var status in distinctCheckStatus)
            {
                var checkStatus = resp.Where(a => a.BRECheckCode == status && a.BRECheckCondition == "HardStop");
                if (checkStatus.Any())
                {
                    lstBreCheck.Add(checkStatus.FirstOrDefault());
                }
                else
                {
                    checkStatus = resp.Where(a => a.BRECheckCode == status && a.BRECheckCondition == "Justification");
                    if (checkStatus.Any())
                    {
                        lstBreCheck.Add(checkStatus.FirstOrDefault());
                    }
                }
            }
            return lstBreCheck;
        }

        public static List<VoiceContractDetails> GetRebateDataContractsBYRegId(int regID)
        {
            List<VoiceContractDetails> VoiceContractDetails = new List<VoiceContractDetails>();
            using (var proxy = new CatalogServiceProxy())
            {
                List<string> contractId = null;
                contractId = proxy.GetDataContractByRegId(regID);
                if (contractId.Count > 0)
                {
                    VoiceContractDetails = proxy.GetVoiceAddendums(contractId).ToList();
                }
            }
            return VoiceContractDetails;
        }

        /// <summary>
        /// Get all dealer available packages based on logged in dealer
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="availablePackages"></param>
        /// <returns></returns>
        public static List<ViewModels.AvailablePackage> GetDealerPlans(List<ViewModels.AvailablePackage> availablePackages)
        {
            List<ViewModels.AvailablePackage> dealerPackages = new List<ViewModels.AvailablePackage>();

            if (HttpContext.Current.Session["LoggedInUserID"] != null)
            {
                int userId = Convert.ToInt32(HttpContext.Current.Session["LoggedInUserID"]);
                List<DealerPlanIds> DealerPlansIds = new List<DealerPlanIds>();
                using (var proxy = new RegistrationServiceProxy())
                {
                    DealerPlansIds = proxy.GetDealerSpecificPlans(userId).ToList();
                }
                if (DealerPlansIds.Count > 0)
                {
                    if (DealerPlansIds.FirstOrDefault().isPlanAvailable == 1)
                    {
                        var fileteredPkgs = new List<ViewModels.AvailablePackage>();
                        foreach (DealerPlanIds id in DealerPlansIds)
                        {
                            var PkgId = id.Id;
                            var selectedPkg = availablePackages.Where(a => a.PgmBdlPkgCompID == PkgId).FirstOrDefault();
                            if (selectedPkg != null)
                            {
                                dealerPackages.Add(selectedPkg);
                            }
                        }
                    }
                }
            }
            return dealerPackages;
        }

        #region :::check whether any greater than 2gb components exists on current line.:::
        // common method for 2GB componect checking on MISM scenario
        // if selectedExternalID is not empty, means it's existing customer, come from customer search screen
        public static bool check2GBComponent(string selectedExternalId, string vasComponentList)
        {
            bool is2GBCompExistOnPkg = false;
            HttpContext.Current.Session["eligibleForMISM"] = false;

            if (!string.IsNullOrEmpty(selectedExternalId))
            {
                retrieveAcctListByICResponse AcctListByIC = null;
                List<Online.Registration.Web.SubscriberICService.Items> AccntList = null;

                string AccountPackageName = string.Empty;

                if (HttpContext.Current.Session["AcctAllDetails"] != null)
                {
                    AcctListByIC = (retrieveAcctListByICResponse)HttpContext.Current.Session["AcctAllDetails"];
                    AccntList = AcctListByIC != null ? AcctListByIC.itemList.Where(x => x.ServiceInfoResponse != null && x.ExternalId.Equals(selectedExternalId)).ToList() : null;
                    var v = AccntList[0];
                    if (!ReferenceEquals(v, null))
                    {
                        if (v.ServiceInfoResponse.lob.ToLower() != "PREGSM".ToLower())
                        {
                            IList<Online.Registration.Web.SubscriberICService.PackageModel> packages = v.Packages;
                            if (!ReferenceEquals(packages, null) && packages.Count > 0)
                            {
                                List<string> UniqCompIds = new List<string>();
                                foreach (var item1 in packages)
                                {
                                    for (int i = 0; i < item1.compList.Count; i++)
                                    {
                                        UniqCompIds.Add(item1.compList[i].componentId.ToString2());
                                    }
                                }
                                Dictionary<string, string> DcList = MasterDataCache.Instance.DataComps2GB;
                                if (DcList != null && DcList.Count > 0)
                                {
                                    foreach (var item in DcList.Values)
                                    {
                                        if (item != string.Empty && UniqCompIds.Contains(item))
                                        {
                                            return is2GBCompExistOnPkg = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {// this is for new registration flow
                List<string> dataCompIdsList = new List<string>();
                using (var proxy = new RegistrationServiceProxy())
                {
                    dataCompIdsList = proxy.GetDataPlanComponentsForMISM();
                }
                var dataCompIds = string.Empty;
                var _vas = vasComponentList.Split(',');
                foreach (string dataMISM in dataCompIdsList)
                {
                    for (var i = 0; i < _vas.Length; i++)
                    {
                        if (_vas[i] == dataMISM)
                        {
                            HttpContext.Current.Session["eligibleForMISM"] = true;
                            is2GBCompExistOnPkg = true;
                            break;
                        }
                    }
                }
            }
            // this is for contract count
            //checkContract(vasComponentList);

            return is2GBCompExistOnPkg;

        }
        #endregion

        // common method to check for iphone 6 and iphone 6 plus data booster - start
        public static int checkDataBooster(List<int> selectedComponent, string planKenanCode, string ArticleID)
        {
            int consist = 1;

            if (Properties.Settings.Default.MaxisONE_KenanCode == planKenanCode && ConfigurationManager.AppSettings["MaxisONEDevice"].ToString().Contains(ArticleID))
            {


                List<PgmBdlPckComponent> requestList = new List<PgmBdlPckComponent>();
                string kenanCode = "";
				int planId = Convert.ToInt32(planKenanCode);
                PgmBdlPckComponentFind oReq = new PgmBdlPckComponentFind();

                foreach (var compID in selectedComponent)
                {
                    if (compID.ToString() != "0")
                    {

                        PgmBdlPckComponent packageKenanCode = new PgmBdlPckComponent();
                        packageKenanCode.ID = compID;
                        oReq.PgmBdlPckComponent = packageKenanCode;
                        using (var proxy = new CatalogServiceProxy())
                        {
                            requestList = proxy.GetLnkPgmBdlPkgComp(oReq);
                            if (!requestList.IsEmpty())
                            {
                                packageKenanCode = requestList.FirstOrDefault();
                            }
                            kenanCode = packageKenanCode.KenanCode;
							string newKenanCode = string.Empty;
							var newValue = proxy.PlanDeviceValidationRulesGet(planId, ArticleID, newKenanCode);
                            if (ConfigurationManager.AppSettings["MaxisONEDataComp"].ToString().Contains(kenanCode))
                            {
                                consist = 2; 
                                break;//11032015 - Anthony - Need to add break, or the value will always 3 and throw error in Select Component page
                            }
                            else
                            {
                                consist = 3;
                            }

                        }
                    }
                }


            }
            return consist;
        }
        // common method to check for iphone 6 and iphone 6 plus data booster - end

        public static void LogToDB(string fileName, string method, string request, string response, string exception, int userId, string regId)
        {
            using (var proxy = new RegistrationServiceProxy())
            {
                proxy.LogToDB(new WebPosLog()
                {
                    exception = exception,
                    fileName = fileName,
                    method = method,
                    request = request,
                    response = response,
                    userId = userId,
                    regId = regId,
                });
            }
        }

        //Email Validation - GTM e-Billing CR - Ricky - 2014.09.25
        public static bool isValidEmail(string inputEmail)
        {
            const String pattern =
                @"^([0-9a-zA-Z]" + //Start with a digit or alphabate
                @"([\+\-_\.][0-9a-zA-Z]+)*" + // No continues or ending +-_. chars in email
                @")+" +
                @"@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";

            //Regex rgx = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", RegexOptions.IgnoreCase);
            return Regex.IsMatch(inputEmail, pattern);
        }


        // Drop 5 - CDPU Dashboard
        public static List<SelectListItem> GetList_CDPU_OrgID()
        {
            var resultList = new List<SelectListItem>();
            var list = new List<SelectListItem>();
            try
            {
                var mep_id = Util.GetList(RefType.OrgType).Where(a=>a.Text == "MEP").Select(a=>a.Value).FirstOrDefault().ToInt();
                var listOrganization = MasterDataCache.Instance.Organization;
                if (listOrganization != null && listOrganization.Organizations.Any())
                {
                    resultList = listOrganization.Organizations.Where(a => a.Active == true && a.OrgTypeID == mep_id).Select(a => new SelectListItem()
                    {
                        Text = a.Name,
                        Value = a.ID.ToString()
                    }).OrderBy(a => a.Text).ToList();
                }
                resultList.Insert(0, new SelectListItem()
                {
                    Text = "Please Select",
                    Value = ""
                });
            }
            catch (Exception ex)
            {
                string strErrorMsg = Util.LogException(ex);
                Logger.Error("Exception in Helper\\Util.cs:GetList_CDPU_OrgID(): " + "\n" + " Error:" + strErrorMsg, ex);

            }
            return resultList;
        }
        //End D5

		public static SelectListItem GetDefaultCaseCategory()
		{
			var resultList = new SelectListItem();

			try
			{
				var allLOV = MasterDataCache.Instance.GetAllLovList;
				resultList = allLOV.Where(a => a.Type == Constants.NBA_RECOMMENDATION_DEFAULT).Select(a => new SelectListItem()
				{
					Text = a.Source,
					Value = a.Value
				}).FirstOrDefault();
			}
			catch (Exception ex)
			{
				string strErrorMsg = Util.LogException(ex);
				Logger.Error("Exception in Helper\\Util.cs:GetDefaultCaseCategory(): " + "\n" + " Error:" + strErrorMsg, ex);
			}
			return resultList;
		}

        public static string getPaymentMethodName (int ID = -1)
        {
            string Payment_Cash = "Cash";
            string Payment_DirectDebit = "Direct Debit";
            string PaymentMethodName = string.Empty;
            switch (ID)
            {
                case -1:
                    {
                        var allAcctDetails = (retrieveAcctListByICResponse)HttpContext.Current.Session["AcctAllDetails"];
                        var kenanAccNumber = HttpContext.Current.Session[SessionKey.KenanACNumber.ToString2()].ToString2();
                        var isDirectDebit = WebHelper.Instance.determineDirectDebitFlag(allAcctDetails, kenanAccNumber);
                        PaymentMethodName = (isDirectDebit.ToInt() == 1) ? Payment_DirectDebit : Payment_Cash;
                        break;
                    }
                case 1: PaymentMethodName = Payment_Cash; break;
                case 2: PaymentMethodName = Payment_DirectDebit; break;
                default: break;
            }
            return PaymentMethodName;
        }

        public static Boolean isUnderAgeCustomer(string idCardNo)
        {
            if ((idCardNo != null))
            {
                int customerYear = idCardNo.Substring(0, 2).ToInt();
                int todayYears = DateTime.Now.ToString("yy").ToInt();
                customerYear = customerYear <= todayYears ? (customerYear < 10 ? ("200" + customerYear).ToInt() : ("20" + customerYear).ToInt()) : ("19" + customerYear).ToInt();
                int customerMonth = idCardNo.Substring(2, 2).ToInt();
                int customerDay = idCardNo.Substring(4, 2).ToInt();
                // get the difference in years
                int years = DateTime.Now.Year - customerYear;
                // subtract another year if we're before the
                // birth day in the current year
                if (DateTime.Now.Month < customerMonth ||
                    (DateTime.Now.Month == customerMonth &&
                    DateTime.Now.Day < customerDay))
                {
                    years--;
                }
                if (years < 18)
                {
                    return true;
                }
            }
            return false;
        }

    }

    #region SUTAN ADDED CODE 5Th March 2013
    public class ImageResize
    {
        public ImageResize()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public enum Dimensions
        {
            Width,
            Height
        }
        public enum AnchorPosition
        {
            Top,
            Center,
            Bottom,
            Left,
            Right
        }

        //It resizes the image based on the given percentage.
        public Image ScaleByPercent(Image imgPhoto, int Percent, int Resolution)
        {
            float nPercent = ((float)Percent / 100);

            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;

            int destX = 0;
            int destY = 0;
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(Resolution, Resolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        //It resizes the images in to given dimension(proportionately).
        public Image ConstrainProportions(Image imgPhoto, int Size, Dimensions Dimension, int Resolution)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;
            float nPercent = 0;

            switch (Dimension)
            {
                case Dimensions.Width:
                    nPercent = ((float)Size / (float)sourceWidth);
                    break;
                default:
                    nPercent = ((float)Size / (float)sourceHeight);
                    break;
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(Resolution, Resolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
            new Rectangle(destX, destY, destWidth, destHeight),
            new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
            GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        //It resizes the images in to given fixed size.
        public Image FixedSize(Image imgPhoto, int Width, int Height, int Resolution)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);

            //if we have to pad the height pad both the top and the bottom
            //with the difference between the scaled height and the desired height
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = (int)((Width - (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = (int)((Height - (sourceHeight * nPercent)) / 2);
            }


            //from here 
            //ram: Did modifications in WaterMark from Horizontal to Diagonal(from bottom left to top right)
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);
           
            Bitmap bmPhoto = new Bitmap(Width, Height);
			if (Resolution == 0)
				bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);
			else
				bmPhoto.SetResolution(Resolution, Resolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.White);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grPhoto.DrawImage(imgPhoto,
            new Rectangle(destX, destY, destWidth, destHeight),
            new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
            GraphicsUnit.Pixel);
            Matrix matrix = new Matrix();
            
            //WaterMark starts from here
            String watermarkText = "For Maxis use only";
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            matrix.Translate(bmPhoto.Width / 2, bmPhoto.Height / 2);
            matrix.Rotate(-40.0f);
            grPhoto.Transform = matrix;
            Font font = new Font("Verdana", 36, FontStyle.Bold, GraphicsUnit.Pixel);
            Color color = Color.FromArgb(100, 0, 0, 0); //Adds a transparent watermark with an 100 alpha value.
            SolidBrush sbrush = new SolidBrush(color);
            grPhoto.DrawString(watermarkText, font, new SolidBrush(color),0, 0,stringFormat);
           
            // over Watermark till here
            grPhoto.Dispose();
            return bmPhoto;
        }
        // Commented by Ram
        //    Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
        //    bmPhoto.SetResolution(Resolution, Resolution);

        //    Graphics grPhoto = Graphics.FromImage(bmPhoto);
        //    grPhoto.Clear(Color.White);
        //    grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

        //    grPhoto.DrawImage(imgPhoto,
        //        new Rectangle(destX, destY, destWidth, destHeight),
        //        new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
        //        GraphicsUnit.Pixel);

        //    // habs - add watermark - start
        //    String watermarkText = "For Maxis use only.";
        //    Font font = new Font("Verdana", 36, FontStyle.Bold, GraphicsUnit.Pixel);
        //    Color color = Color.FromArgb(100, 0, 0, 0); //Adds a transparent watermark with an 100 alpha value.
        //    Point pt = new Point(10,60); //The position where to draw the watermark on the image
        //    SolidBrush sbrush = new SolidBrush(color);
        //    grPhoto.DrawString(watermarkText, font, sbrush, pt);
        //    // habs - add watermark - end

        //    grPhoto.Dispose();
        //    return bmPhoto;
        //}

        //It crop the images to given width and height

        //till here commented by Ram

        public Image Crop(Image imgPhoto, int Width, int Height, AnchorPosition Anchor, int Resolution)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentW;
                switch (Anchor)
                {
                    case AnchorPosition.Top:
                        destY = 0;
                        break;
                    case AnchorPosition.Bottom:
                        destY = (int)(Height - (sourceHeight * nPercent));
                        break;
                    default:
                        destY = (int)((Height - (sourceHeight * nPercent)) / 2);
                        break;
                }
            }
            else
            {
                nPercent = nPercentH;
                switch (Anchor)
                {
                    case AnchorPosition.Left:
                        destX = 0;
                        break;
                    case AnchorPosition.Right:
                        destX = (int)(Width - (sourceWidth * nPercent));
                        break;
                    default:
                        destX = (int)((Width - (sourceWidth * nPercent)) / 2);
                        break;
                }
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(Resolution, Resolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.White);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }
    }
    #endregion SUTAN ADDED CODE 5Th March 2013

    public class HTMLPrinter
    {
        private bool documentLoaded;
        private bool documentPrinted;

        private void ie_DocumentComplete(object pDisp, ref object URL)
        {
            documentLoaded = true;
        }

        private void ie_PrintTemplateTeardown(object pDisp)
        {
            documentPrinted = true;
        }
    }

    #region Exception Handling
    /* BEGIN  Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/

    public class CustomErrorHandlerAttr : HandleErrorAttribute
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CustomErrorHandlerAttr));

        public override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            string strErrorMsg = Util.LogException(ex);
            string actionName = filterContext.Controller.ControllerContext.RouteData.Values["action"] != null ? filterContext.Controller.ControllerContext.RouteData.Values["action"].ToString() : "";
            string controllerName = filterContext.Controller.ControllerContext.RouteData.Values["controller"] != null ? filterContext.Controller.ControllerContext.RouteData.Values["controller"].ToString() : "";
            Logger.Error("Controller: " + controllerName + "\n" + " Action:" + actionName + "\n" + " Error:" + strErrorMsg, ex);
            base.OnException(filterContext);
        }

    }
    /* END Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/

    #endregion




}
