﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.DAL.Models;
using Online.Registration.Web.OrganizationSvc;

namespace Online.Registration.Web.Helper
{
    public class OrganizationServiceProxy : ServiceProxyBase<OrganizationServiceClient>
    {
        public string LastErrorMessage { get; set; }

        public OrganizationServiceProxy()
        {
            _client = new OrganizationServiceClient();
        }

        #region Organization
        public OrganizationCreateResp OrganizationCreate(Organization Organization)
        {
            var organizationCreateRq = new OrganizationCreateReq()
            {
                Organization = Organization
            };

            var resp = _client.OrganizationCreate(organizationCreateRq);

            return resp;
        }
        public void OrganizationUpdate(Organization Organization)
        {
            var OrganizationUpdateRq = new OrganizationUpdateReq()
            {
                Organization = Organization
            };

            var resp = _client.OrganizationUpdate(OrganizationUpdateRq);

        }
        public IEnumerable<int> OrganizationFind(OrganizationFind Org)
        {
            var request = new OrganizationFindReq
            {
                OrganizationFind = Org
            };

            var response = _client.OrganizationFind(request);

            return response.IDs;
        }
        public IEnumerable<Organization> OrganizationGet(IEnumerable<int> Orgs)
        {
            if (Orgs == null)
                return new Organization[] { };

            var request = new OrganizationGetReq()
            {
                IDs = Orgs.ToArray()
            };

            var response = _client.OrganizationGet(request);

            return response.Organizations;
        }

        public OrganizationGetResp OrganizationList()
        {
            
            var response = _client.OrganizationList();

            return response;
        }
        public  IEnumerable<Organization> FindOrg(OrganizationFind Org)
        {
            return OrganizationGet(OrganizationFind(Org));
        }
        public IEnumerable<Organization> OrganizationGetAll()
        {                       

            var response = _client.OrganizationGetAll();

            return response.Organizations;
        }

        #endregion

        #region Org Type

        public OrgTypeCreateResp OrgTypeCreate(OrgType orgType)
        {
            var orgTypeCreateRq = new OrgTypeCreateReq()
            {
                OrgType = orgType
            };

            var resp = _client.OrgTypeCreate(orgTypeCreateRq);
            return resp;
        }
        public IEnumerable<int> OrgTypeFind(OrgTypeFind OrgType)
        {
            var request = new OrgTypeFindReq()
            {
                OrgTypeFind = OrgType
            };

            var response = _client.OrgTypeFind(request);
            return response.IDs;
        }
        /*public IEnumerable<OrgType> OrgTypeGet(IEnumerable<int> Orgs)
        {
            if (Orgs == null)
                return new OrgType[] { };

            var request = new OrgTypeGetReq()
            {
                IDs = Orgs.ToArray()
            };

            var response = _client.OrgTypeGet(request);

            return response.OrgTypes;
        }*/
        public DAL.Models.OrgType OrgTypeGet(int OrgTypeID)
        {
            var OrgTypes = OrgTypeGet(new int[] { OrgTypeID });
            return OrgTypes == null ? null : OrgTypes.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.OrgType> OrgTypeGet(IEnumerable<int> OrgTypes)
        {
            if (OrgTypes == null)
                return new List<OrgType>();

            var request = new OrgTypeGetReq()
            {
                IDs = OrgTypes.ToArray()
            };

            var response = _client.OrgTypeGet(request);

            return response.OrgTypes;
        }
        public OrgTypeGetResp OrgTypesList()
        {
            var response = _client.OrgTypesList();
            return response;
        }

        public  IEnumerable<DAL.Models.OrgType> FindOrgType(OrgTypeFind OrgType)
        {
            return OrgTypeGet(OrgTypeFind(OrgType));
        }
        /*public static IEnumerable<OrgType> FindOrgType(OrgTypeFind Org)
        {
            using (var proxy = new OrganizationServiceProxy())
            {
                return proxy.OrgTypeGet(proxy.OrgTypeFind(Org));
            }
        }*/
        public void OrgTypeUpdate(OrgType orgType)
        {
            var orgTypeUpdateRq = new OrgTypeUpdateReq()
            {
                OrgType = orgType
            };

            var resp = _client.OrgTypeUpdate(orgTypeUpdateRq);
        }
        

        #endregion

    }
}