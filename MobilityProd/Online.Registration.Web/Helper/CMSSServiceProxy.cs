﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.CMSSSvc;
using Online.Registration.DAL.Models;

using log4net;

namespace Online.Registration.Web.Helper
{
    public class CMSSServiceProxy : ServiceProxyBase<CMSSServiceClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(CMSSServiceProxy));
        public string LastErrorMessage { get; set; }

        public CMSSServiceProxy()
        {
            _client = new CMSSServiceClient();
        }

        public bool IsLastCallFailed()
        {
            return !string.IsNullOrEmpty(LastErrorMessage);
        }

        #region Private Methods

        private void LogResponse(WCFResponse response)
        {
            LastErrorMessage = "";

           // if (!response.Success)
            if (response != null && string.IsNullOrEmpty(Convert.ToString(response.Success)))
            {
                LastErrorMessage = response.Message;

                // Log Error
                Logger.ErrorFormat("Code: {0}|Message: {1}", response.Code, response.Message);
            }
        }

        #endregion

        public static CreateInteractionCaseResponse CreateInteractionCase(CreateInteractionCaseRequest request)
        {
            using (var proxy = new CMSSServiceProxy())
            {
                var response = proxy._client.CreateInteractionCase(request);
                proxy.LogResponse(response);

                return response;
            }
        }
    }
}