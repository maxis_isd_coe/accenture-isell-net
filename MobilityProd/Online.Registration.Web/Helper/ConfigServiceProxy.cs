﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.ConfigSvc;
using Online.Registration.DAL.Models;

using log4net;

namespace Online.Registration.Web.Helper
{
    public class ConfigServiceProxy : ServiceProxyBase<ConfigServiceClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(ConfigServiceProxy));
        public string LastErrorMessage { get; set; }

        public ConfigServiceProxy()
        {
            _client = new ConfigServiceClient();
        }

        public bool IsLastCallFailed()
        {
            return !string.IsNullOrEmpty(LastErrorMessage);
        }

        #region Private Methods

        private void LogResponse(string code, string message, string stackTrace = "")
        {
            LastErrorMessage = "";

            if (!Util.IsSuccessCode(code))
            {
                LastErrorMessage = message;

                // Log Error
                Logger.ErrorFormat("Code: {0}|Message: {1}|StackTrace: {2}", code, message, stackTrace);
            }
        }

        private void LogResponse(WCFResponse response)
        {
            LogResponse(response.Code, response.Message, response.StackTrace);
        }

        #endregion

        #region Market

        public void MarketCreate(Market market)
        {
            var request = new MarketCreateReq { Market = market };
            var response = _client.MarketCreate(request);
            LogResponse(response);

            market.ID = response.ID;
        }

        public void MarketUpdate(Market market)
        {
            var request = new MarketUpdateReq { Market = market };
            var response = _client.MarketUpdate(request);
            LogResponse(response);
        }

        public IEnumerable<int> MarketFind(MarketFind findCriteria)
        {
            var request = new MarketFindReq { MarketFind = findCriteria };
            var response = _client.MarketFind(request);
            LogResponse(response);

            return response.IDs;
        }

        public IEnumerable<Market> MarketSearch(MarketFind findCriteria)
        {
            var request = new MarketFindReq { MarketFind = findCriteria };
            var response = _client.MarketSearch(request);
            LogResponse(response);

            return response.Markets;
        }
        public IEnumerable<Market> MarketGet(IEnumerable<int> ids)
        {
            var request = new MarketGetReq { IDs = ids.ToArray() };
            var response = _client.MarketGet(request);
            LogResponse(response);

            return response.Markets;
        }

        public MarketGetResp MarketList()
        {
            var response = _client.MarketList();
            LogResponse(response);
            return response;
        }

        public static void CreateMarket(Market market)
        {
            using (var proxy = new ConfigServiceProxy())
            {
                proxy.MarketCreate(market);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);
            }
        }

        public static void UpdateMarket(Market market)
        {
            using (var proxy = new ConfigServiceProxy())
            {
                proxy.MarketUpdate(market);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);
            }
        }

        public  IEnumerable<Market> FindMarket(MarketFind marketFind)
        {
            return MarketGet(MarketFind(marketFind));
        }

        #endregion

        #region AccountCategory

        public void AccountCategoryCreate(AccountCategory accountCategory)
        {
            var request = new AccountCategoryCreateReq { AccountCategory = accountCategory };
            var response = _client.AccountCategoryCreate(request);
            LogResponse(response);

            accountCategory.ID = response.ID;
        }

        public void AccountCategoryUpdate(AccountCategory accountCategory)
        {
            var request = new AccountCategoryUpdateReq { AccountCategory = accountCategory };
            var response = _client.AccountCategoryUpdate(request);
            LogResponse(response);
        }

        public IEnumerable<int> AccountCategoryFind(AccountCategoryFind findCriteria)
        {
            var request = new AccountCategoryFindReq { AccountCategoryFind = findCriteria };
            var response = _client.AccountCategoryFind(request);
            LogResponse(response);

            return response.IDs;
        }

        public IEnumerable<AccountCategory> AccountCategoryGet(IEnumerable<int> ids)
        {
            var request = new AccountCategoryGetReq { IDs = ids.ToArray() };
            var response = _client.AccountCategoryGet(request);
            LogResponse(response);

            return response.AccountCategories;
        }
        public AccountCategoryGetResp AccountCategoryList()
        {
            var response = _client.AccountCategoryList();
            LogResponse(response);
            return response;
        }

        public static void CreateAccountCategory(AccountCategory accountCategory)
        {
            using (var proxy = new ConfigServiceProxy())
            {
                proxy.AccountCategoryCreate(accountCategory);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);
            }
        }

        public static void UpdateAccountCategory(AccountCategory accountCategory)
        {
            using (var proxy = new ConfigServiceProxy())
            {
                proxy.AccountCategoryUpdate(accountCategory);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);
            }
        }

        public  IEnumerable<AccountCategory> FindAccountCategory(AccountCategoryFind accountCategoryFind)
        {
           
            return AccountCategoryGet(AccountCategoryFind(accountCategoryFind));
        }

        #endregion

        #region Status
        public StatusCreateResp StatusCreate(Status status)
        {
            var statusCreateRq = new StatusCreateReq()
            {
                Status = status
            };

            var resp = _client.StatusCreate(statusCreateRq);
            return resp;
        }
        public IEnumerable<int> StatusFind(StatusFind Status)
        {
            var request = new StatusFindReq()
            {
                StatusFind = Status
            };

            var response = _client.StatusFind(request);
            return response.IDs;
        }

        #region Added by Patanjali to add find for Store Keeper and Cashier

        public IEnumerable<int> StoreKeeperFind(StatusFind Status)
        {
            var request = new StatusFindReq()
            {
                StatusFind = Status
            };

            var response = _client.StoreKeeperFind(request);
            return response.IDs;
        }

        public IEnumerable<int> CashierFind(StatusFind Status)
        {
            var request = new StatusFindReq()
            {
                StatusFind = Status
            };

            var response = _client.CashierFind(request);
            return response.IDs;
        }

        #endregion

        public DAL.Models.Status StatusGet(int StatusID)
        {
            var Statuss = StatusGet(new int[] { StatusID });
            return Statuss == null ? null : Statuss.SingleOrDefault();
        }
       

        public IEnumerable<DAL.Models.Status> StatusGet(IEnumerable<int> Statuss)
        {
            if (Statuss == null)
                return new List<Status>(); ;

            var request = new StatusGetReq()
            {
                IDs = Statuss.ToArray()
            };
            var response = _client.StatusGet(request);
            return response.Statuss;
        }

        public StatusGetResp StatusList()
        {
            var response = _client.StatusList();
            return response;
        }

        public  IEnumerable<DAL.Models.Status> FindStatus(StatusFind Status)
        {
            return StatusGet(StatusFind(Status));
        }

        #region Added by Patanjali to add find for Store Keeper and Cashier

        public static IEnumerable<DAL.Models.Status> FindStoreKeeperStatus(StatusFind Status)
        {
            using (var proxy = new ConfigServiceProxy())
            {
                return proxy.StatusGet(proxy.StoreKeeperFind(Status));
            }
        }

        public static IEnumerable<DAL.Models.Status> FindCashierStatus(StatusFind Status)
        {
            using (var proxy = new ConfigServiceProxy())
            {
                return proxy.StatusGet(proxy.CashierFind(Status));
            }
        }
        #endregion

        public void StatusUpdate(Status status)
        {
            var statusUpdateRq = new StatusUpdateReq()
            {
                Status = status
            };

            var resp = _client.StatusUpdate(statusUpdateRq);
        }
        #endregion

        #region StatusType
        public StatusTypeCreateResp StatusTypeCreate(StatusType statusType)
        {
            var statusTypeCreateRq = new StatusTypeCreateReq()
            {
                StatusType = statusType
            };

            var resp = _client.StatusTypeCreate(statusTypeCreateRq);
            return resp;
        }

        public IEnumerable<int> StatusTypeFind(StatusTypeFind StatusType)
        {
            var request = new StatusTypeFindReq()
            {
                StatusTypeFind = StatusType
            };

            var response = _client.StatusTypeFind(request);
            return response.IDs;
        }

        public DAL.Models.StatusType StatusTypeGet(int StatusTypeID)
        {
            var StatusTypes = StatusTypeGet(new int[] { StatusTypeID });
            return StatusTypes == null ? null : StatusTypes.SingleOrDefault();
        }

        public IEnumerable<DAL.Models.StatusType> StatusTypeGet(IEnumerable<int> StatusTypes)
        {
            if (StatusTypes == null)
                return new List<StatusType>(); ;

            var request = new StatusTypeGetReq()
            {
                IDs = StatusTypes.ToArray()
            };

            var response = _client.StatusTypeGet(request);

            return response.StatusTypes;
        }

        public StatusTypeGetResp StatusTypeList()
        {
            var response = _client.StatusTypeList();
            return response;
        }

        public  IEnumerable<DAL.Models.StatusType> FindStatusType(StatusTypeFind StatusType)
        {
            return StatusTypeGet(StatusTypeFind(StatusType));
        }
        public void StatusTypeUpdate(StatusType statusType)
        {
            var statusTypeUpdateRq = new StatusTypeUpdateReq()
            {
                StatusType = statusType
            };

            var resp = _client.StatusTypeUpdate(statusTypeUpdateRq);
        }
        #endregion

        #region StatusChange
        public void StatusChangeCreate(StatusChange StatusChange)
        {
            var StatusChangeCreateRq = new StatusChangeCreateReq()
            {
                StatusChange = StatusChange
            };

            var resp = _client.StatusChangeCreate(StatusChangeCreateRq);
        }
        public void StatusChangeUpdate(StatusChange StatusChange)
        {
            var statusChangeUpdateRq = new StatusChangeUpdateReq()
            {
                StatusChange = StatusChange
            };

            var resp = _client.StatusChangeUpdate(statusChangeUpdateRq);
        }
        public IEnumerable<int> StatusChangeFind(StatusChangeFind StatusChange)
        {
            var request = new StatusChangeFindReq()
            {
                StatusChangeFind = StatusChange
            };

            var response = _client.StatusChangeFind(request);
            return response.IDs;
        }
        public StatusChange StatusChangeGet(int StatusChangeID)
        {
            var StatusChanges = StatusChangeGet(new int[] { StatusChangeID });
            return StatusChanges == null ? null : StatusChanges.SingleOrDefault();
        }
        public IEnumerable<StatusChange> StatusChangeGet(IEnumerable<int> StatusChanges)
        {
            if (StatusChanges == null)
                return new List<StatusChange>();

            var request = new StatusChangeGetReq()
            {
                IDs = StatusChanges.ToArray()
            };

            var response = _client.StatusChangeGet(request);

            return response.StatusChanges;
        }
        public static IEnumerable<StatusChange> FindStatusChange(StatusChangeFind StatusChange)
        {
            using (var proxy = new ConfigServiceProxy())
            {
                return proxy.StatusChangeGet(proxy.StatusChangeFind(StatusChange));
            }
        }
        #endregion

        #region StatusReason
        public StatusReasonCreateResp StatusReasonCreate(StatusReason statusReason)
        {
            var statusReasonCreateRq = new StatusReasonCreateReq()
            {
                StatusReason = statusReason
            };

            var resp = _client.StatusReasonCreate(statusReasonCreateRq);
            return resp;
        }
        public IEnumerable<int> StatusReasonFind(StatusReasonFind StatusReason)
        {
            var request = new StatusReasonFindReq()
            {
                StatusReasonFind = StatusReason
            };

            var response = _client.StatusReasonFind(request);
            return response.IDs;
        }
        public StatusReason StatusReasonGet(int StatusReasonID)
        {
            var StatusReasons = StatusReasonGet(new int[] { StatusReasonID });
            return StatusReasons == null ? null : StatusReasons.SingleOrDefault();
        }
        public IEnumerable<StatusReason> StatusReasonGet(IEnumerable<int> StatusReasons)
        {
            if (StatusReasons == null)
                return new List<StatusReason>();

            var request = new StatusReasonGetReq()
            {
                IDs = StatusReasons.ToArray()
            };

            var response = _client.StatusReasonGet(request);

            return response.StatusReasons;
        }

        public StatusReasonGetResp StatusReasonList()
        {
            var response = _client.StatusReasonList();
            return response;
        }

        public  IEnumerable<StatusReason> FindStatusReason(StatusReasonFind StatusReason)
        {
            return StatusReasonGet(StatusReasonFind(StatusReason));
        }
        public void StatusReasonUpdate(StatusReason statusReason)
        {
            var statusReasonUpdateRq = new StatusReasonUpdateReq()
            {
                StatusReason = statusReason
            };

            var resp = _client.StatusReasonUpdate(statusReasonUpdateRq);
        }
        #endregion

        #region StatusReasonCode
        public void StatusReasonCodeCreate(StatusReasonCode statusReasonCode)
        {
            var StatusReasonCodeCreateRq = new StatusReasonCodeCreateReq()
            {
                StatusReasonCode = statusReasonCode
            };

            var resp = _client.StatusReasonCodeCreate(StatusReasonCodeCreateRq);
        }
        public void StatusReasonCodeUpdate(StatusReasonCode statusReasonCode)
        {
            var StatusReasonCodeUpdateRq = new StatusReasonCodeUpdateReq()
            {
                StatusReasonCode = statusReasonCode
            };
            var resp = _client.StatusReasonCodeUpdate(StatusReasonCodeUpdateRq);
        }
        public IEnumerable<int> StatusReasonCodeFind(StatusReasonCodeFind StatusReasonCode)
        {
            var request = new StatusReasonCodeFindReq()
            {
                StatusReasonCodeFind = StatusReasonCode
            };

            var response = _client.StatusReasonCodeFind(request);
            return response.IDs;
        }
        public StatusReasonCode StatusReasonCodeGet(int StatusReasonCodeID)
        {
            var StatusReasonCodes = StatusReasonCodeGet(new int[] { StatusReasonCodeID });
            return StatusReasonCodes == null ? null : StatusReasonCodes.SingleOrDefault();
        }
        public IEnumerable<StatusReasonCode> StatusReasonCodeGet(IEnumerable<int> StatusReasonCodes)
        {
            if (StatusReasonCodes == null)
                return new List<StatusReasonCode>();

            var request = new StatusReasonCodeGetReq()
            {
                IDs = StatusReasonCodes.ToArray()
            };

            var response = _client.StatusReasonCodeGet(request);

            return response.StatusReasonCodes;
        }
        public static IEnumerable<StatusReasonCode> FindStatusReasonCode(StatusReasonCodeFind StatusReasonCode)
        {
            using (var proxy = new ConfigServiceProxy())
            {
                return proxy.StatusReasonCodeGet(proxy.StatusReasonCodeFind(StatusReasonCode));
            }
        }
        public IEnumerable<int> StatusReasonCodeIDsFind()
        {
            var response = _client.StatusReasonCodeIDsFind();
            return response.IDs;
        }
        public static IEnumerable<int> FindStatusReasonCodeIDs()
        {
            using (var proxy = new ConfigServiceProxy())
            {
                return proxy.StatusReasonCodeIDsFind();
            }
        }
        #endregion

        #region BRE Check

        public List<BRECheck> GetBRECheckDetails(List<int> userGroupId)
        {
           return  _client.GetBRECheckDetails(userGroupId.ToArray()).ToList();
        }
        

        #endregion
    }
}
