﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//using Online.Registration.Web.SmartKenanService;
using Online.Registration.Web.KenanSvc;

using log4net;

namespace Online.Registration.Web.Helper
{
    public class SmartKenanServiceProxy : ServiceProxyBase<KenanSvc.KenanServiceClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(KenanServiceProxy));
        public string LastErrorMessage { get; set; }

        public SmartKenanServiceProxy()
        {
            _client = new KenanSvc.KenanServiceClient();
        }

        #region Private Methods

        private void LogResponse(WCFResponse response)
        {
            LastErrorMessage = "";

            if (!response.Success)
            {
                LastErrorMessage = response.Message;

                // Log Error
                Logger.ErrorFormat("Code: {0}|Message: {1}", response.Code, response.Message);
            }
        }

        #endregion

        public RetrieveBillingDetailsResponse RetrieveBillingDetails(RetrieveBillingDetailsRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.RetrieveBillingDetails(request);
            LogResponse(response);

            return response;
        }

        public RetrieveComponentInfoResponse RetrieveComponentInfo(RetrieveComponentInfoRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.RetrieveComponentInfo(request);
            LogResponse(response);

            return response;
        }

        //public BlacklistCheckResponse BlacklistCheck(BlacklistCheckRequest request)
        //{
        //    if (request == null)
        //        throw new ArgumentNullException();

        //    var response = _client.BlacklistCheck(request);
        //    LogResponse(response);

        //    return response;
        //}

        public RetrieveExtAccDetailsResponse RetrieveExternalAccount(RetrieveExtAccDetailsRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();
            Logger.DebugFormat("RetrieveExternalAccount:before");
            var response = _client.RetrieveExtAccountDetails(request);
            Logger.DebugFormat("RetrieveExternalAccount:after");
            LogResponse(response);

            return response;
        }

        public MobileNoSelectionResponse MobileNoSelection(MobileNoSelectionRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.MobileNoSelection(request);
            LogResponse(response);

            return response;
        }

        public MobileNoUpdateResponse MobileNoUpdate(MobileNoUpdateRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.MobileNoUpdate(request);
            LogResponse(response);

            return response;
        }

        public PostcodeValidationResponse PostcodeValidation(PostcodeValidationRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.PostcodeValidation(request);
            LogResponse(response);

            return response;
        }

        //public OrderCreationResponse KenanAccountCreate(OrderCreationRequest request)
        public bool KenanAccountCreate(OrderCreationRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.KenanAccountCreate(request);
            //LogResponse(response);

            return response;
        }

        public bool KenanAccountFulfill(OrderFulfillRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.KenanAccountFulfill(request);
            //LogResponse(response);

            return response;
        }

        public UpdateOrderStatusResponse UpdateKenanOrderStatus(UpdateOrderStatusRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.UpdateOrderStatus(request);

            LogResponse(new WCFResponse()
            {
                Code = response.MsgCode,
                Message = response.MsgDesc
            });

            return response;
        }

        public BusinessRuleResponse BusinessRuleCheck(BusinessRuleRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.BusinessRuleCheck(request);
            LogResponse(response);

            return response;
        }

        public BusinessRuleResponse BusinessRuleCheckNew(BusinessRuleRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.BusinessRuleCheckNew(request);
            LogResponse(response);

            return response;
        }

        #region SUTAN ADDED CODE 24rthFeb2013
        /// <summary>
        /// Kenans addition line registration.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public KenanTypeAdditionLineRegistrationResponse KenanAdditionLineRegistration(KenanTypeAdditionLineRegistration request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.KenanAdditionLineRegistration(request);
            LogResponse(response);

            return response;
        }

        /// <summary>
        /// Kenans Add new supp line.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public KenanNewSuppLineResponse KenanAddNewSuppLine(KenanNewSuppLineRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.KenanAddNewSuppLine(request);
            LogResponse(response);

            return response;
        }
        #endregion SUTAN ADDED CODE 24rthFeb2013

        /// <summary>
        /// Kenan createCenterOrderMNP
        /// </summary>
        /// <param name="request"></param>
        /// <returns>TypeOf(MNPOrderCreationResponse)</returns>
        public MNPOrderCreationResponse MNPcreateCenterOrderMNP(OrderCreationRequestMNP request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.MNPcreateCenterOrderMNP(request);
            LogResponse(response);

            return response;
        }

        

        public NewRetrieveComponentInfoResponse NewRetrieveComponentInfo(NewRetrieveComponentInfoRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.NewRetrieveComponentInfo(request);
            LogResponse(response);

            return response;
        }

        public bool validateInventoryExtIdService(TypeValidateInventoryExtIdRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.InventoryCheck(request);
            //LogResponse(response);

            return response;
        }

        public bool CreateIMPOSFile(int RegID)
        {
            bool isCreatedIMPOSFile = false;

            if (RegID > 0)
                isCreatedIMPOSFile = _client.saveFile(RegID, "P");
            return isCreatedIMPOSFile;
        }
        public bool CenterOrderContractCreation(CenterOrderContractRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.CenterOrderContractCreation(request);
            //LogResponse(response);

            return response;
        }
    }


}