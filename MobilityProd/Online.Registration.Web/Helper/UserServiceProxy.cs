﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.UserSvc;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;

using log4net;

namespace Online.Registration.Web.Helper
{
    public class UserServiceProxy : ServiceProxyBase<UserServiceClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(UserServiceProxy));
        public string LastErrorMessage { get; set; }

        public UserServiceProxy()
        {
            _client = new UserServiceClient();
        }

        public bool IsLastCallFailed()
        {
            return !string.IsNullOrEmpty(LastErrorMessage);
        }

        #region Private Methods

        private void LogResponse(string code, string message, string stackTrace = "")
        {
            LastErrorMessage = "";

            if (!Util.IsSuccessCode(code))
            {
                LastErrorMessage = message;

                // Log Error
                Logger.ErrorFormat("Code: {0}|Message: {1}|StackTrace: {2}", code, message, stackTrace);
            }
        }

        private void LogResponse(WCFResponse response)
        {
            LogResponse(response.Code, response.Message, response.StackTrace);
        }

        #endregion

        #region Access
        public IEnumerable<int> FindAccess(Access access)
        {
            var request = new AccessFindReq()
            {
                AccessFind = new AccessFind()
                {
                     Access = access
                }
            };

            var response = _client.AccessFind(request);

            return response.IDs;
        }

        public IEnumerable<Access> GetAccess(IEnumerable<int> accessIDs)
        {
            if (accessIDs == null)
                return new Access[] { };

            var request = new AccessGetReq()
            {
                IDs = accessIDs.ToArray()
            };

            var response = _client.AccessGet(request);

            return response.Accesss;
        }

        public static Access GetAccess(string username)
        {
            using (var proxy = new UserServiceProxy())
            {
                return proxy.GetAccessByUsername(username);
            }
        }

        public static int GetUserIdByName(string username)
        {
            using (var proxy = new UserServiceProxy())
            {
                return proxy.GetIDByUsername(username);
            }
        }

        public Access GetAccess(int id)
        {
            if (id == 0)
                return null;

            return GetAccess(new int[] { id }).SingleOrDefault();
        }

        public Access GetAccessByUsername(string username, string password = "", bool active = true)
        {
            var findCriteria = new Access()
            {
                LastAccessID = "",
                UserName = username,
                Password = password,
                Active = active
            };

            var access = FindAccess(findCriteria);
            var accessID = 0;

            if (access != null && access.Count() > 0)
                accessID = access.SingleOrDefault();

            return GetAccess(accessID);
        }

        public int GetIDByUsername(string username, string password = "", bool active = true)
        {
            var UserID = UserFindbyName(username);
            var ID = 0;

            if (UserID != null && UserID.Count() > 0)
                ID = UserID.SingleOrDefault();

            return ID;
        }

        public int CreateAccess(Access access)
        {
            var response = _client.AccessCreate(new AccessCreateReq() { Access = access });

            if (Util.IsSuccessCode(response.Code))
                return response.ID;

            // log fail message/stacktrace

            return 0;
        }

        public bool UpdateAccess(Access access)
        {
            if (access == null)
                throw new ArgumentNullException("Argument access is not allow null!");

            var response = _client.AccessUpdate(new AccessUpdateReq() { Access = access });

            if (Util.IsSuccessCode(response.Code))
                return true;

            // TODO: log fail message/stacktrace

            return false;
        }

        public bool AccessPasswordUpdate(Access access)
        {
            if (access == null)
                throw new ArgumentNullException("Argument access is not allow null!");

            var response = _client.AccessPasswordUpdate(new AccessUpdateReq() { Access = access });
            LogResponse(response);

            if (Util.IsSuccessCode(response.Code))
                return true;

            return false;
        }

        public static bool UpdateAccessPassword(Access access)
        {
            using (var proxy = new UserServiceProxy())
            {
                return proxy.AccessPasswordUpdate(access);
            }
        }

        public static void AddAccessFailedPasswordAttempt(int accessID)
        {
            using (var proxy = new UserServiceProxy())
            {
                proxy.AccessFailedPasswordAttemptAdd(accessID);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);
            }
        }

        public static void ResetAccessFailedPasswordAttempt(int accessID)
        {
            using (var proxy = new UserServiceProxy())
            {
                proxy.AccessFailedPasswordAttemptReset(accessID);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);
            }
        }

        public static void LockedOutAccess(int accessID)
        {
            using (var proxy = new UserServiceProxy())
            {
                proxy.AccessLockedOut(accessID);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);
            }
        }

        public static void UnlockAccess(int accessID, string lastAccessID)
        {
            using (var proxy = new UserServiceProxy())
            {
                proxy.AccessUnlock(accessID, lastAccessID);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);
            }
        }

        public void AccessFailedPasswordAttemptAdd(int accessID)
        {
            var response = _client.AccessFailedPasswordAttemptAdd(accessID);
            LogResponse(response);
        }

        public void AccessFailedPasswordAttemptReset(int accessID)
        {
            var response = _client.AccessFailedPasswordAttemptReset(accessID);
            LogResponse(response);
        }

        public void AccessLockedOut(int accessID)
        {
            var response = _client.AccessLockedOut(accessID);
            LogResponse(response);
        }

        public void AccessUnlock(int accessID, string lastAccessID)
        {
            var response = _client.AccessUnlock(accessID, lastAccessID);
            LogResponse(response);
        }

        public IEnumerable<DAL.Models.Access> AccessGet(IEnumerable<int> Accesses)
        {
            if (Accesses == null)
                return new List<Access>();

            var request = new AccessGetReq()
            {
                IDs = Accesses.ToArray()
            };

            var response = _client.AccessGet(request);

            return response.Accesss;
        }

        public AccessGetResp AccessList()
        {

            var response = _client.AccessList();

            return response;
        }

        public  IEnumerable<DAL.Models.Access> AccessFind(Access access)
        {
            return GetAccess(FindAccess(access));
        }

        #endregion

        #region User

        public User UserGet(int userID)
        {
            var users = UserGet(new int[] { userID });
            return users == null ? null : users.SingleOrDefault();
        }
        public IEnumerable<int> UserFindbyName(string username, bool? active = true)
        {
            var request = new UserFindbyNameReq()
            {
                UserFindbyName = new UserFindbyName()
                {
                    UserName = username,
                     Active = active
                } 
            };

            var response = _client.UserFindbyName(request);

            return response.IDs;
        }
        public IEnumerable<int> UserFind(User user, bool? active)
        {
            var request = new UserFindReq()
            {
                UserFind = new UserFind()
                {
                     User = user,
                     Active = active
                } 
            };

            var response = _client.UserFind(request);

            return response.IDs;
        }
        public IEnumerable<User> UserGet(IEnumerable<int> userIDs)
        {
            if (userIDs == null)
                return new User[] { };

            var request = new UserGetReq()
            {
                IDs = userIDs.ToArray()
            };

            var response = _client.UserGet(request);
            LogResponse(response);

            return response.Users;
        }

        public UserGetResp UsersList()
        {
            var response = _client.UsersList();
            LogResponse(response);

            return response;
        }

        public static IEnumerable<User> GetUsers(IEnumerable<int> userIDs)
        {
            using (var proxy = new UserServiceProxy())
            {
                var users = proxy.UserGet(userIDs);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);

                return users;
            }
        }
        public  IEnumerable<User> FindUser(User user, bool? active)
        {
            return UserGet(UserFind(user, active));
        }
        public static User GetUser(int userID)
        {
            using (var proxy = new UserServiceProxy())
            {
                var user = proxy.UserGet(userID);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);

                return user;
            }
        }
        public void UserUpdate(User user)
        {
            var userUpdateRq = new UserUpdateReq()
            {
                User = user
            };

            var resp = _client.UserUpdate(userUpdateRq);
        }
        public UserCreateResp UserCreate(User user)
        {
            var userCreateRq = new UserCreateReq()
            {
                User = user
            };

            var resp = _client.UserCreate(userCreateRq);
            return resp;
        }
        #endregion

        #region User Group

        public UserGroupCreateResp UserGroupCreate(UserGroup userGroup)
        {
            var userGroupCreateRq = new UserGroupCreateReq()
            {
                UserGroup = userGroup
            };

            var resp = _client.UserGroupCreate(userGroupCreateRq);
            return resp;
        }
        public IEnumerable<int> UserGroupFind(UserGroupFind userGroup)
        {
            var request = new UserGroupFindReq()
            {
                UserGroupFind = userGroup
            };

            var response = _client.UserGroupFind(request);
            return response.IDs;
        }
        public void UserGroupUpdate(UserGroup userGroup)
        {
            var userGroupUpdateRq = new UserGroupUpdateReq()
            {
                UserGroup = userGroup
            };

            var resp = _client.UserGroupUpdate(userGroupUpdateRq);
        }
        public DAL.Models.UserGroup UserGroupGet(int userGroupID)
        {
            var userGroup = UserGroupGet(new int[] { userGroupID });
            return userGroup == null ? null : userGroup.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.UserGroup> UserGroupGet(IEnumerable<int> UserGroup)
        {
            if (UserGroup == null)
                return new List<UserGroup>();

            var request = new UserGroupGetReq()
            {
                IDs = UserGroup.ToArray()
            };

            var response = _client.UserGroupGet(request);

            return response.UserGroups;
        }

        public UserGroupGetResp UserGroupList()
        {
            var response = _client.UserGroupList();
            return response;
        }

        public  IEnumerable<DAL.Models.UserGroup> FindUserGroup(UserGroupFind userGroup)
        {
            return UserGroupGet(UserGroupFind(userGroup));
        }
    #endregion

        #region User Group Access
        public void UserGroupAccessCreate(UserGrpAccess UserGroupAccess)
        {
            var userGroupAccessCreateRq = new UserGrpAccessCreateReq()
            {
                UserGrpAccess = UserGroupAccess
            };

            var resp = _client.UserGrpAccessCreate(userGroupAccessCreateRq);
        }
        public void UserGroupAccessUpdate(UserGrpAccess UserGroupAccess)
        {
            var userGroupAccessUpdateRq = new UserGrpAccessUpdateReq()
            {
                UserGrpAccess = UserGroupAccess
            };

            var resp = _client.UserGrpAccessUpdate(userGroupAccessUpdateRq);
        }
        public IEnumerable<int> UserGroupAccessFind(UserGrpAccessFind userGrpAccess)
        {
            var request = new UserGrpAccessFindReq()
            {
                UserGrpAccessFind = userGrpAccess
            };

            var response = _client.UserGrpAccessFind(request);
            return response.IDs;
        }
        public DAL.Models.UserGrpAccess UserGroupAccessGet(int userGroupAccessID)
        {
            var userGroupAccess = UserGroupAccessGet(new int[] { userGroupAccessID });
            return userGroupAccess == null ? null : userGroupAccess.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.UserGrpAccess> UserGroupAccessGet(IEnumerable<int> UserGroupAccess)
        {
            if (UserGroupAccess == null)
                return new List<UserGrpAccess>();

            var request = new UserGrpAccessGetReq()
            {
                IDs = UserGroupAccess.ToArray()
            };

            var response = _client.UserGrpAccessGet(request);

            return response.UserGrpAccesss;
        }
        #endregion

        #region User Group Role
        public void UserGroupRoleCreate(UserGrpRole userGrpRole)
        {
            var userGroupRoleCreateRq = new UserGrpRoleCreateReq()
            {
                UserGrpRole = userGrpRole
            };

            var resp = _client.UserGrpRoleCreate(userGroupRoleCreateRq);
        }
        public void UserGroupRoleUpdate(UserGrpRole userGrpRole)
        {
            var userGroupRoleUpdateRq = new UserGrpRoleUpdateReq()
            {
                UserGrpRole = userGrpRole
            };

            var resp = _client.UserGrpRoleUpdate(userGroupRoleUpdateRq);
        }
        public IEnumerable<int> UserGroupRoleFind(UserGrpRoleFind userGrpRoleFind)
        {
            var request = new UserGrpRoleFindReq()
            {
                UserGrpRoleFind = userGrpRoleFind
            };

            var response = _client.UserGrpRoleFind(request);
            return response.IDs;
        }
        public DAL.Models.UserGrpRole UserGroupRoleGet(int userGroupRoleID)
        {
            var userGroupRole = UserGroupRoleGet(new int[] { userGroupRoleID });
            return userGroupRole == null ? null : userGroupRole.SingleOrDefault();
        }
        public IEnumerable<DAL.Models.UserGrpRole> UserGroupRoleGet(IEnumerable<int> UserGroupRole)
        {
            if (UserGroupRole == null)
                return new List<UserGrpRole>();

            var request = new UserGrpRoleGetReq()
            {
                IDs = UserGroupRole.ToArray()
            };

            var response = _client.UserGrpRoleGet(request);

            return response.UserGrpRoles;
        }
        #endregion

        #region UserRole

        public UserRole UserRoleGet(int UserRoleID)
        {
            var UserRoles = UserRoleGet(new int[] { UserRoleID });
            return UserRoles == null ? null : UserRoles.SingleOrDefault();
        }
        public IEnumerable<int> UserRoleFind(UserRole UserRole, bool? active)
        {
            var request = new UserRoleFindReq()
            {
                UserRoleFind = new UserRoleFind()
                {
                    UserRole = UserRole,
                    Active = active
                }
            };

            var response = _client.UserRoleFind(request);

            return response.IDs;
        }
        public IEnumerable<UserRole> UserRoleGet(IEnumerable<int> UserRoleIDs)
        {
            if (UserRoleIDs == null)
                return new UserRole[] { };

            var request = new UserRoleGetReq()
            {
                IDs = UserRoleIDs.ToArray()
            };

            var response = _client.UserRoleGet(request);
            LogResponse(response);

            return response.UserRoles;
        }

        public UserRoleGetResp UserRolesList()
        {

            var response = _client.UserRolesList();

            return response;
        }

        public static IEnumerable<UserRole> GetUserRoles(IEnumerable<int> UserRoleIDs)
        {
            using (var proxy = new UserServiceProxy())
            {
                var UserRoles = proxy.UserRoleGet(UserRoleIDs);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);

                return UserRoles;
            }
        }
        public  IEnumerable<UserRole> FindUserRole(UserRole UserRole, bool? active)
        {
            return UserRoleGet(UserRoleFind(UserRole, active));
        }
        public static UserRole GetUserRole(int UserRoleID)
        {
            using (var proxy = new UserServiceProxy())
            {
                var UserRole = proxy.UserRoleGet(UserRoleID);
                if (proxy.IsLastCallFailed())
                    throw new ApplicationException(proxy.LastErrorMessage);

                return UserRole;
            }
        }
        public void UserRoleUpdate(UserRole userRole)
        {
            var userRoleUpdateRq = new UserRoleUpdateReq()
            {
                UserRole = userRole
            };

            var resp = _client.UserRoleUpdate(userRoleUpdateRq);
        }
        public UserRoleCreateResp UserRoleCreate(UserRole userRole)
        {
            var userRoleCreateRq = new UserRoleCreateReq()
            {
                UserRole = userRole
            };

            var resp = _client.UserRoleCreate(userRoleCreateRq);
            return resp;
        }
        #endregion

        public UsersInRoleFindResp UsersInRoleFind(UsersInRoleFindReq req)
        {
            var response = _client.UsersInRoleFind(req);

            return response;
        }
        public RolesForUserGetResp GetRolesForUser(RolesForUserGetReq req)
        {
            var response = _client.RolesForUserGet(req);

            return response;
        }

        #region apistatusandmessages
        /*  This code is writeen by VLTECH */
        public GetAPIStatusMessagesResp GetAPIStatusMessages(GetAPIStatusMessagesReq oReq)
        {
            var GetAPIStatusMessagesReqval = new GetAPIStatusMessagesReq()
            {
                tblAPIStatusMessagesval=oReq.tblAPIStatusMessagesval
            };

            var resp = _client.GetAPIStatusMessages(GetAPIStatusMessagesReqval);

            return resp;
        }
        /*  This code is writeen by VLTECH */
        #endregion

        #region VLT ADDED CODE
        /// <summary>
        /// Save API call status.
        /// </summary>
        /// <param name="oReq">The o req.</param>
        /// <returns>Int</returns>
        public APISaveCallStatusResp APISaveCallStatus(APISaveCallStatusReq oReq)
        {
            var APISaveCallStatusReqval = new APISaveCallStatusReq()
            {
                tblAPICallStatusMessagesval=oReq.tblAPICallStatusMessagesval                
            };

            var resp = _client.APISaveCallStatus(APISaveCallStatusReqval);

            return resp;
        }
        /// <summary>
        /// Saves API call status.
        /// </summary>
        /// <param name="oReq">The o req.</param>
        /// <returns></returns>
        public APISaveCallStatusResp APIUpdateCallStatus(APISaveCallStatusReq oReq)
        {
            var APISaveCallStatusReqval = new APISaveCallStatusReq()
            {
                tblAPICallStatusMessagesval = oReq.tblAPICallStatusMessagesval
            };

            var resp = _client.APIUpdateCallStatus(APISaveCallStatusReqval);

            return resp;
        }

        public PlanSearchItemResp PlanMaxStatus(string searchItem)
        {
            var PlanMaxval = new PlanSearchItemReq()
            {
                planSearchVal = searchItem
            };

            var resp = _client.PlanSearchItemMaxRange(PlanMaxval);

            return resp;
        }

        public PlanSearchItemResp PlanMinStatus(string searchItem)
        {
            var PlanMinval = new PlanSearchItemReq()
            {
                planSearchVal = searchItem
            };

            var resp = _client.PlanSearchItemMinRange(PlanMinval);

            return resp;
        }
        #endregion VLT ADDED CODE

        #region VLT ADDED CODE by Rajeswari on Feb25th for logging user info
        public UserTransactionLogResp UserTransactionLogCreate(UserTransactionLogReq oReq)
        {
            var userInfolog = new UserTransactionLogReq()
            {
                UserInfoVal = oReq.UserInfoVal
            };

            var resp = _client.UserLogInfo(userInfolog);
            return resp;
        }
        #endregion

        public bool MocICcheckwithwhitelistcustomers(string userName, string strIcno)
        {
            var response = _client.MocICcheckwithwhitelistcustomers(userName, strIcno);

            return response;
        }
        /// <summary>
        /// Returns the available donors
        /// </summary>
        /// <returns>TypeOf(GetDonerMessagesResp)</returns>
        public GetDonerMessagesResp GetDonors()
        {
            var response = _client.GetDonors();
            return response;
        }

        public int GetPPIDCountSearch(string userName,string action)
        {
            return _client.GetPPIDCountSearch(userName, action);
        }
        public List<UserConfigurations> GetUserConfigurations()
        {
            return _client.GetUserConfigurations().ToList();
        }

        public int SaveUserAuditLog(string userName, string sessionId, string ipAddress, int loggedOut)
        {
            var logId = _client.SaveUserAuditLog(userName, sessionId, ipAddress, loggedOut);
            return logId;
        }

        public int SaveUserTransactionLog(int usrAuditLogId, int moduleId, string actionDone, string accountNo, string dealerNo)
        {
            var transtionId = _client.SaveUserTransactionLog(usrAuditLogId, moduleId, actionDone, accountNo, dealerNo);
            return transtionId;
        }
    }
}