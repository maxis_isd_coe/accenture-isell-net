﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;
using Online.Registration.DAL.Models;
using Online.Registration.Web.UpdateSvc;
using Online.Registration.DAL;
using System.Xml;

using SNT.Utility;

namespace Online.Registration.Web.Helper
{
	public class UpdateServiceProxy : ServiceProxyBase<UpdateServiceClient>
	{
		public string LastErrorMessage { get; set; }

		public UpdateServiceProxy()
        {
			_client = new UpdateServiceClient();
        }

		public int SaveRegMdlGrpModel(RegMdlGrpModel oReq)
		{
			var response = _client.saveRegMdlGrpModel(oReq);
			return response;
		}

		public int SaveDocument(RegUploadDoc oReq)
		{
			RegUploadDoc _reqUpload = new RegUploadDoc();

			_reqUpload.FileName = System.Text.RegularExpressions.Regex.Replace(oReq.FileName, "[^a-zA-Z0-9.-]", "") ;
			_reqUpload.FileType = oReq.FileType;
			_reqUpload.RegID = oReq.RegID;
			_reqUpload.FileStream = oReq.FileStream;

			var response = _client.uploadDocs(_reqUpload);
			return response;
		}

		public void CustomerUpdate(DAL.Models.Customer req)
		{
			var regUpdateRq = new CustomerUpdateReq()
			{
				Customer = req
			};

			var resp = _client.updateTblCustomer(regUpdateRq);

		}

		public int SavePhotoToDB(string photoData, string custPhoto, string altCustPhoto, int regid)
		{
			var resp = _client.SavePhotoToDB(photoData,custPhoto,altCustPhoto, regid);

			return resp;
		}

		// this is to find if Contract has uploaded
		// false means the document is not there, and need to be generated & uploaded
		// true  means the document is there, so no need to generate the file and upload the file
		public bool FindDocument(string criteria, int regID)
		{
			bool resp = false;

			// if below flag is true, means this method can be used by rollout Store.
			// if false, means this mehtod should not be used by rollout store, and return always TRUE
			var rollOutCheck = Util.rollOutCheck();
		
			if (rollOutCheck)
			{
				resp = _client.FindDocument(criteria, regID);
			}
			else
			{
				resp = true;
			}

			return resp;
		}

		public List<RegUploadDoc> GetAllDocumentFromDB(int regid)
		{
			List<RegUploadDoc> resp = new List<RegUploadDoc>();
			try
			{
				resp = _client.GetAllDocument(regid).ToList();
			}
			catch (Exception ex)
			{
				return new List<RegUploadDoc>();
			}

			return resp;
		}
		

		//public int SaveRegMdlGrpModels(List<RegMdlGrpModel> oReq)
		//{
		//    var response = _client.saveRegMdlGrpModels()

		//    return response;
		//}

	}
}
