﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.DynamicServiceSvc;
using Online.Registration.Web.ContentRepository;
using log4net;


namespace Online.Registration.Web.Helper
{
    public class DynamicServiceProxy : ServiceProxyBase<DynamicServiceClient>
    {
        #region Member Declaration
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DynamicServiceProxy));
        #endregion
        public DynamicServiceProxy()
        {
            _client = new DynamicServiceClient();
          
        }
        public int GetStockCount(string articleId, string wsdlUrl,int storeId)
        {
            int count = _client.GetStockCount(articleId, wsdlUrl,storeId);

            

            try
            {
                var allOrgStocks = MasterDataCache.Instance.OrganizationStocks;

                if (allOrgStocks != null && allOrgStocks.ContainsKey(wsdlUrl) && allOrgStocks[wsdlUrl] != null && allOrgStocks[wsdlUrl].ContainsKey(articleId) && allOrgStocks[wsdlUrl][articleId] != 0)
                {
                    Logger.Info("OrganizationStocks are being updated for " + articleId + " & " + wsdlUrl);

                    HttpContext.Current.Application.Lock();



                    allOrgStocks[wsdlUrl][articleId] = count;


                    Logger.Info("OrganizationStocks update is done for " + articleId + " & " + wsdlUrl);
                }

                return count;
            }
            finally
            {

                HttpContext.Current.Application.UnLock();


            }

        }

        public Dictionary<string, int> GetStockCounts(List<string> articleId, string wsdlUrl,int storeId)
        {
            var stockCounts = _client.GetStockCounts(articleId.ToArray(), wsdlUrl,storeId);
            return stockCounts;
        }

        public string GetSerialNumberStatus(string storeID,string IMEI, string wsdlUrl)
        {
            string regStatus = _client.GetSerialNumberStatus(storeID, IMEI, wsdlUrl);
            return regStatus;
            //return string.Empty;
        }

        public bool SendDetailsToWebPos(int iSellOrderID, int statusCode = 0)
        {
            bool response = false;
            response = _client.SendDetailsToWebPos(iSellOrderID, statusCode);
            return response;
        }

        //will retire
        //public bool SendPaymentDetails(int iSellOrderID)
        //{
        //    bool response = false;
        //    response = _client.SendPaymentDetails(iSellOrderID);
        //    return response;
        //}
    }
}