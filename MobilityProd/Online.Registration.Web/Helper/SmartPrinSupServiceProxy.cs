﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.DAL.Models;
//using Online.Registration.Web.RegistrationSvc;
//using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.SubscriberICService;
using System.Net;
using System.Xml.Linq;
using log4net;



namespace Online.Registration.Web.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public class SmartPrinSupServiceProxy : ServiceProxyBase<SubscriberICService.SubscriberICServiceClient>
    {
        public static int counter = 0;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PrinSupServiceProxy));
        public SmartPrinSupServiceProxy()
        {
            _client = new SubscriberICService.SubscriberICServiceClient();
        }

        #region Private Methods
        //private void LogResponse(string code, string message, string stackTrace = "")
        //{
        //    LastErrorMessage = "";

        //    if (!Util.IsSuccessCode(code))
        //    {
        //        LastErrorMessage = message;

        //        // Log Error
        //        //Logger.ErrorFormat("Code: {0}|Message: {1}|StackTrace: {2}", code, message, stackTrace);
        //    }
        //}
        #endregion

        #region Public Methods

        public retrievegetPrinSuppResponse getPrinSupplimentarylines(SubscribergetPrinSuppRequest request, bool prinFlag = false)
        {
            retrievegetPrinSuppResponse prinsupp = null;

            try
            {
                prinsupp = _client.getPrinSupplimentarylines(request,prinFlag);
            }
            catch (Exception ex)
            {
                prinsupp = null;
            }

            return prinsupp;

        }
        #endregion
    }
}