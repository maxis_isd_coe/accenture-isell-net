﻿using Online.Registration.Web.WaiverEmailSendingService;
using System.Collections.Generic;

namespace Online.Registration.Web.Helper
{
    public class WaiverEmailSendingServiceProxy : ServiceProxyBase<WaiverEmailSendingServiceClient>
    {
        public WaiverEmailSendingServiceProxy()
        {
            _client = new WaiverEmailSendingServiceClient();
        }
        public string WaiverEmailSend(string to, string from, string subject, Dictionary<string, string> keyValuePairs)
        {

            return _client.WaiverEmailSend(to, from, subject, keyValuePairs);

        }
    }
}
