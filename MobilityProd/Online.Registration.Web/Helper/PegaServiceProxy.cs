﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.PegaSvc;

using log4net;

namespace Online.Registration.Web.Helper
{
    public class PegaServiceProxy : ServiceProxyBase<PegaServiceClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(PegaServiceProxy));
        public string LastErrorMessage { get; set; }
		private string technicalErrorMsg = "Technical problem occured during this request. Please try again later.";

        public PegaServiceProxy()
        {
            _client = new PegaServiceClient();
        }

        public ExecuteDefaultProfileResponse CustomerOneViewDetails (ExecuteDefaultProfileRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            //call the pega service
            var response = _client.ExecuteDefaultProfile(request);
            LogResponse(response);

            return response;
        }

		public ExecuteAssessmentResponse ExecuteAssessment(ExecuteAssessmentRequest request)
		{
			if (request == null)
				throw new ArgumentNullException();

			ExecuteAssessmentResponse response = new ExecuteAssessmentResponse();
			try 
			{
				response = _client.ExecuteAssessment(request);
			}
			catch (TimeoutException ex)
			{
				PopulateExceptionResponse(response, ex);
				response.Message = "TIMEOUT";
				return response;
			}
			catch (Exception ex)
			{
				PopulateExceptionResponse(response, ex);
				response.Message = technicalErrorMsg;
				Logger.Error(string.Format("Error on ExecuteAssessment for MSISDN {0}, Exception {1}", request.ExternalID, Util.LogException(ex)));
				return response;
			}
			return response;
		}

		public UpdateResponseResponse UpdateResponse(UpdateResponseRequest request)
		{
			if (request == null)
				throw new ArgumentNullException();

			UpdateResponseResponse response = new UpdateResponseResponse();
			try
			{
				response = _client.UpdateResponse(request);			
			}
			catch (TimeoutException ex)
			{
				PopulateExceptionResponse(response, ex);
				response.Message = "TIMEOUT";
				return response;
			}
			catch (Exception ex)
			{
				PopulateExceptionResponse(response, ex);
				response.Message = technicalErrorMsg;
				Logger.Error(string.Format("Error on ExecuteAssessment for MSISDN {0}, Exception {1}", request.ExternalID, Util.LogException(ex)));
				return response;
			}

			return response;
		}

        #region Private Methods
        private void LogResponse(WCFResponse response)
        {
            LastErrorMessage = "";

            if (response != null)
            {
                if (!response.Success)
                {
                    LastErrorMessage = response.Message;
                    // Log Error
                    Logger.ErrorFormat("Code: {0}|Message: {1}", response.Code, response.Message);
                }
            }

        }

		private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
		{
			resp.Code = "-1";
			resp.Message = ex.Message;
			resp.Success = false;
		}

        #endregion

    }
}