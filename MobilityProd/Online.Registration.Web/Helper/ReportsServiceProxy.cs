﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using log4net;
using Online.Registration.Web.ReportsSvc;
using Online.Registration.DAL.ReportModels;

namespace Online.Registration.Web.Helper
{
    public class ReportsServiceProxy : ServiceProxyBase<ReportsServiceClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(ReportsServiceProxy));

        public string LastErrorMessage { get; set; }

        #region Private Methods

        private void LogResponse(string code, string message, string stackTrace = "")
        {
            LastErrorMessage = "";

            if (!Util.IsSuccessCode(code))
            {
                LastErrorMessage = message;

                // Log Error
                Logger.ErrorFormat("Code: {0}|Message: {1}|StackTrace: {2}", code, message, stackTrace);
            }
        }

        private void LogResponse(WCFResponse response)
        {
            LogResponse(response.Code, response.Message, response.StackTrace);
        }

        #endregion

        #region Constructor

        public ReportsServiceProxy()
        {
            _client = new ReportsServiceClient();
        }

        #endregion  

        #region Aging Report

        public AgingReportGetResp AgingReportGet(AgingReportGetReq request)
        {
            var response = _client.AgingReportGet(request);
            LogResponse(response);
            return response;
        }

        #endregion

        #region Biometrics Report

        public BiometricsReportGetResp BiometricsReportGet(BiometricsReportGetReq request)
        {
            var response = _client.BiometricsReportGet(request);
            LogResponse(response);
            return response;
        }

        #endregion

        public List<FailTransactionsSearchResult> FailTransactionSearch(FailTransactionsSearchCriteria criteria)
        {
            var request = new FailTransactionReportGetReq()
            {
                Criteria = criteria
            };

            var response = _client.FailTranReportGet(request);

            return response.FailTransactionReport.ToList();
        }

        public List<RegHistoryResult> RegHistoryDetails(int regID)
        {
            var request = new RegHistoryGetReq()
            {
                ID = regID
            };

            var response = _client.RegHistoryDetailsGet(request);

            return response.RegHistoryResult.ToList();
        }
    }
}