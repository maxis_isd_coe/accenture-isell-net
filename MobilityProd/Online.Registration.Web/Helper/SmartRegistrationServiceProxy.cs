﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.DAL.Models;
//using Online.Registration.Web.SmartRegistrationService;
using Online.Registration.Web.RegistrationSvc;

namespace Online.Registration.Web.Helper
{
    //public class SmartRegistrationServiceProxy : ServiceProxyBase<SmartRegistrationServiceProxy>
    public class SmartRegistrationServiceProxy : ServiceProxyBase<RegistrationSvc.RegistrationServiceClient>
    {
        public string LastErrorMessage { get; set; }

        public SmartRegistrationServiceProxy()
        {
            _client = new RegistrationSvc.RegistrationServiceClient();

        }

        #region Private Methods

        private void LogResponse(string code, string message, string stackTrace = "")
        {
            LastErrorMessage = "";

            if (!Util.IsSuccessCode(code))
            {
                LastErrorMessage = message;

                // Log Error
                //Logger.ErrorFormat("Code: {0}|Message: {1}|StackTrace: {2}", code, message, stackTrace);
            }
        }

        private void LogResponse(WCFResponse response)
        {
            LogResponse(response.Code, response.Message, response.StackTrace);
        }

        #endregion

        #region Public Methods

        public bool IsLastCallFailed()
        {
            return !string.IsNullOrEmpty(LastErrorMessage);
        }

        #endregion

        #region Admin

        #region AddressType
        public AddressTypeCreateResp AddressTypeCreate(AddressType addressType)
        {
            var addressTypeCreateRq = new AddressTypeCreateReq()
            {
                AddressType = addressType
            };

            var resp = _client.AddressTypeCreate(addressTypeCreateRq);
            return resp;
        }
        public IEnumerable<int> AddressTypeFind(AddressTypeFind AddressType)
        {
            var request = new AddressTypeFindReq()
            {
                AddressTypeFind = AddressType
            };

            var response = _client.AddressTypeFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.AddressType AddressTypeGet(int AddressTypeID)
        {
            var AddressTypes = AddressTypeGet(new int[] { AddressTypeID });
            return AddressTypes == null ? null : AddressTypes.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.AddressType> AddressTypeGet(IEnumerable<int> AddressTypes)
        {
            if (AddressTypes == null)
                return new List<AddressType>(); ;

            var request = new AddressTypeGetReq()
            {
                IDs = AddressTypes.ToArray()
            };

            var response = _client.AddressTypeGet(request);

            return response.AddressTypes;
        }
        public static IEnumerable<Online.Registration.DAL.Models.AddressType> FindAddressType(AddressTypeFind AddressType)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.AddressTypeGet(proxy.AddressTypeFind(AddressType));
            }
        }
        public void AddressTypeUpdate(AddressType addressType)
        {
            var addressTypeUpdateRq = new AddressTypeUpdateReq()
            {
                AddressType = addressType
            };

            var resp = _client.AddressTypeUpdate(addressTypeUpdateRq);
        }
        #endregion

        #region Bank
        public BankCreateResp BankCreate(Bank bank)
        {
            var bankCreateRq = new BankCreateReq()
            {
                Bank = bank
            };

            var resp = _client.BankCreate(bankCreateRq);
            return resp;
        }
        public IEnumerable<int> BankFind(BankFind bank)
        {
            var request = new BankFindReq()
            {
                BankFind = bank
            };

            var response = _client.BankFind(request);
            return response.IDs;
        }
        public Bank BankGet(int bankID)
        {
            var banks = BankGet(new int[] { bankID });
            return banks == null ? null : banks.SingleOrDefault();
        }
        public IEnumerable<Bank> BankGet(IEnumerable<int> banks)
        {
            if (banks == null)
                return new List<Bank>();

            var request = new BankGetReq()
            {
                IDs = banks.ToArray()
            };

            var response = _client.BankGet(request);

            return response.Bank;
        }
        public static IEnumerable<Online.Registration.DAL.Models.Bank> FindBank(BankFind Bank)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.BankGet(proxy.BankFind(Bank));
            }
        }
        public void BankUpdate(Bank bank)
        {
            var bankUpdateReq = new BankUpdateReq()
            {
                Bank = bank
            };

            var resp = _client.BankUpdate(bankUpdateReq);
        }
        #endregion

        #region CardType
        public CardTypeCreateResp CardTypeCreate(CardType cardType)
        {
            var cardTypeCreateRq = new CardTypeCreateReq()
            {
                CardType = cardType
            };

            var resp = _client.CardTypeCreate(cardTypeCreateRq);
            return resp;
        }
        public IEnumerable<int> CardTypeFind(CardTypeFind CardType)
        {
            var request = new CardTypeFindReq()
            {
                CardTypeFind = CardType
            };

            var response = _client.CardTypeFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.CardType CardTypeGet(int CardTypeID)
        {
            var CardTypes = CardTypeGet(new int[] { CardTypeID });
            return CardTypes == null ? null : CardTypes.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.CardType> CardTypeGet(IEnumerable<int> CardTypes)
        {
            if (CardTypes == null)
                return new List<CardType>(); ;

            var request = new CardTypeGetReq()
            {
                IDs = CardTypes.ToArray()
            };

            var response = _client.CardTypeGet(request);

            return response.CardType;
        }
        public static IEnumerable<Online.Registration.DAL.Models.CardType> FindCardType(CardTypeFind CardType)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.CardTypeGet(proxy.CardTypeFind(CardType));
            }
        }
        public void CardTypeUpdate(CardType cardType)
        {
            var cardTypeUpdateRq = new CardTypeUpdateReq()
            {
                CardType = cardType
            };

            var resp = _client.CardTypeUpdate(cardTypeUpdateRq);
        }
        #endregion

        #region CustomerTitle
        public IEnumerable<int> CustomerTitleFind(CustomerTitleFind CustomerTitle)
        {
            var request = new CustomerTitleFindReq()
            {
                CustomerTitleFind = CustomerTitle
            };

            var response = _client.CustomerTitleFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.CustomerTitle CustomerTitleGet(int CustomerTitleID)
        {
            var CustomerTitles = CustomerTitleGet(new int[] { CustomerTitleID });
            return CustomerTitles == null ? null : CustomerTitles.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.CustomerTitle> CustomerTitleGet(IEnumerable<int> CustomerTitles)
        {
            if (CustomerTitles == null)
                return new List<CustomerTitle>();

            var request = new CustomerTitleGetReq()
            {
                IDs = CustomerTitles.ToArray()
            };

            var response = _client.CustomerTitleGet(request);

            return response.CustomerTitles;
        }
        public static IEnumerable<Online.Registration.DAL.Models.CustomerTitle> FindCustomerTitle(CustomerTitleFind CustomerTitle)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.CustomerTitleGet(proxy.CustomerTitleFind(CustomerTitle));
            }
        }
        public void CustomerTitleUpdate(CustomerTitle customerTitle)
        {
            var customerTitleUpdateRq = new CustomerTitleUpdateReq()
            {
                CustomerTitle = customerTitle
            };

            var resp = _client.CustomerTitleUpdate(customerTitleUpdateRq);
        }
        public CustomerTitleCreateResp CustomerTitleCreate(CustomerTitle customerTitle)
        {
            var customerTitleCreateRq = new CustomerTitleCreateReq()
            {
                CustomerTitle = customerTitle
            };

            var resp = _client.CustomerTitleCreate(customerTitleCreateRq);
            return resp;
        }
        #endregion

        #region IDCardType
        public IDCardTypeCreateResp IDCardTypeCreate(IDCardType idCardType)
        {
            var idCardTypeCreateRq = new IDCardTypeCreateReq()
            {
                IDCardType = idCardType
            };

            var resp = _client.IDCardTypeCreate(idCardTypeCreateRq);
            return resp;
        }
        public IEnumerable<int> IDCardTypeFind(IDCardTypeFind IDCardType)
        {
            var request = new IDCardTypeFindReq()
            {
                IDCardTypeFind = IDCardType
            };

            var response = _client.IDCardTypeFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.IDCardType IDCardTypeGet(int IDCardTypeID)
        {
            var IDCardTypes = IDCardTypeGet(new int[] { IDCardTypeID });
            return IDCardTypes == null ? null : IDCardTypes.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.IDCardType> IDCardTypeGet(IEnumerable<int> IDCardTypes)
        {
            if (IDCardTypes == null)
                return new List<IDCardType>(); ;

            var request = new IDCardTypeGetReq()
            {
                IDs = IDCardTypes.ToArray()
            };

            var response = _client.IDCardTypeGet(request);

            return response.IDCardType;
        }
        public static IEnumerable<Online.Registration.DAL.Models.IDCardType> FindIDCardType(IDCardTypeFind IDCardType)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.IDCardTypeGet(proxy.IDCardTypeFind(IDCardType));
            }
        }
        public void IDCardTypeUpdate(IDCardType idCardType)
        {
            var idCardTypeUpdateRq = new IDCardTypeUpdateReq
            {
                IDCardType = idCardType
            };
            var resp = _client.IDCardTypeUpdate(idCardTypeUpdateRq);
        }
        #endregion

        #region State
        public StateCreateResp StateCreate(State state)
        {
            var stateCreateRq = new StateCreateReq()
            {
                State = state
            };

            var resp = _client.StateCreate(stateCreateRq);
            return resp;
        }
        public IEnumerable<int> StateFind(StateFind state)
        {
            var request = new StateFindReq()
            {
                StateFind = state
            };

            var response = _client.StateFind(request);
            return response.IDs;
        }
        public void StateUpdate(State state)
        {
            var stateUpdateRq = new StateUpdateReq()
            {
                State = state
            };

            var resp = _client.StateUpdate(stateUpdateRq);
        }
        public Online.Registration.DAL.Models.State StateGet(int StateID)
        {
            var States = StateGet(new int[] { StateID });
            return States == null ? null : States.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.State> StateGet(IEnumerable<int> States)
        {
            if (States == null)
                return new List<State>(); ;

            var request = new StateGetReq()
            {
                IDs = States.ToArray()
            };

            var response = _client.StateGet(request);

            return response.State;
        }
        public static IEnumerable<Online.Registration.DAL.Models.State> FindState(StateFind State)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.StateGet(proxy.StateFind(State));
            }
        }
        #endregion

        #region PaymentMode
        public PaymentModeCreateResp PaymentModeCreate(PaymentMode PaymentMode)
        {
            var PaymentModeCreateReq = new PaymentModeCreateReq()
            {
                PaymentMode = PaymentMode
            };

            var resp = _client.PaymentModeCreate(PaymentModeCreateReq);
            return resp;
        }
        public IEnumerable<int> PaymentModeFind(PaymentModeFind PaymentMode)
        {
            var request = new PaymentModeFindReq()
            {
                PaymentModeFind = PaymentMode
            };

            var response = _client.PaymentModeFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.PaymentMode PaymentModeGet(int PaymentModeID)
        {
            var PaymentModes = PaymentModeGet(new int[] { PaymentModeID });
            return PaymentModes == null ? null : PaymentModes.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.PaymentMode> PaymentModeGet(IEnumerable<int> PaymentModes)
        {
            if (PaymentModes == null)
                return new List<PaymentMode>(); ;

            var request = new PaymentModeGetReq()
            {
                IDs = PaymentModes.ToArray()
            };

            var response = _client.PaymentModeGet(request);

            return response.PaymentMode;
        }
        public static IEnumerable<Online.Registration.DAL.Models.PaymentMode> FindPaymentMode(PaymentModeFind PaymentMode)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.PaymentModeGet(proxy.PaymentModeFind(PaymentMode));
            }
        }
        public void PaymentModeUpdate(PaymentMode PaymentMode)
        {
            var PaymentModeUpdateReq = new PaymentModeUpdateReq
            {
                PaymentMode = PaymentMode
            };
            var resp = _client.PaymentModeUpdate(PaymentModeUpdateReq);
        }
        #endregion

        #region Payment Mode Payment Details
        public PaymentModePaymentDetailsCreateResp PaymentModePaymentDetailsCreate(PaymentModePaymentDetails PaymentModePaymentDetails)
        {
            var PaymentModePaymentDetailsCreateReq = new PaymentModePaymentDetailsCreateReq()
            {
                PaymentModePaymentDetails = PaymentModePaymentDetails
            };

            var resp = _client.PaymentModePaymentDetailsCreate(PaymentModePaymentDetailsCreateReq);
            return resp;
        }
        public IEnumerable<int> PaymentModePaymentDetailsFind(PaymentModePaymentDetailsFind PaymentModePaymentDetails)
        {
            var request = new PaymentModePaymentDetailsFindReq()
            {
                PaymentModePaymentDetailsFind = PaymentModePaymentDetails
            };

            var response = _client.PaymentModePaymentDetailsFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.PaymentModePaymentDetails PaymentModePaymentDetailsGet(int PaymentModePaymentDetailsID)
        {
            var PaymentDetailsPaymentModes = PaymentModePaymentDetailsGet(new int[] { PaymentModePaymentDetailsID });
            return PaymentDetailsPaymentModes == null ? null : PaymentDetailsPaymentModes.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.PaymentModePaymentDetails> PaymentModePaymentDetailsGet(IEnumerable<int> PaymentDetailsPaymentModes)
        {
            if (PaymentDetailsPaymentModes == null)
                return new List<PaymentModePaymentDetails>(); ;

            var request = new PaymentModePaymentDetailsGetReq()
            {
                IDs = PaymentDetailsPaymentModes.ToArray()
            };

            var response = _client.PaymentModePaymentDetailsGet(request);

            return response.PaymentModePaymentDetails;
        }
        public static IEnumerable<Online.Registration.DAL.Models.PaymentModePaymentDetails> FindPaymentModePaymentDetails(PaymentModePaymentDetailsFind PaymentModePaymentDetails)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.PaymentModePaymentDetailsGet(proxy.PaymentModePaymentDetailsFind(PaymentModePaymentDetails));
            }
        }
        public void PaymentModePaymentDetailsUpdate(PaymentModePaymentDetails PaymentModePaymentDetails)
        {
            var PaymentModePaymentDetailsUpdateReq = new PaymentModePaymentDetailsUpdateReq
            {
                PaymentModePaymentDetails = PaymentModePaymentDetails
            };
            var resp = _client.PaymentModePaymentDetailsUpdate(PaymentModePaymentDetailsUpdateReq);
        }
        #endregion

        #region CustomerInfo
        public CustomerInfoCreateResp CustomerInfoCreate(CustomerInfo CustomerInfo)
        {
            var CustomerInfoCreateReq = new CustomerInfoCreateReq()
            {
                CustomerInfo = CustomerInfo
            };

            var resp = _client.CustomerInfoCreate(CustomerInfoCreateReq);
            return resp;
        }
        public void CustomerInfoUpdate(CustomerInfo CustomerInfo)
        {
            var CustomerInfoUpdateReq = new CustomerInfoUpdateReq
            {
                CustomerInfo = CustomerInfo
            };
            var resp = _client.CustomerInfoUpdate(CustomerInfoUpdateReq);
        }
        #endregion

        #region Card Type Payment Details
        public CardTypePaymentDetailsCreateResp CardTypePaymentDetailsCreate(CardTypePaymentDetails cardTypePaymentDetails)
        {
            var cardTypePaymentDetailsCreateRq = new CardTypePaymentDetailsCreateReq()
            {
                CardTypePaymentDetails = cardTypePaymentDetails
            };

            var resp = _client.CardTypePaymentDetailsCreate(cardTypePaymentDetailsCreateRq);
            return resp;
        }
        public IEnumerable<int> CardTypePaymentDetailsFind(CardTypePaymentDetailsFind CardTypePaymentDetails)
        {
            var request = new CardTypePaymentDetailsFindReq()
            {
                CardTypePaymentDetailsFind = CardTypePaymentDetails
            };

            var response = _client.CardTypePaymentDetailsFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.CardTypePaymentDetails CardTypePaymentDetailsGet(int CardTypePaymentDetailsID)
        {
            var PaymentDetailsCardTypes = CardTypePaymentDetailsGet(new int[] { CardTypePaymentDetailsID });
            return PaymentDetailsCardTypes == null ? null : PaymentDetailsCardTypes.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.CardTypePaymentDetails> CardTypePaymentDetailsGet(IEnumerable<int> PaymentDetailsCardTypes)
        {
            if (PaymentDetailsCardTypes == null)
                return new List<CardTypePaymentDetails>(); ;

            var request = new CardTypePaymentDetailsGetReq()
            {
                IDs = PaymentDetailsCardTypes.ToArray()
            };

            var response = _client.CardTypePaymentDetailsGet(request);

            return response.CardTypePaymentDetails;
        }
        public static IEnumerable<Online.Registration.DAL.Models.CardTypePaymentDetails> FindCardTypePaymentDetails(CardTypePaymentDetailsFind CardTypePaymentDetails)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.CardTypePaymentDetailsGet(proxy.CardTypePaymentDetailsFind(CardTypePaymentDetails));
            }
        }
        public void CardTypeUpdate(CardTypePaymentDetails cardTypePaymentDetails)
        {
            var cardTypePaymentDetailsUpdateRq = new CardTypePaymentDetailsUpdateReq()
            {
                CardTypePaymentDetails = cardTypePaymentDetails
            };

            var resp = _client.CardTypePaymentDetailsUpdate(cardTypePaymentDetailsUpdateRq);
        }
        #endregion

        #region RegType
        public RegTypeCreateResp RegTypeCreate(RegType RegType)
        {
            var RegTypeCreateReq = new RegTypeCreateReq()
            {
                RegType = RegType
            };

            var resp = _client.RegTypeCreate(RegTypeCreateReq);
            return resp;
        }
        public IEnumerable<int> RegTypeFind(RegTypeFind RegType)
        {
            var request = new RegTypeFindReq()
            {
                RegTypeFind = RegType
            };

            var response = _client.RegTypeFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.RegType RegTypeGet(int RegTypeID)
        {
            var RegTypes = RegTypeGet(new int[] { RegTypeID });
            return RegTypes == null ? null : RegTypes.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.RegType> RegTypeGet(IEnumerable<int> RegTypes)
        {
            if (RegTypes == null)
                return new List<RegType>(); ;

            var request = new RegTypeGetReq()
            {
                IDs = RegTypes.ToArray()
            };

            var response = _client.RegTypeGet(request);

            return response.RegTypes;
        }
        public static IEnumerable<Online.Registration.DAL.Models.RegType> FindRegType(RegTypeFind RegType)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.RegTypeGet(proxy.RegTypeFind(RegType));
            }
        }
        public void RegTypeUpdate(RegType RegType)
        {
            var RegTypeUpdateReq = new RegTypeUpdateReq
            {
                RegType = RegType
            };
            var resp = _client.RegTypeUpdate(RegTypeUpdateReq);
        }
        #endregion

        #region Race
        public RaceCreateResp RaceCreate(Race Race)
        {
            var RaceCreateReq = new RaceCreateReq()
            {
                Race = Race
            };

            var resp = _client.RaceCreate(RaceCreateReq);
            return resp;
        }
        public IEnumerable<int> RaceFind(RaceFind Race)
        {
            var request = new RaceFindReq()
            {
                RaceFind = Race
            };

            var response = _client.RaceFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.Race RaceGet(int RaceID)
        {
            var Races = RaceGet(new int[] { RaceID });
            return Races == null ? null : Races.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.Race> RaceGet(IEnumerable<int> Races)
        {
            if (Races == null)
                return new List<Race>(); ;

            var request = new RaceGetReq()
            {
                IDs = Races.ToArray()
            };

            var response = _client.RaceGet(request);

            return response.Races;
        }
        public static IEnumerable<Online.Registration.DAL.Models.Race> FindRace(RaceFind Race)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.RaceGet(proxy.RaceFind(Race));
            }
        }
        public void RaceUpdate(Race Race)
        {
            var RaceUpdateReq = new RaceUpdateReq
            {
                Race = Race
            };
            var resp = _client.RaceUpdate(RaceUpdateReq);
        }
        #endregion

        #region Language
        public LanguageCreateResp LanguageCreate(Language Language)
        {
            var LangaugeCreateRq = new LanguageCreateReq()
            {
                Language = Language
            };

            var resp = _client.LanguageCreate(LangaugeCreateRq);
            return resp;
        }
        public IEnumerable<int> LanguageFind(LanguageFind Language)
        {
            var request = new LanguageFindReq()
            {
                LanguageFind = Language
            };

            var response = _client.LanguageFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.Language LanguageGet(int LanguageID)
        {
            var Languages = LanguageGet(new int[] { LanguageID });
            return Languages == null ? null : Languages.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.Language> LanguageGet(IEnumerable<int> Languages)
        {
            if (Languages == null)
                return new List<Language>(); ;

            var request = new LanguageGetReq()
            {
                IDs = Languages.ToArray()
            };

            var response = _client.LanguageGet(request);

            return response.Languages;
        }
        public static IEnumerable<Online.Registration.DAL.Models.Language> FindLanguage(LanguageFind Language)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.LanguageGet(proxy.LanguageFind(Language));
            }
        }
        public void LanguageUpdate(Language Language)
        {
            var LanguageUpdateReq = new LanguageUpdateReq
            {
                Language = Language
            };
            var resp = _client.LanguageUpdate(LanguageUpdateReq);
        }

        #endregion

        #region Nationality
        public NationalityCreateResp NationalityCreate(Nationality Nationality)
        {
            var NationalityCreateReq = new NationalityCreateReq()
            {
                Nationality = Nationality
            };

            var resp = _client.NationalityCreate(NationalityCreateReq);
            return resp;
        }
        public IEnumerable<int> NationalityFind(NationalityFind Nationality)
        {
            var request = new NationalityFindReq()
            {
                NationalityFind = Nationality
            };

            var response = _client.NationalityFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.Nationality NationalityGet(int NationalityID)
        {
            var Nationalitys = NationalityGet(new int[] { NationalityID });
            return Nationalitys == null ? null : Nationalitys.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.Nationality> NationalityGet(IEnumerable<int> Nationalitys)
        {
            if (Nationalitys == null)
                return new List<Nationality>(); ;

            var request = new NationalityGetReq()
            {
                IDs = Nationalitys.ToArray()
            };

            var response = _client.NationalityGet(request);

            return response.Nationalities;
        }
        public static IEnumerable<Online.Registration.DAL.Models.Nationality> FindNationality(NationalityFind Nationality)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.NationalityGet(proxy.NationalityFind(Nationality));
            }
        }
        public void NationalityUpdate(Nationality Nationality)
        {
            var NationalityUpdateReq = new NationalityUpdateReq
            {
                Nationality = Nationality
            };
            var resp = _client.NationalityUpdate(NationalityUpdateReq);
        }
        #endregion

        #region Country
        public CountryCreateResp CountryCreate(Country country)
        {
            var countryCreateRq = new CountryCreateReq()
            {
                Country = country
            };

            var resp = _client.CountryCreate(countryCreateRq);

            return resp;
        }
        public void CountryUpdate(Country country)
        {
            var countryUpdateReq = new CountryUpdateReq()
            {
                Country = country
            };

            var resp = _client.CountryUpdate(countryUpdateReq);
        }
        public IEnumerable<int> CountryFind(CountryFind country)
        {
            var request = new CountryFindReq()
            {
                CountryFind = country
            };

            var response = _client.CountryFind(request);

            return response.IDs;
        }
        public Country CountryGet(int countryID)
        {
            var countries = CountryGet(new int[] { countryID });
            return countries == null ? null : countries.SingleOrDefault();
        }
        public IEnumerable<Country> CountryGet(IEnumerable<int> countries)
        {
            if (countries == null)
                return new List<Country>();

            var request = new CountryGetReq()
            {
                IDs = countries.ToArray()
            };

            var response = _client.CountryGet(request);

            return response.Country;
        }
        public static IEnumerable<Online.Registration.DAL.Models.Country> FindCountry(CountryFind Country)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.CountryGet(proxy.CountryFind(Country));
            }
        }
        #endregion

        #region ExtIDType
        public ExtIDTypeCreateResp ExtIDTypeCreate(ExtIDType ExtIDType)
        {
            var ExtIDTypeCreateRq = new ExtIDTypeCreateReq()
            {
                ExtIDType = ExtIDType
            };

            var resp = _client.ExtIDTypeCreate(ExtIDTypeCreateRq);
            return resp;
        }
        public IEnumerable<int> ExtIDTypeFind(ExtIDTypeFind ExtIDType)
        {
            var request = new ExtIDTypeFindReq()
            {
                ExtIDTypeFind = ExtIDType
            };

            var response = _client.ExtIDTypeFind(request);
            return response.IDs;
        }
        public ExtIDType ExtIDTypeGet(int ExtIDTypeID)
        {
            var ExtIDTypes = ExtIDTypeGet(new int[] { ExtIDTypeID });
            return ExtIDTypes == null ? null : ExtIDTypes.SingleOrDefault();
        }
        public IEnumerable<ExtIDType> ExtIDTypeGet(IEnumerable<int> ExtIDTypes)
        {
            if (ExtIDTypes == null)
                return new List<ExtIDType>();

            var request = new ExtIDTypeGetReq()
            {
                IDs = ExtIDTypes.ToArray()
            };

            var response = _client.ExtIDTypeGet(request);

            return response.ExtIDType;
        }
        public static IEnumerable<Online.Registration.DAL.Models.ExtIDType> FindExtIDType(ExtIDTypeFind ExtIDType)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.ExtIDTypeGet(proxy.ExtIDTypeFind(ExtIDType));
            }
        }
        public void ExtIDTypeUpdate(ExtIDType ExtIDType)
        {
            var ExtIDTypeUpdateReq = new ExtIDTypeUpdateReq()
            {
                ExtIDType = ExtIDType
            };

            var resp = _client.ExtIDTypeUpdate(ExtIDTypeUpdateReq);
        }
        #endregion

        #region VIPCode
        public VIPCodeCreateResp VIPCodeCreate(VIPCode VIPCode)
        {
            var VIPCodeCreateRq = new VIPCodeCreateReq()
            {
                VIPCode = VIPCode
            };

            var resp = _client.VIPCodeCreate(VIPCodeCreateRq);
            return resp;
        }
        public IEnumerable<int> VIPCodeFind(VIPCodeFind VIPCode)
        {
            var request = new VIPCodeFindReq()
            {
                VIPCodeFind = VIPCode
            };

            var response = _client.VIPCodeFind(request);
            return response.IDs;
        }
        public VIPCode VIPCodeGet(int VIPCodeID)
        {
            var VIPCodes = VIPCodeGet(new int[] { VIPCodeID });
            return VIPCodes == null ? null : VIPCodes.SingleOrDefault();
        }
        public IEnumerable<VIPCode> VIPCodeGet(IEnumerable<int> VIPCodes)
        {
            if (VIPCodes == null)
                return new List<VIPCode>();

            var request = new VIPCodeGetReq()
            {
                IDs = VIPCodes.ToArray()
            };

            var response = _client.VIPCodeGet(request);

            return response.VIPCode;
        }
        public static IEnumerable<Online.Registration.DAL.Models.VIPCode> FindVIPCode(VIPCodeFind VIPCode)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.VIPCodeGet(proxy.VIPCodeFind(VIPCode));
            }
        }
        public void VIPCodeUpdate(VIPCode VIPCode)
        {
            var VIPCodeUpdateReq = new VIPCodeUpdateReq()
            {
                VIPCode = VIPCode
            };

            var resp = _client.VIPCodeUpdate(VIPCodeUpdateReq);
        }
        #endregion

        #endregion

        #region Registration

        public RegistrationCreateResp RegistrationCreate(Online.Registration.DAL.Models.Registration reg, Customer customer, RegMdlGrpModel regMdlGrpModel,
                                                        List<Address> address, List<RegPgmBdlPkgComp> regPBPCs, RegStatus regStatus,
                                                        List<RegSuppLine> RegSuppLines, List<RegPgmBdlPkgComp> RegSuppLineVASes)
        {
            var request = new RegistrationCreateReq()
            {
                Registration = reg,
                Customer = customer,
                RegMdlGrpModel = regMdlGrpModel,
                Addresses = address.ToArray(),
                RegPgmBdlPkgComps = regPBPCs.ToArray(),
                RegStatus = regStatus,
                RegSuppLines = RegSuppLines.ToArray(),
                RegSuppLineVASes = RegSuppLineVASes.ToArray(),
                //CustPhotos = SplitStringIntoBlocks(reg.CustomerPhoto,100000).ToArray()
            };

            var response = _client.RegistrationCreate(request);

            return response;
        }
        //Home registration create
        public HomeRegistrationCreateResp HomeRegistrationCreate(Online.Registration.DAL.Models.Registration reg, Customer customer, List<Address> address,
                                                            List<RegPgmBdlPkgComp> regPBPCs, RegStatus regStatus)
        {
            var request = new RegistrationCreateReq()
            {
                Registration = reg,
                Customer = customer,
                //RegMdlGrpModel = regMdlGrpModel,
                Addresses = address.ToArray(),
                RegPgmBdlPkgComps = regPBPCs.ToArray(),
                RegStatus = regStatus
            };

            var response = _client.HomeRegistrationCreate(request);

            return response;
        }
        public IEnumerable<int> RegistrationFind(RegistrationFind Registration)
        {
            var request = new RegistrationFindReq()
            {
                RegistrationFind = Registration
            };

            var response = _client.RegistrationFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.Registration RegistrationGet(int RegistrationID)
        {
            var Registrations = RegistrationGet(new int[] { RegistrationID });
            return Registrations == null ? null : Registrations.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.Registration> RegistrationGet(IEnumerable<int> RegistrationIDs)
        {
            if (RegistrationIDs == null)
                return new List<Online.Registration.DAL.Models.Registration>(); ;

            var request = new RegistrationGetReq()
            {
                IDs = RegistrationIDs.ToArray()
            };

            var response = _client.RegistrationGet(request);

            return response.Registrations;
        }
        public static IEnumerable<Online.Registration.DAL.Models.Registration> FindRegistration(RegistrationFind Registration)
        {
            using (var proxy = new SmartRegistrationServiceProxy())
            {
                return proxy.RegistrationGet(proxy.RegistrationFind(Registration));
            }
        }
        public List<RegistrationSearchResult> RegistrationSearch(RegistrationSearchCriteria criteria)
        {
            var request = new RegistrationSearchReq()
            {
                Criteria = criteria
            };

            var response = _client.RegistrationSearch(request);

            return response.RegistrationSearchResults.ToList();
        }

        #region Added by Patanjali on 30-3-2013 to support Storekeeper and Cashier flow

        public List<RegistrationSearchResult> StoreKeeperSearch(RegistrationSearchCriteria criteria)
        {
            var request = new RegistrationSearchReq()
            {
                Criteria = criteria
            };

            var response = _client.StoreKeeperSearch(request);

            return response.RegistrationSearchResults.ToList();
        }

        public List<RegistrationSearchResult> CashierSearch(RegistrationSearchCriteria criteria)
        {
            var request = new RegistrationSearchReq()
            {
                Criteria = criteria
            };

            var response = _client.CashierSearch(request);

            return response.RegistrationSearchResults.ToList();
        }

        #endregion

        #region Added by Patanjali on 07-04-2013 to get Payment status
        public int PaymentStatusGet(int RegID)
        {
            int returnstatus = -1;

            returnstatus = _client.PaymentStatusGet(RegID);

            return returnstatus;
        }
        #endregion


        public void RegistrationClose(RegStatus regStatus)
        {
            var request = new RegistrationCloseReq()
            {
                RegStatus = regStatus
            };

            var response = _client.RegistrationClose(request);
        }
        public void RegistrationCancel(RegStatus regStatus)
        {
            var request = new RegistrationCancelReq()
            {
                RegStatus = regStatus
            };

            var response = _client.RegistrationCancel(request);
        }
        public void RegistrationPaymentSuccess(RegStatus regStatus)
        {
            var request = new RegistrationPaymentSuccessReq()
            {
                RegStatus = regStatus
            };

            var response = _client.RegistrationPaymentSuccess(request);
        }

        public void RegistrationPaymentFailed(RegStatus regStatus)
        {
            var request = new RegistrationPaymentFailedReq()
            {
                RegStatus = regStatus
            };

            var response = _client.RegistrationPaymentFailed(request);
        }
        public void RegistrationUpdate(Online.Registration.DAL.Models.Registration reg)
        {
            var regUpdateRq = new RegistrationUpdateReq()
            {
                Registration = reg
            };

            var resp = _client.RegistrationUpdate(regUpdateRq);

        }
        public KenanCustomerInfoCreateResp KenanCustomerInfoCreate(Online.Registration.DAL.Models.KenamCustomerInfo reg)
        {
            var kencustInfoRq = new KenanCustomerInfoCreateReq()
            {
                KenanCustomerInfo = reg
            };

            var resp = _client.KenanCustomerInfoCreate(kencustInfoRq);
            return resp;
        }

        public List<Online.Registration.DAL.Models.Contracts> GetContractsList()
        {
            var resp = _client.GetActiveContractsList();
            if (resp.Contracts != null)
                return resp.Contracts.ToList();
            return new List<Online.Registration.DAL.Models.Contracts>();
    
        }
        #endregion

        #region Customer

        public IEnumerable<int> CustomerFind(CustomerFind Customer)
        {
            var request = new CustomerFindReq()
            {
                CustomerFind = Customer
            };

            var response = _client.CustomerFind(request);
            return response.IDs;
        }
        public IEnumerable<Customer> CustomerGet(IEnumerable<int> CustomerIDs)
        {
            if (CustomerIDs == null)
                return new List<Customer>(); ;

            var request = new CustomerGetReq()
            {
                IDs = CustomerIDs.ToArray()
            };

            var response = _client.CustomerGet(request);

            return response.Customers;
        }

        #endregion

        #region Reg Model Group Model

        public IEnumerable<int> RegMdlGrpModelFind(RegMdlGrpModelFind regMdlGrpModel)
        {
            var request = new RegMdlGrpModelFindReq()
            {
                RegMdlGrpModelFind = regMdlGrpModel
            };

            var response = _client.RegMdlGrpModelFind(request);
            return response.IDs;
        }
        public IEnumerable<RegMdlGrpModel> RegMdlGrpModelGet(IEnumerable<int> regMdlGrpModelIDs)
        {
            if (regMdlGrpModelIDs == null)
                return new List<RegMdlGrpModel>(); ;

            var request = new RegMdlGrpModelGetReq()
            {
                IDs = regMdlGrpModelIDs.ToArray()
            };

            var response = _client.RegMdlGrpModelGet(request);

            return response.RegMdlGrpModels;
        }

        #endregion

        #region Reg Address

        public IEnumerable<int> RegAddressFind(AddressFind address)
        {
            var request = new AddressFindReq()
            {
                AddressFind = address
            };

            var response = _client.AddressFind(request);
            return response.IDs;
        }
        public IEnumerable<Address> RegAddressGet(IEnumerable<int> addressIDs)
        {
            if (addressIDs == null)
                return new List<Address>(); ;

            var request = new AddressGetReq()
            {
                IDs = addressIDs.ToArray()
            };

            var response = _client.AddressGet(request);

            return response.Addresses;
        }

        #endregion

        #region Reg Program Bundle Package Component

        public IEnumerable<int> RegPgmBdlPkgCompFind(RegPgmBdlPkgCompFind regPgmBdlPkgComp)
        {
            var request = new RegPgmBdlPkgCompFindReq()
            {
                RegPgmBdlPkgCompFind = regPgmBdlPkgComp
            };

            var response = _client.RegPgmBdlPkgCompFind(request);
            return response.IDs;
        }
        public IEnumerable<int> RegPgmBdlPkgComponentFind(RegPgmBdlPkgCompFind regPgmBdlPkgComp)
        {
            var request = new RegPgmBdlPkgCompFindReq()
            {
                RegPgmBdlPkgCompFind = regPgmBdlPkgComp
            };

            var response = _client.RegPgmBdlPkgComponentFind(request);
            return response.IDs;
        }
        public IEnumerable<RegPgmBdlPkgComp> RegPgmBdlPkgCompGet(IEnumerable<int> regPgmBdlPkgCompIDs)
        {
            if (regPgmBdlPkgCompIDs == null)
                return new List<RegPgmBdlPkgComp>(); ;

            var request = new RegPgmBdlPkgCompGetReq()
            {
                IDs = regPgmBdlPkgCompIDs.ToArray()
            };

            var response = _client.RegPgmBdlPkgCompGet(request);

            return response.RegPgmBdlPkgComps;
        }
        public IEnumerable<PgmBdlPckComponent> RegPgmBdlPkgComponentGet(IEnumerable<int> regPgmBdlPkgCompIDs)
        {
            if (regPgmBdlPkgCompIDs == null)
                return new List<PgmBdlPckComponent>(); ;

            var request = new RegPgmBdlPkgCompGetReq()
            {
                IDs = regPgmBdlPkgCompIDs.ToArray()
            };

            var response = _client.RegPgmBdlPkgComponentGet(request);

            return response.RegPgmBdlPkgComps;
        }
        #endregion

        #region Reg Supplimentary Line

        public IEnumerable<int> RegSuppLineFind(RegSuppLineFind regSuppLine)
        {
            var request = new RegSuppLineFindReq()
            {
                RegSuppLineFind = regSuppLine
            };

            var response = _client.RegSuppLineFind(request);
            return response.IDs;
        }
        public IEnumerable<RegSuppLine> RegSuppLineGet(IEnumerable<int> regSuppLineIDs)
        {
            if (regSuppLineIDs == null)
                return new List<RegSuppLine>(); ;

            var request = new RegSuppLineGetReq()
            {
                IDs = regSuppLineIDs.ToArray()
            };

            var response = _client.RegSuppLineGet(request);

            return response.RegSuppLines;
        }

        #endregion

        #region Reg Status

        public int RegStatusCreate(RegStatus regStatus, Online.Registration.DAL.Models.Registration reg)
        {
            var request = new RegStatusCreateReq() { RegStatuses = regStatus, Registrations = reg };
            var response = _client.RegStatusCreate(request);
            LogResponse(response);

            return response.ID;
        }
        public IEnumerable<int> RegStatusFind(RegStatusFind regStatus)
        {
            var request = new RegStatusFindReq()
            {
                RegStatusFind = regStatus
            };

            var response = _client.RegStatusFind(request);
            return response.IDs;
        }
        public IEnumerable<RegStatus> RegStatusGet(IEnumerable<int> regStatusIDs)
        {
            if (regStatusIDs == null)
                return new List<RegStatus>(); ;

            var request = new RegStatusGetReq()
            {
                IDs = regStatusIDs.ToArray()
            };

            var response = _client.RegStatusGet(request);

            return response.RegStatuses;
        }
        public IEnumerable<RegStatusResult> RegStatusLogGet(IEnumerable<int> regStatusIDs)
        {
            if (regStatusIDs == null)
                return new List<RegStatusResult>(); ;

            var request = new RegStatusLogGetReq()
            {
                IDs = regStatusIDs.ToArray()
            };

            var response = _client.RegStatusLogGet(request);

            return response.RegStatusResult;
        }

        #endregion

        #region Biometirc

        public int BiometricCreate(Biometrics Biometric)
        {
            var request = new BiometricCreateReq() { Biometric = Biometric };
            var response = _client.BiometricCreate(request);
            LogResponse(response);

            return response.ID;
        }
        public void BiometricUpdate(Biometrics Biometric)
        {
            var request = new BiometricUpdateReq() { Biometric = Biometric };
            var response = _client.BiometricUpdate(request);
            LogResponse(response);
        }
        public IEnumerable<int> BiometricFind(BiometricFind Biometric)
        {
            var request = new BiometricFindReq()
            {
                BiometricFind = Biometric
            };

            var response = _client.BiometricFind(request);
            return response.IDs;
        }
        public IEnumerable<Biometrics> BiometricGet(IEnumerable<int> BiometricIDs)
        {
            if (BiometricIDs == null)
                return new List<Biometrics>(); ;

            var request = new BiometricGetReq()
            {
                IDs = BiometricIDs.ToArray()
            };

            var response = _client.BiometricGet(request);

            return response.Biometrics;
        }

        #endregion

        #region Reg Account

        public IEnumerable<RegAccount> RegAccountGet(IEnumerable<int> regIDs)
        {
            if (regIDs == null)
                return new List<RegAccount>(); ;

            var request = new RegAccountGetReq()
            {
                RegIDs = regIDs.ToArray()
            };

            var response = _client.RegAccountGet(request);

            return response.RegAccounts;
        }

        #endregion
        
        public GetPackageDetailsResp SelectedPackageDetail(int PackageID)
        {

            var request=new GetPackageDetailsReq()
            {
                IDs=PackageID
            };
            var response = _client.SelectedPackageDetail(request);
            return response;
        }

        public SmartGetExistingPackageDetailsResp ExistingPackageDetails(int PackageID)
        {

            var request = new SmartGetExistingPackageDetailsReq()
            {
                IDs = PackageID
            };
            var response = _client.ExistingPackageDetails(request);
            return response;
        }
        public RegistrationCreateResp RegistrationCreate_Smart(Online.Registration.DAL.Models.Registration reg, Customer customer, RegMdlGrpModel regMdlGrpModel,
                                                     List<Address> address, List<RegPgmBdlPkgComp> regPBPCs, RegStatus regStatus,
                                                     List<RegSuppLine> RegSuppLines, List<RegPgmBdlPkgComp> RegSuppLineVASes, List<PackageComponents> PackageComponents, CMSSComplain cms, List<RegSmartComponents> RegSmartComponents, List<WaiverComponents> RegWaiverComponents)
        {
            var request = new RegistrationCreateReq_smart()
            {
                Registration = reg,
                Customer = customer,
                RegMdlGrpModel = regMdlGrpModel,
                Addresses = address.ToArray(),
                RegPgmBdlPkgComps = regPBPCs.ToArray(),
                RegStatus = regStatus,
                RegSuppLines = RegSuppLines.ToArray(),
                RegSuppLineVASes = RegSuppLineVASes.ToArray(),
                Packages = PackageComponents.ToArray(),
                CMSSID = cms,
                RegSmartComponents = RegSmartComponents.ToArray(),
                RegWaiverComponents = RegWaiverComponents.ToArray()
                //CustPhotos = SplitStringIntoBlocks(reg.CustomerPhoto,100000).ToArray()
            };

            var response = _client.RegistrationCreate_Smart(request);

            return response;
        }
        #region Cancel Reason
        public int RegistrationCancelCreate(RegistrationCancelReason oReq)
        {           
            RegistrationCancelReasonResp resp = null;
            RegistrationCancelReasonReq CancelRegistrationReq = new RegistrationCancelReasonReq();
            CancelRegistrationReq.RegistrationCancelReason = oReq;
            resp = _client.RegistrationCancelCreate(CancelRegistrationReq);
            return resp.ID;
        }
        public IEnumerable<RegistrationCancellationReasons> RegistrationCancellationReason()
        {
            var response = _client.RegCancellationGet();
            return response.RegistrationCancellationReasons;

        }
        #endregion

        /*Chetan added for displaying the reson for failed transcation*/
        //public Online.Registration.DAL.Models.KenanaLogDetails KenanLogDetailsGet(int RegistrationID)
        //{
        //    var KenanLogDetails = KenanLogDetailsGet(new int[] { RegistrationID });
        //    return KenanLogDetails == null ? null : KenanLogDetails.SingleOrDefault();
        //}
        //public IEnumerable<Online.Registration.DAL.Models.KenanaLogDetails> KenanLogDetailsGet(IEnumerable<int> RegistrationIDs)
        //{
        //    if (RegistrationIDs == null)
        //        return new List<Online.Registration.DAL.Models.KenanaLogDetails>(); ;

        //    var request = new KenanLogDetailsGetReq()
        //    {
        //        IDs = RegistrationIDs.ToArray()
        //    };

        //    var response = _client.KenanLogDetailsGet(request);

        //    return response.KenanaLogDetails;
        //}
        //public IEnumerable<Online.Registration.DAL.Models.KenanaLogDetails> KenanLogHistoryDetailsGet(IEnumerable<int> RegistrationIDs)
        //{
        //    if (RegistrationIDs == null)
        //        return new List<Online.Registration.DAL.Models.KenanaLogDetails>(); ;

        //    var request = new KenanLogHistoryDetailsGetReq()
        //    {
        //        IDs = RegistrationIDs.ToArray()
        //    };

        //    var response = _client.KenanLogHistoryDetailsGet(request);

        //    return response.KenanaLogDetails;
        //}
        public List<RegAttributes> RegAttributesGetByRegID(int _regID)
        {
            List<RegAttributes> result = new List<RegAttributes>();
            try
            {
                result = _client.RegAttributesGetByRegID(_regID).ToList();
            }
            catch (Exception ex)
            {
                result = null;
            }
            return result;
        }

    }
}
