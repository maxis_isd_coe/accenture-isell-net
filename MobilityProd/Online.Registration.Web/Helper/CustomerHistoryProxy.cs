﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.SubscriberHistoryService;

namespace Online.Registration.Web.Helper
{
    public class CustomerHistoryProxy : ServiceProxyBase<SubscriberHistoryClient>
    {
        //
        // GET: /CustomerHistory/

         public CustomerHistoryProxy()
        {
            _client = new SubscriberHistoryClient();
        }

        public GetNewCustomerInfoResponse GetCustomerHistory(GetNewCustomerInfoRequest _request)
        {
            GetNewCustomerInfoResponse response = null;

            try
            {
                response = _client.getNewCustomerInfo(_request);
            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;

        }

    }
}
