﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using Online.Registration.Web.SubscriberICService;
using System.Net;
using System.Xml.Linq;

namespace Online.Registration.Web.Helper
{
    public class retrieveServiceInfoProxy : ServiceProxyBase<SubscriberICServiceClient>
    {
        public static int counter = 0;

        public retrieveServiceInfoProxy()
        {
            _client = new SubscriberICServiceClient();
        }


        //public Dictionary<string, string> GetPrepaidSubscriberDetails(List<string> MSISDNList)
        //{
        //    var result = new Dictionary<string, string>();
        //    try
        //    {
        //        result = _client.GetPrepaidSubscriberDetails(MSISDNList);
        //    }
        //    catch (Exception ex)
        //    {
        //        Util.LogException(ex);
        //    }
        //    return result;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Msisdn"></param>
        /// <returns></returns>
        public retrieveAcctListByICResponse retrieveAllDetails(string serviceException, string loggedUserName, string icType, string icValue, string kenancode, string userName = "", string password = "", string postUrl = "", bool isSupAdmin = false)
        {
            retrieveAcctListByICResponse response = null;
            try
            {
                response = _client.retrieveAllDetails(serviceException, loggedUserName, icType, icValue, kenancode, userName, password, postUrl, isSupAdmin);

                //Commented by ravi on comparing with mobility performance code on mar 22 2014 for tfs bug id:1559
                //#region Ristricting AccountType null lines
                //if (response != null && response.itemList != null && response.itemList.Any())
                //response.itemList = response.itemList.Where(a => a.ServiceInfoResponse != null && a.ServiceInfoResponse.prinSuppInd != string.Empty).ToList();
                //#endregion
                //Commented by ravi on comparing with mobility performance code on mar 22 2014 for tfs bug id:1559 ends here
            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;
        }

        public retrieveAcctListByICResponse retrieveUCCSInfo(string IDCardType, string IDCardNumber, retrieveAcctListByICResponse AcctListByICResponse)
        {
            var retrieveAcctListByICResponse = new retrieveAcctListByICResponse();
            try
            {
                var cardType = string.Empty;

                switch(IDCardType)
                {
                    case "NEWIC":
                        cardType = "NEWICNO";
                        break;
                    case "PASSPORT":
                        cardType = "PASSPORTNO";
                        break;
                    case "OLDIC":
                        cardType = "OLDICNO";
                        break;
                    case "OTHERIC":
                        cardType = "OTHERID";
                        break;
                    default:
                        break;
                }

                if (!string.IsNullOrEmpty(cardType))
                {
                    retrieveAcctListByICResponse = _client.RetrieveUCCSInfo(cardType, IDCardNumber, AcctListByICResponse);
                }
                else
                {
                    retrieveAcctListByICResponse = AcctListByICResponse;
                }
            }
            catch (Exception ex)
            {
                retrieveAcctListByICResponse = AcctListByICResponse;
            }
            return retrieveAcctListByICResponse;
        }

        /// <summary>
        /// Retrieves the development service info with dummy data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>List<retrieveServiceInfoResponse></returns>
        public SubscriberRetrieveServiceInfoResponse retrieveDevelopmentServiceInfo(SubscriberRetrieveServiceInfoRequest request)
        {
            SubscriberRetrieveServiceInfoResponse response = null;

            if (counter <= 1)
            {
                if (counter == 0)
                    ///Non GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "HSDPA",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "EXPIRED HSDPA",
                        postPreInd = "POST",
                        pukCode = "21565214",
                        serviceStatus = "A"
                    };
                if (counter == 1)
                    ///GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A"
                    };
                if (counter == 2)
                    ///GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A"
                    };
                if (counter == 3)
                    ///GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A"
                    };
                if (counter == 4)
                    ///Non GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A"
                    };
            }
            counter++;
            return response;
        }


       

        public retrieveDirectoryNumberResponse retrieveMobilenos(string salesChannelId, string inventoryTypeId, int rowNum)
        {
            retrieveDirectoryNumberResponse resp = null;
            try
            {
                resp = _client.retrieveMobilenos(salesChannelId, inventoryTypeId, rowNum);
            }
            catch (TimeoutException)
            {
                resp.MsgCode = "1";
                resp.MsgDesc = "Timeout Occured";
            }
            catch (Exception)
            {

                resp = null;
            }
            return resp;
        }

        // Commented for MISM Virtual numbers flow
       
        //public string retriveVirtualNumbers()
        //{
        //    string selectedMobileNo = string.Empty;
        //    var respmobno = new retrieveDirectoryNumberResponse();
        //    respmobno = retrieveMobilenos("2559", "680", 10);
        //    if(respmobno!=null)
        //    if (respmobno.itemList[0].ExternalId != null)
        //        selectedMobileNo = respmobno.itemList[0].ExternalId;
        //    return selectedMobileNo;
        //}
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SubscriberRetrieveServiceInfoResponse retrieveServiceInfo(SubscriberRetrieveServiceInfoRequest request)
        {
            SubscriberRetrieveServiceInfoResponse response = null;

            try
            {
                response=_client.retrieveServiceInfo(request);
            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="icType"></param>
        /// <param name="icValue"></param>
        /// <param name="serviceException">ServiceException containing any internal exception returned from called service.</param>
        /// <returns></returns>
        public retrieveAcctListByICResponse retrieveAcctListByIC(string icType, string icValue,string loggedUserName, ref string serviceException)
        {
            retrieveAcctListByICResponse response = null;

            try
            {
                response = _client.retrieveAcctListByIC(icType, icValue,loggedUserName ,ref serviceException);
            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;
        }

        #region Added by Sindhu on 16th June 2013 
        public IList<PackageModel> retrievePackageDetls(string MSISDN, string kenancode,string userName , string password , string postUrl)
        {
            IList<PackageModel> response = null;

            try
            {
                response = _client.retrievePackageDetls(MSISDN, kenancode, userName, password, postUrl);
            }
            catch (Exception ex)
            {
                response = null;
            }
            return response;
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Msisdn"></param>
        /// <returns></returns>
        public CustomizedCustomer retrieveSubscriberDetls(string Msisdn, string loggedUserName,bool isSupAdmin)
        {
            CustomizedCustomer CustomizedCustomer = null;
            try
            {
                CustomizedCustomer = _client.retrieveSubscriberDetls(Msisdn, loggedUserName, isSupAdmin);
            }
            catch (Exception ex)
            {
                CustomizedCustomer = null;
            }

            return CustomizedCustomer;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Msisdn"></param>
        /// <returns></returns>
        //public CustomizedCustomerNew retrieveAllDetails(string Msisdn, string loggedUserName, bool isSupAdmin, SubscriberRetrieveServiceInfoRequest request)
        //{
        //    CustomizedCustomerNew CustomizedCustomer = null;
        //    try
        //    {
        //        CustomizedCustomer = _client.retrieveAllDetails(Msisdn, loggedUserName, isSupAdmin, request);
        //    }
        //    catch (Exception ex)
        //    {
        //        CustomizedCustomer = null;
        //    }

        //    return CustomizedCustomer;
        //}


        /// <summary>
        /// Added by sindhu on 25th Jul 2013 to get search results by kenan number
        /// </summary>
        /// <param name="Msisdn"></param>
        /// <returns></returns>
        public CustomizedCustomer retrieveSubscriberDetlsbyKenan(string transType,string transValue, string loggedUserName, bool isSupAdmin)
        {
            CustomizedCustomer CustomizedCustomer = null;
            try
            {
                CustomizedCustomer = _client.retrieveSubscriberDetlsbyKenan(transType, transValue, loggedUserName, isSupAdmin);
            }
            catch (Exception ex)
            {
                CustomizedCustomer = null;
            }

            return CustomizedCustomer;
        }



        #region "Log PPID Req and Res"
        /// <summary>
        /// Save API call status.
        /// </summary>
        /// <param name="oReq">The o req.</param>
        /// <returns>Int</returns>
        public SaveCallStatusResp APISaveCallStatus(SaveCallStatusReq oReq)
        {
            var SaveCallStatusReqval = new SaveCallStatusReq()
            {
                tblAPICallStatusMessagesval = oReq.tblAPICallStatusMessagesval
            };

            var resp = _client.APISaveCallStatus(SaveCallStatusReqval);

            return resp;
        }
        #endregion

        #region Added by Sindhu on 04-Jun-2013 to create method for retrieveBillngInfoResponse
        public Dictionary<string,string> retrieveBillngInfo(List<string> acctNo)
        {
            Dictionary<string,string> response = null;
            try
            {
                response = _client.retrieveBillingInfoAll(acctNo);
            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;
        }

        public retrieveBillngInfoResponse retrieveBillngInfo(string acctNo)
        {
            retrieveBillngInfoResponse response = null;
            try
            {
                response = _client.retrieveBillingInfo(acctNo);
            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;
        }
        #endregion

       //Added by himansu for sim replacement
        public retrieveSimDetlsResponse retreiveSIMDetls(string msisdn)
        {
            if (string.IsNullOrEmpty(msisdn))
                throw new ArgumentNullException();

            var response = _client.retrieveSimDetails(msisdn);

            return response;

        }

        public typeChkFxVasDependencyResponse ChkFxVasDependency(typeChkFxVasDependencyRequest req)
        {
            typeChkFxVasDependencyResponse objResponce = new typeChkFxVasDependencyResponse();
            try
            {
                objResponce = _client.ChkFxVasDependency(req);

            }
            catch (Exception)
            {

            }
            return objResponce;
        }

        public AvalablePackages RetrieveEligiblePackages(int currentPackageId, int mktCode, int acctCategoryId, int rateClass)
        {
            AvalablePackages avalablePackages = null;
            try
            {
                avalablePackages = _client.RetrieveEligiblePackages(currentPackageId, mktCode, acctCategoryId, rateClass);
            }
            catch (Exception ex)
            {
                avalablePackages = null;
            }

            return avalablePackages;
        }
        public List<reassingVases> ReassignVas(string fxAccNo, string fxSubscrNo, string fxSubscrNoResets, List<packagePairs> lstPackagePairs)
        {
            List<reassingVases> lstReAssignVases = null;
            try
            {
                ReassignVasRequest objReassignVasRequest = new ReassignVasRequest();
                objReassignVasRequest.FxAccNo = fxAccNo;
                objReassignVasRequest.FxSubscrNo = fxSubscrNo;
                objReassignVasRequest.FxSubscrNoResets = fxSubscrNoResets;
                objReassignVasRequest.packagePairsList = lstPackagePairs;

                ReassignVasResponce objReassignVasResponce = new ReassignVasResponce();
                objReassignVasResponce = _client.ReassignVas(objReassignVasRequest);
                lstReAssignVases = objReassignVasResponce.reassingVasesList;
            }
            catch
            {
                lstReAssignVases = null;
            }
            return lstReAssignVases;
        }

        #region smart related methods

        public List<SubscriberICService.PackageModel> retrievePackageDetails(string msisdn, string kenacode, string userName, string password)
        {
            List<SubscriberICService.PackageModel> pkg = new List<PackageModel>();

            try
            {
                pkg = _client.retrievePackageDetls(msisdn, kenacode, userName, password, "");
                


            }
            catch (Exception ex)
            {
            }

            return pkg;
        }

        #endregion




        /// <summary>
        /// Method used to Retrieve Master account Information
        /// </summary>
        /// <param name="TranxType">Transaction Type</param>
        /// <param name="TranxValue">Transaction Value</param>
        /// <returns>typeRetrieveMasterAcctListResponse Object</returns>
        public typeRetrieveMasterAcctListResponse RetrieveMasterAccountList(string TranxType, string TranxValue)
        {
            return _client.RetrieveMasterAccountList(TranxType, TranxValue);
        } 

        /// <summary>
        /// Method used to Retrieve Parent account Information
        /// </summary>
        /// <param name="MasterAccountNumber">Master Account Number</param>
        /// <returns>typeRetrieveParentAcctListResponse Object</returns>
        public typeRetrieveParentAcctListResponse RetrieveParentAccountList(string MasterAccountNumber)
        {
            return _client.RetrieveParentAccountList(MasterAccountNumber);
        }

        #region extraten methods
        #region [Extra Ten]
        public ExtraTenRetreivalResponse RetrieveExtraTenForAddEdit(string CorridorId, string ExternalId)
        {
            typeRetrieveExtraTenRequest request = new typeRetrieveExtraTenRequest();
            request.corridorIdField = CorridorId;
            request.externalIdField = ExternalId;
            var response = _client.RetrieveExtraTen(request);
            return response;
        }

        public ExtraTenUpdateResponse ProcessExtraTenForAddEdit(typeProcessExtraTenRequest extraTenRequest, int regID)
        {
            var response = _client.ProcessExtraTen(extraTenRequest, regID);
            return response;
        }
        #endregion    

        #endregion

        public CustomizedCustomer retrieveSubscriberDetlsByAcctNo(string acctNo, string loggedUserName, bool isSupAdmin)
        {
            return _client.retrieveSubscriberDetlsByAcctNo(acctNo, loggedUserName, isSupAdmin);
        }
        public AccountDetails RetrieveAccountDetails(string acctNo)
        {
            return _client.RetrieveAccountDetails(acctNo);
        }
    }
}
