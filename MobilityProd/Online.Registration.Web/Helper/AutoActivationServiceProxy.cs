﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.AutoActivationSvc;
using log4net;

namespace Online.Registration.Web.Helper
{
    public class AutoActivationServiceProxy:ServiceProxyBase<AutoActivationClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(AutoActivationServiceProxy));
        public string LastErrorMessage { get; set; }

        public AutoActivationServiceProxy()
        {
            _client = new AutoActivationClient();
        }

        //public bool SendPaymentDetails(int iSellOrderID)
        //{
        //    bool response = false;
        //    response = _client.SendPaymentDetails(iSellOrderID);
        //    return response;
        //}

        public POSStatusResult UpdateWebPOSStatus(string LoginID, string MsgCode, string MsgDesc, string OrderID)
        {
            POSStatusResult response = new POSStatusResult();
            response    =    _client.UpdateWebPOSStatus(LoginID, MsgCode, MsgDesc, OrderID);
             return response;
        }

		public bool KenanAccountFulfill(int regTypeId, string regId)
		{
			return _client.KenanAccountFulfill(regTypeId, regId);
		}

		public POSStatusResult TriggerEmail(int regID)
		{
			POSStatusResult response = new POSStatusResult();
			response = _client.TriggerEmail(regID);
			
			return response;
		}

    }
}