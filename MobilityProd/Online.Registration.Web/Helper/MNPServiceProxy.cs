﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.MNPSvc;

using log4net;
using Online.Registration.DAL.Models;

namespace Online.Registration.Web.Helper
{
    public class MNPResubmitServiceProxy : ServiceProxyBase<MNPResubmitClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(MNPResubmitServiceProxy));
        public string LastErrorMessage { get; set; }

        public MNPResubmitServiceProxy()
        {
            _client = new MNPResubmitClient();
        }


        public  ResubmitOrderCreationMNPResponse ResubmitCreateCenterOrderMNP(ResubmitOrderCreationRequestMNP request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.ResubmitCreateCenterOrderMNP(request);
            
            return response;
        }

    }
}