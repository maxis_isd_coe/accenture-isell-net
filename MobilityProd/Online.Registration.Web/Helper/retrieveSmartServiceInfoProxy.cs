﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
//using Online.Registration.Web.SmartRegistrationService;
using Online.Registration.Web.RegistrationSvc;
using System.Net;
using System.Xml.Linq;
//using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.SubscriberICService;

namespace Online.Registration.Web.Helper
{
    public class retrieveSmartServiceInfoProxy : ServiceProxyBase<SubscriberICService.SubscriberICServiceClient>
    {
        public static int counter = 0;

        public retrieveSmartServiceInfoProxy()
        {
            _client = new SubscriberICService.SubscriberICServiceClient();
        }
        /// <summary>
        /// Retrieves the development service info with dummy data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>List<retrieveServiceInfoResponse></returns>
        public SubscriberICService.SubscriberRetrieveServiceInfoResponse retrieveDevelopmentServiceInfo(SubscriberICService.SubscriberRetrieveServiceInfoResponse request)
        {
            SubscriberICService.SubscriberRetrieveServiceInfoResponse response = null;

            if (counter <= 1)
            {
                if (counter == 0)
                    ///Non GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "HSDPA",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "EXPIRED HSDPA",
                        postPreInd = "POST",
                        pukCode = "21565214",
                        serviceStatus = "A"
                    };
                if (counter == 1)
                    ///GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A"
                    };
                if (counter == 2)
                    ///GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A"
                    };
                if (counter == 3)
                    ///GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A"
                    };
                if (counter == 4)
                    ///Non GSM
                    response = new SubscriberRetrieveServiceInfoResponse
                    {
                        lob = "POSTGSM",
                        mnpInd = "N",
                        msgCode = "0",
                        msgDesc = "Valid Subscriber",
                        packageName = "Mobile Employee Plan",
                        postPreInd = "POST",
                        prinSuppInd = "P",
                        pukCode = "53733518",
                        serviceStatus = "A"
                    };
            }
            counter++;
            return response;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SubscriberRetrieveServiceInfoResponse retrieveServiceInfo(SubscriberRetrieveServiceInfoRequest request)
        {
            SubscriberRetrieveServiceInfoResponse response = null;

            try
            {
                response = _client.retrieveServiceInfo(request);
            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="icType"></param>
        /// <param name="icValue"></param>
        /// <param name="serviceException">ServiceException containing any internal exception returned from called service.</param>
        /// <returns></returns>
        public retrieveAcctListByICResponse retrieveAcctListByIC(string icType, string icValue, string loggedUserName, ref string serviceException)
        {
            retrieveAcctListByICResponse response = null;

            try
            {
                //response = _client._SmartretrieveAcctListByIC(icType, icValue, loggedUserName, ref serviceException);
                response = _client.retrieveAcctListByIC(icType, icValue, loggedUserName, ref serviceException);


            }
            catch (Exception ex)
            {
                response = null;
                serviceException = ex.Message;
            }

            return response;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Msisdn"></param>
        /// <returns></returns>
        /// 123456
        public CustomizedCustomer retrieveSubscriberDetls(string Msisdn, string loggedUserName, ref string serviceException)
        {
            CustomizedCustomer CustomizedCustomer = null;
            try
            {
                //CustomizedCustomer = _client._SmartretrieveSubscriberDetls(Msisdn, loggedUserName, ref serviceException);
                CustomizedCustomer = _client.retrieveSubscriberDetls(Msisdn, loggedUserName, false);
            }
            catch (Exception ex)
            {
                CustomizedCustomer = null;
                serviceException = ex.Message;
            }

            return CustomizedCustomer;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPackageId"></param>
        /// <param name="mktCode"></param>
        /// <param name="acctCategoryId"></param>
        /// <param name="rateClass"></param>
        /// <returns></returns>
        public AvalablePackages RetrieveEligiblePackages(int currentPackageId, int mktCode, int acctCategoryId, int rateClass)
        {
            AvalablePackages avalablePackages = null;
            try
            {
                avalablePackages = _client.RetrieveEligiblePackages(currentPackageId, mktCode, acctCategoryId, rateClass);
            }
            catch (Exception ex)
            {
                avalablePackages = null;
            }

            return avalablePackages;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fxAccNo"></param>
        /// <param name="fxSubscrNo"></param>
        /// <param name="fxSubscrNoResets"></param>
        /// <param name="lstPackagePairs"></param>
        /// <returns></returns>
        public List<reassingVases> ReassignVas(string fxAccNo, string fxSubscrNo, string fxSubscrNoResets, List<packagePairs> lstPackagePairs)
        {
            List<reassingVases> lstReAssignVases = null;
            try
            {
                ReassignVasRequest objReassignVasRequest = new ReassignVasRequest();
                objReassignVasRequest.FxAccNo = fxAccNo;
                objReassignVasRequest.FxSubscrNo = fxSubscrNo;
                objReassignVasRequest.FxSubscrNoResets = fxSubscrNoResets;
                objReassignVasRequest.packagePairsList = lstPackagePairs;

                ReassignVasResponce objReassignVasResponce = new ReassignVasResponce();
                objReassignVasResponce = _client.ReassignVas(objReassignVasRequest);
                lstReAssignVases = objReassignVasResponce.reassingVasesList;
            }
            catch
            {
                lstReAssignVases = null;
            }
            return lstReAssignVases;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public typeChkFxVasDependencyResponse ChkFxVasDependency(typeChkFxVasDependencyRequest req)
        {
            typeChkFxVasDependencyResponse objResponce = new typeChkFxVasDependencyResponse();
            
            try
            {
                objResponce = _client.ChkFxVasDependency(req);

            }
            catch (Exception)
            {

            }
            return objResponce;
        }
        /// <summary>
        /// Retrieving Package Details
        /// </summary>
        /// <param name="msisdn"></param>
        /// <param name="kenacode"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public List<SubscriberICService.PackageModel> retrievePackageDetails(string msisdn, string kenacode, string userName, string password)
        {
            List<SubscriberICService.PackageModel> pkg = new List<PackageModel>();

            try
            {
                pkg = _client.retrievePackageDetls(msisdn, kenacode, userName, password,"");

            }
            catch (Exception ex)
            {
            }

            return pkg;
        }
        public List<SubscriberICService.retrievePenalty> retrievePenalty(string MSISDN, string ExternalID, string SubscriberNo, string SubscriberNoResets, string userName, string password)
        {
            
            List<SubscriberICService.retrievePenalty> pkg = new List<SubscriberICService.retrievePenalty>();

            try
            {
                pkg = _client.retrievePenalty(MSISDN, ExternalID, SubscriberNo, SubscriberNoResets, userName, password);
                if (pkg != null && pkg.Count > 0)
                {
                    pkg = pkg.GroupBy(sing => sing.contactId).Select(f => f.First()).ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return pkg;
        }
        public List<SubscriberICService.retrievePenaltyByMSISDN> retrievePenalty_Smart(string MSISDN, string ExternalID, string SubscriberNo, string SubscriberNoResets)
        {
            List<SubscriberICService.retrievePenaltyByMSISDN> pkg = new List<SubscriberICService.retrievePenaltyByMSISDN>();

            try
            {
                pkg = _client.retrievePenalty_Smart(MSISDN, ExternalID, SubscriberNo, SubscriberNoResets);


            }
            catch (Exception ex)
            {
            }

            return pkg;
        }   
    }
}