﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Configuration;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using SNT.Utility;
using Online.Registration.Web.Models;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ContentRepository;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.UserSvc;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.Properties;
using System.Web.Security;
using Online.Web.Helper;



namespace Online.Registration.Web.Helper
{
    public class BREChecker
    {
        public static BusinessRuleResponse validate(BusinessRuleRequest req)
        {
            BusinessRuleResponse resp = new BusinessRuleResponse();

            using (var proxy = new KenanServiceProxy())
            {
                resp = proxy.BusinessRuleCheckNew(req);
            }

            return resp;
        }

        public static bool MasterCardValidation(string cardNumber, out string error)
        {
            error = "";
            var length = Settings.Default.MasterCardLength;
            if (cardNumber.Length != length)
            {
                error = ErrorMsg.Registration.CardLength;
                return false;
            }

            if (cardNumber[0] == '5' && (cardNumber[1] == '1' || cardNumber[1] == '2' || cardNumber[1] == '3' || cardNumber[1] == '4' || cardNumber[1] == '5'))
            {
                ;
            }
            else
            {
                error = ErrorMsg.Registration.CardError;
                return false;
            }
            
            var rangeStr = Settings.Default.MasterCardRange;
            var rangeArr = rangeStr.Split(',');

            var start = Int64.Parse(rangeArr[0]);
            var end = Int64.Parse(rangeArr[1]);
            var cardNo = Int64.Parse(cardNumber);

            return CreditCardValidation(cardNumber, length, start, end, out error);
        }

        public static bool VisaCardValidation(string cardNumber, out string error)
        {
            error = "";
            var cardNumberLength = cardNumber.Length.ToString();
            var lengthStr = Settings.Default.VisaCardLength;
            var lengthList = lengthStr.Split(',').ToList();

            if (!lengthList.Contains(cardNumberLength))
            {
                error = ErrorMsg.Registration.CardLength;
                return false;
            }

            if (cardNumber[0] != '4' )
            {
                error = ErrorMsg.Registration.CardError;
                return false;
            }

            var rangeStr = Settings.Default.VisaCardRange;
            var rangeArr = rangeStr.Split(',');

            var start = Int64.Parse(rangeArr[0]);
            var end = Int64.Parse(rangeArr[1]);

            return CreditCardValidation(cardNumber, cardNumber.Length, start, end, out error);
        }

        public static bool AmexCardValidation(string cardNumber, out string error)
        {
            error = "";
            var length = Settings.Default.AmexCardLength;

            if (cardNumber.Length != length)
            {
                error = ErrorMsg.Registration.CardLength;
                return false;
            }
            // 34 / 37
            if (cardNumber[0] == '3' && (cardNumber[1] == '4' || cardNumber[1] == '7'))
            {
                ;
            }
            else
            {
                error = ErrorMsg.Registration.CardError;
                return false;
            }

            var rangeStr = Settings.Default.AmexCardRange;         //.DinersCardRange;
            var rangeArr = rangeStr.Split(',');

            var start = Int64.Parse(rangeArr[0]);
            var end = Int64.Parse(rangeArr[1]);
            var cardNo = Int64.Parse(cardNumber);

            return CreditCardValidation(cardNumber, length, start, end, out error);
        }

        public static bool DinersCardValidation(string cardNumber, out string error)
        {
            error = "";
            var cardNumberLength = cardNumber.Length.ToString();
            var lengthStr = Settings.Default.DinersLength;
            var lengthList = lengthStr.Split(',').ToList();

            if (!lengthList.Contains(cardNumberLength))
            {
                error = ErrorMsg.Registration.CardLength;
                return false;
            }
            // 36 / 38 / 30
            if (cardNumber[0] == '3' && (cardNumber[1] == '6' || cardNumber[1] == '8' || cardNumber[1] == '0'))
            {
                ;
            }// 54 / 55
            else if (cardNumber[0] == '5' && (cardNumber[1] == '4' || cardNumber[1] == '5'))
            {
                ;
            }
            else
            {
                error = ErrorMsg.Registration.CardError;
                return false;
            }

            var rangeStr = Settings.Default.DinersCardRange;         //.AmexCardRange;
            var rangeArr = rangeStr.Split(',');

            var start = Int64.Parse(rangeArr[0]);
            var end = Int64.Parse(rangeArr[1]);
            var cardNo = Int64.Parse(cardNumber);

            return CreditCardValidation(cardNumber, cardNumber.Length, start, end, out error);
        }

        private static bool CreditCardValidation(string cardNumber, int length, Int64 start, Int64 end, out string error)
        {
            error = "";
            if (cardNumber.Length != length)
            {
                error = ErrorMsg.Registration.CardLength;
                return false;
            }

            var cardNo = Int64.Parse(cardNumber);

            if (cardNo < start || cardNo > end)
            {
                error = ErrorMsg.Registration.CardRange;
                return false;
            }

            if (IsValidCreditCard(cardNumber))
            {
                return true;
            }
            else
            {
                error = ErrorMsg.Registration.CardError;
                return false;
            }
        }

        private static bool IsValidCreditCard(string cardNumber)
        {
            int sumOfDigits = cardNumber.Where((e) => e >= '0' && e <= '9')
                    .Reverse()
                    .Select((e, i) => ((int)e - 48) * (i % 2 == 0 ? 1 : 2))
                    .Sum((e) => e / 10 + e % 10);

         
            return sumOfDigits % 10 == 0;    
        }
    }
}