﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.Helper;
using Online.Registration.Web.SubscriberICService;

namespace Online.Registration.Web.Helper
{
    /// <summary>
    /// Session manager holds all possible session keys. Any page/control that needs to access session variables should 
    /// use this class.
    /// "Keys" Enum present in this class should be used to declare all Session Variables.
    /// All Session variable access should be done through "Public Properties{get/set methods}
    /// </summary>
    /// <remarks></remarks>
    // Developer: [Coding Standard] "IAMSession" should be used with in this class only.
    // Developer: [Coding Standard] SessionManager should be used in web pages / controls to access "IAMSession".
    // Developer: [Coding Standard] "Keys" Enum present in this class should be used to declare all Session Variables.
    // Developer: [Coding Standard] All Session variable access should be done through "Public Properties{get/set methods}".
    public static class SessionManager
    {
        // Developer: Creating Single instance for the Application.
        /// <summary>
        /// Holds the session values
        /// </summary>
        internal static IAMSession IAMSession = new IAMSession();

        /// <summary>
        /// Gets a value indicating whether session exist or not.
        /// </summary>
        /// <remarks></remarks>
        public static bool IsSessionExists
        {
            get
            {
                return IAMSession.IsSessionExists;
            }
        }
        /// <summary>
        /// Gets the session ID.
        /// </summary>
        /// <remarks></remarks>
        public static string SessionID
        {
            get
            {
                return IAMSession.SessionID;
            }
        }
        /// <summary>
        /// Removes the specified key.
        /// </summary>
        /// <param name="Key">The key.</param>
        public static void Remove(string Key)
        {
            IAMSession.Remove(Key);
        }
        /// <summary>
        /// Abandons the session.
        /// </summary>
        /// <remarks></remarks>
        public static void Abandon()
        {
            IAMSession.Abandon();
        }
        /// <summary>
        /// Gets the session timeout.
        /// </summary>
        /// <remarks></remarks>
        public static int Timeout
        {
            get
            {
                return IAMSession.Timeout;
            }
        }

        #region Generic Get / Set
        // Developer: Get / Set for dynamic session keys, 
        //for ex: Session["AlertBR:" + requestTypeId.ToString()]

        /// <summary>
        /// Gets the specified session key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static object Get(string key)
        {
            return IAMSession[key];
        }

        /// <summary>
        /// Sets the specified session key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="val">The val.</param>
        /// <remarks></remarks>
        public static void Set(string key, object val)
        {
            IAMSession[key] = val;
        }
        #endregion

        #region Session Properties

        /// <summary>
        /// Gets or sets the session access.
        /// </summary>
        /// <value>
        /// The session access.
        /// </value>
        public static Access SessionAccess
        {
            get
            {
                return (Access)IAMSession[Keys.Access];
            }
            set
            {
                IAMSession[Keys.Access] = value;
            }
        }
        public static string SessionOrgTypeCode
        {
            get
            {
                return (string)IAMSession[Keys.SessionOrgTypeCode];
            }
            set
            {
                IAMSession[Keys.SessionOrgTypeCode] = value;
            }
        }
        public static string MobileRegType
        {
            get
            {
                return (string)IAMSession[Keys.MobileRegType];
            }
            set
            {
                IAMSession[Keys.MobileRegType] = value;
            }
        }
        public static OrderSummaryVM RegMobileReg_OrderSummary
        {
            get
            {
                return (OrderSummaryVM)IAMSession[Keys.RegMobileReg_OrderSummary];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_OrderSummary] = value;
            }
        }
        public static int RegMobileReg_Type
        {
            get
            {
                return (int)IAMSession[Keys.RegMobileReg_Type];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_Type] = value;
            }
        }
        public static List<string> RegMobileReg_VasNames
        {
            get
            {
                return (List<string>)IAMSession[Keys.RegMobileReg_VasNames];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_VasNames] = value;
            }
        }
        public static Online.Registration.Web.UserSvc.GetAPIStatusMessagesResp ApiMessages
        {
            get
            {
                return (Online.Registration.Web.UserSvc.GetAPIStatusMessagesResp)IAMSession[Keys.ApiMessages];
            }
            set
            {
                IAMSession[Keys.ApiMessages] = value;
            }
        }
        public static string MocDefault
        {
            get
            {
                return (string)IAMSession[Keys.MocDefault];
            }
            set
            {
                IAMSession[Keys.MocDefault] = value;
            }
        }
        public static bool Mocandwhitelistcuststatus
        {
            get
            {
                return (bool)IAMSession[Keys.Mocandwhitelistcuststatus];
            }
            set
            {
                IAMSession[Keys.Mocandwhitelistcuststatus] = value;
            }
        }
        public static string RegMobileReg_AgeCheckStatusCode
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_AgeCheckStatusCode];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_AgeCheckStatusCode] = value;
            }
        }
        public static string RegMobileReg_DDMFCheckStatusCode
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_DDMFCheckStatusCode];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_DDMFCheckStatusCode] = value;
            }
        }
        public static string RegMobileReg_AddressCheckStatusCode
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_AddressCheckStatusCode];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_AddressCheckStatusCode] = value;
            }
        }
        public static string RegMobileReg_IsOutstandingCreditCheckStatusCode
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_IsOutstandingCreditCheckStatusCode];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_IsOutstandingCreditCheckStatusCode] = value;
            }
        }
        public static string RegMobileReg_TotalLineCheckStatusCode
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_TotalLineCheckStatusCode];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_TotalLineCheckStatusCode] = value;
            }
        }
        public static string RegMobileReg_PrincipalLineCheckStatusCode
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_PrincipalLineCheckStatusCode];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_PrincipalLineCheckStatusCode] = value;
            }
        }
        public static string RegMobileReg_ContractCheckStatusCode
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_ContractCheckStatusCode];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_ContractCheckStatusCode] = value;
            }
        }
        public static string PPID
        {
            get
            {
                return (string)IAMSession[Keys.PPID];
            }
            set
            {
                IAMSession[Keys.PPID] = value;
            }
        }
        public static string RedirectURL
        {
            get
            {
                return (string)IAMSession[Keys.RedirectURL];
            }
            set
            {
                IAMSession[Keys.RedirectURL] = value;
            }
        }
        public static int? RegMobileReg_PkgPgmBdlPkgCompID
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_PkgPgmBdlPkgCompID];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_PkgPgmBdlPkgCompID] = value;
            }
        }
        public static int? RegMobileReg_ProgramMinAge
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_ProgramMinAge];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_ProgramMinAge] = value;
            }
        }
        public static int? RegMobileReg_ContractID
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_ContractID];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_ContractID] = value;
            }
        }
        public static int? RegMobileReg_DataplanID
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_DataplanID];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_DataplanID] = value;
            }
        }
        public static int? RegMobileReg_VasIDs
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_VasIDs];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_VasIDs] = value;
            }
        }
        public static int? GuidedSales_PkgPgmBdlPkgCompID
        {
            get
            {
                return (int?)IAMSession[Keys.GuidedSales_PkgPgmBdlPkgCompID];
            }
            set
            {
                IAMSession[Keys.GuidedSales_PkgPgmBdlPkgCompID] = value;
            }
        }
        public static string vasmandatoryids
        {
            get
            {
                return (string)IAMSession[Keys.vasmandatoryids];
            }
            set
            {
                IAMSession[Keys.vasmandatoryids] = value;
            }
        }
        public static string intmandatoryidsval
        {
            get
            {
                return (string)IAMSession[Keys.intmandatoryidsval];
            }
            set
            {
                IAMSession[Keys.intmandatoryidsval] = value;
            }
        }
        public static string intdataidsval
        {
            get
            {
                return (string)IAMSession[Keys.intdataidsval];
            }
            set
            {
                IAMSession[Keys.intdataidsval] = value;
            }
        }
        public static string RegMobileReg_DataPkgID
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_DataPkgID];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_DataPkgID] = value;
            }
        }
        public static List<string> RegMobileReg_MobileNo
        {
            get
            {
                return (List<string>)IAMSession[Keys.RegMobileReg_MobileNo];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_MobileNo] = value;
            }
        }
        public static List<SublineVM> RegMobileReg_SublineVM
        {
            get
            {
                return (List<SublineVM>)IAMSession[Keys.RegMobileReg_SublineVM];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_SublineVM] = value;
            }
        }
        public static int? GuidedSales_Type
        {
            get
            {
                return (int?)IAMSession[Keys.GuidedSales_Type];
            }
            set
            {
                IAMSession[Keys.GuidedSales_Type] = value;
            }
        }
        public static int? RegMobileSub_PkgPgmBdlPkgCompID
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileSub_PkgPgmBdlPkgCompID];
            }
            set
            {
                IAMSession[Keys.RegMobileSub_PkgPgmBdlPkgCompID] = value;
            }
        }
        public static List<string> RegMobileReg_SublineSummary
        {
            get
            {
                return (List<string>)IAMSession[Keys.RegMobileReg_SublineSummary];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_SublineSummary] = value;
            }
        }
        public static string RegMobileSub_VasIDs
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileSub_VasIDs];
            }
            set
            {
                IAMSession[Keys.RegMobileSub_VasIDs] = value;
            }
        }        
        public static string RegMobileReg_IDCardType
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_IDCardType];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_IDCardType] = value;
            }
        }
        public static PersonalDetailsVM RegMobileReg_PersonalDetailsVM
        {
            get
            {
                return (PersonalDetailsVM)IAMSession[Keys.RegMobileReg_PersonalDetailsVM];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_PersonalDetailsVM] = value;
            }
        }
        public static string RegMobileReg_IDCardNo
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_IDCardNo];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_IDCardNo] = value;
            }
        }
        public static int? RegMobileReg_MalAdvDeposit
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_MalAdvDeposit];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_MalAdvDeposit] = value;
            }
        }
        public static int? RegMobileReg_OtherAdvDeposit
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_OtherAdvDeposit];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_OtherAdvDeposit] = value;
            }
        }
        public static int? RegMobileReg_MalyDevAdv
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_MalyDevAdv];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_MalyDevAdv] = value;
            }
        }
        public static int? RegMobileReg_MalayPlanAdv
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_MalayPlanAdv];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_MalayPlanAdv] = value;
            }
        }
        public static int? RegMobileReg_MalayDevDeposit
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_MalayDevDeposit];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_MalayDevDeposit] = value;
            }
        }
        public static int? RegMobileReg_MalyPlanDeposit
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_MalyPlanDeposit];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_MalyPlanDeposit] = value;
            }
        }
        public static int? RegMobileReg_OthDevAdv
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_OthDevAdv];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_OthDevAdv] = value;
            }
        }
        public static int? RegMobileReg_OthPlanAdv
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_OthPlanAdv];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_OthPlanAdv] = value;
            }
        }
        public static int? RegMobileReg_OthDevDeposit
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_OthDevDeposit];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_OthDevDeposit] = value;
            }
        }
        public static int? RegMobileReg_OthPlanDeposit
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_OthPlanDeposit];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_OthPlanDeposit] = value;
            }
        }
        public static int? RegMobileReg_BiometricID
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_BiometricID];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_BiometricID] = value;
            }
        }
        public static string RegMobileReg_ResultMessage
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_ResultMessage];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_ResultMessage] = value;
            }
        }
        public static bool RegMobileReg_BiometricVerify
        {
            get
            {
                return (bool)IAMSession[Keys.RegMobileReg_BiometricVerify];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_BiometricVerify] = value;
            }
        }
        public static int? RegMobileReg_SelectedModelImageID
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_SelectedModelImageID];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_SelectedModelImageID] = value;
            }
        }
        public static int? RegMobileReg_SelectedModelID
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_SelectedModelID];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_SelectedModelID] = value;
            }
        }
        public static PhoneVM RegMobileReg_PhoneVM
        {
            get
            {
                return (PhoneVM)IAMSession[Keys.RegMobileReg_PhoneVM];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_PhoneVM] = value;
            }
        }
        public static int? RegID
        {
            get
            {
                return (int?)IAMSession[Keys.RegID];
            }
            set
            {
                IAMSession[Keys.RegID] = value;
            }
        }
        public static Decimal? RegMobileReg_MainDevicePrice
        {
            get
            {
                return (Decimal?)IAMSession[Keys.RegMobileReg_MainDevicePrice];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_MainDevicePrice] = value;
            }
        }
        public static string KenanAccountNo
        {
            get
            {
                return (string)IAMSession[Keys.KenanAccountNo];
            }
            set
            {
                IAMSession[Keys.KenanAccountNo] = value;
            }
        }
        public static retrieveAcctListByICResponse PPIDInfo
        {
            get
            {
                return (retrieveAcctListByICResponse)IAMSession[Keys.PPIDInfo];
            }
            set
            {
                IAMSession[Keys.PPIDInfo] = value;
            }
        }
        public static string AcctExtId
        {
            get
            {
                return (string)IAMSession[Keys.AcctExtId];
            }
            set
            {
                IAMSession[Keys.AcctExtId] = value;
            }
        }
        public static string LoginTime
        {
            get
            {
                return (string)IAMSession[Keys.LoginTime];
            }
            set
            {
                IAMSession[Keys.LoginTime] = value;
            }
        }
        public static int? RegMobileReg_SelectedOptionID
        {
            get
            {
                return (int?)IAMSession[Keys.RegMobileReg_SelectedOptionID];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_SelectedOptionID] = value;
            }
        }
        public static int? GuidedSales_SelectedModelImageID
        {
            get
            {
                return (int?)IAMSession[Keys.GuidedSales_SelectedModelImageID];
            }
            set
            {
                IAMSession[Keys.GuidedSales_SelectedModelImageID] = value;
            }
        }
        public static bool? RegMobileReg_FromSearch
        {
            get
            {
                return (bool?)IAMSession[Keys.RegMobileReg_FromSearch];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_FromSearch] = value;
            }
        }
        public static int? RegStepNo
        {
            get
            {
                return (int?)IAMSession[Keys.RegStepNo];
            }
            set
            {
                IAMSession[Keys.RegStepNo] = value;
            }
        }
        public static string RegMobileReg_SelectedOptionType
        {
            get
            {
                return (string)IAMSession[Keys.RegMobileReg_SelectedOptionType];
            }
            set
            {
                IAMSession[Keys.RegMobileReg_SelectedOptionType] = value;
            }
        }
        public static string MSISDN
        {
            get
            {
                return (string)IAMSession[Keys.MSISDN];
            }
            set
            {
                IAMSession[Keys.MSISDN] = value;
            }
        }
        #endregion

        #region Session keys
        /// <summary>
        /// Session keys
        /// </summary>
        /// <remarks></remarks>
        internal enum Keys
        {
            /// <summary>
            /// AccountController
            /// </summary>
            Access,
            SessionOrgTypeCode,
            /// <summary>
            /// RegistrationController>SelectPlan
            /// </summary>
            MobileRegType,
            RegMobileReg_OrderSummary,
            RegMobileReg_Type,
            RegMobileReg_VasNames,
            /// <summary>
            /// _OrderSummary
            /// </summary>
            ApiMessages,
            MocDefault,
            Mocandwhitelistcuststatus,
            RegMobileReg_AgeCheckStatusCode,
            RegMobileReg_DDMFCheckStatusCode,
            RegMobileReg_AddressCheckStatusCode,
            RegMobileReg_IsOutstandingCreditCheckStatusCode,
            RegMobileReg_TotalLineCheckStatusCode,
            RegMobileReg_PrincipalLineCheckStatusCode,
            RegMobileReg_ContractCheckStatusCode,
            PPID,
            RedirectURL,
            RegMobileReg_TabIndex,
            RegMobileReg_PkgPgmBdlPkgCompID,
            RegMobileReg_ProgramMinAge,
            RegMobileReg_ContractID,
            RegMobileReg_DataplanID,
            RegMobileReg_VasIDs,
            /// <summary>
            /// RegistrationController>MenuType
            /// </summary>
            MenuType,
            GuidedSales_PkgPgmBdlPkgCompID,
            vasmandatoryids,
            intmandatoryidsval,
            intdataidsval,
            RegMobileReg_DataPkgID,
            RegMobileReg_MobileNo,
            RegMobileReg_SublineVM,
            GuidedSales_Type,
            RegMobileSub_PkgPgmBdlPkgCompID,
            RegMobileSub_MobileNo,
            RegMobileSub_VasIDs,
            RegMobileReg_SublineSummary,
            RegMobileReg_IDCardType,
            RegMobileReg_PersonalDetailsVM,
            RegMobileReg_IDCardNo,
            RegMobileReg_MalAdvDeposit,
            RegMobileReg_OtherAdvDeposit,
            RegMobileReg_MalyDevAdv,
            RegMobileReg_MalayPlanAdv,
            RegMobileReg_MalayDevDeposit,
            RegMobileReg_MalyPlanDeposit,
            RegMobileReg_OthDevAdv,
            RegMobileReg_OthPlanAdv,
            RegMobileReg_OthDevDeposit,
            RegMobileReg_OthPlanDeposit,
            RegMobileReg_BiometricID,
            RegMobileReg_ResultMessage,
            RegMobileReg_BiometricVerify,
            RegMobileReg_SelectedModelImageID,
            RegMobileReg_SelectedModelID,
            RegMobileReg_PhoneVM,
            RegID,
            RegMobileReg_MainDevicePrice,
            KenanAccountNo,
            PPIDInfo,
            AcctExtId,
            LoginTime,
            RegMobileReg_SelectedOptionID,
            GuidedSales_SelectedModelImageID,
            RegMobileReg_FromSearch,
            RegStepNo,
            RegMobileReg_SelectedOptionType,
            MSISDN,
        }
        #endregion Session keys
    }
}