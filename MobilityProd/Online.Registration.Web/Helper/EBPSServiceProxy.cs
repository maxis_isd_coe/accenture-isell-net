﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.EBPSSvc;

using log4net;

namespace Online.Registration.Web.Helper
{
    public class EBPSServiceProxy : ServiceProxyBase<EBPSServiceClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(EBPSServiceProxy));
        public string LastErrorMessage { get; set; }

        public EBPSServiceProxy()
        {
            _client = new EBPSServiceClient();
        }

        public GetSubscriptionInfoResponse GetSubscriptionInfo(GetSubscriptionInfoRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            //call the EBPS service
            var response = _client.GetSubscriptionInfo(request);
            LogResponse(response);

            return response;
        }

        public UpdSubscriptionInfoResponse UpdSubscriptionInfo(UpdSubscriptionInfoRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            //call the EBPS service
            var response = _client.UpdSubscriptionInfo(request);
            LogResponse(response);

            return response;
        }

        #region Private Methods

        private void LogResponse(WCFResponse response)
        {
            LastErrorMessage = "";

            if (!response.Success)
            {
                LastErrorMessage = response.Message;

                // Log Error
                Logger.ErrorFormat("Code: {0}|Message: {1}", response.Code, response.Message);
            }
        }

        #endregion
    }
}