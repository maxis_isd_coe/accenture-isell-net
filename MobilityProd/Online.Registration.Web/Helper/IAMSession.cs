﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.Routing;

namespace Online.Registration.Web.Helper
{
    /// <summary>
    /// Driver class for the holding session values. It is essentially 
    /// an abstract layer for accessing session avoiding any inadvertent
    /// access and manipulations
    /// </summary>
    /// <remarks></remarks>

    // Developer: Driver class for "HttpContext.Current.Session"
    // Developer: [Coding Standard] "HttpContext.Current.Session" should be used with in this class only.
    // Developer: [Coding Standard] This class should be internal (compiled as a part of APP_CODE Assembly).
    // Developer: [Coding Standard] Accessible to classes with in APP_CODE, instead use "SessionManager" wrapper class.
    internal sealed class IAMSession
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        /// <remarks></remarks>
        internal IAMSession()
        {
            // Empty constructor.
        }
        /// <summary>
        /// Gets a value indicating whether the session exists or not
        /// </summary>
        /// <remarks></remarks>
        internal bool IsSessionExists
        {
            get
            {
                return Check_IsSessionExists();
            }
        }
        /// <summary>
        /// Gets or sets the Session with the specified key.
        /// </summary>
        /// <remarks></remarks>
        internal object this[string key]
        {
            get
            {
                return Fetch(key);
            }
            set
            {
                Insert(key, value);
            }
        }
        /// <summary>
        /// Gets or sets the Session with the specified key.
        /// </summary>
        /// <remarks></remarks>
        internal object this[SessionManager.Keys key]
        {
            get
            {
                return Fetch(key.ToString());
            }
            set
            {
                Insert(key.ToString(), value);
            }
        }
        /// <summary>
        /// Gets the session ID.
        /// </summary>
        /// <remarks></remarks>
        internal string SessionID
        {
            get
            {
                return CurrentSession == null ? null : CurrentSession.SessionID;
            }
        }
        /// <summary>
        /// Removes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        internal void Remove(string key)
        {
            if (!ReferenceEquals(CurrentSession, null))
                CurrentSession.Remove(key);
        }
        /// <summary>
        /// Abandons the session.
        /// </summary>
        /// <remarks></remarks>
        internal void Abandon()
        {
            if (!ReferenceEquals(CurrentSession,null))
                CurrentSession.Abandon();
        }
        /// <summary>
        /// Gets the session timeout.
        /// </summary>
        /// <remarks></remarks>
        internal int Timeout
        {
            get
            {
                return ReferenceEquals(CurrentSession,null) ? 0 : CurrentSession.Timeout;
            }
        }

        /// <summary>
        /// Fetches the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private object Fetch(string key)
        {
            if (CurrentSession != null)
                return Decrypt(CurrentSession[key]);
            else
                return null;
        }
        /// <summary>
        /// Inserts the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="obj">The obj.</param>
        /// <remarks></remarks>
        private void Insert(string key, Object obj)
        {
            if (!ReferenceEquals(CurrentSession,null))
                CurrentSession[key] = Encrypt(obj);
        }
        /// <summary>
        /// Encrypts the specified obj.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private object Encrypt(object obj)
        {
            // pseudo: Session Custom Encryption Algorithm
            // For now, returning same value.
            return obj;
        }
        /// <summary>
        /// Decrypts the specified obj.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private object Decrypt(object obj)
        {
            // pseudo: Session Custom Decryption Algorithm
            // For now, returning same value.
            return obj;
        }
        /// <summary>
        /// Checks if the session exists.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        private bool Check_IsSessionExists()
        {
            return ((!ReferenceEquals(CurrentSession,null)) && (!ReferenceEquals(Fetch(SessionManager.Keys.Access.ToString()),null))
                && (!ReferenceEquals(Fetch(SessionManager.Keys.LoginTime.ToString()),null)));
        }

        /// <summary>
        /// Gets the current session.
        /// </summary>
        /// <remarks></remarks>
        private HttpSessionState CurrentSession
        {
            get
            {
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                    return HttpContext.Current.Session;
                else
                {
                    // Developer: Exception Handling to be done.
                    return null;
                }
            }
        }
    }
}