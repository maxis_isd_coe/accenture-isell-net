﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using Online.Registration.DAL.Models;
using Online.Registration.Web.breService;
using Online.Registration.DAL.ServiceModels.BreService;

namespace Online.Registration.Web.Helper
{
    /// <summary>
    /// Proxy wrapper class for breService
    /// </summary>
    public class breServiceProxy : ServiceProxyBase<breService_PortTypeClient>
    {
        /// <summary>
        /// Gets or sets the last error message.
        /// </summary>
        /// <value>
        /// The last error message.
        /// </value>
        public string LastErrorMessage { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="breServiceProxy"/> class.
        /// </summary>
        public breServiceProxy()
        {
            _client = new breService_PortTypeClient();
        }

        #region Public Methods
        /// <summary>
        /// Determines whether [is last call failed].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is last call failed]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsLastCallFailed()
        {
            return !string.IsNullOrEmpty(LastErrorMessage);
        }
        /// <summary>
        /// Checks the business rules.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>BusinessRulesResponseType</returns>
        public BusinessRulesResponseType checkBusinessRules(BusinessRulesRequest request)
        {
            eaiHeader eaiHeaderValue = null;            
            BusinessRulesResponseType businessRulesResponseType = new BusinessRulesResponseType();
            checkBusinessRulesRequest ServiceBusinessRulesRequest = null;
            checkBusinessRulesRequestType RuleTypes = null;            
            checkBusinessRulesResponse CheckBusinessRulesResponses = null;

            try
            {
                if (!ReferenceEquals(request, null))
                {
                    ServiceBusinessRulesRequest = new checkBusinessRulesRequest();
                    RuleTypes = new checkBusinessRulesRequestType();

                    //Populate with request header values
                    eaiHeaderValue = new eaiHeader
                    {
                        appId = request.HeaderInfo.AppId,
                        correlationId = request.HeaderInfo.CorrelationId,
                        from = request.HeaderInfo.From,
                        msgId = request.HeaderInfo.MsgId,
                        msgType = request.HeaderInfo.MsgType,
                        timestamp = request.HeaderInfo.Timestamp,
                        to = request.HeaderInfo.To
                    };

                    ServiceBusinessRulesRequest.eaiHeader = eaiHeaderValue;
                    RuleTypes.channelId = request.ChannelId;
                    RuleTypes.ruleNameList = request.RuleNameList;
                    RuleTypes.icNumber = request.IcNumber;
                    RuleTypes.icType = request.IcType;
                    RuleTypes.dateOfBirth = request.DateOfBirth;
                    RuleTypes.postalCode = request.PostalCode;
                    RuleTypes.state = request.State;
                    RuleTypes.kenanExtAcctId = request.KenanExtAcctId;
                    RuleTypes.totalContractsAllowed = request.TotalContractsAllowed;
                    RuleTypes.totalPrincipleLinesAllowed = request.TotalPrincipleLinesAllowed;
                    RuleTypes.totalLinesAllowed = request.TotalLinesAllowed;
                    RuleTypes.totalMsisdnAllowedInContracts = request.TotalMsisdnAllowedInContracts;
                    RuleTypes.totalContracts = request.TotalContracts;

                    List<businessDataType> businessDataTypes = new List<businessDataType>();

                    foreach (BusinessRequestDataType req in request.businessDataList)
                    {
                        businessDataTypes.Add(
                            new businessDataType
                            {
                                dataName = req.dataName,
                                dataValue = req.dataName
                            });
                    }
                    RuleTypes.businessDataList = businessDataTypes.ToArray();
                    //Assign the request object to actual request
                    ServiceBusinessRulesRequest.checkBusinessRulesRequestDetails = RuleTypes;

                    //Initiate the call
                    var headerval = CheckBusinessRulesResponses.eaiHeader;
                    CheckBusinessRulesResponses.checkBusinessRulesResponseDetails = _client.checkBusinessRules(ref headerval, ServiceBusinessRulesRequest.checkBusinessRulesRequestDetails);

                    if (!ReferenceEquals(CheckBusinessRulesResponses, null))
                    {
                        businessRulesResponseType = new BusinessRulesResponseType();
                        if (!ReferenceEquals(CheckBusinessRulesResponses.eaiHeader, null))
                        {
                            businessRulesResponseType.HeaderInfo = new HeaderInfo
                            {
                                AppId = CheckBusinessRulesResponses.eaiHeader.appId,
                                CorrelationId = CheckBusinessRulesResponses.eaiHeader.correlationId,
                                From = CheckBusinessRulesResponses.eaiHeader.from,
                                MsgId = CheckBusinessRulesResponses.eaiHeader.msgId,
                                MsgType = CheckBusinessRulesResponses.eaiHeader.msgType,
                                Timestamp = CheckBusinessRulesResponses.eaiHeader.timestamp,
                                To = CheckBusinessRulesResponses.eaiHeader.to
                            };
                            if (!ReferenceEquals(CheckBusinessRulesResponses.checkBusinessRulesResponseDetails, null))
                            {
                                businessRulesResponseType.MsgCode = CheckBusinessRulesResponses.checkBusinessRulesResponseDetails.msgCode;
                                businessRulesResponseType.MsgDesc = CheckBusinessRulesResponses.checkBusinessRulesResponseDetails.msgDesc;
                                businessRulesResponseType.FailureRuleName = CheckBusinessRulesResponses.checkBusinessRulesResponseDetails.failureRuleName;
                            }
                        }
                    }
                    else
                    {
                        businessRulesResponseType = null;
                    }
                }
                else
                {
                    businessRulesResponseType = null;
                }
            }
            catch
            {
                throw;
            }

            return new BusinessRulesResponseType();
        }
        #endregion Public Methods
    }
}