﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.iContractSvc;

namespace Online.Registration.Web.Helper
{
    public class iContractProxy : ServiceProxyBase<IiContractClient>
    {
        public iContractProxy()
        {
            _client = new IiContractClient();
        }

        public string UploadFiletoIcontract(string Doctype, string regId, string IDcardValue, string MSISDN, string AccNo, string trnType, string StoreID, Byte[] file,string agentCode, string fileName)
        {
            var response = _client.UploadFiletoIcontract(Doctype, regId, IDcardValue, MSISDN, AccNo, trnType,"", StoreID, file,agentCode, fileName);


            return response;
        }
        
        public string UploadFiletoIcontract(iContractReqDto dto)
        {
            return _client.UploadFiletoIcontract(
                dto.DocType,
                dto.RegId,
                dto.IDcardValue,
                dto.MSISDN,
                dto.AccNo,
                dto.TrnType,
                dto.IdTypeDec,
                dto.StoreID,
                dto.File,
                dto.AgentCode,
                dto.FileName
                );
        }

        /// <summary>
        /// Dto object for Web projet usage in IContract
        /// </summary>
        public class iContractReqDto{
            public string DocType { get; set; }

            public string MSISDN { get; set; }

            public string AccNo { get; set; }

            public string TrnType { get; set; }

            public string IdTypeDec { get; set; }

            public string StoreID { get; set; }

            public byte[] File { get; set; }

            public string AgentCode { get; set; }

            public string FileName { get; set; }

            public string RegId { get; set; }

            public string IDcardValue { get; set; }
        }
    }
	
}
