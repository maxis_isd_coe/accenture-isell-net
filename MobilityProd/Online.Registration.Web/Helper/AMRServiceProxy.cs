﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.AMRSvc;

using log4net;

namespace Online.Registration.Web.Helper
{
    public class AMRServiceProxy : ServiceProxyBase<AMRServiceClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(AMRServiceProxy));
        public string LastErrorMessage { get; set; }

        public AMRServiceProxy()
        {
            _client = new AMRServiceClient();
        }

        #region Private Methods

        private void LogResponse(WCFResponse response)
        {
            LastErrorMessage = "";
            if (response != null && string.IsNullOrEmpty(Convert.ToString(response.Success)))
            {
                LastErrorMessage = response.Message;
                // Log Error
                Logger.ErrorFormat("Code: {0}|Message: {1}", response.Code, response.Message);
            }
        }

        #endregion

        public RetrieveMyKadInfoRp RetrieveMyKadInfo(RetrieveMyKadInfoRq request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.RetrieveMyKadInfo(request);
            LogResponse(response);

            return response;
        }
    }
}