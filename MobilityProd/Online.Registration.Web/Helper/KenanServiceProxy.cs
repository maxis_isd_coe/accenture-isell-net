﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.KenanSvc;

using log4net;
using Online.Registration.DAL.Models;


namespace Online.Registration.Web.Helper
{
    public class KenanServiceProxy : ServiceProxyBase<KenanServiceClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(KenanServiceProxy));
        public string LastErrorMessage { get; set; }

        public KenanServiceProxy()
        {
            _client = new KenanServiceClient();
        }

        #region Private Methods

        private void LogResponse(WCFResponse response)
        {
            LastErrorMessage = "";

            //if (!response.Success)
            //{
            //    LastErrorMessage = response.Message;

            //    // Log Error
            //    Logger.ErrorFormat("Code: {0}|Message: {1}", response.Code, response.Message);
            //}
            
                //if (response != null && string.IsNullOrEmpty(Convert.ToString(response.Success)))
                //{
                //    LastErrorMessage = response.Message;
                //    // Log Error
                //     Logger.ErrorFormat("Code: {0}|Message: {1}", response.Code, response.Message);
                //}

            //Reverting to old code as the above code is generating error log without any details. If response.Message,response.Code is coming from service as null then no need to log here,
            //we can find the log details in controller level and we will get proper method name in log file as well for which method we are getting null response.

            if (response != null)
            {
                if (!response.Success)
                {
                    LastErrorMessage = response.Message;

                    // Log Error
                    Logger.ErrorFormat("Code: {0}|Message: {1}", response.Code, response.Message);
                }
            }
           
        }

        #endregion

        #region [Extra Ten]
        public ExtraTenRetreivalResponse RetrieveExtraTen(string CorridorId, string ExternalId)
        {
            typeRetrieveExtraTenRequest request = new typeRetrieveExtraTenRequest();
            request.corridorIdField = CorridorId;
            request.externalIdField = ExternalId;
            var response = _client.RetrieveExtraTen(request);
            return response;
        }

        public ExtraTenUpdateResponse ProcessExtraTen(typeProcessExtraTenRequest extraTenRequest, int regID)
        {
            var response = _client.ProcessExtraTen(extraTenRequest, regID);
            return response;
        }
        #endregion
        public RetrieveBillingDetailsResponse RetrieveBillingDetails(RetrieveBillingDetailsRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.RetrieveBillingDetails(request);
            LogResponse(response);

            return response;
        }

        public RetrieveComponentInfoResponse RetrieveComponentInfo(RetrieveComponentInfoRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.RetrieveComponentInfo(request);
            LogResponse(response);

            return response;
        }

        //public BlacklistCheckResponse BlacklistCheck(BlacklistCheckRequest request)
        //{
        //    if (request == null)
        //        throw new ArgumentNullException();

        //    var response = _client.BlacklistCheck(request);
        //    LogResponse(response);

        //    return response;
        //}

        public RetrieveExtAccDetailsResponse RetrieveExternalAccount(RetrieveExtAccDetailsRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();
            Logger.DebugFormat("RetrieveExternalAccount:before");
            var response = _client.RetrieveExtAccountDetails(request);
            Logger.DebugFormat("RetrieveExternalAccount:after");
            LogResponse(response);

            return response;
        }

        public MobileNoSelectionResponse MobileNoSelection(MobileNoSelectionRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.MobileNoSelection(request);
            LogResponse(response);

            return response;
        }

        public MobileNoUpdateResponse MobileNoUpdate(MobileNoUpdateRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.MobileNoUpdate(request);
            LogResponse(response);

            return response;
        }

        public PostcodeValidationResponse PostcodeValidation(PostcodeValidationRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.PostcodeValidation(request);
            LogResponse(response);

            return response;
        }

        //public OrderCreationResponse KenanAccountCreate(OrderCreationRequest request)
        public bool KenanAccountCreate(OrderCreationRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.KenanAccountCreate(request);
            //LogResponse(response);

            return response;
        }

        public bool KenanAccountFulfill(OrderFulfillRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.KenanAccountFulfill(request);
            //LogResponse(response);

            return response;
        }

        public bool KenanMNPAccountFulfill(MNPtypeFullfillCenterOrderMNP request)
        {
            if (request == null)
                throw new ArgumentNullException();
            var response = _client.KenanMNPAccountFulfill(request);
            return response;
        }

        public bool ProcessAddRemoveVAS(AddRemoveVASReq request, int regId)
        {
            bool status = true;
            if (request == null)
                throw new ArgumentNullException();
            try
            {
                _client.ProcessAddRemoveVAS(request, regId);
            }
            catch (Exception)
            {
                status = false;
                throw;
            }
            return status;

        }
        public UpdateOrderStatusResponse UpdateKenanOrderStatus(UpdateOrderStatusRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.UpdateOrderStatus(request);

            LogResponse(new WCFResponse()
            {
                Code = response.MsgCode,
                Message = response.MsgDesc
            });

            return response;
        }

        public BusinessRuleResponse BusinessRuleCheck(BusinessRuleRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.BusinessRuleCheck(request);
            LogResponse(response);

            return response;
        }

        public TotalLineCheckResponse TotalLineCheck(TotalLineCheckRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.TotalLineCheck(request);
            LogResponse(response);

            return response;
        }

        public BusinessRuleResponse BusinessRuleCheckNew(BusinessRuleRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.BusinessRuleCheckNew(request);
            LogResponse(response);

            return response;
        }

        #region SUTAN ADDED CODE 24rthFeb2013
        /// <summary>
        /// Kenans addition line registration.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public KenanTypeAdditionLineRegistrationResponse KenanAdditionLineRegistration(KenanTypeAdditionLineRegistration request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.KenanAdditionLineRegistration(request);
            LogResponse(response);

            return response;
        }

        /// <summary>
        /// Kenans Add new supp line.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        public KenanNewSuppLineResponse KenanAddNewSuppLine(KenanNewSuppLineRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.KenanAddNewSuppLine(request);
            LogResponse(response);

            return response;
        }
        #endregion SUTAN ADDED CODE 24rthFeb2013

        /// <summary>
        /// Kenan createCenterOrderMNP
        /// </summary>
        /// <param name="request"></param>
        /// <returns>TypeOf(MNPOrderCreationResponse)</returns>
        public MNPOrderCreationResponse MNPcreateCenterOrderMNP(OrderCreationRequestMNP request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.MNPcreateCenterOrderMNP(request);
            LogResponse(response);

            return response;
        }

        /// <summary>
        /// Kenan fulfillCenterOrderMNP
        /// </summary>
        /// <param name="request"></param>
        /// <returns>TypeOf(MNPFulfillCenterOrderMNPResponse)</returns>
        //public MNPFulfillCenterOrderMNPResponse MNPfulfillCenterOrderMNP(MNPtypeFullfillCenterOrderMNP request)
        //{
        //    if (request == null)
        //        throw new ArgumentNullException();

        //    var response = _client.MNPfulfillCenterOrderMNP(request);
        //    LogResponse(response);

        //    return response;
        //}

        public NewRetrieveComponentInfoResponse NewRetrieveComponentInfo(NewRetrieveComponentInfoRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.NewRetrieveComponentInfo(request);
            LogResponse(response);

            return response;
        }

        public bool validateInventoryExtIdService(TypeValidateInventoryExtIdRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.InventoryCheck(request);
            //LogResponse(response);

            return response;
        }

        public bool CreateIMPOSFile(int RegID, string strSIMType = "P")
        {
            bool isCreatedIMPOSFile = false;

            if (RegID > 0)
                isCreatedIMPOSFile = _client.saveFile(RegID, strSIMType);
            return isCreatedIMPOSFile;
        }


        //public ProcessOccSpendLimitResponse ProcessOccSpendLimit(ProcessOccSpendLimitRequest request)
        //{
        //    if (request == null)
        //        throw new ArgumentNullException();

        //    var response = _client.ProcessOccSpendLimit(request);

        //    return response;
        //}

        //public RetrieveOccSpendLimitResponse RetrieveOccSpendLimit(RetrieveOccSpendLimitRequest request)
        //{
        //    if (request == null)
        //        throw new ArgumentNullException();

        //    var response = _client.RetrieveOccSpendLimit(request);

        //    return response;
        //}

        #region SimReplacement

        //Added By Himansu
        public RetrieveSimDetlsResponse retreiveSIMDetls(string msisdn)
        {
            if (string.IsNullOrEmpty(msisdn))
                throw new ArgumentNullException();

            var response = _client.retreiveSIMDetls(msisdn);

            return response;

        }

        public eaiResponseType SwapCenterInventory(int regid)
        {
            if (regid == 0)
                throw new ArgumentNullException();

            var response = _client.SwapCenterInventory(regid);

            return response;

        }

        #endregion

        #region CRP

        public bool CenterOrderContractCreation(CenterOrderContractRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.CenterOrderContractCreationForCRP(request);

            return response;
        }

        public bool CenterOrderContractCreationDeviceCRP(CenterOrderContractRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.CenterOrderContractCreation(request);

            return response;
        }


        #endregion


        #region GetMNPRequest implementation

        public getMNPRequestResponse GetMNPRequest(getMNPRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.GetMNPRequest(request);

            return response;
        }
        #endregion GetMNPRequest implementation

        #region "Preport In Implementation"

        public PreportInResponse PreportInValidationCheck(PreportInRequest _PortInRequest)
        {
            if (_PortInRequest == null)
                throw new ArgumentNullException();
            var _PortInResponse = _client.PreportInValidationCheck(_PortInRequest);

            return _PortInResponse;
        }

        #endregion


        #region MISM

        public KenanTypeAdditionLineRegistrationResponse KenanAdditionLineRegistrationMISMSecondary(KenanTypeAdditionLineRegistration request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.KenanAdditionLineRegistrationMISMSecondary(request);

            return response;
        }

        #endregion

        public List<BusinessRuleResponse> BusinessRuleOneTimeCheck(List<BusinessRuleRequest> breReq)
        {
            if (breReq == null)
                throw new ArgumentNullException();
            List<BusinessRuleResponse> lstBreRes = new List<BusinessRuleResponse>();

            BusinessRuleResponse[] response = _client.BusinessRuleOneTimeCheck(breReq.ToArray());
            lstBreRes = (response.ToArray() as BusinessRuleResponse[]).ToList();

            return lstBreRes;

        }

        public List<BusinessRuleResponse> BusinessRuleOneTimeCheckForWriteOff(List<BusinessRuleRequest> breReq)
        {
            if (breReq == null)
                throw new ArgumentNullException();
            List<BusinessRuleResponse> lstBreRes = new List<BusinessRuleResponse>();

            BusinessRuleResponse[] response = _client.BusinessRuleOneTimeCheckForWriteOff(breReq.ToArray());
            lstBreRes = (response.ToArray() as BusinessRuleResponse[]).ToList();

            return lstBreRes;

        }


        /// <summary>
        /// Device Sale Pos file creation
        /// </summary>
        /// <param name="RegID">registration order id</param>
        /// <returns>bool</returns>
        public bool CreateDeviceSalesPOSFile(int RegID)
        {
            bool isCreatedIMPOSFile = false;

            if (RegID > 0)
            {
                isCreatedIMPOSFile = _client.CreateDeviceSalesPOSFile(RegID);
            }
            return isCreatedIMPOSFile;
        }


        /// <summary>
        /// SIM Replacement Pos file creation
        /// </summary>
        /// <param name="RegID">registration order id</param>
        /// <returns>bool</returns>
        public bool CreateSIMReplacementPOSFile(int RegID)
        {
            bool isCreatedIMPOSFile = false;

            if (RegID > 0)
            {
                isCreatedIMPOSFile = _client.CreateSIMReplacementPOSFile(RegID);
            }
            return isCreatedIMPOSFile;
        }


        /// <summary>
        /// RetrieveInventoryInfo
        /// </summary>
        /// <param name="Msisdn">Msisdn</param>
        /// <returns>KenanSvc.typeRetrieveInventoryDetlsResponse</returns>
        public KenanSvc.typeRetrieveInventoryDetlsResponse RetrieveInventoryInfo(string Msisdn)
        {
            return _client.RetrieveInventoryInfo(Msisdn);
        }


        public string TenureCheck()
        {

            string result;
            result = _client.TenureCheck();
            return result;

        }
        public retrievePrepaidSubscriberResponse retrievePrepaidSubscriberInfo(string msisdn)
        {
            retrievePrepaidSubscriberResponse response = null;
            try
            {
                response = _client.retrievePrepaidSubscriberDetails(msisdn);
            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;
        }


        #region "Qsimreplacement flow method"
        public bool ProcessAddComponentsForPrimaryLine(List<int> Ids)
        {
            var result = _client.ProcessAddComponentsForPrimaryLine(Ids.ToArray());
            return result;
        }
        /// <summary>
        /// This method is used for connect/disconnect primary line > 2GB Components.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>bool</returns>
        public bool CenterOrderContractCreationPrimaryLine(CenterOrderContractRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();
            var response = _client.CenterOrderContractCreation(request);
            return response;
        }
        #endregion
        public DisconnectCenterServiceResponse DisconnectComponents(DisconnectCenterServiceRequest request)
        {
            var response = _client.DisconnectComponents(request);
            return response;
        }

        /// <summary>
        /// Retrieve Dealer Information from Kenan, i.e. Sales Channel Id and Sales Channel Name
        /// </summary>
        /// <param name="request">KenanSvc.retrieveDealerInfoRequest</param>
        /// <returns>KenanSvc.retrieveDealerInfoResponse</returns>
        public KenanSvc.retrieveDealerInfoResponse RetrieveDealerInfo(KenanSvc.retrieveDealerInfoRequest request)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.RetrieveDealerInfo(request);
            LogResponse(response);

            return response;
        }

        #region sendTacViaSMSOnline
        public string sendTacViaSMSOnline(string msisdn, string msg)
        {
            string response = _client.sendTacViaSMSOnline(msisdn, msg);
            return response;
        }
        #endregion
    }

}
