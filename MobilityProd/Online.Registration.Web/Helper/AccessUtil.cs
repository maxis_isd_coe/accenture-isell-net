﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Online.Registration.Web.Helper
{
    /// <summary>
    /// Util class for access related function
    /// </summary>
    public class AccessUtil
    {
        public static bool IsSupervisor()
        {
            //check is supervisor
            return Roles.IsUserInRole("MREG_SV");
        }

        public static bool IsCCC()
        {
            //check is CCC
            bool isCCC = false;
            var username = Util.SessionAccess.UserName;
            if (string.IsNullOrEmpty(username)) return false;
            if (!Roles.IsUserInRole("MREG_RA")) return false;//CCC must with Read all access
            using (var proxy = new RegistrationServiceProxy())
            {
                var response = proxy.GetAgentCodeByUserName(username);
                // if agent code same with Dealer / outlet code, the user is CCC
                if (string.Equals(response.AgentCode.Trim(), Util.SessionAccess.User.Org.DealerCode.Trim()))
                    isCCC = true;
            }
            return isCCC;
        }
    }
}