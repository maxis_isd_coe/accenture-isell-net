﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Online.Registration.Smart.DAL.Models;
using Online.Registration.DAL.Models;
//using Online.Registration.Web.SmartCatelogService;
using Online.Registration.Web.CatalogSvc;

namespace Online.Registration.Web.Helper
{
    public class SmartCatelogServiceProxy : ServiceProxyBase<CatalogSvc.CatalogServiceClient>
    {
        public string LastErrorMessage { get; set; }

        public SmartCatelogServiceProxy()
        {
            _client = new CatalogSvc.CatalogServiceClient();
        }

        #region Private Methods
        //unused methods
        //private void LogResponse(string code, string message, string stackTrace = "")
        //{
        //    LastErrorMessage = "";

        //    if (!Util.IsSuccessCode(code))
        //    {
        //        LastErrorMessage = message;

        //        // Log Error
        //        //Logger.ErrorFormat("Code: {0}|Message: {1}|StackTrace: {2}", code, message, stackTrace);
        //    }
        //}

        //unused methods
        //private void LogResponse(WCFResponse response)
        //{
        //    LogResponse(response.Code, response.Message, response.StackTrace);
        //}

        #endregion

        #region Public Methods

        public bool IsLastCallFailed()
        {
            return !string.IsNullOrEmpty(LastErrorMessage);
        }

        #endregion

        #region Brand
        public BrandCreateResp BrandCreate(Brand Brand)
        {
            var brandCreateRq = new BrandCreateReq()
            {
                Brand = Brand
            };

            var resp = _client.BrandCreate(brandCreateRq);

            return resp;
        }
        public void BrandUpdate(Brand Brand)
        {
            var brandUpdateRq = new BrandUpdateReq()
            {
                Brand = Brand
            };

            var resp = _client.BrandUpdate(brandUpdateRq);

        }
        public IEnumerable<int> BrandFind(BrandFindSmart Brand)
        {
            var request = new BrandFindReqSmart()
            {
                BrandFind = Brand
            };

            var response = _client.BrandFindSmart(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.BrandSmart BrandGet(int brandID)
        {
            var brands = BrandGet(new int[] { brandID });
            return brands == null ? null : brands.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.BrandSmart> BrandGet(IEnumerable<int> Brands)
        {
            if (Brands == null)
                return new List<BrandSmart>(); ;

            var request = new BrandGetReqSmart()
            {
                IDs = Brands.ToArray()
            };

            var response = _client.BrandGetSmart(request);

            return response.Brands;
        }
        public static IEnumerable<Online.Registration.DAL.Models.BrandSmart> FindBrand(Online.Registration.DAL.Models.BrandFindSmart Brand)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.BrandGet(proxy.BrandFind(Brand));
            }
        }
        #endregion

        #region Bundle
        public BundleCreateResp BundleCreate(Bundle bundle)
        {
            var bundleCreateRq = new BundleCreateReq()
            {
                Bundle = bundle
            };

            var resp = _client.BundleCreate(bundleCreateRq);

            return resp;
        }
        public void BundleUpdate(Bundle bundle)
        {
            var bundleUpdateRq = new BundleUpdateReq()
            {
                Bundle = bundle
            };

            var resp = _client.BundleUpdate(bundleUpdateRq);

        }
        public IEnumerable<int> BundleFind(BundleFind bundle)
        {
            var request = new BundleFindReq()
            {
                BundleFind = bundle,
            };

            var response = _client.BundleFind(request);

            return response.IDs;
        }
        public IEnumerable<Online.Registration.DAL.Models.Bundle> BundleGet(IEnumerable<int> bundles)
        {
            if (bundles == null)
                return new List<Bundle>(); ;

            var request = new BundleGetReq()
            {
                IDs = bundles.ToArray()
            };

            var response = _client.BundleGet(request);

            return response.Bundles;
        }
        public static IEnumerable<Online.Registration.DAL.Models.Bundle> FindBundle(Online.Registration.DAL.Models.BundleFind bundle)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.BundleGet(proxy.BundleFind(bundle));
            }
        }
        #endregion

        #region Category

        public int GettllnkContractDataPlan(int selectedid)
        {
            var resp = _client.GettllnkContractDataPlan(selectedid);

            return resp;
        }

        public int[] GetDependentComponents(int componentId)
        {
            var resp = _client.GetDependentComponents(componentId);

            return resp;
        }

        public DataPlanIdsResp getFilterplanvalues(int plantypeid)
        {

            var resp = _client.getFilterplanvalues(plantypeid);

            return resp;
        }
        public CategoryCreateResp CategoryCreate(Category category)
        {
            var categoryCreateRq = new CategoryCreateReq()
            {
                Category = category
            };

            var resp = _client.CategoryCreate(categoryCreateRq);

            return resp;
        }
        public void CategoryUpdate(Category category)
        {
            var categoryUpdateRq = new CategoryUpdateReq()
            {
                Category = category
            };

            var resp = _client.CategoryUpdate(categoryUpdateRq);

        }
        public IEnumerable<int> CategoryFind(CategoryFind Category)
        {
            var request = new CategoryFindReq()
            {
                CategoryFind = Category
            };

            var response = _client.CategoryFind(request);

            return response.IDs;
        }
        public Online.Registration.DAL.Models.Category CategoryGet(int categoryID)
        {
            var categories = CategoryGet(new int[] { categoryID });
            return categories == null ? null : categories.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.Category> CategoryGet(IEnumerable<int> Categorys)
        {
            if (Categorys == null)
                return new List<Category>(); ;

            var request = new CategoryGetReq()
            {
                IDs = Categorys.ToArray()
            };

            var response = _client.CategoryGet(request);

            return response.Categorys;
        }
        public static IEnumerable<Online.Registration.DAL.Models.Category> FindCategory(CategoryFind oRq)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.CategoryGet(proxy.CategoryFind(oRq));
            }
        }
        public static IEnumerable<Category> GetCategory(IEnumerable<int> ids)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.CategoryGet(ids);
            }
        }
        public static Category GetCategory(int id)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.CategoryGet(id);
            }
        }

        #endregion

        #region Colour

        public ColourCreateResp ColourCreate(Colour colour)
        {
            var colourCreateRq = new ColourCreateReq()
            {
                Colour = colour
            };

            var resp = _client.ColourCreate(colourCreateRq);

            return resp;
        }
        public void ColourUpdate(Colour colour)
        {
            var colourUpdateRq = new ColourUpdateReq()
            {
                Colour = colour
            };

            var resp = _client.ColourUpdate(colourUpdateRq);

        }
        public static IEnumerable<Colour> FindColour(ColourFind colour)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ColourGet(proxy.ColourFind(colour));
            }
        }
        public static IEnumerable<Colour> GetColour(IEnumerable<int> ids)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ColourGet(ids);
            }
        }
        public static Colour GetColour(int id)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ColourGet(id);
            }
        }
        public IEnumerable<int> ColourFind(ColourFind colour)
        {
            var request = new ColourFindReq()
            {
                ColourFind = colour
            };

            var response = _client.ColourFind(request);

            return response.IDs;
        }
        public Colour ColourGet(int colourID)
        {
            var colours = ColourGet(new int[] { colourID });
            return colours == null ? null : colours.SingleOrDefault();
        }
        public IEnumerable<Colour> ColourGet(IEnumerable<int> colours)
        {
            if (colours == null)
                return new List<Colour>();

            var request = new ColourGetReq()
            {
                IDs = colours.ToArray()
            };

            var response = _client.ColourGet(request);

            return response.Colours;
        }
        /*public IEnumerable<int> ColourFind(ColourFind Colour)
        {
            var request = new ColourFindReq()
            {
                ColourFind = Colour
            };

            var response = _client.ColourFind(request);

            return response.IDs;
        }
        public IEnumerable<Colour> ColourGet(IEnumerable<int> colours)
        {
            if (colours == null)
                return new List<Colour>(); ;

            var request = new ColourGetReq()
            {
                IDs = colours.ToArray()
            };

            var response = _client.ColourGet(request);

            return response.Colours;
        }
        public static IEnumerable<Colour> FindColour(ColourFind Colour)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ColourGet(proxy.ColourFind(Colour));
            }
        }
        public static IEnumerable<Colour> GetColour(IEnumerable<int> ids)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ColourGet(ids);
            }
        }
        public static Colour GetColour(int id)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ColourGet(id);
            }
        }*/
        #endregion

        #region Component
        public ComponentCreateResp ComponentCreate(Component component)
        {
            var componentCreateRq = new ComponentCreateReq()
            {
                Component = component
            };

            var resp = _client.ComponentCreate(componentCreateRq);

            return resp;
        }
        public void ComponentUpdate(Component component)
        {
            var componentUpdateRq = new ComponentUpdateReq()
            {
                Component = component
            };

            var resp = _client.ComponentUpdate(componentUpdateRq);

        }
        public IEnumerable<int> ComponentFind(ComponentFind component)
        {
            var request = new ComponentFindReq()
            {
                ComponentFind = component,
            };

            var response = _client.ComponentFind(request);

            return response.IDs;
        }
        public IEnumerable<Component> ComponentGet(IEnumerable<int> components)
        {
            if (components == null)
                return new List<Component>(); ;

            var request = new ComponentGetReq()
            {
                IDs = components.ToArray()
            };

            var response = _client.ComponentGet(request);

            return response.Components;
        }
        public IEnumerable<Component> ContractComponentGet()
        {
            var response = _client.ContractComponentGet();

            return response.Components;
        }
        public static IEnumerable<Online.Registration.DAL.Models.Component> FindComponent(ComponentFind component)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ComponentGet(proxy.ComponentFind(component));
            }
        }
        #endregion

        #region ComponentType
        public ComponentTypeCreateResp ComponentTypeCreate(ComponentType componentType)
        {
            var componentTypeCreateRq = new ComponentTypeCreateReq()
            {
                ComponentType = componentType
            };

            var resp = _client.ComponentTypeCreate(componentTypeCreateRq);

            return resp;
        }
        public void ComponentTypeUpdate(ComponentType componentType)
        {
            var componentTypeUpdateRq = new ComponentTypeUpdateReq()
            {
                ComponentType = componentType
            };

            var resp = _client.ComponentTypeUpdate(componentTypeUpdateRq);

        }
        public IEnumerable<int> ComponentTypeFind(ComponentTypeFind componentType)
        {
            var request = new ComponentTypeFindReq()
            {
                ComponentTypeFind = componentType
            };

            var response = _client.ComponentTypeFind(request);

            return response.IDs;
        }
        public IEnumerable<ComponentType> ComponentTypeGet(IEnumerable<int> componentTypes)
        {
            if (componentTypes == null)
                return new List<ComponentType>(); ;

            var request = new ComponentTypeGetReq()
            {
                IDs = componentTypes.ToArray()
            };

            var response = _client.ComponentTypeGet(request);

            return response.ComponentTypes;
        }
        public static IEnumerable<ComponentType> FindComponentType(ComponentTypeFind componentType)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ComponentTypeGet(proxy.ComponentTypeFind(componentType));
            }
        }
        #endregion

        #region Model
        public ModelCreateResp ModelCreate(Model model)
        {
            var modelCreateRq = new ModelCreateReq()
            {
                Model = model
            };

            var resp = _client.ModelCreate(modelCreateRq);

            return resp;
        }
        public void ModelUpdate(Model model)
        {
            var modelUpdateRq = new ModelUpdateReq()
            {
                Model = model
            };

            var resp = _client.ModelUpdate(modelUpdateRq);

        }
        public static IEnumerable<Model> FindModel(ModelFind model)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ModelGet(proxy.ModelFind(model));
            }
        }
        public static IEnumerable<Model> GetModel(IEnumerable<int> ids)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ModelGet(ids);
            }
        }
        public static Model GetModel(int id)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ModelGet(id);
            }
        }
        public IEnumerable<int> ModelFind(ModelFind model)
        {
            var request = new ModelFindReq()
            {
                ModelFind = model
            };

            var response = _client.ModelFind(request);

            return response.IDs;
        }
        public IEnumerable<int> ModelFindSmart(ModelFindSmart model)
        {
            var request = new ModelFindReqSmart()
            {
                ModelFind = model
            };

            var response = _client.ModelFindSmart(request);

            return response.IDs;
        }
        public Model ModelGet(int modelID)
        {
            var models = ModelGet(new int[] { modelID });
            return models == null ? null : models.SingleOrDefault();
        }
        public IEnumerable<Model> ModelGet(IEnumerable<int> models)
        {
            if (models == null)
                return new List<Model>(); ;

            var request = new ModelGetReq()
            {
                IDs = models.ToArray()
            };

            var response = _client.ModelGet(request);

            return response.Models;
        }
        public IEnumerable<ModelSmart> ModelGetSmart(IEnumerable<int> models)
        {
            if (models == null)
                return new List<ModelSmart>(); ;

            var request = new ModelGetReq()
            {
                IDs = models.ToArray()
            };

            var response = _client.ModelGetSmart(request);

            return response.Models;
        }

        #endregion

        #region ModelImage
        public void ModelImageCreate(ModelImage modelImage)
        {
            var modelImageRq = new ModelImageCreateReq()
            {
                ModelImage = modelImage
            };

            var resp = _client.ModelImageCreate(modelImageRq);
        }
        public void ModelImageUpdate(ModelImage modelImage)
        {
            var modelImageUpdateRq = new ModelImageUpdateReq()
            {
                ModelImage = modelImage
            };

            var resp = _client.ModelImageUpdate(modelImageUpdateRq);
        }
        public IEnumerable<int> ModelImageFind(ModelImageFind ModelImage)
        {
            var request = new ModelImageFindReq()
            {
                ModelImageFind = ModelImage
            };

            var response = _client.ModelImageFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.ModelImage ModelImageGet(int ModelImageID)
        {
            var ModelImages = ModelImageGet(new int[] { ModelImageID });
            return ModelImages == null ? null : ModelImages.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.ModelImage> ModelImageGet(IEnumerable<int> ModelImages)
        {
            if (ModelImages == null)
                return new List<ModelImage>(); ;

            var request = new ModelImageGetReq()
            {
                IDs = ModelImages.ToArray()
            };

            var response = _client.ModelImageGet(request);

            return response.ModelImages;
        }
        public static IEnumerable<Online.Registration.DAL.Models.ModelImage> FindModelImage(ModelImageFind ModelImage)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ModelImageGet(proxy.ModelImageFind(ModelImage));
            }
        }
        #endregion

        #region Model Group
        public ModelGroupCreateResp ModelGroupCreate(ModelGroup modelGroup)
        {
            var modelGroupCreateRq = new ModelGroupCreateReq()
            {
                ModelGroup = modelGroup
            };

            var resp = _client.ModelGroupCreate(modelGroupCreateRq);

            return resp;
        }
        public void ModelGroupUpdate(ModelGroup modelGroup)
        {
            var modelGroupUpdateRq = new ModelGroupUpdateReq()
            {
                ModelGroup = modelGroup
            };

            var resp = _client.ModelGroupUpdate(modelGroupUpdateRq);

        }
        public IEnumerable<int> ModelGroupFind(ModelGroupFind mgp)
        {
            var request = new ModelGroupFindReq()
            {
                ModelGroupFind = mgp
            };

            var response = _client.ModelGroupFind(request);

            return response.IDs;
        }
        public IEnumerable<ModelGroup> ModelGroupGet(IEnumerable<int> mgcs)
        {
            if (mgcs == null)
                return new List<ModelGroup>(); ;

            var request = new ModelGroupGetReq()
            {
                IDs = mgcs.ToArray()
            };

            var response = _client.ModelGroupGet(request);

            return response.ModelGroups;
        }
        public IEnumerable<ModelGroup> ModelGroupGetByPBPCIDs(IEnumerable<int> pdpcIDs)
        {
            if (pdpcIDs == null)
                return new List<ModelGroup>(); ;

            var request = new ModelGroupGetReq()
            {
                IDs = pdpcIDs.ToArray()
            };

            var response = _client.ModelGroupGetByPBPCIDs(request);

            return response.ModelGroups;
        }
        public static IEnumerable<ModelGroup> FindModelGroup(ModelGroupFind mgp)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ModelGroupGet(proxy.ModelGroupFind(mgp));
            }
        }
        #endregion

        #region Program
        public ProgramCreateResp ProgramCreate(Program program)
        {
            var programCreateRq = new ProgramCreateReq()
            {
                Program = program
            };

            var resp = _client.ProgramCreate(programCreateRq);

            return resp;
        }
        public void ProgramUpdate(Program program)
        {
            var programUpdateRq = new ProgramUpdateReq()
            {
                Program = program
            };

            var resp = _client.ProgramUpdate(programUpdateRq);

        }
        public IEnumerable<int> ProgramFind(ProgramFind program)
        {
            var request = new ProgramFindReq()
            {
                ProgramFind = program
            };

            var response = _client.ProgramFind(request);

            return response.IDs;
        }
        /*public IEnumerable<Program> ProgramGet(IEnumerable<int> programs)
        {
            if (programs == null)
                return new List<Program>(); ;

            var request = new ProgramGetReq()
            {
                IDs = programs.ToArray()
            };

            var response = _client.ProgramGet(request);

            return response.Programs;
        }*/
        public IEnumerable<Online.Registration.DAL.Models.Program> ProgramGet(IEnumerable<int> programs)
        {
            if (programs == null)
                return new List<Program>(); ;

            var request = new ProgramGetReq()
            {
                IDs = programs.ToArray()
            };

            var response = _client.ProgramGet(request);

            return response.Programs;
        }
        public Online.Registration.DAL.Models.Program ProgramGet(int programID)
        {
            var programs = ProgramGet(new int[] { programID });
            return programs == null ? null : programs.SingleOrDefault();
        }
        public static IEnumerable<Program> FindProgram(ProgramFind program)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ProgramGet(proxy.ProgramFind(program));
            }
        }
        #endregion

        #region Package
        public BundlepackageResp GetMandatoryVas(int selectedid)
        {

            var resp = _client.GetMandatoryVas(selectedid);

            return resp;
        }
        public PackageCreateResp PackageCreate(Package package)
        {
            var packageCreateRq = new PackageCreateReq()
            {
                Package = package
            };

            var resp = _client.PackageCreate(packageCreateRq);

            return resp;
        }
        public void PackageUpdate(Package package)
        {
            var packageUpdateRq = new PackageUpdateReq()
            {
                Package = package
            };

            var resp = _client.PackageUpdate(packageUpdateRq);

        }
        public IEnumerable<int> PackageFind(PackageFind package)
        {
            var request = new PackageFindReq()
            {
                PackageFind = package
            };

            var response = _client.PackageFind(request);

            return response.IDs;
        }
        public IEnumerable<Package> PackageGet(IEnumerable<int> packages)
        {
            if (packages == null)
                return new List<Package>(); ;

            var request = new PackageGetReq()
            {
                IDs = packages.ToArray()
            };

            var response = _client.PackageGet(request);

            return response.Packages;
        }
        public static IEnumerable<Package> FindPackage(PackageFind package)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.PackageGet(proxy.PackageFind(package));
            }
        }
        #endregion

        #region PackageType
        public PackageTypeCreateResp PackageTypeCreate(PackageType package)
        {
            var packageCreateRq = new PackageTypeCreateReq()
            {
                PackageType = package
            };

            var resp = _client.PackageTypeCreate(packageCreateRq);

            return resp;
        }
        public void PackageTypeUpdate(PackageType package)
        {
            var packageUpdateRq = new PackageTypeUpdateReq()
            {
                PackageType = package
            };

            var resp = _client.PackageTypeUpdate(packageUpdateRq);

        }
        public IEnumerable<int> PackageTypeFind(PackageTypeFind PackageType)
        {
            var request = new PackageTypeFindReq()
            {
                PackageTypeFind = PackageType
            };

            var response = _client.PackageTypeFind(request);

            return response.IDs;
        }
        public IEnumerable<PackageType> PackageTypeGet(IEnumerable<int> PackageTypes)
        {
            if (PackageTypes == null)
                return new List<PackageType>(); ;

            var request = new PackageTypeGetReq()
            {
                IDs = PackageTypes.ToArray()
            };

            var response = _client.PackageTypeGet(request);

            return response.PackageTypes;
        }
        public static IEnumerable<PackageType> FindPackageType(PackageTypeFind PackageType)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.PackageTypeGet(proxy.PackageTypeFind(PackageType));
            }
        }
        #endregion

        #region PgmBdlPckComponent
        public PgmBdlPckComponentCreateResp PgmBdlPckComponentCreate(PgmBdlPckComponent pgmBdl)
        {
            var pgmBdlCreateRq = new PgmBdlPckComponentCreateReq()
            {
                PgmBdlPckComponent = pgmBdl
            };

            var resp = _client.PgmBdlPckComponentCreate(pgmBdlCreateRq);

            return resp;
        }
        public IEnumerable<int> PgmBdlPckComponentFind(PgmBdlPckComponentFind PgmBdlPckComponent)
        {
            var request = new PgmBdlPckComponentFindReq()
            {
                PgmBdlPckComponentFind = PgmBdlPckComponent
            };

            var response = _client.PgmBdlPckComponentFind(request);

            return response.IDs;
        }
        public IEnumerable<int> PgmBdlPckComponentFindSmart(PgmBdlPckComponentFindSmart PgmBdlPckComponent)
        {
            var request = new PgmBdlPckComponentFindReqSmart()
            {
                PgmBdlPckComponentFindSmart = PgmBdlPckComponent
            };

            var response = _client.PgmBdlPckComponentFindSmart(request);

            return response.IDs;
        }
        public IEnumerable<PgmBdlPckComponentSmart> PgmBdlPckComponentGetSmart(IEnumerable<int> PgmBdlPckComponents)
        {
            if (PgmBdlPckComponents == null || PgmBdlPckComponents.ToList().Count == 0)
                return new List<PgmBdlPckComponentSmart>();

            var request = new PgmBdlPckComponentGetReqSmart()
            {
                IDs = PgmBdlPckComponents.ToArray()
            };

            var response = _client.PgmBdlPckComponentGetForSmart(request);

            return response.PgmBdlPckComponents;
        }
        public IEnumerable<PgmBdlPckComponent> PgmBdlPckComponentGet(IEnumerable<int> PgmBdlPckComponents)
        {
            if (PgmBdlPckComponents == null || PgmBdlPckComponents.ToList().Count == 0)
                return new List<PgmBdlPckComponent>();

            var request = new PgmBdlPckComponentGetReq()
            {
                IDs = PgmBdlPckComponents.ToArray()
            };

            var response = _client.PgmBdlPckComponentGet(request);

            return response.PgmBdlPckComponents;
        }
        public static IEnumerable<PgmBdlPckComponent> FindPgmBdlPckComponent(PgmBdlPckComponentFind PgmBdlPckComponent)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.PgmBdlPckComponentGet(proxy.PgmBdlPckComponentFind(PgmBdlPckComponent));
            }
        }
        public void PgmBdlPckComponentUpdate(PgmBdlPckComponent pgmBdlPckComponent)
        {
            var pgmBdlPckComponentUpdateRq = new PgmBdlPckComponentUpdateReq()
            {
                PgmBdlPckComponent = pgmBdlPckComponent
            };

            var resp = _client.PgmBdlPckComponentUpdate(pgmBdlPckComponentUpdateRq);

        }
        public Dictionary<string, string> PkgCompContractGet()
        {
            var response = _client.PkgCompContractGet();

            return response.PgmBdlPckComponents;
        }
        public Dictionary<string, string> BdlPkgGet()
        {
            var response = _client.BdlPkgGet();

            return response.PgmBdlPckComponents;
        }
        #endregion

        #region Model Group Model
        public void ModelGroupModelCreate(ModelGroupModel modelGroupModel)
        {
            var modelGroupModelRq = new ModelGroupModelCreateReq()
            {
                ModelGroupModel = modelGroupModel
            };

            var resp = _client.ModelGroupModelCreate(modelGroupModelRq);
        }
        public void ModelGroupModelUpdate(ModelGroupModel modelGroupModel)
        {
            var modelGroupModelUpdateRq = new ModelGroupModelUpdateReq()
            {
                ModelGroupModel = modelGroupModel
            };

            var resp = _client.ModelGroupModelUpdate(modelGroupModelUpdateRq);
        }
        public IEnumerable<int> ModelGroupModelFind(ModelGroupModelFind mgpm)
        {
            var request = new ModelGroupModelFindReq()
            {
                ModelGroupModelFind = mgpm
            };

            var response = _client.ModelGroupModelFind(request);

            return response.IDs;
        }

        public IEnumerable<ModelGroupModel> ModelGroupModelGet(IEnumerable<int> mgpmIDs)
        {
            if (mgpmIDs == null)
                return new List<ModelGroupModel>();

            var request = new ModelGroupModelGetReq()
            {
                IDs = mgpmIDs.ToArray()
            };

            var response = _client.ModelGroupModelGet(request);

            return response.ModelGroupModels;
        }
        public IEnumerable<ModelGroupModel_Smart> ModelGroupModelGet_Smart(IEnumerable<int> mgpmIDs)
        {
            if (mgpmIDs == null)
                return new List<ModelGroupModel_Smart>();

            var request = new ModelGroupModelGetReq_Smart()
            {
                IDs = mgpmIDs.ToArray()
            };

            var response = _client.ModelGroupModelGet_Smart(request);

            return response.ModelGroupModels_Smart;
        }
        public IEnumerable<int> ModelGroupModelFind_Smart(ModelGroupModelFind_Smart mgpm)
        {
            var request = new ModelGroupModelFindReq_Smart()
            {
                ModelGroupModelFind_Smart = mgpm
            };

            var response = _client.ModelGroupModelFind_Smart(request);

            return response.IDs;
        }
        public static IEnumerable<ModelGroupModel> FindModelGroupModel(ModelGroupModelFind mgpm)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(mgpm));
            }
        }
        #endregion

        #region Item Price
        public ItemPriceCreateResp ItemPriceCreate(ItemPrice itemPrice)
        {
            var itemPriceCreateRq = new ItemPriceCreateReq()
            {
                ItemPrice = itemPrice
            };

            var resp = _client.ItemPriceCreate(itemPriceCreateRq);

            return resp;
        }
        public IEnumerable<int> ItemPriceFind(ItemPriceFind ItemPrice)
        {
            var request = new ItemPriceFindReq()
            {
                ItemPriceFind = ItemPrice
            };

            var response = _client.ItemPriceFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.ItemPrice ItemPriceGet(int ItemPriceID)
        {
            var ItemPrices = ItemPriceGet(new int[] { ItemPriceID });
            return ItemPrices == null ? null : ItemPrices.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.ItemPrice> ItemPriceGet(IEnumerable<int> ItemPrices)
        {
            if (ItemPrices == null)
                return new List<ItemPrice>(); ;

            var request = new ItemPriceGetReq()
            {
                IDs = ItemPrices.ToArray()
            };

            var response = _client.ItemPriceGet(request);

            return response.ItemPrices;
        }
        public static IEnumerable<Online.Registration.DAL.Models.ItemPrice> FindItemPrice(ItemPriceFind ItemPrice)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.ItemPriceGet(proxy.ItemPriceFind(ItemPrice));
            }
        }
        public void ItemPriceUpdate(ItemPrice itemPrice)
        {
            var itemPriceUpdateRq = new ItemPriceUpdateReq()
            {
                ItemPrice = itemPrice
            };

            var resp = _client.ItemPriceUpdate(itemPriceUpdateRq);

        }
        #endregion

        #region Advance & Deposit Price for Device
        //added Deepika
        public Online.Registration.DAL.Models.AdvDepositPrice AdvDepositPricesGet(int AdvDepositPriceID)
        {
            var AdvDepositPrices = AdvDepositPricesGet(new int[] { AdvDepositPriceID });
            return AdvDepositPrices == null ? null : AdvDepositPrices.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.AdvDepositPrice> AdvDepositPricesGet(IEnumerable<int> AdvDepositPrices)
        {
            if (AdvDepositPrices == null)
                return new List<AdvDepositPrice>(); ;

            var request = new AdvDepositPriceGetReq()
            {
                IDs = AdvDepositPrices.ToArray()
            };

            var response = _client.AdvDepositPriceGet(request);

            return response.AdvDepositPrices;
        }
        //end region
        #endregion

        #region Property
        //public PropertyCreateResp PropertyCreate(Property Property)
        //{
        //    var PropertyCreateRq = new PropertyCreateReq()
        //    {
        //        Property = Property
        //    };

        //    var resp = _client.PropertyCreate(PropertyCreateRq);

        //    return resp;
        //}
        //public void PropertyUpdate(Property Property)
        //{
        //    var PropertyUpdateRq = new PropertyUpdateReq()
        //    {
        //        Property = Property
        //    };

        //    var resp = _client.PropertyUpdate(PropertyUpdateRq);

        //}
        public IEnumerable<int> PropertyFind(PropertyFind Property)
        {
            var request = new PropertyFindReq()
            {
                PropertyFind = Property
            };

            var response = _client.PropertyFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.Property PropertyGet(int PropertyID)
        {
            var Properties = PropertyGet(new int[] { PropertyID });
            return Properties == null ? null : Properties.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.Property> PropertyGet(IEnumerable<int> Properties)
        {
            if (Properties == null)
                return new List<Property>(); ;

            var request = new PropertyGetReq()
            {
                IDs = Properties.ToArray()
            };

            var response = _client.PropertyGet(request);

            return response.Properties;
        }
        public static IEnumerable<Online.Registration.DAL.Models.Property> FindProperty(PropertyFind Property)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.PropertyGet(proxy.PropertyFind(Property));
            }
        }
        #endregion

        #region Property Value
        //public PropertyValueCreateResp PropertyValueCreate(PropertyValue PropertyValue)
        //{
        //    var PropertyValueCreateRq = new PropertyValueCreateReq()
        //    {
        //        PropertyValue = PropertyValue
        //    };

        //    var resp = _client.PropertyValueCreate(PropertyValueCreateRq);

        //    return resp;
        //}
        //public void PropertyValueUpdate(PropertyValue PropertyValue)
        //{
        //    var PropertyValueUpdateRq = new PropertyValueUpdateReq()
        //    {
        //        PropertyValue = PropertyValue
        //    };

        //    var resp = _client.PropertyValueUpdate(PropertyValueUpdateRq);

        //}
        public IEnumerable<int> PropertyValueFind(PropertyValueFind PropertyValue)
        {
            var request = new PropertyValueFindReq()
            {
                PropertyValueFind = PropertyValue
            };

            var response = _client.PropertyValueFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.PropertyValue PropertyValueGet(int PropertyValueID)
        {
            var PropertyValues = PropertyValueGet(new int[] { PropertyValueID });
            return PropertyValues == null ? null : PropertyValues.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.PropertyValue> PropertyValueGet(IEnumerable<int> PropertyValues)
        {
            if (PropertyValues == null)
                return new List<PropertyValue>(); ;

            var request = new PropertyValueGetReq()
            {
                IDs = PropertyValues.ToArray()
            };

            var response = _client.PropertyValueGet(request);

            return response.PropertyValues;
        }
        public static IEnumerable<Online.Registration.DAL.Models.PropertyValue> FindPropertyValue(PropertyValueFind PropertyValue)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.PropertyValueGet(proxy.PropertyValueFind(PropertyValue));
            }
        }
        #endregion

        #region Plan Property
        //public PlanPropertyCreateResp PlanPropertyCreate(PlanProperty PlanProperty)
        //{
        //    var PlanPropertyCreateRq = new PlanPropertyCreateReq()
        //    {
        //        PlanProperty = PlanProperty
        //    };

        //    var resp = _client.PlanPropertyCreate(PlanPropertyCreateRq);

        //    return resp;
        //}
        //public void PlanPropertyUpdate(PlanProperty PlanProperty)
        //{
        //    var PlanPropertyUpdateRq = new PlanPropertyUpdateReq()
        //    {
        //        PlanProperty = PlanProperty
        //    };

        //    var resp = _client.PlanPropertyUpdate(PlanPropertyUpdateRq);

        //}
        public IEnumerable<int> PlanPropertyFind(PlanPropertyFind PlanProperty)
        {
            var request = new PlanPropertyFindReq()
            {
                PlanPropertyFind = PlanProperty
            };

            var response = _client.PlanPropertyFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.PlanProperty PlanPropertyGet(int PlanPropertyID)
        {
            var PlanProperties = PlanPropertyGet(new int[] { PlanPropertyID });
            return PlanProperties == null ? null : PlanProperties.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.PlanProperty> PlanPropertyGet(IEnumerable<int> PlanProperties)
        {
            if (PlanProperties == null)
                return new List<PlanProperty>(); ;

            var request = new PlanPropertyGetReq()
            {
                IDs = PlanProperties.ToArray()
            };

            var response = _client.PlanPropertyGet(request);

            return response.PlanProperties;
        }
        public static IEnumerable<Online.Registration.DAL.Models.PlanProperty> FindPlanProperty(PlanPropertyFind PlanProperty)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.PlanPropertyGet(proxy.PlanPropertyFind(PlanProperty));
            }
        }
        #endregion

        #region Device Property
        //public DevicePropertyCreateResp DevicePropertyCreate(DeviceProperty DeviceProperty)
        //{
        //    var DevicePropertyCreateRq = new DevicePropertyCreateReq()
        //    {
        //        DeviceProperty = DeviceProperty
        //    };

        //    var resp = _client.DevicePropertyCreate(DevicePropertyCreateRq);

        //    return resp;
        //}
        //public void DevicePropertyUpdate(DeviceProperty DeviceProperty)
        //{
        //    var DevicePropertyUpdateRq = new DevicePropertyUpdateReq()
        //    {
        //        DeviceProperty = DeviceProperty
        //    };

        //    var resp = _client.DevicePropertyUpdate(DevicePropertyUpdateRq);

        //}
        public IEnumerable<int> DevicePropertyFind(DevicePropertyFind DeviceProperty)
        {
            var request = new DevicePropertyFindReq()
            {
                DevicePropertyFind = DeviceProperty
            };

            var response = _client.DevicePropertyFind(request);
            return response.IDs;
        }
        public Online.Registration.DAL.Models.DeviceProperty DevicePropertyGet(int DevicePropertyID)
        {
            var DeviceProperties = DevicePropertyGet(new int[] { DevicePropertyID });
            return DeviceProperties == null ? null : DeviceProperties.SingleOrDefault();
        }
        public IEnumerable<Online.Registration.DAL.Models.DeviceProperty> DevicePropertyGet(IEnumerable<int> DeviceProperties)
        {
            if (DeviceProperties == null)
                return new List<DeviceProperty>(); ;

            var request = new DevicePropertyGetReq()
            {
                IDs = DeviceProperties.ToArray()
            };

            var response = _client.DevicePropertyGet(request);

            return response.DeviceProperties;
        }
        public static IEnumerable<Online.Registration.DAL.Models.DeviceProperty> FindDeviceProperty(DevicePropertyFind DeviceProperty)
        {
            using (var proxy = new SmartCatelogServiceProxy())
            {
                return proxy.DevicePropertyGet(proxy.DevicePropertyFind(DeviceProperty));
            }
        }
        #endregion

        #region BrandArticle

        public ArticleIDGetRespSmart GetArticleID(int p)
        {
            var resp = _client.GetArticleIDSmart(p);
            return resp;
        }

        public Online.Registration.DAL.Models.BrandArticleSmart BrandArticleImageGet(int ModelImageID)
        {
            var ModelImages = BrandArticleModelImageGet(new int[] { ModelImageID });
            return ModelImages == null ? null : ModelImages.SingleOrDefault();
        }

        public IEnumerable<Online.Registration.DAL.Models.BrandArticleSmart> BrandArticleModelImageGet(IEnumerable<int> ModelImages)
        {
            if (ModelImages == null)
                return new List<BrandArticleSmart>(); ;

            var request = new BrandArticleModelImageGetReqSmart()
            {
                IDs = ModelImages.ToArray()
            };

            var response = _client.BrandArticleModelImageGetSmart(request);

            return response.BarndArticleImages;
        }

        public int GetContractDuration(int Id)
        {
            var resp = _client.GetContractDuration(Id);
            return resp;
        }

        public string GetUomId(string ArticleId, int ContractDuration, string contractID, string offerName)
        {
            var resp = _client.GetUomId(ArticleId, ContractDuration, contractID, offerName);
            return resp;
        }
        #endregion

        public IMPOSDetailsResp GetIMOPSDetails(int RegID)
        {
            IMPOSDetailsResp objIMPOSDetailsResp = _client.GetIMOPSDetails_Smart(RegID);
            return objIMPOSDetailsResp;
        }

        public WhiteListDetailsResp GetWhilteListDetails(string UserICNO)
        {
            WhiteListDetailsResp objIWhiteListDetailsResp = _client.getWhilstDescreption(UserICNO);
            return objIWhiteListDetailsResp;
        }

        #region  Added by VLT for SimModels on 09 Apr 2013
        public SimModelTypeGetResp GetSIMModels()
        {
            SimModelTypeGetResp objSimModelsResp = _client.getSIMModels();
            return objSimModelsResp;
        }
        #endregion


        public List<int> GETOfferID(string articleId)
        {

            List<int> offerId = _client.GetOfferID(articleId).ToList();

            return offerId;
        }

        public MOCOfferDetailsRes GETMOCOfferDetails(int offerId)
        {

            var res = _client.GetMOCOfferDetails(offerId);

            return res;
        }

        #region"Get offer Name by Id by VLT"
        /// <summary>
        /// Method to Get Offer Name
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>string</returns>
        public string GetOfferNameById(int offerID)
        {
            string offerName = _client.GetOfferNameById(offerID);

            return offerName;
        }
        #endregion

        public List<int> GetAllDependentComponents(int componentId, int planId)
        {
            var resp = _client.GetAllDependentComponents(componentId, planId).ToList();

            return resp;
        }

        public lnkofferuomarticlepriceResp GetOffersfromUOMArticle(string ArticleId, string planID, string contractID, string MOCStatus)
        {
            var resp = _client.GetOffersfromUOMArticle(ArticleId, planID, contractID, MOCStatus);
            return resp;
        }

        #region"Get CONTRACT Id by VLT"
        /// <summary>
        /// Method CONTRACT ID
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>INT    </returns>
        public int[] GetContractIdByRegID(int regID)
        {
            int[] contractID = _client.GetContractIdByRegID(regID);

            return contractID;
        }
        #endregion

        // Added for Smart Intergration by KiranG

        public IEnumerable<string> ExtendContract(ContractBundleModels Extend)
        {
            var request = new ExtendBrandFindReq()
            {

                ContractBundleModels = Extend
            };
            var response = _client.ExtendBrand(request);
            return response.IDs;
        }
        public IEnumerable<string> ExtendPlanContract(ContractPlans Extend)
        {
            var request = new ExtendPlanFindReq()
            {
                ContractPlan = Extend
            };
            var response = _client.ExtendPlan(request);
            return response.IDs;
        }

        public string GetDiscountID(SmartRebateReq oReq)
        {
            var resp = _client.GetSmartRebate(oReq);
            return resp.ID.ToString();
        }

        //EO Smart Intergration by KiranG

        #region New Impli by Raj

        //public IEnumerable<PgmBdlPckComponentSmart> PgmBdlPckComponentGetForSmartByKenanCode(IEnumerable<string> PgmBdlPckComponents)
        public PgmBdlPckComponentGetResp PgmBdlPckComponentGetForSmartByKenanCode(IEnumerable<string> PgmBdlPckComponents)
        {
            if (PgmBdlPckComponents == null || PgmBdlPckComponents.ToList().Count == 0)
                //return new List<PgmBdlPckComponentSmart>();
                return new PgmBdlPckComponentGetResp();


            var response = _client.PgmBdlPckComponentGetForSmartByKenanCode(PgmBdlPckComponents.ToArray());

            //return response.PgmBdlPckComponents;
            return response;
        }

        public List<ModelPackageInfo> GetModelPackageInfo_smart(int ModelId, int packageId = 0, string KenanCode = "",bool isSmart=false)
        {

            List<ModelPackageInfo> results = _client.GetModelPackageInfo_smart(ModelId, packageId, KenanCode, isSmart).ToList();

            return results;
        }

        //GetSelectedComponentsByRegId
        public List<RegSmartComponents> GetSelectedComponents(string regId)
        {
            List<RegSmartComponents> listRegSmartComponents = new List<RegSmartComponents>();

            try
            {
                listRegSmartComponents = _client.GetSelectedComponents(regId).ToList();

            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }

            return listRegSmartComponents;

        }

        public List<int> GetExtendedPackages(int Modelid)
        {
            List<int> objPackagesExtend = new List<int>();
            objPackagesExtend = _client.GetExtendedPackages(Modelid).ToList();
            return objPackagesExtend;
        }
        
        #endregion

        #region Devices for SuppLines
        public List<BrandSmart> GetBrandSmartsForSuppLines()
        {
            List<BrandSmart> objBrands = new List<BrandSmart>();
            objBrands = _client.GetBrandSmartsForSuppLines().ToList();
            return objBrands;
        }
        #endregion

        #region Models for Smart
               
        public List<ModelSmart> GetSmartModelsForSupplines(int Brandid)
        {
            List<ModelSmart> objModelExtend = new List<ModelSmart>();
            objModelExtend = _client.GetSmartModelsForSupplines(Brandid).ToList();
            return objModelExtend;
        }

        #endregion

        #region packages for Smart

        public List<ModelGroup> GetModelGroupSmartForSupplines(int Modelid)
        {
            List<ModelGroup> objPackagesExtend = new List<ModelGroup>();
            objPackagesExtend = _client.GetModelGroupSmartForSupplines(Modelid).ToList();
            return objPackagesExtend;
        }

        public List<ModelGroup> GetCRPSmartPackages(int Modelid, string type)
        {
            List<ModelGroup> objPackagesExtend = new List<ModelGroup>();
            objPackagesExtend = _client.GetCrpSmartPackages(Modelid, type).ToList();
            return objPackagesExtend;
        }

        #endregion

        #region contract Duration by kenan code

        public int GetContractDurationByKenanCode(string kenanCode)
        {
            var resp = _client.GetContractDurationByKenanCode(kenanCode);
            return resp;
        }

        #endregion


        public List<int> GetSmartExtendedPackages(int Modelid)
        {
            List<int> objPackagesExtend = new List<int>();
            objPackagesExtend = _client.GetSmartExtendedPackages(Modelid).ToList();
            return objPackagesExtend;
        }
    }
}
