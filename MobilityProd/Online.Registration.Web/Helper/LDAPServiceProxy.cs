﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Online.Registration.Web.LDAPSvc;

using log4net;

namespace Online.Registration.Web.Helper
{
    public class LDAPServiceProxy : ServiceProxyBase<LDAPSvc.LDAPServiceClient>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(KenanServiceProxy));
        public string LastErrorMessage { get; set; }

        public LDAPServiceProxy()
        {
            _client = new LDAPServiceClient();
        }

        #region Private Methods

        private void LogResponse(WCFResponse response)
        {
            LastErrorMessage = "";

            if (!response.Success)
            {
                LastErrorMessage = response.Message;

                // Log Error
                Logger.ErrorFormat("Code: {0}|Message: {1}", response.Code, response.Message);
            }
        }

        #endregion

        public LDAPAccessResponse ValidateCredential(LDAPAccessRequest request,string isDealer)
        {
            if (request == null)
                throw new ArgumentNullException();

            var response = _client.ValidateCredential(request,isDealer);
            LogResponse(response);

            return response;
        }
    }
}