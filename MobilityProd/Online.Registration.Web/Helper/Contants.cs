﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Helper
{
    public class Constants
    {
        // BRE Error Message
        public static string BRE_OutstandingCheck = "Outstanding Check";
        public static string BRE_TotalLineCheck = "Total Line Check";
        public static string BRE_ContractCheck = "Contract Check";
        public static string BRE_DDMFCheck = "DDMF Check";

        public static int StatusWebPosToCancel = 1;
        public static string StatusCDPUApprove ="Approved";
        public static string StatusCDPUPending = "Pending";
        public static string StatusCDPURejected ="Rejected";
        public static string IVALUE_ADVANCE = "iValue Advance";
        public static string INV_DASH_OOS = "Out Of Stock";
        public static string NOT_AVAILABLE = "Not Available";
        // CMSS request related
        public static string CMSS_CASE_COMPLEXITY = "3";
        public static string CMSS_CASE_PRIORITY = "59";
        public static string CMSS_CASE_TOPROCEEDWITH = "B";
        public static string CMSS_TYPE = "Walkin"; // PN 3993, change from 40 to Walkin
        public static string ReservedNotFound = "Reserved number does not exist.";
        public static string InfentoryNotExist = "Inventory doesn't exist";

        public static string AVAILABLE = "Available";
        public static string OUT_OF_STOCK = "Show Out of Stock";
        public static string SELLING_FAST = "Fast Selling";
        public static string LIMITED_STOCK = "Limited";

		public static string NEW_USER_ALIAS = "New Customer";
        public static string PRINCIPAL_LINE = "Principal Line";
		public static string CRP = "CRP";
        public static string ADD_CONTRACT = "Add Contract";
		public static string HSDPA_USER = "HSDPA User";
		public static string PREPAID_USER = "Prepaid User";
        public static string New_LINE = "New Line";
        public static string PRINCIPAL_PLAN = "Principal Plan";
        public static string SUPPLEMENTARY_LINE = "Supplementary Line";
        public static string SUPPLEMENTARY_PLAN = "Supplementary Plan";
        public static string MNP_SUPPLEMENTARY_PLAN = "MNP Supplementary Plan";
        public static string PRINCIPAL = "Principal";
        public static string SUPPLEMENTARY = "Supplementary";
        public static string BROADBAND = "Broadband";
        public static string FTTH = "FTTH";
        public static string VOIP = "VOIP";

        #region Order Type for BRE
        public static string BrePlanWithContract = "Plan + Contract"; // 1
        public static string BrePlanOnly = "Plan + W/O Device"; // 2
        public static string BreNewPrinc = "Add New Princ Line"; // 9
        public static string BreNewPrincWithContract = "Add New PrinLine + Contract"; // 9
        public static string BreAddSuppSec = "Add Supp/Secon Line"; // 8 or 27
        public static string BreAddSuppSecWithContract = "Add Supp/Sec Line + Contract"; // 37 or 27
        public static string BreSimRep = "Sim Replacement"; // 14
        public static string BreSimRepPreGSM = "Prepaid SR";
        public static string BreAddRemVas = "Add/Rem VAS";
        public static string BreCRP = "CRP W/O Device"; // 19
        public static string BreCRPWithContract = "CRP + Contract"; // 24
        public static string BreCRPWoDev = "CRP W/O Device";
        public static string BreMNP = "MNP";
        public static string BreOutDev = "Outright Device";
        public static string BreAddContract = "Add Contract";
        public static string BreExtraTen = "Extra Ten";
		public static string BreSMART = "SMART";
        public static string BreThirdPartySIM = "ThirdPartySIM";
        #endregion

        public static string HardStop = "HS";
        public static string Justification = "J";
        public static string DEVICE_PLAN_SUBMIT_VALIDATION = "Please check device / contract selection";
        public static string ERROR_MSG_PPID_IU = "Customer details not found with Account Number search, Please search with ID details.";
        public static string STATUS_NEW = "New";
        public static string STATUS_PARTIAL_ACTIVATED = "Partially Activated";
        public static string STATUS_SERVICE_ACTIVATED = "Service Activated";
        public static string SMART = "SMART";

        
        public static int FAIL_ORDER_STATUS = 28;

        public static string OPTION_YES = "Yes";
        public static string OPTION_NO = "No";
        public static string TOOLTIP_BILLDELIVERY_SMSNOTIF = "Customer will be notified via SMS once the bill is ready and sent.";
        public static string TOOLTIP_BILLDELIVERY_MOBILENO = "Mobile Number to receive SMS notification.";
        public static string BILLDELIVERY_EMAIL = "Email (FREE)";
        public static string BILLDELIVERY_POST = "Post (RM5 per line per month applies)";
        public static string COMPONENT_RELATION_MUTUAL_EXCLUSIVE = "MUTUAL EXCLUSIVE";
		public static string COMPONENT_RELATION_MUTUAL_DEPENDENCY = "MUTUAL DEPENDENCY";

		public static string CONTRACT = "Contract";
		public static string FRONT = "front";
		public static string FILE_IC = "IC";
        public static string WRITTEN_OFF = "One or more acct have been written off. Please collect outstanding amount.";

		public static string KENAN_ACCOUNT_NUMBER = "Account Number";

		public static string REFER_TO_BILL = "Refer to Bill";

		// VAS Section
		public static string VAS_OFFERS = "Offers";
		public static string VAS_DATA_BOOSTER = "Data Boosters";
		public static string VAS_MAIN = "Main";
		public static string VAS_PROMOTIONAL = "Promotional";
		public static string VAS_EXTRA = "Extra";
		//public static string VAS_DEVICE_INSURANCE = "Device Insurance";
		public static string VAS_DEVICE_INSURANCE = "Mobile SafeDevice";
		public static string VAS_SHARING_ADDONS = "MaxisONE (Share) Data Add-Ons";
		public static string VAS_NONSHARING_ADDONS = "MaxisONE Data Add-Ons";
		public static string VAS_DATA_ADDONS = "Data Add-ons";
		public static string NON_SHARE_VRC_DATABOOSTER = "Auto Renew Data Pass";
		public static string SHARE_VRC_DATABOOSTER = "Sharing VRC Booster";
		public static string TRADE_IN_AMOUNT = "Trade-In Amount";
		public static string VAS_DEVICE_FINANCING = "Device Financing";
		public static string MDP_KENAN_CODE = "45569,45568,47535";

		// Device Financing
		public static string DF_CHARGE = "DF_CHARGE";
		public static string DF_UPGRADEFEE = "UPGRADE_FEE";
		public static string DF_MONTH_INSTALLMENT_24 = "MONTH_INSTALLMENT_24";	
		public static string DF_MOC_WAIVER= "MOC_WAIVER";
		public static string DF_BALANCE_WAIVER = "BALANCE_WAIVER";
		public static string DF_CONTRACT_KENANCODE = "CONTRACT_KENANCODE";

        public static int EAN_ID = 1;
        public static int ARTICLE_ID = 2;
        public static int NAME = 3;
        public static int SERIAL = 4;

		// PEGA button action
		public static string ACCEPT = "Accepted";
		public static string REJECT = "Declined";
		public static string LATER = "Later";

		// PEGA settings for UpdateResponse/Application
		public static string NBA_APP_UPDATE_SETTINGS = "NBA_APP_UPDATE_SETTINGS";
		public static string NBA_DEPT_ID = "DEPT_ID";
		public static string NBA_GROUP_ID = "GROUP_ID";
		public static string NBA_ROLE_SA = "ROLEID_SA";
		public static string NBA_ROLE_CCC = "ROLEID_TL";

		public static string NBA_RECOMMENDATION = "NBA_RECOMMENDATION";
		public static string NBA_RECOMMENDATION_DEFAULT = "NBA_RECOMMENDATION_DEFAULT";
		public static string NBA_RESPONSE_STATUS_MAP = "NBA_RESPONSE_STATUS_MAP";

		public static string OPEN = "Open";
        public static string CHOOSE_ONE = "Choose One";

		// msg for ContractCheck
		public static string TERMINATION_MSG_DF = "No Early Termination Fee applicable, only remaining instalment fees applies";
		public static string TERMINATION_MSG_K2 = "Early Termination Fee amount indicated is an estimated amount. Please refer to Kenan CC for the correct Early Termination Fee amount";

		public static string SUBHEADER_K2 = "K2/Device Financing Contract";
		public static string SUBHEADER_AF = "Accessories on Zerolution Contract";

		// addContract & CRP action, need to check in integration.
		public static string TERMINATEASSIGN_DEVICE_ONLY = "AssignOrTerminateDeviceOnly";
		public static string TERMINATEASSIGN_ACC_ONLY = "AssignOrTerminateAccOnly";
		public static string TERMINATEASSIGN_ALL = "AssignOrTerminateAll";



        //Third Party section
        public static string THIRDPARTY_BIOMETRICVERIFY_NA = "NA";
        public static string THIRDPARTY_BIOMETRICVERIFY_NOTAPPLICABLE = "Not Applicable";

        //Zerolution check order flow
        public static string ZEROLUTIONCHECK_ZEROLUTIONORDERONLY = "ZEROLUTION_ORDER_ONLY";
        public static string ZEROLUTIONCHECK_EXCEPTIONALZEROLUTIONONLY = "EXCEPTIONAL_ZEROLUTION_ONLY";
    }

    public class MNPCreationStatus
    {
        public static int CreateNewAccount = 0;
        public static int AddSuppToExistingAccount = 1;
        public static int AddToExistingAccount = 2;
    }
}
