﻿using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;

namespace Online.Registration.Web.Helper
{
    /// <summary>
    /// Util class to provide contract useful feature
    /// </summary>
    public static class ContractUtil
    {
        internal static List<PenaltyInfoVM> GetPenaltyInfo(ServiceInfoVM inVM)
        {
            string externalId = ConfigurationManager.AppSettings["KenanMSISDN"];
            using (var proxy = new retrieveSmartServiceInfoProxy())
            {
                var resp = proxy.retrievePenalty(inVM.Msisdn, externalId, inVM.SubscriberNo, inVM.SubscriberNoReset,
                    userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName, 
                    password: Properties.Settings.Default.retrieveAcctListByICResponsePassword);
                if (resp.Count != 0)
                    return CreatePenaltyInfos(resp);
                return null;
            }
        }

        private static List<PenaltyInfoVM> CreatePenaltyInfos(List<retrievePenalty> resp)
        {
            var lst = new List<PenaltyInfoVM>();
            resp.ForEach(n => {
                lst.Add(CreatePenaltyInfo(n));
            });
            return lst;
        }

        private static PenaltyInfoVM CreatePenaltyInfo(retrievePenalty resp)
        {
            return new PenaltyInfoVM() {
                AdminFee=resp.adminFee.ToDecimal(),
                ComponentId=int.Parse(resp.componentId),
                ContractId=int.Parse(resp.contactId),
                ContractName=resp.ContractName,
                ContractType=resp.contractType,
                EndDate=resp.endDate.ToDate(),
                StartDate=resp.startDate.ToDate(),
                Penalty = resp.penalty.ToDecimal(),
                PenaltyType=resp.penaltyType,
                PhoneModel=resp.phoneModel,
                UpfrontPayment = resp.UpfrontPayment.ToDecimal(),
                UpgradeFee = resp.UpgradeFee.ToDecimal()
            };
        }
    }
}