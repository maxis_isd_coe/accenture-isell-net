﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.KenanSvc;
using System.ComponentModel.DataAnnotations;
using Online.Registration.DAL.Models;

namespace Online.Registration.Web.ViewModels
{
    #region Status Tracking details
    [Serializable]
    public class StatusTrackingVM
    {
        [Display(Name="MSISDN")]
        public string MSISDN
        { get; set; }
        [Display(Name = "Account No")]
        public string AccountNo { get; set; }
        [Display(Name = "Display Port Request ID")]
        public string DispPortReqId { get; set; }
        [Display(Name = "Extention Data")]
        public string ExtensionData { get; set; }
        [Display(Name = "Location")]
        public string Location { get; set; }
        [Display(Name = "Error Code")]
        public string ErrorCode { get; set; }
        [Display(Name = "Error Reason")]
        public string ErrorReason { get; set; }
        [Display(Name = "Status")]
        public string MsisdnStatus { get; set; }
        [Display(Name = "Port Initiated")]
        public string PortInitiatedDt { get; set; }
        [Display(Name = "Request ID")]
        public string PortRequestID { get; set; }
        [Display(Name = "User ID")]
        public string UserId { get; set; }
        [Display(Name = "Request Type")]
        public string RequestType
        { get; set; }
        [Display(Name = "Request Porting")]
        public string ReqPorting
        { get; set; }
        public getMNPRequest getMNPReq
        { get; set; }
        public getMNPRequestResponse getMNPRes
        { get; set; }
        public MnpRequestDetails[] MNPReqList
        { get; set; }
        public string ErrorMessage
        { get; set; }
        public string DisplayMessage
        { get; set; }
        //22052015 - Add for displaying additional entity in MNP Status Tracking - Lus
        public string CustomerName
        { get; set; }
        public MnpRegDetailsVM MnpRegDet
        { get; set; }
    }
    #endregion

}