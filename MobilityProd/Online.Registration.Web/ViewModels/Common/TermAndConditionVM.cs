﻿using Online.Registration.Web.CustomDataAnnotation;
using Online.Registration.Web.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.ViewModels.Common
{
    /// <summary>
    /// Term and condition vm to perform agree and view statment on screen
    /// </summary>
    [Serializable()]
    public class TermAndConditionVM:TermAndConditionVMBase
    {
        /// <summary>
        /// To collect user agree input [default false]
        /// </summary>
        [MustBeTrue(ErrorMessage = "Please Read The T&C")]
        public override bool Agreed { get; set; }
    }
}