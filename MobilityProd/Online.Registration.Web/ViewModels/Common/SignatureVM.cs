﻿using Online.Registration.Web.CustomDataAnnotation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.ViewModels.Common
{
    /// <summary>
    /// Represepcting Sign view model to capture signature from user and display to view
    /// </summary>
    [Serializable]
    public class SignatureVM:VMBase
    {
        public DateTime? SignDate { get; set; }

        [Display(Name="Signature")]
        [AllowHtml]
        [RequiredIfSvCcLogin(ErrorMessage="Signature is required for submission")]
        public string SignSvg { get; set; }

        public bool HasSignature()
        {
            return SignDate != null && !string.IsNullOrEmpty(SignSvg);
        }
    }
}