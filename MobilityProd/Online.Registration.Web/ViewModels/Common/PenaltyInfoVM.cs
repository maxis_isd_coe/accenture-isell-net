﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;

namespace Online.Registration.Web.ViewModels.Common
{
    /// <summary>
    /// Represent a penalty structure in use by web project
    /// </summary>
    public class PenaltyInfoVM
    {
        public decimal AdminFee { get; set; }

        public int ComponentId { get; set; }

        public int ContractId { get; set; }

        public string ContractName { get; set; }

        public string ContractType { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime StartDate { get; set; }

        public decimal Penalty { get; set; }

        public string PenaltyType { get; set; }

        public string PhoneModel { get; set; }

        public decimal UpfrontPayment { get; set; }

        public decimal UpgradeFee { get; set; }

        /// <summary>
        /// Calculate remaining month from start and end and today
        /// </summary>
        public int RemainingMonth {
            get{
                return EndDate.GetMonthDiff(DateTime.Now);
            }
        }
    }
}