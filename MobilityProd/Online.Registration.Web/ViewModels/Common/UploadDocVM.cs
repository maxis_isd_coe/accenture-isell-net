﻿using Online.Registration.Web.Areas.ServiceRequest.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Online.Registration.DAL.Helpers;
using Online.Registration.Web.CustomDataAnnotation;

namespace Online.Registration.Web.ViewModels.Common
{
    [Serializable()]
    public class UploadDocVM : VMBase
    {
        public string Justification { get; set; }

        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Queue must be a number.")]
        [Display(Name="Queue No")]
        public string QueueNum { get; set; }

        [Display(Name = "Front Id File")]
        [DataUnique("File",ErrorMessage="Same document cannot be uploaded more than once")]
        public virtual string FrontIdFile { get; set; }

        [Display(Name = "Front Id File Name")]
        [StringLength(50)]
        public string FrontIdFileName { get; set; }

        [Display(Name = "Back Id File")]
        [DataUnique("File", ErrorMessage = "Same document cannot be uploaded more than once")]
        public virtual string BackIdFile { get; set; }

        [Display(Name = "Front Id Name")]
        [StringLength(50)]
        public string BackIdFileName { get; set; }

        [Display(Name = "Other Id File")]
        [DataUnique("File", ErrorMessage = "Same document cannot be uploaded more than once")]
        public string OtherIdFile { get; set; }

        [Display(Name = "Other Id File Name")]
        [StringLength(50)]
        public string OtherIdFileName { get; set; }

        public string Remarks { get; set; }
    }
}