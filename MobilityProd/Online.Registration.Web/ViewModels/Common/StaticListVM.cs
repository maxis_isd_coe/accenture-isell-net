﻿using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Web.Helper;
using Online.Registration.Web.CustomExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ContentRepository;

namespace Online.Registration.Web.ViewModels.Common
{
    public sealed class StaticListVM
    {
        #region "Singleton Setup"
        static readonly StaticListVM _instance = new StaticListVM();
        public static StaticListVM Instance
        {
            get { 
                return _instance; 
            }
        }
        private StaticListVM()
        {
        }
        #endregion

        //to populate Identification Type dropdown list
        private IEnumerable<SelectListItem> _idTyleList=null;
        public IEnumerable<SelectListItem> IdTypList
        {
            get {
                if(_idTyleList==null)
                    _idTyleList=Util.GetList(RefType.IDCardType);
                return _idTyleList;
            }
        }

        //to populate Identification Type for Transfer Ownership
        private IEnumerable<SelectListItem> _idTransferOwnershipTypeList = null;
        public IEnumerable<SelectListItem> IdTransferOwnershipTypeList
        {
            get
            {
                if (_idTransferOwnershipTypeList == null)
                    _idTransferOwnershipTypeList = Util.GetList(RefType.TransferOwnershipIDType);
                return _idTransferOwnershipTypeList;
            }
        }

        //to populate Third Party Authorization Type dropdown list
        private IEnumerable<SelectListItem> _thirdpartyauthtypeList = null;
        public IEnumerable<SelectListItem> ThirdPartyAuthTypeList
        {
            get
            {
                if (_thirdpartyauthtypeList == null)
                    _thirdpartyauthtypeList = Util.GetList(RefType.ThirdPartyAuthType, defaultValue: "");
                return _thirdpartyauthtypeList;
            }
        }

        //to populate Salutation dropdown list
        private IEnumerable<SelectListItem> _salutationList = null;
        public IEnumerable<SelectListItem> SalutationList
        {
            get
            {
                if (_salutationList == null)
                    _salutationList = Util.GetList(RefType.CustomerTitle, defaultValue: "");
                return _salutationList;
            }
        }

        //to populate Nationality dropdown list
        private IEnumerable<SelectListItem> _nationalityList = null;
        public IEnumerable<SelectListItem> NationalityList
        {
            get
            {
                if (_nationalityList == null)
                    _nationalityList = Util.GetList(RefType.Nationality, defaultValue: "");
                return _nationalityList;
            }
        }

        //to populate Race dropdown list
        private IEnumerable<SelectListItem> _raceList = null;
        public IEnumerable<SelectListItem> RaceList
        {
            get
            {
                if (_raceList == null)
                    _raceList = Util.GetList(RefType.Race, defaultValue: "");
                return _raceList;
            }
        }

        //to populate Language dropdown list
        private IEnumerable<SelectListItem> _languageList = null;
        public IEnumerable<SelectListItem> LanguageList
        {
            get
            {
                if (_languageList == null)
                    _languageList = Util.GetList(RefType.Language, defaultValue: "");
                return _languageList;
            }
        }

        //to populate State dropdown list
        private IEnumerable<SelectListItem> _stateList = null;
        public IEnumerable<SelectListItem> StateList
        {
            get
            {
                if (_stateList == null)
                    _stateList = Util.GetList(RefType.State, defaultValue: "");
                return _stateList;
            }
        }

        //to populate Third Party Auth Type dropdown list
        private IEnumerable<SelectListItem> _thirdpartyauthList = null;
        public IEnumerable<SelectListItem> ThirdPartyAuthList
        {
            get
            {
                if (_thirdpartyauthList == null)
                    _thirdpartyauthList = Util.GetList(RefType.ThirdPartyAuthType, defaultValue: "");
                return _thirdpartyauthList;
            }
        }

        //to populate Maxis Postpaid plans dropdown list
        private static List<SelectListItem> GetPlansList(string defaultText, string defaultValue)
        {
            List<SelectListItem> resultList = new List<SelectListItem>();

            if (defaultText == "default")
                defaultText = "-- Please Select --";

            using (var proxy = new CatalogServiceProxy())
            {
                var list = proxy.BdlPkgGet();

                if (list != null)
                {
                    resultList = list.Select(a => new SelectListItem()
                    {
                        Text = a.Value,
                        Value = a.Key
                    }).ToList();
                }
            }

            return resultList;
        }


        private Dictionary<string,string> _postpaidplanList = null;
        public Dictionary<string, string> PostpaidPlanList
        {
            get{
                if(_postpaidplanList==null)
                {
                    Dictionary<string, string> list;
                    using (var proxy = new CatalogServiceProxy())
                    {
                        list = proxy.BdlPkgGet();
                    }
                    _postpaidplanList = list.ToList()
                        .Where(n=>!n.Value.ToUpper().Contains("EXPIRE"))
                        .Where(n => n.Value.ToUpper().StartsWith("PLANS"))
                        .Where(n => !n.Value.ToUpper().Contains("IDD"))
                        .Where(n => !n.Value.ToUpper().Contains("TALKMORE"))
                        .ToDictionary(
                        n=>n.Key,
                        n => n.Value.Split('-').First()
                            .Split(new string[]{"Plans"},StringSplitOptions.RemoveEmptyEntries).Last().Trim()
                        );

                }
                return _postpaidplanList;
            }
        }

        public IEnumerable<SelectListItem> PostpaidPlanSelectList{
            get{
                return PostpaidPlanList.ToList()
                    .OrderBy(n => n.Value)
                    .Select(n => new SelectListItem() { Text = n.Value, Value = n.Key });
            }
        }

        //to populate date of birth (year) dropdown list
        private IEnumerable<SelectListItem> _dobYear = null;
        public IEnumerable<SelectListItem> DobYear
        {
            get
            {
                if (_dobYear == null)
                {
                    var _dobYear_List = new List<SelectListItem>();
                    for (int year = ((DateTime.Now.Year) - 18); year >= 1900; year--)
                    {
                        _dobYear_List.Add(new SelectListItem() { Text=year.ToString(), Value=year.ToString() });
                    }
                    _dobYear = _dobYear_List;
                }
                return _dobYear;
            }
        }

        //to populate date of birth (month) dropdown list
        private IEnumerable<SelectListItem> _dobMonth = null;
        public IEnumerable<SelectListItem> DobMonth
        {
            get
            {
                if (_dobMonth == null)
                {
                    var _dobMonth_List = new List<SelectListItem>();
                    _dobMonth_List.Add(new SelectListItem() { Text = "January", Value = "January"});
                    _dobMonth_List.Add(new SelectListItem() { Text = "February", Value = "February" });
                    _dobMonth_List.Add(new SelectListItem() { Text = "March", Value = "March" });
                    _dobMonth_List.Add(new SelectListItem() { Text = "April", Value = "April" });
                    _dobMonth_List.Add(new SelectListItem() { Text = "May", Value = "May" });
                    _dobMonth_List.Add(new SelectListItem() { Text = "June", Value = "June" });
                    _dobMonth_List.Add(new SelectListItem() { Text = "July", Value = "July" });
                    _dobMonth_List.Add(new SelectListItem() { Text = "August", Value = "August" });
                    _dobMonth_List.Add(new SelectListItem() { Text = "September", Value = "September" });
                    _dobMonth_List.Add(new SelectListItem() { Text = "October", Value = "October" });
                    _dobMonth_List.Add(new SelectListItem() { Text = "November", Value = "November" });
                    _dobMonth_List.Add(new SelectListItem() { Text = "December", Value = "December" });

                    _dobMonth = _dobMonth_List;
                }
                return _dobMonth;
            }
        }

        //to populate date of birth (day) dropdown list
        private IEnumerable<SelectListItem> _dobDay = null;
        public IEnumerable<SelectListItem> DobDay
        {
            get
            {
                if (_dobDay == null)
                {
                    var _dobDay_List = new List<SelectListItem>();
                    for (int day = 1; day <= 31; day++)
                    {
                        _dobDay_List.Add(new SelectListItem() { Text = day.ToString(), Value = day.ToString() });
                    }
                    _dobDay = _dobDay_List;
                }
                return _dobDay;
            }
        }

        //Populate SROrder status list
        private IEnumerable<SelectListItem> _srStatusList = null;
        public IEnumerable<SelectListItem> SRStatusList {
            get{
                if (_srStatusList == null){
                    //TODO: Hardcode at this moment, will think of move somewhere
                    _srStatusList = Enum.GetValues(typeof(SRStatus)).Cast<SRStatus>()
                        .Select(n=>new SelectListItem(){
                            Text = n.GetLabel(),
                            Value = ((int)n).ToString()
                        }).ToList();
                }
                return _srStatusList;
            }
        }

        private IEnumerable<SelectListItem> _sRRequestTypes = null;
        public IEnumerable<SelectListItem> SRRequestTypes { 
            get{
                if (_sRRequestTypes == null)
                {
                    //TODO : Harcode srrequest type list here, move to somewhere later
                    _sRRequestTypes = Enum.GetValues(typeof(RequestTypes)).Cast<RequestTypes>()
                        .Where(n=>n!=RequestTypes.Prepaid2Postpaid)//Remove pre 2 post
                        .Select(n=>new SelectListItem(){
                            Text = n.GetLabel(),
                            Value = ((int)n).ToString()                       
                        }).ToList();
                }
                return _sRRequestTypes;
            }
        }

        public static List<SelectListItem> GetSRRequestTypes(string defaultText="Please Select"){
            var list = Instance.SRRequestTypes.ToList();
            list.Insert(0, new SelectListItem() {
                Text=defaultText
            });
            return list;
        }

        public static IEnumerable<SelectListItem> GetSRStatus(string defaultText = "Please Select")
        {
            var list = Instance.SRStatusList.ToList();
            list.Insert(0, new SelectListItem()
            {
                Text = defaultText
            });
            return list;
        }

        private IEnumerable<SelectListItem> _simTypeList = null;
        public IEnumerable<SelectListItem> SimTypeList {
            get{
                if(_simTypeList==null){
                    _simTypeList = Util.GetList(RefType.SIMCardType,defaultValue:"");
                }
                return _simTypeList;
            }
        }

        private IEnumerable<SelectListItem> _changeStatusTypeList = null;
        public IEnumerable<SelectListItem> ChangeStatusTypeList { 
            get{
                if(_changeStatusTypeList==null)
                    _changeStatusTypeList = Enum.GetValues(typeof(ChangeStatusTypes)).Cast<ChangeStatusTypes>()
                        .Select(n => new SelectListItem()
                        {
                            Text = n.ToString(),
                            Value = ((int)n).ToString()
                        }).ToList();
                return _changeStatusTypeList;
            }
        }
    }
}