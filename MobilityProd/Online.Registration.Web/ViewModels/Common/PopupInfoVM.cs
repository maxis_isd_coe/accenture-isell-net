﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.ViewModels.Common
{
    /// <summary>
    /// View modal for popup information
    /// </summary>
    [Serializable()]
    public class PopupInfoVM
    {
        private string _header;
        /// <summary>
        /// Title of popup, derive from header if no custom header define
        /// </summary>
        public string Title {
            get{
                if (!string.IsNullOrEmpty(_header))
                    return string.Format("{0} {1}", _header, PopupType.ToString());
                if(!string.IsNullOrEmpty(CustomHeader))
                    return CustomHeader;
                return "&lt;&lt;Unspecified Title&gt;&gt;";
            }
        }
        /// <summary>
        /// Use specified header
        /// </summary>
        public string CustomHeader { get; set; }
        /// <summary>
        /// Message to display
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Type of message, control the color of message
        /// </summary>
        public PopupTypes PopupType { get; set; }
        /// <summary>
        /// Is popup willl show,Default is show
        /// </summary>
        public bool Show { get; set; }

        public PopupInfoVM(string header)
        {
            _header = header;
            //Default info
            PopupType = PopupTypes.Info;
            Message = "&lt;&lt;Unspecified Message&gt;&gt;";
        }

        public enum PopupTypes
        {
            Warning=1,
            Error=2,
            Info=3
        }
    }
}