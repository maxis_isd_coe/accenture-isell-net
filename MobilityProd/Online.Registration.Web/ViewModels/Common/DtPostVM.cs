﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.ViewModels.Common
{
    /// <summary>
    /// This class help to model the param use in JQuery Datatables, any view model that tend to use Jquery datatables should implement this interface
    /// </summary>
    /// <typeparam name="T">Item data type</typeparam>
    [Serializable()]
    public class DtResultVM<T>
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<T> data { get; set; }
    }

    public interface IDtPostVM
    {
         List<DtPostColumnVM> Columns { get; set; }
         int Draw { get; set; }
         int Length { get; set; }
         List<DtPostOrderVM> Order { get; set; }
         int Start { get; set; }
         DtPostSearchVM Search { get; set; }
    }

    [Serializable()]
    public class DtPostVM
    {
        public List<DtPostColumnVM> Columns { get; set; }
        public string Draw { get; set; }
        public int Length { get; set; }
        public List<DtPostOrderVM> Order { get; set; }
        public int Start { get; set; }
        public DtPostSearchVM Search { get; set; }
    }

    [Serializable()]
    public class DtPostColumnVM
    {
        public string Data { get; set; }
        public string Name { get; set; }
        public bool Orderable { get; set; }
        public DtPostSearchVM Search { get; set; }
    }

    [Serializable()]
    public class DtPostSearchVM
    {
        public bool Regex { get; set; }
        public string Value { get; set; }
    }

    [Serializable()]
    public class DtPostOrderVM{
        public int Column { get; set; }
        public string Dir { get; set; }
    }
}