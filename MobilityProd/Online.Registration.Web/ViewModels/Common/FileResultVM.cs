﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.ViewModels.Common
{
    /// <summary>
    /// Serve as view model to handle file download
    /// </summary>
    public class FileResultVM
    {
        private Dictionary<string, byte[]> _allFiles = new Dictionary<string, byte[]>();
        private List<string> _excludeExtension = new List<string>();
        private string _id;
        public FileResultVM(string id,params string[] excludeExtension)
        {
            _id = id;
            if (excludeExtension.Count() > 0)
                foreach(var s in excludeExtension)
                    _excludeExtension.Add(s.ToUpper());
        }

        public byte[] File { 
            get{
                if (_allFiles.Count == 0)
                    throw new Exception("No file to download");
                if (_allFiles.Count > 1)
                    return zipFile();
                return outputFile();
            }
        }

        private byte[] outputFile()
        {
            return _allFiles.Values.First();
        }

        private byte[] zipFile()
        {
            var outputStream = new MemoryStream();
            using (var zip = new ZipFile())
            {
                foreach (var f in _allFiles)
                {
                    zip.AddEntry(f.Key, f.Value);
                }
                zip.Save(outputStream);
                outputStream.Position = 0;
            }
            return outputStream.ToArray();
        }

        public string ContentType {
            get{
                if (_allFiles.Count > 1)
                    return "application/zip";
                return "application/force-download";
            }
        }

        public string FileName { 
            get{
                if(_allFiles.Count>1)
                    return string.Format("{0}{1}{2}.zip", "ISELL", "-", _id);
                return _firstFileName;
            }
        }

        internal void AddBase64File(string fileName, string base64String)
        {
            AddFile(fileName, Convert.FromBase64String(base64String));
        }

        internal void AddFile(string fileName,byte[] file)
        {
            if (!_excludeExtension.Contains(Path.GetExtension(fileName).ToUpper()))
                _allFiles.Add(fileName, file);
        }

        private string _firstFileName{
            get{
                return _allFiles.Keys.First();
            }
        }

        public ContentDisposition ContentDisposition
        {
            get{
                var cd = new ContentDisposition();
                if (_firstFileName.Substring(_firstFileName.Length - 3, 3).ToUpper() != "PDF")
                {
                    cd = new System.Net.Mime.ContentDisposition
                    {
                        FileName = _firstFileName,
                        Inline = false,
                    };
                }
                return cd;
            }
        }

        public bool SingleFile{
            get{
                return _allFiles.Count == 1;
            }
        }
    }
}