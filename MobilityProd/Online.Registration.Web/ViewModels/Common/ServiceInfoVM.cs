﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.ViewModels.Common
{
    /// <summary>
    /// A VM that represent a service Info with billing detail
    /// </summary>
    [Serializable()]
    public class ServiceInfoVM:VMBase
    {
        public string Msisdn { get; set; }
        public string AccNum { get; set; }
        public string Plan { get; set; }
        public List<string> VasList { get; set; }
        public string BillAddLine1 { get; set; }
        public string BillAddLine2 { get; set; }
        public string BillAddLine3 { get; set; }
        public string BillAddPostCode { get; set; }
        public string BillAddState {get;set;}
        public int BillAddStateId { get; set; }
        public string BillAddCity { get; set; }
        public string ExternalID { get; set; }
        public string SubscriberNo { get; set; }
        public string SubscriberNoReset { get; set; }
    }
}