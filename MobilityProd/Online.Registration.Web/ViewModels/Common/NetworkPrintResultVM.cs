﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.ViewModels.Common
{
    /// <summary>
    /// Represent result view model from network printing
    /// </summary>
    [Serializable]
    public class NetworkPrintResultVM:VMBase
    {
        public string ErrorMessage { get; set; }
        public bool Success { get; set; }
    }
}