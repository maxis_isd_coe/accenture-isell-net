﻿using Online.Registration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.ViewModels.Common
{
    [Serializable()]
    public class TermAndConditionVMBase:VMBase
    {
        public virtual bool Agreed { get; set; }
        /// <summary>
        /// Full statement of term and conditon display in html raw format
        /// </summary>
        public string Statements { get; private set; }

        public TermAndConditionVMBase(string version = "V3.0")
        {
            Agreed = false;
            Statements = GetStatement(version);
        }

        private string GetStatement(string version)
        {
            var termAndConditions = string.Empty;
            using (var proxyReg = new RegistrationServiceProxy())
            {
                termAndConditions = proxyReg.GetPrintTsCsInfo(version, 2, "", "", "").TsCs;
            }
            return termAndConditions;
        }
    }
}