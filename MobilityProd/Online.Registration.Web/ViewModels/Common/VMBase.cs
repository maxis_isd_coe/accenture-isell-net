﻿using Online.Registration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.ViewModels.Common
{
    [Serializable]
    public abstract class VMBase
    {
        /// <summary>
        /// Login user id
        /// </summary>
        public string LoginId { get{ return Util.SessionAccess.UserName; } }
        /// <summary>
        /// Login centure org id
        /// </summary>
        public int CenterOrgId { get { return Util.SessionAccess.User.OrgID; } }
        /// <summary>
        /// Date format use by server, will override from web.config
        /// </summary>
        public string DateFormatServer { get { return "dd-MM-yyyy"; } }
        /// <summary>
        /// Date format use for client, will override from web.config
        /// </summary>
        public string DateFormatClient { get { return "dd-mm-yy"; } }
        /// <summary>
        /// String value of current controller name
        /// </summary>
        public string CurrentController { get { return (string)HttpContext.Current.Request.RequestContext.RouteData.Values["controller"]; } }
        /// <summary>
        /// String value of current action
        /// </summary>
        public string CurrentAction { get { return (string)HttpContext.Current.Request.RequestContext.RouteData.Values["action"]; } }
    }
}