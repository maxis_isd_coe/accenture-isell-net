﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.ViewModels
{
    public class WEBPosVM
    {
        public string UserId { get; set; }
        public string StoreID { get; set; }
        public string iSellDeviceID { get; set; }
        public string iSellOrderID { get; set; }
        public string iSellOrderDate { get; set; }
        public string iSellContent { get; set; }
        public string OrderID { get; set; }
    }
}