﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using System.Web.Mvc;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;
using Online.Registration.Web.RegistrationSvc;
using System.Runtime.Serialization;


namespace Online.Registration.Web.ViewModels 
{
    #region Mobile Registration
    [Serializable]
    public class MobileRegistrationVM
    {
        public MobileRegistrationVM()
        {


            Address = new Address();
            Customer = new Customer();
            OrderSummary = new OrderSummaryVM();
            PackageVM = new PackageVM();
            PhoneVM = new PhoneVM();
            VASesVM = new ValueAddedServicesVM();
            MobileNoVM = new MobileNoVM();

            //Packages = new List<PackageVM>();
            //VASes = new List<ValueAddedServicesVM>();
        }

        public int TabNumber { get; set; }

        public MobileNoVM MobileNoVM { get; set; }
        public PhoneVM PhoneVM { get; set; }
        public PackageVM PackageVM { get; set; }
        public ValueAddedServicesVM VASesVM { get; set; }
        public OrderSummaryVM OrderSummary { get; set; }
        public Customer Customer { get; set; }
        public Address Address { get; set; }
    }

    #endregion   

    #region Device & Plan
	[Serializable]
    public class DevicePlanPackageVM
    { 
        public DevicePlanPackageVM()
        {
            DeviceOptionVM = new DeviceOptionVM();
            PhoneVM = new PhoneVM();
            PackageVM = new PackageVM();
            PackageVMList = new List<PackageVM>();
            deviceDetails = new List<Online.Registration.Web.CatalogSvc.GetDeviceImageDetailsResp>();
			errorMsg = string.Empty;
			SimSize = string.Empty;
			isSimRequired = false;
        }
        // brand
        public DeviceOptionVM DeviceOptionVM { get; set; }
        // device + article
        public PhoneVM PhoneVM { get; set; }
        // package
        public PackageVM PackageVM { get; set; }
        public List<PackageVM> PackageVMList { get; set; }
        public List<Online.Registration.Web.CatalogSvc.GetDeviceImageDetailsResp> deviceDetails { get; set; }
		public string errorMsg { get; set; }

		public string BrandSelected { get; set; }
		public string ModelSelected { get; set; }
		public string CapacitySelected { get; set; }
		public string PriceRangeSelected { get; set; }
		public string PriceRangeValue { get; set; }

		public int inputBrandSelected { get; set; }
		public string inputModelSelected { get; set; }
		public int inputCapacitySelected { get; set; }
		public int inputDeviceTypeSelected { get; set; }

		public string SimSize { get; set; }
		public bool isSimRequired { get; set; }

    }
    #endregion

    //<!-- Start -->  20150105 - Samuel add this for "2.2.5	To print registration form if e-Signature is not captured"
    [Serializable]
    public class RegDetailsObjVM
    {
        public RegDetailsObjVM()
        {
            LnkRegDetailsReq = new LnkRegDetailsReq();
            reg = new DAL.Models.Registration();
			isRequreIMPOS = true;
        }
        public LnkRegDetailsReq LnkRegDetailsReq { get; set; }
        public DAL.Models.Registration reg { get; set; }
        public string idDocumentFront { get; set; }
        public int RegID { get; set; }
		public bool isRequreIMPOS { get; set; }
        public PegaRecommendationsVM PegaVM { get; set; }
        
    }

    #region Device Options
    [Serializable]
    public class DeviceOptionVM
    {
        public DeviceOptionVM()
        {
            AvailableBrands = new List<Brand>();
        }

        public int TabNumber { get; set; }
        public string Type { get; set; } // Brand, Hot Seller, Promotion
        public List<Brand> AvailableBrands { get; set; }
        public bool IsBrandSelected { get; set; }
        [Required(ErrorMessage = "Please select Device Brand.")]
        [Display(Name = "Device Brand")]
        public string SelectedDeviceOption { get; set; }
        [Required(ErrorMessage = "Please select Device Brand.")]
        [Display(Name = "Device Brand")]
        public string SelectedDeviceOptionSeco { get; set; }

        public string ExtraTenMobileNumbers { get; set; }

        public PersonalDetailsVM personalDetailsVM { get; set; }
    }

    #endregion 

    #region Device
    [Serializable]
    public class PhoneVM
    {
        public PhoneVM()
        {
            ModelImages = new List<AvailableModelImage>();
        }

        public int TabNumber { get; set; }
        public List<AvailableModelImage> ModelImages { get; set; }
        [Required(ErrorMessage = "Please choose a preferred device.")]
        [Display(Name = "Model")]
        public string SelectedModelImageID { get; set; }
        [Required(ErrorMessage = "Please choose a preferred device.")]
        [Display(Name = "Model")]
        public string SelectedModelImageIDSeco { get; set; }
        public int BrandID { get; set; }
        public int ModelID { get; set; }
        public int ColourID { get; set; }
        public string CBRNo { get; set; }


        public string ExtraTenMobileNumbers { get; set; }

        public PersonalDetailsVM personalDetailsVM { get; set; }

        public string DeviceArticleId { get; set; }

        public decimal DeviceRRPPrice { get; set; }
    }
    [Serializable]
    public class AvailableModelImage
    {
        public BrandArticle BrandArticle { get; set; }
        public ModelImage ModelImage { get; set; }
        public string ModelCode { get; set; }
        public int Count { get; set; }
        public string ColorCode { get; set; }
        public string ColorDescription { get; set; }

        //added by ravi for retail price for device sales on feb 21 2014
        public decimal RetailPrice { get; set; }
        //added by ravi for retail price  for device sales on feb 21 2014 ends here
        public bool dfEligibility { get; set; }     //flag eligibility based on customer and product rule
        public bool dfdeviceEligibility { get; set; }     //flag elibility based on product rule

    }

    #endregion

    #region Value Added Services
    [Serializable]
    public class ValueAddedServicesVM
    {
        public ValueAddedServicesVM()
        {
            MandatoryVASes = new List<AvailableVAS>();
            NonMandatoryVASes = new List<AvailableVAS>();
            AvailableVASes = new List<AvailableVAS>();
            ContractVASes = new List<AvailableVAS>();
            NonMandatoryVASes = new List<AvailableVAS>();
            VASesName = new List<string>();
            SelectedPgmBdlPkgCompIDs = new List<int>();
            DataVASes = new List<AvailableVAS>();
            ExtraComponents = new List<AvailableVAS>();
            Offers = new List<MOCOfferDetails>();
            ContractOffers = new List<lnkofferuomarticleprice>();
            PramotionalComponents = new List<AvailableVAS>();
            InsuranceComponents = new List<AvailableVAS>();
            MutualExclusiveComponents = new List<string>();
            TradeUpList = new List<TradeUpComponent>();//14012015 - Anthony - GST Trade Up
			SharingVases = new List<AvailableVAS>();
			NonShareVRCDataBooster = new List<AvailableVAS>();
			ShareVRCDataBooster = new List<AvailableVAS>();
			DeviceFinancingVAS = new List<AvailableVAS>();
			exceptionalAvailable = false;
			df_offerOneTimeFee = new Dictionary<string, decimal>();
        }
        public string hdnGroupIds { get; set; }
        public int OfferId { get; set; }
        public int OfferIdSeco { get; set; }
        public decimal Discount { get; set; }
        public int TabNumber { get; set; }
        public int TabNumberSeco { get; set; }
        public string SelectedVasIDsSeco { get; set; }
        public string SelectedVasIDs { get; set; }
		public string SelectedVasKenanCodes { get; set; }
        public string SelectedKenanCodes { get; set; }
        public string ExtraTenMobileNumbers { get; set; }
        public List<int> SelectedPgmBdlPkgCompIDs { get; set; }

        public string uomCode { get; set; }
        public string uomCodeSeco { get; set; }
        public string price { get; set; }
        public string priceSeco { get; set; }

        public string DeviceType { get; set; }
        public string DeviceTypeSeco { get; set; }

        //[Required]
        //[Range(1, int.MaxValue, ErrorMessage = "Please Select a Contract")]
        //[Display(Name = "Contract")]
        public string SelectedContractID { get; set; }
        public string SelectedContractName { get; set; }
        public string hdnTradeUpAmount { get; set; }//14012015 - Anthony - GST Trade Up

        public List<string> VASesName { get; set; }
        public List<string> VASesNameSeco { get; set; }
        public List<AvailableVAS> AvailableVASes { get; set; }
        public List<AvailableVAS> ContractVASes { get; set; }
        public List<AvailableVAS> MandatoryVASes { get; set; }
        public List<AvailableVAS> NonMandatoryVASes { get; set; }
        public List<AvailableVAS> MandatorySecoVASes { get; set; }
        public List<AvailableVAS> DataVASes { get; set; }
		public List<AvailableVAS> SharingVases { get; set; }
		public List<AvailableVAS> NonShareVRCDataBooster { get; set; }
		public List<AvailableVAS> ShareVRCDataBooster { get; set; }
        public List<AvailableVAS> ExtraComponents { get; set; }
		public List<AvailableVAS> DeviceFinancingVAS { get; set; }
        public List<MOCOfferDetails> Offers { get; set; }
        public List<lnkofferuomarticleprice> ContractOffers { get; set; }
        public List<AvailableVAS> PramotionalComponents { get; set; }
        public List<AvailableVAS> InsuranceComponents { get; set; }
        public List<AvailableVAS> DeviceFinancingComponents { get; set; }
        public List<string> MutualExclusiveComponents { get; set; }
        public List<TradeUpComponent> TradeUpList { get; set; }//14012015 - Anthony - GST Trade Up
		public bool exceptionalAvailable { get; set; }
		public Dictionary<string, decimal> df_offerOneTimeFee { get; set; }
    }
	
    #region Trade-Up Component
    //14012015 - Anthony - GST Trade Up - Start
    [Serializable]
    public class TradeUpComponent
    {
        public int OfferId { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
    }
    //14012015 - Anthony - GST Trade Up - End
    #endregion

    [Serializable]
    public class AvailableVAS
    {
        public string PgmBdlPkgID { get; set; }
        public int PgmBdlPkgCompID { get; set; }
        public string ComponentName { get; set; }
        public string Value { get; set; }
        public decimal Price { get; set; }
        public int? GroupId { get; set; }
        public string GroupName { get; set; }
        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
        public string KenanCode { get; set; }
        //Added by RAVI on 01 Nov 2013 FOR CONNECT/DISCONNECT CR
        public bool RestricttoKenan { get; set; }
        //Added by RAVI on 01 nov 2013 FOR CONNECT/DISCONNECT CR ENDS HERE
        public int isDefault { get; set; }
        public string CompCodeName { get; set; }
        //RST 8 Nov 2014
        public string AliasName { get; set; }
        public string FavoriteFlag { get; set; }
        //GTM e-Billing CR - Ricky - 2014.09.25
        public bool isHidden { get; set; }
		public bool isSharing { get; set; }
		public bool isVRCDataBooster { get; set; }
		public decimal oneTimeUpgrade { get; set; }
    }

    #endregion

    #region PackageVM
    [Serializable]
    public class ComponentViewModel
    {
		public ComponentViewModel()
		{
			ValueAddedServicesVM = new ValueAddedServicesVM();
		}
        public string AddedVases { get; set; }
        public decimal MonthlySubscription { get; set; }
        public string ChangeFlag { get; set; }
        public string SubscribedComponentIds { get; set; }
        public List<string> KenanMandatoryVasIds { get; set; }
        public IEnumerable<Online.Registration.Web.SubscriberICService.reassingVases> ReassingVases
        {
            set;
            get;
        }
        public string ExtraTenMobileNumbers { get; set; }
        public string RemovedVases { get; set; }
        public string RemovedVasPrices { get; set; }
        public string AddedVasPrices { get; set; }
        public string VasNames { get; set; }
        public string ExtraPackage { get; set; }
        public string MainPackage { get; set; }
        public string DataPackage { get; set; }
        public string AddedVasNames { get; set; }
        public string RemovedVasNames { get; set; }
        public string GroupIds { get; set; }
        public int TabNumber { get; set; }
        public string PramotionalPackage { get; set; }
        public string InsurancePackage { get; set; }
        public string DeviceFinancingPackage { get; set; }

        public ValueAddedServicesVM Allowedcomponents { get; set; }
        public List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel> SubscribedComponents { get; set; }

        public List<OldPackages> OldPackagesList { set; get; }
        public List<NewPackages> NewPackagesList { set; get; }
        public List<OldComponents> OldComponentsList { set; get; }
        public List<NewComponents> NewComponentsList { set; get; }
        public List<NewComponents> DependentVASList { set; get; }
        //CR for auto selection for Discount/Promotion component implementation by chetan
        public string[] Reassign { set; get; }
        public List<string> ReassignOPKenanCodes { set; get; }
        public List<string> ReassignOCKenanCodes { set; get; }
        public List<string> MutualExclusiveComponents { set; get; }
		public bool OldSharingExist { get; set; }
		public bool NewSharingExist { get; set; }
		public string ShareSuppErrorMessage { get; set; }

		public ValueAddedServicesVM ValueAddedServicesVM { get; set; }
    }



    [Serializable]
    public class PackageComponentModel
    {

        public string PackageID { get; set; }

        public string PackageDesc { get; set; }

        public string packageInstId { get; set; }

        public string packageInstIdServ { get; set; }

        public string Contract12Months { get; set; }

        public string Contract24Months { get; set; }

        public string packageActiveDt { get; set; }

        public List<Components> compList { get; set; }

        public IEnumerable<Online.Registration.Web.SubscriberICService.reassingVases> ReassingVases
        {
            set;
            get;
        }

        public List<OldPackages> OldPackagesList { set; get; }
        public List<NewPackages> NewPackagesList { set; get; }
        public List<OldComponents> OldComponentsList { set; get; }
        public List<NewComponents> NewComponentsList { set; get; }

    }

    [Serializable]
    public class Components
    {

        public string componentId { get; set; }

        public string componentInstId { get; set; }

        public string componentInstIdServ { get; set; }

        public string componentActiveDt { get; set; }

        public string componentInactiveDt { get; set; }

        public string componentDesc { get; set; }

        public string componentShortDisplay { get; set; }

        public string lnkpgmbdlpkgcompId { get; set; }

        public string GroupId { get; set; }

        public string Price { get; set; }
    }
    [Serializable]
    public class PackageVM
    {
        public PackageVM()
        {
            AvailablePackages = new List<AvailablePackage>();
            ActivePackages = new List<Controllers.AddRemoveVASController.ARVASPackageComponentModel>();
			SimSize = string.Empty;
			isSimRequired = false;
        }
        // added for know which device is tied to the plan
        public int modelID { get; set; }
        public int brandID { get; set; }
        public PersonalDetailsVM PersonalDetailsVM { get; set; }
        public int TabNumber { get; set; }
        public int TabNumberSeco { get; set; }
        public List<AvailablePackage> AvailablePackages { get; set; }
        public List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel> ActivePackages { get; set; }
        public bool ShowContinueButton { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select a Plan")]
        [Display(Name = "Plan")]
        public int SelectedPgmBdlPkgCompID { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please Select a Secondary Plan")]
        [Display(Name = " Secondary Plan")]
        public int SelectedSecondaryPgmBdlPkgCompID { get; set; }


        //Added by VLT on 19 June 2013 to get SimType
        public string SimType { get; set; }
        public int MISMCount { get; set; }

        public string ExtraTenMobileNumbers { get; set; }
		// reskin
		public string SimSize { get; set; }
		public bool isSimRequired { get; set; }

    }
    [Serializable]
    public class AvailablePackage
    {
        public int PgmBdlPkgCompID { get; set; }
        public string ProgramName { get; set; }
        public string BundleName { get; set; }
        public string PackageName { get; set; }
        public string PackageCode { get; set; }
        public decimal Price { get; set; }
        public decimal MonthlySubscription { get; set; }
        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
        public string KenanCode { get; set; }

        //Added by VLT on 04 June 2013 to get SpendLimt
        public bool IsSpendLimit { get; set; }


    }



    [Serializable]
    public class PromotionalOffers
    {
        public PromotionalOffers()
        {
            OffersList = new List<Online.Registration.DAL.Models.lnkofferuomarticleprice>();

        }
        public List<Online.Registration.DAL.Models.lnkofferuomarticleprice> OffersList { get; set; }


    }



    #endregion


	[Serializable]
	public class LineDetails
	{
		public LineDetails()
		{
			PrincipalSuppLineDetails = new List<PrincipalSuppLineDetails>();
		} 
		public List<PrincipalSuppLineDetails> PrincipalSuppLineDetails { get; set; }
	}

	[Serializable]
	public class PrincipalSuppLineDetails
	{
		public PrincipalSuppLineDetails()
		{
			SuppLineList = new List<string>();
			StandAloneSuppList = new List<string>();
		}

		public string PrincipalLine { get; set; }
		public List<string> SuppLineList { get; set; }
		public List<string> StandAloneSuppList { get; set; }
	}

    #region CRP
    #region ComponentViewModel
    [Serializable]
    public class CRPComponentViewModel
    {
        //public IEnumerable<Online.Registration.Web.SubscriberICService.reassingVases> ReassingVases
        //{
        //    set;
        //    get;
        //}
        public int TabNumber { get; set; }

        public List<OldPackages> OldPackagesList { set; get; }
        public List<NewPackages> NewPackagesList { set; get; }
        public List<OldComponents> OldComponentsList { set; get; }
        public List<NewComponents> NewComponentsList { set; get; }
    }
    [Serializable]
    public class OldComponents
    {
        public string OldComponentId { set; get; }
        public string OldComponentName { set; get; }
        public string OldPackageId { set; get; }

    }
    [Serializable]
    public class NewComponents
    {
        public string NewComponentId { set; get; }
        public string NewComponentKenanCode { set; get; }
        public string NewComponentName { set; get; }
        public string NewPackageId { set; get; }
        public string NewPackageKenanCode { set; get; }
        public bool IsChecked { set; get; }
        public bool ReassignIsmandatory { set; get; }
        public string Type { set; get; }
        public string ComponentGroupId { get; set; }
        public string ComponentGroupName { get; set; }
        public bool isRestrictToKenan { get; set; }
        public bool isCRP { get; set; }
        public int isDefault { get; set; }
        public bool isHidden { get; set; }
		public bool isSharing { get; set; }
		public decimal price { get; set; }
		public decimal oneTimeUpgrade { get; set; }

    }
    [Serializable]
    public class OldPackages
    {
        public string OldPackageId { set; get; }
        public string OldPackageName { set; get; }
    }
    [Serializable]
    public class NewPackages
    {
        public string NewPackageId { set; get; }
        public string NewPackageName { set; get; }
        public string Type { set; get; }
    }
    #endregion
    #endregion

    #region Mobile No
    [Serializable]
    public class MobileNoVM
    {
        public MobileNoVM()
        {
            SelectedMobileNumber = new List<string>();
            AvailableMobileNos = new List<string>();
        }
        public string CapturedIMEINumber { get; set; }
        // normal / mism
        public string SIMCardType { get; set; }
        public string CapturedSIMSerial { get; set; }
        // nano / micro 
        public string SIMCardSize { get; set; }

        public string ExtraTenMobileNumbers { get; set; }
        public int TabNumber { get; set; }
        [Display(Name = "Mobile Number")]
        public List<string> SelectedMobileNumber { get; set; }
        public int NoOfDesireMobileNo { get; set; }
        public int NoOfSubline { get; set; }

        [StringLength(12)]
        [Display(Name = "Desire No")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Desire No must be a number.")]
        public string DesireNo { get; set; }

        public List<string> AvailableMobileNos { get; set; }

        //[Required(ErrorMessage = "Please select a number.")]
        [Display(Name = "Mobile Number")]
        public string SelectedMobileNo { get; set; }
		public string NumberType { get; set; }
    }

    #endregion

    #region Mobile Registration Order Summary
    [Serializable]
    public class OrderSummaryVM
    {
        public OrderSummaryVM()
        {
            Sublines = new List<SublineVM>();
            VasVM = new ValueAddedServicesVM();
            MobileNumbers = new List<string>();
            Sublines = new List<SublineVM>();
            Offers = new List<MOCOfferDetails>();
            SelectedTradeUpComp = new TradeUpComponent();//14012015 - Anthony - GST Trade Up
			MNPSupplinesList = new List<MNPSupplinePlanStorage>();
        }

        public AdvDepositPrice unwaivedAdvancePaymentDeposit { get; set; }

        public PersonalDetailsVM PersonalDetailsVM { get; set; }
        public bool fromSubline { get; set; }
        public List<RegAttributes> DFRegAttribute { get; set; }

        // Device
        public int SelectedModelImageID { get; set; }
        public int SelectedModelImageIDSeco { get; set; }
        public string ModelImageUrl { get; set; }
        public string ModelImageUrlSeco { get; set; }

        [Display(Name = "Brand")]
        public int BrandID { get; set; }
        public string BrandName { get; set; }
        public string ArticleId { get; set; }

        [Display(Name = "Brand")]
        public int BrandIDSeco { get; set; }
        public string BrandNameSeco { get; set; }
        public string ArticleIdSeco { get; set; }

        [Display(Name = "Model")]
        public int ModelID { get; set; }
        public string ModelName { get; set; }

        [Display(Name = "Secondary Model")]
        public int ModelIDSeco { get; set; }
        public string ModelNameSeco { get; set; }

        [Display(Name = "Colour")]
        public int ColourID { get; set; }
        public string ColourName { get; set; }
        public decimal ModelPrice { get; set; }

        [Display(Name = "IMEI Number")]
        public string IMEINumber { get; set; }

        [Display(Name = "Colour")]
        public int ColourIDSeco { get; set; }
        public string ColourNameSeco { get; set; }
        public decimal ModelPriceSeco { get; set; }

        [Display(Name = "IMEI Number")]
        public string IMEINumberSeco { get; set; }

        [Display(Name = "Mobile Number")]
        public List<string> MobileNumbers { get; set; }

        [Display(Name = "Secondary Mobile Number")]
        public List<string> SecondaryMobileNumbers { get; set; }

        public List<string> ExtraTenMobileNumbers { get; set; }

        public int SelectedPgmBdlPkgCompID { get; set; }
        public int SelectedPgmBdlPkgCompIDSeco { get; set; }

        [Display(Name = "Plan Bundle")]
        public string PlanBundle { get; set; }

        [Display(Name = "Plan Name")]
        public string PlanName { get; set; }

        [Display(Name = "Phone Price")]
        public decimal Price { get; set; }

        [Display(Name = "Monthly Subscription")]
        public decimal MonthlySubscription { get; set; }

        public List<decimal> MonthlySubscription2 { get; set; }

        [Display(Name = "Local Calls")]
        public int LocalCalls { get; set; }

        [Display(Name = "Local SMS")]
        public int LocalSMS { get; set; }

        [Display(Name = "Data")]
        public int Data { get; set; }

        [Display(Name = "SIM Serial")]
        public string SIMSerial { get; set; }

        [Display(Name = "Secondary SIM Serial")]
        public string SecondarySIMSerial { get; set; }
        // Plan
        public int SelectedSecondaryPgmBdlPkgCompID { get; set; }

        [Display(Name = "Plan Bundle")]
        public string SecondaryPlanBundle { get; set; }

        [Display(Name = "Plan Name")]
        public string SecondaryPlanName { get; set; }

        [Display(Name = "Phone Price")]
        public decimal SecondaryPrice { get; set; }

        [Display(Name = "Monthly Subscription")]
        public decimal SecondaryMonthlySubscription { get; set; }

        public ValueAddedServicesVM VasVM { get; set; }
        public int VasesPrice { get; set; }
        public int ContractID { get; set; }
        public int DataPlanID { get; set; }
        public int ContractIDSeco { get; set; }
        public int DataPlanIDSeco { get; set; }

        public List<SublineVM> Sublines { get; set; }
        public List<MOCOfferDetails> Offers { get; set; }
        public List<MOCOfferDetails> OffersSeco { get; set; }

        public decimal MalayAdvDevPrice { get; set; }
        public decimal OthersAdvDevPrice { get; set; }
        public decimal SumPrice { get; set; }
        public decimal deposite { get; set; }
        public int RaceID { get; set; }

        public decimal MalyDevAdv { get; set; }
        public decimal MalayPlanAdv { get; set; }
        public decimal MalayDevDeposit { get; set; }
        public decimal MalyPlanDeposit { get; set; }

        public decimal OthDevAdv { get; set; }
        public decimal OthPlanAdv { get; set; }
        public decimal OthDevDeposit { get; set; }
        public decimal OthPlanDeposit { get; set; }

        public decimal MalayAdvDevPriceSeco { get; set; }
        public decimal OthersAdvDevPriceSeco { get; set; }
        public decimal SumPriceSeco { get; set; }
        public decimal depositeSeco { get; set; }
        public int RaceIDSeco { get; set; }

        public decimal MalyDevAdvSeco { get; set; }
        public decimal MalayPlanAdvSeco { get; set; }
        public decimal MalayDevDepositSeco { get; set; }
        public decimal MalyPlanDepositSeco { get; set; }

        public decimal OthDevAdvSeco { get; set; }
        public decimal OthPlanAdvSeco { get; set; }
        public decimal OthDevDepositSeco { get; set; }
        public decimal OthPlanDepositSeco { get; set; }

        public decimal Deposite { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal DevOfferPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public DevicePriceType DevicePriceType { get; set; }

        public decimal ItemPriceSeco { get; set; }
        public decimal TotalPriceSeco { get; set; }
        public DevicePriceType DevicePriceTypeSeco { get; set; }

        public bool IsSpendLimit { get; set; }
        public int DiscountPrice { get; set; }
        public int DiscountPriceSeco { get; set; }

        public string SimType { get; set; }

        public List<RegSmartComponents> SelectedComponets { set; get; }

        public decimal PenaltyAmount { get; set; }
        public decimal PenaltyWaiveOffAmount { get; set; }


        public List<MNPSupplinePlanStorage> MNPSupplinesList { get; set; }
        public List<SupplinePlanStorage> SupplinePlanStorageList { get; set; }
        public decimal UpFrontPayment { get; set; }
        public WaiverComponents WaiverComponent { get; set; }
        public List<DAL.Models.RegRebateDatacontractPenalty> RegRebateDatacontractPenalty { get; set; }
        public TradeUpComponent SelectedTradeUpComp { get; set; }//14012015 - Anthony - GST Trade Up

        //21042015 - Anthony - To differentiate the autowaived component - Start
        public bool isPlanAdvanceAutoWaived { get; set; }
        public bool isPlanDepositAutoWaived { get; set; }
        public bool isDeviceAdvanceAutoWaived { get; set; }
        public bool isDeviceDepositAutoWaived { get; set; }
        //21042015 - Anthony - To differentiate the autowaived component - End
    }

    #endregion

	[Serializable]
	public class ReserveNumberVM
	{
		public ReserveNumberVM()
		{
			selectedNumber = string.Empty;
			numberType = "n";
			isReserved = false;
		}

		public string selectedNumber { get; set; }
		public string numberType { get; set; }
		public bool isReserved { get; set; }
	}

	[Serializable]
	public class DisablePersonalDetails
	{
		public DisablePersonalDetails()
		{
			disableSalutaion = false;
			disableDOB = false;
			disableGender = false;
			disableNationality = false;
			disableRace = false;
			disableEmail = false;
			disableContactNumber = false;
			disableAlternativeNumber = false;
		}
		
		public bool disableSalutaion { get; set; }
		public bool disableDOB { get; set; }
		public bool disableGender { get; set; }
		public bool disableNationality { get; set; }
		public bool disableRace { get; set; }
		public bool disableEmail { get; set; }
		public bool disableContactNumber { get; set; }
		public bool disableAlternativeNumber { get; set; }
	}

	[Serializable]
	public class GeneralResult
	{
		public GeneralResult()
		{
			ResultCode = 0;
			ResultMessage = string.Empty;
		}
		public int ResultCode { get; set; }
		public string ResultMessage { get; set; }
	}

    #region Personal Details
    [Serializable]
    public class PersonalDetailsVM
    {
        public PersonalDetailsVM()
        {
            Customer = new Customer()
            {
                LastAccessID = "SYSTEM"
            };
            Address = new Address()
            {
                LastAccessID = "SYSTEM"
            };
            _WhiteListDetails = new WhiteListDetails()
            {
                Description = string.Empty
            };
			editDisabled = false;
			fromSuppLine = false;
			disablePersonalDetails = new DisablePersonalDetails();
			sharingSuppAdded = false;
			sharingSuppErrorMsg = string.Empty;
			objWaiverComponents = new List<Online.Registration.DAL.Models.WaiverComponents>();
			resultPenaltyAmount = new List<DAL.Models.RegRebateDatacontractPenalty>();
            objRegAccessory = new List<RegAccessoryVM>();
            SimReplacementsVM = new SIMReplacementVM();
        }

		public List<ReserveNumberVM> reserveNumberList { get; set; }
        [Display(Name = "Registration ID")]
        public int RegID { get; set; }
        [Display(Name = "Registration ID")]
        public int RegIDSeco { get; set; }
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Queue must be a number.")]
        [Display(Name = "Queue No")]
        public string QueueNo { get; set; }
        [Display(Name = "Status")]
        public int StatusID { get; set; }
        public string StatusCode { get; set; }
        public int RegTypeID { get; set; }
        public string CRPType { get; set; }
        public string IsDeviceCRP { get; set; }
        public int RegTypeIDSeco { get; set; }
        public bool Supervisor { get; set; }
        public bool IsRegClosed { get; set; }
        public bool IDCardNoValidate { get; set; }
        public int TabNumber { get; set; }
        public Customer Customer { get; set; }
        public Address Address { get; set; }

        public PegaRecommendationsVM PegaVM { get; set; }

        public string ExtraTenMobileNumbers { get; set; }
        public string ExtraTenLimitConfirmation { get; set; }
        public string ExtraTenConfirmation { get; set; }
        public string hdnExtraTenCount { get; set; }

        public string CBRNo { get; set; }
        public string CMSSID { get; set; }
        public string CMSSCaseStatus { get; set; }

        //Third Party SIM Replacement
        public bool ThirdPartyAllowed { get; set; }
        public bool ThirdPartySIM { get; set; }
        public string ThirdParty_ApplicantName { get; set; }
        public string ThirdParty_TypeID { get; set; }
        public string ThirdParty_Id { get; set; }
        public string ThirdParty_Contact { get; set; }
        public string ThirdParty_Letter { get; set; }
        public string ThirdParty_BiometricDesc { get; set; }
        public string ThirdParty_BiometricVerify { get; set; }

        public List<lnkExtraTenDetails> ExtraTenDetails { get; set; }

        [Display(Name = "Day of DOB")]
        public int DOBDay { get; set; }
        public int DOBMonth { get; set; }
        [Display(Name = "Year of DOB")]
        public int DOBYear { get; set; }

        [Display(Name = "Internal")]
        public bool BlacklistInternal { get; set; }
        [Display(Name = "External")]
        public bool BlacklistExternal { get; set; }
        [Display(Name = "Biometric Verification")]
        public bool BiometricVerify { get; set; }
        public string InputIDCardTypeID { get; set; }
        public string KenanAccountNo { get; set; }

        [Display(Name = "SIM Model")]
        public string SIMModel { get; set; }
        [Display(Name = "SIM Model")]
        public string MISMSIMModel { get; set; }

        [Display(Name = "SIM Type")]
        public string SimType { get; set; }   

        [Display(Name = "Liberalisation")]
        public string Liberlization_Status { get; set; }

        [Display(Name = "MOC Status")]
        public string MOCStatus { get; set; }

        public bool IsVerified { get; set; }

        [Display(Name = "PrintName")]
        public string PrintName { get; set; }

        [Display(Name = "Age Verification")]
        public bool AgeCheck { get; set; }
        [Display(Name = "DDMF Verification")]
        public bool DDMFCheck { get; set; }
        [Display(Name = "Address Verification")]
        public bool AddressCheck { get; set; }
        [Display(Name = "Outstanding Credit Verification")]
        public bool OutstandingCreditCheck { get; set; }
        [Display(Name = "Total Line Verification")]
        public bool TotalLineCheck { get; set; }
        [Display(Name = "Principal Line Verification")]
        public bool PrincipalLineCheck { get; set; }

        [Display(Name = "Age Verification")]
        public string AgeCheckStatus { get; set; }
        [Display(Name = "DDMF Verification")]
        public string DDMFCheckStatus { get; set; }
        [Display(Name = "Address Verification")]
        public string AddressCheckStatus { get; set; }
        [Display(Name = "Outstanding Credit Verification")]
        public string OutStandingCheckStatus { get; set; }
        [Display(Name = "Total Line Verification")]
        public string TotalLineCheckStatus { get; set; }
        [Display(Name = "Principal Line Verification")]
        public string PrincipleLineCheckStatus { get; set; }

        [Display(Name = "Whitelist Status")]
        public string Whitelist_Status { get; set; }
        [Display(Name = "MOC Status")]
        public string MOC_Status { get; set; }
        [Display(Name = "Contract Check Verification")]
        public string ContractCheckStatus { get; set; }
        
        [Display(Name = "User Type")]
        public string UserType { get; set; }
        [Display(Name = "Plan Advance")]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Advance must not less than 0.")]
        public decimal PlanAdvance { get; set; }
        [Display(Name = "Plan Deposit")]
        public decimal PlanDeposit { get; set; }
        [Display(Name = "Device Advance")]
        public decimal DeviceAdvance { get; set; }
        [Display(Name = "Device Deposit")]
        public decimal DeviceDeposit { get; set; }
        [Display(Name = "Discount")]
        public decimal Discount { get; set; }
        [Display(Name = "MNP ServiceId Check Status")]
        public string MNPServiceIdCheckStatus { get; set; }
        [Display(Name = "MNP PreportIn Check Status")]
        public string PrePortInCheckStatus { get; set; }
        [Display(Name = "Outstanding Acounts")]
        public string OutstandingAcs { get; set; }
        [Display(Name = "Plan Advance")]
        public decimal PlanAdvanceSeco { get; set; }
        [Display(Name = "Plan Deposit")]
        public decimal PlanDepositSeco { get; set; }
        [Display(Name = "Device Advance")]
        public decimal DeviceAdvanceSeco { get; set; }
        [Display(Name = "Device Deposit")]
        public decimal DeviceDepositSeco { get; set; }
        [Display(Name = "Upfront Payment")]
        public decimal UpfrontPayment { get; set; }

        [Display(Name = "FxAcctNo")]
        public string fxAcctNo { get; set; }

        [Display(Name = "ExternalId")]
        public string externalId { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        public string IMPOSFileName { get; set; }
        public int IMPOSStatus { get; set; }
        public string OrganisationId { get; set; }

        public string InputIDCardNo { get; set; }

        public int InputNationalityID { get; set; }
        public string InputContactNo { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Advance must not less than 0.")]
        [Display(Name = "Device Advance for Malaysians")]
        public decimal MalyDevAdv { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Advance must not less than 0.")]
        [Display(Name = "Plan Advance for Malaysians")]
        public decimal MalayPlanAdv { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Deposit must not less than 0.")]
        [Display(Name = "Device Deposit for Malaysians")]
        public decimal MalayDevDeposit { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Deposit must not less than 0.")]
        [Display(Name = "Plan Deposit for Malaysians")]
        public decimal MalyPlanDeposit { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Advance must not less than 0.")]
        [Display(Name = "Device Advance for Non Malaysians")]
        public decimal OthDevAdv { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Advance must not less than 0.")]
        [Display(Name = "Plan Advance for Non Malaysians")]
        public decimal OthPlanAdv { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Deposit must not less than 0.")]
        [Display(Name = "Device Deposit for Non Malaysians")]
        public decimal OthDevDeposit { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Deposit must not less than 0.")]
        [Display(Name = "Plan Deposit for Non Malaysians")]
        public decimal OthPlanDeposit { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Advance must not less than 0.")]
        [Display(Name = "Secondary Device Advance for Malaysians")]
        public decimal MalyDevAdvSeco { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Advance must not less than 0.")]
        [Display(Name = "Secondary Plan Advance for Malaysians")]
        public decimal MalayPlanAdvSeco { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Deposit must not less than 0.")]
        [Display(Name = "Secondary Device Deposit for Malaysians")]
        public decimal MalayDevDepositSeco { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Deposit must not less than 0.")]
        [Display(Name = "Secondary Plan Deposit for Malaysians")]
        public decimal MalyPlanDepositSeco { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Advance must not less than 0.")]
        [Display(Name = "Secondary Device Advance for Non Malaysians")]
        public decimal OthDevAdvSeco { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Advance must not less than 0.")]
        [Display(Name = "Secondary Plan Advance for Non Malaysians")]
        public decimal OthPlanAdvSeco { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Device Deposit must not less than 0.")]
        [Display(Name = "Secondary Device Deposit for Non Malaysians")]
        public decimal OthDevDepositSeco { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Plan Deposit must not less than 0.")]
        [Display(Name = "Secondary Plan Deposit for Non Malaysians")]
        public decimal OthPlanDepositSeco { get; set; }
   
        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Advance Payment & Deposit must not less than 0.")]
        [Display(Name = "Advance Payment/ Deposit for Non Malaysians *")]
        public decimal OtherAdvDeposit { get; set; }
        public decimal Deposite { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Advance Payment & Deposit must not less than 0.")]
        [Display(Name = "Advance Payment/ Deposit for Malaysians *")]
        public decimal MalayAdvDeposit { get; set; }
        [Display(Name = "Signature")]
        [AllowHtml]
        public string SignatureSVG { get; set; }
        [Display(Name = "Front")]
        [AllowHtml]
        public string CustomerPhoto { get; set; }
        [Display(Name = "Back")]
        [AllowHtml]
        public string AltCustomerPhoto { get; set; }
        [AllowHtml]
        public DateTime RFSalesDT { get; set; }
        [Display(Name = "Other")]
        [AllowHtml]
        public string Photo { get; set; }
        


        [Display(Name = "IMEI Number")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "IMEI Number must be a number.")]
        [StringLength(20)]
        public string CapturedIMEINumber { get; set; }

        [Display(Name = "IMEI Number")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "IMEI Number must be a number.")]
        [StringLength(20)]
        public string CapturedmismIMEINumber { get; set; }

        public int DeviceModelId { get; set; }

        public string RegIMEINumber { get; set; }
        public string RegIMEINumberSeco { get; set; }

        [Display(Name = "SIM Serial")]
        public string CapturedSIMSerial { get; set; }
        [Display(Name = "SIM Serial")]

        public string CapturedMismSIMSerial { get; set; }
        public string RegSIMSerial { get; set; }
        public string RegSIMSerialSeco { get; set; }

        public string ServiceActivateErrMsg { get; set; }

        public string DonorID { get; set; }

        public int suppQty { get; set; }

        [Display(Name = "Payment Status")]
        public string PaymentStatus { get; set; }

        public WhiteListDetails _WhiteListDetails { get; set; }

        [Display(Name = "Message Code")]
        public int MessageCode { get; set; }

        [Display(Name = "Reason")]
        public string MessageDesc { get; set; }

        [Display(Name = "MSISDN")]
        public string MSISDN1 { get; set; }

        [Display(Name = "MSISDN")]
        public string MSISDNSeco { get; set; }

        public string FxSubscrNo { get; set; }

        public string FxSubScrNoResets { get; set; }

        public decimal PenaltyAmount { get; set; }

        public string AccountType { get; set; }

        [Display(Name = "IsSImRequired_Smart")]
        public bool IsSImRequired_Smart { get; set; }

        [Display(Name = "Service Method Name")]
        public string ServiceMethodName { get; set; }

        [Display(Name = "Justification")]
        public string Justification { get; set; }

        [System.ComponentModel.DefaultValue(null)]
        public List<Online.Registration.DAL.Models.RegSuppLine> RegSupplineList { get; set; }

        [System.ComponentModel.DefaultValue(null)]
        public List<Online.Registration.DAL.Models.RegSuppLine> RegSupplineListWithDevice { get; set; }

        public bool haveDevice { get; set; }

        [Display(Name = "Waived")]
        public bool isWaiverOff { get; set; }

        public bool isNeedIMPOSCreation { get; set; }

        [Display(Name = "Printers")]
        public string Printers { get; set; }

        [AllowHtml]
        public string PrintMobileRegsummary { get; set; }

        public string PrintURL { get; set; }

        public string HdnBackClick { get; set; }

        public string PDPDVersion { get; set; }

        public string DocumentUrl { get; set; }

        public string PDPAVersionStatus { get; set; }

        public string CapturedTUTSerial { get; set; }

        public int? SimReplReasonCode { get; set; }

        public int? ContractCheckCount { get; set; }

        public int? TotalLineCheckCount { get; set; }

        //[Required]//06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
        [Display(Name = "SIM Type")]
        public string SIMCardType { get; set; }


        public string CDPUStatus { get; set; }
        public string CDPUDescription { get; set; }
        public string IsLockedBy { get; set; }

        public bool isBREFail { get; set; }

        //[Required]//06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
        [Display(Name = "Secondary SIM Type")]
        public string SecondarySIMType { get; set; }
        public int SimReplacementType { get; set; }
        
		public string frontDocumentName { get; set; }
		public string backDocumentName { get; set; }
		public string othDocumentName { get; set; }
		public string thirdPartyDocumentName { get; set; }

		public string idDocumentFront { get; set; }
		public string idDocumentBack { get; set; }
		public string otherDocuments { get; set; }
		public string thirdPartyFile { get; set; }
        // sam - drop 5
        public string rfDocumentsFile { get; set; }
        public string rfDocumentsFileName { get; set; }

        public string hdnWaiverInfo { get; set; }

        // habs - fix waiver checkboxes - start
        public string[] cbWaiver { get; set; }
        // habs - fix waiver checkboxes - end


        // 20141204 - Display waiver details - Habs - start
        public List<DAL.Models.WaiverComponents> waiverComponents { get; set; }
        // 20141204 - Display waiver details - Habs - end


		public int FileExistInIContract { get; set; }

        public string ArticleID { get; set; }

        public int changeMISM { get; set; }

		public bool editDisabled { get; set; }
		public bool fromSuppLine { get; set; }
		public DisablePersonalDetails disablePersonalDetails { get; set; }

        //GTM e-Billing CR - Ricky - 2014.09.25 - Start
        public List<string> AvailableMsisdn { get; set; }
        public string EmailBillSubscription { get; set; }
        public string EmailAddress { get; set; }
        public string SmsAlertFlag { get; set; }
        public string SmsNo { get; set; }
        public string BillSubmissionStatus { get; set; }
        //GTM e-Billing CR - Ricky - 2014.09.25 - End

		public bool sharingSuppAdded { get; set; }
		public string sharingSuppErrorMsg { get; set; }

		public List<Online.Registration.DAL.Models.WaiverComponents> objWaiverComponents { get; set; }
		public List<DAL.Models.RegRebateDatacontractPenalty> resultPenaltyAmount { get; set; }
        
        //Drop-5 B18 - Dealer Code CR - Pavan - 2015.05.05 - Start
        public string OutletCode { get; set; }
        public string OutletChannelId { get; set; }
        public string SalesCode { get; set; }
        public string SalesChannelId { get; set; }
        public string SalesStaffCode { get; set; }
        public string SalesStaffChannelId { get; set; }
        //Drop-5 B18 - Dealer Code CR - Pavan - 2015.05.05 - End

		public string SurveyFeedBack { get; set; }
        public string SurveyAnswer { get; set; }
        public string SurveyAnswerRate { get; set; }
        public string SingleSignOnValue { get; set; }
        public string SingleSignOnEmailAddress { get; set; }
        public List<RegAttributes> DFRegAttribute { get; set; } 
        public List<RegAccessoryVM> objRegAccessory { get; set; }
        public List<RegAttributes> RegAttributes { get; set; }//20150915 - Samuel - display sales code in order summary page
        public DAL.Models.Registration Registration { get; set; }//20150915 - Samuel - display sales code in order summary page
        public SIMReplacementVM SimReplacementsVM { get; set; }
        public string TacSmsMsisdn { get; set; } // 20151119 - w.loon - Tac Validation
        public string BillingCycle { get; set; }
    }

    #endregion

    #region Print Registration Form
    [Serializable]
    public class RegistrationFormVM
    {
        public RegistrationFormVM()
        {
            Addresses = new List<Address>();
            BundlePackages = new List<PgmBdlPckComponent>();
            BundlePackagesSec = new List<PgmBdlPckComponent>();
            ModelGroupModels = new List<ModelGroupModel>();
            ModelGroupModelsSec = new List<ModelGroupModel>();
            SuppLineForms = new List<SuppLineForm>();
            DeviceForms = new List<DeviceForm>();
            DeviceFormsSec = new List<DeviceForm>();
            DeviceFormsSupp = new List<DeviceForm>();
            VoiceContractDetails = new List<VoiceContractDetails>();
			showInventorySection = true;
            k2Contract = false;
			componentKenanCodeNameLists = new List<ComponentKenanCodeName>();
			ComponentList = new List<PgmBdlPckComponent>();
            BillCycle = "";
        }

        public List<VoiceContractDetails> VoiceContractDetails;
        public DAL.Models.Registration Registration { get; set; }
        public DAL.Models.RegistrationSec RegistrationSec { get; set; }
        public Customer Customer { get; set; }
        public List<RegAttributes> RegAttributes { get; set; }
        public List<Address> Addresses { get; set; }
        public List<PgmBdlPckComponent> BundlePackages { get; set; }
        public List<PgmBdlPckComponent> BundlePackagesSec { get; set; }
        public List<PgmBdlPckComponent> PackageComponents { get; set; }
        public List<PgmBdlPckComponent> PackageComponentsSec { get; set; }
        public List<ModelGroupModel> ModelGroupModels { get; set; }
        public List<ModelGroupModel> ModelGroupModelsSec { get; set; }
        public List<SuppLineForm> SuppLineForms { get; set; }
        public List<DeviceForm> DeviceForms { get; set; }
        public List<DeviceForm> DeviceFormsSec { get; set; }
        public List<DeviceForm> DeviceFormsSupp { get; set; }

        public Online.Registration.DAL.Models.LnkRegDetails LnkRegistrationDetails { get; set; }

        public List<RegSmartComponents> RegSmartComponents { get; set; }

        public List<lnkExtraTenDetails> ExtraTenDetails { get; set; }
        public List<DAL.Models.RegRebateDatacontractPenalty> RegRebateDatacontractPenalty { get; set; }

        public List<DAL.Models.WaiverComponents> waiverComponentsList { get; set; }

		public bool showInventorySection { get; set; }
        public bool k2Contract { get; set; }
        public List<RegAttributes> DFRegAttribute { get; set; }
		public List<ComponentKenanCodeName> componentKenanCodeNameLists { get; set; }
		public List<PgmBdlPckComponent> ComponentList { get; set; }
        public string BillCycle { get; set; }

        public string OutletCode { get; set; }
        public string OutletChannelId { get; set; }
        public string SalesCode { get; set; }
        public string SalesStaffCode { get; set; }

        public List<RegAccessoryVM> RegAccessory { get; set; }

        //Third Party SIM Replacement
        public bool ThirdPartySim { get; set; }
        public string ThirdParty_ApplicantName { get; set; }
        public string ThirdParty_TypeID { get; set; }
        public string ThirdParty_Id { get; set; }
        public string ThirdParty_Contact { get; set; }
        public string ThirdParty_BiometricVerify { get; set; }

    }
    [Serializable]
    public class SuppLineForm
    {
        public SuppLineForm()
        {
            SuppLineVASNames = new List<string>();
        }
        public int SuppLineID;
        public int? SimModelId;
        public int SuppLinePBPCID { get; set; }
        public List<int> SuppLineVASIDs { get; set; }
        public string SuppLineBdlPkgName { get; set; }
        public List<string> SuppLineVASNames { get; set; }
        public string MSISDN { get; set; }
        public string SIMSerial { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Colour { get; set; }
        public decimal Price { get; set; }
        public decimal RetailPrice { get; set; }
        public string IMEI { get; set; }
        public string DeviceArticleId { get; set; }
        public string OfferName { get; set; }
        public int ContractId { get; set; }
        public string ContractName { get; set; }
        public int ContractDuration { get; set; }
        public string ContractTermsAndConditions { get; set; }
        public string ContractVoiceTermsAndConditions { get; set; }
        public decimal PlanAdvance { get; set; }
        public decimal PlanDeposit { get; set; }
        public decimal DeviceAdvance { get; set; }
        public decimal DeviceDeposit { get; set; }
        public bool PlanAdvanceIsWaived { get; set; }
        public bool PlanDepositIsWaived { get; set; }
        public bool DeviceAdvanceIsWaived { get; set; }
        public bool DeviceDepositIsWaived { get; set; }
        public bool IsSuppWithDevice { get; set; }
        public string MsimType { get; set; }
        public bool IsTradeUp { get; set; }//14012015 - Anthony - GST Trade Up
        public decimal TradeUpAmount { get; set; }//14012015 - Anthony - GST Trade Up
		public bool DeviceFinancingOrder { get; set; }
        public List<RegAccessoryVM> regAccessory { get; set; }
        public string AccessoryContractName { get; set; }
    }
    [Serializable]
    public class DeviceForm
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public decimal Price { get; set; }
        public decimal RetailPrice { get; set; }
        public bool IsSuppLine { get; set; }
        public bool IsTradeUp { get; set; }//14012015 - Anthony - GST Trade Up
        public decimal TradeUpAmount { get; set; }//14012015 - Anthony - GST Trade Up
		public bool DeviceFinancingOrder { get; set; }
		public string Colour { get; set; }
    }

    #endregion

	[Serializable]
	public class ComponentKenanCodeName
	{
		public int pbpcID { get; set; }
		public string pbpcKenanCode { get; set; }
		public string componentDescription { get; set; }
	}

    #region Search Registration
    [Serializable]
    public class SearchRegistrationVM
    {
        public SearchRegistrationVM()
        {
            RegistrationSearchResults = new List<RegistrationSearchResult>();
        }

        public RegistrationSearchCriteria RegistrationSearchCriteria { get; set; }
        public List<RegistrationSearchResult> RegistrationSearchResults { get; set; }
    }

    #endregion

    #region Blacklist Check
    [Serializable]
    [XmlRootAttribute(ElementName = "InternalCheckData")]
    public class InternalCheck
    {
        [XmlElement(ElementName = "ExternalIdValue")]
        public string ExternalIdValue { get; set; }
        [XmlElement(ElementName = "ExternalIdType")]
        public string ExternalIdType { get; set; }
    }
    [Serializable]
    [XmlRootAttribute(ElementName = "MxsDDMFCheckData")]
    public class ExternalCheck
    {
        [XmlElement(ElementName = "ExternalIdValue")]
        public string ExternalIdValue { get; set; }
        [XmlElement(ElementName = "ExternalIdType")]
        public string ExternalIdType { get; set; }
    }

    #endregion

    #region Subline Plan
    [Serializable]
    public class SublinePackageVM
    {
        public SublinePackageVM()
        {
            SelectedMobileNumber = new List<string>();
			errorMsg = string.Empty;
        }
        public PersonalDetailsVM PersonalDetailsVM { get; set; }
        public PackageVM PackageVM { get; set; }
        [Display(Name = "Mobile Number")]
        public List<string> SelectedMobileNumber { get; set; }
        public int NoOfDesireMobileNo { get; set; }

        [StringLength(12)]
        [Display(Name = "Desire No")]
        [RegularExpression(@"^[0-9]{0,}$", ErrorMessage = "Desire No must be a number.")]
        public string DesireNo { get; set; }

		public string errorMsg { get; set; }
    }
    [Serializable]
    public class SublineVASVM
    {
        public ValueAddedServicesVM VasVM { get; set; }
    }
    [Serializable]
    public class SublineVM
    {
        public int SupplineID { get; set; }
        public bool CanRemove { get; set; }
        public int SequenceNo { get; set; }
        public int PkgPgmBdlPkgCompID { get; set; }
        public string BundleName { get; set; }
        public string PackageName { get; set; }
        public List<int> SelectedVasIDs { get; set; }
        public List<string> SelectedVasNames { get; set; }
        public List<string> SelectedMobileNos { get; set; }
        public int MainPackageId { get; set; }
        public int DataPackageId { get; set; }
        public int ExtraPackageId { get; set; }
		public int PromoPackageId { get; set; }
		public int InsurancePackageId { get; set; }

        public decimal MalyDevAdv { get; set; }
        public decimal MalayPlanAdv { get; set; }
        public decimal MalayDevDeposit { get; set; }
        public decimal MalyPlanDeposit { get; set; }

        public decimal OthDevAdv { get; set; }
        public decimal OthPlanAdv { get; set; }
        public decimal OthDevDeposit { get; set; }
        public decimal OthPlanDeposit { get; set; }

        public decimal Deposite { get; set; }
        public int ItemPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public DevicePriceType DevicePriceType { get; set; }

        public decimal MalayAdvDevPrice { get; set; }
        public decimal OthersAdvDevPrice { get; set; }
        public decimal SumPrice { get; set; }
        public decimal deposite { get; set; }
        public int RaceID { get; set; }

        public int SelectedModelImageID { get; set; }
        public int SelectModelGroupID { get; set; }
        public string ModelImageUrl { get; set; }

        [Display(Name = "Brand")]
        public int BrandID { get; set; }
        public string BrandName { get; set; }
        public string ArticleId { get; set; }


        [Display(Name = "Model")]
        public int ModelID { get; set; }
        public string ModelName { get; set; }


        [Display(Name = "Colour")]
        public int ColourID { get; set; }
        public string ColourName { get; set; }
        public decimal ModelPrice { get; set; }

        [Display(Name = "IMEI Number")]
        public string IMEINumber { get; set; }

        public bool IsSpendLimit { get; set; }
        public int DiscountPrice { get; set; }
        public int DiscountPriceSeco { get; set; }

        public string SimType { get; set; }
        public int SimTypeID { get; set; }

        public string MSISDN1 { get; set; }
    }

    #endregion

    #region CustomerInformation
    [Serializable]
    public class CustomerPaymentDetailsInfo
    {
        public CustomerInfo CustomerInfo { get; set; }

    }

    #endregion

    #region Inventory Dashboard
    [Serializable]
    public class InventoryDashBoardVM
    {
        public InventoryDashBoardVM()
        { 
            DeviceOptionVM = new DeviceOptionVM();
            OrganizationSelected = "";
            StoreNameSelected = "";
            PhoneVM = new PhoneVM();
            ContractOffers = new List<lnkofferuomarticleprice>();
            regionStores = new List<regionStore>();
            regionStoreName = "";
        }
        public DeviceOptionVM DeviceOptionVM { get; set; }
        public String OrganizationSelected { get; set; }
        public String StoreNameSelected { get; set; }
        public PhoneVM PhoneVM { get; set; }
        public List<lnkofferuomarticleprice> ContractOffers { get; set; }
        public List<regionStore> regionStores { get; set; }
        public string regionStoreName { get; set;  }
		public string BrandSelected { get; set; }
		public string ModelSelected { get; set; }
		public string CapacitySelected { get; set; }

		public int inputBrandSelected { get; set; }
		public string inputModelSelected { get; set; }
		public int inputCapacitySelected { get; set; }
		public int inputDeviceTypeSelected { get; set; }

    }

	[Serializable]
	public class BrandModelID
	{
		public int modelID { get; set; }
		public string modelName { get; set; }
	}

	[Serializable]
    public class regionStore
    {
        public int regionID { get; set; }
        public string regionName { get; set; }
        public int storeID { get; set; }
        public string storeName { get; set; }
    }

    #endregion

    #region Store Keeper

    [Serializable]
    public class StoreKeeperVM
    {
        public StoreKeeperVM()
        {
            RegistrationSearchResults = new List<RegistrationSearchResult>();
        }

        public RegistrationSearchCriteria RegistrationSearchCriteria { get; set; }
        public List<RegistrationSearchResult> RegistrationSearchResults { get; set; }
    }

    #endregion

   
    #region Supervisor for Waiver off
    [Serializable]
    public class SuperVisonforWaiverOffVM
    {
        public SuperVisonforWaiverOffVM()
        {
            RegistrationSearchResults = new List<SearchResultForSuperVisorWaieverOff>();

            AcknowledgedUsers = new List<RegDetails>();
            RegistrationSearchCriteria = new RegistrationSearchCriteriaForWaiverOff();
            RegistrationSearchCriteria.TabNo = 1; // default for waived components
        }

        public RegistrationSearchCriteriaForWaiverOff RegistrationSearchCriteria { get; set; }
        public List<SearchResultForSuperVisorWaieverOff> RegistrationSearchResults { get; set; }
        public List<SearchResultForSuperVisorWaieverOff> RegistrationSearchResultsFO { get; set; }
        public List<RegDetails> AcknowledgedUsers { get; set; }
    }

    #endregion

    #region Pega Recommendations Dashboard
    [Serializable]
    public class PegaRecommendationsVM
    {
        public PegaRecommendationSearchCriteria PegaRecommendationsSearchCriteria { get; set; }
        public List<PegaRecommendation> PegaRecommendationsSearchResult { get; set; }
    }

    #endregion

    #region Cashier
    [Serializable]
    public class CashierVM
    {
        public CashierVM()
        {
            RegistrationSearchResults = new List<RegistrationSearchResult>();
        }

        public RegistrationSearchCriteria RegistrationSearchCriteria { get; set; }
        public List<RegistrationSearchResult> RegistrationSearchResults { get; set; }
    }

    #endregion

    #region MNP PLAN STORAGE
    /// <summary>
    /// REPLACING THE INDIVIDUAL SESSIONS WITH CLASS PROPERTIES FOR MNP MULTIPLE PORT IN ONLY.
    /// </summary>
    [Serializable]
    public class MNPPrimaryPlanStorage
    {
        public string RegMobileReg_VasIDsSeco { get; set; }
        public string RegMobileSub_PkgPgmBdlPkgCompID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public OrderSummaryVM RegMobileReg_OrderSummary { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string SimType { get; set; }
        public int RegMobileReg_PkgPgmBdlPkgCompID { get; set; }
        public int SelectedPlanID { get; set; }
        public string UOMPlanID { get; set; }
        public int RegMobileReg_ProgramMinAge { get; set; }
        public string RegMobileReg_ContractID { get; set; }
        public int RegMobileReg_DataplanID { get; set; }
        public string RegMobileReg_VasIDs { get; set; }
        public List<string> RegMobileReg_VasNames { get; set; }
        public List<string> RegMobileSub_MobileNo { get; set; }
        public List<string> RegMobileReg_MobileNo { get; set; }
        public string OfferId { get; set; }
        public string RegMobileReg_MainDevicePrice { get; set; }
        public string RegMobileReg_OfferDevicePrice { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalyDevAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalayPlanAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalayDevDeposit { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalyPlanDeposit { get; set; }
        public decimal RegMobileReg_MalAdvDeposit { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthDevAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthPlanAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthDevDeposit { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthPlanDeposit { get; set; }
        public decimal RegMobileReg_OtherAdvDeposit { get; set; }
        public string RegMobileReg_UOMCode { get; set; }
        public double Discount { get; set; }
        public string ExtratenMobilenumbers { get; set; }
        public string SelectedComponents { get; set; }
        public string RegMobileReg_DataPkgID { get; set; }
        public string MobileRegType { get; set; }
        public string MISMCount { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_FromSearch { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_ResultMessage { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string intmandatoryidsval { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string intdataidsval { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string intextraidsval { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_BiometricVerify { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_BiometricID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_TabIndex { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_IDCardNo { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public int? RegMobileReg_SelectedModelImageID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public int? RegMobileReg_SelectedModelID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_PhoneVM { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public PersonalDetailsVM RegMobileReg_PersonalDetailsVM { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegStepNo { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_IDCardType { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_SelectedOptionID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_SelectedOptionType { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_SublineVM { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string ArticleId { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string UOMPlanID_Seco { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_OfferDevicePrice_Seco { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public List<string> RegMobileReg_VasNames_Seco { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_VasIDs_Seco { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public OrderSummaryVM RegMobileReg_SublineSummary { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_PkgPgmBdlPkgCompID_Seco { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string SelectedPlanID_Seco { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_ProgramMinAge_Seco { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_ContractID_Seco { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_DataplanID_Seco { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string GuidedSales_PkgPgmBdlPkgCompID_Seco { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public List<string> RegMobileReg_VirtualMobileNo_Sec { get; set; }
        [System.ComponentModel.DefaultValue(0)]
        public int RegMobileReg_Type { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string vasmandatoryids { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string SelectedContractID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string VasGroupIds { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public List<SublineVM> RegMobileReg_SublineVMList { get; set; }
        /// <summary>
        /// Gets or sets the MNP suppline plan storage list.
        /// </summary>
        /// <value>
        /// The MNP suppline plan storage list.
        /// </value>
        [System.ComponentModel.DefaultValue(null)]
        public List<MNPSupplinePlanStorage> MNPSupplinePlanStorageList { get; set; }

        [System.ComponentModel.DefaultValue(null)]
        public string VAS_Seco { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string intpramotionidsval { get; set; }

        [System.ComponentModel.DefaultValue(null)]
        public string intdatadiscountidsval { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string intinsuranceval { get; set; }

		[System.ComponentModel.DefaultValue(null)]
		public string intDeviceFinancingVal { get; set; }


    }

    /// <summary>
    /// CONTAINER FOR SUPPLEMENTARY LINES ATTACHED TO ONE PRIMARY LINE.
    /// </summary>
    [Serializable]
    public class MNPSupplinePlanStorage
    {
        public int RegMobileReg_PkgPgmBdlPkgCompID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public List<string> RegMobileSub_MobileNo { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileSub_VasIDs { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_MSISDN { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public OrderSummaryVM RegMobileReg_SublineSummary { get; set; }
        public int? SelectedPlanID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public OrderSummaryVM RegMobileReg_OrderSummary { get; set; }
        public List<AvailableVAS> RegMobileReg_VasNames { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_VasIDs { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public List<SublineVM> RegMobileReg_SublineVM { get; set; }
        public int GuidedSales_PkgPgmBdlPkgCompID { get; set; }
        public string vasmandatoryids { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string intmandatoryidsval { get; set; }
        [System.ComponentModel.DefaultValue(0)]
        public int RegMobileReg_Type { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string intdataidsval { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string intextraidsval { get; set; }
        public string RegMobileReg_DataPkgID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_ContractID { get; set; }
        public int RegMobileReg_DataplanID { get; set; }
        public string ExtratenMobilenumbers { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public int RegMobileReg_TabIndex { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public PhoneVM RegMobileReg_PhoneVM { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public PersonalDetailsVM RegMobileReg_PersonalDetailsVM { get; set; }
        public Decimal RegMobileReg_MainDevicePrice { get; set; }
        public List<string> RegMobileReg_MobileNo { get; set; }
        public string SelectedComponents { get; set; }
        public string RegMobileReg_OfferDevicePrice { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalyDevAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalayPlanAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalayDevDeposit { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalyPlanDeposit { get; set; }
        public decimal RegMobileReg_MalAdvDeposit { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthDevAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthPlanAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthDevDeposit { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthPlanDeposit { get; set; }
        public decimal RegMobileReg_OtherAdvDeposit { get; set; }
        public string RegMobileReg_UOMCode { get; set; }
        public double Discount { get; set; }
    }
    #endregion  MNP PLAN STORAGE

    public class DocumentObject
    { 
        public string DocType {get; set;}
        public string RegID {get; set;}
        public string InputIDCardNo {get; set;}
        public string MSISDN1 {get; set;}
        public string AccountID {get; set;}
        public string RegType {get; set;}
        public string StoreID {get; set;}
        public byte Document {get; set;}
        public string DocumentName {get; set;}
    }

    #region SUPPLEMENTARY LINES ATTACHED TO ONE PRIMARY LINE.
     /// <summary>
    /// CONTAINER FOR SUPPLEMENTARY LINES ATTACHED TO ONE PRIMARY LINE.
    /// </summary>
    [Serializable]
    public class SupplinePlanStorage
    {
        public string RegMobileReg_ProcessingId { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public int RegMobileReg_PkgPgmBdlPkgCompID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public List<string> RegMobileSub_MobileNo { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileSub_VasIDs { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_MSISDN { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public OrderSummaryVM RegMobileReg_SublineSummary { get; set; }
        public int? SelectedPlanID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public OrderSummaryVM RegMobileReg_OrderSummary { get; set; }
        public List<AvailableVAS> RegMobileReg_VasNames { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_VasIDs { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public List<SublineVM> RegMobileReg_SublineVM { get; set; }
        public int GuidedSales_PkgPgmBdlPkgCompID { get; set; }
        public string vasmandatoryids { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string intmandatoryidsval { get; set; }
        [System.ComponentModel.DefaultValue(0)]
        public int RegMobileReg_Type { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string intdataidsval { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string intextraidsval { get; set; }
        public string RegMobileReg_DataPkgID { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public string RegMobileReg_ContractID { get; set; }
        public int RegMobileReg_DataplanID { get; set; }
        public string ExtratenMobilenumbers { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public int RegMobileReg_TabIndex { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public PhoneVM RegMobileReg_PhoneVM { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public PersonalDetailsVM RegMobileReg_PersonalDetailsVM { get; set; }
        public Decimal RegMobileReg_MainDevicePrice { get; set; }
        public List<string> RegMobileReg_MobileNo { get; set; }
        public string SelectedComponents { get; set; }
        public string RegMobileReg_OfferDevicePrice { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalyDevAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalayPlanAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalayDevDeposit { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_MalyPlanDeposit { get; set; }
        public decimal RegMobileReg_MalAdvDeposit { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthDevAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthPlanAdv { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthDevDeposit { get; set; }
        [System.ComponentModel.DefaultValue(null)]
        public decimal RegMobileReg_OthPlanDeposit { get; set; }
        public decimal RegMobileReg_OtherAdvDeposit { get; set; }
        public string RegMobileReg_UOMCode { get; set; }
        public double Discount { get; set; }
    }
    #endregion  SUPPLEMENTARY LINES ATTACHED TO ONE PRIMARY LINE.

    #region WhiteList
    [Serializable]
    public class WhiteListVM
    {
        public List<Online.Registration.DAL.Models.SMECIWhiteList> WhiteListSearchResults { get; set; }

        public WhiteListVM()
        {
            WhiteListSearchResults = new List<Online.Registration.DAL.Models.SMECIWhiteList>();
        }
    }

    #endregion

    [Serializable]
    public class PrintVersionDetails
    {
        public PrintVersion PrintVersion { get; set; }
    }

    [Serializable]
    public class ClassArticleId
    {
        public int id { get; set; }
        public string articleId { set; get; }
        public int modelId { set; get; }
        public int colourID { set; get; }
        public string imagePath { set; get; }
        public int count { set; get; }
    }

	[Serializable]
	public class existingCustomerContract
	{
		public string contractID;
		public string contractName;
		public DateTime startDate;
		public DateTime endDate;
	}

    [Serializable]
    public class FoundAccounts
    {
        public string Name { get; set; }
        public string AccountNo { get; set; }
        public string MSISDN { get; set; }
        public string ICNo { get; set; }
    }

    [Serializable]
    public class DMEPrintVM
    {
        public DMEPrintVM()
        {
            DMEUserPrint = new List<UserDMEPrint>();
        }
        public List<UserDMEPrint> DMEUserPrint { get; set; }
    }

   
      [Serializable]
    public class PendingOrderVM
    {
         public PendingOrderVM()
        {
            PendingOrderList = new List<PendingRegBREStatus>();
        }
         public List<PendingRegBREStatus> PendingOrderList { get; set; }
    }


      [Serializable]
      public class UpdateBREApproveStatusVM
      {
          public UpdateBREApproveStatusVM()
          {
              PendingOrderApproval = new List<RegBREStatus>();
          }
          public List<RegBREStatus> PendingOrderApproval { get; set; }
      }

    [Serializable]
    public class AccessoriesVM
    {
        public AccessoriesVM()
        {
            SelectedAccessory = new List<AccessoryVM>();
        }
        public List<AccessoryVM> SelectedAccessory { get; set; }
        public string searchBy { get; set; }
        public string searchText { get; set; }
    }

    [Serializable]
    public class AccessoryVM
    {
        public AccessoryVM()
        {
            offerDetail = new List<OfferAccessoryVM>();
        }
        public int index { get; set; }
        public string articleID { get; set; }
        public string EANCode { get; set; }
        public string Name { get; set; }
        public string AttributeName { get; set; }
        public string imageURL { get; set; }
        public List<OfferAccessoryVM> offerDetail { get; set; }
        public int quantity { get; set; }
        public bool isStockAvailable { get; set; }
        public bool isKnockOffRequired { get; set; }
        public string serialNumber { get; set; }
    }

    [Serializable]
    public class OfferAccessoryVM : OfferAccessoryFinancingDetailsVM
    {
        public string articleID { get; set; }
        public string UOMCode { get; set; }
        public string description { get; set; }
        public decimal offerPrice { get; set; }
        public bool isSelected { get; set; }
        public bool isAccFinancing { get; set; }
    }

    [Serializable]
    public class OfferAccessoryFinancingDetailsVM
    {
        public decimal rrpPrice { get; set; }
        public decimal discount_amount { get; set; }
        public decimal monthlyPrice { get; set; }
        public decimal monthlyRemain { get; set; }
        public int tenure { get; set; }
    }

    [Serializable]
    public class RegAccessoryVM : OfferAccessoryFinancingDetailsVM
    {
        public RegAccessoryVM()
        {
            regAccessoryDetails = new List<RegAccessoryDetailsVM>();
        }

        public int ID { get; set; }
        public int regID { get; set; }
        public int? supplineID { get; set; }
        public int? supplineIndex { get; set; }
        public string articleID { get; set; }
        public string EANCode { get; set; }
        public string uomCode { get; set; }
        public decimal price { get; set; }
        public int quantity { get; set; }
        public string Name { get; set; }
        public string AttributeName { get; set; }
        public string imageURL { get; set; }
        public string offerDescription { get; set; }
        public decimal offerPrice { get; set; }
        public bool isKnockOffRequired { get; set; }
        public bool isFinancing { get; set; }
        public List<RegAccessoryDetailsVM> regAccessoryDetails { get; set; }
    }

    [Serializable]
    public class RegAccessoryDetailsVM
    {
        public int ID { get; set; }
        public int regAccessoryID { get; set; }
        public int sequenceID { get; set; }
        public string serialNumber { get; set; }
    }

	[Serializable]
	public class PegaPopUpVM
	{
		public string testVar { get; set; }
		public string selectedOfferText { get; set; }
		public int selectedOfferValue { get; set; }

	}

    [Serializable]
    public class SIMReplacementVM
    {
        public int Type { get; set; }
        public int Reason { get; set; }
        public string SerialNumber { get; set; }
        public int SIMType { get; set; }
        public bool Comp2GBExist { get; set; }
        public string fxAcctNo { get; set; }
        public string externalId { get; set; }
        public string AccExternalID { get; set; }
        public string KenanACNumber { get; set; }
        public string SubscribeNo { get; set; }
        public string SubscribeNoResets { get; set; }
        public string AccountType { get; set; }
        public string AccountCategory { get; set; }
        public string MarketCode { get; set; }
        public string AcctPackageName { get; set; }
        public string AcctPkgsExists { get; set; }
        public string HasSupplyAccounts { get; set; }
        public string DataCompsExistOnAccount { get; set; }
        public string KenanCode { get; set; }
        public SimDetlsList OldSIMDetail { get; set; }
        public string PrimarySimSerial { get; set; }
        public string SecondarySimSerial { get; set; }
    }
}
