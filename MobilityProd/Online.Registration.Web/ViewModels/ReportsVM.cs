﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.ComponentModel.DataAnnotations;

using Online.Registration.Web.Helper;
using Online.Registration.DAL.ReportModels;
using Online.Registration.DAL.Models;

namespace Online.Registration.Web.ViewModels
{
    #region Aging Report

    [Serializable]
    public class SearchViewAgingReportModel
    {
        public SearchViewAgingReportModel()
        {
            SearchCriterias = new SearchAgingReports();
            CenterType = new List<SelectListItem>();
            CenterType.Add(new SelectListItem()
            {
                Text = "All",
                Value = "0"
            });
            CenterType.Add(new SelectListItem()
            {
                Text = "Center",
                Value = "1"
            });
            CenterType.Add(new SelectListItem()
            {
                Text = "Dealer",
                Value = "2"
            });
        }
        public SearchAgingReports SearchCriterias { get; set; }
        public IEnumerable<AgingReport> SearchResults { get; set; }
        public List<SelectListItem> CenterType { get; set; }
        public ReportFormat Format { get; set; }
    }

    [Serializable]
    public class SearchAgingReports
    {
        [Display(Name = "Registration ID")]
        public int? RegID { get; set; }
        [Display(Name = "MSISDN")]
        public string MSISDN { get; set; }
        [Display(Name = "IC No")]
        public string ICNo { get; set; }
        [Display(Name = "Status")]
        public int? StatusID { get; set; }
        [Display(Name = "Organization")]
        public int? CenterOrgID { get; set; }
        [Display(Name = "Center Type")]
        public int CenterTypeID { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
        [Display(Name = "Last Updated By")]
        public string LastUpdatedBy { get; set; }
        [Display(Name = "Registration Date From")]
        public DateTime? RegDateFrom { get; set; }
        [Display(Name = "To")]
        public DateTime? RegDateTo { get; set; }
    }

    #endregion

    #region Biometrics
    [Serializable]
    public class SearchViewBiometricsReportModel
    {
        public SearchViewBiometricsReportModel()
        {
            SearchCriterias = new SearchBiometricsReports();
            CenterType = new List<SelectListItem>();
        }
        public SearchBiometricsReports SearchCriterias { get; set; }
        public IEnumerable<BiometricsReport> SearchResult { get; set; }
        public List<SelectListItem> CenterType { get; set; }
        public ReportFormat Format { get; set; }
    }
    [Serializable]
    public class SearchBiometricsReports
    {
        [Display(Name = "ID Card No.")]
        public string IDCardNo { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
    #endregion

    [Serializable]
    public class SearchViewFailedTranReportModel
    {
        public SearchViewFailedTranReportModel()
        {
            FailTransactionReportSearchResults = new List<FailTransactionsSearchResult>();
        }

        public FailTransactionsSearchCriteria FailTransactionReportSearchCriteria { get; set; }
        public List<FailTransactionsSearchResult> FailTransactionReportSearchResults { get; set; }
    }

    [Serializable]
    public class FailedTransactionReports
    {
        [Display(Name = "Registration ID")]
        public string RegID { get; set; }

        [Display(Name = "ID Card No")]
        public string IDCardNo { get; set; }

        [Display(Name = "Queue No")]
        public string QueueNo { get; set; }

        [Display(Name = "Status")]
        public int StatusID { get; set; }

        [Display(Name = "Organization")]
        public int? CenterOrgID { get; set; }

        [Display(Name = "User ID")]
        public int UserID { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "MSISDN")]
        public string MSISDN { get; set; }

        [Display(Name = "Customer Name")]
        public string CustName { get; set; }

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "Registration From Date")]
        public DateTime? RegDateFrom { get; set; }

        [Display(Name = "Registration To Date")]
        public DateTime? RegDateTo { get; set; }

        public bool IsMobilePlan { get; set; }

        public bool RegTypeID { get; set; }

        public bool RegType { get; set; }
    }

	[Serializable]
	public class TacAuditLogVM
	{
		public TacAuditLogVM()
		{
			TacAuditLogList = new List<TacAuditLog>();
		}
		public List<TacAuditLog> TacAuditLogList { get; set; }
	}

    [Serializable]
    public class RegistrationHistory
    {
		public RegistrationHistory()
		{
			RegIstrationHistoryDetail = new List<DAL.ReportModels.RegHistoryResult>();
			KenanCallHistory = new List<KenanaLogDetails>();
		}
        public List<DAL.ReportModels.RegHistoryResult> RegIstrationHistoryDetail { get; set; }

        public List<KenanaLogDetails> KenanCallHistory { get; set; }

		public int orderID { get; set; }

		
    }

    [Serializable]
    public class RegHistoryDetails
    {
        public int RegID { get; set; }

        public string RegStatus { get; set; }

        public int RegStatusID { get; set; }

        public string ReasonCode { get; set; }

        [Display(Name = "Status Remark")]
        [DataType(DataType.MultilineText)]
        [MaxLength(4000)]
        [Column(TypeName = "nText")]
        public string Remark { get; set; }

        public DateTime? StatusLogDT { get; set; }

        public DateTime CreateDT { get; set; }

        public string LastAccessID { get; set; }

        public string ModemID { get; set; }

        public string KenanAccountNo { get; set; }
    }

  
}