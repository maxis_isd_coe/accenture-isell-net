﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.ViewModels
{

    [Serializable]
    public class BRNVm
    {
        public BRNVm()
        {
            ParentList = new List<SubscriberICService.parentAcctList>();
            MasterList = new List<SubscriberICService.masterAcctList>();
        }
        public List<SubscriberICService.parentAcctList> ParentList { get; set; }
        public string CorpMasterID { get; set; }
        public string CorpMasterExtId { get; set; }
        public string BRN { get; set; }
        public string CorpParentID { get; set; }
        public string CorpParentExtId { get; set; }
        public string CompanyName { get; set; }
        public string MarketCode { get; set; }
        public List<SubscriberICService.masterAcctList> MasterList { get; set; }
        public string SearchType { get; set; }
        public string SearchValue { get; set; }
		public string InputSearchType { get; set; }
		public string InputSearchValue { get; set; }
        
    }
}