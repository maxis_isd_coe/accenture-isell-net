﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.Areas.CRP.CRPViewModels;
using Online.Registration.DAL.Models;

namespace Online.Registration.Web.ViewModels
{
    [Serializable]
    public class ContractVM
    {
        public ContractVM()
        {
            ContractDetails = new ContractDetails();
            ContractDetailsList = new List<ContractDetails>();
            penaltybyMSISDN = new List<SubscriberICService.retrievePenalty>();
			contractGroupDetails = new List<ContractGroupDetails>();
			ParentAction = "";
        }
        public string KenanAccountNo { get; set; }
        public string MSISDN { get; set; }
        public string SubscriberNo { get; set; }
        public string SubscriberNoResets { get; set; }
        public string ErrMsg { get; set; }
        public string ContractType { get; set; }
        public string Penality { get; set; }
        public string PenaltyPhoneModel { get; set; }
        public string PenaltyAdminFee { get; set; }
        public string Upfrontpayment { get; set; }
        public string PenaltyRRP { get; set; }
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
        public string PackageId { get; set; }
        public string Package { get; set; }
        public string PackageDesc { get; set; }
        public string ContractRemainingMonths { get; set; }
        public string VasComponents { get; set; }
        public string VasNames { get; set; }
        public string contractExtend { get; set; }
        public string SimType { get; set; }
        public string maxisOneDataBooster { get; set; }
        public string penaltyType { get; set; }
        public string PenaltyDeviceModel { get; set; }
        public string PenaltyDeviceIMEI { get; set; }
        public string PenaltyUpgradeFee { get; set; }
		public string ParentAction { get; set; }
		public string SubAction { get; set; }

        public ContractDetails ContractDetails { get; set; }
        public List<ContractDetails> ContractDetailsList { get; set; }
        public IList<SubscriberICService.PackageModel> Packages { get; set; }
        public List<SubscriberICService.retrievePenalty> penaltybyMSISDN { get; set; }
		public List<ContractGroupDetails> contractGroupDetails { get; set; }

    }

    [Serializable]
    public class DeviceVM
    {
        public DeviceVM()
        {
            DeviceOption = new DeviceOptionVM();
            OrderSummary = new OrderSummaryVM();
            Phone = new PhoneVM();
            VAS = new ValueAddedServicesVM();
            PersonalDetails = new PersonalDetailsVM();
            Components = new ComponentViewModel();
            ContractList = new Dictionary<string, List<string>>();
            SimSize = string.Empty;
            isSimRequired = false;
        }

        public string AccountType { get; set; }
        public string ContractType { get; set; }
        public string MSISDN { get; set; }
        public List<string> brands { get; set; }
        public string SelectedOptionType { get; set; }
        public string SelectedOptionId { get; set; }
        public string PackageId { get; set; }
        public string PackageDesc { get; set; }
        public string ContractRemainingMonths { get; set; }
        public string ModelId { get; set; }
        public string VasComponents { get; set; }
        public string OfferId { get; set; }
        public string AccountNumber { get; set; }
        public string SubscriberNo { get; set; }
        public string SubscriberNoResets { get; set; }
        public string KenanAccountNo { get; set; }
        public string DevicePrice { get; set; }
        public string UOMCode { get; set; }
        public string Penality { get; set; }
        public string DataPlanId { get; set; }
        public string VasNames { get; set; }
        public string Package { get; set; }
        public string SimType { get; set; }

        public DeviceOptionVM DeviceOption { get; set; }
        public OrderSummaryVM OrderSummary { get; set; }
        public PhoneVM Phone { get; set; }
        public ValueAddedServicesVM VAS { get; set; }
        public PersonalDetailsVM PersonalDetails { get; set; }
        public Dictionary<string, List<string>> ContractList { get; set; } //articleid, List<contractduration>

        public Online.Registration.Web.ViewModels.ComponentViewModel Components { get; set; }

        public string BrandSelected { get; set; }
        public string ModelSelected { get; set; }
        public string CapacitySelected { get; set; }
        public string PriceRangeSelected { get; set; }
        public string PriceRangeValue { get; set; }

        public int inputBrandSelected { get; set; }
        public string inputModelSelected { get; set; }
        public int inputCapacitySelected { get; set; }
        public int inputDeviceTypeSelected { get; set; }

        // reskin
        public string SimSize { get; set; }
        public bool isSimRequired { get; set; }
		public string subAction { get; set; }
    }


}
