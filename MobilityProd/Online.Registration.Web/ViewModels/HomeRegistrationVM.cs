﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

using Online.Registration.DAL.Models;

namespace Online.Registration.Web.ViewModels
{
    #region Home Registration

    [Serializable]
    public class HomeRegistrationVM
    {
        public HomeRegistrationVM()
        {
            AddressVM = new AddressVM();
            Customer = new Customer();
            OrderSummary = new OrderSummaryVM();
            PackageVM = new PackageVM();
            PhoneVM = new PhoneVM();
            VASesVM = new ValueAddedServicesVM();
            MobileNoVM = new MobileNoVM();
        }

        public int TabNumber { get; set; }
        public AddressVM AddressVM { get; set; }
        public MobileNoVM MobileNoVM { get; set; }
        public PhoneVM PhoneVM { get; set; }
        public PackageVM PackageVM { get; set; }
        public ValueAddedServicesVM VASesVM { get; set; }
        public OrderSummaryVM OrderSummary { get; set; }
        public Customer Customer { get; set; }
        

    }

    #endregion

    #region HomePackageVM

    [Serializable]
    public class HomePackageVM
    {
        public HomePackageVM()
        {
            AvailableHomePackages = new List<AvailableHomePackage>();
        }

        public int TabNumber { get; set; }
        public List<AvailableHomePackage> AvailableHomePackages { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select a Plan")]
        [Display(Name = "Plan")]
        public int SelectedPgmBdlPkgCompID { get; set; }

        [Display(Name = "WBB Plan")]
        public int SelectedOptPgmBdlPkgCompID { get; set; }
    }

    [Serializable]
    public class AvailableHomePackage
    {
        public int PgmBdlPkgCompID { get; set; }
        public string ProgramName { get; set; }
        public string BundleName { get; set; }
        public string PackageName { get; set; }
        public string PackageCode { get; set; }
        public decimal Price { get; set; }
        public decimal MonthlySubscription { get; set; }
    }

    #endregion

    #region Mobile No

    [Serializable]
    public class HomeMobileNoVM
    {
        public HomeMobileNoVM()
        {
            SelectedMobileNumber = new List<string>();
        }

        public int TabNumber { get; set; }
        [Display(Name = "Mobile Number")]
        public List<string> SelectedMobileNumber { get; set; }
        public int NoOfDesireMobileNo { get; set; }
        //public List<string> MobileNumbers { get; set; }
    }

    #endregion

    #region Home Registration Order Summary

    [Serializable]
    public class HomeOrderSummaryVM
    {
        public HomeOrderSummaryVM()
        {
            Sublines = new List<SublineVM>();
            VasVM = new ValueAddedServicesVM();
            MobileNumbers = new List<string>();
            Sublines = new List<SublineVM>();
        }

        public bool fromSubline { get; set; }

        // Device
        public int SelectedModelImageID { get; set; }
        public string ModelImageUrl { get; set; }
        [Display(Name = "Brand")]
        public int BrandID { get; set; }
        public string BrandName { get; set; }
        [Display(Name = "Model")]
        public int ModelID { get; set; }
        public string ModelName { get; set; }
        [Display(Name = "Colour")]
        public int ColourID { get; set; }
        public string ColourName { get; set; }

        // Mobile No
        [Display(Name = "Mobile Number")]
        public List<string> MobileNumbers { get; set; }

        // Plan
        public int SelectedPgmBdlPkgCompID { get; set; }
        [Display(Name = "Plan Bundle")]
        public string PlanBundle { get; set; }
        [Display(Name = "Plan Name")]
        public string PlanName { get; set; }
        [Display(Name = "Phone Price")]
        public decimal Price { get; set; }
        [Display(Name = "Monthly Subscription")]
        public decimal MonthlySubscription { get; set; }
        [Display(Name = "Local Calls")]
        public int LocalCalls { get; set; }
        [Display(Name = "Local SMS")]
        public int LocalSMS { get; set; }
        [Display(Name = "Data")]
        public int Data { get; set; }

        // Wbb pLAN
        public int SelectedOptPgmBdlPkgCompID { get; set; }
        [Display(Name = "WBB Plan Bundle")]
        public string OptPlanBundle { get; set; }
        [Display(Name = "WBB Plan Name")]
        public string OptPlanName { get; set; }
        [Display(Name = "WBB Phone Price")]
        public decimal OptPrice { get; set; }
        [Display(Name = "WBB Monthly Subscription")]
        public decimal OptMonthlySubscription { get; set; }
        [Display(Name = "WBB Local Calls")]
        public int OptLocalCalls { get; set; }
        [Display(Name = "WBB Local SMS")]
        public int OptLocalSMS { get; set; }
        [Display(Name = "WBB Data")]
        public int OptData { get; set; }

        // VAS
        public ValueAddedServicesVM VasVM { get; set; }

        // Subline
        public List<SublineVM> Sublines { get; set; }

        // Total Price
        public decimal TotalPrice { get; set; }

    }

    #endregion

    #region Customer Details

    [Serializable]
    public class CustomerDetailsVM
    {
        public CustomerDetailsVM()
        {
            Customer = new Customer()
            {
                LastAccessID = "SYSTEM"
            };
            Address = new Address()
            {
                LastAccessID = "SYSTEM"
            };

            RegStatus = new DAL.Models.RegStatus();
            AddressVM = new AddressVM();
            RegStatusResult = new List<RegStatusResult>();
            StatusReasonIDs = new List<int>();
        }

        [Display(Name = "Registration ID")]
        public int RegID { get; set; }
        [Display(Name = "Queue No")]
        public string QueueNo { get; set; }
        [Display(Name = "Status")]
        public int StatusID { get; set; }

        public bool IsRegClosed { get; set; }
        public bool IDCardNoValidate { get; set; }
        public int TabNumber { get; set; }
        public int RegTypeID { get; set; }
        [Required]
        [Display(Name = "Day of DOB")]
        public int DOBDay { get; set; }
        [Required]
        [Display(Name = "Month of DOB")]
        public int DOBMonth { get; set; }
        [Required]
        [Display(Name = "Year of DOB")]
        public int DOBYear { get; set; }
        public Customer Customer { get; set; }
        public Address Address { get; set; }
        public AddressVM AddressVM { get; set; }
        public RegStatus RegStatus { get; set; }
        public List<RegStatusResult> RegStatusResult { get; set; }
        [Required]
        [Display(Name = "Day of Sales")]
        public int RFSalesDateDay { get; set; }
        [Required]
        [Display(Name = "Month of Sales")]
        public int RFSalesDateMonth { get; set; }
        [Required]
        [Display(Name = "Year of Sales")]
        public int RFSalesDateYear { get; set; }
        public List<int> StatusReasonIDs { get; set; }

        [Display(Name = "Internal")]
        public bool BlacklistInternal { get; set; }
        [Display(Name = "External")]
        public bool BlacklistExternal { get; set; }
        [Display(Name = "Biometric Verification")]
        public bool BiometricVerify { get; set; }
        [Display(Name = "Duplicate Check")]
        public bool DuplicateCheck { get; set; }
        [Required]
        [Display(Name = "Kenan Account No *")]
        public string KenanAccountNo { get; set; }
        [Required]
        [Display(Name = "Modem ID *")]
        public string ModemID { get; set; }
        [Required]
        [Display(Name = "Status *")]
        public int RegStatusID { get; set; }
    }

    #endregion

    #region Address

    [Serializable]
    public class AddressVM
    {
        public AddressVM()
        {
            Addresses = new List<Address>();
            Address = new InstallationAddress();
            BillingAddress = new ViewModels.BillingAddress();
            PermanentAddress = new ViewModels.PermanentAddress();
            InstallationAddress = new ViewModels.InstallationAddress();
            Registration = new DAL.Models.Registration();
        }

        public int TabNumber { get; set; }
        public Address Address { get; set; }
        public DAL.Models.Registration Registration { get; set; }
        public List<Address> Addresses { get; set; }
        public PermanentAddress PermanentAddress { get; set; }
        public BillingAddress BillingAddress { get; set; }
        public InstallationAddress InstallationAddress { get; set; }

        public string InstallationFullAddress { get; set; }

        public string ValidatedPostcode { get; set; }

        [Display(Name = "Preferred Installation Time")]
        public DateTime PreferredInstallationTime { get; set; }
    }

    [Serializable]
    public class InstallationAddress : Address 
    {
        public InstallationAddress()
        {
            LastAccessID = "SYSTEM";
        }

        [Required]
        [Display(Name = "Building/House No *")]
        public string InstallBuildingNo { get; set; }

        [Required]
        [Display(Name = "Street/Area *")]
        public string InstallStreet { get; set; }
    }

    [Serializable]
    public class BillingAddress : Address
    {
        public BillingAddress()
        {
            LastAccessID = "SYSTEM";
        }
        public int AlternateStateID { get; set; }
    }

    [Serializable]
    public class PermanentAddress : Address
    {
        public PermanentAddress()
        {
            LastAccessID = "SYSTEM";
        }
        public int AlternateStateID { get; set; }
    }

    #endregion

    #region Search Registration

    [Serializable]
    public class SearchHomeRegistrationVM
    {
        public SearchHomeRegistrationVM()
        {
            RegistrationSearchResults = new List<RegistrationSearchResult>();
        }

        public RegistrationSearchCriteria RegistrationSearchCriteria { get; set; }
        public List<RegistrationSearchResult> RegistrationSearchResults { get; set; }
    }

    #endregion

    #region Postcode Validation

    [Serializable]
    [XmlRootAttribute(ElementName = "MxsPostCodeValidationData")]
    public class PostcodeValidate
    {
        [XmlElement(ElementName = "postcode")]
        public string Postcode { get; set; }
    }

    #endregion
}
