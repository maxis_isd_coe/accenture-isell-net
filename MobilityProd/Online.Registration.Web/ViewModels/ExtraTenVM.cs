﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.SubscriberICService;

namespace Online.Registration.Web.ViewModels
{

    [Serializable]
    public class extraTenObject
    {
        public string ID
        {
            get;
            set;
        }
        public string OperationType
        {
            get; set;
        }

        public string OldMSISDN
        {
            get;set;
        }

        public string NewMSISDN
        {
            get;set;
        }

        public string Status
        {
            get;set;
        }
        public string StatusReason
        {
            get;set;
        }
        public string RegID
        {
            get;set;
        }        
        public DateTime CreateDate
        {
            get;set;
        }
        public DateTime UpdateDate
        {
            get;set;
        }
        public string CreatedyBy
        {
            get;set;
        }
        public string UpdatedBy
        {
            get;set;
        }
        public string ExtraTenMSISDN
        {
            get;
            set;
        }
        public string CounterValue
        {
            get;
            set;
        }

    }

    [Serializable]
    public class ExtraTenResponse
    {
        private string msgCode;
        private string msgDesc;
        private string chargeAmt;
        private string counter;
        private string externalMSISDN;
        private List<string> retrivedMSISDNList = new List<string>();
        public string MsgCode
        {
            get { return msgCode; }
            set { msgCode = value; }
        }
        public string MsgDesc
        {
            get { return msgDesc; }
            set { msgDesc = value; }
        }
        public string ChargeAmt
        {
            get { return chargeAmt; }
            set { chargeAmt = value; }
        }
        public string Counter
        {
            get { return counter; }
            set { counter = value; }
        }
        public string ExternalMSISDN
        {
            get { return externalMSISDN; }
            set { externalMSISDN = value; }
        }
        public List<string> RetrivedMSISDNList
        {
            get { return retrivedMSISDNList; }
            set { retrivedMSISDNList = value; }
        }
    }

    [Serializable]
    public class ExtraTenVM
    {
        private CustomizedAccount _account;
        private CustomizedCustomer _customer;
        private string _externalId;
        private string _acctExtId;
        private Online.Registration.Web.SubscriberICService.Components components= new SubscriberICService.Components();
        private string corriderID;
        private ExtraTenResponse _extraTenMSISDNDetails = new ExtraTenResponse();
        public CustomizedAccount Account
        {
            get { return _account; }
            set { _account = value; }
        }
        //customer filed
        public CustomizedCustomer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }
        
        public string ExternalId
        {
            get { return _externalId; }
            set { _externalId = value; }
        }
        
        public string AcctExtId
        {
            get { return _acctExtId; }
            set { _acctExtId = value; }
        }

        public Online.Registration.Web.SubscriberICService.Components Components
        {
            get { return components; }
            set { components = value; }
        }
        
        public string CorriderID
        {
            get { return corriderID; }
            set { corriderID = value; }
        }
        public ExtraTenResponse ExtraTenMSISDNDetails
        {
            get { return _extraTenMSISDNDetails; }
            set { _extraTenMSISDNDetails = value; }
        }
       
    }

    [Serializable]
    public class ExtraTenAccounts
    {
        private List<ExtraTenVM> _extraTenAccountList = new List<ExtraTenVM>();

        public List<ExtraTenVM> ExtraTenAccounts1
        {
            get { return _extraTenAccountList; }
            set { _extraTenAccountList = value; }
        }
    }

   

}