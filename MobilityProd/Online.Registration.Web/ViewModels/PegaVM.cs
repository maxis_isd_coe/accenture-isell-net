﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.Helper;

using Online.Registration.Web.PegaSvc;

namespace Online.Registration.Web.ViewModels
{
	[Serializable]
	public class PegaVM
	{
		public PegaVM()
		{
			executeAssessmentResponse = new ExecuteAssessmentResponse();
			updateResponseResponse = new UpdateResponseResponse();
			selectedOfferText = string.Empty;
			selectedOfferValue = 0;
			defaultCaseCategory = Util.GetDefaultCaseCategory().Text;
			defaultCaseCategoryValue = Util.GetDefaultCaseCategory().Value;
		}
		public ExecuteAssessmentResponse executeAssessmentResponse { get; set; }
		public UpdateResponseResponse updateResponseResponse { get; set; }
		public string selectedOfferText { get; set; }
		public int selectedOfferValue { get; set; }
		public string defaultCaseCategory { get; set; }
		public string defaultCaseCategoryValue { get; set; }
		public string subscriberNo { get; set; }
	}

	[Serializable]
	public class PegaOfferVM
	{
		public PegaOfferVM()
		{
			customerOfferDetailsList = new List<CustomerOfferDetailsVM>();
		}
		public List<CustomerOfferDetailsVM> customerOfferDetailsList { get; set; }
		public string idCardNumber { get; set; }
	}

	[Serializable]
	public class CustomerOfferDetailsVM
	{
		public int id { get; set; }
		public string msisdn { get; set; }
		public string kenanCode { get; set; }
		public string offerName { get; set; }
		public string action { get; set; }
		public string prepositionID { get; set; }
	}

	[Serializable]
	public class UpdateResponseVM
	{
		public string externalID { get; set; }
	}

}