﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using Online.Registration.DAL.Models;

namespace Online.Registration.Web.ViewModels
{
    #region Guided Sales

    [Serializable]
    public class GuidedSalesVM
    {
        public GuidedSalesVM()
        {
            DevicePropertyVM = new DevicePropertyVM();
            PlanPropertyVM = new PlanPropertyVM();
        }

        public bool IsSearch { get; set; }
        public string SelectedCriterias { get; set; }
        public DevicePropertyVM DevicePropertyVM  { get; set; }
        public PlanPropertyVM PlanPropertyVM { get; set; }
    }

    #endregion
    #region Device Property
    [Serializable]
    public class DevicePropertyVM
    {
        public DevicePropertyVM()
        {
            DeviceOptions = new List<DeviceOption>();
            AvailableModelImage = new List<AvailableModelImage>();
        }

        public List<AvailableModelImage> AvailableModelImage { get; set; }
        public List<DeviceOption> DeviceOptions { get; set; }
        public string SelectedCriterias { get; set; }
        public string HasCamera { get; set; }
        //[Required]
        //[Display(Name = "Model")]
        //public string SelectedModelImageID { get; set; }

      
        [Display(Name = "PlanSearch")]
        public string PlanSearch { get; set; }

        [Display(Name = "DeviceSearch")]
        public string DeviceSearch { get; set; }
        
        [Display(Name = "Plan&DeviceSearch")]
        public string PlanAndDeviceSearch { get; set; }


        public bool IsSearch { get; set; }

        public SelectedModel SelectedModel { get; set; }
    }

    [Serializable]
    public class SelectedModel
    {
        [Required(ErrorMessage = "Please select a Plan or a Model.")]
        [Display(Name = "Model")]
        public string ImageID { get; set; }
    }

    [Serializable]
    public class DeviceOption
    {
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
        public Dictionary<int,string> Criterias { get; set; }
    }
    #endregion
    
    #region Plan Property

    [Serializable]
    public class PlanPropertyVM
    {
        public PlanPropertyVM()
        {
            AvailablePackages = new List<AvailablePackage>();
            PlanOption = new PlanOption();
            SelectedPgmBdlPkgComp = new SelectedPgmBdlPkgComp();
        }

        //public int SelectedPgmBdlPkgCompID { get; set; }
        public List<AvailablePackage> AvailablePackages { get; set; }
        public PlanOption PlanOption { get; set; }
        public string SelectedCriterias { get; set; }
        public bool IsSearch { get; set; }

        public SelectedPgmBdlPkgComp SelectedPgmBdlPkgComp { get; set; }
    }

    [Serializable]
    public class SelectedPgmBdlPkgComp
    {
        [Required(ErrorMessage = "Please select a Plan or a Model.")]
        [Display(Name = "Plan")]
        public string PgmBdlPkgCompID { get; set; }
    }

    [Serializable]
    public class PlanOption
    {
        public int VoiceUsage { get; set; }
        public int DataUsage { get; set; }
        public int SMSUsage { get; set; }
        public int Price { get; set; }
    }

    #endregion
}