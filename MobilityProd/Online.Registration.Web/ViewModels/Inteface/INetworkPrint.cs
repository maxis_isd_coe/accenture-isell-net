﻿using Online.Registration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Online.Registration.Web.ViewModels.Inteface
{
    public interface INetworkPrint
    {
        /// <summary>
        /// Get or set printer id to pass for printing
        /// </summary>
        string PrinterId { get; set; }
        /// <summary>
        /// Print order id for generate file name
        /// </summary>
        string PrintOrderId {get;}
        /// <summary>
        /// Get or set printer name to pass for printing
        /// </summary>
        string PrinterName{get;set;}
        /// <summary>
        /// Get all printer list for this context
        /// </summary>
        IEnumerable<SelectListItem> GetPrinters {get;}
        /// <summary>
        /// Get preview print url for this context
        /// </summary>
        string PreviewPrintUrl { get;}
        /// <summary>
        /// Get network print url for this context
        /// </summary>
        string NetworkPrintUrl { get;}
    }
}
