﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.DAL.Models;
using System.ComponentModel.DataAnnotations;

namespace Online.Registration.Web.ViewModels
{
    [Serializable]
    public class SimReplacementVm
    {

        public SimReplacementReasons SimReplacementReasons { get; set; }
        public SimReplacementReasonDetails SimReplacementReasonDetail { get; set; }
        public SimDetlsList simDetlsList { get; set; }
        public PersonalDetailsVM PersonalDetails { get; set; }
    }
    [Serializable]
    public class SimReplacementPaymentRecvd
    {
        public int RegId { get; set; }

        public string AccountType { get; set; }
    }
    [Serializable]
    public class SimReplacementReasonDetails
    {
       
        public string CmssId { get; set; }

        public bool IsWaiver { get; set; }

        public Decimal PenaltyAmount { get; set; }

        public decimal WaiverAmount { get; set; }

        public int SimModelId { get; set; }

        public int SimArticalId { get; set; }

        public string ActiveDt { get; set; }

        public string SimType { get; set; }
    }
    [Serializable]
    public class SimDetlsList
    {
        public SimDetlsList(string ExteranalId, string ExteranalIdType, string InventoryTypeId, string InventoryDisplayValue, string Reason, string ActiveDt, string InActiveDt)
        {

            this.ExteranalId = ExteranalId;
            this.ExteranalIdType = ExteranalIdType;
            this.InventoryTypeId = InventoryTypeId;
            this.InventoryDisplayValue = InventoryDisplayValue;
            this.Reason = Reason;
            this.ActiveDt = ActiveDt;
            this.InActiveDt = InActiveDt;
        }

        public string ExteranalId { get; set; }

        public string ExteranalIdType { get; set; }
       
        public string InventoryTypeId { get; set; }
       
        public string InventoryDisplayValue { get; set; }
       
        public string Reason { get; set; }
       
        public string ActiveDt { get; set; }
        
        public string InActiveDt { get; set; }
    }
}