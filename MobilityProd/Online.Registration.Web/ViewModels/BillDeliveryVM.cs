﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.Web.SubscriberICService;

namespace Online.Registration.Web.ViewModels
{

    [Serializable]
    public class billDeliveryObject
    {
        public string ID
        {
            get;
            set;
        }
        public string EmailBillSubscription
        {
            get;
            set;
        }
        public string EmailAddress
        {
            get;
            set;
        }
        public string SmsAlertFlag
        {
            get;
            set;
        }
        public string SmsNo
        {
            get;
            set;
        }
        public string RegID
        {
            get;
            set;
        }
        public DateTime CreateDate
        {
            get;
            set;
        }
        public DateTime UpdateDate
        {
            get;
            set;
        }
        public string CreatedyBy
        {
            get;
            set;
        }
        public string UpdatedBy
        {
            get;
            set;
        }

    }

    [Serializable]
    public class BillDeliveryResponse
    {
        private string msgCode;
        private string msgDesc;
        private string hcSuppressSubscription;
        private string emailBillSubscription;
        private string emailAddress;
        private string smsAlertFlag;
        private string smsNo;
        private string emailSubscriptionStatus;
        private string updatedDate;
        private string updatedTime;
        private string updatedBy;
        private string updatedChannel;

        public string MsgCode
        {
            get { return msgCode; }
            set { msgCode = value; }
        }
        public string MsgDesc
        {
            get { return msgDesc; }
            set { msgDesc = value; }
        }
        public string HcSuppressSubscription
        {
            get { return hcSuppressSubscription; }
            set { hcSuppressSubscription = value; }
        }
        public string EmailBillSubscription
        {
            get { return emailBillSubscription; }
            set { emailBillSubscription = value; }
        }
        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; }
        }
        public string SmsAlertFlag
        {
            get { return smsAlertFlag; }
            set { smsAlertFlag = value; }
        }
        public string SmsNo
        {
            get { return smsNo; }
            set { smsNo = value; }
        }
        public string EmailSubscriptionStatus
        {
            get { return emailSubscriptionStatus; }
            set { emailSubscriptionStatus = value; }
        }
        public string UpdatedDate
        {
            get { return updatedDate; }
            set { updatedDate = value; }
        }
        public string UpdatedTime
        {
            get { return updatedTime; }
            set { updatedTime = value; }
        }
        public string UpdatedBy
        {
            get { return updatedBy; }
            set { updatedBy = value; }
        }
        public string UpdatedChannel
        {
            get { return updatedChannel; }
            set { updatedChannel = value; }
        }
    }

    [Serializable]
    public class BillDeliveryVM
    {
        private string _acctExtId;
        private List<string> _availableMsisdn;
        private BillDeliveryResponse _billDeliveryDetails = new BillDeliveryResponse();
        private bool _result;
        private string _errorMessage;

        public int TabNumber { get; set; }

        public string AcctExtId
        {
            get { return _acctExtId; }
            set { _acctExtId = value; }
        }

        public List<string> AvailableMsisdn
        {
            get { return _availableMsisdn; }
            set { _availableMsisdn = value; }
        }

        public BillDeliveryResponse BillDeliveryDetails
        {
            get { return _billDeliveryDetails; }
            set { _billDeliveryDetails = value; }
        }

        public bool Result
        {
            get { return _result; }
            set { _result = value; }
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

    }

}