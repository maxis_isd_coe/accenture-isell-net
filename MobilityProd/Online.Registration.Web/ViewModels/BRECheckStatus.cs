﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.ViewModels
{
    [Serializable]
    public class BRECheckStatus
    {
        public bool isMenuSuccess { get; set; }
        public bool BREStatus { get; set; }
    }
}
