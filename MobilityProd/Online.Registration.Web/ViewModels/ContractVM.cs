﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Online.Registration.Web.ViewModels
{
    [Serializable]
    public class ContractObject
    {
        [Display(Name = "Offer")]
        public string principalOffer { get; set; }

        [Display(Name = "Phone Model & Recommended Retail")]
        public string principalPhoneModelRRP { get; set; }

        [Display(Name = "Offer Option (Fill upfront payment option)")]
        public string principalPhoneModelBundle { get; set; }

        [Display(Name = "Bill Plan")]
        public string principalPlan { get; set; }

        [Display(Name = "Contract")]
        public string principalContract { get; set; }


        [Display(Name = "Offer")]
        public string secondaryOffer { get; set; }

        [Display(Name = "Phone Model & Recommended Retail")]
        public string secondaryPhoneModelRRP { get; set; }

        [Display(Name = "Offer Option (Fill upfront payment option)")]
        public string secondaryPhoneModelBundle { get; set; }

        [Display(Name = "Bill Plan")]
        public string secondaryPlan { get; set; }

        [Display(Name = "Contract")]
        public string secondaryContract { get; set; }

        public bool isSupplementary { get; set; }
        public string ContractTermsAndConditions { get; set; }
        public string ContractVoiceTermsAndConditions { get; set; }
        public string DeviceFinancingMonthlyFee { get; set; }
        public string DeviceFinancingUpgradeDuration { get; set; }
        public string IMEI { get; set; }
    }
}