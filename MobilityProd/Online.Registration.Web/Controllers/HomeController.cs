﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using log4net;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.Models;
using Online.Registration.Web.Properties;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.UserSvc;
using SNT.Utility;
using Online.Registration.Web.CommonEnum;
using System.Threading.Tasks;
using System.Threading;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.PegaSvc;
using System.IO;
using System.Web;
using Online.Registration.Web.ContentRepository;
using Online.Registration.Web.CatalogSvc;


namespace Online.Registration.Web.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class HomeController : Controller
    {
        private static CorpType corporateSearchType;

        public string UpdatePopUpMessage(string ID, string action)
        {
            string ErrorMsg = "ERROR: Failed to Update Message";

            if (!string.IsNullOrEmpty(ID) && (action == "accept" || action == "reject"))
            {
                try
                {
                    Logger.InfoFormat("Entering {0}; Action:{1}; ID:{2}", "UpdatePopUpMessage", action, ID);
                    using (var regProxy = new RegistrationServiceProxy())
                    {
                        int result = regProxy.UpdatePopUpMessageAcceptReject(ID.ToInt(), action);
                        ErrorMsg = result > 0 ? "Success" : "Error";
                    }
                    Logger.InfoFormat("Exit {0}; Action:{1}; ID:{2}; {3} ", "UpdatePopUpMessage", action, ID, ErrorMsg);
                }

                catch (Exception ex)
                {
                    Logger.Info("Exception while excuting the UpdatePopUpMessage method:" + ex);
                    ErrorMsg = "ERROR: Exception Failed to Update Message";
                }
            }
            return ErrorMsg;
        }

        #region GenerateStateCityByPostCode RST 30MR15
        [HttpPost]
        public JsonResult GenerateStateCityByPostCode(string postcode)
        {
            /*
            //string result = string.Empty;
            string _postcode = string.Empty;
            string _state = string.Empty;
            int _stateID = 0;
            string _city = string.Empty;
            //List<PostCodeCityState> result = new List<PostCodeCityState>();
            var ALLPostCodeList = MasterDataCache.Instance.GetPostCodeCityState;
            var StateList = MasterDataCache.Instance.State;
            if (!string.IsNullOrEmpty(postcode))
            {
                int found = ALLPostCodeList.Where(a => a.PostCode == postcode && a.Active == true).Count();
                if (found == 1)
                {
                    var Active_postcodelist = ALLPostCodeList.Where(a => a.PostCode == postcode && a.Active == true).ToList();
                    _postcode = Active_postcodelist[0].PostCode.ToString2();
                    _state = Active_postcodelist[0].State.ToString2();
                    _city = Active_postcodelist[0].City.ToString2();
                    _stateID = StateList.State.Where(a => a.Name == _state && a.Active == true).FirstOrDefault().ID;
                }
            }*/

            string city = string.Empty;
            string state = string.Empty;
            int stateID = 0;
            var result = WebHelper.Instance.checkStateCityByPostcode(postcode);
            postcode = string.Empty;
            if (!ReferenceEquals(result,null))
            {
                if (string.IsNullOrEmpty(result.ErrorMessage))
                {
                    postcode = result.PostCode;
                    state = result.State;
                    city = result.City;
                    stateID = result.stateID;
                }
            }
            
            return new JsonResult() { Data = new { postCode = postcode, state = state, stateID = stateID, city = city } };
        }
        #endregion

        #region Members

        private static readonly ILog Logger = LogManager.GetLogger(typeof(HomeController));
        bool isSuperAdmin = false;

        #endregion

        #region Actions

        [Authorize]
        public ActionResult WebPOSStatusUpdate(string loginID, string msgCode, string msgDesc, string orderID)
        {
            Logger.Info("WebPOSStatusUpdate is called");
            Logger.Info("WebPOSStatusUpdate is Invoked with MsgCode:" + loginID + "msgCode:" + msgCode + " msgDesc:"+ msgDesc + " with OrderID:" + orderID);
            string methodName = "";
            //DB LOG
            string response = "WebPOSStatusUpdate is Invoked with MsgCode:" + loginID + " msgCode:" + msgCode + " msgDesc: " + msgDesc + " with OrderID:" + orderID;
            Util.LogToDB("HomeController", "WebPOSStatusUpdate", "", response, "", 0, orderID.ToString2());
            
            if (!string.IsNullOrEmpty(msgCode) && !string.IsNullOrEmpty(orderID))
            {
                //Logger.Info("Order Payment Status Updation is Invoked with OrderID " + orderID);
                //using (var proxy = new RegistrationServiceProxy())
                //{
                //    proxy.RegistrationCancel(new RegStatus()
                //    {
                //        RegID = orderID.ToInt(),
                //        Active = true,
                //        CreateDT = DateTime.Now,
                //        StartDate = DateTime.Now,
                //        LastAccessID = Util.SessionAccess.UserName,
                //        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                //    });
                //}
                //Logger.Info("Order Payment Status Updation is done with OrderID " + orderID);

                using (var proxy = new AutoActivationServiceProxy())
                {
                    proxy.UpdateWebPOSStatus(loginID, msgCode, msgDesc, orderID);
                }

                if (msgCode == "0")
                {
                    methodName = "PaymntReceived";
                }
                else
                {
                    // methodName = "MobileRegFail";
                    Logger.Info("WebPOSStatusUpdate is Invoked with MsgCode" + msgCode + " with OrderID " + orderID);
                    Logger.Info("WebPOSStatusUpdate is ended");
                    return View(msgCode.ToInt());
                    //return RedirectToAction(methodName, "Home", new { regID = msgCode });
                }
            }
            else
            {
                Logger.Info("WebPOSStatusUpdate is Invoked with MsgCode" + msgCode + " with OrderID " + orderID);
                Logger.Info("WebPOSStatusUpdate is ended");
                return View(msgCode.ToInt());
            }
            Logger.Info("WebPOSStatusUpdate is Invoked with MsgCode" + msgCode + " with OrderID " + orderID);
            Logger.Info("WebPOSStatusUpdate is ended");

            return RedirectToAction(methodName, "Registration", new { regID = orderID });
        }

        [Authorize]
        public ActionResult Index()
        {
            return RedirectToAction("IndexNew");
            /*
            Session[SessionKey.WriteOffAccts.ToString()] = null;
            Session[SessionKey.TransStartTime.ToString()] = null;
            Session[SessionKey.ContractDetails.ToString()] = null;
            Session[SessionKey.FxInternalCheckMessage.ToString()] = null;
            Session["IsOlo"] = null;
            Session["_componentViewModel"] = null;
            Session["WriteOffHardstop"] = null;
            Session["AllowSimReplacement"] = null;
            Session["AllowSimReplacementForWriteOff"] = null;
            Session["HardStop"] = null;
            Session["BreChecksbyUserID"] = null;
            Session["BreStatusDetails"] = null;
            Session["isMenuSuccess"] = null;
            Session["isHardStop"] = null;
            Session["BREFailedRules"] = null;
            ClearSessionsALL();
            //added by ravi session clearing for business checks on Feb 10 2014
            Session["BusinessCheck"] = null;
            //added by ravi session clearing for business checks on Feb 10 2014 ends here

            Session["AccountswithContractCount"] = null;
            Session["HasContract"] = null;
            //Added for CLearing Sessions in case of promotional offers
            Session[SessionKey.FromPromoOffer.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_UOMCode.ToString()] = null;
            Session[SessionKey.ArticleId.ToString()] = null;
            Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
            //added by ravi session clearing by ravi ends here

            ViewBag.Message = "Home Screen";
            ViewBag.SearchBy = "No Search";
            Session[SessionKey.Searchstatus.ToString()] = null;
            Session[SessionKey.MenuType.ToString()] = null;
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = null;
            WebHelper.Instance.ResetDepositSessions();
            Session[SessionKey.RegK2_Status.ToString()] = null;
            Session[SessionKey.SelectedComponents.ToString()] = null;
            Session[SessionKey.AssignNew.ToString()] = null;
            Session[SessionKey.SpendLimit.ToString()] = null;

            Session[SessionKey.SuppleMsisdns.ToString()] = null;
            Session[SessionKey.IsSuppleNewAccount.ToString()] = null;
            Session[SessionKey.isVIP.ToString()] = null;
            #region VLT ADDED CODE
            clearUserRegistration();
            #endregion VLT ADDED CODE

            Session[SessionKey.IsSuppleNewAccount.ToString()] = false;
            Session[SessionKey.isSuperAdmin.ToString()] = Roles.IsUserInRole("MREG_SV");

            Session[SessionKey.CompanyInfoResponse.ToString()] = null;
            Session[SessionKey.isFromBRNSearch.ToString()] = null;

            //Added by Pradeep
            Session["RegMobileReg_BiometricVerify"] = null;
            Session["RegMobileReg_BiometricID"] = null;
            Session[SessionKey.BiometricDesc.ToString()] = null;

            //Added by Mahesh for Outstanding accounts for Dealers
            Session["OutStandingAmount"] = null;
            Session["OutStandingAcctDtls"] = null;
            Session["IDCardTypeKenanCode"] = null;

            DropFourObj dropObj = new DropFourObj();
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            

            //Code added by VLT For Redirection on Apr 18 2013
            if (Roles.IsUserInRole("MREG_CH") && Roles.IsUserInRole("MREG_SK"))
            {
            }
            else if (Roles.IsUserInRole("MREG_C") || Roles.IsUserInRole("MREG_W") || Roles.IsUserInRole("MREG_U") || Roles.IsUserInRole("MREG_R")
                                || Roles.IsUserInRole("MREG_RA") || Roles.IsUserInRole("MREG_TL") || Roles.IsUserInRole("SMRT_USER"))
            {
            }
            else
            {
                if (Roles.IsUserInRole("MREG_CH"))
                {
                    //Code Added by VLT for Menu Type on Apr 2 2013
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.Cashier);
                    //end of Code Added by VLT for Menu Type on Apr 2 2013
                    Response.Redirect("~/Registration/Cashier");
                }
                if (!Roles.IsUserInRole("MREG_DSV") && Roles.IsUserInRole("MREG_SK"))
                {
                    //Code Added by VLT for Menu Type on Apr 2 2013
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.StoreKeeper);
                    //end of Code Added by VLT for Menu Type on Apr 2 2013
                    Response.Redirect("~/Registration/StoreKeeper");
                    //Response.Redirect("~/Registration/SuperVisorForWaiveOff");           

                }

                if (Roles.IsUserInRole("MREG_DIC") || Roles.IsUserInRole("MREG_DSK"))
                {
                    //Code Added by VLT for Menu Type on Apr 2 2013
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.StoreKeeper);
                    //end of Code Added by VLT for Menu Type on Apr 2 2013
                    Response.Redirect("~/Registration/StoreKeeper");
                    //Response.Redirect("~/Registration/SuperVisorForWaiveOff");           

                }
                if (Roles.IsUserInRole("MREG_DCH"))
                {
                    //Code Added by VLT for Menu Type on Apr 2 2013
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.Cashier);
                    //end of Code Added by VLT for Menu Type on Apr 2 2013
                    Response.Redirect("~/Registration/Cashier");
                }
                if (!Roles.IsUserInRole("MREG_DSV") && (Roles.IsUserInRole("MREG_CH") || Roles.IsUserInRole("DREG_CH")))
                {
                    //Code Added by VLT for Menu Type on Apr 2 2013
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.Cashier);
                    //end of Code Added by VLT for Menu Type on Apr 2 2013
                    Response.Redirect("~/Registration/Cashier");
                }
                
               
            }
            //End of Code added by VLT For Redirection on Apr 18 2013
            return View();*/
        }



        public ActionResult About()
        {
            return View();
        }

        public ActionResult AdminLinks()
        {
            Session[SessionKey.MenuType.ToString()] = null;

            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        public ActionResult StoreError()
        {
            return View();
        }

        public ActionResult MediaGallery()
        {
            return View();
        }


        [Authorize]
        public ActionResult MobilityServices()
        {
            return View();
        }

        public string CreateCMSSCase(string newCBRNo)
        {
            string custIdentity = string.Empty;
            if (Session[SessionKey.PPID.ToString()] != null)
            {
                var AcctListByICResponse = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                SubscriberICService.CustomizedCustomer customerInfo = new CustomizedCustomer();
                SubscriberICService.Items selectedAccount = new Items();
                foreach (var active_acc in AcctListByICResponse.itemList)
                {
                    if (active_acc.Customer != null)
                    {
                        selectedAccount = active_acc;
                    }
                }
                //selectedAccount = AcctListByICResponse.itemList.FirstOrDefault();
                customerInfo = selectedAccount.Customer;
                if (customerInfo.NewIC != string.Empty)
                    custIdentity = customerInfo.NewIC;
                else if (customerInfo.PassportNo != string.Empty)
                    custIdentity = customerInfo.PassportNo;
                else if (customerInfo.OldIC != string.Empty)
                    custIdentity = customerInfo.OldIC;
                else if (customerInfo.OtherIC != string.Empty)
                    custIdentity = customerInfo.OtherIC;
                if (WebHelper.Instance.CompareCMSSDtForAcct(custIdentity) > 14.0)
                {
                    //Commented for timebeing as the service is throwing exception
                    var Cresp = WebHelper.Instance.CreateCaseInCMSS(selectedAccount.AcctExtId,
                        Constants.CMSS_CASE_COMPLEXITY,
                            ConfigurationManager.AppSettings["CMSSQueue"],
                            Constants.CMSS_CASE_PRIORITY,
                            ConfigurationManager.AppSettings["CMSSDirection"],
                            selectedAccount.AccountDetails.FxAcctNo,
                            selectedAccount.ExternalId,
                            (newCBRNo.ToString2().Length > 0 ? newCBRNo.ToString2() : string.Empty),
                            ConfigurationManager.AppSettings["CMSSProduct"],
                            ConfigurationManager.AppSettings["CMSSReason1"],
                            ConfigurationManager.AppSettings["CMSSReason2"],
                            ConfigurationManager.AppSettings["CMSSReason3"],
                            ConfigurationManager.AppSettings["CMSSSysName"],
                            ConfigurationManager.AppSettings["CMSSTitle"],
                            Constants.CMSS_TYPE,
                            Constants.CMSS_CASE_TOPROCEEDWITH,
                              0, 
                              custIdentity);
                    if (Cresp != null)
                        return Cresp.Id;
                }
            }

            return null;
        }
        [Authorize]
        public ActionResult SupplementaryLinePopUp(string id, string accid)
        {
            if (!string.IsNullOrWhiteSpace(id) && !string.IsNullOrEmpty(accid))
            {
                ViewBag.msnid = id;
                ViewBag.accid = accid;
            }
            return View();
        }


        [HttpPost]
        public ActionResult ValidationLinesAjax(FormCollection collection)
        {
            String errorMessage = String.Empty;
            try
            {
                retrieveAcctListByICResponse AcctListByIC = null;
                List<Online.Registration.Web.SubscriberICService.Items> AccntList = null;
                String serviceStatus = String.Empty;
                bool isSuccess = true;
                var status = String.Empty;
                int WriteOffAmount = 0;
                bool isMISM = false;
                string submittype = collection["submit1"].ToString();
                bool supplementaryLine = false;
				bool cdpuUser = false;

                String selectedExternalId = collection["SelectExternalId"].ToString();
                if (Session["AcctAllDetails"] != null)
                {
                    AcctListByIC = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                    AccntList = AcctListByIC != null ? AcctListByIC.itemList != null ? AcctListByIC.itemList.Where(c => c.ExternalId == selectedExternalId).ToList() : null : null;
                    serviceStatus = AccntList[0].ServiceInfoResponse.serviceStatus;
                    WriteOffAmount = AccntList[0].Customer != null ? AccntList[0].Customer.TotalWriteOff.ToInt() : 0;
                    isMISM = AccntList[0].IsMISM;
                }
                switch (submittype)
                {
                    case "btnAddContract":
                            if (serviceStatus != "A" && serviceStatus != "")
                            {
                                if (serviceStatus.ToString() == "S")
                                {
                                    status = "Suspend";
                                }
                                else if (serviceStatus.ToString() == "B")
                                {
                                    status = "Barred";
                                }
                                else if (serviceStatus.ToString() == "D")
                                {
                                    status = "Deactive";
                                }
                                errorMessage = "Your Number is on " + status + " status";
                                isSuccess = false;
                                // $("html, body").animate({ scrollTop: 0 }, "slow");
                            }
                            break;

                    case "btnAddCRP":
                             
                             if (serviceStatus != "A" && serviceStatus != "") 
                                {
                                    if (serviceStatus.ToString() == "S") {
                                        status = "Suspend";
                                    }
                                    else if (serviceStatus.ToString() == "B") {
                                        status = "Barred";
                                    }
                                    else if (serviceStatus.ToString() == "D") {
                                        status = "Deactive";
                                    }
                                   errorMessage ="Your Number is on " + status + " status";
                                   isSuccess = false;
                                   // $("html, body").animate({ scrollTop: 0 }, "slow");
                                }

                            if (WriteOffAmount > 0) {
                                errorMessage = "The selected MSISDN is classified under Writeoff Account, CRP is not allowed";
                                isSuccess = false;
                            }
                        break;

                    case "btnAddSuppline":
                        
                        Online.Registration.Web.Helper.PrinSupServiceProxy _proxyObj = new PrinSupServiceProxy();
                        int SupplinesCount = _proxyObj.getPrinSupplimentarylines(new SubscribergetPrinSuppRequest() { msisdn = AccntList[0].ExternalId }).itemList.Count;
                         int supliAcctConfig = System.Configuration.ConfigurationManager.AppSettings.Get("totalSupplimentaryLinesAllowed").ToInt();
                        if (serviceStatus != "A" && serviceStatus != "") {
                            if (serviceStatus.ToString() == "S") {
                                status = "Suspend";
                            }
                            else if (serviceStatus.ToString() == "B") {
                                status = "Barred";
                            }
                            else if (serviceStatus.ToString() == "D") {
                                status = "Deactive";
                            }
                              isSuccess = false;
                            errorMessage = "Your Number is on " + status + " status";
                        }

                        if (SupplinesCount != null && SupplinesCount > 0) {
                            if (SupplinesCount >= supliAcctConfig) {
                                errorMessage = "Selected Principal Account has more than or equal to " + supliAcctConfig + " supplementary Account Lines, you cannot proceed further.";
                                isSuccess = false;
                            }
                        }
						cdpuUser = Roles.IsUserInRole("MREG_CUSR");
                        supplementaryLine = true;
                        break;

                    case "btnAddMultiSim":

                        if (serviceStatus != "A" && serviceStatus != "")
                        {
                            if (serviceStatus.ToString() == "S")
                            {
                                status = "Suspend";
                            }
                            else if (serviceStatus.ToString() == "B")
                            {
                                status = "Barred";
                            }
                            else if (serviceStatus.ToString() == "D")
                            {
                                status = "Deactive";
                            }
                            isSuccess = false;
                            errorMessage = "Your Number is on " + status + " status";
                        }
						if (string.IsNullOrEmpty(errorMessage))
						{
							if (!Util.check2GBComponent(selectedExternalId, ""))
							{
								isSuccess = false;
								errorMessage = "Primary shoud have more than or equal to 2GB data component";
							}
						}
                        break;
                    //19062915 - Anthony - Add validation if the number is not in active status (for MNP Add Supplementary) [Bug #1019] - Start
                    case "btnAddSupplineMNP":
                        if (serviceStatus != "A" && serviceStatus != "")
                        {
                            if (serviceStatus.ToString() == "S")
                            {
                                status = "Suspend";
                            }
                            else if (serviceStatus.ToString() == "B")
                            {
                                status = "Barred";
                            }
                            else if (serviceStatus.ToString() == "D")
                            {
                                status = "Deactive";
                            }
                            errorMessage = "Your Number is on " + status + " status";
                            isSuccess = false;
                        }
                        break;
                    //19062915 - Anthony - Add validation if the number is not in active status (for MNP Add Supplementary) [Bug #1019] - End

                }

				return Json(new { success = isSuccess, message = errorMessage, supplementaryline = supplementaryLine, cdpuuser = cdpuUser });
            }
            catch (Exception ex)
            {
                //TODO: log
                return Json(new { success = false, message = errorMessage });
            }
        }


        [Authorize]
        [HttpPost, ValidateInput(false)]
        public ActionResult Index(FormCollection collection)
        {
           
            string IDCardTypeID = string.Empty;
            string IDCardNo = string.Empty;
            ViewBag.SearchBy = null;
            Session[SessionKey.VIP.ToString()] = null;
            Session[SessionKey.WO.ToString()] = null;
            Session[SessionKey.IDCardTypeName.ToString()] = null;
            Session["HardStop"] = null;
            Session["WriteOffHardstop"] = null;
            Session["AllowSimReplacementForWriteOff"] = null;
            Session["AllowSimReplacement"] = null ;
			Session["eligibleForMISM"] = false;
            retrieveAcctListByICResponse AcctListByIC = null;
            List<Online.Registration.Web.SubscriberICService.Items> AccntList = null;
            String SelectAccountDtls = String.Empty;
            String accntId = String.Empty;
            String susbcrNo = String.Empty;
            String susbcrNoResets = String.Empty;
            String serviceStatus = String.Empty;
            String isMISM = String.Empty;
            String princiSuppLine = String.Empty;
            String checkedState = String.Empty;
            String externalId = String.Empty;
            String accntIntId = String.Empty;
            String category = String.Empty;
            String MarketCode = String.Empty;
            try
            {
                string submittype = collection["submit1"].ToString();

                String selectedExternalId = collection["SelectExternalId"].ToString();
                if (Session["AcctAllDetails"] != null)
                {
                    AcctListByIC = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                    AccntList = AcctListByIC != null ? AcctListByIC.itemList != null ? AcctListByIC.itemList.Where(c => c.ExternalId == selectedExternalId).ToList() : null : null;
                    if (AccntList.Count > 0)
                    {
                        accntId = AccntList[0].AcctExtId;
                        susbcrNo = AccntList[0].SusbcrNo;
                        susbcrNoResets = AccntList[0].SusbcrNoResets;
                        serviceStatus = AccntList[0].ServiceInfoResponse.serviceStatus;
                        isMISM = Convert.ToString(AccntList[0].IsMISM);
                        princiSuppLine = AccntList[0].ServiceInfoResponse.prinSuppInd;
                        externalId = AccntList[0].ExternalId;
                        accntIntId = AccntList[0].AcctIntId;
                        checkedState = (AccntList[0].Account.IDType == "MSISDN") ? "true" : "false";
                        category = AccntList[0].AccountDetails != null ? AccntList[0].AccountDetails.AcctCategory.ToString2() : string.Empty;
                        MarketCode = AccntList[0].AccountDetails != null ? AccntList[0].AccountDetails.MktCode.ToString2() : string.Empty;
                    }
                }
                switch (submittype)
                {

                    case "btnAddContract":

                        SelectAccountDtls = accntId + "," + susbcrNo + "," + susbcrNoResets + "," + serviceStatus + "," + isMISM + "," 
                            + princiSuppLine + "," + checkedState;

                        return RedirectToAction("ContractCheck", "AddContract", new { SelectAccountDtls = SelectAccountDtls });
                        break;

                    case "btnAddCRP":
                        Session["GetPrinPlanDetails"] = SelectAccountDtls.ToString2();
                        Session[SessionKey.MSISDN.ToString()] = Session[SessionKey.ExternalID.ToString()] = externalId;
                        Session[SessionKey.SimType.ToString()] = isMISM;
                        Session["AccountType"] = "P";
                        Session[SessionKey.KenanACNumber.ToString()] = accntId.ToString();
                        using (var proxy = new UserServiceProxy())
                        {
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.DeviceCRP), "Select Account", Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                        }
                        Session[SessionKey.AccountCategory.ToString()] = category;
                        Session[SessionKey.MarketCode.ToString()] = MarketCode;
                        Session["sMsisdn"] = externalId.ToString2();

                        return RedirectToAction("ContractCheck", "CRP", new { KenanID = accntId, MSISDN = externalId, SubscriberNo = susbcrNo, SubscriberNoResets = susbcrNoResets
                            , Ismism = isMISM });
                       break;

                    case "btnAddSuppline":

                        
                        // New Changes supplementary New or Existing Account
                        Session[SessionKey.IsSuppleNewAccount.ToString()] = (collection["hdnIsSupplineNew"].ToString2() == "1" ? true : false);
                        Session["IsDeviceRequired"] = collection["hdnIsDeviceRequired"].ToString2();
                        //End of New Changes
                        Session["FxAccNo"] = accntIntId.ToString2();
                        Session["ExternalID"] = externalId.ToString2();
                        Session["AccExternalID"] = accntId.ToString2();
                        Session["KenanACNumber"] = accntId.ToString2();
                        Session[SessionKey.AccountCategory.ToString()] = category;
                        Session[SessionKey.MarketCode.ToString()] = MarketCode;

                        if (Session["IsDeviceRequired"].ToString2() == "Yes")
                        {
                            return RedirectToAction("SelectDevice", "SuppLine", new { type = (int)MobileRegType.DevicePlan });
                        }
                        else
                        {
                            return RedirectToAction("SuppLinePlan", "SuppLine", new { type = (int)MobileRegType.SuppPlan });
                        }
                       break;

                    case "btnAddMultiSim":

                        // New Changes secondary line New or Existing Account
                       Util.IsSecLineNewAccount = (collection["hdnIsSeclineNew"].ToString2() == "1" ? false : true);
                       //End of New Changes
                        Session["FxAccNo"] = accntIntId.ToString2();
                        Session["ExternalID"] = externalId.ToString2();
                        Session["AccExternalID"] = accntId.ToString2();
                        Session["KenanACNumber"] = accntId.ToString2();
                        Session[SessionKey.AccountCategory.ToString()] = category;
                        Session[SessionKey.MarketCode.ToString()] = MarketCode;
                        Session["Secondary"] = "SecondaryPlan";
                        Session["SelectedMsisdn"] = selectedExternalId;

                       //return RedirectToAction("SecondaryLinePlan", "SuppLine", new { type = (int)MobileRegType.SecPlan });
                        return RedirectToAction("SecondaryLineRegSummaryNew", "SuppLine");
                       break;

                    case "whitelist":
                        return RedirectToAction("WhiteListCustomers", "WhiteListSMECI", new { area = "Admin" });
                        break;
                    case "plan":
                        if (Session[SessionKey.PPIDCheck.ToString()] != null)
                        {
                            Session[SessionKey.PPIDCheck.ToString()] = null;
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            Session[SessionKey.MenuType.ToString()] = (int)MenuType.MobileRegistration;

                            if (Session[SessionKey.PPID.ToString()] != null)
                            {
                                string validationCheckType = (Session[SessionKey.PPID.ToString()].ToString() == "E" ? "E" : "N");
                                var context = ControllerContext.HttpContext.ApplicationInstance.Context;

                                Session[SessionKey.FailedAcctIds.ToString()] = null;
                                //Task backGroundCheck = Task.Factory.StartNew(() =>
                                //{
                                //    System.Web.HttpContext.Current = context;
                                WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), validationCheckType);
                                //}
                                //);
                                //backGroundCheck.Wait(2000);                                
                            }
                            //return RedirectToAction("MobilityServices");
                            if (Session["isOnlyPREGSMSIMReplace"] != null && Session["isOnlyPREGSMSIMReplace"].ToString2() == "Y" && !Roles.IsUserInRole("MREG_NEW"))
                            {
                                return RedirectToAction("AccountDetails", "QSIMReplacement", new { type = 1, Area = "" });
                            }
                            else if (Roles.IsUserInRole("MREG_C") || Roles.IsUserInRole("MREG_W") && (Roles.IsUserInRole("MREG_CRP") || Roles.IsUserInRole("MREG_SIMR") || Roles.IsUserInRole("MREG_ARVAS")))
                            {
                                return RedirectToAction("SelectPlan", "Registration", new { type = (int)MobileRegType.PlanOnly });
                            }
                            else if (Roles.IsUserInRole("MREG_CRP") || Roles.IsUserInRole("MREG_SIMR") || Roles.IsUserInRole("MREG_ARVAS"))
                            {
                                return RedirectToAction("AccountDetails", "QSIMReplacement", new { type = 1, Area = "" });
                            }
                            else if (Roles.IsUserInRole("MREG_C") || Roles.IsUserInRole("MREG_W"))
                            {
                                return RedirectToAction("SelectPlan", "Registration", new { type = (int)MobileRegType.PlanOnly });
                            }
                        }
                        break;
                    case "reports":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.Reports;
                        return RedirectToAction("ReportsMain", "Reports");
                        break;
                    case "storekeeper":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.StoreKeeper;
                        return RedirectToAction("StoreKeeper", "Registration");
                    case "cashier":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.Cashier;
                        return RedirectToAction("Cashier", "Registration");
                    case "teamleader":
                        if (Roles.IsUserInRole("MREG_TL"))
                        {
                            Session[SessionKey.MenuType.ToString()] = (int)MenuType.TeamLeader;
                            return RedirectToAction("StoreKeeper", "Registration");
                        }
                        break;
                    case "smart":
                        if (collection["hdnaccountSelected"].ToString().Split('|').Length > 1)
                        {
                            Session[SessionKey.CMSID.ToString()] = "Y";
                            string[] par = collection["hdnaccountSelected"].ToString().Split('|');
                            Session[SessionKey.MSISDN.ToString()] = Session[SessionKey.ExternalID.ToString()] = par[1];
                            return RedirectToAction("CmssAndContractCheck", "Retention", new { KenanID = par[0], MSISDN = par[1], SubscriberNo = par[2], SubscriberNoResets = par[3], Area = "Retention" });
                        }
                        break;
                    case "dashboard":
                        if (Roles.IsUserInRole("MREG_DAB") || Roles.IsUserInRole("MREG_HQ"))
                        {
                            Session[SessionKey.MenuType.ToString()] = (int)MenuType.DashBoard;

                            return RedirectToAction("SuperVisorForWaiveOff", "Registration", new { RefreshSession = "1", Area = "" });
                        }
                        break;
                    case "guidedsales":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.MobileRegistration;

                        #region Added by Rajeswari
                        if (Session[SessionKey.PPID.ToString()] != null)
                        {
                            if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                            {
                                WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "E");
                            }
                        }
                        #endregion
                        return RedirectToAction("Index", "guidedsales");
                        break;
                    case "home":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.HomeRegistration;

                        if (Util.SessionOrgTypeCode == Properties.Settings.Default.OrgType_HSOM)
                        {
                            return RedirectToAction("SearchRegistration", "HomeRegistration");
                        }
                        else if (Util.SessionOrgTypeCode == Properties.Settings.Default.OrgType_Dealer)
                        {
                            return RedirectToAction("BlacklistChecking", "HomeRegistration");
                        }
                        else if (Util.SessionOrgTypeCode == Properties.Settings.Default.OrgType_Center)
                        {
                            return RedirectToAction("BlacklistChecking", "HomeRegistration");
                        }
                        break;
                    case "Complaint":
                        if (SessionManager.Get("PPIDInfo") != null)
                            return RedirectToAction("Create", "Complaint");
                        break;
                    case "search":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.SearchOrder;
                        return RedirectToAction("SearchRegistration", "Registration");
                        break;
                    case "searchCust":
                        var model = new InquiryAccountSummaryModel();
                        WebHelper.Instance.PopulateAccountInfo(model, collection["txtMSISDN"].ToString2());
                        return View(model);
                        break;
                    case "searchCustIDNumber":
                        Session["WrfAlertMessage"] = null;
                        return SearchCustId(collection);
                        break;
                    case "mnp":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.MNPStatusTrack;
                        if (Session[SessionKey.PPID.ToString()] != null)
                        {
                            if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                            {
                                WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "E");
                            }
                            else
                            {
                                WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");
                            }
                        }
                        return RedirectToAction("EnterDetails", "MNP");
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
            }

            return View();
        }

		private void queryRecommendationsOfferByIC(string idCardNo)
		{
			List<PegaRecommendation> recommendationsList = new List<PegaRecommendation>();
			using (var proxy = new RegistrationServiceProxy())
			{
				recommendationsList = proxy.GetPegaRecommendationbyMSISDN(new PegaRecommendationSearchCriteria { IDCardNo = idCardNo, MSISDN = "0", Response = Constants.ACCEPT });
			}
			if (recommendationsList.Any())
			{
				PegaOfferVM pegaOfferVM = new PegaOfferVM();
				pegaOfferVM.idCardNumber = idCardNo;
				foreach (var offerDB in recommendationsList)
				{
					if (offerDB.Status.ToLower().Contains("new")) // to query only for : New / New (non-iSell)
					{
						CustomerOfferDetailsVM customerOfferDetail = new CustomerOfferDetailsVM();
						customerOfferDetail.msisdn = offerDB.MSISDN;
						customerOfferDetail.kenanCode = offerDB.KenanCode;
						customerOfferDetail.offerName = offerDB.ProductName;
						customerOfferDetail.prepositionID = offerDB.PrepositionId;
						customerOfferDetail.action = offerDB.Response;

						pegaOfferVM.customerOfferDetailsList.Add(customerOfferDetail);
					}
				}

				Session["PegaOfferList"] = pegaOfferVM;
			}
			else
			{
				Session["PegaOfferList"] = new PegaOfferVM();
			}
		}

        /// <summary>
        /// This method is used to search the customer Id by Number 
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        ActionResult SearchCustId(FormCollection collection)
        {
            #region initializing

            WebHelper.Instance.ClearSessions(); //RST - Smart Discount Issue 18Mar15
            clearUserRegistration();
            var ppidCount = 0;
            //String mktCode = String.Empty;
            //String acctCategory = String.Empty;
            bool isCorporateExist = false;
            bool isCorporateSMEExist = false;
            bool isCorpIndividualOnly = false;
            bool isSMEIndividualOnly = false;
            HttpContext.Session["BreStatusDetails"] = null;
            Session[SessionKey.FailedAcctIds.ToString()] = null;
            HttpContext.Session["isWrittenOff"] = null;
            HttpContext.Session["NoRegistrationForCRP"] = null;
            HttpContext.Session["Fraudmessage"] = string.Empty;
            HttpContext.Session["PPID"] = null;
            //ppidCount = GetPPIDCount();
            string validationCheckType2;
            Session["AllowSimReplacement"] = "False";
            CustomizedCustomer customer = null;
            CustomizedCustomer customerVip = null;
            retrieveAcctListByICResponse AcctListByICResponse = null;
            retrieveAcctListByICResponse SimReplacementAcctListByICResponse = new retrieveAcctListByICResponse();
            retrieveAcctListByICResponse allCustomerDetails = new retrieveAcctListByICResponse();
			retrieveAcctListByICResponse AllServiceDetails = new retrieveAcctListByICResponse();
            retrievegetPrinSuppResponse response = null;
            CustomizedAccount IndividualAccount = null;
            BusinessRuleResponse fxInternalCheckResponse = null;
            SubscriberRetrieveServiceInfoResponse ServiceInfoResponse = null;
            string ExceptionFromService = string.Empty;
            string ExternalId_Msisdn = string.Empty;
            string SubscriberNo = string.Empty;
            string SubscriberNoResets = string.Empty;

            string IDCardTypeID = string.Empty;
            string IDCardNo = string.Empty;

            Session["OutStandingAcctDtls"] = null;
            Session["IDCardTypeKenanCode"] = null;
            Session[SessionKey.TradeUpAmount.ToString()] = 0; //RST GST
			Session[SessionKey.DF_Eligibility.ToString()] = false;
			Session["isVIP_Masking"] = false;

			Session["frontImage"] = null;
			Session["backImage"] = null;
			Session["otherImage"] = null;
			Session["thirdImage"] = null;
			

            IDCardTypeID = collection["Customer.IDCardTypeID"] != null ? Convert.ToString(collection["Customer.IDCardTypeID"].Split(',')[1]) : Convert.ToString(collection["idCardTypeIDINT"]);
            IDCardNo = collection["Customer.IDCardNo"] != null ? Convert.ToString(collection["Customer.IDCardNo"]) : Convert.ToString(collection["idCardNo"]);

            //IDCardTypeID = Convert.ToString(collection["idCardTypeIDINT"]);
            //IDCardNo = Convert.ToString(collection["idCardNo"]);
            
            //IDCardTypeID = Convert.ToString(collection["Customer.IDCardTypeID"].Split(',')[1]);
            //IDCardNo = Convert.ToString(collection["Customer.IDCardNo"]);

            retrieveServiceInfoProxy retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
            using (var proxy = new Online.Registration.Web.Helper.RegistrationServiceProxy())
            {
                TempData["hdnWhiteList"] = proxy.CheckWhiteListCustomer(IDCardNo);
            }

            if (!string.IsNullOrEmpty(IDCardTypeID))
            {
                Session[SessionKey.IDCardTypeID.ToString()] = IDCardTypeID;
                Session[SessionKey.RegMobileReg_IDCardType.ToString()] = IDCardTypeID;
                Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()] = IDCardTypeID;
            }
            else
            {
                Session[SessionKey.IDCardTypeID.ToString()] = string.Empty;
                Session[SessionKey.RegMobileReg_IDCardType.ToString()] = string.Empty;
            }

            if (!string.IsNullOrEmpty(IDCardNo))
            {
                Session[SessionKey.IDCardNo.ToString()] = IDCardNo;
                Session[SessionKey.IDCardMSiSDNNew.ToString()] = IDCardNo;
            }
            else
            {
                Session[SessionKey.IDCardNo.ToString()] = string.Empty;
            }

            switch (IDCardTypeID)
            {
                case "1": IDCardTypeID = "NEWIC";
                    break;
                case "2": IDCardTypeID = "PASSPORT";
                    break;
                case "3": IDCardTypeID = "OLDIC";
                    break;
                case "4": IDCardTypeID = "OTHERIC";
                    break;
                case "6": IDCardTypeID = "MSISDN";
                    break;
                case "1001": IDCardTypeID = "ACCT";
                    break;
                default: IDCardTypeID = "NEWIC";
                    break;
            }
			if (IDCardTypeID == "NEWIC")
			{
				PerformBiometricCheck(IDCardNo);
			}
			else
			{
				Session["RegMobileReg_BiometricVerify"] = null;
				Session[SessionKey.BiometricDesc.ToString()] = null;
				Session[SessionKey.BiometricResponse.ToString()] = null;
			}

            Session[SessionKey.IDCardTypeName.ToString()] = IDCardTypeID;
            Session["PPIDSearchIDType"] = IDCardTypeID;
            //RST
            Session["SearchBy"] = IDCardTypeID;
            Session["cardNumber"] = IDCardNo;
            //End RST

            Session[SessionKey.CustomerContractsByType.ToString()] = null;
            #endregion

            #region PPID CHECK

            if (!ReferenceEquals(Session[SessionKey.PPIDInfo.ToString()], null))
            {
                Session.Contents.Remove("PPIDInfo");
            }

            if (!ReferenceEquals(Session[SessionKey.SR_PPID.ToString()], null))
            {
                Session.Contents.Remove("SR_PPID");
            }

            #region Added by KDPrasad to support MSISDN in PPID

            if (IDCardTypeID.ToString().ToUpper() == "MSISDN")
            {
                using (var proxy = new retrieveServiceInfoProxy())
                {

                    if (!Session[SessionKey.IDCardNo.ToString()].ToString2().StartsWith("6"))
                        Session[SessionKey.IDCardNo.ToString()] = "6" + Session[SessionKey.IDCardNo.ToString()].ToString2();

                    if (!ReferenceEquals(Session[SessionKey.isSuperAdmin.ToString()], null))
                    {
                        isSuperAdmin = (bool)Session[SessionKey.isSuperAdmin.ToString()];
                    }
                    if (isSuperAdmin)
                    {
                        customerVip = proxy.retrieveSubscriberDetls(Msisdn: Session[SessionKey.IDCardNo.ToString()].ToString2(), loggedUserName: Request.Cookies["CookieUser"].Value, isSupAdmin: false);
                        Session[SessionKey.isVIP.ToString()] = customerVip != null ? customerVip.Dob : string.Empty;
                    }
                    customer = proxy.retrieveSubscriberDetls(Msisdn: Session[SessionKey.IDCardNo.ToString()].ToString2(), loggedUserName: Request.Cookies["CookieUser"].Value, isSupAdmin: true);
                }

                if (customer != null)
                {
					// Lus-bugzilla #679 
					if (customer.MsgCode != "0")
					{
						Session[SessionKey.PPID.ToString()] = "TO";
						Session[SessionKey.IDCardTypeName.ToString()] = IDCardTypeID;
						return View();
					}
                    isCorporateExist = String.Equals(ConfigurationManager.AppSettings["CorporateParentMarketCode"].ToString2(), customer.MarketCode.ToString2());
                    isCorporateSMEExist = String.Equals(ConfigurationManager.AppSettings["SMEParentMarketCode"].ToString2(), customer.MarketCode.ToString2());
                    isCorpIndividualOnly = String.Equals(ConfigurationManager.AppSettings["CIParentMarketCode"].ToString2(), customer.MarketCode.ToString2());
                    isSMEIndividualOnly = String.Equals(ConfigurationManager.AppSettings["SMEIParentMarketCode"].ToString2(), customer.MarketCode.ToString2());

                    Session[SessionKey.CustomerInformation.ToString()] = customer;
                    if (!string.IsNullOrEmpty(customer.NewIC))
                    {
                        IDCardNo = customer.NewIC;
                        IDCardTypeID = "NEWIC";
                        Session[SessionKey.IDCardTypeID.ToString()] = 1;
                    }
                    else if (!string.IsNullOrEmpty(customer.PassportNo))
                    {
                        IDCardNo = customer.PassportNo;
                        IDCardTypeID = "PASSPORT";
                        Session[SessionKey.IDCardTypeID.ToString()] = 2;
                    }
                    else if (!string.IsNullOrEmpty(customer.OldIC))
                    {
                        IDCardNo = customer.OldIC;
                        IDCardTypeID = "OLDIC";
                        Session[SessionKey.IDCardTypeID.ToString()] = 3;
                    }
                    else if (!string.IsNullOrEmpty(customer.OtherIC))
                    {
                        IDCardNo = customer.OtherIC;
                        IDCardTypeID = "OTHERIC";
                        Session[SessionKey.IDCardTypeID.ToString()] = 4;
                    }
                    Session[SessionKey.IDCardNo.ToString()] = IDCardNo;
                    Session[SessionKey.IDCardTypeName.ToString()] = IDCardTypeID;
                    if (IDCardTypeID == "NEWIC")
                    {
                        PerformBiometricCheck(IDCardNo);
                    }

                    //2015.07.23 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - Start
                    //Applicable for searching by MSISDN & ACCT only
                    if (Util.SessionAccess.User.isDealer && (isCorpIndividualOnly || isSMEIndividualOnly))
                    {
                        Session[SessionKey.IsCorpIndividualOnly.ToString()] = isCorpIndividualOnly;
                        Session[SessionKey.IsSMEIndividualOnly.ToString()] = isSMEIndividualOnly;
                        return View();
                    }
                    //2015.07.23 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - End
                }
                else
                {
                    Session[SessionKey.PPID.ToString()] = "IU";
                    Session[SessionKey.IDCardTypeName.ToString()] = IDCardTypeID;
                    return View();
                }
            }
            #endregion


            #region Added by Sindhu to support Kenan Search in PPID

            if (IDCardTypeID.ToString().ToUpper() == "ACCT")
            {
                using (var proxy = new retrieveServiceInfoProxy())
                {
                    if (!ReferenceEquals(Session[SessionKey.isSuperAdmin.ToString()], null))
                    {
                        isSuperAdmin = (bool)Session[SessionKey.isSuperAdmin.ToString()];
                    }
                    customer = proxy.retrieveSubscriberDetlsbyKenan(IDCardTypeID.ToString().ToUpper(), Session[SessionKey.IDCardNo.ToString()].ToString(), Request.Cookies["CookieUser"].Value.ToString(), true);
                }

                if (customer != null)
                {
					// Lus-bugzilla #679 
					if (customer.MsgCode != "0")
                    {
                        Session[SessionKey.PPID.ToString()] = "TO";
                        Session[SessionKey.IDCardTypeName.ToString()] = IDCardTypeID;
                        return View();
                    }

                    isCorporateExist = String.Equals(ConfigurationManager.AppSettings["CorporateParentMarketCode"].ToString2(), customer.MarketCode.ToString2());
                    isCorporateSMEExist = String.Equals(ConfigurationManager.AppSettings["SMEParentMarketCode"].ToString2(), customer.MarketCode.ToString2());
                    isCorpIndividualOnly = String.Equals(ConfigurationManager.AppSettings["CIParentMarketCode"].ToString2(), customer.MarketCode.ToString2());
                    isSMEIndividualOnly = String.Equals(ConfigurationManager.AppSettings["SMEIParentMarketCode"].ToString2(), customer.MarketCode.ToString2());

                    Session[SessionKey.CustomerInformation.ToString()] = customer;
                    if (!string.IsNullOrEmpty(customer.NewIC))
                    {
                        IDCardNo = customer.NewIC;
                        IDCardTypeID = "NEWIC";
                        Session[SessionKey.IDCardTypeID.ToString()] = 1;
                    }
                    else if (!string.IsNullOrEmpty(customer.PassportNo))
                    {
                        IDCardNo = customer.PassportNo;
                        IDCardTypeID = "PASSPORT";
                        Session[SessionKey.IDCardTypeID.ToString()] = 2;
                    }
                    else if (!string.IsNullOrEmpty(customer.OldIC))
                    {
                        IDCardNo = customer.OldIC;
                        IDCardTypeID = "OLDIC";
                        Session[SessionKey.IDCardTypeID.ToString()] = 3;
                    }
                    else if (!string.IsNullOrEmpty(customer.OtherIC))
                    {
                        IDCardNo = customer.OtherIC;
                        IDCardTypeID = "OTHERIC";
                        Session[SessionKey.IDCardTypeID.ToString()] = 4;
                    }
                    Session[SessionKey.IDCardNo.ToString()] = IDCardNo;
                    Session[SessionKey.IDCardTypeName.ToString()] = IDCardTypeID;
                    if (IDCardTypeID == "NEWIC")
                    {
                        PerformBiometricCheck(IDCardNo);
                    }

                    //2015.07.23 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - Start
                    //Applicable for searching by MSISDN & ACCT only
                    if (Util.SessionAccess.User.isDealer && (isCorpIndividualOnly || isSMEIndividualOnly))
                    {
                        Session[SessionKey.IsCorpIndividualOnly.ToString()] = isCorpIndividualOnly;
                        Session[SessionKey.IsSMEIndividualOnly.ToString()] = isSMEIndividualOnly;
                        return View();
                    }
                    //2015.07.23 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - End
                }
                else
                {
                    Session[SessionKey.PPID.ToString()] = "IU";
                    Session[SessionKey.IDCardTypeName.ToString()] = IDCardTypeID;
                    return View();
                }
            }
            #endregion
			
			// to refresh pega offer from DB.
			// no longer required since FD v1.0,
			//queryRecommendationsOfferByIC(IDCardNo); 
			PegaOfferVM pegaOfferVM = new PegaOfferVM();
			pegaOfferVM.idCardNumber = IDCardNo;
			Session["PegaOfferList"] = pegaOfferVM;

            List<string> msisdnForQuotaShare = new List<string>();//27042015 - Anthony - To display harcoded pop-up message for MaxisONE Share offer
            List<ViewPopUpMessage> ViewMessage = new List<ViewPopUpMessage>();
            using (var proxy = new retrieveServiceInfoProxy())
            {
                if (!ReferenceEquals(Session[SessionKey.isSuperAdmin.ToString()], null))
                {
					// MEPS will always be masked, in backend, superAdmin means no need to masked. (Drop4 PN-2455)
                    isSuperAdmin = Util.SessionAccess.User.isDealer ? false : (bool)Session[SessionKey.isSuperAdmin.ToString()];
                }
				
				AcctListByICResponse = proxy.retrieveAllDetails(ExceptionFromService, Request.Cookies["CookieUser"].Value, IDCardTypeID, IDCardNo, System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"], Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName, Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword, "", isSuperAdmin);

				// Lus-bugzilla #679 
                if ((customer != null && customer.MsgCode != "0") || (AcctListByICResponse != null && AcctListByICResponse.MsgCode.ToInt() != 0))
				{
					Session[SessionKey.PPID.ToString()] = "TO";
					Session[SessionKey.IDCardTypeName.ToString()] = IDCardTypeID;
					return View();
				}
				
				Session["AcctAllDetails"] = AcctListByICResponse;

				// in any case customer doing search twice
				Session["AllCustomerDetails"] = null;
				Session["AllServiceDetails"] = null;
				Session["CustomerLiberalisation"] = null;
				Session["CustomerLiberalisationRank"] = null;

                if (AcctListByICResponse != null && AcctListByICResponse.itemList.Any())
                {
                    string accountNo = "";
                    string isBillable = "";
                    //retrieveBillngInfoResponse billInfo = new retrieveBillngInfoResponse();

                    foreach (var itemAccount in AcctListByICResponse.itemList)
                    {
                        if (itemAccount.IsActive.ToString2() == "True")
                        {
                            accountNo = itemAccount.AcctExtId;
                            //billInfo = proxy.retrieveBillngInfo(accountNo);
                            //itemAccount.IsBillable = !ReferenceEquals(billInfo,null) ? billInfo.payResp : "N";
                            var accountType = itemAccount.AccountDetails.AcctType.ToString2();// AcctType = 1 means billable
                            itemAccount.IsBillable = !string.IsNullOrEmpty(accountType) && accountType == "1" ? "Y" : "N";
                        }
                        else
                        {
                            itemAccount.IsBillable = "";
                        }
                    }

                    #region "AllCustomerDetails" and "AllServiceDetails"
                    allCustomerDetails.itemList = new List<Items>();
                    AllServiceDetails.itemList = new List<Items>();
                    allCustomerDetails.itemList.AddRange(AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList());
                    AllServiceDetails.itemList.AddRange(AcctListByICResponse.itemList.Where(s => s.ServiceInfoResponse != null && s.IsActive).ToList());

                    Session[SessionKey.AllCustomerDetails.ToString()] = allCustomerDetails;
                    Session[SessionKey.AllServiceDetails.ToString()] = AllServiceDetails;
                    Logger.Info("allCustomerDetails = " + XMLHelper.ConvertObjectToXml(allCustomerDetails));
                    Logger.Info("AllServiceDetails = " + XMLHelper.ConvertObjectToXml(AllServiceDetails));
                    #endregion

					if (isSuperAdmin)
					{
						string accountExtID = "";
						if (AllServiceDetails.itemList != null && AllServiceDetails.itemList.Count() > 0)
							accountExtID = AllServiceDetails.itemList.FirstOrDefault().AcctExtId;
						else if (allCustomerDetails.itemList != null && allCustomerDetails.itemList.Count() > 0)
							accountExtID = allCustomerDetails.itemList.FirstOrDefault().AcctExtId;
						
						if (accountExtID != "")
						{
							customerVip = proxy.retrieveSubscriberDetlsbyKenan("ACCT", accountExtID, Request.Cookies["CookieUser"].Value.ToString(), isSupAdmin: false);
							Session["isVIP_Masking"] = customerVip.Dob == "*****" ? true : false;
						}
						
					}

                    #region To get Customer Liberalization value globally for waving functionalities
                    string customerLiberalisation = string.Empty;
                    string liberalisationRank = string.Empty;
                    if (!ReferenceEquals(AllServiceDetails, null) && AllServiceDetails.itemList.Any(c => c.Customer != null))
                    {
                        customerLiberalisation = Util.getMarketCodeLiberalisationMOC(AllServiceDetails, "LIBERALISATION");

                        if (customerLiberalisation.Contains("_"))
                        {
                            string[] strArray = customerLiberalisation.Split('_');
                            liberalisationRank = strArray[1].ToString2();
                        }
                    }
                    Session["CustomerLiberalisation"] = customerLiberalisation;
                    Session["CustomerLiberalisationRank"] = liberalisationRank;
                    #endregion

                    var writeOffAccounts = AcctListByICResponse.itemList.Where(e => e.IsActive == false && e.AccountDetails != null && e.AccountDetails.TotalWriteOff.ToDecimal() > 0);

                    if (writeOffAccounts != null && writeOffAccounts.Any())
                    {
                        allCustomerDetails = proxy.retrieveUCCSInfo(IDCardTypeID, IDCardNo, allCustomerDetails);
                        Session["AllCustomerDetails"] = allCustomerDetails;
                        Session[SessionKey.WriteOffAccts.ToString()] = writeOffAccounts.ToList();
                    }

                    #region To display MaxisONE Share offer Pop-Up Message
                    //27042015 - Anthony - To display harcoded pop-up message for MaxisONE Share offer - Start
                    if (ConfigurationManager.AppSettings["showPopUpQuotaShare"].ToBool())
                    {
                        checkExistingMaxisONEPlan(AllServiceDetails, out msisdnForQuotaShare);
                        msisdnForQuotaShare = checkMaxisONEShareEligibility(AllServiceDetails, msisdnForQuotaShare);
                        ViewBag.MSISDNForQuotaShare = msisdnForQuotaShare;
                        generatePopUpMessage(AllServiceDetails, IDCardNo, out ViewMessage);
                        ViewBag.MSISDNMessage = ViewMessage;
                    }
                    //27042015 - Anthony - To display harcoded pop-up message for MaxisONE Share offer - End
                    #endregion

                    #region To block SMEI/CI for MEPS
                    //2015.07.23 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - Start
                    var ActiveAcctList = new List<Items>();
                    ActiveAcctList.AddRange(AcctListByICResponse.itemList.Where(c => c.IsActive && c.ServiceInfoResponse != null && c.ServiceInfoResponse.serviceStatus.ToUpper() != "D"));
                    Session[SessionKey.IsCorpIndividualOnly.ToString()] = ActiveAcctList.Where(c => c.AccountDetails != null && c.AccountDetails.MktCode == ConfigurationManager.AppSettings["CIParentMarketCode"].ToString2()).ToList().Count() == ActiveAcctList.Count();
                    Session[SessionKey.IsSMEIndividualOnly.ToString()] = ActiveAcctList.Where(c => c.AccountDetails != null && c.AccountDetails.MktCode == ConfigurationManager.AppSettings["SMEIParentMarketCode"].ToString2()).ToList().Count() == ActiveAcctList.Count();
                    //2015.07.23 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - End
                    #endregion
                }
                else
                {
                    #region Pop-Up Message
                    if (ConfigurationManager.AppSettings["showPopUpQuotaShare"].ToBool())
                    {
                        generatePopUpMessage(AllServiceDetails, IDCardNo, out ViewMessage);
                        ViewBag.MSISDNMessage = ViewMessage;
                    }
                    #endregion
                }

                #region Added by VLT on 17-05-2013 if ItemList = null make user as new

                if (AcctListByICResponse != null)
                {
                    if (AcctListByICResponse.itemList == null)
                    {
                        Session[SessionKey.PPID.ToString()] = "N";

                        LogUserInfo(IDCardNo, IDCardTypeID);

                        if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null) && !string.IsNullOrWhiteSpace(Convert.ToString(Session[SessionKey.RedirectURL.ToString()])))
                        {
                            string redirectUrl, controllerName, actionName = string.Empty;
                            try
                            {
                                redirectUrl = Convert.ToString(Session[SessionKey.RedirectURL.ToString()]);
                                string[] urlParts = redirectUrl.Split('/');

                                controllerName = urlParts[0];
                                actionName = urlParts[1];

                                Session[SessionKey.MenuType.ToString()] = (int)MenuType.MobileRegistration;

                                WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");

                                return RedirectToAction(actionName, controllerName);
                            }
                            catch
                            {

                            }
                        }
                        return View();
                    }
                }
                #endregion

                if (!string.IsNullOrWhiteSpace(ExceptionFromService))
                {
                    ViewBag.ServerConnStatus = ExceptionFromService;
                }

                #region storing "AcctExtId", "AccountsExt", "AcctExtId" sessions
                if (!ReferenceEquals(AcctListByICResponse, null))
                {
                    if (!ReferenceEquals(AcctListByICResponse.itemList, null) && (AcctListByICResponse.itemList.Count > 0) && (!ReferenceEquals(AcctListByICResponse.itemList[0].AcctExtId, null)))
                    {
                        Session[SessionKey.AcctExtId.ToString()] = AcctListByICResponse.itemList[0].AcctExtId.ToString();
                        Session[SessionKey.AccountsExt.ToString()] = AcctListByICResponse;
                    }
                    else
                    {
                        Session[SessionKey.AcctExtId.ToString()] = string.Empty;
                    }
                }
                #endregion

            }

            #region Save to Pop-Up Message Audit Log
            //20160129 - 1620 Customer tagging for product pop up - Samuel - Start
            using (var proxy = new RegistrationServiceProxy())
            {
                if (msisdnForQuotaShare.Count() > 0 || ViewMessage.Count() > 0)
                {
                    List<PopUpMessageAuditLog> responseList = new List<PopUpMessageAuditLog>();
                    PopUpMessageAuditLog objPopUpMessageAuditLogList = new PopUpMessageAuditLog();
                    objPopUpMessageAuditLogList.LoginID = Util.SessionAccess.UserName;
                    objPopUpMessageAuditLogList.OrgID = Util.SessionAccess.User.Org.ID;
                    objPopUpMessageAuditLogList.CreateDT = DateTime.Now;
                    objPopUpMessageAuditLogList.SearchType = Session["SearchBy"].ToString2();
                    objPopUpMessageAuditLogList.SearchValue = Session["cardNumber"].ToString2();
                    objPopUpMessageAuditLogList.IDCardTypeID = IDCardTypeID;
                    objPopUpMessageAuditLogList.IDCardNo = IDCardNo;
                    objPopUpMessageAuditLogList.WhitelistMsgDisplayed = ViewMessage.Count() > 0 ? true : false;
                    objPopUpMessageAuditLogList.BusinessRuleMsgDisplayed = msisdnForQuotaShare.Count() > 0 ? true : false;
                    responseList.Add(objPopUpMessageAuditLogList);
                    var popupMessageAuditLog = proxy.SavePopUpMessageAuditLog(responseList);

                }
            }
            //20160129 - 1620 Customer tagging for product pop up - Samuel - End
            #endregion
            
            bool isWrittenOff = false;
            ViewBag.isCorporateExist = isCorporateExist;
            ViewBag.isCorporateSMEExist = isCorporateSMEExist;
            if (isCorporateExist || isCorporateSMEExist)
            {
                return View();
            }

            if (ReferenceEquals(AcctListByICResponse, null))
            {
                #region Age Check
                var SR_PPIDInfo = (retrieveAcctListByICResponse)Session[SessionKey.SR_PPID.ToString()];
                var idCardNo = Session[SessionKey.IDCardNo.ToString()].ToString2();
                if (SR_PPIDInfo == null && idCardNo != null && (Session[SessionKey.IDCardTypeID.ToString()].ToString() == "1"))
                {
                    if (Util.isUnderAgeCustomer(IDCardNo))
                    {
                        ViewBag.ageCheckFailed = true;
                        Session["PPID"] = string.Empty;
                        return View();
                    }
                }
                #endregion

                Session[SessionKey.SR_PPIDCheck.ToString()] = "N";//If there is no principle account . used for sim replacement                   
                fxInternalCheckResponse = BusinessFxInternalCheck();
                Logger.Info("fxInternalCheckResponse:--" + fxInternalCheckResponse.ResultMessage);
                if (!ReferenceEquals(fxInternalCheckResponse.Message, null) && fxInternalCheckResponse.Message.Equals("1"))
                {
                    string[] fxInternalCheckReply = fxInternalCheckResponse.ResultMessage.Split(',');
                    ViewBag.FxInternalCheckMessage = string.Join(",", fxInternalCheckReply, 1, fxInternalCheckReply.Count() - 1);
                    Session["Fraudmessage"] = ViewBag.FxInternalCheckMessage;
                    Session[SessionKey.VIP.ToString()] = "*****";
                    Session[SessionKey.WO.ToString()] = "WO";
                    //02072015 - Anthony - Enhance Customer Search: remove duplicate BusinessValidationChecks in BusinessFxInternalCheck - Start
                    var customerType = Session[SessionKey.PPID.ToString2()].ToString2() == "E" ? "E" : "N";
                    WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), customerType);
                    //02072015 - Anthony - Enhance Customer Search: remove duplicate BusinessValidationChecks in BusinessFxInternalCheck - End
                    return View();
                }

                ///New user
                if (IDCardNo == "*****")
                {

                    Session[SessionKey.PPID.ToString()] = "N";
                    ViewBag.FxInternalCheckMessage = ConfigurationManager.AppSettings["VIPMessage"].ToString();
                    Session["Fraudmessage"] = ViewBag.FxInternalCheckMessage;
                    Session[SessionKey.VIP.ToString()] = IDCardNo.ToString();
                }
                else
                {
                    Session[SessionKey.PPID.ToString()] = "N";
                }
                LogUserInfo(IDCardNo, IDCardTypeID);

                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null) && !string.IsNullOrWhiteSpace(Convert.ToString(Session[SessionKey.RedirectURL.ToString()])))
                {
                    string redirectUrl, controllerName, actionName = string.Empty;
                    try
                    {
                        redirectUrl = Convert.ToString(Session[SessionKey.RedirectURL.ToString()]);
                        string[] urlParts = redirectUrl.Split('/');

                        controllerName = urlParts[0];
                        actionName = urlParts[1];

                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.MobileRegistration;

                        WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");

                        if (Session[SessionKey.VIP.ToString()] != null && Session[SessionKey.VIP.ToString()].ToString() != "")
                        {
                            return RedirectToAction("Index", new { UserStop = "VIP" });
                        }
                        else
                        {
                            return RedirectToAction(actionName, controllerName);
                        }
                    }
                    catch
                    {

                    }
                }

                validationCheckType2 = (Session[SessionKey.PPID.ToString()].ToString() == "E" ? "E" : "N");
                WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), validationCheckType2);
                return View();
            }
            else
            {
                #region Call method BusinessFxInternalCheck() - Added by VLT on 17-05-2013
                Logger.Info("FxInternalCheckMessage entering start");
                if (AcctListByICResponse.MsgCode == "0")
                {
                    fxInternalCheckResponse = BusinessFxInternalCheck();

                    if (!fxInternalCheckResponse.IsFxInternalCheckFailed)
                    {
                        Logger.Info("FxInternalCheckMessage entering if" + ViewBag.FxInternalCheckMessage);
                        if (fxInternalCheckResponse.Message == "0")
                        {
                            ViewBag.FxInternalCheckMessage = "Active";
                            Session["Fraudmessage"] = ViewBag.FxInternalCheckMessage;
                            Logger.Info("FxInternalCheckMessage" + ViewBag.FxInternalCheckMessage);
                        }
                    }
                    else if (!ReferenceEquals(fxInternalCheckResponse.Message, null) && fxInternalCheckResponse.Message.Equals("1"))
                    {
                        Session[SessionKey.WO.ToString()] = "WO";
                        Logger.Info("FxInternalCheckMessage end" + fxInternalCheckResponse.ResultMessage);
                        ViewBag.FxInternalCheckMessage = fxInternalCheckResponse.ResultMessage.Substring(fxInternalCheckResponse.ResultMessage.IndexOf(',') + 1, fxInternalCheckResponse.ResultMessage.Length - (fxInternalCheckResponse.ResultMessage.IndexOf(',') + 1));
                        Session["Fraudmessage"] = ViewBag.FxInternalCheckMessage;
                        Session["AllowSimReplacement"] = "False";

                        if (Session["Fraudmessage"].ToString2() == "One or more acct have been written off. Please collect outstanding amount.")
                        {
                            ValidateWriteOffLayout(AcctListByICResponse);
                        }
                    }
                }
                #endregion

                #region If No Active Account
                if (AcctListByICResponse.itemList.Where(a => a.IsActive == true).ToList().Count == 0)
                {
                    //02072015 - Anthony - Enhance Customer Search: remove duplicate BusinessValidationChecks in BusinessFxInternalCheck - Start
                    var customerType = Session[SessionKey.PPID.ToString2()].ToString2() == "E" ? "E" : "N";
                    WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), customerType);
                    //02072015 - Anthony - Enhance Customer Search: remove duplicate BusinessValidationChecks in BusinessFxInternalCheck - End
                    Session[SessionKey.PPID.ToString()] = "N";  
                    Session[SessionKey.SR_PPIDCheck.ToString()] = "N";
                    return View();
                }
                #endregion

                #region MISM Check
                int index = 0;
                foreach (var v in AcctListByICResponse.itemList)
                {
                    ServiceInfoResponse = AcctListByICResponse.itemList[index].ServiceInfoResponse;

                    if (!ReferenceEquals(ServiceInfoResponse, null))
                    {
                        if (ServiceInfoResponse.msgCode == "0")
                        {
                            v.ServiceInfoResponse = ServiceInfoResponse;
                            v.EmfConfigId = ServiceInfoResponse.emf_config_id;
                            v.IsMISM = (v.EmfConfigId == ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2() ? true : false);
                        }
                        else
                        { 
                            v.ServiceInfoResponse = null;
                            v.EmfConfigId = "";
                            v.IsMISM = false;
                        }
                    }
                    else
                    {
                        v.ServiceInfoResponse = null;
                        v.EmfConfigId = "";
                        v.IsMISM = false;
                    }
                    index++;
                }
                #endregion

                #region If Write-off Check Failed
                if (ViewBag.FxInternalCheckMessage == "One or more acct have been written off. Please collect outstanding amount.")
				{ 
					isWrittenOff = true;
                    ValidateWriteOffLayout(AcctListByICResponse);
					Session["isWrittenOff"] = true;
                }
                #endregion

                #region VIP Check
                bool _isCustVIP = false;
				if (!ReferenceEquals(AcctListByICResponse, null))
				{
					foreach (Online.Registration.Web.SubscriberICService.Items i in AcctListByICResponse.itemList)
					{
						if (i.Customer != null && i.Customer.Dob == "*****")
						{
							_isCustVIP = true;
							Session[SessionKey.WO.ToString()] = string.Empty;
							Session[SessionKey.VIP.ToString()] = i.Customer.Dob;
						}
					}
                }
                #endregion

                var PrinSupproxy = new PrinSupServiceProxy();
                var isUserCCCAbove = WebHelper.Instance.IsUserCCCAbove();

                if (AcctListByICResponse.itemList.Exists(c => c.ServiceInfoResponse != null) || (AcctListByICResponse.itemList.Any() && AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse == null).ToList().Count() == AcctListByICResponse.itemList.Count()))
                {
                    ExternalId_Msisdn = string.Empty;
                    string msisdn = string.Empty;

                    #region Himansu SimReplacement

                    SimReplacementAcctListByICResponse.ExtensionData = AcctListByICResponse.ExtensionData;
                    SimReplacementAcctListByICResponse.MsgCode = AcctListByICResponse.MsgCode;
                    SimReplacementAcctListByICResponse.MsgDesc = AcctListByICResponse.MsgDesc;
                    SimReplacementAcctListByICResponse.itemList = new List<Items>();
                    for (int i = 0; i < AcctListByICResponse.itemList.Count; i++)
                    {
                        SimReplacementAcctListByICResponse.itemList.Add(AcctListByICResponse.itemList[i]);
                    }

                    if (SimReplacementAcctListByICResponse.itemList != null)
                    {
                        for (int i = 0; i < SimReplacementAcctListByICResponse.itemList.Count; i++)
                        {
                            if (SimReplacementAcctListByICResponse.itemList[i].ServiceInfoResponse != null && SimReplacementAcctListByICResponse.itemList[i].IsActive)
                            {
                                ExternalId_Msisdn = SimReplacementAcctListByICResponse.itemList[i].ExternalId;
                                break;
                            }
                        }

                        if (Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()] != null && Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString() == "6")
                        {
                            if (SimReplacementAcctListByICResponse != null)
                            {

                                ExternalId_Msisdn = SimReplacementAcctListByICResponse.itemList.Where(a => a.ExternalId == Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString()).FirstOrDefault().ExternalId;

                            }
                        }

                        if (ExternalId_Msisdn == string.Empty)
                        {
                            if (SimReplacementAcctListByICResponse.itemList.Any() && SimReplacementAcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse == null).ToList().Count() == SimReplacementAcctListByICResponse.itemList.Count())
                            {
                                ExternalId_Msisdn = SimReplacementAcctListByICResponse.itemList.Where(s => s.Customer != null).FirstOrDefault().ExternalId;
                            }
                        }


                        #region Added by Sindhu on 25th Jul 2013 to get A/c list based on the selected Accoutn Number in Search PAge
                        if (Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()] != null && Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString() == "1001")
                        {
                            if (SimReplacementAcctListByICResponse != null)
                                ExternalId_Msisdn = SimReplacementAcctListByICResponse.itemList.Where(a => a.AcctExtId == Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString() || a.EmfConfigId == ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2()).FirstOrDefault().ExternalId;

                        }
                        #endregion

                        ///CALL SUPPLINE AND ADD TO COLLECTION BY MSISDN FOR GSM A/C'S                                
                        PrinSupServiceProxy.counter = 0;//Applicable to Properties.Settings.Default.DevelopmentModeOnForPpidChecking=true mode only. Will not affect the live mode.
                        var req1 = new SubscribergetPrinSuppRequest();
                        if (ExternalId_Msisdn.Length > 0)
                        {
                            foreach (var v in SimReplacementAcctListByICResponse.itemList.Where(c => (c.ServiceInfoResponse != null && (c.ServiceInfoResponse.lob == "POSTGSM" || c.ServiceInfoResponse.lob == "PREGSM"))).ToList())
                            {
                                response = null;
                                if (v.EmfConfigId == ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2())
                                {
                                    retrieveMismDetlsResponse primSecResponse = PrinSupproxy.getPrimSecondaryines(v.ExternalId);
                                    //Code added by ravi as it was throwing exception since we are getting PrimSecInd as 's' on Feb 13 2014 
                                    var curPrimSecResponse = primSecResponse.itemList.Where(a => a.PrimSecInd == "P");
                                    //Code added by ravi as it was throwing exception since we are getting PrimSecInd as 's' on Feb 13 2014  ends here

                                    //if condition also changed by ravi on feb 13 2014
                                    if (primSecResponse != null && primSecResponse.itemList.ToList().Count > 0 && curPrimSecResponse.Count() > 0)
                                    {
                                        var prinExternalID = curPrimSecResponse.FirstOrDefault().Msisdn;
                                        var prinIsMISM = SimReplacementAcctListByICResponse.itemList.Where(i => i.ExternalId == prinExternalID).FirstOrDefault();
                                        if (prinIsMISM != null && !prinIsMISM.IsMISM)
                                        {
                                            SimReplacementAcctListByICResponse.itemList.Where(i => i.ExternalId == prinExternalID).ToList().ForEach(i => i.SecondarySimList = primSecResponse.itemList.Where(a => a.PrimSecInd != "P").ToList());
                                        }
                                        SimReplacementAcctListByICResponse.itemList.Where(i => i.ExternalId == prinExternalID).ToList().ForEach(i => i.IsMISM = true);
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < AcctListByICResponse.itemList.Count; i++)
                                    {
                                        if (AcctListByICResponse.itemList[i].ExternalId == v.ExternalId && AcctListByICResponse.itemList[i].IsActive)
                                        {
                                            response = AcctListByICResponse.itemList[i].PrinSupplimentaryResponse;
                                        }
                                    }

                                    ///SET THE CORRESPONDING PrinSuppResponse OBJECT WITH RETURNED response
                                    if (response != null && response.itemList != null && response.itemList.ToList().Count > 0)
                                    {
                                        SimReplacementAcctListByICResponse.itemList.Where(i => i.ExternalId == v.ExternalId && i.IsActive).ToList().ForEach(i => i.PrinSuppResponse = response);
                                        SimReplacementAcctListByICResponse.itemList.Where(i => i.ExternalId == v.ExternalId && i.IsActive).ToList().ForEach(i => i.IsMISM = false);
                                    }
                                }
                            }
                        }
                        req1 = null;

                        ///EXISTING USER
                        Session[SessionKey.SR_PPIDCheck.ToString()] = "E";
                        Session[SessionKey.PPID.ToString()] = "E";
                    }

                    else
                    {
                        ///EVEN IF THE USER DOES NOT HAVE ANY GSM A/C AS PER REQUIREMENT ABOVE GET THE CUSTOMER INFORMATION BASED ON NON GSM A/C
                        ExternalId_Msisdn = SimReplacementAcctListByICResponse.itemList.FirstOrDefault(c => (c.ServiceInfoResponse != null && (c.ServiceInfoResponse.lob != "POSTGSM" || c.ServiceInfoResponse.lob != "PREGSM"))).ExternalId;
                        ///New user
                        Session[SessionKey.SR_PPIDCheck.ToString()] = "N";
                        Session[SessionKey.PPID.ToString()] = "N";
                    }

                    //Get customer race
                    try
                    {
                        Session["RaceKenanCodeId"] = "";
                        foreach (var il in AcctListByICResponse.itemList)
                        {
                            if (il.Customer != null)
                            {
                                var custInfo = il.Customer;
                                Session["RaceKenanCodeId"] = custInfo.Races;
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Session["RaceKenanCodeId"] = "";
                    }

                    if (!string.IsNullOrWhiteSpace(ExternalId_Msisdn))
                    {
                        #region CUSTOMER INFORMATION

                        customer = null;
                        ///GET THE CUSTOMER

                        if (!ReferenceEquals(Session[SessionKey.isSuperAdmin.ToString()], null))
                        {
                            isSuperAdmin = (bool)Session[SessionKey.isSuperAdmin.ToString()];
                        }
                        //checking VIP user or not
                        if (isSuperAdmin)
                        {
                            try
                            {
                                customerVip = AcctListByICResponse.itemList.FirstOrDefault(i => i.Customer != null).Customer;
                                Session[SessionKey.isVIP.ToString()] = customerVip.Dob;
                            }
                            catch (Exception ex)
                            {
                                Logger.InfoFormat("Exception for VIP customer : Message -" + ex.Message + ", Inner Exception : " + ex.InnerException);
                                Session[SessionKey.PPID.ToString()] = "IU";
                                Session[SessionKey.SR_PPIDCheck.ToString()] = string.Empty;
                                Session[SessionKey.IDCardTypeName.ToString()] = IDCardTypeID;
                                return View();
                            }
                        }
                        if (Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()] != null && Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString() == "6")
                        {
                            if (SimReplacementAcctListByICResponse != null && (Session["IsDealer"].ToBool() || isUserCCCAbove == false))
                            {
                                SimReplacementAcctListByICResponse.itemList = SimReplacementAcctListByICResponse.itemList.Where(a => a.ExternalId == Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString()).ToList();
                            }
                        }

                        #region Added by Sindhu on 25th Jul 2013 to get A/c list based on the selected Accoutn Number in Search PAge
                        if (Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()] != null && Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString() == "1001")
                        {
                            if (SimReplacementAcctListByICResponse != null && (Session["IsDealer"].ToBool() || isUserCCCAbove == false))
                                SimReplacementAcctListByICResponse.itemList = SimReplacementAcctListByICResponse.itemList.Where(a => a.AcctExtId == Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString() || a.EmfConfigId == ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2()).ToList();
                        }
                        #endregion


                        if (!ReferenceEquals(Session["AcctAllDetails"], null))
                        {
                            AcctListByICResponse = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                        }

                        #endregion CUSTOMER INFORMATION
                        #region ACCOUNT INFORMATION

                        ///GET THE ACCOUNT DETAILS WITH PAYMENT DETAILS SAME RULE LIKE CUSTOMER AS EXPLAINED BY PATANJALI OVER PHONE

                        if (!ReferenceEquals(SimReplacementAcctListByICResponse, null))
                        {
                            Session[SessionKey.SR_PPID.ToString()] = SimReplacementAcctListByICResponse;
                        }
                        IndividualAccount = PopulateSimReplacementAccountInfo(msisdn: ExternalId_Msisdn);

                        SimReplacementAcctListByICResponse.itemList.ToList().ForEach(i => i.Account = IndividualAccount);

                        #endregion ACCOUNT INFORMATION

                    }
                    #endregion

                    #region fetching user info from accounts
                    ///if the user is having any accounts, then we will fetch the user information from the accounts. 
                    ///For this current phase, if any GSM account is available then we will use the first GSM account to fetch user information. 
                    ///Else we have to use first account to fetch the user information.

                    if ((AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse != null).ToList().Exists(c => c.ServiceInfoResponse.lob == "POSTGSM"))
                        || (AcctListByICResponse.itemList.Any()
                            && AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse == null).ToList().Count() == AcctListByICResponse.itemList.Count()
                            )
                        )
                    {
                        #region GET THE EXTERNAL ID OR MSISDN FOR THE FIRST GSM A/C IF AVAILABLE
                        if (AcctListByICResponse.itemList != null)
                        {

                            for (int i = 0; i < AcctListByICResponse.itemList.Count; i++)
                            {
                                if (AcctListByICResponse.itemList[i].ServiceInfoResponse != null)
                                {
                                    if (AcctListByICResponse.itemList[i].ServiceInfoResponse.lob == "POSTGSM" && AcctListByICResponse.itemList[i].ServiceInfoResponse.serviceStatus != "D" && AcctListByICResponse.itemList[i].IsActive && (AcctListByICResponse.itemList[i].ServiceInfoResponse.prinSuppInd == "P" || AcctListByICResponse.itemList[i].ServiceInfoResponse.prinSuppInd == ""))
                                    {
                                        ExternalId_Msisdn = AcctListByICResponse.itemList[i].ExternalId;
                                        break;
                                    }

                                }
                            }

                            if (Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()] != null && Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString() == "6")
                            {
                                if (AcctListByICResponse != null)
                                {

                                    ExternalId_Msisdn = AcctListByICResponse.itemList.Where(a => a.ExternalId == Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString()).FirstOrDefault().ExternalId;

                                }
                            }

                            if (ExternalId_Msisdn == string.Empty)
                            {
                                if (AcctListByICResponse.itemList.Any() && AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse == null).ToList().Count() == AcctListByICResponse.itemList.Count())
                                {
                                    ExternalId_Msisdn = AcctListByICResponse.itemList.Where(s => s.Customer != null).FirstOrDefault().ExternalId;
                                }
                            }

                            #region Added by Sindhu on 25th Jul 2013 to get A/c list based on the selected Accoutn Number in Search PAge
                            if (Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()] != null && Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString() == "1001")
                            {
                                if (AcctListByICResponse != null && AcctListByICResponse.itemList.Where(a => a.IsActive && a.AcctExtId == Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString() || a.EmfConfigId == ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2()) != null)
                                {
                                    Items objSelectedItem = AcctListByICResponse.itemList.Where(a => a.IsActive && a.AcctExtId == Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString() || a.EmfConfigId == ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2()).FirstOrDefault();
                                    if (objSelectedItem != null)
                                        ExternalId_Msisdn = objSelectedItem.ExternalId;
                                }

                            }
                            #endregion
                        }
                        #endregion

                        #region CALL SUPPLINE AND ADD TO COLLECTION BY MSISDN FOR GSM A/C'S                                
                        PrinSupServiceProxy.counter = 0;//Applicable to Properties.Settings.Default.DevelopmentModeOnForPpidChecking=true mode only. Will not affect the live mode.

                        if (ExternalId_Msisdn.Length > 0)
                        {
                            foreach (var v in AcctListByICResponse.itemList.Where(c => (c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob == "POSTGSM")).ToList())
                            {
                                if (v.EmfConfigId == ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2())
                                {
                                    retrieveMismDetlsResponse primSecResponse = PrinSupproxy.getPrimSecondaryines(v.ExternalId);

                                    //Code added by ravi as it was throwing exception since we are getting PrimSecInd as 's' on Feb 13 2014 
                                    var curprimSecResponse = primSecResponse.itemList.Where(a => a.PrimSecInd == "P");
                                    //Code added by ravi as it was throwing exception since we are getting PrimSecInd as 's' on Feb 13 2014  ends here

                                    //if condition also changed by ravi on feb 13 2014
                                    if (primSecResponse != null && primSecResponse.itemList.ToList().Count > 0 && curprimSecResponse.Count() > 0)
                                    {
                                        var prinExternalID = curprimSecResponse.FirstOrDefault().Msisdn;
                                        var prinIsMISM = AcctListByICResponse.itemList.Where(i => i.ExternalId == prinExternalID).FirstOrDefault();
                                        if (prinIsMISM != null && !prinIsMISM.IsMISM)
                                        {
                                            AcctListByICResponse.itemList.Where(i => i.ExternalId == prinExternalID).ToList().ForEach(i => i.SecondarySimList = primSecResponse.itemList.Where(a => a.PrimSecInd != "P").ToList());
                                        }
                                        AcctListByICResponse.itemList.Where(i => i.ExternalId == prinExternalID).ToList().ForEach(i => i.IsMISM = true);
                                    }
                                }
                            }
                            var req2 = new SubscribergetPrinSuppRequest();
                            foreach (var v in AcctListByICResponse.itemList.Where(c => (c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob == "POSTGSM" && c.IsMISM == false)).ToList())
                            {
                                for (int i = 0; i < AcctListByICResponse.itemList.Count; i++)
                                {
                                    if (AcctListByICResponse.itemList[i].ExternalId == v.ExternalId && AcctListByICResponse.itemList[i].IsActive)
                                    {
                                        response = AcctListByICResponse.itemList[i].PrinSupplimentaryResponse;
                                    }
                                }

                                ///SET THE CORRESPONDING PrinSuppResponse OBJECT WITH RETURNED response
                                if (response != null && response.itemList != null && response.itemList.ToList().Count > 0)
                                {
                                    AcctListByICResponse.itemList.Where(i => i.ExternalId == v.ExternalId && i.IsActive).ToList().ForEach(i => i.PrinSuppResponse = response);
                                    AcctListByICResponse.itemList.Where(i => i.ExternalId == v.ExternalId && i.IsActive).ToList().ForEach(i => i.IsMISM = false);
                                }
                            }
                            req2 = null;
                            AcctListByICResponse.itemList.Where(i => i.EmfConfigId == ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2()).ToList().ForEach(i => i.ServiceInfoResponse.prinSuppInd = "S");
                        }
                        #endregion

                        ///EXISTING USER
                        Session[SessionKey.PPID.ToString()] = "E";
                    }
                    else if (AcctListByICResponse.itemList.Exists(c => (c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob != "POSTGSM")))
                    {
                        ///EVEN IF THE USER DOES NOT HAVE ANY GSM A/C AS PER REQUIREMENT ABOVE GET THE CUSTOMER INFORMATION BASED ON NON GSM A/C
                        ExternalId_Msisdn = AcctListByICResponse.itemList.FirstOrDefault(c => (c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob != "POSTGSM")).ExternalId;
                        ///New user

                        // edited by Gry, since it's replacing the flag.
                        Session[SessionKey.PPID.ToString()] = !string.IsNullOrEmpty(Session[SessionKey.PPID.ToString()].ToString2()) ? Session[SessionKey.PPID.ToString()].ToString2() : "N";
                    }
                    else if (AcctListByICResponse.itemList.Any() && AcctListByICResponse.itemList.Exists(c => (c.ServiceInfoResponse == null)) && AcctListByICResponse.itemList.Where(c => (c.ServiceInfoResponse == null)).ToList().Count == AcctListByICResponse.itemList.Count())
                    {
                        ///EVEN IF THE USER DOES NOT HAVE ANY GSM A/C AS PER REQUIREMENT ABOVE GET THE CUSTOMER INFORMATION BASED ON NON GSM A/C
                        ExternalId_Msisdn = AcctListByICResponse.itemList.FirstOrDefault(c => (c.ServiceInfoResponse == null)).ExternalId;
                        ///New user
                        Session[SessionKey.PPID.ToString()] = "E";
                    }
                    #endregion

                    #region GET THE ACCOUNT DETAILS AND CUSTOMER INFORMATION NOW, BEING THE BELOW MSISDN BELONGING TO GSM OR NONGSM
                    if (!string.IsNullOrWhiteSpace(ExternalId_Msisdn))
                    {
                        #region CUSTOMER INFORMATION
                        string loggeduser = Request.Cookies["CookieUser"].Value;
                        customer = null;
                        ///GET THE CUSTOMER

                        if (!ReferenceEquals(Session[SessionKey.isSuperAdmin.ToString()], null))
                        {
                            isSuperAdmin = (bool)Session[SessionKey.isSuperAdmin.ToString()];
                        }
                        if (isSuperAdmin)
                        {
                            customerVip = AcctListByICResponse.itemList.FirstOrDefault(i => i.Customer != null).Customer;
                            Session[SessionKey.isVIP.ToString()] = customerVip.Dob;
                        }

                        /*
                        if (Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()] != null && Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString() == "6")
                        {
                            if (AcctListByICResponse != null)
                            {
                                AcctListByICResponse.itemList = AcctListByICResponse.itemList.Where(a => a.ExternalId == Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString()).ToList();
                                Logger.Debug("key for filtering in MSSIDN: " + Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString());
                            }
                        }
                        */

                        #region CR #1652 - For Contract Count Display in Search Result
                        Session[SessionKey.CustomerContractsByType.ToString()] = WebHelper.Instance.GetActiveContract(AcctListByICResponse);
                        #endregion

                        #region Added by Sindhu on 25th Jul 2013 to get A/c list based on the selected Accoutn Number in Search PAge edited by hadi, now it search MSISDN too, show all MSISDN in account searched MSISDN
                        string accId = "";
                        String principleAcc = "";
                        string searchType = Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString2();
                        string searchValue = Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString2();
                        // 1001 = Acct, 6 = MSISDN
                        Boolean isSuplementaryStandAlone = false;
                        String searchedMSISDN = string.Empty;

                        if (!string.IsNullOrEmpty(searchType) && (searchType.Equals("1001") || searchType.Equals("6")))
                        {
                            if (AcctListByICResponse != null)
                            {
                                if (searchType.Equals("6"))
                                {
                                    accId = AcctListByICResponse.itemList.Where(a => a.ExternalId.Equals(searchValue) && !ReferenceEquals(a.ServiceInfoResponse, null) && (a.ServiceInfoResponse.serviceStatus != "D") && a.IsActive).Select(a => a.AcctExtId).FirstOrDefault();
                                    searchedMSISDN = searchValue;//21032015 - Anthony - Displaying data for standalone supplementary properly
                                }
                                else
                                {
                                    accId = searchValue;
                                    if (AcctListByICResponse.itemList.Where(x => x.AcctExtId == accId && x.IsActive == true).Any())
                                    {
                                        searchedMSISDN = AcctListByICResponse.itemList.Where(x => x.AcctExtId == accId && x.IsActive == true).FirstOrDefault().ExternalId;//21032015 - Anthony - Displaying data for standalone supplementary properly
                                    }
                                }

                                //String searchedMSISDN = AcctListByICResponse.itemList.Where(x => x.AcctExtId == accId && x.IsActive == true).FirstOrDefault().ExternalId;//21032015 - Anthony - Displaying data for standalone supplementary properly
                                String accIntId = AcctListByICResponse.itemList.Where(x => x.AcctExtId == accId).FirstOrDefault().AcctIntId;

                                #region gerry for drop 4 enhance standalone supplementary
                                /*
							var prinRow = AcctListByICResponse.itemList.Where(x => x.AcctExtId == accId && x.PrinSuppResponse != null && x.PrinSuppResponse.itemList != null && x.PrinSuppResponse.itemList.Count() > 0).Select(y => y.PrinSuppResponse.itemList).ToList();
							var searchExcludePrin = AcctListByICResponse.itemList.Where(x => x.AcctExtId != accId).ToList();
							retrieveAcctListByICResponse ssDetails = new retrieveAcctListByICResponse();
							ssDetails.itemList = new List<Items>();
							bool standAloneSuppExist = false;

							//search if getting standalone supplementary
							foreach (var acc in searchExcludePrin)
							{
								if (!ReferenceEquals(prinRow, null))
								{
									foreach (var prinSupp in prinRow)
									{
										foreach (var p in prinSupp)
										{
											if (p.msisdnField.Equals(acc.ExternalId)){
												ssDetails.itemList.AddRange(AcctListByICResponse.itemList.Where(x => x.ExternalId.Equals(p.msisdnField) && x.ServiceInfoResponse.prinSuppInd.Equals("S")).ToList());
												standAloneSuppExist = true;
											}
										}
									}
								}
							}
							//foreach (var acc in AcctListByICResponse.itemList.Where(x => x.AcctExtId != accId))
							//{
							//    if (acc.PrinSupplimentaryResponse != null && acc.PrinSupplimentaryResponse.itemList != null && !acc.PrinSupplimentaryResponse.itemList.IsEmpty())
							//    {
							//        if (!acc.PrinSupplimentaryResponse.itemList.Where(x => x.msisdnField == searchedMSISDN).IsEmpty())
							//        {
							//            principleAcc = acc.AcctExtId;
							//            isSuplementaryStandAlone = true;
							//        }
							//    }
							//}

								*/
                                #endregion

                                foreach (var acc in AcctListByICResponse.itemList.Where(x => x.AcctExtId != accId))
                                {
                                    if (acc.PrinSupplimentaryResponse != null && acc.PrinSupplimentaryResponse.itemList != null && !acc.PrinSupplimentaryResponse.itemList.IsEmpty())
                                    {
                                        if (!acc.PrinSupplimentaryResponse.itemList.Where(x => x.msisdnField == searchedMSISDN).IsEmpty())
                                        {
                                            principleAcc = acc.AcctExtId;
                                            isSuplementaryStandAlone = true;
                                            AcctListByICResponse.itemList.Where(x => x.AcctExtId == principleAcc || x.ExternalId == searchedMSISDN);
                                            break;
                                        }
                                    }
                                }

                                //Logger.Debug("Account id for searching is: " + accId);
                                //Logger.Debug("What is EMF Config ID: " + ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2());
                                //Logger.Debug("Is standalone: " + isSuplementaryStandAlone);
                                //AcctListByICResponse.itemList = AcctListByICResponse.itemList.Where(a => a.AcctExtId == accId).ToList();

                                if (Session["IsDealer"].ToBool() || isUserCCCAbove == false)
                                {
                                    if (isSuplementaryStandAlone)
                                    {
                                        AcctListByICResponse.itemList = AcctListByICResponse.itemList.Where(x => x.AcctExtId == principleAcc || x.ExternalId == searchedMSISDN && !ReferenceEquals(x.ServiceInfoResponse, null)).ToList();

                                    }
                                    else
                                    {
                                        AcctListByICResponse.itemList = AcctListByICResponse.itemList.Where(a => a.AcctExtId == accId || a.EmfConfigId == ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2() && !ReferenceEquals(a.ServiceInfoResponse, null)).ToList();
                                    }
                                }
                                //if (standAloneSuppExist)
                                //{
                                //    AcctListByICResponse.itemList.AddRange(ssDetails.itemList);
                                //}   
                            }
                        }
                        #endregion

                        if (!ReferenceEquals(Session["AcctAllDetails"], null))
                        {
                            AcctListByICResponse = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                        }


                        #endregion CUSTOMER INFORMATION
                        #region ACCOUNT INFORMATION

                        if (_isCustVIP)
                            AcctListByICResponse = VIPMassking(AcctListByICResponse);

                        ///GET THE ACCOUNT DETAILS WITH PAYMENT DETAILS SAME RULE LIKE CUSTOMER AS EXPLAINED BY PATANJALI OVER PHONE

                        if (!ReferenceEquals(AcctListByICResponse, null))
                        {
                            // Vikas - If service info is null then data should not be displayed
                            //AcctListByICResponse.itemList = AcctListByICResponse.itemList.Where(e => (e.ServiceInfoResponse == null || (e.ServiceInfoResponse != null && e.ServiceInfoResponse.serviceStatus.ToUpper() != "D"))).ToList();
                            AcctListByICResponse.itemList = AcctListByICResponse.itemList.Where(e => (e.ServiceInfoResponse != null && e.ServiceInfoResponse.serviceStatus.ToUpper() != "D") && e.IsActive).ToList();

                            //28092015 - Karyne - MSISDN searched will be top in display list if customer search by MSISDN
                            if (searchType.Equals("6"))
                            {
                                var oldIndex = 0;
                                var newIndex = 0;

                                oldIndex = AcctListByICResponse.itemList.FindIndex(b => b.ExternalId == searchValue);
                                
                                var tmpData = AcctListByICResponse.itemList[newIndex];

                                AcctListByICResponse.itemList[newIndex] = AcctListByICResponse.itemList[oldIndex];
                                AcctListByICResponse.itemList[oldIndex] = tmpData;

                            }

                            //28092015 - Karyne - Account Number searched will be top in display list if customer search by Account Number
                            if (searchType.Equals("1001"))
                            {
                                var oldIndex = 0;
                                var newIndex = 0;

                                oldIndex = AcctListByICResponse.itemList.FindIndex(b => b.AcctExtId == searchValue);
                                
                                var tmpData = AcctListByICResponse.itemList[newIndex];

                                AcctListByICResponse.itemList[newIndex] = AcctListByICResponse.itemList[oldIndex];
                                AcctListByICResponse.itemList[oldIndex] = tmpData;                               
                            }

                            Session[SessionKey.PPIDInfo.ToString()] = AcctListByICResponse;

                            Logger.Debug("AcctListByICResponse = " + XMLHelper.ConvertObjectToXml(AcctListByICResponse));

                            //Ricky - Bugzilla 830 - If customer has no data, need to be displayed as a new customer
                            if (AcctListByICResponse.itemList.Count() == 0)
                            {
                                Session[SessionKey.PPID.ToString()] = "N";
                                Session[SessionKey.SR_PPIDCheck.ToString()] = "N";
                            }
                        }
                        IndividualAccount = WebHelper.Instance.PopulateAccountInfo(msisdn: ExternalId_Msisdn);

                        //TODO:Shobha Integration of services to get the count of MISM count is pending 
                        Session[SessionKey.MISMCount.ToString()] = 3;

                        ///ATTACH THE ACCOUNT INFORMATION        
                        if (!ReferenceEquals(IndividualAccount, null))
                        {
                            AcctListByICResponse.itemList.ToList().ForEach(i => i.Account = IndividualAccount);
                        }
                        ///SET OTHER CUSTOMER INFORMATION TO NULL THOUGH THEY WERE NEVER INITIALIZED
                        #endregion ACCOUNT INFORMATION
                    }
                    #endregion
                }
                else
                {
                    ///AS ALL ServiceInfoResponse = null
                    ///New user
                    Session[SessionKey.PPID.ToString()] = "N";

                    ///Newly implemented functionality if there is no response from the service display sorry message
                    ///As All SericeInfoResponse = null
                    ViewBag.ServerConnStatus = ConfigurationManager.AppSettings["ServerConnStatus"].ToString();

                    #region VLT ADDED CODE by Rajeswari on Feb 25th to log user PPID result
                    LogUserInfo(collection["Customer.IDCardNo"], IDCardTypeID);
                    #endregion

                    #region VLT ADDED CODE
                    if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null) && !string.IsNullOrWhiteSpace(Convert.ToString(Session[SessionKey.RedirectURL.ToString()])))
                    {
                        string redirectUrl, controllerName, actionName = string.Empty;
                        try
                        {
                            redirectUrl = Convert.ToString(Session[SessionKey.RedirectURL.ToString()]);
                            string[] urlParts = redirectUrl.Split('/');

                            controllerName = urlParts[0];
                            actionName = urlParts[1];

                            Session[SessionKey.MenuType.ToString()] = (int)MenuType.MobileRegistration;

                            WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");

                            return RedirectToAction(actionName, controllerName);
                        }
                        catch
                        {

                        }
                    }
                    #endregion VLT ADDED CODE
                    validationCheckType2 = (Session[SessionKey.PPID.ToString()].ToString() == "E" ? "E" : "N");
                    WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), validationCheckType2);
                    return View();
                }

                if (!ReferenceEquals(Session["SearchBy"].ToString2(), IDCardTypeID) && !ReferenceEquals(Session["cardNumber"].ToString2(), IDCardNo))
                {
                    ViewBag.IDCardTypeID = Session["SearchBy"].ToString2();
                    ViewBag.IDCardNo = Session["cardNumber"].ToString2();
                }
                else
                {
                    ViewBag.IDCardTypeID = IDCardTypeID;
                    ViewBag.IDCardNo = IDCardNo;
                }

                #region VLT ADDED CODE by Rajeswari on Feb 25th to log user PPID result
                LogUserInfo(IDCardNo, IDCardTypeID);
                #endregion

                string stricno = "";
                string MOCIDs = "";
                using (var MOCandWhitelistcheck = new UserServiceProxy())
                {
                    foreach (var v in AcctListByICResponse.itemList.Where(c => (c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob == "POSTGSM")).ToList())
                    {
                        if (!ReferenceEquals(v.Customer, null) && v.Customer.Moc != "")
                        {
                            if (MOCIDs != "")
                                MOCIDs = MOCIDs + ";" + v.Customer.Moc;
                            else
                                MOCIDs = v.Customer.Moc; ;

                        }
                        else
                        {
                            if (MOCIDs == "")
                            {
                                MOCIDs = string.Empty;
                            }
                        }

                    }
                    stricno = IDCardNo;
                    bool Mocandwhitelistcuststatus = MOCandWhitelistcheck.MocICcheckwithwhitelistcustomers(Convert.ToString(Properties.Settings.Default.retrieveAcctListByICResponseUserName), stricno);
                    Session[SessionKey.Mocandwhitelistcuststatus.ToString()] = Mocandwhitelistcuststatus;
                    Session[SessionKey.MocDefault.ToString()] = Properties.Settings.Default.MOCdiscount;
                    Session[SessionKey.MocandwhitelistSstatus.ToString()] = Mocandwhitelistcuststatus;
                    Session[SessionKey.MOCIDs.ToString()] = MOCIDs;
                }
            }

            #endregion PPID CHECK

            //GTM e-Billing CR - Ricky - 2014.09.25
            Session[SessionKey.billDelivery_msisdnListByAccount.ToString()] = WebHelper.Instance.GetMsisdnListByAccount(AcctListByICResponse);

            //Ricky - Drop5 UAT Bugzilla 1000 - Only check if the count is more than 0
            if (AcctListByICResponse.itemList.Count() > 0 && AcctListByICResponse.itemList.Count(e => e.ServiceInfoResponse == null) == AcctListByICResponse.itemList.Count)
            {
                Session["WriteOffHardstop"] = "True";
            }

            #region VLT Added - RedirectURL
            if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null) && !string.IsNullOrWhiteSpace(Convert.ToString(Session[SessionKey.RedirectURL.ToString()])))
            {
                string redirectUrl, controllerName, actionName = string.Empty;
                try
                {
                    redirectUrl = Convert.ToString(Session[SessionKey.RedirectURL.ToString()]);
                    string[] urlParts = redirectUrl.Split('/');

                    controllerName = urlParts[0];
                    actionName = urlParts[1];

                    Session[SessionKey.MenuType.ToString()] = (int)MenuType.MobileRegistration;

                    if (Session[SessionKey.PPID.ToString()] != null)
                    {
                        if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                        {
                            WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "E");
                        }
                        else
                        {
                            WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");
                        }

                    }
                    if (Session[SessionKey.VIP.ToString()] != null && Session[SessionKey.VIP.ToString()].ToString() != "")
                    {
                        return RedirectToAction("Index", new { UserStop = "VIP" });
                    }
                    else if (Session[SessionKey.WO.ToString()] != null && Session[SessionKey.WO.ToString()].ToString() != "")
                    {
                        return RedirectToAction("Index", new { UserStop = "WO" });
                    }
                    else
                    {
                        return RedirectToAction(actionName, controllerName);
                    }
                }
                catch
                {

                }
            }
            #endregion

            #region Code to get Outstanding Account details for Dealer

            if (Session["IsDealer"].ToBool())
            {
                try
                {
                    var lstBREList = new List<BusinessRuleRequest>();
                    var respBRE = new List<BusinessRuleResponse>();
                    string IDcardTypeKenanCode = string.Empty;

                    if (Session["IDCardTypeKenanCode"] != null)
                        IDcardTypeKenanCode = Session["IDCardTypeKenanCode"].ToString2();
                    else
                    {
                        var idCardType = new IDCardType();
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            idCardType = proxy.IDCardTypeGet(Convert.ToInt32(Session[SessionKey.IDCardTypeID.ToString()].ToString()));
                        }
                        IDcardTypeKenanCode = idCardType.KenanCode;
                    }

                    //Outstanding Credit Check
                    var distinctExternalIDS = AcctListByICResponse.itemList.Select(x => x.AcctExtId).Distinct();

                    lstBREList.AddRange(distinctExternalIDS.Select(externalID => new BusinessRuleRequest
                    {
                        RuleNames = "OutstandingCreditCheck".Split(' '),
                        kenanExtAcctId = externalID,
                        IDCardNo = IDCardNo,
                        IDCardType = IDcardTypeKenanCode,
                        businessRuleType = BusinessRuleType.OutstandingCreditCheck
                    }));

                    using (var proxy = new KenanServiceProxy())
                    {
                        respBRE = proxy.BusinessRuleOneTimeCheck(lstBREList);
                    }

                    if (respBRE.Count(e => e.businessRuleType == BusinessRuleType.OutstandingCreditCheck && e.Success == true) > 0)
                    {
                        var outStandingCheckResponse = respBRE.Where(e => e.businessRuleType == BusinessRuleType.OutstandingCreditCheck && e.IsOutstandingCreditCheckFailed);

                        List<KeyValuePair<string, string>> outStandingAccts = outStandingCheckResponse.Select(x => new KeyValuePair<string, string>(x.KenanAccountId, string.IsNullOrEmpty(x.OutStandingAmount) ? "" : x.OutStandingAmount)).ToList();

                        if (outStandingAccts.Count > 0)
                            Session["OutStandingAcctDtls"] = outStandingAccts;
                    }
                }
                catch (Exception ex)
                {
                }
            }
            #endregion

            validationCheckType2 = (Session[SessionKey.PPID.ToString()].ToString() == "E" ? "E" : "N");
            WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), validationCheckType2);
            return View();
        }

        public void generatePopUpMessage(retrieveAcctListByICResponse allServiceDetails, string IDCardNo, out List<ViewPopUpMessage> ViewMessage)
        {
            using (var proxy = new RegistrationServiceProxy())
            {
                ViewMessage = new List<ViewPopUpMessage>();
                List<PopUpMessage> Message = new List<PopUpMessage>();

                if (!string.IsNullOrEmpty(IDCardNo))
                {
                    // list all msisdn w/ message
                    Message = proxy.GetPopUpMessageByIdCard(IDCardNo);

                    var msisdn = new List<ViewPopUpMessage>();
                    #region list all msisdn w/ plan name
                    //var supplineMSISDNs = new List<ViewPopUpMessage>();
                    //var anyTelephonyComponent = false;
                    //var anySharingComponent = false;
                    //if (allServiceDetails != null && allServiceDetails.itemList != null)
                    //{
                    //    if (allServiceDetails.itemList.Count > 0)
                    //    {
                    //        foreach (var items in allServiceDetails.itemList)
                    //        {
                    //            var extID = items.ExternalId;
                    //            var desc = items.ServiceInfoResponse.packageName;
                    //            var packageID = items.ServiceInfoResponse.packageId;
                    //            msisdn.Add(new ViewPopUpMessage { MSISDN = extID, PlanName = desc });

                    //        }
                    //    }
                    //}
                    #endregion

                    //if (!ReferenceEquals(msisdn, null) && !ReferenceEquals(Message,null))
                    if (!ReferenceEquals(Message, null))
                    {
                        foreach (var msg in Message)
                        {
                            //var planName = msisdn.Where(x => x.MSISDN == msg.MSISDN).FirstOrDefault().PlanName;
                            var planName = string.Empty;
                            ViewMessage.Add(new ViewPopUpMessage { ID = msg.ID, MSISDN = msg.MSISDN, PlanName = planName, Message = msg.Message });
                        }
                    }
                }
            }

        }

        /// <summary>
        /// To display harcoded pop-up message for MaxisONE Share offer
        /// </summary>
        /// <param name="allServiceDetails"></param>
        /// <param name="msisdn"></param>
        public void checkExistingMaxisONEPlan(retrieveAcctListByICResponse allServiceDetails, out List<string> msisdn)
        {
            msisdn = new List<string>();
            var prinKenanCode = ConfigurationManager.AppSettings["MaxisONEPlan_KenanCode"].ToString2();
            var telephonyKenanCode = ConfigurationManager.AppSettings["MaxisONEPlan_Telephony_KenanCode"].ToString2();
            var sharingComponentKenanCode = ConfigurationManager.AppSettings["MaxisONEPlan_SharingComponent_KenanCode"].ToString2();
            var supplineONEShareKenanCode = ConfigurationManager.AppSettings["Supplementary_MaxisONEShare_KenanCode"].ToString2();
            var maxSharingSuppCount = ConfigurationManager.AppSettings["MaxSharingSuppInExistingPrincipal"].ToInt();
            var supplineMSISDNs = new List<string>();
            var anyTelephonyComponent = false;
            var anySharingComponent = false;
            try
            {
                if (allServiceDetails != null && allServiceDetails.itemList != null)
                {
                    if (allServiceDetails.itemList.Count > 0)
                    {
                        foreach (var prinItem in allServiceDetails.itemList)
                        {

                            if (prinItem.Packages != null && prinItem.Packages.Count > 0)
                            {
                                var prinPackage = prinItem.Packages.Where(x => !string.IsNullOrEmpty(x.PackageID) && prinKenanCode.Contains(x.PackageID)).ToList();

                                if (prinPackage.Count > 0)
                                {
                                    //Principal Section
                                    var maxisONEPlanPackage = prinPackage.FirstOrDefault();
                                    var packageDescription = maxisONEPlanPackage.PackageDesc.Split('-')[0];
                                    var telephonyComponent = maxisONEPlanPackage.compList.Where(x => !string.IsNullOrEmpty(x.componentId) && telephonyKenanCode.Contains(x.componentId)).ToList();
                                    if (telephonyComponent.Count > 0)
                                        anyTelephonyComponent = true;

                                    var sharingComponent = maxisONEPlanPackage.compList.Where(x => !string.IsNullOrEmpty(x.componentId) && sharingComponentKenanCode.Contains(x.componentId)).ToList();
                                    if (sharingComponent.Count > 0) anySharingComponent = true;

                                    //Suppline Section
                                    if (prinItem.PrinSupplimentaryResponse != null && prinItem.PrinSupplimentaryResponse.itemList != null)
                                    {
                                        var suppMSISDNs = prinItem.PrinSupplimentaryResponse.itemList.Select(x => x.msisdnField).ToList();

                                        if (suppMSISDNs.Count > 0)
                                        {
                                            var supplineONEShareCount = 0;
                                            var supplineMSISDNWithDecsription = new List<string>();
                                            foreach (var suppMSISDN in suppMSISDNs.ToList())
                                            {
                                                var suppItems = allServiceDetails.itemList.Where(x => x.ExternalId == suppMSISDN && x.IsActive == true).ToList();
                                                var suppPackageDescription = string.Empty;
                                                if (suppItems.Count > 0)
                                                {
                                                    var suppItemList = suppItems.FirstOrDefault();
                                                    var suppONESharePackage = suppItemList.Packages.Where(x => !string.IsNullOrEmpty(x.PackageID) && supplineONEShareKenanCode.Contains(x.PackageID)).ToList();
                                                    var suppPackageList = suppItemList.Packages.ToList();

                                                    if (suppONESharePackage.Count > 0)
                                                    {
                                                        supplineONEShareCount++;
                                                    }

                                                    foreach (var suppPackage in suppPackageList)
                                                    {
                                                        var hasTelephonyPackage = suppPackage.compList.Where(x => x.componentDesc.ToUpper().Equals("MOBILE - TELEPHONY")).Any();
                                                        if (hasTelephonyPackage) suppPackageDescription = suppPackage.PackageDesc.Split('-')[0];
                                                    }

                                                    if (suppONESharePackage.Count > 0 ? false : true)
                                                    {
                                                        supplineMSISDNWithDecsription.Add(suppMSISDN + "#" + suppPackageDescription);
                                                    }
                                                }
                                            }

                                            //Main logic section
                                            //if (supplineONEShareCount > 1) anySharingComponent = true;//DEBUG TEST
                                            if (anyTelephonyComponent && (anySharingComponent == false || (anySharingComponent && supplineONEShareCount == 0)))
                                            {
                                                msisdn.Add(prinItem.ExternalId + "#" + packageDescription);
                                                foreach (var suppMSISDNWithDescription in supplineMSISDNWithDecsription)
                                                {
                                                    msisdn.Add(suppMSISDNWithDescription);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (anyTelephonyComponent && anySharingComponent == false)
                                            {
                                                msisdn.Add(prinItem.ExternalId + "#" + packageDescription);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        /// <summary>
        /// To display the eligible MSISDN for MaxisONE Share Offer (Extended Version)
        /// </summary>
        /// <param name="allServiceDetails"></param>
        /// <param name="msisdn"></param>
        /// <returns></returns>
        public List<string> checkMaxisONEShareEligibility(retrieveAcctListByICResponse allServiceDetails, List<string> msisdn)
        {
            var mopFamilyKenanCode = ConfigurationManager.AppSettings["MaxisONEPlanFamily_KenanCode"].ToString2();
            var maxisONEShareKenanCode = ConfigurationManager.AppSettings["Supplementary_MaxisONEShare_KenanCode"].ToString2();
            var mopPackages = new List<PackageModel>();
            var mopPackage = new PackageModel();
            var suppMSISDNList = new List<string>();

            if (allServiceDetails != null && allServiceDetails.itemList != null && allServiceDetails.itemList.Count > 0)
            {
                foreach (var itemList in allServiceDetails.itemList)
                {
                    if (itemList.Packages != null && itemList.Packages.Count > 0)
                    {
                        mopPackages = itemList.Packages.Where(x => !string.IsNullOrEmpty(x.PackageID) && mopFamilyKenanCode.Contains(x.PackageID)).ToList();

                        if (mopPackages.Count > 0)
                        {
                            mopPackage = mopPackages.FirstOrDefault();

                            if (itemList.PrinSupplimentaryResponse != null && itemList.PrinSupplimentaryResponse.itemList != null && itemList.PrinSupplimentaryResponse.itemList.Count > 0)
                            {
                                //if the principal line has supplementary
                                suppMSISDNList = itemList.PrinSupplimentaryResponse.itemList.Select(x => x.msisdnField).ToList();

                                if (suppMSISDNList.Count > 0)
                                {
                                    var maxisONEShareCount = 0;
                                    var supplementaryMSISDNList = new List<string>();

                                    foreach (var suppMSISDN in suppMSISDNList)
                                    {
                                        var supplementaryItemList = allServiceDetails.itemList.Where(x => x.ExternalId == suppMSISDN && x.IsActive == true).ToList();

                                        if (supplementaryItemList.Count > 0)
                                        {
                                            var supplementaryPackages = supplementaryItemList.FirstOrDefault().Packages.Where(x => !string.IsNullOrEmpty(x.PackageID) && maxisONEShareKenanCode.Contains(x.PackageID)).ToList();

                                            if (supplementaryPackages.Count > 0)
                                            {
                                                maxisONEShareCount++;
                                            }
                                            else
                                            {
                                                var supplementaryPackage = supplementaryItemList.FirstOrDefault().Packages;
                                                var supplementaryPackageName = string.Empty;
                                                foreach (var suppPackage in supplementaryPackage)
                                                {
                                                    var hasTelephonyPackage = suppPackage.compList.Where(x => x.componentDesc.ToUpper().Equals("MOBILE - TELEPHONY")).Any();
                                                    if (hasTelephonyPackage) supplementaryPackageName = suppPackage.PackageDesc.Split('-')[0];
                                                }

                                                supplementaryMSISDNList.Add(suppMSISDN + "#" + supplementaryPackageName);
                                            }
                                        }
                                    }

                                    if (maxisONEShareCount == 0)
                                    {
                                        msisdn.Add(itemList.ExternalId + "#" + mopPackage.PackageDesc.Split('-')[0]);
                                        msisdn.AddRange(supplementaryMSISDNList);
                                    }
                                }
                            }
                            else
                            {
                                msisdn.Add(itemList.ExternalId + "#" + mopPackage.PackageDesc.Split('-')[0]);
                            }
                        }
                    }
                }
            }
            return msisdn;
        }
        
        /// <summary>
        /// This method will redirect to Plan Selection of Corporate type
        /// </summary>
        /// <returns></returns>
        public ActionResult SMEISelectPlan()
        {
            Session[SessionKey.isFromBRNSearch.ToString()] = true;
            return RedirectToAction("SelectPlan", "Registration", new { type = (int)MobileRegType.PlanOnly });
        }

        /// <summary>
        /// This method will redirect to Device Selection of Corporate type
        /// </summary>
        /// <returns></returns>
        public ActionResult SMEISelectDevice()
        {
            Session[SessionKey.isFromBRNSearch.ToString()] = true;
            return RedirectToAction("SelectDevice", "Registration", new { type = (int)MobileRegType.DevicePlan });
        }

        /// <summary>
        /// This method will redirect to AddSuppline of Corporate type
        /// </summary>
        /// <returns></returns>
        public ActionResult SMEIAddSuppline()
        {
            Session[SessionKey.isFromBRNSearch.ToString()] = true;
            return RedirectToAction("AddSuppline", "Suppline", new { type = (int)MobileRegType.SuppPlan });
        }

        /// <summary>
        /// This method will redirect to MNP of Corporate type
        /// </summary>
        /// <returns></returns>
        public ActionResult SMEIViewOrders()
        {
            Session[SessionKey.isFromBRNSearch.ToString()] = true;
            return RedirectToAction("StoreKeeper", "Registration");
        }

        /// <summary>
        /// This method will redirect to MNP of Corporate type
        /// </summary>
        /// <returns></returns>
        public ActionResult SMEIMNP()
        {
            Session[SessionKey.isFromBRNSearch.ToString()] = true;
            return RedirectToAction("EnterDetails", "MNP");
        }

        /// <summary>
        /// This method returns BRNsearch view
        /// </summary>
        /// <returns></returns>
        public ActionResult BRNSearch()
        {
            Session[SessionKey.isFromBRNSearch.ToString()] = null;
            Session[SessionKey.CompanyInfoResponse.ToString()] = null;
            return View();
        }

        public ActionResult BRNSearchNew()
        {
            var dropObj = new DropFourObj();
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            Session[SessionKey.isFromBRNSearch.ToString()] = null;
            Session[SessionKey.CompanyInfoResponse.ToString()] = null;
            return View();
        }

        /// <summary>
        /// This method is used to search Company details by BRN or Comapny Name based on the collection
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        /// 
        [Authorize]
        [HttpPost]
        public ActionResult BRNSearch(FormCollection collection)
        {
            SubscriberICService.typeRetrieveMasterAcctListResponse masterAccountList = null;
            SubscriberICService.typeRetrieveParentAcctListResponse parentAccountList = null;
            Online.Registration.Web.ViewModels.BRNVm objCompanyInfoResponse = null;
            if (Session[SessionKey.CompanyInfoResponse.ToString2()] != null)
            {
                objCompanyInfoResponse = (Online.Registration.Web.ViewModels.BRNVm)Session[SessionKey.CompanyInfoResponse.ToString2()];
            }
            string SearchType = collection["SearchType"].ToString2();
            string SearchValue = collection["SearchValue"].ToString2().Trim();
            string[] companyInfo = collection["hdnSelectCompanyDtls"].ToString2().Split(',');
            string submitType = collection["SumbitType"].ToString2();
            corporateSearchType = Util.getSearchCorporateType(SearchType);
            if (collection["hdnRegType"].ToString2().Length > 0)
            {
                Session["RegMobileReg_Type"] = collection["hdnRegType"];
            }
            if (submitType == "Search")
            {
                objCompanyInfoResponse = null;
            }
            try
            {
                if (objCompanyInfoResponse != null && (objCompanyInfoResponse.SearchType.ToString2() == "Parent" || Util.getSearchCorporateType(objCompanyInfoResponse.SearchType.ToString2()) == CorpType.PARENTACCT))
                {

                    if (companyInfo != null && companyInfo.Length == 4)
                    {
                        objCompanyInfoResponse.CompanyName = companyInfo[0];
                        objCompanyInfoResponse.MarketCode = companyInfo[1];
                        objCompanyInfoResponse.CorpParentID = companyInfo[2];
                        objCompanyInfoResponse.CorpParentExtId = companyInfo[3];
                        Session[SessionKey.isFromBRNSearch.ToString()] = true;
                        Session[SessionKey.CompanyInfoResponse.ToString2()] = objCompanyInfoResponse;
                        MobileRegType objRegType = (MobileRegType)Session["RegMobileReg_Type"].ToInt();
                        switch (objRegType)
                        {
                            case MobileRegType.PlanOnly:
                                return RedirectToAction("SelectPlanNew", "Registration", new { type = (int)MobileRegType.PlanOnly });
                                break;
                            case MobileRegType.DevicePlan:
                                // change for drop 4
                                return RedirectToAction("SelectDevicePlanNew", "Registration", new { type = (int)MobileRegType.DevicePlan });
                                break;
                            default:
                                return RedirectToAction("EnterDetailsNew", "MNP");
                                break;
                        }

                    }

                }
                else
                {
                    switch (corporateSearchType)
                    {
                        case CorpType.MASTERACCT:
                            using (var proxy = new retrieveServiceInfoProxy())
                            {
                                string vaidMarketCodes = (System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"] != null ? System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"].ToString2() : "");
                                string[] listValidMktCodes = vaidMarketCodes.Split(',');
                                if (objCompanyInfoResponse == null)
                                {
                                    CustomizedCustomer subscriberDetails = proxy.retrieveSubscriberDetlsByAcctNo(SearchValue, Request.Cookies["CookieUser"].Value, isSuperAdmin);
                                    if (subscriberDetails != null && subscriberDetails.MsgCode == "0")
                                    {
                                        AccountDetails accountDetails = proxy.RetrieveAccountDetails(SearchValue);
                                        if (accountDetails != null && !string.IsNullOrEmpty(accountDetails.MktCode) && listValidMktCodes.Contains(accountDetails.MktCode) && string.IsNullOrEmpty(accountDetails.ParentId))
                                        {
                                            objCompanyInfoResponse = new ViewModels.BRNVm();
                                            masterAcctList MasterList = new masterAcctList();
                                            MasterList.fxAcctExtIdField = SearchValue;
                                            MasterList.companyNameField = subscriberDetails.CustomerName;
                                            MasterList.brnField = subscriberDetails.Brn;
                                            MasterList.fxAcctNoField = subscriberDetails.CorpMasterExtId;
                                            objCompanyInfoResponse.MasterList.Add(MasterList);
                                            objCompanyInfoResponse.SearchType = "Master";
                                            objCompanyInfoResponse.SearchValue = SearchValue;
                                        }
                                        else
                                        {
                                            string errorMessage = string.Format("Invalid Master Account Number due to wrong Market Code or Parent ID exist.", SearchValue);
                                            ModelState.AddModelError("Error", errorMessage);
                                        }
                                    }
                                    else if (subscriberDetails == null)
                                    {
                                        ModelState.AddModelError("Error", "No data found.");
                                    }
                                    else if (subscriberDetails != null && subscriberDetails.MsgCode != "0")
                                    {
                                        ModelState.AddModelError("Error", "No data found.");

                                    }
                                }
                                else
                                {
                                    string masteAcctInt = objCompanyInfoResponse.SearchValue.Substring(0, SearchValue.Length - 1);
                                    parentAccountList = proxy.RetrieveParentAccountList(masteAcctInt);

                                    if (parentAccountList != null && parentAccountList.msgCodeField.ToString2() == "0")
                                    {

                                        if (string.IsNullOrEmpty(vaidMarketCodes) == true)
                                        {
                                            objCompanyInfoResponse.ParentList = parentAccountList.parentAcctListField;
                                        }
                                        else
                                        {
                                            objCompanyInfoResponse.ParentList = parentAccountList.parentAcctListField.Where(e => vaidMarketCodes.Split(',').Contains(e.mktCodeField) == true).ToList();
                                            if (objCompanyInfoResponse.ParentList.Any() == false)
                                            {
                                                objCompanyInfoResponse.ParentList = null;
                                                ModelState.AddModelError("Error", "No data found.");
                                            }
                                        }
                                        if (objCompanyInfoResponse.ParentList != null && objCompanyInfoResponse.ParentList.Any())
                                        {
                                            objCompanyInfoResponse.SearchType = "Parent";
                                            objCompanyInfoResponse.CorpMasterID = companyInfo[2];
                                            objCompanyInfoResponse.CorpMasterExtId = companyInfo[3];
                                            objCompanyInfoResponse.CompanyName = objCompanyInfoResponse.ParentList[0].companyNameField;
                                            objCompanyInfoResponse.MarketCode = objCompanyInfoResponse.ParentList[0].mktCodeField;
                                            objCompanyInfoResponse.CorpParentID = objCompanyInfoResponse.ParentList[0].fxAcctNoField;
                                            objCompanyInfoResponse.CorpParentExtId = objCompanyInfoResponse.ParentList[0].fxAcctExtIdField;
                                            if (!string.IsNullOrEmpty(objCompanyInfoResponse.MarketCode))
                                            {
                                                using (var configProxy = new ConfigServiceProxy())
                                                {
                                                    foreach (var parent in parentAccountList.parentAcctListField)
                                                    {
                                                        DAL.Models.Market market = new Market();
                                                        market.KenanCode = parent.mktCodeField;
                                                        var markets = configProxy.MarketSearch(new MarketFind { Market = market });
                                                        parent.noBillField = markets.Any() ? markets.ToList()[0].Name : string.Empty;
                                                    }


                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error occured in contacting WM.";
                                        if (parentAccountList != null)
                                        {
                                            if (parentAccountList.msgCodeField.ToString2() == "2")
                                            {
                                                errorMessage = "No data found.";
                                            }
                                            else
                                            {
                                                errorMessage = parentAccountList.msgDescField.ToString2();
                                            }
                                        }
                                        ModelState.AddModelError("Error", errorMessage);
                                    }
                                }
                            }
                            break;
                        case CorpType.PARENTACCT:
                            using (var proxy = new retrieveServiceInfoProxy())
                            {
                                string vaidMarketCodes = (System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"] != null ? System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"].ToString2() : "");
                                string[] listValidMktCodes = vaidMarketCodes.Split(',');
                                if (objCompanyInfoResponse == null)
                                {
                                    CustomizedCustomer subscriberDetails = proxy.retrieveSubscriberDetlsByAcctNo(SearchValue, Request.Cookies["CookieUser"].Value, isSuperAdmin);
                                    AccountDetails accountDetails = proxy.RetrieveAccountDetails(SearchValue);
                                    if (subscriberDetails != null && subscriberDetails.MsgCode == "0" && accountDetails != null)
                                    {
                                        if (accountDetails != null && !string.IsNullOrEmpty(accountDetails.MktCode) && listValidMktCodes.Contains(accountDetails.MktCode) && !string.IsNullOrEmpty(accountDetails.ParentId))
                                        {
                                            objCompanyInfoResponse = new ViewModels.BRNVm();
                                            parentAcctList parentList = new parentAcctList();
                                            parentList.fxAcctExtIdField = SearchValue;
                                            parentList.companyNameField = subscriberDetails.CustomerName.ToString2();
                                            parentList.fxAcctNoField = subscriberDetails.CorpMasterExtId.ToString2();
                                            parentList.billAddr1Field = subscriberDetails.Address1.ToString2();
                                            parentList.billAddr2Field = subscriberDetails.Address2.ToString2();
                                            parentList.fxAcctNoField = accountDetails.FxAcctNo.ToString2();
                                            parentList.mktCodeField = accountDetails.MktCode.ToString2();
                                            objCompanyInfoResponse.ParentList.Add(parentList);
                                            objCompanyInfoResponse.SearchType = SearchType;
                                            objCompanyInfoResponse.SearchValue = SearchValue;
                                            if (string.IsNullOrEmpty(vaidMarketCodes) == true)
                                            {
                                                objCompanyInfoResponse.ParentList.Add(parentList);
                                            }
                                            objCompanyInfoResponse.CorpMasterID = accountDetails.HierarchyId.ToString2();
                                            objCompanyInfoResponse.CorpMasterExtId = accountDetails.HierarchyId.ToString2();
                                            objCompanyInfoResponse.CompanyName = subscriberDetails.CustomerName.ToString2();
                                            objCompanyInfoResponse.MarketCode = accountDetails.MktCode.ToString2();
                                            objCompanyInfoResponse.CorpParentID = accountDetails.FxAcctNo.ToString2();
                                            objCompanyInfoResponse.CorpParentExtId = accountDetails.FxAcctExtId.ToString2();
                                            if (!string.IsNullOrEmpty(objCompanyInfoResponse.MarketCode))
                                            {
                                                using (var configProxy = new ConfigServiceProxy())
                                                {
                                                    DAL.Models.Market market = new Market();
                                                    market.KenanCode = accountDetails.MktCode;
                                                    var markets = configProxy.MarketSearch(new MarketFind { Market = market });
                                                    parentList.noBillField = markets.Any() ? markets.ToList()[0].Name : string.Empty;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string errorMessage = string.Format("Invalid Parent Account Number due to wrong Market Code or no Parent ID exist.", SearchValue);
                                            ModelState.AddModelError("Error", errorMessage);
                                        }
                                    }
                                    else
                                    {

                                        ModelState.AddModelError("Error", "No data found.");
                                    }
                                }
                            }
                            break;
                        default:
                            using (var proxy = new retrieveServiceInfoProxy())
                            {
                                if (objCompanyInfoResponse == null)
                                {
                                    masterAccountList = proxy.RetrieveMasterAccountList(SearchType, SearchValue);
                                    objCompanyInfoResponse = new ViewModels.BRNVm();
                                    objCompanyInfoResponse.SearchType = SearchType;
                                    objCompanyInfoResponse.SearchValue = SearchValue;
                                    if (masterAccountList != null && masterAccountList.msgCodeField.ToString2() == "0")
                                    {
                                        objCompanyInfoResponse.MasterList = masterAccountList.masterAcctListField;
                                        objCompanyInfoResponse.SearchType = "Master";

                            }
                            else
                            {
                                string errorMessage = "Error occured in contacting WM.";
                                if (masterAccountList != null)
                                {
                                    if (masterAccountList.msgCodeField.ToString2() == "2")
                                    {
                                        errorMessage = "No data found.";
                                    }
                                    else
                                    {
                                        errorMessage = masterAccountList.msgDescField.ToString2();
                                    }
                                }
                                ModelState.AddModelError("Error", errorMessage);
                            }
                        }
                        else
                        {
                            objCompanyInfoResponse.SearchType = "Parent";
                            objCompanyInfoResponse.CompanyName = companyInfo[0];
                            objCompanyInfoResponse.BRN = companyInfo[1];
                            objCompanyInfoResponse.CorpMasterID = companyInfo[2];
                            objCompanyInfoResponse.CorpMasterExtId = companyInfo[3];
                            parentAccountList = proxy.RetrieveParentAccountList(companyInfo[2]);
                            if (parentAccountList != null && parentAccountList.msgCodeField.ToString2() == "0")
                            {
                                string vaidMarketCodes = (System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"] != null ? System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"].ToString2() : "");

                                if (string.IsNullOrEmpty(vaidMarketCodes) == true)
                                {
                                    objCompanyInfoResponse.ParentList = parentAccountList.parentAcctListField;
                                }
                                else
                                {
                                    objCompanyInfoResponse.ParentList = parentAccountList.parentAcctListField.Where(e => vaidMarketCodes.Split(',').Contains(e.mktCodeField) == true).ToList();
                                    if (objCompanyInfoResponse.ParentList.Any() == false)
                                    {
                                        objCompanyInfoResponse.ParentList = null;
                                        ModelState.AddModelError("Error", "No data found.");
                                    }
                                }
                                if (objCompanyInfoResponse.ParentList != null && objCompanyInfoResponse.ParentList.Count > 0)
                                {
                                    objCompanyInfoResponse.CompanyName = objCompanyInfoResponse.ParentList[0].companyNameField;
                                    objCompanyInfoResponse.MarketCode = objCompanyInfoResponse.ParentList[0].mktCodeField;
                                    objCompanyInfoResponse.CorpParentID = objCompanyInfoResponse.ParentList[0].fxAcctNoField;
                                    objCompanyInfoResponse.CorpParentExtId = objCompanyInfoResponse.ParentList[0].fxAcctExtIdField;
                                    if (!string.IsNullOrEmpty(objCompanyInfoResponse.MarketCode))
                                    {
                                        using (var configProxy = new ConfigServiceProxy())
                                        {
                                            foreach (var parent in parentAccountList.parentAcctListField)
                                            {
                                                DAL.Models.Market market = new Market();
                                                market.KenanCode = parent.mktCodeField;
                                                var markets = configProxy.MarketSearch(new MarketFind { Market = market });
                                                parent.noBillField = markets.Any() ? markets.ToList()[0].Name : string.Empty;
                                            }


                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error occured in contacting WM";
                                        if (parentAccountList != null)
                                        {
                                            if (parentAccountList.msgCodeField.ToString2() == "2")
                                            {
                                                errorMessage = "No data found";
                                            }
                                            else
                                            {
                                                errorMessage = parentAccountList.msgDescField.ToString2();
                                            }
                                        }
                                        ModelState.AddModelError("Error", errorMessage);
                                    }
                                }
                            }
                            break;
                    }
                }

                //Storing in session and will be used to determine the logged user is corporate user, will be reset in new search
                Session[SessionKey.CompanyInfoResponse.ToString2()] = objCompanyInfoResponse;

                ViewBag.SearchType = SearchType;
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(objCompanyInfoResponse);
        }

        [Authorize]
        [HttpPost]
        public ActionResult BRNSearchNew(FormCollection collection)
        {
            SubscriberICService.typeRetrieveMasterAcctListResponse masterAccountList = null;
            SubscriberICService.typeRetrieveParentAcctListResponse parentAccountList = null;
			BRNVm objCompanyInfoResponse = null;
            if (Session[SessionKey.CompanyInfoResponse.ToString2()] != null)
            {
                objCompanyInfoResponse = (Online.Registration.Web.ViewModels.BRNVm)Session[SessionKey.CompanyInfoResponse.ToString2()];
            }
            string SearchType = collection["SearchType"].ToString2();
            string SearchValue = collection["SearchValue"].ToString2().Trim();
            string[] companyInfo = collection["hdnSelectCompanyDtls"].ToString2().Split(',');
            string submitType = collection["SumbitType"].ToString2();
            corporateSearchType = Util.getSearchCorporateType(SearchType);
            if (collection["hdnRegType"].ToString2().Length > 0)
            {
                Session["RegMobileReg_Type"] = collection["hdnRegType"];
            }
            if (submitType == "Search")
            {
                objCompanyInfoResponse = null;
            }
            try
            {
                if (objCompanyInfoResponse != null && (objCompanyInfoResponse.SearchType.ToString2() == "Parent" || Util.getSearchCorporateType(objCompanyInfoResponse.SearchType.ToString2()) == CorpType.PARENTACCT))
                {

                    if (companyInfo != null && companyInfo.Length == 4)
                    {
                        objCompanyInfoResponse.CompanyName = companyInfo[0];
                        objCompanyInfoResponse.MarketCode = companyInfo[1];
                        objCompanyInfoResponse.CorpParentID = companyInfo[2];
                        objCompanyInfoResponse.CorpParentExtId = companyInfo[3];
                        Session[SessionKey.isFromBRNSearch.ToString()] = true;
                        Session[SessionKey.CompanyInfoResponse.ToString2()] = objCompanyInfoResponse;
                        MobileRegType objRegType = (MobileRegType)Session["RegMobileReg_Type"].ToInt();
                        switch (objRegType)
                        {
                            case MobileRegType.PlanOnly:
                                return RedirectToAction("SelectPlanNew", "Registration", new { type = (int)MobileRegType.PlanOnly });
                                break;
                            case MobileRegType.DevicePlan:
                                // change for drop 4
                                return RedirectToAction("SelectDevicePlan", "Registration", new { type = (int)MobileRegType.DevicePlan });
                                break;
                            default:
                                return RedirectToAction("EnterDetailsNew", "MNP");
                                break;
                        }

                    }

                }
                else
                {
                    switch (corporateSearchType)
                    {
                        case CorpType.MASTERACCT:
                            using (var proxy = new retrieveServiceInfoProxy())
                            {
                                string vaidMarketCodes = (System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"] != null ? System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"].ToString2() : "");
                                string[] listValidMktCodes = vaidMarketCodes.Split(',');
                                if (objCompanyInfoResponse == null)
                                {
                                    CustomizedCustomer subscriberDetails = proxy.retrieveSubscriberDetlsByAcctNo(SearchValue, Request.Cookies["CookieUser"].Value, isSuperAdmin);
                                    if (subscriberDetails != null && subscriberDetails.MsgCode == "0")
                                    {
                                        AccountDetails accountDetails = proxy.RetrieveAccountDetails(SearchValue);
                                        if (accountDetails != null && !string.IsNullOrEmpty(accountDetails.MktCode) && listValidMktCodes.Contains(accountDetails.MktCode) && string.IsNullOrEmpty(accountDetails.ParentId))
                                        {
                                            objCompanyInfoResponse = new ViewModels.BRNVm();
                                            masterAcctList MasterList = new masterAcctList();
                                            MasterList.fxAcctExtIdField = SearchValue;
                                            MasterList.companyNameField = subscriberDetails.CustomerName;
                                            MasterList.brnField = subscriberDetails.Brn;
                                            MasterList.fxAcctNoField = subscriberDetails.CorpMasterExtId;
                                            objCompanyInfoResponse.MasterList.Add(MasterList);
                                            objCompanyInfoResponse.SearchType = "Master";
                                            objCompanyInfoResponse.SearchValue = SearchValue;
											objCompanyInfoResponse.InputSearchType = !string.IsNullOrEmpty(SearchType) ? SearchType : string.Empty;
											objCompanyInfoResponse.InputSearchValue = !string.IsNullOrEmpty(SearchValue) ? SearchValue : string.Empty;
                                        }
                                        else
                                        {
                                            string errorMessage = string.Format("Invalid Master Account Number due to wrong Market Code or Parent ID exist.", SearchValue);
                                            ModelState.AddModelError("Error", errorMessage);
                                        }
                                    }
                                    else if (subscriberDetails == null)
                                    {
                                        ModelState.AddModelError("Error", "No data found.");
                                    }
                                    else if (subscriberDetails != null && subscriberDetails.MsgCode != "0")
                                    {
                                        ModelState.AddModelError("Error", "No data found.");

                                    }
                                }
                                else
                                {
                                    string masteAcctInt = objCompanyInfoResponse.SearchValue.Substring(0, SearchValue.Length - 1);
                                    parentAccountList = proxy.RetrieveParentAccountList(masteAcctInt);

									objCompanyInfoResponse.InputSearchType = !string.IsNullOrEmpty(SearchType) ? SearchType : string.Empty;
									objCompanyInfoResponse.InputSearchValue = !string.IsNullOrEmpty(SearchValue) ? SearchValue : string.Empty;

                                    if (parentAccountList != null && parentAccountList.msgCodeField.ToString2() == "0")
                                    {

                                        if (string.IsNullOrEmpty(vaidMarketCodes) == true)
                                        {
                                            objCompanyInfoResponse.ParentList = parentAccountList.parentAcctListField;
                                        }
                                        else
                                        {
                                            objCompanyInfoResponse.ParentList = parentAccountList.parentAcctListField.Where(e => vaidMarketCodes.Split(',').Contains(e.mktCodeField) == true).ToList();
                                            if (objCompanyInfoResponse.ParentList.Any() == false)
                                            {
                                                objCompanyInfoResponse.ParentList = null;
                                                ModelState.AddModelError("Error", "No data found.");
                                            }
                                        }
                                        if (objCompanyInfoResponse.ParentList != null && objCompanyInfoResponse.ParentList.Any())
                                        {
											objCompanyInfoResponse.InputSearchType = !string.IsNullOrEmpty(SearchType) ? SearchType : string.Empty;
											objCompanyInfoResponse.InputSearchValue = !string.IsNullOrEmpty(SearchValue) ? SearchValue : string.Empty;
                                            objCompanyInfoResponse.SearchType = "Parent";
                                            objCompanyInfoResponse.CorpMasterID = companyInfo[2];
                                            objCompanyInfoResponse.CorpMasterExtId = companyInfo[3];
                                            objCompanyInfoResponse.CompanyName = objCompanyInfoResponse.ParentList[0].companyNameField;
                                            objCompanyInfoResponse.MarketCode = objCompanyInfoResponse.ParentList[0].mktCodeField;
                                            objCompanyInfoResponse.CorpParentID = objCompanyInfoResponse.ParentList[0].fxAcctNoField;
                                            objCompanyInfoResponse.CorpParentExtId = objCompanyInfoResponse.ParentList[0].fxAcctExtIdField;
                                            if (!string.IsNullOrEmpty(objCompanyInfoResponse.MarketCode))
                                            {
                                                using (var configProxy = new ConfigServiceProxy())
                                                {
                                                    foreach (var parent in parentAccountList.parentAcctListField)
                                                    {
                                                        DAL.Models.Market market = new Market();
                                                        market.KenanCode = parent.mktCodeField;
                                                        var markets = configProxy.MarketSearch(new MarketFind { Market = market });
                                                        parent.noBillField = markets.Any() ? markets.ToList()[0].Name : string.Empty;
                                                    }


                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error occured in contacting WM.";
                                        if (parentAccountList != null)
                                        {
                                            if (parentAccountList.msgCodeField.ToString2() == "2")
                                            {
                                                errorMessage = "No data found.";
                                            }
                                            else
                                            {
                                                errorMessage = parentAccountList.msgDescField.ToString2();
                                            }
                                        }
                                        ModelState.AddModelError("Error", errorMessage);
                                    }
                                }
                            }
                            break;
                        case CorpType.PARENTACCT:
                            using (var proxy = new retrieveServiceInfoProxy())
                            {
                                string vaidMarketCodes = (System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"] != null ? System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"].ToString2() : "");
                                string[] listValidMktCodes = vaidMarketCodes.Split(',');
                                if (objCompanyInfoResponse == null)
                                {
                                    CustomizedCustomer subscriberDetails = proxy.retrieveSubscriberDetlsByAcctNo(SearchValue, Request.Cookies["CookieUser"].Value, isSuperAdmin);
                                    AccountDetails accountDetails = proxy.RetrieveAccountDetails(SearchValue);
                                    if (subscriberDetails != null && subscriberDetails.MsgCode == "0" && accountDetails != null)
                                    {
                                        if (accountDetails != null && !string.IsNullOrEmpty(accountDetails.MktCode) && listValidMktCodes.Contains(accountDetails.MktCode) && !string.IsNullOrEmpty(accountDetails.ParentId))
                                        {
                                            objCompanyInfoResponse = new ViewModels.BRNVm();
                                            parentAcctList parentList = new parentAcctList();
                                            parentList.fxAcctExtIdField = SearchValue;
                                            parentList.companyNameField = subscriberDetails.CustomerName.ToString2();
                                            parentList.fxAcctNoField = subscriberDetails.CorpMasterExtId.ToString2();
                                            parentList.billAddr1Field = subscriberDetails.Address1.ToString2();
                                            parentList.billAddr2Field = subscriberDetails.Address2.ToString2();
                                            parentList.fxAcctNoField = accountDetails.FxAcctNo.ToString2();
                                            parentList.mktCodeField = accountDetails.MktCode.ToString2();
                                            objCompanyInfoResponse.ParentList.Add(parentList);
                                            objCompanyInfoResponse.SearchType = SearchType;
                                            objCompanyInfoResponse.SearchValue = SearchValue;
											objCompanyInfoResponse.InputSearchType = !string.IsNullOrEmpty(SearchType) ? SearchType : string.Empty;
											objCompanyInfoResponse.InputSearchValue = !string.IsNullOrEmpty(SearchValue) ? SearchValue : string.Empty;

                                            if (string.IsNullOrEmpty(vaidMarketCodes) == true)
                                            {
                                                objCompanyInfoResponse.ParentList.Add(parentList);
                                            }
                                            objCompanyInfoResponse.CorpMasterID = accountDetails.HierarchyId.ToString2();
                                            objCompanyInfoResponse.CorpMasterExtId = accountDetails.HierarchyId.ToString2();
                                            objCompanyInfoResponse.CompanyName = subscriberDetails.CustomerName.ToString2();
                                            objCompanyInfoResponse.MarketCode = accountDetails.MktCode.ToString2();
                                            objCompanyInfoResponse.CorpParentID = accountDetails.FxAcctNo.ToString2();
                                            objCompanyInfoResponse.CorpParentExtId = accountDetails.FxAcctExtId.ToString2();
                                            if (!string.IsNullOrEmpty(objCompanyInfoResponse.MarketCode))
                                            {
                                                using (var configProxy = new ConfigServiceProxy())
                                                {
                                                    DAL.Models.Market market = new Market();
                                                    market.KenanCode = accountDetails.MktCode;
                                                    var markets = configProxy.MarketSearch(new MarketFind { Market = market });
                                                    parentList.noBillField = markets.Any() ? markets.ToList()[0].Name : string.Empty;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string errorMessage = string.Format("Invalid Parent Account Number due to wrong Market Code or no Parent ID exist.", SearchValue);
                                            ModelState.AddModelError("Error", errorMessage);
                                        }
                                    }
                                    else
                                    {

                                        ModelState.AddModelError("Error", "No data found.");
                                    }
                                }
                            }
                            break;
                        default:
                            using (var proxy = new retrieveServiceInfoProxy())
                            {
                                if (objCompanyInfoResponse == null)
                                {
                                    masterAccountList = proxy.RetrieveMasterAccountList(SearchType, SearchValue);
                                    objCompanyInfoResponse = new ViewModels.BRNVm();
                                    objCompanyInfoResponse.SearchType = SearchType;
                                    objCompanyInfoResponse.SearchValue = SearchValue;
									objCompanyInfoResponse.InputSearchType = !string.IsNullOrEmpty(SearchType) ? SearchType : string.Empty;
									objCompanyInfoResponse.InputSearchValue = !string.IsNullOrEmpty(SearchValue) ? SearchValue : string.Empty;
                                    if (masterAccountList != null && masterAccountList.msgCodeField.ToString2() == "0")
                                    {
                                        objCompanyInfoResponse.MasterList = masterAccountList.masterAcctListField;
                                        objCompanyInfoResponse.SearchType = "Master";

                                    }
                                    else
                                    {
                                        string errorMessage = "Error occured in contacting WM.";
                                        if (masterAccountList != null)
                                        {
                                            if (masterAccountList.msgCodeField.ToString2() == "2")
                                            {
                                                errorMessage = "No data found.";
                                            }
                                            else
                                            {
                                                errorMessage = masterAccountList.msgDescField.ToString2();
                                            }
                                        }
                                        ModelState.AddModelError("Error", errorMessage);
                                    }
                                }
                                else
                                {
									objCompanyInfoResponse.InputSearchType = !string.IsNullOrEmpty(SearchType) ? SearchType : string.Empty;
									objCompanyInfoResponse.InputSearchValue = !string.IsNullOrEmpty(SearchValue) ? SearchValue : string.Empty;
                                    objCompanyInfoResponse.SearchType = "Parent";
                                    objCompanyInfoResponse.CompanyName = companyInfo[0];
                                    objCompanyInfoResponse.BRN = companyInfo[1];
                                    objCompanyInfoResponse.CorpMasterID = companyInfo[2];
                                    objCompanyInfoResponse.CorpMasterExtId = companyInfo[3];
                                    parentAccountList = proxy.RetrieveParentAccountList(companyInfo[2]);
                                    if (parentAccountList != null && parentAccountList.msgCodeField.ToString2() == "0")
                                    {
                                        string vaidMarketCodes = (System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"] != null ? System.Configuration.ConfigurationManager.AppSettings["ValidMarketCodes"].ToString2() : "");

                                        if (string.IsNullOrEmpty(vaidMarketCodes) == true)
                                        {
                                            objCompanyInfoResponse.ParentList = parentAccountList.parentAcctListField;
                                        }
                                        else
                                        {
                                            objCompanyInfoResponse.ParentList = parentAccountList.parentAcctListField.Where(e => vaidMarketCodes.Split(',').Contains(e.mktCodeField) == true).ToList();
                                            if (objCompanyInfoResponse.ParentList.Any() == false)
                                            {
                                                objCompanyInfoResponse.ParentList = null;
                                                ModelState.AddModelError("Error", "No data found.");
                                            }
                                        }
                                        if (objCompanyInfoResponse.ParentList != null && objCompanyInfoResponse.ParentList.Count > 0)
                                        {
                                            objCompanyInfoResponse.CompanyName = objCompanyInfoResponse.ParentList[0].companyNameField;
                                            objCompanyInfoResponse.MarketCode = objCompanyInfoResponse.ParentList[0].mktCodeField;
                                            objCompanyInfoResponse.CorpParentID = objCompanyInfoResponse.ParentList[0].fxAcctNoField;
                                            objCompanyInfoResponse.CorpParentExtId = objCompanyInfoResponse.ParentList[0].fxAcctExtIdField;
                                            if (!string.IsNullOrEmpty(objCompanyInfoResponse.MarketCode))
                                            {
                                                using (var configProxy = new ConfigServiceProxy())
                                                {
                                                    foreach (var parent in parentAccountList.parentAcctListField)
                                                    {
                                                        DAL.Models.Market market = new Market();
                                                        market.KenanCode = parent.mktCodeField;
                                                        var markets = configProxy.MarketSearch(new MarketFind { Market = market });
                                                        parent.noBillField = markets.Any() ? markets.ToList()[0].Name : string.Empty;
                                                    }


                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string errorMessage = "Error occured in contacting WM";
                                        if (parentAccountList != null)
                                        {
                                            if (parentAccountList.msgCodeField.ToString2() == "2")
                                            {
                                                errorMessage = "No data found";
                                            }
                                            else
                                            {
                                                errorMessage = parentAccountList.msgDescField.ToString2();
                                            }
                                        }
                                        ModelState.AddModelError("Error", errorMessage);
                                    }
                                }
                            }
                            break;
                    }
                }

                //Storing in session and will be used to determine the logged user is corporate user, will be reset in new search
                Session[SessionKey.CompanyInfoResponse.ToString2()] = objCompanyInfoResponse;

                ViewBag.SearchType = SearchType;
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.Message);
            }

            return View(objCompanyInfoResponse);
        }


        public static AMRSvc.RetrieveMyKadInfoRp FromXmlString(string xmlString)
        {
            try
            {
                var reader = new System.IO.StringReader(xmlString);
                xmlString = xmlString.Replace("http://www.w3.org/2001/XMLSchema-instance", "http://schemas.datacontract.org/2004/07/Online.Registration.IntegrationService.Models");
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(AMRSvc.RetrieveMyKadInfoRp), "http://schemas.datacontract.org/2004/07/Online.Registration.IntegrationService.Models");
                var instance = (AMRSvc.RetrieveMyKadInfoRp)serializer.Deserialize(reader);
                return instance;
            }
            catch (Exception ex)
            {
                return new AMRSvc.RetrieveMyKadInfoRp();
            }
        }

        public ActionResult ManualCacheUpdate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ManualCacheUpdate(FormCollection collection, CacheModels CacheModels)
        {
            Online.Registration.Web.ContentRepository.MasterDataCache.Instance.InitializeCacheManual(CacheModels);
            return View();
        }
        #endregion

        #region Ajax Json Call

        /// <summary>
        /// Gets the supplementary line via Ajax Call
        /// </summary>
        /// <Author>
        /// Sutan Dan
        /// </Author>
        /// <param name="id">The id.</param>
        /// <returns>TypeOf(Json)</returns>
        [HttpPost]
        public ActionResult GetSupplementaryLine(string id = null)
        {
            StringBuilder BuildResponse = new StringBuilder("");
            ///GET THE retrieveAcctListByICResponse TYPE OF OBJECT FROM SESSION IF PPID="E" FOLLOWING SHOULD BE AVAILABLE    
            retrieveAcctListByICResponse AcctListByICResponse = null;

            if (!ReferenceEquals(Session[SessionKey.PPIDInfo.ToString()], null))
            {
                AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()];

                ///Check  Session[SessionKey.PPID.ToString()] IF IT CONTAINS 'E' IF EXISTING SET IsExistingUser TO TRUE AGAIN CHECK IF AcctListByICResponse IS NULL THEN SET IT TO FALSE AS NO DATA TO DISPLAY
                bool IsExistingUser = false;
                if (!ReferenceEquals(Session[SessionKey.PPID.ToString()], null) && !string.IsNullOrWhiteSpace(Convert.ToString(Session[SessionKey.PPID.ToString()])))
                {
                    IsExistingUser = "E".Equals(Convert.ToString(Session[SessionKey.PPID.ToString()])) ? true : false;
                    IsExistingUser = !ReferenceEquals(Session[SessionKey.PPIDInfo.ToString()], null) ? true : false;
                }
                string msisdn = string.Empty;

                if (!ReferenceEquals(id, null))
                {
                    msisdn = id;

                    if (IsExistingUser)
                    {
                        int count = 0;
                        if (!ReferenceEquals(AcctListByICResponse, null) && !ReferenceEquals(AcctListByICResponse.itemList.Where(c => c.PrinSuppResponse != null).ToList(), null))
                        {
                            ///GET THE MATCHING ACCOUNTS WITH MSISDN AND DISPLAY CORRESPONDING SUPPLEMENTARY LIST FROM ATTACHED OBJECTS
                            foreach (var v in AcctListByICResponse.itemList.Where(c => c.ExternalId == msisdn).ToList().Select(i => i.PrinSuppResponse.itemList.ToList()))
                            {
                                BuildResponse.AppendLine("<div id=\"Grid\" class=\"t-widget t-grid\">");
                                BuildResponse.AppendLine("<table cellspacing=\"0\">");
                                BuildResponse.AppendLine("<colgroup>");
                                BuildResponse.AppendLine("<col><col><col><col><col><col><col>");
                                BuildResponse.AppendLine("</colgroup>");
                                BuildResponse.AppendLine("<thead class=\"t-grid-header\">");
                                BuildResponse.AppendLine("<tr>");

                                BuildResponse.AppendLine("<th scope=\"col\" class=\"t-header\">");
                                BuildResponse.AppendLine("<span class=\"t-link\">A/C Number</span>");
                                BuildResponse.AppendLine("</th>");
                                BuildResponse.AppendLine("<th scope=\"col\" class=\"t-header\">");
                                BuildResponse.AppendLine("<span class=\"t-link\">Customer Name</span>");
                                BuildResponse.AppendLine("</th>");
                                BuildResponse.AppendLine("<th scope=\"col\" class=\"t-header\">");
                                BuildResponse.AppendLine("<span class=\"t-link\">Msisdn</span>");
                                BuildResponse.AppendLine("</th>");
                                BuildResponse.AppendLine("<th scope=\"col\" class=\"t-header\">");
                                BuildResponse.AppendLine("<span class=\"t-link\">Print Supp ind</span>");
                                BuildResponse.AppendLine("</th>");
                                BuildResponse.AppendLine("<th scope=\"col\" class=\"t-header\">");
                                BuildResponse.AppendLine("<span class=\"t-link\">Subscriber No</span>");
                                BuildResponse.AppendLine("</th>");
                                BuildResponse.AppendLine("<th scope=\"col\" class=\"t-header\">");
                                BuildResponse.AppendLine("<span class=\"t-link\">Subscriber No of resets</span>");
                                BuildResponse.AppendLine("</th>");
                                BuildResponse.AppendLine("<th scope=\"col\" class=\"t-header\">");
                                BuildResponse.AppendLine("<span class=\"t-link\">Subscriber Status</span>");
                                BuildResponse.AppendLine("</th>");

                                BuildResponse.AppendLine("</tr>");
                                BuildResponse.AppendLine("</thead>");
                                BuildResponse.AppendLine("<tbody>");
                                for (int i = 0; i <= v.Count - 1; i++)
                                {
                                    BuildResponse.AppendLine("<tr class=\"\">");
                                    BuildResponse.AppendLine(@"<td>" + @v[i].acct_noField + "</td>");
                                    BuildResponse.AppendLine(@"<td>" + @v[i].cust_nmField + "</td>");
                                    BuildResponse.AppendLine(@"<td>" + @v[i].msisdnField + "</td>");
                                    BuildResponse.AppendLine(@"<td>" + @v[i].prinsupp_indField + "</td>");
                                    BuildResponse.AppendLine(@"<td>" + @v[i].subscr_noField + "</td>");
                                    BuildResponse.AppendLine(@"<td>" + @v[i].subscr_no_resetsField + "</td>");
                                    BuildResponse.AppendLine(@"<td>" + @v[i].subscr_statusField + "</td>");
                                    BuildResponse.AppendLine("</tr>");
                                    count++;
                                }
                                BuildResponse.AppendLine("</tbody>");
                                BuildResponse.AppendLine("</table>");
                                BuildResponse.AppendLine("</div>");
                            }

                            if (count == 0)
                            {
                                BuildResponse.AppendLine("<tr class=\"\">");
                                BuildResponse.Append(@"<td>Only Principal Account Exists <br />No Supplementary Line Available for this User</td>");
                                BuildResponse.AppendLine("</tr>");
                            }
                        }
                    }
                    else
                    {
                        BuildResponse.AppendLine("<tr class=\"\">");
                        BuildResponse.Append(@"<pre> No Supplementary Lines Exists</pre>");
                        BuildResponse.AppendLine("</tr>");
                    }
                }
                else
                {
                    BuildResponse.AppendLine("<tr class=\"\">");
                    BuildResponse.Append(@"<td> No Supplementary Lines Exists</td>");
                    BuildResponse.AppendLine("</tr>");
                }
            }

            var jsonData = new { supplementaryList = BuildResponse.ToString() };

            return Json(jsonData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SetVariable(string value)
        {
            Session["SelectedTab"] = value;
            return this.Json(new { success = true });
        }

        /// <summary>
        /// SaveMenuOption
        /// </summary>
        /// <param name="spendLimit">Selected Menu Value</param>
        /// <returns>string</returns>
        [HttpPost]
        public JsonResult SaveMenuOption(string curMenu)
        {
            bool isSuccess = true;
            BRECheckStatus objBRECheckStatus = new BRECheckStatus();
            objBRECheckStatus.isMenuSuccess = true;
            try
            {
                ClearSessions();
				Session["BusinessCheck"] = null;
				Session["BreStatusDetails"] = null;
                Session[SessionKey.FailedAcctIds.ToString()] = null;
				Session["BREFailedRules"] = null;
                if (Session["BusinessCheck"] == null && Session["PPID"].ToString2() != string.Empty)
                {
                    string validationCheckType = (Session[SessionKey.PPID.ToString()].ToString() == "E" ? "E" : "N");
                    if (Session[SessionKey.PPIDInfo.ToString()] != null)
                    {
                        var ppIdInfo = (retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()];
                        var ppIdInfoItemList = ppIdInfo.itemList.Where(e => e.IsActive == true);
                        if (ppIdInfoItemList != null)
                        {
                            ppIdInfo.itemList = ppIdInfoItemList.ToList();

                            Session[SessionKey.PPIDInfo.ToString()] = ppIdInfo;
                        }
                        var SR_PPIDInfo = (retrieveAcctListByICResponse)Session[SessionKey.SR_PPID.ToString()];
                        var SR_PPIDInfooItemList = SR_PPIDInfo.itemList.Where(e => e.IsActive == true);
                        SR_PPIDInfo.itemList = ppIdInfoItemList.ToList();
                        Session[SessionKey.SR_PPID.ToString()] = SR_PPIDInfo;
                    }

                    WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), validationCheckType);
                    List<BreStatusDetails> objBreStatusDetails = new List<BreStatusDetails>();
                    if (!ReferenceEquals(Session["BreStatusDetails"], null))
                    {
                        objBreStatusDetails = (List<BreStatusDetails>)Session["BreStatusDetails"];
                    }

                    List<BreStatusDetails> itemList = new List<BreStatusDetails>();
                    if (validationCheckType != "N")
                    {
                        if (Session["IsDealer"].ToBool() && objBreStatusDetails.Count(a => ((a.Code == "AgeCheck" || a.Code == "OutstandingCreditCheck") && a.KenanStatus == false)) > 0)
                        {
                            itemList = objBreStatusDetails.Where(a => ((a.Code == "AgeCheck" || a.Code == "OutstandingCreditCheck") && a.KenanStatus == false)).ToList();
                            Session["isHardStop"] = true;
                        }
                        else
                        {
                            Session["isHardStop"] = objBreStatusDetails.Where(a => (a.Action == "HardStop" && a.KenanStatus == false)).Any();
                            itemList = objBreStatusDetails.Where(a => (a.Action == "HardStop" && a.KenanStatus == false)).ToList();
                        }

                        if (itemList.Count > 0)
                        {
                            foreach (var item in itemList)
                            {
                                if (Session["BREFailedRules"] != null)
                                {
                                    Session["BREFailedRules"] = Session["BREFailedRules"] + "," + item.Code;
                                }
                                else
                                {
                                    Session["BREFailedRules"] = item.Code;
                                }
                            }
                        }

                    }
                    Session["BusinessCheck"] = 1;
                }
                objBRECheckStatus.BREStatus = Session["isHardStop"].ToBool();
                Session["SaveMenuOption"] = curMenu;

                Session[SessionKey.TransStartTime.ToString()] = DateTime.Now;

                if (curMenu != "ancMenu_SMEI")
                {
                    Session[SessionKey.CompanyInfoResponse.ToString()] = null;
                    Session[SessionKey.isFromBRNSearch.ToString()] = null;
                    Session[SessionKey.AccountCategory.ToString()] = null;
                    Session[SessionKey.MarketCode.ToString()] = null;
                }

                Session["DeviceSelected"] = 0;
            }
            catch (Exception ex)
            {
                Util.LogException(ex);
                isSuccess = false;
                objBRECheckStatus.isMenuSuccess = false;
            }
            //   return new JsonResult() { Data = isSuccess };
            return new JsonResult() { Data = new { BREStatus = objBRECheckStatus.BREStatus,BREFailedRules = Session["BREFailedRules"] != null ?Session["BREFailedRules"].ToString() : "" , isMenuSuccess = objBRECheckStatus.isMenuSuccess } };
        }


        #region moved to Util.IsAgeBelowLevelForNewIC - Dec Drop
        /// <summary>
        /// AgeCheck for New Customer NewIC
        /// </summary>
        /// <param name="spendLimit">Age Check for NewIC</param>
        /// <returns>string</returns>
        /*
        public Boolean IsAgeBelowLevelForNewIC(bool ThirdParty = false)
        {
            var SR_PPIDInfo = (retrieveAcctListByICResponse)Session[SessionKey.SR_PPID.ToString()];
            var idCardNo = Session[SessionKey.IDCardNo.ToString()].ToString2();
            if ((Session[SessionKey.IDCardTypeID.ToString()].ToString() == "1") && (SR_PPIDInfo == null) && (idCardNo != null))
            {
                int customerYear = idCardNo.Substring(0, 2).ToInt();
				int todayYears = DateTime.Now.ToString("yy").ToInt();
				customerYear = customerYear <= todayYears ? (customerYear < 10 ? ("200" + customerYear).ToInt() : ("20" + customerYear).ToInt()) : ("19" + customerYear).ToInt();
                int customerMonth = idCardNo.Substring(2, 2).ToInt();
                int customerDay = idCardNo.Substring(4, 2).ToInt();
                // get the difference in years
                int years = DateTime.Now.Year - customerYear;
                // subtract another year if we're before the
                // birth day in the current year
                if (DateTime.Now.Month < customerMonth ||
                    (DateTime.Now.Month == customerMonth &&
                    DateTime.Now.Day < customerDay))
                {
                    years--;
                }
                if (years < 18)
                {
                    return true;
                }
            }
            return false;

        }
        */
        #endregion

		[HttpPost]
		public JsonResult setSession(string id) {
			var result = new Dictionary<string, string>();
			result.Add("spv_redirectURL", Session["spv_redirectURL"].ToString2());
			result.Add("spv_id", Session["spv_id"].ToString2());
			result.Add("spv_msisdn", Session["spv_msisdn"].ToString2());
			return Json(result);
		}

        /// <summary>
        /// CheckBRE_HS
        /// </summary>
        /// <param name="spendLimit">Selected Menu Value</param>
        /// <returns>string</returns>
        [HttpPost]
		public JsonResult CheckBRE_HS(string button, string id, string msisdn, string redirectURL)
        {
            #region // Old Definitions
            //BRECheckStatus objBRECheckStatus = new BRECheckStatus();
            //Dictionary<string, string> approvalAndJustificationDict = new Dictionary<string, string>();
            //Session["approvalAndJustificationDict"] = approvalAndJustificationDict;
            #endregion

            WebHelper.Instance.ClearSessions(); //RST - Smart Discount Issue 18Mar15
            Session[SessionKey.TradeUpAmount.ToString()] = 0; //RST GST
            Session[SessionKey.isMNPNewlineWithSupplementary.ToString()] = null;//12052015 - Anthony - Clear the Session for MNP Newline with Supplementary line

            Session["spv_redirectURL"] = redirectURL;
            Session["spv_id"] = id;
            Session["spv_msisdn"] = msisdn;

            var BreList = MasterDataCache.Instance.GetBreCheckTreatment;
            List<BreStatusDetails> breStatusLists = (List<BreStatusDetails>)HttpContext.Session["BreStatusDetails"];
            string flow = WebHelper.Instance.IdentifyOrderFlow(true, button);
            string UserType = Session["IsDealer"].ToBool() == true ? "Dealer" : "MC";

            var result = new Dictionary<string, string>();
            string BreTreatment = string.Empty;
            string HSList = string.Empty;
			string ApprovalList = string.Empty;
            string JustificationList = string.Empty;
            string CheckBREHS = string.Empty;
            string FraudTreatment = string.Empty;
            string isFraud = string.Empty;

            try
            {
                if (!ReferenceEquals(breStatusLists, null))
                {
                    foreach (var bre in breStatusLists)
                    {
                        if (bre.KenanStatus == false)
                        {
                            #region List of Required BRE Checks
                            BreTreatment = string.Empty;
                            BreTreatment = bre.Code == "DDMFCheck" ? BreList.Where(x => x.UserType.Contains(UserType) && x.Flow.Equals(flow) && x.BreCheck.Contains("DDMF Check")).Select(y => y.BreTreatment).SingleOrDefault().ToString() : BreTreatment;
                            BreTreatment = bre.Code == "AddressCheck" ? BreList.Where(x => x.UserType.Contains(UserType) && x.Flow.Equals(flow) && x.BreCheck.Contains("Address Check")).Select(y => y.BreTreatment).SingleOrDefault().ToString() : BreTreatment;
                            BreTreatment = bre.Code == "AgeCheck" ? BreList.Where(x => x.UserType.Contains(UserType) && x.Flow.Equals(flow) && x.BreCheck.Contains(bre.Code)).Select(y => y.BreTreatment).SingleOrDefault().ToString() : BreTreatment;
                            BreTreatment = bre.Code == "OutstandingCheck" ? BreList.Where(x => x.UserType.Contains(UserType) && x.Flow.Equals(flow) && x.BreCheck.Contains("Outstanding Credit Check")).Select(y => y.BreTreatment).SingleOrDefault().ToString() : BreTreatment;
                            //if (button != "CRP")
                                BreTreatment = bre.Code == "ContractCheck" ? BreList.Where(x => x.UserType.Contains(UserType) && x.Flow.Equals(flow) && x.BreCheck.Contains("Contract Check")).Select(y => y.BreTreatment).SingleOrDefault().ToString() : BreTreatment;
                            BreTreatment = bre.Code == "Biometric" ? BreList.Where(x => x.UserType.Contains(UserType) && x.Flow.Equals(flow) && x.BreCheck.Contains("Biometric")).Select(y => y.BreTreatment).SingleOrDefault().ToString() : BreTreatment;
                            BreTreatment = bre.Code == "TotalLineCheck" ? BreList.Where(x => x.UserType.Contains(UserType) && x.Flow.Equals(flow) && x.BreCheck.Contains("Total Line Check")).Select(y => y.BreTreatment).SingleOrDefault().ToString() : BreTreatment;
                            #endregion

                            if (BreTreatment == "HS")
                            {
                                HSList += string.IsNullOrEmpty(HSList) ? bre.Code : ", " + bre.Code;
                            }
                            else if (BreTreatment == "A")
                            {
                                //if (!approvalAndJustificationDict.ContainsKey(bre.Code)) approvalAndJustificationDict.Add(bre.Code, "");
                                ApprovalList += string.IsNullOrEmpty(ApprovalList) ? bre.Code : ", " + bre.Code;
                            }
                            else if (BreTreatment == "J")
                            {
                                JustificationList += string.IsNullOrEmpty(JustificationList) ? bre.Code : ", " + bre.Code;
                            }
                        }
                    }

                    // #1331 - w.loon - To add supervisor approval to bypass of BRE Contract Check
                    #region Write Off Check
                    if (Session["Fraudmessage"].ToString2() == Constants.WRITTEN_OFF)
                    {
                        BreTreatment = BreList.Where(x => x.UserType.Contains(UserType) && x.Flow.Equals(flow) && x.BreCheck.Contains("Writeoff Check")).Select(y => y.BreTreatment).SingleOrDefault().ToString2();

                        if (BreTreatment == "HS")
                        {
                            HSList += string.IsNullOrEmpty(HSList) ? "Writeoff Check" : ", " + "Writeoff Check";
                        }
                        else if (BreTreatment == "A")
                        {
                            ApprovalList += string.IsNullOrEmpty(ApprovalList) ? "Writeoff Check" : ", " + "Writeoff Check";
                        }
                        else if (BreTreatment == "J")
                        {
                            JustificationList += string.IsNullOrEmpty(JustificationList) ? "Writeoff Check" : ", " + "Writeoff Check";
                        }
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(HSList) || !string.IsNullOrEmpty(ApprovalList) || !string.IsNullOrEmpty(JustificationList))
                        CheckBREHS = "True";
                }

                // Fraud Check
                FraudTreatment = BreList.Where(x => x.UserType.Contains(UserType) && x.Flow.Equals(flow) && x.BreCheck.Contains("Fraud")).Select(y => y.BreTreatment).SingleOrDefault().ToString2();
                isFraud = Session["FraudMessage"].ToString2().ToLower().Contains("fraud") ? "True" : "False";

                //Session["approvalAndJustificationDict"] = approvalAndJustificationDict;
                result.Add("ApprovalList", ApprovalList);
                result.Add("HSList", HSList.ToString());
                result.Add("JustificationList", JustificationList);
                result.Add("CheckBREHS", CheckBREHS.ToString());
                result.Add("FraudTreatment", FraudTreatment.ToString());
                result.Add("isFraud", isFraud.ToString());
            }
            catch (Exception ex)
            {
                Util.LogException(ex);
            }

            return Json(result);
        }

        /// <summary>
        /// SaveMenuOption
        /// </summary>
        /// <param name="spendLimit">Selected Menu Value</param>
        /// <returns>string</returns>
        [HttpPost]
        public string SetParentInfo(string ParentInfo)
        {
            Online.Registration.Web.ViewModels.BRNVm objCompanyInfoResponse = null;
            string[] companyInfo = ParentInfo.ToString2().Split(',');
            if (Session[SessionKey.CompanyInfoResponse.ToString2()] != null)
            {
                objCompanyInfoResponse = (Online.Registration.Web.ViewModels.BRNVm)Session[SessionKey.CompanyInfoResponse.ToString2()];
            }
            if (objCompanyInfoResponse != null && objCompanyInfoResponse.SearchType.ToString2() == "Parent")
            {

                if (companyInfo != null && companyInfo.Length == 4)
                {
                    objCompanyInfoResponse.CompanyName = companyInfo[0];
                    objCompanyInfoResponse.MarketCode = companyInfo[1];
                    objCompanyInfoResponse.CorpParentID = companyInfo[2];
                    objCompanyInfoResponse.CorpParentExtId = companyInfo[3];
                }

            }
            Session[SessionKey.CompanyInfoResponse.ToString2()] = objCompanyInfoResponse;
            return string.Empty;
        }

        private void ClearSessions()
        {
            //Clearing selected plan 
            Session[SessionKey.ContractDetails.ToString()] = null;
            Session[SessionKey.WaiverRules.ToString()] = null;
            Session["SelectedPlanID"] = 0;
            Session["RegMobileReg_MalAdvDeposit_Seco"] = null;
            Session["RegMobileReg_OtherAdvDeposit_Seco"] = null;
            Session["RegMobileReg_MalyDevAdv_Seco"] = null;
            Session["RegMobileReg_MalayPlanAdv_Seco"] = null;
            Session["RegMobileReg_MalayDevDeposit_Seco"] = null;
            Session["RegMobileReg_MalyPlanDeposit_Seco"] = null;
            Session["RegMobileReg_OthDevAdv_Seco"] = null;
            Session["RegMobileReg_OthPlanAdv_Seco"] = null;
            Session["RegMobileReg_OthDevDeposit_Seco"] = null;
            Session["RegMobileReg_OthPlanDeposit_Seco"] = null;
            Session["IsOlo"] = null;

        }

        private void ClearSessionsALL()
        {
            #region PlanFlow Sessions

            Session["FromPromoOffer"] = null;
            Session["RegMobileReg_ContractID"] = null;
            Session["MarketCode"] = null;
            Session["ArticleId"] = null;
            Session["RegMobileReg_PhoneVM"] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session["OfferId"] = null;
            Session["ArticleId_Seco"] = null;
            Session["RegMobileReg_VasNames"] = null;
            Session["UOMPlanID"] = null;
            Session["RegMobileReg_OfferDevicePrice"] = null;
            Session["SelectedComponents"] = null;
            Session["RegMobileReg_MainDevicePrice"] = null;
            Session["RegMobileReg_OrderSummary"] = null;
            Session["RegMobileReg_PkgPgmBdlPkgCompID"] = null;
            Session["RgMobileReg_UOMCode"] = null;
            Session["RegMobileReg_DataplanID"] = null;
            Session["Searchstatus"] = null;
            Session["MenuType"] = null;
            Session["SelectedPlanID"] = null;
            Session["Discount"] = null;
            Session["ExtratenMobilenumbers"] = null;
            Session["RegMobileReg_MalyDevAdv"] = null;
            Session["RegMobileReg_MalayPlanAdv"] = null;
            Session["RegMobileReg_MalayDevDeposit"] = null;
            Session["RegMobileReg_MalyPlanDeposit"] = null;
            Session["RegMobileReg_MalAdvDeposit"] = null;
            Session["RegMobileReg_OthDevAdv"] = null;
            Session["RegMobileReg_OthPlanAdv"] = null;
            Session["RegMobileReg_OthDevDeposit"] = null;
            Session["RegMobileReg_OthPlanDeposit"] = null;
            Session["RegMobileReg_OtherAdvDeposit"] = null;
            Session["RegK2_Status"] = null;
            Session["SelectedComponents"] = null;
            Session["AssignNew"] = null;
            Session["SpendLimit"] = null;
            Session["SuppleMsisdns"] = null;
            Session["IsSuppleNewAccount"] = null;
            Session["isVIP"] = null;
            Session["RegMobileReg_IsMNPServiceIdCheckStatusCode"] = null;
            Session["RegMobileReg_IsPortInStatusCode"] = null;
            Session["isSuperAdmin"] = null;
            Session["VIP"] = null;
            Session["WO"] = null;
            Session["IDCardTypeIDMSiSDNNew"] = null;
            Session["IDCardMSiSDNNew"] = null;
            Session["IDCardTypeName"] = null;
            Session["AcctExtId"] = null;
            Session["AcctAllDetails"] = null;
            Session["AccountsExt"] = null;
            Session["FxInternalCheck"] = null;
            Session["FxInternalCheckStatusCode"] = null;
            //Session["SR_PPIDCheck"] = null;
            //Session["SR_PPID"] = null;
            Session["PostalCode"] = null;
            Session["State"] = null;
            //Session["AccountHolder"] = null;
            Session["AccountNumber"] = null;
            Session["Mocandwhitelistcuststatus"] = null;
            Session["MocDefault"] = null;
            Session["MocandwhitelistSstatus"] = null;
            Session["MOCIDs"] = null;
            Session["Condition"] = null;
            Session["SaveMenuOption"] = null;
            Session["VasGroupIds"] = null;
            Session["Penalty"] = null;
            Session["CRPPenalty"] = null;
            Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"] = null;
            Session["RegMobileReg_VirtualMobileNo_Sec"] = null;
            Session["RegMobileReg_MobileNo"] = null;
            Session["SelectedPlanID_Seco"] = null;
            Session["SimType"] = null;
            Session["RegMobileReg_FromSearch"] = null;
            Session["RegMobileReg_ResultMessage"] = null;
            Session["RegMobileReg_FromSearch_Seco"] = null;
            Session["RegMobileReg_ResultMessage_Seco"] = null;
            Session["RegMobileReg_MalyDevAdv_Seco"] = null;
            Session["RegMobileReg_MalayPlanAdv_Seco"] = null;
            Session["RegMobileReg_MalayDevDeposit_Seco"] = null;
            Session["RegMobileReg_MalyPlanDeposit_Seco"] = null;
            Session["RegMobileReg_OthDevAdv_Seco"] = null;
            Session["RegMobileReg_OthPlanAdv_Seco"] = null;
            Session["RegMobileReg_OthDevDeposit_Seco"] = null;
            Session["RegMobileReg_OthPlanDeposit_Seco"] = null;
            Session["intdataidsval"] = null;
            Session["intextraidsval"] = null;
            Session["intmandatoryidsvalSec"] = null;
            Session["intdataidsvalSec"] = null;
            Session["intextraidsvalSec"] = null;
            Session["RegMobileReg_BiometricVerify"] = null;
            Session["RegMobileReg_BiometricID"] = null;
            Session["RegMobileReg_TabIndex"] = null;
            Session["RegMobileReg_IDCardNo"] = null;
            Session["RegMobileReg_SelectedModelID"] = null;
            Session["RegMobileReg_SelectedModelImageID_Seco"] = null;
            Session["RegMobileReg_SelectedModelID_Seco"] = null;
            Session["RegMobileReg_ProgramMinAge"] = null;
            Session["RegMobileReg_PhoneVM"] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session["RegStepNo"] = null;
            Session["RegMobileReg_IDCardType"] = null;
            Session["RegMobileReg_SelectedOptionID_Seco"] = null;
            Session["RegMobileReg_SelectedOptionType"] = null;
            Session["RegMobileReg_SelectedOptionType_Seco"] = null;
            Session["RegMobileReg_SublineVM"] = null;
            Session["ArticleId_Seco"] = null;
            Session["UOMPlanID"] = null;
            Session["RegMobileReg_OfferDevicePrice_Seco"] = null;
            Session["RegMobileReg_VasNames_Seco"] = null;
            Session["RegMobileReg_VasIDs_Seco"] = null;
            Session["UOMPlanID_Seco"] = null;
            Session["RegMobileReg_ProgramMinAge_Seco"] = null;
            Session["RegMobileReg_ContractID_Seco"] = null;
            Session["RegMobileReg_DataplanID_Seco"] = null;
            Session["GuidedSales_PkgPgmBdlPkgCompID_Seco"] = null;
            Session["vasBackClick"] = null;
            Session["RegMobileSub_MobileNo"] = null;
            Session["RegMobileSub_VasIDs"] = null;
            Session["RegMobileReg_SublineSummary"] = null;
            Session["OCCmpntList"] = null;
            Session["IsMNPWithMultpleLines"] = null;
            Session["RegMobileReg_Type"] = null;
            Session["GuidedSales_PkgPgmBdlPkgCompID"] = null;
            Session["GuidedSales_SelectedModelImageID"] = null;
            Session["GuidedSales_SelectedModelID"] = null;
            Session["GuidedSales_SelectedBrandID"] = null;
            Session["GuidedSales_Type"] = null;
            Session["SelectedComponents_Seco"] = null;
            Session["RegMobileReg_DataPkgID_Seco"] = null;
            Session["RegMobileReg_MalAdvDeposit_Seco"] = null;
            Session["RegMobileReg_OtherAdvDeposit_Seco"] = null;
            Session["MobileRegType"] = null;
            Session["DeviceType"] = null;
            Session["MandatoryVasIds"] = null;
            Session["MandatoryVasKenanIds"] = null;
            Session["vasmandatoryids"] = null;
            Session["VAS"] = null;
            Session["SelectedKenanCodes"] = null;
            Session["VAS_Seco"] = null;
            Session["RegMobileReg_DataPkgID"] = null;
            Session["MobileNo_Seco"] = null;
            Session["RegMobileReg_IsBlacklisted"] = null;
            Session["Crp-Plan"] = null;
            Session["RegID"] = null;
            Session["KenanAccountNo"] = null;
            Session["IsOlo"] = null;
            //DDMF Checking
            Session["HistCompany"] = null;

            #endregion

            #region Divice + Plan sessions

            Session["OCCmpntList"] = null;
            Session["RegMobileReg_SublineSummary"] = null;
            Session["MandatoryVasIds"] = null;
            Session["MandatoryVasKenanIds"] = null;
            Session["vasmandatoryids"] = null;
            Session["SelectedKenanCodes"] = null;
            Session["VAS_Seco"] = null;
            Session["RegMobileReg_DataPkgID"] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session["OfferId_Seco"] = null;
            Session["Discount_Seco"] = null;
            Session["RegMobileReg_MainDevicePrice_Seco"] = null;
            Session["RegMobileReg_OrderSummary_Seco"] = null;
            Session["intmandatoryidsval"] = null;
            Session["RegMobileReg_BiometricID"] = null;
            Session["PhoneVM"] = null;
            Session["_componentViewModel"] = null;
            Session["MobileRegType"] = null;
            Session["MobileRegType"] = null;
            Session["MobileRegType"] = null;
            Session["DeviceCatalogSeco"] = null;
            Session["RegMobileReg_RRPDevicePrice"] = null;
            Session["RegMobileReg_UpfrontPayment"] = null;
            Session["RegMobileReg_UpfrontPayment_isWaiverReq"] = null;

            #endregion

            #region Suppline

            Session["VasGroupIds_Seco"] = null;
            Session["FxAccNo"] = null;
            Session["ExternalID"] = null;
            Session["AccExternalID"] = null;
            Session["KenanACNumber"] = null;
            Session["Secondary"] = null;
            Session["RegMobileReg_BiometricVerify"] = null;
            Session["isSuppAsNew"] = null;
            Session["SecondaryLineVas"] = null;

            #endregion
            Session[SessionKey.WaiverRules.ToString()] = null;
            Session["FromMNP"] = null;

            #region Clear TAC Cookies
            HttpCookie tacCookie = Request.Cookies.Get("lastTac");
            HttpCookie tacCookieDt = Request.Cookies.Get("lastTacRequestDt");
            if (tacCookie != null && tacCookieDt != null)
            {
                tacCookie.Expires = DateTime.Now.AddDays(-1);
                tacCookieDt.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(tacCookie);
                Response.Cookies.Add(tacCookieDt);
            };
            #endregion
            WebHelper.Instance.clearApprovalCheckboxes();
        }
        #endregion

        #region Private Methods

        private void clearUserRegistration()
        {
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_IsBlacklisted");
            }

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_ResultMessage.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_ResultMessage");
            }
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_IsAgeCheckFailed.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_IsAgeCheckFailed");
            }

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_AgeCheckResultMessage.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_AgeCheckResultMessage");
            }
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_IsDDMFCheckFailed.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_IsDDMFCheckFailed");
            }

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_DDMFCheckResultMessage.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_DDMFCheckResultMessage");
            }
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_IsBlacklisted");
            }

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_IsAddressCheckFailed.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_IsAddressCheckFailed");
            }
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_AddressCheckResultMessage.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_AddressCheckResultMessage");
            }
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_IsTotalLineCheckFailed.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_IsTotalLineCheckFailed");
            }

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_TotalLineCheckResultMessage.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_TotalLineCheckResultMessage");
            }
            if (!ReferenceEquals(Session["existingTotalLine"], null))
            {
                Session.Contents.Remove("existingTotalLine");
            }
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckFailed.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_IsOutstandingCreditCheckFailed");
            }

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OutstandingCreditCheckResultMessage.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_OutstandingCreditCheckResultMessage");
            }
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_PricinpalLineCheckResultMessage.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_PricinpalLineCheckResultMessage");
            }

            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
            {
                Session.Contents.Remove("FromMNP");
            }

            ///ADDED TO REMOVE MNP SUPPLEMENTARY LINES
            if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
            {
                Session.Contents.Remove("MNPSupplementaryLines");
            }
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_IsPortInDonerMessage.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_IsPortInDonerMessage");
            }

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_IsPortInDonerCheckFailed.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_IsPortInDonerCheckFailed");
            }

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckFailed.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_IsMNPServiceIdCheckFailed");
            }

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckMessage.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_IsMNPServiceIdCheckMessage");
            }

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_MSISDN.ToString()], null))
            {
                Session.Contents.Remove("RegMobileReg_MSISDN");
            }
            if (!ReferenceEquals(Session[SessionKey.IDCardMSiSDNNew.ToString()], null))
            {
                Session.Contents.Remove("IDCardMSiSDNNew");
            }
            if (!ReferenceEquals(Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()], null))
            {
                Session.Contents.Remove("IDCardTypeIDMSiSDNNew");
            }
            if (!ReferenceEquals(Session[SessionKey.KenanACNumber.ToString()], null))
            {
                Session.Contents.Remove("KenanACNumber");
            }

            if (!ReferenceEquals(Session[SessionKey.FailedAcctIds.ToString()], null))
            {
                Session.Contents.Remove("FailedAcctIds");
            }
            if (!ReferenceEquals(Session["BreStatusDetails"], null))
            {
                Session.Contents.Remove("BreStatusDetails");
            }
            #region VLT ADDED CODE
            if (!ReferenceEquals(Session[SessionKey.PPID.ToString()], null))
            {
                Session[SessionKey.PPID.ToString()] = string.Empty;
            }

            if (!ReferenceEquals(Session[SessionKey.PPIDInfo.ToString()], null))
            {
                Session.Contents.Remove("PPIDInfo");
            }
            if (!ReferenceEquals(Session[SessionKey.SR_PPID.ToString()], null))
            {
                Session[SessionKey.SR_PPID.ToString()] = null;
            }
            if (!ReferenceEquals(Session[SessionKey.SR_PPIDCheck.ToString()], null))
            {
                Session[SessionKey.SR_PPIDCheck.ToString()] = string.Empty;
            }
            #endregion VLT ADDED CODE
            Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()] = "AN";

            Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()] = "DN";

            Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()] = "ADN";

            Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()] = "TLN";

            Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()] = "CN";

            Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()] = "PLN";

            Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()] = "ON";

            Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()] = "MPPN";

            Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()] = "MSIN";

            if (!ReferenceEquals(Session[SessionKey.AcctExtId.ToString()], null))
            {
                Session.Contents.Remove("AcctExtId");
            }
            if (!ReferenceEquals(Session[SessionKey.VIP.ToString()], null))
            {
                Session.Contents.Remove("VIP");
            }
            if (!ReferenceEquals(Session[SessionKey.WO.ToString()], null))
            {
                Session.Contents.Remove("WO");
            }
            if (!ReferenceEquals(Session[SessionKey.WO.ToString()], null))
            {
                Session[SessionKey.isVIP.ToString()] = null;
            }

            if (!ReferenceEquals(Session[SessionKey.MNPResetSupplines.ToString()], null))
            {
                Session.Contents.Remove("MNPResetSupplines");
            }
            if (!ReferenceEquals(Session["isOnlyPREGSMSIMReplace"], null))
            {
                Session.Contents.Remove("isOnlyPREGSMSIMReplace");
            }
            Session[SessionKey.IsSMEIndividualOnly.ToString()] = null;
            Session[SessionKey.IsCorpIndividualOnly.ToString()] = null;
        }

        private CustomizedAccount PopulateSimReplacementAccountInfo(string msisdn)
        {
            retrieveAcctListByICResponse AcctListByICResponse = null;
            AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.SR_PPID.ToString()];

            List<Items> itemList = AcctListByICResponse.itemList.Where(a => a.ExternalId == (msisdn)).ToList();

            if (itemList.Count > 1 && itemList.Exists(a => a.ServiceInfoResponse != null))
                itemList = AcctListByICResponse.itemList.Where(a => a.ExternalId == (msisdn) && a.ServiceInfoResponse != null).ToList();

            CustomizedAccount Account = null;
            try
            {
                if (!ReferenceEquals(itemList[0].Customer, null))
                {
                    if (itemList[0].Customer.PostCode != null)
                    {
                        Session[SessionKey.PostalCode.ToString()] = itemList[0].Customer.PostCode;
                    }
                    else
                    {
                        Session[SessionKey.PostalCode.ToString()] = string.Empty;
                    }

                    if (itemList[0].Customer.State != null)
                    {
                        Session[SessionKey.State.ToString()] = itemList[0].Customer.State;
                    }
                    else
                    {
                        Session[SessionKey.State.ToString()] = string.Empty;
                    }


                    var addressDetails = Util.ConcatenateStrings(" ", itemList[0].Customer.City, itemList[0].Customer.PostCode, itemList[0].Customer.State);

                    Account = new CustomizedAccount();
                    Account.AccountNumber = itemList[0].AcctExtId;
                    Account.Holder = itemList[0].Customer.CustomerName;
                    Account.CompanyName = "";
                    Account.Category = "";
                    Account.MarketCode = "";

                    var address1 = itemList[0].Customer.Address1.Trim();
                    var address2 = itemList[0].Customer.Address2.Trim();
                    var address3 = itemList[0].Customer.Address3.Trim();

                    if (!string.IsNullOrEmpty(address1) && address1.Length > 0 )
                    {
                        var lastChar = address1[address1.Length - 1];
                        if (lastChar == ',')
                        {
                            address1 = address1.Substring(0, address1.Length - 1);
                        }
                    }

                    if (!string.IsNullOrEmpty(address2) && address2.Length > 0)
                    {
                        var lastChar = address2[address2.Length - 1];
                        if (lastChar == ',')
                        {
                            address2 = address2.Substring(0, address2.Length - 1);
                        }
                    }

                    if (!string.IsNullOrEmpty(address3) && address3.Length > 0)
                    {
                        var lastChar = address3[address3.Length - 1];
                        if (lastChar == ',')
                        {
                            address3 = address3.Substring(0, address3.Length - 1);
                        }
                    }

                    Account.Address = Util.ConcatenateStrings(", ", address1, address2, address3, addressDetails);
                    Account.SubscribeNo = "";
                    Account.SubscribeNoResets = "";
                    Account.ActiveDate = ""; //TODO: Find out what api needed for this
                    Account.Plan = ""; //TODO: Find out what api needed for this
                    Account.IDNumber = Session[SessionKey.IDCardNo.ToString()].ToString();
                    Account.IDType = Util.GetNameByID(RefType.IDCardType, Session[SessionKey.IDCardTypeID.ToString()].ToInt());
                    // Added for Smart Intergration by KiranG
                    //Session[SessionKey.AccountHolder.ToString()] = Account.Holder;
                    //Session[SessionKey.AccountNumber.ToString()] = itemList[0].AcctIntId;
                    Session["AccountHolder"] = Account.Holder;
                    Session["AccountNumber"] = itemList[0].AcctIntId;
                    //EO Smart Intergration by KiranG
                }
                else
                {
                    Account = null;
                }
            }
            catch (Exception ex)
            {

                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                Account = null;
            }

            return Account;
        }

        private BusinessRuleResponse BusinessFxInternalCheck()
        {
            string idCardNo = string.Empty;
            string idCardTypeID = string.Empty;
            var idCardType = new IDCardType();
            var resp = new BusinessRuleResponse();
            int? GeneratedID = null;
            string xmlRes = string.Empty;

            try
            {
                idCardNo = Session["IDCardNo"].ToString2();
                idCardTypeID = Session["IDCardTypeID"].ToString2();
            }
            catch
            {
                idCardNo = Session[SessionKey.IDCardNo.ToString()].ToString();
                idCardTypeID = Session[SessionKey.IDCardTypeID.ToString()].ToString();
            }


            using (var proxy = new RegistrationServiceProxy())
            {
                idCardType = proxy.IDCardTypeGet(Convert.ToInt32(idCardTypeID));
            }

            if (idCardType != null)
                Session["IDCardTypeKenanCode"] = idCardType.KenanCode;

            try
            {
                var req = new BusinessRuleRequest();
                req.RuleNames = "FxInternalCheck".Split(' ');
                req.IDCardNo = idCardNo;
                if (idCardType != null)
                    req.IDCardType = idCardType.KenanCode;
                DateTime dtReq = DateTime.Now;

                resp = BREChecker.validate(req);
                //02072015 - Anthony - Enhance Customer Search: remove duplicate BusinessValidationChecks - Start
                //if (Session[SessionKey.PPID.ToString()] != null)
                //{
                //    if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                //    {
                //        WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "E");
                //    }
                //    else
                //    {
                //        WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");
                //    }

                //}
                //02072015 - Anthony - Enhance Customer Search: remove duplicate BusinessValidationChecks - End
                DateTime dtRes = DateTime.Now;
                if (resp != null)
                {
                    Session[SessionKey.FxInternalCheck.ToString()] = resp.IsFxInternalCheckFailed;
                    Session[SessionKey.FxInternalCheckStatusCode.ToString()] = resp.IsFxInternalCheckFailed ? "FF" : "FS";
                }
                string xmlReq = Util.ConvertObjectToXml(req);
                xmlRes = Util.ConvertObjectToXml(resp);

                WebHelper.Instance.LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: null, dtRes: null);
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);

                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                Session[SessionKey.FxInternalCheckStatusCode.ToString()] = "FE";

            }

            return resp;
        }

        /// <summary>
        /// Log Modified to make async call
        /// </summary>
        /// <param name="APIname"></param>
        /// <param name="MethodName"></param>
        /// <param name="status"></param>
        /// <param name="userName"></param>
        /// <param name="xmlReqObj"></param>
        /// <param name="xmlResObj"></param>
        /// <param name="idCardNo"></param>
        /// <param name="dtReq"></param>
        /// <param name="dtRes"></param>
        private void LogPPIDReqandResAsync(string APIname, string MethodName, string status = "", string userName = "", object xmlReqObj = null, object xmlResObj = null, string idCardNo = "", System.DateTime? dtReq = null, System.DateTime? dtRes = null)
        {
            string xmlReq = Util.ConvertObjectToXml(xmlReqObj);
            string xmlRes = (xmlResObj == null ? "" : Util.ConvertObjectToXml(xmlResObj));

            SaveCallStatusResp responseFromBusinessValidation = null;

            string currentLoggedInUserName = string.Empty;

            if (!ReferenceEquals(Request, null))
            {
                //Get username from cookie
                if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                {
                    currentLoggedInUserName = Request.Cookies["CookieUser"].Value;
                }
                else
                {
                    return;
                }
            }
            else
            {
                currentLoggedInUserName = userName;
            }


            using (var proxy = new retrieveServiceInfoProxy())
            {
                responseFromBusinessValidation = new SaveCallStatusResp();

                responseFromBusinessValidation = proxy.APISaveCallStatus(new SaveCallStatusReq
                {
                    tblAPICallStatusMessagesval = new tblAPICallStatus
                    {
                        APIName = APIname,
                        MethodName = MethodName,
                        RequestTime = (dtReq == null ? System.DateTime.Now : Convert.ToDateTime(dtReq)),
                        UserName = currentLoggedInUserName,
                        BreXmlReq = xmlReq,
                        BreXmlRes = xmlRes,
                        IdCardNo = idCardNo,
                        ResponseTime = (dtRes == null ? System.DateTime.Now : Convert.ToDateTime(dtRes)),
                        Status = status
                    }
                });
            }

        }
        /// <summary>
        /// End Log Modified to make async call
        /// </summary>
        /// <returns></returns>

        private retrieveAcctListByICResponse VIPMassking(retrieveAcctListByICResponse objMasking)
        {
            foreach (var v in objMasking.itemList)
            {
				if (v.Account != null)
				{
					v.Account.IDNumber = "************";
					v.Account.Holder = "************";
					v.Account.Address = "*****, *****, *****, ***** ***** ***** ";
				}
                
                if (v.ServiceInfoResponse != null)
                {
                    v.ExternalId = "**********";
                    v.ServiceInfoResponse.lob = "******";

                }

                if (v.PrinSuppResponse != null && v.PrinSuppResponse.itemList != null && v.PrinSuppResponse.itemList.Count > 0)
                {
                    foreach (var v1 in v.PrinSuppResponse.itemList)
                    {
                        v1.cust_nmField = "************";
                        v1.msisdnField = "**********";
                        v1.subscr_noField = "**********";


                    }
                }
            }

            return objMasking;
        }

        private void ValidateWriteOffLayout(retrieveAcctListByICResponse AcctListByICResponse)
        {
            bool hasActiveAcounts = false;
            hasActiveAcounts = AcctListByICResponse.itemList.Exists(a => a.IsActive == true);
            foreach (var accountLine in AcctListByICResponse.itemList)
            {

                //if (Session["AllowSimReplacement"].ToString() == "True")
                //{
                //    continue;
                //}
                //if (accountLine.ServiceInfoResponse != null)
                //{
                if (accountLine.Customer != null && accountLine.AccountDetails != null)
                {
                    //2015-08-14 Evan Fix, if the transaction write off from customer got related account need to
                    //if (accountLine.AccountDetails != null && accountLine.AccountDetails.TotalWriteOff.ToDecimal() <= 0)
                    //{
                    //    continue;
                    //}
                    switch (accountLine.Customer.RiskCategory)
                    {
                        case "MASS_A":
                        case "MASS_B":
                        case "MASS_C":
                            //Evan move the Mass A, B and C into CMSS case, accordin to Margarets explanation
                            ViewBag.AllowSimReplacement = "True";
                            Session["AllowSimReplacement"] = "True";
                            break;
                            //if (accountLine.Customer.AcctActiveDt != null)
                            //{
                            //    var tenureDate = WebHelper.Instance.TenureCheckInMonths(accountLine.AccountDetails.AcctActiveDt);
                            //    if (tenureDate > 36)
                            //    {
                            //        ViewBag.AllowSimReplacement = "True";
                            //        Session["AllowSimReplacement"] = "True";
                            //        break;
                            //    }
                            //    else
                            //    {
                            //        if (tenureDate < 36 && accountLine.AccountDetails.TotalWriteOff.ToDecimal() >= 100)
                            //        {
                            //            // if(hasActiveAcounts)
                            //            //   Session["WriteOffHardstop"] = "True";
                            //            //else
                            //            if (WebHelper.Instance.CompareCMSSDtForAcct(Session[SessionKey.IDCardNo.ToString()].ToString2()) > 14.0)
                            //            {
                            //                Session["HardStop"] = "True";
                            //            }

                            //        }
                            //        if (accountLine.AccountDetails.TotalWriteOff.ToDecimal() < 100)
                            //        {
                            //            ViewBag.AllowSimReplacement = "True";
                            //            Session["AllowSimReplacement"] = "True";
                            //            break;
                            //        }
                            //    }
                            //}
                            //break;
                        default:
                            break;
                    }
                }
                if (AcctListByICResponse.itemList.Any() && AcctListByICResponse.itemList.Exists(c => (c.ServiceInfoResponse == null)) && AcctListByICResponse.itemList.Where(c => (c.ServiceInfoResponse == null)).ToList().Count == AcctListByICResponse.itemList.Count())
                {
                    ///EVEN IF THE USER DOES NOT HAVE ANY GSM A/C AS PER REQUIREMENT ABOVE GET THE CUSTOMER INFORMATION BASED ON NON GSM A/C
                    // ExternalId_Msisdn = AcctListByICResponse.itemList.FirstOrDefault(c => (c.ServiceInfoResponse == null)).ExternalId;
                    ///New user
                    //Session[SessionKey.PPID.ToString()] = "E";

                    //if (AcctListByICResponse.itemList.Any(c => c.Customer != null && c.Customer.RiskCategory.ToLower() == "mass_c"))
                    //{
                    if (WebHelper.Instance.CompareCMSSDtForAcct(Session[SessionKey.IDCardNo.ToString()].ToString2()) > 14.0)
                    {
                        Session["HardStop"] = "True";
                    }
                    // }
                }
                //}
            }
        }

        private void PerformBiometricCheck(string icno)
        {
            try
            {
                bool bioCheck = false;
                AMRSvc.RetrieveMyKadInfoRp resp = new AMRSvc.RetrieveMyKadInfoRp();

                bool loadKadInfoFromStub = ConfigurationSettings.AppSettings["loadKadInfoFromStub"] != null && ConfigurationSettings.AppSettings["loadKadInfoFromStub"].ToString().ToLower().Equals("true");
				// for testing purpose, make loadKadInfoFromStub true
                //bool loadKadInfoFromStub = true;
                if (!loadKadInfoFromStub)
                {
                    using (var proxy = new AMRServiceProxy())
                    {
                        resp = proxy.RetrieveMyKadInfo(new AMRSvc.RetrieveMyKadInfoRq()
                        {
                            ApplicationID = 3,
                            DealerCode = Util.SessionAccess.User.Org.DealerCode,
                            NewIC = icno
                        });
                    }
                }
                else
                {
                    //Debug case. Load data from Xml file instead of Kenan. 
                    string path = Server.MapPath(@"~\KadTestData\kadDebugResponse.xml");
                    string content = System.IO.File.ReadAllText(path);
                    resp = FromXmlString(content);
                }

                if (!string.IsNullOrEmpty(resp.DOB))
                {
                    resp.DOB = resp.DOB.Replace("/", "");
                }
               

                if (!string.IsNullOrEmpty(resp.NewIC))
                {
                    int val = int.Parse(resp.NewIC[resp.NewIC.Length - 1].ToString());
                    resp.Gender = val % 2 != 0 ? "M" : "F";
                }


                if (!string.IsNullOrEmpty(resp.Race))
                {
                    if (resp.Race == "MELAYU")
                    {
                        resp.Race = "MALAY";
                    }
                    else if (resp.Race == "CINA")
                    {
                        resp.Race = "CHINESE";
                    }
                    else if (resp.Race == "INDIA")
                    {
                        resp.Race = "INDIAN";
                    }
                    else
                    {
                        resp.Race = "OTHERS";
                    }
                }

                Session[SessionKey.BiometricResponse.ToString()] = resp;

                if ((!resp.FingerMatch && string.IsNullOrEmpty(resp.FingerThumb)))
                {
                    Session[SessionKey.BiometricDesc.ToString()] = "No Biometric Data found";
                    Session["RegMobileReg_BiometricVerify"] = null;
                    Session.Contents.Remove("RegMobileReg_BiometricID");
                }
                else
                {
                    bioCheck = !string.IsNullOrEmpty(resp.FingerThumb) && (resp.FingerThumb == "1" || resp.FingerThumb == "2");

                    if (resp.FingerMatch)
                        Session[SessionKey.BiometricDesc.ToString()] = "Biometric check is performed, thumb impression matched!";
                    else
                    {
                        bioCheck = false;
                        Session[SessionKey.BiometricDesc.ToString()] = "Biometric check is performed, but thumb impression not matched!";
                    }

                    Session["RegMobileReg_BiometricVerify"] = bioCheck;
                    if (bioCheck)
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            Session["RegMobileReg_BiometricID"] = proxy.BiometricCreate(WebHelper.Instance.ConstructBriometri(resp));
                        }
                    }
                }
				if (!string.IsNullOrEmpty(resp.Code) && resp.Code.Equals("-1"))
				{
					Session[SessionKey.BiometricDesc.ToString()] = "Unable to connect to Biometric";
				}

            }
            catch (Exception ex)
            {
                Session[SessionKey.BiometricDesc.ToString()] = "Unable to connect to Biometric";//20012015 - Anthony - Biometric CR, to modify the error message
                //Session[SessionKey.BiometricDesc.ToString()] = "No Biometric Data found. Biometric check not performed";
                Util.LogException(ex);
            }

        }

        #endregion

        #region Protected Methods

        protected override void Initialize(RequestContext requestContext)
        {
            Util.CheckSessionAccess(requestContext);
            base.Initialize(requestContext);
        }

        /// <summary>
        /// To show contract details on home page
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <param name="cardType"></param>
        protected void LogUserInfo(string cardNumber, string cardType)
        {
            //To log the user details
            UserTransactionLogResp responseFromBusinessValidation = null;
            using (var proxy = new UserServiceProxy())
            {
                responseFromBusinessValidation = new UserTransactionLogResp();

                responseFromBusinessValidation = proxy.UserTransactionLogCreate(new UserTransactionLogReq
                {
                    UserInfoVal = new UserTransactionLog
                    {
                        Username = Util.SessionAccess.UserName,
                        IDType = cardNumber,
                        IDNumber = cardType,
                        Result = Session[SessionKey.PPID.ToString()].ToString(),
                        Action = "PPID Check"
                    }
                });
                proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), 0, "PPID CHECK", "", "");
            }
            //End To log the user details
        }
        /// <summary>
        /// End to show contract details on home page
        /// </summary>

        #endregion

        #region Public Methods

        //Log Modified to make async call

        public void LogPPIDReqandRes(string APIname, string MethodName, string status = "", string userName = "", string xmlReq = "", string xmlRes = "", string idCardNo = "", System.DateTime? dtReq = null, System.DateTime? dtRes = null)
        {
            System.Threading.Thread logThread = new System.Threading.Thread(() => LogPPIDReqandResAsync(APIname, MethodName, status, userName, xmlReq, xmlRes, idCardNo, dtReq, dtRes));
            logThread.Start();
        }

        public void LogPPIDReqandRes(string APIname, string MethodName, string status = "", string userName = "", object xmlReqObj = null, object xmlResObj = null, string idCardNo = "", System.DateTime? dtReq = null, System.DateTime? dtRes = null)
        {
            System.Threading.Thread logThread = new System.Threading.Thread(() => LogPPIDReqandResAsync(APIname, MethodName, status, userName, xmlReqObj, xmlResObj, idCardNo, dtReq, dtRes));
            logThread.Start();
        }

        //End log Modified to make async call
		
		[Authorize]
		public ActionResult GetRecommendations(string externalID)
		{
			var acctListByICResponse = (retrieveAcctListByICResponse)Session["AllServiceDetails"];
			ExecuteAssessmentResponse response = new ExecuteAssessmentResponse();
			PegaVM pegaVM = new PegaVM();

			pegaVM.executeAssessmentResponse.Channel = "iSELL";
			ViewBag._ExtID = externalID;
			ViewBag._ViewID = "PEGA_" + externalID;

			if (acctListByICResponse == null)
			{
				pegaVM.executeAssessmentResponse.Code = "-2";
				pegaVM.executeAssessmentResponse.Message = "There was a technical error during processing customer details. Please try again";

				return View("_PegaRecommendations", pegaVM);
			}
			pegaVM = executeAssessment(externalID, pegaVM);

			return View("_PegaRecommendations", pegaVM);
		}

		public ActionResult UpdateResponse(string externalID, string kenanCode, string pegaPropID, string action, string caseCategory)
		{
			var executeAssessmentResponse = (ExecuteAssessmentResponse)Session["ExecuteAssessmentResponse"];
			UpdateResponseResponse objResp = new UpdateResponseResponse();
			string responseCode = "";
			using (var proxy = new PegaServiceProxy())
			{
				/********************************************/
				var objReq = constructUpdateResponseRequest(executeAssessmentResponse, externalID, kenanCode, caseCategory, pegaPropID, action);
				objResp = proxy.UpdateResponse(objReq);
				/*******************************************/
				responseCode = objResp.Code;
				if (objResp.Code == "0")
				{
					using (var regProxy = new RegistrationServiceProxy())
					{
						var pegaRecommendationReq = constructPegaRecommendationList(objReq);
						// this is to save to DB.
                        pegaRecommendationReq = regProxy.SavePegaRecommendation(pegaRecommendationReq);
						// this is for session store
						constructPegaOfferListSession(pegaRecommendationReq);
					}
				}
			}

			return ReassessRecommendations(externalID, caseCategory, objResp);
		}

		public ActionResult ReassessRecommendations(string externalID, string caseCategory, UpdateResponseResponse objResp)
		{
			PegaVM pegaVM = new PegaVM();
			ViewBag._ExtID = externalID;

			pegaVM = executeAssessment(externalID, pegaVM, caseCategory: caseCategory);
			pegaVM.defaultCaseCategory = caseCategory;
			pegaVM.defaultCaseCategoryValue = Util.GetValueFromSource(Constants.NBA_RECOMMENDATION, caseCategory);
			pegaVM.updateResponseResponse = objResp;

			return View("_PegaOfferList", pegaVM);
		}

		private PegaVM executeAssessment(string externalID, PegaVM _pegaVM, string caseCategory = "")
		{
			var acctListByICResponse = (retrieveAcctListByICResponse)Session["AllServiceDetails"];
			Session["ExecButeAssessmentResponse"] = null;
			foreach (string acctExtID in acctListByICResponse.itemList.Where(x => x.ServiceInfoResponse != null).Select(x => x.AcctExtId).Distinct())
			{
				foreach (var custValues in acctListByICResponse.itemList.Where(x => x.AcctExtId == acctExtID && x.ServiceInfoResponse != null))
				{
					if (custValues.ExternalId == externalID)
					{
						using (var proxy = new PegaServiceProxy())
						{
							var objReq = constructExecuteAssessmentReq(custValues, caseCategory);
							//objReq.Case_id = "80000319";
							/********************************************/
							_pegaVM.executeAssessmentResponse = proxy.ExecuteAssessment(objReq);
							_pegaVM.subscriberNo = objReq.Case_id;
							Session["ExecuteAssessmentResponse"] = _pegaVM.executeAssessmentResponse;
							/*******************************************/
						}
						break;
					}
				}
			}

			return _pegaVM;
		}
		#region Construct Method
		
		private ExecuteAssessmentRequest constructExecuteAssessmentReq(Items custValues, string caseCategory)
		{
			return new ExecuteAssessmentRequest
			{
				Case_id = string.Format("{0}_{1}", custValues.SusbcrNo, custValues.SusbcrNoResets),
				CallReason = !string.IsNullOrEmpty(caseCategory) ? caseCategory : Util.GetDefaultCaseCategory().Text,
				ExternalID = custValues.ExternalId,
				SalesPerson = Request.Cookies["CookieUser"] != null ? Request.Cookies["CookieUser"].Value : Util.SessionAccess != null ? Util.SessionAccess.UserName : ""
			};
		}

		private UpdateResponseRequest constructUpdateResponseRequest(ExecuteAssessmentResponse assessmentResponse, string externalID, string kenanCode, string caseCategory, string prepID, string action)
		{
			string _displayCategory = assessmentResponse.productList.Where(x=> x.PrepositionID == prepID).FirstOrDefault().Category;
			string _pegaIndexPosition = assessmentResponse.productList.Where(x => x.PrepositionID == prepID).FirstOrDefault().PegaIndex;

			return new UpdateResponseRequest
			{
				DecisionResultId = assessmentResponse.DecisionResultId,
				DisplayCategory = _displayCategory,
				TopRank = _pegaIndexPosition,
				Category = constructCaseCategory(caseCategory, 0), // split -
				Reason = constructCaseCategory(caseCategory, 1), // split - 
				CaseID = assessmentResponse.caseID,
				DecisionID = assessmentResponse.DecisionID,
				ProjectName = assessmentResponse.ProjectName,
				PropositionIdentifier = prepID,
				Response = action,
				Application = constructApplicationForUpdateResponse(),
				Channel = assessmentResponse.Channel,
				Customer = assessmentResponse.Customer,
				ExternalID = externalID,
				KenanCode = kenanCode,
				CaseCategory = caseCategory,
				ProductName = assessmentResponse.productList.FirstOrDefault(x=> x.PrepositionID == prepID).Name
			};
		}
		/// <summary>
		/// Department ID - Department Name
		//1 - CS-CO-SAVE-DESK
		//2 - CS-PRIORITY_SERVICES_UNIT(PSU) << isell will always take this
		//3 - CS-RETAIL-OPS
		//4 - CS-SCC 
		// Group ID - hardcoded to 2
		// Role ID - Role Name
		//1 - Manager
		//2 - Team Lead << for CCC
		//3 – Agent << for SA
		// sample value : /3/2/3/AgentID
		/// </summary>
		/// <returns>/Department/group/role/loginid</returns>
		private string constructApplicationForUpdateResponse()
		{
			string deptID = Util.GetValueFromSource(Constants.NBA_APP_UPDATE_SETTINGS, Constants.NBA_DEPT_ID);
			// need to change 
			//string groupID = Util.GetValueFromSource(Constants.NBA_APP_UPDATE_SETTINGS, Constants.NBA_GROUP_ID);
			int groupID = Util.SessionAccess.User.Org.SMARTTeamID;
			string agentID = Util.SessionAccess.UserName;
			string roleID = "";
			if (WebHelper.Instance.IsUserCCCAbove())
				roleID = Util.GetValueFromSource(Constants.NBA_APP_UPDATE_SETTINGS, Constants.NBA_ROLE_CCC);
			else
				roleID = Util.GetValueFromSource(Constants.NBA_APP_UPDATE_SETTINGS, Constants.NBA_ROLE_SA);

			return string.Format("{0}/{1}/{2}/{3}/{4}", "RA",deptID, groupID, roleID, agentID);
		}

		private string constructCaseCategory(string inputString, int wordLocation)
		{
			if (wordLocation == 0)
				return inputString.Split('-')[0].Trim();
			else
			{
				int i = inputString.IndexOf('-') + 1; // to include dash
				return inputString.Substring(i).Trim();
			}

		}
		
		private List<PegaRecommendation> constructPegaRecommendationList(UpdateResponseRequest request)
		{
			List<PegaRecommendation> responseList = new List<PegaRecommendation>();

			PegaRecommendation pegaRecommendationObj = new PegaRecommendation();
			pegaRecommendationObj.ProductName = request.ProductName;
			pegaRecommendationObj.MSISDN = request.ExternalID;
			pegaRecommendationObj.IDCardType = Session[SessionKey.IDCardTypeID.ToString()].ToString2() != "" ? Session[SessionKey.IDCardTypeID.ToString()].ToInt() : 0;
			pegaRecommendationObj.IDCardNo = Session[SessionKey.IDCardNo.ToString()].ToString2();
			pegaRecommendationObj.AcceptedDate = DateTime.Now;
			pegaRecommendationObj.CapturedById = Util.SessionAccess.UserName;
			pegaRecommendationObj.LastUpdDate = pegaRecommendationObj.AcceptedDate;
			pegaRecommendationObj.LastUpdId = pegaRecommendationObj.CapturedById;
			pegaRecommendationObj.Response = Util.capitalizeFirstChar(request.Response);
			pegaRecommendationObj.Status = Util.GetValueFromSource(Constants.NBA_RESPONSE_STATUS_MAP, request.Response);
			pegaRecommendationObj.KenanCode = request.KenanCode;
			pegaRecommendationObj.PrepositionId = request.PropositionIdentifier;
			pegaRecommendationObj.Application = request.Application;
			pegaRecommendationObj.CaseCategory = request.CaseCategory;

			responseList.Add(pegaRecommendationObj);

			return responseList;
		}

		private void constructPegaOfferListSession(List<PegaRecommendation> PegaRecommendationList)
		{
			var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];
			if (pegaOfferList == null)
				pegaOfferList = new PegaOfferVM();

			pegaOfferList.idCardNumber = PegaRecommendationList.FirstOrDefault().IDCardNo;
			foreach (var offer in PegaRecommendationList)
			{
				pegaOfferList.customerOfferDetailsList.Add(new CustomerOfferDetailsVM
				{
					id = offer.ID,
					msisdn = offer.MSISDN,
					action = offer.Response,
					kenanCode = offer.KenanCode,
					offerName = offer.ProductName,
					prepositionID = offer.PrepositionId

				});
			}
			Session["PegaOfferList"] = pegaOfferList;
		}
		#endregion

		 //<summary>
		 //Call to Pega service, to retrieve most usefull information in One View
		 //</summary>
		 //<param name="externalID"></param>
		 //<returns>ExecuteDefaultProfileResponse</returns>
		[Authorize]
		public ActionResult OneViewDetails(string externalID)
		{
		    var acctListByICResponse = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
		    int? GeneratedID = null;
		    string xmlReq = string.Empty;
		    string xmlResp = string.Empty;
		    string inputRequestLog = string.Empty;
		    string externalIDType = ConfigurationManager.AppSettings["KenanMSISDN"].ToString();
		    var AllContractList = MasterDataCache.Instance.AllContractList.Where(x => x.Type.ToLower().Contains("k2")).ToList();
		    List<string> contractTenure = new List<string>();
		    bool haveNonK2Contract = false;
		    Session["haveNonK2Contract"] = false;

		    if (acctListByICResponse == null)
		    {
		        throw new ArgumentNullException();
		    }

		    foreach (string acctExtID in acctListByICResponse.itemList.Where(x => x.ServiceInfoResponse != null).Select(x => x.AcctExtId).Distinct())
		    {
		        foreach (var custValues in acctListByICResponse.itemList.Where(x => x.AcctExtId == acctExtID && x.ServiceInfoResponse != null))
		        {
		            if (custValues.ExternalId == externalID)
		            {
		                try
		                {
		                    //var contractDetailsList = Util.retrieveContractListPenalty(externalID, externalIDType, custValues.SusbcrNo, custValues.SusbcrNoResets);

		                    List<SubscriberICService.retrievePenalty> contractDetailsList = new List<SubscriberICService.retrievePenalty>();
		                    using (var Smartproxy = new retrieveSmartServiceInfoProxy())
		                    {
		                        contractDetailsList = Smartproxy.retrievePenalty(externalID, ConfigurationManager.AppSettings["KenanMSISDN"], custValues.SusbcrNo, custValues.SusbcrNoResets, userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
		                            password: Properties.Settings.Default.retrieveAcctListByICResponsePassword);
		                    }
		                    DateTime minDate = DateTime.MaxValue;
		                    DateTime maxDate = DateTime.MinValue;
		                    string phoneModel = string.Empty;
		                    List<DateTime> listDate = new List<DateTime>();
		                    bool haveContract = false;
		                    List<existingCustomerContract> existingContractList = new List<existingCustomerContract>();

		                    if (!ReferenceEquals(contractDetailsList, null) && contractDetailsList.Count() > 0)
		                    {
		                        foreach (var contract in contractDetailsList)
		                        {
		                            phoneModel = contract.phoneModel;
		                            haveContract = true;

		                            // getting for contract list with start date & end date
		                            existingCustomerContract existingContract = new existingCustomerContract();
		                            existingContract.startDate = DateTime.ParseExact(contract.startDate.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
		                            existingContract.endDate = DateTime.ParseExact(contract.endDate.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
		                            existingContract.contractID = contract.contactId;
		                            existingContract.contractName = contract.ContractName;

		                            if (!existingContractList.Where(x => x.contractID == existingContract.contractID).Any())
		                            {
		                                existingContractList.Add(existingContract);
		                            }
		                        }
		                    }
		                    List<existingCustomerContract> finalExistingContract = new List<existingCustomerContract>();
		                    if (!ReferenceEquals(existingContractList, null) && existingContractList.Count() > 0)
		                    {
		                        // distinct the contractID
		                        //existingContractList = existingContractList.GroupBy(x => x.contractID).Select(grp => grp.First()).ToList();
		                        foreach (var contract in existingContractList)
		                        {
		                            bool contractFound = AllContractList.Where(x => x.Name.ToLower().Contains(contract.contractName.ToLower()) || x.Description.ToLower().Contains(contract.contractName.ToLower())).Any();

		                            if (contractFound)
		                            {
		                                finalExistingContract.Add(contract);
		                                haveContract = true;
		                            }
		                            else
		                            {
		                                haveNonK2Contract = true;
		                                Session["haveNonK2Contract"] = haveNonK2Contract;
		                            }
		                        }
		                        if (!ReferenceEquals(finalExistingContract, null) && finalExistingContract.Count() > 0)
		                        {
		                            // if contractlist 3, and k2Contract = 2, need to shown note that this line have non-k2 contract
		                            if (existingContractList.Count() > finalExistingContract.Count())
		                            {
		                                haveNonK2Contract = true;
		                                Session["haveNonK2Contract"] = haveNonK2Contract;
		                            }

		                            foreach (var tenure in finalExistingContract)
		                            {
		                                if (tenure.endDate > DateTime.Now)
		                                {
		                                    listDate.Add(tenure.startDate);
		                                    listDate.Add(tenure.endDate);
		                                    contractTenure.Add(Util.getDiffBetweenDate(tenure.startDate, tenure.endDate, showYears: false).Replace("Months", "").Trim());
		                                }
		                            }
		                        }
		                    }
		                    if (haveContract)
		                    {
		                        foreach (var date in listDate)
		                        {
		                            if (date < minDate)
		                                minDate = date;
		                            if (date > maxDate)
		                                maxDate = date;
		                        }
		                    }

		                    ExecuteDefaultProfileResponse objResp = new ExecuteDefaultProfileResponse();
		                    DateTime dtReq = DateTime.Now;
		                    using (var proxy = new PegaServiceProxy())
		                    {
		                        ExecuteDefaultProfileRequest objReq = new ExecuteDefaultProfileRequest();
		                        // test data : 82255499
		                        objReq.subscrNo = custValues.SusbcrNo;
		                        inputRequestLog = custValues.SusbcrNo + "#" + custValues.ExternalId;
		                        xmlReq = Util.ConvertObjectToXml(objReq);

		                        /**********************************************/
		                        objResp = proxy.CustomerOneViewDetails(objReq);
		                        /**********************************************/
		                        if (!ReferenceEquals(custValues.Packages, null) && custValues.Packages.Count() > 0)
		                        {
		                            string tempPlan = string.Empty;
		                            foreach (var package in custValues.Packages)
		                            {
		                                if (string.IsNullOrEmpty(tempPlan))
		                                {
		                                    foreach (var component in package.compList)
		                                    {
		                                        if (component.componentDesc.ToLower().Contains("mobile - telephony"))
		                                        {
		                                            tempPlan = package.PackageDesc;
		                                            break;
		                                        }
		                                    }
		                                }
		                            }

		                            //string tempPlan = custValues.Packages.Where(p => p.PackageDesc.ToLower().Contains(Constants.MANDATORY_PACKAGE)).SingleOrDefault().PackageDesc;
		                            if (tempPlan.Contains("-"))
		                            {
		                                string[] currPlan = tempPlan.Trim().Split('-');
		                                objResp.CurrentPlan = currPlan[0].Trim();
		                            }
		                            else
		                            {
		                                objResp.CurrentPlan = tempPlan;
		                            }
		                        }

		                        objResp.PrincipalMSISDN = custValues.ExternalId;
		                        // change lineActiveDuration to Line Tenure
		                        objResp.LineActiveDuration = Util.getDiffDate(custValues.ServiceInfoResponse.serviceActiveDt).ToString2();
		                        objResp.DeviceModel = phoneModel;
		                        objResp.CustomerTenure = string.Empty;
		                        // dummy reply
		                        //Session["haveNonK2Contract"] = true;

		                        if (contractTenure.Count() > 0)
		                        {
		                            foreach (var ct in contractTenure)
		                            {
		                                if (string.IsNullOrEmpty(objResp.CustomerTenure))
		                                {
		                                    objResp.CustomerTenure = ct + "+";
		                                }
		                                else
		                                {
		                                    objResp.CustomerTenure = objResp.CustomerTenure + ct + "+";
		                                }
		                            }
		                        }
		                        objResp.CustomerTenure = !string.IsNullOrEmpty(objResp.CustomerTenure) ? objResp.CustomerTenure.Remove(objResp.CustomerTenure.Length - 1) : string.Empty;

		                        if (haveContract && listDate.Count() > 0)
		                        {
                                    //Evan-150323 Change for ContractCheck sync with Kenan - Start
                                    //objResp.DurationToEnd = Util.getDiffDate(maxDate, showYears: false).ToString2();
                                    objResp.DurationToEnd = Util.getDiffDate(maxDate).ToString2();
                                    //Evan-150323 Change for ContractCheck sync with Kenan - Start
		                            string tempDay = maxDate.Day.ToString2().Length == 1 ? "0" + maxDate.Day.ToString2() : maxDate.Day.ToString2();
		                            string tempMonth = maxDate.Month.ToString2().Length == 1 ? "0" + maxDate.Month.ToString2() : maxDate.Month.ToString2();
		                            string tempDate = string.Format("{0}{1}{2}", maxDate.Year, tempMonth, tempDay);
		                            objResp.ContractEndDate = DateTime.ParseExact(tempDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy");
		                        }
		                        else
		                        {
		                            objResp.DurationToEnd = string.Empty;
		                            objResp.ContractEndDate = string.Empty;
		                        }
		                        xmlResp = Util.ConvertObjectToXml(objResp);
		                    }
		                    DateTime dtResp = DateTime.Now;
		                    Logger.Info("CustomerOneViewDetails - " + externalID + " - " + custValues.SusbcrNo);
		                    // return the response
		                    WebHelper.Instance.LogService(APIname: "PegaServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "RetrieveContractInfo", IsBefore: true, ID: GeneratedID, status: "Success", xmlRes: Util.ConvertObjectToXml(contractDetailsList), idCardNo: inputRequestLog, dtReq: dtReq, dtRes: dtResp);
		                    WebHelper.Instance.LogService(APIname: "PegaServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "CustomerOneView", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlResp, idCardNo: inputRequestLog, dtReq: dtReq, dtRes: dtResp);
		                    return View(objResp);
		                }
		                catch (Exception ex)
		                {
		                    WebHelper.Instance.LogService(APIname: "PegaServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "CustomerOneView", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message, idCardNo: inputRequestLog, dtReq: DateTime.Now);
		                }
		            }
		        }
		    }
		    return View();
		}

               
        #endregion
        [HttpPost]
        public int GetPPIDCount()
        {
            int ppidCount = 0;
            int status = 0;
            List<UserConfigurations> objUserConfiguration = new List<UserConfigurations>();
            using (var ppidCountProxy = new UserServiceProxy())
            {
                objUserConfiguration = ppidCountProxy.GetUserConfigurations();
                foreach (var v in objUserConfiguration)
                {
                    if (Util.SessionAccess.UserName == v.UserName)
                    {
                        var count = objUserConfiguration.Where(a => a.Action.Contains("PPID Check") && a.UserName == Util.SessionAccess.UserName).FirstOrDefault().Count;
                        //(var v in AcctListByICResponse.itemList.Where(c => (c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob == "POSTGSM")).ToList())
                        ppidCount = ppidCountProxy.GetPPIDCountSearch(Util.SessionAccess.UserName, "PPID Check");
                        if (ppidCount > count)
                        {
                            status = 1;
                        }
                        break; // break the foreach loop when user find.
                    }
                }


            }
            return status;
        }

        [Authorize]
        public ActionResult Index2()
        {
            Session[SessionKey.WriteOffAccts.ToString()] = null;
            Session[SessionKey.TransStartTime.ToString()] = null;
            Session[SessionKey.ContractDetails.ToString()] = null;
            Session[SessionKey.FxInternalCheckMessage.ToString()] = null;
            Session["IsOlo"] = null;
            Session["_componentViewModel"] = null;
            Session["WriteOffHardstop"] = null;
            Session["AllowSimReplacement"] = null;
            Session["AllowSimReplacementForWriteOff"] = null;
            Session["HardStop"] = null;
            Session["BreChecksbyUserID"] = null;
            Session["BreStatusDetails"] = null;
            Session["isMenuSuccess"] = null;
            Session["isHardStop"] = null;
            Session["BREFailedRules"] = null;
            ClearSessionsALL();

            Session["BusinessCheck"] = null;

            Session["AccountswithContractCount"] = null;
            Session["HasContract"] = null;

            Session[SessionKey.FromPromoOffer.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_UOMCode.ToString()] = null;
            Session[SessionKey.ArticleId.ToString()] = null;
            Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;

            ViewBag.Message = "Home Screen";
            ViewBag.SearchBy = "No Search";
            Session[SessionKey.Searchstatus.ToString()] = null;
            Session[SessionKey.MenuType.ToString()] = null;
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = null;
            WebHelper.Instance.ResetDepositSessions();
            Session[SessionKey.RegK2_Status.ToString()] = null;
            Session[SessionKey.SelectedComponents.ToString()] = null;
            Session[SessionKey.AssignNew.ToString()] = null;
            Session[SessionKey.SpendLimit.ToString()] = null;

            Session[SessionKey.SuppleMsisdns.ToString()] = null;
            Session[SessionKey.IsSuppleNewAccount.ToString()] = null;
            Session[SessionKey.isVIP.ToString()] = null;
            #region VLT ADDED CODE
            clearUserRegistration();
            #endregion VLT ADDED CODE

            Session[SessionKey.IsSuppleNewAccount.ToString()] = false;
            Session[SessionKey.isSuperAdmin.ToString()] = Roles.IsUserInRole("MREG_SV");

            Session[SessionKey.CompanyInfoResponse.ToString()] = null;
            Session[SessionKey.isFromBRNSearch.ToString()] = null;

            Session["RegMobileReg_BiometricVerify"] = null;
            Session["RegMobileReg_BiometricID"] = null;
            Session[SessionKey.BiometricDesc.ToString()] = null;

            Session["OutStandingAmount"] = null;
            Session["OutStandingAcctDtls"] = null;
            Session["IDCardTypeKenanCode"] = null;

            if (Roles.IsUserInRole("MREG_CH") && Roles.IsUserInRole("MREG_SK"))
            {
            }
            else if (Roles.IsUserInRole("MREG_C") || Roles.IsUserInRole("MREG_W") || Roles.IsUserInRole("MREG_U") || Roles.IsUserInRole("MREG_R")
                                || Roles.IsUserInRole("MREG_RA") || Roles.IsUserInRole("MREG_TL") || Roles.IsUserInRole("SMRT_USER"))
            {
            }
            else
            {
                if (Roles.IsUserInRole("MREG_CH"))
                {
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.Cashier);
                    Response.Redirect("~/Registration/Cashier");
                }
                if (!Roles.IsUserInRole("MREG_DSV") && Roles.IsUserInRole("MREG_SK"))
                {
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.StoreKeeper);
                    Response.Redirect("~/Registration/StoreKeeper");       

                }

                if (Roles.IsUserInRole("MREG_DIC") || Roles.IsUserInRole("MREG_DSK"))
                {
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.StoreKeeper);
                    Response.Redirect("~/Registration/StoreKeeper");         
                }
                if (Roles.IsUserInRole("MREG_DCH"))
                {
                    if (!Session["IsDealer"].ToBool())
                    {
                        Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.Cashier);
                        Response.Redirect("~/Registration/Cashier");
                    }
                    else
                    {
                        Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.OrderStatusReport);
                        Response.Redirect("~/Registration/OrderStatusReport");
                    }

                }
                if (!Roles.IsUserInRole("MREG_DSV") && (Roles.IsUserInRole("MREG_CH") || Roles.IsUserInRole("DREG_CH")))
                {
                    //Code Added by VLT for Menu Type on Apr 2 2013
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.Cashier);
                    //end of Code Added by VLT for Menu Type on Apr 2 2013
                    Response.Redirect("~/Registration/Cashier");
                }


            }

            return View();
        }

		public ActionResult TacAuditLog()
		{
			int rowCount = System.Configuration.ConfigurationManager.AppSettings.Get("TacAuditLogRowCount").ToInt();
			TacAuditLogVM tacAuditLogVM = new TacAuditLogVM();

			using (var proxy = new RegistrationServiceProxy())
			{
				tacAuditLogVM.TacAuditLogList = proxy.GetAllTacAuditLog(rowCount);
			}

			return View(tacAuditLogVM);
		}


		public ActionResult ISellKenanLogs(int? regid)
		{
			RegistrationHistory objRegistrationHistory = new RegistrationHistory();

			return View(objRegistrationHistory);
		}

		[HttpPost]
		public ActionResult ISellKenanLogs(FormCollection collection, RegistrationHistory model)
		{
			RegistrationHistory objRegistrationHistory = new RegistrationHistory();
			ReportsServiceProxy reportsProxy = new ReportsServiceProxy();
			int regID = model.orderID;
			
			if (regID == 0)
				return View();

			List<int> objRegistrationIDs = new List<int>();
			objRegistrationIDs.Add(regID);
			objRegistrationHistory.RegIstrationHistoryDetail = reportsProxy.RegHistoryDetails(regID);
			objRegistrationHistory.orderID = regID;

			IEnumerable<int> registrationIDs = (IEnumerable<int>)objRegistrationIDs;
			List<KenanaLogDetails> objKenanHistory = new List<KenanaLogDetails>();
			try
			{
				using (var proxy = new RegistrationServiceProxy())
				{

					objKenanHistory = proxy.KenanLogHistoryDetailsGet(registrationIDs).ToList();
					if (objKenanHistory != null && objKenanHistory.Count > 0)
						objRegistrationHistory.KenanCallHistory = objKenanHistory;
				}
			}
			catch (Exception ex)
			{
				//return RedirectToException(ex);
			}
			return View(objRegistrationHistory);
		}


        public ActionResult Error404()
        {
            return View();
        }

        public ActionResult IndexNew()
        {
            Session[SessionKey.AccountswithContractCount.ToString()] = null;
            //Added by wenhao - (24/oct/2014) - to clear mnp session
            Session[SessionKey.FromMNP.ToString()] = null;
            Session[SessionKey.WriteOffAccts.ToString()] = null;
            Session[SessionKey.TransStartTime.ToString()] = null;
            Session[SessionKey.ContractDetails.ToString()] = null;
            Session[SessionKey.FxInternalCheckMessage.ToString()] = null;
            Session["IsOlo"] = null;
            Session["_componentViewModel"] = null;
            Session["WriteOffHardstop"] = null;
            Session["AllowSimReplacement"] = null;
            Session["AllowSimReplacementForWriteOff"] = null;
            Session["HardStop"] = null;
            Session["BreChecksbyUserID"] = null;
            Session["BreStatusDetails"] = null;
            Session[SessionKey.FailedAcctIds.ToString()] = null;
            Session["isMenuSuccess"] = null;
            Session["isHardStop"] = null;
            Session["BREFailedRules"] = null;
            ClearSessionsALL();
            Session["BusinessCheck"] = null;
            //RST
            Session["AcctStatRST"] = null;
            Session["SearchBy"] = null;
            Session["cardNumber"] = null;
            //end RST
            Session["AccountswithContractCount"] = null;
            Session["HasContract"] = null;
            Session["MNPName"] = null;
            Session[SessionKey.SelectedContractName_Device.ToString()] = null;
            Session[SessionKey.SimType.ToString()] = null;
            Session[SessionKey.FromPromoOffer.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_UOMCode.ToString()] = null;
            Session[SessionKey.ArticleId.ToString()] = null;
            Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;

            ViewBag.Message = "Home Screen";
            ViewBag.SearchBy = "No Search";
            Session[SessionKey.Searchstatus.ToString()] = null;
            Session[SessionKey.MenuType.ToString()] = null;
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = null;
            WebHelper.Instance.ResetDepositSessions();
            Session[SessionKey.RegK2_Status.ToString()] = null;
            Session[SessionKey.SelectedComponents.ToString()] = null;
            Session[SessionKey.AssignNew.ToString()] = null;
            Session[SessionKey.SpendLimit.ToString()] = null;

            Session[SessionKey.SuppleMsisdns.ToString()] = null;
            Session[SessionKey.IsSuppleNewAccount.ToString()] = null;
            Session[SessionKey.isVIP.ToString()] = null;
            #region VLT ADDED CODE
            clearUserRegistration();
            #endregion VLT ADDED CODE

            Session[SessionKey.IsSuppleNewAccount.ToString()] = false;
            Session[SessionKey.isSuperAdmin.ToString()] = Roles.IsUserInRole("MREG_SV");

            Session[SessionKey.CompanyInfoResponse.ToString()] = null;
            Session[SessionKey.isFromBRNSearch.ToString()] = null;

            Session["RegMobileReg_BiometricVerify"] = null;
            Session["RegMobileReg_BiometricID"] = null;
            Session[SessionKey.BiometricDesc.ToString()] = null;

            Session["OutStandingAmount"] = null;
            Session["OutStandingAcctDtls"] = null;
            Session["IDCardTypeKenanCode"] = null;

            Session[SessionKey.MNPCreationStatus.ToString()] = null;
            Session[SessionKey.DedicatedSuppFlow.ToString()] = false;
            Session[SessionKey.NewLineSupplementary.ToString()] = false;
            Session[SessionKey.DirectMISMOrder.ToString()] = false;
			Session["existingTotalLine"] = 0;

            DropFourObj dropObj = new DropFourObj();

			dropObj.existingTotalLine = Session["existingTotalLine"].ToInt();
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
			Session["selectedKenanCode"] = null;
			Session["selectedPackageCode"] = null;

			// this is for initiate back the liberalisation
			Session["AllCustomerDetails"] = null;
			Session["AllServiceDetails"] = null;
			Session["CustomerLiberalisation"] = null;
			Session["CustomerLiberalisationRank"] = null;

			ApprovalModels approvalModel = new ApprovalModels();
			Session[SessionKey.ApprovalModel.ToString()] = approvalModel;

            if (Roles.IsUserInRole("MREG_CH") && Roles.IsUserInRole("MREG_SK"))
            {
            }
            else if (Roles.IsUserInRole("MREG_C") || Roles.IsUserInRole("MREG_W") || Roles.IsUserInRole("MREG_U") || Roles.IsUserInRole("MREG_R")
                                || Roles.IsUserInRole("MREG_RA") || Roles.IsUserInRole("MREG_TL") || Roles.IsUserInRole("SMRT_USER") || Roles.IsUserInRole("MREG_DSA"))
            {
            }
            else
            {
                if (Roles.IsUserInRole("MREG_CH"))
                {
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.Cashier);
                    Response.Redirect("~/Registration/Cashier");
                }
                if (!Roles.IsUserInRole("MREG_DSV") && Roles.IsUserInRole("MREG_SK"))
                {
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.StoreKeeper);
                    Response.Redirect("~/Registration/StoreKeeper");
                }

                if (Roles.IsUserInRole("MREG_DIC") || Roles.IsUserInRole("MREG_DSK"))
                {
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.StoreKeeper);
                    Response.Redirect("~/Registration/StoreKeeper");         
                }
                if (Roles.IsUserInRole("MREG_DCH"))
                {
                    if (!Session["IsDealer"].ToBool())
                    {
                        Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.Cashier);
                        Response.Redirect("~/Registration/Cashier");
                    }
                    else
                    {
                        Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.OrderStatusReport);
                        Response.Redirect("~/Registration/OrderStatusReport");
                    }
                }
                if (!Roles.IsUserInRole("MREG_DSV") && (Roles.IsUserInRole("MREG_CH") || Roles.IsUserInRole("DREG_CH")))
                {
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.Cashier);
                    Response.Redirect("~/Registration/Cashier");
                }
                if (Roles.IsUserInRole("MREG_DAPP"))
                {
                    Session[SessionKey.MenuType.ToString()] = Convert.ToInt32(MenuType.CDPUDashBoard);
                    Response.Redirect("~/Registration/CDPUDashBoard?RefreshSession=1");
                }

            }
            return View();
        }

        [Authorize]
        [HttpPost, ValidateInput(false)]
        public ActionResult IndexNew(FormCollection collection)
        {
            string IDCardTypeID = string.Empty;
            string IDCardNo = string.Empty;
            ViewBag.SearchBy = null;
            //ViewBag.SearchNumber = collection["idCardNo"].ToString();
            //RST
            if ( !ReferenceEquals(Session["cardNumber"], null) && !ReferenceEquals(Session["cardNumber"], ""))
		        {
                    if (!ReferenceEquals(collection["idCardNo"].ToString2(), null) && !ReferenceEquals(collection["idCardNo"].ToString2(), ""))
			        {
                        if (!ReferenceEquals(Session["cardNumber"], collection["idCardNo"].ToString2()))
                        {
                            ViewBag.SearchNumber = collection["idCardNo"].ToString2();
                            ViewBag.SearchBy = null;
                        }
                        else
                        {
                            ViewBag.SearchNumber = Session["cardNumber"].ToString2();
                            ViewBag.SearchBy = Session["SearchBy"].ToString2();
                        }
			        }
			        else
			        {
				        ViewBag.SearchNumber = Session["cardNumber"].ToString2();
                        ViewBag.SearchBy = Session["SearchBy"].ToString2();
			        }
			        Session["cardNumber"] = ViewBag.SearchNumber;
		        }
		        else
		        {
			        ViewBag.SearchNumber = collection["idCardNo"].ToString();
			        Session["cardNumber"] = ViewBag.SearchNumber;
		        }
            //End RST

            //Clear session to cater for Back button
            DropFourObj dropObj = new DropFourObj();
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
			Session[SessionKey.DedicatedSuppFlow.ToString()] = false;
			Session[SessionKey.NewLineSupplementary.ToString()] = false;
			Session[SessionKey.KenanACNumber.ToString()] = null;
            Session[SessionKey.AccExternalID.ToString()] = null;
            Session[SessionKey.VIP.ToString()] = null;
            Session[SessionKey.WO.ToString()] = null;
            Session[SessionKey.IDCardTypeName.ToString()] = null;
            Session["HardStop"] = null;
            Session["WriteOffHardstop"] = null;
            Session["AllowSimReplacementForWriteOff"] = null;
            //Session["AllowSimReplacement"] = null; // moved to the landing page
            Session["SelectedPlanID"] = null;//ram
            //Session["BreStatusDetails"] = null;
            //Session[SessionKey.FailedAcctIds.ToString()] = null;
            WebHelper.Instance.clearApprovalCheckboxes();
            Session["model"] = null;
            Session["modelCRP"] = null;
            Session["Penalty"] = null;
            Session[SessionKey.PenaltyWaiveOff.ToString()] = null;
            Session[SessionKey.AccExternalID.ToString()] = null;

            WebHelper.Instance.ClearRegistrationSession();
            WebHelper.Instance.ClearSessions();
            WebHelper.Instance.ClearSubLineSession();
            retrieveAcctListByICResponse AcctListByIC = null;
            List<Online.Registration.Web.SubscriberICService.Items> AccntList = null;
            String SelectAccountDtls = String.Empty;
            String accntId = String.Empty;
            String susbcrNo = String.Empty;
            String susbcrNoResets = String.Empty;
            String serviceStatus = String.Empty;
            String isMISM = String.Empty;
            String princiSuppLine = String.Empty;
            String checkedState = String.Empty;
            String externalId = String.Empty;
            String accntIntId = String.Empty;
            String category = String.Empty;
            String MarketCode = String.Empty;
            string accountStatus = String.Empty;

            try
            {
                if (string.IsNullOrEmpty(collection["submit1"].ToString2())) return View();

                string submittype = collection["submit1"].ToString();

                // validation for ID
                //IDCardTypeID = collection["Customer.IDCardTypeID"] != null ? Convert.ToString(collection["Customer.IDCardTypeID"].Split(',')[1]) : Convert.ToString(collection["idCardTypeIDINT"]);
                //IDCardNo = collection["Customer.IDCardNo"] != null ? Convert.ToString(collection["Customer.IDCardNo"]) : Convert.ToString(collection["idCardNo"]);
                IDCardTypeID = collection["Customer.IDCardTypeID"] != null ? Convert.ToString(collection["Customer.IDCardTypeID"].Split(',')[1]) : (collection["idCardTypeIDINT"] != null && collection["idCardTypeIDINT"] != "" ? Convert.ToString(collection["idCardTypeIDINT"]) : Session["SearchBy"].ToString2());
                IDCardNo = collection["Customer.IDCardNo"] != null ? Convert.ToString(collection["Customer.IDCardNo"]) : (collection["idCardNo"] != null && collection["idCardNo"] != "" ? Convert.ToString(collection["idCardNo"]) : Session["cardNumber"].ToString2());

                // newIC = 1
                if (IDCardTypeID.Equals("1"))
                {
                    if (!Util.isValidNewNRIC(IDCardNo))
                    {
                        ViewBag.ICValidationError = "Please enter a valid IC number";
                        Session["PPID"] = string.Empty;
                        return View();
                    }
                }

                ViewBag.SearchValue = IDCardNo;
                if (IDCardTypeID != "MSISDN")
                {
                    // 6 = msisdn || 1001 account || other means NRIC
                    if (IDCardTypeID.Equals("6"))
                    {
                        ViewBag.SearchBy = "MSISDN";
                    }
                    else if (IDCardTypeID.Equals("1001"))
                    {
                        ViewBag.SearchBy = "account";
                    }
                    else
                    {
                        ViewBag.SearchBy = "NRIC";
                    }
                }
                else
                {
                    ViewBag.SearchBy = "MSISDN";
                }

                //RST
                Session["SearchBy"] = ViewBag.SearchBy;
                Session["cardNumber"] = ViewBag.SearchValue;
                //End RST

                String selectedExternalId = collection["SelectExternalId"].ToString();
				Session["SelectedMsisdn"] = collection["SelectExternalId"].ToString();
                if (Session["AcctAllDetails"] != null)
                {
                    AcctListByIC = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                    AccntList = AcctListByIC != null ? AcctListByIC.itemList != null ? AcctListByIC.itemList.Where(c => c.ExternalId == selectedExternalId).ToList() : null : null;
                    if (AccntList.Count > 0)
                    {
                        accntId = AccntList[0].AcctExtId;
                        susbcrNo = AccntList[0].SusbcrNo;
                        susbcrNoResets = AccntList[0].SusbcrNoResets;
                        serviceStatus = AccntList[0].ServiceInfoResponse.serviceStatus;
                        isMISM = Convert.ToString(AccntList[0].IsMISM);
                        princiSuppLine = AccntList[0].ServiceInfoResponse.prinSuppInd;
                        externalId = AccntList[0].ExternalId;
                        accntIntId = AccntList[0].AcctIntId;
                        checkedState = !ReferenceEquals(AccntList[0].Account,null) ? (AccntList[0].Account.IDType == "MSISDN") ? "true" : "false" : "false";
                        category = AccntList[0].AccountDetails != null ? AccntList[0].AccountDetails.AcctCategory.ToString2() : string.Empty;
                        MarketCode = AccntList[0].AccountDetails != null ? AccntList[0].AccountDetails.MktCode.ToString2() : string.Empty;
                        accountStatus = AccntList[0].ServiceInfoResponse != null ? AccntList[0].ServiceInfoResponse.serviceStatus : string.Empty;
                    }
                }

                switch (submittype)
                {

                    case "btnAddContract":

                        //SelectAccountDtls = accntId + "," + susbcrNo + "," + susbcrNoResets + "," + serviceStatus + "," + isMISM + ","
                        //    + princiSuppLine + "," + checkedState;

                        SelectAccountDtls = accntId + "," + externalId + "," + susbcrNo + "," + susbcrNoResets + "," + accountStatus + "," + isMISM + "," + princiSuppLine + "," + checkedState;
                        Session[SessionKey.MSISDN.ToString()] = Session[SessionKey.ExternalID.ToString()] = externalId;
                        ApprovalModels appr = new ApprovalModels();
                        Session["ApprovalModel"] = appr;
						Session["SelectAccountDtls"] = SelectAccountDtls.ToString2();
                        Session["NoRegistrationForCRP"] = (int)MobileRegType.Contract;
                        Session["model"] = null;
                        return RedirectToAction("ContractCheck", "AddContract", new { SelectAccountDtls = SelectAccountDtls });
                        break;

                    case "btnAddCRP":

                        /*
                        2347 & 2348 - fix for not populating the principle account during CRP - Start
                        */

                        SelectAccountDtls = accntId + "," + externalId + "," + susbcrNo + "," + susbcrNoResets + "," + accountStatus + "," + isMISM + "," + princiSuppLine + "," + checkedState;
                        
                        /*
                        2347 & 2348 - fix for not populating the principle account during CRP - End
                        */
						Session["NoRegistrationForCRP"] = (int)MobileRegType.CRP;
						Session["SelectAccountDtls"] = SelectAccountDtls.ToString2();
                        Session["GetPrinPlanDetails"] = SelectAccountDtls.ToString2();
                        Session[SessionKey.MSISDN.ToString()] = Session[SessionKey.ExternalID.ToString()] = externalId;
                        Session[SessionKey.SimType.ToString()] = isMISM;
						Session["AccountType"] = princiSuppLine;
                        Session[SessionKey.KenanACNumber.ToString()] = accntId.ToString();
                        using (var proxy = new UserServiceProxy())
                        {
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.DeviceCRP), "Select Account", Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                        }
                        Session[SessionKey.AccountCategory.ToString()] = category;
                        Session[SessionKey.MarketCode.ToString()] = MarketCode;
                        Session["sMsisdn"] = externalId.ToString2();
                        Session["modelCRP"] = null;
                        return RedirectToAction("ContractCheck", "CRP", new
                        {
                            KenanID = accntId,
                            MSISDN = externalId,
                            SubscriberNo = susbcrNo,
                            SubscriberNoResets = susbcrNoResets
                            ,
                            Ismism = isMISM
                        });
                        break;

                    case "btnAddNewLine":
                        string[] strAccountDtls = SelectAccountDtls.Split(',');
                        Session[SessionKey.FxAccNo.ToString()] = accntId.ToString2();
                        Session[SessionKey.ExternalID.ToString()] = strAccountDtls[1].ToString();
                        Session[SessionKey.AccExternalID.ToString()] = strAccountDtls[2].ToString();
                        Session[SessionKey.KenanACNumber.ToString()] = strAccountDtls[3].ToString();
                        Session[SessionKey.IsMISM.ToString()] = strAccountDtls[4].ToString();

                        Session[SessionKey.AccountCategory.ToString()] = category;
                        Session[SessionKey.MarketCode.ToString()] = MarketCode;
                        Session["SMEIPARENTID"] = strAccountDtls[7];

                        return RedirectToAction("SecondaryLinePlan", "NewLine", new { type = (int)MobileRegType.SecPlan });
                        break;
                    case "btnAddSuppline":
                        Session[SessionKey.IsSuppleNewAccount.ToString()] = false;
                        Session["IsDeviceRequired"] = collection["hdnIsDeviceRequired"].ToString2();
                        Session["FxAccNo"] = accntIntId.ToString2();
                        Session["ExternalID"] = externalId.ToString2();
                        Session["AccExternalID"] = accntId.ToString2();
                        Session["KenanACNumber"] = accntId.ToString2();
                        Session[SessionKey.AccountCategory.ToString()] = category;
                        Session[SessionKey.MarketCode.ToString()] = MarketCode;
                        Session[SessionKey.DedicatedSuppFlow.ToString()] = true;

                        if (Session["IsDeviceRequired"].ToString2() == "Yes")
                        {
                            return RedirectToAction("SelectDevicePlan", "SuppLine", new { type = (int)MobileRegType.DevicePlan });
                        }
                        else
                        {
                            return RedirectToAction("SuppLinePlanNew", "SuppLine", new { type = (int)MobileRegType.SuppPlan });
                        }
                        break;
                    case "btnAddSupplineMNP":
                        Session[SessionKey.IsSuppleNewAccount.ToString()] = false;
                        Session["IsDeviceRequired"] = collection["hdnIsDeviceRequired"].ToString2();
                        Session["FxAccNo"] = accntIntId.ToString2();
                        Session["ExternalID"] = externalId.ToString2();
                        Session["AccExternalID"] = accntId.ToString2();
                        Session["KenanACNumber"] = accntId.ToString2();
                        Session[SessionKey.AccountCategory.ToString()] = category;
                        Session[SessionKey.MarketCode.ToString()] = MarketCode;
                        Session[SessionKey.DedicatedSuppFlow.ToString()] = true;
                        SelectAccountDtls = accntIntId + "," + externalId + "," + accntId + "," + accntId + "," + isMISM + "," + accountStatus + "," + string.Empty + "," + category + "," + MarketCode;
                        
                        return RedirectToAction("EnterDetailsNew", "MNP", new { type = (int)MobileRegType.SuppPlan, SelectAccountDtls = SelectAccountDtls });
                        break;

                    case "btnAddMultiSim":
                        Util.IsSecLineNewAccount = (collection["hdnIsSeclineNew"].ToString2() == "1" ? false : true);
                        Session["FxAccNo"] = accntIntId.ToString2();
                        Session["ExternalID"] = externalId.ToString2();
                        Session["AccExternalID"] = accntId.ToString2();
                        Session["KenanACNumber"] = accntId.ToString2();
                        Session[SessionKey.AccountCategory.ToString()] = category;
                        Session[SessionKey.MarketCode.ToString()] = MarketCode;
                        Session["Secondary"] = "SecondaryPlan";
                        Session["SelectedMsisdn"] = selectedExternalId;

                       //return RedirectToAction("SecondaryLinePlan", "SuppLine", new { type = (int)MobileRegType.SecPlan });
                        return RedirectToAction("SecondaryLineRegSummaryNew", "SuppLine");
                        //return RedirectToAction("MobileRegSummaryNew", "NewLine");
                        break;

                    case "whitelist":
                        return RedirectToAction("WhiteListCustomers", "WhiteListSMECI", new { area = "Admin" });
                        break;
                    case "plan":
                        if (Session[SessionKey.PPIDCheck.ToString()] != null)
                        {
                            Session[SessionKey.PPIDCheck.ToString()] = null;
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            Session[SessionKey.MenuType.ToString()] = (int)MenuType.MobileRegistration;

                            if (Session[SessionKey.PPID.ToString()] != null)
                            {
                                string validationCheckType = (Session[SessionKey.PPID.ToString()].ToString() == "E" ? "E" : "N");
                                var context = ControllerContext.HttpContext.ApplicationInstance.Context;

                                Session[SessionKey.FailedAcctIds.ToString()] = null;
                                WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), validationCheckType);                           
                            }

                            if (Session["isOnlyPREGSMSIMReplace"] != null && Session["isOnlyPREGSMSIMReplace"].ToString2() == "Y" && !Roles.IsUserInRole("MREG_NEW"))
                            {
                                return RedirectToAction("AccountDetails", "QSIMReplacement", new { type = 1, Area = "" });
                            }
                            else if (Roles.IsUserInRole("MREG_C") || Roles.IsUserInRole("MREG_W") && (Roles.IsUserInRole("MREG_CRP") || Roles.IsUserInRole("MREG_SIMR") || Roles.IsUserInRole("MREG_ARVAS")))
                            {
                                return RedirectToAction("SelectPlan", "Registration", new { type = (int)MobileRegType.PlanOnly });
                            }
                            else if (Roles.IsUserInRole("MREG_CRP") || Roles.IsUserInRole("MREG_SIMR") || Roles.IsUserInRole("MREG_ARVAS"))
                            {
                                return RedirectToAction("AccountDetails", "QSIMReplacement", new { type = 1, Area = "" });
                            }
                            else if (Roles.IsUserInRole("MREG_C") || Roles.IsUserInRole("MREG_W"))
                            {
                                return RedirectToAction("SelectPlan", "Registration", new { type = (int)MobileRegType.PlanOnly });
                            }
                        }
                        break;
                    case "reports":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.Reports;
                        return RedirectToAction("ReportsMain", "Reports");
                        break;
                    case "storekeeper":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.StoreKeeper;
                        return RedirectToAction("StoreKeeper", "Registration");
                    case "cashier":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.Cashier;
                        return RedirectToAction("Cashier", "Registration");
                    case "orderstatusreport":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.OrderStatusReport;
                        return RedirectToAction("OrderStatusReport", "Registration");
                    case "teamleader":
                        if (Roles.IsUserInRole("MREG_TL"))
                        {
                            Session[SessionKey.MenuType.ToString()] = (int)MenuType.TeamLeader;
                            return RedirectToAction("StoreKeeper", "Registration");
                        }
                        break;
                    case "smart":
                        if (collection["hdnaccountSelected"].ToString().Split('|').Length > 1)
                        {
                            Session[SessionKey.CMSID.ToString()] = "Y";
                            string[] par = collection["hdnaccountSelected"].ToString().Split('|');
                            Session[SessionKey.MSISDN.ToString()] = Session[SessionKey.ExternalID.ToString()] = par[1];
                            return RedirectToAction("CmssAndContractCheck", "Retention", new { KenanID = par[0], MSISDN = par[1], SubscriberNo = par[2], SubscriberNoResets = par[3], Area = "Retention" });
                        }
                        break;
                    case "dashboard":
                        if (Roles.IsUserInRole("MREG_DAB") || Roles.IsUserInRole("MREG_HQ"))
                        {
                            Session[SessionKey.MenuType.ToString()] = (int)MenuType.DashBoard;

                            return RedirectToAction("SuperVisorForWaiveOff", "Registration", new { RefreshSession = "1", Area = "" });
                        }
                        break;
                        case "guidedsales":
                            Session[SessionKey.MenuType.ToString()] = (int)MenuType.MobileRegistration;

                            #region Added by Rajeswari
                            if (Session[SessionKey.PPID.ToString()] != null)
                            {
                                if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                                {
                                    WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "E");
                                }
                            }
                            #endregion
                            return RedirectToAction("Index", "guidedsales");
                            break;
                    case "home":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.HomeRegistration;

                        if (Util.SessionOrgTypeCode == Properties.Settings.Default.OrgType_HSOM)
                        {
                            return RedirectToAction("SearchRegistration", "HomeRegistration");
                        }
                        else if (Util.SessionOrgTypeCode == Properties.Settings.Default.OrgType_Dealer)
                        {
                            return RedirectToAction("BlacklistChecking", "HomeRegistration");
                        }
                        else if (Util.SessionOrgTypeCode == Properties.Settings.Default.OrgType_Center)
                        {
                            return RedirectToAction("BlacklistChecking", "HomeRegistration");
                        }
                        break;
                    case "Complaint":
                        if (SessionManager.Get("PPIDInfo") != null)
                            return RedirectToAction("Create", "Complaint");
                        break;
                    case "search":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.SearchOrder;
                        return RedirectToAction("SearchRegistration", "Registration");
                        break;
                    case "searchCust":
                        var model = new InquiryAccountSummaryModel();
                        WebHelper.Instance.PopulateAccountInfo(model, collection["txtMSISDN"].ToString2());
                        return View(model);
                        break;
                    case "searchCustIDNumber":
                        Session["WrfAlertMessage"] = null;
                        return SearchCustId(collection);
                        break;
                    case "mnp":
                        Session[SessionKey.MenuType.ToString()] = (int)MenuType.MNPStatusTrack;
                        if (Session[SessionKey.PPID.ToString()] != null)
                        {
                            if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                            {
                                WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "E");
                            }
                            else
                            {
                                WebHelper.Instance.BusinessValidationChecks(null, this.GetType(), "N");
                            }
                        }
                        return RedirectToAction("EnterDetails", "MNP");
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
            }


            return View();
        }


        
    }

}
