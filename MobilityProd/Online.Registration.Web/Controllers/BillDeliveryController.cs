﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.EBPSSvc;
using SNT.Utility;
using System.Configuration;
using Online.Registration.Web.CommonEnum;
using log4net;
using System.Web.Script.Serialization;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.ContentRepository;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.Properties;
using Online.Registration.DAL;


namespace Online.Registration.Web.Controllers
{
    [SessionAuthenticationAttribute]
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class BillDeliveryController : Controller
    {
        #region Member Declaration
        private static readonly ILog Logger = LogManager.GetLogger(typeof(RegistrationController));
        #endregion

        [Authorize]
        [HttpGet]
        [DoNoTCache]
        public ActionResult AccountsList()
        {

            retrieveAcctListByICResponse AcctListByICResponse = null;
            SupplementaryListAccounts SuppListAccounts = null;
            var isUserCCCAbove = WebHelper.Instance.IsUserCCCAbove();

            if ((!ReferenceEquals(Session[SessionKey.PPID.ToString()], null) && "E".Equals(Convert.ToString(Session[SessionKey.PPID.ToString()]))) && !ReferenceEquals(Session[SessionKey.PPIDInfo.ToString()], null))
            {
                ///GET PPIDInfo FROM SESSION
                AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()];
                string strMISMEmfConfigId = ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2();

                string supAccountStatus = string.Empty;

                if (AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse != null && (c.ServiceInfoResponse.lob == "POSTGSM") && (c.ServiceInfoResponse.serviceStatus.ToUpper() == "A" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "S" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "B" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "D" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "T")).Count() > 0)
                {
                    SuppListAccounts = new SupplementaryListAccounts();
                    List<string> lstMsisdnList = new List<string>();
                    Dictionary<string, List<string>> msisdnListByAccount = new Dictionary<string, List<string>>();


                    foreach (var v in AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse != null && c.ServiceInfoResponse.prinSuppInd != "" && (c.ServiceInfoResponse.lob == "POSTGSM") && c.EmfConfigId != strMISMEmfConfigId && (c.ServiceInfoResponse.serviceStatus.ToUpper() == "A" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "S" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "B")).ToList())
                    {
                        //bool is2GBCompExistOnPkg = false;
                        //int PackageKenanCode = 0;
                        //bool isPkgExists = false;
                        //string AccountPackageName = string.Empty;
                        
                        //2015.07.27 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - Start
                        if (Util.SessionAccess.User.isDealer && v.AccountDetails != null && (
                            v.AccountDetails.MktCode == ConfigurationManager.AppSettings["CIParentMarketCode"].ToString2() ||
                            v.AccountDetails.MktCode == ConfigurationManager.AppSettings["SMEIParentMarketCode"].ToString2()))
                        {
                            continue;
                        }
                        //2015.07.27 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - End

                        if (Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString2() == "6" && (Session["IsDealer"].ToBool() || isUserCCCAbove == false)) //MSISDN Search only
                        {
                            #region "MSISDN Search Only Flow "
                            var prinExternalID = string.Empty;
                            if (Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString() == v.ExternalId)
                            {
                                if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "S")
                                {
                                    //Get the MISM primary msisdn 
                                    using (var PrinSupproxy = new PrinSupServiceProxy())
                                    {
                                        retrieveMismDetlsResponse primSecResponse = PrinSupproxy.getPrimSecondaryines(v.ExternalId);
                                        if (primSecResponse != null && primSecResponse.itemList.ToList().Count > 0)
                                        {
                                            prinExternalID = primSecResponse.itemList.Where(a => a.PrimSecInd == "P").FirstOrDefault().Msisdn;
                                        }
                                    }
                                }

                                SuppListAccounts.SuppListAccounts.Add(
                                    new AddSuppInquiryAccount
                                    {
                                        AccountNumber = v.AcctExtId,
                                        ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                        Category = v.Account != null ? v.Account.Category : string.Empty,
                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                        MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                        SubscribeNo = v.SusbcrNo,
                                        SubscribeNoResets = v.SusbcrNoResets,
                                        externalId = v.ExternalId,
                                        accountExtId = v.AcctExtId,
                                        accountIntId = v.AcctIntId,
                                        AccountName = v.ServiceInfoResponse.lob,
                                        AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                        IsMISM = v.IsMISM,
                                        // SimSerial = v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "S" ? GetsimSerial(v.ExternalId) : string.Empty,
                                        AccountType = !string.IsNullOrEmpty(v.ServiceInfoResponse.prinSuppInd) ? v.ServiceInfoResponse.prinSuppInd : "P",
                                        PrinMsisdn = !string.IsNullOrEmpty(prinExternalID) ? prinExternalID : string.Empty,
                                        //IsGreater2GBCompExists = is2GBCompExistOnPkg,
                                        //IsPackagesExistsOnAccountLine = isPkgExists,
                                        //AccountKenanCode = PackageKenanCode.ToString2(),
                                        //AccountPackageName = AccountPackageName,
                                        HasSupplyAccounts = v.PrinSuppResponse != null & v.PrinSuppResponse.itemList.Count > 0 ? true : false

                                    }
                                );

                                if (msisdnListByAccount.ContainsKey(v.AcctExtId)) msisdnListByAccount[v.AcctExtId].Add(v.ExternalId);
                                else msisdnListByAccount.Add(v.AcctExtId, new List<string> { v.ExternalId });

                                if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "P" && v.PrinSupplimentaryResponse != null && v.PrinSupplimentaryResponse.itemList.Count > 0)
                                {

                                    foreach (var supp in v.PrinSupplimentaryResponse.itemList)
                                    {
                                        if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                        {
                                            if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                supAccountStatus = "S";
                                            else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                supAccountStatus = "D";
                                            else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                supAccountStatus = "T";
                                            else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                supAccountStatus = "B";
                                            else
                                                supAccountStatus = "A";
                                        }
                                        string accountNumber = string.Empty;
                                        var suppAccount = AcctListByICResponse.itemList.Where(s => s.ExternalId == supp.msisdnField && s.ServiceInfoResponse.prinSuppInd == "S" && !s.IsMISM).FirstOrDefault();
                                        accountNumber = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId;

                                    }

                                }

                            }
                            #endregion
                        }
                        else
                        {
                            if (!v.IsMISM)
                            {
                                if (v.ServiceInfoResponse.prinSuppInd != "S" && v.ServiceInfoResponse.prinSuppInd != "" && !v.IsMISM)
                                {
                                    if (!lstMsisdnList.Contains(v.ExternalId))
                                    {
                                        lstMsisdnList.Add(v.ExternalId);

                                        if (msisdnListByAccount.ContainsKey(v.AcctExtId)) msisdnListByAccount[v.AcctExtId].Add(v.ExternalId);
                                        else msisdnListByAccount.Add(v.AcctExtId, new List<string> { v.ExternalId });

                                        //Add only Principal Lines
                                        SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = v.AcctExtId,
                                                        ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        Category = v.Account != null ? v.Account.Category : string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = v.SusbcrNo,
                                                        SubscribeNoResets = v.SusbcrNoResets,
                                                        externalId = v.ExternalId,
                                                        accountExtId = v.AcctExtId,
                                                        accountIntId = v.AcctIntId,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                                        IsMISM = v.IsMISM,
                                                        //  SimSerial = string.Empty,
                                                        AccountType = "P",
                                                        PrinMsisdn = string.Empty,
                                                        //IsGreater2GBCompExists = is2GBCompExistOnPkg,
                                                        //AccountKenanCode = PackageKenanCode.ToString2(),
                                                        //IsPackagesExistsOnAccountLine = isPkgExists,
                                                        //AccountPackageName = AccountPackageName,
                                                        HasSupplyAccounts = false
                                                    });
                                    }
                                    if (v.PrinSuppResponse != null && v.PrinSuppResponse.itemList != null && v.PrinSuppResponse.itemList.Count > 0)
                                    {
                                        #region "Supplimentary Accounts"
                                        foreach (var supp in v.PrinSuppResponse.itemList)
                                        {
                                            if (v.ExternalId != supp.msisdnField)
                                            {
                                                if (!lstMsisdnList.Contains(supp.msisdnField))
                                                {
                                                    var suppAccount = AcctListByICResponse.itemList.Where(i => i.ExternalId == supp.msisdnField && i.ServiceInfoResponse != null && i.ServiceInfoResponse.prinSuppInd == "S" && !i.IsMISM).FirstOrDefault();
                                                    //SuppListAccounts.SuppListAccounts.Where(A => A.externalId == v.ExternalId) = SuppListAccounts.SuppListAccounts.Where(a => a.externalId == v.ExternalId).Select(s => { s.HasSupplyAccounts = true; return s; }).ToList();
                                                    foreach (var item in SuppListAccounts.SuppListAccounts.Where(a => a.externalId == v.ExternalId))
                                                    {
                                                        item.HasSupplyAccounts = true;
                                                    }
                                                    if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                                    {
                                                        if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                            supAccountStatus = "S";
                                                        else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                            supAccountStatus = "D";
                                                        else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                            supAccountStatus = "T";
                                                        else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                            supAccountStatus = "B";
                                                        else
                                                            supAccountStatus = "A";
                                                    }
                                                    lstMsisdnList.Add(supp.msisdnField);

                                                    string tempAccountNumber = (suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId)) ? suppAccount.AcctExtId : v.AcctExtId;
                                                    if (msisdnListByAccount.ContainsKey(tempAccountNumber)) msisdnListByAccount[tempAccountNumber].Add(supp.msisdnField);
                                                    else msisdnListByAccount.Add(tempAccountNumber, new List<string> { supp.msisdnField });

                                                    SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = tempAccountNumber,
                                                        ActiveDate = string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        Category = string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        //  Holder = v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = supp.subscr_noField,
                                                        SubscribeNoResets = supp.subscr_no_resetsField,
                                                        externalId = supp.msisdnField,
                                                        accountExtId = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId, //string.IsNullOrEmpty(supp.acc_AccExtId) ? v.AcctExtId : supp.acc_AccExtId,
                                                        accountIntId = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctIntId) ? suppAccount.AcctIntId : supp.acct_noField,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        //Added by Rajender SIMReplacement Issue SIMReplacement Issue
                                                        AccountStatus = supAccountStatus, //v.ServiceInfoResponse.serviceStatus,
                                                        IsMISM = v.IsMISM,
                                                        // SimSerial = string.Empty,
                                                        AccountType = "S",
                                                        PrinMsisdn = v.ExternalId != supp.msisdnField ? v.ExternalId : string.Empty
                                                        //IsGreater2GBCompExists = is2GBCompExistOnPkg,
                                                        //AccountKenanCode = PackageKenanCode.ToString2(),
                                                        //IsPackagesExistsOnAccountLine = isPkgExists,
                                                        //AccountPackageName = AccountPackageName
                                                    });
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }

                            }
                            else
                            {
                                //Listing the MISM LIST
                                #region "MISM Accounts"
                                if ((v.ServiceInfoResponse.prinSuppInd == "P" || v.ServiceInfoResponse.prinSuppInd != string.Empty) && v.IsMISM)
                                {
                                    if (!lstMsisdnList.Contains(v.ExternalId))
                                    {
                                        lstMsisdnList.Add(v.ExternalId);

                                        if (msisdnListByAccount.ContainsKey(v.AcctExtId)) msisdnListByAccount[v.AcctExtId].Add(v.ExternalId);
                                        else msisdnListByAccount.Add(v.AcctExtId, new List<string> { v.ExternalId });

                                        //Adding primary  line for MISM

                                        //IList<Online.Registration.Web.SubscriberICService.PackageModel>
                                        //                      packages = icserviceobj.retrievePackageDetls(v.ExternalId
                                        //               , System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"]
                                        //               , userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName
                                        //               , password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword
                                        //               , postUrl: "");

                                        //IList<PackageModel> unifiedPackages = packages.GroupBy(a => a.PackageID).Select(a => a.Last()).ToList();
                                        //if (unifiedPackages != null & unifiedPackages.Count > 0)
                                        //{
                                        //    isPkgExists = true;
                                        //    PackageKenanCode = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageID).FirstOrDefault().ToInt();
                                        //    AccountPackageName = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageDesc).FirstOrDefault().ToString2();
                                        //}
                                        SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = v.AcctExtId,
                                                        ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        Category = v.Account != null ? v.Account.Category : string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = v.SusbcrNo,
                                                        SubscribeNoResets = v.SusbcrNoResets,
                                                        externalId = v.ExternalId,
                                                        accountExtId = v.AcctExtId,
                                                        accountIntId = v.AcctIntId,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                                        IsMISM = v.IsMISM,
                                                        // SimSerial = string.Empty,
                                                        AccountType = "P",
                                                        PrinMsisdn = string.Empty
                                                        //IsGreater2GBCompExists = false,
                                                        //AccountKenanCode = PackageKenanCode.ToString2(),
                                                        //IsPackagesExistsOnAccountLine = isPkgExists,
                                                        //AccountPackageName = AccountPackageName
                                                    });
                                    }
                                }

                                if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "P" && v.PrinSupplimentaryResponse != null && v.PrinSupplimentaryResponse.itemList.Count > 0)
                                {

                                    foreach (var supp in v.PrinSupplimentaryResponse.itemList)
                                    {
                                        if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                        {
                                            if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                supAccountStatus = "S";
                                            else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                supAccountStatus = "D";
                                            else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                supAccountStatus = "T";
                                            else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                supAccountStatus = "B";
                                            else
                                                supAccountStatus = "A";
                                        }

                                        string accountNumber = string.Empty;
                                        var suppAccount = AcctListByICResponse.itemList.Where(s => s.ExternalId == supp.msisdnField && s.ServiceInfoResponse.prinSuppInd == "S" && !s.IsMISM).FirstOrDefault();
                                        accountNumber = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId;

                                        if (msisdnListByAccount.ContainsKey(accountNumber)) msisdnListByAccount[accountNumber].Add(v.ExternalId);
                                        else msisdnListByAccount.Add(accountNumber, new List<string> { v.ExternalId });

                                        SuppListAccounts.SuppListAccounts.Add(
                                           new AddSuppInquiryAccount
                                           {
                                               AccountNumber = accountNumber,// v.AcctExtId,
                                               ActiveDate = string.Empty,
                                               Address = v.Account != null ? v.Account.Address : string.Empty,
                                               Category = string.Empty,
                                               CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                               Holder = supp.cust_nmField, //v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                               IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                               IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                               MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                               Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                               SubscribeNo = supp.subscr_noField,
                                               SubscribeNoResets = supp.subscr_no_resetsField,
                                               externalId = supp.msisdnField,
                                               accountExtId = v.AcctExtId,
                                               accountIntId = supp.acct_noField,
                                               AccountName = v.ServiceInfoResponse.lob,
                                               AccountStatus = supAccountStatus, //v.ServiceInfoResponse.serviceStatus,
                                               IsMISM = supp.IsMISM,
                                               //SimSerial = supp.PrimSecInd == "S" ? GetsimSerial(supp.Msisdn) : string.Empty,
                                               AccountType = "S",
                                               PrinMsisdn = v.ExternalId // != supp.Msisdn ? v.ExternalId : string.Empty
                                               //IsGreater2GBCompExists = is2GBCompExistOnPkg
                                           });
                                    }

                                }
                                #endregion

                            }
                        }

                    }
                    Session[SessionKey.billDelivery_msisdnListByAccount.ToString()] = msisdnListByAccount;
                }

            }
            return View(SuppListAccounts);
        }

        [HttpPost]
        [Authorize]
        [DoNoTCache]
        public ActionResult AccountsList(FormCollection collection)
        {
            //to reset the session
            Session[SessionKey.AccExternalID.ToString()] = null;
            Session[SessionKey.ExternalID.ToString()] = null;
            Session[SessionKey.FxAccNo.ToString()] = null;
            Session[SessionKey.billDelivery_availableMsisdn.ToString()] = null;
            Session[SessionKey.billDelivery_emailBillSubscription.ToString()] = null;
            Session[SessionKey.billDelivery_emailAddress.ToString()] = null;
            Session[SessionKey.billDelivery_smsAlertFlag.ToString()] = null;
            Session[SessionKey.billDelivery_smsNo.ToString()] = null;

            string[] input = collection["SelectAccountDtls"].ToString2().Split(',');
            if (input.Count() > 0)
            {

                Session[SessionKey.AccExternalID.ToString()] = input[0].ToString2();
                Session[SessionKey.ExternalID.ToString()] = input[1].ToString2();
                Session[SessionKey.FxAccNo.ToString()] = input[2].ToString2();
                return RedirectToAction("BillDeliveryOptionsList");
            }
            return RedirectToAction("AccountList");
        }

        [Authorize]
        [DoNoTCache]
        public ActionResult BillDeliveryOptionsList()
        {
            string SelectAccountDtls = Session[SessionKey.AccExternalID.ToString()].ToString2();
            if (TempData["ErrorMessage"] != null) ModelState.AddModelError(string.Empty, TempData["ErrorMessage"].ToString2());

            var billDeliveryVM = new BillDeliveryVM();

            if (SelectAccountDtls != null && SelectAccountDtls != "")
            {
                Dictionary<string, List<string>> msisdnListByAccount = (Dictionary<string, List<string>>)Session[SessionKey.billDelivery_msisdnListByAccount.ToString()];

                billDeliveryVM.AcctExtId = SelectAccountDtls;
                billDeliveryVM.AvailableMsisdn = msisdnListByAccount[SelectAccountDtls];

                if (string.IsNullOrEmpty(Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2()))
                {
                    WebHelper.Instance.BillDeliveryRetrieve(billDeliveryVM);
                    if (!billDeliveryVM.Result)
                    {
                        ModelState.AddModelError(string.Empty, string.Format("{0} {1}", ConfigurationManager.AppSettings["BillDelivery_ErrorMessage_EBPSFailed1"].ToString(), billDeliveryVM.ErrorMessage));
                        Session[SessionKey.billDelivery_ableToSubmit.ToString()] = false;
                    }
                    else
                    {
                        Session[SessionKey.billDelivery_ableToSubmit.ToString()] = true;
                    }
                }
                else
                {
                    //retrieve from session
                    billDeliveryVM.BillDeliveryDetails.EmailBillSubscription = Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2();
                    billDeliveryVM.BillDeliveryDetails.EmailAddress = Session[SessionKey.billDelivery_emailAddress.ToString()].ToString2();
                    billDeliveryVM.BillDeliveryDetails.SmsAlertFlag = Session[SessionKey.billDelivery_smsAlertFlag.ToString()].ToString2();
                    billDeliveryVM.BillDeliveryDetails.SmsNo = Session[SessionKey.billDelivery_smsNo.ToString()].ToString2();
                }

            }
            return View(billDeliveryVM);
        }

        [HttpPost]
        [Authorize]
        [DoNoTCache]
        public ActionResult BillDeliveryOptionsList(BillDeliveryVM billDeliveryVM)
        {
            bool isValid = false;
            string ErrorMsg = string.Empty;

            if (billDeliveryVM.TabNumber == 2)
            {
                return RedirectToAction("AccountsList");
            }

            WebHelper.Instance.BillDeliveryValidate(
                billDeliveryVM.BillDeliveryDetails.EmailBillSubscription,
                billDeliveryVM.BillDeliveryDetails.EmailAddress,
                billDeliveryVM.BillDeliveryDetails.SmsAlertFlag,
                billDeliveryVM.BillDeliveryDetails.SmsNo,
                out isValid, out ErrorMsg);
            //store the data into the session
            Session[SessionKey.billDelivery_emailBillSubscription.ToString()] = billDeliveryVM.BillDeliveryDetails.EmailBillSubscription;
            Session[SessionKey.billDelivery_emailAddress.ToString()] = billDeliveryVM.BillDeliveryDetails.EmailAddress;
            Session[SessionKey.billDelivery_smsAlertFlag.ToString()] = billDeliveryVM.BillDeliveryDetails.SmsAlertFlag;
            Session[SessionKey.billDelivery_smsNo.ToString()] = billDeliveryVM.BillDeliveryDetails.SmsNo;

            if (!isValid)
            {
                //ModelState.AddModelError(string.Empty, ErrorMsg);
                TempData["ErrorMessage"] = ErrorMsg;
                return RedirectToAction("BillDeliveryOptionsList");
            }
            else
            {
                return RedirectToAction("BillDeliveryOptionsConfirm");
            }
        }

        [Authorize]
        [DoNoTCache]
        public ActionResult BillDeliveryOptionsConfirm(int? regID)
        {
            retrieveAcctListByICResponse AcctListByICResponse = null;
            CustomizedCustomer CustomerPersonalInfo = null;
            PersonalDetailsVM personalDetailsVM = new PersonalDetailsVM()
            {
                RegID = regID.ToInt(),
            };

            var regDetails = new RegistrationDetails();
            var reg = new DAL.Models.Registration();
            if (regID != null && regID.ToInt() > 0)
            {
                using (var proxy = new RegistrationServiceProxy())
                {
                    //Added by Pradeep - Combines multiple service calls to Registration proxy
                    regDetails = proxy.GetRegistrationFullDetails(personalDetailsVM.RegID);

                    reg = regDetails.Regs.SingleOrDefault();
                    personalDetailsVM.QueueNo = reg.QueueNo;
                    personalDetailsVM.Remarks = reg.Remarks;
                    personalDetailsVM.SignatureSVG = reg.SignatureSVG;
                    personalDetailsVM.RegTypeID = reg.RegTypeID;
                    personalDetailsVM.UserType = reg.UserType;
                    personalDetailsVM.MOC_Status = reg.MOC_Status;
                    personalDetailsVM.externalId = reg.externalId;
                    personalDetailsVM.IsVerified = reg.IsVerified;
                    personalDetailsVM.StatusID = regDetails.RegStatus.SingleOrDefault().StatusID;
                    personalDetailsVM.Justification = regDetails.lnkregdetails.Justification;

                    using (var configProxy = new ConfigServiceProxy())
                    {
                        personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;
                    }

                    personalDetailsVM.Customer = regDetails.Customers.SingleOrDefault();
                    personalDetailsVM.Address = regDetails.RegAddresses.SingleOrDefault();

                    //get active T&C
                    using (var registrationProxy = new RegistrationServiceProxy())
                    {
                        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                    }

                    //Bill Delivery Options
                    //GTM e-Billing CR - Ricky - 2014.09.25
                    personalDetailsVM.EmailBillSubscription = reg.billDeliveryViaEmail ? "Y" : "N";
                    personalDetailsVM.EmailAddress = reg.billDeliveryViaEmail ? reg.billDeliveryEmailAddress : "";
                    personalDetailsVM.SmsAlertFlag = reg.billDeliverySmsNotif ? "Y" : "N";
                    personalDetailsVM.SmsNo = reg.billDeliverySmsNotif ? reg.billDeliverySmsNo : "";
                    personalDetailsVM.BillSubmissionStatus = reg.billDeliverySubmissionStatus != null ? reg.billDeliverySubmissionStatus : "";
                }
            }
            else if (!ReferenceEquals(Session["PPID"], null) && "E".Equals(Convert.ToString(Session["PPID"])))
            {
                if (!ReferenceEquals(Session["PPIDInfo"], null))
                {
                    ///CHECK IF EXISTING USER THEN ONLY RETRIEVE CUSTOMER INFORMATION
                    AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                    ///CHECK IF CUSTOMER INFORMATION IS THERE OR NOT
                    if (!ReferenceEquals(AcctListByICResponse, null) && !ReferenceEquals(AcctListByICResponse.itemList.Where(c => c.Customer != null && c.AcctExtId == Session["ExternalID"].ToString2()).ToList(), null))
                    {
                        CustomerPersonalInfo = AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList().FirstOrDefault().Customer;
                        if (!String.IsNullOrEmpty(CustomerPersonalInfo.NewIC))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 1;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.NewIC;
                            personalDetailsVM.Customer.NationalityID = 1;
                        }
                        else if (!String.IsNullOrEmpty(CustomerPersonalInfo.PassportNo))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 2;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.PassportNo;
                            personalDetailsVM.Customer.NationalityID = 132;
                        }
                        else if (!String.IsNullOrEmpty(CustomerPersonalInfo.OldIC))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 3;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.OldIC;
                            personalDetailsVM.Customer.NationalityID = CustomerPersonalInfo.NationalityID;
                        }
                        else if (!String.IsNullOrEmpty(CustomerPersonalInfo.OtherIC))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 4;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.OtherIC;
                            personalDetailsVM.Customer.NationalityID = CustomerPersonalInfo.NationalityID;
                        }
                        if (CustomerPersonalInfo.Title != string.Empty)
                        {
                            if (!CustomerPersonalInfo.Title.EndsWith("."))
                            {
                                CustomerPersonalInfo.Title = CustomerPersonalInfo.Title + ".";
                            }
                        }
                    }

                    personalDetailsVM.Address.Postcode = CustomerPersonalInfo.PostCode;
                    personalDetailsVM.Address.Line1 = CustomerPersonalInfo.Address1;
                    personalDetailsVM.Address.Line2 = CustomerPersonalInfo.Address2;
                    personalDetailsVM.Address.Line3 = CustomerPersonalInfo.Address3;
                    personalDetailsVM.Customer.FullName = CustomerPersonalInfo.CustomerName;
                    personalDetailsVM.Address.Town = CustomerPersonalInfo.City;
                    if (CustomerPersonalInfo != null)
                    {
                        //Changes made by chetan
                        if ((CustomerPersonalInfo.Dob != null) && (CustomerPersonalInfo.Dob != "") && CustomerPersonalInfo.Dob.Length >= 8)
                        {
                            personalDetailsVM.DOBDay = (CustomerPersonalInfo.Dob.Substring(6, 2)).ToInt();
                            personalDetailsVM.DOBMonth = (CustomerPersonalInfo.Dob.Substring(4, 2)).ToInt();
                            personalDetailsVM.DOBYear = (CustomerPersonalInfo.Dob.Substring(0, 4)).ToInt();
                        }
                        if (CustomerPersonalInfo.Dob.Length >= 8)
                        {
                            personalDetailsVM.DOBDay = (CustomerPersonalInfo.Dob.Substring(6, 2)).ToInt();
                            personalDetailsVM.DOBMonth = (CustomerPersonalInfo.Dob.Substring(4, 2)).ToInt();
                            personalDetailsVM.DOBYear = (CustomerPersonalInfo.Dob.Substring(0, 4)).ToInt();
                        }
                        else
                        {
                            // given fix for min date if no date is avail.
                            if (Session["IDCardNo"] != null)
                            {
                                int OutPut;
                                if (int.TryParse(Session["IDCardNo"].ToString2().Substring(0, 2), out OutPut))
                                {
                                    CustomerPersonalInfo.Dob = Session["IDCardNo"].ToString2();
                                    int year = Session["IDCardNo"].ToString2().Substring(0, 2).ToInt();
                                    int CurrYear = DateTime.Now.Year;
                                    string PreYear = "19" + year.ToString2();
                                    if (CurrYear - PreYear.ToInt() <= 100)
                                    {
                                        personalDetailsVM.DOBYear = PreYear.ToInt();
                                    }
                                    else
                                    {
                                        PreYear = "20" + year.ToString2();
                                        personalDetailsVM.DOBYear = PreYear.ToInt();
                                    }
                                    personalDetailsVM.DOBDay = (CustomerPersonalInfo.Dob.Substring(4, 2)).ToInt();
                                    personalDetailsVM.DOBMonth = (CustomerPersonalInfo.Dob.Substring(2, 2)).ToInt();
                                }
                                else
                                    personalDetailsVM.Customer.DateOfBirth = DateTime.MinValue;

                            }
                        }
                        personalDetailsVM.Customer.CustomerTitleID = CustomerPersonalInfo.CustomerTitleID;
                    }
					if (CustomerPersonalInfo != null && !string.IsNullOrEmpty(CustomerPersonalInfo.Cbr))
                    {
						CustomerPersonalInfo.Cbr = Util.FormatContactNumber(CustomerPersonalInfo.Cbr);
						personalDetailsVM.Customer.ContactNo = CustomerPersonalInfo.Cbr;
                    }
                    else
                    {
                        personalDetailsVM.Customer.ContactNo = "";
                    }
					List<SelectListItem> objStateList = Util.GetList(RefType.State);
					List<SelectListItem> curStateId = objStateList.Where(e => e.Text == CustomerPersonalInfo.State).ToList();
					//if (personalDetailsVM.Address.StateID == 0)
					//{
					//    personalDetailsVM.Address.StateID = 19;
					//}
					if (curStateId.Count > 0)
					{
						personalDetailsVM.Address.StateID = Convert.ToInt32(curStateId[0].Value);
					}
                    //Feb Drop UAT Bug 1671 - Wrong state being displayed (remove hardcoded values)
                    //else
                    //{
                    //    personalDetailsVM.Address.StateID = 19;
                    //}

                    #region MOC_Status and Liberization status

                    string[] Result = null; // Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session[SessionKey.ExternalID.ToString()].ToString2()) ? string.Empty : Session[SessionKey.ExternalID.ToString()].ToString2(), regID.ToInt());
                    if (Result != null && Result.Length > 1)
                    {
                        personalDetailsVM.Liberlization_Status = Result[0];
                        personalDetailsVM.MOCStatus = Result[1];
                    }
                    #endregion

                    using (var registrationProxy = new RegistrationServiceProxy())
                    {
                        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                    }

                    personalDetailsVM.EmailBillSubscription = Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2();
                    personalDetailsVM.EmailAddress = (Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2() == "Y" ? Session[SessionKey.billDelivery_emailAddress.ToString()].ToString2() : "");
                    personalDetailsVM.SmsAlertFlag = Session[SessionKey.billDelivery_smsAlertFlag.ToString()].ToString2();
                    personalDetailsVM.SmsNo = (Session[SessionKey.billDelivery_smsAlertFlag.ToString()].ToString2() == "Y" ? Session[SessionKey.billDelivery_smsNo.ToString()].ToString2() : "");


                }
            }
            Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;
            if (TempData["ErrorMessage"] != null) ModelState.AddModelError(string.Empty, TempData["ErrorMessage"].ToString2());
            return View(personalDetailsVM);
        }

        [HttpPost]
        [ValidateInput(false)]
        [Authorize]
        [DoNoTCache]
        public ActionResult BillDeliveryOptionsConfirm(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            if (collection["TabNumber"].ToString2() == "3")
            {
                return RedirectToAction("BillDeliveryOptionsList");
            }
            bool isError = false;
            string ErrorMsg = string.Empty;
            UpdSubscriptionInfoResponse response = null;
            UpdSubscriptionInfoRequest request = new UpdSubscriptionInfoRequest();

            try
            {
                //store information into DB
                var resp = new RegistrationCreateResp();
                using (var proxy = new RegistrationServiceProxy())
                {
                    resp = proxy.RegistrationCreate(
                        ConstructRegistration(collection["QueueNo"].ToString2(), collection["Remarks"].ToString2(), "", "", Session["billDelivery_smsNo"].ToString2(), "", "", Session[SessionKey.AccExternalID.ToString()].ToString2(), Session[SessionKey.AccExternalID.ToString()].ToString2(), "", "", "", personalDetailsVM.SignatureSVG),
                        ConstructCustomer(),
                        null, //ConstructRegMdlGroupModel(), 
                        ConstructRegAddress(),
                        null, //ConstructRegPgmBdlPkgComponent(), 
                        ConstructRegStatus(),
                        null, //ConstructRegSuppLines(),
                        null, //ConstructRegSuppLineVASes(), 
                        null, null);

                    personalDetailsVM.RegID = resp.ID;
                }

                using (var proxy = new RegistrationServiceProxy())
                {
                    //store information into lnkRegDetails
                    LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
                    Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                    objRegDetails.CreatedDate = DateTime.Now;
                    objRegDetails.UserName = Util.SessionAccess.UserName;
                    objRegDetails.RegId = resp.ID;
                    objRegDetails.PdpaVersion = personalDetailsVM.PDPDVersion;
                    if (collection["txtJustfication"].ToString2().Length > 0)
                        objRegDetails.Justification = collection["txtJustfication"].ToString2();
                    objRegDetails.MOC_Status = personalDetailsVM.MOCStatus;
                    objRegDetails.Liberlization_Status = personalDetailsVM.Liberlization_Status;
                    objRegDetailsReq.LnkDetails = objRegDetails;
                    proxy.SaveLnkRegistrationDetails(objRegDetailsReq);

                    #region insert into trnRegAttributes
                    var regAttribList = new List<RegAttributes>();
                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_USER_DEVICE_AGENT, ATT_Value = Request.UserAgent });

                    proxy.SaveListRegAttributes(regAttribList);
                    #endregion
                }

                //EBPS Submission
                using (var proxy = new EBPSServiceProxy())
                {
                    request.acctExtId = Session[SessionKey.AccExternalID.ToString()].ToString2();
                    //the value of hardCopySuppressSubscription is always the same with emailBillSubscription
                    request.hcSuppressSubscription = Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2();
                    request.emailBillSubscription = Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2();
                    request.emailAddress = (Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2() == "Y" ? Session[SessionKey.billDelivery_emailAddress.ToString()].ToString2() : "");
                    request.smsAlertFlag = Session[SessionKey.billDelivery_smsAlertFlag.ToString()].ToString2();
                    request.smsNo = (Session[SessionKey.billDelivery_smsAlertFlag.ToString()].ToString2() == "Y" ? Session[SessionKey.billDelivery_smsNo.ToString()].ToString2() : "");
                    request.updatedBy = !ReferenceEquals(Request.Cookies["CookieUser"], null) ? Request.Cookies["CookieUser"].Value : "ISELL";

                    response = proxy.UpdSubscriptionInfo(request);

                    if (!response.Success)
                    {
                        isError = true;
                        ErrorMsg = string.Format("{0} {1}", ConfigurationManager.AppSettings["BillDelivery_ErrorMessage_EBPSFailed1"].ToString(), response.Message);
                        Logger.ErrorFormat("Controller{0}:Method{1}### Error:{2} {3}", "BillDeliveryController", "BillDeliveryOptionsUpdate", "EBPS Submission Failed", response.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                isError = true;
                ErrorMsg = ConfigurationManager.AppSettings["BillDelivery_ErrorMessage_EBPSFailed1"].ToString();
                Logger.ErrorFormat("Controller{0}:Method{1}### Error:{2}", "BillDeliveryController", "BillDeliveryOptionsUpdate", ex);
            }

            try
            {
                using (var proxy = new RegistrationServiceProxy())
                {
                    //update the order status to 'Closed' if no error
                    if (!isError)
                    {
                        proxy.RegStatusCreate(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                        }, null);

                        DAL.Models.Registration regUpd = new DAL.Models.Registration();
                        regUpd.ID = personalDetailsVM.RegID;
                        regUpd.billDeliverySubmissionStatus = "Success";
                        proxy.RegistrationUpdate(regUpd);
                    }
                    //update the order status to 'Fail' if error found
                    else
                    {
                        proxy.RegStatusCreate(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail),
                            ResponseCode = response.Success.ToString(),
                            ResponseDescription = response.Message
                        }, null);

                        DAL.Models.Registration regUpd = new DAL.Models.Registration();
                        regUpd.ID = personalDetailsVM.RegID;
                        regUpd.billDeliverySubmissionStatus = "Failed";
                        proxy.RegistrationUpdate(regUpd);
                    }
                }

            }
            catch (Exception ex)
            {
                isError = true;
                if (personalDetailsVM.RegID != null)
                    ErrorMsg = string.Format("Unable to submit the request for {1}. Please contact the administrator.", personalDetailsVM.RegID);
                else
                    ErrorMsg = "Unable to submit the request. Please contact the administrator.";

                Logger.ErrorFormat("Controller{0}:Method{1}### Error:{2}", "BillDeliveryController", "BillDeliveryOptionsUpdate", ex);
            }

            //Redirection section
            if (isError)
            {
                ModelState.AddModelError(string.Empty, ErrorMsg);
                TempData["ErrorMessage"] = ErrorMsg;
                return RedirectToAction("BillDeliveryOptionsConfirm");
            }
            else
            {
                return RedirectToAction("BillDeliveryOptionsSuccess", new { regID = personalDetailsVM.RegID });
            }
        }

        /// <summary>
        /// Constructing registration object.
        /// </summary>
        /// <param name="queueNo"></param>
        /// <param name="remarks"></param>
        /// <param name="ContractType"></param>
        /// <param name="AccountType"></param>
        /// <param name="MSISDN"></param>
        /// <param name="subscriberNo"></param>
        /// <param name="subscriberNoReset"></param>
        /// <param name="accountNo"></param>
        /// <param name="KenanAccountNo"></param>
        /// <param name="OfferId"></param>
        /// <param name="UOMCode"></param>
        /// <param name="penality"></param>
        /// <param name="signatureSVG"></param>
        /// <param name="custPhoto"></param>
        /// <param name="altCustPhoto"></param>
        /// <param name="photo"></param>
        /// <param name="isSimRequired"></param>
        /// <param name="SIMModelSelected"></param>
        /// <returns></returns>
        private DAL.Models.Registration ConstructRegistration(string queueNo, string remarks, string ContractType, string AccountType, string MSISDN, string subscriberNo, string subscriberNoReset, string accountNo, string KenanAccountNo, string OfferId, string UOMCode, string penality, string signatureSVG = "", string custPhoto = "", string altCustPhoto = "", string photo = "", string isSimRequired = "", string SIMModelSelected = "")
        {
            var regTypeID = 0;
            if (Session[SessionKey.RegMobileReg_Type.ToString()] != null)
            {
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.BillDelivery;
            }
            DAL.Models.Registration registration = new DAL.Models.Registration();

            //we set this always TRUE why because based on this we are storing the ApproveBlacklistCheck  in trnregistration this we are sending to kenan as sales code
            var isBlacklisted = true;
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            regTypeID = Util.GetRegTypeID(REGTYPE.BillDelivery);

            registration.K2_Status = Session[SessionKey.RegK2_Status.ToString()].ToString2();
            registration.CenterOrgID = Util.SessionAccess.User.OrgID;
            registration.RegTypeID = regTypeID;
            registration.Remarks = remarks;
            registration.QueueNo = queueNo;
            registration.SignatureSVG = signatureSVG;
            registration.CustomerPhoto = custPhoto;
            registration.AltCustomerPhoto = altCustPhoto;
            registration.Photo = photo;
            registration.AdditionalCharges = personalDetailsVM.Deposite;
            registration.InternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;
            registration.ExternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;
            registration.AgeCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]) : "AF";
            registration.DDMFCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]) : "DF";
            registration.AddressCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]) : "ADF";
            registration.OutStandingCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()]) : "OF";
            registration.TotalLineCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()]) : "TLF";
            registration.PrincipleLineCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()]) : "PLF";
            registration.BiometricVerify = ((Session["RegMobileReg_BiometricVerify"].ToBool() != null) ? (Session["RegMobileReg_BiometricVerify"].ToBool()) : false);
            registration.IsVerified = !ReferenceEquals(Session["RegMobileReg_BiometricVerify"], null) ? true : false;
            registration.ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
            registration.RFSalesDT = DateTime.Now;
            registration.MSISDN1 = MSISDN;
            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
            {
                registration.SalesPerson = Request.Cookies["CookieUser"].Value;
            }
            if (Session[SessionKey.OfferId.ToString()] != null)
                registration.OfferID = Convert.ToDecimal(Session[SessionKey.OfferId.ToString()]);
            else
                registration.OfferID = 0;
            // registration.OfferID = Convert.ToDecimal(OfferId);
            registration.externalId = MSISDN;
            registration.CreateDT = DateTime.Now;
            registration.LastAccessID = Util.SessionAccess.UserName;
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "0";
            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
            registration.ContractCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()]) : "CF";
            registration.UserType = Session[SessionKey.PPID.ToString()].ToString() == null ? "N" : Session[SessionKey.PPID.ToString()].ToString();
            //registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()].ToString()) : 0);
            registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthDevAdv.ToString()].ToString()) : 0);
            //registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalyPlanDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyPlanDeposit"].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()].ToString()) : 0);
            registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalayDevDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayDevDeposit"].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()].ToString()) : 0);
            registration.OutstandingAcs = Session[SessionKey.FailedAcctIds.ToString()] == null ? string.Empty : Session[SessionKey.FailedAcctIds.ToString()].ToString();
            registration.PrePortInCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()]) : "MPPF";
            registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()]) : "MSIF";
            registration.ArticleID = !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty;
            registration.upfrontPayment = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_UpfrontPayment"] != null) ? Convert.ToDecimal(Session["RegMobileReg_UpfrontPayment"].ToString2()) : 0) : ((Session["RegMobileReg_UpfrontPayment"] != null) ? Convert.ToDecimal(Session["RegMobileReg_UpfrontPayment"].ToString2()) : 0);
            registration.UOMCode = string.Empty;
            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : "No";
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "No";
            registration.OrganisationId = Util.SessionAccess.User.Org.OrganisationId;

            registration.Trn_Type = ConfigurationManager.AppSettings["Trn_Type"];
            registration.IMPOSStatus = 0;
            registration.SimModelId = 0;
            registration.InternationalRoaming = false;

            registration.KenanAccountNo = KenanAccountNo;
            registration.fxAcctNo = accountNo;
            registration.fxSubscrNo = subscriberNo;
            registration.fxSubScrNoResets = subscriberNoReset;
            if (isSimRequired.ToLower() == "true")
                registration.IsSImRequired_Smart = true;
            else
                registration.IsSImRequired_Smart = false;

            if (!string.IsNullOrEmpty(SIMModelSelected.Split(',')[0]) && SIMModelSelected.Split(',')[0].ToLower() != "false")
                registration.SimModelId = SIMModelSelected.Split(',')[0].ToInt();

            registration.PenaltyAmount = penality == null ? 0 : penality == "" ? 0 : Convert.ToDecimal(Convert.ToDouble(penality));
            //registration.Discount = Session[SessionKey.PenaltyWaiveOff.ToString()] == null ? 0 : Session[SessionKey.PenaltyWaiveOff.ToString()].ToString() == "" ? 0 : Convert.ToDecimal(Session[SessionKey.PenaltyWaiveOff.ToString()]);
            registration.PenaltyWaivedAmt = Session[SessionKey.PenaltyWaiveOff.ToString()].ToString2() == string.Empty ? 0 : Convert.ToDecimal(Session[SessionKey.PenaltyWaiveOff.ToString()]);
            registration.Discount_ID = "0";
            registration.IsK2 = ((!ReferenceEquals(Session["IsExistingK2"], null) && Session["IsExistingK2"].ToString() == "1") ? true : false);

            // S - for supplines and other type is principle line
            registration.CRPType = AccountType;
            registration.IMEINumber = Session["IMEIForDevice"].ToString2();

            //GTM e-Billing CR - Ricky - 2014.09.25
            registration.billDeliveryViaEmail = Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2() == "Y" ? true : false;
            registration.billDeliveryEmailAddress = Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2() == "Y" ? Session[SessionKey.billDelivery_emailAddress.ToString()].ToString2() : "";
            registration.billDeliverySmsNotif = Session[SessionKey.billDelivery_smsAlertFlag.ToString()].ToString2() == "Y" ? true : false;
            registration.billDeliverySmsNo = Session[SessionKey.billDelivery_smsAlertFlag.ToString()].ToString2() == "Y" ? Session[SessionKey.billDelivery_smsNo.ToString()].ToString2() : "";

            return registration;
        }

        /// <summary>
        /// Constructing customer object.
        /// </summary>
        /// <returns></returns>
        private Customer ConstructCustomer()
        {
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            var allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
            var CustomerDetails = WebHelper.Instance.getCustomerDetails(allCustomerDetails);
            personalDetailsVM.Customer.PayModeID = 1;
            personalDetailsVM.Customer.LanguageID = 1;
            //personalDetailsVM.Customer.EmailAddr = "test@test.com.my";
            personalDetailsVM.Customer.RaceID = 4;

            if (personalDetailsVM.Customer.DateOfBirth.Equals(DateTime.MinValue))
                personalDetailsVM.Customer.DateOfBirth = DateTime.MinValue;
            else
                personalDetailsVM.Customer.DateOfBirth = Convert.ToDateTime(personalDetailsVM.DOBYear + "-" + personalDetailsVM.DOBMonth + "-" + personalDetailsVM.DOBDay);

            
            // PN-3965 
            personalDetailsVM.Customer.ContactNo = Util.FormatContactNumber(personalDetailsVM.Customer.ContactNo);

            var cust = new Customer()
            {
                FullName = personalDetailsVM.Customer.FullName,
                Gender = personalDetailsVM.Customer.Gender,
                DateOfBirth = personalDetailsVM.DOBMonth != 0 ? (Convert.ToDateTime(personalDetailsVM.DOBYear + "-" + personalDetailsVM.DOBMonth + "-" + personalDetailsVM.DOBDay)) : DateTime.MinValue,
                CustomerTitleID = CustomerDetails.CustomerTitleID > 0 ? CustomerDetails.CustomerTitleID : personalDetailsVM.Customer.CustomerTitleID,
                RaceID = personalDetailsVM.Customer.RaceID,
                LanguageID = personalDetailsVM.Customer.LanguageID,
                NationalityID = personalDetailsVM.Customer.NationalityID,
                IDCardTypeID = personalDetailsVM.Customer.IDCardTypeID,
                IDCardNo = personalDetailsVM.Customer.IDCardNo,
                ContactNo = personalDetailsVM.Customer.ContactNo,
                AlternateContactNo = personalDetailsVM.Customer.AlternateContactNo,
                EmailAddr = personalDetailsVM.Customer.EmailAddr,
                PayModeID = personalDetailsVM.Customer.PayModeID,
                CardTypeID = personalDetailsVM.Customer.CardTypeID,
                NameOnCard = personalDetailsVM.Customer.NameOnCard,
                CardNo = personalDetailsVM.Customer.CardNo,
                CardExpiryDate = personalDetailsVM.Customer.CardExpiryDate,
                Active = true,
                LastAccessID = Util.SessionAccess.UserName,
                CreateDT = DateTime.Now
            };

            return cust;
        }

        /// <summary>
        /// Constructing customer address object.
        /// </summary>
        /// <returns></returns>
        private List<Address> ConstructRegAddress()
        {
            var addresses = new List<Address>();
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            addresses.Add(new Address()
            {
                AddressTypeID = 1,
                Line1 = personalDetailsVM.Address.Line1,
                Line2 = personalDetailsVM.Address.Line2,
                Line3 = personalDetailsVM.Address.Line3,
                Postcode = personalDetailsVM.Address.Postcode,
                StateID = personalDetailsVM.Address.StateID,
                Town = personalDetailsVM.Address.Town,
                Street = "",
                BuildingNo = "",
                CountryID = Util.GetIDByCode(RefType.Country, Properties.Settings.Default.Country_MY),
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });

            return addresses;
        }

        /// <summary>
        /// Constructing registration status object.
        /// </summary>
        /// <returns></returns>
        private RegStatus ConstructRegStatus()
        {
            var regStatus = new RegStatus()
            {
                Active = false,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName,
                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegNew)
            };

            return regStatus;
        }

        /// <summary>
        /// Clear registration sessions.
        /// Note: Method was copied from AddContractController
        /// </summary>
        private void ClearRegistrationSession()
        {

            Session["RegMobileReg_BiometricID"] = null;
            Session["RegMobileReg_SelectedModelID"] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
            Session[SessionKey.RegMobileReg_MobileNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session[SessionKey.RegMobileReg_IDCardType.ToString()] = null;
            Session[SessionKey.RegMobileReg_SublineVM.ToString()] = null;
            Session[SessionKey.ArticleId.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session["RebatePenalty"] = null;
            Session[SessionKey.billDelivery_ableToSubmit.ToString()] = null;
            //Not required for Bill Delivery Options
            //ClearSubLineSession();
        }

        /// <summary>
        /// Constructing the order summary.
        /// Note: Method was copied from AddContractController
        /// </summary>
        /// <param name="packageId">PackageId</param>
        /// <param name="modelId">Model Id</param>
        /// <param name="vasComponents">VAS components</param>
        /// <param name="vasNames"></param>
        /// <param name="contractId">Contract Id</param>
        /// <param name="penality">Penality</param>
        /// <param name="fromSubline">from subline or not.</param>
        /// <returns></returns>
        private OrderSummaryVM ConstructOrderSummary(string packageId, string modelId, string vasComponents, string vasNames, string contractId, string penality, bool fromSubline = false)
        {
            Session["RegMobileReg_SelectedModelImageID"] = modelId;
            Session["RegMobileReg_PkgPgmBdlPkgCompID"] = packageId;
            Session["RegMobileReg_ContractID"] = contractId;
            Session["RegMobileReg_VasIDs"] = vasComponents;
            if (!string.IsNullOrEmpty(penality))
                Session["Penalty"] = penality;

            if (vasNames != null)
                Session[SessionKey.RegMobileReg_VasNames.ToString()] = vasNames.Split(',').ToList();

            Session["Condition"] = "1";

            if (!string.IsNullOrEmpty(modelId))
                Session["RegMobileReg_Type"] = 1;

            OrderSummaryVM SummaryData = WebHelper.Instance.ConstructOrderSummary(string.Empty, this.GetType().Name, fromSubline);

            if (SummaryData.VasVM != null && SummaryData.VasVM.VASesName != null)
                Session["RegMobileReg_SelectedContracts"] = SummaryData.VasVM.VASesName.Distinct().ToList();
            return SummaryData;
        }

        /// <summary>
        /// Gets the customer details.
        /// Note: Method was copied from AddContractController
        /// </summary>
        /// <returns></returns>
        private PersonalDetailsVM GetPersonalDetails()
        {
            PersonalDetailsVM personalDetails = new PersonalDetailsVM();
            retrieveAcctListByICResponse AcctListByICResponse = null;
            CustomizedCustomer CustomerPersonalInfo = null;
            if (!ReferenceEquals(Session["PPIDInfo"], null))
            {
                ///CHECK IF EXISTING USER THEN ONLY RETRIEVE CUSTOMER INFORMATION
                AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                ///CHECK IF CUSTOMER INFORMATION IS THERE OR NOT
                if (!ReferenceEquals(AcctListByICResponse, null) && !ReferenceEquals(AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList(), null))
                {
                    // CustomerPersonalInfo = AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList().FirstOrDefault().Customer;
                    CustomerPersonalInfo = Util.GetCustomerList(AcctListByICResponse, Session["ExternalID"] != null ? Session["ExternalID"].ToString() : "0");
                }
            }
            if (CustomerPersonalInfo != null)
            {
                //if (CustomerPersonalInfo.Title != "")
                if (!string.IsNullOrWhiteSpace(CustomerPersonalInfo.Title))
                {
                    if (CustomerPersonalInfo.CustomerTitleID != null)
                    {
                        Online.Registration.Web.RegistrationSvc.RegistrationServiceClient client = new Online.Registration.Web.RegistrationSvc.RegistrationServiceClient();
                        int titleID = client.GetIdByTitleName(CustomerPersonalInfo.Title);
                        personalDetails.Customer.CustomerTitleID = titleID;
                    }
                }
                else
                {
                    personalDetails.Customer.CustomerTitleID = 0;
                }
                personalDetails.Customer.FullName = CustomerPersonalInfo.CustomerName;

                if (CustomerPersonalInfo.Cbr.ToString2().Length > 0)
                {
                    // PN 3965
                    CustomerPersonalInfo.Cbr = Util.FormatContactNumber(CustomerPersonalInfo.Cbr);
                }
                else
                {
                    CustomerPersonalInfo.Cbr = Session["MSISDN"].ToString2();
                }

                personalDetails.Customer.ContactNo = CustomerPersonalInfo.Cbr.ToString2();

                var idCardTypeID = Session["IDCardTypeID"] != null ? Session["IDCardTypeID"].ToString() : null;
                personalDetails.Customer.IDCardTypeID = idCardTypeID.ToInt();

                personalDetails.Customer.IDCardNo = CustomerPersonalInfo.NewIC != string.Empty ? CustomerPersonalInfo.NewIC : CustomerPersonalInfo.OldIC;
                if ((CustomerPersonalInfo.NewIC).ToString2() != string.Empty)
                {
                    personalDetails.Customer.IDCardNo = CustomerPersonalInfo.NewIC;
                }
                else if ((CustomerPersonalInfo.OldIC).ToString2() != string.Empty)
                {
                    personalDetails.Customer.IDCardNo = CustomerPersonalInfo.OldIC;
                }
                else if ((CustomerPersonalInfo.PassportNo).ToString2() != string.Empty)
                {
                    personalDetails.Customer.IDCardNo = CustomerPersonalInfo.PassportNo;
                }
                personalDetails.Customer.NationalityID = (!string.IsNullOrEmpty(CustomerPersonalInfo.NewIC)) ? 1 : 132;
                if (CustomerPersonalInfo.Dob.Length >= 8)
                {
                    personalDetails.DOBDay = (CustomerPersonalInfo.Dob.Substring(6, 2)).ToInt();
                    personalDetails.DOBMonth = (CustomerPersonalInfo.Dob.Substring(4, 2)).ToInt();
                    personalDetails.DOBYear = (CustomerPersonalInfo.Dob.Substring(0, 4)).ToInt();


                    CustomerPersonalInfo.DateOfBirth = new DateTime(personalDetails.DOBYear, personalDetails.DOBMonth, personalDetails.DOBDay);
                    if (personalDetails.Customer != null)
                    {
                        personalDetails.Customer.DateOfBirth = CustomerPersonalInfo.DateOfBirth;
                    }
                }
                personalDetails.Address.Line1 = CustomerPersonalInfo.Address1;
                personalDetails.Address.Line2 = CustomerPersonalInfo.Address2;
                personalDetails.Address.Line3 = CustomerPersonalInfo.Address3;
                personalDetails.Address.Postcode = CustomerPersonalInfo.PostCode;
                personalDetails.Address.Town = CustomerPersonalInfo.City;

                if (!string.IsNullOrEmpty(CustomerPersonalInfo.State))
                {
                    List<SelectListItem> objStateList = Util.GetList(RefType.State);
                    List<SelectListItem> curStateId = objStateList.Where(e => e.Text == CustomerPersonalInfo.State).ToList();
                    if (curStateId.Count() > 0)
                    {
                        personalDetails.Address.StateID = Convert.ToInt32(curStateId[0].Value);
                    }
                    else
                    {
                        personalDetails.Address.StateID = 20;
                    }
                }
                else
                {
                    personalDetails.Address.StateID = 20;
                }
            }
            return personalDetails;
        }

        /// <summary>
        /// Displays the summary information.
        /// Note: Method was copied from AddContractController
        /// </summary>
        /// <param name="regID">Registration Id</param>
        /// <returns></returns>
        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSV,MREG_DSA,MREG_DAI,MREG_DSK,MREG_DCH,MREG_DIC,MREG_DAC")]
        [DoNoTCache]
        public ActionResult BillDeliveryOptionsSuccess(int? regID)
        {
            DeviceVM deviceVM = new DeviceVM();
            if (Session["model"] != null)
                deviceVM = (DeviceVM)Session["model"];

            var personalDetailsVM = new PersonalDetailsVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            int paymentstatus = -1;
            #region VLT ADDED CODE 21stFEB for Email functionality
            Session["RegID"] = regID;
            #endregion VLD ADDED CODE 21stFEB for Email functionality
            var KenanLogDetails = new DAL.Models.KenanaLogDetails();
            if (regID.ToInt() > 0)
            {

                ClearRegistrationSession();
                personalDetailsVM = new PersonalDetailsVM()
                {
                    RegID = regID.Value.ToInt(),
                };
                using (var proxy = new RegistrationServiceProxy())
                {
                    reg = proxy.RegistrationGet(regID.Value);
                    KenanLogDetails = proxy.KenanLogDetailsGet(regID.Value);
                    if (!ReferenceEquals(KenanLogDetails, null))
                    {
                        personalDetailsVM.MessageCode = KenanLogDetails.MessageCode.ToInt();
                        personalDetailsVM.MessageDesc = KenanLogDetails.MessageDesc.ToString();
                    }
                    paymentstatus = proxy.PaymentStatusGet(regID.Value);
                    if (paymentstatus != -1)
                    {
                        if (paymentstatus == 0)
                        {
                            personalDetailsVM.PaymentStatus = "0";
                        }
                        else
                        {
                            personalDetailsVM.PaymentStatus = "1";
                        }
                    }
                    else
                    {
                        personalDetailsVM.PaymentStatus = "0";
                    }
                    personalDetailsVM.QueueNo = reg.QueueNo;
                    personalDetailsVM.Remarks = reg.Remarks;
                    personalDetailsVM.BiometricVerify = reg.BiometricVerify;
                    personalDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
                    personalDetailsVM.BlacklistInternal = reg.InternalBlacklisted;
                    personalDetailsVM.SignatureSVG = reg.SignatureSVG;
                    personalDetailsVM.CustomerPhoto = reg.CustomerPhoto;
                    personalDetailsVM.AltCustomerPhoto = reg.AltCustomerPhoto;
                    personalDetailsVM.Photo = reg.Photo;
                    personalDetailsVM.RegSIMSerial = reg.SIMSerial;
                    personalDetailsVM.RegIMEINumber = reg.IMEINumber;
                    personalDetailsVM.RegTypeID = reg.RegTypeID;
                    personalDetailsVM.AgeCheckStatus = reg.AgeCheckStatus;
                    personalDetailsVM.DDMFCheckStatus = reg.DDMFCheckStatus;
                    personalDetailsVM.AddressCheckStatus = reg.AddressCheckStatus;
                    personalDetailsVM.OutStandingCheckStatus = reg.OutStandingCheckStatus;
                    personalDetailsVM.TotalLineCheckStatus = reg.TotalLineCheckStatus;
                    personalDetailsVM.PrincipleLineCheckStatus = reg.PrincipleLineCheckStatus;
                    personalDetailsVM.ContractCheckStatus = reg.ContractCheckStatus;
                    personalDetailsVM.UserType = reg.UserType;
                    personalDetailsVM.PlanAdvance = reg.PlanAdvance;
                    personalDetailsVM.PlanDeposit = reg.PlanDeposit;
                    personalDetailsVM.DeviceAdvance = reg.DeviceAdvance;
                    personalDetailsVM.DeviceDeposit = reg.DeviceDeposit;
                    personalDetailsVM.Discount = reg.Discount;
                    personalDetailsVM.KenanAccountNo = reg.KenanAccountNo;
                    personalDetailsVM.OutstandingAcs = reg.OutstandingAcs;
                    personalDetailsVM.PrePortInCheckStatus = reg.PrePortInCheckStatus;
                    personalDetailsVM.MNPServiceIdCheckStatus = reg.MNPServiceIdCheckStatus;
                    personalDetailsVM.MOC_Status = reg.MOC_Status;
                    personalDetailsVM.Whitelist_Status = reg.Whitelist_Status;
                    personalDetailsVM.CRPType = reg.CRPType.ToString2();
                    personalDetailsVM.IsVerified = reg.IsVerified;
                    personalDetailsVM.IMPOSFileName = reg.IMPOSFileName;
                    personalDetailsVM.IsSImRequired_Smart = reg.IsSImRequired_Smart;
                    personalDetailsVM.PenaltyAmount = reg.PenaltyAmount;
                    personalDetailsVM.SIMModel = reg.SimModelId.ToString2() == "" ? "0" : reg.SimModelId.ToString();
                    // RegAccount
                    var regAccount = proxy.RegAccountGet(new int[] { reg.ID }).SingleOrDefault();
                    if (regAccount != null)
                    {
                        personalDetailsVM.ServiceActivateErrMsg = regAccount.KenanActivateServiceErrMessage;
                    }

                    LnkRegDetailsReq lnkregdetails = proxy.GetLnkRegistrationDetails(regID.ToInt());
                    if (lnkregdetails.LnkDetails != null)
                    {
                        if (lnkregdetails.LnkDetails.SimType != null)
                            deviceVM.SimType = lnkregdetails.LnkDetails.SimType;
                        else
                            deviceVM.SimType = "Normal";

                        personalDetailsVM.ContractCheckCount = lnkregdetails.LnkDetails.ContractCheckCount;
                    }
                    else
                    {
                        deviceVM.SimType = "Normal";
                    }

                    // Registration Status
                    var regStatusID = proxy.RegStatusFind(new RegStatusFind()
                    {
                        RegStatus = new RegStatus()
                        {
                            RegID = reg.ID
                        },
                        Active = true
                    }).SingleOrDefault();

                    personalDetailsVM.Deposite = reg.AdditionalCharges;
                    personalDetailsVM.StatusID = proxy.RegStatusGet(new int[] { regStatusID }).SingleOrDefault().StatusID;

                    using (var configProxy = new ConfigServiceProxy())
                    {
                        personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;

                    }

                    //for cancel reason
                    if (personalDetailsVM.StatusID == 21)
                    {
                        using (var regProxy = new RegistrationServiceProxy())
                        {
                            string reason = regProxy.CancelReasonGet(personalDetailsVM.RegID);
                            personalDetailsVM.MessageDesc = reason;
                        }
                    }
                    //End Cancell 

                    if (reg.upfrontPayment != null && reg.upfrontPayment.ToString2() != "")
                    {
                        personalDetailsVM.UpfrontPayment = reg.upfrontPayment;
                    }

                    personalDetailsVM.PenaltyAmount = reg.PenaltyAmount;

                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);
                    Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;

                    // Customer
                    personalDetailsVM.Customer = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    // Address
                    var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                    {
                        Active = true,
                        Address = new Address()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();
                    personalDetailsVM.Address = proxy.RegAddressGet(regAddrIDs).SingleOrDefault();

                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    // RegMdlGrpModels
                    var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                    {
                        RegMdlGrpModel = new RegMdlGrpModel()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();
                    if (regMdlGrpModelIDs.Count() > 0)
                        regMdlGrpModel = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).SingleOrDefault();

                    if (regMdlGrpModel.ID == 0)
                    {
                    }
                    else
                    {
                        Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = regMdlGrpModel.Price.ToDecimal();
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                        deviceVM.DevicePrice = regMdlGrpModel.Price.ToString();
                        deviceVM.ModelId = regMdlGrpModel.ModelImageID.ToString();
                    }

                    // RegPgmBdlPkgComponent
                    var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                    {
                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();

                    // Main Line PgmBdlPkgComponent
                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    if (regPgmBdlPkgCompIDs.Count() > 0)
                        regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind()
                    {
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();

                    if (suppLineIDs.Count() > 0)
                    {
                        suppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    }
                }

                if (suppLines.Count() > 0)
                    //Not required for Bill Delivery Options
                    //ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

                    using (var proxy = new CatalogServiceProxy())
                    {
                        string userICNO = Util.SessionAccess.User.IDCardNo;

                        if (userICNO != null && userICNO.Length > 0)
                        {
                            objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                        }
                        if (objWhiteListDetailsResp != null)
                        {
                            if (objWhiteListDetailsResp.Description != null)
                            {
                                personalDetailsVM._WhiteListDetails.Description = objWhiteListDetailsResp.Description;
                            }
                        }

                        if (regMdlGrpModel.ID > 0)
                            Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;


                        var mainLinePBPCs = new List<PgmBdlPckComponent>();
                        if (mainLinePBPCIDs.Count() > 0)
                            mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

                        var bpCode = Properties.Settings.Default.Bundle_Package;
                        var pcCode = Properties.Settings.Default.Package_Component;
                        var DPCode = Properties.Settings.Default.RegType_DevicePlan;
                        var MAcode = Properties.Settings.Default.Master_Component;
                        var MPcode = Properties.Settings.Default.Main_package;
                        var CPCode = Properties.Settings.Default.ComplimentaryPlan;
                        var DCCode = Properties.Settings.Default.CompType_DataCon;
                        var ECCode = Properties.Settings.Default.Extra_Component;
                        var SPCode = Properties.Settings.Default.SupplimentaryPlan;
                        var OCCode = Properties.Settings.Default.Other_Component;


                        if (mainLinePBPCs.Count() > 0)
                        {
                            if (personalDetailsVM.CRPType == "S")
                                deviceVM.PackageId = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == SPCode)).SingleOrDefault().ID.ToString2();
                            else
                                deviceVM.PackageId = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode)).SingleOrDefault().ID.ToString2();

                            var vasIDs = string.Empty;
                            var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode || a.LinkType == SPCode || a.LinkType == OCCode || a.LinkType == "RC" || a.LinkType == "IC" || a.LinkType == "MD").Select(a => a.ID).ToList();

                            foreach (var id in pkgCompIDs)
                            {
                                vasIDs = vasIDs + "," + id;
                            }
                            vasIDs = vasIDs + ",";
                            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = vasIDs;
                            deviceVM.VasComponents = vasIDs;
                        }
                    }
                deviceVM.PersonalDetails = personalDetailsVM;

            }

            if (reg.OfferID != null && reg.OfferID > 0)
            {
                deviceVM.OfferId = reg.OfferID.ToString2();
            }

            deviceVM.OrderSummary = ConstructOrderSummary(deviceVM.PackageId, deviceVM.ModelId, deviceVM.VasComponents, deviceVM.VasNames, deviceVM.OfferId, null);
            deviceVM.OrderSummary.IMEINumber = reg.IMEINumber;
            deviceVM.OrderSummary.SIMSerial = reg.SIMSerial;
            deviceVM.OrderSummary.PenaltyWaiveOffAmount = reg.PenaltyWaivedAmt;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = deviceVM.OrderSummary;

            if (regID > 0)
            {
            }
            else
            {
                deviceVM.PersonalDetails = GetPersonalDetails();
                deviceVM.PersonalDetails.Customer.Gender = "M";
                deviceVM.SimType = ((!string.IsNullOrEmpty(deviceVM.SimType) && deviceVM.SimType.ToLower() == "true") ? "MISM" : "Normal");
            }

            #region MOC_Status and Liberization status

            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(deviceVM.MSISDN) ? string.Empty : deviceVM.MSISDN, regID.ToInt());
            deviceVM.PersonalDetails.Liberlization_Status = Result[0];
            deviceVM.PersonalDetails.MOCStatus = Result[1];
            deviceVM.PersonalDetails.Justification = Result[2].ToString2();

            Session["RegMobileReg_PersonalDetailsVM"] = deviceVM.PersonalDetails;

            #endregion

            return View(deviceVM);
        }

        /// <summary>
        /// Displays the details in print page in sales agent.
        /// Note: Method was copied from AddContractController
        /// </summary>
        /// <param name="id">Registration Id</param>
        /// <returns></returns>
        public ActionResult PrintMobileRF(int id)
        {
            var regFormVM = new RegistrationFormVM();
            var modelImageIDs = new List<int>();
            var pbpcIDs = new List<int>();
            var suppLineVASIDs = new List<int>();
            var suppLineForms = new List<SuppLineForm>();
            var regMdlGrpModels = new List<RegMdlGrpModel>();
            string PlanKenanCode = string.Empty;
            using (var proxy = new RegistrationServiceProxy())
            {
                // Registration
                regFormVM.Registration = proxy.RegistrationGet(id);

                // Customer
                regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

                // Address
                var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                {
                    Active = true,
                    Address = new Address()
                    {
                        RegID = id
                    }
                }).ToList();
                regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

                // RegMdlGrpModels
                var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                {
                    RegMdlGrpModel = new RegMdlGrpModel()
                    {
                        RegID = id
                    }
                }).ToList();
                if (regMdlGrpModelIDs.Count() > 0)
                {
                    regMdlGrpModels = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
                    modelImageIDs = regMdlGrpModels.Select(a => a.ModelImageID).ToList();
                }
                // RegPgmBdlPkgComponent
                var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                {
                    RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                    {
                        RegID = id,
                    }
                }).ToList();

                var pbpc = new List<RegPgmBdlPkgComp>();
                if (regPgmBdlPkgCompIDs.Count() > 0)
                    pbpc = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                //main line
                if (pbpc.Count() > 0)
                    pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();
                // Reg Supp Line

                var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = id } }).ToList();
                if (suppLineIDs.Count() > 0)
                {
                    var regSuppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    foreach (var regSuppLine in regSuppLines)
                    {
                        suppLineForms.Add(new SuppLineForm()
                        {
                            // supp line pbpc ID
                            SuppLinePBPCID = regSuppLine.PgmBdlPckComponentID,

                            // supp line vas ID
                            SuppLineVASIDs = pbpc.Where(a => a.RegSuppLineID == regSuppLine.ID).Select(a => a.PgmBdlPckComponentID).ToList(),

                            MSISDN = regSuppLine.MSISDN1
                        });
                    }
                }

            }

            using (var proxy = new CatalogServiceProxy())
            {
                // ModelGroupModel
                if (modelImageIDs.Count() > 0)
                {
                    var modelImages = proxy.BrandArticleModelImageGet(modelImageIDs).ToList();
                    var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());

                    foreach (var regMdlGrpModel in regMdlGrpModels)
                    {
                        var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
                        regFormVM.DeviceForms.Add(new DeviceForm()
                        {
                            Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
                            Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
                            Price = regMdlGrpModel.Price.ToDecimal()
                        });
                    }
                }
                var vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
                // BundlePackage, PackageComponents
                if (pbpcIDs.Count() > 0)
                {
                    var pgmBdlPkgComps = MasterDataCache.Instance.FilterComponents(pbpcIDs).Distinct().ToList();
                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var spCode = Properties.Settings.Default.SecondaryPlan;
                    var cpCode = Properties.Settings.Default.ComplimentaryPlan;
                    regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode).ToList();
                    if (regFormVM.BundlePackages != null && regFormVM.BundlePackages.Count <= 0)
                    {
                        regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode && (a.PlanType == "CP" || a.PlanType == "SP")).ToList();
                    }
                    else
                    {
                        //regFormVM.PackageComponents = pgmBdlPkgComps.Where((a => a.LinkType.ToLower() == "pc" || a.LinkType.ToLower() == "ec" || a.LinkType.ToLower() == "dc" || a.LinkType.ToLower() == "ma" || a.LinkType.ToLower() == "rc" || a.LinkType.ToLower() == "md" || a.LinkType.ToLower() == "ic")).ToList();
                        regFormVM.PackageComponents = pgmBdlPkgComps.Where((a => a.LinkType.ToLower() == "pc" || a.LinkType.ToLower() == "ec" || a.LinkType.ToLower() == "dc" || a.LinkType.ToLower() == "ma" || a.LinkType.ToLower() == "oc" || a.LinkType.ToLower() == "rc" || a.LinkType.ToLower() == "cr" || a.LinkType.ToLower() == "md" || a.LinkType.ToLower() == "dm" || a.LinkType.ToLower() == "ic")).ToList();
                    }

                }
                //********************************//
                if (regFormVM.BundlePackages.Count > 0)
                    PlanKenanCode = regFormVM.BundlePackages[0].KenanCode;
                //********************************//
                foreach (var suppLineForm in suppLineForms)
                {
                    //supp line package name
                    suppLineForm.SuppLineBdlPkgName = proxy.PackageGet(MasterDataCache.Instance.FilterComponents(new int[] { suppLineForm.SuppLinePBPCID })
                                                                .Select(a => a.ChildID)).SingleOrDefault().Name;
                    //supp line vas names
                    suppLineForm.SuppLineVASNames = proxy.ComponentGet(MasterDataCache.Instance.FilterComponents(suppLineForm.SuppLineVASIDs).Select(a => a.ChildID).ToList()).Select(a => a.Name).ToList();
                }
                regFormVM.SuppLineForms = suppLineForms;

                List<RegSmartComponents> listRegSmartComponents = new List<RegSmartComponents>();

                listRegSmartComponents = WebHelper.Instance.GetSelectedComponents(id.ToString2());
                if (listRegSmartComponents.Count > 0)
                {
                    regFormVM.RegSmartComponents = listRegSmartComponents;
                }

                //Contract Name Dispaly
                if (regFormVM.Registration.RegTypeID == Util.GetIDByCode(RefType.RegType, Settings.Default.RegType_AddContract))
                {
                    List<DAL.Models.Component> componentsList = Util.GetContractsBYRegId(id);
                    if (componentsList != null && componentsList.Count > 0)
                    {
                        Session["SelectedContractName"] = componentsList.Select(vas => vas.Name).ToList();
                        int Duration = 0;
                        foreach (var item in componentsList)
                        {
                            using (var RSproxy = new CatalogServiceProxy())
                            {
                                string strValue = RSproxy.GetContractByCode(componentsList[0].Code);
                                if (Duration < strValue.ToInt())
                                {
                                    Duration = strValue.ToInt();
                                }
                            }

                        }
                        Session["SelectedContractValue"] = Duration;
                    }
                }
            }

            using (var proxy = new RegistrationServiceProxy())
            {
                DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(id);
                Online.Registration.DAL.Models.PrintVersion objPrintVersionDetails = new Online.Registration.DAL.Models.PrintVersion();

                //Condition added by ravi on nov 1 2013 
                if (lnkregdetails != null)
                {

                    objPrintVersionDetails = proxy.GetPrintTsCsInfo(lnkregdetails.PrintVersionNo.ToString2(), 24, regFormVM.Registration.ArticleID == null ? string.Empty : regFormVM.Registration.ArticleID, PlanKenanCode, "");
                    if (objPrintVersionDetails != null)
                    {
                        Session["TsCs"] = objPrintVersionDetails.TsCs;
                    }
                    else
                    {
                        Session["TsCs"] = null;
                    }
                }

            }

            using (var proxyOrg = new OrganizationServiceProxy())
            {
                if (!ReferenceEquals(regFormVM.Registration, null) && !ReferenceEquals(regFormVM.Registration.CenterOrgID, null) && regFormVM.Registration.CenterOrgID != 0)
                {
                    regFormVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { regFormVM.Registration.CenterOrgID }).FirstOrDefault();
                }
            }

            WebHelper.Instance.MapRegAttributesByRegID(regFormVM, !ReferenceEquals(regFormVM.Registration, null) ? regFormVM.Registration.ID : id);
            return View(regFormVM);
        }


    }
}
