﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
//using MaxisVASPortal.DynamicProxy;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.UserSvc;
using Online.Registration.Web.ViewModels;
using SNT.Utility;
using log4net;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.ContentRepository;
namespace Online.Registration.Web.Controllers
{


    [Authorize]
    //[HandleError(View = "GenericError")]
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class GuidedSalesController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(GuidedSalesController));
        private void ResetDepositSessions()
        {
            Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = null;
             Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = null;
            Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = null;
           Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = null;
            Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = null;
            Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = null;

        }


        public ActionResult Index()
        {
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
             Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
             Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
             Session[SessionKey.Condition.ToString()] = null;
            Session[SessionKey.SelectedComponents.ToString()] = null;
            //Session[SessionKey.Searchstatus.ToString()] = "";
            ResetDepositSessions();
            var planIDs = new List<int>();
            var guidedSalesVM = new GuidedSalesVM();


            using (var proxy = new UserServiceProxy())
            {
                PlanSearchItemResp obj = new PlanSearchItemResp();
                obj.VoiceUsage = "VoiceUsage";
                var voiceUsageMax = proxy.PlanMaxStatus(obj.VoiceUsage);
                Session["VoiceUsageMax"] = voiceUsageMax.VoiceUsage;

                obj.VoiceUsage = "DataUsage";
                var dataUsageMax = proxy.PlanMaxStatus(obj.VoiceUsage);
                Session["DataUsageMax"] = dataUsageMax.VoiceUsage;

                obj.VoiceUsage = "SMSUsage";
                var smsUsageMax = proxy.PlanMaxStatus(obj.VoiceUsage);
                Session["smsUsageMax"] = smsUsageMax.VoiceUsage;

                obj.VoiceUsage = "Price";
                var priceMax = proxy.PlanMaxStatus(obj.VoiceUsage);
                Session["PriceMax"] = priceMax.VoiceUsage;

                var voiceUsageMin = proxy.PlanMinStatus(obj.VoiceUsage);
                Session["VoiceUsageMin"] = voiceUsageMin.VoiceUsage;

                var dataUsageMin = proxy.PlanMinStatus(obj.VoiceUsage);
                Session["DataUsageMin"] = dataUsageMin.VoiceUsage;

                var smsUsageMin = proxy.PlanMinStatus(obj.VoiceUsage);
                Session["smsUsageMin"] = smsUsageMin.VoiceUsage;

                var priceMin = proxy.PlanMinStatus(obj.VoiceUsage);
                Session["PriceMin"] = priceMin.VoiceUsage;
            }


            try
            {



                guidedSalesVM.DevicePropertyVM.SelectedModel = new SelectedModel { ImageID = Session["GuidedSales_SelectedModelImageID"].ToString2() };
                guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID = Session["GuidedSales_PkgPgmBdlPkgCompID"].ToString2();


                if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt())
                {
                    if (!string.IsNullOrEmpty(guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID))
                    {
                        planIDs.Add(guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID.ToInt());

                        GetAvailablePackages(guidedSalesVM.PlanPropertyVM, planIDs);
                    }
                    else
                    {
                        GetAvailablePackages(guidedSalesVM.PlanPropertyVM);
                    }
                }
                else if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt())
                {
                    GetAvailableDevices(guidedSalesVM.DevicePropertyVM);
                }

                if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt() ||
                    Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt())
                {
                    guidedSalesVM.IsSearch = true;
                }

                ConstructDeviceSearchCriteria(guidedSalesVM.DevicePropertyVM);

                Session["GuidedSales_PkgPgmBdlPkgCompID"] = null;

                guidedSalesVM.IsSearch = false;

                if (Request.QueryString["type"] == null)
                {
                    if (Session[SessionKey.Searchstatus.ToString()].ToString2() == "DeviceSearch")
                    {
                        Session[SessionKey.Searchstatus.ToString()] = "DeviceSearch";
                        Session["GuidedSales_Type"] = MobileRegType.DevicePlan.ToInt();
                        Session[SessionKey.RegMobileReg_Type.ToString()] = "1";
                        Session[SessionKey.MobileRegType.ToString()] = "1";
                        guidedSalesVM.SelectedCriterias = Session["SelectedCriterias"].ToString2();
                        #region Search

                        bool? hasCamera = null;
                        var models = new List<Model>();
                        var modelIDs = new List<int>();
                        var articleID = new List<BrandArticle>();
                        var availableModelImages = new List<AvailableModelImage>();
                        var modelImages = new List<ModelImage>();
                        var deviceProperties = new List<DeviceProperty>();
                        var props = new List<Property>();
                        var chkBoxes = string.IsNullOrEmpty(guidedSalesVM.SelectedCriterias) ? new List<string>().ToArray() : guidedSalesVM.SelectedCriterias.Split(',');
                        var criterias = chkBoxes.Select(a => a.Split('_')).ToList();
                        using (var proxy = new CatalogServiceProxy())
                        {
                            props = proxy.PropertyGet(proxy.PropertyFind(new DAL.Models.PropertyFind()
                            {
                                Property = new DAL.Models.Property()
                                {
                                    Type = Properties.Settings.Default.Property_DeviceType
                                },
                                Active = true
                            }).ToList()).ToList();
                        }
                        var devicePropertyFind = new DevicePropertyFind();
                        var brandCode = Properties.Settings.Default.Property_Brand;
                        var TouchScreen = Properties.Settings.Default.Property_TouchScreen;
                        var GPS = Properties.Settings.Default.Property_GPS;
                        var Camera = Properties.Settings.Default.Property_Camera;
                        var Bluetooth = Properties.Settings.Default.Property_Bluetooth;
                        var ScreenType = Properties.Settings.Default.Property_ScreenType;
                        var Dual = Properties.Settings.Default.Property_Dual;
                        var priceRangeCode = Properties.Settings.Default.Property_PriceRange;

                        foreach (var values in criterias)
                        {
                            if (!string.IsNullOrEmpty(values[0]))
                            {
                                if (values[1].ToInt() == props.Where(a => a.Code == brandCode).Select(a => a.ID).SingleOrDefault().ToInt())
                                {
                                    devicePropertyFind.BrandIDs.Add(values[2].ToInt());
                                }
                                else if (values[1].ToInt() == props.Where(a => a.Code == priceRangeCode).Select(a => a.ID).SingleOrDefault().ToInt())
                                {
                                    devicePropertyFind.PriceRangeIDs.Add(values[2].ToInt());
                                }
                                else if (values[1].ToInt() == props.Where(a => a.Code == TouchScreen).Select(a => a.ID).SingleOrDefault().ToInt())
                                {
                                    devicePropertyFind.TouchScreenIDs.Add(values[2].ToInt());
                                }
                                else if (values[1].ToInt() == props.Where(a => a.Code == GPS).Select(a => a.ID).SingleOrDefault().ToInt())
                                {
                                    devicePropertyFind.GPS.Add(values[2].ToInt());
                                }
                                else if (values[1].ToInt() == props.Where(a => a.Code == Camera).Select(a => a.ID).SingleOrDefault().ToInt())
                                {
                                    devicePropertyFind.Camera.Add(values[2].ToInt());
                                }
                                else if (values[1].ToInt() == props.Where(a => a.Code == Bluetooth).Select(a => a.ID).SingleOrDefault().ToInt())
                                {
                                    devicePropertyFind.Bluetooth.Add(values[2].ToInt());
                                }
                                else if (values[1].ToInt() == props.Where(a => a.Code == ScreenType).Select(a => a.ID).SingleOrDefault().ToInt())
                                {
                                    devicePropertyFind.ScreenType.Add(values[2].ToInt());
                                }
                                else if (values[1].ToInt() == props.Where(a => a.Code == Dual).Select(a => a.ID).SingleOrDefault().ToInt())
                                {
                                    devicePropertyFind.DualFront.Add(values[2].ToInt());
                                }
                            }
                        }

                        using (var proxy = new CatalogServiceProxy())
                        {

                            deviceProperties = proxy.DevicePropertyGet(proxy.DevicePropertyFind(devicePropertyFind).ToList()).ToList();

                            modelImages = proxy.ModelImageGet(proxy.ModelImageFind(new ModelImageFind()
                            {
                                ModelImage = new ModelImage(),
                                ModelIDs = deviceProperties.Select(a => a.ModelID).ToList(),
                                Active = true
                            })).OrderByDescending(a => a.CreateDT).ToList();

                            models = proxy.ModelGet(deviceProperties.Select(a => a.ModelID).ToList()).ToList();

                            //Begin code added by VLT on 9 Apr 2013 to implement stock logic in guided sales similar to device+plan 

                            modelIDs = deviceProperties.Select(a => a.ModelID).ToList();


                            #region Code for getting the count from the service by using dynamic proxy and geting the article id
                            List<Online.Registration.Web.ViewModels.ClassArticleId> lstarticleId = new List<Online.Registration.Web.ViewModels.ClassArticleId>();

                            int counter = 0;
                            using (var catalogProxy = new CatalogServiceClient())
                            {
                                for (int i = 0; i < modelIDs.Count; i++)
                                {
                                    articleID = proxy.GetArticleID(modelIDs[i].ToInt()).ArticleIDs.ToList();

                                    if (articleID.Count > 0)
                                    {

                                        for (int j = 0; j < articleID.Count; j++)
                                        {
                                            Online.Registration.Web.ViewModels.ClassArticleId ClassArticleId = new Online.Registration.Web.ViewModels.ClassArticleId()
                                            {
                                                id = articleID[j].ID,
                                                articleId = articleID[j].ArticleID.Trim(),
                                                modelId = articleID[j].ModelID,
                                                imagePath = ReferenceEquals(articleID[j].ImagePath, null) ? string.Empty : articleID[j].ImagePath.ToString().Trim(),
                                                colourID = articleID[j].ColourID,

                                            };
                                            lstarticleId.Add(ClassArticleId);
                                        }
                                        counter++;
                                    }
                                }
                                if (counter <= 0)
                                {
                                    lstarticleId = null;
                                }
                            }

                            //Code by chetan moved the dynamic proxy call to integration service
                            string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                            int storeId = 0;
                            string[] maxisStoreIds = ConfigurationManager.AppSettings["OrganizationsIds"].ToString().Split(",".ToCharArray());
                            if (!maxisStoreIds.Contains(Util.SessionAccess.User.Org.OrganisationId))
                            {
                                storeId = Convert.ToInt32(Util.SessionAccess.User.Org.OrganisationId);
                            }
                            try
                            {
                                using (var dsproxy = new DynamicServiceProxy())
                                {
                                    lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = (int)(dsproxy.GetStockCount(i.articleId, wsdlUrl,storeId)));
                                }
                            }
                            catch(Exception ex)
                            {
                               // RedirectToAction("StoreError", "HOME");
                                LogExceptions(ex);
                                throw ex;
                            }
                            //END HERE

                            #region Code commented by chetan moved the dynamic proxy call to integration service

                            #endregion


                            if (!ReferenceEquals(lstarticleId, null))
                            {
                                foreach (var item in lstarticleId)
                                {
                                    availableModelImages.Add(new AvailableModelImage()
                                    {

                                        BrandArticle = new BrandArticle()
                                        {
                                            ID = item.id,
                                            ColourID = item.colourID,
                                            ImagePath = item.imagePath,
                                            ModelID = item.modelId,
                                            ArticleID = item.articleId,
                                        },
                                        ModelCode = models.Where(a => a.ID == item.modelId).SingleOrDefault().Code,
                                        Count = item.count

                                    });
                                }
                            }
                            #endregion
                        }


                        guidedSalesVM.DevicePropertyVM.AvailableModelImage = availableModelImages;

                        guidedSalesVM.DevicePropertyVM.IsSearch = true;
                        guidedSalesVM.DevicePropertyVM.SelectedModel = new SelectedModel();



                        #endregion
                    }
                }
                else
                {
                    Session[SessionKey.Searchstatus.ToString()] = string.Empty;
                }

            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return View(guidedSalesVM);
        }

        private void LogService(string APIname, string MethodName, out int? LastGeneratedID, bool IsBefore = true, int? ID = 0, string status = "", string userName = "", string xmlReq = "", string xmlRes = "", string idCardNo = "", System.DateTime? dtReq = null, System.DateTime? dtRes = null)
        {
            APISaveCallStatusResp responseFromBusinessValidation = null;
            LastGeneratedID = null;

            string currentLoggedInUserName = string.Empty;

            //Get username from cookie
            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
            {
                currentLoggedInUserName = Request.Cookies["CookieUser"].Value;
            }
            else
            {
                return;
            }

            if (IsBefore)
            {//If true then called before the actual service
                using (var proxy = new UserServiceProxy())
                {
                    responseFromBusinessValidation = new APISaveCallStatusResp();

                    responseFromBusinessValidation = proxy.APISaveCallStatus(new APISaveCallStatusReq
                    {
                        tblAPICallStatusMessagesval = new tblAPICallStatus
                        {
                            APIName = APIname,
                            MethodName = MethodName,
                            // RequestTime = DateTime.Now,
                            UserName = currentLoggedInUserName,
                            RequestTime = (dtReq == null ? System.DateTime.Now : Convert.ToDateTime(dtReq)),
                            BreXmlReq = xmlReq,
                            BreXmlRes = xmlRes,
                            IdCardNo = idCardNo,
                            ResponseTime = (dtRes == null ? System.DateTime.Now : Convert.ToDateTime(dtRes)),
                            Status = status
                        }
                    });
                }

                LastGeneratedID = responseFromBusinessValidation.APIID;
            }
            else
            {
                using (var proxy = new UserServiceProxy())
                {
                    var response = proxy.APIUpdateCallStatus(new APISaveCallStatusReq
                    {
                        tblAPICallStatusMessagesval = new tblAPICallStatus
                        {
                            ID = ID == null ? 0 : (int)ID,
                            ResponseTime = DateTime.Now,
                            Status = status
                        }
                    });
                }
            }

        }

        private void BusinessValidationChecks(string type)
        {
            string idCardNo = string.Empty;
            string idCardTypeID = string.Empty;
            string dob = string.Empty;
            string state = string.Empty;
            string postalCode = string.Empty;
            int? GeneratedID = null;

            var resp = new BusinessRuleResponse();

            idCardNo = Session[SessionKey.IDCardNo.ToString()].ToString();
            idCardTypeID = Session[SessionKey.IDCardTypeID.ToString()].ToString();
            retrieveAcctListByICResponse AcctListByICResponse = null;
            CustomizedCustomer CustomerPersonalInfo = null;
            AcctListByICResponse = (retrieveAcctListByICResponse) Session[SessionKey.PPIDInfo.ToString()];
            if (!ReferenceEquals(AcctListByICResponse, null) && !ReferenceEquals(AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList(), null))
            {
                CustomerPersonalInfo = AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList().FirstOrDefault().Customer;
                dob = CustomerPersonalInfo.Dob;

            }

            if (Session[SessionKey.State.ToString()] != null)
            {
                state = Session[SessionKey.State.ToString()].ToString();
            }
            else
            {
                state = string.Empty;
            }
            if (Session[SessionKey.PostalCode.ToString()] != null)
            {
                postalCode = Session[SessionKey.PostalCode.ToString()].ToString();
            }
            else
            {
                postalCode = string.Empty;
            }
            var idCardType = new IDCardType();

            using (var proxy = new RegistrationServiceProxy())
            {
                idCardType = proxy.IDCardTypeGet(idCardTypeID.ToInt());
            }

            //For existing users checking
            if (type == "E")
            {
                if (Properties.Settings.Default.CheckBRE == "Y")
                {

                    LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true);

                    #region Age Check
                    try
                    {

                        var req = new BusinessRuleRequest();
                        req.RuleNames = "AgeCheck".Split(' ');
                        req.DateOfBirth = dob;
                        req.IDCardNo = idCardNo;
                        req.IDCardType = idCardType.KenanCode;

                        DateTime dtReq = DateTime.Now;

                        //AgeCheck
                        resp = BREChecker.validate(req);

                        DateTime dtRes = DateTime.Now;
                        string xmlReq = Util.ConvertObjectToXml(req);
                        string xmlRes = Util.ConvertObjectToXml(resp);

                        SetAgeSession(resp);

                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                    }
                    catch (Exception ex)
                    {
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);

                        LogExceptions(ex);
                        Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()] = "AE";

                    }
                    #endregion

                    #region Address Check
                    try
                    {
                        var req = new BusinessRuleRequest();
                        req.RuleNames = "AddressCheck".Split(' ');
                        req.IDCardNo = idCardNo;
                        req.IDCardType = idCardType.KenanCode;
                        req.state = state;
                        req.postalCode = postalCode;
                        DateTime dtReq = DateTime.Now;
                        //AddressCheck
                        resp = BREChecker.validate(req);

                        DateTime dtRes = DateTime.Now;
                        string xmlReq = Util.ConvertObjectToXml(req);
                        string xmlRes = Util.ConvertObjectToXml(resp);

                        SetAddressSession(resp);
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                    }
                    catch (Exception ex)
                    {
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);

                        LogExceptions(ex);
                        Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()] = "ADE";
                    }

                    #endregion

                    #region DDMF Check
                    try
                    {
                        var req = new BusinessRuleRequest();
                        req.RuleNames = "DDMFCheck".Split(' ');
                        req.IDCardNo = idCardNo;
                        req.IDCardType = idCardType.KenanCode;
                        DateTime dtReq = DateTime.Now;
                        //DDMFCheck
                        resp = BREChecker.validate(req);

                        DateTime dtRes = DateTime.Now;
                        string xmlReq = Util.ConvertObjectToXml(req);
                        string xmlRes = Util.ConvertObjectToXml(resp);

                        SetDDMFSession(resp);
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                    }
                    catch (Exception ex)
                    {
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);

                        LogExceptions(ex);
                        Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()] = "DE";
                    }

                    #endregion

                    #region TotalLine Check
                    try
                    {
                        var req = new BusinessRuleRequest();
                        req.RuleNames = "TotalLineCheck".Split(' ');
                        req.totalLinesAllowed = ConfigurationManager.AppSettings["totalLinesAllowed"].ToString();
                        req.IDCardType = idCardType.KenanCode;
                        req.IDCardNo = idCardNo;
                        DateTime dtReq = DateTime.Now;

                        //TotalLineCheck 9 allowed
                        resp = BREChecker.validate(req);

                        DateTime dtRes = DateTime.Now;
                        string xmlReq = Util.ConvertObjectToXml(req);
                        string xmlRes = Util.ConvertObjectToXml(resp);

                        SetTotalLineCheckSession(resp);
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                    }
                    catch (Exception ex)
                    {
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);

                        LogExceptions(ex);
                        Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()] = "TLE";

                    }
                    #endregion

                    #region Outstanding Credit Check
                    try
                    {
                        var req = new BusinessRuleRequest();
                        req.RuleNames = "OutstandingCreditCheck".Split(' ');
                        req.kenanExtAcctId = Session[SessionKey.AcctExtId.ToString()].ToString();
                        req.IDCardNo = idCardNo;
                        req.IDCardType = idCardType.KenanCode;
                        DateTime dtReq = DateTime.Now;

                        resp = BREChecker.validate(req);

                        DateTime dtRes = DateTime.Now;
                        string xmlReq = Util.ConvertObjectToXml(req);
                        string xmlRes = Util.ConvertObjectToXml(resp);

                        SetOutstandingCheckSession(resp);
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                    }
                    catch (Exception ex)
                    {
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);

                        LogExceptions(ex);
                        Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()] = "OE";

                    }

                    #endregion

                    #region Principal Line Check
                    try
                    {
                        var req = new BusinessRuleRequest();
                        req.RuleNames = "PrincipleLineCheck".Split(' ');
                        req.totalPrincipleLinesAllowed = ConfigurationManager.AppSettings["totalPrincipleLinesAllowed"].ToString();
                        req.IDCardNo = idCardNo;
                        req.IDCardType = idCardType.KenanCode;
                        DateTime dtReq = DateTime.Now;

                        //PrincipleLineCheck 5 allowed
                        resp = BREChecker.validate(req);

                        DateTime dtRes = DateTime.Now;
                        string xmlReq = Util.ConvertObjectToXml(req);
                        string xmlRes = Util.ConvertObjectToXml(resp);

                        SetPrincipalLineCheckSession(resp);
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                    }
                    catch (Exception ex)
                    {
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);

                        LogExceptions(ex);
                        Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()] = "PLE";
                    }
                    #endregion

                    #region ContractCheck
                    try
                    {
                        var req = new BusinessRuleRequest();
                        req.RuleNames = "ContractCheck".Split(' ');

                        req.totalContractsAllowed = ConfigurationManager.AppSettings["totalContractsAllowed"].ToString();
                        req.IDCardNo = idCardNo;
                        req.IDCardType = idCardType.KenanCode;
                        DateTime dtReq = DateTime.Now;

                        //ContractCheck 2 allowed
                        resp = BREChecker.validate(req);

                        DateTime dtRes = DateTime.Now;
                        string xmlReq = Util.ConvertObjectToXml(req);
                        string xmlRes = Util.ConvertObjectToXml(resp);
                        
                        SetContractCheckSession(resp);

                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);

                    }
                    catch (Exception ex)
                    {
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);

                        LogExceptions(ex);
                        Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()] = "CE";
                    }
                    #endregion
                }

            }
            //for new User
            else
            {
                #region DDMF Check
                try
                {
                    var req = new BusinessRuleRequest();
                    req.RuleNames = "DDMFCheck".Split(' ');
                    req.IDCardNo = idCardNo;
                    req.IDCardType = idCardType.KenanCode;
                    DateTime dtReq = DateTime.Now;

                    //DDMFCheck
                    resp = BREChecker.validate(req);

                    DateTime dtRes = DateTime.Now;
                    string xmlReq = Util.ConvertObjectToXml(req);
                    string xmlRes = Util.ConvertObjectToXml(resp);

                    SetDDMFSession(resp);
                    
                    LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                }
                catch (Exception ex)
                {
                    LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);

                    LogExceptions(ex);
                    Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()] = "DE";
                }

                #endregion

                #region Outstanding Credit Check
                try
                {
                    if (Session[SessionKey.AcctExtId.ToString()] == null)
                    {
                        Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckFailed.ToString()] = true;
                        Session[SessionKey.RegMobileReg_OutstandingCreditCheckResultMessage.ToString()] = " New User";
                        Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()] = "ON";
                    }
                    else
                    {
                        var req = new BusinessRuleRequest();
                        req.RuleNames = "OutstandingCreditCheck".Split(' ');
                        req.kenanExtAcctId = Session[SessionKey.AcctExtId.ToString()].ToString();
                        req.IDCardNo = idCardNo;
                        req.IDCardType = idCardType.KenanCode;
                        DateTime dtReq = DateTime.Now;

                        resp = BREChecker.validate(req);

                        DateTime dtRes = DateTime.Now;
                        string xmlReq = Util.ConvertObjectToXml(req);
                        string xmlRes = Util.ConvertObjectToXml(resp);

                        SetOutstandingCheckSession(resp);
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                    }

                }
                catch (Exception ex)
                {
                    LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);

                    LogExceptions(ex);
                    Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()] = "OE";

                }
                #endregion


            }
        }

        //Code by VLT
        [HttpPost]
        public ActionResult SelectDeviceCatalog(FormCollection collection, DevicePropertyVM devicePropertyVM)
        {

            try
            {
                if (collection["submit1"].ToString2() == "SelectDeviceCatalog")
                {
                    #region Search

                    bool? hasCamera = null;
                    var models = new List<Model>();
                    var modelIDs = new List<int>();
                    var articleID = new List<BrandArticle>();
                    var availableModelImages = new List<AvailableModelImage>();
                    var modelImages = new List<ModelImage>();
                    var brandArticle = new List<BrandArticle>();
                    var deviceProperties = new List<DeviceProperty>();
                    var props = new List<Property>();
                    var chkBoxes = string.IsNullOrEmpty(devicePropertyVM.SelectedCriterias) ? new List<string>().ToArray() : devicePropertyVM.SelectedCriterias.Split(',');
                    var criterias = chkBoxes.Select(a => a.Split('_')).ToList();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        props = proxy.PropertyGet(proxy.PropertyFind(new DAL.Models.PropertyFind()
                        {
                            Property = new DAL.Models.Property()
                            {
                                Type = Properties.Settings.Default.Property_DeviceType
                            },
                            Active = true
                        }).ToList()).ToList();
                    }

                    var devicePropertyFind = new DevicePropertyFind();
                    var brandCode = Properties.Settings.Default.Property_Brand;
                    var priceRangeCode = Properties.Settings.Default.Property_PriceRange;
                    var TouchScreen = Properties.Settings.Default.Property_TouchScreen;
                    var GPS = Properties.Settings.Default.Property_GPS;
                    var Camera = Properties.Settings.Default.Property_Camera;
                    var Bluetooth = Properties.Settings.Default.Property_Bluetooth;
                    var ScreenType = Properties.Settings.Default.Property_ScreenType;
                    var Dual = Properties.Settings.Default.Property_Dual;


                    foreach (var values in criterias)
                    {
                        if (!string.IsNullOrEmpty(values[0]))
                        {

                            if (values[1].ToInt() == props.Where(a => a.Code == brandCode).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.BrandIDs.Add(values[2].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == priceRangeCode).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[2].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == TouchScreen).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.TouchScreenIDs.Add(values[2].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == GPS).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.GPS.Add(values[2].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == Camera).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.Camera.Add(values[2].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == Bluetooth).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.Bluetooth.Add(values[2].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == ScreenType).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.ScreenType.Add(values[2].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == Dual).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.DualFront.Add(values[2].ToInt());
                            }
                    #endregion
                        }
                    }

                    using (var proxy = new CatalogServiceProxy())
                    {
                        deviceProperties = proxy.DevicePropertyGet(proxy.DevicePropertyFind(devicePropertyFind).ToList()).ToList();

                        modelImages = proxy.ModelImageGet(proxy.ModelImageFind(new ModelImageFind()
                        {
                            ModelImage = new ModelImage(),
                            ModelIDs = deviceProperties.Select(a => a.ModelID).ToList(),
                            Active = true
                        })).OrderByDescending(a => a.CreateDT).ToList();

                        models = proxy.ModelGet(deviceProperties.Select(a => a.ModelID).ToList()).ToList();

                        modelIDs = deviceProperties.Select(a => a.ModelID).ToList();


                        #region Code for getting the count from the service by using dynamic proxy and geting the article id
                        List<Online.Registration.Web.ViewModels.ClassArticleId> lstarticleId = new List<Online.Registration.Web.ViewModels.ClassArticleId>();

                        int counter = 0;
                        using (var catalogProxy = new CatalogServiceClient())
                        {
                            for (int i = 0; i < modelIDs.Count; i++)
                            {
                                articleID = proxy.GetArticleID(modelIDs[i].ToInt()).ArticleIDs.ToList();

                                if (articleID.Count > 0)
                                {

                                    for (int j = 0; j < articleID.Count; j++)
                                    {
                                        Online.Registration.Web.ViewModels.ClassArticleId ClassArticleId = new Online.Registration.Web.ViewModels.ClassArticleId()
                                        {
                                            id = articleID[j].ID,
                                            articleId = articleID[j].ArticleID.Trim(),
                                            modelId = articleID[j].ModelID,
                                            imagePath = ReferenceEquals(articleID[j].ImagePath, null) ? string.Empty : articleID[j].ImagePath.ToString().Trim(),
                                            colourID = articleID[j].ColourID,

                                        };
                                        lstarticleId.Add(ClassArticleId);
                                    }
                                    counter++;
                                }
                            }
                            if (counter <= 0)
                            {
                                lstarticleId = null;
                            }
                        }

                        //Code by chetan moved the dynamic proxy call to integration service

                        //Added by Vahini to call new method to get stock counts for dealers
                        string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                        int storeId = 0;
                        string[] maxisStoreIds = ConfigurationManager.AppSettings["OrganizationsIds"].ToString().Split(",".ToCharArray());
                        if (!maxisStoreIds.Contains(Util.SessionAccess.User.Org.OrganisationId))
                        {
                            storeId = Convert.ToInt32(Util.SessionAccess.User.Org.OrganisationId);
                        }
                        try
                        {
                            using (var dsproxy = new DynamicServiceProxy())
                            {
                                lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = (int)(dsproxy.GetStockCount(i.articleId, wsdlUrl,storeId)));
                            }
                        }
                        catch (Exception ex)
                        {
                            LogExceptions(ex);
                            throw ex;
                           // RedirectToAction("StoreError", "HOME");
                        }
                        //END HERE

                        #region Code commented by chetan moved the dynamic proxy call to integration service
                        /* adding the count depending on the article id */
                        //if (!ReferenceEquals(lstarticleId, null))
                        //{
                        //    lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = Count(i.articleId));
                        //}
                        /*Up to here*/

                        /*Code for calling the service using the dynamic proxy*/
                        //string strAPIUrl = "http://10.200.76.217/Storeservice/Services.asmx?wsdl";
                        //string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                        //string strArgs = "100000061001";
                        //int subscriptionReqFromVasResult;
                        //try
                        //{
                        //    DynamicProxyFactory factory = new DynamicProxyFactory(wsdlUrl);
                        //    if (factory.Contracts.Count() == 1)
                        //    {
                        //        DynamicProxy dynamicproxy = factory.CreateProxy(factory.Contracts.First().Name);
                        //        //subscriptionReqFromVasResult = (int)(dynamicproxy.CallMethod("GetStockCount", strArgs));
                        //        if (!ReferenceEquals(lstarticleId, null))
                        //        {
                        //            lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = (int)(dynamicproxy.CallMethod("GetStockCount", i.articleId)));
                        //        }

                        //        dynamicproxy.Close();
                        //    }
                        //    else
                        //    {
                        //        //TODO: if more than one, we need to loop through all contracts and findout our method. And need to use corresponding Contract otherwise throw an error
                        //    }
                        //}
                        //catch (Exception ex)
                        //{
                        //    RedirectToAction("StoreError", "HOME");
                        //}
                        /*end here*/
                        #endregion


                        if (!ReferenceEquals(lstarticleId, null))
                        {
                            foreach (var item in lstarticleId)
                            {
                                availableModelImages.Add(new AvailableModelImage()
                                {

                                    BrandArticle = new BrandArticle()
                                    {
                                        ID = item.id,
                                        ColourID = item.colourID,
                                        ImagePath = item.imagePath,
                                        ModelID = item.modelId,
                                        ArticleID = item.articleId,
                                    },
                                    ModelCode = models.Where(a => a.ID == item.modelId).SingleOrDefault().Code,
                                    Count = item.count

                                });
                            }
                        }
                        #endregion
                    }

                    devicePropertyVM.AvailableModelImage = availableModelImages;


                    devicePropertyVM.IsSearch = true;
                    //devicePropertyVM.SelectedModelImageID = "0";
                    devicePropertyVM.SelectedModel = new SelectedModel();

                    ConstructDeviceSearchCriteria(devicePropertyVM);

                    return RedirectToAction("SelectDeviceCatalog", "Registration");

                }
                else if (devicePropertyVM.SelectedModel != null && !string.IsNullOrEmpty(devicePropertyVM.SelectedModel.ImageID))
                {
                    //Session["GuidedSales_SelectedModelImageID"] = devicePropertyVM.SelectedModelImageID;
                    Session["GuidedSales_SelectedModelImageID"] = devicePropertyVM.SelectedModel.ImageID;

                    if (Session["GuidedSales_PkgPgmBdlPkgCompID"] != null)
                    {
                        using (var proxy = new CatalogServiceProxy())
                        {
                            var pbpc = MasterDataCache.Instance.FilterComponents(new int[] { Convert.ToInt32(Session["GuidedSales_PkgPgmBdlPkgCompID"].ToString()) }).SingleOrDefault();
                            if (pbpc != null)

                                //Chnages made by chetan for SKMM
                                Session["UOMPlanID"] = pbpc.KenanCode;

                        }
                    }
                    if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt()) // go to credit check
                    {
                        ConstructGuidedSalesSession();

                        return RedirectToAction("SelectDevice", "Registration", new { type = MobileRegType.DevicePlan.ToInt() });
                    }
                    else if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt()) // go to guided sales plan screen
                    {
                        return RedirectToAction("Plan");
                    }
                }
                else // Back to Previous page
                {
                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return View(devicePropertyVM);
        }

        //Code by VLT
        [HttpPost]
        public ActionResult DeviceSearch(FormCollection collection, DevicePropertyVM devicePropertyVM)
        {

            try
            {
                if (collection["submit1"].ToString2() == "DeviceSearch")
                {
                    #region Search

                    bool? hasCamera = null;
                    var models = new List<Model>();
                    var modelIDs = new List<int>();
                    var articleID = new List<BrandArticle>();
                    var availableModelImages = new List<AvailableModelImage>();
                    var modelImages = new List<ModelImage>();
                    var deviceProperties = new List<DeviceProperty>();
                    var props = new List<Property>();
                    var chkBoxes = string.IsNullOrEmpty(devicePropertyVM.SelectedCriterias) ? new List<string>().ToArray() : devicePropertyVM.SelectedCriterias.Split(',');
                    var criterias = chkBoxes.Select(a => a.Split('_')).ToList();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        props = proxy.PropertyGet(proxy.PropertyFind(new DAL.Models.PropertyFind()
                        {
                            Property = new DAL.Models.Property()
                            {
                                Type = Properties.Settings.Default.Property_DeviceType
                            },
                            Active = true
                        }).ToList()).ToList();
                    }

                    var devicePropertyFind = new DevicePropertyFind();
                    var brandCode = Properties.Settings.Default.Property_Brand;
                    var priceRangeCode = Properties.Settings.Default.Property_PriceRange;
                    var TouchScreen = Properties.Settings.Default.Property_TouchScreen;
                    var GPS = Properties.Settings.Default.Property_GPS;
                    var Camera = Properties.Settings.Default.Property_Camera;
                    var Bluetooth = Properties.Settings.Default.Property_Bluetooth;
                    var ScreenType = Properties.Settings.Default.Property_ScreenType;
                    var Dual = Properties.Settings.Default.Property_Dual;

                    foreach (var values in criterias)
                    {
                        if (!string.IsNullOrEmpty(values[0]))
                        {
                            if (values[1].ToInt() == props.Where(a => a.Code == brandCode).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.BrandIDs.Add(values[2].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == TouchScreen).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.TypeOfDeviceIDs.Add(values[2].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == GPS).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[3].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == Camera).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[4].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == Bluetooth).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[5].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == ScreenType).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[6].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == Dual).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[7].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == priceRangeCode).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[2].ToInt());
                            }
                        }
                    }

                    using (var proxy = new CatalogServiceProxy())
                    {
                        deviceProperties = proxy.DevicePropertyGet(proxy.DevicePropertyFind(devicePropertyFind).ToList()).ToList();

                        modelImages = proxy.ModelImageGet(proxy.ModelImageFind(new ModelImageFind()
                        {
                            ModelImage = new ModelImage(),
                            ModelIDs = deviceProperties.Select(a => a.ModelID).ToList(),
                            Active = true
                        })).OrderByDescending(a => a.CreateDT).ToList();

                        models = proxy.ModelGet(deviceProperties.Select(a => a.ModelID).ToList()).ToList();

                        //Begin code added by VLT on 9 Apr 2013 to implement stock logic in guided sales similar to device+plan 

                        modelIDs = deviceProperties.Select(a => a.ModelID).ToList();


                        #region Code for getting the count from the service by using dynamic proxy and geting the article id
                        List<Online.Registration.Web.ViewModels.ClassArticleId> lstarticleId = new List<Online.Registration.Web.ViewModels.ClassArticleId>();

                        int counter = 0;
                        using (var catalogProxy = new CatalogServiceClient())
                        {
                            for (int i = 0; i < modelIDs.Count; i++)
                            {
                                articleID = proxy.GetArticleID(modelIDs[i].ToInt()).ArticleIDs.ToList();

                                if (articleID.Count > 0)
                                {

                                    for (int j = 0; j < articleID.Count; j++)
                                    {
                                        Online.Registration.Web.ViewModels.ClassArticleId ClassArticleId = new Online.Registration.Web.ViewModels.ClassArticleId()
                                        {
                                            id = articleID[j].ID,
                                            articleId = articleID[j].ArticleID.Trim(),
                                            modelId = articleID[j].ModelID,
                                            imagePath = ReferenceEquals(articleID[j].ImagePath, null) ? string.Empty : articleID[j].ImagePath.ToString().Trim(),
                                            colourID = articleID[j].ColourID,

                                        };
                                        lstarticleId.Add(ClassArticleId);
                                    }
                                    counter++;
                                }
                            }
                            if (counter <= 0)
                            {
                                lstarticleId = null;
                            }
                        }


                        //Code by chetan moved the dynamic proxy call to integration service
                        string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                        int storeId = 0;
                        string[] maxisStoreIds = ConfigurationManager.AppSettings["OrganizationsIds"].ToString().Split(",".ToCharArray());
                        if (!maxisStoreIds.Contains(Util.SessionAccess.User.Org.OrganisationId))
                        {
                            storeId = Convert.ToInt32(Util.SessionAccess.User.Org.OrganisationId);
                        }
                        try
                        {
                            using (var dsproxy = new DynamicServiceProxy())
                            {
                                lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = (int)(dsproxy.GetStockCount(i.articleId, wsdlUrl,storeId)));
                            }
                        }
                        catch(Exception ex)
                        {
                            //RedirectToAction("StoreError", "HOME");
                            LogExceptions(ex);
                            throw ex;
                        }
                        //END HERE

                        #region Code commented by chetan moved the dynamic proxy call to integration service
                        /* adding the count depending on the article id */
                        //if (!ReferenceEquals(lstarticleId, null))
                        //{
                        //    lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = Count(i.articleId));
                        //}
                        /*Up to here*/

                        /*Code for calling the service using the dynamic proxy*/
                        //string strAPIUrl = "http://10.200.76.217/Storeservice/Services.asmx?wsdl";
                        //string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                        //string strArgs = "100000061001";
                        //int subscriptionReqFromVasResult;
                        //try
                        //{
                        //    DynamicProxyFactory factory = new DynamicProxyFactory(wsdlUrl);
                        //    if (factory.Contracts.Count() == 1)
                        //    {
                        //        DynamicProxy dynamicproxy = factory.CreateProxy(factory.Contracts.First().Name);
                        //        //subscriptionReqFromVasResult = (int)(dynamicproxy.CallMethod("GetStockCount", strArgs));
                        //        if (!ReferenceEquals(lstarticleId, null))
                        //        {
                        //            lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = (int)(dynamicproxy.CallMethod("GetStockCount", i.articleId)));
                        //        }

                        //        dynamicproxy.Close();
                        //    }
                        //    else
                        //    {
                        //        //TODO: if more than one, we need to loop through all contracts and findout our method. And need to use corresponding Contract otherwise throw an error
                        //    }
                        //}
                        //catch (Exception ex)
                        //{
                        //    RedirectToAction("StoreError", "HOME");
                        //}
                        /*end here*/
                        #endregion



                        if (!ReferenceEquals(lstarticleId, null))
                        {
                            foreach (var item in lstarticleId)
                            {
                                availableModelImages.Add(new AvailableModelImage()
                                {

                                    BrandArticle = new BrandArticle()
                                    {
                                        ID = item.id,
                                        ColourID = item.colourID,
                                        ImagePath = item.imagePath,
                                        ModelID = item.modelId,
                                        ArticleID = item.articleId,
                                    },
                                    ModelCode = models.Where(a => a.ID == item.modelId).SingleOrDefault().Code,
                                    Count = item.count

                                });
                            }
                        }
                        #endregion
                    }

                    devicePropertyVM.AvailableModelImage = availableModelImages;

                    //if (models.Count > 0)
                    //{
                    //    foreach (var item in modelImages)
                    //    {
                    //        devicePropertyVM.AvailableModelImage.Add(new AvailableModelImage()
                    //        {
                    //            ModelImage = new ModelImage()
                    //            {
                    //                ID = item.ID,
                    //                ColourID = item.ColourID,
                    //                ImagePath = item.ImagePath,
                    //                ModelID = item.ModelID
                    //            },
                    //            ModelCode = models.Where(a => a.ID == item.ModelID).SingleOrDefault().Code
                    //        });
                    //    }
                    //}

                    devicePropertyVM.IsSearch = true;
                    devicePropertyVM.SelectedModel = new SelectedModel();

                    ConstructDeviceSearchCriteria(devicePropertyVM);

                    return RedirectToAction("SelectDevice", "Registration");
                    #endregion
                }
                else if (devicePropertyVM.SelectedModel != null && !string.IsNullOrEmpty(devicePropertyVM.SelectedModel.ImageID))
                {

                    Session["GuidedSales_SelectedModelImageID"] = devicePropertyVM.SelectedModel.ImageID;
                    Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = devicePropertyVM.SelectedModel.ImageID;

                    if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt()) // go to credit check
                    {
                        ConstructGuidedSalesSession();

                        return RedirectToAction("SelectDevice", "Registration", new { type = MobileRegType.DevicePlan.ToInt() });
                    }
                    else if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt()) // go to guided sales plan screen
                    {
                        return RedirectToAction("Plan");
                    }
                }
                else // Back to Previous page
                {
                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return View(devicePropertyVM);
        }

        //Code by VLT
        public ActionResult DeviceSearch()
        {
            var planIDs = new List<int>();
            var guidedSalesVM = new GuidedSalesVM();
            try
            {
                guidedSalesVM.DevicePropertyVM.SelectedModel = new SelectedModel { ImageID = Session["GuidedSales_SelectedModelImageID"].ToString2() };
                guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID = Session["GuidedSales_PkgPgmBdlPkgCompID"].ToString2();


                if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt())
                {
                    if (!string.IsNullOrEmpty(guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID))
                    {
                        planIDs.Add(guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID.ToInt());

                        GetAvailablePackages(guidedSalesVM.PlanPropertyVM, planIDs);
                    }
                    else
                    {
                        GetAvailablePackages(guidedSalesVM.PlanPropertyVM);
                    }
                }
                else if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt())
                {
                    GetAvailableDevices(guidedSalesVM.DevicePropertyVM);
                }

                if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt() ||
                    Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt())
                {
                    guidedSalesVM.IsSearch = true;
                }

                ConstructDeviceSearchCriteria(guidedSalesVM.DevicePropertyVM);

                Session["GuidedSales_PkgPgmBdlPkgCompID"] = null;
            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return View(guidedSalesVM);
        }

        [HttpPost]
        public ActionResult PlanSearch(FormCollection collection, GuidedSalesVM guidedSalesVM)
        {
            if (collection["submit1"].ToString2() == "PlanSearch")
            {
                return RedirectToAction("SelectPlan", "Registration");
            }
            else if (collection["submit1"].ToString2() == "PlanAndDeviceSearch")
            {
                return RedirectToAction("SelectPlan", "Registration");
            }
            else
                return RedirectToAction("SelectPlan", "Registration");
        }

        //Coded by VLT
        public ActionResult PlanAndDevice()
        {
            var planIDs = new List<int>();
            var guidedSalesVM = new GuidedSalesVM();
            try
            {
                guidedSalesVM.DevicePropertyVM.SelectedModel = new SelectedModel { ImageID = Session["GuidedSales_SelectedModelImageID"].ToString2() };
                guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID = Session["GuidedSales_PkgPgmBdlPkgCompID"].ToString2();


                if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt())
                {
                    if (!string.IsNullOrEmpty(guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID))
                    {
                        planIDs.Add(guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID.ToInt());

                        GetAvailablePackages(guidedSalesVM.PlanPropertyVM, planIDs);
                    }
                    else
                    {
                        GetAvailablePackages(guidedSalesVM.PlanPropertyVM);
                    }
                }
                else if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt())
                {
                    GetAvailableDevices(guidedSalesVM.DevicePropertyVM);
                }

                if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt() ||
                    Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt())
                {
                    guidedSalesVM.IsSearch = true;
                }

                ConstructDeviceSearchCriteria(guidedSalesVM.DevicePropertyVM);

                Session["GuidedSales_PkgPgmBdlPkgCompID"] = null;
            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return View(guidedSalesVM);
        }

        //Coded by VLT
        public ActionResult PlanSearch()
        {
            var planIDs = new List<int>();
            var guidedSalesVM = new GuidedSalesVM();
            try
            {
                #region VLT ADDED CODE (18th Feb) newly added code for plan search range values for usages
                using (var proxy = new UserServiceProxy())
                {
                    PlanSearchItemResp obj = new PlanSearchItemResp();
                    obj.VoiceUsage = "VoiceUsage";
                    var voiceUsageMax = proxy.PlanMaxStatus(obj.VoiceUsage);
                    ViewBag.VoiceUsageMax = voiceUsageMax.VoiceUsage;

                    obj.VoiceUsage = "DataUsage";
                    var dataUsageMax = proxy.PlanMaxStatus(obj.VoiceUsage);
                    ViewBag.DataUsageMax = dataUsageMax.VoiceUsage;

                    obj.VoiceUsage = "SMSUsage";
                    var smsUsageMax = proxy.PlanMaxStatus(obj.VoiceUsage);
                    ViewBag.smsUsageMax = smsUsageMax.VoiceUsage;

                    obj.VoiceUsage = "Price";
                    var priceMax = proxy.PlanMaxStatus(obj.VoiceUsage);
                    ViewBag.PriceMax = priceMax.VoiceUsage;

                    var voiceUsageMin = proxy.PlanMinStatus(obj.VoiceUsage);
                    ViewBag.VoiceUsageMin = voiceUsageMin.VoiceUsage;

                    var dataUsageMin = proxy.PlanMinStatus(obj.VoiceUsage);
                    ViewBag.DataUsageMin = dataUsageMin.VoiceUsage;

                    var smsUsageMin = proxy.PlanMinStatus(obj.VoiceUsage);
                    ViewBag.smsUsageMin = smsUsageMin.VoiceUsage;

                    var priceMin = proxy.PlanMinStatus(obj.VoiceUsage);
                    ViewBag.PriceMin = priceMin.VoiceUsage;
                }
                #endregion range values

                guidedSalesVM.DevicePropertyVM.SelectedModel = new SelectedModel { ImageID = Session["GuidedSales_SelectedModelImageID"].ToString2() };
                guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID = Session["GuidedSales_PkgPgmBdlPkgCompID"].ToString2();


                if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt())
                {
                    if (!string.IsNullOrEmpty(guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID))
                    {
                        planIDs.Add(guidedSalesVM.PlanPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID.ToInt());

                        GetAvailablePackages(guidedSalesVM.PlanPropertyVM, planIDs);
                    }
                    else
                    {
                        GetAvailablePackages(guidedSalesVM.PlanPropertyVM);
                    }
                }
                else if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt())
                {
                    GetAvailableDevices(guidedSalesVM.DevicePropertyVM);
                }

                if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt() ||
                    Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt())
                {
                    guidedSalesVM.IsSearch = true;
                }

                ConstructDeviceSearchCriteria(guidedSalesVM.DevicePropertyVM);

                Session["GuidedSales_PkgPgmBdlPkgCompID"] = null;
            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return View(guidedSalesVM);
        }

        public ActionResult Plan()
        {
            var planPropertyVM = new PlanPropertyVM();
            var planIDs = new List<int>();

            try
            {

                planPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID = Session["GuidedSales_PkgPgmBdlPkgCompID"].ToString2();


                if (!string.IsNullOrEmpty(planPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID))
                {
                    planIDs.Add(planPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID.ToInt());

                    GetAvailablePackages(planPropertyVM, planIDs);
                }
                else if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt())
                {
                    GetAvailablePackages(planPropertyVM);

                    Session[SessionKey.MobileRegType.ToString()] = "Other";
                }



            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return View(planPropertyVM);
        }

        public ActionResult Device()
        {
            var modelImage = new ModelImage();
            var model = new Model();
            var devicePropertyVM = new DevicePropertyVM();

            try
            {
                devicePropertyVM.SelectedModel = new SelectedModel { ImageID = Session["GuidedSales_SelectedModelImageID"].ToString2() };

                GetAvailableDevices(devicePropertyVM);
                ConstructDeviceSearchCriteria(devicePropertyVM);

                if (!string.IsNullOrEmpty(devicePropertyVM.SelectedModel.ImageID))
                    devicePropertyVM.IsSearch = true;

                Session["GuidedSales_SelectedModelImageID"] = null;
            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }

            return View(devicePropertyVM);
        }

        private void ClearRegistrationSession()
        {
            Session["intmandatoryidsval"] = null;
            Session["intdataidsval"] = null;
            Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
            Session["RegMobileReg_BiometricVerify"] = null;
            Session["RegMobileReg_BiometricID"] = null;
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
            Session["RegMobileReg_SelectedModelID"] = null;
             Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
             Session[SessionKey.RegMobileReg_MobileNo.ToString()] = null;
             Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session["RegMobileReg_PhoneVM"] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session[SessionKey.RegStepNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardType.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = null;
             Session[SessionKey.RegMobileReg_SublineVM.ToString()] = null;

        }

        [HttpPost]
        public ActionResult Index(FormCollection collection, GuidedSalesVM guidedSalesVM)
        {
            #region VLT ADDED CODE
            #region added by Rajeswari on 13thMarch
            ClearRegistrationSession();
            #endregion
            Session[SessionKey.Searchstatus.ToString()] =string.Empty;

            if (collection["submit1"].ToString2() == "device")
            {

                Session["GuidedSales_Type"] = MobileRegType.DevicePlan.ToInt();
                return RedirectToAction("Device");
            }
            else if (collection["submit1"].ToString2() == "plan")
            {
                Session[SessionKey.MobileRegType.ToString()] = "Planonly";
                Session["GuidedSales_Type"] = MobileRegType.PlanOnly.ToInt();
                return RedirectToAction("Plan");
            }
            #endregion VLT ADDED CODE

            if (collection["submit1"].ToString2() == "PlanSearch" || collection["submit1"].ToString2() == "DeviceSearch")
            {
                Session["GuidedSales_PkgPgmBdlPkgCompID"] = null;
                Session["GuidedSales_SelectedModelImageID"] = null;
                guidedSalesVM.IsSearch = true;
                ConstructDeviceSearchCriteria(guidedSalesVM.DevicePropertyVM);
            }

            if (collection["submit1"].ToString2() == "PlanSearch")
            {
                Session[SessionKey.Searchstatus.ToString()] = "PlanSearch";
                Session["GuidedSales_Type"] = MobileRegType.PlanOnly.ToInt();

                Session[SessionKey.RegMobileReg_Type.ToString()] = "2";
                Session[SessionKey.MobileRegType.ToString()] = "2";

                #region Search

                var planProperties = new List<PlanProperty>();

                using (var proxy = new CatalogServiceProxy())
                {
                    planProperties = proxy.PlanPropertyGet(proxy.PlanPropertyFind(new PlanPropertyFind()
                    {
                        PlanProperty = new PlanProperty()
                        {
                            DataUsage = guidedSalesVM.PlanPropertyVM.PlanOption.DataUsage,
                            VoiceUsage = guidedSalesVM.PlanPropertyVM.PlanOption.VoiceUsage,
                            SMSUsage = guidedSalesVM.PlanPropertyVM.PlanOption.SMSUsage,
                            Price = guidedSalesVM.PlanPropertyVM.PlanOption.Price
                        },
                        Active = true
                    })).ToList();
                }

                GetAvailablePackages(guidedSalesVM.PlanPropertyVM, planProperties.Select(a => a.BdlPkgID).ToList());

                #endregion
            }

            else if (collection["submit1"].ToString2() == "DeviceSearch")
            {
                Session[SessionKey.Searchstatus.ToString()] = "DeviceSearch";
                Session["GuidedSales_Type"] = MobileRegType.DevicePlan.ToInt();
                Session[SessionKey.RegMobileReg_Type.ToString()] = "1";
                Session[SessionKey.MobileRegType.ToString()] = "1";

                #region Search

                bool? hasCamera = null;
                var models = new List<Model>();
                var modelIDs = new List<int>();
                var articleID = new List<BrandArticle>();
                var availableModelImages = new List<AvailableModelImage>();
                var modelImages = new List<ModelImage>();
                var deviceProperties = new List<DeviceProperty>();
                var props = new List<Property>();
                Session["SelectedCriterias"] = guidedSalesVM.SelectedCriterias;
                var chkBoxes = string.IsNullOrEmpty(guidedSalesVM.SelectedCriterias) ? new List<string>().ToArray() : guidedSalesVM.SelectedCriterias.Split(',');
                var criterias = chkBoxes.Select(a => a.Split('_')).ToList();
                using (var proxy = new CatalogServiceProxy())
                {
                    props = proxy.PropertyGet(proxy.PropertyFind(new DAL.Models.PropertyFind()
                    {
                        Property = new DAL.Models.Property()
                        {
                            Type = Properties.Settings.Default.Property_DeviceType
                        },
                        Active = true
                    }).ToList()).ToList();
                }
                var devicePropertyFind = new DevicePropertyFind();
                var brandCode = Properties.Settings.Default.Property_Brand;
                var TouchScreen = Properties.Settings.Default.Property_TouchScreen;
                var GPS = Properties.Settings.Default.Property_GPS;
                var Camera = Properties.Settings.Default.Property_Camera;
                var Bluetooth = Properties.Settings.Default.Property_Bluetooth;
                var ScreenType = Properties.Settings.Default.Property_ScreenType;
                var Dual = Properties.Settings.Default.Property_Dual;
                var priceRangeCode = Properties.Settings.Default.Property_PriceRange;

                foreach (var values in criterias)
                {
                    if (!string.IsNullOrEmpty(values[0]))
                    {
                        if (values[1].ToInt() == props.Where(a => a.Code == brandCode).Select(a => a.ID).SingleOrDefault().ToInt())
                        {
                            devicePropertyFind.BrandIDs.Add(values[2].ToInt());
                        }
                        else if (values[1].ToInt() == props.Where(a => a.Code == priceRangeCode).Select(a => a.ID).SingleOrDefault().ToInt())
                        {
                            devicePropertyFind.PriceRangeIDs.Add(values[2].ToInt());
                        }
                        else if (values[1].ToInt() == props.Where(a => a.Code == TouchScreen).Select(a => a.ID).SingleOrDefault().ToInt())
                        {
                            devicePropertyFind.TouchScreenIDs.Add(values[2].ToInt());
                        }
                        else if (values[1].ToInt() == props.Where(a => a.Code == GPS).Select(a => a.ID).SingleOrDefault().ToInt())
                        {
                            devicePropertyFind.GPS.Add(values[2].ToInt());
                        }
                        else if (values[1].ToInt() == props.Where(a => a.Code == Camera).Select(a => a.ID).SingleOrDefault().ToInt())
                        {
                            devicePropertyFind.Camera.Add(values[2].ToInt());
                        }
                        else if (values[1].ToInt() == props.Where(a => a.Code == Bluetooth).Select(a => a.ID).SingleOrDefault().ToInt())
                        {
                            devicePropertyFind.Bluetooth.Add(values[2].ToInt());
                        }
                        else if (values[1].ToInt() == props.Where(a => a.Code == ScreenType).Select(a => a.ID).SingleOrDefault().ToInt())
                        {
                            devicePropertyFind.ScreenType.Add(values[2].ToInt());
                        }
                        else if (values[1].ToInt() == props.Where(a => a.Code == Dual).Select(a => a.ID).SingleOrDefault().ToInt())
                        {
                            devicePropertyFind.DualFront.Add(values[2].ToInt());
                        }
                    }
                }

                using (var proxy = new CatalogServiceProxy())
                {

                    deviceProperties = proxy.DevicePropertyGet(proxy.DevicePropertyFind(devicePropertyFind).ToList()).ToList();

                    modelImages = proxy.ModelImageGet(proxy.ModelImageFind(new ModelImageFind()
                    {
                        ModelImage = new ModelImage(),
                        ModelIDs = deviceProperties.Select(a => a.ModelID).ToList(),
                        Active = true
                    })).OrderByDescending(a => a.CreateDT).ToList();

                    models = proxy.ModelGet(deviceProperties.Select(a => a.ModelID).ToList()).ToList();

                    //Begin code added by VLT on 9 Apr 2013 to implement stock logic in guided sales similar to device+plan 

                    modelIDs = deviceProperties.Select(a => a.ModelID).ToList();


                    #region Code for getting the count from the service by using dynamic proxy and geting the article id
                    List<Online.Registration.Web.ViewModels.ClassArticleId> lstarticleId = new List<Online.Registration.Web.ViewModels.ClassArticleId>();

                    int counter = 0;
                    using (var catalogProxy = new CatalogServiceClient())
                    {
                        for (int i = 0; i < modelIDs.Count; i++)
                        {
                            articleID = proxy.GetArticleID(modelIDs[i].ToInt()).ArticleIDs.ToList();

                            if (articleID.Count > 0)
                            {

                                for (int j = 0; j < articleID.Count; j++)
                                {
                                    Online.Registration.Web.ViewModels.ClassArticleId ClassArticleId = new Online.Registration.Web.ViewModels.ClassArticleId()
                                    {
                                        id = articleID[j].ID,
                                        articleId = articleID[j].ArticleID.Trim(),
                                        modelId = articleID[j].ModelID,
                                        imagePath = ReferenceEquals(articleID[j].ImagePath, null) ? string.Empty : articleID[j].ImagePath.ToString().Trim(),
                                        colourID = articleID[j].ColourID,

                                    };
                                    lstarticleId.Add(ClassArticleId);
                                }
                                counter++;
                            }
                        }
                        if (counter <= 0)
                        {
                            lstarticleId = null;
                        }
                    }

                    //Code by chetan moved the dynamic proxy call to integration service
                    string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                    int storeId = 0;
                    string[] maxisStoreIds = ConfigurationManager.AppSettings["OrganizationsIds"].ToString().Split(",".ToCharArray());
                    if (!maxisStoreIds.Contains(Util.SessionAccess.User.Org.OrganisationId))
                    {
                        storeId = Convert.ToInt32(Util.SessionAccess.User.Org.OrganisationId);
                    }
                    try
                    {
                        using (var dsproxy = new DynamicServiceProxy())
                        {
                            lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = (int)(dsproxy.GetStockCount(i.articleId, wsdlUrl,storeId)));
                        }
                    }
                    catch(Exception ex)
                    {
                        //RedirectToAction("StoreError", "HOME");
                        LogExceptions(ex);
                        throw ex;
                    }
                    //END HERE

                    if (!ReferenceEquals(lstarticleId, null))
                    {
                        foreach (var item in lstarticleId)
                        {
                            availableModelImages.Add(new AvailableModelImage()
                            {

                                BrandArticle = new BrandArticle()
                                {
                                    ID = item.id,
                                    ColourID = item.colourID,
                                    ImagePath = item.imagePath,
                                    ModelID = item.modelId,
                                    ArticleID = item.articleId,
                                },
                                ModelCode = models.Where(a => a.ID == item.modelId).SingleOrDefault().Code,
                                Count = item.count

                            });
                        }
                    }
                    #endregion
                }


                guidedSalesVM.DevicePropertyVM.AvailableModelImage = availableModelImages;

                guidedSalesVM.DevicePropertyVM.IsSearch = true;
                guidedSalesVM.DevicePropertyVM.SelectedModel = new SelectedModel();

                #endregion
            }



            return View(guidedSalesVM);
        }

        [HttpPost]
        public ActionResult Device(FormCollection collection, DevicePropertyVM devicePropertyVM)
        {
            try
            {
                if (collection["submit1"].ToString2() == "Search")
                {
                    Session[SessionKey.RegMobileReg_Type.ToString()] = "1";
                    Session[SessionKey.MobileRegType.ToString()] = "1";
                    #region Search

                    bool? hasCamera = null;
                    var models = new List<Model>();
                    var modelIDs = new List<int>();
                    var articleID = new List<BrandArticle>();
                    var availableModelImages = new List<AvailableModelImage>();
                    var modelImages = new List<ModelImage>();
                    var deviceProperties = new List<DeviceProperty>();
                    var props = new List<Property>();
                    var chkBoxes = string.IsNullOrEmpty(devicePropertyVM.SelectedCriterias) ? new List<string>().ToArray() : devicePropertyVM.SelectedCriterias.Split(',');
                    var criterias = chkBoxes.Select(a => a.Split('_')).ToList();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        props = proxy.PropertyGet(proxy.PropertyFind(new DAL.Models.PropertyFind()
                        {
                            Property = new DAL.Models.Property()
                            {
                                Type = Properties.Settings.Default.Property_DeviceType
                            },
                            Active = true
                        }).ToList()).ToList();
                    }

                    var devicePropertyFind = new DevicePropertyFind();

                    var brandCode = Properties.Settings.Default.Property_Brand;
                    var priceRangeCode = Properties.Settings.Default.Property_PriceRange;
                    var TouchScreen = Properties.Settings.Default.Property_TouchScreen;
                    var GPS = Properties.Settings.Default.Property_GPS;
                    var Camera = Properties.Settings.Default.Property_Camera;
                    var Bluetooth = Properties.Settings.Default.Property_Bluetooth;
                    var ScreenType = Properties.Settings.Default.Property_ScreenType;
                    var Dual = Properties.Settings.Default.Property_Dual;

                    foreach (var values in criterias)
                    {
                        if (!string.IsNullOrEmpty(values[0]))
                        {
                            if (values[1].ToInt() == props.Where(a => a.Code == brandCode).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.BrandIDs.Add(values[2].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == TouchScreen).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.TypeOfDeviceIDs.Add(values[2].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == GPS).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[3].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == Camera).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[4].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == Bluetooth).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[5].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == ScreenType).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[6].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == Dual).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[7].ToInt());
                            }
                            else if (values[1].ToInt() == props.Where(a => a.Code == priceRangeCode).Select(a => a.ID).SingleOrDefault().ToInt())
                            {
                                devicePropertyFind.PriceRangeIDs.Add(values[2].ToInt());
                            }
                        }
                    }

                    using (var proxy = new CatalogServiceProxy())
                    {
                        deviceProperties = proxy.DevicePropertyGet(proxy.DevicePropertyFind(devicePropertyFind).ToList()).ToList();

                        modelImages = proxy.ModelImageGet(proxy.ModelImageFind(new ModelImageFind()
                        {
                            ModelImage = new ModelImage(),
                            ModelIDs = deviceProperties.Select(a => a.ModelID).ToList(),
                            Active = true
                        })).OrderByDescending(a => a.CreateDT).ToList();

                        models = proxy.ModelGet(deviceProperties.Select(a => a.ModelID).ToList()).ToList();

                        //Begin code added by VLT on 9 Apr 2013 to implement stock logic in guided sales similar to device+plan 

                        modelIDs = deviceProperties.Select(a => a.ModelID).ToList();


                        #region Code for getting the count from the service by using dynamic proxy and geting the article id
                        List<Online.Registration.Web.ViewModels.ClassArticleId> lstarticleId = new List<Online.Registration.Web.ViewModels.ClassArticleId>();

                        int counter = 0;
                        using (var catalogProxy = new CatalogServiceClient())
                        {
                            for (int i = 0; i < modelIDs.Count; i++)
                            {
                                articleID = proxy.GetArticleID(modelIDs[i].ToInt()).ArticleIDs.ToList();

                                if (articleID.Count > 0)
                                {

                                    for (int j = 0; j < articleID.Count; j++)
                                    {
                                        Online.Registration.Web.ViewModels.ClassArticleId ClassArticleId = new Online.Registration.Web.ViewModels.ClassArticleId()
                                        {
                                            id = articleID[j].ID,
                                            articleId = articleID[j].ArticleID.Trim(),
                                            modelId = articleID[j].ModelID,
                                            imagePath = ReferenceEquals(articleID[j].ImagePath, null) ? string.Empty : articleID[j].ImagePath.ToString().Trim(),
                                            colourID = articleID[j].ColourID,

                                        };
                                        lstarticleId.Add(ClassArticleId);
                                    }
                                    counter++;
                                }
                            }
                            if (counter <= 0)
                            {
                                lstarticleId = null;
                            }
                        }

                        //Code by chetan moved the dynamic proxy call to integration service
                        string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                        int storeId = 0;
                        string[] maxisStoreIds = ConfigurationManager.AppSettings["OrganizationsIds"].ToString().Split(",".ToCharArray());
                        if (!maxisStoreIds.Contains(Util.SessionAccess.User.Org.OrganisationId))
                        {
                            storeId = Convert.ToInt32(Util.SessionAccess.User.Org.OrganisationId);
                        }
                        try
                        {
                            using (var dsproxy = new DynamicServiceProxy())
                            {
                                lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = (int)(dsproxy.GetStockCount(i.articleId, wsdlUrl,storeId)));
                            }
                        }
                        catch (Exception ex)
                        {
                           // RedirectToAction("StoreError", "HOME");
                            LogExceptions(ex);
                            throw ex;
                        }
                        //END HERE

                        if (!ReferenceEquals(lstarticleId, null))
                        {
                            foreach (var item in lstarticleId)
                            {
                                availableModelImages.Add(new AvailableModelImage()
                                {

                                    BrandArticle = new BrandArticle()
                                    {
                                        ID = item.id,
                                        ColourID = item.colourID,
                                        ImagePath = item.imagePath,
                                        ModelID = item.modelId,
                                        ArticleID = item.articleId,
                                    },
                                    ModelCode = models.Where(a => a.ID == item.modelId).SingleOrDefault().Code,
                                    Count = item.count

                                });
                            }
                        }
                        #endregion
                    }

                    devicePropertyVM.AvailableModelImage = availableModelImages;

                    devicePropertyVM.IsSearch = true;
                    devicePropertyVM.SelectedModel = new SelectedModel();

                    ConstructDeviceSearchCriteria(devicePropertyVM);

                    return View(devicePropertyVM);

                    #endregion
                }
                else if (devicePropertyVM.SelectedModel != null && !string.IsNullOrEmpty(devicePropertyVM.SelectedModel.ImageID))
                {
                    Session["GuidedSales_SelectedModelImageID"] = devicePropertyVM.SelectedModel.ImageID;

                    if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt()) // go to credit check
                    {
                        ConstructGuidedSalesSession();

                        return RedirectToAction("SelectDevice", "Registration", new { type = MobileRegType.DevicePlan.ToInt() });
                    }
                    else if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt()) // go to guided sales plan screen
                    {
                        if (ReferenceEquals( Session[SessionKey.PPID.ToString()], null))
                        {
                            #region Added by Rajeswari
                            BusinessValidationChecks("N");
                            #endregion
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            return RedirectToAction("Plan");
                        }

                    }
                }
                else // Back to Previous page
                {
                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return View(devicePropertyVM);
        }

        [HttpPost]
        public ActionResult Plan(FormCollection collection, PlanPropertyVM planPropertyVM)
        {
            if (collection["submit1"].ToString2() == "Search")
            {

                #region Search

                var planProperties = new List<PlanProperty>();

                using (var proxy = new CatalogServiceProxy())
                {
                    planProperties = proxy.PlanPropertyGet(proxy.PlanPropertyFind(new PlanPropertyFind()
                    {
                        PlanProperty = new PlanProperty()
                        {
                            DataUsage = planPropertyVM.PlanOption.DataUsage,
                            VoiceUsage = planPropertyVM.PlanOption.VoiceUsage,
                            SMSUsage = planPropertyVM.PlanOption.SMSUsage,
                            Price = planPropertyVM.PlanOption.Price
                        },
                        Active = true
                    })).ToList();
                }

                GetAvailablePackages(planPropertyVM, planProperties.Select(a => a.BdlPkgID).ToList());



                #endregion
            }
            else if (planPropertyVM.SelectedPgmBdlPkgComp != null && !string.IsNullOrEmpty(planPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID))
            {

                Session["GuidedSales_PkgPgmBdlPkgCompID"] = planPropertyVM.SelectedPgmBdlPkgComp.PgmBdlPkgCompID;

                if (Session["GuidedSales_PkgPgmBdlPkgCompID"] != null)
                {
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var pbpc = MasterDataCache.Instance.FilterComponents(new int[] { Convert.ToInt32(Session["GuidedSales_PkgPgmBdlPkgCompID"].ToString()) }).SingleOrDefault();
                        if (pbpc != null)

                            //Chnages made by chetan for SKMM
                            Session["UOMPlanID"] = pbpc.KenanCode;

                    }
                }
                if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt()) // go to credit check
                {
                    ConstructGuidedSalesSession();

                    //  return RedirectToAction("SelectPlan", "Registration", new { type = MobileRegType.DevicePlan.ToInt() });
                    return RedirectToAction("SelectVAS", "Registration");
                }
                else if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt()) // go to guided sales device screen
                {
                    Session[SessionKey.MobileRegType.ToString()] = "Planonly";
                    return RedirectToAction("SelectVAS", "Registration", new { type = MobileRegType.PlanOnly.ToInt() });

                }
            }
            else
            {
                return RedirectToAction("Index");
            }

            return View(planPropertyVM);
        }

        private void ConstructDeviceSearchCriteria(DevicePropertyVM devicePropertyVM)
        {
            var propValues = new List<PropertyValue>();
            var props = new List<Property>();

            using (var proxy = new CatalogServiceProxy())
            {
                var propIDs = proxy.PropertyFind(new DAL.Models.PropertyFind()
                {
                    Property = new DAL.Models.Property()
                    {
                        Type = Properties.Settings.Default.Property_DeviceType
                    },
                    Active = true
                }).ToList();

                if (propIDs.Count() > 0)
                {
                    props = proxy.PropertyGet(propIDs).ToList();

                    var propValueIDs = proxy.PropertyValueFind(new DAL.Models.PropertyValueFind()
                    {
                        PropertyValue = new PropertyValue(),
                        PropertyIDs = propIDs,
                        Active = true
                    });

                    propValues = proxy.PropertyValueGet(propValueIDs).ToList();
                }
            }

            var deviveOptions = props.Select(a => new DeviceOption()
            {
                PropertyID = a.ID,
                PropertyName = a.Name,
                Criterias = propValues.Where(aa => aa.PropertyID == a.ID).Select(aa => new { aa.ID, aa.Name }).AsEnumerable().ToDictionary(b => b.ID, b => b.Name)
            }).ToList();

            if (deviveOptions == null)
                deviveOptions = new List<DeviceOption>();

            devicePropertyVM.DeviceOptions = deviveOptions;
        }

        private void GetAvailablePackages(PlanPropertyVM planPropertyVM, List<int> planIDs = null)
        {
            var bundleIDs = new List<int>();
            var filterBdlIDs = new List<int>();
            var bundlePackages = new List<PgmBdlPckComponent>();
            var packageIDs = new List<int>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var packages = new List<Package>();
            var bundles = new List<Bundle>();
            var programBundles = new List<PgmBdlPckComponent>();
            var programs = new List<Program>();
            var availablePackages = new List<AvailablePackage>();
            var pgmBdlIDs = planIDs;
            var orderVM = new OrderSummaryVM();
            using (var proxy = new CatalogServiceProxy())
            {
                // retrieve bundles that assigned to current user
                var filterBdlPkgIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.Program_Bundle,
                        FilterOrgType = Util.SessionOrgTypeCode
                    },
                    Active = true
                }).ToList();

                filterBdlIDs = MasterDataCache.Instance.FilterComponents(filterBdlPkgIDs).Select(a => a.ChildID).ToList();
                bundleIDs = filterBdlIDs;

                if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt())
                {
                    /*Commented for articleid*/
                    //var modelID = proxy.ModelImageGet(Session["GuidedSales_SelectedModelImageID"].ToInt()).ModelID;
                    var modelID = proxy.BrandArticleImageGet(Session["GuidedSales_SelectedModelImageID"].ToInt()).ModelID;

                    var modelGroupIDs = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                    {
                        ModelGroupIDs = new List<int>(),
                        ModelGroupModel = new ModelGroupModel()
                        {
                            ModelID = modelID
                        },
                        Active = true
                    })).Select(a => a.ModelGroupID).Distinct().ToList();

                    // Bundle - Package
                    var pbpcIDs = proxy.ModelGroupGet(modelGroupIDs).Select(a => a.PgmBdlPckComponentID).ToList();

                    bundlePackages = MasterDataCache.Instance.FilterComponents(pbpcIDs).Where(a => a.Active == true).ToList();
                    bundleIDs = filterBdlIDs.Intersect(bundlePackages.Select(a => a.ParentID)).ToList();
                    packageIDs = bundlePackages.Select(a => a.ChildID).ToList();
                }

                var query = new List<PgmBdlPckComponent>();
                //if (planIDs == null)
                //{
                query = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.Bundle_Package,
                        PlanType = Properties.Settings.Default.ComplimentaryPlan,
                    },
                    ParentIDs = bundleIDs,
                    ChildIDs = packageIDs,
                    Active = true
                })).ToList();
               
                if (Session["GuidedSales_Type"].ToInt() == (int)MobileRegType.DevicePlan)
                {
                    pgmBdlPckComponents = query.Where(a => bundleIDs.Contains(a.ParentID)).ToList();
                }
                else if (Session["GuidedSales_Type"].ToInt() == (int)MobileRegType.PlanOnly)
                {
                    pgmBdlPckComponents = query.ToList();
                }
                //pgmBdlPckComponents = query.Where(a => bundleIDs.Contains(a.ParentID)).ToList();

                if (pgmBdlPckComponents.Count() > 0)
                {
                    packages = proxy.PackageGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();
                    bundles = proxy.BundleGet(pgmBdlPckComponents.Select(a => a.ParentID).Distinct()).ToList();
                }

                pgmBdlIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.Program_Bundle,
                    },
                    ChildIDs = pgmBdlPckComponents.Select(a => a.ParentID).ToList()
                }).ToList();

                programBundles = MasterDataCache.Instance.FilterComponents(pgmBdlIDs).Distinct().ToList();

                var programIDs = programBundles.Select(a => a.ParentID).ToList();

                if (programIDs != null)
                {
                    if (programIDs.Count() > 0)
                    {
                        programs = proxy.ProgramGet(programIDs).ToList();
                    }
                }
            }

            foreach (var pbpc in pgmBdlPckComponents)
            {
                availablePackages.Add(new AvailablePackage()
                {
                    PgmBdlPkgCompID = pbpc.ID,
                    ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
                    BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
                    PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                    PackageCode = pbpc.Code,
                    //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
                    KenanCode = pbpc.KenanCode,
                });
            }
            if ((OrderSummaryVM) Session[SessionKey.RegMobileReg_OrderSummary.ToString()] != null)
                orderVM = (OrderSummaryVM) Session[SessionKey.RegMobileReg_OrderSummary.ToString()];

            planPropertyVM.IsSearch = true;

            //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
            if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt())
            {
                if (Session["GuidedSales_SelectedModelImageID"] != null)
                {
                    BrandArticle brandArticle = new BrandArticle();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        brandArticle = proxy.BrandArticleImageGet(Session["GuidedSales_SelectedModelImageID"].ToInt());

                        if (brandArticle != null)
                        {
                            if (brandArticle.ArticleID != null)
                            {
                                string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
                   
                                List<string> objPackages = new List<string>();

                                objPackages = proxy.GetPackagesForArticleId(brandArticle.ArticleID, MOCStatus);

                                if (objPackages != null && objPackages.Count > 0)
                                    planPropertyVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).Where(a => string.Join(",", objPackages).Contains(a.KenanCode)).ToList();
                            }
                            else
                                planPropertyVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();



                        }
                        else
                            planPropertyVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();

                    }
                }
                else
                    planPropertyVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();
            }
            else planPropertyVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();
            //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid

        }

        private void GetAvailableDevices(DevicePropertyVM devicePropertyVM)
        {
            var models = new List<Model>();
            var modelImages = new List<ModelImage>();
            var modelImage = new ModelImage();
            var brandArticle = new BrandArticle();
            var model = new Model();


            if (Session["GuidedSales_SelectedModelImageID"].ToInt() != 0)
            {
                using (var proxy = new CatalogServiceProxy())
                {
                    /*Commented for articleid*/
                    //modelImage = proxy.ModelImageGet(Session["GuidedSales_SelectedModelImageID"].ToInt());
                    brandArticle = proxy.BrandArticleImageGet(Session["GuidedSales_SelectedModelImageID"].ToInt());

                    /*Commented for articleid*/
                    //model = proxy.ModelGet(modelImage.ModelID);
                    model = proxy.ModelGet(brandArticle.ModelID);
                }

                devicePropertyVM.AvailableModelImage.Add(new AvailableModelImage()
                {
                    BrandArticle = new BrandArticle()
                    {
                        ID = brandArticle.ID,
                        ColourID = brandArticle.ColourID,
                        ImagePath = brandArticle.ImagePath,
                        ModelID = brandArticle.ModelID,
                        ArticleID = brandArticle.ArticleID
                    },
                    ModelCode = model.Code
                });
            }
            //}
            if (Session["GuidedSales_Type"].ToInt() == MobileRegType.PlanOnly.ToInt())
            {
                var pbpcIDs = new List<int>();
                pbpcIDs.Add(Session["GuidedSales_PkgPgmBdlPkgCompID"].ToInt());

                using (var proxy = new CatalogServiceProxy())
                {
                    var modelGroupIDs = proxy.ModelGroupFind(new ModelGroupFind()
                    {
                        ModelGroup = new ModelGroup(),
                        PgmBdlPkgCompIDs = pbpcIDs,
                        Active = true
                    }).ToList();

                    var mdlGrpModelIDs = proxy.ModelGroupModelFind(new ModelGroupModelFind()
                    {
                        ModelGroupModel = new ModelGroupModel(),
                        ModelGroupIDs = modelGroupIDs,
                        Active = true
                    });

                    var mdlGrpModels = proxy.ModelGroupModelGet(mdlGrpModelIDs).ToList();

                    modelImages = proxy.ModelImageGet(proxy.ModelImageFind(new ModelImageFind()
                    {
                        ModelImage = new ModelImage(),
                        ModelIDs = mdlGrpModels.Select(a => a.ModelID).ToList(),
                        Active = true
                    })).OrderByDescending(a => a.CreateDT).ToList();

                    models = proxy.ModelGet(mdlGrpModels.Select(a => a.ModelID).ToList()).ToList();
                }

                foreach (var item in modelImages)
                {
                    devicePropertyVM.AvailableModelImage.Add(new AvailableModelImage()
                    {
                        ModelImage = new ModelImage()
                        {
                            ID = item.ID,
                            ColourID = item.ColourID,
                            ImagePath = item.ImagePath,
                            ModelID = item.ModelID
                        },
                        ModelCode = models.Where(a => a.ID == item.ModelID).SingleOrDefault().Code
                    });
                }
            }
        }

        private void ConstructGuidedSalesSession()
        {
            Session["GuidedSales_Type"] = MobileRegType.DevicePlan.ToInt();

            using (var proxy = new CatalogServiceProxy())
            {
                /*Commented for articleid*/
                //var modelImage = proxy.ModelImageGet(Session["GuidedSales_SelectedModelImageID"].ToInt());
                var modelImage = proxy.BrandArticleImageGet(Session["GuidedSales_SelectedModelImageID"].ToInt());

                var model = proxy.ModelGet(modelImage.ModelID);

                Session["GuidedSales_SelectedBrandID"] = model.BrandID;
                Session["GuidedSales_SelectedModelID"] = model.ID;
                /*Coded for articleid*/
                Session[SessionKey.ArticleId.ToString()] = modelImage.ArticleID;
            }
        }

        private void LogExceptions(Exception ex)
        {
            /* BEGIN  Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/
            string strErrorMsg = Util.LogException(ex);
            string actionName = this.ControllerContext.RouteData.Values["action"] != null ? this.ControllerContext.RouteData.Values["action"].ToString() : "";
            string controllerName = this.ControllerContext.RouteData.Values["controller"] != null ? this.ControllerContext.RouteData.Values["controller"].ToString() : "";
            Logger.Error("Controller: " + controllerName + "\n" + " Action:" + actionName + "\n" + " Error:" + strErrorMsg, ex);
            /* END Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/

        }

        private void SetAgeSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsAgeCheckFailed.ToString()] = resp.IsAgeCheckFailed;
            Session[SessionKey.RegMobileReg_AgeCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()] = resp.IsAgeCheckFailed ? "AF" : "AS";
        }

        private void SetDDMFSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsDDMFCheckFailed.ToString()] = resp.IsDDMFCheckFailed;
            Session[SessionKey.RegMobileReg_DDMFCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()] = resp.IsDDMFCheckFailed ? "DF" : "DS";
        }

        private void SetAddressSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsAddressCheckFailed.ToString()] = resp.IsAddressCheckFailed;
            Session[SessionKey.RegMobileReg_AddressCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()] = resp.IsAddressCheckFailed ? "ADF" : "ADS";
        }

        private void SetTotalLineCheckSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsTotalLineCheckFailed.ToString()] = resp.IsTotalLineCheckFailed;
            Session[SessionKey.RegMobileReg_TotalLineCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()] = resp.IsTotalLineCheckFailed ? "TLF" : "TLS";
        }

        private void SetPrincipalLineCheckSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsPrincipalLineCheckFailed.ToString()] = resp.IsPrincipleLineCheckFailed;
            Session[SessionKey.RegMobileReg_PricinpalLineCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()] = resp.IsPrincipleLineCheckFailed ? "PLF" : "PLS";
        }

        private void SetContractCheckSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsContractCheckFailed.ToString()] = resp.IsContractCheckFailed;
            Session[SessionKey.RegMobileReg_ContractCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()] = resp.IsContractCheckFailed ? "CF" : "CS";
        }

        private void SetOutstandingCheckSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckFailed.ToString()] = resp.IsOutstandingCreditCheckFailed;
            Session[SessionKey.RegMobileReg_OutstandingCreditCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()] = resp.IsOutstandingCreditCheckFailed ? "OF" : "OS";
        }
    }
}
