﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using log4net;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.ContentRepository;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.Models;
using Online.Registration.Web.Properties;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.ViewModels;
using Online.Web.Helper;
using SNT.Utility;

namespace Online.Registration.Web.Controllers
{
    //[HandleError(View = "GenericError")]
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class NewLineController : Controller
    {
        //
        // GET: /NewLine/
        #region Member Declarations
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NewLineController));
        #endregion

        private void LogException(Exception ex)
        {
            Logger.DebugFormat("Exception: {0}", ex.Message);
        }

        #region Action Methods
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult AddNewLine()
        {
            var dropObj = new DropFourObj();
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            Session[SessionKey.FromPromoOffer.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session[SessionKey.SelectedPlanID_Seco.ToString()] = null;
            Session[SessionKey.VasGroupIds.ToString()] = null;
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session["vasBackClick"] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] = null;
            Session[SessionKey.RegMobileReg_MobileNo.ToString()] = null;
            Session[SessionKey.SelectedPlanID_Seco.ToString()] = null;
            Session[SessionKey.SimType.ToString()] = null;
            Session[SessionKey.Condition.ToString()] = null;
            Session["AccountHolder"] = null;
            Session["_componentViewModel"] = null;
            Session[SessionKey.AccountCategory.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session["SMEIPARENTID"] = null;
            WebHelper.Instance.ResetDepositSessions();

            #region added by Rajeswari
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
            {
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            }
            #endregion

            retrieveAcctListByICResponse AcctListByICResponse = null;
            SupplementaryListAccounts SuppListAccounts = null;

            ///BELOW ERROR MIGHT GET POPULATED FROM TARGET PAGE
            if (!ReferenceEquals(TempData["Error"], null))
            {
                ModelState.AddModelError("Error", "Select an account");
            }

            ///CHECK PPID AND PPIDINFO SESSIONS
            if ((!ReferenceEquals(Session[SessionKey.PPID.ToString()], null) && "E".Equals(Convert.ToString(Session[SessionKey.PPID.ToString()]))) && !ReferenceEquals(Session[SessionKey.PPIDInfo.ToString()], null))
            {
                ///GET PPIDInfo FROM SESSION
                AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()];

                ///LIST ONLY GSM A/C'S
                if (AcctListByICResponse.itemList.Where(c => c.Account != null && c.ServiceInfoResponse != null && ((c.ServiceInfoResponse.prinSuppInd != "" && c.ServiceInfoResponse.lob == "POSTGSM") || c.ServiceInfoResponse.lob == "HSDPA")).Count() > 0)
                {
                    SuppListAccounts = new SupplementaryListAccounts();

                    foreach (var v in AcctListByICResponse.itemList.Where(c => c.Account != null && c.ServiceInfoResponse != null && ((c.ServiceInfoResponse.prinSuppInd != "" && c.ServiceInfoResponse.lob == "POSTGSM") || c.ServiceInfoResponse.lob == "HSDPA")).ToList())
                    {
                        if (v.ServiceInfoResponse.prinSuppInd == "P" || v.ServiceInfoResponse.prinSuppInd == "")
                        {
                            SuppListAccounts.SuppListAccounts.Add(
                                new AddSuppInquiryAccount
                                {
                                    AccountNumber = v.AcctExtId,
                                    ActiveDate = v.Account.ActiveDate,
                                    Address = v.Account.Address,
                                    CompanyName = v.Account.CompanyName,
                                    Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                    IDNumber = v.Account.IDNumber,
                                    IDType = v.Account.IDType,
                                    Plan = v.Account.Plan,
                                    SubscribeNo = v.Account.SubscribeNo,
                                    SubscribeNoResets = v.Account.SubscribeNoResets,
                                    AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                    AccountName = v.ServiceInfoResponse.lob,
                                    AccountType = "P",
                                    externalId = v.ExternalId,
                                    accountExtId = v.AcctExtId,
                                    accountIntId = v.AcctIntId,
                                    IsMISM = v.IsMISM,
                                    Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                                    MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                                    ParentID = v.AccountDetails != null ? v.AccountDetails.ParentId.ToString2() : string.Empty
                                });
                        }
                    }
                }
            }
            return View(SuppListAccounts);
        }

        [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_DSA,MREG_W,MREG_C,MREG_DSV,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DN")]
        public ActionResult SelectDevicePlan(int? type, string accid)
        {
			DropFourObj dropObj = new DropFourObj();
			Session[SessionKey.DedicatedSuppFlow.ToString()] = false;
            Session[SessionKey.Selected_Accessory.ToString()] = null;
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session["OrderType"] = (int)MobileRegType.NewLine;

			Session[SessionKey.MarketCode.ToString()] = null;
			Session[SessionKey.MSISDN.ToString()] = string.Empty;

            if (!string.IsNullOrEmpty(accid))
            {
                string[] AcntArray = accid.Split(',');
                //Session[SessionKey.KenanACNumber.ToString()] = AcntArray[2];

                Session[SessionKey.FxAccNo.ToString()] = AcntArray[0];
                Session[SessionKey.ExternalID.ToString()] = AcntArray[1];
                Session[SessionKey.AccExternalID.ToString()] = AcntArray[2];
                Session[SessionKey.KenanACNumber.ToString()] = AcntArray[3];
                Session[SessionKey.IsMISM.ToString()] = AcntArray[4];
                Session[SessionKey.AccountCategory.ToString()] = AcntArray[5];
                Session[SessionKey.MarketCode.ToString()] = AcntArray[6];
            }

            //var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW && dropObj.mainFlow.RegType == -1)
            {
                dropObj.mainFlow.RegType = (int)MobileRegType.DevicePlan;
            }
            else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
            {
                if (dropObj.suppFlow[dropObj.suppIndex - 1].RegType == -1)
                {
                    dropObj.suppFlow[dropObj.suppIndex - 1].RegType = (int)MobileRegType.DevicePlan;
                }
            }
            //else if (dropObj.currentFlow == DropFourConstant.MSISM_FLOW)
            //{
            //    if (dropObj.msimFlow[dropObj.msimIndex - 1].RegType == -1)
            //    {
            //        dropObj.msimFlow[dropObj.msimIndex - 1].RegType = (int)MobileRegType.DevicePlan;
            //    }
            //}
            Session[SessionKey.DropFourObj.ToString()] = dropObj;

            Session["_componentViewModel"] = null;
            Session[SessionKey.FromPromoOffer.ToString()] = null;
            
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
            Session[SessionKey.SelectedPlanID_Seco.ToString()] = null;
            Session["OfferId_Seco"] = null;
            Session["Discount_Seco"] = null;
            Session["RegMobileReg_MainDevicePrice_Seco"] = null;
            Session["RegMobileReg_OrderSummary_Seco"] = null;
            Session["RegMobileReg_OfferDevicePrice_Seco"] = null;
            Session["RegMobileReg_SelectedModelImageID_Seco"] = null;
            Session["Condition"] = null;
            WebHelper.Instance.ClearSessions();
            WebHelper.Instance.ResetDepositSessions();
            ResetDepositSessionsSeco();
            ResetAllSession(type);
            Session["ExtratenMobilenumbers"] = null;
            Session["AccountHolder"] = null;
            SetRegStepNo(MobileRegistrationSteps.Device);
            Session["RebatePenalty"] = null;
            Session[SessionKey.DeviceLowestContractPrice.ToString2()] = null;
			Session["selectedContractLength"] = null;

            bool bExceptionRaised = false;
            Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;
            var devicePlanVM = new DevicePlanPackageVM();

            var test = Session["RegMobileReg_Type"].ToString2();

            devicePlanVM.DeviceOptionVM = GetAvailableDeviceOptions();
            // get hero device for the first view
            devicePlanVM.PhoneVM = WebHelper.Instance.GetHeroDevice(this.GetType(), out bExceptionRaised);
            List<PackageVM> packageList = new List<PackageVM>();

            // get plan based on modelID
            if (!ReferenceEquals(devicePlanVM.PhoneVM, null))
            {
                foreach (var device in devicePlanVM.PhoneVM.ModelImages)
                {
                    packageList.Add(WebHelper.Instance.GetDevicePackage(device.BrandArticle.ModelID, articleID: device.BrandArticle.ArticleID, isDealer: Util.SessionAccess.User.isDealer));
                    using (var catalogProxy = new CatalogServiceClient())
                    {
                        devicePlanVM.deviceDetails.Add(catalogProxy.GetDeviceImageDetails(device.BrandArticle.ArticleID, device.BrandArticle.ModelID));
                    }
                }
            }
            devicePlanVM.PackageVMList = packageList.GroupBy(a => a.modelID).Select(grp => grp.First()).ToList();

            #region only show for available package
            List<PackageVM> tempPackageVMList = new List<PackageVM>();
            List<int> availableModelID = new List<int>();
            PhoneVM tempPhoneVM = new PhoneVM();
            List<AvailableModelImage> tempModelImages = new List<AvailableModelImage>();

            foreach (var availablepackage in devicePlanVM.PackageVMList)
            {
                if (availablepackage.AvailablePackages.Count > 0)
                {
                    tempPackageVMList.Add(availablepackage);
                    availableModelID.Add(availablepackage.modelID);
                }
            }
            devicePlanVM.PackageVMList = tempPackageVMList;

            foreach (var phone in devicePlanVM.PhoneVM.ModelImages)
            {
                if (availableModelID.Contains(phone.BrandArticle.ModelID))
                {
                    tempModelImages.Add(phone);
                }
            }

            devicePlanVM.PhoneVM.ModelImages = tempModelImages;
            #endregion

            // get lowest price
            Session["DeviceLowestContractPrice"] = WebHelper.Instance.GetLowestHighestPlanAndPrice(devicePlanVM.PhoneVM.ModelImages, useArticleID: false);

            if (type == 2)
            {
                Session["MobileRegType"] = "Planonly";
                if (WebHelper.Instance.IsMNPWithMultpleLines)
                {
                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).MobileRegType = "Planonly";
                }
            }
            else
            {
                Session["MobileRegType"] = "Other";
                if (WebHelper.Instance.IsMNPWithMultpleLines)
                {
                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).MobileRegType = "Other";
                }
            }
            ViewBag.BackButton = GetDevicePlanBackButton();
			//Session["newLine_devicePlanVM"] = devicePlanVM;

            return View(devicePlanVM);
        }


        [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C")]
        private void ResetDepositSessionsSeco()
        {
            Session["RegMobileReg_MalyDevAdv_Seco"] = null;
            Session["RegMobileReg_MalayPlanAdv_Seco"] = null;
            Session["RegMobileReg_MalayDevDeposit_Seco"] = null;
            Session["RegMobileReg_MalyPlanDeposit_Seco"] = null;
            Session["RegMobileReg_MalAdvDeposit_Seco"] = null;
            Session["RegMobileReg_OthDevAdv_Seco"] = null;
            Session["RegMobileReg_OthPlanAdv_Seco"] = null;
            Session["RegMobileReg_OthDevDeposit_Seco"] = null;
            Session["RegMobileReg_OthPlanDeposit_Seco"] = null;
            Session["RegMobileReg_OtherAdvDeposit_Seco"] = null;

        }

        private string GetDevicePlanBackButton()
        {
            string backURL = Url.Content("~/Home/IndexNew");
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            if (dropObj.suppIndex == 0)
            {
                return backURL;
            }
            else
            {
                //will back to main flow last screen
            }

            return backURL;
        }

        private int GetPersonalDetailsBackButton()
        {
            int tabNumber = 0;

            if (!ReferenceEquals(Session["FromMNP"], null) && Convert.ToBoolean(Session["FromMNP"]))
            {
                bool IsMNPWithMultpleLines = ((!ReferenceEquals(Session["FromMNP"], null) && Convert.ToBoolean(Session["FromMNP"])) && (!ReferenceEquals(Session["MNPSupplementaryLines"], null) && (!ReferenceEquals(((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"]).SupplimentaryMSISDNs, null) && ((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"]).SupplimentaryMSISDNs.Count > 0)));
                if (IsMNPWithMultpleLines)
                {
                    tabNumber = 11;
                }
                else
                {
                    tabNumber = 3;
                }
            }
            else
            {
                var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                if (dropObj.suppIndex == 0)
                {
                    tabNumber = Session[SessionKey.SimType.ToString2()] == "MISM" ? (int)RegistrationSteps.MobileNo : (int)MobileRegistrationSteps.MobileNo;
                }
                else
                {
                    //will back to main flow last screen
                }

            }
            return tabNumber;
        }

        private DeviceOptionVM GetAvailableDeviceOptions()
        {
            var deviceOptVM = new DeviceOptionVM();

            using (var proxy = new CatalogServiceProxy())
            {
                var brandIDs = proxy.BrandFind(new BrandFind()
                {
                    Brand = new Brand() { },
                    Active = true
                });

                deviceOptVM.AvailableBrands = proxy.BrandGet(brandIDs).ToList();
            }

            if (!string.IsNullOrEmpty(Session["RegMobileReg_SelectedOptionID"].ToString2()))
            {
                deviceOptVM.SelectedDeviceOption = "btnDvcOpt_" + Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToString2() + "_" + Session["RegMobileReg_SelectedOptionID"].ToString2();
            }
            if (!string.IsNullOrEmpty(Session["RegMobileReg_SelectedOptionID_Seco"].ToString2()))
            {
                deviceOptVM.SelectedDeviceOptionSeco = "btnDvcOpt_" + Session["RegMobileReg_SelectedOptionType_Seco"].ToString2() + "_" + Session["RegMobileReg_SelectedOptionID_Seco"].ToString2();
            }
            deviceOptVM.Type = Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToString2();
            if (!string.IsNullOrEmpty(Session["RegMobileReg_SelectedOptionID_Seco"].ToString2()))
            {
                deviceOptVM.Type = Session["RegMobileReg_SelectedOptionType_Seco"].ToString2();
            }
            return deviceOptVM;
        }


        [HttpPost]
        public ActionResult SelectDevicePlan(FormCollection collection, DevicePlanPackageVM devicePlanPackageVM)
        {
            bool bExceptionRaised = false;
            var devicePlanVM = new DevicePlanPackageVM();
            Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            retrieveAcctListByICResponse allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
            var checkCustEligibilityAF = WebHelper.Instance.checkCustEligibilityAF(allCustomerDetails, null);

            #region filter
            if (collection["submitType"].ToString2() == "filter")
			{
				//devicePlanVM = (DevicePlanPackageVM)Session["newLine_devicePlanVM"];

                List<Online.Registration.Web.CatalogSvc.GetDeviceImageDetailsResp> deviceDetails = new List<GetDeviceImageDetailsResp>();
                int brandFilter = collection["brandFilter"].ToInt();
                string modelFilter = collection["modelFilter"].ToString2();
                int capacityFilter = collection["capacityFilter"].ToInt();
                string priceFilter = collection["priceFilter"].ToString2();
                int deviceType = collection["deviceTypeSelected"].ToInt();
                // stock filter
                bool availableChecked = collection["availableChecked"].ToBool();
                bool limitedStockChecked = collection["limitedStockChecked"].ToBool();
                bool sellingFastChecked = collection["sellingFastChecked"].ToBool();
                bool outOfStockChecked = collection["outOfStockChecked"].ToBool();

				List<SelectListItem> listOfAvailableBrand = Util.GetList(RefType.Brand, defaultValue: string.Empty, defaultText: "All Brand");
				devicePlanVM.BrandSelected = listOfAvailableBrand.Where(x => x.Value == brandFilter.ToString()).Select(y => y.Text).FirstOrDefault();

				//inventoryDashboardVM.ModelSelected = models.Where(x=> x.ID == modelFilter.ToInt()).FirstOrDefault().Name;
				List<SelectListItem> listOfModel = Util.GetList(RefType.Model, defaultValue: string.Empty, defaultText: "All Model");
				devicePlanVM.ModelSelected = listOfModel.Where(x => x.Value == modelFilter).FirstOrDefault().Text;
				devicePlanVM.ModelSelected = System.Text.RegularExpressions.Regex.Replace(devicePlanVM.ModelSelected, @"\S*GB", string.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				List<SelectListItem> listOfCapacity = Util.GetList(RefType.Capacity, defaultValue: string.Empty, defaultText: "All");
				devicePlanVM.CapacitySelected = listOfCapacity.Where(x => x.Value == capacityFilter.ToString()).Select(y => y.Text).FirstOrDefault();

				devicePlanVM.PriceRangeValue = priceFilter;
				devicePlanVM.inputBrandSelected = brandFilter;
				devicePlanVM.inputCapacitySelected = capacityFilter;
				devicePlanVM.inputModelSelected = modelFilter;
				devicePlanVM.inputDeviceTypeSelected = deviceType;


                int priceFrom = 0;
                int priceTo = 0;
                if (priceFilter.Contains("#"))
                {
                    string[] priceArray = priceFilter.Trim().Split('#');
                    priceFrom = int.Parse(priceArray[0]);
                    priceTo = int.Parse(priceArray[1]);

					string firstword = string.Empty; string secondWord = string.Empty;
					firstword = priceArray[0] == "0" ? "0" : "RM " + priceArray[0];
					secondWord = priceArray[1] == "0" ? "0" : "RM " + priceArray[1];

					devicePlanVM.PriceRangeSelected = string.Format("{0} - {1}", firstword, secondWord);
                }

                devicePlanVM.PhoneVM = WebHelper.Instance.GetFilteredDevice(this.GetType(), out bExceptionRaised, brandFilter, modelFilter, capacityFilter, deviceType, priceFrom, priceTo, availableChecked, limitedStockChecked, sellingFastChecked, outOfStockChecked, 0);

				#region need to increase performance here
				List<PackageVM> packageListFilterDevice = new List<PackageVM>();

				if (!ReferenceEquals(devicePlanVM.PhoneVM, null))
				{
					foreach (var device in devicePlanVM.PhoneVM.ModelImages)
					{
						packageListFilterDevice.Add(WebHelper.Instance.GetDevicePackage(device.BrandArticle.ModelID, device.BrandArticle.ArticleID, isDealer: Util.SessionAccess.User.isDealer));
						using (var catalogProxy = new CatalogServiceClient())
						{

							devicePlanVM.deviceDetails.Add(catalogProxy.GetDeviceImageDetails(device.BrandArticle.ArticleID, device.BrandArticle.ModelID));
						}
					}
				}
				devicePlanVM.PackageVMList = packageListFilterDevice.GroupBy(a => a.modelID).Select(grp => grp.First()).ToList();

				#region only show for available package
				List<PackageVM> tempPackageVMList = new List<PackageVM>();
				List<int> availableModelID = new List<int>();
				PhoneVM tempPhoneVM = new PhoneVM();
				List<AvailableModelImage> tempModelImages = new List<AvailableModelImage>();

				foreach (var availablepackage in devicePlanVM.PackageVMList)
				{
					if (availablepackage.AvailablePackages.Count > 0)
					{
						tempPackageVMList.Add(availablepackage);
						availableModelID.Add(availablepackage.modelID);
					}
				}
				devicePlanVM.PackageVMList = tempPackageVMList;

				foreach (var phone in devicePlanVM.PhoneVM.ModelImages)
				{
					if (availableModelID.Contains(phone.BrandArticle.ModelID))
					{
						tempModelImages.Add(phone);
					}
				}

				devicePlanVM.PhoneVM.ModelImages = tempModelImages;
				#endregion
				#endregion

                List<String> LabelReplace = new List<String>();
                ViewBag.GSTMark = Settings.Default.GSTMark;
                LabelReplace.Add("GSTMark");
                ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);

                //Session["DeviceLowestContractPrice"] = WebHelper.Instance.GetLowestHighestPlanAndPrice(devicePlanVM.PhoneVM.ModelImages, useArticleID: false);
                devicePlanVM.PhoneVM.SelectedModelImageID = "0";
                devicePlanVM.PhoneVM.SelectedModelImageIDSeco = "0";
                devicePlanVM.PackageVM.SelectedPgmBdlPkgCompID = 0;
                devicePlanVM.PackageVM.SelectedSecondaryPgmBdlPkgCompID = 0;


                return View(devicePlanVM);
            }
            #endregion

            #region submit
            if (collection["submitType"].ToString2() == "")
            {
                object SuplementRequired = Session["IsSupplementaryRequired"];
                Session["ArticleId"] = collection["selectedArticleID"].ToString().Trim();
                Session["RegMobileReg_OfferDevicePrice"] = collection["deviceOfferPrice"].ToDecimal();
                Session["selectedKenanCode"] = collection["selectedKenanCode"];
                Session["selectedPackageCode"] = collection["selectedPackageCode"];
                string selectedContractLength = collection["selectedContractLength"];
				Session["selectedContractLength"] = selectedContractLength;

                bool PrimarySupplementaryRequired = (SuplementRequired != null) ? Convert.ToBoolean(SuplementRequired) : false;

				devicePlanPackageVM.PhoneVM.DeviceArticleId = collection["selectedArticleID"].ToString().Trim();

				var allArticles = MasterDataCache.Instance.Article;
				devicePlanPackageVM.PhoneVM.SelectedModelImageID = allArticles.Where(x => x.ArticleID.Equals(devicePlanPackageVM.PhoneVM.DeviceArticleId)).SingleOrDefault().ID.ToString2();
				devicePlanPackageVM.PhoneVM.ColourID = allArticles.Where(x => x.ArticleID.Equals(devicePlanPackageVM.PhoneVM.DeviceArticleId)).SingleOrDefault().ColourID;

				Session["RegMobileReg_SelectedModelImageID"] = devicePlanPackageVM.PhoneVM.SelectedModelImageID;

                var packageVM = devicePlanPackageVM.PackageVM;
                packageVM.TabNumber = MobileRegistrationSteps.Vas.ToInt();

                if (Session["PPID"].ToString2() != "")
                {
                    if (!ReferenceEquals(Session["RedirectURL"], null))
                    {
                        Session.Contents.Remove("RedirectURL");
                    }
                    if (WebHelper.Instance.IsMNPWithMultpleLines)
                    {
                        ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).SimType = packageVM.SimType;
                    }
                    Session["RegMobileReg_TabIndex"] = packageVM.TabNumber;
                    try
                    {
                        if (packageVM.TabNumber == (int)MobileRegistrationSteps.Vas || ((Convert.ToString(Session["SimType"]) == "MISM") && packageVM.TabNumber == (int)RegistrationSteps.Vas))
                        {
                            if (Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt() != packageVM.SelectedPgmBdlPkgCompID)
                            {
                                Session["RegMobileReg_PkgPgmBdlPkgCompID"] = packageVM.SelectedPgmBdlPkgCompID;
                                Session["RegMobileReg_PkgPgmBdlPkgCompID_Prin"] = packageVM.SelectedPgmBdlPkgCompID;
                                Session["SelectedPlanID"] = packageVM.SelectedPgmBdlPkgCompID;
                                if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                                {
                                    if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                    {
                                        ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).RegMobileReg_PkgPgmBdlPkgCompID = packageVM.SelectedPgmBdlPkgCompID;
                                        ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).SelectedPlanID = packageVM.SelectedPgmBdlPkgCompID;
                                    }
                                }

                                // Retrieve Plan Min Age
                                using (var proxy = new CatalogServiceProxy())
                                {
                                    var pbpc = MasterDataCache.Instance.FilterComponents(new int[] { packageVM.SelectedPgmBdlPkgCompID }).SingleOrDefault();
                                    Session["UOMPlanID"] = pbpc.KenanCode;
                                    if (WebHelper.Instance.IsMNPWithMultpleLines)
                                    {
                                        ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).UOMPlanID = pbpc.KenanCode;
                                    }
                                    var bundleID = pbpc.ParentID;
                                    var programID = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
                                    {
                                        PgmBdlPckComponent = new PgmBdlPckComponent()
                                        {
                                            ChildID = bundleID,
                                            LinkType = Properties.Settings.Default.Program_Bundle
                                        },
                                        Active = true
                                    }).SingleOrDefault().ParentID;
                                    var minAge = proxy.ProgramGet(new int[] { programID }).SingleOrDefault().MinAge;
                                    Session["RegMobileReg_ProgramMinAge"] = minAge;
                                    if (WebHelper.Instance.IsMNPWithMultpleLines)
                                    {
                                        ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).RegMobileReg_ProgramMinAge = minAge;
                                    }
                                }
                                Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
                                Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
                                Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
                                Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
                                Session[SessionKey.OfferId.ToString()] = null;
                                Session[SessionKey.selectedOfferID.ToString()] = null;
                                Session[SessionKey.RegMobileReg_UOMCode.ToString()] = null;

                                ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
                                string processingMsisdn = string.Empty;
                                Lstvam = WebHelper.Instance.GetAvailablePackageComponents(Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(), ref processingMsisdn, isMandatory: false);
                                string offernames = string.Empty;
                                string planid = Session["UOMPlanID"].ToString();
                                string articleid = Session[SessionKey.ArticleId.ToString()].ToString();
                                string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
								
								// drop 5
								Lstvam.ContractOffers = WebHelper.Instance.GetContractOffers(Lstvam, articleid, planid, MOCStatus);

                                foreach (var contract in Lstvam.ContractOffers)
                                {
                                    if (contract.OfferName.Contains(selectedContractLength))
                                    {
                                        //24022015 - Anthony - fix for inconsistent price in order summary - Start
                                        var PBPCPlan = 0;
                                        try
                                        {
                                            PBPCPlan = MasterDataCache.Instance.FilterComponents(new int[] { devicePlanPackageVM.PackageVM.SelectedPgmBdlPkgCompID.ToInt() }).FirstOrDefault().ChildID;
                                        }
                                        catch (Exception e)
                                        {
                                            PBPCPlan = 0;
                                        }

                                        if (PBPCPlan != 0)
                                        {
                                            var findCriteria = new PgmBdlPckComponentFind()
                                            {
                                                PgmBdlPckComponent = new PgmBdlPckComponent()
                                                {
                                                    ParentID = PBPCPlan,
                                                    LinkType = Properties.Settings.Default.Package_Component
                                                },
                                                Active = true
                                            };

                                            if (!string.IsNullOrEmpty(contract.KenanComponent1Id) && !string.IsNullOrEmpty(selectedContractLength))
                                            {
                                                var ContractID = MasterDataCache.Instance.FilterComponents(findCriteria).Where(x => x.KenanCode.Equals(contract.KenanComponent1Id) && x.Value == selectedContractLength.ToInt()).FirstOrDefault().ID;
                                                Session["RegMobileReg_ContractID"] = ContractID;
                                            }
                                        }
                                        //24022015 - Anthony - fix for inconsistent price in order summary - End

                                        Session["selectedKenanComponent1Id"] = contract.KenanComponent1Id;
                                        Session["selectedOfferID"] = contract.Id;
                                        Session["OfferID"] = contract.Id;//14012015 - Anthony - GST Trade Up
                                        Session[SessionKey.TradeUpAmount.ToString()] = null;//14012015 - Anthony - GST Trade Up
                                        break;
                                    }
                                }

                                foreach (var contractVAS in Lstvam.ContractVASes)
                                {
                                    if (Session["selectedKenanComponent1Id"].ToString2() == contractVAS.KenanCode)
                                    {
                                        Session["RegMobileReg_ContractID"] = contractVAS.PgmBdlPkgCompID;
                                        break;
                                    }
                                }
                               

                            }
                        }
                        using (var proxy = new UserServiceProxy())
                        {
                            //TODO Session[SessionKey.KenanACNumber.ToString()].ToString()
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Session["RegMobileReg_Type"].ToInt(), "Select Plan", "", "");
                        }
                    }
                    catch (Exception ex)
                    {
                        WebHelper.Instance.LogExceptions(this.GetType(), ex);
                        throw ex;
                    }

                    Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();

                    
                    DropFourHelpers.UpdateDevice(dropObj, devicePlanPackageVM.DeviceOptionVM);
                    DropFourHelpers.UpdatePhone(dropObj, devicePlanPackageVM.PhoneVM);
                    DropFourHelpers.UpdatePackage(dropObj, devicePlanPackageVM.PackageVM);
                    Session[SessionKey.DropFourObj.ToString()] = dropObj;

                    //Anthony: New IC (search id type: 1) and Other IC (search id type: 3) will be tagged as Malaysian
                    if (Session["SearchBy"].ToString2().ToUpper() == "NRIC" || Session["SearchBy"].ToString2().ToUpper() == "NEWIC" || Session["SearchBy"].ToString2().ToUpper() == "OTHERIC")
                    {
                        if (!ReferenceEquals(dropObj.personalInformation, null))
                            dropObj.personalInformation.Customer.NationalityID = 1;
                    }
                    else if (Session[SessionKey.IDCardTypeID.ToString()].ToInt() != 2)
                    {//other than passport, the nationality id = 1
                        if (!ReferenceEquals(dropObj.personalInformation, null))
                            dropObj.personalInformation.Customer.NationalityID = 1;
                    }
                }
            }
            #endregion submit
			// this is for reset vas grouping
			Session["VasGroupIds"] = null;

            //if (Session["IsDealer"].ToBool())
            //{
            //    return RedirectToAction(WebHelper.Instance.GetRegActionStepNew((int)MobileRegistrationSteps.Vas));
            //}
            //else
            //{
                return RedirectToAction(WebHelper.Instance.GetRegActionStepNew((int)MobileRegistrationSteps.Accessory));
            //}
        }

        //[Authorize(Roles = "MREG_W,MREG_C,DREG_N,DREG_C,MREG_DAI,MREG_DAC")]
        [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_DSA,MREG_W,MREG_C,MREG_DSV,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DN")]
        public ActionResult SelectAccessory(int? type)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var accessoriesVM = new AccessoriesVM();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            Session["af_showOffer"] = null;
            var isDealer = Session["IsDealer"].ToBool();
            Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();
            ViewBag.imageURLForAccessory = WebHelper.Instance.GetImageURLFromModelID(dropObj);

            var afEligibilityCheck = Session[SessionKey.AF_Eligibility.ToString()].ToBool();
            var dfException = Session["af_showOffer"].ToString2() == "show" ? true : false;

            var allServiceDetails = (retrieveAcctListByICResponse)Session["AllServiceDetails"];
            //var hasAccessoryFinancingContract = false;
            //var contractDetails = WebHelper.Instance.retrieveContractDetails(allServiceDetails, out hasAccessoryFinancingContract);

            if (isDealer && Util.isCDPUUser() == false && afEligibilityCheck == false)
            {
                ViewBag.ErrorMessage = "Customer is not eligible for Accessories Financing. ";
                ViewBag.HardStopMessage = "Please click \"Next\" to continue to the next page";
            }
			//else if (hasAccessoryFinancingContract)
			//{
			//    ViewBag.ErrorMessage = "Customer has an existing active Accessories Financing contract for this MSISDN. Accessories Financing offer will not be displayed. ";
			//    ViewBag.HardStopMessage = "Please click \"Next\" to continue to the next page";
			//}
            
            return View(accessoriesVM);
        }

        [HttpPost]
        public ActionResult SelectAccessory(FormCollection collection, AccessoriesVM accessoriesVM)
        {
            var tabNumber = collection["TabNumber"].ToInt();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();

            if (!string.IsNullOrEmpty(collection["TabNumber"].ToString2()) && tabNumber == (int)MobileRegistrationSteps.Device)
            {
                return RedirectToAction(WebHelper.Instance.GetRegActionStepNew((int)MobileRegistrationSteps.Device));
            }

            if (collection["af_showOffer"].ToString2() == "show")
            {
                Session["af_showOffer"] = collection["af_showOffer"].ToString2();
                Session[SessionKey.AF_Eligibility.ToString()] = true;
                return RedirectToAction("SelectAccessory");
            }
            
            var selectedAccessory = Session[SessionKey.Selected_Accessory.ToString()] as List<AccessoryVM> ?? new List<AccessoryVM>();
            DropFourHelpers.UpdateSelectedAccessory(dropObj, selectedAccessory);

            Session[SessionKey.DropFourObj.ToString()] = dropObj;

            return RedirectToAction(WebHelper.Instance.GetRegActionStepNew((int)MobileRegistrationSteps.Vas));
        }

        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        public ActionResult SelectMobileNo(string desireNo = "", string numberType = "n", string prefix = "")
        {
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();
            SetRegStepNo(MobileRegistrationSteps.MobileNo);

            var mobileNoVM = new MobileNoVM();
            var mobileNos = new MobileNoSelectionResponse();
            List<string> objMobileNos = new List<string>();
            if (numberType == "n")
            {
                // **************** Dealer Number ******************
                if (prefix == "-1")
                {
                    prefix = string.Empty;
                }
                using (var proxy = new KenanServiceProxy())
                {
                    mobileNos = proxy.MobileNoSelection(new MobileNoSelectionRequest()
                    {
                        DesireNo = desireNo,
                        MobileNoPrefix = prefix
                    });
                }
                // **************************************************

                objMobileNos = mobileNos.MobileNos.ToList();
            }
            else
            {
                var mobileNosResp = WebHelper.Instance.RetriveMobileNumbers(desireNo, numberType);
                if (mobileNosResp.Status == false)
                {
                    ModelState.AddModelError(string.Empty, mobileNosResp.Message);
                    return View(mobileNoVM);
                }

                objMobileNos = mobileNosResp.Data;

            }
            mobileNoVM = new MobileNoVM()
            {
                // AvailableMobileNos = mobileNos.MobileNos.ToList(),
                AvailableMobileNos = objMobileNos != null ? objMobileNos : new List<string>(),
                NoOfDesireMobileNo = Properties.Settings.Default.NoOfDesireMobileNo
            };

            if (((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()]) != null)
            {
                mobileNoVM.SelectedMobileNumber = ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()]);
            }

            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
            if (sublines != null)
                mobileNoVM.NoOfSubline = sublines.Count();

            return View(mobileNoVM);
        }

        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
		public ActionResult SelectMobileNoNew(string desireNo = "", string numberType = "n", string prefix = "", bool validateAgentCode = false, string salesAgentCode = "")
        {
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();
            SetRegStepNo(MobileRegistrationSteps.MobileNo);
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session["SimType"] = null;
            var mobileNoVM = new MobileNoVM();
            var mobileNos = new MobileNoSelectionResponse();
            List<string> objMobileNos = new List<string>();
            if (numberType == "n")
            {
                // **************** Dealer Number ******************
                if (prefix == "-1")
                {
                    prefix = string.Empty;
                }
                using (var proxy = new KenanServiceProxy())
                {
                    mobileNos = proxy.MobileNoSelection(new MobileNoSelectionRequest()
                    {
                        DesireNo = desireNo,
                        MobileNoPrefix = prefix
                    });
                }
                // **************************************************

                objMobileNos = mobileNos.MobileNos.ToList();
            }
            else
            {
				var mobileNosResp = WebHelper.Instance.RetriveMobileNumbers(desireNo, numberType, validateAgentCode, salesAgentCode);
                if (mobileNosResp.Status == false)
                {
                    if (Equals(mobileNosResp.Message, Constants.InfentoryNotExist))
                    {
                        mobileNosResp.Message = Constants.ReservedNotFound;
                    }
                    ViewBag.errReserve = mobileNosResp.Message;
                    ModelState.AddModelError(string.Empty, mobileNosResp.Message);
                    return View(mobileNoVM);
                }

				// since Drop 4, got multi suppline, in mobile number selection should remove the selected number before.
                if (ReferenceEquals(mobileNos.MobileNos, null))
                {
                    mobileNos.MobileNos = new string[] {desireNo};
                }
				var tempMobileNos = mobileNos.MobileNos.ToList();
				var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
				if (!ReferenceEquals(dropObj, null))
				{
					if (!ReferenceEquals(dropObj.selectedMobileNoinFlow, null) && dropObj.selectedMobileNoinFlow.Count() > 0)
					{
						var newList = tempMobileNos.Except(dropObj.selectedMobileNoinFlow).ToList();
						tempMobileNos.Clear();
						tempMobileNos.AddRange(newList);
					}
				}

				objMobileNos = tempMobileNos;

            }
            mobileNoVM = new MobileNoVM()
            {
                // AvailableMobileNos = mobileNos.MobileNos.ToList(),
                AvailableMobileNos = objMobileNos != null ? objMobileNos : new List<string>(),
                NoOfDesireMobileNo = Properties.Settings.Default.NoOfDesireMobileNo
            };

            if (((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()]) != null)
            {
                mobileNoVM.SelectedMobileNumber = ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()]);
            }

            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
            if (sublines != null)
                mobileNoVM.NoOfSubline = sublines.Count();

            return View(mobileNoVM);
        }



        [HttpPost]
        public ActionResult AddNewLine(FormCollection collection, string SelectAccountDtls = null)
        {
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            if (collection["submit"].ToString() == "next")
            {
                if (SelectAccountDtls != null)
                {
                    string[] strAccountDtls = SelectAccountDtls.Split(',');
                    Session[SessionKey.FxAccNo.ToString()] = strAccountDtls[0].ToString();
                    Session[SessionKey.ExternalID.ToString()] = strAccountDtls[1].ToString();
                    Session[SessionKey.AccExternalID.ToString()] = strAccountDtls[2].ToString();
                    Session[SessionKey.KenanACNumber.ToString()] = strAccountDtls[3].ToString();
                    Session[SessionKey.IsMISM.ToString()] = strAccountDtls[4].ToString();

                    Session[SessionKey.AccountCategory.ToString()] = strAccountDtls[5];
                    Session[SessionKey.MarketCode.ToString()] = strAccountDtls[6];
                    Session["SMEIPARENTID"] = strAccountDtls[7];

                    using (var proxy = new UserServiceProxy())
                    {
                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Select Account", Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                    }

                    return RedirectToAction("SelectPlanNew");
                }
            }
            if (string.IsNullOrWhiteSpace(SelectAccountDtls))
            {
                TempData["Error"] = "Select an account";
                return RedirectToAction("AddNewLine");
            }
            ///BELOW WILL CONTAIN THE SELECTED ACCOUNTNUMBER
            ///PLEASE REDIRECT TO APPROPRIATE PAGE FROM THIS POINT ONWARDS. DO NOT FORGET TO EXPIRE THE SESSION WHEN YOU ARE DONE WITH IT.
            Session[SessionKey.SelectedAccountNumber.ToString()] = SelectAccountDtls;


            ///REPLACE THE BELOW REDIRECTION TO YOUR PAGE
            return RedirectToAction("AddNewLine");

        }

        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        [ValidateInput(false)]
        public ActionResult SelectPlanNew(int type=2, string accid="")
        {
            //var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            Session["OrderType"] = (int)MobileRegType.NewLine;
			DropFourObj dropObj = new DropFourObj();
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
			Session[SessionKey.DedicatedSuppFlow.ToString()] = false;

            dropObj.mainFlow.RegType = (int)MobileRegType.PlanOnly;
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            
            Session[SessionKey.SelectedComponents.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;

            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] = null;
            Session[SessionKey.RegMobileReg_MobileNo.ToString()] = null;
            Session[SessionKey.SelectedPlanID_Seco.ToString()] = null;
            Session[SessionKey.SimType.ToString()] = null;
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = null;
            Session["PrimarySim"] = null;
			Session[SessionKey.MSISDN.ToString()] = string.Empty;

            if (!string.IsNullOrEmpty(accid))
            {
                string[] strAccountDtls = accid.Split(',');
                Session[SessionKey.FxAccNo.ToString()] = strAccountDtls[0].ToString();
                Session[SessionKey.ExternalID.ToString()] = strAccountDtls[1].ToString();
                Session[SessionKey.AccExternalID.ToString()] = strAccountDtls[2].ToString();
                Session[SessionKey.KenanACNumber.ToString()] = strAccountDtls[3].ToString();
                Session[SessionKey.IsMISM.ToString()] = strAccountDtls[4].ToString();

                Session[SessionKey.AccountCategory.ToString()] = strAccountDtls[5];
                Session[SessionKey.MarketCode.ToString()] = strAccountDtls[6];
                Session["SMEIPARENTID"] = strAccountDtls[7];

                Session[SessionKey.SelectedAccountNumber.ToString()] = accid;
            }

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
            {
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            }
            if (type > 0)
            {
                ResetAllSession(type);
            }

            //Added for hiding the contrats & vas in planonly
            if (type == 2)
            {
                Session[SessionKey.MobileRegType.ToString()] = "Planonly";
            }
            else
            {
                Session[SessionKey.MobileRegType.ToString()] = "Other";
            }


            SetRegStepNo(MobileRegistrationSteps.Plan);

            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary(fromSubline: true);
            var packageVM = GetAvailablePackages();

            if (!ReferenceEquals(Session[SessionKey.MISMCount.ToString()], null))
            {
                packageVM.MISMCount = Convert.ToInt32(Session[SessionKey.MISMCount.ToString()].ToString());
            }
            PersonalDetailsVM objCurDetails = new PersonalDetailsVM();

            packageVM.PersonalDetailsVM = objCurDetails;

            //Dealer specific plans            
            var dealerPkgs = Util.GetDealerPlans(packageVM.AvailablePackages);
            if (dealerPkgs.Count > 0)
            {
                packageVM.AvailablePackages = dealerPkgs;
            }
            // EOF Dealer specific plans
            return View(packageVM);
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA")]
        public ActionResult SelectPlanNew(PackageVM packageVM)
        {


            Session[SessionKey.RegMobileReg_Type.ToString()] = "2";
            if (Session[SessionKey.PPID.ToString()].ToString() != string.Empty)
            {
                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null))
                {
                    Session.Contents.Remove("RedirectURL");
                }
                Session[SessionKey.SimType.ToString()] = packageVM.SimType ?? "Normal";
                Session[SessionKey.RegMobileReg_TabIndex.ToString()] = packageVM.TabNumber;
                try
                {
                    if (packageVM.TabNumber == (int)MobileRegistrationSteps.Vas)
                    {
                        // selected different Plan
                        if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt() != packageVM.SelectedPgmBdlPkgCompID)
                        {
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = packageVM.SelectedPgmBdlPkgCompID;
                            Session[SessionKey.SelectedPlanID.ToString()] = packageVM.SelectedPgmBdlPkgCompID;
                            // Retrieve Plan Min Age
                            using (var proxy = new CatalogServiceProxy())
                            {
                                var pbpc = MasterDataCache.Instance.FilterComponents(new int[] { packageVM.SelectedPgmBdlPkgCompID }).SingleOrDefault();

                                var bundleID = pbpc.ParentID;
                                var programID = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
                                {
                                    PgmBdlPckComponent = new PgmBdlPckComponent()
                                    {
                                        ChildID = bundleID,
                                        LinkType = Properties.Settings.Default.Program_Bundle
                                    },
                                    Active = true
                                }).SingleOrDefault().ParentID;

                                var minAge = proxy.ProgramGet(new int[] { programID }).SingleOrDefault().MinAge;
                                Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = minAge;
                            }

                            // reset session
                            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
                            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
                            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
                            Session[SessionKey.VasGroupIds.ToString()] = null;
                        }
                    }
                    using (var proxy = new UserServiceProxy())
                    {
                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Select Plan", Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                    }
                }
                catch (Exception ex)
                {
                    LogExceptions(ex);
                    throw ex;
                }

                Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();

                var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                DropFourHelpers.UpdatePackage(dropObj, packageVM);

                //Anthony: New IC (search id type: 1) and Other IC (search id type: 3) will be tagged as Malaysian
                if (Session["SearchBy"].ToString2().ToUpper() == "NRIC" || Session["SearchBy"].ToString2().ToUpper() == "NEWIC" || Session["SearchBy"].ToString2().ToUpper() == "OTHERIC")
                {
                    if (!ReferenceEquals(dropObj.personalInformation, null))
                        dropObj.personalInformation.Customer.NationalityID = 1;
                }
                else if (Session[SessionKey.IDCardTypeID.ToString()].ToInt() != 2)
                {//other than passport, the nationality id = 1
                    if (!ReferenceEquals(dropObj.personalInformation, null))
                        dropObj.personalInformation.Customer.NationalityID = 1;
                }

				Session[SessionKey.DropFourObj.ToString()] = dropObj;

                return RedirectToAction(WebHelper.Instance.GetRegActionStep(packageVM.TabNumber));
            }
            else
            {
                Session[SessionKey.RedirectURL.ToString()] = "GuidedSales/PlanSearch";
                return RedirectToAction("IndexNew", "Home");
            }
        }
        [Authorize(Roles = "MREG_W,MREG_C,DREG_N,DREG_C,MREG_DC,MREG_DSV,MREG_DSA,MREG_DAI,MREG_DAC")]
        public ActionResult SelectSecondaryPlan(int type = 0)
        {
            Session[SessionKey.SelectedComponents_Seco.ToString()] = null;

            Session["MandatoryVasIds"] = null;
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
            {
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            }
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();

            var packageVM = GetAvailablePackages();


            if (Session[SessionKey.MISMCount.ToString()] == null)
                Session[SessionKey.MISMCount.ToString()] = "0";
            packageVM.MISMCount = Convert.ToInt32(Session[SessionKey.MISMCount.ToString()].ToString());

            //Dealer specific plans            
            var dealerPkgs = Util.GetDealerPlans(packageVM.AvailablePackages);
            if (dealerPkgs.Count > 0)
            {
                packageVM.AvailablePackages = dealerPkgs;
            }
            // EOF Dealer specific plans

            PersonalDetailsVM objCurDetails = new PersonalDetailsVM();

            packageVM.PersonalDetailsVM = objCurDetails;

            return View(packageVM);
        }
        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,DREG_N,DREG_C,MREG_DC,MREG_DSV,MREG_DSA,MREG_DAI,MREG_DAC")]
        public ActionResult SelectSecondaryPlan(PackageVM packageVM)
        {
            //VLT ADDED CODE to check for PPID verification
            if (Session[SessionKey.PPID.ToString()].ToString() != "")
            {
                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null))
                {
                    Session.Contents.Remove("RedirectURL");
                }

                Session[SessionKey.RegMobileReg_TabIndex.ToString()] = packageVM.TabNumber;
                try
                {
                    if (packageVM.TabNumberSeco == (int)RegistrationSteps.SecoVas || ((Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM") && packageVM.TabNumberSeco == (int)RegistrationSteps.SecoVas))
                    {
                        //Added for checking extra ten numbers
                        if (packageVM.ExtraTenMobileNumbers.ToString2().Length > 0)
                        {
                            Session[SessionKey.ExtratenMobilenumbers.ToString()] = packageVM.ExtraTenMobileNumbers;
                        }
                        else
                        {
                            Session[SessionKey.ExtratenMobilenumbers.ToString()] = null;
                        }
                        // selected different Plan
                        if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt() != packageVM.SelectedSecondaryPgmBdlPkgCompID)
                        {
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = packageVM.SelectedSecondaryPgmBdlPkgCompID;
                            Session[SessionKey.SelectedPlanID_Seco.ToString()] = packageVM.SelectedSecondaryPgmBdlPkgCompID;
                            // Retrieve Plan Min Age
                            using (var proxy = new CatalogServiceProxy())
                            {
                                var pbpc = MasterDataCache.Instance.FilterComponents(new int[] { packageVM.SelectedSecondaryPgmBdlPkgCompID }).SingleOrDefault();


                                //Chnages made by chetan for SKMM
                                Session[SessionKey.UOMPlanID_Seco.ToString()] = pbpc.KenanCode;

                                var bundleID = pbpc.ParentID;
                                var programID = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
                                {
                                    PgmBdlPckComponent = new PgmBdlPckComponent()
                                    {
                                        ChildID = bundleID,
                                        LinkType = Properties.Settings.Default.Program_Bundle
                                    },
                                    Active = true
                                }).SingleOrDefault().ParentID;

                                var minAge = proxy.ProgramGet(new int[] { programID }).SingleOrDefault().MinAge;
                                Session[SessionKey.RegMobileReg_ProgramMinAge_Seco.ToString()] = minAge;
                            }

                            // reset session
                            Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] = null;
                            Session[SessionKey.RegMobileReg_DataplanID_Seco.ToString()] = null;
                            Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = null;
                            Session[SessionKey.RegMobileReg_VasNames_Seco.ToString()] = null;
                        }
                    }

                }
                catch (Exception ex)
                {
                    LogExceptions(ex);
                    throw ex;
                }
                if (packageVM.TabNumberSeco == 5)
                {
                    return RedirectToAction("SelectSecondaryVAS");
                }
                else if (Session[SessionKey.SimType.ToString()].ToString2() == "MISM" && Session["DeviceType"].ToString2() == "Seco_WithDevice")
                {
                    return RedirectToAction("SelectDeviceCatalogSeco");
                }
                else if (Session[SessionKey.SimType.ToString()].ToString2() == "MISM" && Session["DeviceType"].ToString2() == "Seco_NoDevice")
                {
                    return RedirectToAction("SelectVAS");
                }
                else
                {
                    return RedirectToAction(WebHelper.Instance.GetRegActionStep(packageVM.TabNumber));
                }

            }
            else
            {
                Session[SessionKey.RedirectURL.ToString()] = "GuidedSales/PlanSearch";
                return RedirectToAction("Index", "Home");
            }
        }
        [Authorize(Roles = "MREG_W,MREG_C,DREG_N,DREG_C,MREG_DC,MREG_DSV,MREG_DSA,MREG_DAI,MREG_DAC")]
        public ActionResult SelectSecondaryVAS()
        {
            Session["OfferId_Seco"] = null;
            Session["MandatoryVasIds"] = null;
            Session["MandatoryVasIdsSeco"] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session["RegMobileReg_OfferDevicePrice_Seco"] = null;
            Session[SessionKey.RegMobileReg_VasNames_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = null;
            WebHelper.Instance.ResetDepositSessions();
            List<MOCOfferDetails> offers = new List<MOCOfferDetails>();


            ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
            if (Session["VasGroupIds_Seco"].ToString2() != "")
            {
                Lstvam2.hdnGroupIds = Session["VasGroupIds_Seco"].ToString2();
            }

			// Gerry - Drop5
			offers = WebHelper.Instance.GetMOCOfferByArticleID(Session["ArticleId_Seco"].ToString2());

            if (offers != null)
                Lstvam2.Offers = offers;

            if (Session[SessionKey.PPID.ToString()].ToString() != "")
            {
                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null))
                {
                    Session.Contents.Remove("RedirectURL");
                    Session[SessionKey.MenuType.ToString()] = "1";
                }
                if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] == null || (Convert.ToString(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()]) == String.Empty))
                {
                    Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = Session["GuidedSales_PkgPgmBdlPkgCompID_Seco"];
                }
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();


                Lstvam = GetAvailablePackageComponents(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(), isMandatory: false);

                string MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();

                Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);

                Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);

                var Ivalexists = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).PlanBundle;

                /* For Mandatory packages */
                Session["vasmandatoryids"] = "";
                using (var proxy = new CatalogServiceProxy())
                {

                    BundlepackageResp respon = new BundlepackageResp();

                    respon = proxy.GetMandatoryVas(Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()]));
                    int i = 0;
                    for (i = 0; i < respon.values.Count(); i++)
                    {

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Master_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            //  Session["intmandatoryidsval"] = respon.values[i].BdlDataPkgId;

                            Session["intmandatoryidsvalSec"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
                        }

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.CompType_DataCon)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault,
                                                                 isHidden = e.isHidden
                                                             };


                            Session["intdataidsvalSec"] = respon.values[i].BdlDataPkgId;
                            Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts);
                        }
                        /*Extra packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Extra_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault,
                                                                 isHidden = e.isHidden
                                                             };


                            // Session["intextraidsval"] = respon.values[i].BdlDataPkgId;

                            Session["intextraidsvalSec"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);
                        }
                        //Pramotional Packages
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Pramotional_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            #region COmmented filterComponents for Pramotional components
                            //List<int> Finalintvalues = null;
                            //if (Session["RegMobileReg_Type"].ToInt() == 1)
                            //{
                            //    List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();
                            //    string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();
                            //    Finalintvalues = values.Select(a => a.BdlDataPkgId).Distinct().ToList();
                            //    Lstvam.DeviceType = "Seco_NoDevice";
                            //}
                            //else if (Session["RegMobileReg_Type"].ToInt() == 2)
                            //{
                            //    List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
                            //    Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
                            //}
                            //var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                            //                                 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                            //                                 select new AvailableVAS
                            //                                 {
                            //                                     ComponentName = e.ComponentName,
                            //                                     PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                            //                                     Price = e.Price,
                            //                                     Value = e.Value,
                            //                                     GroupId = e.GroupId,
                            //                                     GroupName = e.GroupName,
                            //                                     KenanCode = e.KenanCode
                            //                                 }; 
                            #endregion
                            Session["intpramotionidsvalSec"] = respon.values[i].BdlDataPkgId;
                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intpramotionidsval = respon.values[i].BdlDataPkgId;
                                }
                            }

                            Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
                        }
                        /*Data Contract Discount packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.DiscountDataContract_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            if (Session["RegMobileReg_Type"].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session["RegMobileReg_Type"].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }
                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault,
                                                                 isHidden = e.isHidden
                                                             };
                            Session["intdatadiscountidsval"] = respon.values[i].BdlDataPkgId;
                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intdatadiscountidsval = respon.values[i].BdlDataPkgId;
                                }
                            }

                            Lstvam2.PramotionalComponents.AddRange(finalfiltereddatacontracts);
                        }

                    }
                }


                if (MobileRegType == "Planonly")
                {
                    Lstvam.ContractVASes.Clear();
                }

                Session["SecondaryLineVas"] = Lstvam2;

                if (Lstvam2 != null)
                {
                    return View(Lstvam2);
                }
                else
                {
                    Lstvam2 = (ValueAddedServicesVM)Session["SecondaryLineVas"];
                    return View(Lstvam2);
                }


            }
            else
            {
                Session[SessionKey.RedirectURL.ToString()] = "Registration/SelectVAS";
                return RedirectToAction("Index", "Home");
            }
        }


        [Authorize(Roles = "MREG_W,MREG_C,DREG_N,DREG_C,MREG_DC,MREG_DSV,MREG_DSA,MREG_DAI,MREG_DAC")]
        public ActionResult SelectSecondaryVASNew()
        {
            Session["OfferId_Seco"] = null;
            Session["MandatoryVasIds"] = null;
            Session["MandatoryVasIdsSeco"] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session["RegMobileReg_OfferDevicePrice_Seco"] = null;
            Session[SessionKey.RegMobileReg_VasNames_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = null;
            WebHelper.Instance.ResetDepositSessions();
            List<MOCOfferDetails> offers = new List<MOCOfferDetails>();


            ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
            if (Session["VasGroupIds_Seco"].ToString2() != "")
            {
                Lstvam2.hdnGroupIds = Session["VasGroupIds_Seco"].ToString2();
            }


			// Gerry - Drop5
			offers = WebHelper.Instance.GetMOCOfferByArticleID(Session["ArticleId_Seco"].ToString2());

            if (offers != null)
                Lstvam2.Offers = offers;

            if (Session[SessionKey.PPID.ToString()].ToString() != "")
            {
                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null))
                {
                    Session.Contents.Remove("RedirectURL");
                    Session[SessionKey.MenuType.ToString()] = "1";
                }
                if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] == null || (Convert.ToString(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()]) == String.Empty))
                {
                    Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = Session["GuidedSales_PkgPgmBdlPkgCompID_Seco"];
                }
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();


                Lstvam = GetAvailablePackageComponents(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(), isMandatory: false);

                string MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();

                Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);

                Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);

                var Ivalexists = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).PlanBundle;

                /* For Mandatory packages */
                Session["vasmandatoryids"] = "";
                using (var proxy = new CatalogServiceProxy())
                {

                    BundlepackageResp respon = new BundlepackageResp();

                    respon = proxy.GetMandatoryVas(Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()]));
                    int i = 0;
                    for (i = 0; i < respon.values.Count(); i++)
                    {

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Master_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            //  Session["intmandatoryidsval"] = respon.values[i].BdlDataPkgId;

                            Session["intmandatoryidsvalSec"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
                        }

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.CompType_DataCon)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault
                                                             };


                            Session["intdataidsvalSec"] = respon.values[i].BdlDataPkgId;
                            Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts);
                        }
                        /*Extra packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Extra_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault
                                                             };


                            // Session["intextraidsval"] = respon.values[i].BdlDataPkgId;

                            Session["intextraidsvalSec"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);
                        }
                        //Pramotional Packages
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Pramotional_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            #region COmmented filterComponents for Pramotional components
                            //List<int> Finalintvalues = null;
                            //if (Session["RegMobileReg_Type"].ToInt() == 1)
                            //{
                            //    List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();
                            //    string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();
                            //    Finalintvalues = values.Select(a => a.BdlDataPkgId).Distinct().ToList();
                            //    Lstvam.DeviceType = "Seco_NoDevice";
                            //}
                            //else if (Session["RegMobileReg_Type"].ToInt() == 2)
                            //{
                            //    List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
                            //    Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
                            //}
                            //var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                            //                                 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                            //                                 select new AvailableVAS
                            //                                 {
                            //                                     ComponentName = e.ComponentName,
                            //                                     PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                            //                                     Price = e.Price,
                            //                                     Value = e.Value,
                            //                                     GroupId = e.GroupId,
                            //                                     GroupName = e.GroupName,
                            //                                     KenanCode = e.KenanCode
                            //                                 }; 
                            #endregion
                            Session["intpramotionidsvalSec"] = respon.values[i].BdlDataPkgId;
                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intpramotionidsval = respon.values[i].BdlDataPkgId;
                                }
                            }

                            Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
                        }
                        /*Data Contract Discount packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.DiscountDataContract_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            if (Session["RegMobileReg_Type"].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session["RegMobileReg_Type"].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }
                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault
                                                             };
                            Session["intdatadiscountidsval"] = respon.values[i].BdlDataPkgId;
                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intdatadiscountidsval = respon.values[i].BdlDataPkgId;
                                }
                            }

                            Lstvam2.PramotionalComponents.AddRange(finalfiltereddatacontracts);
                        }

                    }
                    //GTM e-Billing CR - Ricky - 2014.09.25
                    //To retrieve the component relation
                    Lstvam2.MutualExclusiveComponents = proxy.ComponentRelationGetMutualExclusive(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));

                }


                if (MobileRegType == "Planonly")
                {
                    Lstvam.ContractVASes.Clear();
                }

                Session["SecondaryLineVas"] = Lstvam2;

                if (Lstvam2 != null)
                {
                    return View(Lstvam2);
                }
                else
                {
                    Lstvam2 = (ValueAddedServicesVM)Session["SecondaryLineVas"];
                    return View(Lstvam2);
                }


            }
            else
            {
                Session[SessionKey.RedirectURL.ToString()] = "Registration/SelectVASNew";
                return RedirectToAction("Index", "Home");
            }
        }


        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,DREG_N,DREG_C,MREG_DC,MREG_DSV,MREG_DSA,MREG_DAI,MREG_DAC")]
        public ActionResult SelectSecondaryVAS(FormCollection collection, ValueAddedServicesVM vasVM)
        {
            //added by ravi on jan 03 2014 as it is giving exception
            if (vasVM.SelectedVasIDs.ToString2().Length > 0)
            {
                vasVM.SelectedVasIDs = vasVM.SelectedVasIDs.Replace("[object XMLDocument]", string.Empty);
            }
            //added by ravi on jan 03 2014 as it is giving exception ends here.

            #region fxDependency Check
            if (vasVM.TabNumber == 6)
            {
                List<FxDependChkComps> Comps = new List<FxDependChkComps>();
                using (var Proxy = new RegistrationServiceProxy())
                {
                    string strCompIds = string.Empty;
                    if (vasVM.SelectedVasIDs.ToString2() != string.Empty)
                    {
                        strCompIds = vasVM.SelectedVasIDs;
                        //session changed by ravi on jan 23 2014
                        Session[SessionKey.SelectedComponents_Seco.ToString()] = vasVM.SelectedVasIDs;
                        //session changed by ravi on jan 23 2014 ends here

                    }
                    if (Session["MandatoryVasIdsSeco"].ToString2().Length > 0)
                    {

                        strCompIds = strCompIds + "," + Session["MandatoryVasIdsSeco"].ToString();
                    }
                    strCompIds += "," + Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToString2();

                    Session[SessionKey.OfferId.ToString()] = vasVM.OfferId.ToString();
                    //for adding contract id
                    if (vasVM.SelectedContractID.ToString2().Length > 0)
                    {
                        strCompIds += "," + vasVM.SelectedContractID;
                    }
                    // added by Nreddy by pass the dependece check method when click on back button
                    if (collection["TabNumber"] != "2")
                    {
                        Comps = Proxy.GetAllComponentsbyPlanId(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(), strCompIds, "SP");
                    }
                }


                List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();

                foreach (var comp in Comps)
                {
                    Online.Registration.Web.SubscriberICService.componentList component = new componentList();
                    component.componentIdField = comp.ComponentKenanCode;
                    component.packageIdField = comp.KenanCode;
                    componentList.Add(component);
                    if (comp.DependentKenanCode != -1)
                    {
                        component = new componentList();
                        component.componentIdField = comp.DependentKenanCode.ToString();
                        component.packageIdField = comp.KenanCode;

                        componentList.Add(component);
                    }
                }
                if (componentList.Count > 0)
                {
					// Drop 5
					retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
					string pbpcID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
					typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, pbpcID: pbpcID);

                    typeChkFxVasDependencyResponse response = _retrieveServiceInfoProxy.ChkFxVasDependency(request);
                    ValueAddedServicesVM vas = (ValueAddedServicesVM)Session["SecondaryLineVas"];
                    if (response.mxsRuleMessagesField != null && response.mxsRuleMessagesField.Count > 0)
                    {
                        foreach (string errmsg in response.mxsRuleMessagesField)
                        {
                            ModelState.AddModelError("", errmsg);
                        }
                        return View(vas);
                    }
                    else if (response.msgCodeField == "3")
                    {
                        ModelState.AddModelError("", "OPF system error - Unable to fetch data.");   // Need to change msg
                        return View(vas);
                    }
                    else if (response.msgCodeField != "0")
                    {
                        ModelState.AddModelError("", "Selected components are not available.");   // Need to change msg
                        return View(vas);
                    }
                }
            }
            #endregion
            Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] = vasVM.SelectedContractID;
            Session["Secondaryvas"] = "Secondaryvas";
            Session["VasGroupIds_Seco"] = vasVM.hdnGroupIds;
            int selectedPlan = 0;
            List<int> planIds = new List<int>();
            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] != null)
            {
                selectedPlan = Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()]);
                planIds.Add(selectedPlan);
            }
            Session["RegMobileReg_MainDevicePrice_Seco"] = vasVM.priceSeco;
            Session["RegMobileReg_OfferDevicePrice_Seco"] = vasVM.priceSeco;
            Session[SessionKey.RegMobileReg_UOMCode.ToString()] = vasVM.uomCodeSeco;
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = vasVM.TabNumber;
            PgmBdlPckComponent package = new PgmBdlPckComponent();
            Session["OfferId_Seco"] = vasVM.OfferIdSeco.ToString();
            using (var proxy = new CatalogServiceProxy())
            {
                if (planIds.Count > 0)
                {
                    package = MasterDataCache.Instance.FilterComponents(planIds).First();
                    var respOffers = proxy.GETMOCOfferDetails(vasVM.OfferIdSeco);
                    MOCOfferDetails mocOffer = new MOCOfferDetails();
                    if (respOffers != null)
                    {
                        mocOffer.DiscountAmount = respOffers.DiscountAmount;
                        mocOffer.ID = respOffers.ID;
                        mocOffer.Name = respOffers.Name;
                        mocOffer.OfferDescription = respOffers.OfferDescription;
                        Session[SessionKey.Discount.ToString()] = mocOffer.DiscountAmount;
                    }
                }
            }

            var selectedVasIDs = "";
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = vasVM.ExtraTenMobileNumbers;

            if (vasVM.SelectedVasIDs != null)
            {
                if (vasVM.SelectedVasIDs != null && !Regex.IsMatch(vasVM.SelectedVasIDs, @"^,+$") && vasVM.SelectedVasIDs != "[object XMLDocument]")
                {
                    selectedVasIDs = vasVM.SelectedVasIDs;
                    Session["SelectedComponents_Seco"] = vasVM.SelectedVasIDs;

                    Session["RegMobileReg_DataPkgID_Seco"] = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
                }
            }
            else
            {
                if (!ReferenceEquals(Session["RegMobileReg_DataPkgID_Seco"], null))
                {
                    Session.Contents.Remove("RegMobileReg_DataPkgID_Seco");
                }
                if (!ReferenceEquals(Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()], null))
                {
                    Session.Contents.Remove("RegMobileReg_VasIDs_Seco");
                }
            }
            if (selectedVasIDs != "[object XMLDocument]")
            {
                if (package.ParentID == 2 || package.ParentID == 1)
                {
                    string defCompId = string.Empty;

                    if (defCompId != string.Empty)
                    {
                        if (selectedVasIDs == null || selectedVasIDs == string.Empty)
                            selectedVasIDs = defCompId;
                        else
                            selectedVasIDs = selectedVasIDs + "," + defCompId;

                        if (selectedVasIDs != string.Empty && selectedVasIDs.Contains(","))
                            selectedVasIDs = string.Join(",", selectedVasIDs.TrimEnd(',').Split(',').ToList<string>().Select(id => id.ToString()).Distinct()) + ",";
                    }
                }
            }
            try
            {
                if ((vasVM.TabNumber == (int)MobileRegistrationSteps.MobileNo || ((Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM") && vasVM.TabNumber == (int)RegistrationSteps.MobileNo)) && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) || !string.IsNullOrEmpty(vasVM.SelectedContractID)))
                {
                    if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                    {
                        #region VLT ADDED CODE
                        //Contract check for existing users
                        //Retrieve data from session
                        #endregion VLT ADDED CODE
                        vasVM.SelectedVasIDs = selectedVasIDs;
                        if (!string.IsNullOrEmpty(Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()].ToString2()))
                            if (vasVM.SelectedVasIDs != null)
                                selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()].ToString2()), "");
                        /*Condition added by chetan on 27042013*/
                        if (selectedVasIDs != "")
                        {
                            if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
                            {
                                selectedVasIDs += "," + vasVM.SelectedContractID + ",";
                            }
                        }
                        else
                        {
                            selectedVasIDs += vasVM.SelectedContractID + ",";
                        }
                    }

                    vasVM.SelectedVasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;

                    Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;

                    Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] = vasVM.SelectedContractID;

                    using (var proxy = new CatalogServiceProxy())
                    {
                        Session[SessionKey.RegMobileReg_DataplanID_Seco.ToString()] = proxy.GettllnkContractDataPlan(Convert.ToInt32(Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()]));
                    }

                }
            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == true)
            {
                return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber + 1));
            }
            else
            {
                if (vasVM.TabNumber == 10)
                {
                    if (Session["GuidedSales_PkgPgmBdlPkgCompID_Seco"] != null)
                        return RedirectToAction("Plan", "GuidedSales");
                }
                if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM")
                {
                    if (!string.IsNullOrEmpty(Session[SessionKey.MobileRegType.ToString()].ToString()))
                    {
                        if (vasVM.TabNumberSeco == 6)
                        {
                            return RedirectToAction(WebHelper.Instance.GetRegActionStep(RegistrationSteps.MobileNo.ToInt()), new { type = 2 });
                        }
                        else if (vasVM.TabNumberSeco == 4)
                        {
                            return RedirectToAction(WebHelper.Instance.GetRegActionStep(RegistrationSteps.SecoPlan.ToInt()), new { type = 2 });
                        }

                    }
                    else
                    {
                        return RedirectToAction(WebHelper.Instance.GetRegActionStep(RegistrationSteps.SecoPlan.ToInt()), new { type = 2 });
                    }

                }
                if (!string.IsNullOrEmpty(Session[SessionKey.MobileRegType.ToString()].ToString()))
                {
                    string MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();
                    if (MobileRegType == "Planonly")
                    {
                        return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber), new { type = 2 });
                    }
                    else
                    {
                        return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber));
                    }
                }
                else
                {
                    return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber));
                }
            }
        }



        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,DREG_N,DREG_C,MREG_DC,MREG_DSV,MREG_DSA,MREG_DAI,MREG_DAC")]
        public ActionResult SelectSecondaryVASNew(FormCollection collection, ValueAddedServicesVM vasVM)
        {
            //added by ravi on jan 03 2014 as it is giving exception
            if (vasVM.SelectedVasIDs.ToString2().Length > 0)
            {
                vasVM.SelectedVasIDs = vasVM.SelectedVasIDs.Replace("[object XMLDocument]", string.Empty);
            }
            //added by ravi on jan 03 2014 as it is giving exception ends here.

            #region fxDependency Check
            if (vasVM.TabNumber == 6)
            {
                List<FxDependChkComps> Comps = new List<FxDependChkComps>();
                using (var Proxy = new RegistrationServiceProxy())
                {
                    string strCompIds = string.Empty;
                    if (vasVM.SelectedVasIDs.ToString2() != string.Empty)
                    {
                        strCompIds = vasVM.SelectedVasIDs;
                        //session changed by ravi on jan 23 2014
                        Session[SessionKey.SelectedComponents_Seco.ToString()] = vasVM.SelectedVasIDs;
                        //session changed by ravi on jan 23 2014 ends here

                    }
                    if (Session["MandatoryVasIdsSeco"].ToString2().Length > 0)
                    {

                        strCompIds = strCompIds + "," + Session["MandatoryVasIdsSeco"].ToString();
                    }
                    strCompIds += "," + Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToString2();

                    Session[SessionKey.OfferId.ToString()] = vasVM.OfferId.ToString();
                    //for adding contract id
                    if (vasVM.SelectedContractID.ToString2().Length > 0)
                    {
                        strCompIds += "," + vasVM.SelectedContractID;
                    }
                    // added by Nreddy by pass the dependece check method when click on back button
                    if (collection["TabNumber"] != "2")
                    {
                        Comps = Proxy.GetAllComponentsbyPlanId(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(), strCompIds, "SP");
                    }
                }


                List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();

                foreach (var comp in Comps)
                {
                    Online.Registration.Web.SubscriberICService.componentList component = new componentList();
                    component.componentIdField = comp.ComponentKenanCode;
                    component.packageIdField = comp.KenanCode;
                    componentList.Add(component);
                    if (comp.DependentKenanCode != -1)
                    {
                        component = new componentList();
                        component.componentIdField = comp.DependentKenanCode.ToString();
                        component.packageIdField = comp.KenanCode;

                        componentList.Add(component);
                    }
                }
                if (componentList.Count > 0)
                {
					// Drop 5
					retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
					string pbpcID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
					typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, pbpcID: pbpcID);

                    typeChkFxVasDependencyResponse response = _retrieveServiceInfoProxy.ChkFxVasDependency(request);
                    ValueAddedServicesVM vas = (ValueAddedServicesVM)Session["SecondaryLineVas"];
                    if (response.mxsRuleMessagesField != null && response.mxsRuleMessagesField.Count > 0)
                    {
                        foreach (string errmsg in response.mxsRuleMessagesField)
                        {
                            ModelState.AddModelError("", errmsg);
                        }
                        return View(vas);
                    }
                    else if (response.msgCodeField == "3")
                    {
                        ModelState.AddModelError("", "OPF system error - Unable to fetch data.");   // Need to change msg
                        return View(vas);
                    }
                    else if (response.msgCodeField != "0")
                    {
                        ModelState.AddModelError("", "Selected components are not available.");   // Need to change msg
                        return View(vas);
                    }
                }
            }
            #endregion
            Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] = vasVM.SelectedContractID;
            Session["Secondaryvas"] = "Secondaryvas";
            Session["VasGroupIds_Seco"] = vasVM.hdnGroupIds;
            int selectedPlan = 0;
            List<int> planIds = new List<int>();
            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] != null)
            {
                selectedPlan = Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()]);
                planIds.Add(selectedPlan);
            }
            Session["RegMobileReg_MainDevicePrice_Seco"] = vasVM.priceSeco;
            Session["RegMobileReg_OfferDevicePrice_Seco"] = vasVM.priceSeco;
            Session[SessionKey.RegMobileReg_UOMCode.ToString()] = vasVM.uomCodeSeco;
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = vasVM.TabNumber;
            PgmBdlPckComponent package = new PgmBdlPckComponent();
            Session["OfferId_Seco"] = vasVM.OfferIdSeco.ToString();
            using (var proxy = new CatalogServiceProxy())
            {
                if (planIds.Count > 0)
                {
                    package = MasterDataCache.Instance.FilterComponents(planIds).First();
                    var respOffers = proxy.GETMOCOfferDetails(vasVM.OfferIdSeco);
                    MOCOfferDetails mocOffer = new MOCOfferDetails();
                    if (respOffers != null)
                    {
                        mocOffer.DiscountAmount = respOffers.DiscountAmount;
                        mocOffer.ID = respOffers.ID;
                        mocOffer.Name = respOffers.Name;
                        mocOffer.OfferDescription = respOffers.OfferDescription;
                        Session[SessionKey.Discount.ToString()] = mocOffer.DiscountAmount;
                    }
                }
            }

            var selectedVasIDs = "";
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = vasVM.ExtraTenMobileNumbers;

            if (vasVM.SelectedVasIDs != null)
            {
                if (vasVM.SelectedVasIDs != null && !Regex.IsMatch(vasVM.SelectedVasIDs, @"^,+$") && vasVM.SelectedVasIDs != "[object XMLDocument]")
                {
                    selectedVasIDs = vasVM.SelectedVasIDs;
                    Session["SelectedComponents_Seco"] = vasVM.SelectedVasIDs;

                    Session["RegMobileReg_DataPkgID_Seco"] = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
                }
            }
            else
            {
                if (!ReferenceEquals(Session["RegMobileReg_DataPkgID_Seco"], null))
                {
                    Session.Contents.Remove("RegMobileReg_DataPkgID_Seco");
                }
                if (!ReferenceEquals(Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()], null))
                {
                    Session.Contents.Remove("RegMobileReg_VasIDs_Seco");
                }
            }
            if (selectedVasIDs != "[object XMLDocument]")
            {
                if (package.ParentID == 2 || package.ParentID == 1)
                {
                    string defCompId = string.Empty;

                    if (defCompId != string.Empty)
                    {
                        if (selectedVasIDs == null || selectedVasIDs == string.Empty)
                            selectedVasIDs = defCompId;
                        else
                            selectedVasIDs = selectedVasIDs + "," + defCompId;

                        if (selectedVasIDs != string.Empty && selectedVasIDs.Contains(","))
                            selectedVasIDs = string.Join(",", selectedVasIDs.TrimEnd(',').Split(',').ToList<string>().Select(id => id.ToString()).Distinct()) + ",";
                    }
                }
            }
            try
            {
                if ((vasVM.TabNumber == (int)MobileRegistrationSteps.MobileNo || ((Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM") && vasVM.TabNumber == (int)RegistrationSteps.MobileNo)) && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) || !string.IsNullOrEmpty(vasVM.SelectedContractID)))
                {
                    if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                    {
                        #region VLT ADDED CODE
                        //Contract check for existing users
                        //Retrieve data from session
                        #endregion VLT ADDED CODE
                        vasVM.SelectedVasIDs = selectedVasIDs;
                        if (!string.IsNullOrEmpty(Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()].ToString2()))
                            if (vasVM.SelectedVasIDs != null)
                                selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()].ToString2()), "");
                        /*Condition added by chetan on 27042013*/
                        if (selectedVasIDs != "")
                        {
                            if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
                            {
                                selectedVasIDs += "," + vasVM.SelectedContractID + ",";
                            }
                        }
                        else
                        {
                            selectedVasIDs += vasVM.SelectedContractID + ",";
                        }
                    }

                    vasVM.SelectedVasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;

                    Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;

                    Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] = vasVM.SelectedContractID;

                    using (var proxy = new CatalogServiceProxy())
                    {
                        Session[SessionKey.RegMobileReg_DataplanID_Seco.ToString()] = proxy.GettllnkContractDataPlan(Convert.ToInt32(Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()]));
                    }

                }
            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == true)
            {
                return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber + 1));
            }
            else
            {
                if (vasVM.TabNumber == 10)
                {
                    if (Session["GuidedSales_PkgPgmBdlPkgCompID_Seco"] != null)
                        return RedirectToAction("Plan", "GuidedSales");
                }
                if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM")
                {
                    if (!string.IsNullOrEmpty(Session[SessionKey.MobileRegType.ToString()].ToString()))
                    {
                        if (vasVM.TabNumberSeco == 6)
                        {
                            return RedirectToAction(WebHelper.Instance.GetRegActionStep(RegistrationSteps.MobileNo.ToInt()), new { type = 2 });
                        }
                        else if (vasVM.TabNumberSeco == 4)
                        {
                            return RedirectToAction(WebHelper.Instance.GetRegActionStep(RegistrationSteps.SecoPlan.ToInt()), new { type = 2 });
                        }

                    }
                    else
                    {
                        return RedirectToAction(WebHelper.Instance.GetRegActionStep(RegistrationSteps.SecoPlan.ToInt()), new { type = 2 });
                    }

                }
                if (!string.IsNullOrEmpty(Session[SessionKey.MobileRegType.ToString()].ToString()))
                {
                    string MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();
                    if (MobileRegType == "Planonly")
                    {
                        return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber), new { type = 2 });
                    }
                    else
                    {
                        return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber));
                    }
                }
                else
                {
                    return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber));
                }
            }
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA")]
        public ActionResult SelectMobileNo(FormCollection collection, MobileNoVM mobileNoVM)
        {
            try
            {
                Session[SessionKey.ExtratenMobilenumbers.ToString()] = mobileNoVM.ExtraTenMobileNumbers;
                using (var proxy = new UserServiceProxy())
                {
                    proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Select Mobile Number", Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                }
                //Added by ravi on Apr 10 2014 for by passing mobile no update 
                Session[SessionKey.NumberType.ToString()] = collection["hdnNumberType"].ToString2();
                //Added by ravi on Apr 10 2014 for by passing mobile no update  ends here
                if (collection["submit1"].ToString() == "subline")
                {
                    WebHelper.Instance.ClearSubLineSession();
                    return RedirectToAction("SubLinePlan");
                }
                else if (collection["submit1"].ToString() == "search")
                {
					return RedirectToAction("SelectMobileNoNew", new
					{
						desireNo = mobileNoVM.DesireNo,
						numberType = collection["hdnNumberType"].ToString2(),
						prefix = collection["ddlPrefix"].ToString2(),
						validateAgentCode = collection["validateAgentCode"].ToBool(),
						salesAgentCode = collection["salesAgentCode"].ToString2()
					});
                }
                else if (collection["submit1"].ToInt() > 0)
                {
                    var squeenceNo = collection["submit1"].ToInt();
                    var subLines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
                    var removeItem = subLines.Where(a => a.SequenceNo == squeenceNo).SingleOrDefault();
                    subLines.Remove(removeItem);

                    for (int i = 1; i <= subLines.Count(); i++)
                    {
                        subLines[i - 1].SequenceNo = i;
                    }

                    Session[SessionKey.RegMobileReg_SublineVM.ToString()] = subLines;

                    return RedirectToAction(WebHelper.Instance.GetRegActionStep((int)Session[SessionKey.RegMobileReg_TabIndex.ToString()]));
                }
                else
                {
                    Session[SessionKey.RegMobileReg_TabIndex.ToString()] = mobileNoVM.TabNumber;

                    var mobileNos = new List<string>();
                    if (mobileNoVM != null && mobileNoVM.SelectedMobileNo != null)
                    {
                        if (!mobileNoVM.SelectedMobileNo.StartsWith("60"))
                        {
                            mobileNoVM.SelectedMobileNo = "60" + mobileNoVM.SelectedMobileNo;
                        }
                        mobileNos.Add(mobileNoVM.SelectedMobileNo);
                    }

                    if ((int)MobileRegistrationSteps.PersonalDetails == mobileNoVM.TabNumber || ((Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM") && mobileNoVM.TabNumber == (int)RegistrationSteps.MobileNo + 1))
                    {
                        Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;
                    }
                }
                if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM")
                {
                    if (Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] == null)
                    {
                        var proxy = new retrieveServiceInfoProxy();
                        List<string> mobileNos = new List<string>();
                        Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] = "";

                    }
                }
                if (mobileNoVM != null && mobileNoVM.SelectedMobileNo != null)
                {
                    bool checkval = false;
                    using (var proxy = new KenanServiceProxy())
                    {
                        checkval = proxy.validateInventoryExtIdService(new TypeValidateInventoryExtIdRequest { externalId = mobileNoVM.SelectedMobileNo });
                    }
                    if (checkval)
                    {
                        return RedirectToAction(WebHelper.Instance.GetRegActionStep(mobileNoVM.TabNumber));
                    }
                    else
                    {
                        return RedirectToAction("SelectMobileNo", new { invchk = "False" });
                    }
                }
                else
                {
                    return RedirectToAction(WebHelper.Instance.GetRegActionStep(mobileNoVM.TabNumber));
                }


            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }

        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA")]
        public ActionResult SelectMobileNoNew(FormCollection collection, MobileNoVM mobileNoVM)
        {
            try
            {
                ActionResult redirectPage = null;
                String simSize = collection["SimSize"].ToString2();
				string selectedNumber = string.Empty;
				selectedNumber = mobileNoVM.SelectedMobileNo;

                Session[SessionKey.ExtratenMobilenumbers.ToString()] = mobileNoVM.ExtraTenMobileNumbers;
                using (var proxy = new UserServiceProxy())
                {
                    //TODO Session[SessionKey.KenanACNumber.ToString()].ToString()
                    proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Select Mobile Number", "", "");
                }
                //Added by ravi on Apr 10 2014 for by passing mobile no update 
                Session[SessionKey.NumberType.ToString()] = collection["hdnNumberType"].ToString2();
                //Added by ravi on Apr 10 2014 for by passing mobile no update  ends here
                if (collection["submit1"].ToString() == "subline")
                {
                    WebHelper.Instance.ClearSubLineSession();
                    return RedirectToAction("SubLinePlan");
                }
                else if (collection["submit1"].ToString() == "search")
                {
					return RedirectToAction("SelectMobileNoNew", new
					{
						desireNo = mobileNoVM.DesireNo,
						numberType = collection["hdnNumberType"].ToString2(),
						prefix = collection["ddlPrefix"].ToString2(),
						validateAgentCode = collection["validateAgentCode"].ToBool(),
						salesAgentCode = collection["salesAgentCode"].ToString2()
					});
                }
                else if (collection["submit1"].ToInt() > 0)
                {
                    var squeenceNo = collection["submit1"].ToInt();
                    var subLines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
                    var removeItem = subLines.Where(a => a.SequenceNo == squeenceNo).SingleOrDefault();
                    subLines.Remove(removeItem);

                    for (int i = 1; i <= subLines.Count(); i++)
                    {
                        subLines[i - 1].SequenceNo = i;
                    }

                    Session[SessionKey.RegMobileReg_SublineVM.ToString()] = subLines;

                    return RedirectToAction(WebHelper.Instance.GetRegActionStep((int)Session[SessionKey.RegMobileReg_TabIndex.ToString()]));
                }
                else
                {
                    Session[SessionKey.RegMobileReg_TabIndex.ToString()] = mobileNoVM.TabNumber;

                    var mobileNos = new List<string>();
                    if (mobileNoVM != null && mobileNoVM.SelectedMobileNo != null)
                    {
                        if (!mobileNoVM.SelectedMobileNo.StartsWith("60"))
                        {
                            mobileNoVM.SelectedMobileNo = "60" + mobileNoVM.SelectedMobileNo;
                        }
                        mobileNos.Add(mobileNoVM.SelectedMobileNo);
                    }

                    if ((int)MobileRegistrationSteps.PersonalDetails == mobileNoVM.TabNumber || ((Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM") && mobileNoVM.TabNumber == (int)RegistrationSteps.MobileNo + 1))
                    {
                        Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;
                    }
                }
                if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM")
                {
                    if (Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] == null)
                    {
                        var proxy = new retrieveServiceInfoProxy();
                        List<string> mobileNos = new List<string>();
                        Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] = "";

                    }
                }
				
                PersonalDetailsVM personalDetailsVM = new PersonalDetailsVM();
                var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
				
				if (!dropObj.selectedMobileNoinFlow.Contains(selectedNumber))
					dropObj.selectedMobileNoinFlow.Add(selectedNumber);

                mobileNoVM.SIMCardType = simSize;
                mobileNoVM.NumberType = collection["hdnNumberType"].ToString2();//20032015 - Anthony - Fix for reserved number type is null

                DropFourHelpers.UpdateMobile(dropObj, mobileNoVM);
                DropFourHelpers.SimSize(dropObj, simSize);

                var msismType = collection["MsismType"];
                if (!string.IsNullOrEmpty(msismType))
                {
                    dropObj.msimIndex = dropObj.msimIndex + 1;
                    dropObj.msimFlow.Add(new Flow());
                    dropObj.mainFlow.mobile.SIMCardType = "MISM";
                    dropObj.msimFlow[0].simSize = msismType;
                }
                else
                {
                    dropObj.msimIndex = 0;
                    dropObj.msimFlow = new List<Flow>();
                    dropObj.mainFlow.mobile.SIMCardType = "Normal";
                }


                var flowID = collection["flowHidden"].ToString2();
                if (flowID != "None")
                {
                    switch (flowID)
                    {
                        case "PlanNDevice":
                            Session["IsNewBillingAddress"] = "ExistingBillingAddress";
                            Session[SessionKey.IsSuppleNewAccount.ToString()] = false;
                            Session["IsDeviceRequired"] = "Yes";
                            //dropObj.currentFlow = DropFourConstant.SUPP_FLOW;
                            //dropObj.suppIndex = dropObj.suppIndex + 1;
                            //dropObj.suppFlow.Add(new Flow());
                            //dropObj.suppFlow[dropObj.suppIndex - 1].RegType = (int)MobileRegType.DevicePlan;
                            Session[SessionKey.NewLineSupplementary.ToString()] = true;
                            //redirectPage = RedirectToAction("SelectDevicePlan", "SuppLine");
                            redirectPage = RedirectToAction("SelectDevicePlan", "SuppLine", new { type = 1 });//Anthony - [#2450] Send RegType to SuppLineController
                            break;
                        case "Plan":
                            Session["IsNewBillingAddress"] = "ExistingBillingAddress";
                            Session[SessionKey.IsSuppleNewAccount.ToString()] = false;
                            Session["IsDeviceRequired"] = "No";
                            //dropObj.currentFlow = DropFourConstant.SUPP_FLOW;
                            //dropObj.suppIndex = dropObj.suppIndex + 1;
                            //dropObj.suppFlow.Add(new Flow());
                            //dropObj.suppFlow[dropObj.suppIndex - 1].RegType = (int)MobileRegType.PlanOnly;
                            Session[SessionKey.NewLineSupplementary.ToString()] = true;
                            //redirectPage = RedirectToAction("SuppLinePlanNew", "SuppLine");
                            redirectPage = RedirectToAction("SuppLinePlanNew", "SuppLine", new { type = 8 });//Anthony - [#2450] Send RegType to SuppLineController
                            break;
                        default:
                            break;
                    }
                }
                else
                {

                    if (mobileNoVM != null && mobileNoVM.SelectedMobileNo != null)
                    {
                        bool checkval = false;
                        using (var proxy = new KenanServiceProxy())
                        {
                            checkval = proxy.validateInventoryExtIdService(new TypeValidateInventoryExtIdRequest { externalId = mobileNoVM.SelectedMobileNo });
                        }
                        if (!checkval)
                        {
                            redirectPage = RedirectToAction("SelectMobileNoNew", new { invchk = "False" });
                        }
                        else
                        {   //Setting SimType to Normal if choose None
                            if (!string.IsNullOrEmpty(msismType))
                            {
                                Session["SimType"] = "MISM";
                                mobileNoVM.SIMCardType = "MISM";
                            }
                            else
                            {
                                Session["SimType"] = "Normal";
                                mobileNoVM.SIMCardType = "Normal";
                            }

                            DropFourHelpers.UpdateMobile(dropObj, mobileNoVM);
                            Session[SessionKey.DropFourObj.ToString()] = dropObj;
                            redirectPage = RedirectToAction(WebHelper.Instance.GetRegActionStep(mobileNoVM.TabNumber));
                        }
                        if (checkval)
                        {
                            redirectPage =  RedirectToAction(WebHelper.Instance.GetRegActionStep(mobileNoVM.TabNumber));
                        }
                        else
                        {
                            redirectPage = RedirectToAction("SelectMobileNoNew", new { invchk = "False" });
                        }
                    }
                    else
                    {
                       redirectPage = RedirectToAction(WebHelper.Instance.GetRegActionStep(mobileNoVM.TabNumber));
                    }
                }
                Session[SessionKey.DropFourObj.ToString()] = dropObj;
                return redirectPage;

            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }

        }




        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA")]
        public ActionResult SelectVAS()
        {
            Session["MandatoryVasIds"] = null;
            Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"] = null;
            Session["SelectedKenanCodes"] = null;
            ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
            if (Session[SessionKey.PPID.ToString()].ToString() != "")
            {
                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null))
                {
                    Session.Contents.Remove("RedirectURL");
                    Session[SessionKey.MenuType.ToString()] = "1";
                }
                SetRegStepNo(MobileRegistrationSteps.Vas);
                if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] == null || (Convert.ToString(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]) == String.Empty))
                {
                    Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = Session["GuidedSales_PkgPgmBdlPkgCompID"];
                }
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();

                if (Session[SessionKey.VasGroupIds.ToString()].ToString2() != "")
                {
                    Lstvam2.hdnGroupIds = Session[SessionKey.VasGroupIds.ToString()].ToString2();
                }
                Lstvam = GetAvailablePackageComponents(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), isMandatory: false);

                string MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();

                Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);

                Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);

                var Ivalexists = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).PlanBundle;

                /* For Mandatory packages */
                Session["vasmandatoryids"] = "";
                using (var proxy = new CatalogServiceProxy())
                {

                    BundlepackageResp respon = new BundlepackageResp();

                    respon = proxy.GetMandatoryVas(Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]));
                    //get Non mandatory vas those have to display in datacomponents section
                    Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]), "MD", isMandatory: false);
                    Lstvam2.DataVASes.AddRange(Lstvam.AvailableVASes);
                    //get Main vas those have to display in datacomponents section
                    Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]), "DM", isMandatory: false);
                    Lstvam2.DataVASes.AddRange(Lstvam.AvailableVASes);
                    int i = 0;
                    for (i = 0; i < respon.values.Count(); i++)
                    {

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Master_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            Session["intmandatoryidsval"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
                        }

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.CompType_DataCon)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault

                                                             };


                            Session["intdataidsval"] = respon.values[i].BdlDataPkgId;
                            Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts);
                        }

                        /*Extra packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Extra_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault

                                                             };


                            Session["intextraidsval"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);
                        }
                        //Pramotional Packages
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Pramotional_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            #region COmmented filterComponents for Pramotional components
                            //List<int> Finalintvalues = null;
                            //if (Session["RegMobileReg_Type"].ToInt() == 1)
                            //{
                            //    List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();
                            //    string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();
                            //    Finalintvalues = values.Select(a => a.BdlDataPkgId).Distinct().ToList();
                            //    Lstvam.DeviceType = "Seco_NoDevice";
                            //}
                            //else if (Session["RegMobileReg_Type"].ToInt() == 2)
                            //{
                            //    List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
                            //    Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
                            //}
                            //var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                            //                                 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                            //                                 select new AvailableVAS
                            //                                 {
                            //                                     ComponentName = e.ComponentName,
                            //                                     PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                            //                                     Price = e.Price,
                            //                                     Value = e.Value,
                            //                                     GroupId = e.GroupId,
                            //                                     GroupName = e.GroupName,
                            //                                     KenanCode = e.KenanCode
                            //                                 }; 
                            #endregion
                            Session["intpramotionidsval"] = respon.values[i].BdlDataPkgId;
                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intpramotionidsval = respon.values[i].BdlDataPkgId;
                                }
                            }

                            Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
                        }

                        /*Data Contract Discount packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.DiscountDataContract_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            if (Session["RegMobileReg_Type"].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session["RegMobileReg_Type"].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }
                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault
                                                             };
                            Session["intdatadiscountidsval"] = respon.values[i].BdlDataPkgId;
                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intdatadiscountidsval = respon.values[i].BdlDataPkgId;
                                }
                            }

                            Lstvam2.PramotionalComponents.AddRange(finalfiltereddatacontracts);
                        }

                    }


                }


                if (MobileRegType == "Planonly")
                {
                    Lstvam.ContractVASes.Clear();
                }

                Session["newLineVas"] = Lstvam2;
                return View(Lstvam2);

            }
            else
            {
                Session[SessionKey.RedirectURL.ToString()] = "Registration/SelectVAS";
                return RedirectToAction("Index", "Home");
            }
        }

        private int GetVasBackButton()
        {
            int tabNumber = 0;

            if (Session["GuidedSales_Type"].ToString2() != string.Empty)
            {
                if (Convert.ToInt32(Session["GuidedSales_Type"]) != MobileRegType.PlanOnly.ToInt())
                {
                    return MobileRegistrationSteps.CustomerPersonalDetails.ToInt();
                }
            }


            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            if (dropObj.suppIndex == 0)
            {
                if (dropObj.mainFlow.RegType == (int)MobileRegistrationSteps.DevicePlan)
                {
                    tabNumber = (int)MobileRegistrationSteps.DevicePlan;
                }
                else if (dropObj.mainFlow.RegType == (int)MobileRegistrationSteps.Plan)
                {
                    tabNumber = (int)MobileRegistrationSteps.Plan;
                }
            }
            else
            {
                //will back to main flow last screen
            }

            return tabNumber;
        }


        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA")]
        public ActionResult SelectVASNew()
        {
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            ViewBag.BackButton = GetVasBackButton();
            Session["SimType"] = null;

            Session["MandatoryVasIds"] = null;
            Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"] = null;
            Session["SelectedKenanCodes"] = null;
            Session["df_showOffer"] = null;

            List<MOCOfferDetails> offers = new List<MOCOfferDetails>();
            ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();

			// Gerry - Drop5
			offers = WebHelper.Instance.GetMOCOfferByArticleID(Session[SessionKey.ArticleId.ToString()].ToString2());

            if (offers != null)
                Lstvam2.Offers = offers;

            if (Session[SessionKey.PPID.ToString()].ToString() != "")
            {
                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null))
                {
                    Session.Contents.Remove("RedirectURL");
                    Session[SessionKey.MenuType.ToString()] = "1";
                }
                SetRegStepNo(MobileRegistrationSteps.Vas);
                if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] == null || (Convert.ToString(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]) == String.Empty))
                {
                    Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = Session["GuidedSales_PkgPgmBdlPkgCompID"];
                }
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();

                if (Session[SessionKey.VasGroupIds.ToString()].ToString2() != "")
                {
                    Lstvam2.hdnGroupIds = Session[SessionKey.VasGroupIds.ToString()].ToString2();
                }

                // commented for device and plan flow
                //Lstvam = GetAvailablePackageComponents(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), isMandatory: false);
                
                string processingMsisdn = string.Empty;
                Lstvam = WebHelper.Instance.GetAvailablePackageComponents(Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(), ref processingMsisdn, isMandatory: false);


                string _mobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();

                string _regMobileReg_Type = Session["RegMobileReg_Type"].ToString2();
                if (_regMobileReg_Type.ToNullableInt() == (int)MobileRegType.DevicePlan)
                {
                    string offernames = string.Empty;
                    string planid = Session["UOMPlanID"].ToString();
                    string articleid = Session[SessionKey.ArticleId.ToString()].ToString();
                    string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
                    
					// drop 5
					Lstvam.ContractOffers = WebHelper.Instance.GetContractOffers(Lstvam, articleid, planid, MOCStatus);
                }

                Lstvam2.ContractOffers.AddRange(Lstvam.ContractOffers);

                Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);

                Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);

                //14012015 - Anthony - GST Trade Up - Start
                foreach (var tradeUpAmountList in Lstvam2.ContractOffers)
                {
                    if (tradeUpAmountList.isTradeUp.ToBool())
                    {
                        var OfferID = tradeUpAmountList.Id;
                        var tradeUpAmounts = tradeUpAmountList.TradeUpAmount.ToString().Split(',').ToList();

                        foreach (var tradeUpAmount in tradeUpAmounts)
                        {
                            var tradeUpList = new TradeUpComponent();
                            var price = tradeUpAmount.ToInt();
                            tradeUpList.OfferId = OfferID;
                            tradeUpList.Name = "RM " + price;
                            tradeUpList.Price = price * -1;
                            Lstvam2.TradeUpList.Add(tradeUpList);
                        }
                    }
                }

                if (string.IsNullOrEmpty(Session[SessionKey.TradeUpAmount.ToString()].ToString2()) || Session[SessionKey.TradeUpAmount.ToString()].ToString2().Equals("0"))
                {
                    Session[SessionKey.TradeUpAmount.ToString()] = "empty";
                }
                //14012015 - Anthony - GST Trade Up - End

                var Ivalexists = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).PlanBundle;

                /* For Mandatory packages */
                Session["vasmandatoryids"] = "";
                using (var proxy = new CatalogServiceProxy())
                {

                    BundlepackageResp respon = new BundlepackageResp();

                    respon = proxy.GetMandatoryVas(Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]));
                    //get Non mandatory vas those have to display in datacomponents section
                    Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]), "MD", isMandatory: false);
					Lstvam2.DataVASes.AddRange(Lstvam.AvailableVASes.Where(x => x.isSharing == false));
					Lstvam2.SharingVases.AddRange(Lstvam.AvailableVASes.Where(x => x.isSharing == true));
                    
                    //get Main vas those have to display in datacomponents section
                    Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]), "DM", isMandatory: false);
					Lstvam2.DataVASes.AddRange(Lstvam.AvailableVASes.Where(x => x.isSharing == false));
					Lstvam2.SharingVases.AddRange(Lstvam.AvailableVASes.Where(x => x.isSharing == true));
					
					int i = 0;
                    for (i = 0; i < respon.values.Count(); i++)
                    {

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Master_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            Session["intmandatoryidsval"] = respon.values[i].BdlDataPkgId;

                            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
                            {
                                dropObj.mainFlow.MandatoryPackage = respon.values[i].BdlDataPkgId;
                            }
                            else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
                            {
                                dropObj.suppFlow[dropObj.suppIndex - 1].MandatoryPackage = respon.values[i].BdlDataPkgId;
                            }

                            Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
                        }

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.CompType_DataCon)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == "0").Select(a => a.BdlDataPkgId).ToList();
                                if (Finalintvalues.Count() == 0)
                                {
                                    Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();
                                }

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault,
																 isSharing = !ReferenceEquals(e.isSharing, null) ? e.isSharing : false,
																 isVRCDataBooster = !ReferenceEquals(e.isVRCDataBooster, null) ? e.isVRCDataBooster : false

                                                             };


                            Session["intdataidsval"] = respon.values[i].BdlDataPkgId;

                            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
                            {
                                dropObj.mainFlow.DataPackage = respon.values[i].BdlDataPkgId;
                            }
                            else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
                            {
                                dropObj.suppFlow[dropObj.suppIndex - 1].DataPackage = respon.values[i].BdlDataPkgId;
                            }

							// normal dataComponent will not be as Databooster or sharing
							Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts.Where(x => !x.isVRCDataBooster && !x.isSharing));

							Lstvam2.NonShareVRCDataBooster.AddRange(finalfiltereddatacontracts.Where(x => x.isVRCDataBooster == true && x.isSharing == false));
							Lstvam2.ShareVRCDataBooster.AddRange(finalfiltereddatacontracts.Where(x => x.isVRCDataBooster == true && x.isSharing == true));
                        }

                        /*Extra packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Extra_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            List<int> Finalintvalues = null;
                            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session[SessionKey.RegMobileReg_OrderSummary.ToString()])).ModelID.ToString();

								//Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();
								Finalintvalues = values.Select(a => a.BdlDataPkgId).Distinct().ToList();

                            }
                            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault

                                                             };


                            Session["intextraidsval"] = respon.values[i].BdlDataPkgId;

                            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
                            {
                                dropObj.mainFlow.ExtraPackage = respon.values[i].BdlDataPkgId;
                            }
                            else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
                            {
                                dropObj.suppFlow[dropObj.suppIndex - 1].ExtraPackage = respon.values[i].BdlDataPkgId;
                            }

                            Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);
                        }
                        //Pramotional Packages
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Pramotional_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            #region COmmented filterComponents for Pramotional components
                            //List<int> Finalintvalues = null;
                            //if (Session["RegMobileReg_Type"].ToInt() == 1)
                            //{
                            //    List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();
                            //    string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();
                            //    Finalintvalues = values.Select(a => a.BdlDataPkgId).Distinct().ToList();
                            //    Lstvam.DeviceType = "Seco_NoDevice";
                            //}
                            //else if (Session["RegMobileReg_Type"].ToInt() == 2)
                            //{
                            //    List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
                            //    Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
                            //}
                            //var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                            //                                 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                            //                                 select new AvailableVAS
                            //                                 {
                            //                                     ComponentName = e.ComponentName,
                            //                                     PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                            //                                     Price = e.Price,
                            //                                     Value = e.Value,
                            //                                     GroupId = e.GroupId,
                            //                                     GroupName = e.GroupName,
                            //                                     KenanCode = e.KenanCode
                            //                                 }; 
                            #endregion
                            Session["intpramotionidsval"] = respon.values[i].BdlDataPkgId;

                            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
                            {
                                dropObj.mainFlow.PromotionPackage = respon.values[i].BdlDataPkgId;
                            }
                            else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
                            {
                                dropObj.suppFlow[dropObj.suppIndex - 1].PromotionPackage = respon.values[i].BdlDataPkgId;
                            }

                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intpramotionidsval = respon.values[i].BdlDataPkgId;
                                }
                            }

                            Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
                        }

                        /*Data Contract Discount packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.DiscountDataContract_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            if (Session["RegMobileReg_Type"].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

								Finalintvalues = values.Where(mid => mid.Modelid == "0").Select(a => a.BdlDataPkgId).ToList();
								if (Finalintvalues.Count() == 0)
								{
									Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();
								}

                            }
                            else if (Session["RegMobileReg_Type"].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }
                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault
                                                             };
                            Session["intdatadiscountidsval"] = respon.values[i].BdlDataPkgId;

                            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
                            {
                                dropObj.mainFlow.DiscountPackage = respon.values[i].BdlDataPkgId;
                            }
                            else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
                            {
                                dropObj.suppFlow[dropObj.suppIndex - 1].DiscountPackage = respon.values[i].BdlDataPkgId;
                            }


                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intdatadiscountidsval = respon.values[i].BdlDataPkgId;
                                }
                            }

                            Lstvam2.PramotionalComponents.AddRange(finalfiltereddatacontracts);
                        }

                        if (respon.values[i].Plan_Type == Settings.Default.Device_Insurecomponent)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            if (Session["RegMobileReg_Type"].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session["RegMobileReg_Type"].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }
                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault
                                                             };
                            Session["intinsuranceval"] = respon.values[i].BdlDataPkgId;
                            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
                            {
                                dropObj.mainFlow.InsurancePackage = respon.values[i].BdlDataPkgId;
                            }
                            else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
                            {
                                dropObj.suppFlow[dropObj.suppIndex - 1].InsurancePackage = respon.values[i].BdlDataPkgId;
                            }

                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intinsuranceval = respon.values[i].BdlDataPkgId;
                                }
                            }

                            Lstvam2.InsuranceComponents.AddRange(finalfiltereddatacontracts);
                        }


						#region device financing
						retrieveAcctListByICResponse allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
						var dfEligibilityCheck = Session[SessionKey.DF_Eligibility.ToString()].ToBool();
						var dfException = Session["df_showOffer"].ToString2() == "show" ? true : false;

						dropObj.mainFlow.isDeviceFinancingExceptionalOrder = dfException;

                        if (dfEligibilityCheck || dfException || Roles.IsUserInRole("MREG_CUSR"))
						{
							if (respon.values[i].Plan_Type == Settings.Default.Device_Financing_Component)
							{
                                var pbpcCriteria = new PgmBdlPckComponentFind()
                                {
                                    PgmBdlPckComponent = new PgmBdlPckComponent()
                                    {
                                        LinkType = "DF"
                                    },
                                    Active = true
                                };

                                var contractPBPCs = MasterDataCache.Instance.FilterComponents(pbpcCriteria);

                                List<int> contractDFDuration = new List<int>();

                                if (contractPBPCs.Any())
                                {
                                    using (var SmartCatProxy = new SmartCatelogServiceProxy())
                                    {
                                        foreach (var contractPBPC in contractPBPCs)
                                        {
                                            if (!string.IsNullOrEmpty(contractPBPC.KenanCode))
                                            {
                                                contractDFDuration.Add(SmartCatProxy.GetContractDurationByKenanCode(contractPBPC.KenanCode));
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    contractDFDuration.Clear();
                                }
								Session["intDeviceFinancingVal"] = respon.values[i].BdlDataPkgId;
								if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
								{
									dropObj.mainFlow.DeviceFinancingPackage = respon.values[i].BdlDataPkgId;
								}
								else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
								{
									dropObj.suppFlow[dropObj.suppIndex - 1].DeviceFinancingPackage = respon.values[i].BdlDataPkgId;
								}
								string articleid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ArticleId.ToString2();
								Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

								if (Session["RegMobileReg_Type"].ToInt() == 1 && Lstvam.AvailableVASes.Any())
								{
									var deviceFinancingInfo = proxy.GetDeviceFinancingInfoByArticleID(articleid, Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));
									if (deviceFinancingInfo.Any())
									{
										var contractListDF = Lstvam.AvailableVASes.Where(x => !string.IsNullOrWhiteSpace(x.Value)).ToList();
										var upgradeFeeKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_UPGRADEFEE);
										var deviceFinancingVAS = Lstvam.AvailableVASes.Where(x => x.isHidden == false).ToList();

										if (deviceFinancingVAS.Any())
										{
											deviceFinancingVAS.FirstOrDefault(x => x.KenanCode == upgradeFeeKenanCode).Price = deviceFinancingInfo.FirstOrDefault().UpgradeFee;
											deviceFinancingVAS.FirstOrDefault(x => x.KenanCode == upgradeFeeKenanCode).oneTimeUpgrade = deviceFinancingInfo.FirstOrDefault().OneTimeUpgrade;
											Lstvam2.DeviceFinancingVAS.AddRange(deviceFinancingVAS);
											
											//var planID = proxy.PgmBdlPckComponentGet(new int[] { respon.values[i].BdlDataPkgId.ToInt() }).SingleOrDefault().KenanCode;
											string planID = Session["UOMPlanID"].ToString2();

											foreach (var df in contractListDF)
											{
												Lstvam2.ContractOffers.AddRange(WebHelper.Instance.GetContractOffersDF(new string[] { df.KenanCode }.ToList(), articleid, planID, string.Empty));
											}


                                            if (((contractDFDuration.Any() && !string.IsNullOrEmpty(Session["selectedContractLength"].ToString2())) ? contractDFDuration.Contains(Session["selectedContractLength"].ToInt()) : false) && ReferenceEquals(Session["RegMobileReg_UOMCode"], null))
                                                Session["OfferId"] = Lstvam2.ContractOffers[Lstvam2.ContractOffers.Count() - 1].Id;

                                            //if (Session["selectedContractLength"].ToInt() == 24 && ReferenceEquals(Session["RegMobileReg_UOMCode"],null))
                                            //	Session["OfferId"] = Lstvam2.ContractOffers[Lstvam2.ContractOffers.Count() - 1].Id;

											//if (Lstvam2.ContractOffers.Any())
											//    Session["OfferId"] = Lstvam2.ContractOffers[Lstvam2.ContractOffers.Count() - 1].Id;

											var dfContract = WebHelper.Instance.GetAvailablePackageComponents(respon.values[i].BdlDataPkgId.ToInt(), ref processingMsisdn, isMandatory: false, isDF: true);

											Lstvam2.ContractVASes.AddRange(dfContract.ContractVASes);
										}
									}
								}
							}
						}
						#endregion


                    }

                    //GTM e-Billing CR - Ricky - 2014.09.25
                    //To retrieve the component relation
                    Lstvam2.MutualExclusiveComponents = proxy.ComponentRelationGetMutualExclusive(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));

                }

                if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
                {
                    dropObj.mainFlow.MandatoryVAS = (String)Session["MandatoryVasIds"];
                }
                else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
                {
                    dropObj.suppFlow[dropObj.suppIndex - 1].MandatoryVAS = (String)Session["MandatoryVasIds"];
                }
                Session[SessionKey.DropFourObj.ToString()] = dropObj;

                if (_mobileRegType == "Planonly")
                {
                    Lstvam.ContractVASes.Clear();
                }

                Session["newLineVas"] = Lstvam2;

                if (Session["selectedOfferID"].ToString2() != "" || Session["selectedOfferID"].ToString2() != "")
                {
                    var check1 = (!Session["OfferId"].ToString2().IsEmpty()) ? Session["OfferId"].ToString2() : Session["selectedOfferID"].ToString2();
                    foreach (var item in Lstvam2.ContractOffers)
                    {
                        if (check1.Equals(item.Id.ToString()))
                        {
                            Session["RegMobileReg_UOMCode"] = item.UOMCode;
							dropObj.mainFlow.OfferDevicePrice = Convert.ToDouble(item.Price);//11032015 - Anthony - Fix for Device Offer Price is not sync with the offer
                            dropObj.mainFlow.UOMCode = item.UOMCode;
                        }
                    }
                }

                //24022015 - Anthony - fix for inconsistent price in order summary - Start
                Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();

                if (!Session[SessionKey.FromMNP.ToString()].ToBool() && dropObj.mainFlow.orderSummary.ModelID > 0)
                {
                    dropObj.mainFlow.orderSummary.MalayPlanAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalayPlanAdv;
                    dropObj.mainFlow.orderSummary.MalyPlanDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalyPlanDeposit;
                    dropObj.mainFlow.orderSummary.MalyDevAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalyDevAdv;
                    dropObj.mainFlow.orderSummary.MalayDevDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalayDevDeposit;
                    dropObj.mainFlow.orderSummary.OthPlanAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthPlanAdv;
                    dropObj.mainFlow.orderSummary.OthPlanDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthPlanDeposit;
                    dropObj.mainFlow.orderSummary.OthDevAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthDevAdv;
                    dropObj.mainFlow.orderSummary.OthDevDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthDevDeposit;
                }
                //24022015 - Anthony - fix for inconsistent price in order summary - End

                dropObj.mainFlow.MandatoryVAS = (String)Session["MandatoryVasIds"];

				if (dropObj.mainFlow.phone != null && !string.IsNullOrWhiteSpace(dropObj.mainFlow.phone.DeviceArticleId))
				{
					using (var catProxy = new CatalogServiceProxy())
					{
						var anyDFDeviceEligible = catProxy.GetDeviceFinancingInfoByArticleID(dropObj.mainFlow.phone.DeviceArticleId, dropObj.mainFlow.PkgPgmBdlPkgCompID);
						Lstvam2.exceptionalAvailable = anyDFDeviceEligible != null ? anyDFDeviceEligible.Any() : false;
					}
				}

                if (Lstvam2 != null)
                {
                    return View(Lstvam2);
                }
                Lstvam2 = (ValueAddedServicesVM)Session["VAS"];
                return View(Lstvam2);

            }
            else
            {
                Session[SessionKey.RedirectURL.ToString()] = "Registration/SelectVASNew";
                return RedirectToAction("IndexNew", "Home");
            }
        }



        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA")]
        public ActionResult SelectVAS(FormCollection collection, ValueAddedServicesVM vasVM)
        {
            //added by ravi on jan 03 2014 as it is giving exception
            if (vasVM.SelectedVasIDs.ToString2().Length > 0)
            {
                vasVM.SelectedVasIDs = vasVM.SelectedVasIDs.Replace("[object XMLDocument]", string.Empty);
            }
            //added by ravi on jan 03 2014 as it is giving exception ends here.

            #region fxDependency Check
            if (vasVM.TabNumber == 4)
            {
                List<FxDependChkComps> Comps = new List<FxDependChkComps>();
                using (var Proxy = new RegistrationServiceProxy())
                {
                    string strCompIds = string.Empty;
                    if (vasVM.SelectedVasIDs.ToString2() != string.Empty)
                    {
                        strCompIds = vasVM.SelectedVasIDs;
                        Session[SessionKey.SelectedComponents.ToString()] = vasVM.SelectedVasIDs;
                        Session[SessionKey.SelectedKenanCodes.ToString()] = vasVM.SelectedKenanCodes;
                    }
                    if (Session["MandatoryVasIds"].ToString2().Length > 0)
                    {
                        strCompIds = strCompIds + "," + Session["MandatoryVasIds"].ToString();
                    }
                    strCompIds += "," + Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToString2();
                    //for adding contract id
                    if (vasVM.SelectedContractID.ToString2().Length > 0)
                    {
                        strCompIds += "," + vasVM.SelectedContractID;
                    }
                    //added by Nreddy by pass the dependece check method when click on back button
                    if (collection["TabNumber"] != "2" || collection["TabNumber"] != "2")
                    {
                        Comps = Proxy.GetAllComponentsbyPlanId(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), strCompIds, "BP");
                    }
                }


                List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();

                foreach (var comp in Comps)
                {
                    Online.Registration.Web.SubscriberICService.componentList component = new componentList();
                    component.componentIdField = comp.ComponentKenanCode;
                    component.packageIdField = comp.KenanCode;
                    componentList.Add(component);
                    if (comp.DependentKenanCode != -1)
                    {
                        component = new componentList();
                        component.componentIdField = comp.DependentKenanCode.ToString();
                        component.packageIdField = comp.KenanCode;

                        componentList.Add(component);
                    }
                }
                if (componentList.Count > 0)
                {
                    // Drop 5
					retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
					string pbpcID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
					typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, pbpcID: pbpcID);

                    typeChkFxVasDependencyResponse response = _retrieveServiceInfoProxy.ChkFxVasDependency(request);
                    //Session["MandatoryVasIds"] = null;//clearing selection 
                    ValueAddedServicesVM vas = (ValueAddedServicesVM)Session["newLineVas"];
                    if (response.mxsRuleMessagesField != null && response.mxsRuleMessagesField.Count > 0)
                    {
                        foreach (string errmsg in response.mxsRuleMessagesField)
                        {
                            ModelState.AddModelError("", errmsg);
                        }
                        return View(vas);
                    }
                    else if (response.msgCodeField == "3")
                    {
                        ModelState.AddModelError("", "OPF system error - Unable to fetch data.");   // Need to change msg
                        return View(vas);
                    }
                    else if (response.msgCodeField != "0")
                    {
                        ModelState.AddModelError("", "Selected components are not available.");   // Need to change msg
                        return View(vas);
                    }
                }
            }
            #endregion

            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = vasVM.TabNumber;
            Session[SessionKey.VasGroupIds.ToString()] = vasVM.hdnGroupIds;
            var selectedVasIDs = vasVM.SelectedVasIDs;

            if (vasVM.ExtraTenMobileNumbers.ToString2().Length > 0)
            {
                Session[SessionKey.ExtratenMobilenumbers.ToString()] = vasVM.ExtraTenMobileNumbers;
            }
            else
            {
                Session[SessionKey.ExtratenMobilenumbers.ToString()] = null;
            }

            if (vasVM.SelectedVasIDs != null)
            {
                if (Session["RegMobileReg_DataPkgID"] != null && !Regex.IsMatch(vasVM.SelectedVasIDs, @"^,+$") && vasVM.SelectedVasIDs != "[object XMLDocument]")
                {
                    if (selectedVasIDs != null)
                    {
                        Session["RegMobileReg_DataPkgID"] = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
                    }
                }
            }
            else
            {
                if (!ReferenceEquals(Session["RegMobileReg_DataPkgID"], null))
                {
                    Session.Contents.Remove("RegMobileReg_DataPkgID");
                }
                if (!ReferenceEquals(Session[SessionKey.RegMobileReg_VasIDs.ToString()], null))
                {
                    Session.Contents.Remove("RegMobileReg_VasIDs");
                }
                Session[SessionKey.SelectedComponents.ToString()] = null;
            }

            try
            {
                if (vasVM.TabNumber == (int)MobileRegistrationSteps.MobileNo && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) || !string.IsNullOrEmpty(vasVM.SelectedContractID)))
                {
                    if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                    {
                        //Contract check for existing users
                        //Retrieve data from session                         
                        if (!string.IsNullOrEmpty(Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()))
                            selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()), "");

                        selectedVasIDs += vasVM.SelectedContractID + ",";
                    }

                    vasVM.SelectedVasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
                    Session[SessionKey.RegMobileReg_VasIDs.ToString()] = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;

                    Session[SessionKey.RegMobileReg_ContractID.ToString()] = vasVM.SelectedContractID;

                }
                using (var proxy = new UserServiceProxy())
                {
                    proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Select VAS", Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                }
            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            if (ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) || (vasVM.TabNumber == (int)MobileRegistrationSteps.Plan))
            {
                if (!string.IsNullOrEmpty(Session[SessionKey.MobileRegType.ToString()].ToString()))
                {
                    string MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();
                    if (MobileRegType == "Planonly")
                    {
                        return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber), new { type = 2 });
                    }
                    return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber));
                }
                else
                {
                    return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber));
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                {
                    if (!string.IsNullOrEmpty(Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()))
                        selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()), "");

                    selectedVasIDs += vasVM.SelectedContractID + ",";
                }

                vasVM.SelectedVasIDs = selectedVasIDs;

                Session[SessionKey.RegMobileReg_VasIDs.ToString()] = selectedVasIDs;
                Session[SessionKey.RegMobileReg_ContractID.ToString()] = vasVM.SelectedContractID;
                return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber + 1));
            }

        }




        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA")]
        public ActionResult SelectVASNew(FormCollection collection, ValueAddedServicesVM vasVM)
        {
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

            //added by ravi on jan 03 2014 as it is giving exception
            if (vasVM.SelectedVasIDs.ToString2().Length > 0)
            {
                vasVM.SelectedVasIDs = vasVM.SelectedVasIDs.Replace("[object XMLDocument]", string.Empty);
            }
            //added by ravi on jan 03 2014 as it is giving exception ends here.
			string componentToCheckWithMISM = string.Empty;
           
			if (collection["df_showOffer"].ToString2() == "show")
			{
				Session["df_showOffer"] = collection["df_showOffer"].ToString2();
                Session["DF_Eligibility"] = true;
				return RedirectToAction("SelectVASNew");
			}
			// DF
			if (dropObj.mainFlow.phone != null)
			{
				string df_articleID = dropObj.mainFlow.phone.DeviceArticleId;
				int df_pbpcID = dropObj.mainFlow.PkgPgmBdlPkgCompID;
				vasVM = WebHelper.Instance.autoAddWaiverComponent(vasVM, df_articleID, df_pbpcID, "DF");
			}
			

            #region fxDependency Check
            if (vasVM.TabNumber == 4)
            {
                Session["OfferId"] = vasVM.OfferId.ToString2();
                Session["RegMobileReg_MainDevicePrice"] = vasVM.price;
                Session["RegMobileReg_OfferDevicePrice"] = vasVM.price;//Up to here
                Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID;

                string currVasIds = collection["currVasIds"];

                using (var proxy = new CatalogServiceProxy())
                {
                    List<int> lstContracts = WebHelper.Instance.CommaSplitToList(Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2());
                    Session[SessionKey.RegMobileReg_DataplanID.ToString()] = proxy.GettllnkContractDataPlan(lstContracts);
                    dropObj.mainFlow.SelectedDataPlanID = (int[])Session[SessionKey.RegMobileReg_DataplanID.ToString()];
                }

                
                //20141205 - Refresh the contract details here since the contractId is moved here - Habs - start
                OrderSummaryVM orderVM = dropObj.mainFlow.orderSummary;
                PersonalDetailsVM personalDetailsVM = dropObj.personalInformation;

                orderVM = WebHelper.Instance.ConstructPrimaryContractDetails(orderVM, personalDetailsVM, Session["RegMobileReg_ContractID"].ToString2(), Session["RegMobileReg_DataPkgID"].ToString2(), Session["MobileRegType"].ToString2());
                dropObj.mainFlow.orderSummary = orderVM;
                //20141205 - Refresh the contract details here since the contractId is moved here - Habs - end

                List<FxDependChkComps> Comps = new List<FxDependChkComps>();
                using (var Proxy = new RegistrationServiceProxy())
                {
                    string strCompIds = string.Empty;
                    if (vasVM.SelectedVasIDs.ToString2() != string.Empty)
                    {
                        strCompIds = vasVM.SelectedVasIDs;
                        Session[SessionKey.SelectedComponents.ToString()] = vasVM.SelectedVasIDs;
                        Session[SessionKey.SelectedKenanCodes.ToString()] = vasVM.SelectedKenanCodes;
                    }
                    if (Session["MandatoryVasIds"].ToString2().Length > 0)
                    {
                        strCompIds = strCompIds + "," + Session["MandatoryVasIds"].ToString();
                    }
                    strCompIds += "," + Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToString2();
                    //for adding contract id
                    if (vasVM.SelectedContractID.ToString2().Length > 0)
                    {
                        strCompIds += "," + vasVM.SelectedContractID;
                    }
                    //added by Nreddy by pass the dependece check method when click on back button
                    if (collection["TabNumber"] != "2" || collection["TabNumber"] != "2")
                    {
                        Comps = Proxy.GetAllComponentsbyPlanId(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), strCompIds, "BP");
                    }
                }


                List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();

				dropObj.mainFlow.isDeviceFinancingOrder = WebHelper.Instance.determineDeviceFinancingOrder(Comps);

				if (dropObj.mainFlow.isDeviceFinancingOrder)
				{
					// dropObj.DFcustomerEligibility <<< customer eligibility
					dropObj.mainFlow.isDeviceFinancingExceptionalOrder = !dropObj.DFcustomerEligibility ? true : false;
				}
                
                if (dropObj.mainFlow.isAccFinancingOrder)
                {
                    var selectedVASIDs = vasVM.SelectedVasIDs;
                    var selectedKenanCodes = vasVM.SelectedKenanCodes;
                    WebHelper.Instance.AutoAddAccessoryFinancingComponents(ref Comps, ref selectedVASIDs, ref selectedKenanCodes);

                    vasVM.SelectedVasIDs = selectedVASIDs;
                    vasVM.SelectedKenanCodes = selectedKenanCodes;
                    Session[SessionKey.SelectedComponents.ToString()] = selectedVASIDs;
                    Session[SessionKey.SelectedKenanCodes.ToString()] = selectedKenanCodes;
                }

                foreach (var comp in Comps)
                {
					componentToCheckWithMISM += comp.ComponentKenanCode + ",";
                    Online.Registration.Web.SubscriberICService.componentList component = new componentList();
                    component.componentIdField = comp.ComponentKenanCode;
                    component.packageIdField = comp.KenanCode;
                    componentList.Add(component);
                    if (comp.DependentKenanCode != -1)
                    {
                        component = new componentList();
                        component.componentIdField = comp.DependentKenanCode.ToString();
                        component.packageIdField = comp.KenanCode;

                        componentList.Add(component);
                    }
                }
                if (componentList.Count > 0)
                {
					// Drop 5
					retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
					string pbpcID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
					typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, pbpcID: pbpcID);

                    typeChkFxVasDependencyResponse response = _retrieveServiceInfoProxy.ChkFxVasDependency(request);
                    //Session["MandatoryVasIds"] = null;//clearing selection 
                    ValueAddedServicesVM vas = (ValueAddedServicesVM)Session["newLineVas"];
                    //Evan - assign GST Notif start
                    ViewBag.GSTMark = Settings.Default.GSTMark;
                    List<String> LabelReplace = new List<String>();
                    LabelReplace.Add("GSTMark");
                    ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
                    //Evan - assign GST Notif end
                    if (response.mxsRuleMessagesField != null && response.mxsRuleMessagesField.Count > 0)
                    {
                        string selectedComp = (string)Session[SessionKey.SelectedComponents.ToString()];

                        foreach (string errmsg in response.mxsRuleMessagesField)
                        {
                            ModelState.AddModelError("", errmsg);
                        }
                        return View(vas);
                    }
                    else if (response.msgCodeField == "3")
                    {
                        string selectedComp = (string)Session[SessionKey.SelectedComponents.ToString()];

                        //Ricky: This is not required - don't cleanup the previous selected VAS
                        /*
                        if (currVasIds != null)
                        {
                            string[] vasIds = currVasIds.Split(',');
                            foreach (string vasId in vasIds)
                            {
                                if (!string.IsNullOrEmpty(vasId) && selectedComp.Contains(vasId))
                                {
                                    selectedComp = selectedComp.Replace(vasId + ",", "");
                                }
                            }
                            Session[SessionKey.SelectedComponents.ToString()] = selectedComp;
                        }
                        */

                        ModelState.AddModelError("", "OPF system error - Unable to fetch data.");   // Need to change msg
                        return View(vas);
                    }
                    else if (response.msgCodeField != "0")
                    {
                        string selectedComp = (string)Session[SessionKey.SelectedComponents.ToString()];

                        //Ricky: This is not required - don't cleanup the previous selected VAS
                        /*
                        if (currVasIds != null)
                        {
                            string[] vasIds = currVasIds.Split(',');
                            foreach (string vasId in vasIds)
                            {
                                if (!string.IsNullOrEmpty(vasId) && selectedComp.Contains(vasId))
                                {
                                    selectedComp = selectedComp.Replace(vasId + ",", "");
                                }
                            }
                            Session[SessionKey.SelectedComponents.ToString()] = selectedComp;
                        }
                        */

                        ModelState.AddModelError("", "Selected components are not available.");   // Need to change msg
                        return View(vas);
                    }

					string errorDependent = WebHelper.Instance.checkVASDependentDF(Comps);
					if (!string.IsNullOrWhiteSpace(errorDependent))
					{
						ModelState.AddModelError("", errorDependent);
						return View(vas);
					}

                    // iphone 6 and iphone 6 plus data booster - Start
                    if (null != Session["ArticleId"])
                    {
                        string ArticleID = Session[SessionKey.ArticleId.ToString()].ToString();
                        List<int> selectedComponent = new List<int>();
                        foreach (var component in vasVM.SelectedVasIDs.ToString2().Split(','))
                        {
                            if (!selectedComponent.Contains(component.ToInt()) && (component != null || component.ToInt() == 0))
                                selectedComponent.Add(component.ToInt());
                        }

                        if (null != selectedComponent)
                        {
                            int consist = Util.checkDataBooster(selectedComponent, Session["UOMPlanID"].ToString(), ArticleID);

                            if (consist == 3)
                            {
                                ModelState.AddModelError(string.Empty, ConfigurationManager.AppSettings["maxisDataBoosterError"].ToString());
                                return View(vas);
                            }
                        }

                    }
                    // iphone 6 and iphone 6 plus data booster - End
                }
            }
            #endregion

            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = vasVM.TabNumber;
            Session[SessionKey.VasGroupIds.ToString()] = vasVM.hdnGroupIds;

            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = vasVM.price;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = vasVM.price;
            Session[SessionKey.RegMobileReg_UOMCode.ToString()] = vasVM.uomCode;
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = vasVM.TabNumber;

            var selectedVasIDs = vasVM.SelectedVasIDs;

            if (vasVM.ExtraTenMobileNumbers.ToString2().Length > 0)
            {
                Session[SessionKey.ExtratenMobilenumbers.ToString()] = vasVM.ExtraTenMobileNumbers;
            }
            else
            {
                Session[SessionKey.ExtratenMobilenumbers.ToString()] = null;
            }

            if (vasVM.SelectedVasIDs != null)
            {
                if (Session["RegMobileReg_DataPkgID"] != null && !Regex.IsMatch(vasVM.SelectedVasIDs, @"^,+$") && vasVM.SelectedVasIDs != "[object XMLDocument]")
                {
                    if (selectedVasIDs != null)
                    {
                        Session["RegMobileReg_DataPkgID"] = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
                    }
                }
            }
            else
            {
                if (!ReferenceEquals(Session["RegMobileReg_DataPkgID"], null))
                {
                    Session.Contents.Remove("RegMobileReg_DataPkgID");
                }
                if (!ReferenceEquals(Session[SessionKey.RegMobileReg_VasIDs.ToString()], null))
                {
                    Session.Contents.Remove("RegMobileReg_VasIDs");
                }
                Session[SessionKey.SelectedComponents.ToString()] = null;
            }

            try
            {
                if (vasVM.TabNumber == (int)MobileRegistrationSteps.MobileNo && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) || !string.IsNullOrEmpty(vasVM.SelectedContractID)))
                {
                    if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                    {
                        //Contract check for existing users
                        //Retrieve data from session                         
                        if (!string.IsNullOrEmpty(Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()))
                            selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()), "");


                        if (selectedVasIDs != "")
                        {
                            if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
                            {
                                selectedVasIDs += "," + vasVM.SelectedContractID + ",";
                            }
                            else
                            {
                                selectedVasIDs += vasVM.SelectedContractID + ",";
                            }
                        }
                        else
                        {
                            selectedVasIDs += vasVM.SelectedContractID + ",";
                        }

                        //add monthly fee component for DF
                        if (dropObj.mainFlow.isDeviceFinancingOrder)
                        {
                            var vasIDList = new List<int>();

                            if (vasVM.SelectedContractID.Contains(','))
                            {
                                try
                                {
                                    vasIDList.AddRange(vasVM.SelectedContractID.Split(',').Select(int.Parse).ToList());
                                }
                                catch (Exception ex)
                                {
                                    vasIDList = new List<int>();
                                }
                            }
                            else
                            {
                                vasIDList.Add(vasVM.SelectedContractID.ToInt());
                            }

                            if (vasIDList.Count > 0)
                            {
                                var dependentComponentIDs = WebHelper.Instance.GetDepenedencyComponents(vasIDList);

                                if (dependentComponentIDs != null && dependentComponentIDs.Count > 0)
                                {
                                    var dependentCompString = string.Join(",", dependentComponentIDs);

                                    if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
                                    {
                                        selectedVasIDs += "," + dependentCompString + ",";
                                    }
                                    else
                                    {
                                        selectedVasIDs += dependentCompString + ",";
                                    }
                                }
                            }
                        }
                    }

                    vasVM.SelectedVasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
                    Session[SessionKey.RegMobileReg_VasIDs.ToString()] = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;

                    Session[SessionKey.RegMobileReg_ContractID.ToString()] = vasVM.SelectedContractID.ToString2();

                    //Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID.ToString();
                }
                using (var proxy = new UserServiceProxy())
                {
                    //TODO //TODO Session[SessionKey.KenanACNumber.ToString()].ToString()
                    proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Select VAS", "", "");
                }
            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
			
			if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
			{
				dropObj.mainFlow.MandatoryVAS = (String)Session["MandatoryVasIds"];
                dropObj.mainFlow.OfferName = collection["OfferName"];
			}
			else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
			{
				dropObj.suppFlow[dropObj.suppIndex - 1].MandatoryVAS = (String)Session["MandatoryVasIds"];
                dropObj.suppFlow[dropObj.suppIndex - 1].OfferName = collection["OfferName"];
			}

			//df
			DropFourHelpers.UpdateVAS(dropObj, vasVM);
			Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();
			Session["RegMobileReg_VasNames"] = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).VasVM.VASesName;

			if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
			{
				dropObj.mainFlow.MandatoryVAS = (String)Session["MandatoryVasIds"];
			}
			else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
			{
				dropObj.suppFlow[dropObj.suppIndex - 1].MandatoryVAS = (String)Session["MandatoryVasIds"];
			}

			// this is for checking 2GB component for MISM eligibility, used in RegistrationController as well
			Util.check2GBComponent("", componentToCheckWithMISM);

            DropFourHelpers.UpdateVAS(dropObj, vasVM);

            //14012015 - Anthony - GST Trade Up - Start
            if ((!ReferenceEquals(Session["FromMNP"], null) || ReferenceEquals(Session["FromMNP"], null)) && !(Session["FromMNP"]).ToBool())
            {
                Session[SessionKey.TradeUpAmount.ToString()] = vasVM.hdnTradeUpAmount;

                if (!string.IsNullOrEmpty(vasVM.hdnTradeUpAmount))
                {
                    //ValueAddedServicesVM lstOffers = (ValueAddedServicesVM)Session["VAS"];
                    ValueAddedServicesVM lstOffers = (ValueAddedServicesVM)Session["newLineVas"];
                    var offerIDSelected = vasVM.hdnTradeUpAmount.Split('_')[1].ToInt();
                    var tradeUpAmountSelected = vasVM.hdnTradeUpAmount.Split('_')[2].ToInt();//negative value
                    var deviceOfferPrice = vasVM.price.ToInt();
                    var tradeUpList = new TradeUpComponent();

                    foreach (var tradeUpAmountList in lstOffers.TradeUpList)
                    {
                        if (tradeUpAmountList.OfferId == offerIDSelected && tradeUpAmountList.Price == tradeUpAmountSelected)
                        {
                            tradeUpList.OfferId = offerIDSelected;
                            tradeUpList.Name = "RM " + tradeUpAmountSelected * -1;
                            tradeUpList.Price = tradeUpAmountSelected;

                            Session[SessionKey.TradeUpAmount.ToString()] = vasVM.hdnTradeUpAmount;

                            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
                            {
                                dropObj.mainFlow.orderSummary.SelectedTradeUpComp = tradeUpList;
                                dropObj.mainFlow.isTradeUp = true;
                            }
                            else if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
                            {
                                dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.SelectedTradeUpComp = tradeUpList;
                                dropObj.suppFlow[dropObj.suppIndex - 1].isTradeUp = true;
                            }
                            break;
                        }
                        else
                        {
                            Session[SessionKey.TradeUpAmount.ToString()] = 0;
                        }
                    }
                }
            }
            //14012015 - Anthony - GST Trade Up - End

            Session[SessionKey.DropFourObj.ToString()] = dropObj;

            if (ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) || (vasVM.TabNumber == (int)MobileRegistrationSteps.Plan))
            {
                if (!string.IsNullOrEmpty(Session[SessionKey.MobileRegType.ToString()].ToString()))
                {
                    string MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();
                    if (MobileRegType == "Planonly")
                    {
                        return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber), new { type = 2 });
                    }
                    return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber));
                }
                else
                {
                    return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber));
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                {
                    if (!string.IsNullOrEmpty(Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()))
                        selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2()), "");

                    selectedVasIDs += vasVM.SelectedContractID + ",";
                }

                vasVM.SelectedVasIDs = selectedVasIDs;

                Session[SessionKey.RegMobileReg_VasIDs.ToString()] = selectedVasIDs;
                Session[SessionKey.RegMobileReg_ContractID.ToString()] = vasVM.SelectedContractID;
                return RedirectToAction(WebHelper.Instance.GetRegActionStep(vasVM.TabNumber + 1));
            }

        }



        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA")]
        public ActionResult PersonalDetailsNew()
        {
            SetRegStepNo(MobileRegistrationSteps.PersonalDetails);
            //Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();//25022015 - Anthony - To fix the Device and Plan in MainFlow mixed up with SuppFlow
            ViewBag.BackButton = GetPersonalDetailsBackButton();
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var idCardTypeID = Session[SessionKey.RegMobileReg_IDCardType.ToString()] != null ? Session[SessionKey.RegMobileReg_IDCardType.ToString()] : null;
            var personalDetailsVM = new PersonalDetailsVM();
            if ((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"] != null)
            {
                personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            }

            personalDetailsVM.Customer.IDCardTypeID = idCardTypeID.ToInt();
            personalDetailsVM.Customer.IDCardNo = Session[SessionKey.RegMobileReg_IDCardNo.ToString()] == null ? "" : Session[SessionKey.RegMobileReg_IDCardNo.ToString()].ToString2();

            //GTM e-Billing CR - Ricky - 2014.09.25 - Start
            string SelectAccountDtls = Session[SessionKey.AccExternalID.ToString()].ToString2();
            var availableMsisdn = WebHelper.Instance.GetAvailableMsisdnForSmsNotification(SelectAccountDtls);
            var billDeliveryVM = new BillDeliveryVM();
            billDeliveryVM.AcctExtId = SelectAccountDtls;
			//Session["SelectedAccountNo"] = SelectAccountDtls;
			//if (personalDetailsVM.RegID == 0)
			//{
			//    personalDetailsVM.KenanAccountNo = Session[SessionKey.AccExternalID.ToString()].ToString2();
			//}
            if (string.IsNullOrEmpty(Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2()))
            {
                //set default value for first landing, which is to be retrieved from EBPS
                WebHelper.Instance.BillDeliveryRetrieve(billDeliveryVM);
                if (!billDeliveryVM.Result)
                {
                    TempData["ErrorMessage"] = string.Format(ConfigurationManager.AppSettings["BillDelivery_ErrorMessage_EBPSNotification"].ToString() + billDeliveryVM.ErrorMessage);
                }
                personalDetailsVM.AvailableMsisdn = availableMsisdn;
                personalDetailsVM.EmailBillSubscription = billDeliveryVM.BillDeliveryDetails.EmailBillSubscription;
                personalDetailsVM.EmailAddress = billDeliveryVM.BillDeliveryDetails.EmailAddress;
                personalDetailsVM.SmsAlertFlag = billDeliveryVM.BillDeliveryDetails.SmsAlertFlag;
                personalDetailsVM.SmsNo = billDeliveryVM.BillDeliveryDetails.SmsNo;
            }
            else
            {
                //retrieve previous value from session if user press the back button
                personalDetailsVM.AvailableMsisdn = availableMsisdn;
                personalDetailsVM.EmailBillSubscription = Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2();
                personalDetailsVM.EmailAddress = Session[SessionKey.billDelivery_emailAddress.ToString()].ToString2();
                personalDetailsVM.SmsAlertFlag = Session[SessionKey.billDelivery_smsAlertFlag.ToString()].ToString2();
                personalDetailsVM.SmsNo = Session[SessionKey.billDelivery_smsNo.ToString()].ToString2();
            }
            //GTM e-Billing CR - Ricky - 2014.09.25 - End

            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

            //25022015 - Anthony - To fix the Device and Plan in MainFlow mixed up with SuppFlow - Start
            if (dropObj == null)
            {
                Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();
            }
            else
            {
                if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
                {
                    Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(true);
                }
                else
                {
                    Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();
                }
            }
            //25022015 - Anthony - To fix the Device and Plan in MainFlow mixed up with SuppFlow - End
            if (dropObj.msimIndex > 0)
            {
                PackageVM pVM = WebHelper.Instance.GetAvailablePackagesCache(iSecondary: true);
                List<AvailablePackage> MandatoryPackages = pVM.AvailablePackages;
                string planID = string.Empty;
                int packageCompID = 0;

                planID = MandatoryPackages.Where(x => x.KenanCode.Equals(Properties.Settings.Default.MISMDefaultPlan)).SingleOrDefault().PgmBdlPkgCompID.ToString2();
                //packageCompID = dropObj.mainFlow.orderSummary.SelectedPgmBdlPkgCompID;
                packageCompID = planID.ToInt();

            }

            if (Convert.ToBoolean(Session["FromMNP"]))
            {
                MobileNoVM mobileNoVM = new MobileNoVM();
                mobileNoVM.SelectedMobileNo = Session[SessionKey.RegMobileReg_MSISDN.ToString()].ToString2();
                mobileNoVM.SIMCardType = "Normal";
                dropObj.mainFlow.mobile = mobileNoVM;

            }

			if (Roles.IsUserInRole("MREG_HQ") || Roles.IsUserInRole("MREG_DAB") || Roles.IsUserInRole("MREG_SV"))
			{
				personalDetailsVM.Supervisor = true;
			}
           
            if (!string.IsNullOrEmpty(dropObj.mainFlow.mobile.SelectedMobileNo))
            {
                personalDetailsVM.Customer.ContactNo = dropObj.mainFlow.mobile.SelectedMobileNo;
            }
			// to default all personal details from response
			personalDetailsVM = Util.FormPersonalDetailsandCustomer(personalDetailsVM);
			personalDetailsVM = Util.CheckDisablePersonalDetailsPage(personalDetailsVM);
            
            //Drop 5 B18 - Ricky - 2015.05.12 - Populate the default value for Outlet/Sales Code
            WebHelper.Instance.PopulateDealerInfo(personalDetailsVM);

            retrieveAcctListByICResponse allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
            personalDetailsVM.Liberlization_Status = WebHelper.Instance.determineRiskCategory(allCustomerDetails);

            //Drop 5 - Karyne - 2015.09.08 - Validate BRE on page load - Start
            string JustificationMessage = string.Empty;
            string HardStopMessage = string.Empty;
            string ApprovalMessage = string.Empty;
            bool isBREFail = false;
            WebHelper.Instance.BRECheckMessage(out HardStopMessage, out JustificationMessage, out ApprovalMessage, out isBREFail);
            //Drop 5 - Karyne - 2015.09.08 - Validate BRE on page load - End

            return View(personalDetailsVM);
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA")]
        public ActionResult PersonalDetailsNew(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

            // to default all personal details from response -[w.loon] BUG #1525 retain new input values and disable pre-populated fields
            Util.FormPersonalDetailsandCustomer(personalDetailsVM);
            Util.CheckDisablePersonalDetailsPage(personalDetailsVM);
            if (personalDetailsVM.Customer.AlternateContactNo == "")
                personalDetailsVM.Customer.AlternateContactNo = collection["Customer.AlternateContactNo"];
            if (personalDetailsVM.Customer.EmailAddr == "")
                personalDetailsVM.Customer.EmailAddr = collection["Customer.EmailAddr"];
            if (personalDetailsVM.Customer.RaceID == 0)
                personalDetailsVM.Customer.RaceID = collection["Customer.RaceID"].ToInt();

            //tempstore card number
            dropObj.tempCardNumber = personalDetailsVM.Customer.CardNo;
            try
            {	
                if (((Convert.ToString(Session[SessionKey.SimType.ToString()]) != "MISM") && personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.Summary) || ((Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM") && personalDetailsVM.TabNumber == (int)RegistrationSteps.Summary))
                {
                    personalDetailsVM.Customer.IDCardTypeID = personalDetailsVM.InputIDCardTypeID.ToInt();
                    personalDetailsVM.Customer.IDCardNo = personalDetailsVM.InputIDCardNo;
                    //personalDetailsVM.Customer.ContactNo = personalDetailsVM.InputContactNo;//Anthony-[#2004]

                    personalDetailsVM.Customer.NationalityID = personalDetailsVM.InputNationalityID;
                    Session[SessionKey.ExtratenMobilenumbers.ToString()] = personalDetailsVM.ExtraTenMobileNumbers;
                    Session[SessionKey.RegMobileReg_TabIndex.ToString()] = personalDetailsVM.TabNumber;
					personalDetailsVM.idDocumentFront = collection["frontPhoto"];
					personalDetailsVM.frontDocumentName = collection["frontPhotoFileName"];
					personalDetailsVM.idDocumentBack = collection["backPhoto"];
					personalDetailsVM.backDocumentName = collection["backPhotoFileName"];
					personalDetailsVM.otherDocuments = collection["otherPhoto"];
					personalDetailsVM.othDocumentName = collection["otherFileName"];
					personalDetailsVM.thirdPartyFile = collection["thirdPartyPhoto"];
					personalDetailsVM.thirdPartyDocumentName = collection["thirdPartyFileName"];

					//clearing session if filename not exist 
					if (string.IsNullOrEmpty(personalDetailsVM.frontDocumentName)) Session["frontImage"] = null;
					if (string.IsNullOrEmpty(personalDetailsVM.backDocumentName)) Session["backImage"] = null;
					if (string.IsNullOrEmpty(personalDetailsVM.othDocumentName)) Session["otherImage"] = null;
					if (string.IsNullOrEmpty(personalDetailsVM.thirdPartyDocumentName)) Session["thirdImage"] = null;

					string SelectAccountDtls = Session[SessionKey.AccExternalID.ToString()].ToString2();
					personalDetailsVM.AvailableMsisdn = WebHelper.Instance.GetAvailableMsisdnForSmsNotification(SelectAccountDtls);

					if (!ReferenceEquals(Session["CustPersonalInfo"], null))
					{
						var customerPersonalInfo = (CustomizedCustomer)Session["CustPersonalInfo"];

						personalDetailsVM.Customer.EmailAddr = !string.IsNullOrEmpty(personalDetailsVM.Customer.EmailAddr) ? personalDetailsVM.Customer.EmailAddr :
																	customerPersonalInfo.EmailAddrs;
						personalDetailsVM.Customer.ContactNo = Util.FormatContactNumber(customerPersonalInfo.Cbr);
                        personalDetailsVM.Customer.AlternateContactNo = !string.IsNullOrEmpty(personalDetailsVM.Customer.AlternateContactNo) ? personalDetailsVM.Customer.AlternateContactNo :
                                                                    customerPersonalInfo.AlternateContactNo;
					}

					// MyMaxis app survey 
					personalDetailsVM.SurveyFeedBack = collection["surveyFeedback"].ToString2();
                    //personalDetailsVM.SurveyAnswer = !string.IsNullOrWhiteSpace(personalDetailsVM.SurveyFeedBack) ? "Yes" : "No";
                    personalDetailsVM.SurveyAnswerRate = collection["surveyAnswerRate"].ToString2();
                    personalDetailsVM.SurveyAnswer = collection["surveyAnswer"].ToString2();

                    personalDetailsVM.SingleSignOnValue = collection["SingleSignOnValue"].ToString2();
                    personalDetailsVM.SingleSignOnEmailAddress = collection["SingleSignOnEmailAddress"].ToString2();
                    
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;
                    
                    #region RST BRE CR - comment out - Moved to webhelper breValidate region Contract Check
                    /*
                    if (ReferenceEquals(Session["FromMNP"], null) || (!ReferenceEquals(Session["FromMNP"], null) && !Convert.ToBoolean(Session["FromMNP"])))
					{
						**************** Contract Check ***********************
						dropObj = dropObj.getExistingContractCount(dropObj);
						**************** Contract Check ***********************
                        
						// if contract check qty is exceed the maximum number allowed then do hardstop
						// do hardstop for MEPS & justification for MC
						if ((dropObj.additionalContract + dropObj.existingContract) > Convert.ToInt32(ConfigurationManager.AppSettings["MaxOrderTotalContract"]))
						{
							if (Util.SessionAccess.User.isDealer)
							{
								TempData["ErrorMessage_2"] = ConfigurationManager.AppSettings["ContractCheckHSMsg"].ToString2();
								return View(personalDetailsVM);
							}
							else
							{
								if (string.IsNullOrEmpty(validationResult))
								{
									validationResult = "Contract Check";
								}
								else
								{
									if (!validationResult.Contains("Contract Check"))
									{
										validationResult = validationResult + " , " + "Contract Check";
									}
								}
							}
						}
                    }*/
                    #endregion

                    //Check the business validations sharvin
                    WebHelper.Instance.BusinessValidationChecks(personalDetailsVM, this.GetType());
                    var validationResult = WebHelper.Instance.breValidate_list();

                    string JustificationMessage = string.Empty;
                    string HardStopMessage = string.Empty;
                    foreach (var _result in validationResult)
                    {
                        JustificationMessage = (_result.Action == "J") ?
                            string.IsNullOrEmpty(JustificationMessage) ? _result.Code : JustificationMessage + ", " + _result.Code :
                            JustificationMessage;

                        if (_result.Action == "HS")
                        {
                            switch (_result.Code)
                            {
                                case "Contract Check":
                                    HardStopMessage = string.IsNullOrEmpty(HardStopMessage) ? ErrorMsg.Registration.ContractFail : HardStopMessage + ", " + ErrorMsg.Registration.ContractFail;
                                    break;
                                case "Age Check":
                                    HardStopMessage = string.IsNullOrEmpty(HardStopMessage) ? ErrorMsg.Registration.AgeFail : HardStopMessage + ", " + ErrorMsg.Registration.AgeFail;
                                    break;
                                case "DDMF Check":
                                    HardStopMessage = string.IsNullOrEmpty(HardStopMessage) ? ErrorMsg.Registration.DDMFFail : HardStopMessage + ", " + ErrorMsg.Registration.DDMFFail;
                                    break;
                                case "Address Check":
                                    HardStopMessage = string.IsNullOrEmpty(HardStopMessage) ? ErrorMsg.Registration.AddressFail : HardStopMessage + ", " + ErrorMsg.Registration.AddressFail;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(HardStopMessage))
                    {
                        TempData["ErrorMessage"] = HardStopMessage;
                        return View(personalDetailsVM);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(JustificationMessage) && (string.IsNullOrEmpty(personalDetailsVM.Justification)))
                        {
                            TempData["ErrorMessage"] = "Please Enter Justification for - " + JustificationMessage;
                            return View(personalDetailsVM);
                        }
                    }

                    #region TAC Validation
                    // 20151117 - [w.loon] - TAC Validation - start
                    if (WebHelper.Instance.IsTacRequired() && !Online.Registration.Web.Properties.Settings.Default.DevelopmentModeOnForTacCheck && !Session["IsTacExceptional"].ToBool())
                    {
                        string submittedTac = collection["tacInput"].ToString2();
                        string EmptyTacError = "Please enter TAC.";
                        string ExpiredTacError = "TAC has expired. Please request a new TAC.";
                        string TacNotRequestedError = "TAC has not been requested. Please request a TAC.";
                        string InvalidTacError = "TAC is invalid. Please enter a valid TAC.";

                        // TempData["ErrorMessage"] is for New Layout (_PersonalDetailsNewPV.cshtml); ModelState.AddModelError() is for Old Layout (PersonalDetails.cshtml)
                        if (submittedTac == "" && collection["tacRequested"].ToString2() == "true")
                        {
                            TempData["ErrorMessage"] = EmptyTacError;
                            ModelState.AddModelError(string.Empty, EmptyTacError);
                            return View(personalDetailsVM);
                        }
                        else
                        {
                            HttpCookie tacCookie = Request.Cookies.Get("lastTac");
                            HttpCookie tacCookieDt = Request.Cookies.Get("lastTacRequestDt");
                            if (tacCookie == null && tacCookieDt == null)
                            {
                                if (collection["tacRequested"].ToString2() == "true")
                                {
                                    TempData["ErrorMessage"] = ExpiredTacError;
                                    ModelState.AddModelError(string.Empty, ExpiredTacError);
                                }
                                else
                                {
                                    TempData["ErrorMessage"] = TacNotRequestedError;
                                    ModelState.AddModelError(string.Empty, TacNotRequestedError);
                                }
                                return View(personalDetailsVM);
                            }
                            else
                            {
                                if (submittedTac != tacCookie.Value.ToString())
                                {
                                    TempData["ErrorMessage"] = InvalidTacError;
                                    ModelState.AddModelError(string.Empty, InvalidTacError);
                                    return View(personalDetailsVM);
                                }
                                else
                                {
                                    // clear TAC cookies
                                    tacCookie.Expires = DateTime.Now.AddDays(-1);
                                    tacCookieDt.Expires = DateTime.Now.AddDays(-1);
                                    Response.Cookies.Add(tacCookie);
                                    Response.Cookies.Add(tacCookieDt);
                                }
                            }
                        }
                    }
                    // 20151117 - [w.loon] - TAC Validation - end
                    #endregion

                    //drop 5
                    if (!string.IsNullOrEmpty(HardStopMessage) || !string.IsNullOrEmpty(JustificationMessage))
                    {
                        personalDetailsVM.isBREFail = true;
                    }
                    string postcode = personalDetailsVM.Address.Postcode;
                    var result = WebHelper.Instance.checkStateCityByPostcode(postcode);
                    if (!ReferenceEquals(result, null))
                    {
                        if (!string.IsNullOrEmpty(result.ErrorMessage))
                        {
                            TempData["ErrorMessage"] = result.ErrorMessage;
                            return View(personalDetailsVM);
                        }
                    }

                    #region comment out - breCheck hardstop/justy logic
                    /*
					if (!string.IsNullOrEmpty(validationResult))
					{
						//ModelState.AddModelError(string.Empty, validationResult);

						//Gregory PN 2546 add conditional for justification not null
						if (string.IsNullOrEmpty(personalDetailsVM.Justification) || personalDetailsVM.Justification.Trim().Length == 0)
						{
							TempData["ErrorMessage"] = validationResult;
							return View(personalDetailsVM);
						}
						
                        if (personalDetailsVM.Justification != null)
                        {
                            /*RST BRE CR - 20150126
                            if (validationResult == ErrorMsg.Registration.ContractFail || validationResult.Contains(ErrorMsg.Registration.ContractFail))
                            {
                                TempData["ErrorMessage"] = validationResult;
                                return View(personalDetailsVM);
                            }
                            //end RST*
                            if (validationResult == ErrorMsg.Registration.DDMFFail)
                            {
                                TempData["ErrorMessage"] = validationResult;
                                return View(personalDetailsVM);
                            }

                            if (validationResult == ErrorMsg.Registration.AgeFail)
                            {
                                TempData["ErrorMessage"] = validationResult;
                                return View(personalDetailsVM);
                            }

                            if (validationResult == ErrorMsg.Registration.AddressFail)
                            {
                                TempData["ErrorMessage"] = validationResult;
                                return View(personalDetailsVM);
                            }
                        }
					}
                    */
                    #endregion
                    Session["RegMobileReg_IsAddressCheckFailed"] = false;
                    Session["RegMobileReg_IsAgeCheckFailed"] = false;

                    if (personalDetailsVM.Customer.PayModeID == Util.GetPaymentModeID(PAYMENTMODE.AutoBilling))
                    {
                        if (!ValidateCardDetails(personalDetailsVM.Customer))
                        {
                            return View(personalDetailsVM);
                        }
                    }

                    //GTM e-Billing CR - Ricky - 2014.09.25 - Start
                    bool isValid = false;
                    string ErrorMessage = string.Empty;
                    WebHelper.Instance.BillDeliveryValidate(personalDetailsVM.EmailBillSubscription, personalDetailsVM.EmailAddress, personalDetailsVM.SmsAlertFlag, personalDetailsVM.SmsNo, out isValid, out ErrorMessage);
					if (!isValid)
                    {
                        TempData["ErrorMessage"] = ErrorMessage;
                        return View(personalDetailsVM);
                    }
                    //if valid, store into Session
                    Session[SessionKey.billDelivery_emailBillSubscription.ToString()] = personalDetailsVM.EmailBillSubscription;
                    Session[SessionKey.billDelivery_emailAddress.ToString()] = personalDetailsVM.EmailAddress;
                    Session[SessionKey.billDelivery_smsAlertFlag.ToString()] = personalDetailsVM.SmsAlertFlag;
                    Session[SessionKey.billDelivery_smsNo.ToString()] = personalDetailsVM.SmsNo;
                    //GTM e-Billing CR - Ricky - 2014.09.25 - End

                    //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - Start
                    WebHelper.Instance.ValidateDealerInfo(personalDetailsVM, out isValid, out ErrorMessage);
                    if (!isValid)
                    {
                        TempData["ErrorMessage"] = ErrorMessage;
                        return View(personalDetailsVM);
                    }
                    //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - End
                }
                else
                {
                    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                    {
                        Session[SessionKey.RegMobileReg_TabIndex.ToString()] = (int)MobileRegistrationSteps.DeviceCatalog;
                        personalDetailsVM.TabNumber = (int)MobileRegistrationSteps.DeviceCatalog;
                    }
                }
                using (var proxy = new UserServiceProxy())
                {
                    //TODO Session[SessionKey.KenanACNumber.ToString()].ToString()
                    proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Personal Detais", "", "");
                }
            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }

            Session[SessionKey.Email.ToString()] = personalDetailsVM.Customer.EmailAddr;

            dropObj.personalInformation = personalDetailsVM;
            //for back button scenario, undo/remove the last selected number from the selectedMobileNo bucket 
            if (dropObj.selectedMobileNoinFlow.Count() > 0)
                dropObj.selectedMobileNoinFlow.RemoveAt(dropObj.selectedMobileNoinFlow.Count() - 1);
			
			dropObj.FromMobileDevice = !string.IsNullOrEmpty(collection["fromMobileDevice"]) ? collection["fromMobileDevice"].ToBool() : false;
            dropObj.ApproverID = collection["HdnApproverID"].ToInt();
            Session[SessionKey.DropFourObj.ToString()] = dropObj;

            if ((Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("MREG_DSV")))
            {
                return RedirectToAction(WebHelper.Instance.GetRegActionStep(personalDetailsVM.TabNumber));
            }
            else
            {
                return RedirectToAction(WebHelper.Instance.GetRegActionStep(personalDetailsVM.TabNumber));
            }
        }

        [Authorize(Roles = "DREG_CH,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DAC,MREG_DC,MREG_DN,MREG_DAI,MREG_DSV,DREG_N,DREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSK")]
        [DoNoTCache]
        public ActionResult MobileRegSuccess(int regID)
        {
            //Save the User TransactionLog
            WebHelper.Instance.SaveUserTransactionLogs(regID);

            return View(regID);
        }

        [Authorize(Roles = "DREG_CH,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DAC,MREG_DC,MREG_DN,MREG_DAI,MREG_DSV,DREG_N,DREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSK")]
        [DoNoTCache]
        public ActionResult MobileRegSuccessNew(int regID)
        {
            //Save the User TransactionLog
            WebHelper.Instance.SaveUserTransactionLogs(regID);
            var reg = new DAL.Models.Registration();
            PersonalDetailsVM personalDetails = new PersonalDetailsVM();
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];

            using (var regProxy = new RegistrationServiceProxy())
            {
                reg = regProxy.RegistrationGet(regID);
            }
            personalDetails.RegID = regID;
            personalDetails.SignatureSVG = reg.SignatureSVG;

			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var proxy = new RegistrationServiceProxy())
				{
					personalDetails.PegaVM = new PegaRecommendationsVM();
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					personalDetails.PegaVM.PegaRecommendationsSearchResult = proxy.GetPegaRecommendationbyMSISDN(personalDetails.PegaVM.PegaRecommendationsSearchCriteria);
					personalDetails.PegaVM.PegaRecommendationsSearchResult = personalDetails.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();
				}
			}

            return View(personalDetails);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MobileRegSuccessNew(FormCollection collection, PersonalDetailsVM personalDetails)
        {
            DropFourObj dropObj = new DropFourObj();
            dropObj.personalInformation = new ViewModels.PersonalDetailsVM();
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];
			dropObj.personalInformation.rfDocumentsFile = collection["frontPhoto"].ToString2() == "" ? Session["frontImage"].ToString2() : collection["frontPhoto"].ToString2();
            dropObj.personalInformation.rfDocumentsFileName = collection["frontPhotoFileName"];
            WebHelper.Instance.SaveDocumentToTable(dropObj, personalDetails.RegID);
            ViewBag.documentUploaded = true;
            using (var proxy = new WebPOSCallBackSvc.WebPOSCallBackServiceClient())
            {
                proxy.sendDocsToIcontract(personalDetails.RegID, true);
            }

			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var proxy = new RegistrationServiceProxy())
				{
					personalDetails.PegaVM = new PegaRecommendationsVM();
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					personalDetails.PegaVM.PegaRecommendationsSearchResult = proxy.GetPegaRecommendationbyMSISDN(personalDetails.PegaVM.PegaRecommendationsSearchCriteria);
					personalDetails.PegaVM.PegaRecommendationsSearchResult = personalDetails.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();
				}
			}

            return View(personalDetails);
        }

        [Authorize(Roles = "MREG_DAPP,DREG_CH,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DAC,MREG_DC,MREG_DN,MREG_DAI,MREG_DSV,DREG_N,DREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSK,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegCanceled(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,DREG_CH,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DAC,MREG_DC,MREG_DN,MREG_DAI,MREG_DSV,DREG_N,DREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSK,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegAccCreated(int regID)
        {
            return View(regID);
        }

        [HttpPost]
        public ActionResult MobileRegAccCreated(FormCollection collection)
        {
            int orderID = collection["OrderId"].ToInt();
            return View(orderID);            
        }

        #region "Deactivated due to auto send transaction to webpos w/o login"
        // [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DSK,MREG_U,MREG_SK,MREG_DC,MREG_DN,MREG_DCH,MREG_DIC,MREG_DAC")]
        //[DoNoTCache]
        //[HttpGet]
        //public ActionResult WebPOS(int regID)
        //{
        //    WEBPosVM webPOSVM = WebHelper.Instance.ConstructWebPOS(regID);
        //    return View(webPOSVM);
        //}
        #endregion

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_CH,MREG_DC,MREG_DSV,MREG_DSA,MREG_DAC,MREG_DAI,MREG_DSK,MREG_U,MREG_SK,MREG_DIC,MREG_DN")]
        [DoNoTCache]
        public ActionResult MobileRegSvcActivated(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_CH,MREG_DC,MREG_DC,MREG_DSV,MREG_DSA,MREG_DAC,MREG_DAI,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegClosed(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_CH,MREG_SK,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegBreFail(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_CH,MREG_SK,REG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegFail(int regID)
        {
            return View(regID);
        }

        [HttpPost]
        public ActionResult MobileRegFail(FormCollection collection)
        {
            return RedirectToAction(WebHelper.Instance.GetRegActionStep(collection["TabNumber"].ToInt()));
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_CH,MREG_SK,DREG_N,DREG_C,DREG_CH,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSV,MREG_DSA")]
        [DoNoTCache]
        public ActionResult MobileRegWMFail(int regID)
        {
            return View(regID);
        }

        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,DSupervisor,MREG_DSV,DREG_N,DREG_C,DREG_CH,MREG_DAI,MREG_DIC,MREG_DAC,MREG_DC,MREG_DSK,MREG_DSV,MREG_DSA")]
        public ActionResult MobileRegSummary(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            var resp = new RegistrationCreateResp();
            var response = new KenanTypeAdditionLineRegistrationResponse();
            var reg = new DAL.Models.Registration();
            List<DAL.Models.WaiverComponents> objWaiverComp = new List<WaiverComponents>();
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = personalDetailsVM.TabNumber;
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = personalDetailsVM.ExtraTenMobileNumbers;
            try
            {

                if (collection["submit1"].ToString() == "PaymentRecived")
                {
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                        personalDetailsVM.RegTypeID = reg.RegTypeID;
                    }
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                        });
                    }

                    //Added for MISM virtual numbers flow

                    if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM" && personalDetailsVM.RegTypeID != Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_Secp))
                    {

                        List<string> mobileNos = new List<string>();
                        mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers(true));
                        using (var proxyobj = new RegistrationServiceProxy())
                        {
                            if (mobileNos[0] == string.Empty)
                                mobileNos[0] = "60179900033";
                            int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
                            if (res == 1)
                            {
                                proxyobj.RegistrationUpdateMISM(new DAL.Models.RegistrationSec()
                                {
                                    RegistrationID = personalDetailsVM.RegID,

                                    MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,

                                    LastAccessID = Util.SessionAccess.UserName

                                });
                            }

                        }
                    }

                    else if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM" && personalDetailsVM.RegTypeID == Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_Secp))
                    {
                        List<string> mobileNos = new List<string>();
                        mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers());
                        using (var proxyobj = new RegistrationServiceProxy())
                        {
                            if (mobileNos[0] == string.Empty)
                                mobileNos[0] = "60179900033";
                            int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
                            if (res == 1)
                            {
                                proxyobj.RegistrationUpdate(new DAL.Models.Registration()
                                {
                                    ID = personalDetailsVM.RegID,

                                    MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,

                                    LastAccessID = Util.SessionAccess.UserName

                                });
                            }

                        }
                    }

                    if (personalDetailsVM.RegTypeID == 12)
                    {
                        #region For MNP Flow

                        using (var proxy = new KenanServiceProxy())
                        {

                            var request1 = new MNPtypeFullfillCenterOrderMNP()
                            {
                                orderId = personalDetailsVM.RegID.ToString(),
                            };
                            bool statusMNPNew = proxy.KenanMNPAccountFulfill(request1);
                            if (statusMNPNew)
                            {
                                return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                            }
                            else
                            {
                                using (var regproxy = new RegistrationServiceProxy())
                                {
                                    regproxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail),
                                        ResponseCode = response.Code,
                                        ResponseDescription = response.Message,
                                        MethodName = "KenanAdditionLineRegistration"
                                    });
                                }
                                return RedirectToAction("MobileRegWMFail", new { regID = personalDetailsVM.RegID });
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        #region For Normal Flow

                        response = FulfillKenanAccount(personalDetailsVM.RegID);
                        return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                        #endregion
                    }
                }

                #region code for implementing the epetrol changes
                //if (collection["submit1"].ToString() == "MakePayment")
                //{
                //    return Redirect("../PaymentDetails?id=" + personalDetailsVM.RegID);
                //}
                #endregion

                #region close
                
                if (collection["submit1"].ToString() == "close")
                {
                    if (personalDetailsVM.RegID == 0)
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resp = proxy.RegistrationCreateWithSec(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString() : "false")),
                            WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]),
                            WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
                           WebHelper.Instance.ConstructRegSuppLineVASes(), ConstructRegistrationForSec(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo),
                            ConstructRegPgmBdlPkgSecComponent(), WebHelper.Instance.ConstructRegMdlGroupModelSec(), ConstructSmartComponents(), null,personalDetailsVM.isBREFail);
                            personalDetailsVM.RegID = resp.ID;
                        }
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegBreFail)
                            });
                        }
                        return RedirectToAction("MobileRegBreFail", new { regID = personalDetailsVM.RegID });
                    }
                    else
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                            });
                        }
                        return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
                    }


                }

                if (collection["submit1"].ToString() == "cancel")
                {
                    string reason = string.Empty;
                    int prevStatus = WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID);
                    reason = ReferenceEquals(reason, null) ? "No reason provided." : collection["hdnReasonCancel"].ToString();
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)

                        });

                        proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                        {
                            RegID = personalDetailsVM.RegID,
                            CancelReason = reason,
                            CreateDT = DateTime.Now,
                            LastUpdateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });

                        var totalPrice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID);
                        if (prevStatus == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment) && WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID) == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan) && totalPrice > 0)
                        {
                            WebHelper.Instance.SendDetailsToWebPos(personalDetailsVM.RegID, Constants.StatusWebPosToCancel);
                        }
                    }

                    return RedirectToAction("MobileRegCanceled", new { regID = personalDetailsVM.RegID });
                }
                #endregion

                #region createAcc
                
                if (collection["submit1"].ToString() == "createAcc")
                {
                    try
                    {
                        reg = new DAL.Models.Registration();
                        var regsec = new DAL.Models.RegistrationSec();
                        //var regMdlGrpModels = new List<RegMdlGrpModel>();
                        OrderSummaryVM orderSummaryVM = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                            regsec = proxy.RegistrationSecGet(personalDetailsVM.RegID);
                        }

                        List<string> errorValidationList = new List<string>();
                        
                        if (string.IsNullOrEmpty(reg.SIMSerial))
                        {
                            // fill error msg
                            errorValidationList.Add("Sim serial");
                        }
                        if (!ReferenceEquals(orderSummaryVM, null))
                        {
                            if (string.IsNullOrEmpty(reg.IMEINumber) && !string.IsNullOrEmpty(orderSummaryVM.ModelImageUrl))
                            {
                                // fill error msg
                                errorValidationList.Add("IMEI number");
                            }
                        }
                        if (Session["SimType"].ToString2() == "Normal")
                        {
                            if (!ReferenceEquals(personalDetailsVM.RegSupplineList, null) && personalDetailsVM.RegSupplineList.Count > 0)
                            {
                                int countSupp = 1;
                                foreach (var supp in personalDetailsVM.RegSupplineList)
                                {
                                    if (string.IsNullOrEmpty(supp.SIMSerial))
                                    {
                                        errorValidationList.Add("SubLine Sim Serial" + " " + countSupp.ToString2());
                                    }
                                    if (!string.IsNullOrEmpty(supp.ArticleID) && supp.ArticleID != "0")
                                    {
                                        if (string.IsNullOrEmpty(supp.IMEINumber))
                                        {
                                            errorValidationList.Add("SubLine IMEI Number" + " " + countSupp.ToString2());
                                        }
                                    }
                                    countSupp++;
                                }
                            }
                        }
                        else if(Session["SimType"].ToString2() == "MISM") {
                            if (string.IsNullOrEmpty(regsec.SIMSerial))
                            {
                                errorValidationList.Add("MISM Sim serial");
                            }
                            if (Session["SecDeviceExists"].ToBool() && string.IsNullOrEmpty(regsec.IMEINumber))
                            {
                                errorValidationList.Add("MISM IMEI number");
                            }                        
                        }
                        if (errorValidationList.Any())
                        {
                            TempData[SessionKey.errorValidationList.ToString()] = errorValidationList;
                            using (var proxy = new UserServiceProxy())
                            {
                                String errorTrace = "fail insert inventory" + (String)(errorValidationList.Contains("Sim serial") ? " SIM(" + personalDetailsVM.RegSIMSerial + ") " : "") + (String)(errorValidationList.Contains("IMEI number") ? " IMEI(" + personalDetailsVM.RegIMEINumber + ") " : "") + "for regID: " + reg.ID;
                                proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), errorTrace, "", "");
                            }
                            return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                        }
                        #region CDPU Approval - status to pending cdpu approval/cdpu rejected
                        if (Session["IsDealer"].ToBool() || Roles.IsUserInRole("MREG_DAPP"))
                        {
                            if (personalDetailsVM.CDPUStatus == Constants.StatusCDPUPending)  // have bre failed
                            {
                                //Update Order Status
                                using (var proxy = new RegistrationServiceProxy())
                                {
                                    proxy.RegistrationCancel(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCDPUPendingApp)
                                    });
                                }
                                return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                            }
                            if (personalDetailsVM.CDPUStatus == Constants.StatusCDPURejected)
                            {
                                GeneralResult result = WebHelper.Instance.UnreserveNumberDealerNet(personalDetailsVM.RegID);
                                if (!ReferenceEquals(result, null))
                                {
                                    if (result.ResultCode == 0 && result.ResultMessage == "success")
                                    {
                                        using (var proxy = new RegistrationServiceProxy())
                                        {
                                            proxy.RegistrationCancel(new RegStatus()
                                            {
                                                RegID = personalDetailsVM.RegID,
                                                Active = true,
                                                CreateDT = DateTime.Now,
                                                StartDate = DateTime.Now,
                                                LastAccessID = Util.SessionAccess.UserName,
                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCDPURejected)
                                            });

                                            proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                                            {
                                                RegID = personalDetailsVM.RegID,
                                                CancelReason = Properties.Settings.Default.Status_RegCDPURejected,
                                                CreateDT = DateTime.Now,
                                                LastUpdateDT = DateTime.Now,
                                                LastAccessID = Util.SessionAccess.UserName
                                            });

                                            proxy.UpsertBRE(new RegBREStatus()
                                            {
                                                RegId = personalDetailsVM.RegID,
                                                TransactionStatus = Constants.StatusCDPURejected,
                                                TransactionReason = personalDetailsVM.CDPUDescription,
                                                TransactionDT = DateTime.Now,
                                                TransactionBy = Util.SessionAccess.UserName
                                            });
                                        }
                                        return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                    }
                                }
                                else
                                {
                                    return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                                }
                            }
                            if (personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove)
                            {
                                using (var proxy = new RegistrationServiceProxy())
                                {
                                    proxy.UpsertBRE(new RegBREStatus()
                                    {
                                        RegId = personalDetailsVM.RegID,
                                        TransactionStatus = Constants.StatusCDPUApprove,
                                        TransactionReason = personalDetailsVM.CDPUDescription,
                                        TransactionDT = DateTime.Now,
                                        TransactionBy = Util.SessionAccess.UserName
                                    });
                                }
                            }
                        }
                        #endregion

                        int simModelID = 0;
                        int articleID = 0;
                        string uOMCode = string.Empty;

                        #region "sales person update"
                        string username = string.Empty;

                        using (var proxy = new RegistrationServiceProxy())
                        {
                            reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                            {
                                username = Request.Cookies["CookieUser"].Value;
                            }
                            // Add new update to fix PN3987 - NullUOM // Szeto
                            if (reg.SimModelId != null)
                            {
                                // Get SimModelID 
                                simModelID = reg.SimModelId;
                            }
                            if (simModelID > 0)
                            {
                                using (var cat_proxy = new CatalogServiceProxy())
                                {
                                    //Get ArticleID & UOMCode // 
                                    articleID = cat_proxy.GetArticleIdById(simModelID);
                                    var simModelsDetails = cat_proxy.GetSimModelDetails(simModelID, articleID);

                                    uOMCode = simModelsDetails != null ? simModelsDetails.UOM : string.Empty;
                                }
                            }

                            var regDetails = proxy.RegistrationGet(personalDetailsVM.RegID);

                            // to make sure this is new line, because in drop 4 there's new line + device
                            if (string.IsNullOrEmpty(regDetails.IMEINumber))
                            {
                                proxy.RegistrationUpdate(new DAL.Models.Registration()
                                {
                                    ID = personalDetailsVM.RegID,
                                    //SalesPerson = username,
                                    LastAccessID = Util.SessionAccess.UserName,
                                    //Update Into trnRegistration //
                                    ArticleID = articleID.ToString(),
                                    UOMCode = uOMCode
                                });
                            }
                            // End // 
                        }

                        #endregion
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                            personalDetailsVM.RegTypeID = reg.RegTypeID;
                        }

                        bool isCDPUorderRecs = WebHelper.Instance.isCDPUorder(personalDetailsVM.RegID);

                        if (!isCDPUorderRecs)
                        {
						    try
						    {
							    using (var proxy = new UpdateServiceProxy())
							    {
								    if (!proxy.FindDocument(Constants.CONTRACT, personalDetailsVM.RegID))
								    {
									    //hadi getting String html from printcontractdetails view and convert it to pdf byte arrays         
									    // generate PDF and store into db after input sim / imei
									    String contractHTMLSource = string.Empty;
									    contractHTMLSource = Util.RenderPartialViewToString(this, "PrintContractDetails", WebHelper.Instance.GetContractDetailsViewModel(personalDetailsVM.RegID, false));
									    if (!string.IsNullOrEmpty(contractHTMLSource))
									    {
										    Byte[] PDFBytes = Util.convertHTMLStringToPDFByteArray(contractHTMLSource, personalDetailsVM.RegID);
									    }
								    }
							    }
						    }
						    catch (Exception ex)
						    {
							    Logger.Error("ICONTRACT EXCEPTION = " + ex);
							    ;
						    }
                        }
                        else
                        {
                            bool isFilecreated = WebHelper.Instance.GenerateCDPUcontractFile(personalDetailsVM.RegID);
                        }

                        var totalprice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID);

                        if (personalDetailsVM.RegTypeID == 12)
                        {
                            #region For MNP Flow
                            response = FulfillKenanAccount(personalDetailsVM.RegID);
                            if ((response.Message == "success") && (response.Code == "0"))
                            {
                                using (var regproxy = new RegistrationServiceProxy())
                                {
                                    regproxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate),
                                        ResponseCode = response.Code,
                                        ResponseDescription = response.Message,
                                        MethodName = "KenanAdditionLineRegistration"
                                    });
                                }
                                //if (totalprice == 0 && (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove))
                                //{
                                //    PaymentRecived(personalDetailsVM);
                                //    WebHelper.Instance.SaveUserTransactionLogs(personalDetailsVM.RegID);
                                //}
                                if (Roles.IsUserInRole("MREG_DAPP"))
                                {
                                    return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                }
                                return RedirectToAction("StoreKeeper", "Registration");
                            }
                            else
                            {
                                using (var proxy = new RegistrationServiceProxy())
                                {
                                    proxy.RegistrationCancel(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                                    });
                                }
                                if (Roles.IsUserInRole("MREG_DAPP"))
                                {
                                    return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                }
                                return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                            }
                            #endregion
                        }
                        else
                        {
                            #region For Normal Flow
                            using (var proxy = new KenanServiceProxy())
                            {
                                if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                                {
                                    String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    //if (!ReferenceEquals(Action, String.Empty))
                                    //    return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //proxy.CreateIMPOSFile(personalDetailsVM.RegID);
                                    //using (var regproxy = new RegistrationServiceProxy())
                                    //{
                                    //    LnkRegDetailsReq objLnkRegDetails = regproxy.GetLnkRegistrationDetails(personalDetailsVM.RegID);
                                    //    if (!ReferenceEquals(objLnkRegDetails, null) && !ReferenceEquals(objLnkRegDetails.LnkDetails.SimType, null) && objLnkRegDetails.LnkDetails.SimType == "MISM")
                                    //    {
                                    //        Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    //        //if (!ReferenceEquals(Action, String.Empty))
                                    //        //    return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //        //proxy.CreateIMPOSFile(personalDetailsVM.RegID, "S");
                                    //    }
                                    //}
                                }

                            }
                            //using (var regproxy = new RegistrationServiceProxy())
                            //{
                            //    regproxy.RegistrationClose(new RegStatus()
                            //    {
                            //        RegID = personalDetailsVM.RegID,
                            //        Active = true,
                            //        CreateDT = DateTime.Now,
                            //        StartDate = DateTime.Now,
                            //        LastAccessID = Util.SessionAccess.UserName,
                            //        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                            //        ResponseCode = response.Code,
                            //        ResponseDescription = response.Message,
                            //        MethodName = "KenanAdditionLineRegistration"
                            //    });
                            //}
                            //activate by fulfill
                            //if (totalprice == 0 && (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove))
                            if (totalprice == 0)
                            {
                                response = FulfillKenanAccount(personalDetailsVM.RegID);
                                //PaymentRecived(personalDetailsVM);
                                WebHelper.Instance.SaveUserTransactionLogs(personalDetailsVM.RegID);
                            }
                            if (Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("MREG_DSV"))
                            {
                                return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                            }
                            else if (Roles.IsUserInRole("MREG_DAPP") && !Roles.IsUserInRole("MREG_CUSR"))
                            {
                                return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                            }
                            return RedirectToAction("StoreKeeper", "Registration");
                            #endregion
                        }
						
                    }
                    catch (Exception ex)
                    {
                        LogException(ex);

                        LogExceptions(ex);

                    }
                }
                #endregion

                #region activateSvc
                
                if (collection["submit1"].ToString() == "activateSvc")
                {
                    try
                    {
                        #region "sales person update"
                        string username = string.Empty;
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                            {
                                username = Request.Cookies["CookieUser"].Value;
                            }

                            proxy.RegistrationUpdate(new DAL.Models.Registration()
                            {
                                ID = personalDetailsVM.RegID,
                                //SalesPerson = username,
                                LastAccessID = Util.SessionAccess.UserName,
                            });
                        }

                        #endregion
                        response = FulfillKenanAccount(personalDetailsVM.RegID);
                        if (response.Message == "success")
                        {
                            //BEGIN   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                if (personalDetailsVM.ExtraTenLimitConfirmation == "true")
                                {

                                    if (personalDetailsVM.hdnExtraTenCount != null && personalDetailsVM.hdnExtraTenCount != string.Empty)
                                    {
                                        proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                        {
                                            ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                            {
                                                ConfirmedBy = Util.SessionAccess.UserName,
                                                ConfirmedStatus = "Y",
                                                Count = Convert.ToInt32(personalDetailsVM.hdnExtraTenCount),
                                                CreatedDate = System.DateTime.Now,
                                                RegId = personalDetailsVM.RegID
                                            }
                                        });
                                    }
                                }

                                if (personalDetailsVM.ExtraTenConfirmation == "true")
                                {

                                    proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                    {
                                        ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                        {
                                            ConfirmedBy = Util.SessionAccess.UserName,
                                            ConfirmedStatus = "Y",
                                            CreatedDate = System.DateTime.Now,
                                            RegId = personalDetailsVM.RegID
                                        }
                                    });

                                }
                                else
                                {
                                    proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                    {
                                        ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                        {
                                            ConfirmedBy = Util.SessionAccess.UserName,
                                            ConfirmedStatus = "N",
                                            CreatedDate = System.DateTime.Now,
                                            RegId = personalDetailsVM.RegID
                                        }
                                    });
                                }


                                proxy.DeleteExtraTenDetails(personalDetailsVM.RegID);
                                if (personalDetailsVM.ExtraTenConfirmation == "true")
                                {
                                    foreach (var extratenMSISDN in personalDetailsVM.ExtraTenMobileNumbers.Split(','))
                                    {
                                        if (extratenMSISDN != string.Empty)
                                        {
                                            proxy.SaveExtraTen(new ExtraTenReq
                                            {

                                                ExtraTenDetails = new lnkExtraTenDetails { CreatedBy = Util.SessionAccess.UserName, CreatedDate = System.DateTime.Now, Msisdn = extratenMSISDN, RegId = personalDetailsVM.RegID, Status = "A" }


                                            });
                                        }
                                    }
                                }
                            }
                            //END   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database
                            if (Roles.IsUserInRole("DSupervisor")||Roles.IsUserInRole("MREG_DSV"))
                            {
                                return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                            }
                            else
                            {
                                return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                            }
                        }
                        else
                        {
                            return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
                        }
                    }
                    catch (Exception ex)
                    {
                        LogException(ex);

                        LogExceptions(ex);

                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                    }

                }
                #endregion

                int stepNo = 0;
                if (Session[SessionKey.SimType.ToString()] != null && Session[SessionKey.SimType.ToString()].ToString() == "MISM")
                    stepNo = (int)MobileRegistrationSteps.Submit + 2;
                else
                    stepNo = (int)MobileRegistrationSteps.Submit;
                if (personalDetailsVM.TabNumber == stepNo)
                {
                    if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == false)
                    {
                        // Update Dealer Mobile No
                        var kenanResp = new MobileNoUpdateResponse();
                        using (var proxy = new KenanServiceProxy())
                        {
                            kenanResp = proxy.MobileNoUpdate(WebHelper.Instance.ConstructMobileNoUpdate());
                        }

                        if (!kenanResp.Success)
                        {
                            personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

                            ModelState.AddModelError(string.Empty, string.Format("Failed to update Dealer Mobile Number ({0}).",
                                ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2()));

                            return View("MobileRegSummary", personalDetailsVM);
                        }

                        // submit registration
                    }
                    using (var proxy = new RegistrationServiceProxy())
                    {

                        // 20141204 - Read the waiver components from the dropFourObj - start
                        /*
                        //Constrct waiver components by ravi on sep 25 2013 by ravi
                         objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                        //Constrct waiver components by ravi on sep 25 2013 by ravi ends here
                        */
                         var dropFourObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                         objWaiverComp = Util.constructWaiverComponentList(dropFourObj);

                         if (objWaiverComp == null || objWaiverComp.Count == 0)
                         {
                             objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                         }
                         // 20141204 - Read the waiver components from the dropFourObj - start
                        

                        resp = proxy.RegistrationCreateWithSec(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString() : "false")),
                           WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]),
                            WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
                          WebHelper.Instance.ConstructRegSuppLineVASes(), ConstructRegistrationForSec(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelectedMism"]) ? collection["SIMModelSelectedMism"].ToString() : "false")),
                           ConstructRegPgmBdlPkgSecComponent(), WebHelper.Instance.ConstructRegMdlGroupModelSec(), ConstructSmartComponents(), objWaiverComp,personalDetailsVM.isBREFail);
                        personalDetailsVM.RegID = resp.ID;
                        
                        if (resp.ID > 0)
                        {
                            ///BYPASS THE BELOW CODE FOR Extra Ten IF REQUEST FROM Multiple MNP
                            bool isMultipleMNP = personalDetailsVM.RegTypeID == (int)MobileRegType.RegType_MNPPlanWithMultiSuppline ? true : false;
                            if (!isMultipleMNP)
                                Util.SaveExtraTen(personalDetailsVM.ExtraTenMobileNumbers, resp.ID);
                        }
                        var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
                        if (biometricID != 0)
                        {
                            var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
                            biometric.RegID = resp.ID;

                            proxy.BiometricUpdate(biometric);
                        }
                    }
                    using (var proxy = new UserServiceProxy())
                    {
                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Registration Done with RegID " + resp.ID, Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                    }

                    #region VLT ADDED CODE on 19th June for Adding SimType
                    if (resp.ID > 0)
                    {
                        int regTypeID = 0;
                        if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
                        {
                            regTypeID = Util.GetRegTypeID(REGTYPE.MNPNewLine);
                        }
                        else
                        {
                            regTypeID = Util.GetRegTypeID(REGTYPE.NewLine);
                        }

                        //commented as disabling pos file feature
                        //#region Saving POS FileName in DB for Dealers
                        //if (Session["IsDealer"].ToBool() == true && collection["hdnTotalPayable"].ToString2() != string.Empty && collection["hdnTotalPayable"].ToDecimal() > 0 )
                        //{
                        //    string posFileName = WebHelper.Instance.CreatePOSFileName(resp.ID);
                        //    using (var proxy = new CatalogServiceProxy())
                        //    {
                        //        proxy.UpdateIMPOSDetails(resp.ID, posFileName, 1);
                        //    }
                        //}
                        //#endregion

                        string PlanKenanCode = MasterDataCache.Instance.PackageComponents.Where(a => a.ID == Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"])).Select(a => a.KenanCode).SingleOrDefault().ToString2();
                        string latestPrintVersion = Util.GetPrintVersionNum(regTypeID, !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty, PlanKenanCode);
                        LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
                        Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                        objRegDetails.CreatedDate = DateTime.Now;


                        //Insert Sim Type
                        if (Session[SessionKey.SimType.ToString()].ToString2().Length > 0)
                        {
                            objRegDetails.SimType = Session[SessionKey.SimType.ToString()].ToString();
                        }

                        //if (Session["IsDealer"].ToBool() == false)
                        //{
                            if (!string.IsNullOrEmpty(collection["SimCardTypeSelected"]))
                                objRegDetails.SimCardType = collection["SimCardTypeSelected"].ToString2();

                            if (!string.IsNullOrEmpty(collection["SecondarySimCardTypeSelected"]))
                                objRegDetails.MismSimCardType = collection["SecondarySimCardTypeSelected"].ToString2();
                        //}
                        objRegDetails.UserName = Util.SessionAccess.UserName;
                        objRegDetails.RegId = resp.ID;
                        objRegDetails.Liberlization_Status = personalDetailsVM.Liberlization_Status;
                        objRegDetails.MOC_Status = personalDetailsVM.MOCStatus;
                        if (!string.IsNullOrEmpty(latestPrintVersion))
                        {
                            objRegDetails.PrintVersionNo = latestPrintVersion;
                        }
                        objRegDetailsReq.LnkDetails = objRegDetails;

                        /*Active PDPA Commented*/
                        /*Changes by chetan related to PDPA implementation*/
                        //using (var registrationProxy = new RegistrationServiceProxy())
                        //{
                        //    var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        //    objRegDetails.PdpaVersion = resultdata.Version;
                        //}

                        /*Recode Active PDPA Commented*/
                        objRegDetails.PdpaVersion = personalDetailsVM.PDPDVersion;

                        //Insert justification
                        if (collection["txtJustfication"].ToString2().Length > 0)
                        {
                            objRegDetails.Justification = collection["txtJustfication"].ToString();
                        }

                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.SaveLnkRegistrationDetails(objRegDetailsReq);
                        }
                    }
                    #endregion

                    #region VLT ADDED CODE on 21stFEB for email functionality
                    if (resp.ID > 0)
                    {

                        /* BEGIN Added by VLT 5 Apr 2013 to add kenaninfo to database*/
                        if (Session["KenanCustomerInfo"] != null)
                        {
                            string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString().Split('╚');
                            KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
                            kenanInfo.FullName = strArrKenanInfo[0];
                            kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
                            kenanInfo.IDCardNo = strArrKenanInfo[2];
                            kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
                            kenanInfo.Address1 = strArrKenanInfo[4];
                            kenanInfo.Address2 = strArrKenanInfo[5];
                            kenanInfo.Address3 = strArrKenanInfo[6];
                            kenanInfo.Postcode = strArrKenanInfo[8];
                            kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
                            kenanInfo.RegID = resp.ID;
                            using (var proxys = new RegistrationServiceProxy())
                            {
                                var kenanResp = new KenanCustomerInfoCreateResp();

                                kenanResp = proxys.KenanCustomerInfoCreate(kenanInfo);
                            }
                        }
                        Session["KenanCustomerInfo"] = null;
                        /* END Added by VLT 5 Apr 2013 to add kenaninfo to database*/

						Session["NewLineTotalPayable"] = collection["hdnTotalPayable"];

                        //Joshi added for Delear
                        if (Roles.IsUserInRole("MREG_DSV"))
                        {
                            string username = string.Empty;
                            using (var proxyObj = new RegistrationServiceProxy())
                            {
                                if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                                {
                                    username = Request.Cookies["CookieUser"].Value;
                                }

                                proxyObj.RegistrationUpdate(new DAL.Models.Registration()
                                {
                                    ID = personalDetailsVM.RegID,
                                    //SalesPerson = username,
                                    LastAccessID = Util.SessionAccess.UserName,
                                });
                            }

                    #endregion
                            using (var proxyobj = new RegistrationServiceProxy())
                            {
                                reg = proxyobj.RegistrationGet(personalDetailsVM.RegID);
                                personalDetailsVM.RegTypeID = reg.RegTypeID;
                            }
                            if (personalDetailsVM.RegTypeID == 12)
                            {
                                #region For MNP Flow
                                response = FulfillKenanAccount(personalDetailsVM.RegID);
                                if ((response.Message == "success") && (response.Code == "0"))
                                {
                                    using (var regproxy = new RegistrationServiceProxy())
                                    {
                                        regproxy.RegistrationClose(new RegStatus()
                                        {
                                            RegID = personalDetailsVM.RegID,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            StartDate = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate),
                                            ResponseCode = response.Code,
                                            ResponseDescription = response.Message,
                                            MethodName = "KenanAdditionLineRegistration"
                                        });
                                    }
                                    return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
                                }
                                else
                                {
                                    using (var proxyobj = new RegistrationServiceProxy())
                                    {
                                        proxyobj.RegistrationCancel(new RegStatus()
                                        {
                                            RegID = personalDetailsVM.RegID,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            StartDate = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                                        });
                                    }
                                    return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                }
                                #endregion
                            }
                            else
                            {
                                #region For Normal Flow
                                using (var proxyobj = new KenanServiceProxy())
                                {
                                    if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                                    {
                                        String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                        //if (!ReferenceEquals(Action, String.Empty))
                                        //    return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                        //proxyobj.CreateIMPOSFile(personalDetailsVM.RegID);
                                        //using (var regproxy = new RegistrationServiceProxy())
                                        //{
                                        //    LnkRegDetailsReq objLnkRegDetails = regproxy.GetLnkRegistrationDetails(personalDetailsVM.RegID);
                                        //    if (!ReferenceEquals(objLnkRegDetails, null) && !ReferenceEquals(objLnkRegDetails.LnkDetails.SimType, null) && objLnkRegDetails.LnkDetails.SimType == "MISM")
                                        //    {
                                        //        Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                        //        //if (!ReferenceEquals(Action, String.Empty))
                                        //        //    return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                        //        //proxyobj.CreateIMPOSFile(personalDetailsVM.RegID, "S");
                                        //    }
                                        //}
                                    }

                                }

                                //using (var regproxy = new RegistrationServiceProxy())
                                //{
                                //    regproxy.RegistrationClose(new RegStatus()
                                //    {
                                //        RegID = personalDetailsVM.RegID,
                                //        Active = true,
                                //        CreateDT = DateTime.Now,
                                //        StartDate = DateTime.Now,
                                //        LastAccessID = Util.SessionAccess.UserName,
                                //        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                //        ResponseCode = response.Code,
                                //        ResponseDescription = response.Message,
                                //        MethodName = "KenanAdditionLineRegistration"
                                //    });
                                //}

                                //if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() == 0) && (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove))
                                //{                                   
                                //    //DAL.Models.Registration regstrn;
                                //    //using (var regProxy = new RegistrationServiceProxy())
                                //    //{
                                //    //    regstrn = regProxy.RegistrationGet(resp.ID);
                                //    //}
                                //    //personalDetailsVM.RegTypeID = regstrn.RegTypeID;
                                //    //personalDetailsVM.RegID = resp.ID;
                                //    //PaymentRecived(personalDetailsVM);
                                //    response = FulfillKenanAccount(personalDetailsVM.RegID);
                                //    //Save the User TransactionLog
                                //    WebHelper.Instance.SaveUserTransactionLogs(resp.ID);
                                //}               
                                
                            }
                        }
                        //End of joshi


                        ClearRegistrationSession();

                        if (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove)
                        {
                            //if (Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("DAgent") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_C"))
                            //{
                            //   // return RedirectToAction("MobileRegSummary", new { regID = resp.ID });
                            //    //return RedirectToAction("MobileRegAccCreated", new { regID = resp.ID });

                            //    if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() == 0))
                            //    {
                            //        return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });
                            //    }
                            //    else if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() > 0))
                            //    {
                            //        return RedirectToAction("MobileRegAccCreated", new { regID = resp.ID });
                            //    }
                            //    Session["NewLineTotalPayable"] = null;
                            //}

                            //else
                            //{
                            //if (Roles.IsUserInRole("MREG_DAI"))
                            //{
                                using (var regproxy = new RegistrationServiceProxy())
                                {
                                    regproxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                        ResponseCode = response.Code,
                                        ResponseDescription = response.Message,
                                        MethodName = "KenanAdditionLineRegistration"
                                    });
                                }
                                if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() == 0))
                                {
                                    //DAL.Models.Registration regstrn;
                                    //using (var regProxy = new RegistrationServiceProxy())
                                    //{
                                    //    regstrn = regProxy.RegistrationGet(resp.ID);
                                    //}
                                    //personalDetailsVM.RegTypeID = regstrn.RegTypeID;
                                    //personalDetailsVM.RegID = resp.ID;
                                    //PaymentRecived(personalDetailsVM);
                                    response = FulfillKenanAccount(personalDetailsVM.RegID);
                                    //Save the User TransactionLog
                                    WebHelper.Instance.SaveUserTransactionLogs(resp.ID);
                                }
                            //}
                            //if (Session["IsDealer"].ToBool())
                            //{
                            if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() == 0) && Roles.IsUserInRole("MREG_DSA"))
                            {
                                return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                            }
                            else if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() == 0))
                            {
                                return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });
                            }
                            else if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() > 0))
                            {
                                return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                            }
                            Session["NewLineTotalPayable"] = null;
                        }
                            //}
                            //else
                            //{
                            //    return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                            //}
                        //}
                        /*upto here*/
                    }
                    #endregion VLT ADDED CODE on 21stFEB for email functionality
                }
                else if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.PersonalDetails)
                {
                    return RedirectToAction("PersonalDetailsNew");
                }

            }
            catch (Exception ex)
            {
                Logger.Info("Throw Exception for Order ID:" + resp.ID + "Exception:" + ex);
                LogExceptions(ex);
                throw ex;
            }

            return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
        }


        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,DSupervisor,MREG_DSV,DREG_N,DREG_C,DREG_CH,MREG_DAI,MREG_DIC,MREG_DAC,MREG_DC,MREG_DSK,MREG_DSV,MREG_DSA")]
        public ActionResult MobileRegSummaryNew(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            var resp = new RegistrationCreateResp();
            var response = new KenanTypeAdditionLineRegistrationResponse();
            var reg = new DAL.Models.Registration();
            DateTime? TimeApproval = null;
            if (!ReferenceEquals(Session[SessionKey.TimeApproval.ToString()], null))
            {
                TimeApproval = (DateTime)Session[SessionKey.TimeApproval.ToString()];
            }
            List<DAL.Models.WaiverComponents> objWaiverComp = new List<WaiverComponents>();
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = personalDetailsVM.TabNumber;
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = personalDetailsVM.ExtraTenMobileNumbers;

            // DropFour Object for MultiSuppline - start

            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            if (dropObj != null)
            {
            //    if (collection["MainSimSerial"] != null && !string.IsNullOrEmpty(collection["MainSimSerial"].ToString()))
            //    {
            //        dropObj.mainFlow.simSerial = collection["MainSimSerial"].ToString();
            //    }

            //    if (collection["MainImei"] != null && !string.IsNullOrEmpty(collection["MainImei"].ToString()))
            //    {
            //        dropObj.mainFlow.imeiNumber = collection["MainImei"].ToString();
            //    }

                if (collection["MismSimSerial"] != null && !string.IsNullOrEmpty(collection["MismSimSerial"].ToString()))
                {
                    dropObj.msimFlow[0].simSerial = collection["MismSimSerial"].ToString();
                }

            //    if (dropObj.suppIndex > 0)
            //    {
            //        for (int i = 0; i < dropObj.suppIndex; i++)
            //        {
            //            if (!string.IsNullOrEmpty(collection["SuppSimSerial" + i.ToString()].ToString2()))
            //            {
            //                dropObj.suppFlow[i].simSerial = collection["SuppSimSerial" + i.ToString()].ToString2();
            //            }
            //        }
            //    }


                // Drop 5 - use for CDPU 
                personalDetailsVM.isBREFail = dropObj.personalInformation.isBREFail;

                Session[SessionKey.DropFourObj.ToString()] = dropObj;
            }

            // DropFour object for multiSuppline - end

            try
            {
                #region paymentRecived
                
                if (collection["submit1"].ToString() == "PaymentRecived")
                {
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                        personalDetailsVM.RegTypeID = reg.RegTypeID;
                    }
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                        });
                    }

                    //Added for MISM virtual numbers flow

                    if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM" && personalDetailsVM.RegTypeID != Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_Secp))
                    {

                        List<string> mobileNos = new List<string>();
                        mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers(true));
                        using (var proxyobj = new RegistrationServiceProxy())
                        {
                            if (mobileNos[0] == string.Empty)
                                mobileNos[0] = "60179900033";
                            int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
                            if (res == 1)
                            {
                                proxyobj.RegistrationUpdateMISM(new DAL.Models.RegistrationSec()
                                {
                                    RegistrationID = personalDetailsVM.RegID,

                                    MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,

                                    LastAccessID = Util.SessionAccess.UserName

                                });
                            }

                        }
                    }

                    else if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM" && personalDetailsVM.RegTypeID == Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_Secp))
                    {
                        List<string> mobileNos = new List<string>();
                        mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers());
                        using (var proxyobj = new RegistrationServiceProxy())
                        {
                            if (mobileNos[0] == string.Empty)
                                mobileNos[0] = "60179900033";
                            int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
                            if (res == 1)
                            {
                                proxyobj.RegistrationUpdate(new DAL.Models.Registration()
                                {
                                    ID = personalDetailsVM.RegID,

                                    MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,

                                    LastAccessID = Util.SessionAccess.UserName

                                });
                            }

                        }
                    }

                    if (personalDetailsVM.RegTypeID == 12)
                    {
                        #region For MNP Flow

                        using (var proxy = new KenanServiceProxy())
                        {

                            var request1 = new MNPtypeFullfillCenterOrderMNP()
                            {
                                orderId = personalDetailsVM.RegID.ToString(),
                            };
                            bool statusMNPNew = proxy.KenanMNPAccountFulfill(request1);
                            if (statusMNPNew)
                            {
                                Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                                return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                            }
                            else
                            {
                                using (var regproxy = new RegistrationServiceProxy())
                                {
                                    regproxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail),
                                        ResponseCode = response.Code,
                                        ResponseDescription = response.Message,
                                        MethodName = "KenanAdditionLineRegistration"
                                    });
                                }
                                Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                                return RedirectToAction("MobileRegWMFail", new { regID = personalDetailsVM.RegID });
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        #region For Normal Flow

                        response = FulfillKenanAccount(personalDetailsVM.RegID);
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                        #endregion
                    }
                }
                #endregion

                #region close
                
                if (collection["submit1"].ToString() == "close")
                {
                    if (personalDetailsVM.RegID == 0)
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resp = proxy.RegistrationCreateWithSec(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString() : "false")),
                            WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]),
                            WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
                            WebHelper.Instance.ConstructRegSuppLineVASes(), ConstructRegistrationForSec(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo),
                            ConstructRegPgmBdlPkgSecComponent(), WebHelper.Instance.ConstructRegMdlGroupModelSec(), ConstructSmartComponents(), null, personalDetailsVM.isBREFail, dropObj.ApproverID, TimeApproval);
                            personalDetailsVM.RegID = resp.ID;
                        }
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegBreFail)
                            });
                        }
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegBreFail", new { regID = personalDetailsVM.RegID });
                    }
                    else
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                            });
                        }
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
                    }


                }
                #endregion

                #region cancel
                
                if (collection["submit1"].ToString() == "cancel")
                {
                    string reason = string.Empty;
                    int prevStatus = WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID);
                    reason = ReferenceEquals(reason, null) ? "No reason provided." : collection["hdnReasonCancel"].ToString();
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)

                        });

                        proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                        {
                            RegID = personalDetailsVM.RegID,
                            CancelReason = reason,
                            CreateDT = DateTime.Now,
                            LastUpdateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });

                        var totalPrice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID);
                        if (prevStatus == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment) && WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID) == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan) && totalPrice > 0)
                        {
                            WebHelper.Instance.SendDetailsToWebPos(personalDetailsVM.RegID, Constants.StatusWebPosToCancel);
                        }
                    }

                    //return RedirectToAction("MobileRegCanceled", new { regID = personalDetailsVM.RegID });
                    Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                    return RedirectToAction("IndexNew", "Home");
                }
                #endregion

                #region create account
                if (collection["submit1"].ToString() == "createAcc")
                {
                    try
                    {
                        
                        #region "sales person update"
                        string username = string.Empty;
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                            {
                                username = Request.Cookies["CookieUser"].Value;
                            }

                            proxy.RegistrationUpdate(new DAL.Models.Registration()
                            {
                                ID = personalDetailsVM.RegID,
                                //SalesPerson = username,
                                LastAccessID = Util.SessionAccess.UserName,
                            });
                        }

                        #endregion
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                            personalDetailsVM.RegTypeID = reg.RegTypeID;
                        }
                        if (personalDetailsVM.RegTypeID == 12)
                        {
                            #region For MNP Flow
                            response = FulfillKenanAccount(personalDetailsVM.RegID);
                            if ((response.Message == "success") && (response.Code == "0"))
                            {
                                using (var regproxy = new RegistrationServiceProxy())
                                {
                                    regproxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate),
                                        ResponseCode = response.Code,
                                        ResponseDescription = response.Message,
                                        MethodName = "KenanAdditionLineRegistration"
                                    });
                                }
                                Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                                return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
                            }
                            else
                            {
                                using (var proxy = new RegistrationServiceProxy())
                                {
                                    proxy.RegistrationCancel(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                                    });
                                }
                                Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                                return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                            }
                            #endregion
                        }
                        else
                        {
                            #region For Normal Flow
                            using (var proxy = new KenanServiceProxy())
                            {
                                if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                                {
                                    String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    if (!ReferenceEquals(Action, String.Empty))
                                        return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //proxy.CreateIMPOSFile(personalDetailsVM.RegID);
                                    //using (var regproxy = new RegistrationServiceProxy())
                                    //{
                                    //    LnkRegDetailsReq objLnkRegDetails = regproxy.GetLnkRegistrationDetails(personalDetailsVM.RegID);
                                    //    if (!ReferenceEquals(objLnkRegDetails, null) && !ReferenceEquals(objLnkRegDetails.LnkDetails.SimType, null) && objLnkRegDetails.LnkDetails.SimType == "MISM")
                                    //    {
                                    //        Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    //        if (!ReferenceEquals(Action, String.Empty))
                                    //            return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //        //proxy.CreateIMPOSFile(personalDetailsVM.RegID, "S");
                                    //    }
                                    //}
                                }

                            }
                            using (var regproxy = new RegistrationServiceProxy())
                            {
                                regproxy.RegistrationClose(new RegStatus()
                                {
                                    RegID = personalDetailsVM.RegID,
                                    Active = true,
                                    CreateDT = DateTime.Now,
                                    StartDate = DateTime.Now,
                                    LastAccessID = Util.SessionAccess.UserName,
                                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                    ResponseCode = response.Code,
                                    ResponseDescription = response.Message,
                                    MethodName = "KenanAdditionLineRegistration"
                                });
                            }
                            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                            //if (Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("MREG_DSV"))
                            //{
                            //    return RedirectToAction("MobileRegSummaryNew", new { regID = personalDetailsVM.RegID });
                            //}
                            //else
                            //{
                                return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
                            //}
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        LogException(ex);

                        LogExceptions(ex);

                    }
                }
                #endregion

                #region activate service
                if (collection["submit1"].ToString() == "activateSvc")
                {
                    try
                    {
                        #region "sales person update"
                        string username = string.Empty;
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                            {
                                username = Request.Cookies["CookieUser"].Value;
                            }

                            proxy.RegistrationUpdate(new DAL.Models.Registration()
                            {
                                ID = personalDetailsVM.RegID,
                                //SalesPerson = username,
                                LastAccessID = Util.SessionAccess.UserName,
                            });
                        }

                        #endregion
                        response = FulfillKenanAccount(personalDetailsVM.RegID);
                        if (response.Message == "success")
                        {
                            //BEGIN   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                if (personalDetailsVM.ExtraTenLimitConfirmation == "true")
                                {

                                    if (personalDetailsVM.hdnExtraTenCount != null && personalDetailsVM.hdnExtraTenCount != string.Empty)
                                    {
                                        proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                        {
                                            ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                            {
                                                ConfirmedBy = Util.SessionAccess.UserName,
                                                ConfirmedStatus = "Y",
                                                Count = Convert.ToInt32(personalDetailsVM.hdnExtraTenCount),
                                                CreatedDate = System.DateTime.Now,
                                                RegId = personalDetailsVM.RegID
                                            }
                                        });
                                    }
                                }

                                if (personalDetailsVM.ExtraTenConfirmation == "true")
                                {

                                    proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                    {
                                        ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                        {
                                            ConfirmedBy = Util.SessionAccess.UserName,
                                            ConfirmedStatus = "Y",
                                            CreatedDate = System.DateTime.Now,
                                            RegId = personalDetailsVM.RegID
                                        }
                                    });

                                }
                                else
                                {
                                    proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                    {
                                        ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                        {
                                            ConfirmedBy = Util.SessionAccess.UserName,
                                            ConfirmedStatus = "N",
                                            CreatedDate = System.DateTime.Now,
                                            RegId = personalDetailsVM.RegID
                                        }
                                    });
                                }


                                proxy.DeleteExtraTenDetails(personalDetailsVM.RegID);
                                if (personalDetailsVM.ExtraTenConfirmation == "true")
                                {
                                    foreach (var extratenMSISDN in personalDetailsVM.ExtraTenMobileNumbers.Split(','))
                                    {
                                        if (extratenMSISDN != string.Empty)
                                        {
                                            proxy.SaveExtraTen(new ExtraTenReq
                                            {

                                                ExtraTenDetails = new lnkExtraTenDetails { CreatedBy = Util.SessionAccess.UserName, CreatedDate = System.DateTime.Now, Msisdn = extratenMSISDN, RegId = personalDetailsVM.RegID, Status = "A" }


                                            });
                                        }
                                    }
                                }
                            }
                            //END   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database
                            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                            //if (Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("MREG_DSV"))
                            //{
                            //    return RedirectToAction("MobileRegSummaryNew", new { regID = personalDetailsVM.RegID });
                            //}
                            //else
                            //{
                                return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                            //}
                        }
                        else
                        {
                            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                            return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
                        }
                    }
                    catch (Exception ex)
                    {
                        LogException(ex);

                        LogExceptions(ex);

                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                    }

                }
                #endregion

                int stepNo = 0;
                if (Session[SessionKey.SimType.ToString()] != null && Session[SessionKey.SimType.ToString()].ToString() == "MISM")
                    stepNo = (int)MobileRegistrationSteps.Submit + 2;
                else
                    stepNo = (int)MobileRegistrationSteps.Submit;



                if (personalDetailsVM.TabNumber == stepNo)
                {
					if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == false)
					{
						var reservedNumberList = WebHelper.Instance.constructMobileNoSelection();
						foreach (var number in reservedNumberList)
						{
							if (!number.isReserved)// If not a reserved number
							{
								var kenanResp = new MobileNoUpdateResponse();
								using (var proxy = new KenanServiceProxy())
								{
									kenanResp = proxy.MobileNoUpdate(WebHelper.Instance.ConstructMobileNoUpdateNew(number, personalDetailsVM));
								}
								if (!kenanResp.Success)
								{
									personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

									ModelState.AddModelError(string.Empty, string.Format("Failed to update Dealer Mobile Number ({0}).",
										((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2()));

									return View("MobileRegSummaryNew", personalDetailsVM);
								}
							}
						}
					}

					if (string.IsNullOrEmpty(personalDetailsVM.QueueNo))
					{
						personalDetailsVM.QueueNo = !ReferenceEquals(dropObj.personalInformation, null) ? dropObj.personalInformation.QueueNo : string.Empty;
					}

					string simType = string.Empty;
                    using (var proxy = new RegistrationServiceProxy())
                    {


                        // 20141204 - Read the waiver components from the dropFourObj - start

                        /*
                        //Constrct waiver components by ravi on sep 25 2013 by ravi
                        objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                        //Constrct waiver components by ravi on sep 25 2013 by ravi ends here
                        */
                        objWaiverComp = Util.constructWaiverComponentList(dropObj);

                        if (objWaiverComp == null || objWaiverComp.Count == 0)
                        {                                                    
                            objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                        }
                        // 20141204 - Read the waiver components from the dropFourObj - start


                        //var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

						string principalSimSize = dropObj.mainFlow.simSize;
                        string principalSimModel = dropObj.mainFlow.mobile.SIMCardType;

						simType = principalSimModel;

                        string mismSize = string.Empty;
                        if (principalSimModel == "MISM")
                        { 
                            if (dropObj.msimIndex > 0){
                                mismSize = dropObj.msimFlow[0].simSize;
                            }
                        }

                        var cRegistration = ConstructRegistration(
                            personalDetailsVM.QueueNo, 
                            personalDetailsVM.Remarks, 
                            personalDetailsVM.SignatureSVG, 
                            personalDetailsVM.CustomerPhoto, 
                            personalDetailsVM.AltCustomerPhoto, 
                            personalDetailsVM.Photo, (!string.IsNullOrEmpty(principalSimSize) ?principalSimSize: "false"));

                        var cSupplines = ConstructRegSuppLines(
                                collection["InputNationalityID"].ToString2() == "" ? personalDetailsVM.Customer.NationalityID : Convert.ToInt32(collection["InputNationalityID"]),
                                collection["SIMModelSelectedMnpSuppline"].ToString2()
                            );

                        var cCustomer = WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]);
                        var cGroupModel = WebHelper.Instance.ConstructRegMdlGroupModel();
                        var cAddress = WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]);
						//var cComponent = WebHelper.Instance.ConstructRegPgmBdlPkgComponent();
						var cComponent = ConstructRegPgmBdlPkgComponent();
                        var cStatus = WebHelper.Instance.ConstructRegStatus();
                        //var cSuppVas = ConstructRegSuppLineVASes();
						// constructing vas for suppline, this is shared between Registration & NewLine
						var cSuppVas = WebHelper.Instance.ConstructSuppLineVasComponent();
                        var cMism = ConstructRegistrationForSec(personalDetailsVM.QueueNo, 
                            personalDetailsVM.Remarks, 
                            personalDetailsVM.SignatureSVG, 
                            personalDetailsVM.CustomerPhoto, 
                            personalDetailsVM.AltCustomerPhoto, 
                            personalDetailsVM.Photo, 
                            (!string.IsNullOrEmpty(mismSize) ? mismSize : "false"));
                        var cPkgComponentSec = ConstructRegPgmBdlPkgSecComponent();
                        var cModelSec = WebHelper.Instance.ConstructRegMdlGroupModelSec();
                        var cSmart = ConstructSmartComponents();

                        resp = proxy.RegistrationCreateWithSec(
                                cRegistration,
                                cCustomer,
                                cGroupModel,
                                cAddress,
                                cComponent,
                                cStatus,
                                cSupplines,
                                cSuppVas,
                                cMism,
                                cPkgComponentSec,
                                cModelSec,
                                cSmart,
                                objWaiverComp,
                                personalDetailsVM.isBREFail,
                                dropObj.ApproverID,
                                TimeApproval
                            );

                        // drop 4 sanitazion

                        //    resp = proxy.RegistrationCreateWithSec(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(principalSimSize) ?principalSimSize: "false")),
                        //   WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]),
                        //    WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), cSupplines,
                        //  ConstructRegSuppLineVASes(), ConstructRegistrationForSec(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(mismSize) ? mismSize : "false")),
                        //   ConstructRegPgmBdlPkgSecComponent(), WebHelper.Instance.ConstructRegMdlGroupModelSec(), ConstructSmartComponents(), objWaiverComp, personalDetailsVM.isBREFail);
                        //personalDetailsVM.RegID = resp.ID;

                        if (resp.ID > 0)
                        {
                            // Drop 5
                            //if ((personalDetailsVM.isBREFail && Session["IsDealer"].ToBool()) || (dropObj.mainFlow.isDeviceFinancingExceptionalOrder && dropObj.mainFlow.isDeviceFinancingOrder && Roles.IsUserInRole("MREG_CUSR")))
                            var isRouteToCDPU = WebHelper.Instance.routeToCDPU();

                            if (isRouteToCDPU)
                            {
                                try
                                {
                                    proxy.UpsertBRE(new RegBREStatus()
                                    {
                                        RegId = resp.ID,
                                        TransactionStatus = Constants.StatusCDPUPending
                                    });
                                }
                                catch
                                {

                                }
                            }

                            #region Insert into trnRegAttributes
                            var regAttribList = new List<RegAttributes>();
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletCode))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCODE, ATT_Value = personalDetailsVM.OutletCode });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletChannelId))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCHANNELID, ATT_Value = personalDetailsVM.OutletChannelId });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesCode))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCODE, ATT_Value = personalDetailsVM.SalesCode });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesChannelId))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCHANNELID, ATT_Value = personalDetailsVM.SalesChannelId });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffCode))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCODE, ATT_Value = personalDetailsVM.SalesStaffCode });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffChannelId))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCHANNELID, ATT_Value = personalDetailsVM.SalesStaffChannelId });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.BillingCycle))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_ACC_BILL_CYCLE, ATT_Value = personalDetailsVM.BillingCycle });


                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SingleSignOnValue))
                            {
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_SINGLESIGNON_STATUS, ATT_Value = personalDetailsVM.SingleSignOnValue });

                                if (personalDetailsVM.SingleSignOnValue.ToUpper() == "YES")
                                {
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_SINGLESIGNON_EMAILADDRESS, ATT_Value = personalDetailsVM.SingleSignOnEmailAddress });
                                }
                            }
                            
                            regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_PAYMENT_METHOD, ATT_Value = Util.getPaymentMethodName(dropObj.personalInformation.Customer.PayModeID) });
                            regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_USER_DEVICE_AGENT, ATT_Value = Request.UserAgent });

							proxy.SaveListRegAttributes(regAttribList);
                            #endregion

							#region Insert into trnRegJustification
							WebHelper.Instance.saveTrnRegJustification(resp.ID);
                            #endregion

                            #region Insert into trnRegBreFailTreatment
                            WebHelper.Instance.saveTrnRegBreFailTreatment(resp.ID);
                            #endregion

							#region Insert into trnSurveyResponse
                            WebHelper.Instance.SaveSurveyResult(personalDetailsVM, RegSurveyResponse.MAXIS_APP, resp.ID);
							#endregion

                            #region Insert into trnRegAccessory
                            if (cGroupModel != null)
                            {
                                var serialNumberList = new Dictionary<int, string>();
                                WebHelper.Instance.SaveTrnRegAccessory(dropObj, resp.ID, ref serialNumberList);
                                WebHelper.Instance.SaveTrnRegAccessoryDetails(resp.ID, serialNumberList);
                            }
                            #endregion
							/* added by Gry*/
							reg = proxy.RegistrationGet(resp.ID);
							personalDetailsVM.RegID = reg.ID;
							personalDetailsVM.RegTypeID = reg.RegTypeID;
							/* added by Gry*/
                            ///BYPASS THE BELOW CODE FOR Extra Ten IF REQUEST FROM Multiple MNP
                            bool isMultipleMNP = personalDetailsVM.RegTypeID == (int)MobileRegType.RegType_MNPPlanWithMultiSuppline ? true : false;
                            if (!isMultipleMNP)
                                Util.SaveExtraTen(personalDetailsVM.ExtraTenMobileNumbers, resp.ID);

							/**DROP 4**/
							Util.updateSuppLineExtOrderID(resp.ID);
							Util.updateLnkMdlGrpMdlforSupp(resp.ID);
							/**DROP 4**/

							// update cardType if existing customer is direct debit, flag get from _BillingPaymentnew
							// if change this need to change in newLine also.
							if (!ReferenceEquals(Session["directDebit"], null))
							{
								var _ddFlag = Session["directDebit"].ToString2();
								if (_ddFlag.Equals("1"))
								{
									var cust = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();
									if (!ReferenceEquals(cust, null))
									{
										using (var _updateProxy = new UpdateServiceProxy())
										{
											cust.CardTypeID = 99;
											_updateProxy.CustomerUpdate(cust);
										}
									}
									
								}
							}

							if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM" && personalDetailsVM.RegTypeID != Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_Secp))
							{

								List<string> mobileNos = new List<string>();
								mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers(true));
								using (var proxyobj = new RegistrationServiceProxy())
								{
									if (mobileNos[0] == string.Empty)
										mobileNos[0] = "60179900033";
									int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
									if (res == 1)
									{
										proxyobj.RegistrationUpdateMISM(new DAL.Models.RegistrationSec()
										{
											RegistrationID = personalDetailsVM.RegID,

											MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,

											LastAccessID = Util.SessionAccess.UserName

										});
									}

								}
							}
							else if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM" && personalDetailsVM.RegTypeID == Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_Secp))
							{
								List<string> mobileNos = new List<string>();
								mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers());
								using (var proxyobj = new RegistrationServiceProxy())
								{
									if (mobileNos[0] == string.Empty)
										mobileNos[0] = "60179900033";
									int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
									if (res == 1)
									{
										proxyobj.RegistrationUpdate(new DAL.Models.Registration()
										{
											ID = personalDetailsVM.RegID,

											MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,

											LastAccessID = Util.SessionAccess.UserName

										});
									}

								}
							}
                            Session[SessionKey.TradeUpAmount.ToString()] = null;//14012015 - Anthony - GST Trade Up
                        }
                        var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
                        if (biometricID != 0)
                        {
                            var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
                            biometric.RegID = resp.ID;

                            proxy.BiometricUpdate(biometric);
                        }
                    }


					bool iContractActive = Convert.ToBoolean(ConfigurationManager.AppSettings["iContractActive"]);
					//bool rollOutCheck = Util.rollOutCheck();
					bool rollOutCheck = true;

					if (iContractActive)
					{
						WebHelper.Instance.SaveDocumentToTable(dropObj, resp.ID);
					}
					
					using (var proxy = new UserServiceProxy())
                    {
                        //TODO Session[SessionKey.KenanACNumber.ToString()].ToString()
                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Registration Done with RegID " + resp.ID, "", "");
                    }

                    #region VLT ADDED CODE on 19th June for Adding SimType
                    if (resp.ID > 0)
                    {
                        int regTypeID = 0;
                        if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
                        {
                            regTypeID = Util.GetRegTypeID(REGTYPE.MNPNewLine);
                        }
                        else
                        {
                            regTypeID = Util.GetRegTypeID(REGTYPE.NewLine);
                        }

						using (var proxy = new RegistrationServiceProxy())
						{
							int[] mnpOrder = new int[] { 10, 11, 12, 26 };
							if (!mnpOrder.Contains(regTypeID))
							{
								var regAttribList = new List<RegAttributes>();
                                WebHelper.Instance.ConstructRegAttributesForDF(resp.ID, ref regAttribList, out regAttribList);
                                WebHelper.Instance.ConstructRegAttributesForAF(resp.ID, ref regAttribList);
								proxy.SaveListRegAttributes(regAttribList);

							}
						}

                        //commented as disabling pos file feature
                        //#region Saving POS FileName in DB for Dealers
                        //if (Session["IsDealer"].ToBool() == true && collection["hdnTotalPayable"].ToString2() != string.Empty && collection["hdnTotalPayable"].ToDecimal() > 0)
                        //{
                        //    string posFileName = WebHelper.Instance.CreatePOSFileName(resp.ID);
                        //    using (var proxy = new CatalogServiceProxy())
                        //    {
                        //        proxy.UpdateIMPOSDetails(resp.ID, posFileName, 1);
                        //    }
                        //}
                        //#endregion

                        string PlanKenanCode = MasterDataCache.Instance.PackageComponents.Where(a => a.ID == Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"])).Select(a => a.KenanCode).SingleOrDefault().ToString2();
                        string latestPrintVersion = Util.GetPrintVersionNum(regTypeID, !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty, PlanKenanCode);
                        LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
                        Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                        objRegDetails.CreatedDate = DateTime.Now;


                        //Insert Sim Type
                        if (Session[SessionKey.SimType.ToString()].ToString2().Length > 0)
                        {
                            objRegDetails.SimType = Session[SessionKey.SimType.ToString()].ToString();
                        }

						if (simType.Equals("MISM"))
						{
							objRegDetails.SimType = simType;
						}

                        //if (Session["IsDealer"].ToBool() == false)
                        //{
                            if (!string.IsNullOrEmpty(collection["SimCardTypeSelected"]))
                                objRegDetails.SimCardType = collection["SimCardTypeSelected"].ToString2();

                            if (!string.IsNullOrEmpty(collection["SecondarySimCardTypeSelected"]))
                                objRegDetails.MismSimCardType = collection["SecondarySimCardTypeSelected"].ToString2();
                        //}

                        if (string.IsNullOrEmpty(objRegDetails.SimCardType))
                        {
                            if (!string.IsNullOrEmpty(dropObj.mainFlow.simSize))
                            {
                                objRegDetails.SimCardType = dropObj.mainFlow.simSize;
                            }
                            else
                            {
								if ( dropObj.suppIndex > 0 ) 
									objRegDetails.SimCardType = dropObj.suppFlow[dropObj.suppIndex - 1].simSize;
                            }
                        }
                        if (dropObj.msimIndex != 0)
                        {
                            objRegDetails.MismSimCardType = dropObj.msimFlow[dropObj.msimIndex - 1].simSize;
                        }

                        objRegDetails.UserName = Util.SessionAccess.UserName;
                        objRegDetails.RegId = resp.ID;
                        objRegDetails.Liberlization_Status = !ReferenceEquals(Session["CustomerLiberalisation"], null) ? Session["CustomerLiberalisation"].ToString2() : personalDetailsVM.Liberlization_Status;
                        objRegDetails.MOC_Status = personalDetailsVM.MOCStatus;
                        objRegDetails.ContractCheckCount = dropObj.additionalContract + dropObj.existingContract; //RST BRE CR
                        objRegDetails.TotalLineCheckCount = dropObj.getExistingLineCount() + dropObj.getNewLineCount();
                        bool IsWriteOff = false;
                        string acc = string.Empty;
                        WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
                        objRegDetails.WriteOffDetails = acc;

                        if (!string.IsNullOrEmpty(latestPrintVersion))
                        {
                            objRegDetails.PrintVersionNo = latestPrintVersion;
                        }
                        objRegDetailsReq.LnkDetails = objRegDetails;

                        /*Active PDPA Commented*/
                        /*Changes by chetan related to PDPA implementation*/
                        //using (var registrationProxy = new RegistrationServiceProxy())
                        //{
                        //    var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        //    objRegDetails.PdpaVersion = resultdata.Version;
                        //}

                        /*Recode Active PDPA Commented*/
                        objRegDetails.PdpaVersion = personalDetailsVM.PDPDVersion;

                        //Insert justification
						//if (collection["txtJustfication"].ToString2().Length > 0)
						//{
						//    objRegDetails.Justification = collection["txtJustfication"].ToString();
						//}

						if (!ReferenceEquals(dropObj.personalInformation, null) && !string.IsNullOrEmpty(dropObj.personalInformation.Justification))
						{
							objRegDetails.Justification = dropObj.personalInformation.Justification;
						}
						// Gerry - PN 3967 - Reserve Number
						Logger.Debug("NewLine skipkenan = " + Session["SkipKenanReservation"].ToBool());
						objRegDetails.IsMSISDNReserved = Session["SkipKenanReservation"].ToBool() ? true : false;


                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.SaveLnkRegistrationDetails(objRegDetailsReq);
                        }
                    }
                    #endregion

                    #region VLT ADDED CODE on 21stFEB for email functionality
                    if (resp.ID > 0)
                    {

                        /* BEGIN Added by VLT 5 Apr 2013 to add kenaninfo to database*/
                        if (Session["KenanCustomerInfo"] != null)
                        {
                            string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString().Split('╚');
                            KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
                            kenanInfo.FullName = strArrKenanInfo[0];
                            kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
                            kenanInfo.IDCardNo = strArrKenanInfo[2];
                            kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
                            kenanInfo.Address1 = strArrKenanInfo[4];
                            kenanInfo.Address2 = strArrKenanInfo[5];
                            kenanInfo.Address3 = strArrKenanInfo[6];
                            kenanInfo.Postcode = strArrKenanInfo[8];
                            kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
                            kenanInfo.RegID = resp.ID;
                            using (var proxys = new RegistrationServiceProxy())
                            {
                                var kenanResp = new KenanCustomerInfoCreateResp();

                                kenanResp = proxys.KenanCustomerInfoCreate(kenanInfo);
                            }
                        }
                        Session["KenanCustomerInfo"] = null;
                        Session[SessionKey.RegMobileReg_MSISDN.ToString()] = null;
                        /* END Added by VLT 5 Apr 2013 to add kenaninfo to database*/

                        //Joshi added for Delear
                        if (Roles.IsUserInRole("MREG_DSV") && false)
                        {
                            string username = string.Empty;
                            using (var proxyObj = new RegistrationServiceProxy())
                            {
                                if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                                {
                                    username = Request.Cookies["CookieUser"].Value;
                                }

                                proxyObj.RegistrationUpdate(new DAL.Models.Registration()
                                {
                                    ID = personalDetailsVM.RegID,
                                    //SalesPerson = username,
                                    LastAccessID = Util.SessionAccess.UserName,
                                });
                            }

                            if (personalDetailsVM.RegTypeID == 12)
                            {
                                #region For MNP Flow
                                // autoknockoff only for Dealer with no BRE Failed or MC.
                                if ((Session["isDealer"].ToBool() && !personalDetailsVM.isBREFail) || !Session["isDealer"].ToBool())
                                {
                                    response = FulfillKenanAccount(personalDetailsVM.RegID);
                                    if ((response.Message == "success") && (response.Code == "0"))
                                    {
                                        using (var regproxy = new RegistrationServiceProxy())
                                        {
                                            regproxy.RegistrationClose(new RegStatus()
                                            {
                                                RegID = personalDetailsVM.RegID,
                                                Active = true,
                                                CreateDT = DateTime.Now,
                                                StartDate = DateTime.Now,
                                                LastAccessID = Util.SessionAccess.UserName,
                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate),
                                                ResponseCode = response.Code,
                                                ResponseDescription = response.Message,
                                                MethodName = "KenanAdditionLineRegistration"
                                            });
                                        }
                                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                                        return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
                                    }
                                    else
                                    {
                                        using (var proxyobj = new RegistrationServiceProxy())
                                        {
                                            proxyobj.RegistrationCancel(new RegStatus()
                                            {
                                                RegID = personalDetailsVM.RegID,
                                                Active = true,
                                                CreateDT = DateTime.Now,
                                                StartDate = DateTime.Now,
                                                LastAccessID = Util.SessionAccess.UserName,
                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                                            });
                                        }
                                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region For Normal Flow
                                using (var proxyobj = new KenanServiceProxy())
                                {
                                    if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                                    {
                                        String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                        if (!ReferenceEquals(Action, String.Empty))
                                            return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                        //proxyobj.CreateIMPOSFile(personalDetailsVM.RegID);
                                        //using (var regproxy = new RegistrationServiceProxy())
                                        //{
                                        //    LnkRegDetailsReq objLnkRegDetails = regproxy.GetLnkRegistrationDetails(personalDetailsVM.RegID);
                                        //    if (!ReferenceEquals(objLnkRegDetails, null) && !ReferenceEquals(objLnkRegDetails.LnkDetails.SimType, null) && objLnkRegDetails.LnkDetails.SimType == "MISM")
                                        //    {
                                        //        Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                        //        if (!ReferenceEquals(Action, String.Empty))
                                        //            return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                        //        //proxyobj.CreateIMPOSFile(personalDetailsVM.RegID, "S");
                                        //    }
                                        //}
                                    }

                                }

                                using (var regproxy = new RegistrationServiceProxy())
                                {
                                    regproxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                        ResponseCode = response.Code,
                                        ResponseDescription = response.Message,
                                        MethodName = "KenanAdditionLineRegistration"
                                    });
                                }

                                if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() == 0))
                                {
                                    //DAL.Models.Registration regstrn;
                                    //using (var regProxy = new RegistrationServiceProxy())
                                    //{
                                    //    regstrn = regProxy.RegistrationGet(resp.ID);
                                    //}
                                    //personalDetailsVM.RegTypeID = regstrn.RegTypeID;
                                    //personalDetailsVM.RegID = resp.ID;
                                    //PaymentRecived(personalDetailsVM);
                                    response = FulfillKenanAccount(personalDetailsVM.RegID);
                                    //Save the User TransactionLog
                                    WebHelper.Instance.SaveUserTransactionLogs(resp.ID);
                                }
                                #endregion
                            }
                        }
                        //End of joshi


                        ClearRegistrationSession();
                        if ((Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("DAgent") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_C")) && false)
                        {
                            // return RedirectToAction("MobileRegSummary", new { regID = resp.ID });
                            //return RedirectToAction("MobileRegAccCreated", new { regID = resp.ID });
                            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                            if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() == 0))
                            {
                                return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });
                            }
                            else if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() > 0))
                            {
                                return RedirectToAction("MobileRegAccCreated", new { regID = resp.ID });
                            }
                            Session["NewLineTotalPayable"] = null;
                        }

                        else
                        {
                            if (Roles.IsUserInRole("MREG_DAI"))
                            {
                                using (var regproxy = new RegistrationServiceProxy())
                                {
                                    regproxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                        ResponseCode = response.Code,
                                        ResponseDescription = response.Message,
                                        MethodName = "KenanAdditionLineRegistration"
                                    });
                                }
                                if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() == 0))
                                {
                                    //DAL.Models.Registration regstrn;
                                    //using (var regProxy = new RegistrationServiceProxy())
                                    //{
                                    //    regstrn = regProxy.RegistrationGet(resp.ID);
                                    //}
                                    //personalDetailsVM.RegTypeID = regstrn.RegTypeID;
                                    //personalDetailsVM.RegID = resp.ID;
                                    //PaymentRecived(personalDetailsVM);
                                    response = FulfillKenanAccount(personalDetailsVM.RegID);
                                    //Save the User TransactionLog
                                    WebHelper.Instance.SaveUserTransactionLogs(resp.ID);
                                }
                            }
                            if (Session["IsDealer"].ToBool())
                            {
                                Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                                if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() == 0) && (Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_CUSR")))
                                {
                                    return RedirectToAction("MobileRegSuccessNew", new { regID = resp.ID });
                                }
                                else if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() == 0))
                                {
                                    return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });
                                }
                                else if ((!string.IsNullOrEmpty(Session["NewLineTotalPayable"].ToString2())) && (Session["NewLineTotalPayable"].ToDecimal() > 0))
                                {
                                    return RedirectToAction("MobileRegSuccessNew", new { regID = resp.ID });
                                }
                                else {
                                    return RedirectToAction("MobileRegSuccessNew", new { regID = resp.ID });
                                }
                            }
                            else
                            {
                                Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                                return RedirectToAction("MobileRegSuccessNew", new { regID = resp.ID });
                            }
                        }
                    }
                    #endregion VLT ADDED CODE on 21stFEB for email functionality
                }
                else if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.PersonalDetails)
                {
                    Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                    return RedirectToAction("PersonalDetailsNew");
                }

            }
            catch (Exception ex)
            {
                Logger.Info("Throw Exception for Order ID:" + resp.ID + "Exception:" + ex);
                LogExceptions(ex);
                throw ex;
            }
            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
            return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
        }

        public int GetRegStatus(int regId)
        {
            DAL.Models.Registration reg = new DAL.Models.Registration();
            List<DAL.Models.WaiverComponents> objWaiverComponents = new List<WaiverComponents>();
            int statusID = 0;
            //statusID = WebHelper.Instance.GetRegStatus(regId);
            if ((Roles.IsUserInRole("MREG_DIC") || Roles.IsUserInRole("MREG_DAI") || Roles.IsUserInRole("MREG_DSV")))
            {
                using (var regProxy = new RegistrationServiceProxy())
                {
                    reg = regProxy.RegistrationGet(regId);
                    objWaiverComponents = regProxy.GetWaiverComponentsbyRegID(regId);
                }
            }
            statusID = WebHelper.Instance.GetRegStatus(regId);
            bool isWaived = true;
            if ((Roles.IsUserInRole("MREG_DIC") || Roles.IsUserInRole("MREG_DAI") || Roles.IsUserInRole("MREG_DSV")))
            {
                int regtypeID = reg.RegTypeID;
                if (regtypeID > 0 & regtypeID <= 3)
                {
                    regtypeID = regtypeID - 1;
                }

				int waivedCount = 0;

                if (ConfigurationManager.AppSettings["ReadyForPaymentStatusID"].ToInt() == statusID && reg != null && (regtypeID == (int)MobileRegType.MNPNewLine || regtypeID==(int)MobileRegType.NewLine))
                {
                    foreach (var waiverComp in objWaiverComponents)
                    {
                        if (waiverComp.IsWaived == false)
                        {
                            isWaived = false;
                            break;
                        }
                    }

					decimal totalPrice = Util.getPrinSuppTotalPrice(regId, IMEInumberStored: true);
					if (totalPrice == 0)
					{
						if (reg.RegTypeID == (int)MobileRegType.MNPNewLine)
						{
							#region For MNP Flow
							using (var proxy = new RegistrationServiceProxy())
							{
								proxy.RegistrationCancel(new RegStatus()
								{
									RegID = regId,
									Active = true,
									CreateDT = DateTime.Now,
									StartDate = DateTime.Now,
									LastAccessID = Util.SessionAccess.UserName,
									StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
								});
							}
							using (var proxy = new KenanServiceProxy())
							{

								var request1 = new MNPtypeFullfillCenterOrderMNP()
								{
									orderId = regId.ToString(),
								};
								bool statusMNPNew = proxy.KenanMNPAccountFulfill(request1);
								if (statusMNPNew)
								{
									return -500;
								}

							}

							#endregion
						}
						else
						{

							var request = new OrderFulfillRequest()
							{
								OrderID = regId.ToString(),
								UserSession = Util.SessionAccess.UserName,
							};
							using (var proxy = new KenanServiceProxy())
							{
								PersonalDetailsVM PVm = new PersonalDetailsVM();
								PVm.RegID = reg.ID;
								PVm.RegTypeID = reg.RegTypeID;
								PaymentRecived(PVm);
							}
							return -500;
						}
					}
					
                }
            }
            return statusID;
        }


		private List<RegPgmBdlPkgComp> ConstructRegPgmBdlPkgComponent()
		{
			var regPBPCs = new List<RegPgmBdlPkgComp>();
			var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

			if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DeviceOnly)
				return regPBPCs;

			// Plan PgmBdlPkgComponent
			regPBPCs.Add(new RegPgmBdlPkgComp()
			{
				RegSuppLineID = null,
				//PgmBdlPckComponentID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(),
				PgmBdlPckComponentID = dropObj.mainFlow.PkgPgmBdlPkgCompID.ToInt(),
				IsNewAccount = true,
				Active = true,
				CreateDT = DateTime.Now,
				LastAccessID = Util.SessionAccess.UserName
			});
			if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.SecPlan)
			{
				// Plan PgmBdlPkgComponent
				regPBPCs.Add(new RegPgmBdlPkgComp()
				{
					RegSuppLineID = null,
					PgmBdlPckComponentID = ConfigurationManager.AppSettings["IncludeMandatoryPackageMISM"].ToInt(),
					IsNewAccount = true,
					Active = true,
					CreateDT = DateTime.Now,
					LastAccessID = Util.SessionAccess.UserName
				});
			}
			// Mandatory VAS PgmBdlPkgComponent
			var vasPBPCIDs = new List<int>();
			using (var proxy = new CatalogServiceProxy())
			{
				var pkgID = MasterDataCache.Instance.FilterComponents(new int[] { dropObj.mainFlow.PkgPgmBdlPkgCompID.ToInt() }).SingleOrDefault().ChildID;

				vasPBPCIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
				{
					IsMandatory = true,
					PgmBdlPckComponent = new PgmBdlPckComponent()
					{
						ParentID = pkgID,
						LinkType = Properties.Settings.Default.Package_Component
					},
					Active = true
				}).ToList();

				foreach (var vasPBPCID in vasPBPCIDs)
				{
					regPBPCs.Add(new RegPgmBdlPkgComp()
					{
						RegSuppLineID = null,
						PgmBdlPckComponentID = vasPBPCID,
						IsNewAccount = true,
						Active = true,
						CreateDT = DateTime.Now,
						LastAccessID = Util.SessionAccess.UserName
					});
				}
			}

			// Optional VAS PgmBdlPkgComponent
			//var vasIDs = Session["RegMobileReg_VasIDs"].ToString2().Split(',');
			var vasIDs = dropObj.mainFlow.vas.SelectedVasIDs.ToString2().Split(',');	
			// vasIDs[vasIDs.Length] = session[SessionKey.RegMobileReg_DataplanID.ToString()].ToString();
			// session[SessionKey.RegMobileReg_DataplanID.ToString()]

			foreach (var vasID in vasIDs)
			{
				if (!string.IsNullOrEmpty(vasID.ToString()))
				{
					regPBPCs.Add(new RegPgmBdlPkgComp()
					{
						PgmBdlPckComponentID = vasID.ToInt(),
						IsNewAccount = true,
						Active = true,
						CreateDT = DateTime.Now,
						LastAccessID = Util.SessionAccess.UserName
					});
				}
			}
			if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.SecPlan)
			{
				regPBPCs.Add(new RegPgmBdlPkgComp()
				{
					PgmBdlPckComponentID = ConfigurationManager.AppSettings["IncludeMandatoryComponentMISM"].ToInt(),
					IsNewAccount = true,
					Active = true,
					CreateDT = DateTime.Now,
					LastAccessID = Util.SessionAccess.UserName
				});
			}

			List<int> lstDPIds = new List<int>();
			string depcompIds = string.Empty;
			if (!ReferenceEquals(Session[SessionKey.RegMobileReg_DataplanID.ToString()], null))
			{
				try
				{
					int[] ids = (int[])Session[SessionKey.RegMobileReg_DataplanID.ToString()];
					foreach (int id in ids)
						lstDPIds.Add(id);
				}
				catch (Exception ex)
				{
					WebHelper.Instance.LogExceptions(this.GetType(), ex);
					lstDPIds.Add(Convert.ToInt32(Session[SessionKey.RegMobileReg_DataplanID.ToString()]));
				}
				depcompIds = WebHelper.Instance.GetDepenedencyComponents(string.Join(",", lstDPIds), false);


			}
			List<string> depListCompIds = new List<string>();
			depListCompIds = depcompIds.Split(',').ToList();

			foreach (string compid in depListCompIds)
			{

				if (compid != string.Empty && Convert.ToInt32(compid) > 0)
				{
					regPBPCs.Add(new RegPgmBdlPkgComp()
					{
						PgmBdlPckComponentID = compid.ToInt(),
						IsNewAccount = true,
						Active = true,
						CreateDT = DateTime.Now,
						LastAccessID = Util.SessionAccess.UserName
					});
				}

			}
			/*Mandatory package*/
            if (!string.IsNullOrEmpty(dropObj.mainFlow.MandatoryPackage) && Convert.ToInt32(dropObj.mainFlow.MandatoryPackage) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = dropObj.mainFlow.MandatoryPackage.ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            /*data plan*/
            if (!string.IsNullOrEmpty(dropObj.mainFlow.DataPackage))
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = dropObj.mainFlow.DataPackage.ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }

			/*extra plan*/
            if (!string.IsNullOrEmpty(dropObj.mainFlow.ExtraPackage) && Convert.ToInt32(dropObj.mainFlow.ExtraPackage) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = dropObj.mainFlow.ExtraPackage.ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
			/*Pramotional plan*/
            if (dropObj.mainFlow.PromotionPackage.ToInt() != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = dropObj.mainFlow.PromotionPackage.ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
			/*discount plan*/
            if (!string.IsNullOrEmpty(dropObj.mainFlow.DiscountPackage) && Convert.ToInt32(dropObj.mainFlow.DiscountPackage) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = dropObj.mainFlow.DiscountPackage.ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
			/*Insurance package*/
            if (!string.IsNullOrEmpty(dropObj.mainFlow.InsurancePackage) && Convert.ToInt32(dropObj.mainFlow.InsurancePackage) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = dropObj.mainFlow.InsurancePackage.ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }

			/*DF package*/
			if (!string.IsNullOrEmpty(dropObj.mainFlow.DeviceFinancingPackage) && Convert.ToInt32(dropObj.mainFlow.DeviceFinancingPackage) != 0)
			{
				regPBPCs.Add(new RegPgmBdlPkgComp()
				{
					PgmBdlPckComponentID = dropObj.mainFlow.DeviceFinancingPackage.ToInt(),
					IsNewAccount = true,
					Active = true,
					CreateDT = DateTime.Now,
					LastAccessID = Util.SessionAccess.UserName
				});
			}

			return regPBPCs;
		}


        private void PaymentRecived(PersonalDetailsVM personalDetailsVM)
        {
            var response = new KenanTypeAdditionLineRegistrationResponse();
            var request = new OrderFulfillRequest()
            {
                OrderID = personalDetailsVM.RegID.ToString(),
                UserSession = Util.SessionAccess.UserName,
            };
            var request1 = new MNPtypeFullfillCenterOrderMNP()
            {
                orderId = personalDetailsVM.RegID.ToString(),
            };
            //Added for MISM virtual numbers flow
            if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM" && personalDetailsVM.RegTypeID != Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_Secp))
            {
                //var proxy = new retrieveServiceInfoProxy();
                List<string> mobileNos = new List<string>();
                mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers(true));
                using (var proxyobj = new RegistrationServiceProxy())
                {
                    if (mobileNos[0] == string.Empty)
                        mobileNos[0] = "60179900033";
                    int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
                    if (res == 1)
                    {
                        proxyobj.RegistrationUpdateMISM(new DAL.Models.RegistrationSec()
                        {
                            RegistrationID = personalDetailsVM.RegID,
                            MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }
                }
            }
            else if (Convert.ToString(Session["SimType"]) == "MISM" && personalDetailsVM.RegTypeID == Util.GetIDByCode(RefType.RegType, Settings.Default.RegType_Secp))
            {
                //var proxy = new retrieveServiceInfoProxy();
                List<string> mobileNos = new List<string>();
                mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers(true));
                using (var proxyobj = new RegistrationServiceProxy())
                {
                    if (mobileNos[0] == string.Empty)
                        mobileNos[0] = "60179900033";
                    int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
                    if (res == 1)
                    {
                        proxyobj.RegistrationUpdate(new DAL.Models.Registration()
                        {
                            ID = personalDetailsVM.RegID,
                            MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }
                }
            }
            using (var proxy = new RegistrationServiceProxy())
            {
                proxy.RegistrationCancel(new RegStatus()
                {
                    RegID = personalDetailsVM.RegID,
                    Active = true,
                    CreateDT = DateTime.Now,
                    StartDate = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName,
                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                });
            }
            if (personalDetailsVM.RegTypeID == 10 || personalDetailsVM.RegTypeID == 26)
            {
                using (var proxy = new KenanServiceProxy())
                {
                    proxy.KenanMNPAccountFulfill(request1);
                }
            }
            else if (personalDetailsVM.RegTypeID == (int)MobileRegType.CRP)
            {
                using (var proxy = new KenanServiceProxy())
                {
                    CenterOrderContractRequest crpOrder = new CenterOrderContractRequest();
                    crpOrder.orderId = personalDetailsVM.RegID.ToString();
                    proxy.CenterOrderContractCreation(crpOrder);
                }
            }
            else if (personalDetailsVM.RegTypeID == 12)
            {
                #region For MNP Flow

                using (var proxy = new KenanServiceProxy())
                {

                    var request2 = new MNPtypeFullfillCenterOrderMNP()
                    {
                        orderId = personalDetailsVM.RegID.ToString(),
                    };
                    bool statusMNPNew = proxy.KenanMNPAccountFulfill(request2);
                    if (statusMNPNew)
                    {
                        
                    }
                    else
                    {
                        using (var regproxy = new RegistrationServiceProxy())
                        {
                            regproxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail),
                                ResponseCode = response.Code,
                                ResponseDescription = response.Message,
                                MethodName = "KenanAdditionLineRegistration"
                            });
                        }                        
                    }
                }

                #endregion
            }
            else
            {
                #region For Normal Flow

                response = FulfillKenanAccount(personalDetailsVM.RegID);
                
                #endregion
            }

            //else
            //{
            //    using (var proxy = new KenanServiceProxy())
            //    {
            //        proxy.KenanAccountFulfill(request);
            //    }
            //}
        }
        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,DSupervisor,MREG_DSV,DREG_N,DREG_C,DREG_CH,MREG_DAI,MREG_DIC,MREG_DAC,MREG_DSV,MREG_DSA")]
        public ActionResult DMobileRegSummary(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            var resp = new RegistrationCreateResp();
            var response = new KenanTypeAdditionLineRegistrationResponse();
            var reg = new DAL.Models.Registration();
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = personalDetailsVM.TabNumber;
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = personalDetailsVM.ExtraTenMobileNumbers;
            try
            {

                if (collection["submit1"].ToString() == "PaymentRecived")
                {
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                        personalDetailsVM.RegTypeID = reg.RegTypeID;
                    }
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                        });
                    }

                    //Added for MISM virtual numbers flow

                    if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM" && personalDetailsVM.RegTypeID != Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_Secp))
                    {

                        List<string> mobileNos = new List<string>();
                        mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers(true));
                        using (var proxyobj = new RegistrationServiceProxy())
                        {
                            if (mobileNos[0] == string.Empty)
                                mobileNos[0] = "60179900033";
                            int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
                            if (res == 1)
                            {
                                proxyobj.RegistrationUpdateMISM(new DAL.Models.RegistrationSec()
                                {
                                    RegistrationID = personalDetailsVM.RegID,

                                    MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,

                                    LastAccessID = Util.SessionAccess.UserName

                                });
                            }

                        }
                    }

                    else if (Convert.ToString(Session[SessionKey.SimType.ToString()]) == "MISM" && personalDetailsVM.RegTypeID == Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_Secp))
                    {
                        List<string> mobileNos = new List<string>();
                        mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers());
                        using (var proxyobj = new RegistrationServiceProxy())
                        {
                            if (mobileNos[0] == string.Empty)
                                mobileNos[0] = "60179900033";
                            int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
                            if (res == 1)
                            {
                                proxyobj.RegistrationUpdate(new DAL.Models.Registration()
                                {
                                    ID = personalDetailsVM.RegID,

                                    MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,

                                    LastAccessID = Util.SessionAccess.UserName

                                });
                            }

                        }
                    }

                    if (personalDetailsVM.RegTypeID == 12)
                    {
                        #region For MNP Flow

                        using (var proxy = new KenanServiceProxy())
                        {

                            var request1 = new MNPtypeFullfillCenterOrderMNP()
                            {
                                orderId = personalDetailsVM.RegID.ToString(),
                            };
                            bool statusMNPNew = proxy.KenanMNPAccountFulfill(request1);
                            if (statusMNPNew)
                            {
                                return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                            }
                            else
                            {
                                using (var regproxy = new RegistrationServiceProxy())
                                {
                                    regproxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail),
                                        ResponseCode = response.Code,
                                        ResponseDescription = response.Message,
                                        MethodName = "KenanAdditionLineRegistration"
                                    });
                                }
                                return RedirectToAction("MobileRegWMFail", new { regID = personalDetailsVM.RegID });
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        #region For Normal Flow

                        response = FulfillKenanAccount(personalDetailsVM.RegID);
                        return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                        #endregion
                    }
                }

                #region code for implementing the epetrol changes
                //if (collection["submit1"].ToString() == "MakePayment")
                //{
                //    return Redirect("../PaymentDetails?id=" + personalDetailsVM.RegID);
                //}
                #endregion

                if (collection["submit1"].ToString() == "close")
                {
                    if (personalDetailsVM.RegID == 0)
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resp = proxy.RegistrationCreateWithSec(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString() : "false")),
                            WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]),
                            WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
                           WebHelper.Instance.ConstructRegSuppLineVASes(), ConstructRegistrationForSec(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo),
                            ConstructRegPgmBdlPkgSecComponent(), WebHelper.Instance.ConstructRegMdlGroupModelSec(), ConstructSmartComponents(), null, personalDetailsVM.isBREFail);
                            personalDetailsVM.RegID = resp.ID;
                        }
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegBreFail)
                            });
                        }
                        return RedirectToAction("MobileRegBreFail", new { regID = personalDetailsVM.RegID });
                    }
                    else
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                            });
                        }
                        return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
                    }


                }

                if (collection["submit1"].ToString() == "cancel")
                {
                    string reason = string.Empty;
                    reason = ReferenceEquals(reason, null) ? "No reason provided." : collection["hdnReasonCancel"].ToString();
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)

                        });

                        proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                        {
                            RegID = personalDetailsVM.RegID,
                            CancelReason = reason,
                            CreateDT = DateTime.Now,
                            LastUpdateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }

                    return RedirectToAction("MobileRegCanceled", new { regID = personalDetailsVM.RegID });
                }

                if (collection["submit1"].ToString() == "createAcc")
                {
                    try
                    {
                        #region "sales person update"
                        string username = string.Empty;
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                            {
                                username = Request.Cookies["CookieUser"].Value;
                            }

                            proxy.RegistrationUpdate(new DAL.Models.Registration()
                            {
                                ID = personalDetailsVM.RegID,
                                SalesPerson = username,
                                LastAccessID = Util.SessionAccess.UserName,
                            });
                        }

                        #endregion
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                            personalDetailsVM.RegTypeID = reg.RegTypeID;
                        }
                        if (personalDetailsVM.RegTypeID == 12)
                        {
                            #region For MNP Flow
                            response = FulfillKenanAccount(personalDetailsVM.RegID);
                            if ((response.Message == "success") && (response.Code == "0"))
                            {
                                using (var regproxy = new RegistrationServiceProxy())
                                {
                                    regproxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate),
                                        ResponseCode = response.Code,
                                        ResponseDescription = response.Message,
                                        MethodName = "KenanAdditionLineRegistration"
                                    });
                                }
                                return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
                            }
                            else
                            {
                                using (var proxy = new RegistrationServiceProxy())
                                {
                                    proxy.RegistrationCancel(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                                    });
                                }
                                return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                            }
                            #endregion
                        }
                        else
                        {
                            #region For Normal Flow
                            using (var proxy = new KenanServiceProxy())
                            {
                                if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                                {
                                    String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    if (!ReferenceEquals(Action, String.Empty))
                                        return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //proxy.CreateIMPOSFile(personalDetailsVM.RegID);
                                    //using (var regproxy = new RegistrationServiceProxy())
                                    //{
                                    //    LnkRegDetailsReq objLnkRegDetails = regproxy.GetLnkRegistrationDetails(personalDetailsVM.RegID);
                                    //    if (!ReferenceEquals(objLnkRegDetails, null) && !ReferenceEquals(objLnkRegDetails.LnkDetails.SimType, null) && objLnkRegDetails.LnkDetails.SimType == "MISM")
                                    //    {
                                    //        Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    //        if (!ReferenceEquals(Action, String.Empty))
                                    //            return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //        //proxy.CreateIMPOSFile(personalDetailsVM.RegID, "S");
                                    //    }
                                    //}
                                }

                            }
                            using (var regproxy = new RegistrationServiceProxy())
                            {
                                regproxy.RegistrationClose(new RegStatus()
                                {
                                    RegID = personalDetailsVM.RegID,
                                    Active = true,
                                    CreateDT = DateTime.Now,
                                    StartDate = DateTime.Now,
                                    LastAccessID = Util.SessionAccess.UserName,
                                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                    ResponseCode = response.Code,
                                    ResponseDescription = response.Message,
                                    MethodName = "KenanAdditionLineRegistration"
                                });
                            }
                            if (Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("MREG_DSV"))
                            {
                                return RedirectToAction("DMobileRegSummary", new { regID = personalDetailsVM.RegID });
                            }
                            else
                            {
                                return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
                            }
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        LogException(ex);

                        LogExceptions(ex);

                    }
                }

                if (collection["submit1"].ToString() == "activateSvc")
                {
                    try
                    {
                        #region "sales person update"
                        string username = string.Empty;
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                            {
                                username = Request.Cookies["CookieUser"].Value;
                            }

                            proxy.RegistrationUpdate(new DAL.Models.Registration()
                            {
                                ID = personalDetailsVM.RegID,
                                SalesPerson = username,
                                LastAccessID = Util.SessionAccess.UserName,
                            });
                        }

                        #endregion
                        response = FulfillKenanAccount(personalDetailsVM.RegID);
                        if (response.Message == "success")
                        {
                            //BEGIN   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                if (personalDetailsVM.ExtraTenLimitConfirmation == "true")
                                {

                                    if (personalDetailsVM.hdnExtraTenCount != null && personalDetailsVM.hdnExtraTenCount != string.Empty)
                                    {
                                        proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                        {
                                            ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                            {
                                                ConfirmedBy = Util.SessionAccess.UserName,
                                                ConfirmedStatus = "Y",
                                                Count = Convert.ToInt32(personalDetailsVM.hdnExtraTenCount),
                                                CreatedDate = System.DateTime.Now,
                                                RegId = personalDetailsVM.RegID
                                            }
                                        });
                                    }
                                }

                                if (personalDetailsVM.ExtraTenConfirmation == "true")
                                {

                                    proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                    {
                                        ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                        {
                                            ConfirmedBy = Util.SessionAccess.UserName,
                                            ConfirmedStatus = "Y",
                                            CreatedDate = System.DateTime.Now,
                                            RegId = personalDetailsVM.RegID
                                        }
                                    });

                                }
                                else
                                {
                                    proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                    {
                                        ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                        {
                                            ConfirmedBy = Util.SessionAccess.UserName,
                                            ConfirmedStatus = "N",
                                            CreatedDate = System.DateTime.Now,
                                            RegId = personalDetailsVM.RegID
                                        }
                                    });
                                }


                                proxy.DeleteExtraTenDetails(personalDetailsVM.RegID);
                                if (personalDetailsVM.ExtraTenConfirmation == "true")
                                {
                                    foreach (var extratenMSISDN in personalDetailsVM.ExtraTenMobileNumbers.Split(','))
                                    {
                                        if (extratenMSISDN != string.Empty)
                                        {
                                            proxy.SaveExtraTen(new ExtraTenReq
                                            {

                                                ExtraTenDetails = new lnkExtraTenDetails { CreatedBy = Util.SessionAccess.UserName, CreatedDate = System.DateTime.Now, Msisdn = extratenMSISDN, RegId = personalDetailsVM.RegID, Status = "A" }


                                            });
                                        }
                                    }
                                }
                            }
                            //END   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database
                            if (Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("MREG_DSV"))
                            {
                                return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                            }
                            else
                            {
                                return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                            }
                        }
                        else
                        {
                            return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
                        }
                    }
                    catch (Exception ex)
                    {
                        LogException(ex);

                        LogExceptions(ex);

                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                    }

                }
                int stepNo = 0;
                if (Session[SessionKey.SimType.ToString()] != null && Session[SessionKey.SimType.ToString()].ToString() == "MISM")
                    stepNo = (int)MobileRegistrationSteps.Submit + 2;
                else
                    stepNo = (int)MobileRegistrationSteps.Submit;
                if (personalDetailsVM.TabNumber == stepNo)
                {
                    if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == false || Session["NumberType"].ToString2() == "n")
                    {
                        if (Session[SessionKey.NumberType.ToString()].ToString2() == "n")
                        {
                            // Update Dealer Mobile No
                            var kenanResp = new MobileNoUpdateResponse();
                            using (var proxy = new KenanServiceProxy())
                            {
                                kenanResp = proxy.MobileNoUpdate(WebHelper.Instance.ConstructMobileNoUpdate());
                            }

                            if (!kenanResp.Success)
                            {
                                personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

                                ModelState.AddModelError(string.Empty, string.Format("Failed to update Dealer Mobile Number ({0}).",
                                    ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2()));

                                return View("MobileRegSummary", personalDetailsVM);
                            }
                        }
                        // submit registration
                    }
                    using (var proxy = new RegistrationServiceProxy())
                    {



                        // 20141204 - Read the waiver components from the dropFourObj - start
                        var dropFourObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                        List<DAL.Models.WaiverComponents> objWaiverComp = Util.constructWaiverComponentList(dropFourObj);

                        if (objWaiverComp == null || objWaiverComp.Count == 0)
                        {
                            //Constrct waiver components by ravi on sep 25 2013 by ravi
                            objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                            //Constrct waiver components by ravi on sep 25 2013 by ravi ends here
                        }
                        // 20141204 - Read the waiver components from the dropFourObj - start


                        resp = proxy.RegistrationCreateWithSec(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString() : "false")),
                           WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]),
                            WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
                          WebHelper.Instance.ConstructRegSuppLineVASes(), ConstructRegistrationForSec(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelectedMism"]) ? collection["SIMModelSelectedMism"].ToString() : "false")),
                           ConstructRegPgmBdlPkgSecComponent(), WebHelper.Instance.ConstructRegMdlGroupModelSec(), ConstructSmartComponents(), objWaiverComp, personalDetailsVM.isBREFail);
                        personalDetailsVM.RegID = resp.ID;

                        if (resp != null && resp.ID.ToInt() > 0)
                        {
                            if (!string.IsNullOrEmpty(personalDetailsVM.Photo) || !string.IsNullOrEmpty(personalDetailsVM.CustomerPhoto) || !string.IsNullOrEmpty(personalDetailsVM.AltCustomerPhoto))
                            {
                                proxy.SaveDocumentToDB(personalDetailsVM.Photo, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.RegID);
                            }

                            ///BYPASS THE BELOW CODE FOR Extra Ten IF REQUEST FROM Multiple MNP
                            bool isMultipleMNP = personalDetailsVM.RegTypeID == (int)MobileRegType.RegType_MNPPlanWithMultiSuppline ? true : false;
                            if (!isMultipleMNP)
                                Util.SaveExtraTen(personalDetailsVM.ExtraTenMobileNumbers, resp.ID);
                        }
                        var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
                        if (biometricID != 0)
                        {
                            var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
                            biometric.RegID = resp.ID;

                            proxy.BiometricUpdate(biometric);
                        }
                    }
                    using (var proxy = new UserServiceProxy())
                    {
                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Registration Done with RegID " + resp.ID, Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                    }

                    #region VLT ADDED CODE on 19th June for Adding SimType
                    if (resp.ID > 0)
                    {
                        int regTypeID = 0;
                        if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
                        {
                            regTypeID = Util.GetRegTypeID(REGTYPE.MNPNewLine);
                        }
                        else
                        {
                            regTypeID = Util.GetRegTypeID(REGTYPE.NewLine);
                        }

                        string PlanKenanCode = MasterDataCache.Instance.PackageComponents.Where(a => a.ID == Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"])).Select(a => a.KenanCode).SingleOrDefault().ToString2();
                        string latestPrintVersion = Util.GetPrintVersionNum(regTypeID, !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty, PlanKenanCode);
                        LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
                        Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                        objRegDetails.CreatedDate = DateTime.Now;


                        //Insert Sim Type
                        if (Session[SessionKey.SimType.ToString()].ToString2().Length > 0)
                        {
                            objRegDetails.SimType = Session[SessionKey.SimType.ToString()].ToString();
                        }

                        objRegDetails.UserName = Util.SessionAccess.UserName;
                        objRegDetails.RegId = resp.ID;
                        objRegDetails.Liberlization_Status = personalDetailsVM.Liberlization_Status;
                        objRegDetails.MOC_Status = personalDetailsVM.MOCStatus;
                        if (!string.IsNullOrEmpty(latestPrintVersion))
                        {
                            objRegDetails.PrintVersionNo = latestPrintVersion;
                        }

                        //******selected sim card type is assigned*****//
                        if (!string.IsNullOrEmpty(collection["SimCardTypeSelected"]))
                            objRegDetails.SimCardType = collection["SimCardTypeSelected"].ToString2();

                        if (!string.IsNullOrEmpty(collection["SecondarySimCardTypeSelected"]))
                            objRegDetails.MismSimCardType = collection["SecondarySimCardTypeSelected"].ToString2();


                        //******selected sim card type is assigned*****//

                        objRegDetailsReq.LnkDetails = objRegDetails;

                        /*Active PDPA Commented*/
                        /*Changes by chetan related to PDPA implementation*/
                        //using (var registrationProxy = new RegistrationServiceProxy())
                        //{
                        //    var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        //    objRegDetails.PdpaVersion = resultdata.Version;
                        //}

                        /*Recode Active PDPA Commented*/
                        objRegDetails.PdpaVersion = personalDetailsVM.PDPDVersion;

                        //Insert justification
                        if (collection["txtJustfication"].ToString2().Length > 0)
                        {
                            objRegDetails.Justification = collection["txtJustfication"].ToString();
                        }
                        if (Session[SessionKey.NumberType.ToString()].ToString2() == "g")
                        {

                            objRegDetails.IsMSISDNReserved = true;
                        }
                        else
                        {
                            objRegDetails.IsMSISDNReserved = false;
                        }
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.SaveLnkRegistrationDetails(objRegDetailsReq);
                        }

                        Session[SessionKey.NumberType.ToString()] = null;
                    }
                    #endregion

                    #region VLT ADDED CODE on 21stFEB for email functionality
                    if (resp.ID > 0)
                    {

                        /* BEGIN Added by VLT 5 Apr 2013 to add kenaninfo to database*/
                        if (Session["KenanCustomerInfo"] != null)
                        {
                            string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString().Split('╚');
                            KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
                            kenanInfo.FullName = strArrKenanInfo[0];
                            kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
                            kenanInfo.IDCardNo = strArrKenanInfo[2];
                            kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
                            kenanInfo.Address1 = strArrKenanInfo[4];
                            kenanInfo.Address2 = strArrKenanInfo[5];
                            kenanInfo.Address3 = strArrKenanInfo[6];
                            kenanInfo.Postcode = strArrKenanInfo[8];
                            kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
                            kenanInfo.RegID = resp.ID;
                            using (var proxys = new RegistrationServiceProxy())
                            {
                                var kenanResp = new KenanCustomerInfoCreateResp();

                                kenanResp = proxys.KenanCustomerInfoCreate(kenanInfo);
                            }
                        }
                        Session["KenanCustomerInfo"] = null;
                        /* END Added by VLT 5 Apr 2013 to add kenaninfo to database*/

                        //Joshi added for Delear
                        string username = string.Empty;
                        using (var proxyObj = new RegistrationServiceProxy())
                        {
                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                            {
                                username = Request.Cookies["CookieUser"].Value;
                            }

                            proxyObj.RegistrationUpdate(new DAL.Models.Registration()
                            {
                                ID = personalDetailsVM.RegID,
                                SalesPerson = username,
                                LastAccessID = Util.SessionAccess.UserName,
                            });
                        }

                        using (var proxyobj = new RegistrationServiceProxy())
                        {
                            reg = proxyobj.RegistrationGet(personalDetailsVM.RegID);
                            personalDetailsVM.RegTypeID = reg.RegTypeID;
                        }
                        if (personalDetailsVM.RegTypeID == 12)
                        {
                            #region For MNP Flow
                            response = FulfillKenanAccount(personalDetailsVM.RegID);
                            if ((response.Message == "success") && (response.Code == "0"))
                            {
                                using (var regproxy = new RegistrationServiceProxy())
                                {
                                    regproxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate),
                                        ResponseCode = response.Code,
                                        ResponseDescription = response.Message,
                                        MethodName = "KenanAdditionLineRegistration"
                                    });
                                }
                                return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
                            }
                            else
                            {
                                using (var proxyobj = new RegistrationServiceProxy())
                                {
                                    proxyobj.RegistrationCancel(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                                    });
                                }
                                return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                            }
                            #endregion
                        }
                        else
                        {
                            #region For Normal Flow
                            using (var proxyobj = new KenanServiceProxy())
                            {
                                if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                                {
                                    String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    if (!ReferenceEquals(Action, String.Empty))
                                        return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //proxyobj.CreateIMPOSFile(personalDetailsVM.RegID);
                                    //using (var regproxy = new RegistrationServiceProxy())
                                    //{
                                    //    LnkRegDetailsReq objLnkRegDetails = regproxy.GetLnkRegistrationDetails(personalDetailsVM.RegID);
                                    //    if (!ReferenceEquals(objLnkRegDetails, null) && !ReferenceEquals(objLnkRegDetails.LnkDetails.SimType, null) && objLnkRegDetails.LnkDetails.SimType == "MISM")
                                    //    {
                                    //        Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    //        if (!ReferenceEquals(Action, String.Empty))
                                    //            return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //        //proxyobj.CreateIMPOSFile(personalDetailsVM.RegID, "S");
                                    //    }
                                    //}
                                }

                            }
                            using (var regproxy = new RegistrationServiceProxy())
                            {
                                regproxy.RegistrationClose(new RegStatus()
                                {
                                    RegID = personalDetailsVM.RegID,
                                    Active = true,
                                    CreateDT = DateTime.Now,
                                    StartDate = DateTime.Now,
                                    LastAccessID = Util.SessionAccess.UserName,
                                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                    ResponseCode = response.Code,
                                    ResponseDescription = response.Message,
                                    MethodName = "KenanAdditionLineRegistration"
                                });
                            }
                            #endregion
                        }
                        //End of joshi

                        ClearRegistrationSession();
                        if (Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("DAgent") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_C") || Roles.IsUserInRole("MREG_DAI"))
                        {
                            return RedirectToAction("DMobileRegSummary", new { regID = resp.ID });
                        }
                        else
                        {
                            return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                        }

                    }
                    #endregion VLT ADDED CODE on 21stFEB for email functionality
                }
                else if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.PersonalDetails)
                {
                    return RedirectToAction("PersonalDetailsNew");
                }

            }
            catch (Exception ex)
            {
                Logger.Info("Throw Exception for Order ID:" + resp.ID + "Exception:" + ex);
                LogExceptions(ex);
                throw ex;
            }

            return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
        }

        private ActionResult CreateAccountKenan(PersonalDetailsVM personalDetailsVM)
        {
            try
            {
                #region "sales person update"
                var reg = new DAL.Models.Registration();
                var response = new KenanTypeAdditionLineRegistrationResponse();
                string username = string.Empty;
                using (var proxy = new RegistrationServiceProxy())
                {
                    if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                    {
                        username = Request.Cookies["CookieUser"].Value;
                    }

                    proxy.RegistrationUpdate(new DAL.Models.Registration()
                    {
                        ID = personalDetailsVM.RegID,
                        SalesPerson = username,
                        LastAccessID = Util.SessionAccess.UserName,
                    });
                }

                #endregion
                using (var proxy = new RegistrationServiceProxy())
                {
                    reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                    personalDetailsVM.RegTypeID = reg.RegTypeID;
                }
                if (personalDetailsVM.RegTypeID == 12)
                {
                    #region For MNP Flow
                    response = FulfillKenanAccount(personalDetailsVM.RegID);
                    if ((response.Message == "success") && (response.Code == "0"))
                    {
                        using (var regproxy = new RegistrationServiceProxy())
                        {
                            regproxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate),
                                ResponseCode = response.Code,
                                ResponseDescription = response.Message,
                                MethodName = "KenanAdditionLineRegistration"
                            });
                        }
                        return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
                    }
                    else
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                    }
                    #endregion
                }
                else
                {
                    #region For Normal Flow
                    using (var proxy = new KenanServiceProxy())
                    {
                        if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                        {
                            String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                            if (!ReferenceEquals(Action, String.Empty))
                                return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                            //proxy.CreateIMPOSFile(personalDetailsVM.RegID);
                            using (var regproxy = new RegistrationServiceProxy())
                            {
                                LnkRegDetailsReq objLnkRegDetails = regproxy.GetLnkRegistrationDetails(personalDetailsVM.RegID);
                                if (!ReferenceEquals(objLnkRegDetails, null) && !ReferenceEquals(objLnkRegDetails.LnkDetails.SimType, null) && objLnkRegDetails.LnkDetails.SimType == "MISM")
                                {
                                    Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    if (!ReferenceEquals(Action, String.Empty))
                                        return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //proxy.CreateIMPOSFile(personalDetailsVM.RegID, "S");
                                }
                            }
                        }

                    }
                    using (var regproxy = new RegistrationServiceProxy())
                    {
                        regproxy.RegistrationClose(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                            ResponseCode = response.Code,
                            ResponseDescription = response.Message,
                            MethodName = "KenanAdditionLineRegistration"
                        });
                    }
                    if (Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("MREG_DSV"))
                    {
                        return RedirectToAction("DMobileRegSummary", new { regID = personalDetailsVM.RegID });
                    }
                    else
                    {
                        return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LogException(ex);

                LogExceptions(ex);

            }
            return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,SMRT_MANAGER,MREG_HQ,MREG_DAB,DSupervisor,DAgent,MREG_DSV,DREG_N,DREG_C,DREG_CH,MREG_DAI,MREG_DIC,MREG_DAC,MREG_DC,MREG_DSK,MREG_DCH,MREG_DSV,MREG_DSA")]
        [DoNoTCache]
        public ActionResult MobileRegSummary(int? regID)
        {
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            if (ReferenceEquals(Session["RegMobileReg_PersonalDetailsVM"], null))
            {
                if (!(Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("DAgent") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_C") || Roles.IsUserInRole("MREG_DAI")))
                {
                    if (!ReferenceEquals(Request.UrlReferrer, null))
                    {
                        if (!Request.UrlReferrer.AbsolutePath.Contains("/Registration/CDPUDashBoard") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/OrderStatusReport") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/StoreKeeper") && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegSuccess") && !Request.UrlReferrer.AbsolutePath.Contains("/Storekeeper/Storekeeper") && !Request.UrlReferrer.AbsolutePath.Contains("/Cashier/Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/SuperVisorForWaiveOff"))
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }
            }

            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            var orderSummary = new OrderSummaryVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            var regMdlGrpModelSec = new RegMdlGrpModelSec();
            var mainLinePBPCIDsSec = new List<int>();
            var regSec = new DAL.Models.RegistrationSec();
            var suppLineRegPBPCsSec = new List<RegPgmBdlPkgCompSec>();
            var allRegDetails = new Online.Registration.DAL.RegistrationDetails();
            int paymentstatus = -1;
            #region VLT ADDED CODE 21stFEB for Email functionality
            Session["RegID"] = regID;
            #endregion VLD ADDED CODE 21stFEB for Email functionality
            /*Chetan added for displaying the reson for failed transcation*/
            var KenanLogDetails = new DAL.Models.KenanaLogDetails();
            bool IsMNPWithMultpleLines = false;
			var mainLinePBPCs = new List<PgmBdlPckComponent>();
            
            IsMNPWithMultpleLines = (
            (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
            &&
            (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null) && ((MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()]).SupplimentaryMSISDNs.Count > 0)
            &&
            (ReferenceEquals(regID, null))
            );
            
            if (regID.ToInt() > 0)
            {
                ClearRegistrationSession();

                Session["RegMobileReg_PhoneVM"] = null;
                

                using (var proxy = new RegistrationServiceProxy())
                {
					allRegDetails = proxy.GetRegistrationFullDetails(regID.Value);

                    if (allRegDetails.Customers.FirstOrDefault().IsVIP == 1 && !Roles.IsUserInRole("MREG_SV"))
                    {
                        Session["VIPFailMsg"] = true;
                        return RedirectToAction("StoreKeeper", "Registration");
                    }

					WebHelper.Instance.MapRegistrationDetailsToVM(IsMNPWithMultpleLines, regID.Value.ToInt(), ref allRegDetails, out personalDetailsVM);

                    reg = proxy.RegistrationGet(regID.Value);

                    // Offer Name - Lindy
                    var prinOfferID = reg.OfferID;
                    var prinOfferName = "";

                    if (prinOfferID > 0)
                    {
                        var prinOffID = proxy.GetOffersById(prinOfferID.ToInt());
                        prinOfferName = prinOffID.OfferName;
                    }

                    Session["prinOfferName"] = prinOfferName;

                    Session[SessionKey.ExtratenMobilenumbers.ToString()] = personalDetailsVM.ExtraTenMobileNumbers;
                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);
                    Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;

                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;
					
					// RegMdlGrpModels
                    var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                    {
                        RegMdlGrpModel = new RegMdlGrpModel()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();

					if (regMdlGrpModelIDs.Count() > 0)
					{
						var tempRegMdlGrpModel = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
						regMdlGrpModel = tempRegMdlGrpModel.Where(x => x.SuppLineID == null).SingleOrDefault();
					}

					if (regMdlGrpModel == null)
					{
						regMdlGrpModel = new RegMdlGrpModel();
						regMdlGrpModel.ID = 0;
					}

					if (regMdlGrpModel.ID == 0)
                    {
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
                    }
                    else
                    {
                        Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = regMdlGrpModel.Price.ToDecimal();
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
                        Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;
                    }

                    if (personalDetailsVM.RegTypeID == (int)MobileRegType.CRP)
                        Session[SessionKey.RegMobileReg_Type.ToString()] = personalDetailsVM.RegTypeID;


                    // gerry - drop 4
                    if (!string.IsNullOrEmpty(reg.ArticleID))
                    {
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
                    }

                    // RegPgmBdlPkgComponent
                    var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                    {
                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();

                    // Main Line PgmBdlPkgComponent
                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    if (regPgmBdlPkgCompIDs.Count() > 0)
                        regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind()
                    {
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();

                    if (suppLineIDs.Count() > 0)
                    {
                        suppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    }

                    //<Start> - 20150914 - Samuel - display sales code in summary page
                    //personalDetailsVM.Registration = proxy.RegistrationGet(personalDetailsVM.RegID);
                    personalDetailsVM.RegAttributes = proxy.RegAttributesGetByRegID(personalDetailsVM.RegID);
                    /*using (var proxyOrg = new OrganizationServiceProxy())
                    {
                        if (!ReferenceEquals(personalDetailsVM.Registration, null) && !ReferenceEquals(personalDetailsVM.Registration.CenterOrgID, null) && personalDetailsVM.Registration.CenterOrgID != 0)
                        {
                            personalDetailsVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { personalDetailsVM.Registration.CenterOrgID }).FirstOrDefault();
                        }
                    }*/
                    var salesPerson = allRegDetails.Regs.FirstOrDefault().SalesPerson;
                    var orgID = allRegDetails.Regs.FirstOrDefault().CenterOrgID;
                    var outletCode = string.Empty;
                    var salesCode = string.Empty;

                    WebHelper.Instance.generateOutletAndSalesCode(orgID, salesPerson, personalDetailsVM.RegAttributes, out outletCode, out salesCode);
                    personalDetailsVM.OutletCode = outletCode;
                    personalDetailsVM.SalesCode = salesCode;
                    //<End> - 20150914 - Samuel - display sales code in summary page
                }

                if (suppLines.Count() > 0)
                    WebHelper.Instance.ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);
                // added by Gry
                personalDetailsVM.RegSupplineList = suppLines;
                personalDetailsVM.RegSupplineListWithDevice = suppLines.Where(x => !string.IsNullOrEmpty(x.ArticleID)).ToList();
                if (ReferenceEquals(personalDetailsVM.RegSupplineListWithDevice, null) || personalDetailsVM.RegSupplineListWithDevice.Count() == 0)
                {
                    personalDetailsVM.RegSupplineListWithDevice = new List<Online.Registration.DAL.Models.RegSuppLine>();
                }

                using (var proxy = new RegistrationServiceProxy())
                {
                    regSec = proxy.RegistrationSecGet(regID.Value);
                    if (regSec != null)
                    {
                        personalDetailsVM.RegIDSeco = regSec.ID;
                        personalDetailsVM.MSISDNSeco = regSec.MSISDN1;
                        personalDetailsVM.PlanAdvanceSeco = regSec.PlanAdvance;
                        personalDetailsVM.PlanDepositSeco = regSec.PlanDeposit;
                        personalDetailsVM.DeviceAdvanceSeco = regSec.DeviceAdvance;
                        personalDetailsVM.DeviceDepositSeco = regSec.DeviceDeposit;
                        personalDetailsVM.RegTypeIDSeco = regSec.RegTypeID;
                        personalDetailsVM.RegSIMSerialSeco = regSec.SIMSerial;
                        personalDetailsVM.RegIMEINumberSeco = regSec.IMEINumber;
                        personalDetailsVM.MISMSIMModel = regSec.SimModelId.ToString2() == string.Empty ? "0" : regSec.SimModelId.ToString();
                        // MobileNo
                        var SecmobileNos = new List<string>();
                        SecmobileNos.Add(regSec.MSISDN1);
                        SecmobileNos.Add(regSec.MSISDN2);
                        //Commented for MISM virtual numbers flow
                        Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] = "";

                        // RegMdlGrpModels for Secondary
                        var regMdlGrpModelIDs = proxy.RegMdlGrpModelSecFind(new RegMdlGrpModelSecFind()
                        {
                            RegMdlGrpModelSec = new RegMdlGrpModelSec()
                            {
                                SecRegID = regSec.ID
                            },
                            Active = true
                        }).ToList();
                        if (regMdlGrpModelIDs.Count() > 0)
                            regMdlGrpModelSec = proxy.RegMdlGrpModelSecGet(regMdlGrpModelIDs).SingleOrDefault();

                        if (regMdlGrpModelSec.ID == 0 && ((regMdlGrpModel.ID == 0) && regMdlGrpModelSec.ID == 0))
                        {
                            Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
                        }
                        else
                        {
                            Session["RegMobileReg_MainDevicePrice_Seco"] = regMdlGrpModelSec.Price.ToDecimal();
                            Session["RegMobileReg_SelectedModelImageID_Seco"] = regMdlGrpModelSec.ModelImageID;
                            Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
                        }

                        if (personalDetailsVM.RegTypeID == (int)MobileRegType.CRP)
                            Session[SessionKey.RegMobileReg_Type.ToString()] = personalDetailsVM.RegTypeID;

                        // RegPgmBdlPkgComponent for Secondary
                        var regPgmBdlPkgCompIDsSec = proxy.RegPgmBdlPkgCompSecFind(new RegPgmBdlPkgCompSecFind()
                        {
                            RegPgmBdlPkgCompSec = new RegPgmBdlPkgCompSec()
                            {
                                RegID = regSec.ID
                            },
                            Active = true
                        }).ToList();

                        // Secondary Line PgmBdlPkgComponent
                        var regPBPCsSec = new List<RegPgmBdlPkgCompSec>();
                        if (regPgmBdlPkgCompIDsSec.Count() > 0)
                            regPBPCsSec = proxy.RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec).ToList();

                        if (regPBPCsSec.Count() > 0)
                            mainLinePBPCIDsSec = regPBPCsSec.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();


                        // Sub Line RegPgmBdlPkgComponent
                        suppLineRegPBPCsSec = regPBPCsSec.Where(a => a.RegSuppLineID != null).ToList();
                    }
                }

                using (var proxy = new CatalogServiceProxy())
                {
                    string userICNO = Util.SessionAccess.User.IDCardNo;

                    if (userICNO != null && userICNO.Length > 0)
                    {
                        objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                    }
                    if (objWhiteListDetailsResp != null)
                    {
                        if (objWhiteListDetailsResp.Description != null)
                        {
                            personalDetailsVM._WhiteListDetails.Description = objWhiteListDetailsResp.Description;
                        }
                    }

                    if (regMdlGrpModel.ID > 0)
                        Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;
                    //Model for Secondary
                    if (regMdlGrpModelSec.ID > 0)
                        Session["RegMobileReg_SelectedModelID_Seco"] = proxy.BrandArticleImageGet(regMdlGrpModelSec.ModelImageID).ModelID;

                    
                    if (mainLinePBPCIDs.Count() > 0)
                        mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

                    var mainLinePBPCsSec = new List<PgmBdlPckComponent>();
                    if (mainLinePBPCIDsSec.Count() > 0)
                        mainLinePBPCsSec = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDsSec).ToList();

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var DPCode = Properties.Settings.Default.RegType_DevicePlan;
                    var MAcode = Properties.Settings.Default.Master_Component;
                    var MPcode = Properties.Settings.Default.Main_package;
                    var CPCode = Properties.Settings.Default.ComplimentaryPlan;
                    var DCCode = Properties.Settings.Default.CompType_DataCon;
                    var ECCode = Properties.Settings.Default.Extra_Component;
                    var SPCode = Properties.Settings.Default.SecondaryPlan;
                    var RCCode = Properties.Settings.Default.Pramotional_Component;
                    var CRCode = Properties.Settings.Default.DiscountDataContract_Component;
                    var MDCode = Properties.Settings.Default.Mandatory_DataComponent;
                    var DMCode = Properties.Settings.Default.Main_Datacomponent;
					var DFCode = Properties.Settings.Default.Device_Financing_Component;
                    //Uncommented below code as  Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] is assigned with current planytype which is used in turn to get package details
                    //Commented below If conditions as it is being not used any where. 
                    //Date: 08 july 2013 .This If condition is impacting SK module. So commented condition. Please discuss with CRP Plan only team before uncommenting.  
                    if (mainLinePBPCs.Count() > 0)
                    {
                        Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode || a.PlanType == SPCode)).ToList().FirstOrDefault().ID;
                        var vasIDs = string.Empty;
                        //Added by chetan to exclusively check DiscountDataContract_Component as CRCode & RCCode
						var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == CRCode || a.LinkType == RCCode || a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode || a.LinkType == RCCode || a.LinkType == CRCode || a.LinkType == MDCode || a.LinkType == DMCode || a.LinkType.ToLower() == "ic" || a.LinkType == DFCode).Select(a => a.ID).ToList();

                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session[SessionKey.RegMobileReg_VasIDs.ToString()] = vasIDs;

                    }
                    if (mainLinePBPCsSec.Count() > 0)
                    {
                        var componentList = mainLinePBPCsSec.Where(a => (a.LinkType == bpCode || a.LinkType == SPCode) && (a.PlanType == CPCode || a.PlanType == SPCode)).ToList();
                        if (componentList != null && componentList.Count > 0)
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = mainLinePBPCsSec.Where(a => (a.LinkType == bpCode || a.LinkType == SPCode) && (a.PlanType == CPCode || a.PlanType == SPCode)).FirstOrDefault().ID;
                        else
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = mainLinePBPCsSec.Where(a => a.LinkType == SPCode && (a.PlanType == MPcode)).FirstOrDefault().ID;

                        var vasIDs = string.Empty;
                        //Added by chetan to exclusively check DiscountDataContract_Component as CRCode & RCCode
                        var pkgCompIDs = mainLinePBPCsSec.Where(a => a.LinkType == CRCode || a.LinkType == RCCode || a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode || a.LinkType == RCCode || a.LinkType == CRCode || a.LinkType == MDCode || a.LinkType == DMCode || a.LinkType.ToLower() == "ic").Select(a => a.ID).ToList();



                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = vasIDs;
                    }
                }
                //CR 29JAN2016 - display bill cycle
                    if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault() != null)
                    {
                        personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault().ATT_Value;
                    }
                    else
                    {
                        personalDetailsVM.BillingCycle = "N/A";
                    }
                
            }
            if (reg.OfferID > 0)
            {
                Session[SessionKey.OfferId.ToString()] = reg.OfferID;
            }
            //Pavan
            var orderSummaryVM = ConstructOrderSummary();


			#region monthly subscription charge for principal device financing

			//if (regID.ToInt() > 0)
			//{
			//    var upgradeFeeKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_UPGRADEFEE);
			//    var mocWaiverKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_MOC_WAIVER);

			//    if (mainLinePBPCs.Where(x => x.KenanCode == upgradeFeeKenanCode) != null && mainLinePBPCs.Where(x => x.KenanCode == upgradeFeeKenanCode).Any())
			//    {
			//        using (var regProxy = new RegistrationServiceProxy())
			//        {
			//            var objRegAttributes = regProxy.RegAttributesGetByRegID(regID.ToInt());
			//            try
			//            {
			//                bool dfWaiverExist = mainLinePBPCs.Where(x => x.KenanCode == mocWaiverKenanCode) != null ? mainLinePBPCs.Where(x => x.KenanCode == mocWaiverKenanCode).Any() : false;
			//                if (!dfWaiverExist)
			//                {
			//                    orderSummaryVM.MonthlySubscription += objRegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_DF_UPGRADE_FEE).FirstOrDefault().ATT_Value.ToDecimal();
			//                }
			//            }
			//            catch (Exception ex)
			//            { 
			//                //
			//            }
			//        }

			//    }
			//}


			#endregion


            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] != null)
            {
                if (Session["regid"] != null)
                {
                    if (regID.Value > 0)
                    {
                        CatalogServiceProxy objCSproxy = new CatalogServiceProxy();
                        var deviceproxys = objCSproxy.GetDiscountPriceDetails(orderSummaryVM.ModelID, Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), ReferenceEquals(Session[SessionKey.RegMobileReg_ContractID.ToString()], null) ? 0 : Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt(), 0);
                        if (deviceproxys.NewPlanName != null)
                        {
                            orderSummaryVM.DiscountPrice = deviceproxys.DiscountPrice.ToInt();
                            orderSummaryVM.PlanName = deviceproxys.NewPlanName;
                            orderSummaryVM.MonthlySubscription = orderSummaryVM.MonthlySubscription - orderSummaryVM.DiscountPrice;
                        }
                    }

                }
            }

            //For Secondary plan
            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] != null)
            {
                if (Session["regid"] != null)
                {
                    if (regID.Value > 0)
                    {
                        CatalogServiceProxy objCSproxy = new CatalogServiceProxy();
                        var deviceproxys = objCSproxy.GetDiscountPriceDetails(orderSummaryVM.ModelID, Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(), Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt(), 0);
                        if (deviceproxys.NewPlanName != null)
                        {
                            orderSummaryVM.DiscountPrice = deviceproxys.DiscountPrice.ToInt();
                            orderSummaryVM.SecondaryPlanName = deviceproxys.NewPlanName;
                            Session[SessionKey.SelectedPlanID_Seco.ToString()] = deviceproxys.PlanID;
                            orderSummaryVM.SecondaryMonthlySubscription = orderSummaryVM.SecondaryMonthlySubscription - orderSummaryVM.DiscountPrice;
                        }
                        else
                        {
                            Session[SessionKey.SelectedPlanID_Seco.ToString()] = 0;
                        }
                    }
                }
            }

            using (var proxy = new RegistrationServiceProxy())
            {
                if (regID.ToInt() > 0)
				{
					#region	moved to WebHelper.MapRegistrationDetailsToVM - Drop 5
					/*
					DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(regID.ToInt());
                    if (lnkregdetails != null)
					{
						// Active PDPA Commented
                        // Changes by chetan related to PDPA implementation
						
                        string documentversion = lnkregdetails.PdpaVersion;
                        personalDetailsVM.PDPAVersionStatus = documentversion;
                        if (!ReferenceEquals(documentversion, null))
                        {
                            using (var registrationProxy = new RegistrationServiceProxy())
                            {
                                var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", documentversion);
                                personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                            }
						}
					}
                    else
                    {
                        personalDetailsVM.SimType = "Normal";
					}
					*/
					#endregion
				}
                /*Active PDPA Commented*/
                else
                {
                    /*Changes by chetan related to PDPA implementation*/
                    using (var registrationProxy = new RegistrationServiceProxy())
                    {
                        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                        personalDetailsVM.PDPDVersion = resultdata.Version;
                    }
                }
            }

            orderSummaryVM.IMEINumber = reg.IMEINumber;
            orderSummaryVM.SIMSerial = reg.SIMSerial;

            if (regSec != null)
            {
                orderSummaryVM.IMEINumberSeco = regSec.IMEINumber.ToString2();
                orderSummaryVM.SecondarySIMSerial = regSec.SIMSerial.ToString2();
            }
            if (regID.ToInt() > 0)
            {
                orderSummaryVM.SimType = personalDetailsVM.SimType;
                Session[SessionKey.SimType.ToString()] = personalDetailsVM.SimType;
            }

            if (reg.KenanAccountNo != null)
            { Session["KenanAccountNo"] = reg.KenanAccountNo; }
            else
            { Session["KenanAccountNo"] = "0"; }
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderSummaryVM;


            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session[SessionKey.ExternalID.ToString()].ToString2()) ? string.Empty : Session[SessionKey.ExternalID.ToString()].ToString2(), regID.ToInt());
            personalDetailsVM.Liberlization_Status = Result[0];
            personalDetailsVM.MOCStatus = Result[1];

            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//Anthony-[#2068]
            var supplementIndex = dropObj.suppIndex;
            if (dropObj.mainFlow.simSize != null && dropObj.mainFlow.simSize != "null")
            {
                personalDetailsVM.SIMCardType = dropObj.mainFlow.simSize;//Anthony-[#2068]
            }
            for (int i = 0; i < supplementIndex; i++ )
            {
                personalDetailsVM.RegSupplineList[i].MNPSuppLineSimCardType = dropObj.suppFlow[i].simSize;//Anthony-[#2068]
            }
            dropObj.mainFlow.imeiNumber = personalDetailsVM.RegIMEINumber;//Anthony-[#2220]

            for (int i = 0; i < personalDetailsVM.RegSupplineList.Count; i++)
            {
                if (ReferenceEquals(personalDetailsVM.RegSupplineList[i].MNPSuppLineSimCardType, null) && !ReferenceEquals(personalDetailsVM.RegSupplineList[i].SimModelId, null))
                {
                    switch (personalDetailsVM.RegSupplineList[i].SimModelId.ToInt())
                    {
                        case 1:
                            personalDetailsVM.RegSupplineList[i].MNPSuppLineSimCardType = "Normal";
                            break;
                        case 2:
                            personalDetailsVM.RegSupplineList[i].MNPSuppLineSimCardType = "Micro";
                            break;
                        case 3:
                            personalDetailsVM.RegSupplineList[i].MNPSuppLineSimCardType = "Nano";
                            break;
                        default:
                            break;
                    }
                }
            }

            #region Retrieve Monthly Subscription for SuppLine
            //Anthony: [#2365] Retrieve Monthly Subscription for SuppLine in Order Summary panel
            if (/*!IsMNPWithMultpleLines*/ true)
            {
                var suppDetails = !ReferenceEquals(allRegDetails.SuppLines, null) ? allRegDetails.SuppLines.ToList() : null;
                if (!ReferenceEquals(suppDetails, null))
                {
                    var suppLineID = suppDetails.Select(x => x.ID).ToList();
                    int suppindex = 1;
					var upgradeFeeKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_UPGRADEFEE);
					var mocWaiverKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_MOC_WAIVER);

					var upgradeFeeID = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
					{
						PgmBdlPckComponent = new PgmBdlPckComponent()
						{
							LinkType = "DF"
						},
						Active = true
					}).Where(x => x.KenanCode == upgradeFeeKenanCode).FirstOrDefault().ID.ToString2();
					var mocWaiverID = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
					{
						PgmBdlPckComponent = new PgmBdlPckComponent()
						{
							LinkType = "DF"
						},
						Active = true
					}).Where(x => x.KenanCode == mocWaiverKenanCode).FirstOrDefault().ID.ToInt();

                    using (var proxy = new CatalogServiceProxy())
                    {
                        foreach (var suppID in suppLineID)
                        {
                            decimal monthlyCharges = 0;
                            //var suppPBPCID = suppDetails.Where(x => x.ID == suppID).Select(x => x.PgmBdlPckComponentID).ToList();
                            var PBPCList = allRegDetails.RegPgmBdlPkgComps.Where(x => x.RegSuppLineID == suppID).Select(x => x.PgmBdlPckComponentID).ToList();

                            foreach (var pbpcID in PBPCList)
                            {
                                var pbpcPrice = proxy.PgmBdlPckComponentGet(new int[] { pbpcID }).SingleOrDefault().Price;

								#region device financing
								if (pbpcID == upgradeFeeID.ToInt())
								{
									using (var regProxy = new RegistrationServiceProxy())
									{
										var objRegAttributes = regProxy.RegAttributesGetBySuppLineID(suppID);
										try
										{
											if (!PBPCList.Contains(mocWaiverID))
											{
												pbpcPrice = objRegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_DF_UPGRADE_FEE).FirstOrDefault().ATT_Value.ToDecimal();
											}
										}
										catch (Exception ex)
										{ 
											//
										}
									}
								}
								#endregion

								monthlyCharges += pbpcPrice;
                            }
                            Session["MonthlyCharges_S" + suppindex++] = monthlyCharges;
                        }
                    }
                }
            }
            #endregion

            #region Retrieve Device Information for Primary & Supplementary
            //Anthony: retrieve device informations for order summary panel
            if (!IsMNPWithMultpleLines)
            {
                using (var proxy = new CatalogServiceProxy())
                {
                    var deviceInfo = allRegDetails.RegMdlGrpMdls.ToList() != null ? allRegDetails.RegMdlGrpMdls.ToList() : null;
                    var primaryDevice = deviceInfo != null ? deviceInfo.Where(x => x.SuppLineID == null).ToList() : null;
                    var suppDevice = deviceInfo != null ? deviceInfo.Where(x => x.SuppLineID != null).ToList() : null;
                    var supplementaryDetail = (!ReferenceEquals(personalDetailsVM.RegSupplineList, null) && personalDetailsVM.RegSupplineList.Count.ToInt() > 0) ? personalDetailsVM.RegSupplineList.ToList() : null;

                    //09072015 - Anthony - Fix for Submit button is showed even though the IMEI number is not populated yet - Start
                    var hasDevice = (deviceInfo != null && deviceInfo.Count > 0);
                    ViewBag.thisOrderHasDevice = hasDevice ? true : false;
                    ViewBag.principalHasDevice = hasDevice && primaryDevice != null && primaryDevice.Count > 0 ? true : false;
                    ViewBag.supplineHasDevice = hasDevice && suppDevice != null && suppDevice.Count > 0 ? true : false;
                    //09072015 - Anthony - Fix for Submit button is showed even though the IMEI number is not populated yet - End
                    //var supplementaryDetail2 = personalDetailsVM.RegSupplineListWithDevice.ToList() != null ? personalDetailsVM.RegSupplineListWithDevice.ToList() : null;
                    #region Primary Device Section
                    if (!ReferenceEquals(primaryDevice, null) && primaryDevice.Count.ToInt() > 0)
                    {
                        var modelImages = proxy.BrandArticleModelImageGet(primaryDevice.Select(y => y.ModelImageID)).ToList();
                        var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());
                        var modelID = modelImages.Select(a => a.ModelID).SingleOrDefault();
                        var colourDeviceID = modelImages.Select(x => x.ColourID).SingleOrDefault();

                        var articleID = modelImages.Select(x => x.ArticleID).SingleOrDefault();
                        var imageURL = !ReferenceEquals(modelImages.Select(x => x.ImagePath).ToList(), null) ? modelImages.Select(x => x.ImagePath).SingleOrDefault() : "";
                        var brandName = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID);//Apple
                        var modelName = models.Where(a => a.ID == modelID).SingleOrDefault().Name;//IPAD WIFI CELL 64GB
                        var colour = Util.GetNameByID(RefType.Colour, colourDeviceID);//Space Gray
                        var RRPPrice = models.Select(x => x.RetailPrice).SingleOrDefault().ToDecimal();
                        var offerPrice = primaryDevice.Select(x => x.Price).SingleOrDefault().ToInt();
                        var isTradeUp = primaryDevice.Select(x => x.IsTradeUp).SingleOrDefault();
                        var tradeUpAmount = primaryDevice.Select(x => x.TradeUpAmount).SingleOrDefault();

						List<RegAttributes> objRegAttributes = new List<RegAttributes>();
						using (var proxyReg = new RegistrationServiceProxy())
						{
							objRegAttributes = proxyReg.RegAttributesGetByRegID(regID.ToInt());
							if (objRegAttributes != null)
							{
								orderSummary.DFRegAttribute = objRegAttributes;

								if (WebHelper.Instance.determineDFOrderbyRegID(regID.ToInt()))
								{
									string totalprice = orderSummary.DFRegAttribute.Where(a => a.ATT_Name == RegAttributes.ATTRIB_DF_TOTAL_PRICE && a.SuppLineID == 0).FirstOrDefault().ATT_Value;
									string discountprice = orderSummary.DFRegAttribute.Where(a => a.ATT_Name == RegAttributes.ATTRIB_DF_DISCOUNT && a.SuppLineID == 0).FirstOrDefault().ATT_Value;
									offerPrice = totalprice.ToInt() - discountprice.ToInt();
								}
							}
						}

                        orderSummary.ArticleId = articleID;
                        orderSummary.ModelImageUrl = imageURL;
                        orderSummary.BrandName = brandName;
                        orderSummary.ModelName = modelName;
                        orderSummary.ColourName = colour;
                        orderSummary.ModelPrice = RRPPrice;
                        orderSummary.Price = offerPrice;
                        orderSummary.SelectedTradeUpComp = new TradeUpComponent()
                        {
                            Name = isTradeUp.ToString(),
                            Price = tradeUpAmount.ToInt()
                        };
                        Session["RegMobileReg_OfferDevicePrice"] = offerPrice;
                        Session["RegMobileReg_RRPDevicePrice"] = RRPPrice;
                        Session["PrimaryDeviceSummary"] = orderSummary;
                    }
                    #endregion

                    #region Supplementary Device Section
                    if (!ReferenceEquals(supplementaryDetail, null) && supplementaryDetail.Count.ToInt() > 0)
                    {
                        int suppIndex = 1;
                        foreach (var suppDetail in supplementaryDetail)
                        {
                            if (!string.IsNullOrEmpty(suppDetail.ArticleID) && !string.Equals(suppDetail.ArticleID, "0"))
                            {
                                var joinSuppDetail = deviceInfo.Where(x => x.SuppLineID == suppDetail.ID).ToList();
                                //var offerPrice = suppDetail.Price;
                                //var description = suppDetail.Description;
								if (joinSuppDetail.Count > 0)
								{
									var modelImages = proxy.BrandArticleModelImageGet(joinSuppDetail.Select(y => y.ModelImageID)).ToList();
									var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());
									var modelID = modelImages.Select(a => a.ModelID).SingleOrDefault();
									var colourDeviceID = modelImages.Select(x => x.ColourID).SingleOrDefault();

									var imageURL = !ReferenceEquals(modelImages.Select(x => x.ImagePath).ToList(), null) ? modelImages.Select(x => x.ImagePath).SingleOrDefault() : "";
									var brandName = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID);//Apple
									var modelName = models.Where(a => a.ID == modelID).SingleOrDefault().Name;//IPAD WIFI CELL 64GB
									var colour = Util.GetNameByID(RefType.Colour, colourDeviceID);//Space Gray
									var RRPPrice = models.Select(x => x.RetailPrice).SingleOrDefault();
									var offerPrice = joinSuppDetail.Select(y => y.Price).SingleOrDefault();
									var SuppIMEI = !string.IsNullOrEmpty(suppDetail.IMEINumber.ToString2())
															? suppDetail.IMEINumber.ToString2()
															: "";
                                    //Tradeup
                                    var isTradeUp = joinSuppDetail.Select(a => a.IsTradeUp).SingleOrDefault();
                                    var tradeUpAmount = joinSuppDetail.Select(a => a.TradeUpAmount).SingleOrDefault();
                                    var suppOfferName = string.Empty;
									List<RegAttributes> objRegAttributes = new List<RegAttributes>();
									using (var proxyReg = new RegistrationServiceProxy())
									{
                                        if (suppDetail.OfferID != null && suppDetail.OfferID > 0)
                                        {
                                            var suppOffID = proxyReg.GetOffersById(suppDetail.OfferID.ToInt());
                                            suppOfferName = suppOffID.OfferName;
                                        }

										objRegAttributes = proxyReg.RegAttributesGetByRegID(regID.ToInt());
										if (objRegAttributes != null)
										{
											orderSummary.DFRegAttribute = objRegAttributes;

											if (WebHelper.Instance.determineDFOrderbySuppLineID(suppDetail.ID))
											{
												var objRegAttributesSupp = orderSummary.DFRegAttribute.Where(a => a.SuppLineID == suppDetail.ID && a.ATT_Name == RegAttributes.ATTRIB_DF_ORDER && a.ATT_Value == "True");
                                                if (objRegAttributesSupp.Count() > 0)
                                                {
                                                    string totalprice = orderSummary.DFRegAttribute.Where(a => a.ATT_Name == RegAttributes.ATTRIB_DF_TOTAL_PRICE && a.SuppLineID == suppDetail.ID).FirstOrDefault().ATT_Value;
                                                    string discountprice = orderSummary.DFRegAttribute.Where(a => a.ATT_Name == RegAttributes.ATTRIB_DF_DISCOUNT && a.SuppLineID == suppDetail.ID).FirstOrDefault().ATT_Value;
                                                    offerPrice = totalprice.ToInt() - discountprice.ToInt();
                                                }
											}

										}
									}

									suppDetail.Price = offerPrice;
                                    suppDetail.Description = suppIndex.ToString() + '#' + imageURL + '#' + brandName + '#' + modelName + '#' + colour + '#' + RRPPrice + '#' + suppDetail.Price + '#' + SuppIMEI + '#' + isTradeUp + '#' + tradeUpAmount + '#' + suppOfferName + '#';
									Session["SuppDeviceSummary"] = personalDetailsVM;
								}
                            }
                            suppIndex++;

                        }
                    }
                    #endregion
                }
            }
            #endregion
            
            //using (var registrationProxy = new RegistrationServiceClient())
            //{
            //    personalDetailsVM.CDPUStatus = registrationProxy.GetCDPUStatus(regID.ToInt());
            //}
            
            #region CDPU Approval - drop 5
            using (var registrationProxy = new RegistrationServiceClient())
            {
                if (regID > 0)
                {
                    var details = registrationProxy.GetCDPUDetails(new RegBREStatus() { RegId = regID.ToInt() });
                    if (!ReferenceEquals(details, null))
                    {
                        personalDetailsVM.CDPUStatus = details.TransactionStatus;
                        personalDetailsVM.CDPUDescription = details.TransactionReason;
                        personalDetailsVM.IsLockedBy = details.IsLockedBy;
                    }
                }

            }
            #endregion

            if (!ReferenceEquals(((List<string>)TempData[SessionKey.errorValidationList.ToString()]), null) && ((List<string>)TempData[SessionKey.errorValidationList.ToString()]).Any())
            {
                string inventoryValidation = "Please re-insert the ";

                if (((List<string>)TempData[SessionKey.errorValidationList.ToString()]).Count() > 1)
                {
                    foreach (var errorMsg in ((List<string>)TempData[SessionKey.errorValidationList.ToString()]))
                    {
                        switch (errorMsg)
                        {
                            case "Sim serial": ModelState.Remove("RegSIMSerial"); break;
                            case "IMEI number": ModelState.Remove("RegIMEINumber"); break;
                            case "MISM Sim serial": ModelState.Remove("RegSIMSerialSeco"); break;
                        }

                        if (!ReferenceEquals(personalDetailsVM, null) && !ReferenceEquals(personalDetailsVM.RegSupplineList, null))
                        {
                            for (int supCount = 0; supCount < personalDetailsVM.RegSupplineList.Count(); supCount++)
                            {
                                Expression<Func<PersonalDetailsVM, string>> expressionSIM = x => x.RegSupplineList[supCount].SIMSerial;
                                string keySim = ExpressionHelper.GetExpressionText(expressionSIM);
                                Expression<Func<PersonalDetailsVM, string>> expressionIMEI = x => x.RegSupplineList[supCount].IMEINumber;
                                string keyIMEI = ExpressionHelper.GetExpressionText(expressionIMEI);
                                if (errorMsg == "SubLine Sim serial " + (supCount + 1).ToString2())
                                    ModelState.Remove(keySim);
                                if (errorMsg == "SubLine IMEI number " + (supCount + 1).ToString2())
                                    ModelState.Remove(keyIMEI);
                            }
                        }
                        inventoryValidation += errorMsg + " and ";
                    }
                }
                else
                { // only 1 error
                    inventoryValidation += ((List<string>)TempData[SessionKey.errorValidationList.ToString()]).FirstOrDefault();
                }
                if (inventoryValidation.EndsWith(" and "))
                {
                    inventoryValidation = inventoryValidation.Substring(0, inventoryValidation.Length - 5);
                }
                ModelState.AddModelError(string.Empty, inventoryValidation);
            }

            //CR 29JAN2016 - display bill cycle
            if (regID.ToInt() > 0)
            {
                if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault() != null)
                {
                    personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault().ATT_Value;
                }
                else
                {
                    personalDetailsVM.BillingCycle = "N/A";
                }
            }
            else
            {
                var selectedMsisdn = Session["SelectedMsisdn"].ToString2();
                retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                var selectedAccount = AcctListByICResponse.itemList.Where(c => c.ExternalId == selectedMsisdn).FirstOrDefault();
                var billCycle = (selectedAccount != null && selectedAccount.AccountDetails != null ? selectedAccount.AccountDetails.BillCycle : "N/A");
                personalDetailsVM.BillingCycle = (!string.IsNullOrWhiteSpace(billCycle) ? billCycle : "N/A");
            }

            return View(personalDetailsVM);
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,SMRT_MANAGER,MREG_HQ,MREG_DAB,DSupervisor,DAgent,MREG_DSV,DREG_N,DREG_C,DREG_CH,MREG_DAI,MREG_DIC,MREG_DAC,MREG_DC,MREG_DSK,MREG_DCH,MREG_DSV,MREG_DSA")]
        [DoNoTCache]
        public ActionResult MobileRegSummaryNew(int? regID)
        {
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            if (ReferenceEquals(Session["RegMobileReg_PersonalDetailsVM"], null))
            {
                if (!(Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("DAgent") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_C") || Roles.IsUserInRole("MREG_DAI")))
                {
                    if (!Request.UrlReferrer.AbsolutePath.Contains("/Registration/CDPUDashBoard") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/OrderStatusReport") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/StoreKeeper") && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegSuccess") && !Request.UrlReferrer.AbsolutePath.Contains("/Storekeeper/Storekeeper") && !Request.UrlReferrer.AbsolutePath.Contains("/Cashier/Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/SuperVisorForWaiveOff"))
                    {
                        return RedirectToAction("IndexNew", "Home");
                    }
                }
            }

            var orderSummary = new OrderSummaryVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            var regMdlGrpModelSec = new RegMdlGrpModelSec();
            var mainLinePBPCIDsSec = new List<int>();
            var regSec = new DAL.Models.RegistrationSec();
           
            var suppLineRegPBPCsSec = new List<RegPgmBdlPkgCompSec>();
            int paymentstatus = -1;

            #region VLT ADDED CODE 21stFEB for Email functionality
            Session["RegID"] = regID;
            #endregion VLD ADDED CODE 21stFEB for Email functionality
            /*Chetan added for displaying the reson for failed transcation*/
            var KenanLogDetails = new DAL.Models.KenanaLogDetails();
            if (regID.ToInt() > 0)
            {
                ClearRegistrationSession();
                personalDetailsVM = new PersonalDetailsVM()
                {
                    RegID = regID.Value.ToInt(),
                };

                Session["RegMobileReg_PhoneVM"] = null;

                using (var proxy = new RegistrationServiceProxy())
                {
                    reg = proxy.RegistrationGet(regID.Value);
                  

                    /*Himansu added for sim replacement . BASED ON THE MSISDN NEED TO GET THE SIM SERIAL NUM AND TYPE */

                    personalDetailsVM.MSISDN1 = reg.MSISDN1.ToString2();
                    /*Chetan added for displaying the reson for failed transcation*/
                    KenanLogDetails = proxy.KenanLogDetailsGet(regID.Value);
                    if (!ReferenceEquals(KenanLogDetails, null))
                    {
                        personalDetailsVM.MessageCode = KenanLogDetails.MessageCode.ToInt();
                        personalDetailsVM.MessageDesc = KenanLogDetails.MessageDesc.ToString();
                    }
                    //Added by Patanjali to get the payment status on 07-04-2013
                    paymentstatus = proxy.PaymentStatusGet(regID.Value);
                    if (paymentstatus != -1)
                    {
                        if (paymentstatus == 0)
                        {
                            personalDetailsVM.PaymentStatus = "0";
                        }
                        else
                        {
                            personalDetailsVM.PaymentStatus = "1";
                        }
                    }
                    else
                    {
                        personalDetailsVM.PaymentStatus = "0";
                    }
                    //Added by Patanjali to get the payment status on 07-04-2013

                    personalDetailsVM.QueueNo = reg.QueueNo;
                    //Added by Patanjali to support remarks on 30-03-2013
                    personalDetailsVM.Remarks = reg.Remarks;
                    //Added by Patanjali to support remarks on 30-03-2013 ends here
                    personalDetailsVM.BiometricVerify = reg.BiometricVerify;
                    personalDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
                    personalDetailsVM.BlacklistInternal = reg.InternalBlacklisted;
                    personalDetailsVM.SignatureSVG = reg.SignatureSVG;
                    personalDetailsVM.CustomerPhoto = reg.CustomerPhoto;
                    personalDetailsVM.AltCustomerPhoto = reg.AltCustomerPhoto;
                    personalDetailsVM.RFSalesDT = reg.RFSalesDT;
                    personalDetailsVM.Photo = reg.Photo;
                    personalDetailsVM.RegSIMSerial = reg.SIMSerial;
                    personalDetailsVM.RegIMEINumber = reg.IMEINumber;
                    personalDetailsVM.RegTypeID = reg.RegTypeID;
                    personalDetailsVM.AgeCheckStatus = reg.AgeCheckStatus;
                    personalDetailsVM.DDMFCheckStatus = reg.DDMFCheckStatus;
                    personalDetailsVM.AddressCheckStatus = reg.AddressCheckStatus;
                    personalDetailsVM.OutStandingCheckStatus = reg.OutStandingCheckStatus;
                    personalDetailsVM.TotalLineCheckStatus = reg.TotalLineCheckStatus;
                    personalDetailsVM.PrincipleLineCheckStatus = reg.PrincipleLineCheckStatus;
                    personalDetailsVM.ContractCheckStatus = reg.ContractCheckStatus;
                    personalDetailsVM.UserType = reg.UserType;
                    personalDetailsVM.PlanAdvance = reg.PlanAdvance;
                    personalDetailsVM.PlanDeposit = reg.PlanDeposit;
                    personalDetailsVM.DeviceAdvance = reg.DeviceAdvance;
                    personalDetailsVM.DeviceDeposit = reg.DeviceDeposit;
                    personalDetailsVM.Discount = reg.Discount;
                    personalDetailsVM.KenanAccountNo = reg.KenanAccountNo;
                    personalDetailsVM.OutstandingAcs = reg.OutstandingAcs;
                    personalDetailsVM.PrePortInCheckStatus = reg.PrePortInCheckStatus;
                    personalDetailsVM.MNPServiceIdCheckStatus = reg.MNPServiceIdCheckStatus;
                    personalDetailsVM.MOC_Status = reg.MOC_Status;
                    personalDetailsVM.Whitelist_Status = reg.Whitelist_Status;
                    personalDetailsVM.SIMModel = reg.SimModelId.ToString2() == string.Empty ? "0" : reg.SimModelId.ToString();
                    //Added By Himansu for Sim Replacement
                    personalDetailsVM.fxAcctNo = reg.fxAcctNo;
                    personalDetailsVM.FxSubscrNo = reg.fxSubscrNo;
                    personalDetailsVM.FxSubScrNoResets = reg.fxSubScrNoResets;
                    personalDetailsVM.CRPType = reg.CRPType;
                    personalDetailsVM.IsVerified = reg.IsVerified;
                    Session[SessionKey.Condition.ToString()] = personalDetailsVM.CRPType.ToString2();
                    //Added by VLT on 13 Apr 2013
                    personalDetailsVM.IMPOSFileName = reg.IMPOSFileName;
                    //Added by VLT on 13 Apr 2013 ends here

                    //**getting sim card type start**//
                    var regDetails = proxy.GetRegistrationFullDetails(regID.Value);
                    personalDetailsVM.SIMCardType = regDetails.lnkregdetails.SimCardType;
                    personalDetailsVM.SecondarySIMType = regDetails.lnkregdetails.MismSimCardType;
                    //**getting sim card type end**//

                    // RegAccount
                    var regAccount = proxy.RegAccountGet(new int[] { reg.ID }).SingleOrDefault();
                    if (regAccount != null)
                    {
                        personalDetailsVM.ServiceActivateErrMsg = regAccount.KenanActivateServiceErrMessage;
                    }

                    // Registration Status
                    var regStatusID = proxy.RegStatusFind(new RegStatusFind()
                    {
                        RegStatus = new RegStatus()
                        {
                            RegID = reg.ID
                        },
                        Active = true
                    }).SingleOrDefault();

                    personalDetailsVM.Deposite = reg.AdditionalCharges;
                    personalDetailsVM.StatusID = proxy.RegStatusGet(new int[] { regStatusID }).SingleOrDefault().StatusID;


                    using (var configProxy = new ConfigServiceProxy())
                    {
                        personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;

                    }

                    //for cancel reason
                    if (personalDetailsVM.StatusID == 21)
                    {

                        using (var regProxy = new RegistrationServiceProxy())
                        {
                            string reason = regProxy.CancelReasonGet(personalDetailsVM.RegID);
                            personalDetailsVM.MessageDesc = reason;

                        }
                    }

                    ExtraTenGetResp objExtraTenDetails = new ExtraTenGetResp();
                    objExtraTenDetails = proxy.GetExtraTenDetails(personalDetailsVM.RegID);
                    List<lnkExtraTenDetails> modListExtraTen = new List<lnkExtraTenDetails>();
                    personalDetailsVM.ExtraTenDetails = objExtraTenDetails.ExtraTenDetails.ToList();
                    if (personalDetailsVM.ExtraTenDetails != null && personalDetailsVM.ExtraTenDetails.Count > 0)
                    {
                        foreach (var extraTen in personalDetailsVM.ExtraTenDetails)
                        {
                            if (string.IsNullOrEmpty(extraTen.MsgCode) == true)
                            {
                                extraTen.MsgCode = "Processing";
                            }
                            else if (extraTen.MsgCode == "0")
                            {
                                extraTen.MsgCode = "Success";
                            }
                            else
                            {
                                extraTen.MsgCode = "Fail";
                            }
                            modListExtraTen.Add(extraTen);

                        }
                        personalDetailsVM.ExtraTenDetails = modListExtraTen;
                    }
                    personalDetailsVM.ExtraTenMobileNumbers = string.Join(",", objExtraTenDetails.ExtraTenDetails.Where(e => e.Status == "A").Select(e => e.Msisdn).ToList());
                    Session[SessionKey.ExtratenMobilenumbers.ToString()] = personalDetailsVM.ExtraTenMobileNumbers;
                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);
                    Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;

                    // Customer
                    personalDetailsVM.Customer = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    // Address
                    var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                    {
                        Active = true,
                        Address = new Address()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();
                    personalDetailsVM.Address = proxy.RegAddressGet(regAddrIDs).SingleOrDefault();

                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;
                    
                    regMdlGrpModel = regDetails.RegMdlGrpMdls.SingleOrDefault();

                    // RegMdlGrpModels
                    var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                    {
                        RegMdlGrpModel = new RegMdlGrpModel()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();
                    if (regMdlGrpModelIDs.Count() > 0)
                        regMdlGrpModel = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).SingleOrDefault();

                    if (regMdlGrpModel.ID == 0)
                    {
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
                    }
                    else
                    {
                        Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = regMdlGrpModel.Price.ToDecimal();
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
                        Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;
                    }

                    if (personalDetailsVM.RegTypeID == (int)MobileRegType.CRP)
                        Session[SessionKey.RegMobileReg_Type.ToString()] = personalDetailsVM.RegTypeID;

                    // RegPgmBdlPkgComponent
                    var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                    {
                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();

                    // Main Line PgmBdlPkgComponent
                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    if (regPgmBdlPkgCompIDs.Count() > 0)
                        regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind()
                    {
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();

                    if (suppLineIDs.Count() > 0)
                    {
                        suppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    }

                    //Bill Delivery Options
                    //GTM e-Billing CR - Ricky - 2014.09.25
                    personalDetailsVM.EmailBillSubscription = reg.billDeliveryViaEmail ? "Y" : "N";
                    personalDetailsVM.EmailAddress = reg.billDeliveryViaEmail ? reg.billDeliveryEmailAddress : "";
                    personalDetailsVM.SmsAlertFlag = reg.billDeliverySmsNotif ? "Y" : "N";
                    personalDetailsVM.SmsNo = reg.billDeliverySmsNotif ? reg.billDeliverySmsNo : "";
                    personalDetailsVM.BillSubmissionStatus = reg.billDeliverySubmissionStatus != null ? reg.billDeliverySubmissionStatus : "";

                }

                if (suppLines.Count() > 0)
                    WebHelper.Instance.ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

                using (var proxy = new RegistrationServiceProxy())
                {
                    regSec = proxy.RegistrationSecGet(regID.Value);
                    if (regSec != null)
                    {
                        personalDetailsVM.RegIDSeco = regSec.ID;
                        personalDetailsVM.MSISDNSeco = regSec.MSISDN1;
                        personalDetailsVM.PlanAdvanceSeco = regSec.PlanAdvance;
                        personalDetailsVM.PlanDepositSeco = regSec.PlanDeposit;
                        personalDetailsVM.DeviceAdvanceSeco = regSec.DeviceAdvance;
                        personalDetailsVM.DeviceDepositSeco = regSec.DeviceDeposit;
                        personalDetailsVM.RegTypeIDSeco = regSec.RegTypeID;
                        personalDetailsVM.RegSIMSerialSeco = regSec.SIMSerial;
                        personalDetailsVM.RegIMEINumberSeco = regSec.IMEINumber;
                        personalDetailsVM.MISMSIMModel = regSec.SimModelId.ToString2() == string.Empty ? "0" : regSec.SimModelId.ToString();
                        // MobileNo
                        var SecmobileNos = new List<string>();
                        SecmobileNos.Add(regSec.MSISDN1);
                        SecmobileNos.Add(regSec.MSISDN2);
                        //Commented for MISM virtual numbers flow
                        Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] = "";

                        // RegMdlGrpModels for Secondary
                        var regMdlGrpModelIDs = proxy.RegMdlGrpModelSecFind(new RegMdlGrpModelSecFind()
                        {
                            RegMdlGrpModelSec = new RegMdlGrpModelSec()
                            {
                                SecRegID = regSec.ID
                            },
                            Active = true
                        }).ToList();
                        if (regMdlGrpModelIDs.Count() > 0)
                            regMdlGrpModelSec = proxy.RegMdlGrpModelSecGet(regMdlGrpModelIDs).SingleOrDefault();

                        if (regMdlGrpModelSec.ID == 0)
                        {
                            Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
                        }
                        else
                        {
                            Session["RegMobileReg_MainDevicePrice_Seco"] = regMdlGrpModelSec.Price.ToDecimal();
                            Session["RegMobileReg_SelectedModelImageID_Seco"] = regMdlGrpModelSec.ModelImageID;
                            Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
                        }

                        if (personalDetailsVM.RegTypeID == (int)MobileRegType.CRP)
                            Session[SessionKey.RegMobileReg_Type.ToString()] = personalDetailsVM.RegTypeID;

                        // RegPgmBdlPkgComponent for Secondary
                        var regPgmBdlPkgCompIDsSec = proxy.RegPgmBdlPkgCompSecFind(new RegPgmBdlPkgCompSecFind()
                        {
                            RegPgmBdlPkgCompSec = new RegPgmBdlPkgCompSec()
                            {
                                RegID = regSec.ID
                            },
                            Active = true
                        }).ToList();

                        // Secondary Line PgmBdlPkgComponent
                        var regPBPCsSec = new List<RegPgmBdlPkgCompSec>();
                        if (regPgmBdlPkgCompIDsSec.Count() > 0)
                            regPBPCsSec = proxy.RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec).ToList();

                        if (regPBPCsSec.Count() > 0)
                            mainLinePBPCIDsSec = regPBPCsSec.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();


                        // Sub Line RegPgmBdlPkgComponent
                        suppLineRegPBPCsSec = regPBPCsSec.Where(a => a.RegSuppLineID != null).ToList();
                    }
                }

                using (var proxy = new CatalogServiceProxy())
                {
                    string userICNO = Util.SessionAccess.User.IDCardNo;

                    if (userICNO != null && userICNO.Length > 0)
                    {
                        objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                    }
                    if (objWhiteListDetailsResp != null)
                    {
                        if (objWhiteListDetailsResp.Description != null)
                        {
                            personalDetailsVM._WhiteListDetails.Description = objWhiteListDetailsResp.Description;
                        }
                    }

                    if (regMdlGrpModel.ID > 0)
                        Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;
                    //Model for Secondary
                    if (regMdlGrpModelSec.ID > 0)
                        Session["RegMobileReg_SelectedModelID_Seco"] = proxy.BrandArticleImageGet(regMdlGrpModelSec.ModelImageID).ModelID;

                    var mainLinePBPCs = new List<PgmBdlPckComponent>();
                    if (mainLinePBPCIDs.Count() > 0)
                        mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

                    var mainLinePBPCsSec = new List<PgmBdlPckComponent>();
                    if (mainLinePBPCIDsSec.Count() > 0)
                        mainLinePBPCsSec = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDsSec).ToList();

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var DPCode = Properties.Settings.Default.RegType_DevicePlan;
                    var MAcode = Properties.Settings.Default.Master_Component;
                    var MPcode = Properties.Settings.Default.Main_package;
                    var CPCode = Properties.Settings.Default.ComplimentaryPlan;
                    var DCCode = Properties.Settings.Default.CompType_DataCon;
                    var ECCode = Properties.Settings.Default.Extra_Component;
                    var SPCode = Properties.Settings.Default.SecondaryPlan;
                    var RCCode = Properties.Settings.Default.Pramotional_Component;
                    var CRCode = Properties.Settings.Default.DiscountDataContract_Component;
                    var MDCode = Properties.Settings.Default.Mandatory_DataComponent;
                    var DMCode = Properties.Settings.Default.Main_Datacomponent;
                    //Uncommented below code as  Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] is assigned with current planytype which is used in turn to get package details
                    //Commented below If conditions as it is being not used any where. 
                    //Date: 08 july 2013 .This If condition is impacting SK module. So commented condition. Please discuss with CRP Plan only team before uncommenting.  
                    if (mainLinePBPCs.Count() > 0)
                    {
                        Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode || a.PlanType == SPCode)).ToList().FirstOrDefault().ID;
                        var vasIDs = string.Empty;
                        //Added by chetan to exclusively check DiscountDataContract_Component as CRCode & RCCode
                        var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == CRCode || a.LinkType == RCCode || a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode || a.LinkType == RCCode || a.LinkType == CRCode || a.LinkType == MDCode || a.LinkType == DMCode || a.LinkType.ToLower() == "ic").Select(a => a.ID).ToList();

                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session[SessionKey.RegMobileReg_VasIDs.ToString()] = vasIDs;

                    }
                    if (mainLinePBPCsSec.Count() > 0)
                    {
                        var componentList = mainLinePBPCsSec.Where(a => (a.LinkType == bpCode || a.LinkType == SPCode) && (a.PlanType == CPCode || a.PlanType == SPCode)).ToList();
                        if (componentList != null && componentList.Count > 0)
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = mainLinePBPCsSec.Where(a => (a.LinkType == bpCode || a.LinkType == SPCode) && (a.PlanType == CPCode || a.PlanType == SPCode)).FirstOrDefault().ID;
                        else
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = mainLinePBPCsSec.Where(a => a.LinkType == SPCode && (a.PlanType == MPcode)).FirstOrDefault().ID;

                        var vasIDs = string.Empty;
                        //Added by chetan to exclusively check DiscountDataContract_Component as CRCode & RCCode
                        var pkgCompIDs = mainLinePBPCsSec.Where(a => a.LinkType == CRCode || a.LinkType == RCCode || a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode || a.LinkType == RCCode || a.LinkType == CRCode || a.LinkType == MDCode || a.LinkType == DMCode || a.LinkType.ToLower() == "ic").Select(a => a.ID).ToList();



                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = vasIDs;
                    }
                }
            }
            if (reg.OfferID > 0)
            {
                Session[SessionKey.OfferId.ToString()] = reg.OfferID;
            }

            // waived component start

            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

			personalDetailsVM = Util.ConstructWaiverPrice(personalDetailsVM, dropObj);
			#region Waiver for Pricing, moved to Util.cs (ConstructWaiverPrice) to generalize it.
			/**
			var waiveComponent = new List<string>();
            List<string> suppElement = new List<string>();
            Dictionary<string, List<string>> suppWaiveComponent = new Dictionary<string, List<string>>();
            string suppIndex = "";
            int index = 0;

            if (!string.IsNullOrEmpty(personalDetailsVM.hdnWaiverInfo))
            {
                var waiveObject = (personalDetailsVM.hdnWaiverInfo.Split('+'));

                foreach (var item in waiveObject)
                {
                    var split = item.ToString().Split('~');
                    if (!split[3].Contains("S"))
                    {
                        waiveComponent.Add(split[index]);
                    }
                    else
                    {

                        suppIndex = split[3].Substring(split[3].Count() - 1);
                        if (suppWaiveComponent.ContainsKey(suppIndex))
                        {

                            suppElement.Add(split[index]);
                            suppWaiveComponent.Clear();
                            suppWaiveComponent.Add(suppIndex, suppElement);
                        }
                        else
                        {
                            if (null != suppElement.Count())
                            {
                                suppElement.Clear();
                            }
                            suppElement.Add(split[index]);
                            suppWaiveComponent.Add(suppIndex, suppElement);
                        }
                    }
                }
            }

            if (personalDetailsVM.Customer.NationalityID.ToInt() == 1)
            {
                if (waiveComponent.Contains("Plan Advance"))
                {
                    dropObj.mainFlow.orderSummary.MalayPlanAdv = 0;
                }
                if (waiveComponent.Contains("Device Advance"))
                {
                    dropObj.mainFlow.orderSummary.MalyDevAdv = 0;
                }
                if (waiveComponent.Contains("Device Deposit"))
                {
                    dropObj.mainFlow.orderSummary.MalayDevDeposit = 0;
                }
                if (waiveComponent.Contains("Registration Deposit"))
                {

                    dropObj.mainFlow.orderSummary.MalyPlanDeposit = 0;
                }
                if (null != suppWaiveComponent)
                {

                    for (int i = 0; i < dropObj.suppFlow.Count(); i++)
                    {
                        string check = (i + 1).ToString();
                        if (suppWaiveComponent.ContainsKey(check))
                        {
                            if (waiveComponent.Contains("Plan Advance"))
                            {
                                dropObj.suppFlow[i].orderSummary.MalayPlanAdv = 0;
                            }
                            if (waiveComponent.Contains("Device Advance"))
                            {
                                dropObj.suppFlow[i].orderSummary.MalyDevAdv = 0;
                            }
                            if (waiveComponent.Contains("Device Deposit"))
                            {
                                dropObj.suppFlow[i].orderSummary.MalayDevDeposit = 0;
                            }
                            if (waiveComponent.Contains("Registration Deposit"))
                            {

                                dropObj.suppFlow[i].orderSummary.MalyPlanDeposit = 0;
                            }

                        }
                    }
                }

            }
            else
            {
                if (waiveComponent.Contains("Plan Advance"))
                {
                    dropObj.mainFlow.orderSummary.OthPlanAdv = 0;
                }
                if (waiveComponent.Contains("Device Advance"))
                {

                    dropObj.mainFlow.orderSummary.OthDevAdv = 0;

                }
                if (waiveComponent.Contains("Device Deposit"))
                {

                    dropObj.mainFlow.orderSummary.OthDevDeposit = 0;
                }
                if (waiveComponent.Contains("Registration Deposit"))
                {
                    dropObj.mainFlow.orderSummary.OthPlanDeposit = 0;
                }


                if (null != suppWaiveComponent)
                {

                    for (int i = 0; i < dropObj.suppFlow.Count(); i++)
                    {
                        string check = (i + 1).ToString();
                        if (suppWaiveComponent.ContainsKey(check))
                        {
                            if (waiveComponent.Contains("Plan Advance"))
                            {
                                dropObj.suppFlow[i].orderSummary.OthPlanAdv = 0;
                            }
                            if (waiveComponent.Contains("Device Advance"))
                            {
                                dropObj.suppFlow[i].orderSummary.OthDevAdv = 0;
                            }
                            if (waiveComponent.Contains("Device Deposit"))
                            {
                                dropObj.suppFlow[i].orderSummary.OthDevDeposit = 0;
                            }
                            if (waiveComponent.Contains("Registration Deposit"))
                            {

                                dropObj.suppFlow[i].orderSummary.OthPlanDeposit = 0;
                            }

                        }
                    }
                }
			}
			**/
			#endregion
			var isSuppline = false;
            if (dropObj != null && dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
            {
                isSuppline = true;
            }

            // waived component end


            //Pavan
			var orderSummaryVM = dropObj.mainFlow.orderSummary;


            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] != null)
            {
                if (Session["regid"] != null)
                {
                    if (regID.Value > 0)
                    {
                        CatalogServiceProxy objCSproxy = new CatalogServiceProxy();
                        var deviceproxys = objCSproxy.GetDiscountPriceDetails(orderSummaryVM.ModelID, Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), ReferenceEquals(Session[SessionKey.RegMobileReg_ContractID.ToString()], null) ? 0 : Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt(), 0);
                        if (deviceproxys.NewPlanName != null)
                        {
                            orderSummaryVM.DiscountPrice = deviceproxys.DiscountPrice.ToInt();
                            orderSummaryVM.PlanName = deviceproxys.NewPlanName;
                            orderSummaryVM.MonthlySubscription = orderSummaryVM.MonthlySubscription - orderSummaryVM.DiscountPrice;
                        }
                    }

                }
            }

            //For Secondary plan
            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] != null)
            {
                if (Session["regid"] != null)
                {
                    if (regID.Value > 0)
                    {
                        CatalogServiceProxy objCSproxy = new CatalogServiceProxy();
                        var deviceproxys = objCSproxy.GetDiscountPriceDetails(orderSummaryVM.ModelID, Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(), Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt(), 0);
                        if (deviceproxys.NewPlanName != null)
                        {
                            orderSummaryVM.DiscountPrice = deviceproxys.DiscountPrice.ToInt();
                            orderSummaryVM.SecondaryPlanName = deviceproxys.NewPlanName;
                            Session[SessionKey.SelectedPlanID_Seco.ToString()] = deviceproxys.PlanID;
                            orderSummaryVM.SecondaryMonthlySubscription = orderSummaryVM.SecondaryMonthlySubscription - orderSummaryVM.DiscountPrice;
                        }
                        else
                        {
                            Session[SessionKey.SelectedPlanID_Seco.ToString()] = 0;
                        }
                    }
                }
            }

            using (var proxy = new RegistrationServiceProxy())
            {
                if (regID.ToInt() > 0)
                {
                    DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(regID.ToInt());
                    if (lnkregdetails != null)
                    {
                        personalDetailsVM.Justification = lnkregdetails.Justification.ToString2();

                        if (lnkregdetails.SimType != null)
                        {
                            personalDetailsVM.SimType = lnkregdetails.SimType;
                        }
                        else
                        {
                            personalDetailsVM.SimType = "Normal";
                        }

                        /*Active PDPA Commented*/
                        /*Changes by chetan related to PDPA implementation*/
                        string documentversion = lnkregdetails.PdpaVersion;
                        personalDetailsVM.PDPAVersionStatus = documentversion;
                        if (!ReferenceEquals(documentversion, null))
                        {
                            using (var registrationProxy = new RegistrationServiceProxy())
                            {
                                var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", documentversion);
                                personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                            }
                        }
                    }
                    else
                    {
                        personalDetailsVM.SimType = "Normal";
                    }

                }
                /*Active PDPA Commented*/
                else
                {
                    /*Changes by chetan related to PDPA implementation*/
                    using (var registrationProxy = new RegistrationServiceProxy())
                    {
                        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                        personalDetailsVM.PDPDVersion = resultdata.Version;
                    }
                }
            }

            orderSummaryVM.IMEINumber = reg.IMEINumber;
            orderSummaryVM.SIMSerial = reg.SIMSerial;

            if (regSec != null)
            {
                orderSummaryVM.IMEINumberSeco = regSec.IMEINumber.ToString2();
                orderSummaryVM.SecondarySIMSerial = regSec.SIMSerial.ToString2();
            }
            if (regID.ToInt() > 0)
            {
                orderSummaryVM.SimType = personalDetailsVM.SimType;
                Session[SessionKey.SimType.ToString()] = personalDetailsVM.SimType;
            }

            if (reg.KenanAccountNo != null)
            { Session["KenanAccountNo"] = reg.KenanAccountNo; }
            else
            { Session["KenanAccountNo"] = "0"; }
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderSummaryVM;


            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session[SessionKey.ExternalID.ToString()].ToString2()) ? string.Empty : Session[SessionKey.ExternalID.ToString()].ToString2(), regID.ToInt());
            personalDetailsVM.Liberlization_Status = Result[0];
            personalDetailsVM.MOCStatus = Result[1];
            personalDetailsVM.SIMCardType = dropObj.mainFlow.simSize;//Anthony-[#2068]

			if (dropObj.msimIndex > 0)
			{
				if (!ReferenceEquals(dropObj.msimFlow[0], null) &&  !string.IsNullOrEmpty(dropObj.msimFlow[0].simSize))
				{
					personalDetailsVM.SecondarySIMType = dropObj.msimFlow[0].simSize;
				}
			}
            using (var registrationProxy = new RegistrationServiceClient())
            {
                personalDetailsVM.CDPUStatus = registrationProxy.GetCDPUStatus(regID.ToInt());
            }

            //CR 29JAN2016 - display bill cycle
            if (regID.ToInt() > 0)
            {
                if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault() != null)
                {
                    personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault().ATT_Value;
                }
                else
                {
                    personalDetailsVM.BillingCycle = "N/A";
                }
            }
            else
            {
                var accID = Session["AccExternalID"].ToString2();
                retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                var selectedAccount = AcctListByICResponse.itemList.Where(c => c.AcctExtId == accID).FirstOrDefault();
                var billCycle = (selectedAccount != null && selectedAccount.AccountDetails != null ? selectedAccount.AccountDetails.BillCycle : "N/A");
                personalDetailsVM.BillingCycle = (!string.IsNullOrWhiteSpace(billCycle) ? billCycle : "N/A");
            }

            return View(personalDetailsVM);
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,SMRT_MANAGER,MREG_HQ,MREG_DAB,DSupervisor,DAgent,MREG_DSV,DREG_N,DREG_C,DREG_CH,MREG_DAI,MREG_DIC,MREG_DAC")]
        [DoNoTCache]
        public ActionResult DMobileRegSummary(int? regID)
        {
            Session[SessionKey.SelectedPlanID.ToString()] = null;

            if (ReferenceEquals(Session["RegMobileReg_PersonalDetailsVM"], null))
            {
                if (!(Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("DAgent") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_C") || Roles.IsUserInRole("MREG_DAI")))
                {
                    if (!Request.UrlReferrer.AbsolutePath.Contains("/Registration/Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/StoreKeeper") && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegSuccess") && !Request.UrlReferrer.AbsolutePath.Contains("/Storekeeper/Storekeeper") && !Request.UrlReferrer.AbsolutePath.Contains("/Cashier/Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/SuperVisorForWaiveOff"))
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            var orderSummary = new OrderSummaryVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            var regMdlGrpModelSec = new RegMdlGrpModelSec();
            var mainLinePBPCIDsSec = new List<int>();
            var regSec = new DAL.Models.RegistrationSec();
            var suppLineRegPBPCsSec = new List<RegPgmBdlPkgCompSec>();
            int paymentstatus = -1;
            #region VLT ADDED CODE 21stFEB for Email functionality
            Session["RegID"] = regID;
            #endregion VLD ADDED CODE 21stFEB for Email functionality
            /*Chetan added for displaying the reson for failed transcation*/
            var KenanLogDetails = new DAL.Models.KenanaLogDetails();
            if (regID.ToInt() > 0)
            {
                ClearRegistrationSession();
                personalDetailsVM = new PersonalDetailsVM()
                {
                    RegID = regID.Value.ToInt(),
                };

                Session["RegMobileReg_PhoneVM"] = null;

                using (var proxy = new RegistrationServiceProxy())
                {
                    reg = proxy.RegistrationGet(regID.Value);

                    /*Himansu added for sim replacement . BASED ON THE MSISDN NEED TO GET THE SIM SERIAL NUM AND TYPE */

                    personalDetailsVM.MSISDN1 = reg.MSISDN1.ToString2();
                    /*Chetan added for displaying the reson for failed transcation*/
                    KenanLogDetails = proxy.KenanLogDetailsGet(regID.Value);
                    if (!ReferenceEquals(KenanLogDetails, null))
                    {
                        personalDetailsVM.MessageCode = KenanLogDetails.MessageCode.ToInt();
                        personalDetailsVM.MessageDesc = KenanLogDetails.MessageDesc.ToString();
                    }
                    //Added by Patanjali to get the payment status on 07-04-2013
                    paymentstatus = proxy.PaymentStatusGet(regID.Value);
                    if (paymentstatus != -1)
                    {
                        if (paymentstatus == 0)
                        {
                            personalDetailsVM.PaymentStatus = "0";
                        }
                        else
                        {
                            personalDetailsVM.PaymentStatus = "1";
                        }
                    }
                    else
                    {
                        personalDetailsVM.PaymentStatus = "0";
                    }
                    //Added by Patanjali to get the payment status on 07-04-2013

                    personalDetailsVM.QueueNo = reg.QueueNo;
                    //Added by Patanjali to support remarks on 30-03-2013
                    personalDetailsVM.Remarks = reg.Remarks;
                    //Added by Patanjali to support remarks on 30-03-2013 ends here
                    personalDetailsVM.BiometricVerify = reg.BiometricVerify;
                    personalDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
                    personalDetailsVM.BlacklistInternal = reg.InternalBlacklisted;
                    personalDetailsVM.SignatureSVG = reg.SignatureSVG;
                    personalDetailsVM.CustomerPhoto = reg.CustomerPhoto;
                    personalDetailsVM.AltCustomerPhoto = reg.AltCustomerPhoto;
                    personalDetailsVM.Photo = reg.Photo;
                    personalDetailsVM.RegSIMSerial = reg.SIMSerial;
                    personalDetailsVM.RegIMEINumber = reg.IMEINumber;
                    personalDetailsVM.RegTypeID = reg.RegTypeID;
                    personalDetailsVM.AgeCheckStatus = reg.AgeCheckStatus;
                    personalDetailsVM.DDMFCheckStatus = reg.DDMFCheckStatus;
                    personalDetailsVM.AddressCheckStatus = reg.AddressCheckStatus;
                    personalDetailsVM.OutStandingCheckStatus = reg.OutStandingCheckStatus;
                    personalDetailsVM.TotalLineCheckStatus = reg.TotalLineCheckStatus;
                    personalDetailsVM.PrincipleLineCheckStatus = reg.PrincipleLineCheckStatus;
                    personalDetailsVM.ContractCheckStatus = reg.ContractCheckStatus;
                    personalDetailsVM.UserType = reg.UserType;
                    personalDetailsVM.PlanAdvance = reg.PlanAdvance;
                    personalDetailsVM.PlanDeposit = reg.PlanDeposit;
                    personalDetailsVM.DeviceAdvance = reg.DeviceAdvance;
                    personalDetailsVM.DeviceDeposit = reg.DeviceDeposit;
                    personalDetailsVM.Discount = reg.Discount;
                    personalDetailsVM.KenanAccountNo = reg.KenanAccountNo;
                    personalDetailsVM.OutstandingAcs = reg.OutstandingAcs;
                    personalDetailsVM.PrePortInCheckStatus = reg.PrePortInCheckStatus;
                    personalDetailsVM.MNPServiceIdCheckStatus = reg.MNPServiceIdCheckStatus;
                    personalDetailsVM.MOC_Status = reg.MOC_Status;
                    personalDetailsVM.Whitelist_Status = reg.Whitelist_Status;
                    personalDetailsVM.SIMModel = reg.SimModelId.ToString2() == string.Empty ? "0" : reg.SimModelId.ToString();
                    //Added By Himansu for Sim Replacement
                    personalDetailsVM.fxAcctNo = reg.fxAcctNo;
                    personalDetailsVM.FxSubscrNo = reg.fxSubscrNo;
                    personalDetailsVM.FxSubScrNoResets = reg.fxSubScrNoResets;
                    personalDetailsVM.CRPType = reg.CRPType;
                    personalDetailsVM.IsVerified = reg.IsVerified;

                    Session[SessionKey.Condition.ToString()] = personalDetailsVM.CRPType.ToString2();
                    //Added by VLT on 13 Apr 2013
                    personalDetailsVM.IMPOSFileName = reg.IMPOSFileName;
                    //Added by VLT on 13 Apr 2013 ends here

                    // RegAccount
                    var regAccount = proxy.RegAccountGet(new int[] { reg.ID }).SingleOrDefault();
                    if (regAccount != null)
                    {
                        personalDetailsVM.ServiceActivateErrMsg = regAccount.KenanActivateServiceErrMessage;
                    }

                    // Registration Status
                    var regStatusID = proxy.RegStatusFind(new RegStatusFind()
                    {
                        RegStatus = new RegStatus()
                        {
                            RegID = reg.ID
                        },
                        Active = true
                    }).SingleOrDefault();

                    personalDetailsVM.Deposite = reg.AdditionalCharges;
                    personalDetailsVM.StatusID = proxy.RegStatusGet(new int[] { regStatusID }).SingleOrDefault().StatusID;


                    using (var configProxy = new ConfigServiceProxy())
                    {
                        personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;

                    }

                    //for cancel reason
                    if (personalDetailsVM.StatusID == 21)
                    {

                        using (var regProxy = new RegistrationServiceProxy())
                        {
                            string reason = regProxy.CancelReasonGet(personalDetailsVM.RegID);
                            personalDetailsVM.MessageDesc = reason;

                        }
                    }

                    ExtraTenGetResp objExtraTenDetails = new ExtraTenGetResp();
                    objExtraTenDetails = proxy.GetExtraTenDetails(personalDetailsVM.RegID);
                    List<lnkExtraTenDetails> modListExtraTen = new List<lnkExtraTenDetails>();
                    personalDetailsVM.ExtraTenDetails = objExtraTenDetails.ExtraTenDetails.ToList();
                    if (personalDetailsVM.ExtraTenDetails != null && personalDetailsVM.ExtraTenDetails.Count > 0)
                    {
                        foreach (var extraTen in personalDetailsVM.ExtraTenDetails)
                        {
                            if (string.IsNullOrEmpty(extraTen.MsgCode) == true)
                            {
                                extraTen.MsgCode = "Processing";
                            }
                            else if (extraTen.MsgCode == "0")
                            {
                                extraTen.MsgCode = "Success";
                            }
                            else
                            {
                                extraTen.MsgCode = "Fail";
                            }
                            modListExtraTen.Add(extraTen);

                        }
                        personalDetailsVM.ExtraTenDetails = modListExtraTen;
                    }
                    personalDetailsVM.ExtraTenMobileNumbers = string.Join(",", objExtraTenDetails.ExtraTenDetails.Where(e => e.Status == "A").Select(e => e.Msisdn).ToList());
                    Session[SessionKey.ExtratenMobilenumbers.ToString()] = personalDetailsVM.ExtraTenMobileNumbers;
                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);
                    Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;

                    // Customer
                    personalDetailsVM.Customer = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    // Address
                    var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                    {
                        Active = true,
                        Address = new Address()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();
                    personalDetailsVM.Address = proxy.RegAddressGet(regAddrIDs).SingleOrDefault();

                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    // RegMdlGrpModels
                    var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                    {
                        RegMdlGrpModel = new RegMdlGrpModel()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();
                    if (regMdlGrpModelIDs.Count() > 0)
                        regMdlGrpModel = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).SingleOrDefault();

                    if (regMdlGrpModel.ID == 0)
                    {
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
                    }
                    else
                    {
                        Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = regMdlGrpModel.Price.ToDecimal();
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
                    }

                    if (personalDetailsVM.RegTypeID == (int)MobileRegType.CRP)
                        Session[SessionKey.RegMobileReg_Type.ToString()] = personalDetailsVM.RegTypeID;

                    // RegPgmBdlPkgComponent
                    var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                    {
                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();

                    // Main Line PgmBdlPkgComponent
                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    if (regPgmBdlPkgCompIDs.Count() > 0)
                        regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind()
                    {
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();

                    if (suppLineIDs.Count() > 0)
                    {
                        suppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    }

                    //<Start> - 20150914 - Samuel - display sales code in summary page
                    personalDetailsVM.Registration = proxy.RegistrationGet(personalDetailsVM.RegID);
                    personalDetailsVM.RegAttributes = proxy.RegAttributesGetByRegID(personalDetailsVM.RegID);
                    using (var proxyOrg = new OrganizationServiceProxy())
                    {
                        if (!ReferenceEquals(personalDetailsVM.Registration, null) && !ReferenceEquals(personalDetailsVM.Registration.CenterOrgID, null) && personalDetailsVM.Registration.CenterOrgID != 0)
                        {
                            personalDetailsVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { personalDetailsVM.Registration.CenterOrgID }).FirstOrDefault();
                        }
                    }
                    //<End> - 20150914 - Samuel - display sales code in summary page
                }

                if (suppLines.Count() > 0)
                    WebHelper.Instance.ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

                using (var proxy = new RegistrationServiceProxy())
                {
                    regSec = proxy.RegistrationSecGet(regID.Value);
                    if (regSec != null)
                    {
                        personalDetailsVM.RegIDSeco = regSec.ID;
                        personalDetailsVM.MSISDNSeco = regSec.MSISDN1;
                        personalDetailsVM.PlanAdvanceSeco = regSec.PlanAdvance;
                        personalDetailsVM.PlanDepositSeco = regSec.PlanDeposit;
                        personalDetailsVM.DeviceAdvanceSeco = regSec.DeviceAdvance;
                        personalDetailsVM.DeviceDepositSeco = regSec.DeviceDeposit;
                        personalDetailsVM.RegTypeIDSeco = regSec.RegTypeID;
                        personalDetailsVM.RegSIMSerialSeco = regSec.SIMSerial;
                        personalDetailsVM.RegIMEINumberSeco = regSec.IMEINumber;
                        personalDetailsVM.MISMSIMModel = regSec.SimModelId.ToString2() == string.Empty ? "0" : regSec.SimModelId.ToString();
                        // MobileNo
                        var SecmobileNos = new List<string>();
                        SecmobileNos.Add(regSec.MSISDN1);
                        SecmobileNos.Add(regSec.MSISDN2);
                        //Commented for MISM virtual numbers flow
                        Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] = "";

                        // RegMdlGrpModels for Secondary
                        var regMdlGrpModelIDs = proxy.RegMdlGrpModelSecFind(new RegMdlGrpModelSecFind()
                        {
                            RegMdlGrpModelSec = new RegMdlGrpModelSec()
                            {
                                SecRegID = regSec.ID
                            },
                            Active = true
                        }).ToList();
                        if (regMdlGrpModelIDs.Count() > 0)
                            regMdlGrpModelSec = proxy.RegMdlGrpModelSecGet(regMdlGrpModelIDs).SingleOrDefault();

                        if (regMdlGrpModelSec.ID == 0)
                        {
                            Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
                        }
                        else
                        {
                            Session["RegMobileReg_MainDevicePrice_Seco"] = regMdlGrpModelSec.Price.ToDecimal();
                            Session["RegMobileReg_SelectedModelImageID_Seco"] = regMdlGrpModelSec.ModelImageID;
                            Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
                        }

                        if (personalDetailsVM.RegTypeID == (int)MobileRegType.CRP)
                            Session[SessionKey.RegMobileReg_Type.ToString()] = personalDetailsVM.RegTypeID;

                        // RegPgmBdlPkgComponent for Secondary
                        var regPgmBdlPkgCompIDsSec = proxy.RegPgmBdlPkgCompSecFind(new RegPgmBdlPkgCompSecFind()
                        {
                            RegPgmBdlPkgCompSec = new RegPgmBdlPkgCompSec()
                            {
                                RegID = regSec.ID
                            },
                            Active = true
                        }).ToList();

                        // Secondary Line PgmBdlPkgComponent
                        var regPBPCsSec = new List<RegPgmBdlPkgCompSec>();
                        if (regPgmBdlPkgCompIDsSec.Count() > 0)
                            regPBPCsSec = proxy.RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec).ToList();

                        if (regPBPCsSec.Count() > 0)
                            mainLinePBPCIDsSec = regPBPCsSec.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();


                        // Sub Line RegPgmBdlPkgComponent
                        suppLineRegPBPCsSec = regPBPCsSec.Where(a => a.RegSuppLineID != null).ToList();
                    }
                }

                using (var proxy = new CatalogServiceProxy())
                {
                    string userICNO = Util.SessionAccess.User.IDCardNo;

                    if (userICNO != null && userICNO.Length > 0)
                    {
                        objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                    }
                    if (objWhiteListDetailsResp != null)
                    {
                        if (objWhiteListDetailsResp.Description != null)
                        {
                            personalDetailsVM._WhiteListDetails.Description = objWhiteListDetailsResp.Description;
                        }
                    }

                    if (regMdlGrpModel.ID > 0)
                        Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;
                    //Model for Secondary
                    if (regMdlGrpModelSec.ID > 0)
                        Session["RegMobileReg_SelectedModelID_Seco"] = proxy.BrandArticleImageGet(regMdlGrpModelSec.ModelImageID).ModelID;

                    var mainLinePBPCs = new List<PgmBdlPckComponent>();
                    if (mainLinePBPCIDs.Count() > 0)
                        mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

                    var mainLinePBPCsSec = new List<PgmBdlPckComponent>();
                    if (mainLinePBPCIDsSec.Count() > 0)
                        mainLinePBPCsSec = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDsSec).ToList();

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var DPCode = Properties.Settings.Default.RegType_DevicePlan;
                    var MAcode = Properties.Settings.Default.Master_Component;
                    var MPcode = Properties.Settings.Default.Main_package;
                    var CPCode = Properties.Settings.Default.ComplimentaryPlan;
                    var DCCode = Properties.Settings.Default.CompType_DataCon;
                    var ECCode = Properties.Settings.Default.Extra_Component;
                    var SPCode = Properties.Settings.Default.SecondaryPlan;
                    //Uncommented below code as  Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] is assigned with current planytype which is used in turn to get package details
                    //Commented below If conditions as it is being not used any where. 
                    //Date: 08 july 2013 .This If condition is impacting SK module. So commented condition. Please discuss with CRP Plan only team before uncommenting.  
                    if (mainLinePBPCs.Count() > 0)
                    {
                        Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode || a.PlanType == SPCode)).ToList().FirstOrDefault().ID;
                        var vasIDs = string.Empty;
                        var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode).Select(a => a.ID).ToList();

                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session[SessionKey.RegMobileReg_VasIDs.ToString()] = vasIDs;

                    }
                    if (mainLinePBPCsSec.Count() > 0)
                    {
                        var componentList = mainLinePBPCsSec.Where(a => (a.LinkType == bpCode || a.LinkType == SPCode) && (a.PlanType == CPCode || a.PlanType == SPCode)).ToList();
                        if (componentList != null && componentList.Count > 0)
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = mainLinePBPCsSec.Where(a => (a.LinkType == bpCode || a.LinkType == SPCode) && (a.PlanType == CPCode || a.PlanType == SPCode)).FirstOrDefault().ID;
                        else
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = mainLinePBPCsSec.Where(a => a.LinkType == SPCode && (a.PlanType == MPcode)).FirstOrDefault().ID;

                        var vasIDs = string.Empty;
                        var pkgCompIDs = mainLinePBPCsSec.Where(a => a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode).Select(a => a.ID).ToList();

                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = vasIDs;
                    }
                }
            }
            if (reg.OfferID > 0)
            {
                Session[SessionKey.OfferId.ToString()] = reg.OfferID;
            }
            //Pavan
            var orderSummaryVM = ConstructOrderSummary();


            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] != null)
            {
                if (Session["regid"] != null)
                {
                    if (regID.Value > 0)
                    {
                        CatalogServiceProxy objCSproxy = new CatalogServiceProxy();
                        var deviceproxys = objCSproxy.GetDiscountPriceDetails(orderSummaryVM.ModelID, Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), ReferenceEquals(Session[SessionKey.RegMobileReg_ContractID.ToString()], null) ? 0 : Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt(), 0);
                        if (deviceproxys.NewPlanName != null)
                        {
                            orderSummaryVM.DiscountPrice = deviceproxys.DiscountPrice.ToInt();
                            orderSummaryVM.PlanName = deviceproxys.NewPlanName;
                            orderSummaryVM.MonthlySubscription = orderSummaryVM.MonthlySubscription - orderSummaryVM.DiscountPrice;
                        }
                    }

                }
            }

            //For Secondary plan
            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] != null)
            {
                if (Session["regid"] != null)
                {
                    if (regID.Value > 0)
                    {
                        CatalogServiceProxy objCSproxy = new CatalogServiceProxy();
                        var deviceproxys = objCSproxy.GetDiscountPriceDetails(orderSummaryVM.ModelID, Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(), Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt(), 0);
                        if (deviceproxys.NewPlanName != null)
                        {
                            orderSummaryVM.DiscountPrice = deviceproxys.DiscountPrice.ToInt();
                            orderSummaryVM.SecondaryPlanName = deviceproxys.NewPlanName;
                            Session[SessionKey.SelectedPlanID_Seco.ToString()] = deviceproxys.PlanID;
                            orderSummaryVM.SecondaryMonthlySubscription = orderSummaryVM.SecondaryMonthlySubscription - orderSummaryVM.DiscountPrice;
                        }
                        else
                        {
                            Session[SessionKey.SelectedPlanID_Seco.ToString()] = 0;
                        }
                    }
                }
            }

            using (var proxy = new RegistrationServiceProxy())
            {
                if (regID.ToInt() > 0)
                {
                    DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(regID.ToInt());
                    if (lnkregdetails != null)
                    {
                        personalDetailsVM.Justification = lnkregdetails.Justification.ToString2();

                        if (lnkregdetails.SimType != null)
                        {
                            personalDetailsVM.SimType = lnkregdetails.SimType;
                        }
                        else
                        {
                            personalDetailsVM.SimType = "Normal";
                        }

                        /*Active PDPA Commented*/
                        /*Changes by chetan related to PDPA implementation*/
                        string documentversion = lnkregdetails.PdpaVersion;
                        personalDetailsVM.PDPAVersionStatus = documentversion;
                        if (!ReferenceEquals(documentversion, null))
                        {
                            using (var registrationProxy = new RegistrationServiceProxy())
                            {
                                var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", documentversion);
                                personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                            }
                        }
                    }
                    else
                    {
                        personalDetailsVM.SimType = "Normal";
                    }

                }
                /*Active PDPA Commented*/
                else
                {
                    /*Changes by chetan related to PDPA implementation*/
                    using (var registrationProxy = new RegistrationServiceProxy())
                    {
                        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                        personalDetailsVM.PDPDVersion = resultdata.Version;
                    }
                }
            }

            orderSummaryVM.IMEINumber = reg.IMEINumber;
            orderSummaryVM.SIMSerial = reg.SIMSerial;

            if (regSec != null)
            {
                orderSummaryVM.IMEINumberSeco = regSec.IMEINumber.ToString2();
                orderSummaryVM.SecondarySIMSerial = regSec.SIMSerial.ToString2();
            }
            if (regID.ToInt() > 0)
            {
                orderSummaryVM.SimType = personalDetailsVM.SimType;
                Session[SessionKey.SimType.ToString()] = personalDetailsVM.SimType;
            }

            if (reg.KenanAccountNo != null)
            { Session["KenanAccountNo"] = reg.KenanAccountNo; }
            else
            { Session["KenanAccountNo"] = "0"; }
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderSummaryVM;


            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session[SessionKey.ExternalID.ToString()].ToString2()) ? string.Empty : Session[SessionKey.ExternalID.ToString()].ToString2(), regID.ToInt());
            personalDetailsVM.Liberlization_Status = Result[0];
            personalDetailsVM.MOCStatus = Result[1];

            using (var registrationProxy = new RegistrationServiceClient())
            {
                personalDetailsVM.CDPUStatus = registrationProxy.GetCDPUStatus(regID.ToInt());
            }

            return View(personalDetailsVM);
        }
        public ActionResult PaymntReceived(int regID)
        {
            return View(regID);
        }
        /// <summary>        
        /// 1.Extracts the file information passed from Request collection. 
        /// 2.Uploads the actual image and resized image in temporary location.
        /// 3.Returns the image in base 64 encoded format.
        /// </summary>
        /// <Author>Sutan Dan</Author>
        /// <returns>TypeOf(Json)</returns>
        [HttpPost]
        public ActionResult UploadImage()
        {
            ///Get the upload and resize locations from settings
            ///PLEASE CREATE 'Uploads' AND 'Resized' FOLDERS IN THE PROJECT
            ///FOR EXAMPLE Online.Registration.Web/Uploads AND Online.Registration.Web/Resized
            string imageUploadLocation = Server.MapPath(Properties.Settings.Default.imageUploadLocation);
            string imageResizeLocation = Server.MapPath(Properties.Settings.Default.imageResizeLocation);
            string fitImage = Properties.Settings.Default.fitImage;

            bool isResized = false;

            string SourceImageName, ActualImageName, onlyFileExtension, imageUrl, imageType = string.Empty;

            System.Drawing.Image srcimage = null;
            imageUrl = string.Empty;

            ///CHECKS WHETHER THE Request.Files
            if (!ReferenceEquals(Request.Files, null) && Request.Files.Count > 0)
            {
                ///RETRIEVES THE FIRST KEY IN CASE THERE WOULD BE MULTIPLE FOUND.
                string PostedFileName = Request.Files.AllKeys.FirstOrDefault();
                ///TYPECASTING TO HttpPostedFileBase
                HttpPostedFileBase hpf = Request.Files[PostedFileName] as HttpPostedFileBase;

                ///ONLY PROCESS IF LENGTH >0
                if (hpf.ContentLength > 0)
                {
					//Keep the actual imagename for future reference
					ActualImageName = SourceImageName = Path.GetFileName(hpf.FileName);
					imageType = hpf.ContentType;
					///Check whether there is already an image in the folder with the same name
					Util.checkFileExistance(imageUploadLocation, ref SourceImageName, out onlyFileExtension);
					imageUploadLocation = Path.Combine(imageUploadLocation + @"\", SourceImageName);
					///Save the image
					hpf.SaveAs(imageUploadLocation);

					srcimage = System.Drawing.Image.FromFile(imageUploadLocation);
					///CALL TO CHECK AND LOWER THE RESOLUTION OF HIGH RESOULTION IMAGE
					//isResized = Util.ResizeImage(srcimage, 73, imageResizeLocation + SourceImageName, 540, 365, fitImage);
					isResized = Util.AlwaysResizeImage(srcimage, 23, imageResizeLocation + SourceImageName, 540, 365, fitImage);

					///Return the processed or unprocessed image as url
					if (isResized)
					{
						imageResizeLocation = Path.Combine(imageResizeLocation + @"\", SourceImageName);
						imageUrl = string.Format(@"data:{0};base64,", imageType) + Convert.ToBase64String(System.IO.File.ReadAllBytes(imageResizeLocation));
					}
					else
					{
						imageUrl = string.Format(@"data:{0};base64,", imageType) + Convert.ToBase64String(System.IO.File.ReadAllBytes(imageUploadLocation));
					}
                }
            }
            ///RETURNS THE IMAGE IN BASE 64 ENCODED FORMAT
            var jsonData = new { imageUrl = imageUrl };

            //return Json(jsonData);
            var serializer = new JavaScriptSerializer();

            serializer.MaxJsonLength = Int32.MaxValue;

            var data = new ContentResult
            {
                Content = serializer.Serialize(jsonData),
                ContentType = "application/json"
            };

            return data;
        }
        [HttpPost]
        public ActionResult RetrieveMyKadInfo(string icno)
        {
            var resp = new AMRSvc.RetrieveMyKadInfoRp();


            using (var proxy = new AMRServiceProxy())
            {

                resp = proxy.RetrieveMyKadInfo(new AMRSvc.RetrieveMyKadInfoRq()
                {
                    ApplicationID = 3,
                    DealerCode = Util.SessionAccess.User.Org.DealerCode,
                    NewIC = icno
                });
            }

            if (string.IsNullOrEmpty(resp.FingerThumb))
            {
                Session["RegMobileReg_BiometricVerify"] = false;
            }
            else
            {
                Session["RegMobileReg_BiometricVerify"] = true;

                using (var proxy = new RegistrationServiceProxy())
                {
                    Session["RegMobileReg_BiometricID"] = proxy.BiometricCreate(WebHelper.Instance.ConstructBriometri(resp));
                }
            }

            return Json(resp);
        }

        public ActionResult PaymentDetails(int id)
        {
            Online.Registration.Web.ViewModels.OrderSummaryVM MobileRegOrderSummary = (OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()];
            string Amount = (MobileRegOrderSummary.PersonalDetailsVM.PlanAdvance + MobileRegOrderSummary.PersonalDetailsVM.PlanDeposit + MobileRegOrderSummary.PersonalDetailsVM.DeviceAdvance + MobileRegOrderSummary.PersonalDetailsVM.DeviceDeposit + ((MobileRegOrderSummary.ItemPrice != 0) ? MobileRegOrderSummary.ItemPrice : MobileRegOrderSummary.ModelPrice)).ToString();
            Session["OrderId"] = null;
            Session["Amount"] = null;
            ViewBag.Amount = Amount;
            ViewBag.OrderId = id;
            return View();
        }

        [HttpPost]
        public ActionResult PaymentDetails(FormCollection frm)
        {
            string OrderId = Session["OrderId"].ToString();
            string Amount = Session["Amount"].ToString() + "00";
            string HashingSaltValue = "1234567890";
            string HashingSalt = WebHelper.Instance.CalculateMD5Hash(HashingSaltValue + Amount + OrderId + OrderId);

            if (frm["CustomerInfo.PayModeID"] == "1")
            {
                return Redirect("PaymentDetailsSuccess?id=" + OrderId + "&paymentstatus=Success");
            }
            else if (frm["CustomerInfo.PayModeID"] == "2")
            {
                //return Redirect("https://merchantstore/payment.aspx?MerchTxnRef=" + OrderId + "&OrderInfo=" + OrderId + "&Amount=" + Amount + "&Hash=" + HashingSalt);
                return Redirect("http://203.121.86.30/MaxisPaymentISELL/PGGP/doCardPayment.aspx?MerchTxnRef=" + OrderId + "&OrderInfo=" + OrderId + "&Amount=" + Amount + "&Hash=" + HashingSalt);
            }
            return Redirect("PaymentDetailsSuccess?id=" + OrderId + "&paymentstatus=Failed");
        }

        [ValidateInput(false)]
        public ActionResult PaymentDetailsSuccess(string id, string paymentstatus)
        {

            Int32 getId = 0;
            ViewBag.Paymentstatus = paymentstatus;

            if (!Int32.TryParse(id, out getId))
            {
                ViewBag.Paymentstatus = "Failed";
                return View();
            }

            if (!ReferenceEquals(ViewBag.Paymentstatus, null))
            {
                using (var proxy = new RegistrationServiceProxy())
                {
                    proxy.RegistrationPaymentSuccess(new RegStatus()
                    {
                        RegID = Convert.ToInt32(getId),
                        Active = true,
                        CreateDT = DateTime.Now,
                        StartDate = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName,
                        StatusID = "Success".Equals(ViewBag.Paymentstatus) ? Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegPaymentSuccess) : Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegPaymentFailed)
                    });
                }
            }
            return RedirectToAction("MobileRegSummary", new { id = getId });

        }

		public ActionResult PrintContractDetails(int id, bool showInventorySection = true)
		{
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
			var regFormVM = new RegistrationFormVM();
			var modelImageIDs = new List<int>();
			var pbpcIDs = new List<int>();
			var suppLineVASIDs = new List<int>();
			var suppLineForms = new List<SuppLineForm>();
			var regMdlGrpModels = new List<RegMdlGrpModel>();
			var regMdlGrpModelsSec = new List<RegMdlGrpModelSec>();
			var modelImageIDsSec = new List<int>();
			var pbpcIDsSec = new List<int>();
			var suppLineFormsSec = new List<SuppLineForm>();
			var printVersionInfo = new List<PrintVersionDetails>();

			//Joshi Added for DME Printer
			//if (HttpContext.Current.Request.UserAgent.ToLower().Contains("ipad"))
			if (HttpContext.Request.Headers["User-Agent"].ToLower().Contains("ipad"))
			{
				var UserDMEPrint = new UserDMEPrint();
				UserDMEPrint.RegID = id;
				UserDMEPrint.UserID = Util.SessionAccess.UserID;
				var status = Util.SaveUserDMEPrint(UserDMEPrint);
				Session["StatusDME"] = status;
			}
			//End of Joshi


			var PlanKenanCode = string.Empty;
			using (var proxy = new RegistrationServiceProxy())
			{
				// Registration
				regFormVM.Registration = proxy.RegistrationGet(id);

				// Registration Secondary
				regFormVM.RegistrationSec = proxy.RegistrationSecGet(id);

				// Customer
				regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

				// Address
				var regAddrIDs = proxy.RegAddressFind(new AddressFind()
				{
					Active = true,
					Address = new Address()
					{
						RegID = id
					}
				}).ToList();
				regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

				// RegMdlGrpModels
				var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
				{
					RegMdlGrpModel = new RegMdlGrpModel()
					{
						RegID = id
					}
				}).ToList();
				if (regMdlGrpModelIDs.Count() > 0)
				{
					regMdlGrpModels = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
					modelImageIDs = regMdlGrpModels.Select(a => a.ModelImageID).ToList();
				}


				// RegMdlGrpModels for Secondary
				if (regFormVM.RegistrationSec != null)
				{
					var regMdlGrpModelIDsSec = proxy.RegMdlGrpModelSecFind(new RegMdlGrpModelSecFind()
					{
						RegMdlGrpModelSec = new RegMdlGrpModelSec()
						{
							SecRegID = regFormVM.RegistrationSec.ID
						}
					}).ToList();


					if (regMdlGrpModelIDsSec.Count() > 0)
					{
						regMdlGrpModelsSec = proxy.RegMdlGrpModelSecGet(regMdlGrpModelIDsSec).ToList();
						modelImageIDsSec = regMdlGrpModelsSec.Select(a => a.ModelImageID).ToList();
					}

				}


				//Commented by VLT on 11 Apr 2013
				// RegPgmBdlPkgComponent
				var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
				{
					RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
					{
						RegID = id,
					}
				}).ToList();

				var pbpc = new List<RegPgmBdlPkgComp>();
				if (regPgmBdlPkgCompIDs.Count() > 0)
					pbpc = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();


				//main line
				if (pbpc.Count() > 0)
					pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();

				// RegPgmBdlPkgComponent for Secondary
				if (regFormVM.RegistrationSec != null)
				{
					var regPgmBdlPkgCompIDsSec = proxy.RegPgmBdlPkgCompSecFind(new RegPgmBdlPkgCompSecFind()
					{
						RegPgmBdlPkgCompSec = new RegPgmBdlPkgCompSec()
						{
							RegID = regFormVM.RegistrationSec.ID,
						}
					}).ToList();

					var pbpcSec = new List<RegPgmBdlPkgCompSec>();
					if (regPgmBdlPkgCompIDsSec.Count() > 0)
						pbpcSec = proxy.RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec).ToList();

					//main line
					if (pbpcSec.Count() > 0)
						pbpcIDsSec = pbpcSec.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();


				}


				var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = id } }).ToList();
				if (suppLineIDs.Count() > 0)
				{
					var regSuppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
					foreach (var regSuppLine in regSuppLines)
					{
						regFormVM.Registration.PlanAdvance += regSuppLine.planadvance;
						regFormVM.Registration.PlanDeposit += regSuppLine.plandeposit;

						suppLineForms.Add(new SuppLineForm()
						{
							SuppLinePBPCID = regSuppLine.PgmBdlPckComponentID,
							SuppLineVASIDs = pbpc.Where(a => a.RegSuppLineID == regSuppLine.ID && a.PgmBdlPckComponentID != regSuppLine.PgmBdlPckComponentID).Select(a => a.PgmBdlPckComponentID).ToList(),
							SIMSerial = regSuppLine.SIMSerial,
							MSISDN = regSuppLine.MSISDN1,
							SimModelId = regSuppLine.SimModelId
						});
					}
				}

				ExtraTenGetResp objExtraTenDetails = new ExtraTenGetResp();
				objExtraTenDetails = proxy.GetExtraTenDetails(id);
				List<lnkExtraTenDetails> modListExtraTen = new List<lnkExtraTenDetails>();
				regFormVM.ExtraTenDetails = objExtraTenDetails.ExtraTenDetails.ToList();

			}

			using (var proxy = new CatalogServiceProxy())
			{

				// ModelGroupModel
				if (modelImageIDs.Count() > 0)
				{
					var modelImages = proxy.BrandArticleModelImageGet(modelImageIDs).ToList();
					var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());

					foreach (var regMdlGrpModel in regMdlGrpModels)
					{
						var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
						regFormVM.DeviceForms.Add(new DeviceForm()
						{
							Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
							Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
							Price = regMdlGrpModel.Price.ToDecimal()
						});

						var colour = Util.GetNameByID(RefType.Colour, modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ColourID);
						Session["RegMobileReg_DeviceColour"] = null;
						Session["RegMobileReg_DeviceColour"] = colour;
					}
				}
				var vasNames = (List<string>)Session["RegMobileReg_VasNames"];
				int NewPackageId = 0;

				// ModelGroupModel for Secondary
				if (modelImageIDsSec.Count() > 0)
				{
					var modelImages = proxy.BrandArticleModelImageGet(modelImageIDsSec).ToList();
					var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());

					foreach (var regMdlGrpModel in regMdlGrpModelsSec)
					{
						var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
						regFormVM.DeviceFormsSec.Add(new DeviceForm()
						{
							Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
							Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
							Price = regMdlGrpModel.Price.ToDecimal()
						});
					}
				}

				var vasNamesSec = (List<string>)Session["RegMobileReg_VasNames_Seco"];

				// BundlePackage, PackageComponents
				if (pbpcIDs.Count() > 0)
				{
					var pgmBdlPkgComps = MasterDataCache.Instance.FilterComponents(pbpcIDs).Distinct().ToList();
					var bpCode = Properties.Settings.Default.Bundle_Package;
					var pcCode = Properties.Settings.Default.Package_Component;
					var spCode = Properties.Settings.Default.SecondaryPlan;
					regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode && (a.PlanType == "CP" || a.PlanType == "SP")).ToList();
					if (regFormVM.BundlePackages.Count > 0)
					{
						regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode && (a.PlanType == "CP" || a.PlanType == "SP")).ToList();
					}
					else
					{
						regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == spCode).ToList();
					}
					if (regFormVM.BundlePackages.Count > 0)
						NewPackageId = regFormVM.BundlePackages[0].ID;
					PlanKenanCode = regFormVM.BundlePackages[0].KenanCode;//Added for T&C implementation

					regFormVM.PackageComponents = pgmBdlPkgComps.Where((a => a.LinkType.ToLower() == "pc" || a.LinkType.ToLower() == "ec" || a.LinkType.ToLower() == "dc" || a.LinkType.ToLower() == "ma" || a.LinkType.ToLower() == "oc" || a.LinkType.ToLower() == "rc" || a.LinkType.ToLower() == "cr" || a.LinkType.ToLower() == "md" || a.LinkType.ToLower() == "dm" || a.LinkType.ToLower() == "ic")).ToList();
				}
				List<int> vasids = new List<int>();
				if ((regFormVM.Registration.CRPType == "1") || (regFormVM.Registration.CRPType == "3"))
				{
					List<RegSmartComponents> selectedcomps = WebHelper.Instance.GetSelectedComponents(regFormVM.Registration.ID.ToString());
					List<string> compkenancodes = selectedcomps.Select(a => a.ComponentID).ToList();
					ComponentViewModel Componentresponse = GetSelectedPackageInfo_smart(0, NewPackageId, "");
					List<string> vasnames = new List<string>();

					if (Componentresponse.NewComponentsList.Count > 0)
					{
						for (int cnt = 0; cnt < Componentresponse.NewComponentsList.Count; cnt++)
						{
							if (compkenancodes.Contains(Componentresponse.NewComponentsList[cnt].NewComponentKenanCode))
								vasids.Add(Componentresponse.NewComponentsList[cnt].NewComponentId.ToInt());

						}

						// getting dependency ids
						List<int> depCompIds = new List<int>();
						foreach (var vasid in vasids)
						{
							if (vasid > 0)
							{

								depCompIds.AddRange(WebHelper.Instance.GetDepenedencyComponentsCRPPlan(vasid.ToString()));

							}
						}
						foreach (var depid in depCompIds)
						{
							if (depid > 0)
							{
								vasids.Add(depid.ToInt());
							}

						}

						Session["RegMobileReg_VasNames"] = proxy.ComponentGet(MasterDataCache.Instance.FilterComponents(vasids).Select(a => a.ChildID).ToList()).Select(a => a.Name).ToList();
					}
				}
				Session["RegMobileReg_VasNamesWithPackages"] = null;
				//Getting Vas names with Packages for Change Rate Plan
				if (regFormVM.Registration.RegTypeID == 19)
				{
					if ((regFormVM.Registration.CRPType == "2") || (regFormVM.Registration.CRPType == "4"))
					{
						// p2s and s2p                         
						vasids = (from cmp in regFormVM.PackageComponents select cmp.ID).ToList();
					}

					var components = MasterDataCache.Instance.FilterComponents(vasids);
					List<ComponentWithPkg> compswithpackages = (from ct in components select new ComponentWithPkg(ct.ChildID, ct.LinkType)).ToList();

					if (compswithpackages != null && compswithpackages.Count > 0)
					{
						compswithpackages = compswithpackages.GroupBy(p => new { p.ComponentId, p.LinkType }).Select(g => g.FirstOrDefault()).ToList();
					}

					Session["RegMobileReg_VasNamesWithPackages"] = proxy.ComponentGetWithPackage(compswithpackages);
				}

				// BundlePackage, PackageComponents for Secondary
				if (pbpcIDsSec.Count() > 0)
				{
					var pgmBdlPkgCompsSec = MasterDataCache.Instance.FilterComponents(pbpcIDsSec).Distinct().ToList();
					var bpCode = Properties.Settings.Default.Bundle_Package;
					var pcCode = Properties.Settings.Default.Package_Component;

					//regFormVM.BundlePackagesSec = pgmBdlPkgCompsSec.Where(a => a.LinkType == bpCode).ToList();
					regFormVM.BundlePackagesSec = pgmBdlPkgCompsSec.Where(a => (a.LinkType == bpCode || a.LinkType == "SP") && (a.PlanType == "CP" || a.PlanType == "SP")).ToList();
					regFormVM.PackageComponentsSec = pgmBdlPkgCompsSec.Where((a => a.LinkType.ToLower() == "pc" || a.LinkType.ToLower() == "ec" || a.LinkType.ToLower() == "dc" || a.LinkType.ToLower() == "ma" || a.LinkType.ToLower() == "rc" || a.LinkType.ToLower() == "cr" || a.LinkType.ToLower() == "md" || a.LinkType == "ic")).ToList();
				}

				foreach (var suppLineForm in suppLineForms)
				{
					//supp line package name
					suppLineForm.SuppLineBdlPkgName = proxy.PackageGet(MasterDataCache.Instance.FilterComponents(new int[] { suppLineForm.SuppLinePBPCID })
																.Select(a => a.ChildID)).SingleOrDefault().Name;
					//supp line vas names
					suppLineForm.SuppLineVASNames = proxy.ComponentGet(MasterDataCache.Instance.FilterComponents(suppLineForm.SuppLineVASIDs).Where(a => a.LinkType.ToLower() != "bp").Select(a => a.ChildID).ToList()).Select(a => a.Name).ToList();
				}
				regFormVM.SuppLineForms = suppLineForms;

				if (regFormVM.Registration.OfferID > 0)
				{
					string offerName = proxy.GetOfferNameById(Convert.ToInt32(regFormVM.Registration.OfferID));
					Session["offerNamebyId"] = offerName;

				}
				if (regFormVM.Registration.RegTypeID == Util.GetIDByCode(RefType.RegType, Settings.Default.RegType_DevicePlan))
				{
					List<DAL.Models.Component> componentsList = Util.GetContractsBYRegId(id);
					if (componentsList != null && componentsList.Count > 0)
					{
						Session["SelectedContractName"] = componentsList.Select(vas => vas.Name).ToList();
						int Duration = 0;
						foreach (var item in componentsList)
						{
							using (var RSproxy = new CatalogServiceProxy())
							{
								string strValue = RSproxy.GetContractByCode(item.Code);
								if (Duration < strValue.ToInt())
								{
									Duration = strValue.ToInt();
								}
							}

						}
						Session["SelectedContractValue"] = Duration;
					}

					List<VoiceContractDetails> lstVoiceContractAddendums = Util.GetVoiceContractsBYRegId(id);
					if (lstVoiceContractAddendums != null && lstVoiceContractAddendums.Count > 0)
					{
						Session["VoiceAddendums"] = lstVoiceContractAddendums[0].ContractDetails;
					}
				}
				//secondary Offer Name
				if (regFormVM.RegistrationSec != null)
				{
					if (regFormVM.RegistrationSec.RegistrationID > 0)
					{
						if (regFormVM.RegistrationSec.OfferID > 0)
						{
							string offerName = proxy.GetOfferNameById(Convert.ToInt32(regFormVM.RegistrationSec.OfferID));
							Session["offerNamebyId_Seco"] = offerName;

						}
					}
				}
				//Contract Name Dispaly for Secondary
				if (regFormVM.RegistrationSec != null)

					if (regFormVM.RegistrationSec.RegTypeID == Util.GetIDByCode(RefType.RegType, Settings.Default.RegType_DevicePlan))
					{

						int contractId = proxy.GetContractIdByRegIDSeco(regFormVM.RegistrationSec.ID);
						if (contractId > 0)
						{
							List<int> contractsList = new List<int>();
							List<Component> componentsList = new List<Component>();
							contractsList.Add(contractId);

							var vases = MasterDataCache.Instance.FilterComponents
							(
							(IEnumerable<int>)contractsList
							).ToList();

							componentsList = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();
							if (componentsList != null && componentsList.Count > 0)
							{
								string selectedContract = string.Empty;
								string strValue = proxy.GetContractByCode(componentsList[0].Code);
								Session["SelectedContractValue_Seco"] = strValue;
								List<string> lstComponentNames = new List<string>();
								lstComponentNames = componentsList.Where(vas => vas.ID == vases[0].ChildID).Select(vas => vas.Name).ToList();
								if (lstComponentNames != null && lstComponentNames.Count > 0)
								{
									selectedContract = lstComponentNames[0];
									Session["SelectedContractName_Seco"] = selectedContract;
								}

							}
						}
					}

			}

			using (var proxy = new RegistrationServiceProxy())
			{
				DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(id);
				Online.Registration.DAL.Models.PrintVersion objPrintVersionDetails = new Online.Registration.DAL.Models.PrintVersion();

				//Condition added by ravi on nov 1 2013 as it was throwing exception.
				if (lnkregdetails != null)
				{
					objPrintVersionDetails = proxy.GetPrintTsCsInfo(lnkregdetails.PrintVersionNo.ToString2(), regFormVM.Registration.RegTypeID, regFormVM.Registration.ArticleID == null ? string.Empty : regFormVM.Registration.ArticleID, PlanKenanCode, "");
					if (objPrintVersionDetails != null)
					{
						if (objPrintVersionDetails.ConTC1ModelIds != null && objPrintVersionDetails.ConTC1ModelIds != null)
						{
							string BrandModelID = proxy.GetBrandModelIDbyArticleID(regFormVM.Registration.ArticleID == null ? string.Empty : regFormVM.Registration.ArticleID);
							if (!string.IsNullOrEmpty(BrandModelID))
							{
								string[] arrModelIDs = objPrintVersionDetails.ConTC1ModelIds.Split('|');
								if (arrModelIDs.Contains(BrandModelID))
								{
									Session["TsCs"] = objPrintVersionDetails.TsCs;
									Session["ConTsCs"] = objPrintVersionDetails.ConTC1;
								}
								else
								{
									Session["TsCs"] = objPrintVersionDetails.TsCs;
									Session["ConTsCs"] = objPrintVersionDetails.ConTC;
								}
							}
							else
							{
								Session["TsCs"] = objPrintVersionDetails.TsCs;
								Session["ConTsCs"] = objPrintVersionDetails.ConTC;
							}

						}
						else
						{

							Session["TsCs"] = objPrintVersionDetails.TsCs;
							Session["ConTsCs"] = objPrintVersionDetails.ConTC;
						}
					}
					else
					{
						Session["TsCs"] = null;
					}
				}
			}

            using (var proxyOrg = new OrganizationServiceProxy())
            {
                if (!ReferenceEquals(regFormVM.Registration, null) && !ReferenceEquals(regFormVM.Registration.CenterOrgID, null) && regFormVM.Registration.CenterOrgID != 0)
                {
                    regFormVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { regFormVM.Registration.CenterOrgID }).FirstOrDefault();
                }
            }
            WebHelper.Instance.MapRegAttributesByRegID(regFormVM, !ReferenceEquals(regFormVM.Registration, null) ? regFormVM.Registration.ID : id);
			//Added to display terms and condition for MI rewards/ Added by Sumit start
			regFormVM.VoiceContractDetails = Util.GetRebateDataContractsBYRegId(id);
			//Added to display terms and condition for MI rewards/ Added by Sumit end
            // this is for PDF generation, for iContract no need to show Inventory Chit
            regFormVM.showInventorySection = showInventorySection;


			return View(regFormVM);
		}


        [HttpPost, ValidateInput(false)]
        public ActionResult PrintMobileRegSummary(FormCollection collection, RegistrationFormVM personalDetailsVM)
        {
            Util.CommonPrintMobileRegSummary(collection);
            //////Session["selectedPrinter"] = collection[5].ToString();
            //////int printstatus = Util.PrintFile(collection[3].ToString2(), collection[0].ToString2());

            //////if (printstatus == 1)
            //////{
            //////    Session["printsuccess"] = "PrintSuccessfullyCompleted";
            //////}
            //////else
            //////{
            //////    Session["printsuccess"] = "Printing Failed";
            //////}

            //string HTML = collection[3].ToString();
            //string sDate = string.Format("{0:MMddyyyyhmmtt}", System.DateTime.Now);
            //string sPath = Server.MapPath("~/PrintBill");

            //if (!System.IO.Directory.Exists(sPath))
            //{
            //    System.IO.Directory.CreateDirectory(sPath);
            //}


            //string sFileName = "myfile" + sDate + "P-" + collection[0] + ".htm";


            //sPath = sPath + "/" + sFileName;

            //StreamWriter swXLS = new StreamWriter(sPath);

            //swXLS.Write(HTML.ToString());

            //swXLS.Close();

            ////After Hosting
            ////string Output = Request.Url.Host;

            ////Testing Local
            //string Output = Util.URLPath();

            //string uri = "http://" + Output + "/PrintBill/" + sFileName; ;


            //string Apppath = Server.MapPath("~/App/wp.exe");

            //int printstatus = Util.PrintThroughExe(collection[0].ToString(), uri, Apppath, collection[5].ToString(), collection[1].ToString());

            //if (printstatus == 1)
            //{
            //    Session["printsuccess"] = "PrintSuccessfullyCompleted";
            //}
            //else
            //{
            //    if (printstatus == (int)PrintingIssues.Internalproblem)
            //    {
            //        Session["printsuccess"] = "Internal problem while posting request to printer";
            //    }
            //    else if (printstatus == (int)PrintingIssues.Networkproblem)
            //    {
            //        Session["printsuccess"] = "Network problem";
            //    }
            //    else if (printstatus == (int)PrintingIssues.Printernotdetected)
            //    {
            //        Session["printsuccess"] = "Printer not detected";
            //    }
            //    else
            //    {
            //        Session["printsuccess"] = "Printing Failed";
            //    }


            //}
            //Session["selectedPrinter"] = collection[5].ToString();

            return RedirectToAction("MobileRegSummary", new { id = collection[1].ToString2() });

        }

        #endregion

        #region Private Methods
        private void ResetAllSession(int? type)
        {
            Session["RegMobileReg_FromSearch"] = null;
            Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
            Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = null;
            Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = null;
            Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = null;
            Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = null;

            Session["RegMobileReg_FromSearch_Seco"] = null;
            Session["RegMobileReg_ResultMessage_Seco"] = null;
            Session["RegMobileReg_MalyDevAdv_Seco"] = null;
            Session["RegMobileReg_MalayPlanAdv_Seco"] = null;
            Session["RegMobileReg_MalayDevDeposit_Seco"] = null;
            Session["RegMobileReg_MalyPlanDeposit_Seco"] = null;
            Session["RegMobileReg_OthDevAdv_Seco"] = null;
            Session["RegMobileReg_OthPlanAdv_Seco"] = null;
            Session["RegMobileReg_OthDevDeposit_Seco"] = null;
            Session["RegMobileReg_OthPlanDeposit_Seco"] = null;
            if (ReferenceEquals(Session["vasBackClick"], null))
            {
                ClearRegistrationSession();
            }

            if (type == (int)MobileRegType.DevicePlan)
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
            else if (type == (int)MobileRegType.PlanOnly)
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
            else if (type == (int)MobileRegType.DeviceOnly)
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DeviceOnly;
            else if (type == (int)MobileRegType.NewLine)
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.NewLine;


            // From guided sales
            if (Session["GuidedSales_SelectedModelImageID"].ToInt() != 0 && Session["GuidedSales_PkgPgmBdlPkgCompID"].ToInt() != 0)
                WebHelper.Instance.ConstructGuidedSalesDetails();
            else
                WebHelper.Instance.ClearGuidedSalesSession();

            //Secondary Sim reset
            Session["RegMobileReg_OfferDevicePrice_Seco"] = null;
            Session[SessionKey.RegMobileReg_VasNames_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = null;
            Session[SessionKey.SelectedPlanID_Seco.ToString()] = null;
            Session[SessionKey.UOMPlanID_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_ProgramMinAge_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] = null;
            Session["SelectedComponents_Seco"] = null;
            Session["RegMobileReg_DataPkgID_Seco"] = null;
            Session[SessionKey.RegMobileReg_DataplanID_Seco.ToString()] = null;
        }
        private void SetRegStepNo(MobileRegistrationSteps regStep)
        {
            switch (regStep)
            {
                case MobileRegistrationSteps.Device:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Device;
                    break;
                case MobileRegistrationSteps.DeviceCatalog:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.DeviceCatalog;
                    break;
                case MobileRegistrationSteps.Plan:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Plan;
                    break;
                case MobileRegistrationSteps.Vas:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Vas;
                    break;
                case MobileRegistrationSteps.MobileNo:
                    Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.MobileNo;
                    break;
                case MobileRegistrationSteps.PersonalDetails:
                    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                    {
                        Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() + 1;
                    }
                    else
                    {
                        Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.PersonalDetails;
                    }
                    break;
                default:
                    break;
            }

            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan || Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
            {
                if (regStep == MobileRegistrationSteps.Device)
                    Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() + 1;
            }
            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
            {
                Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() - 1;
            }
            else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.NewLine)
            {
                Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() - 1;
            }

        }

        #region Commnetd old order summary method
        //private OrderSummaryVM ConstructOrderSummary(bool fromSubline = false)
        //{

        //    string strDataPlanID;
        //    string strContractId;
        //    string strDataPlanIDSeco;
        //    string strContractIdSeco;
        //    var components = new List<Component>();
        //    var modelImage = new ModelImage();
        //    var package = new Package();
        //    var bundle = new Bundle();
        //    var orderVM = new OrderSummaryVM();
        //    var phoneVM = (PhoneVM)Session["RegMobileReg_PhoneVM"];
        //    int varDataPlanId = 0;
        //    int varDataPlanIdSeco = 0;
        //    var personalDetailsVM = Session["RegMobileReg_PersonalDetailsVM"] == null ? new PersonalDetailsVM() : (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

        //    // Main Line
        //    if (!fromSubline)
        //    {
        //        if ((OrderSummaryVM) Session[SessionKey.RegMobileReg_OrderSummary.ToString()] != null)
        //            orderVM = (OrderSummaryVM) Session[SessionKey.RegMobileReg_OrderSummary.ToString()];

        //        if ( Session[SessionKey.SimType.ToString()] != null)
        //            orderVM.SimType =  Session[SessionKey.SimType.ToString()].ToString();

        //        if (Session["RegMobileReg_DataPkgID"] != null)
        //        {
        //            strDataPlanID = Session["RegMobileReg_DataPkgID"].ToString();

        //            if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] != null) && (Session[SessionKey.MobileRegType.ToString()] != "Planonly"))
        //            {
        //                strContractId = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString();

        //                using (var proxys = new CatalogServiceProxy())
        //                varDataPlanId = proxys.GettllnkContractDataPlan(Convert.ToInt32(strContractId));

        //                if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID != 0 && strContractId.ToInt() != 0 && varDataPlanId.ToInt() != 0 || varDataPlanId.ToInt() == 0)
        //                {
        //                    using (var proxy = new CatalogServiceProxy())
        //                    {
        //                        List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
        //                        varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, strContractId.ToInt(), varDataPlanId.ToInt() }).ToList();
        //                        for (int i = 0; i < varAdvDeposit.Count; i++)
        //                            if (varAdvDeposit[i] != null && varAdvDeposit[i].modelId != 0)
        //                            {
        //                                var Malaytotalprice = varAdvDeposit[i].malyDevAdv + varAdvDeposit[i].malayPlanAdv + varAdvDeposit[i].malayDevDeposit + varAdvDeposit[i].malyPlanDeposit;
        //                                var Othertotalprice = varAdvDeposit[i].othDevAdv + varAdvDeposit[i].othPlanAdv + varAdvDeposit[i].othDevDeposit + varAdvDeposit[i].othPlanDeposit;
        //                                orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
        //                                orderVM.MalayAdvDevPrice = Malaytotalprice;
        //                                orderVM.OthersAdvDevPrice = Othertotalprice;
        //                                Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
        //                                Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

        //                                orderVM.MalyDevAdv = varAdvDeposit[i].malyDevAdv;
        //                                orderVM.MalayPlanAdv = varAdvDeposit[i].malayPlanAdv;
        //                                orderVM.MalayDevDeposit = varAdvDeposit[i].malayDevDeposit;
        //                                orderVM.MalyPlanDeposit = varAdvDeposit[i].malyPlanDeposit;
        //                                orderVM.OthDevAdv = varAdvDeposit[i].othDevAdv;
        //                                orderVM.OthPlanAdv = varAdvDeposit[i].othPlanAdv;
        //                                orderVM.OthDevDeposit = varAdvDeposit[i].othDevDeposit;
        //                                orderVM.OthPlanDeposit = varAdvDeposit[i].othPlanDeposit;

        //                                Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
        //                                 Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
        //                                Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
        //                               Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
        //                                Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
        //                                Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
        //                                Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
        //                                Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;

        //                                if (personalDetailsVM.Customer.NationalityID != 0)
        //                                {
        //                                    if (personalDetailsVM.Customer.NationalityID == 1)
        //                                    {
        //                                        Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
        //                                         Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
        //                                        Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
        //                                       Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
        //                                        Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
        //                                        orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
        //                                    }
        //                                    else
        //                                    {
        //                                        Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
        //                                        Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
        //                                        Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
        //                                        Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
        //                                        Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
        //                                        orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
        //                                    }
        //                                }
        //                            }
        //                    }
        //                }

        //            }
        //            else if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] == null) && (Session[SessionKey.MobileRegType.ToString()].ToString2().ToUpper() == "PLANONLY"))
        //            {
        //                if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID == 0 && orderVM.ContractID == 0)
        //                {
        //                    using (var proxy = new CatalogServiceProxy())
        //                    {
        //                        var varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, orderVM.ContractID, 0 }).SingleOrDefault();
        //                        if (varAdvDeposit != null)
        //                        {
        //                            var Malaytotalprice = varAdvDeposit.malyDevAdv + varAdvDeposit.malayPlanAdv + varAdvDeposit.malayDevDeposit + varAdvDeposit.malyPlanDeposit;
        //                            var Othertotalprice = varAdvDeposit.othDevAdv + varAdvDeposit.othPlanAdv + varAdvDeposit.othDevDeposit + varAdvDeposit.othPlanDeposit;
        //                            orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
        //                            orderVM.MalayAdvDevPrice = Malaytotalprice;
        //                            orderVM.OthersAdvDevPrice = Othertotalprice;
        //                            Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
        //                            Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

        //                            orderVM.MalyDevAdv = varAdvDeposit.malyDevAdv;
        //                            orderVM.MalayPlanAdv = varAdvDeposit.malayPlanAdv;
        //                            orderVM.MalayDevDeposit = varAdvDeposit.malayDevDeposit;
        //                            orderVM.MalyPlanDeposit = varAdvDeposit.malyPlanDeposit;
        //                            orderVM.OthDevAdv = varAdvDeposit.othDevAdv;
        //                            orderVM.OthPlanAdv = varAdvDeposit.othPlanAdv;
        //                            orderVM.OthDevDeposit = varAdvDeposit.othDevDeposit;
        //                            orderVM.OthPlanDeposit = varAdvDeposit.othPlanDeposit;

        //                            Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
        //                             Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
        //                            Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
        //                           Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
        //                            Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
        //                            Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
        //                            Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
        //                            Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;

        //                            if (personalDetailsVM.Customer.NationalityID != 0)
        //                            {
        //                                if (personalDetailsVM.Customer.NationalityID == 1)
        //                                {
        //                                    Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
        //                                     Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
        //                                    Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
        //                                   Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
        //                                    Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
        //                                    orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
        //                                }
        //                                else
        //                                {
        //                                    Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
        //                                    Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
        //                                    Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
        //                                    Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
        //                                    Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
        //                                    orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }

        //            }
        //        }
        //        else
        //        {
        //            strDataPlanID = "0";

        //            if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] != null) && (Session[SessionKey.MobileRegType.ToString()] != "Planonly"))
        //            {
        //                strContractId = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString();

        //                using (var proxys = new CatalogServiceProxy())

        //                    varDataPlanId = proxys.GettllnkContractDataPlan(Convert.ToInt32(strContractId));


        //                if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID != 0 && strContractId.ToInt() != 0 && varDataPlanId.ToInt() != 0 || varDataPlanId.ToInt() == 0)
        //                {
        //                    using (var proxy = new CatalogServiceProxy())
        //                    {
        //                        List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
        //                        varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, strContractId.ToInt(), varDataPlanId.ToInt() }).ToList();
        //                        for (int i = 0; i < varAdvDeposit.Count; i++)
        //                            if (varAdvDeposit[i] != null && varAdvDeposit[i].modelId != 0)
        //                            {
        //                                var Malaytotalprice = varAdvDeposit[i].malyDevAdv + varAdvDeposit[i].malayPlanAdv + varAdvDeposit[i].malayDevDeposit + varAdvDeposit[i].malyPlanDeposit;
        //                                var Othertotalprice = varAdvDeposit[i].othDevAdv + varAdvDeposit[i].othPlanAdv + varAdvDeposit[i].othDevDeposit + varAdvDeposit[i].othPlanDeposit;
        //                                orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
        //                                orderVM.MalayAdvDevPrice = Malaytotalprice;
        //                                orderVM.OthersAdvDevPrice = Othertotalprice;
        //                                Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
        //                                Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

        //                                orderVM.MalyDevAdv = varAdvDeposit[i].malyDevAdv;
        //                                orderVM.MalayPlanAdv = varAdvDeposit[i].malayPlanAdv;
        //                                orderVM.MalayDevDeposit = varAdvDeposit[i].malayDevDeposit;
        //                                orderVM.MalyPlanDeposit = varAdvDeposit[i].malyPlanDeposit;
        //                                orderVM.OthDevAdv = varAdvDeposit[i].othDevAdv;
        //                                orderVM.OthPlanAdv = varAdvDeposit[i].othPlanAdv;
        //                                orderVM.OthDevDeposit = varAdvDeposit[i].othDevDeposit;
        //                                orderVM.OthPlanDeposit = varAdvDeposit[i].othPlanDeposit;

        //                                Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
        //                                 Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
        //                                Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
        //                               Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
        //                                Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
        //                                Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
        //                                Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
        //                                Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;

        //                                if (personalDetailsVM.Customer.NationalityID != 0)
        //                                {
        //                                    if (personalDetailsVM.Customer.NationalityID == 1)
        //                                    {
        //                                        Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
        //                                         Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
        //                                        Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
        //                                       Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
        //                                        Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
        //                                        orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
        //                                    }
        //                                    else
        //                                    {
        //                                        Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
        //                                        Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
        //                                        Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
        //                                        Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
        //                                        Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
        //                                        orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
        //                                    }
        //                                }
        //                            }
        //                    }
        //                }

        //            }
        //            else if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] == null) && (Session[SessionKey.MobileRegType.ToString()].ToString2().ToUpper() == "PLANONLY"))
        //            {
        //                if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID == 0 && orderVM.ContractID == 0)
        //                {
        //                    using (var proxy = new CatalogServiceProxy())
        //                    {
        //                        var varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, orderVM.ContractID, 0 }).SingleOrDefault();
        //                        if (varAdvDeposit != null)
        //                        {
        //                            var Malaytotalprice = varAdvDeposit.malyDevAdv + varAdvDeposit.malayPlanAdv + varAdvDeposit.malayDevDeposit + varAdvDeposit.malyPlanDeposit;
        //                            var Othertotalprice = varAdvDeposit.othDevAdv + varAdvDeposit.othPlanAdv + varAdvDeposit.othDevDeposit + varAdvDeposit.othPlanDeposit;
        //                            orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
        //                            orderVM.MalayAdvDevPrice = Malaytotalprice;
        //                            orderVM.OthersAdvDevPrice = Othertotalprice;
        //                            Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
        //                            Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

        //                            orderVM.MalyDevAdv = varAdvDeposit.malyDevAdv;
        //                            orderVM.MalayPlanAdv = varAdvDeposit.malayPlanAdv;
        //                            orderVM.MalayDevDeposit = varAdvDeposit.malayDevDeposit;
        //                            orderVM.MalyPlanDeposit = varAdvDeposit.malyPlanDeposit;
        //                            orderVM.OthDevAdv = varAdvDeposit.othDevAdv;
        //                            orderVM.OthPlanAdv = varAdvDeposit.othPlanAdv;
        //                            orderVM.OthDevDeposit = varAdvDeposit.othDevDeposit;
        //                            orderVM.OthPlanDeposit = varAdvDeposit.othPlanDeposit;

        //                            Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
        //                             Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
        //                            Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
        //                           Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
        //                            Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
        //                            Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
        //                            Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
        //                            Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;

        //                            if (personalDetailsVM.Customer.NationalityID != 0)
        //                            {
        //                                if (personalDetailsVM.Customer.NationalityID == 1)
        //                                {
        //                                    Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
        //                                     Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
        //                                    Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
        //                                   Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
        //                                    Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
        //                                    orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
        //                                }
        //                                else
        //                                {
        //                                    Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
        //                                    Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
        //                                    Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
        //                                    Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
        //                                    Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
        //                                    orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }

        //            }
        //        }
        //        // END Deepika
        //        #region Added by Shobha Plan and device details for Secondary

        //        if (Session["RegMobileReg_DataPkgID_Seco"] != null)
        //        {
        //            strDataPlanIDSeco = Session["RegMobileReg_DataPkgID_Seco"].ToString();
        //            if ((Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] != null) && (Session[SessionKey.MobileRegType.ToString()] != "Planonly"))
        //            {
        //                strContractIdSeco = Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()].ToString();

        //                using (var proxys = new CatalogServiceProxy())

        //                    varDataPlanIdSeco = proxys.GettllnkContractDataPlan(Convert.ToInt32(strContractIdSeco));


        //                //added by deepika 10 march
        //                if (orderVM.SelectedPgmBdlPkgCompIDSeco != 0 && orderVM.ModelID != 0 && strContractIdSeco.ToInt() != 0 && varDataPlanIdSeco.ToInt() != 0 || varDataPlanIdSeco.ToInt() == 0)
        //                {
        //                    using (var proxy = new CatalogServiceProxy())
        //                    {
        //                        List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
        //                        varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelIDSeco, orderVM.SelectedPgmBdlPkgCompIDSeco, strContractIdSeco.ToInt(), varDataPlanIdSeco.ToInt() }).ToList();
        //                        for (int i = 0; i < varAdvDeposit.Count; i++)
        //                            if (varAdvDeposit[i] != null && varAdvDeposit[i].modelId != 0)
        //                            {
        //                                var Malaytotalprice = varAdvDeposit[i].malyDevAdv + varAdvDeposit[i].malayPlanAdv + varAdvDeposit[i].malayDevDeposit + varAdvDeposit[i].malyPlanDeposit;
        //                                var Othertotalprice = varAdvDeposit[i].othDevAdv + varAdvDeposit[i].othPlanAdv + varAdvDeposit[i].othDevDeposit + varAdvDeposit[i].othPlanDeposit;
        //                                orderVM.RaceIDSeco = personalDetailsVM.Customer.NationalityID;
        //                                orderVM.MalayAdvDevPriceSeco = Malaytotalprice;
        //                                orderVM.OthersAdvDevPriceSeco = Othertotalprice;
        //                                Session["RegMobileReg_MalAdvDeposit_Seco"] = orderVM.MalayAdvDevPriceSeco;
        //                                Session["RegMobileReg_OtherAdvDeposit_Seco"] = orderVM.OthersAdvDevPriceSeco;

        //                                /*Added by chetan for split adv & deposit*/
        //                                orderVM.MalyDevAdvSeco = varAdvDeposit[i].malyDevAdv;
        //                                orderVM.MalayPlanAdvSeco = varAdvDeposit[i].malayPlanAdv;
        //                                orderVM.MalayDevDepositSeco = varAdvDeposit[i].malayDevDeposit;
        //                                orderVM.MalyPlanDepositSeco = varAdvDeposit[i].malyPlanDeposit;
        //                                orderVM.OthDevAdvSeco = varAdvDeposit[i].othDevAdv;
        //                                orderVM.OthPlanAdvSeco = varAdvDeposit[i].othPlanAdv;
        //                                orderVM.OthDevDepositSeco = varAdvDeposit[i].othDevDeposit;
        //                                orderVM.OthPlanDepositSeco = varAdvDeposit[i].othPlanDeposit;

        //                                Session["RegMobileReg_MalyDevAdv_Seco"] = orderVM.MalyDevAdvSeco;
        //                                Session["RegMobileReg_MalayPlanAdv_Seco"] = orderVM.MalayPlanAdvSeco;
        //                                Session["RegMobileReg_MalayDevDeposit_Seco"] = orderVM.MalayDevDepositSeco;
        //                                Session["RegMobileReg_MalyPlanDeposit_Seco"] = orderVM.MalyPlanDepositSeco;
        //                                Session["RegMobileReg_OthDevAdv_Seco"] = orderVM.OthDevAdvSeco;
        //                                Session["RegMobileReg_OthPlanAdv_Seco"] = orderVM.OthPlanAdvSeco;
        //                                Session["RegMobileReg_OthDevDeposit_Seco"] = orderVM.OthDevDepositSeco;
        //                                Session["RegMobileReg_OthPlanDeposit_Seco"] = orderVM.OthPlanDepositSeco;
        //                                /*up to here*/

        //                                if (personalDetailsVM.Customer.NationalityID != 0)
        //                                {
        //                                    if (personalDetailsVM.Customer.NationalityID == 1)
        //                                    {
        //                                        Session["RegMobileReg_MalyDevAdv_Seco"] = orderVM.MalyDevAdvSeco;
        //                                        Session["RegMobileReg_MalayPlanAdv_Seco"] = orderVM.MalayPlanAdvSeco;
        //                                        Session["RegMobileReg_MalayDevDeposit_Seco"] = orderVM.MalayDevDepositSeco;
        //                                        Session["RegMobileReg_MalyPlanDeposit_Seco"] = orderVM.MalyPlanDepositSeco;
        //                                        Session["RegMobileReg_MalAdvDeposit_Seco"] = orderVM.MalayAdvDevPriceSeco;
        //                                        orderVM.SumPriceSeco = (orderVM.ItemPriceSeco != 0 ? orderVM.ItemPriceSeco : orderVM.ModelPriceSeco) + orderVM.MalayAdvDevPriceSeco + orderVM.SecondaryMonthlySubscription;
        //                                    }
        //                                    else
        //                                    {
        //                                        Session["RegMobileReg_OthDevAdv_Seco"] = orderVM.OthDevAdvSeco;
        //                                        Session["RegMobileReg_OthPlanAdv_Seco"] = orderVM.OthPlanAdvSeco;
        //                                        Session["RegMobileReg_OthDevDeposit_Seco"] = orderVM.OthDevDepositSeco;
        //                                        Session["RegMobileReg_OthPlanDeposit_Seco"] = orderVM.OthPlanDepositSeco;
        //                                        Session["RegMobileReg_OtherAdvDeposit_Seco"] = orderVM.OthersAdvDevPriceSeco;
        //                                        orderVM.SumPriceSeco = orderVM.OthersAdvDevPriceSeco + (orderVM.ItemPriceSeco != 0 ? orderVM.ItemPriceSeco : orderVM.ModelPriceSeco) + orderVM.SecondaryMonthlySubscription;
        //                                    }
        //                                }
        //                            }
        //                    }
        //                }
        //            }
        //            else if ((Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] == null) && (Session[SessionKey.MobileRegType.ToString()] == "Planonly"))
        //            {
        //                //added by deepika 10 march
        //                if (orderVM.SelectedPgmBdlPkgCompIDSeco != 0 && orderVM.ModelIDSeco == 0 && orderVM.ContractIDSeco == 0)
        //                {
        //                    using (var proxy = new CatalogServiceProxy())
        //                    {
        //                        var varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelIDSeco, orderVM.SelectedPgmBdlPkgCompIDSeco, orderVM.ContractIDSeco, 0 }).SingleOrDefault();
        //                        if (varAdvDeposit != null)
        //                        {
        //                            var Malaytotalprice = varAdvDeposit.malyDevAdv + varAdvDeposit.malayPlanAdv + varAdvDeposit.malayDevDeposit + varAdvDeposit.malyPlanDeposit;
        //                            var Othertotalprice = varAdvDeposit.othDevAdv + varAdvDeposit.othPlanAdv + varAdvDeposit.othDevDeposit + varAdvDeposit.othPlanDeposit;
        //                            orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
        //                            orderVM.MalayAdvDevPriceSeco = Malaytotalprice;
        //                            orderVM.OthersAdvDevPriceSeco = Othertotalprice;
        //                            Session["RegMobileReg_MalAdvDeposit_Seco"] = orderVM.MalayAdvDevPriceSeco;
        //                            Session["RegMobileReg_OtherAdvDeposit_Seco"] = orderVM.OthersAdvDevPriceSeco;

        //                            orderVM.MalyDevAdvSeco = varAdvDeposit.malyDevAdv;
        //                            orderVM.MalayPlanAdvSeco = varAdvDeposit.malayPlanAdv;
        //                            orderVM.MalayDevDepositSeco = varAdvDeposit.malayDevDeposit;
        //                            orderVM.MalyPlanDepositSeco = varAdvDeposit.malyPlanDeposit;
        //                            orderVM.OthDevAdvSeco = varAdvDeposit.othDevAdv;
        //                            orderVM.OthPlanAdvSeco = varAdvDeposit.othPlanAdv;
        //                            orderVM.OthDevDepositSeco = varAdvDeposit.othDevDeposit;
        //                            orderVM.OthPlanDepositSeco = varAdvDeposit.othPlanDeposit;

        //                            Session["RegMobileReg_MalyDevAdv_Seco"] = orderVM.MalyDevAdvSeco;
        //                            Session["RegMobileReg_MalayPlanAdv_Seco"] = orderVM.MalayPlanAdvSeco;
        //                            Session["RegMobileReg_MalayDevDeposit_Seco"] = orderVM.MalayDevDepositSeco;
        //                            Session["RegMobileReg_MalyPlanDeposit_Seco"] = orderVM.MalyPlanDepositSeco;
        //                            Session["RegMobileReg_OthDevAdv_Seco"] = orderVM.OthDevAdvSeco;
        //                            Session["RegMobileReg_OthPlanAdv_Seco"] = orderVM.OthPlanAdvSeco;
        //                            Session["RegMobileReg_OthDevDeposit_Seco"] = orderVM.OthDevDepositSeco;
        //                            Session["RegMobileReg_OthPlanDeposit_Seco"] = orderVM.OthPlanDepositSeco;

        //                            if (personalDetailsVM.Customer.NationalityID != 0)
        //                            {
        //                                if (personalDetailsVM.Customer.NationalityID == 1)
        //                                {
        //                                    Session["RegMobileReg_MalyDevAdv_Seco"] = orderVM.MalyDevAdvSeco;
        //                                    Session["RegMobileReg_MalayPlanAdv_Seco"] = orderVM.MalayPlanAdvSeco;
        //                                    Session["RegMobileReg_MalayDevDeposit_Seco"] = orderVM.MalayDevDepositSeco;
        //                                    Session["RegMobileReg_MalyPlanDeposit_Seco"] = orderVM.MalyPlanDepositSeco;
        //                                    Session["RegMobileReg_MalAdvDeposit_Seco"] = orderVM.MalayAdvDevPriceSeco;
        //                                    orderVM.SumPriceSeco = (orderVM.ItemPriceSeco != 0 ? orderVM.ItemPriceSeco : orderVM.ModelPriceSeco) + orderVM.MalayAdvDevPriceSeco + orderVM.SecondaryMonthlySubscription;
        //                                }
        //                                else
        //                                {
        //                                    Session["RegMobileReg_OthDevAdv_Seco"] = orderVM.OthDevAdvSeco;
        //                                    Session["RegMobileReg_OthPlanAdv_Seco"] = orderVM.OthPlanAdvSeco;
        //                                    Session["RegMobileReg_OthDevDeposit_Seco"] = orderVM.OthDevDepositSeco;
        //                                    Session["RegMobileReg_OthPlanDeposit_Seco"] = orderVM.OthPlanDepositSeco;
        //                                    Session["RegMobileReg_OtherAdvDeposit_Seco"] = orderVM.OthersAdvDevPriceSeco;
        //                                    orderVM.SumPriceSeco = orderVM.OthersAdvDevPriceSeco + (orderVM.ItemPriceSeco != 0 ? orderVM.ItemPriceSeco : orderVM.ModelPriceSeco) + orderVM.SecondaryMonthlySubscription;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }

        //            }
        //        }
        //        else
        //        {
        //            strDataPlanIDSeco = "0";
        //            if ((Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] != null) && (Session[SessionKey.MobileRegType.ToString()] != "Planonly"))
        //            {
        //                strContractIdSeco = Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()].ToString();

        //                using (var proxys = new CatalogServiceProxy())

        //                    varDataPlanIdSeco = proxys.GettllnkContractDataPlan(Convert.ToInt32(strContractIdSeco));

        //                if (orderVM.SelectedPgmBdlPkgCompIDSeco != 0 && orderVM.ModelIDSeco != 0 && strContractIdSeco.ToInt() != 0 && varDataPlanIdSeco.ToInt() != 0 || varDataPlanIdSeco.ToInt() == 0)
        //                {
        //                    using (var proxy = new CatalogServiceProxy())
        //                    {
        //                        List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
        //                        varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelIDSeco, orderVM.SelectedPgmBdlPkgCompIDSeco, strContractIdSeco.ToInt(), varDataPlanIdSeco.ToInt() }).ToList();
        //                        for (int i = 0; i < varAdvDeposit.Count; i++)
        //                            if (varAdvDeposit[i] != null && varAdvDeposit[i].modelId != 0)
        //                            {
        //                                var Malaytotalprice = varAdvDeposit[i].malyDevAdv + varAdvDeposit[i].malayPlanAdv + varAdvDeposit[i].malayDevDeposit + varAdvDeposit[i].malyPlanDeposit;
        //                                var Othertotalprice = varAdvDeposit[i].othDevAdv + varAdvDeposit[i].othPlanAdv + varAdvDeposit[i].othDevDeposit + varAdvDeposit[i].othPlanDeposit;
        //                                orderVM.RaceIDSeco = personalDetailsVM.Customer.NationalityID;
        //                                orderVM.MalayAdvDevPriceSeco = Malaytotalprice;
        //                                orderVM.OthersAdvDevPriceSeco = Othertotalprice;
        //                                Session["RegMobileReg_MalAdvDeposit_Seco"] = orderVM.MalayAdvDevPriceSeco;
        //                                Session["RegMobileReg_OtherAdvDeposit_Seco"] = orderVM.OthersAdvDevPriceSeco;

        //                                orderVM.MalyDevAdvSeco = varAdvDeposit[i].malyDevAdv;
        //                                orderVM.MalayPlanAdvSeco = varAdvDeposit[i].malayPlanAdv;
        //                                orderVM.MalayDevDepositSeco = varAdvDeposit[i].malayDevDeposit;
        //                                orderVM.MalyPlanDepositSeco = varAdvDeposit[i].malyPlanDeposit;
        //                                orderVM.OthDevAdvSeco = varAdvDeposit[i].othDevAdv;
        //                                orderVM.OthPlanAdvSeco = varAdvDeposit[i].othPlanAdv;
        //                                orderVM.OthDevDepositSeco = varAdvDeposit[i].othDevDeposit;
        //                                orderVM.OthPlanDepositSeco = varAdvDeposit[i].othPlanDeposit;

        //                                Session["RegMobileReg_MalyDevAdv_Seco"] = orderVM.MalyDevAdvSeco;
        //                                Session["RegMobileReg_MalayPlanAdv_Seco"] = orderVM.MalayPlanAdvSeco;
        //                                Session["RegMobileReg_MalayDevDeposit_Seco"] = orderVM.MalayDevDepositSeco;
        //                                Session["RegMobileReg_MalyPlanDeposit_Seco"] = orderVM.MalyPlanDepositSeco;
        //                                Session["RegMobileReg_OthDevAdv_Seco"] = orderVM.OthDevAdvSeco;
        //                                Session["RegMobileReg_OthPlanAdv_Seco"] = orderVM.OthPlanAdvSeco;
        //                                Session["RegMobileReg_OthDevDeposit_Seco"] = orderVM.OthDevDepositSeco;
        //                                Session["RegMobileReg_OthPlanDeposit_Seco"] = orderVM.OthPlanDepositSeco;

        //                                if (personalDetailsVM.Customer.NationalityID != 0)
        //                                {
        //                                    if (personalDetailsVM.Customer.NationalityID == 1)
        //                                    {
        //                                        Session["RegMobileReg_MalyDevAdv_Seco"] = orderVM.MalyDevAdvSeco;
        //                                        Session["RegMobileReg_MalayPlanAdv_Seco"] = orderVM.MalayPlanAdvSeco;
        //                                        Session["RegMobileReg_MalayDevDeposit_Seco"] = orderVM.MalayDevDepositSeco;
        //                                        Session["RegMobileReg_MalyPlanDeposit_Seco"] = orderVM.MalyPlanDepositSeco;
        //                                        Session["RegMobileReg_MalAdvDeposit_Seco"] = orderVM.MalayAdvDevPriceSeco;
        //                                        orderVM.SumPriceSeco = (orderVM.ItemPriceSeco != 0 ? orderVM.ItemPriceSeco : orderVM.ModelPriceSeco) + orderVM.MalayAdvDevPriceSeco + orderVM.SecondaryMonthlySubscription;
        //                                    }
        //                                    else
        //                                    {
        //                                        Session["RegMobileReg_OthDevAdv_Seco"] = orderVM.OthDevAdvSeco;
        //                                        Session["RegMobileReg_OthPlanAdv_Seco"] = orderVM.OthPlanAdvSeco;
        //                                        Session["RegMobileReg_OthDevDeposit_Seco"] = orderVM.OthDevDepositSeco;
        //                                        Session["RegMobileReg_OthPlanDeposit_Seco"] = orderVM.OthPlanDepositSeco;
        //                                        Session["RegMobileReg_OtherAdvDeposit_Seco"] = orderVM.OthersAdvDevPriceSeco;
        //                                        orderVM.SumPriceSeco = orderVM.OthersAdvDevPriceSeco + (orderVM.ItemPriceSeco != 0 ? orderVM.ItemPriceSeco : orderVM.ModelPriceSeco) + orderVM.SecondaryMonthlySubscription;
        //                                    }
        //                                }
        //                            }
        //                    }
        //                }

        //            }
        //            else if ((Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] == null) && (Session[SessionKey.MobileRegType.ToString()] == "Planonly"))
        //            {
        //                if (orderVM.SelectedPgmBdlPkgCompIDSeco != 0 && orderVM.ModelIDSeco == 0 && orderVM.ContractIDSeco == 0)
        //                {
        //                    using (var proxy = new CatalogServiceProxy())
        //                    {
        //                        var varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelIDSeco, orderVM.SelectedPgmBdlPkgCompIDSeco, orderVM.ContractIDSeco, 0 }).SingleOrDefault();
        //                        if (varAdvDeposit != null)
        //                        {
        //                            var Malaytotalprice = varAdvDeposit.malyDevAdv + varAdvDeposit.malayPlanAdv + varAdvDeposit.malayDevDeposit + varAdvDeposit.malyPlanDeposit;
        //                            var Othertotalprice = varAdvDeposit.othDevAdv + varAdvDeposit.othPlanAdv + varAdvDeposit.othDevDeposit + varAdvDeposit.othPlanDeposit;
        //                            orderVM.RaceIDSeco = personalDetailsVM.Customer.NationalityID;
        //                            orderVM.MalayAdvDevPriceSeco = Malaytotalprice;
        //                            orderVM.OthersAdvDevPriceSeco = Othertotalprice;
        //                            Session["RegMobileReg_MalAdvDeposit_Seco"] = orderVM.MalayAdvDevPriceSeco;
        //                            Session["RegMobileReg_OtherAdvDeposit_Seco"] = orderVM.OthersAdvDevPriceSeco;

        //                            orderVM.MalyDevAdvSeco = varAdvDeposit.malyDevAdv;
        //                            orderVM.MalayPlanAdvSeco = varAdvDeposit.malayPlanAdv;
        //                            orderVM.MalayDevDepositSeco = varAdvDeposit.malayDevDeposit;
        //                            orderVM.MalyPlanDepositSeco = varAdvDeposit.malyPlanDeposit;
        //                            orderVM.OthDevAdvSeco = varAdvDeposit.othDevAdv;
        //                            orderVM.OthPlanAdvSeco = varAdvDeposit.othPlanAdv;
        //                            orderVM.OthDevDepositSeco = varAdvDeposit.othDevDeposit;
        //                            orderVM.OthPlanDepositSeco = varAdvDeposit.othPlanDeposit;

        //                            Session["RegMobileReg_MalyDevAdv_Seco"] = orderVM.MalyDevAdvSeco;
        //                            Session["RegMobileReg_MalayPlanAdv_Seco"] = orderVM.MalayPlanAdvSeco;
        //                            Session["RegMobileReg_MalayDevDeposit_Seco"] = orderVM.MalayDevDepositSeco;
        //                            Session["RegMobileReg_MalyPlanDeposit_Seco"] = orderVM.MalyPlanDepositSeco;
        //                            Session["RegMobileReg_OthDevAdv_Seco"] = orderVM.OthDevAdvSeco;
        //                            Session["RegMobileReg_OthPlanAdv_Seco"] = orderVM.OthPlanAdvSeco;
        //                            Session["RegMobileReg_OthDevDeposit_Seco"] = orderVM.OthDevDepositSeco;
        //                            Session["RegMobileReg_OthPlanDeposit_Seco"] = orderVM.OthPlanDepositSeco;

        //                            if (personalDetailsVM.Customer.NationalityID != 0)
        //                            {
        //                                if (personalDetailsVM.Customer.NationalityID == 1)
        //                                {
        //                                    Session["RegMobileReg_MalyDevAdv_Seco"] = orderVM.MalyDevAdvSeco;
        //                                    Session["RegMobileReg_MalayPlanAdv_Seco"] = orderVM.MalayPlanAdvSeco;
        //                                    Session["RegMobileReg_MalayDevDeposit_Seco"] = orderVM.MalayDevDepositSeco;
        //                                    Session["RegMobileReg_MalyPlanDeposit_Seco"] = orderVM.MalyPlanDepositSeco;
        //                                    Session["RegMobileReg_MalAdvDeposit_Seco"] = orderVM.MalayAdvDevPriceSeco;
        //                                    orderVM.SumPriceSeco = (orderVM.ItemPriceSeco != 0 ? orderVM.ItemPriceSeco : orderVM.ModelPriceSeco) + orderVM.MalayAdvDevPriceSeco + orderVM.SecondaryMonthlySubscription;
        //                                }
        //                                else
        //                                {
        //                                    Session["RegMobileReg_OthDevAdv_Seco"] = orderVM.OthDevAdvSeco;
        //                                    Session["RegMobileReg_OthPlanAdv_Seco"] = orderVM.OthPlanAdvSeco;
        //                                    Session["RegMobileReg_OthDevDeposit_Seco"] = orderVM.OthDevDepositSeco;
        //                                    Session["RegMobileReg_OthPlanDeposit_Seco"] = orderVM.OthPlanDepositSeco;
        //                                    Session["RegMobileReg_OtherAdvDeposit_Seco"] = orderVM.OthersAdvDevPriceSeco;
        //                                    orderVM.SumPriceSeco = orderVM.OthersAdvDevPriceSeco + (orderVM.ItemPriceSeco != 0 ? orderVM.ItemPriceSeco : orderVM.ModelPriceSeco) + orderVM.SecondaryMonthlySubscription;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }

        //            }
        //        }


        //        #endregion

        //        // Device Details
        //        if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan || Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
        //        {
        //            if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt() != orderVM.SelectedModelImageID)
        //            {
        //                orderVM.SelectedModelImageID = Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt();

        //                using (var proxy = new CatalogServiceProxy())
        //                {
        //                    modelImage = proxy.ModelImageGet(orderVM.SelectedModelImageID);
        //                }

        //                Session["RegMobileReg_SelectedModelID"] = modelImage.ModelID;

        //                var model = new Model();
        //                using (var proxy = new CatalogServiceProxy())
        //                {
        //                    model = proxy.ModelGet(modelImage.ModelID);
        //                }

        //                orderVM.ModelImageUrl = modelImage.ImagePath;
        //                orderVM.ModelID = model.ID;
        //                orderVM.ModelName = model.Name;
        //                orderVM.ColourName = modelImage.ColourID.HasValue ? Util.GetNameByID(RefType.Colour, modelImage.ColourID.Value) : "";
        //                orderVM.BrandID = model.BrandID;
        //                orderVM.BrandName = Util.GetNameByID(RefType.Brand, model.BrandID);

        //                if (personalDetailsVM.RegID > 0)
        //                {
        //                    orderVM.TotalPrice = Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()].ToDecimal();
        //                    orderVM.ModelPrice = Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()].ToDecimal();
        //                    orderVM.ItemPrice = Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()].ToInt();
        //                }
        //                else
        //                {
        //                    orderVM.DevicePriceType = DevicePriceType.RetailPrice;
        //                    orderVM.TotalPrice -= orderVM.ModelPrice;
        //                    orderVM.TotalPrice += model.RetailPrice.ToInt();
        //                    orderVM.ModelPrice = model.RetailPrice.ToInt();

        //                    Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = orderVM.ModelPrice;
        //                }
        //            }
        //        }
        //    }
        //    else //Sub Line
        //    {
        //        if ((OrderSummaryVM)Session["RegMobileReg_SublineSummary"] != null)
        //            orderVM = (OrderSummaryVM)Session["RegMobileReg_SublineSummary"];
        //    }
        //    #region Offer details for secondary

        //    MOCOfferDetails offerSeco = new MOCOfferDetails();
        //    using (var proxy = new CatalogServiceProxy())
        //    {

        //        if (Session["OfferId_Seco"] != null)
        //        {
        //            int offerId = Convert.ToInt32(Session["OfferId_Seco"]);
        //            if (offerId > 0)
        //            {
        //                var respOffers = proxy.GETMOCOfferDetails(offerId);

        //                if (respOffers != null)
        //                {
        //                    offerSeco.DiscountAmount = respOffers.DiscountAmount;
        //                    offerSeco.ID = respOffers.ID;
        //                    offerSeco.Name = respOffers.Name;
        //                    offerSeco.OfferDescription = respOffers.OfferDescription;

        //                }


        //            }
        //        }
        //    }
        //    List<MOCOfferDetails> listOffersSeco = new List<MOCOfferDetails>();
        //    if (offerSeco != null && offerSeco.ID > 0)
        //    {
        //        if (orderVM != null)
        //        {
        //            //orderVM.Offers = null;
        //            listOffersSeco.Add(offerSeco);
        //            orderVM.OffersSeco = listOffersSeco;
        //        }

        //    }

        //    #endregion
        //    //Package Details
        //    var sessionPkgPgmBdlPkgCompID = fromSubline ? Session["RegMobileSub_PkgPgmBdlPkgCompID"].ToInt() :  Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(); //Main or Sub Line
        //    if (sessionPkgPgmBdlPkgCompID != orderVM.SelectedPgmBdlPkgCompID)
        //    {
        //        var bundlePackage = new PgmBdlPckComponent();
        //        orderVM.SelectedPgmBdlPkgCompID = sessionPkgPgmBdlPkgCompID;

        //        // Package Details
        //        if (sessionPkgPgmBdlPkgCompID != 0)
        //        {
        //            using (var proxy = new CatalogServiceProxy())
        //            {
        //                bundlePackage = MasterDataCache.Instance.FilterComponents(new int[] 
        //            { 
        //                sessionPkgPgmBdlPkgCompID 
        //            }).SingleOrDefault();

        //                package = proxy.PackageGet(new int[] 
        //            { 
        //                bundlePackage.ChildID 
        //            }).SingleOrDefault();

        //                bundle = proxy.BundleGet(new int[] 
        //            { 
        //                bundlePackage.ParentID 
        //            }).SingleOrDefault();
        //            }
        //        }

        //        orderVM.PlanBundle = bundle.Name;
        //        orderVM.PlanName = package.Name;

        //        // Plan Monthly Subscription
        //        if (orderVM.MonthlySubscription != bundlePackage.Price)
        //        {
        //            orderVM.MonthlySubscription = bundlePackage.Price;
        //        }

        //        // Mandatory Package Components
        //        if (sessionPkgPgmBdlPkgCompID > 0)
        //            orderVM.VasVM.MandatoryVASes = GetAvailablePackageComponents(orderVM.SelectedPgmBdlPkgCompID, isMandatory: true).MandatoryVASes;

        //        else
        //            orderVM.VasVM = new ValueAddedServicesVM();
        //    }
        //    //Secondary Vas & package details Details
        //    var sessionPkgPgmBdlPkgCompID_Seco = fromSubline ? Session["RegMobileSub_PkgPgmBdlPkgCompID_Seco"].ToInt() :  Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(); //Main or Sub Line
        //    if (sessionPkgPgmBdlPkgCompID_Seco != orderVM.SelectedPgmBdlPkgCompIDSeco)
        //    {
        //        var bundlePackageComp = new PgmBdlPckComponent();
        //        orderVM.SelectedPgmBdlPkgCompIDSeco = sessionPkgPgmBdlPkgCompID_Seco;

        //        // Package Details
        //        if (sessionPkgPgmBdlPkgCompID_Seco != 0)
        //        {
        //            using (var proxy = new CatalogServiceProxy())
        //            {
        //                bundlePackageComp = MasterDataCache.Instance.FilterComponents(new int[] 
        //            { 
        //                sessionPkgPgmBdlPkgCompID_Seco 
        //            }).SingleOrDefault();

        //                package = proxy.PackageGet(new int[] 
        //            { 
        //                bundlePackageComp.ChildID 
        //            }).SingleOrDefault();

        //                bundle = proxy.BundleGet(new int[] 
        //            { 
        //                bundlePackageComp.ParentID 
        //            }).SingleOrDefault();
        //            }
        //        }

        //        orderVM.SecondaryPlanBundle = bundle.Name;
        //        orderVM.SecondaryPlanName = package.Name;

        //        // Plan Monthly Subscription
        //        if (orderVM.SecondaryMonthlySubscription != bundlePackageComp.Price)
        //        {
        //            orderVM.SecondaryMonthlySubscription = bundlePackageComp.Price;
        //        }

        //        // Mandatory Package Components
        //        if (sessionPkgPgmBdlPkgCompID_Seco > 0)
        //            orderVM.VasVM.MandatorySecoVASes = GetAvailablePackageComponents(orderVM.SelectedPgmBdlPkgCompIDSeco, isMandatory: true).MandatoryVASes;
        //    }
        //    //VAS details
        //    if (!fromSubline)
        //    {
        //        var selectedVasIDs = Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2(); //Main or Sub Line
        //        var selectedCompIDs = new List<int>();

        //        // Mandatory VASes
        //        if (orderVM.VasVM.MandatoryVASes.Count() > 0)
        //        {
        //            selectedCompIDs = orderVM.VasVM.MandatoryVASes.Select(a => a.PgmBdlPkgCompID).ToList();
        //        }

        //        if (orderVM.VasVM.SelectedVasIDs != selectedVasIDs || orderVM.VasVM.MandatoryVASes.Count() > 0)
        //        {
        //            orderVM.MonthlySubscription = 0;
        //            decimal vasPrice = 0;
        //            if (Session[SessionKey.RegMobileReg_ContractID.ToString()] != null)
        //                orderVM.ContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt();

        //            List<int> lstDataPlanIds = new List<int>();
        //            try
        //            {
        //                if (Session[SessionKey.RegMobileReg_DataplanID.ToString()] != null)
        //                {
        //                    int[] arrDpIds = (int[])Session[SessionKey.RegMobileReg_DataplanID.ToString()];

        //                    foreach (int id in arrDpIds)
        //                    {
        //                        lstDataPlanIds.Add(id);
        //                    }
        //                }

        //            }
        //            catch (Exception ex)
        //            {
        //                if (Session[SessionKey.RegMobileReg_DataplanID.ToString()] != null)
        //                    lstDataPlanIds.Add(Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt());

        //                LogExceptions(ex);

        //            }
        //            if (lstDataPlanIds.Count > 0)
        //                orderVM.DataPlanID = Convert.ToInt32(lstDataPlanIds[0]);

        //            // orderVM.DataPlanID = Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt();
        //            orderVM.VasVM.SelectedVasIDs = selectedVasIDs;

        //            var vasIDs = orderVM.VasVM.SelectedVasIDs.Split(',').ToList();
        //            foreach (var id in vasIDs)
        //            {
        //                //Changed by chetan for handleing the id equals to [object XMLDocument]
        //                if (!string.IsNullOrEmpty(id) && id != "[object XMLDocument]")
        //                {
        //                    selectedCompIDs.Add(id.ToInt());
        //                }
        //            }

        //            if (selectedCompIDs != null)
        //            {
        //                if (selectedCompIDs.Count() > 0)
        //                {
        //                    using (var proxy = new CatalogServiceProxy())
        //                    {
        //                        var vases = MasterDataCache.Instance.FilterComponents
        //                           (
        //                               selectedCompIDs
        //                           ).ToList();

        //                        components = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();
        //                        components = components.Where(a => a.RestricttoKenan == false).ToList();

        //                        var query = from selectedVases in vases
        //                                    join comps in components on
        //                                        selectedVases.ChildID equals comps.ID
        //                                    select new { name = comps.Name + " (RM" + selectedVases.Price + ")" };

        //                        orderVM.VasVM.VASesName = query.Select(a => a.name).ToList();


        //                        if ((!string.IsNullOrEmpty(selectedVasIDs)) || (orderVM.VasVM.SelectedVasIDs != selectedVasIDs) || (orderVM.VasVM.VASesName.Count > 0))
        //                        {
        //                            var query1 = from selectedVases in vases
        //                                         join comps in components on
        //                                            selectedVases.ChildID equals comps.ID
        //                                         select new { selectedVases.Price };
        //                            if (query1 != null)
        //                            {
        //                                orderVM.MonthlySubscription2 = query1.Select(b => b.Price).ToList();
        //                                for (int i = 0; i < orderVM.MonthlySubscription2.Count; i++)
        //                                {
        //                                    vasPrice += orderVM.MonthlySubscription2[i];
        //                                }
        //                            }

        //                        }
        //                    }
        //                }
        //            }

        //            orderVM.MonthlySubscription += vasPrice;
        //            Session[SessionKey.RegMobileReg_VasNames.ToString()] = orderVM.VasVM.VASesName;
        //        }
        //    }
        //    var selectedVasIDsSeco = Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()].ToString2(); //Main or Sub Line
        //    var selectedCompIDsSeco = new List<int>();

        //    // Mandatory VASes
        //    if (orderVM.VasVM.MandatorySecoVASes != null && orderVM.VasVM.MandatorySecoVASes.Count() > 0)
        //    {
        //        selectedCompIDsSeco = orderVM.VasVM.MandatorySecoVASes.Select(a => a.PgmBdlPkgCompID).ToList();
        //    }

        //    if (orderVM.VasVM.MandatorySecoVASes != null)
        //    {

        //        if (orderVM.VasVM.SelectedVasIDsSeco.ToString2() != selectedVasIDsSeco.ToString2() || (orderVM.VasVM.MandatorySecoVASes.ToString2() != null && orderVM.VasVM.MandatorySecoVASes.Count() > 0))
        //        {

        //            if (orderVM.VasVM.SelectedVasIDsSeco != selectedVasIDsSeco)
        //            {
        //                orderVM.SecondaryMonthlySubscription = 0;
        //                decimal vasPrice = 0;
        //                orderVM.ContractIDSeco = Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()].ToInt();
        //                orderVM.DataPlanIDSeco = Session[SessionKey.RegMobileReg_DataplanID_Seco.ToString()].ToInt();
        //                orderVM.VasVM.SelectedVasIDsSeco = selectedVasIDsSeco;


        //                if (orderVM.VasVM.MandatorySecoVASes != null && (orderVM.VasVM.SelectedVasIDsSeco != selectedVasIDsSeco || orderVM.VasVM.MandatorySecoVASes.Count() > 0))
        //                {
        //                    orderVM.SecondaryMonthlySubscription = 0;
        //                    orderVM.ContractIDSeco = Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()].ToInt();
        //                    orderVM.DataPlanIDSeco = Session[SessionKey.RegMobileReg_DataplanID_Seco.ToString()].ToInt();
        //                    orderVM.VasVM.SelectedVasIDsSeco = selectedVasIDsSeco;
        //                    var vasIDsSeco = orderVM.VasVM.SelectedVasIDsSeco.Split(',').ToList();
        //                    foreach (var id in vasIDsSeco)
        //                    {
        //                        if (!string.IsNullOrEmpty(id))
        //                        {
        //                            selectedCompIDsSeco.Add(id.ToInt());
        //                        }
        //                    }

        //                    if (Session[SessionKey.RegMobileReg_DataplanID_Seco.ToString()] != null && Session[SessionKey.RegMobileReg_DataplanID_Seco.ToString()].ToInt() != 0)
        //                    {
        //                        string depcompIdsSeco = WebHelper.Instance.GetDepenedencyComponents(Session[SessionKey.RegMobileReg_DataplanID_Seco.ToString()].ToString());
        //                        List<string> depListCompIdsSeco = new List<string>();
        //                        depListCompIdsSeco = depcompIdsSeco.Split(',').ToList();

        //                        foreach (string compid in depListCompIdsSeco)
        //                        {

        //                            if (compid != string.Empty)
        //                                selectedCompIDsSeco.Add(Convert.ToInt32(compid));

        //                        }
        //                    }


        //                    if (selectedCompIDsSeco != null)
        //                    {
        //                        if (selectedCompIDsSeco.Count() > 0)
        //                        {
        //                            using (var proxy = new CatalogServiceProxy())
        //                            {
        //                                var vases = MasterDataCache.Instance.FilterComponents
        //                                   (
        //                                       selectedCompIDsSeco
        //                                   ).ToList();

        //                                components = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();

        //                                components = components.Where(a => a.RestricttoKenan == false).ToList();

        //                                var query = from selectedVases in vases
        //                                            join comps in components on
        //                                                selectedVases.ChildID equals comps.ID
        //                                            select new { name = comps.Name + " (RM" + selectedVases.Price + ")" };
        //                                orderVM.VasVM.VASesNameSeco = query.Select(a => a.name).Distinct().ToList();


        //                                if ((!string.IsNullOrEmpty(selectedVasIDsSeco)) || (orderVM.VasVM.SelectedVasIDsSeco != selectedVasIDsSeco) || (orderVM.VasVM.VASesNameSeco.Count > 0))
        //                                {
        //                                    var query1 = from selectedVases in vases
        //                                                 join comps in components on
        //                                                    selectedVases.ChildID equals comps.ID
        //                                                 select new { selectedVases.Price };
        //                                    if (query1 != null)
        //                                    {
        //                                        orderVM.MonthlySubscription2 = query1.Select(b => b.Price).ToList();
        //                                        for (int i = 0; i < orderVM.MonthlySubscription2.Count; i++)
        //                                        {
        //                                            vasPrice += orderVM.MonthlySubscription2[i];
        //                                        }
        //                                    }

        //                                }

        //                            }
        //                        }
        //                    }

        //                    orderVM.SecondaryMonthlySubscription += vasPrice;

        //                    //Pavan
        //                    if (orderVM.ContractIDSeco != 0)
        //                    {

        //                        CatalogServiceProxy objCSproxy = new CatalogServiceProxy();
        //                        if ( Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] != null)
        //                        {
        //                            var deviceproxys = objCSproxy.GetDiscountPriceDetails(orderVM.ModelID,  Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(), Convert.ToInt32(orderVM.ContractIDSeco), varDataPlanId);
        //                            if (deviceproxys.NewPlanName != null)
        //                            {
        //                                orderVM.DiscountPrice = deviceproxys.DiscountPrice.ToInt();
        //                                orderVM.PlanName = deviceproxys.NewPlanName;
        //                                orderVM.MonthlySubscription = orderVM.MonthlySubscription - orderVM.DiscountPrice;
        //                            }
        //                        }
        //                        //price
        //                    }

        //                    //Pavan

        //                    Session[SessionKey.RegMobileReg_VasNames_Seco.ToString()] = orderVM.VasVM.VASesNameSeco;
        //                }

        //            }
        //        }
        //    }
        //    // Advance Payment/ Deposite
        //    if (orderVM.Deposite != personalDetailsVM.Deposite)
        //    {
        //        orderVM.TotalPrice -= orderVM.Deposite;
        //        orderVM.TotalPrice += personalDetailsVM.Deposite;

        //        orderVM.Deposite = personalDetailsVM.Deposite;
        //    }


        //    // Device Price (cheaper)
        //    if (Session[SessionKey.RegMobileReg_ContractID.ToString()] != null && orderVM.SelectedPgmBdlPkgCompID.ToInt() != 0 && orderVM.ModelID != 0)
        //    {
        //        if (orderVM.ItemPrice != orderVM.ModelPrice)
        //        {
        //            var itemPrice = new ItemPrice();
        //            using (var proxy = new CatalogServiceProxy())
        //            {
        //                var itemPriceID = proxy.ItemPriceFind(new ItemPriceFind()
        //                {
        //                    ItemPrice = new ItemPrice()
        //                    {
        //                        BdlPkgID = orderVM.SelectedPgmBdlPkgCompID.ToInt(),
        //                        PkgCompID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt(),
        //                        ModelID = orderVM.ModelID,
        //                        pkgDataID = Session["RegMobileReg_DataPkgID"].ToInt()
        //                    },
        //                    Active = true
        //                }).SingleOrDefault();

        //                if (personalDetailsVM.RegID == 0)
        //                {
        //                    if (itemPriceID.ToInt() != 0) // item price configured
        //                    {
        //                        itemPrice = proxy.ItemPriceGet(itemPriceID);

        //                        if (orderVM.DevicePriceType == DevicePriceType.RetailPrice)
        //                        {
        //                            orderVM.TotalPrice -= orderVM.ModelPrice;
        //                        }
        //                        else if (orderVM.DevicePriceType == DevicePriceType.ItemPrice)
        //                        {
        //                            orderVM.TotalPrice -= orderVM.ItemPrice;
        //                        }

        //                        if (itemPrice.Price == 0) // RM 0 price for device
        //                        {
        //                            orderVM.DevicePriceType = DevicePriceType.NoPrice;
        //                        }
        //                        else // cheaper price for device
        //                        {
        //                            orderVM.DevicePriceType = DevicePriceType.ItemPrice;
        //                        }

        //                        orderVM.TotalPrice += itemPrice.Price;
        //                        orderVM.ItemPrice = itemPrice.Price.ToInt();

        //                        Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = orderVM.ItemPrice.ToDecimal();
        //                    }
        //                    else // retail price
        //                    {
        //                        if (orderVM.DevicePriceType == DevicePriceType.ItemPrice)
        //                        {
        //                            orderVM.TotalPrice -= orderVM.ItemPrice;
        //                            orderVM.TotalPrice += orderVM.ModelPrice;
        //                            orderVM.DevicePriceType = DevicePriceType.RetailPrice;

        //                            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = orderVM.ModelPrice;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    else //(retail price)
        //    {
        //        if (personalDetailsVM.RegID == 0)
        //        {
        //            if (orderVM.DevicePriceType != DevicePriceType.RetailPrice)
        //            {
        //                orderVM.TotalPrice -= orderVM.ItemPrice;
        //                orderVM.ItemPrice = 0;
        //                orderVM.TotalPrice += orderVM.ItemPrice;
        //            }
        //        }
        //    }

        //    // MobileNo
        //    var mobileNos = fromSubline ? (List<string>)Session["RegMobileSub_MobileNo"] : (List<string>) Session[SessionKey.RegMobileReg_MobileNo.ToString()]; // Main or Sub Line
        //    orderVM.MobileNumbers = mobileNos == null ? new List<string>() : mobileNos;

        //    //Commented for MISM virtual numbers flow
        //    //SecondarymobileNo
        //    if ( Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] != null &&  Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()].ToString() != "")
        //        orderVM.SecondaryMobileNumbers = (List<string>) Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()];


        //    if (!fromSubline) //main line
        //         Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderVM;
        //    else //sub line
        //    {
        //        Session["RegMobileReg_SublineSummary"] = orderVM;
        //        orderVM.fromSubline = true;
        //    }

        //    // Subline
        //    var sublines = (List<SublineVM>) Session[SessionKey.RegMobileReg_SublineVM.ToString()];
        //    if (sublines != null)
        //        orderVM.Sublines = (List<SublineVM>) Session[SessionKey.RegMobileReg_SublineVM.ToString()];

        //    return orderVM;
        //}
        #endregion

        #region New order summary method

        private OrderSummaryVM ConstructOrderSummary(bool fromSubline = false)
        {
            return WebHelper.Instance.ConstructOrderSummary(string.Empty, this.GetType().Name, fromSubline);
        }

        #endregion

		private ComponentViewModel GetSelectedPackageInfo_smart(int modelId, int packageId, string kenanCode)
		{
			ComponentViewModel pkgResults = new ComponentViewModel();
			List<ModelPackageInfo> listRes = null;
			using (var proxy = new CatalogServiceProxy())
			{
				listRes = proxy.GetModelPackageInfo_smart(modelId, packageId, kenanCode);
			}

			List<NewPackages> listNewPackages = new List<NewPackages>();
			NewComponents objNewComponents;
			List<NewComponents> listNewComponents = new List<NewComponents>();

			NewPackages objNewPackagesPC = new NewPackages();
			NewPackages objNewPackagesMA = new NewPackages();
			NewPackages objNewPackagesEC = new NewPackages();
			NewPackages objNewPackagesDC = new NewPackages();
			NewPackages objNewPackagesOC = new NewPackages();
			NewPackages objNewPackagesRC = new NewPackages();
			NewPackages objNewPackagesCR = new NewPackages();

			if (listRes != null)
			{
				foreach (var item in listRes)
				{

					if (item.packageType == "PC")
					{
						objNewPackagesPC.NewPackageId = item.PackageId.ToString2();
						objNewPackagesPC.NewPackageName = item.PackageName;
						objNewPackagesPC.Type = item.packageType;

						objNewComponents = new NewComponents();
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.ReassignIsmandatory = item.IsMandatory;
						objNewComponents.Type = item.packageType;
						objNewComponents.isDefault = item.isDefault;
						listNewComponents.Add(objNewComponents);

					}
					if (item.packageType == "MA")
					{
						objNewPackagesMA.NewPackageId = item.PackageId.ToString2();
						objNewPackagesMA.NewPackageName = item.PackageName;
						objNewPackagesMA.Type = item.packageType;

						objNewComponents = new NewComponents();
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.IsChecked = item.IsMandatory;
						objNewComponents.Type = item.packageType;
						objNewComponents.isDefault = item.isDefault;
						listNewComponents.Add(objNewComponents);
						Session["NewMPID"] = item.PackageId;

					}
					if (item.packageType == "EC")
					{
						objNewPackagesEC.NewPackageId = item.PackageId.ToString2();
						objNewPackagesEC.NewPackageName = item.PackageName;
						objNewPackagesEC.Type = item.packageType;

						objNewComponents = new NewComponents();
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.IsChecked = item.IsMandatory;
						objNewComponents.Type = item.packageType;
						objNewComponents.isDefault = item.isDefault;
						listNewComponents.Add(objNewComponents);

					}
					if (item.packageType == "DC" || item.packageType == "DM" || item.packageType == "MD")
					{
						TempData["DataComponentPkgKenancode"] = item.KenanCode;
						TempData["DataComponentPkgId"] = item.PackageId.ToString2();
						objNewPackagesDC.NewPackageId = item.PackageId.ToString2();
						objNewPackagesDC.NewPackageName = item.PackageName;
						objNewPackagesDC.Type = item.packageType;

						objNewComponents = new NewComponents();
						objNewComponents.ComponentGroupId = item.ComponentGroupId;
						objNewComponents.ComponentGroupName = item.packageType;
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.IsChecked = item.IsMandatory;
						objNewComponents.Type = item.packageType;
						objNewComponents.isDefault = item.isDefault;
						listNewComponents.Add(objNewComponents);
						//}
					}

					if (item.packageType == "OC")
					{

						objNewPackagesOC.NewPackageId = item.PackageId.ToString2();
						objNewPackagesOC.NewPackageName = item.PackageName;
						objNewPackagesOC.Type = item.packageType;
						objNewComponents = new NewComponents();
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.isDefault = item.isDefault;
						objNewComponents.Type = item.packageType;
						objNewComponents.IsChecked = item.IsMandatory;
						listNewComponents.Add(objNewComponents);
					}


					if (item.packageType == "RC")
					{


						objNewPackagesRC.NewPackageId = item.PackageId.ToString2();
						objNewPackagesRC.NewPackageName = item.PackageName;
						objNewPackagesPC.Type = item.packageType;

						objNewComponents = new NewComponents();
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.ReassignIsmandatory = item.IsMandatory;
						objNewComponents.isCRP = item.isCRP;
						objNewComponents.Type = item.packageType;
						objNewComponents.isDefault = item.isDefault;

						listNewComponents.Add(objNewComponents);




					}
					if (item.packageType == "CR")
					{
						objNewPackagesCR.NewPackageId = item.PackageId.ToString2();
						objNewPackagesCR.NewPackageName = item.PackageName;
						objNewPackagesCR.Type = item.packageType;
						if (!item.isCRP)
						{
							objNewComponents = new NewComponents();
							objNewComponents.NewComponentId = item.ComponentId.ToString2();
							objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
							objNewComponents.NewComponentName = item.ComponentName;
							objNewComponents.NewPackageId = item.PackageId.ToString2();
							objNewComponents.NewPackageKenanCode = item.KenanCode;
							objNewComponents.ReassignIsmandatory = item.IsMandatory;
							objNewComponents.Type = item.packageType;
							objNewComponents.isDefault = item.isDefault;
							objNewComponents.ComponentGroupId = item.ComponentGroupId;

							listNewComponents.Add(objNewComponents);
						}
						else
						{
							if (Session["Package"] != null)
							{
								IList<Online.Registration.Web.SubscriberICService.PackageModel> oldCompPackage = (IList<Online.Registration.Web.SubscriberICService.PackageModel>)Session["Package"];
								foreach (var pkg in oldCompPackage)
								{
									foreach (var comp in pkg.compList)
									{
										if ((comp.componentId == "45310" || comp.componentId == "45308"))
										{
											if (comp.componentId == item.ComponentKenanCode.ToString())
											{
												objNewComponents = new NewComponents();
												objNewComponents.NewComponentId = item.ComponentId.ToString2();
												objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
												objNewComponents.NewComponentName = item.ComponentName;
												objNewComponents.NewPackageId = item.PackageId.ToString2();
												objNewComponents.NewPackageKenanCode = item.KenanCode;
												objNewComponents.ReassignIsmandatory = item.IsMandatory;
												objNewComponents.isCRP = item.isCRP;
												objNewComponents.Type = item.packageType;
												objNewComponents.ComponentGroupId = item.ComponentGroupId;
												listNewComponents.Add(objNewComponents);
											}

										}
									}
								}
							}

						}
					}
					NewPackages pkgGroup = new NewPackages();
					pkgGroup.NewPackageId = item.PackageId.ToString2();
					pkgGroup.NewPackageName = item.PackageName;
					pkgGroup.Type = item.packageType;

					var existingpkg = from pkg in listNewPackages where pkg.NewPackageId == item.PackageId.ToString() select pkg;

					if (existingpkg.Count() <= 0)
					{
						listNewPackages.Add(pkgGroup);
					}
					// end
				}

				if (!ReferenceEquals(objNewPackagesOC, null))
					listNewPackages.Add(objNewPackagesOC);

				pkgResults.NewComponentsList = listNewComponents;
				pkgResults.NewPackagesList = listNewPackages;
			}

			return pkgResults;

		}


        private PackageVM GetAvailablePackages(bool fromSubline = false)
        {
            var packageVM = new PackageVM();
            var availablePackages = new List<AvailablePackage>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var programBundles = new List<PgmBdlPckComponent>();
            var packages = new List<Package>();
            var bundles = new List<Bundle>();
            var bundleIDs = new List<int>();
            var programs = new List<Program>();
            var filterBdlIDs = new List<int>();
            var bundlePackages = new List<PgmBdlPckComponent>();
            var packageIDs = new List<int>();
            var orderVM = new OrderSummaryVM();
            string linktype = string.Empty;

            using (var proxy = new CatalogServiceProxy())
            {
                // retrieve bundles that assigned to current user

                filterBdlIDs = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.Program_Bundle,
                        FilterOrgType = Util.SessionOrgTypeCode
                    },
                    Active = true
                }).Select(a => a.ChildID).ToList();
                bundleIDs = filterBdlIDs;
                if (Session[SessionKey.SimType.ToString()] != null && Session[SessionKey.SimType.ToString()].ToString() == "MISM")
                {
                    linktype = Properties.Settings.Default.SecondaryPlan;
                }
                else
                {
                    linktype = Properties.Settings.Default.Bundle_Package;
                }
                var query = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        // LinkType = Properties.Settings.Default.Bundle_Package,
                        LinkType = linktype,
                        PlanType = fromSubline ? Properties.Settings.Default.SupplimentaryPlan : Properties.Settings.Default.ComplimentaryPlan,
                    },
                    ParentIDs = bundleIDs,
                    ChildIDs = packageIDs,
                    Active = true
                });

                // Main Line
                if (!fromSubline)
                {
                    if (Session[SessionKey.SimType.ToString()] != null && Session[SessionKey.SimType.ToString()].ToString() == "MISM")
                    {
                        var querySeco = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
                        {
                            PgmBdlPckComponent = new PgmBdlPckComponent()
                            {
                                LinkType = Properties.Settings.Default.SecondaryPlan,
                                PlanType = Properties.Settings.Default.MandatoryPlan,
                            },
                            ParentIDs = bundleIDs,
                            ChildIDs = packageIDs,
                            Active = true
                        });
                        if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
                        {
                            pgmBdlPckComponents = query.Where(a => bundleIDs.Contains(a.ParentID)).ToList();
                            List<PgmBdlPckComponent> seocndary = querySeco.Where(a => bundleIDs.Contains(a.ParentID)).ToList();
                            foreach (PgmBdlPckComponent component in seocndary)
                                pgmBdlPckComponents.Add(component);
                        }
                        else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                        {
                            pgmBdlPckComponents = query.ToList();
                            List<PgmBdlPckComponent> seocndary = querySeco.ToList();
                            foreach (PgmBdlPckComponent component in seocndary)
                                pgmBdlPckComponents.Add(component);
                        }
                    }
                    else
                    {
                        if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                        {
                            pgmBdlPckComponents = query.ToList();
                        }
                    }
                }
                else //Sub Line
                {
                    pgmBdlPckComponents = query.ToList();
                }

                if (pgmBdlPckComponents.Count() > 0)
                {
                    packages = proxy.PackageGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();
                    bundles = proxy.BundleGet(pgmBdlPckComponents.Select(a => a.ParentID).Distinct()).ToList();
                }

                programBundles = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.Program_Bundle,
                    },
                    ChildIDs = pgmBdlPckComponents.Select(a => a.ParentID).ToList()
                }).Distinct().ToList();

                var programIDs = programBundles.Select(a => a.ParentID).ToList();

                if (programIDs != null)
                {
                    if (programIDs.Count() > 0)
                    {
                        programs = proxy.ProgramGet(programIDs).ToList();
                    }
                }
            }

            foreach (var pbpc in pgmBdlPckComponents)
            {
                availablePackages.Add(new AvailablePackage()
                {
                    PgmBdlPkgCompID = pbpc.ID,
                    ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
                    BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
                    PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                    PackageCode = pbpc.Code
                });
            }

            packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();

            // Main Line
            if (!fromSubline)

                packageVM.SelectedPgmBdlPkgCompID = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt();
            else
                packageVM.SelectedPgmBdlPkgCompID = Session["RegMobileSub_PkgPgmBdlPkgCompID"].ToInt();

            return packageVM;
        }
        private List<RegPgmBdlPkgCompSec> ConstructRegPgmBdlPkgSecComponent()
        {

            var regPBPCs = new List<RegPgmBdlPkgCompSec>();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            if (dropObj.msimIndex == 0)
            {
                return regPBPCs;
            }

            if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                return regPBPCs;

            int packageCompID = 0;
            string vasIds = "";

            // standard plan for Mism - start
            PackageVM pVM = WebHelper.Instance.GetAvailablePackagesCache(iSecondary: true);
            List<AvailablePackage> MandatoryPackages = pVM.AvailablePackages;
            string planID = string.Empty;
            // standard plan for Mism - end

            if (dropObj.msimIndex > 0)
            {
                planID = MandatoryPackages.Where(x => x.KenanCode.Equals(Properties.Settings.Default.MISMDefaultPlan)).SingleOrDefault().PgmBdlPkgCompID.ToString2();
                //packageCompID = dropObj.mainFlow.orderSummary.SelectedPgmBdlPkgCompID;
                packageCompID = planID.ToInt();
            }

            // Plan PgmBdlPkgComponent
            regPBPCs.Add(new RegPgmBdlPkgCompSec()
            {
                RegSuppLineID = null,
                PgmBdlPckComponentID = packageCompID,
                IsNewAccount = true,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });
            // Plan PgmBdlPkgComponent
            regPBPCs.Add(new RegPgmBdlPkgCompSec()
            {
                RegSuppLineID = null,
                PgmBdlPckComponentID = ConfigurationManager.AppSettings["IncludeMandatoryPackageMISM"].ToInt(),
                IsNewAccount = true,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });

            // Mandatory VAS PgmBdlPkgComponent
            var vasPBPCIDs = new List<int>();
            using (var proxy = new CatalogServiceProxy())
            {
                var pkgID = MasterDataCache.Instance.FilterComponents(new int[] { packageCompID }).SingleOrDefault().ChildID;

                vasPBPCIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    IsMandatory = true,
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        ParentID = pkgID,
                        LinkType = Properties.Settings.Default.Package_Component
                    },
                    Active = true
                }).ToList();


                // TODO Check the VAS, if principle plan has roaming vas then add the roaming vas details in secondary plan as well
                List<RegPgmBdlPkgComp> primaryVas = ConstructRegPgmBdlPkgComponent();
                if (primaryVas != null && primaryVas.Count > 0)
                {
                    foreach (var pV in primaryVas)
                    {
                        var kenanCodeId = MasterDataCache.Instance.FilterComponents(new int[] { pV.PgmBdlPckComponentID }).SingleOrDefault().KenanCode;
                        if (kenanCodeId == ConfigurationManager.AppSettings["InternationalRoamingKenanCodes"])
                        {
                            vasPBPCIDs.Add(pV.PgmBdlPckComponentID);
                            break;
                        }
                    }
                }


                foreach (var vasPBPCID in vasPBPCIDs)
                {
                    regPBPCs.Add(new RegPgmBdlPkgCompSec()
                    {
                        RegSuppLineID = null,
                        PgmBdlPckComponentID = vasPBPCID,
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }

            string[] vasIdsStr = null;
            if (dropObj.msimIndex > 0)
            {
                //vasIdsStr = dropObj.mainFlow.SelectedVASID.Split(',');
                var unetPBPCComponentID = ConfigurationManager.AppSettings["UNETPBPCID"].ToString2();
                var vasComponentID = WebHelper.Instance.GetDepenedencyComponents(unetPBPCComponentID);
                vasIdsStr = vasComponentID.Split(',');
            }

            if (vasIdsStr != null)
            {
                foreach (var vasID in vasIdsStr)
                {
                    if (!string.IsNullOrEmpty(vasID.ToString()))
                    {
                        regPBPCs.Add(new RegPgmBdlPkgCompSec()
                        {
                            PgmBdlPckComponentID = vasID.ToInt(),
                            IsNewAccount = true,
                            Active = true,
                            CreateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }
                }
            }

            regPBPCs.Add(new RegPgmBdlPkgCompSec()
            {
                PgmBdlPckComponentID = ConfigurationManager.AppSettings["IncludeMandatoryComponentMISM"].ToInt(),
                IsNewAccount = true,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });
            // Optional VAS PgmBdlPkgComponent
           
            string depcompIds = string.Empty;
            List<int> lstDPids = new List<int>();
            int[] dpVals = dropObj.msimIndex == 0 ? (int[])Session["RegMobileReg_DataplanID_Seco"] : dropObj.mainFlow.SelectedDataPlanID;
            try
            {
                foreach (int idval in dpVals)
                {
                    lstDPids.Add(idval);
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
            }


                depcompIds = WebHelper.Instance.GetDepenedencyComponents(string.Join(",", lstDPids), false);


            List<string> depListCompIds = new List<string>();
            depListCompIds = depcompIds.Split(',').ToList();

            foreach (string compid in depListCompIds)
            {

                if (compid != string.Empty && Convert.ToInt32(compid) > 0)
                {
                    regPBPCs.Add(new RegPgmBdlPkgCompSec()
                    {
                        PgmBdlPckComponentID = compid.ToInt(),
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }

            }

            if (dropObj.msimIndex > 0)
            {
                var intmandatoryidsval = 0;
                var intdataidsval = 0;
                using (var proxy = new CatalogServiceProxy())
                {
                    var respon = new BundlepackageResp();
                    respon = proxy.GetMandatoryVas(packageCompID);
                    for (int i = 0; i < respon.values.Count(); i++)
                    {
                        if (respon.values[i].Plan_Type == Settings.Default.Master_Component)
                        {
                            intmandatoryidsval = respon.values[i].BdlDataPkgId.ToInt();
                        }

                        if (respon.values[i].Plan_Type == Settings.Default.CompType_DataCon)
                        {
                            intdataidsval = respon.values[i].BdlDataPkgId.ToInt();
                        }
                    }
                }

                /*Mandatory package*/
                if (intmandatoryidsval > 0)
                {
                    regPBPCs.Add(new RegPgmBdlPkgCompSec()
                    {
                        PgmBdlPckComponentID = intmandatoryidsval,
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }

                /*data plan*/
                if (intdataidsval > 0)
                {
                    regPBPCs.Add(new RegPgmBdlPkgCompSec()
                    {
                        PgmBdlPckComponentID = intdataidsval,
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }

            /*Mandatory package*/
            //if (dropObj.msimIndex == 0)
            //{
            //    if (Session["intmandatoryidsvalSec"] != null && Session["intmandatoryidsvalSec"].ToString() != "" && Convert.ToInt32(Session["intmandatoryidsvalSec"]) != 0)
            //    {
            //        regPBPCs.Add(new RegPgmBdlPkgCompSec()
            //        {
            //            PgmBdlPckComponentID = Session["intmandatoryidsvalSec"].ToInt(),
            //            IsNewAccount = true,
            //            Active = true,
            //            CreateDT = DateTime.Now,
            //            LastAccessID = Util.SessionAccess.UserName
            //        });
            //    }
            //}
            //else
            //{
            //    if (dropObj.msimFlow[0].MandatoryPackage != null && dropObj.msimFlow[0].MandatoryPackage.ToString() != "" && Convert.ToInt32(dropObj.msimFlow[0].MandatoryPackage) != 0)
            //    {
            //        regPBPCs.Add(new RegPgmBdlPkgCompSec()
            //        {
            //            PgmBdlPckComponentID = dropObj.msimFlow[0].MandatoryPackage.ToInt(),
            //            IsNewAccount = true,
            //            Active = true,
            //            CreateDT = DateTime.Now,
            //            LastAccessID = Util.SessionAccess.UserName
            //        });
            //    }
            //}



            /*data plan*/
            //if (dropObj.mainFlow.DataPackage != null && dropObj.mainFlow.DataPackage.ToString() != "" && Convert.ToInt32(dropObj.mainFlow.DataPackage) != 0)
            //{
            //    regPBPCs.Add(new RegPgmBdlPkgCompSec()
            //    {
            //        PgmBdlPckComponentID = dropObj.mainFlow.DataPackage.ToInt(),
            //        IsNewAccount = true,
            //        Active = true,
            //        CreateDT = DateTime.Now,
            //        LastAccessID = Util.SessionAccess.UserName
            //    });
            //}

            /*extra plan*/
            //if (dropObj.mainFlow.ExtraPackage != null && dropObj.mainFlow.ExtraPackage.ToString() != "" && Convert.ToInt32(dropObj.mainFlow.ExtraPackage) != 0)
            //{
            //    regPBPCs.Add(new RegPgmBdlPkgCompSec()
            //    {
            //        PgmBdlPckComponentID = dropObj.mainFlow.ExtraPackage.ToInt(),
            //        IsNewAccount = true,
            //        Active = true,
            //        CreateDT = DateTime.Now,
            //        LastAccessID = Util.SessionAccess.UserName
            //    });
            //}

            /*Pramotional plan*/
            //if (dropObj.mainFlow.PromotionPackage != null && dropObj.mainFlow.PromotionPackage.ToString() != "" && Convert.ToInt32(dropObj.mainFlow.PromotionPackage) != 0)
            //{
            //    regPBPCs.Add(new RegPgmBdlPkgCompSec()
            //    {
            //        PgmBdlPckComponentID = dropObj.msimFlow[0].PromotionPackage.ToInt(),
            //        IsNewAccount = true,
            //        Active = true,
            //        CreateDT = DateTime.Now,
            //        LastAccessID = Util.SessionAccess.UserName
            //    });
            //}

            return regPBPCs;
        }

        private void ClearRegistrationSession()
        {
            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
            Session["intpramotionidsval"] = null;
            Session["intmandatoryidsval"] = null;
            Session["intdataidsval"] = null;
            Session["intextraidsval"] = null;
            Session["intdatadiscountidsval"] = null;
            Session["intmandatoryidsvalSec"] = null;
            Session["intdataidsvalSec"] = null;
            Session["intextraidsvalSec"] = null;
            Session["intpramotionidsvalSec"] = null;
            Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
            //Session["RegMobileReg_BiometricVerify"] = null;
            Session["RegMobileReg_BiometricID"] = null;
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
            Session["RegMobileReg_SelectedModelID"] = null;
            Session["RegMobileReg_SelectedModelImageID_Seco"] = null;
            Session["RegMobileReg_SelectedModelID_Seco"] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session["RegMobileReg_PhoneVM"] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session[SessionKey.RegStepNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardType.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = null;
            Session["RegMobileReg_SelectedOptionID_Seco"] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = null;
            Session["RegMobileReg_SelectedOptionType_Seco"] = null;
            Session[SessionKey.RegMobileReg_SublineVM.ToString()] = null;
            Session[SessionKey.ArticleId.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session["UOMPlanID"] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session[SessionKey.SelectedComponents.ToString()] = null;
            Session["RegMobileReg_OfferDevicePrice_Seco"] = null;
            Session[SessionKey.RegMobileReg_VasNames_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = null;
            Session[SessionKey.UOMPlanID_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_ProgramMinAge_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_DataplanID_Seco.ToString()] = null;
            Session["GuidedSales_PkgPgmBdlPkgCompID_Seco"] = null;
            Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] = null;
            Session["RegMobileReg_SelectedModelImageID_Seco"] = null;
            Session["_componentViewModel"] = null;
            // Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.SelectedPlanID_Seco.ToString()] = null;
            Session[SessionKey.Condition.ToString()] = null;
            Session["vasBackClick"] = null;
            Session["RebatePenalty"] = null;
            Session["SelectedAccountNo"] = null;
            Session["PrimarySim"] = null;
            Session[SessionKey.CDPU_onBehalf.ToString()] = string.Empty;
            WebHelper.Instance.ClearSubLineSession();
        }
        private ValueAddedServicesVM GetAvailablePackageComponents(int pbpcID, bool fromSubline = false, bool isMandatory = false)
        {
            var vasVM = new ValueAddedServicesVM();
            var retrievedVases = new List<AvailableVAS>();
            var contractVases = new List<AvailableVAS>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var components = new List<Component>();
            var componentTypes = new List<ComponentType>();

            using (var proxy = new CatalogServiceProxy())
            {
                var packageID = (pbpcID != null && pbpcID != 0) ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().ChildID : 0;
                if (packageID != 0)
                {
                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            LinkType = Properties.Settings.Default.Package_Component,
                            ParentID = packageID,
                        },
                        IsMandatory = isMandatory,
                        Active = true
                    }).ToList();
                }
                if (pgmBdlPckComponents.Count() > 0)
                {
                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();

                    componentTypes = proxy.ComponentTypeGet(components.Select(a => a.ComponentTypeID).Distinct()).ToList();
                }
            }

            foreach (var pbpc in pgmBdlPckComponents)
            {
                var component = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault();
                var compTypeCode = componentTypes.Where(a => a.ID == component.ComponentTypeID).SingleOrDefault().Code;

                if (compTypeCode == Properties.Settings.Default.CompType_DataCon || compTypeCode == Properties.Settings.Default.CompType_VoiceCon)
                {
                    contractVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        KenanCode = pbpc.KenanCode,
                        RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false,
                        isDefault = pbpc.isdefault,
                        isHidden = pbpc.IsHidden.HasValue ? (bool)pbpc.IsHidden : false
                    });
                }
                else
                {
                    retrievedVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        KenanCode = pbpc.KenanCode,
                        RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false,
                        isDefault = pbpc.isdefault,
                        isHidden = pbpc.IsHidden.HasValue ? (bool)pbpc.IsHidden : false

                    });
                    if (isMandatory)
                    {
                        if (Session["MandatoryVasIds"].ToString2().Length > 0)
                        {
                            Session["MandatoryVasIds"] = Session["MandatoryVasIds"].ToString2() + "," + pbpc.ID.ToString();
                            Session["MandatoryVasKenanIds"] = Session["MandatoryVasKenanIds"].ToString2() + "," + pbpc.KenanCode.ToString();
                        }
                        else
                        {
                            Session["MandatoryVasIds"] = pbpc.ID.ToString();
                            Session["MandatoryVasKenanIds"] = pbpc.KenanCode.ToString();
                        }
                    }
                }
            }

            if (isMandatory == true)
            {
                vasVM.MandatoryVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }
            else
            {
                vasVM.AvailableVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }
            vasVM.ContractVASes = contractVases;

            vasVM.SelectedContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2();
            vasVM.SelectedVasIDs = fromSubline ? Session["RegMobileSub_VasIDs"].ToString2() : Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2();

            return vasVM;
        }
        private ValueAddedServicesVM GetAvailablePackageComponents_PC_MC_DC(int pbpcID, string listtype, bool fromSubline = false, bool isMandatory = false)
        {
            List<string> componentsToRemove = ConfigurationManager.AppSettings["RestrictComponentsForDealer"].ToString2().Split(',').ToList();
            var vasVM = new ValueAddedServicesVM();
            var retrievedVases = new List<AvailableVAS>();
            var contractVases = new List<AvailableVAS>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var components = new List<Component>();
            var componentTypes = new List<ComponentType>();


            int packageID = 0;
            using (var proxy = new CatalogServiceProxy())
            {

                packageID = (pbpcID != null) ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().ChildID : 0;

                if (packageID != 0)
                {

                    pgmBdlPckComponents.AddRange(MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            LinkType = listtype,
                            ParentID = packageID,
                        },
                        IsMandatory = isMandatory,
                        Active = true
                    }).ToList());



                }
                if (pgmBdlPckComponents.Count() > 0)
                {
                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();

                    componentTypes = proxy.ComponentTypeGet(components.Select(a => a.ComponentTypeID).Distinct()).ToList();
                }
            }


            foreach (var pbpc in pgmBdlPckComponents)
            {
                if (Session["IsDealer"].ToBool()) // as per phase2 in dealer flow, below components to be hidden
                {
                    if (componentsToRemove.Contains(pbpc.KenanCode))
                        continue;
                }
                var component = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault();
                var compTypeCode = componentTypes.Where(a => a.ID == component.ComponentTypeID).SingleOrDefault().Code;

                if (compTypeCode == Properties.Settings.Default.CompType_DataCon || compTypeCode == Properties.Settings.Default.CompType_VoiceCon)
                {
                    contractVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
						ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().AliasName,    //RST
                        FavoriteFlag = pbpc.FavoriteFlag,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        GroupId = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().GroupId,
                        GroupName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Contains("╚") ? components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Split('╚')[1] : "",
                        KenanCode = pbpc.KenanCode,
						isDefault = pbpc.isdefault,
						isHidden = !ReferenceEquals(pbpc.IsHidden, null) ? false : (bool)pbpc.IsHidden,

                        RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false,
						isSharing = component.IsSharing,
						isVRCDataBooster = !ReferenceEquals(component.VRCDataBooster, null) ? component.VRCDataBooster : false

                    });
                }
                else
                {
                    retrievedVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
						ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().AliasName,    //RST
                        FavoriteFlag = pbpc.FavoriteFlag,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        GroupId = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().GroupId,
                        GroupName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Contains("╚") ? components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Split('╚')[1] : "",
                        KenanCode = pbpc.KenanCode,
						isDefault = pbpc.isdefault,
						isHidden = !ReferenceEquals(pbpc.IsHidden, null) ? false : (bool)pbpc.IsHidden,
                        RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false,
						isSharing = component.IsSharing,
						isVRCDataBooster = !ReferenceEquals(component.VRCDataBooster, null) ? component.VRCDataBooster : false
                    });
                }
            }

            if (isMandatory == true)
            {
                vasVM.MandatoryVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }
            else
            {
                vasVM.AvailableVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }
            vasVM.ContractVASes = contractVases;

            vasVM.SelectedContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2();
            vasVM.SelectedVasIDs = fromSubline ? Session["RegMobileSub_VasIDs"].ToString2() : Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2();

            return vasVM;
        }
        private bool ValidateCardDetails(Customer customer)
        {
            string error;
            //15122014 - Anthony - Parse the error message to PersonalDetailsNew if there's any error
            if (!WebHelper.Instance.IsCardDetailsEntered(customer, out error))
            {
                if (!string.IsNullOrEmpty(error))
                {
                    ModelState.AddModelError("CreditCardError", error);
                }
                else
                {
                    ModelState.AddModelError("CreditCardError", "Please enter all Card details.");
                }
                return false;
            }
            return true;
        }
        private KenanTypeAdditionLineRegistrationResponse FulfillKenanAccount(int regID)
        {
            //var result = "";
            var reg = new DAL.Models.Registration();
            var cust = new Customer();
            var billAddr = new Address();
            var response = new KenanTypeAdditionLineRegistrationResponse();
            MNPOrderCreationResponse rs = new MNPOrderCreationResponse();
            int? GeneratedID = null;
            HomeController myHomeControl = new HomeController();

            try
            {

                using (var proxy = new RegistrationServiceProxy())
                {
                    reg = proxy.RegistrationGet(regID);
                    cust = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    var billAddrID = proxy.RegAddressFind(new AddressFind()
                    {
                        Address = new Address()
                        {
                            RegID = reg.ID,
                            AddressTypeID = Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Billing)
                        }
                    }).SingleOrDefault();

                    billAddr = proxy.RegAddressGet(new int[] { billAddrID }).SingleOrDefault();
                }

                var extid = new List<KenanTypeExternalId>();
                var extidnew = new List<KenanTypeExternalId>();
                extidnew.Add(new KenanTypeExternalId() { ExternalId = string.Empty, ExternalIdType = string.Empty });
                extid.Add(new KenanTypeExternalId() { ExternalId = (reg.externalId == null ? "" : reg.externalId), ExternalIdType = (reg.AccExternalID == null ? "" : reg.AccExternalID) });

                var request = new KenanTypeAdditionLineRegistration()
                {
                    OrderId = regID.ToString(),
                    FxAcctNo = (reg.fxAcctNo == null) ? "" : reg.fxAcctNo,
                    ExternalId = (extid.ToList().ToArray() == null) ? extidnew.ToList().ToArray() : extid.ToList().ToArray(),
                };


                using (var proxy = new KenanServiceProxy())
                {
                    if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                    {

                        rs = proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
                        {
                            orderId = regID.ToString(),
                            externalId = reg.MSISDN1,
                            extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
                            Customer = WebHelper.Instance.ConstructKenanCustomerMNP(cust, billAddr),
                            Account = WebHelper.Instance.ConstructKenanAccountMNP(cust),
                            UserName = reg.SalesPerson,
                        });

                        if (reg.RegTypeID == (int)MobileRegType.MNPNewLine)
                        {
                            if (!ReferenceEquals(rs, null))
                            {
                                response.Message = rs.Message;
                                response.Success = rs.Success;
                                response.Code = rs.Code;
                            }
                        }

                    }
                    else
                    {
                        if (reg.RegTypeID == (int)MobileRegType.MNPNewLine)
                        {

                            rs = proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
                            {
                                orderId = regID.ToString(),
                                externalId = reg.MSISDN1,
                                extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
                                Customer = WebHelper.Instance.ConstructKenanCustomerMNP(cust, billAddr),
                                Account = WebHelper.Instance.ConstructKenanAccountMNP(cust),
                                UserName = reg.SalesPerson,

                            });
                        }
                        else
                        {
                            response = proxy.KenanAdditionLineRegistration(request);
                        }
                        /*Added by narayanareddy*/
                        if (reg.RegTypeID == (int)MobileRegType.MNPNewLine)
                        {
                            if (!ReferenceEquals(rs, null))
                            {
                                response.Message = rs.Message;
                                response.Success = rs.Success;
                                response.Code = rs.Code;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex);

                LogExceptions(ex);

            }
            return response;

        }
        private DAL.Models.RegistrationSec ConstructRegistrationForSec(string queueNo, string remarks, string signatureSVG = "", string custPhoto = "", string altCustPhoto = "", string photo = "", string SIMModelSelected = "")
        {
            DAL.Models.RegistrationSec registration;

            //Added for MISM Virtual numbers flow
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

            if ((dropObj.mainFlow.mobile.SIMCardType.ToString2() == "Normal") && dropObj.msimIndex == 0)
            {
                registration = null;
                return registration;
            }
            registration = new DAL.Models.RegistrationSec();
            var regTypeID = 0;

            //we set this always TRUE why because based on this we are storing the ApproveBlacklistCheck  in trnregistration this we are sending to kenan as sales code
            var isBlacklisted = true;
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
            {
                regTypeID = Util.GetRegTypeID(REGTYPE.MNPPlanOnly);
            }
            else
            {
                if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)
                    regTypeID = Util.GetRegTypeID(REGTYPE.DevicePlan);
                else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)
                    regTypeID = Util.GetRegTypeID(REGTYPE.PlanOnly);
                else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                    regTypeID = Util.GetRegTypeID(REGTYPE.DeviceOnly);
                else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.CRP)
                    regTypeID = Util.GetRegTypeID(REGTYPE.CRP);
                else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.NewLine)
                    regTypeID = Util.GetRegTypeID(REGTYPE.NewLine);
            }
            regTypeID = 9;
            //Added by VLT ON APR 08 2013
            registration.K2_Status = Session[SessionKey.RegK2_Status.ToString()].ToString2();
            //End by VLT ON APR 08 2013

            registration = new DAL.Models.RegistrationSec();
            registration.CenterOrgID = Util.SessionAccess.User.OrgID;
            registration.RegTypeID = regTypeID;
            //Added by Patanjali to support remarks on 30-03-2013
            registration.Remarks = remarks;
            //Added by Patanjali to support remarks on 30-03-2013 ends her
            registration.QueueNo = queueNo;
            registration.SignatureSVG = signatureSVG;
            registration.CustomerPhoto = custPhoto;
            registration.AltCustomerPhoto = altCustPhoto;
            registration.Photo = photo;
            registration.AdditionalCharges = personalDetailsVM.Deposite;
            registration.InternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;
            registration.ExternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;

            // w.loon - Is Justification/Supervisor Approval Required
            string ListOfRequiredBreValidation = WebHelper.Instance.ListOfRequiredBreValidation()["ListOfRequiredBreValidation"];

            registration.AgeCheckStatus = ListOfRequiredBreValidation.Contains("AgeCheck") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]) : "AF"
                                                    : "AN";
            registration.DDMFCheckStatus = ListOfRequiredBreValidation.Contains("DDMF Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]) : "DF"
                                                    : "DN";
            registration.AddressCheckStatus = ListOfRequiredBreValidation.Contains("Address Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]) : "ADF"
                                                    : "ADN";
            registration.OutStandingCheckStatus = ListOfRequiredBreValidation.Contains("Outstanding Credit Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()]) : "OF"
                                                    : "ON";
            registration.TotalLineCheckStatus = ListOfRequiredBreValidation.Contains("Total Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()]) : "TLF"
                                                    : "TLN";
            registration.PrincipleLineCheckStatus = ListOfRequiredBreValidation.Contains("Principle Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()]) : "PLF"
                                                    : "PLN";
            registration.BiometricVerify = ListOfRequiredBreValidation.Contains("Biometric") ?
                                                    (!ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? Session[SessionKey.RegMobileReg_BiometricVerify.ToString()].ToBool() : false)
                                                    : false;
            //added by Nreddy for MyKad
            registration.IsVerified = !ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? true : false;
            registration.ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
            registration.RFSalesDT = DateTime.Now;

            registration.MSISDN1 = WebHelper.Instance.retriveVirtualNumbers(true);

            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
            {
                registration.SalesPerson = Request.Cookies["CookieUser"].Value;
            }
            if (Session["OfferId_Seco"] != null)
                registration.OfferID = Convert.ToDecimal(Session["OfferId_Seco"]);
            else
                registration.OfferID = 0;

            //Added by VLT on 1st May 2013 to add K2Type in Registration
            using (var proxy = new CatalogServiceProxy())
            {
                int contractId = 0;
                List<int> contractsList = new List<int>();
                List<Component> componentsList = new List<Component>();


                if (Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()] != null)
                {
                    contractId = Convert.ToInt32(Session[SessionKey.RegMobileReg_ContractID_Seco.ToString()]);
                    contractsList.Add(contractId);

                    var vases = MasterDataCache.Instance.FilterComponents
                    (
                    (IEnumerable<int>)contractsList
                    ).ToList();

                    componentsList = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();
                    if (componentsList != null && componentsList.Count > 0)
                    {
                        string selectedContract = string.Empty;
                        List<string> lstComponentNames = new List<string>();
                        lstComponentNames = componentsList.Where(vas => vas.ID == vases[0].ChildID).Select(vas => vas.Name).ToList();
                        if (lstComponentNames != null && lstComponentNames.Count > 0)
                        {
                            selectedContract = lstComponentNames[0];
                        }
                        if (ConfigurationManager.AppSettings["K2ContractName"] != null)
                        {
                            if (selectedContract.ToLower().Contains(ConfigurationManager.AppSettings["K2ContractName"].ToString().ToLower()))
                                registration.K2Type = true;
                            else registration.K2Type = false;
                        }
                    }

                }
            }

            //Added by VLT on 1st May 2013 to add K2Type in Registration

            registration.CreateDT = DateTime.Now;
            registration.LastAccessID = Util.SessionAccess.UserName;
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "0";
            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
            registration.ContractCheckStatus = ListOfRequiredBreValidation.Contains("Contract Check") ?
                                                !ReferenceEquals(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()]) : "CF"
                                                : "CN";
            registration.UserType = Session[SessionKey.PPID.ToString()].ToString() == null ? "N" : Session[SessionKey.PPID.ToString()].ToString();

            if (dropObj != null)
            {
                if (dropObj.msimIndex > 0)
                {
                    var mainFlow = dropObj.mainFlow;
                    registration.DeviceAdvance = System.Configuration.ConfigurationManager.AppSettings["MISMDeviceAdvance"].ToInt();
                    registration.DeviceDeposit = System.Configuration.ConfigurationManager.AppSettings["MISMDeviceDeposit"].ToInt();
                    registration.PlanAdvance = System.Configuration.ConfigurationManager.AppSettings["MISMPlanAdvance"].ToInt();//Anthony - [#2439] need to be configured in web.config
                    registration.PlanDeposit = System.Configuration.ConfigurationManager.AppSettings["MISMPlanDeposit"].ToInt();
                    //registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? mainFlow.orderSummary.MalayPlanAdv : mainFlow.orderSummary.OthPlanAdv;
                    //registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? mainFlow.orderSummary.MalyDevAdv : mainFlow.orderSummary.OthDevAdv;
                    //registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? mainFlow.orderSummary.MalyPlanDeposit : mainFlow.orderSummary.OthPlanDeposit;
                    //registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? mainFlow.orderSummary.MalayDevDeposit : mainFlow.orderSummary.MalayDevDeposit;
                }
                else
                {
                    var mainFlow = dropObj.mainFlow;
                    registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? mainFlow.orderSummary.MalayPlanAdv : mainFlow.orderSummary.OthPlanAdv;
                    registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? mainFlow.orderSummary.MalyDevAdv : mainFlow.orderSummary.OthDevAdv;
                    registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? mainFlow.orderSummary.MalyPlanDeposit : mainFlow.orderSummary.OthPlanDeposit;
                    registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? mainFlow.orderSummary.MalayDevDeposit : mainFlow.orderSummary.MalayDevDeposit;
                }
            }
            else
            {
                registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalayPlanAdv_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayPlanAdv_Seco"].ToString()) : 0) : ((Session["RegMobileReg_OthPlanAdv_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthPlanAdv_Seco"].ToString()) : 0);
                registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalyDevAdv_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyDevAdv_Seco"].ToString()) : 0) : ((Session["RegMobileReg_OthDevAdv_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthDevAdv_Seco"].ToString()) : 0);
                registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalyPlanDeposit_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyPlanDeposit_Seco"].ToString()) : 0) : ((Session["RegMobileReg_OthPlanDeposit_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthPlanDeposit_Seco"].ToString()) : 0);
                registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalayDevDeposit_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayDevDeposit_Seco"].ToString()) : 0) : ((Session["RegMobileReg_OthDevDeposit_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthDevDeposit_Seco"].ToString()) : 0);
            }
            registration.fxAcctNo = Session[SessionKey.FxAccNo.ToString()] == null ? null : Session[SessionKey.FxAccNo.ToString()].ToString();
            registration.externalId = Session[SessionKey.ExternalID.ToString()] == null ? null : Session[SessionKey.ExternalID.ToString()].ToString();
            registration.AccExternalID = Session[SessionKey.AccExternalID.ToString()] == null ? null : Session[SessionKey.AccExternalID.ToString()].ToString();
            registration.OutstandingAcs = Session[SessionKey.FailedAcctIds.ToString()] == null ? string.Empty : Session[SessionKey.FailedAcctIds.ToString()].ToString();
            registration.PrePortInCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()]) : "MPPF";
            registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()]) : "MSIF";

            if (Session[SessionKey.RegMobileReg_UOMCode.ToString()] != null)
            {
                registration.UOMCode = Session[SessionKey.RegMobileReg_UOMCode.ToString()].ToString();
            }
            else
            {
                registration.UOMCode = string.Empty;
            }
            // For plan setting UOM code and offerid to generic values -- By Patanjali on 29-04-2013
            if (registration.RegTypeID != 2)
            {
                registration.OfferID = 0;
                registration.UOMCode = "EA";
            }

            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : "No";
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "No";
            registration.OrganisationId = Util.SessionAccess.User.Org.OrganisationId;

            registration.Trn_Type = ConfigurationManager.AppSettings["Trn_Type"];
            registration.IMPOSStatus = 0;
            registration.SimModelId = 0;
            if (!string.IsNullOrEmpty(SIMModelSelected.Split(',')[0]) && SIMModelSelected.Split(',')[0].ToLower() != "false")
                registration.SimModelId = SIMModelSelected.Split(',')[0].ToInt();


            //Code Added by VLT Check for International Roaming
            if (Session[SessionKey.RegMobileReg_VasNames_Seco.ToString()] != null)
            {
                var vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames_Seco.ToString()];
                string strVasArray = string.Join(",", vasNames.ToArray());
                if (strVasArray.Contains(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["InternationalRoaming"])) == true)
                {
                    registration.InternationalRoaming = true;
                }
            }

            //End of International Roaming Check   
            //registration.SIMSerial = Session["DelearSecondarySim"].ToString2();
            //registration.SIMSerial = dropObj != null ? dropObj.msimFlow[0].simSerial : Session["DelearSecondarySim"].ToString2();
            //registration.SIMSerial = dropObj != null ? dropObj.mainFlow.simSerial : Session["DelearSecondarySim"].ToString2();
            if (dropObj.msimFlow.Count() != 0)
            {
                registration.SIMSerial = dropObj.msimFlow[0].simSerial;
            }

            if (Session["IsDealer"].ToBool() == true)
            {
                registration.MSISDN1 = WebHelper.Instance.retriveVirtualNumbers(isRegistraton: true);
            }
            return registration;
        }
        private DAL.Models.Registration ConstructRegistration(string queueNo, string remarks, string signatureSVG = "", string custPhoto = "", string altCustPhoto = "", string photo = "", string SIMModelSelected = "")
        {
            var regTypeID = 0;
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

            DAL.Models.Registration registration = new DAL.Models.Registration();
            //we set this always TRUE why because based on this we are storing the ApproveBlacklistCheck  in trnregistration this we are sending to kenan as sales code
            var isBlacklisted = true;
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
            {
                regTypeID = Util.GetRegTypeID(REGTYPE.MNPNewLine);
            }
            else
            {
                regTypeID = Util.GetRegTypeID(REGTYPE.NewLine);
            }
            var reg = new DAL.Models.Registration();
            try
            {
                registration = new DAL.Models.Registration();
                registration.CenterOrgID = Util.SessionAccess.User.OrgID;
                registration.RegTypeID = regTypeID;
                registration.QueueNo = queueNo;
                registration.Remarks = remarks;
                registration.SignatureSVG = signatureSVG;
                // registration.CustomerPhoto = custPhoto;
                //registration.AltCustomerPhoto = altCustPhoto;
                // registration.Photo = photo;
                registration.AdditionalCharges = personalDetailsVM.Deposite;
                registration.InternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;
                registration.ExternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;

                // w.loon - Is Justification/Supervisor Approval Required
                string ListOfRequiredBreValidation = WebHelper.Instance.ListOfRequiredBreValidation()["ListOfRequiredBreValidation"];

                registration.AgeCheckStatus = ListOfRequiredBreValidation.Contains("AgeCheck") ?
                                                        !ReferenceEquals(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]) : "AF"
                                                        : "AN";
                registration.DDMFCheckStatus = ListOfRequiredBreValidation.Contains("DDMF Check") ?
                                                        !ReferenceEquals(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]) : "DF"
                                                        : "DN";
                registration.AddressCheckStatus = ListOfRequiredBreValidation.Contains("Address Check") ?
                                                        !ReferenceEquals(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]) : "ADF"
                                                        : "ADN";
                registration.OutStandingCheckStatus = ListOfRequiredBreValidation.Contains("Outstanding Credit Check") ?
                                                        !ReferenceEquals(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()]) : "OF"
                                                        : "ON";
                registration.TotalLineCheckStatus = ListOfRequiredBreValidation.Contains("Total Line Check") ?
                                                        !ReferenceEquals(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()]) : "TLF"
                                                        : "TLN";
                registration.PrincipleLineCheckStatus = ListOfRequiredBreValidation.Contains("Principle Line Check") ?
                                                        !ReferenceEquals(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()]) : "PLF"
                                                        : "PLN";
                registration.BiometricVerify = ListOfRequiredBreValidation.Contains("Biometric") ?
                                                        (!ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? Session[SessionKey.RegMobileReg_BiometricVerify.ToString()].ToBool() : false)
                                                        : false;
                //added by Nreddy for MyKad
                registration.IsVerified = !ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? true : false;
                registration.ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
                registration.RFSalesDT = DateTime.Now;
                if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && !ReferenceEquals(Session[SessionKey.RegMobileReg_MSISDN.ToString()], null))
                {
                    registration.MSISDN1 = Convert.ToString(Session[SessionKey.RegMobileReg_MSISDN.ToString()]);
                }
                else
                {
                    //registration.MSISDN1 = Session[SessionKey.RegMobileReg_MobileNo.ToString()] == null ? null : ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2();
                    if (dropObj != null && dropObj.mainFlow != null)
                    {
                        registration.MSISDN1 = dropObj.mainFlow.mobile.SelectedMobileNo;
                    }
                    else
                    {
                        registration.MSISDN1 = Session[SessionKey.RegMobileReg_MobileNo.ToString()] == null ? null : ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2();
                    }
                }
                if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                {
                    registration.SalesPerson = Request.Cookies["CookieUser"].Value;
                }
                registration.CreateDT = DateTime.Now;
                registration.LastAccessID = Util.SessionAccess.UserName;
                registration.Whitelist_Status = Session[SessionKey.Mocandwhitelistcuststatus.ToString()] == null ? null : Session[SessionKey.Mocandwhitelistcuststatus.ToString()].ToString();
                registration.MOC_Status = Session[SessionKey.MocDefault.ToString()] == null ? null : Session[SessionKey.MocDefault.ToString()].ToString();
                registration.ContractCheckStatus = ListOfRequiredBreValidation.Contains("Contract Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()]) : "CF"
                                                    : "CN";
                registration.UserType = "E";
                bool IsWriteOff = false;
                string acc = string.Empty;
                WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
                registration.WriteOffCheckStatus = ListOfRequiredBreValidation.Contains("Writeoff Check") ?
                                                        !IsWriteOff ? "WOS" : "WOF"
                                                        : "WON";                
               
                if (dropObj != null)
                {
                    registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((dropObj.mainFlow.orderSummary.MalayPlanAdv != null) ? Convert.ToDecimal(dropObj.mainFlow.orderSummary.MalayPlanAdv) : 0) : ((dropObj.mainFlow.orderSummary.OthPlanAdv != null) ? Convert.ToDecimal(dropObj.mainFlow.orderSummary.OthPlanAdv) : 0);
                    registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((dropObj.mainFlow.orderSummary.MalyDevAdv != null) ? Convert.ToDecimal(dropObj.mainFlow.orderSummary.MalyDevAdv) : 0) : ((dropObj.mainFlow.orderSummary.OthDevAdv != null) ? Convert.ToDecimal(dropObj.mainFlow.orderSummary.OthDevAdv) : 0);
                    registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((dropObj.mainFlow.orderSummary.MalyPlanDeposit != null) ? Convert.ToDecimal(dropObj.mainFlow.orderSummary.MalyPlanDeposit) : 0) : ((dropObj.mainFlow.orderSummary.OthPlanDeposit != null) ? Convert.ToDecimal(dropObj.mainFlow.orderSummary.OthPlanDeposit) : 0);
                    registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((dropObj.mainFlow.orderSummary.MalayDevDeposit != null) ? Convert.ToDecimal(dropObj.mainFlow.orderSummary.MalayDevDeposit) : 0) : ((dropObj.mainFlow.orderSummary.OthDevDeposit != null) ? Convert.ToDecimal(dropObj.mainFlow.orderSummary.OthDevDeposit) : 0);
                    registration.ArticleID = dropObj.mainFlow.orderSummary.ArticleId;
                    registration.OfferID = dropObj.mainFlow.vas.OfferId != 0 ? dropObj.mainFlow.vas.OfferId : (Session["OfferId"] != null ? Convert.ToDecimal(Session["OfferId"]) : 0);
                    registration.IMEINumber = dropObj.mainFlow.imeiNumber;
					string simSerialDealer = !string.IsNullOrEmpty(Session["PrimarySim"].ToString2()) ? Session["PrimarySim"].ToString2() : dropObj.mainFlow.simSerial;
					registration.SIMSerial = simSerialDealer;
                }

                else
                {
                    registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()].ToString()) : 0);
                    registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthDevAdv.ToString()].ToString()) : 0);
                    registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalyPlanDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyPlanDeposit"].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()].ToString()) : 0);
                    registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalayDevDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayDevDeposit"].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()].ToString()) : 0);
                    registration.OfferID = 0;
                    registration.ArticleID = !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Session[SessionKey.ArticleId.ToString()].ToString()  : string.Empty;
					string simSerialDealer = !string.IsNullOrEmpty(Session["PrimarySim"].ToString2()) ? Session["PrimarySim"].ToString2() : dropObj.mainFlow.simSerial;
					registration.SIMSerial = simSerialDealer;
                }


                registration.fxAcctNo = Session[SessionKey.FxAccNo.ToString()] == null ? null : Session[SessionKey.FxAccNo.ToString()].ToString();
                registration.externalId = Session[SessionKey.ExternalID.ToString()] == null ? null : Session[SessionKey.ExternalID.ToString()].ToString();
                registration.AccExternalID = Session[SessionKey.AccExternalID.ToString()] == null ? null : Session[SessionKey.AccExternalID.ToString()].ToString();
                registration.OutstandingAcs = Session[SessionKey.FailedAcctIds.ToString()] == null ? string.Empty : Session[SessionKey.FailedAcctIds.ToString()].ToString();
                registration.PrePortInCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()]) : "MPPF";
                registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()]) : "MSIF";
                registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : "No";
                registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "NO";
                registration.KenanAccountNo = !ReferenceEquals(Session[SessionKey.KenanACNumber.ToString()], null) ? Session[SessionKey.KenanACNumber.ToString()].ToString() : "";
                registration.OrganisationId = Util.SessionAccess.User.Org.OrganisationId;
                registration.IMPOSStatus = 0;
                registration.Trn_Type = ConfigurationManager.AppSettings["Trn_Type"];

                //GTM e-Billing CR - Ricky - 2014.09.25
                registration.billDeliveryViaEmail = personalDetailsVM.EmailBillSubscription == "Y" ? true : false;
                registration.billDeliveryEmailAddress = personalDetailsVM.EmailBillSubscription == "Y" ? personalDetailsVM.EmailAddress : "";
                registration.billDeliverySmsNotif = personalDetailsVM.SmsAlertFlag == "Y" ? true : false;
                registration.billDeliverySmsNo = personalDetailsVM.SmsAlertFlag == "Y" ? personalDetailsVM.SmsNo : "";
                registration.billDeliverySubmissionStatus = "Pending";

				if (dropObj != null && dropObj.mainFlow != null && !string.IsNullOrEmpty(dropObj.mainFlow.UOMCode))
				{
					registration.UOMCode = dropObj.mainFlow.UOMCode.ToString2();
				}
				else {
					registration.UOMCode = !ReferenceEquals(Session[SessionKey.RegMobileReg_UOMCode.ToString()], null) ? Session[SessionKey.RegMobileReg_UOMCode.ToString()].ToString() : null;
				}

                //Code Added by VLT Check for International Roaming
                if (Session[SessionKey.RegMobileReg_VasNames.ToString()] != null)
                {
                    var vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
                    string strVasArray = string.Join(",", vasNames.ToArray());
                    if (strVasArray.Contains(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["InternationalRoaming"])) == true)
                    {
                        registration.InternationalRoaming = true;
                    }
                }
                //End of International Roaming Check
                //Added by Chetan for saving DonorID & Master PrePortReqId
                registration.PrePortReqId = ReferenceEquals(Session[SessionKey.Master_PrePortReqId.ToString()], null) ? "" : Session[SessionKey.Master_PrePortReqId.ToString()].ToString();
                registration.DonorId = ReferenceEquals(Session[SessionKey.DonorId.ToString()], null) ? "" : Session[SessionKey.DonorId.ToString()].ToString();
                Session[SessionKey.Master_PrePortReqId.ToString()] = null;
                Session[SessionKey.DonorId.ToString()] = null;
                //End Here
                if (!string.IsNullOrEmpty(SIMModelSelected.Split(',')[0]) && SIMModelSelected.Split(',')[0].ToLower() != "false")
                    registration.SimModelId = SIMModelSelected.Split(',')[0].ToInt();
                
            }
            catch (Exception ex)
            {
                LogExceptions(ex);
            }
            return registration;
        }
        private List<RegSmartComponents> ConstructSmartComponents()
        {
            List<RegSmartComponents> regSmartComponents = new List<RegSmartComponents>();
            if (Session["_componentViewModel"] != null)
            {
                ComponentViewModel sessionVal = (ComponentViewModel)Session["_componentViewModel"];
                RegSmartComponents smartcomp = new RegSmartComponents();
                foreach (var c in sessionVal.NewComponentsList)
                {
                    if (c.IsChecked)
                    {
                        smartcomp = new RegSmartComponents();
                        smartcomp.PackageID = c.NewPackageKenanCode;
                        smartcomp.ComponentID = c.NewComponentKenanCode;
                        smartcomp.ComponentDesc = c.NewComponentName;
                        smartcomp.PackageDesc = string.Empty;                     // write package name here if require
                        regSmartComponents.Add(smartcomp);
                    }
                }
            }
            return regSmartComponents;
        }
        private void LogExceptions(Exception ex)
        {
            WebHelper.Instance.LogExceptions(this.GetType(), ex);
        }

        #endregion

        #region RegSupplines
        public List<RegSuppLine> ConstructRegSuppLines(int nationalityId = 1, string supplinesSimModelIds = "")
        {
            var regSuppLines = new List<RegSuppLine>();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                if (dropObj != null && dropObj.suppIndex > 0)
                {
                    using (var proxy = new CatalogServiceProxy())
                    {
                        for (int i = 0; i < dropObj.suppIndex; i++)
                        {
                            var tempDropObj = dropObj.suppFlow[i];

                            var modelGroupIDs = proxy.ModelGroupFind(new ModelGroupFind()
                            {
                                ModelGroup = new ModelGroup()
                                {
                                    PgmBdlPckComponentID = tempDropObj.PkgPgmBdlPkgCompID
                                },
                                Active = true
                            });
                            
                                var modelGroupModels = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                                {
                                    ModelGroupModel = new ModelGroupModel()
                                    {
                                        ModelID = tempDropObj.phone != null ? tempDropObj.phone.SelectedModelImageID.ToInt() : 0
                                    }
                                })).ToList();

								//int simSizeId = 1;
								//if (tempDropObj.simSize.ToLower() == "micro")
								//{
								//    simSizeId = 2;
								//}
								//else if (tempDropObj.simSize.ToLower() == "nano")
								//{
								//    simSizeId = 3;
								//}

                            //20141210 - Habs - start
                            String mismTypeInput = tempDropObj.mismType;
                            //20141210 - Habs - end

                            regSuppLines.Add(new RegSuppLine()
                            {
                                SequenceNo = (i + 1),
                                PgmBdlPckComponentID = tempDropObj.PkgPgmBdlPkgCompID,
                                IsNewAccount = true,
                                MSISDN1 = tempDropObj.mobile.SelectedMobileNo,
                                Active = true,
                                CreateDT = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                BrandDeviceArticleID = tempDropObj.orderSummary.ArticleId,
                                planadvance = (nationalityId == 1) ? ((tempDropObj.orderSummary.MalayPlanAdv != null) ? tempDropObj.orderSummary.MalayPlanAdv : 0) : ((tempDropObj.orderSummary.OthPlanAdv != null) ? tempDropObj.orderSummary.OthPlanAdv : 0),
                                deviceadvance = (nationalityId == 1) ? ((tempDropObj.orderSummary.MalyDevAdv != null) ? tempDropObj.orderSummary.MalyDevAdv : 0) : ((tempDropObj.orderSummary.OthDevAdv != null) ? tempDropObj.orderSummary.OthDevAdv : 0),
                                plandeposit = (nationalityId == 1) ? ((tempDropObj.orderSummary.MalyPlanDeposit != null) ? tempDropObj.orderSummary.MalyPlanDeposit : 0) : ((tempDropObj.orderSummary.OthPlanDeposit != null) ? tempDropObj.orderSummary.OthPlanDeposit : 0),
                                devicedeposit = (nationalityId == 1) ? ((tempDropObj.orderSummary.MalayDevDeposit != null) ? tempDropObj.orderSummary.MalayDevDeposit : 0) : ((tempDropObj.orderSummary.OthDevDeposit != null) ? tempDropObj.orderSummary.OthDevDeposit : 0),
                                ModelGroupModelID = ((modelGroupModels.Where(a => modelGroupIDs.Contains(a.ModelGroupID)).Select(a => a.ID).FirstOrDefault() != null) ? modelGroupModels.Where(a => modelGroupIDs.Contains(a.ModelGroupID)).Select(a => a.ID).FirstOrDefault() : 0),
                               // ModelImageID = modelGroupModels,
                                Price = tempDropObj.orderSummary.ModelPrice,
                                OfferID = tempDropObj.vas != null ? tempDropObj.vas.OfferId : 0,
                                SIMSerial = tempDropObj.simSerial,
                                //SimModelId = simSizeId,
								//simModel is refering to refSimSerials so below must filled with Normal / Micro / Nano
								MNPSuppLineSimCardType = tempDropObj.simSize,
                                ArticleID = tempDropObj.orderSummary.ArticleId,
                                UOMCode = tempDropObj.UOMCode,
                                MismType = mismTypeInput
                            });
                        }
                    }
                }
                else
                {

                    var sublines = (List<SublineVM>)Session["RegMobileReg_SublineVM"];
                    if (sublines != null)
                    {
                        foreach (var subline in sublines)
                        {
                            // SuppLine Plan
                            regSuppLines.Add(new RegSuppLine()
                            {
                                SequenceNo = subline.SequenceNo,
                                PgmBdlPckComponentID = subline.PkgPgmBdlPkgCompID,
                                IsNewAccount = true,
                                MSISDN1 = subline.SelectedMobileNos[0].ToString2(),
                                Active = true,
                                CreateDT = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName
                            });
                        }
                    }
                }
            return regSuppLines;
        }

        #endregion Regsupplines

        public List<RegPgmBdlPkgComp> ConstructRegSuppLineVASes()
        {
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            var sublineVMs = new List<SublineVM>();
            #region  ::"Changes for primay and Multiple secondary"::
            if (dropObj != null && dropObj.suppIndex > 0)
            {
                //List<SupplinePlanStorage> supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : null;

                for (int i = 0; i < dropObj.suppIndex; i++)
                {
					var _suppLine = dropObj.suppFlow[i];

					var vasNames = new List<string>();
					var selectedCompIDs = new List<int>();
					var package = new Package();
					var bundle = new Bundle();

					selectedCompIDs = _suppLine.vas.SelectedPgmBdlPkgCompIDs;

					using (var proxy = new CatalogServiceProxy())
					{
						var bundlePackage = MasterDataCache.Instance.FilterComponents(new[] { _suppLine.PkgPgmBdlPkgCompID }).SingleOrDefault();
						package = proxy.PackageGet(new int[] { bundlePackage.ChildID }).SingleOrDefault();
						bundle = proxy.BundleGet(new int[] { bundlePackage.ParentID }).SingleOrDefault();
						vasNames = proxy.ComponentGet(MasterDataCache.Instance.FilterComponents(selectedCompIDs).Select(a => a.ChildID)).Select(a => a.Name).ToList();
					}

					List<string> srnumbers = new List<string>();
					List<string> selectedMobileNumber = new List<string>();
					var mainID = _suppLine.MandatoryPackage != null ? _suppLine.MandatoryPackage.ToInt() : 0;
					var extraID = _suppLine.ExtraPackage != null ? _suppLine.ExtraPackage.ToInt() : 0;
					var dataID = _suppLine.DataPackage != null ? _suppLine.DataPackage.ToInt() : 0;
					var promotionID = _suppLine.PromotionPackage != null ? _suppLine.PromotionPackage.ToInt() : 0;
					var insuranceID = _suppLine.InsurancePackage != null ? _suppLine.InsurancePackage.ToInt() : 0;

					if (null != _suppLine.mobile)
					{
						if (null != _suppLine.mobile.SelectedMobileNumber)
						{
							string mobileNumber = _suppLine.mobile.SelectedMobileNumber[0];
							selectedMobileNumber.Add(mobileNumber);
						}
					}
					else
					{
						string msisdn = dropObj.suppFlow[i].orderSummary.MNPSupplinesList[0].RegMobileReg_MSISDN;
						selectedMobileNumber.Add(msisdn);
					}
                    sublineVMs.Add(new SublineVM()
                    {
                        SequenceNo = (i + 1),
                        PkgPgmBdlPkgCompID = _suppLine.PkgPgmBdlPkgCompID,
                        SelectedMobileNos = _suppLine.mobile.SelectedMobileNumber,
                        SelectedVasIDs = selectedCompIDs,
                        SelectedVasNames = vasNames,
                        BundleName = bundle.Name,
						PackageName = package.Name,
						MainPackageId = mainID,
						DataPackageId = dataID,
						ExtraPackageId = extraID,
						PromoPackageId = promotionID,
						InsurancePackageId = insuranceID
                    });
                }
            }
            #endregion


            var regSuppLinePBPCs = new List<RegPgmBdlPkgComp>();
            var sublines = (dropObj.suppIndex > 0) ? sublineVMs : (List<SublineVM>)Session["RegMobileReg_SublineVM"];

            if (sublines != null)
            {
                foreach (var subline in sublines)
                {
                    var suppLineVasIDs = new List<int>();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var pkgID = MasterDataCache.Instance.FilterComponents(new int[] { subline.PkgPgmBdlPkgCompID }).SingleOrDefault().ChildID;

                        suppLineVasIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                        {
                            IsMandatory = true,
                            PgmBdlPckComponent = new PgmBdlPckComponent()
                            {
                                ParentID = pkgID,
                                LinkType = Properties.Settings.Default.Package_Component,
                                Active = true
                            },
                            Active = true
                        }).ToList();
                    }

					regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
					{
						SequenceNo = subline.SequenceNo,
						PgmBdlPckComponentID = subline.PkgPgmBdlPkgCompID,
						IsNewAccount = true,
						Active = true,
						CreateDT = DateTime.Now,
						LastAccessID = Util.SessionAccess.UserName
					});
					// add subvasess for suppline  mandatory
					foreach (var suppLineVasID in suppLineVasIDs)
					{
						regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
						{
							SequenceNo = subline.SequenceNo,
							PgmBdlPckComponentID = suppLineVasID,
							IsNewAccount = true,
							Active = true,
							CreateDT = DateTime.Now,
							LastAccessID = Util.SessionAccess.UserName
						});
					}

					if (subline.MainPackageId > 0)
					{
						regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
						{
							SequenceNo = subline.SequenceNo,
							PgmBdlPckComponentID = subline.MainPackageId,
							IsNewAccount = true,
							Active = true,
							CreateDT = DateTime.Now,
							LastAccessID = Util.SessionAccess.UserName
						});
					}

					if (subline.DataPackageId > 0)
					{
						regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
						{
							SequenceNo = subline.SequenceNo,
							PgmBdlPckComponentID = subline.DataPackageId,
							IsNewAccount = true,
							Active = true,
							CreateDT = DateTime.Now,
							LastAccessID = Util.SessionAccess.UserName
						});
					}

					if (subline.ExtraPackageId > 0)
					{
						regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
						{
							SequenceNo = subline.SequenceNo,
							PgmBdlPckComponentID = subline.ExtraPackageId,
							IsNewAccount = true,
							Active = true,
							CreateDT = DateTime.Now,
							LastAccessID = Util.SessionAccess.UserName
						});
					}

                    // Optional SuppLine VAS
                    foreach (var vasID in subline.SelectedVasIDs)
                    {
                        regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
                        {
                            SequenceNo = subline.SequenceNo,
                            PgmBdlPckComponentID = vasID.ToInt(),
                            IsNewAccount = true,
                            Active = true,
                            CreateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }
                }
            }
            return regSuppLinePBPCs;
        }

        //private void PaymentRecived(PersonalDetailsVM personalDetailsVM)
        //{
        //    using (var proxy = new RegistrationServiceProxy())
        //    {
        //        proxy.RegistrationCancel(new RegStatus()
        //        {
        //            RegID = personalDetailsVM.RegID,
        //            Active = true,
        //            CreateDT = DateTime.Now,
        //            StartDate = DateTime.Now,
        //            LastAccessID = Util.SessionAccess.UserName,
        //            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
        //        });
        //    }

        //    using (var proxy = new KenanServiceProxy())
        //    {
        //        CenterOrderContractRequest crpOrder = new CenterOrderContractRequest();
        //        crpOrder.orderId = personalDetailsVM.RegID.ToString();
        //        proxy.CenterOrderContractCreationDeviceCRP(crpOrder);

        //    }

        //    using (var proxy = new RegistrationServiceProxy())
        //    {
        //        proxy.RegistrationCancel(new RegStatus()
        //        {
        //            RegID = personalDetailsVM.RegID,
        //            Active = true,
        //            CreateDT = DateTime.Now,
        //            StartDate = DateTime.Now,
        //            LastAccessID = Util.SessionAccess.UserName,
        //            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_SvcPendingActivate)
        //        });
        //    }                

        //}


    }
}
