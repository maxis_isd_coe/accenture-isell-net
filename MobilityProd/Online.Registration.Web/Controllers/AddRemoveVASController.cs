﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc; 
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.Properties;
using Online.Web.Helper;
using SNT.Utility;
using log4net;
using System.Diagnostics;
using System.IO;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.ContentRepository;
using Online.Registration.Web.CMSSSvc;
using CMSS = Online.Registration.Web.CMSSSvc;
using Online.Registration.Web.Models;

using System.Web.Security;
namespace Online.Registration.Web.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    //[HandleError(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class AddRemoveVASController : Controller
    {
        #region Members
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AddRemoveVASController));
		private bool sharingQuotaFeature = ConfigurationManager.AppSettings["SharingSupplementaryFlag"].ToBool();
        #endregion

        #region class
        [Serializable]
        public class ARVASPackageComponentModel
        {
            public string PackageID { get; set; }


            public string PackageDesc { get; set; }
            public string packageInstId { get; set; }

            public string packageInstIdServ { get; set; }

            public string Contract12Months { get; set; }

            public string Contract24Months { get; set; }

            public string packageActiveDt { get; set; }

            public List<ARVASComponents> compList { get; set; }
        }
        [Serializable]
        public class ARVASComponents
        {
            public string packagedId { get; set; }

            public string componentId { get; set; }

            public string componentInstId { get; set; }

            public string componentInstIdServ { get; set; }

            public string componentActiveDt { get; set; }

            public string componentInactiveDt { get; set; }

            public string componentDesc { get; set; }

            public string componentShortDisplay { get; set; }

            public string lnkpgmbdlpkgcompId { get; set; }

            public string GroupId { get; set; }

            public string Price { get; set; }

            public int Value { get; set; }
        }
        #endregion

        #region Private Methods

        private List<ARVASPackageComponentModel> GetPackageDetails(string MSISDN, string Kenancode, out List<Online.Registration.Web.SubscriberICService.componentList> vasComponentList, ref List<string> subscribedComponents)
        {
            List<string> componentsToRemove = ConfigurationManager.AppSettings["RestrictComponentsForDealer"].ToString2().Split(',').ToList();
            Session["OCCmpntList"] = null;
            Session["ComponentInfo"] = null;
            Online.Registration.Web.SubscriberICService.componentList component = new SubscriberICService.componentList();
            List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<SubscriberICService.componentList>();
            var resp_existing = new SmartGetExistingPackageDetailsResp();
            List<PackageComponentModel> packageComponentModel = new List<PackageComponentModel>();
            List<string> ExistingKenanIDs = new List<string>();
            PackageModel package;
            List<SubscriberICService.Components> components;
            List<SubscriberICService.PackageModel> packages = new List<PackageModel>();

            List<SubscriberICService.PackageModel> retrievedPackages = new List<PackageModel>();
            int tempAdd = 0;

            #region Retrieve Rebate Contract Id
            RetrieveComponentInfoResponse objRetrieveComponentInfoRes = null;
            using (var proxy = new KenanServiceProxy())
            {
                RetrieveComponentInfoRequest objRetrieveComponentInfoReq = new RetrieveComponentInfoRequest();
                objRetrieveComponentInfoReq.ExternalID = Session[SessionKey.ExternalID.ToString()].ToString2();
                Session["ComponentInfo"] = objRetrieveComponentInfoRes = proxy.RetrieveComponentInfo(objRetrieveComponentInfoReq);
            }
            #endregion

            using (var proxy = new retrieveServiceInfoProxy())
            {
                packages = proxy.retrievePackageDetls(MSISDN, "23", userName: "", password: "", postUrl: "").ToList();

                IEnumerable<IGrouping<string, PackageModel>> query = packages.GroupBy(k => k.PackageDesc);
                foreach (IGrouping<string, PackageModel> groupItem in query)
                {
                    package = new PackageModel();
                    components = new List<SubscriberICService.Components>();
                    
                    foreach (PackageModel vasItem in groupItem)
                    {
                        tempAdd++;
                        foreach (var packageComp in vasItem.compList)
                        {
                            components.Add(packageComp);
                            component = new componentList();
                            component.packageIdField = vasItem.PackageID;
                            component.componentIdField = packageComp.componentId;
                            componentList.Add(component);
                            if (Session["OCCmpntList"].ToString2().Length > 0)
                            {
                                Session["OCCmpntList"] = Session["OCCmpntList"].ToString2() + "," + packageComp.componentId.ToString();
                            }
                            else
                            {
                                Session["OCCmpntList"] = packageComp.componentId.ToString();
                            }
                        }
                        package.PackageDesc = vasItem.PackageDesc;
                        package.PackageID = vasItem.PackageID;

                    }
                    package.compList = components;
                    if (tempAdd > 0)
                        retrievedPackages.Add(package);

                    tempAdd = 0;

                }
            }
            packages = retrievedPackages;
            List<ARVASPackageComponentModel> listPakcagecomponents = new List<ARVASPackageComponentModel>();
            ARVASPackageComponentModel compModel = null;
            List<ARVASComponents> vascomponents = new List<ARVASComponents>();
            ARVASComponents comp = null;
            long planId = 0;
            long respPlanid = 0;
            string response = string.Empty;
            List<string> mandatoryPkgIds = new List<string>();
            string packageId = "";
            mandatoryPkgIds = packages.Where(p => p.PackageDesc.ToLower().Contains("mandatory")).Select(p => p.PackageID).ToList();
            if (mandatoryPkgIds != null && mandatoryPkgIds.Count > 0)
            {

                packageId = mandatoryPkgIds[0];

            }
            string mandatoryKenancode = string.Empty;
            string planType = "CP";
            foreach (var item in packages)
            {
                using (var proxy_kenan = new RegistrationServiceProxy())
                {
                    if (Session["AccountType"] != null && Session["AccountType"].ToString() == "MISMSec")
                    {
                        planId = proxy_kenan.GetLnkPgmBdlPkgCompIdByKenanCode(packageId, "CP", "SP");
                        if (planId > 0)
                        {
                            respPlanid = planId;
                            planType = "CP";
                        }
                    }
                    if (Session["AccountType"] != null && Session["AccountType"].ToString() == "Suppline")
                    {
                        planId = proxy_kenan.GetLnkPgmBdlPkgCompIdByKenanCode(packageId, "SP", "BP");
                        if (planId > 0)
                        {
                            respPlanid = planId;
                            planType = "CP";
                        }
                    }
                    if (Session["AccountType"] != null && Session["AccountType"].ToString() == "Primary" || Session["AccountType"] != null && Session["AccountType"].ToString() == "MISMPrimary")
                    {

                        planId = proxy_kenan.GetLnkPgmBdlPkgCompIdByKenanCode(packageId, "CP", "BP");
                        planType = "CP";
                        if (planId > 0)
                        {
                            respPlanid = planId;

                        }
                    }
                }
            }
            List<string> groupIds = new List<string>();
            List<string> vasNames = new List<string>();
            if (respPlanid > 0)
                Session["RegMobileReg_PkgPgmBdlPkgCompID"] = respPlanid;
            foreach (var item in packages)
            {
                compModel = new ARVASPackageComponentModel();
                vascomponents = new List<ARVASComponents>();
                compModel.packageActiveDt = item.packageActiveDt;
                compModel.PackageDesc = item.PackageDesc;
                compModel.PackageID = item.PackageID;
                compModel.packageInstId = item.packageInstId;
                compModel.packageInstIdServ = item.packageInstIdServ;
                ExistingKenanIDs.Add(item.PackageID);
                using (var proxy_kenan = new RegistrationServiceProxy())
                {
                    resp_existing = proxy_kenan.ExistingPackageDetails(item.PackageID.ToInt());

                    foreach (var vcomponent in item.compList)
                    {
                        if (Session["IsDealer"].ToBool())
                        {
                            if (componentsToRemove.Contains(vcomponent.componentId))
                                continue;
                        }
                        comp = new ARVASComponents();
                        if (resp_existing != null && resp_existing.ExistingPackageDetails != null && resp_existing.ExistingPackageDetails.PlanType != null && resp_existing.ExistingPackageDetails.PlanType.ToUpper() == "PP")
                        {
                            string strRebateExpDate = string.Empty;
                            List<string> lstContractTypes = null;
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                lstContractTypes = proxy.getRebateContractIDbyComponentID(vcomponent.componentId);
                            }
                            if (lstContractTypes != null && lstContractTypes.Count > 0)
                            {
                                foreach (var i in lstContractTypes)
                                {
                                    if (objRetrieveComponentInfoRes != null && objRetrieveComponentInfoRes.Contracts != null && objRetrieveComponentInfoRes.Contracts.Count() > 0)
                                    {
                                        foreach (var contract in objRetrieveComponentInfoRes.Contracts.Where(a => a.ContractType == i))
                                        {
                                            if (contract.EndDate != DateTime.MinValue && contract.EndDate >= DateTime.Now.Date)
                                                strRebateExpDate = "<b style='color:red'>" + "(" + "Rebate Exp Date :" + contract.EndDate.ToString("dd-MMM-yy") + ")" + "</b>";
                                        }
                                        
                                    }
                                   
                                }
                            }

                            comp = new ARVASComponents();
                            comp.componentActiveDt = vcomponent.componentActiveDt;
                            comp.componentDesc = vcomponent.componentDesc + strRebateExpDate;
                            comp.componentId = vcomponent.componentId;
                            comp.componentInstId = vcomponent.componentInstId;
                            comp.componentInstIdServ = vcomponent.componentInstIdServ;
                            comp.componentShortDisplay = vcomponent.componentShortDisplay;
                            subscribedComponents.Add(comp.componentId);

                        }
                        else
                        {
                            comp = new ARVASComponents();
                            comp.componentActiveDt = vcomponent.componentActiveDt;
                            comp.componentDesc = vcomponent.componentDesc;
                            comp.componentId = vcomponent.componentId;
                            comp.componentInstId = vcomponent.componentInstId;
                            comp.componentInstIdServ = vcomponent.componentInstIdServ;
                            comp.componentShortDisplay = vcomponent.componentShortDisplay;
                            subscribedComponents.Add(comp.componentId);
                        }
                        if (resp_existing != null && resp_existing.ExistingPackageDetails.PlanType != null)
                        {
                            response = proxy_kenan.GetlnkpgmbdlpkgcompidForKenancomponent(resp_existing.ExistingPackageDetails.PlanType, Convert.ToInt32(item.PackageID), vcomponent.componentId.ToString());
                            if (response != null && response.Contains(","))
                            {
                                if (ReferenceEquals(comp, null))
                                    comp = new ARVASComponents();
                                comp.GroupId = response.Split(',')[2];
                                comp.Price = response.Split(',')[1];
                                comp.lnkpgmbdlpkgcompId = response.Split(',')[0];
                                comp.Value = Convert.ToInt32(response.Split(',')[3]);
                            }

                        }
                        if (comp.Price == string.Empty || comp.Price == null)
                            comp.Price = "0.00";
                        vascomponents.Add(comp);
                        vasNames.Add(vcomponent.componentDesc + " (RM " + comp.Price + ")");
                        //}

                    }
                    compModel.compList = vascomponents;


                }
                listPakcagecomponents.Add(compModel);
            }
            Session["existingKenanIDs"] = ExistingKenanIDs;
            if (Session["RegMobileReg_VasNames"] == null)
                Session["RegMobileReg_VasNames"] = vasNames.Distinct().ToList();
            
            vasComponentList = componentList;
            return listPakcagecomponents;
        }
        private List<ARVASPackageComponentModel> GetRemovedVases(string MSISDN, string Kenancode, string removedVases, ref List<string> removedComponents)
        {

            var resp_existing = new SmartGetExistingPackageDetailsResp();
            List<PackageComponentModel> packageComponentModel = new List<PackageComponentModel>();
            List<string> ExistingKenanIDs = new List<string>();
            List<SubscriberICService.PackageModel> packages = new List<PackageModel>();
            List<SubscriberICService.Components> components;
            int tempAdd = 0;
            PackageModel package;
            List<PackageModel> retrievedPackages = new List<PackageModel>();
            using (var proxy = new retrieveServiceInfoProxy())
            {
                packages = proxy.retrievePackageDetls(MSISDN, "23", userName: "", password: "", postUrl: "").ToList();

                IEnumerable<IGrouping<string, PackageModel>> query = packages.GroupBy(k => k.PackageDesc);
                foreach (IGrouping<string, PackageModel> groupItem in query)
                {
                    package = new PackageModel();
                    components = new List<SubscriberICService.Components>();
                    
                    foreach (PackageModel vasItem in groupItem)
                    {
                        tempAdd++;
                        foreach (var packageComp in vasItem.compList)
                        {
                            components.Add(packageComp);
                        }
                        package.PackageDesc = vasItem.PackageDesc;
                        package.PackageID = vasItem.PackageID;

                    }
                    package.compList = components;
                    if (tempAdd > 0)
                        retrievedPackages.Add(package);

                    tempAdd = 0;

                }
            }
            long planId = 0;
            long respPlanid = 0;
            using (var proxy_kenan = new RegistrationServiceProxy())
            {
                foreach (var item in packages)
                {
                    resp_existing = proxy_kenan.ExistingPackageDetails(item.PackageID.ToInt());

                    if (Session["AccountType"] != null && Session["AccountType"].ToString() == "MISMSec")
                    {
                        planId = proxy_kenan.GetLnkPgmBdlPkgCompIdByKenanCode(item.PackageID, "CP", "SP");
                        if (planId > 0)
                        {
                            respPlanid = planId;
                        }
                    }
                    if (Session["AccountType"] != null && Session["AccountType"].ToString() == "Suppline")
                    {
                        planId = proxy_kenan.GetLnkPgmBdlPkgCompIdByKenanCode(item.PackageID, "SP", "BP");
                        if (planId > 0)
                        {
                            respPlanid = planId;
                        }
                    }
                    if (Session["AccountType"] != null && Session["AccountType"].ToString() == "Primary" || Session["AccountType"] != null && Session["AccountType"].ToString() == "MISMPrimary")
                    {

                        planId = proxy_kenan.GetLnkPgmBdlPkgCompIdByKenanCode(item.PackageID, "CP", "BP");
                        if (planId > 0)
                        {
                            respPlanid = planId;
                        }
                    }

                   
                }
            }
            List<string> vasNames = new List<string>();
            if (Session[SessionKey.RegMobileReg_VasNames.ToString()] != null)
            {
                vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
            }
            if (respPlanid > 0)
                Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = respPlanid;
            List<ARVASPackageComponentModel> listPakcagecomponents = new List<ARVASPackageComponentModel>();
            ARVASPackageComponentModel compModel = null;
            List<ARVASComponents> vascomponents = new List<ARVASComponents>();
            ARVASComponents comp = null;
            string response = string.Empty;
            foreach (var vas in removedVases.Split(','))
            {
                if (vas != string.Empty)
                {



                    foreach (var item in retrievedPackages)
                    {
                        compModel = new ARVASPackageComponentModel();
                        vascomponents = new List<ARVASComponents>();
                        compModel.packageActiveDt = item.packageActiveDt;
                        compModel.PackageDesc = item.PackageDesc;
                        compModel.PackageID = item.PackageID;
                        compModel.packageInstId = item.packageInstId;
                        compModel.packageInstIdServ = item.packageInstIdServ;
                        ExistingKenanIDs.Add(item.PackageID);
                        using (var proxy_kenan = new RegistrationServiceProxy())
                        {
                            resp_existing = proxy_kenan.ExistingPackageDetails(item.PackageID.ToInt());

                            foreach (var component in item.compList)
                            {
                                if (component.componentId == vas)
                                {
                                    comp = new ARVASComponents();
                                    comp.componentActiveDt = component.componentActiveDt;
                                    comp.componentDesc = component.componentDesc;
                                    comp.componentId = component.componentId;
                                    comp.componentInstId = component.componentInstId;
                                    comp.componentInstIdServ = component.componentInstIdServ;
                                    comp.componentShortDisplay = component.componentShortDisplay;
                                    removedComponents.Add(component.componentId);
                                    if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] != null && resp_existing != null && resp_existing.ExistingPackageDetails != null)
                                    {
                                        if (resp_existing.ExistingPackageDetails.PlanType != null)
                                        {
                                            response = proxy_kenan.GetlnkpgmbdlpkgcompidForKenancomponent(resp_existing.ExistingPackageDetails.PlanType, Convert.ToInt32(item.PackageID), component.componentId.ToString());
                                            if (response != null && response.Contains(","))
                                            {
                                                comp.GroupId = response.Split(',')[2];
                                                comp.Price = response.Split(',')[1];
                                                comp.lnkpgmbdlpkgcompId = response.Split(',')[0];
                                            }
                                        }
                                    }
                                    vascomponents.Add(comp);

                                    //get dependency components for each of the unselected components and remove them
                                    using (var catProxy = new CatalogServiceProxy())
                                    {
                                        List<int> depCompIds = new List<int>();

                                        List<PgmBdlPckComponent> pgmComponents = new List<PgmBdlPckComponent>();
                                        if (comp.lnkpgmbdlpkgcompId != null)
                                        {
                                            var depcomp = new ARVASComponents();
                                            depCompIds = GetDepenedencyComponents(comp.lnkpgmbdlpkgcompId.ToString());
                                            //depCompIds = GetDepenedencyComponents("2191");
                                            if (depCompIds != null && depCompIds.Count > 0)
                                            {
                                                pgmComponents = MasterDataCache.Instance.FilterComponents(depCompIds).ToList();
                                                List<Component> comps = new List<Component>();
                                                comps = catProxy.ComponentGet(pgmComponents.Select(a => a.ChildID)).ToList();
                                                int i = 0;
                                                foreach (var dComp in pgmComponents)
                                                {
                                                    //commented to fix issue, when data component is removed its associtaed dependency component has to be removed
                                                    if (item.compList.Select(c => c.componentId).ToList().Contains(dComp.KenanCode))
                                                    {

                                                        if (comps != null && comps.Count > 0)
                                                        {
                                                            depcomp = new ARVASComponents();
                                                            depcomp.componentId = dComp.KenanCode;
                                                            depcomp.componentDesc = comps[i].Description;
                                                            depcomp.Price = dComp.Price.ToString();
                                                            if (!removedComponents.Contains(dComp.KenanCode))
                                                            {
                                                                vascomponents.Add(depcomp);
                                                                vasNames.Remove(depcomp.componentDesc + " (RM " + depcomp.Price + ")");
                                                                removedComponents.Add(dComp.KenanCode);
                                                            }
                                                        }

                                                    }

                                                    i++;

                                                }



                                            }
                                        }
                                    }
                                }
                            }
                            if (vascomponents != null && vascomponents.Count > 0)
                                compModel.compList = vascomponents;


                        }
                        if (vascomponents != null && vascomponents.Count > 0)
                            listPakcagecomponents.Add(compModel);
                    }
                }
            }

            Session[SessionKey.RegMobileReg_VasNames.ToString()] = vasNames;
            return listPakcagecomponents;
        }
        private DAL.Models.Registration ConstructRegistration(PersonalDetailsVM PersonalDetails,string queueNo, string remarks, string signatureSVG = "", string custPhoto = "", string altCustPhoto = "", string photo = "", bool isBREFail=false)
        {
            var regTypeID = 0;
            DAL.Models.Registration registration = new DAL.Models.Registration();
            //we set this always TRUE why because based on this we are storing the ApproveBlacklistCheck  in trnregistration this we are sending to kenan as sales code
            var isBlacklisted = true;
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            if (!ReferenceEquals(Session["FromMNP"], null))
            {
                regTypeID = 13;
            }
            else
            {
                if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan)

                    regTypeID = 13;
                else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.PlanOnly)

                    regTypeID = 13;
                else if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)

                    regTypeID = 13;
            }

            registration.K2_Status = Session["RegK2_Status"].ToString2();
            registration = new DAL.Models.Registration();
            registration.CenterOrgID = Util.SessionAccess.User.OrgID;
            registration.RegTypeID = regTypeID;
            //Added by Patanjali to support remarks on 30-03-2013
            registration.Remarks = remarks;
            //Added by Patanjali to support remarks on 30-03-2013 ends her
            registration.QueueNo = queueNo;
            registration.SignatureSVG = signatureSVG;
            //registration.CustomerPhoto = Uri.EscapeDataString(custPhoto);
            registration.CustomerPhoto = custPhoto;
            registration.AltCustomerPhoto = altCustPhoto;
            registration.Photo = photo;
            //registration.AdditionalCharges = personalDetailsVM.Deposite;
            registration.InternalBlacklisted = Session["RegMobileReg_IsBlacklisted"] != null ? (((bool)Session["RegMobileReg_IsBlacklisted"] == null) ? false : (bool)Session["RegMobileReg_IsBlacklisted"]) : true;
            registration.InternalBlacklisted = Session["RegMobileReg_IsBlacklisted"] != null ? (bool)Session["RegMobileReg_IsBlacklisted"] : true;
            registration.ExternalBlacklisted = Session["RegMobileReg_IsBlacklisted"] != null ? (((bool)Session["RegMobileReg_IsBlacklisted"] == null) ? false : (bool)Session["RegMobileReg_IsBlacklisted"]) : true;
            registration.ExternalBlacklisted = Session["RegMobileReg_IsBlacklisted"] != null ? (bool)Session["RegMobileReg_IsBlacklisted"] : true;

            // w.loon - Is Justification/Supervisor Approval Required
            string ListOfRequiredBreValidation = WebHelper.Instance.ListOfRequiredBreValidation()["ListOfRequiredBreValidation"];

            bool IsWriteOff = false;
            string acc = string.Empty;
            WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
            registration.WriteOffCheckStatus = ListOfRequiredBreValidation.Contains("Writeoff Check") ?
                                                    !IsWriteOff ? "WOS" : "WOF"
                                                    : "WON";
            registration.AgeCheckStatus = ListOfRequiredBreValidation.Contains("AgeCheck") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]) : "AF"
                                                    : "AN";
            registration.DDMFCheckStatus = ListOfRequiredBreValidation.Contains("DDMF Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]) : "DF"
                                                    : "DN";
            registration.AddressCheckStatus = ListOfRequiredBreValidation.Contains("Address Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]) : "ADF"
                                                    : "ADN";
            registration.OutStandingCheckStatus = ListOfRequiredBreValidation.Contains("Outstanding Credit Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()]) : "OF"
                                                    : "ON";
            registration.TotalLineCheckStatus = ListOfRequiredBreValidation.Contains("Total Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()]) : "TLF"
                                                    : "TLN";
            registration.PrincipleLineCheckStatus = ListOfRequiredBreValidation.Contains("Principle Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()]) : "PLF"
                                                    : "PLN";
            registration.BiometricVerify = ListOfRequiredBreValidation.Contains("Biometric") ?
                                                    (!ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? Session[SessionKey.RegMobileReg_BiometricVerify.ToString()].ToBool() : false)
                                                    : false;

            //added by Nreddy for MyKad
            registration.IsVerified = !ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? true : false;
            registration.ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
            registration.RFSalesDT = DateTime.Now;

            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
            {
                registration.SalesPerson = Request.Cookies["CookieUser"].Value;
            }


            if (Session[SessionKey.ExternalID.ToString()] != null)
                registration.MSISDN1 = Session[SessionKey.ExternalID.ToString()].ToString();

            registration.CreateDT = DateTime.Now;
            registration.LastAccessID = Util.SessionAccess.UserName;
            registration.Whitelist_Status = !ReferenceEquals(Session["MocandwhitelistSstatus"], null) ? Session["MocandwhitelistSstatus"].ToString() : "0";
            registration.MOC_Status = !ReferenceEquals(Session["MOCIDs"], null) ? Session["MOCIDs"].ToString() : string.Empty;
            registration.ContractCheckStatus = ListOfRequiredBreValidation.Contains("Contract Check") ?
                                                !ReferenceEquals(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()]) : "CF"
                                                : "CN";
            registration.UserType = Session[SessionKey.PPID.ToString()].ToString() == null ? "N" : Session[SessionKey.PPID.ToString()].ToString();

            registration.OutstandingAcs = Session["FailedAcctIds"] == null ? string.Empty : Session["FailedAcctIds"].ToString();
            registration.PrePortInCheckStatus = !ReferenceEquals(Session["RegMobileReg_IsPortInStatusCode"], null) ? Convert.ToString(Session["RegMobileReg_IsPortInStatusCode"]) : "MPPF";
            registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session["RegMobileReg_IsMNPServiceIdCheckStatusCode"], null) ? Convert.ToString(Session["RegMobileReg_IsMNPServiceIdCheckStatusCode"]) : "MSIF";
            registration.ArticleID = !ReferenceEquals(Session["ArticleId"], null) ? Convert.ToString(Session["ArticleId"]) : string.Empty;
            registration.KenanAccountNo = Session[SessionKey.KenanACNumber.ToString()].ToString() != null ? Session[SessionKey.KenanACNumber.ToString()].ToString() : string.Empty;
            //registration.fxAcctNo = Session["FxAccNo"].ToString() != null ? Session["FxAccNo"].ToString() : string.Empty;
			if (!ReferenceEquals(Session["AccountType"], null) && Equals(Session["AccountType"], "Suppline"))
			{
				#region bug 821
				// Gerry - recheck for FxAccNo for suppline vs standalone suppline.
				// since standalone suppline have its own acccount, so need to send the acccount number instead of the internal account No.
				//if (WebHelper.Instance.determineStandAloneSuppline(Session[SessionKey.ExternalID.ToString()].ToString()))
				//{
				//    registration.fxAcctNo = Session[SessionKey.KenanACNumber.ToString()].ToString() != null ? Session[SessionKey.KenanACNumber.ToString()].ToString() : string.Empty;
				//}
				//else
				//{
				//    registration.fxAcctNo = Session["FxAccNo"].ToString() != null ? Session["FxAccNo"].ToString() : string.Empty;
				//}
				// 
				#endregion
				
				registration.fxAcctNo = Session["FxAccNo"].ToString() != null ? Session["FxAccNo"].ToString() : string.Empty;
			}
			else
			{
				// Primary, MISMPrimary, MISMSec should come here.
				registration.fxAcctNo = Session["FxAccNo"].ToString() != null ? Session["FxAccNo"].ToString() : string.Empty;
			}
            registration.fxSubscrNo = Session["fxSuscriberNo"].ToString() != null ? Session["fxSuscriberNo"].ToString() : string.Empty;
            registration.fxSubScrNoResets = Session["fxSuscriberNoResets"].ToString() != null ? Session["fxSuscriberNoResets"].ToString() : string.Empty;
            registration.upfrontPayment = !ReferenceEquals(Session["RegMobileReg_UpfrontPayment"], null) ? Convert.ToDecimal(Session["RegMobileReg_UpfrontPayment"]) : 0;
            if (Session["RegMobileReg_UOMCode"] != null)
            {
                registration.UOMCode = Session["RegMobileReg_UOMCode"].ToString();

            }
            else
            {
                registration.UOMCode = string.Empty;
            }
            // For plan setting UOM code and offerid to generic values -- By Patanjali on 29-04-2013
            if (registration.RegTypeID != 2)
            {
                registration.OfferID = 0;
                registration.UOMCode = "EA";
            }

            registration.MOC_Status = !ReferenceEquals(Session["MOCIDs"], null) ? Session["MOCIDs"].ToString() : "No";
            registration.Whitelist_Status = !ReferenceEquals(Session["MocandwhitelistSstatus"], null) ? Session["MocandwhitelistSstatus"].ToString() : "No";
            registration.OrganisationId = Util.SessionAccess.User.Org.OrganisationId;

            registration.Trn_Type = ConfigurationManager.AppSettings["Trn_Type"];
            // registration.OfferID = 0;
            registration.IMPOSStatus = 0;
            registration.SimModelId = 0;
            registration.RegTypeID = 13;

            //Code Added by VLT Check for International Roaming
            if (Session[SessionKey.RegMobileReg_VasNames.ToString()] != null)
            {
                var vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
                string strVasArray = string.Join(",", vasNames.ToArray());
                if (strVasArray.Contains(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["InternationalRoaming"])) == true)
                {
                    registration.InternationalRoaming = true;
                }
            }

            //End of International Roaming Check

            return registration;
        }
        private Customer ConstructCustomer()
        {
            var cust = new Customer();
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            if (personalDetailsVM != null && personalDetailsVM.Customer != null)
            {
                personalDetailsVM.Customer.PayModeID = 1;
                personalDetailsVM.Customer.LanguageID = 1;
                personalDetailsVM.Customer.EmailAddr = string.Empty;
                personalDetailsVM.Customer.RaceID = 4;


                if (personalDetailsVM.DOBYear > 0 && personalDetailsVM.DOBMonth <= 12 && personalDetailsVM.DOBDay <= 31)
                {
                    personalDetailsVM.Customer.DateOfBirth = Convert.ToDateTime(personalDetailsVM.DOBYear + "-" + personalDetailsVM.DOBMonth + "-" + personalDetailsVM.DOBDay);
                }
                else
                {
                    if (personalDetailsVM.Customer.DateOfBirth.Equals(DateTime.MinValue))
                        personalDetailsVM.Customer.DateOfBirth = DateTime.MinValue;

                }


                int CustomerTitleIDfromConfig = ConfigurationManager.AppSettings["CustomerTitleID"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["CustomerTitleID"]) : 0;
                if (personalDetailsVM.Customer.CustomerTitleID <= 0)
                {
                    personalDetailsVM.Customer.CustomerTitleID = CustomerTitleIDfromConfig;
                }
                if (string.IsNullOrEmpty(personalDetailsVM.Customer.ContactNo))
                {
                    personalDetailsVM.Customer.ContactNo = "60123456789";
                }
                else if (personalDetailsVM.Customer.ContactNo.Length == 10 && personalDetailsVM.Customer.ContactNo.Substring(0, 2) != "60")
                    personalDetailsVM.Customer.ContactNo = "6" + personalDetailsVM.Customer.ContactNo;
                cust = new Customer()
                {
                    FullName = personalDetailsVM.Customer.FullName,
                    Gender = personalDetailsVM.Customer.Gender,
                    DateOfBirth = personalDetailsVM.Customer.DateOfBirth,
                    CustomerTitleID = personalDetailsVM.Customer.CustomerTitleID,
                    RaceID = personalDetailsVM.Customer.RaceID,
                    LanguageID = personalDetailsVM.Customer.LanguageID,
                    NationalityID = personalDetailsVM.Customer.NationalityID,
                    IDCardTypeID = personalDetailsVM.Customer.IDCardTypeID,
                    IDCardNo = personalDetailsVM.Customer.IDCardNo,
                    ContactNo = personalDetailsVM.Customer.ContactNo,
                    AlternateContactNo = personalDetailsVM.Customer.AlternateContactNo,
                    EmailAddr = personalDetailsVM.Customer.EmailAddr,
                    PayModeID = personalDetailsVM.Customer.PayModeID,
                    CardTypeID = personalDetailsVM.Customer.CardTypeID,
                    NameOnCard = personalDetailsVM.Customer.NameOnCard,
                    CardNo = personalDetailsVM.Customer.CardNo,
                    CardExpiryDate = personalDetailsVM.Customer.CardExpiryDate,
                    IsVIP = Session["isVIP_Masking"].ToBool() ? 1 : 0,
                    Active = true,
                    LastAccessID = Util.SessionAccess.UserName,
                    CreateDT = DateTime.Now
                };
            }
            return cust;
        }
        private List<Address> ConstructRegAddress()
        {
            var personalDetailsVM = new PersonalDetailsVM();
            if (Session["RegMobileReg_PersonalDetailsVM"] != null)
                personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            //Added by KD for Language,email,Race
            personalDetailsVM.Customer.LanguageID = 1;
            personalDetailsVM.Customer.EmailAddr = string.Empty;
            personalDetailsVM.Customer.RaceID = 4;
            return WebHelper.Instance.ConstructRegAddress(personalDetailsVM);
        }
        private ValueAddedServicesVM GetAvailablePackageComponents(int pbpcID, bool fromSubline = false, bool isMandatory = false)
        {
            var vasVM = new ValueAddedServicesVM();
            var retrievedVases = new List<AvailableVAS>();
            var contractVases = new List<AvailableVAS>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var components = new List<Component>();
            var componentTypes = new List<ComponentType>();
            using (var proxy = new CatalogServiceProxy())
            {
                var packageID = (pbpcID != null && pbpcID != 0) ? ((MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().ChildID) != null ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().ChildID : 0) : 0;
                if (packageID != 0)
                {
                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            LinkType = Properties.Settings.Default.Package_Component,
                            ParentID = packageID,
                        },
                        IsMandatory = isMandatory,
                        Active = true
                    })).ToList();
                }
                if (pgmBdlPckComponents.Count() > 0)
                {
                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();

                    componentTypes = proxy.ComponentTypeGet(components.Select(a => a.ComponentTypeID).Distinct()).ToList();
                }
            }
            foreach (var pbpc in pgmBdlPckComponents)
            {
                var component = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault();
                var compTypeCode = componentTypes.Where(a => a.ID == component.ComponentTypeID).SingleOrDefault().Code;

                if (compTypeCode == Properties.Settings.Default.CompType_DataCon || compTypeCode == Properties.Settings.Default.CompType_VoiceCon)
                {
                    contractVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
                        KenanCode = pbpc.KenanCode,
                        RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false

                    });
                }
                else
                {
                    retrievedVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        KenanCode = pbpc.KenanCode,
                        RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false
                    });
                }
            }

            if (isMandatory)
            {
                vasVM.MandatoryVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }
            else
            {
                vasVM.NonMandatoryVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }

            //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
            if (Session["ArticleId"] != null)
            {
                if (contractVases != null && contractVases.Count > 0)
                {
                    string MOCStatus = !ReferenceEquals(Session["MOCIDs"], null) ? Session["MOCIDs"].ToString() : string.Empty;

                    List<string> objContracts = new List<string>();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var kenanPackageID = (pbpcID != null && pbpcID != 0) ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().KenanCode : string.Empty;

                        objContracts = proxy.GetContractsForArticleId(Session["ArticleId"].ToString(), kenanPackageID, MOCStatus);

                    }
                    if (objContracts != null && objContracts.Count > 0)
                        vasVM.ContractVASes = contractVases.Where(a => string.Join(",", objContracts).Contains(a.KenanCode)).ToList();

                }
                else
                    vasVM.ContractVASes = contractVases;
            }
            else
                vasVM.ContractVASes = contractVases;

            vasVM.SelectedContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2();
            vasVM.SelectedVasIDs = fromSubline ? Session["RegMobileSub_VasIDs"].ToString2() : Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2();

            return vasVM;
        }
        private ValueAddedServicesVM GetAvailablePackageComponents_PC_MC_DC(int pbpcID, string listtype, bool fromSubline = false, bool isMandatory = false)
        {
            var vasVM = new ValueAddedServicesVM();
            var retrievedVases = new List<AvailableVAS>();
            var contractVases = new List<AvailableVAS>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var components = new List<Component>();
            var componentTypes = new List<ComponentType>();


            int packageID = 0;
            using (var proxy = new CatalogServiceProxy())
            {

                packageID = (pbpcID != null) ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().ChildID : 0;

                if (packageID != 0)
                {

                    pgmBdlPckComponents.AddRange(MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            LinkType = listtype,
                            ParentID = packageID,
                        },
                        IsMandatory = isMandatory,
                        Active = true
                    })).ToList());



                }
                if (pgmBdlPckComponents.Count() > 0)
                {
                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();

                    componentTypes = proxy.ComponentTypeGet(components.Select(a => a.ComponentTypeID).Distinct()).ToList();
                }
            }


            foreach (var pbpc in pgmBdlPckComponents)
            {
                var component = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault();
                var compTypeCode = componentTypes.Where(a => a.ID == component.ComponentTypeID).SingleOrDefault().Code;

                if (compTypeCode == Properties.Settings.Default.CompType_DataCon || compTypeCode == Properties.Settings.Default.CompType_VoiceCon)
                {
                    contractVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
                        KenanCode = pbpc.KenanCode,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        GroupId = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().GroupId,
                        GroupName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Contains("╚") ? components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Split('╚')[1] : "",
                        RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false
                    });
                }
                else
                {
                    retrievedVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        KenanCode = pbpc.KenanCode,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        GroupId = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().GroupId,
                        GroupName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Contains("╚") ? components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Split('╚')[1] : "",
                        RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false
                    });
                }
            }

            if (isMandatory)
            {
                vasVM.MandatoryVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }
            else
            {
                vasVM.AvailableVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }

            //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
            if (Session["ArticleId"] != null)
            {
                if (contractVases != null && contractVases.Count > 0)
                {
                    string MOCStatus = !ReferenceEquals(Session["MOCIDs"], null) ? Session["MOCIDs"].ToString() : string.Empty;

                    List<string> objContracts = new List<string>();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var kenanPackageID = (pbpcID != null && pbpcID != 0) ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().KenanCode : string.Empty;

                        objContracts = proxy.GetContractsForArticleId(Session["ArticleId"].ToString(), kenanPackageID, MOCStatus);

                    }
                    if (objContracts != null && objContracts.Count > 0)
                        vasVM.ContractVASes = contractVases.Where(a => string.Join(",", objContracts).Contains(a.KenanCode)).ToList();

                }
                else
                    vasVM.ContractVASes = contractVases;
            }
            else
                vasVM.ContractVASes = contractVases;
            //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid



            vasVM.SelectedContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2();
            vasVM.SelectedVasIDs = fromSubline ? Session["RegMobileSub_VasIDs"].ToString2() : Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2();

            return vasVM;
        }
        #region Old Summary Method

        private OrderSummaryVM ConstructOrderSummary(bool fromSubline = false)
        {
            string strDataPlanID;
            string strContractId;
            var components = new List<Component>();
            var brandArticleImage = new BrandArticle();
            var package = new Package();
            var bundle = new Bundle();
            var orderVM = new OrderSummaryVM();
            var phoneVM = (PhoneVM)Session["RegMobileReg_PhoneVM"];
            int varDataPlanId = 0;
            var personalDetailsVM = Session["RegMobileReg_PersonalDetailsVM"] == null ? new PersonalDetailsVM() : (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            if (personalDetailsVM.RegID == 0)
                WebHelper.Instance.LoadLiberizationWaiveOffRules(this.GetType().Name);
            // Main Line
            if (!fromSubline)
            {
                if ((OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()] != null)
                    orderVM = (OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()];
                //added by deepika 10 march
                if (Session["RegMobileReg_DataPkgID"] != null)
                {

                    strDataPlanID = Session["RegMobileReg_DataPkgID"].ToString();

                    if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] != null) && (Session[SessionKey.MobileRegType.ToString()] != "Planonly"))
                    {
                        strContractId = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString();

                        using (var proxys = new CatalogServiceProxy())

                            varDataPlanId = proxys.GettllnkContractDataPlan(Convert.ToInt32(strContractId));


                        //added by deepika 10 march
                        if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID != 0 && strContractId.ToInt() != 0 && varDataPlanId.ToInt() != 0 || varDataPlanId.ToInt() == 0)
                        {
                            using (var proxy = new CatalogServiceProxy())
                            {
                                List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
                                varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, strContractId.ToInt(), varDataPlanId.ToInt() }).ToList();
                                for (int i = 0; i < varAdvDeposit.Count; i++)
                                    if (varAdvDeposit[i] != null && varAdvDeposit[i].modelId != 0)
                                    {
                                        var Malaytotalprice = varAdvDeposit[i].malyDevAdv + varAdvDeposit[i].malayPlanAdv + varAdvDeposit[i].malayDevDeposit + varAdvDeposit[i].malyPlanDeposit;
                                        var Othertotalprice = varAdvDeposit[i].othDevAdv + varAdvDeposit[i].othPlanAdv + varAdvDeposit[i].othDevDeposit + varAdvDeposit[i].othPlanDeposit;
                                        orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
                                        orderVM.MalayAdvDevPrice = Malaytotalprice;
                                        orderVM.OthersAdvDevPrice = Othertotalprice;
                                        Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                        Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

                                        /*Added by chetan for split adv & deposit*/
                                        orderVM.MalyDevAdv = varAdvDeposit[i].malyDevAdv;
                                        orderVM.MalayPlanAdv = varAdvDeposit[i].malayPlanAdv;
                                        orderVM.MalayDevDeposit = varAdvDeposit[i].malayDevDeposit;
                                        orderVM.MalyPlanDeposit = varAdvDeposit[i].malyPlanDeposit;
                                        orderVM.OthDevAdv = varAdvDeposit[i].othDevAdv;
                                        orderVM.OthPlanAdv = varAdvDeposit[i].othPlanAdv;
                                        orderVM.OthDevDeposit = varAdvDeposit[i].othDevDeposit;
                                        orderVM.OthPlanDeposit = varAdvDeposit[i].othPlanDeposit;

                                        Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                        Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                        Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                        Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                        Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                        Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                        Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                        Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                        /*up to here*/

                                        if (personalDetailsVM.Customer.NationalityID != 0)
                                        {
                                            if (personalDetailsVM.Customer.NationalityID == 1)
                                            {
                                                /*Added by chetan for split adv & deposit*/
                                                Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                                Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                                Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                                Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                                /*up to here*/
                                                Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                                orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
                                            }
                                            else
                                            {
                                                /*Added by chetan for split adv & deposit*/
                                                Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                                Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                                Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                                Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                                /*up to here*/
                                                Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
                                                orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
                                            }
                                        }
                                    }
                            }
                        }

                    }
                    else if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] == null) && (Session[SessionKey.MobileRegType.ToString()].ToString2().ToUpper() == "PLANONLY"))
                    {
                        //added by deepika 10 march
                        if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID == 0 && orderVM.ContractID == 0)
                        {
                            using (var proxy = new CatalogServiceProxy())
                            {
                                var varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, orderVM.ContractID, 0 }).SingleOrDefault();
                                if (varAdvDeposit != null)
                                {
                                    var Malaytotalprice = varAdvDeposit.malyDevAdv + varAdvDeposit.malayPlanAdv + varAdvDeposit.malayDevDeposit + varAdvDeposit.malyPlanDeposit;
                                    var Othertotalprice = varAdvDeposit.othDevAdv + varAdvDeposit.othPlanAdv + varAdvDeposit.othDevDeposit + varAdvDeposit.othPlanDeposit;
                                    orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
                                    orderVM.MalayAdvDevPrice = Malaytotalprice;
                                    orderVM.OthersAdvDevPrice = Othertotalprice;
                                    Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                    Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

                                    /*Added by chetan for split adv & deposit*/
                                    orderVM.MalyDevAdv = varAdvDeposit.malyDevAdv;
                                    orderVM.MalayPlanAdv = varAdvDeposit.malayPlanAdv;
                                    orderVM.MalayDevDeposit = varAdvDeposit.malayDevDeposit;
                                    orderVM.MalyPlanDeposit = varAdvDeposit.malyPlanDeposit;
                                    orderVM.OthDevAdv = varAdvDeposit.othDevAdv;
                                    orderVM.OthPlanAdv = varAdvDeposit.othPlanAdv;
                                    orderVM.OthDevDeposit = varAdvDeposit.othDevDeposit;
                                    orderVM.OthPlanDeposit = varAdvDeposit.othPlanDeposit;

                                    Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                    Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                    Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                    Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                    Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                    Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                    Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                    Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                    /*up to here*/

                                    if (personalDetailsVM.Customer.NationalityID != 0)
                                    {
                                        if (personalDetailsVM.Customer.NationalityID == 1)
                                        {
                                            /*Added by chetan for split adv & deposit*/
                                            Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                            Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                            Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                            Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                            /*up to here*/
                                            Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                            orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
                                        }
                                        else
                                        {
                                            /*Added by chetan for split adv & deposit*/
                                            Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                            Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                            Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                            Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                            /*up to here*/
                                            Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
                                            orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                else
                {
                    strDataPlanID = "0";
                    if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] != null) && (Session[SessionKey.MobileRegType.ToString()] != "Planonly"))
                    {
                        strContractId = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString();

                        using (var proxys = new CatalogServiceProxy())

                            varDataPlanId = proxys.GettllnkContractDataPlan(Convert.ToInt32(strContractId));


                        //added by deepika 10 march
                        if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID != 0 && strContractId.ToInt() != 0 && varDataPlanId.ToInt() != 0 || varDataPlanId.ToInt() == 0)
                        {
                            using (var proxy = new CatalogServiceProxy())
                            {
                                List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
                                varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, strContractId.ToInt(), varDataPlanId.ToInt() }).ToList();
                                for (int i = 0; i < varAdvDeposit.Count; i++)
                                    if (varAdvDeposit[i] != null && varAdvDeposit[i].modelId != 0)
                                    {
                                        var Malaytotalprice = varAdvDeposit[i].malyDevAdv + varAdvDeposit[i].malayPlanAdv + varAdvDeposit[i].malayDevDeposit + varAdvDeposit[i].malyPlanDeposit;
                                        var Othertotalprice = varAdvDeposit[i].othDevAdv + varAdvDeposit[i].othPlanAdv + varAdvDeposit[i].othDevDeposit + varAdvDeposit[i].othPlanDeposit;
                                        orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
                                        orderVM.MalayAdvDevPrice = Malaytotalprice;
                                        orderVM.OthersAdvDevPrice = Othertotalprice;
                                        Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                        Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

                                        /*Added by chetan for split adv & deposit*/
                                        orderVM.MalyDevAdv = varAdvDeposit[i].malyDevAdv;
                                        orderVM.MalayPlanAdv = varAdvDeposit[i].malayPlanAdv;
                                        orderVM.MalayDevDeposit = varAdvDeposit[i].malayDevDeposit;
                                        orderVM.MalyPlanDeposit = varAdvDeposit[i].malyPlanDeposit;
                                        orderVM.OthDevAdv = varAdvDeposit[i].othDevAdv;
                                        orderVM.OthPlanAdv = varAdvDeposit[i].othPlanAdv;
                                        orderVM.OthDevDeposit = varAdvDeposit[i].othDevDeposit;
                                        orderVM.OthPlanDeposit = varAdvDeposit[i].othPlanDeposit;

                                        Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                        Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                        Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                        Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                        Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                        Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                        Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                        Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                        /*up to here*/

                                        if (personalDetailsVM.Customer.NationalityID != 0)
                                        {
                                            if (personalDetailsVM.Customer.NationalityID == 1)
                                            {
                                                /*Added by chetan for split adv & deposit*/
                                                Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                                Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                                Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                                Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                                /*up to here*/
                                                Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                                orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
                                            }
                                            else
                                            {
                                                /*Added by chetan for split adv & deposit*/
                                                Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                                Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                                Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                                Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                                /*up to here*/
                                                Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
                                                orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
                                            }
                                        }
                                    }
                            }
                        }

                    }
                    else if ((Session[SessionKey.RegMobileReg_ContractID.ToString()] == null) && (Session[SessionKey.MobileRegType.ToString()].ToString2().ToUpper() == "PLANONLY"))
                    {
                        //added by deepika 10 march
                        if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID == 0 && orderVM.ContractID == 0)
                        {
                            using (var proxy = new CatalogServiceProxy())
                            {
                                var varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, orderVM.ContractID, 0 }).SingleOrDefault();
                                if (varAdvDeposit != null)
                                {
                                    var Malaytotalprice = varAdvDeposit.malyDevAdv + varAdvDeposit.malayPlanAdv + varAdvDeposit.malayDevDeposit + varAdvDeposit.malyPlanDeposit;
                                    var Othertotalprice = varAdvDeposit.othDevAdv + varAdvDeposit.othPlanAdv + varAdvDeposit.othDevDeposit + varAdvDeposit.othPlanDeposit;
                                    orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
                                    orderVM.MalayAdvDevPrice = Malaytotalprice;
                                    orderVM.OthersAdvDevPrice = Othertotalprice;
                                    Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                    Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;

                                    /*Added by chetan for split adv & deposit*/
                                    orderVM.MalyDevAdv = varAdvDeposit.malyDevAdv;
                                    orderVM.MalayPlanAdv = varAdvDeposit.malayPlanAdv;
                                    orderVM.MalayDevDeposit = varAdvDeposit.malayDevDeposit;
                                    orderVM.MalyPlanDeposit = varAdvDeposit.malyPlanDeposit;
                                    orderVM.OthDevAdv = varAdvDeposit.othDevAdv;
                                    orderVM.OthPlanAdv = varAdvDeposit.othPlanAdv;
                                    orderVM.OthDevDeposit = varAdvDeposit.othDevDeposit;
                                    orderVM.OthPlanDeposit = varAdvDeposit.othPlanDeposit;

                                    Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                    Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                    Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                    Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                    Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                    Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                    Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                    Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                    /*up to here*/

                                    if (personalDetailsVM.Customer.NationalityID != 0)
                                    {
                                        if (personalDetailsVM.Customer.NationalityID == 1)
                                        {
                                            /*Added by chetan for split adv & deposit*/
                                            Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = orderVM.MalyDevAdv;
                                            Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = orderVM.MalayPlanAdv;
                                            Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = orderVM.MalayDevDeposit;
                                            Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = orderVM.MalyPlanDeposit;
                                            /*up to here*/
                                            Session[SessionKey.RegMobileReg_MalAdvDeposit.ToString()] = orderVM.MalayAdvDevPrice;
                                            orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
                                        }
                                        else
                                        {
                                            /*Added by chetan for split adv & deposit*/
                                            Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = orderVM.OthDevAdv;
                                            Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = orderVM.OthPlanAdv;
                                            Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = orderVM.OthDevDeposit;
                                            Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = orderVM.OthPlanDeposit;
                                            /*up to here*/
                                            Session[SessionKey.RegMobileReg_OtherAdvDeposit.ToString()] = orderVM.OthersAdvDevPrice;
                                            orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                // END Deepika


                // Device Details
                if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DevicePlan || Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                {
                    if (Session["GuidedSales_Type"].ToInt() == MobileRegType.DevicePlan.ToInt()) // go to credit check
                    {
                        Session["RegMobileReg_SelectedModelImageID"] = Session["GuidedSales_SelectedModelImageID"];
                    }

                    if (Session["RegMobileReg_SelectedModelImageID"].ToInt() != orderVM.SelectedModelImageID)
                    {

                        orderVM.SelectedModelImageID = Session["RegMobileReg_SelectedModelImageID"].ToInt();

                        using (var proxy = new CatalogServiceProxy())
                        {
                            brandArticleImage = proxy.BrandArticleImageGet(orderVM.SelectedModelImageID);
                        }

                        Session["RegMobileReg_SelectedModelID"] = brandArticleImage.ModelID;
                        MOCOfferDetails offers = new MOCOfferDetails();
                        var model = new Model();
                        using (var proxy = new CatalogServiceProxy())
                        {

                            model = proxy.ModelGet(brandArticleImage.ModelID);
                        }


                        using (var proxy = new CatalogServiceProxy())
                        {

                            model = proxy.ModelGet(brandArticleImage.ModelID);
                        }

                        orderVM.ModelImageUrl = brandArticleImage.ImagePath;
                        orderVM.ModelID = model.ID;
                        orderVM.ModelName = model.Name;
                        orderVM.ColourName = brandArticleImage.ColourID > 0 ? Util.GetNameByID(RefType.Colour, brandArticleImage.ColourID) : "";
                        orderVM.BrandID = model.BrandID;
                        orderVM.BrandName = Util.GetNameByID(RefType.Brand, model.BrandID);
                        orderVM.ArticleId = brandArticleImage.ArticleID.ToString().Trim();
                        Session["ArticleId"] = orderVM.ArticleId.ToString().Trim();

                        if (personalDetailsVM.RegID > 0)
                        {
                            orderVM.TotalPrice = Convert.ToDecimal(Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()]);
                            orderVM.ModelPrice = Convert.ToDecimal(Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()]);
                            orderVM.ItemPrice = Convert.ToInt32(Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()]);
                        }
                        else
                        {
                            orderVM.DevicePriceType = DevicePriceType.RetailPrice;
                            orderVM.TotalPrice -= orderVM.ModelPrice;
                            orderVM.TotalPrice += model.RetailPrice.ToInt();
                            orderVM.ModelPrice = model.RetailPrice.ToInt();

                            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = orderVM.ModelPrice;
                        }

                        if (Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] != null)
                        {
                            orderVM.TotalPrice = Convert.ToDecimal(Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()]);
                            orderVM.ModelPrice = Convert.ToDecimal(Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()]);
                            orderVM.ItemPrice = Convert.ToInt32(Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()]);
                            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = orderVM.ModelPrice;

                        }
                    }
                }
            }
            else //Sub Line
            {
                if ((OrderSummaryVM)Session["RegMobileReg_SublineSummary"] != null)
                    orderVM = (OrderSummaryVM)Session["RegMobileReg_SublineSummary"];
            }
            MOCOfferDetails offer = new MOCOfferDetails();
            using (var proxy = new CatalogServiceProxy())
            {

                if (Session[SessionKey.OfferId.ToString()] != null)
                {
                    int offerId = Convert.ToInt32(Session[SessionKey.OfferId.ToString()]);
                    if (offerId > 0)
                    {
                        var respOffers = proxy.GETMOCOfferDetails(offerId);

                        if (respOffers != null)
                        {
                            offer.DiscountAmount = respOffers.DiscountAmount;
                            offer.ID = respOffers.ID;
                            offer.Name = respOffers.Name;
                            offer.OfferDescription = respOffers.OfferDescription;

                        }


                    }
                }
            }
            List<MOCOfferDetails> listOffers = new List<MOCOfferDetails>();
            if (offer != null && offer.ID > 0)
            {
                if (orderVM != null)
                {
                    //orderVM.Offers = null;
                    listOffers.Add(offer);
                    orderVM.Offers = listOffers;
                }

            }

            //Package Details
            var sessionPkgPgmBdlPkgCompID = fromSubline ? Session["RegMobileSub_PkgPgmBdlPkgCompID"].ToInt() : Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(); //Main or Sub Line
            if (sessionPkgPgmBdlPkgCompID != orderVM.SelectedPgmBdlPkgCompID)
            {
                var bundlePackage = new PgmBdlPckComponent();
                orderVM.SelectedPgmBdlPkgCompID = sessionPkgPgmBdlPkgCompID;

                // Package Details
                if (sessionPkgPgmBdlPkgCompID != 0)
                {
                    using (var proxy = new CatalogServiceProxy())
                    {
                        bundlePackage = MasterDataCache.Instance.FilterComponents(new int[] 
                    { 
                        sessionPkgPgmBdlPkgCompID 
                    }).SingleOrDefault();

                        package = proxy.PackageGet(new int[] 
                    { 
                        bundlePackage.ChildID 
                    }).SingleOrDefault();

                        bundle = proxy.BundleGet(new int[] 
                    { 
                        bundlePackage.ParentID 
                    }).SingleOrDefault();
                    }
                }

                orderVM.PlanBundle = bundle.Name;
                orderVM.PlanName = package.Name;

                // Plan Monthly Subscription
                if (orderVM.MonthlySubscription != bundlePackage.Price)
                {
                    orderVM.MonthlySubscription = bundlePackage.Price;
                }

                // Mandatory Package Components
                if (sessionPkgPgmBdlPkgCompID > 0)
                    orderVM.VasVM.MandatoryVASes = GetAvailablePackageComponents(orderVM.SelectedPgmBdlPkgCompID, isMandatory: true).MandatoryVASes;

                else
                    orderVM.VasVM = new ValueAddedServicesVM();
            }

            //VAS details
            if (!fromSubline)
            {
                var selectedVasIDs = Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2(); //Main or Sub Line
                var selectedCompIDs = new List<int>();

                // Mandatory VASes
                if (orderVM.VasVM.MandatoryVASes.Count() > 0)
                {
                    selectedCompIDs = orderVM.VasVM.MandatoryVASes.Select(a => a.PgmBdlPkgCompID).ToList();
                }

                if (orderVM.VasVM.SelectedVasIDs != selectedVasIDs || orderVM.VasVM.MandatoryVASes.Count() > 0)
                {
                    orderVM.MonthlySubscription = 0;
                    decimal vasPrice = 0;
                    orderVM.ContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt();
                    orderVM.DataPlanID = Session["RegMobileReg_DataplanID"].ToInt();
                    orderVM.VasVM.SelectedVasIDs = selectedVasIDs;


                    var vasIDs = orderVM.VasVM.SelectedVasIDs.Split(',').ToList();
                    foreach (var id in vasIDs)
                    {
                        if (!string.IsNullOrEmpty(id))
                        {
                            selectedCompIDs.Add(id.ToInt());
                        }
                    }

                    if (Session["RegMobileReg_DataplanID"] != null && Session["RegMobileReg_DataplanID"].ToInt() != 0)
                    {

                        string depcompIds = "";
                        List<string> depListCompIds = new List<string>();
                        depListCompIds = depcompIds.Split(',').ToList();

                        foreach (string compid in depListCompIds)
                        {

                            if (compid != string.Empty)
                                selectedCompIDs.Add(Convert.ToInt32(compid));

                        }
                    }


                    if (selectedCompIDs != null)
                    {
                        if (selectedCompIDs.Count() > 0)
                        {
                            using (var proxy = new CatalogServiceProxy())
                            {
                                var vases = MasterDataCache.Instance.FilterComponents
                                   (
                                       selectedCompIDs
                                   ).ToList();

                                components = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();

                                components = components.Where(a => a.RestricttoKenan == false).ToList();

                                var query = from selectedVases in vases
                                            join comps in components on
                                                selectedVases.ChildID equals comps.ID
                                            select new { name = comps.Name + " (RM" + selectedVases.Price + ")" };
                                /*Distinct condition added by chetan*/
                                orderVM.VasVM.VASesName = query.Select(a => a.name).Distinct().ToList();

                                /*Added by chetan for vasplan price adding*/

                                if ((!string.IsNullOrEmpty(selectedVasIDs)) || (orderVM.VasVM.SelectedVasIDs != selectedVasIDs) || (orderVM.VasVM.VASesName.Count > 0))
                                {
                                    var query1 = from selectedVases in vases
                                                 join comps in components on
                                                    selectedVases.ChildID equals comps.ID
                                                 select new { selectedVases.Price };
                                    if (query1 != null)
                                    {
                                        orderVM.MonthlySubscription2 = query1.Select(b => b.Price).ToList();
                                        for (int i = 0; i < orderVM.MonthlySubscription2.Count; i++)
                                        {
                                            vasPrice += orderVM.MonthlySubscription2[i];
                                        }
                                    }

                                }

                                /*Upto here*/
                            }
                        }
                    }

                    orderVM.MonthlySubscription += vasPrice;
                    if (orderVM.ContractID != 0)
                    {

                        CatalogServiceProxy objCSproxy = new CatalogServiceProxy();

                        if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToString() != null)
                        {
                            var deviceproxys = objCSproxy.GetDiscountPriceDetails(orderVM.ModelID, Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), Convert.ToInt32(orderVM.ContractID), varDataPlanId);
                            if (deviceproxys.NewPlanName != null)
                            {
                                orderVM.DiscountPrice = deviceproxys.DiscountPrice.ToInt();
                                orderVM.PlanName = deviceproxys.NewPlanName;
                                orderVM.MonthlySubscription = orderVM.MonthlySubscription - orderVM.DiscountPrice;
                            }
                        }

                    }


                }
            }

            // Advance Payment/ Deposite
            if (orderVM.Deposite != personalDetailsVM.Deposite)
            {
                orderVM.TotalPrice -= orderVM.Deposite;
                orderVM.TotalPrice += personalDetailsVM.Deposite;

                orderVM.Deposite = personalDetailsVM.Deposite;
            }


            // Device Price (cheaper)
            if (Session[SessionKey.RegMobileReg_ContractID.ToString()] != null && orderVM.SelectedPgmBdlPkgCompID.ToInt() != 0 && orderVM.ModelID != 0)
            {
                if (orderVM.ItemPrice != orderVM.ModelPrice)
                {
                    var itemPrice = new ItemPrice();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var itemPriceID = proxy.ItemPriceFind(new ItemPriceFind()
                        {
                            ItemPrice = new ItemPrice()
                            {
                                BdlPkgID = orderVM.SelectedPgmBdlPkgCompID.ToInt(),
                                PkgCompID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt(),
                                ModelID = orderVM.ModelID,
                                pkgDataID = Session["RegMobileReg_DataPkgID"].ToInt()
                            },
                            Active = true
                        }).SingleOrDefault();

                        if (personalDetailsVM.RegID == 0)
                        {
                            if (itemPriceID.ToInt() != 0) // item price configured
                            {
                                itemPrice = proxy.ItemPriceGet(itemPriceID);

                                if (orderVM.DevicePriceType == DevicePriceType.RetailPrice)
                                {
                                    orderVM.TotalPrice -= orderVM.ModelPrice;
                                }
                                else if (orderVM.DevicePriceType == DevicePriceType.ItemPrice)
                                {
                                    orderVM.TotalPrice -= orderVM.ItemPrice;
                                }

                                if (itemPrice.Price == 0) // RM 0 price for device
                                {
                                    orderVM.DevicePriceType = DevicePriceType.NoPrice;
                                }
                                else // cheaper price for device
                                {
                                    orderVM.DevicePriceType = DevicePriceType.ItemPrice;
                                }

                                orderVM.TotalPrice += itemPrice.Price;
                                orderVM.ItemPrice = itemPrice.Price.ToInt();

                            }
                            else // retail price
                            {
                                if (orderVM.DevicePriceType == DevicePriceType.ItemPrice)
                                {
                                    orderVM.TotalPrice -= orderVM.ItemPrice;
                                    orderVM.TotalPrice += orderVM.ModelPrice;
                                    orderVM.DevicePriceType = DevicePriceType.RetailPrice;
                                }
                            }
                        }
                    }
                }
            }
            else //(retail price)
            {
                if (personalDetailsVM.RegID == 0)
                {
                    if (orderVM.DevicePriceType != DevicePriceType.RetailPrice)
                    {
                        orderVM.TotalPrice -= orderVM.ItemPrice;
                        orderVM.ItemPrice = 0;
                        orderVM.TotalPrice += orderVM.ItemPrice;
                    }
                }
            }

            // MobileNo
            var mobileNos = fromSubline ? (List<string>)Session["RegMobileSub_MobileNo"] : (List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()]; // Main or Sub Line
            orderVM.MobileNumbers = mobileNos == null ? new List<string>() : mobileNos;

            if (!fromSubline) //main line
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderVM;
            else //sub line
            {
                Session["RegMobileReg_SublineSummary"] = orderVM;
                orderVM.fromSubline = true;
            }

            // Subline
            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
            if (sublines != null)
                orderVM.Sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];

            return orderVM;
        }

        #endregion

        private ValueAddedServicesVM GetVasComponents(List<string> subscribedComponents, out BundlepackageResp bundlePackages, out  List<int> mandatoryVasIds, ref List<string> KenanMandatoryVasIds)
        {
            RetrieveComponentInfoRequest objRetrieveComponentInfoReq = null;
            RetrieveComponentInfoResponse objRetrieveComponentInfoRes = null;
            using (var proxy = new KenanServiceProxy())
            {
                if (Session["ComponentInfo"] != null)
                {
                    objRetrieveComponentInfoRes = (RetrieveComponentInfoResponse)Session["ComponentInfo"];

                }
                else
                {
                    objRetrieveComponentInfoReq = new RetrieveComponentInfoRequest();
                    objRetrieveComponentInfoReq.ExternalID = Session[SessionKey.ExternalID.ToString()].ToString2();
                    objRetrieveComponentInfoRes = proxy.RetrieveComponentInfo(objRetrieveComponentInfoReq);
                }
            }
            ValueAddedServicesVM mandatoryVasVM = new ValueAddedServicesVM();
            List<int> mandatoryVasIDs = new List<int>();
            mandatoryVasVM = GetMandatoryVasDependencies(subscribedComponents, out mandatoryVasIDs, ref KenanMandatoryVasIds);
            mandatoryVasIds = mandatoryVasIDs;
            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] == null || (Convert.ToString(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]) == String.Empty))
            {
                Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = Session["GuidedSales_PkgPgmBdlPkgCompID"];
            }
            ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();

            string MobileRegType = string.Empty;
            Lstvam = GetAvailablePackageComponents(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), isMandatory: false);
            if (!ReferenceEquals(Session["MobileRegType"], null))
            {
                MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();
            }
            Session[SessionKey.MobileRegType.ToString()] = "13";

            if (Lstvam.ContractVASes != null)
                Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);

            if (Lstvam.AvailableVASes != null)
                Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);

            if (Lstvam.NonMandatoryVASes != null)
                Lstvam2.NonMandatoryVASes.AddRange(Lstvam.NonMandatoryVASes);

            /* For Mandatory packages */
            Session["vasmandatoryids"] = "";
            using (var proxy = new CatalogServiceProxy())
            {

                BundlepackageResp respon = new BundlepackageResp();
                List<int> bundles = new List<int>();
                List<PgmBdlPckComponent> bundleComponents = new List<PgmBdlPckComponent>();
                respon = proxy.GetMandatoryVas(Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]));

                if (Session["RegMobileReg_PkgPgmBdlPkgCompID"] != null)
                {
                    //get Non mandatory vas those have to display in datacomponents section
                    Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]), "MD", isMandatory: false);
                    if (Lstvam.AvailableVASes != null && Lstvam.AvailableVASes.Count > 0)
                    {
                        bundles = new List<int>();
                        bundleComponents = new List<PgmBdlPckComponent>();
                        bundles.Add(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));
                        bundleComponents = MasterDataCache.Instance.FilterComponents(bundles).ToList();
                        for (int j = 0; j < Lstvam.AvailableVASes.Count; j++)
                        {
                            Lstvam.AvailableVASes[j].PgmBdlPkgID = bundleComponents[0].KenanCode;
                        }
                    }
                    Lstvam2.DataVASes.AddRange(Lstvam.AvailableVASes);
                    //get Main vas those have to display in datacomponents section
                    Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]), "DM", isMandatory: false);
                    Lstvam2.DataVASes.AddRange(Lstvam.AvailableVASes);
                }
                if (respon.values != null)
                {
                    for (int i = 0; i < respon.values.Count(); i++)
                    {

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Master_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            Session["intmandatoryidsval"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
                            bundles = new List<int>();
                            bundleComponents = new List<PgmBdlPckComponent>();
                            bundles.Add(Convert.ToInt32(respon.values[i].BdlDataPkgId));
                            bundleComponents = MasterDataCache.Instance.FilterComponents(bundles).ToList();
                            if (bundleComponents.Count > 0)
                                respon.values[i].BdlDataPkgId = bundleComponents[0].KenanCode;
                        }

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.CompType_DataCon)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;

                            List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                            Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
                            bundles = new List<int>();
                            bundleComponents = new List<PgmBdlPckComponent>();
                            bundles.Add(Convert.ToInt32(respon.values[i].BdlDataPkgId));
                            bundleComponents = MasterDataCache.Instance.FilterComponents(bundles).ToList();
                            if (bundleComponents.Count > 0)
                                respon.values[i].BdlDataPkgId = bundleComponents[0].KenanCode;
                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 PgmBdlPkgID = bundleComponents[0].KenanCode,
                                                                 isDefault = e.isDefault
                                                             };
                            Session["intdataidsval"] = respon.values[i].BdlDataPkgId;
                            Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts);

                          

                        }
                        /*Extra packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Extra_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            List<int> Finalintvalues = null;

                            List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                            Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();



                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault
                                                             };
                            Session["intextraidsval"] = respon.values[i].BdlDataPkgId;
                            Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);
                            bundles = new List<int>();
                            bundleComponents = new List<PgmBdlPckComponent>();
                            bundles.Add(Convert.ToInt32(respon.values[i].BdlDataPkgId));
                            bundleComponents = MasterDataCache.Instance.FilterComponents(bundles).ToList();
                            if (bundleComponents.Count > 0)
                                respon.values[i].BdlDataPkgId = bundleComponents[0].KenanCode;
                        }
                        //Pramotional Packages
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Pramotional_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            
                            Session["intpramotionidsval"] = respon.values[i].BdlDataPkgId;
                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intpramotionidsval = respon.values[i].BdlDataPkgId;
                                }
                            }
                            bundles = new List<int>();
                            bundleComponents = new List<PgmBdlPckComponent>();
                            bundles.Add(Convert.ToInt32(respon.values[i].BdlDataPkgId));
                            bundleComponents = MasterDataCache.Instance.FilterComponents(bundles).ToList();
                            if (bundleComponents.Count > 0)
                                respon.values[i].BdlDataPkgId = bundleComponents[0].KenanCode;
                            if (Lstvam.AvailableVASes != null && Lstvam.AvailableVASes.Count > 0)
                            {
                                for (int j = 0; j < Lstvam.AvailableVASes.Count; j++)
                                {
                                    Lstvam.AvailableVASes[j].PgmBdlPkgID = bundleComponents[0].KenanCode;
                                }
                            }
                            Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);

                        }
                        /*Data Contract Discount packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.DiscountDataContract_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
                            Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();


                            bundles = new List<int>();
                            bundleComponents = new List<PgmBdlPckComponent>();
                            bundles.Add(Convert.ToInt32(respon.values[i].BdlDataPkgId));
                            bundleComponents = MasterDataCache.Instance.FilterComponents(bundles).ToList();
                            if (bundleComponents.Count > 0)
                                respon.values[i].BdlDataPkgId = bundleComponents[0].KenanCode;
   
                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes
                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)
                                                             select new AvailableVAS
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 PgmBdlPkgID = bundleComponents[0].KenanCode,
                                                                 isDefault = e.isDefault
                                                             };


                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intdatadiscountidsval = respon.values[i].BdlDataPkgId;
                                }
                            }
                            Session["intdatadiscountidsval"] = respon.values[i].BdlDataPkgId;
                            Lstvam2.PramotionalComponents.AddRange(finalfiltereddatacontracts);
                        }
                        //insurance Components
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Device_Insurecomponent)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
                            Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
                          

                            bundles = new List<int>();
                            bundleComponents = new List<PgmBdlPckComponent>();
                            bundles.Add(Convert.ToInt32(respon.values[i].BdlDataPkgId));
                            bundleComponents = MasterDataCache.Instance.FilterComponents(bundles).ToList();
                            if (bundleComponents.Count > 0)
                                respon.values[i].BdlDataPkgId = bundleComponents[0].KenanCode;

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes
                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)
                                                             select new AvailableVAS
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 PgmBdlPkgID = bundleComponents[0].KenanCode,
                                                                 isDefault = e.isDefault
                                                             };
                           
                       
                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intinsuranceval = respon.values[i].BdlDataPkgId;
                                }
                            }
                            Session["intinsuranceval"] = respon.values[i].BdlDataPkgId;
                            Lstvam2.InsuranceComponents.AddRange(finalfiltereddatacontracts);
                        }

                    }

                }
                bundlePackages = respon;
            }
            if (MobileRegType == "Planonly")
            {

                Lstvam.ContractVASes.Clear();

            }
            if (subscribedComponents != null && subscribedComponents.Count > 0)
            {
                List<AvailableVAS> availableVases = new List<AvailableVAS>();
                List<AvailableVAS> dataVases = new List<AvailableVAS>();
                List<AvailableVAS> extraVases = new List<AvailableVAS>();
                List<AvailableVAS> mandatoryVASes = new List<AvailableVAS>();
                List<AvailableVAS> nonMandatoryVASes = new List<AvailableVAS>();
                List<AvailableVAS> pramotionalVASes = new List<AvailableVAS>();
                List<AvailableVAS> DeviceInsuranceVASes = new List<AvailableVAS>();
                foreach (var vas in Lstvam2.AvailableVASes)
                {
                    List<string> checkVases = new List<string>();
                    checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                    if (checkVases == null || checkVases.Count == 0)
                        availableVases.Add(vas);
                }
                foreach (var vas in Lstvam2.DataVASes)
                {
                    List<string> checkVases = new List<string>();
                    checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                    if (checkVases == null || checkVases.Count == 0)
                        dataVases.Add(vas);
                }
                foreach (var vas in Lstvam2.ExtraComponents)
                {
                    List<string> checkVases = new List<string>();
                    checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                    if (checkVases == null || checkVases.Count == 0)
                        extraVases.Add(vas);
                }
                foreach (var vas in Lstvam2.NonMandatoryVASes)
                {
                    List<string> checkVases = new List<string>();
                    checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                    if (checkVases == null || checkVases.Count == 0)
                        nonMandatoryVASes.Add(vas);
                }
                foreach (var vas in mandatoryVasVM.MandatoryVASes)
                {
                    List<string> checkVases = new List<string>();
                    checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                    if (checkVases == null || checkVases.Count == 0)
                        mandatoryVASes.Add(vas);
                }
                foreach (var vas in Lstvam2.PramotionalComponents)
                {
                    List<string> lstContractTypes = null;
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        lstContractTypes = proxy.getRebateContractIDbyComponentID(vas.KenanCode);
                    }
                    if (lstContractTypes != null && lstContractTypes.Any())
                    {
                        foreach (var i in lstContractTypes)
                        {
                            List<Online.Registration.Web.KenanSvc.Contract> objResNew = new List<KenanSvc.Contract>();
                            if (objRetrieveComponentInfoRes.Contracts != null && objRetrieveComponentInfoRes.Contracts.Any())
                            {
                                objResNew = objRetrieveComponentInfoRes.Contracts.Where(a => a.ContractType == i && a.EndDate > DateTime.Now.Date).ToList();
                            }
                            if (objResNew != null && objResNew.Any())
                            {
                                var item = from e in objResNew
                                           where e.EndDate.Date < DateTime.Now.Date
                                           select e;
                                if (item != null && item.Count() > 0)
                                    pramotionalVASes.Add(vas);
                            }
                            else
                            {
                                List<string> checkVases = new List<string>();
                                checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                                if (checkVases == null || checkVases.Count == 0)
                                    pramotionalVASes.Add(vas);
                            }
                        }
                    }
                    else
                    {
                        List<string> checkVases = new List<string>();
                        checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                        if (checkVases == null || checkVases.Count == 0)
                            pramotionalVASes.Add(vas);
                    }
                }
                foreach (var vas in Lstvam2.InsuranceComponents)
                {
                    List<string> checkVases = new List<string>();
                    checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                    if (checkVases == null || checkVases.Count == 0)
                        DeviceInsuranceVASes.Add(vas);
                }
                Lstvam2.AvailableVASes = availableVases;
                Lstvam2.DataVASes = dataVases;
                Lstvam2.ExtraComponents = extraVases;
                Lstvam2.MandatoryVASes = mandatoryVASes;
                Lstvam2.NonMandatoryVASes = nonMandatoryVASes;
                Lstvam2.PramotionalComponents = pramotionalVASes;
                Lstvam2.InsuranceComponents = DeviceInsuranceVASes;


            }
            return Lstvam2;

        }
        private bool IsNeedIMPOSCreation()
        {
            bool results = false;
            List<ARVASComponents> objlstComponents = new List<ARVASComponents>();
            List<ARVASComponents> objlstConferenceComponents = new List<ARVASComponents>();
            string callConferenceKenancode = string.Empty;
            if (ConfigurationManager.AppSettings["CallConferencingKenanCode"] != null)
                callConferenceKenancode = ConfigurationManager.AppSettings["CallConferencingKenanCode"].ToString();
            string strConvalue = string.Empty;
            if (Session["AddedVas"] != null)
            {
                if (ConfigurationManager.AppSettings["InternationCompID"] != null)
                    strConvalue = ConfigurationManager.AppSettings["InternationCompID"].ToString();
                var addedVas = (List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel>)Session["AddedVas"];
                if (addedVas != null)
                {
                    foreach (var vas in addedVas)
                    {
                        if (vas != null && vas.compList != null)
                        {
                            objlstComponents = vas.compList.Where(c => c.componentId == strConvalue).ToList();
                            objlstConferenceComponents = vas.compList.Where(c => c.componentId == callConferenceKenancode).ToList();
                            if (!ReferenceEquals(objlstComponents, null) && objlstComponents.Count > 0)
                            {
                                results = true;
                            }
                            if (!ReferenceEquals(objlstConferenceComponents, null) && objlstConferenceComponents.Count > 0)
                            {
                                results = true;
                            }
                        }
                    }
                }
            }
            return results;
        }
        private void GetPaymentAndSendToKenan(PersonalDetailsVM personalDetailsVM)
        {
            var request = new OrderFulfillRequest()
            {
                OrderID = personalDetailsVM.RegID.ToString(),
                UserSession = Util.SessionAccess.UserName,

            };
            //if (!Session["IsDealer"].ToBool())
            //{
                using (var proxy = new RegistrationServiceProxy())
                {
                    proxy.RegistrationCancel(new RegStatus()
                    {
                        RegID = personalDetailsVM.RegID,
                        Active = true,
                        CreateDT = DateTime.Now,
                        StartDate = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName,
                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                    });

                    proxy.RegistrationUpdate(new DAL.Models.Registration()
                    {
                        ID = personalDetailsVM.RegID,
                        LastAccessID = Util.SessionAccess.UserName,

                    });
                }
            //}

            using (var proxy = new KenanServiceProxy())
            {
                if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                {
                    CenterOrderContractRequest crpOrder = new CenterOrderContractRequest();
                    crpOrder.orderId = personalDetailsVM.RegID.ToString2();
                    proxy.CenterOrderContractCreation(crpOrder);
                    using (var regProxy = new RegistrationServiceProxy())
                    {

                        regProxy.RegistrationUpdate(new DAL.Models.Registration()
                        {
                            ID = personalDetailsVM.RegID,
                            LastAccessID = Util.SessionAccess.UserName,

                        });
                    }
                }
            }

        }
        private void LogException(Exception ex)
        {
            Logger.DebugFormat("Exception: {0}", ex.Message);
        }
        private void ClearRegistrationSession()
        {
            Session["intpramotionidsval"] = null;
            Session["intmandatoryidsval"] = null;
            Session["intdataidsval"] = null;
            Session["intextraidsval"] = null;
            Session["intdatadiscountidsval"] = null;
            Session["intinsuranceval"] = null;
            Session["RegMobileReg_ResultMessage"] = null;
            //Session["RegMobileReg_BiometricVerify"] = null;
            Session["RegMobileReg_BiometricID"] = null;
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardNo.ToString()] = null;
            Session["RegMobileReg_SelectedModelImageID"] = null;
            Session["RegMobileReg_SelectedModelID"] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
            Session[SessionKey.RegMobileReg_MobileNo.ToString()] = null;
            Session["RegMobileReg_PhoneVM"] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session[SessionKey.RegStepNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardType.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session["RegMobileReg_SelectedOptionID"] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = null;
            Session[SessionKey.RegMobileReg_SublineVM.ToString()] = null;
            Session["ArticleId"] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session["UOMPlanID"] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session["SelectedComponents"] = null;
            Session["OCCmpntList"] = null;
            Session["RebatePenalty"] = null;
            WebHelper.Instance.ClearSubLineSession(true);
        }
        private void FilterDuplicatesInAddRemoveVAS()
        {
            List<string> addedVases = new List<string>();
            List<string> removedVases = new List<string>();
            List<ARVASPackageComponentModel> addedVas = new List<ARVASPackageComponentModel>();
            List<ARVASPackageComponentModel> removedVas = new List<ARVASPackageComponentModel>();
            if (Session["AddedVas"] != null)
            {
                addedVas = (List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel>)Session["AddedVas"];
                if (addedVas != null)
                {
                    foreach (var vas in addedVas)
                    {
                        if (vas != null && vas.compList != null)
                        {
                            foreach (var comp in vas.compList)
                            {
                                addedVases.Add(comp.componentId);
                            }
                        }
                    }
                }
            }
            if (Session["RemovedVas"] != null)
            {
                removedVas = (List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel>)Session["RemovedVas"];
                if (removedVas != null)
                {
                    foreach (var vas in removedVas)
                    {
                        if (vas != null && vas.compList != null)
                        {
                            foreach (var comp in vas.compList)
                            {
                                removedVases.Add(comp.componentId);
                            }
                        }
                    }
                }
            }

            List<ARVASComponents> comps = new List<ARVASComponents>();
            for (int i = 0; i < addedVas.Count; i++)
            {
                comps = addedVas[i].compList;
                if (!ReferenceEquals(addedVas[i], null) && !ReferenceEquals(addedVas[i].compList, null))
                {
                    ARVASComponents comp = new ARVASComponents();
                    for (int j = 0; j < addedVas[i].compList.Count; j++)
                    {
                        comp = addedVas[i].compList[j];
                        for (int k = 0; k < removedVases.Count; k++)
                        {
                            if (removedVases[k].ToString().Equals(comp.componentId))
                            {
                                comps.Remove(comp);
                            }
                        }
                    }
                    addedVas[i].compList = comps;
                }
            }

            List<ARVASComponents> Rcomps = new List<ARVASComponents>();

            for (int i = 0; i < removedVas.Count; i++)
            {
                Rcomps = removedVas[i].compList;
                if (!ReferenceEquals(Rcomps, null)) //&& !ReferenceEquals(Rcomps.compList, null)
                {
                    ARVASComponents rComp = new ARVASComponents();
                    for (int j = 0; j < Rcomps.Count; j++)
                    {
                        rComp = Rcomps[j];
                        for (int k = 0; k < addedVases.Count; k++)
                        {
                            if (addedVases[k].ToString().Equals(rComp.componentId))
                            {
                                Rcomps.Remove(rComp);
                            }
                        }
                    }
                    removedVas[i].compList = Rcomps;
                }
            }

            Session["RemovedVas"] = removedVas;
            Session["AddedVas"] = addedVas;


        }
        private ComponentViewModel LoadComponents()
        {
            ComponentViewModel model = new ComponentViewModel();
            List<string> subscribedComponents = new List<string>();
            decimal price = 0;
            List<List<ARVASComponents>> packages = new List<List<ARVASComponents>>();
            List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();

            model.SubscribedComponents = GetPackageDetails(Session[SessionKey.ExternalID.ToString()].ToString2(), "23", out componentList, ref subscribedComponents);
            if (subscribedComponents != null && subscribedComponents.Count > 0)
            {
                if (Session["IsDealer"].ToBool())
                {
                    List<string> componentsToRemove = ConfigurationManager.AppSettings["RestrictComponentsForDealer"].ToString2().Split(',').ToList();
                    string[] lstcomps = new string[subscribedComponents.Count];
                    for (int i = 0; i < subscribedComponents.Count; i++)
                    {
                        lstcomps[i] = subscribedComponents[i];
                    }
                    lstcomps = lstcomps.Except( lstcomps.Where(val => componentsToRemove.Contains( val) )).ToArray();

                    if (lstcomps != null & lstcomps.Count() > 0)
                    {
                        subscribedComponents = lstcomps.ToList();
                        model.SubscribedComponentIds = string.Join(",", subscribedComponents);
                    }
                }
                else
                {
                    model.SubscribedComponentIds = string.Join(",", subscribedComponents);
                }
            }
            List<string> KenanMandatoryVasIds = new List<string>();
            BundlepackageResp bundlePackages = new BundlepackageResp();
            List<int> mandatoryVasIDs = new List<int>();
            model.Allowedcomponents = GetVasComponents(subscribedComponents, out bundlePackages, out mandatoryVasIDs, ref KenanMandatoryVasIds);
            model.KenanMandatoryVasIds = KenanMandatoryVasIds;
            packages = model.SubscribedComponents.Select(p => p.compList).ToList();
            if (Session["IsDealer"].ToBool())
            {
                List<string> componentsToRemove = ConfigurationManager.AppSettings["RestrictComponentsForDealer"].ToString2().Split(',').ToList();
                List<ARVASPackageComponentModel> lstPacks = model.SubscribedComponents;
                for (int i = 0; i < model.SubscribedComponents.Count; i++)
                {
                    for (int j = 0; j < model.SubscribedComponents[i].compList.Count; j++)
                    {
                        if (componentsToRemove.Contains(model.SubscribedComponents[i].compList[j].componentId))
                        {
                            lstPacks.Remove(model.SubscribedComponents[i]);
                            break;
                        }
                    }
                }
                model.SubscribedComponents = lstPacks;
                packages = model.SubscribedComponents.Select(a => a.compList).ToList();
            }
            if (Session[SessionKey.RegMobileReg_OrderSummary.ToString()] == null)
            {
                OrderSummaryVM orderSummary = new OrderSummaryVM();
                orderSummary = ConstructOrderSummary();
                if (orderSummary.PlanName == null)
                {
                    List<string> plans = new List<string>();
                    plans = model.SubscribedComponents.Where(p => p.PackageDesc.ToLower().Contains("mandatory")).Select(p => p.PackageDesc).ToList();

                    if (plans != null && plans.Count > 0)
                    {
                        orderSummary.PlanName = plans[0];

                    }

                }
                packages = model.SubscribedComponents.Select(p => p.compList).ToList();
                if (packages != null && packages.Count > 0)
                {
                    foreach (var package in packages)
                    {
                        price = price + package.Select(c => Convert.ToDecimal(c.Price)).Sum();
                    }
                    orderSummary.MonthlySubscription = price;
                }
                model.MonthlySubscription = orderSummary.MonthlySubscription;
                if (orderSummary.VasVM.VASesName == null || orderSummary.VasVM.VASesName.Count == 0)
                {
                    orderSummary.VasVM.VASesName = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];

                }

                if (Session["mandatoryPkgCompIds"] != null)
                {
                    var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
                    List<Component> components = new List<Component>();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents((List<int>)Session["mandatoryPkgCompIds"]).ToList();
                        components = proxy.ComponentGet(pgmBdlPckComponents.Select(c => c.ChildID)).ToList();
                    }
                    List<string> curVases = new List<string>();
                    if (Session[SessionKey.RegMobileReg_VasNames.ToString()] != null)
                    {
                        curVases = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
                    }

                    for (int compCnt = 0; compCnt < components.Count; compCnt++)
                    {
                        decimal curPrice = 0;
                        if (pgmBdlPckComponents[compCnt] != null)
                        {
                            curPrice = pgmBdlPckComponents[compCnt].Price;
                            orderSummary.MonthlySubscription += curPrice;
                        }
                        curVases.Add(components[compCnt].Name + " (RM " + curPrice.ToString() + ")");

                    }



                    Session[SessionKey.RegMobileReg_VasNames.ToString()] = curVases.Distinct().ToList();
                }

                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderSummary;
            }
            model.MainPackage = "Main Components";
            model.ExtraPackage = "Extra Components";
            model.DataPackage = "Data Components";
            model.PramotionalPackage = "Promotional Components";
            //model.InsurancePackage = "Device Insurance Components";
			model.InsurancePackage = "Mobile SafeDevice";

            if (Session["IsDealer"].ToBool())
            { 
                List<string> componentsToRemove =  ConfigurationManager.AppSettings["RestrictComponentsForDealer"].ToString2().Split(',').ToList();

                model.Allowedcomponents.AvailableVASes =  model.Allowedcomponents.AvailableVASes.Except(model.Allowedcomponents.AvailableVASes.Where(x => componentsToRemove.Contains(x.KenanCode))).ToList();
            }

          
            if (Session["ShowDComp"].ToString2() == "False")
            {
                var removeDC = model.Allowedcomponents.PramotionalComponents.Where(x => x.KenanCode == ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2()).SingleOrDefault();
                if (removeDC != null)
                {
                    model.Allowedcomponents.PramotionalComponents.Remove(removeDC);
                }
            }

            return model;
        }
        #endregion

        #region Public Methods
        public string validateExtraTenMSISDNAgainstAccount(string arrMSISDNs)
        {

            return Util.validateExtraTenMSISDNAgainstAccount(arrMSISDNs);
        }
        public string validateExtraTenMSISDN(string arrMSISDNs)
        {
            return Util.validateExtraTenMSISDN(arrMSISDNs);

        }
        public string SaveExtraTenMobiles(string arrMSISDNs, int regId)
        {

            using (var proxy = new RegistrationServiceProxy())
            {
                proxy.DeleteExtraTenDetails(regId);
            }
            if (Util.SaveExtraTen(arrMSISDNs, regId) == true)
            {
                return string.Empty;
            }
            else
            {
                return "Saving Failed...";
            }
        }
        public ValueAddedServicesVM GetMandatoryVasDependencies(List<string> subscribedComponents, out  List<int> mandatoryIds, ref List<string> kenanMandatoryVasIds)
        {

            int selectedPlan = 0;
            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] != null)
            {
                selectedPlan = Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]);

            }
            //Get the dependent components of the mandatory vases

            List<int> depCompIds = new List<int>();
            List<int> mandatoryVasIds = new List<int>();
            List<string> mandatoryVasKenanCodes = new List<string>();
            mandatoryIds = new List<int>();
            List<string> mandatoryDepCompIds = new List<string>();

            List<int> mandatoryPkgCompIds = new List<int>();

            ValueAddedServicesVM vasVM = new ValueAddedServicesVM();
            if (selectedPlan > 0)
            {
                vasVM = GetAvailablePackageComponents(selectedPlan, isMandatory: true);
                mandatoryVasIds = vasVM.MandatoryVASes.Select(cas => cas.PgmBdlPkgCompID).ToList();
                mandatoryVasKenanCodes = vasVM.MandatoryVASes.Select(cas => cas.KenanCode).ToList();
                kenanMandatoryVasIds = mandatoryVasKenanCodes;
                if (mandatoryVasIds != null && mandatoryVasIds.Count > 0)
                {
                    using (var proxy = new CatalogServiceProxy())
                    {
                        int loop = 0;
                        foreach (var vasId in mandatoryVasIds)
                        {

                            if (subscribedComponents.Contains(mandatoryVasKenanCodes[loop]))
                            {
                                mandatoryIds.Add(vasId);
                                List<PgmBdlPckComponent> pgmComponents = new List<PgmBdlPckComponent>();
                                List<Component> components = new List<Component>();
                                pgmComponents = proxy.PgmBdlPckComponentGet2(Convert.ToInt32(vasId), 0).ToList();
                                foreach (var component in pgmComponents)
                                {
                                    mandatoryDepCompIds.Add(component.KenanCode);
                                    mandatoryPkgCompIds.Add(component.ID);
                                }
                            }
                            loop++;
                        }
                    }

                }
            }

            if (mandatoryDepCompIds != null && mandatoryDepCompIds.Count > 0)
            {

                Session["MandatoryDepCompIds"] = mandatoryDepCompIds;

                Session["mandatoryPkgCompIds"] = mandatoryPkgCompIds;

            }
            return vasVM;
        }
        public List<int> GetDepenedencyComponents(string componentIds)
        {
            List<int> depCompIds = new List<int>();

            using (var proxy = new CatalogServiceProxy())
            {
                depCompIds = proxy.GetAllDependentComponents(Convert.ToInt32(componentIds), 0);
            }

            return depCompIds;
        }
        public string retrievePenalty(string addedVas, string removedVas, bool changeFlag = true)
        {
            decimal strPenalty = 0.00M;
            var lstRebateContracts = new List<RebateDataContractComponents>();
            var lstRebatePenality = new List<RegRebateDatacontractPenalty>();
            var RegRebatePenalty = new RegRebateDatacontractPenalty();
            if (!string.IsNullOrEmpty(removedVas))
            {
                string[] arrremovedVas = removedVas.Split(',');
                if (arrremovedVas.Count() > 0)
                {
                    lstRebateContracts = MasterDataCache.Instance.RebateContractComponents.ToList();
                    foreach (var s in arrremovedVas)
                    {
                        #region getting fixed penalty for Data Contract Type=DC
                        var item = lstRebateContracts.Where(a => a.ComponentID == s && a.Type == "DC").FirstOrDefault();
                        if (item != null)
                        {
                            RegRebatePenalty = new RegRebateDatacontractPenalty();
                            
                            RegRebatePenalty.ComponetID = item.ComponentID;
                            RegRebatePenalty.RegID = 0;
                            RegRebatePenalty.Penalty = Convert.ToDecimal(item.penalty);
                            RegRebatePenalty.Active = item.Status;
                            RegRebatePenalty.nrcID = item.nrcID;
                            RegRebatePenalty.CreateDT = DateTime.Now;
                            lstRebatePenality.Add(RegRebatePenalty);
                            
                            if (!string.IsNullOrEmpty(item.penalty))
                                strPenalty = strPenalty + Convert.ToDecimal(item.penalty);
                        }
                        #endregion
                        #region getting penalty for Reducing  Type=RP
                        var itemRC = lstRebateContracts.Where(a => a.ComponentID == s && a.Type == "RP").FirstOrDefault();
                        if (itemRC != null)
                        {
                            string strCompID = string.Empty;
                            string strNrcID = string.Empty;
                            List<SubscriberICService.retrievePenalty> penaltybyMSISDN = new List<SubscriberICService.retrievePenalty>();
                            using (var Smartproxy = new retrieveSmartServiceInfoProxy())
                            {
                                penaltybyMSISDN = Smartproxy.retrievePenalty(Session[SessionKey.ExternalID.ToString()].ToString2(), ConfigurationManager.AppSettings["KenanMSISDN"], Session["fxSuscriberNo"].ToString2(), Session["fxSuscriberNoResets"].ToString2(), userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
                                    password: Properties.Settings.Default.retrieveAcctListByICResponsePassword);
                            }
                            if (penaltybyMSISDN != null && penaltybyMSISDN.Count > 0)
                            {
                                foreach (var item1 in penaltybyMSISDN)
                                {
                                    if (item1 != null)
                                    {
                                        RegRebatePenalty = new RegRebateDatacontractPenalty();
                                        if (!string.IsNullOrEmpty(itemRC.ContractName))
                                            strCompID = lstRebateContracts.Where(a => a.ContractName == itemRC.ContractName).Select(a => a.ComponentID).FirstOrDefault();
                                        if (!string.IsNullOrEmpty(itemRC.ContractName))
                                            strNrcID = lstRebateContracts.Where(a => a.ContractName == itemRC.ContractName).Select(a => a.nrcID).FirstOrDefault();
                                        
                                        RegRebatePenalty.ComponetID = strCompID;
                                        RegRebatePenalty.RegID = 0;
                                        RegRebatePenalty.Penalty = Convert.ToDecimal(item1.penalty);
                                        RegRebatePenalty.Active = true;
                                        RegRebatePenalty.nrcID = strNrcID;
                                        RegRebatePenalty.CreateDT = DateTime.Now;
                                        if (!string.IsNullOrEmpty(strCompID) && !string.IsNullOrEmpty(item1.penalty))
                                        {
                                            lstRebatePenality.Add(RegRebatePenalty);
                                        }
                                        if (!string.IsNullOrEmpty(strNrcID) && !string.IsNullOrEmpty(item1.penalty))
                                            strPenalty = strPenalty + Convert.ToDecimal(item1.penalty);
                                    }
                                }
                            }
                        }
                        #endregion

                    }
                }
            }
            Session["RebatePenalty"] = strPenalty;
            Session["lstRebatePenality"] = lstRebatePenality;
            return strPenalty.ToString();
        }
        #endregion

        #region Action
        public ActionResult Index()
        {
            Session[SessionKey.FromPromoOffer.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session[SessionKey.MobileRegType.ToString()] = "13";
            Session["AddedVases"] = null;
            Session["RemovedVases"] = null;
            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session["AddedVasNames"] = null;
            Session["RemovedVasNames"] = null;
            Session["AddedVasPrices"] = null;
            Session["RemovedVasPrices"] = null;
            Session["ARVasGroupIds"] = null;
            Session[SessionKey.VasGroupIds.ToString()] = null;
            Session[SessionKey.Condition.ToString()] = null;
            Session["ChangeFlag"] = null;
            Session["mandatoryPkgCompIds"] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session["RegMobileReg_DataplanID"] = null;
            Session["AccountHolder"] = null;
            Session["RebatePenalty"] = null;
            Session["ShowDComp"] = null;
            Session["HardStopCRP"] = null;
            Session["OrderType"] = (int)MobileRegType.AddRemoveVAS;
            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            return View();
        }
        [HttpPost]
        public ActionResult Index(FormCollection collection, string SelectAccountDtls = null)
        {

            if (collection["submit"].ToString2() == "next")
            {
                if (SelectAccountDtls != null)
                {
                    string[] strAccountDtls = SelectAccountDtls.Split(',');
                    Session[SessionKey.FxAccNo.ToString()] = strAccountDtls[0].ToString();
					Session[SessionKey.MSISDN.ToString()] = Session[SessionKey.ExternalID.ToString()] = strAccountDtls[1].ToString(); //Acd.externalId
                    Session[SessionKey.AccExternalID.ToString()] = strAccountDtls[2].ToString();
                    Session[SessionKey.KenanACNumber.ToString()] = strAccountDtls[3].ToString();
                    
                    Session["fxSuscriberNo"] = strAccountDtls[6].ToString();
                    Session["fxSuscriberNoResets"] = strAccountDtls[7].ToString();
                    Session["AccountType"] = strAccountDtls[8].ToString();

                    if (Session["AccountType"].ToString2() == "Suppline")
                    {
                        Session["PrincMSISDN"] = strAccountDtls[9].ToString();
                        Session["SuppMSISDN"] = strAccountDtls[1].ToString();
                    }
                    else
                    {
                        Session["PrincMSISDN"] = strAccountDtls[1].ToString();
                    }
                    if (Session["AccountType"].ToString2() == "Suppline")
                    {
                        
                        if (Session["PrincMSISDN"] != Session["SuppMSISDN"])
                            Session["ShowDComp"] = WebHelper.Instance.DucklingComponentCheck(Session["PrincMSISDN"].ToString(), "AddRemoveVAS", "");
                        else
                            Session["ShowDComp"] = false;
                    }

                    #region title

                    retrieveAcctListByICResponse AcctListByICResponse1 = null;
                    CustomizedCustomer CustomerPersonalInfo = null;

                    if (!ReferenceEquals(Session["PPID"], null) && "E".Equals(Convert.ToString(Session["PPID"])))
                    {
                        if (!ReferenceEquals(Session["PPIDInfo"], null))
                        {
                            int CustomerTitleIDfromConfig = ConfigurationManager.AppSettings["CustomerTitleID"] != null ? Convert.ToInt32(ConfigurationManager.AppSettings["CustomerTitleID"]) : 0;
                            ///CHECK IF EXISTING USER THEN ONLY RETRIEVE CUSTOMER INFORMATION
                            AcctListByICResponse1 = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                            ///CHECK IF CUSTOMER INFORMATION IS THERE OR NOT
                            if (!ReferenceEquals(AcctListByICResponse1, null) && AcctListByICResponse1.itemList.Any() && !ReferenceEquals(AcctListByICResponse1.itemList.Where(c => c.Customer != null).ToList(), null))
                            {
                                if (Session["AccountType"].ToString2() != "MISMSec")
                                {
                                    CustomerPersonalInfo = AcctListByICResponse1.itemList.FirstOrDefault(c => c.Customer != null && c.ExternalId.ToString2() == Session[SessionKey.ExternalID.ToString()].ToString2()).Customer;
                                }
                                else //"MISMSec"
                                {
                                    CustomerPersonalInfo = AcctListByICResponse1.itemList.FirstOrDefault(i => (i.Customer != null && i.SecondarySimList != null && i.SecondarySimList.Any(s => s.Msisdn.Equals(Session[SessionKey.ExternalID.ToString()].ToString2())))).Customer;
                                }
                                //if (CustomerPersonalInfo != null && CustomerPersonalInfo.Title != "")
                                if (CustomerPersonalInfo != null && !string.IsNullOrWhiteSpace(CustomerPersonalInfo.Title))
                                {
                                    if (CustomerPersonalInfo.CustomerTitleID != null)
                                    {
                                        List<SelectListItem> objTitleLst = new List<SelectListItem>();
                                        objTitleLst = Util.GetList(RefType.CustomerTitle, defaultValue: string.Empty);
                                        string customerTitle = CustomerPersonalInfo.Title;
                                        if (!customerTitle.EndsWith("."))
                                        {
                                            CustomerPersonalInfo.Title = CustomerPersonalInfo.Title + ".";
                                        }
                                        else
                                        {
                                            customerTitle = customerTitle.Substring(0, customerTitle.LastIndexOf('.'));

                                        }
                                        if (objTitleLst != null && objTitleLst.Any())
                                        {
                                            objTitleLst = objTitleLst.Where(a => a.Text == CustomerPersonalInfo.Title || a.Text == customerTitle).ToList();
                                            int titleID = objTitleLst.FirstOrDefault().Value.ToInt();
                                            Session["AddCustomerTitleID"] = titleID;

                                            string titleName = Util.GetNameByID(RefType.CustomerTitle, titleID > 0 ? titleID : CustomerTitleIDfromConfig);
                                            Session["AccountHolder"] = titleName + " " + CustomerPersonalInfo.CustomerName;
                                        }
                                        else
                                        {
                                            Session["AddCustomerTitleID"] = CustomerTitleIDfromConfig;
                                            string titleName = Util.GetNameByID(RefType.CustomerTitle, CustomerTitleIDfromConfig);
                                            Session["AccountHolder"] = titleName + " " + CustomerPersonalInfo.CustomerName;
                                        }
                                    }
                                    else
                                    {
                                        Session["AddCustomerTitleID"] = CustomerTitleIDfromConfig;
                                        string titleName = Util.GetNameByID(RefType.CustomerTitle, CustomerTitleIDfromConfig);
                                        Session["AccountHolder"] = titleName + " " + CustomerPersonalInfo.CustomerName;
                                    }
                                }
                                else
                                {
                                    Session["AddCustomerTitleID"] = CustomerTitleIDfromConfig;
                                    string titleName = Util.GetNameByID(RefType.CustomerTitle, CustomerTitleIDfromConfig);
                                    Session["AccountHolder"] = titleName + " " + CustomerPersonalInfo.CustomerName;
                                }
                            }
                        }
                    }
                    #endregion
                    using (var proxy = new UserServiceProxy())
                    {
                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.AddRemoveVAS, "Select Account", Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                    }
                    return RedirectToAction("SelectVAS");
                }
            }
            Session[SessionKey.SelectedAccountNumber.ToString()] = SelectAccountDtls;
			Session["SelectAccountDtls"] = SelectAccountDtls.ToString2();
            ///REPLACE THE BELOW REDIRECTION TO YOUR PAGE            
            return RedirectToAction("Index");
        }
        public ActionResult SelectVAS()
        {
            #region Drop 5: BRE Check for non-Drop 4 Flow
            //07042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - Start
            var dropObj = new DropFourObj();

            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW && dropObj.mainFlow.RegType == -1)
            {
                dropObj.mainFlow.RegType = MobileRegType.AddRemoveVAS.ToInt();
            }

            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            //07042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - End
            #endregion

            List<String> LabelReplace = new List<String>();
            ViewBag.GSTMark = Settings.Default.GSTMark;
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            ComponentViewModel model = new ComponentViewModel();
            model = LoadComponents();
            return View(model);

        }
        public ActionResult MobileRegSuccess(int regID, bool isClosed=false)
        {
            RegDetailsObjVM regDetailsObj = new RegDetailsObjVM();
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];
            regDetailsObj.RegID = regID;

			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var proxy = new RegistrationServiceProxy())
				{
					regDetailsObj.PegaVM = new PegaRecommendationsVM();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = proxy.GetPegaRecommendationbyMSISDN(regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria);
					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = regDetailsObj.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();
				}
			}

            return View(regDetailsObj);
            //return View(regID);
        }
        public ActionResult MobileRegSummary(int? regID)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            if (regID > 0)
            {
                Session["RegMobileReg_PersonalDetailsVM"] = null;
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            }
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            var personalDetailsVM = new PersonalDetailsVM();
            personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];


            var idCardTypeID = Session[SessionKey.RegMobileReg_IDCardType.ToString()] != null ? Session[SessionKey.RegMobileReg_IDCardType.ToString()] : null;


            var minAge = Session[SessionKey.RegMobileReg_ProgramMinAge.ToString()].ToInt();
            var NRICType = Util.GetIDCardTypeID(IDCARDTYPE.NRIC);

            if (!ReferenceEquals(Session[SessionKey.PPID.ToString()], null) && "E".Equals(Convert.ToString(Session[SessionKey.PPID.ToString()])))
            {
                if (personalDetailsVM != null)
                {
                    personalDetailsVM.AgeCheckStatus = Session["RegMobileReg_AgeCheckStatusCode"].ToString2() != null ? Session["RegMobileReg_AgeCheckStatusCode"].ToString2() : string.Empty;
                    personalDetailsVM.DDMFCheckStatus = Session["RegMobileReg_DDMFCheckStatusCode"].ToString2() != null ? Session["RegMobileReg_DDMFCheckStatusCode"].ToString2() : string.Empty;
                    personalDetailsVM.AddressCheckStatus = Session["RegMobileReg_AddressCheckStatusCode"].ToString2() != null ? Session["RegMobileReg_AddressCheckStatusCode"].ToString2() : string.Empty;
                    personalDetailsVM.OutStandingCheckStatus = Session["RegMobileReg_IsOutstandingCreditCheckStatusCode"].ToString2() != null ? Session["RegMobileReg_IsOutstandingCreditCheckStatusCode"].ToString2() : string.Empty;
                    personalDetailsVM.TotalLineCheckStatus = Session["RegMobileReg_TotalLineCheckStatusCode"].ToString2() != null ? Session["RegMobileReg_TotalLineCheckStatusCode"].ToString2() : string.Empty;
                    personalDetailsVM.PrincipleLineCheckStatus = Session["RegMobileReg_PrincipalLineCheckStatusCode"].ToString2() != null ? Session["RegMobileReg_PrincipalLineCheckSta:tusCode"].ToString2() : string.Empty;
                }
            }

            List<AddRemoveVASComponents> selectedPlan = new List<AddRemoveVASComponents>();

            Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;
            var orderSummary = new OrderSummaryVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            int paymentstatus = -1;
            Session["RegID"] = regID;
            var regStatusID = 0;
            var KenanLogDetails = new DAL.Models.KenanaLogDetails();
			var allRegDetails = new Online.Registration.DAL.RegistrationDetails();
            if (regID.ToInt() > 0)
            {
                ClearRegistrationSession();

                Session["RegMobileReg_PhoneVM"] = null;

                using (var proxy = new RegistrationServiceProxy())
                {
					allRegDetails = proxy.GetRegistrationFullDetails(regID.Value);

                    if (allRegDetails.Customers.FirstOrDefault().IsVIP == 1 && !Roles.IsUserInRole("MREG_SV"))
                    {
                        Session["VIPFailMsg"] = true;
                        return RedirectToAction("StoreKeeper", "Registration");
                    }

					WebHelper.Instance.MapRegistrationDetailsToVM(false, regID.Value.ToInt(), ref allRegDetails, out personalDetailsVM, isPDPA: false);
					reg = allRegDetails.Regs.SingleOrDefault();

                    Session[SessionKey.ExtratenMobilenumbers.ToString()] = personalDetailsVM.ExtraTenMobileNumbers;

                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);
                    Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;

                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    // RegMdlGrpModels
                    var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                    {
                        RegMdlGrpModel = new RegMdlGrpModel()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();
                    if (regMdlGrpModelIDs.Count() > 0)
                        regMdlGrpModel = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).SingleOrDefault();

                    if (regMdlGrpModel.ID == 0)
                    {
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;
                    }
                    else
                    {
                        Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = regMdlGrpModel.Price.ToDecimal();
                        Session["RegMobileReg_SelectedModelImageID"] = regMdlGrpModel.ModelImageID;
                        Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.DevicePlan;
                    }

                    // RegPgmBdlPkgComponent
                    var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                    {
                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();

                    // Main Line PgmBdlPkgComponent
                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    if (regPgmBdlPkgCompIDs.Count() > 0)
                        regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind()
                    {
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();

                    if (suppLineIDs.Count() > 0)
                    {
                        suppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    }

                    //<Start> - 20150914 - Samuel - display sales code in summary page
                    //personalDetailsVM.Registration = proxy.RegistrationGet(personalDetailsVM.RegID);
                    personalDetailsVM.RegAttributes = proxy.RegAttributesGetByRegID(personalDetailsVM.RegID);
                    /*using (var proxyOrg = new OrganizationServiceProxy())
                    {
                        if (!ReferenceEquals(personalDetailsVM.Registration, null) && !ReferenceEquals(personalDetailsVM.Registration.CenterOrgID, null) && personalDetailsVM.Registration.CenterOrgID != 0)
                        {
                            personalDetailsVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { personalDetailsVM.Registration.CenterOrgID }).FirstOrDefault();
                        }
                    }*/
                    var salesPerson = allRegDetails.Regs.FirstOrDefault().SalesPerson;
                    var orgID = allRegDetails.Regs.FirstOrDefault().CenterOrgID;
                    var outletCode = string.Empty;
                    var salesCode = string.Empty;

                    WebHelper.Instance.generateOutletAndSalesCode(orgID, salesPerson, personalDetailsVM.RegAttributes, out outletCode, out salesCode);
                    personalDetailsVM.OutletCode = outletCode;
                    personalDetailsVM.SalesCode = salesCode;
                    //<End> - 20150914 - Samuel - display sales code in summary page
                }


                using (var proxy = new CatalogServiceProxy())
                {


                    //regAddremoveVasCompnents
                    AddRemoveVASResp response = proxy.GetAddRemoveVASComponents(Convert.ToInt32(regID));
                    List<ARVASPackageComponentModel> addeVases = new List<ARVASPackageComponentModel>();
                    ARVASPackageComponentModel addPackage = new ARVASPackageComponentModel();
                    List<ARVASComponents> addComponents = new List<ARVASComponents>();
                    ARVASComponents addComp = null;
                    List<ARVASPackageComponentModel> removeVases = new List<ARVASPackageComponentModel>();
                    ARVASPackageComponentModel removePackage = new ARVASPackageComponentModel();
                    List<ARVASComponents> removeComponents = new List<ARVASComponents>();
                    ARVASComponents removeComp = null;
                    List<string> vasNames = new List<string>();
                    string msisdn = string.Empty;
                    if (response.AddRemoveVASComponents != null && response.AddRemoveVASComponents.Count() > 0)
                        msisdn = response.AddRemoveVASComponents[0].MSISDN;
                    Session[SessionKey.ExternalID.ToString()] = msisdn;
                    //Added by chetan for handline null if response of addremove is null
                    selectedPlan = !ReferenceEquals(response.AddRemoveVASComponents, null) ? response.AddRemoveVASComponents.Where(c => c.Status == "P").ToList() : null;
                    if (selectedPlan != null && selectedPlan.Count() > 0)
                    {
                        Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = selectedPlan.Select(p => p.lnkPgmBdlPkgCompId).ToList()[0];
                    }
                    List<string> subscribedComponents = new List<string>();
                    List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();
                    GetPackageDetails(msisdn, "23", out componentList, ref subscribedComponents);
                    if (Session[SessionKey.RegMobileReg_VasNames.ToString()] != null)
                    {
                        vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
                    }
                    int tempAdd = 0;
                    int tempRemove = 0;

                   

                    if (response.AddRemoveVASComponents != null)
                    {
                        IEnumerable<IGrouping<string, AddRemoveVASComponents>> query = response.AddRemoveVASComponents.GroupBy(k => k.PackageDesc);
                        foreach (IGrouping<string, AddRemoveVASComponents> groupItem in query)
                        {
                            addPackage = new ARVASPackageComponentModel();
                            removePackage = new ARVASPackageComponentModel();
                            addComponents = new List<ARVASComponents>();
                            removeComponents = new List<ARVASComponents>();
                            //addPackage.PackageDesc = removePackage.PackageDesc = groupItem.Key;
                            foreach (AddRemoveVASComponents vasItem in groupItem)
                            {
                                if (vasItem.Status.Equals("A"))
                                {
                                    tempAdd++;
                                    addComp = new ARVASComponents();
                                    addComp.componentId = vasItem.ComponentId;
                                    addComp.componentDesc = vasItem.ComponentDesc;
                                    addComp.Price = vasItem.Price.ToString();
                                    if (addComp.Price == null || addComp.Price == string.Empty)
                                        addComp.Price = "0.00";
                                    if (personalDetailsVM.StatusID != 25 && personalDetailsVM.StatusID != 2)
                                        vasNames.Add(vasItem.ComponentDesc + " (RM " + addComp.Price + ")");
                                    addComponents.Add(addComp);
                                    addPackage.compList = addComponents;
                                    addPackage.PackageDesc = vasItem.PackageDesc;
                                }
                                if (vasItem.Status.Equals("D"))
                                {
                                    tempRemove++;
                                    removeComp = new ARVASComponents();
                                    removeComp.componentId = vasItem.ComponentId;
                                    removeComp.componentDesc = vasItem.ComponentDesc;
                                    removeComp.Price = vasItem.Price.ToString();
                                    if (removeComp.Price == null || removeComp.Price == string.Empty)
                                        removeComp.Price = "0.00";
                                    vasNames.Remove(vasItem.ComponentDesc + " (RM " + removeComp.Price + ")");
                                    removeComponents.Add(removeComp);
                                    removePackage.compList = removeComponents;
                                    removePackage.PackageDesc = vasItem.PackageDesc;
                                }

                            }


                            if (tempAdd > 0)
                                addeVases.Add(addPackage);
                            if (tempRemove > 0)
                                removeVases.Add(removePackage);

                            tempAdd = 0;
                            tempRemove = 0;
                        }
                    }
                    Session[SessionKey.RegMobileReg_VasNames.ToString()] = vasNames.Distinct().ToList();


                    Session["AddedVas"] = addeVases;
                    Session["RemovedVas"] = removeVases;
                    string userICNO = null;
                    if (Util.SessionAccess != null && Util.SessionAccess.User != null)
                        userICNO = Util.SessionAccess.User.IDCardNo;

                    if (userICNO != null && userICNO.Length > 0)
                    {
                        objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                    }
                    if (objWhiteListDetailsResp != null)
                    {
                        if (objWhiteListDetailsResp.Description != null)
                        {
                            personalDetailsVM._WhiteListDetails.Description = objWhiteListDetailsResp.Description;
                        }
                    }

                    if (regMdlGrpModel.ID > 0)
                        Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;


                    var mainLinePBPCs = new List<PgmBdlPckComponent>();
                    if (mainLinePBPCIDs.Count() > 0)
                        mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var DPCode = Properties.Settings.Default.RegType_DevicePlan;
                    var MAcode = Properties.Settings.Default.Master_Component;
                    var MPcode = Properties.Settings.Default.Main_package;
                    var CPCode = Properties.Settings.Default.ComplimentaryPlan;
                    var DCCode = Properties.Settings.Default.CompType_DataCon;
                    var ECCode = Properties.Settings.Default.Extra_Component;



                    if (mainLinePBPCs.Count() > 0)
                    {
                        Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode)).SingleOrDefault().ID;

                        var vasIDs = "";
                        var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode).Select(a => a.ID).ToList();

                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session[SessionKey.RegMobileReg_VasIDs.ToString()] = vasIDs;
                    }


                    #region Added by Raj on 31 August 2013 for CR
                    using (var lnkregproxy = new RegistrationServiceProxy())
                    {
                        if (regID.ToInt() > 0)
                        {
                            DAL.Models.LnkRegDetails lnkregdetails = lnkregproxy.LnkRegistrationGetByRegId(regID.ToInt());
                            if (lnkregdetails != null)
                            {
                                if (lnkregdetails.IsWaivedOff != null)
                                    personalDetailsVM.isWaiverOff = lnkregdetails.IsWaivedOff.GetValueOrDefault();
                            }
                        }
                    }
                    #endregion
                }
            }
            if (reg.OfferID != null && reg.OfferID > 0)
            {
                Session[SessionKey.OfferId.ToString()] = reg.OfferID;
            }


            if (reg.KenanAccountNo != null)
            { Session["KenanAccountNo"] = reg.KenanAccountNo; }
            else
            { Session["KenanAccountNo"] = "0"; }
            OrderSummaryVM orderSummaryVM = new OrderSummaryVM();
            if (regID > 0)
            {
                orderSummary = ConstructOrderSummary();

                if (selectedPlan != null && selectedPlan.Count > 0)
                {
                    orderSummary.PlanName = selectedPlan[0].PackageDesc;
                    orderSummary.MonthlySubscription = selectedPlan[0].Price;
                }
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderSummary;
                //}
            }

            #region Added by VLT on 21 June 2013 for getting Sim Type
            using (var proxy = new RegistrationServiceProxy())
            {
                if (regID.ToInt() > 0)
                {
                    DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(regID.ToInt());
                    if (lnkregdetails != null)
                    {
                        personalDetailsVM.Justification = lnkregdetails.Justification.ToString2();

                        if (lnkregdetails.SimType != null)
                            personalDetailsVM.SimType = lnkregdetails.SimType;
                        else
                            personalDetailsVM.SimType = "Normal";
                    }
                    else
                        personalDetailsVM.SimType = "Normal";



                    #region Commented code for PDPA
                    ///*Changes by chetan related to PDPA implementation*/
                    //string documentversion = lnkregdetails.PdpaVersion;
                    //personalDetailsVM.PDPAVersionStatus = documentversion;
                    //if (!ReferenceEquals(documentversion, null))
                    //{
                    //    using (var registrationProxy = new RegistrationServiceProxy())
                    //    {
                    //        var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", documentversion);
                    //        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                    //    }
                    //}
                    #endregion

                }
                #region Commented code for PDPA
                //else
                //{
                //    /*Changes by chetan related to PDPA implementation*/
                //    using (var registrationProxy = new RegistrationServiceProxy())
                //    {
                //        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                //        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                //    }
                //}
                #endregion Commented code for PDPA

            }
            #endregion

            
            #region title

            retrieveAcctListByICResponse AcctListByICResponse1 = null;
            CustomizedCustomer CustomerPersonalInfo = null;

            if (!ReferenceEquals(Session["PPID"], null) && "E".Equals(Convert.ToString(Session["PPID"])))
            {
                if (!ReferenceEquals(Session["PPIDInfo"], null))
                {
                    ///CHECK IF EXISTING USER THEN ONLY RETRIEVE CUSTOMER INFORMATION
                    AcctListByICResponse1 = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                    ///CHECK IF CUSTOMER INFORMATION IS THERE OR NOT
                    if (!ReferenceEquals(AcctListByICResponse1, null) && !ReferenceEquals(AcctListByICResponse1.itemList.Where(c => c.Customer != null).ToList(), null))
                    {
                        CustomerPersonalInfo = AcctListByICResponse1.itemList.Where(c => c.Customer != null).ToList().FirstOrDefault().Customer;
                        //if (CustomerPersonalInfo != null && CustomerPersonalInfo.Title != "")
                        if (CustomerPersonalInfo != null && !string.IsNullOrWhiteSpace(CustomerPersonalInfo.Title))
                        {
                            if (CustomerPersonalInfo.CustomerTitleID != null)
                            {
                                Online.Registration.Web.RegistrationSvc.RegistrationServiceClient client = new Online.Registration.Web.RegistrationSvc.RegistrationServiceClient();
                                int titleID = client.GetIdByTitleName(CustomerPersonalInfo.Title);
                                personalDetailsVM.Customer.CustomerTitleID = titleID;

                            }
                        }
                        else
                        {
                            personalDetailsVM.Customer.CustomerTitleID = 0;
                        }
                    }
                }
            }
            #endregion

            #region MOC_Status and Liberization status

            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session[SessionKey.ExternalID.ToString()].ToString2()) ? string.Empty : Session[SessionKey.ExternalID.ToString()].ToString2(), regID.ToInt());
            if (Result != null && Result.Length > 1)
            {
                personalDetailsVM.Liberlization_Status = Result[0];
                personalDetailsVM.MOCStatus = Result[1];
            }

            //28052015 - Anthony - to retrieve the liberalization based on the customer level - Start
            if (regID.ToInt() == 0)
            {
                var allCustomerDetails = SessionManager.Get("AllServiceDetails") as retrieveAcctListByICResponse;
                if (!ReferenceEquals(allCustomerDetails, null))
                {
                    personalDetailsVM.Liberlization_Status = Util.getMarketCodeLiberalisationMOC(allCustomerDetails, "LIBERALISATION");
                }
            }
            //28052015 - Anthony - to retrieve the liberalization based on the customer level - End

            #endregion

            if (regID.ToInt() == 0)
            {
                //Drop 5 B18 - Ricky - 2015.05.12 - Populate the default value for Outlet/Sales Code
                WebHelper.Instance.PopulateDealerInfo(personalDetailsVM);
            }
            /*
            using (var registrationProxy = new RegistrationServiceClient())
            {
                
                personalDetailsVM.CDPUStatus = "A";
                // "A - Approved" by decault, Commented code for CDPU approve as requirement changed
            }*/

            #region CDPU Approval
            using (var registrationProxy = new RegistrationServiceClient())
            {
                if (regID > 0)
                {
                    var details = registrationProxy.GetCDPUDetails(new RegBREStatus() { RegId = regID.ToInt() });
                    if (!ReferenceEquals(details, null))
                    {
                        personalDetailsVM.CDPUStatus = details.TransactionStatus;
                        personalDetailsVM.CDPUDescription = details.TransactionReason;
                        personalDetailsVM.IsLockedBy = details.IsLockedBy;
                    }
                }

            }
            #endregion
            
            // 29JAN2016 - display bill cycle
            if (regID.ToInt() > 0)
            {
                if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE).FirstOrDefault() != null)
                {
                    personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE).FirstOrDefault().ATT_Value;
                }
                else
                {
                    personalDetailsVM.BillingCycle = "N/A";
                }
            }
            else
            {
                var selectedMsisdn = Session["MSISDN"].ToString2();
                retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                var selectedAccount = AcctListByICResponse.itemList.Where(c => c.ExternalId == selectedMsisdn).FirstOrDefault();
                var billCycle = (selectedAccount != null && selectedAccount.AccountDetails != null ? selectedAccount.AccountDetails.BillCycle : "N/A");
                personalDetailsVM.BillingCycle = (!string.IsNullOrWhiteSpace(billCycle) ? billCycle : "N/A");
            }
            

            return View(personalDetailsVM);
        }
        [HttpPost]
        public ActionResult SelectVAS(ComponentViewModel vasVM)
        {
            Session["AddedVases"] = vasVM.AddedVases;
            Session["RemovedVases"] = vasVM.RemovedVases;
            Session["AddedVasNames"] = vasVM.AddedVasNames;
            Session["RemovedVasNames"] = vasVM.RemovedVasNames;
            Session["AddedVasPrices"] = vasVM.AddedVasPrices;
            Session["RemovedVasPrices"] = vasVM.RemovedVasPrices;
            Session["ARVasGroupIds"] = vasVM.GroupIds;

            if (vasVM.ChangeFlag != null && vasVM.ChangeFlag != string.Empty)
            {
                Session["ChangeFlag"] = vasVM.ChangeFlag;
            }
            ComponentViewModel model = new ComponentViewModel();
            List<ARVASPackageComponentModel> packages = new List<ARVASPackageComponentModel>();
            List<string> vasNames = new List<string>();
            OrderSummaryVM orderSummary = new OrderSummaryVM();
            List<string> subscribedComponents = new List<string>();
            decimal vasPrice = 0;
            string mandatoryPackageId = string.Empty;
            string dataPackageId = string.Empty;
            string mainPackageId = string.Empty;
            string extraPackageId = string.Empty;
            string pramotionalPackageId = string.Empty;

            List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();
            Online.Registration.Web.SubscriberICService.componentList component = new componentList();
            List<List<ARVASComponents>> newpackages = new List<List<ARVASComponents>>();
            // Condition added by chetan for null checking
            if (!ReferenceEquals(vasVM.ChangeFlag, null))
            {
                if (vasVM.ChangeFlag == "true" || Session["ChangeFlag"].ToString() == "true")
                {
                    Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
                    model.SubscribedComponents = GetPackageDetails(Session[SessionKey.ExternalID.ToString()].ToString(), "23", out componentList, ref subscribedComponents);
                    mandatoryPackageId = model.SubscribedComponents.Where(p => p.PackageDesc.ToLower().Contains("mandatory")).Select(p => p.PackageID).FirstOrDefault();
                    orderSummary = ConstructOrderSummary();
                    if (orderSummary.PlanName == null)
                    {
                        List<string> plans = new List<string>();
                        plans = model.SubscribedComponents.Where(p => p.PackageDesc.ToLower().Contains("mandatory")).Select(p => p.PackageDesc).ToList();
                        if (plans != null && plans.Count > 0)
                        {
                            orderSummary.PlanName = plans[0];
                        }

                    }
                    newpackages = model.SubscribedComponents.Select(p => p.compList).ToList();
                    if (newpackages != null && newpackages.Count > 0)
                    {
                        foreach (var package in newpackages)
                        {
                            vasPrice = vasPrice + package.Select(c => Convert.ToDecimal(c.Price)).Sum();
                        }
                        orderSummary.MonthlySubscription = vasPrice;
                    }
                    Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderSummary;
                }
            }
            vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
            if (vasVM.AddedVasNames != null)
            {
                foreach (var vas in vasVM.AddedVasNames.Split(','))
                {
                    if (vas != string.Empty)
                    {
                        vasNames.Add(vas);
                    }
                }
            }
            if (vasVM.RemovedVasNames != null)
            {
                foreach (var vas in vasVM.RemovedVasNames.Split(','))
                {
                    if (vas != string.Empty)
                    {
                        vasNames.Remove(vas);
                    }
                }
            }
            List<string> removedComponents = new List<string>();
            if (vasVM.RemovedVases != null)
            {
                packages = GetRemovedVases(Session[SessionKey.ExternalID.ToString()].ToString(), "23", vasVM.RemovedVases, ref removedComponents);
                foreach (var remVas in vasVM.RemovedVases.Split(','))
                {
                    if (remVas != string.Empty)
                    {
                        componentList.Remove(componentList.Where(c => c.componentIdField == remVas).FirstOrDefault());
                    }
                }
				if (sharingQuotaFeature)
				{
					string mutualCheckResult = WebHelper.Instance.MutualDependencyheckForARS(removedVASes: WebHelper.Instance.CommaSplitToListString(vasVM.RemovedVases), addedVASes: WebHelper.Instance.CommaSplitToListString(vasVM.AddedVases));
					if (!string.IsNullOrEmpty(mutualCheckResult))
					{
						model = LoadComponents();
						model.ShareSuppErrorMessage = mutualCheckResult;
						return View(model);
					}
				}
            }
            Session[SessionKey.RegMobileReg_VasNames.ToString()] = vasNames.Distinct().ToList();
            Session["RemovedVas"] = packages;
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = vasVM.ExtraTenMobileNumbers;
            List<ARVASPackageComponentModel> listPakcagecomponents = new List<ARVASPackageComponentModel>();
            ARVASPackageComponentModel compModel = null;
            List<ARVASComponents> vascomponents = new List<ARVASComponents>();
            ARVASComponents comp = null;
            string response = string.Empty;
            ValueAddedServicesVM vam = new ValueAddedServicesVM();
            List<string> packageKenanCodes = new List<string>();
            BundlepackageResp bundlePackages = new BundlepackageResp();
            List<int> mandatoryVasIds = new List<int>();
            List<string> KenanMandatoryVasIds = new List<string>();
            vam = GetVasComponents(subscribedComponents, out bundlePackages, out mandatoryVasIds, ref KenanMandatoryVasIds);
            List<int> depCompIds = new List<int>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            int selectedPlan = 0;
            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] != null)
            {
                selectedPlan = Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]);
            }
            if (bundlePackages != null)
            {
                //string mandatoryPackageId = model.SubscribedComponents.Where(p => p.PackageDesc.ToLower().Contains("mandatory")).Select(p => p.PackageID).FirstOrDefault();
                dataPackageId = bundlePackages.values.Where(b => b.Plan_Type == Properties.Settings.Default.CompType_DataCon).Select(b => b.BdlDataPkgId).FirstOrDefault();
                mainPackageId = bundlePackages.values.Where(b => b.Plan_Type == Properties.Settings.Default.Master_Component).Select(b => b.BdlDataPkgId).FirstOrDefault();
                extraPackageId = bundlePackages.values.Where(b => b.Plan_Type == Properties.Settings.Default.Extra_Component).Select(b => b.BdlDataPkgId).FirstOrDefault();
                pramotionalPackageId = bundlePackages.values.Where(b => b.Plan_Type == Properties.Settings.Default.Pramotional_Component).Select(b => b.BdlDataPkgId).FirstOrDefault();
                pramotionalPackageId = bundlePackages.values.Where(b => b.Plan_Type == Properties.Settings.Default.Device_Insurecomponent).Select(b => b.BdlDataPkgId).FirstOrDefault();

            }
            decimal price = 0;
            //Get the dependent components of the mandatory vases

            if (selectedPlan > 0)
            {
                //mandatoryVasIds = GetAvailablePackageComponents(selectedPlan, isMandatory: true).MandatoryVASes.Select(cas => cas.PgmBdlPkgCompID).ToList();
                if (mandatoryVasIds != null && mandatoryVasIds.Count > 0)
                {
                    using (var proxy = new CatalogServiceProxy())
                    {

                        foreach (var vasId in mandatoryVasIds)
                        {
                            compModel = new ARVASPackageComponentModel();
                            vascomponents = new List<ARVASComponents>();
                            compModel.PackageDesc = "Mandatory Component                                        ";
                            comp = new ARVASComponents();
                            depCompIds = GetDepenedencyComponents(vasId.ToString());
                            
                            if (depCompIds != null && depCompIds.Count > 0)
                            {
                                foreach (var depComp in depCompIds)
                                {
                                    List<int> curDecomp = new List<int>();
                                    curDecomp.Add(depComp);
                                    List<Component> components = new List<Component>();
                                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(curDecomp).ToList();
                                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID)).ToList();
                                    
                                    components = components.Where(a => a.RestricttoKenan == false).ToList();
                                    if (components != null && components.Count > 0)
                                    {
                                        comp = new ARVASComponents();
                                        comp.componentId = pgmBdlPckComponents[0].KenanCode;
                                        comp.componentDesc = components[0].Description;
                                        comp.Price = pgmBdlPckComponents[0].Price.ToString();
                                        comp.packagedId = mandatoryPackageId;
                                        component = new componentList();
                                        component.packageIdField = mandatoryPackageId;
                                        component.componentIdField = comp.componentId;
                                        price = price + Convert.ToDecimal(comp.Price);
                                        if (subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode) && removedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                        {
                                            subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                            vascomponents.Add(comp);
                                            vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                            componentList.Add(component);
                                        }
                                        else if (!subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                        {
                                            subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                            vascomponents.Add(comp);
                                            vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                            componentList.Add(component);
                                        }
                                    }
                                }
                            }
                            if (vascomponents != null && vascomponents.Count > 0)
                            {
                                compModel.compList = vascomponents;
                                listPakcagecomponents.Add(compModel);
                            }
                        }
                    }

                }
            }
            // Condition added by chetan for null checking
            if (!ReferenceEquals(vasVM.ChangeFlag, null))
            {
                if (vasVM.ChangeFlag == "true" || Session["ChangeFlag"].ToString() == "true")
                {
                    if (vasVM.AddedVases != null)
                    {
                        foreach (var vas in vasVM.AddedVases.Split(','))
                        {
                            if (vas != string.Empty)
                            {
                                if (vam.MandatoryVASes.Where(v => v.KenanCode == vas) != null)
                                {
                                    #region "Mandatory Components "
                                    compModel = new ARVASPackageComponentModel();
                                    vascomponents = new List<ARVASComponents>();
                                    compModel.PackageDesc = "Mandatory Components";
                                    comp = new ARVASComponents();
                                    List<AvailableVAS> listVas = vam.MandatoryVASes.Where(v => v.KenanCode == vas).ToList();
                                    if (listVas != null && listVas.Count > 0)
                                    {
                                        comp.componentId = listVas[0].KenanCode;
                                        comp.componentDesc = listVas[0].ComponentName;
                                        comp.Price = listVas[0].Price.ToString();
                                        comp.packagedId = mandatoryPackageId;
                                        price = price + Convert.ToDecimal(comp.Price);
                                        vascomponents.Add(comp);
                                        component = new componentList();
                                        component.packageIdField = mandatoryPackageId;
                                        component.componentIdField = comp.componentId;
                                        componentList.Add(component);
                                        depCompIds = GetDepenedencyComponents(listVas[0].PgmBdlPkgCompID.ToString());
                                        if (depCompIds != null && depCompIds.Count > 0)
                                        {
                                            foreach (var depComp in depCompIds)
                                            {
                                                List<int> curDecomp = new List<int>();
                                                curDecomp.Add(depComp);

                                                List<Component> components = new List<Component>();
                                                using (var proxy = new CatalogServiceProxy())
                                                {
                                                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(curDecomp).ToList();

                                                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID)).ToList();

                                                    components = components.Where(a => a.RestricttoKenan == false).ToList();
                                                }
                                                if (components != null && components.Count > 0)
                                                {
                                                    comp = new ARVASComponents();
                                                    comp.componentId = pgmBdlPckComponents[0].KenanCode;
                                                    comp.componentDesc = components[0].Description;
                                                    comp.packagedId = mandatoryPackageId;
                                                    comp.Price = pgmBdlPckComponents[0].Price.ToString();
                                                    price = price + Convert.ToDecimal(comp.Price);
                                                    component = new componentList();
                                                    component.packageIdField = mandatoryPackageId;
                                                    component.componentIdField = comp.componentId;

                                                    if (subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode) && removedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                    else if (!subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                }
                                            }

                                        }
                                        compModel.compList = vascomponents;
                                        listPakcagecomponents.Add(compModel);
                                    }

                                    #endregion
                                }
                                if (vam.NonMandatoryVASes.Where(v => v.KenanCode == vas) != null)
                                {
                                    #region "Main Components - NonMandatoryVASes"
                                    compModel = new ARVASPackageComponentModel();
                                    vascomponents = new List<ARVASComponents>();
                                    compModel.PackageDesc = "Main Component";
                                    comp = new ARVASComponents();
                                    List<AvailableVAS> listVas = vam.NonMandatoryVASes.Where(v => v.KenanCode == vas).ToList();
                                    if (listVas != null && listVas.Count > 0)
                                    {
                                        comp.componentId = listVas[0].KenanCode;
                                        comp.componentDesc = listVas[0].ComponentName;
                                        comp.Price = listVas[0].Price.ToString();
                                        comp.packagedId = mandatoryPackageId;
                                        price = price + Convert.ToDecimal(comp.Price);
                                        vascomponents.Add(comp);
                                        component = new componentList();
                                        component.packageIdField = mandatoryPackageId;
                                        component.componentIdField = comp.componentId;
                                        componentList.Add(component);
                                        depCompIds = GetDepenedencyComponents(listVas[0].PgmBdlPkgCompID.ToString());
                                        if (depCompIds != null && depCompIds.Count > 0)
                                        {
                                            foreach (var depComp in depCompIds)
                                            {
                                                List<int> curDecomp = new List<int>();
                                                curDecomp.Add(depComp);
                                                List<Component> components = new List<Component>();
                                                using (var proxy = new CatalogServiceProxy())
                                                {
                                                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(curDecomp).ToList();
                                                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID)).ToList();
                                                    components = components.Where(a => a.RestricttoKenan == false).ToList();
                                                }
                                                if (components != null && components.Count > 0)
                                                {
                                                    comp = new ARVASComponents();
                                                    comp.componentId = pgmBdlPckComponents[0].KenanCode;
                                                    comp.componentDesc = components[0].Description;
                                                    comp.packagedId = mandatoryPackageId;
                                                    comp.Price = pgmBdlPckComponents[0].Price.ToString();
                                                    price = price + Convert.ToDecimal(comp.Price);
                                                    component = new componentList();
                                                    component.packageIdField = mandatoryPackageId;
                                                    component.componentIdField = comp.componentId;

                                                    if (subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode) && removedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                    else if (!subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                }
                                            }
                                        }
                                        compModel.compList = vascomponents;
                                        listPakcagecomponents.Add(compModel);
                                    }
                                    #endregion
                                }
                                if (vam.AvailableVASes.Where(v => v.KenanCode == vas) != null)
                                {
                                    #region "Main Components - Available Vases"
                                    compModel = new ARVASPackageComponentModel();
                                    vascomponents = new List<ARVASComponents>();
                                    compModel.PackageDesc = "Main Component";
                                    comp = new ARVASComponents();
                                    List<AvailableVAS> listVas = vam.AvailableVASes.Where(v => v.KenanCode == vas).ToList();
                                    if (listVas != null && listVas.Count > 0)
                                    {
                                        comp.componentId = listVas[0].KenanCode;
                                        comp.componentDesc = listVas[0].ComponentName;
                                        comp.Price = listVas[0].Price.ToString();
                                        comp.packagedId = mainPackageId;
                                        price = price + Convert.ToDecimal(comp.Price);
                                        vascomponents.Add(comp);
                                        component = new componentList();
                                        component.packageIdField = mainPackageId;
                                        component.componentIdField = comp.componentId;
                                        componentList.Add(component);
                                        depCompIds = GetDepenedencyComponents(listVas[0].PgmBdlPkgCompID.ToString());
                                        if (depCompIds != null && depCompIds.Count > 0)
                                        {
                                            foreach (var depComp in depCompIds)
                                            {
                                                List<int> curDecomp = new List<int>();
                                                curDecomp.Add(depComp);

                                                List<Component> components = new List<Component>();
                                                using (var proxy = new CatalogServiceProxy())
                                                {

                                                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(curDecomp).ToList();

                                                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID)).ToList();

                                                    components = components.Where(a => a.RestricttoKenan == false).ToList();
                                                }
                                                if (components != null && components.Count > 0)
                                                {
                                                    comp = new ARVASComponents();
                                                    comp.componentId = pgmBdlPckComponents[0].KenanCode;
                                                    comp.componentDesc = components[0].Description;
                                                    comp.packagedId = mainPackageId;
                                                    comp.Price = pgmBdlPckComponents[0].Price.ToString();
                                                    price = price + Convert.ToDecimal(comp.Price);
                                                    component = new componentList();
                                                    component.packageIdField = mainPackageId;
                                                    component.componentIdField = comp.componentId;

                                                    if (subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode) && removedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                    else if (!subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                }
                                            }

                                        }
                                        compModel.compList = vascomponents;
                                        listPakcagecomponents.Add(compModel);
                                    }
                                    #endregion
                                }
                                if (vam.DataVASes.Where(v => v.KenanCode == vas) != null)
                                {
                                    #region "DataVases"
                                    compModel = new ARVASPackageComponentModel();
                                    compModel.PackageDesc = "Data Component";
                                    comp = new ARVASComponents();
                                    vascomponents = new List<ARVASComponents>();
                                    List<AvailableVAS> listVas = vam.DataVASes.Where(v => v.KenanCode == vas).ToList();
                                    if (listVas != null && listVas.Count > 0)
                                    {
                                        comp.componentId = listVas[0].KenanCode;
                                        comp.componentDesc = listVas[0].ComponentName;
                                        comp.Price = listVas[0].Price.ToString();
                                        //comp.packagedId = dataPackageId;
                                        comp.packagedId = listVas[0].PgmBdlPkgID;
                                        price = price + Convert.ToDecimal(comp.Price);
                                        component = new componentList();
                                        //component.packageIdField = dataPackageId;
                                        component.packageIdField = listVas[0].PgmBdlPkgID;
                                        component.componentIdField = comp.componentId;
                                        vascomponents.Add(comp);
                                        componentList.Add(component);
                                        depCompIds = GetDepenedencyComponents(listVas[0].PgmBdlPkgCompID.ToString());
                                        if (depCompIds != null && depCompIds.Count > 0)
                                        {
                                            foreach (var depComp in depCompIds)
                                            {
                                                List<int> curDecomp = new List<int>();
                                                curDecomp.Add(depComp);

                                                List<Component> components = new List<Component>();
                                                using (var proxy = new CatalogServiceProxy())
                                                {

                                                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(curDecomp).ToList();

                                                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID)).ToList();

                                                    components = components.Where(a => a.RestricttoKenan == false).ToList();
                                                }
                                                if (components != null && components.Count > 0)
                                                {
                                                    comp = new ARVASComponents();
                                                    comp.componentId = pgmBdlPckComponents[0].KenanCode;
                                                    comp.componentDesc = components[0].Description;
                                                    comp.Price = pgmBdlPckComponents[0].Price.ToString();
                                                    comp.packagedId = dataPackageId;
                                                    component = new componentList();
                                                    component.packageIdField = dataPackageId;
                                                    component.componentIdField = comp.componentId;
                                                    price = price + Convert.ToDecimal(comp.Price);
                                                    if (subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode) && removedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                    else if (!subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                }
                                            }

                                        }
                                        compModel.compList = vascomponents;
                                        listPakcagecomponents.Add(compModel);

                                    }
                                    #endregion
                                }
                                if (vam.ExtraComponents.Where(v => v.KenanCode == vas) != null)
                                {
                                    #region "Extra Components"
                                    vascomponents = new List<ARVASComponents>();
                                    compModel = new ARVASPackageComponentModel();
                                    compModel.PackageDesc = "Extra Component";
                                    comp = new ARVASComponents();
                                    List<AvailableVAS> listVas = vam.ExtraComponents.Where(v => v.KenanCode == vas).ToList();
                                    if (listVas != null && listVas.Count > 0)
                                    {
                                        comp.componentId = listVas[0].KenanCode;
                                        comp.componentDesc = listVas[0].ComponentName;
                                        comp.Price = listVas[0].Price.ToString();
                                        comp.packagedId = extraPackageId;
                                        price = price + Convert.ToDecimal(comp.Price);
                                        vascomponents.Add(comp);
                                        component = new componentList();
                                        component.packageIdField = extraPackageId;
                                        component.componentIdField = comp.componentId;
                                        componentList.Add(component);
                                        depCompIds = GetDepenedencyComponents(listVas[0].PgmBdlPkgCompID.ToString());
                                        if (depCompIds != null && depCompIds.Count > 0)
                                        {
                                            foreach (var depComp in depCompIds)
                                            {
                                                List<int> curDecomp = new List<int>();
                                                curDecomp.Add(depComp);

                                                List<Component> components = new List<Component>();
                                                using (var proxy = new CatalogServiceProxy())
                                                {

                                                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(curDecomp).ToList();

                                                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID)).ToList();

                                                    components = components.Where(a => a.RestricttoKenan == false).ToList();
                                                }
                                                if (components != null && components.Count > 0)
                                                {
                                                    comp = new ARVASComponents();
                                                    comp.componentId = pgmBdlPckComponents[0].KenanCode;
                                                    comp.componentDesc = components[0].Description;
                                                    comp.Price = pgmBdlPckComponents[0].Price.ToString();
                                                    comp.packagedId = extraPackageId;
                                                    component = new componentList();
                                                    component.packageIdField = extraPackageId;
                                                    component.componentIdField = comp.componentId;
                                                    price = price + Convert.ToDecimal(comp.Price);
                                                    if (subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode) && removedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                    else if (!subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                }
                                            }

                                        }
                                        compModel.compList = vascomponents;
                                        listPakcagecomponents.Add(compModel);
                                    }
                                    #endregion
                                }
                                if (vam.PramotionalComponents.Where(v => v.KenanCode == vas) != null)
                                {
                                    #region "Promotional Components"
                                    vascomponents = new List<ARVASComponents>();
                                    compModel = new ARVASPackageComponentModel();
                                    compModel.PackageDesc = "Promotional Component";
                                    comp = new ARVASComponents();
                                    List<AvailableVAS> listVas = vam.PramotionalComponents.Where(v => v.KenanCode == vas).ToList();
                                    if (listVas != null && listVas.Count > 0)
                                    {
                                        comp.componentId = listVas[0].KenanCode;
                                        comp.componentDesc = listVas[0].ComponentName;
                                        comp.Price = listVas[0].Price.ToString();
                                        comp.packagedId = listVas[0].PgmBdlPkgID;
                                        price = price + Convert.ToDecimal(comp.Price);
                                        vascomponents.Add(comp);
                                        component = new componentList();
                                        component.packageIdField = listVas[0].PgmBdlPkgID; ;
                                        component.componentIdField = comp.componentId;
                                        componentList.Add(component);
                                        depCompIds = GetDepenedencyComponents(listVas[0].PgmBdlPkgCompID.ToString());
                                        if (depCompIds != null && depCompIds.Count > 0)
                                        {
                                            foreach (var depComp in depCompIds)
                                            {
                                                List<int> curDecomp = new List<int>();
                                                curDecomp.Add(depComp);

                                                List<Component> components = new List<Component>();
                                                using (var proxy = new CatalogServiceProxy())
                                                {

                                                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(curDecomp).ToList();

                                                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID)).ToList();

                                                    components = components.Where(a => a.RestricttoKenan == false).ToList();
                                                }
                                                if (components != null && components.Count > 0)
                                                {
                                                    comp = new ARVASComponents();
                                                    comp.componentId = pgmBdlPckComponents[0].KenanCode;
                                                    comp.componentDesc = components[0].Description;
                                                    comp.Price = pgmBdlPckComponents[0].Price.ToString();
                                                    comp.packagedId = extraPackageId;
                                                    component = new componentList();
                                                    component.packageIdField = extraPackageId;
                                                    component.componentIdField = comp.componentId;
                                                    price = price + Convert.ToDecimal(comp.Price);
                                                    if (subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode) && removedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                    else if (!subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                }
                                            }

                                        }
                                        compModel.compList = vascomponents;
                                        listPakcagecomponents.Add(compModel);
                                    }
                                    #endregion
                                }
                                if (vam.InsuranceComponents.Where(v => v.KenanCode == vas) != null)
                                {
                                    #region "InsuranceComponents"
                                    vascomponents = new List<ARVASComponents>();
                                    compModel = new ARVASPackageComponentModel();
                                    compModel.PackageDesc = "Device Insurance Components";
                                    comp = new ARVASComponents();
                                    List<AvailableVAS> listVas = vam.InsuranceComponents.Where(v => v.KenanCode == vas).ToList();
                                    if (listVas != null && listVas.Count > 0)
                                    {
                                        comp.componentId = listVas[0].KenanCode;
                                        comp.componentDesc = listVas[0].ComponentName;
                                        comp.Price = listVas[0].Price.ToString();
                                        comp.packagedId = listVas[0].PgmBdlPkgID;
                                        price = price + Convert.ToDecimal(comp.Price);
                                        vascomponents.Add(comp);
                                        component = new componentList();
                                        component.packageIdField = listVas[0].PgmBdlPkgID; ;
                                        component.componentIdField = comp.componentId;
                                        componentList.Add(component);
                                        depCompIds = GetDepenedencyComponents(listVas[0].PgmBdlPkgCompID.ToString());
                                        if (depCompIds != null && depCompIds.Count > 0)
                                        {
                                            foreach (var depComp in depCompIds)
                                            {
                                                List<int> curDecomp = new List<int>();
                                                curDecomp.Add(depComp);

                                                List<Component> components = new List<Component>();
                                                using (var proxy = new CatalogServiceProxy())
                                                {

                                                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(curDecomp).ToList();

                                                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID)).ToList();

                                                    components = components.Where(a => a.RestricttoKenan == false).ToList();
                                                }
                                                if (components != null && components.Count > 0)
                                                {
                                                    comp = new ARVASComponents();
                                                    comp.componentId = pgmBdlPckComponents[0].KenanCode;
                                                    comp.componentDesc = components[0].Description;
                                                    comp.Price = pgmBdlPckComponents[0].Price.ToString();
                                                    comp.packagedId = extraPackageId;
                                                    component = new componentList();
                                                    component.packageIdField = extraPackageId;
                                                    component.componentIdField = comp.componentId;
                                                    price = price + Convert.ToDecimal(comp.Price);
                                                    if (subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode) && removedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                    else if (!subscribedComponents.Contains(pgmBdlPckComponents[0].KenanCode))
                                                    {
                                                        subscribedComponents.Add(pgmBdlPckComponents[0].KenanCode);
                                                        vascomponents.Add(comp);
                                                        vasNames.Add(comp.componentDesc + " (RM " + comp.Price + ")");
                                                        componentList.Add(component);
                                                    }
                                                }
                                            }

                                        }
                                        compModel.compList = vascomponents;
                                        listPakcagecomponents.Add(compModel);
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
            // Condition added by chetan for null checking

            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);

            if (!ReferenceEquals(vasVM.ChangeFlag, null))
            {
                if (vasVM.ChangeFlag == "true" || Session["ChangeFlag"].ToString() == "true")
                {
                    if (componentList.Count > 0)
                    {
						// Drop 5
						retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
						typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, mandatoryPackageID: mandatoryPackageId);
                        typeChkFxVasDependencyResponse chkFxVasDependencyResponse = _retrieveServiceInfoProxy.ChkFxVasDependency(request);

                        if (chkFxVasDependencyResponse.mxsRuleMessagesField != null && chkFxVasDependencyResponse.mxsRuleMessagesField.Count > 0)
                        {
                            foreach (string errmsg in chkFxVasDependencyResponse.mxsRuleMessagesField)
                            {
                                ModelState.AddModelError("", errmsg);
                            }
                            model = LoadComponents();
                            return View(model);
                        }
                        else if (chkFxVasDependencyResponse.msgCodeField == "3")
                        {
                            ModelState.AddModelError("", ErrorMsg.VAS.FetchData);   // Need to change msg
                            
                            model = LoadComponents();
                            return View(model);
                        }
                        else if (chkFxVasDependencyResponse.msgCodeField != "0")
                        {
                            ModelState.AddModelError("", ErrorMsg.VAS.ComponentNotAvailable);   // Need to change msg
                            
                            model = LoadComponents();
                            return View(model);
                        }
                    }
                }
            }
            Session[SessionKey.RegMobileReg_VasNames.ToString()] = vasNames.Distinct().ToList();
            Session["AddedVas"] = listPakcagecomponents;
            OrderSummaryVM cusOrderSummary = new OrderSummaryVM();
            if (Session[SessionKey.RegMobileReg_OrderSummary.ToString()] == null)
            {
                cusOrderSummary = ConstructOrderSummary();
            }
            else
            {
                cusOrderSummary = (OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()];
                
            }
            // Condition added by chetan for null checking
            if (!ReferenceEquals(vasVM.ChangeFlag, null))
            {
                if (vasVM.ChangeFlag == "true" || Session["ChangeFlag"].ToString() == "true")
                {
                    
                    if (vasVM.RemovedVasPrices != null && vasVM.RemovedVasPrices != string.Empty)
                    {
                        foreach (var vPrice in vasVM.RemovedVasPrices.Split(','))
                        {
                            if (vPrice != string.Empty && vPrice.ToString() != "undefined" && vPrice != null)
                            {
                                cusOrderSummary.MonthlySubscription = cusOrderSummary.MonthlySubscription - Convert.ToDecimal(vPrice);
                            }
                        }
                    }
                    if (vasVM.AddedVasPrices != null && vasVM.AddedVasPrices != string.Empty)
                    {
                        foreach (var vPrice in vasVM.AddedVasPrices.Split(','))
                        {
                            if (vPrice != string.Empty && vPrice.ToString() != "undefined" && vPrice != null)
                            {
                                cusOrderSummary.MonthlySubscription = cusOrderSummary.MonthlySubscription + Convert.ToDecimal(vPrice);
                            }
                        }
                    }
                }
            }
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = cusOrderSummary;
            FilterDuplicatesInAddRemoveVAS();
            
            #region "Personal Details Formation by ravi on 21-Nov 2013"
            retrieveAcctListByICResponse AcctListByICResponse = null;
            CustomizedCustomer CustomerPersonalInfo = null;
            PersonalDetailsVM personalDetailsVM = new PersonalDetailsVM();
            if (!ReferenceEquals(Session["PPID"], null) && "E".Equals(Convert.ToString(Session["PPID"])))
            {
                if (!ReferenceEquals(Session["PPIDInfo"], null))
                {
                    ///CHECK IF EXISTING USER THEN ONLY RETRIEVE CUSTOMER INFORMATION
                    AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                    ///CHECK IF CUSTOMER INFORMATION IS THERE OR NOT
                    if (!ReferenceEquals(AcctListByICResponse, null) && !ReferenceEquals(AcctListByICResponse.itemList.Where(c => c.Customer != null && c.AcctExtId == Session["ExternalID"].ToString2()).ToList(), null))
                    {
                        CustomerPersonalInfo = AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList().FirstOrDefault().Customer;
                        if (!String.IsNullOrEmpty(CustomerPersonalInfo.NewIC))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 1;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.NewIC;
                            personalDetailsVM.Customer.NationalityID = 1;
                        }
                        else if (!String.IsNullOrEmpty(CustomerPersonalInfo.PassportNo))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 2;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.PassportNo;
                            personalDetailsVM.Customer.NationalityID = 132;
                        }
                        else if (!String.IsNullOrEmpty(CustomerPersonalInfo.OldIC))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 3;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.OldIC;
                            personalDetailsVM.Customer.NationalityID = CustomerPersonalInfo.NationalityID;
                        }
                        else if (!String.IsNullOrEmpty(CustomerPersonalInfo.OtherIC))
                        {
                            personalDetailsVM.Customer.IDCardTypeID = 4;
                            personalDetailsVM.Customer.IDCardNo = CustomerPersonalInfo.OtherIC;
                            personalDetailsVM.Customer.NationalityID = CustomerPersonalInfo.NationalityID;
                        }
                        if (CustomerPersonalInfo.Title != string.Empty)
                        {
                            if (!CustomerPersonalInfo.Title.EndsWith("."))
                            {
                                CustomerPersonalInfo.Title = CustomerPersonalInfo.Title + ".";
                            }
                        }
                    }
                }
            }
            personalDetailsVM.Address.Postcode = CustomerPersonalInfo.PostCode;
            personalDetailsVM.Address.Line1 = CustomerPersonalInfo.Address1;
            personalDetailsVM.Address.Line2 = CustomerPersonalInfo.Address2;
            personalDetailsVM.Address.Line3 = CustomerPersonalInfo.Address3;
            personalDetailsVM.Customer.FullName = CustomerPersonalInfo.CustomerName;
            personalDetailsVM.Address.Town = CustomerPersonalInfo.City;
            if (CustomerPersonalInfo != null)
            {
                //Changes made by chetan
                if ((CustomerPersonalInfo.Dob != null) && (CustomerPersonalInfo.Dob != "") && CustomerPersonalInfo.Dob.Length >= 8)
                {
                    personalDetailsVM.DOBDay = (CustomerPersonalInfo.Dob.Substring(6, 2)).ToInt();
                    personalDetailsVM.DOBMonth = (CustomerPersonalInfo.Dob.Substring(4, 2)).ToInt();
                    personalDetailsVM.DOBYear = (CustomerPersonalInfo.Dob.Substring(0, 4)).ToInt();
                }
                if (CustomerPersonalInfo.Dob.Length >= 8)
                {
                    personalDetailsVM.DOBDay = (CustomerPersonalInfo.Dob.Substring(6, 2)).ToInt();
                    personalDetailsVM.DOBMonth = (CustomerPersonalInfo.Dob.Substring(4, 2)).ToInt();
                    personalDetailsVM.DOBYear = (CustomerPersonalInfo.Dob.Substring(0, 4)).ToInt();
                }
                else
                {
                    // given fix for min date if no date is avail.
                    if (Session["IDCardNo"] != null)
                    {
                        int OutPut;
                        if (int.TryParse(Session["IDCardNo"].ToString2().Substring(0, 2), out OutPut))
                        {
                            CustomerPersonalInfo.Dob = Session["IDCardNo"].ToString2();
                            int year = Session["IDCardNo"].ToString2().Substring(0, 2).ToInt();
                            int CurrYear = DateTime.Now.Year;
                            string PreYear = "19" + year.ToString2();
                            if (CurrYear - PreYear.ToInt() <= 100)
                            {
                                personalDetailsVM.DOBYear = PreYear.ToInt();
                            }
                            else
                            {
                                PreYear = "20" + year.ToString2();
                                personalDetailsVM.DOBYear = PreYear.ToInt();
                            }
                            personalDetailsVM.DOBDay = (CustomerPersonalInfo.Dob.Substring(4, 2)).ToInt();
                            personalDetailsVM.DOBMonth = (CustomerPersonalInfo.Dob.Substring(2, 2)).ToInt();
                        }
                        else
                            personalDetailsVM.Customer.DateOfBirth = DateTime.MinValue;

                    }
                }
                personalDetailsVM.Customer.CustomerTitleID = CustomerPersonalInfo.CustomerTitleID;
            }
			if (CustomerPersonalInfo != null && !string.IsNullOrEmpty(CustomerPersonalInfo.Cbr))
            {

				CustomerPersonalInfo.Cbr = Util.FormatContactNumber(CustomerPersonalInfo.Cbr);
				personalDetailsVM.Customer.ContactNo = CustomerPersonalInfo.Cbr;
            }
            else
            {
                personalDetailsVM.Customer.ContactNo = "";
            }
			List<SelectListItem> objStateList = Util.GetList(RefType.State);
			List<SelectListItem> curStateId = objStateList.Where(e => e.Text == CustomerPersonalInfo.State).ToList();
			//if (personalDetailsVM.Address.StateID == 0)
			//{
			//    personalDetailsVM.Address.StateID = 19;
			//}
			if (curStateId.Count > 0)
			{
				personalDetailsVM.Address.StateID = Convert.ToInt32(curStateId[0].Value);
			}
            //Feb Drop UAT Bug 1671 - Wrong state being displayed (remove hardcoded values)
            //else {
            //    personalDetailsVM.Address.StateID = 19;
            //}
			


            Session["ExtratenMobilenumbers"] = vam.ExtraTenMobileNumbers;
            Session["RegMobileReg_TabIndex"] = vam.TabNumber;
            Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;
            int? regid = 0;

            using (var proxy = new UserServiceProxy())
            {
                proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.AddRemoveVAS, "Select VAS", Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
            }

            return RedirectToAction("MobileRegSummary", regid);
            #endregion "Personal Details Formation by ravi on 21-Nov 2013 Ends Here"

        }
        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSV,DREG_CH,DREG_N,DREG_C,MREG_DC")]
        [DoNoTCache]
        public ActionResult PersonalDetails(PersonalDetailsVM personalDetailsVM)
        {
            try
            {
                if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.Summary)
                {
                    personalDetailsVM.Customer.IDCardTypeID = personalDetailsVM.InputIDCardTypeID.ToInt();
                    personalDetailsVM.Customer.IDCardNo = personalDetailsVM.InputIDCardNo;
                    /*Added by Rajeswari on 24th March to disable nationality*/
                    personalDetailsVM.Customer.NationalityID = personalDetailsVM.InputNationalityID;

                    //Feb Drop UAT Bug 1671 - Wrong state being displayed (remove hardcoded values)
                    //if (personalDetailsVM.Address.StateID == 0)
                    //{
                    //    personalDetailsVM.Address.StateID = 19;
                    //}
                    Session[SessionKey.ExtratenMobilenumbers.ToString()] = personalDetailsVM.ExtraTenMobileNumbers;
                    Session[SessionKey.RegMobileReg_TabIndex.ToString()] = personalDetailsVM.TabNumber;
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;
                }
                else
                {
                    if (Session[SessionKey.RegMobileReg_Type.ToString()].ToInt() == (int)MobileRegType.DeviceOnly)
                    {
                        Session[SessionKey.RegMobileReg_TabIndex.ToString()] = (int)MobileRegistrationSteps.DeviceCatalog;
                        personalDetailsVM.TabNumber = (int)MobileRegistrationSteps.DeviceCatalog;
                    }
                    else if (personalDetailsVM.TabNumber == 4)
                    {
                        return RedirectToAction("SelectVas");
                    }
                }
            }

            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            int? regid = 0;
            return RedirectToAction("MobileRegSummary", regid);

        }
        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSV,DREG_CH,DREG_N,DREG_C,MREG_DAC,MREG_DC,MREG_DSK")]
        [DoNoTCache]
        public ActionResult MobileRegCancelled(int regID)
        {
            return View(regID);
        }
        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSV,DREG_CH,DREG_N,DREG_C,DSupervisor,MREG_DAI,MREG_DAC,MREG_DIC,MREG_DC,MREG_DSK,MREG_DCH")]
        [DoNoTCache]
        public ActionResult MobileRegSummary(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            var cmssCaseId = string.Empty;
            var cmssCaseStatus=string.Empty;
            //if (Session["RemovedVas"] != null)
            //    personalDetailsVM.isBREFail = true;
            var resp = new RegistrationCreateResp();
            Session[SessionKey.RegMobileReg_TabIndex.ToString()] = personalDetailsVM.TabNumber;
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = personalDetailsVM.ExtraTenMobileNumbers;
            var lstRebatePenality = (List<RegRebateDatacontractPenalty>)Session["lstRebatePenality"];
            OrderSummaryVM orderSummary = new OrderSummaryVM();
            if (Session[SessionKey.RegMobileReg_OrderSummary.ToString()] == null)
            {

                orderSummary = ConstructOrderSummary();

            }
            try
            {

                #region PaymentReceived
                if (collection["submit1"].ToString() == "PaymentRecived")
                {
                    GetPaymentAndSendToKenan(personalDetailsVM);
                    TempData["PageFrom"] = "Cashier";

                    return RedirectToAction("MobileRegSuccess", new { regID = personalDetailsVM.RegID });
                }
                #endregion

                #region close
                if (collection["submit1"].ToString() == "close")
                {
                    if (personalDetailsVM.RegID == 0)
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegBreFail)
                            });
                        }
                        TempData["PageFrom"] = "RegFail";

                        return RedirectToAction("MobileRegSuccess", new { regID = personalDetailsVM.RegID });

                    }
                    else
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                            });


                            proxy.RegistrationUpdate(new DAL.Models.Registration()
                            {
                                ID = personalDetailsVM.RegID,
                                LastAccessID = Util.SessionAccess.UserName,
                            });

                        }
                        TempData["PageFrom"] = "RegSuccess";
                        return RedirectToAction("MobileRegSuccess", new { regID = personalDetailsVM.RegID, isClosed = true });

                    }

                }
                #endregion

                #region cancel
                if (collection["submit1"].ToString() == "cancel")
                {
                    string reason = string.Empty;
                    int prevStatus = WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID);
                    reason = ReferenceEquals(reason, null) ? "No reason provided." : collection["hdnReasonCancel"].ToString();

                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)
                        });

                        proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                        {
                            RegID = personalDetailsVM.RegID,
                            CancelReason = reason,
                            CreateDT = DateTime.Now,
                            LastUpdateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });

                        proxy.RegistrationUpdate(new DAL.Models.Registration()
                        {
                            ID = personalDetailsVM.RegID,
                            SalesPerson = Util.SessionAccess.UserName,
                            LastAccessID = Util.SessionAccess.UserName,
                        });

                        var totalPrice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID);
                        if (prevStatus == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment) && WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID) == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan) && totalPrice > 0)
                        {
                            WebHelper.Instance.SendDetailsToWebPos(personalDetailsVM.RegID, Constants.StatusWebPosToCancel);
                        }
                    }


                    return RedirectToAction("MobileRegCancelled", new { regID = personalDetailsVM.RegID });

                }
                #endregion

                #region createAcc
                if (collection["submit1"].ToString() == "createAcc")
                {
                    

                    try
                    {
                        string username = string.Empty;
                        if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                        {
                            username = Request.Cookies["CookieUser"].Value;
                        }
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationUpdate(new DAL.Models.Registration()
                            {
                                ID = personalDetailsVM.RegID,
                                SalesPerson = username,
                                LastAccessID = Util.SessionAccess.UserName,
                            });
                        }


                        //BEGIN   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database


                        using (var proxy = new RegistrationServiceProxy())
                        {
                            if (personalDetailsVM.ExtraTenLimitConfirmation == "true")
                            {

                                if (personalDetailsVM.hdnExtraTenCount != null && personalDetailsVM.hdnExtraTenCount != string.Empty)
                                {
                                    proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                    {
                                        ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                        {
                                            ConfirmedBy = Util.SessionAccess.UserName,
                                            ConfirmedStatus = "Y",
                                            Count = Convert.ToInt32(personalDetailsVM.hdnExtraTenCount),
                                            CreatedDate = System.DateTime.Now,
                                            RegId = personalDetailsVM.RegID
                                        }
                                    });
                                }
                            }

                            if (personalDetailsVM.ExtraTenConfirmation == "true")
                            {

                                proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                {
                                    ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                    {
                                        ConfirmedBy = Util.SessionAccess.UserName,
                                        ConfirmedStatus = "Y",
                                        CreatedDate = System.DateTime.Now,
                                        RegId = personalDetailsVM.RegID
                                    }
                                });

                            }
                            else
                            {
                                proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                {
                                    ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                    {
                                        ConfirmedBy = Util.SessionAccess.UserName,
                                        ConfirmedStatus = "N",
                                        CreatedDate = System.DateTime.Now,
                                        RegId = personalDetailsVM.RegID
                                    }
                                });
                            }


                            proxy.DeleteExtraTenDetails(personalDetailsVM.RegID);
                            if (personalDetailsVM.ExtraTenConfirmation == "true")
                            {
                                foreach (var extratenMSISDN in personalDetailsVM.ExtraTenMobileNumbers.Split(','))
                                {
                                    if (extratenMSISDN != string.Empty)
                                    {
                                        proxy.SaveExtraTen(new ExtraTenReq
                                        {

                                            ExtraTenDetails = new lnkExtraTenDetails { CreatedBy = Util.SessionAccess.UserName, CreatedDate = System.DateTime.Now, Msisdn = extratenMSISDN, RegId = personalDetailsVM.RegID, Status = "A" }


                                        });
                                    }
                                }
                            }
                        }
                        //END   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database

                        return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });

                    }
                    catch (Exception ex)
                    {
                        LogException(ex);
                        WebHelper.Instance.LogExceptions(this.GetType(), ex);
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        TempData["PageFrom"] = "RegFail";

                        return RedirectToAction("MobileRegSuccess", new { regID = personalDetailsVM.RegID });
                    }
                }
                #endregion

                #region activateSvc
                if (collection["submit1"].ToString() == "activateSvc")
                {

                    #region CDPU Approval - status to pending cdpu approval/cdpu rejected
                    if (Session["IsDealer"].ToBool() || Roles.IsUserInRole("MREG_DAPP"))
                    {
                        if (personalDetailsVM.CDPUStatus == Constants.StatusCDPUPending)  // have bre failed
                        {
                            //Update Order Status
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                proxy.RegistrationCancel(new RegStatus()
                                {
                                    RegID = personalDetailsVM.RegID,
                                    Active = true,
                                    CreateDT = DateTime.Now,
                                    StartDate = DateTime.Now,
                                    LastAccessID = Util.SessionAccess.UserName,
                                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCDPUPendingApp)
                                });
                            }
                            return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                        }
                        if (personalDetailsVM.CDPUStatus == Constants.StatusCDPURejected)
                        {
                            GeneralResult result = WebHelper.Instance.UnreserveNumberDealerNet(personalDetailsVM.RegID);
                            if (!ReferenceEquals(result, null))
                            {
                                if (result.ResultCode == 0 && result.ResultMessage == "success")
                                {
                                    using (var proxy = new RegistrationServiceProxy())
                                    {
                                        proxy.RegistrationCancel(new RegStatus()
                                        {
                                            RegID = personalDetailsVM.RegID,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            StartDate = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCDPURejected)
                                        });

                                        proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                                        {
                                            RegID = personalDetailsVM.RegID,
                                            CancelReason = Properties.Settings.Default.Status_RegCDPURejected,
                                            CreateDT = DateTime.Now,
                                            LastUpdateDT = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName
                                        });

                                        proxy.UpsertBRE(new RegBREStatus()
                                        {
                                            RegId = personalDetailsVM.RegID,
                                            TransactionStatus = Constants.StatusCDPURejected,
                                            TransactionReason = personalDetailsVM.CDPUDescription,
                                            TransactionDT = DateTime.Now,
                                            TransactionBy = Util.SessionAccess.UserName
                                        });
                                    }
                                    return RedirectToAction("MobileRegCanceled", new { regID = personalDetailsVM.RegID });
                                }
                            }
                            else
                            {
                                return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                            }
                        }
                        if (personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove)
                        {
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                proxy.UpsertBRE(new RegBREStatus()
                                {
                                    RegId = personalDetailsVM.RegID,
                                    TransactionStatus = Constants.StatusCDPUApprove,
                                    TransactionReason = personalDetailsVM.CDPUDescription,
                                    TransactionDT = DateTime.Now,
                                    TransactionBy = Util.SessionAccess.UserName
                                });
                            }
                        }
                    }
                    #endregion

                    try
                    {

                        using (var proxy = new KenanServiceProxy())
                        {
                            if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                            {
                                
                                CenterOrderContractRequest crpOrder = new CenterOrderContractRequest();
                                crpOrder.orderId = personalDetailsVM.RegID.ToString2();
                                proxy.CenterOrderContractCreation(crpOrder);

                                using (var regProxy = new RegistrationServiceProxy())
                                {
                                    //regProxy.RegStatusCreate(WebHelper.Instance.ConstructRegStatus(Convert.ToInt32(personalDetailsVM.RegID), Properties.Settings.Default.AddRemoveVASReq_ServiceProcessed), null);
                                    regProxy.RegistrationUpdate(new DAL.Models.Registration()
                                    {
                                        ID = personalDetailsVM.RegID,
                                        LastAccessID = Util.SessionAccess.UserName,
                                    });

                                }



                                using (var regProxy = new RegistrationServiceProxy())
                                {

                                    var registration = regProxy.RegistrationGet(personalDetailsVM.RegID);
                                    if (!ReferenceEquals(registration, null) && registration.InternationalRoaming)
                                    {
                                        using (var kenanProxy = new KenanServiceProxy())
                                        {
                                            String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                            if (!ReferenceEquals(Action, String.Empty))
                                                return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                            //kenanProxy.CreateIMPOSFile(personalDetailsVM.RegID);
                                        }
                                    }

                                }
                            }
                        }
                        if (Roles.IsUserInRole("MREG_DAPP"))
                        {
                            return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                        }
                        TempData["PageFrom"] = "StoreKeeper";
                        return RedirectToAction("MobileRegSuccess", new { regID = personalDetailsVM.RegID });
                    }
                    catch (Exception ex)
                    {
                        LogException(ex);
                        WebHelper.Instance.LogExceptions(this.GetType(), ex);
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        TempData["PageFrom"] = "RegFail";

                        return RedirectToAction("MobileRegSuccess", new { regID = personalDetailsVM.RegID });
                    }
                }
                #endregion

                var removedVas = new List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel>();
                bool isInternationalRoamingSelected = false;

                
                if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.Submit)
                {
                    #region submit
                    bool isValid = false;
                    string ErrorMessage = string.Empty;

                    #region Drop 5: BRE Check for non-Drop 4 Flow
                    //07042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - Start
                    var justificationInput = collection["txtJustfication"].ToString2().Trim();
                    var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                    string JustificationMessage = string.Empty;
                    string HardStopMessage = string.Empty;
                    string ApprovalMessage = string.Empty;
                    bool isBREFail = false;
                    WebHelper.Instance.BRECheckMessage(out HardStopMessage, out JustificationMessage, out ApprovalMessage, out isBREFail);

                    //if (!string.IsNullOrEmpty(HardStopMessage) || (!string.IsNullOrEmpty(JustificationMessage) && string.IsNullOrEmpty(justificationInput)))
                    //{
                    //    ViewBag.GSTMark = Settings.Default.GSTMark;
                    //    List<String> LabelReplace = new List<String>();
                    //    LabelReplace.Add("GSTMark");
                    //    ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
                    //    personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
                    //}

                    //if (!string.IsNullOrEmpty(HardStopMessage))
                    //{
                    //    ModelState.AddModelError(string.Empty, HardStopMessage);
                    //    return View(personalDetailsVM);
                    //}
                    //else if (!string.IsNullOrEmpty(JustificationMessage) && string.IsNullOrEmpty(justificationInput))
                    //{
                    //    ModelState.AddModelError(string.Empty, "Please Enter Justification for - " + JustificationMessage);
                    //    return View(personalDetailsVM);
                    //}

                    personalDetailsVM.isBREFail = personalDetailsVM.isBREFail == true 
                                                        ? personalDetailsVM.isBREFail 
                                                        : isBREFail;
                    //07042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - End
                    #endregion

                    //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - Start
                    WebHelper.Instance.ValidateDealerInfo(personalDetailsVM, out isValid, out ErrorMessage);
                    if (!isValid)
                    {
                        ModelState.AddModelError(string.Empty, ErrorMessage);
                        return View(personalDetailsVM);
                    }
                    //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - End

                    // submit registration
                    List<DAL.Models.WaiverComponents> objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                    using (var client = new RegistrationServiceProxy())
                    {
                        resp = client.RegistrationCreateNew(ConstructRegistration(personalDetailsVM, personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, personalDetailsVM.isBREFail),
                                                        ConstructCustomer(), ConstructRegAddress(),
                                                      WebHelper.Instance.ConstructRegStatus(true, personalDetailsVM.isBREFail, Util.SessionAccess.User.isDealer), objWaiverComp, personalDetailsVM.isBREFail);

                        //Create Impos File For AddRemove Vas after                        

                        using (var regProxy = new RegistrationServiceProxy())
                        {


                            var registration = regProxy.RegistrationGet(personalDetailsVM.RegID);
                            if (!ReferenceEquals(registration, null) && registration.InternationalRoaming)
                            {
                                using (var kenanProxy = new KenanServiceProxy())
                                {
                                    String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    if (!ReferenceEquals(Action, String.Empty))
                                        return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //kenanProxy.CreateIMPOSFile(personalDetailsVM.RegID);
                                }
                            }

                        }

                        if (resp.ID > 0)
                        {
                            #region CDPU Approval
                            // Drop 5
                            bool routeToCDPU = isBREFail && Session["IsDealer"].ToBool();
                            if (routeToCDPU)
                            {
                                try
                                {
                                    using (var regProxy = new RegistrationServiceProxy())
                                    {
                                        regProxy.UpsertBRE(new RegBREStatus()
                                        {
                                            RegId = resp.ID,
                                            TransactionStatus = Constants.StatusCDPUPending
                                        });
                                        personalDetailsVM.CDPUStatus = Constants.StatusCDPUPending;
                                    }
                                }
                                catch
                                {

                                }
                            }
                            #endregion
                            //commented as disabling pos file feature
                            //#region Saving POS FileName in DB for Dealers
                            //if ((Session["IsDealer"].ToBool() == true && collection["hdnTotalPayable"].ToString2() != string.Empty && collection["hdnTotalPayable"].ToDecimal() > 0))
                            //{
                            //    string posFileName = WebHelper.Instance.CreatePOSFileName(resp.ID);
                            //    using (var proxy = new CatalogServiceProxy())
                            //    {
                            //        proxy.UpdateIMPOSDetails(resp.ID, posFileName, 1);
                            //    }
                            //}
                            //#endregion


                            personalDetailsVM.RegID = resp.ID;
							using (var catalog = new CatalogServiceProxy())
							{
								string planId = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToString2();
								// Gerry - PBI000000008639 - to prevent duplicate insertion
								List<string> VASList = new List<string>();

								if (Session["AddedVas"] != null)
								{
									var addedVas = (List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel>)Session["AddedVas"];
									if (addedVas != null)
									{
										foreach (var vas in addedVas)
										{
											if (vas != null && vas.compList != null)
											{
												foreach (var comp in vas.compList)
												{
													if (comp.componentId == ConfigurationManager.AppSettings["InternationalRoamingKenanCodes"].ToString2())
														isInternationalRoamingSelected = true;

													// Gerry - PBI000000008639 - to prevent duplicate insertion
													if (!VASList.Contains(comp.componentId))
													{
														CatalogSvc.AddRemoveVASReq req = new CatalogSvc.AddRemoveVASReq();
														req.AddRemoveVASComponents = new AddRemoveVASComponents
														{
															ComponentId = comp.componentId,
															CreatedBy = Util.SessionAccess.UserName,
															CreatedDate = DateTime.Now,
															RegId = resp.ID,
															ModifiedBy = Util.SessionAccess.UserName,
															ModifiedDate = DateTime.Now,
															MSISDN = Session["ExternalID"].ToString(),
															ComponentDesc = comp.componentDesc,
															Status = "A",
															PackageDesc = vas.PackageDesc,
															PackageID = comp.packagedId,
															Price = Convert.ToDecimal(comp.Price),
															lnkPgmBdlPkgCompId = Convert.ToInt32(comp.lnkpgmbdlpkgcompId)
														};
														catalog.SaveAddRemoveVAS(req);

														// to prevent duplicate insertion - PBI000000008639
														VASList.Add(comp.componentId);
													}

												}
											}

										}
									}
								}

								if (Session["RemovedVas"] != null)
								{
									removedVas = (List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel>)Session["RemovedVas"];
									if (removedVas != null)
									{
										VASList = new List<string>();
										foreach (var vas in removedVas)
										{
											if (vas != null && vas.compList != null)
											{
												foreach (var comp in vas.compList)
												{
													if (!VASList.Contains(comp.componentId))
													{
														CatalogSvc.AddRemoveVASReq req = new CatalogSvc.AddRemoveVASReq();
														req.AddRemoveVASComponents = new AddRemoveVASComponents
														{
															ComponentId = comp.componentId,
															RegId = resp.ID,
															CreatedBy = Util.SessionAccess.UserName,
															CreatedDate = DateTime.Now,
															MSISDN = Session[SessionKey.ExternalID.ToString()].ToString(),
															ModifiedBy = Util.SessionAccess.UserName,
															ModifiedDate = DateTime.Now,
															ComponentDesc = comp.componentDesc,
															Status = "D",
															PackageDesc = vas.PackageDesc,
															PackageID = vas.PackageID,
															Price = Convert.ToDecimal(comp.Price),
															lnkPgmBdlPkgCompId = Convert.ToInt32(comp.lnkpgmbdlpkgcompId)
														};
														catalog.SaveAddRemoveVAS(req);

														// to prevent duplicate insertion - PBI000000008639
														VASList.Add(comp.componentId);
													}
												}
											}

										}
									}
								}
                                personalDetailsVM.isNeedIMPOSCreation = IsNeedIMPOSCreation();

                                #region Insert into lnkRegDetails
                                LnkRegDetails objLnkRegDetails = new LnkRegDetails()
                                {
                                    CreatedDate = DateTime.Now,
                                    IsSuppNewAc = false,
                                    RegId = resp.ID,
                                    UserName = Util.SessionAccess.UserName,
                                    IsWaivedOff = personalDetailsVM.isWaiverOff,
                                    Liberlization_Status = personalDetailsVM.Liberlization_Status,
                                    MOC_Status = personalDetailsVM.MOCStatus,
                                    Justification = collection["txtJustfication"].ToString2(),
                                    TotalLineCheckCount = Session["existingTotalLine"].ToInt()
                                };
                                bool IsWriteOff = false;
                                string acc = string.Empty;
                                WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
                                objLnkRegDetails.WriteOffDetails = acc;

                                Session["PDPDVersion"] = null;

                                using (var reglnkProxy = new RegistrationServiceProxy())
                                {
                                    int res = reglnkProxy.SaveLnkRegistrationDetails(new LnkRegDetailsReq
                                    {
                                        LnkDetails = objLnkRegDetails
                                    });
                                }
                                #endregion

                                #region Insert into lnkRegAttributes
                                var regAttribList = new List<RegAttributes>();
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCODE, ATT_Value = personalDetailsVM.OutletCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCHANNELID, ATT_Value = personalDetailsVM.OutletChannelId });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCODE, ATT_Value = personalDetailsVM.SalesCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCHANNELID, ATT_Value = personalDetailsVM.SalesChannelId });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCODE, ATT_Value = personalDetailsVM.SalesStaffCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCHANNELID, ATT_Value = personalDetailsVM.SalesStaffChannelId });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.BillingCycle))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_ACC_BILL_CYCLE, ATT_Value = personalDetailsVM.BillingCycle });

                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_PAYMENT_METHOD, ATT_Value = Util.getPaymentMethodName() });
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_USER_DEVICE_AGENT, ATT_Value = Request.UserAgent });
                                client.SaveListRegAttributes(regAttribList);
                                #endregion

								#region Insert into trnRegJustification
								WebHelper.Instance.saveTrnRegJustification(resp.ID);
                                #endregion

                                #region Insert into trnRegBreFailTreatment
                                WebHelper.Instance.saveTrnRegBreFailTreatment(resp.ID);
                                #endregion

                                var waivedComp = objWaiverComp.Where(e => e.IsWaived == false).ToList();
                                using (var regProxy = new RegistrationServiceProxy())
                                {
                                    if (waivedComp != null && waivedComp.Count > 0)
                                    {
                                        if (!routeToCDPU)
                                        {
                                            using (var kenanProxy = new KenanServiceProxy())
                                            {
                                                String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                                if (!ReferenceEquals(Action, String.Empty))
                                                    return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                                //bool results = kenanProxy.CreateIMPOSFile(personalDetailsVM.RegID);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        personalDetailsVM.isWaiverOff = true; // using this flag to decide whether we given all componets viweroff or no componets to viwer off
                                    }

                                }


                                //}


                                if (orderSummary != null)
                                {
                                    if (Session[SessionKey.RegMobileReg_OrderSummary.ToString()] != null)
                                        orderSummary = (OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()];
                                    CatalogSvc.AddRemoveVASReq req = new CatalogSvc.AddRemoveVASReq();
                                    req.AddRemoveVASComponents = new AddRemoveVASComponents { RegId = resp.ID, ComponentId = orderSummary.SelectedPgmBdlPkgCompID.ToString2(), CreatedBy = Util.SessionAccess.UserName, CreatedDate = DateTime.Now, MSISDN = Session[SessionKey.ExternalID.ToString()].ToString(), ModifiedBy = Util.SessionAccess.UserName, ModifiedDate = DateTime.Now, Status = "P", PackageDesc = orderSummary.PlanName, PackageID = orderSummary.SelectedPgmBdlPkgCompID.ToString2(), Price = Convert.ToDecimal(orderSummary.MonthlySubscription), lnkPgmBdlPkgCompId = Convert.ToInt32(orderSummary.SelectedPgmBdlPkgCompID.ToString()) };
                                    catalog.SaveAddRemoveVAS(req);
                                }

                            }


                            #region Code for SRWWO by chetan
                           
                            if (isInternationalRoamingSelected)
                            {
                                if (Session["AllowSimReplacement"].ToString2() == "True")
                                {
                                    string _selectedexternalID = !string.IsNullOrEmpty(Session[SessionKey.ExternalID.ToString2()].ToString2()) ? Session[SessionKey.ExternalID.ToString2()].ToString2() : null;

                                    var custIdentity = string.Empty;
                                    if (Session[SessionKey.PPID.ToString()] != null)
                                    {
                                        var AcctListByICResponse = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                                        SubscriberICService.CustomizedCustomer customerInfo = new CustomizedCustomer();
                                        SubscriberICService.Items selectedAccount=new Items();
                                        selectedAccount = AcctListByICResponse.itemList.Where(v => v.ExternalId == _selectedexternalID).FirstOrDefault();
                                        customerInfo = selectedAccount.Customer;
                                        if(customerInfo.NewIC!=string.Empty)
                                            custIdentity=customerInfo.NewIC;
                                        else if(customerInfo.PassportNo!=string.Empty)
                                             custIdentity=customerInfo.PassportNo;
                                        else if (customerInfo.OldIC != string.Empty)
                                            custIdentity = customerInfo.OldIC;
                                        else if (customerInfo.OtherIC != string.Empty)
                                            custIdentity = customerInfo.OtherIC;
                                        if (WebHelper.Instance.CompareCMSSDtForAcct(custIdentity) > 14.0)
                                        {
                                            //Commented for timebeing as the service is throwing exception
											var Cresp = WebHelper.Instance.CreateCaseInCMSS(selectedAccount.AcctExtId, Constants.CMSS_CASE_COMPLEXITY, ConfigurationManager.AppSettings["CMSSQueue"], "59", ConfigurationManager.AppSettings["CMSSDirection"], selectedAccount.Account.AccountNumber, selectedAccount.ExternalId,
                                                       (collection["NewCBR"].ToString().Length > 0 ? collection["NewCBR"].ToString() : string.Empty), ConfigurationManager.AppSettings["CMSSProduct"], ConfigurationManager.AppSettings["CMSSReason1"],
                                                      ConfigurationManager.AppSettings["CMSSReason2"], ConfigurationManager.AppSettings["CMSSReason3"], ConfigurationManager.AppSettings["CMSSSysName"], ConfigurationManager.AppSettings["CMSSTitle"], Constants.CMSS_TYPE, Constants.CMSS_CASE_TOPROCEEDWITH,
                                                      resp.ID, custIdentity);
                                            ViewBag.AllowSubmit = "True";
                                            if (Cresp != null && !string.IsNullOrEmpty(Cresp.Id))
                                            {
                                                cmssCaseStatus="Success";
                                                cmssCaseId = Cresp.Id; 
                                            }
                                            else
                                                 cmssCaseStatus="Fail";
                                        }
                                    }
                                }
                            }
                            #endregion upto here code for SRWWO
                            if (lstRebatePenality != null)
                            {
                                List<RegRebateDatacontractPenalty> objlstRebatePenality = new List<RegRebateDatacontractPenalty>();
                                RegRebateDatacontractPenalty objRebateDatacontractPenalty = new RegRebateDatacontractPenalty();
                                LnkLnkRegRebateDatacontractPenaltyReq objRebatePenaltyReq = new LnkLnkRegRebateDatacontractPenaltyReq();
                                foreach (var v in lstRebatePenality)
                                {
                                    objRebateDatacontractPenalty.ID = v.ID;
                                    objRebateDatacontractPenalty.nrcID = v.nrcID;
                                    objRebateDatacontractPenalty.Penalty = v.Penalty;
                                    objRebateDatacontractPenalty.RegID = personalDetailsVM.RegID;
                                    objRebateDatacontractPenalty.Active = v.Active;
                                    objRebateDatacontractPenalty.ComponetID = v.ComponetID;
                                    objRebateDatacontractPenalty.CreateDT = v.CreateDT;
                                    objlstRebatePenality.Add(objRebateDatacontractPenalty);
                                }
                                objRebatePenaltyReq.rebatePenaltyDetails = objlstRebatePenality.ToArray();
                                //Addind penality to session to display in order summary page
                                decimal penality = 0;
                                foreach (var v in lstRebatePenality)
                                {
                                    penality = Convert.ToDecimal(v.Penalty) + penality;
                                }
                                Session["penalty"] = penality;
                                //end
                                using (var proxy = new RegistrationServiceProxy())
                                {
                                    int result = proxy.SaveLnkRegRebateDatacontractPenalty(objRebatePenaltyReq);
                                }

                            }
                        }
                        if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == false)
                        {
                            //BEGIN   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database
                            //added if condition  by Nreddy for checking whether ExtraTenMobileNumbers null or not
                            if (!ReferenceEquals(personalDetailsVM.ExtraTenMobileNumbers, null))
                            {

                                foreach (var extratenMSISDN in personalDetailsVM.ExtraTenMobileNumbers.Split(','))
                                {
                                    if (extratenMSISDN != string.Empty)
                                    {
                                        client.SaveExtraTen(new ExtraTenReq
                                        {
                                            ExtraTenDetails = new lnkExtraTenDetails { CreatedBy = Util.SessionAccess.UserName, CreatedDate = System.DateTime.Now, Msisdn = extratenMSISDN, RegId = resp.ID, Status = "A" }


                                        });
                                    }
                                }

                            }
                            //END   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database
                        }

                    }
                    if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
                    {
                        Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
                    }
                    if (resp.ID > 0)
                    {
                        ClearRegistrationSession();

                        if (string.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove)
                        {
                            //Created by Raj for to submit request to kenan from SA if no IMPOS needed
                            if (personalDetailsVM.isWaiverOff) // if all componets waiwedoff than we need to send to kenan here
                            {
                                GetPaymentAndSendToKenan(personalDetailsVM);
                            }
                        }
                        TempData["PageFrom"] = "SalesAgent";

                        if ((Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("MREG_DAC")) && false)
                        {
                            using (var regProxy = new RegistrationServiceProxy())
                            {
                                var registration = regProxy.RegistrationGet(personalDetailsVM.RegID);
                                if (!ReferenceEquals(registration, null) && registration.InternationalRoaming)
                                {
                                    using (var proxy = new UserServiceProxy())
                                    {
                                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.AddRemoveVAS, "Registration Done with RegID " + resp.ID, Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                                    }
                                }
                                else
                                {
                                    using (var proxy = new UserServiceProxy())
                                    {
                                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.AddRemoveVAS, "Registration Done with RegID " + resp.ID, Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                                    }
                                }
                            }
                        }                       
                        else
                        {
                            using (var proxy = new UserServiceProxy())
                            {
                                proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.AddRemoveVAS, "Registration Done with RegID " + resp.ID, Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                            }
                        }

                       
                        TempData["CMSSCaseID"] = cmssCaseId;
                        TempData["CMSSCaseStatus"] = cmssCaseStatus;
                        return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                    }
                    #endregion
                }
                else if (personalDetailsVM.TabNumber == 3)
                {
                    return RedirectToAction("SelectVAS");
                }
                


            }
            catch (Exception ex)
            {
                Logger.Info("Throw Exception for Order ID:" + resp.ID + "Exception:" + ex);
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

            TempData["PageFrom"] = "RegFail";
          
            return RedirectToAction("MobileRegSuccess", new { regID = personalDetailsVM.RegID });
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC")]
        public ActionResult PersonalDetails()
        {
            if (Session[SessionKey.RegMobileReg_OrderSummary.ToString()] == null)
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = ConstructOrderSummary();

            var idCardTypeID = Session[SessionKey.RegMobileReg_IDCardType.ToString()] != null ? Session[SessionKey.RegMobileReg_IDCardType.ToString()] : null;
            var personalDetailsVM = new PersonalDetailsVM();
            if ((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"] != null)
            {
                personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            }

            personalDetailsVM.Customer.IDCardTypeID = idCardTypeID.ToInt();
            personalDetailsVM.Customer.IDCardNo = Session[SessionKey.RegMobileReg_IDCardNo.ToString()] == null ? "" : Session[SessionKey.RegMobileReg_IDCardNo.ToString()].ToString2();

            return View(personalDetailsVM);
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult PrintMobileRegSummary(FormCollection collection, RegistrationFormVM personalDetailsVM)
        {
            Util.CommonPrintMobileRegSummary(collection);
            
            return RedirectToAction("MobileRegSummary", new { id = collection[1].ToString2() });

        }
        public ActionResult PrintAddRemoveVas(int id)
        {
            var regFormVM = new RegistrationFormVM();
            var modelImageIDs = new List<int>();
            var pbpcIDs = new List<int>();
            var suppLineVASIDs = new List<int>();
            var suppLineForms = new List<SuppLineForm>();
            var regMdlGrpModels = new List<RegMdlGrpModel>();

            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);

            //Joshi Added for DME Printer
            if (Request.Headers["User-Agent"].ToLower().Contains("ipad"))
            {
                var UserDMEPrint = new UserDMEPrint();
                UserDMEPrint.RegID = id;
                UserDMEPrint.UserID = Util.SessionAccess.UserID;
                var status = Util.SaveUserDMEPrint(UserDMEPrint);
                Session["StatusDME"] = status;
            }

            using (var proxy = new RegistrationServiceProxy())
            {
                //Bill Cycle
                regFormVM.RegAttributes = proxy.RegAttributesGetByRegID(id);

                // Registration
                regFormVM.Registration = proxy.RegistrationGet(id);

                // Customer
                regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

                // Address
                var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                {
                    Active = true,
                    Address = new Address()
                    {
                        RegID = id
                    }
                }).ToList();
                regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

                // RegMdlGrpModels
                var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                {
                    RegMdlGrpModel = new RegMdlGrpModel()
                    {
                        RegID = id
                    }
                }).ToList();
                if (regMdlGrpModelIDs.Count() > 0)
                {
                    regMdlGrpModels = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
                    modelImageIDs = regMdlGrpModels.Select(a => a.ModelImageID).ToList();
                }
                // RegPgmBdlPkgComponent
                var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                {
                    RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                    {
                        RegID = id,
                    }
                }).ToList();

                var pbpc = new List<RegPgmBdlPkgComp>();
                if (regPgmBdlPkgCompIDs.Count() > 0)
                    pbpc = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();


                //main line
                if (pbpc.Count() > 0)
                    pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();


                var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = id } }).ToList();
                if (suppLineIDs.Count() > 0)
                {
                    var regSuppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    foreach (var regSuppLine in regSuppLines)
                    {
                        suppLineForms.Add(new SuppLineForm()
                        {
                            // supp line pbpc ID
                            SuppLinePBPCID = regSuppLine.PgmBdlPckComponentID,

                            // supp line vas ID
                            //Commented by VLT on 11 Apr 2013
                            SuppLineVASIDs = pbpc.Where(a => a.RegSuppLineID == regSuppLine.ID).Select(a => a.PgmBdlPckComponentID).ToList(),


                            MSISDN = regSuppLine.MSISDN1
                        });
                    }
                }

            }

            using (var proxy = new CatalogServiceProxy())
            {

                // ModelGroupModel
                if (modelImageIDs.Count() > 0)
                {

                    var modelImages = proxy.BrandArticleModelImageGet(modelImageIDs).ToList();
                    var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());

                    foreach (var regMdlGrpModel in regMdlGrpModels)
                    {
                        var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
                        regFormVM.DeviceForms.Add(new DeviceForm()
                        {
                            Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
                            Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
                            Price = regMdlGrpModel.Price.ToDecimal()
                        });
                    }
                }
                // BundlePackage, PackageComponents
                if (pbpcIDs.Count() > 0)
                {
                    var pgmBdlPkgComps = MasterDataCache.Instance.FilterComponents(pbpcIDs).Distinct().ToList();
                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;

                    regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode).ToList();
                    regFormVM.PackageComponents = pgmBdlPkgComps.Where((a => a.LinkType.ToLower() == "pc" || a.LinkType.ToLower() == "ec" || a.LinkType.ToLower() == "dc" || a.LinkType.ToLower() == "ma")).ToList();
                }

                foreach (var suppLineForm in suppLineForms)
                {
                    //supp line package name
                    suppLineForm.SuppLineBdlPkgName = proxy.PackageGet(MasterDataCache.Instance.FilterComponents(new int[] { suppLineForm.SuppLinePBPCID })
                                                                .Select(a => a.ChildID)).SingleOrDefault().Name;
                    //supp line vas names
                    suppLineForm.SuppLineVASNames = proxy.ComponentGet(MasterDataCache.Instance.FilterComponents(suppLineForm.SuppLineVASIDs).Select(a => a.ChildID).ToList()).Select(a => a.Name).ToList();
                }
                regFormVM.SuppLineForms = suppLineForms;

                //Add / Removed Vases
                //regAddremoveVasCompnents
                AddRemoveVASResp response = proxy.GetAddRemoveVASComponents(Convert.ToInt32(id));
                List<ARVASPackageComponentModel> addeVases = new List<ARVASPackageComponentModel>();
                ARVASPackageComponentModel addPackage = new ARVASPackageComponentModel();
                List<ARVASComponents> addComponents = new List<ARVASComponents>();
                ARVASComponents addComp = null;
                List<ARVASPackageComponentModel> removeVases = new List<ARVASPackageComponentModel>();
                ARVASPackageComponentModel removePackage = new ARVASPackageComponentModel();
                List<ARVASComponents> removeComponents = new List<ARVASComponents>();
                ARVASComponents removeComp = null;
                List<string> vasNames = new List<string>();
                string msisdn = string.Empty;
                if (response.AddRemoveVASComponents != null && response.AddRemoveVASComponents.Count() > 0)
                    msisdn = response.AddRemoveVASComponents[0].MSISDN;

                List<AddRemoveVASComponents> selectedPlan = new List<AddRemoveVASComponents>();
                selectedPlan = response.AddRemoveVASComponents.Where(c => c.Status == "P").ToList();
                if (selectedPlan != null && selectedPlan.Count() > 0)
                {
                    Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = selectedPlan.Select(p => p.lnkPgmBdlPkgCompId).ToList()[0];
                }
                List<string> subscribedComponents = new List<string>();
                List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();

                GetPackageDetails(msisdn, "23", out componentList, ref subscribedComponents);
                if (Session[SessionKey.RegMobileReg_VasNames.ToString()] != null)
                {
                    vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
                }
                int tempAdd = 0;
                int tempRemove = 0;
                List<AddRemoveVASComponents> mandatorycomponents = new List<AddRemoveVASComponents>();
                List<AddRemoveVASComponents> vascomponents = new List<AddRemoveVASComponents>();
                vascomponents = response.AddRemoveVASComponents.ToList();

                if (response.AddRemoveVASComponents != null)
                {
                    IEnumerable<IGrouping<string, AddRemoveVASComponents>> query = response.AddRemoveVASComponents.GroupBy(k => k.PackageDesc);
                    foreach (IGrouping<string, AddRemoveVASComponents> groupItem in query)
                    {
                        addPackage = new ARVASPackageComponentModel();
                        removePackage = new ARVASPackageComponentModel();
                        addComponents = new List<ARVASComponents>();
                        removeComponents = new List<ARVASComponents>();
                        
                        foreach (AddRemoveVASComponents vasItem in groupItem)
                        {
                            if (vasItem.Status.Equals("A"))
                            {
                                tempAdd++;
                                addComp = new ARVASComponents();
                                addComp.componentId = vasItem.ComponentId;
                                addComp.componentDesc = vasItem.ComponentDesc;
                                vasNames.Add(vasItem.ComponentDesc);
                                addComponents.Add(addComp);
                                addPackage.compList = addComponents;
                                addPackage.PackageDesc = vasItem.PackageDesc;
                            }
                            if (vasItem.Status.Equals("D"))
                            {
                                tempRemove++;
                                removeComp = new ARVASComponents();
                                removeComp.componentId = vasItem.ComponentId;
                                removeComp.componentDesc = vasItem.ComponentDesc;
                                vasNames.Remove(vasItem.ComponentDesc);
                                removeComponents.Add(removeComp);
                                removePackage.compList = removeComponents;
                                removePackage.PackageDesc = vasItem.PackageDesc;
                            }

                        }


                        if (tempAdd > 0)
                            addeVases.Add(addPackage);
                        if (tempRemove > 0)
                            removeVases.Add(removePackage);

                        tempAdd = 0;
                        tempRemove = 0;
                    }
                }
                Session[SessionKey.RegMobileReg_VasNames.ToString()] = vasNames;


                Session["AddedVas"] = addeVases;
                Session["RemovedVas"] = removeVases;
                string userICNO = string.Empty;
                if (!ReferenceEquals(Util.SessionAccess.User.IDCardNo, null))
                {
                    userICNO = Util.SessionAccess.User.IDCardNo;
                }



            }
            using (var proxyOrg = new OrganizationServiceProxy())
            {
                if (!ReferenceEquals(regFormVM.Registration, null) && !ReferenceEquals(regFormVM.Registration.CenterOrgID, null) && regFormVM.Registration.CenterOrgID != 0)
                {
                    regFormVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { regFormVM.Registration.CenterOrgID }).FirstOrDefault();
                }
            }

            if (regFormVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == id).FirstOrDefault() != null)
            {
                regFormVM.BillCycle = regFormVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == id).FirstOrDefault().ATT_Value;
            }
            else
            {
                regFormVM.BillCycle = "N/A";
            }

            WebHelper.Instance.MapRegAttributesByRegID(regFormVM, !ReferenceEquals(regFormVM.Registration, null) ? regFormVM.Registration.ID : id);
            //Added to display terms and condition for MI rewards/ Added by Sumit start
            regFormVM.VoiceContractDetails = Util.GetRebateDataContractsBYRegId(id);
            //Added to display terms and condition for MI rewards/ Added by Sumit end
            return View(regFormVM);
        }
        #endregion
        public int PrintDMEOrder(int regId)
        {
            var UserDMEPrint = new UserDMEPrint();
            UserDMEPrint.RegID = regId;
            UserDMEPrint.UserID = Util.SessionAccess.UserID;
            var status = Util.SaveUserDMEPrint(UserDMEPrint);
            return status;
        }
    }
}

