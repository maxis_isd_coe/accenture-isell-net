﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;

using SNT.Utility;
using System.Text;

using Online.Registration.Web.Helper;
using Online.Registration.Web.ViewModels;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ContentRepository;

namespace Online.Registration.Web.Controllers
{
    public class InventoryDashboardController : Controller
    {
        bool isDealer = Util.SessionAccess.User.isDealer;
        /// <summary>
        /// initialize device
        /// </summary>
        /// <returns>list of device + price</returns>
        public ActionResult DeviceAvailability()
        {
            Session[SessionKey.DeviceLowestContractPrice.ToString2()] = null;
            Session[SessionKey.StoreSelected.ToString2()] = null;
            bool bExceptionRaised = false;

            var inventoryDashboardVM = new InventoryDashBoardVM();

            #region for MC only, can search stock in other store
            // get region
            if (!this.isDealer)
            {
                inventoryDashboardVM.regionStores = GetRegionStoreList();
            }
            #endregion

            var phoneVM = WebHelper.Instance.GetHeroDevice(this.GetType(), out bExceptionRaised);
                
            if (bExceptionRaised)
                RedirectToAction("StoreError", "Home");

            inventoryDashboardVM.PhoneVM = phoneVM;
            
            return View(inventoryDashboardVM);
        }

        [HttpPost]
        public ActionResult DeviceAvailability(InventoryDashBoardVM inventoryDashboardVM, FormCollection collection)
        {
            int brandFilter = collection["brandFilter"].ToInt();
            string modelFilter = collection["modelFilter"].ToString2();
            int capacityFilter = collection["capacityFilter"].ToInt();
            int deviceType = collection["deviceTypeSelected"].ToInt();
            // stock filter
            bool availableChecked = collection["availableChecked"].ToBool();
            bool limitedStockChecked = collection["limitedStockChecked"].ToBool();
            bool sellingFastChecked = collection["sellingFastChecked"].ToBool();
            bool outOfStockChecked = collection["outOfStockChecked"].ToBool();
            bool bExceptionRaised = false;

			inventoryDashboardVM.inputBrandSelected = brandFilter;
			inventoryDashboardVM.inputCapacitySelected = capacityFilter;
			inventoryDashboardVM.inputModelSelected = modelFilter;
			inventoryDashboardVM.inputDeviceTypeSelected = deviceType;

			//19052015 - Add brand, model, and capacity selected to inventoryDashboardVM
			List<SelectListItem> listOfAvailableBrand = Util.GetList(RefType.Brand, defaultValue: string.Empty, defaultText: "All Brand");
			inventoryDashboardVM.BrandSelected = listOfAvailableBrand.Where(x => x.Value == brandFilter.ToString()).Select(y => y.Text).FirstOrDefault();
			
			//inventoryDashboardVM.ModelSelected = models.Where(x=> x.ID == modelFilter.ToInt()).FirstOrDefault().Name;
			List<SelectListItem> listOfModel = Util.GetList(RefType.Model, defaultValue: string.Empty, defaultText: "All Model");
			inventoryDashboardVM.ModelSelected = listOfModel.Where(x => x.Value == modelFilter).FirstOrDefault().Text;
			inventoryDashboardVM.ModelSelected = System.Text.RegularExpressions.Regex.Replace(inventoryDashboardVM.ModelSelected, @"\S*GB", string.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
			List<SelectListItem> listOfCapacity = Util.GetList(RefType.Capacity, defaultValue: string.Empty, defaultText: "All");
			inventoryDashboardVM.CapacitySelected = listOfCapacity.Where(x => x.Value == capacityFilter.ToString()).Select(y => y.Text).FirstOrDefault();

            if (!this.isDealer)
            {
                inventoryDashboardVM.OrganizationSelected = collection["StoreNameSelected"].ToString2();
                int organizationID = !string.IsNullOrEmpty(inventoryDashboardVM.OrganizationSelected) ? int.Parse(inventoryDashboardVM.OrganizationSelected) : 0;
                inventoryDashboardVM.PhoneVM = WebHelper.Instance.GetFilteredDevice(this.GetType(), out bExceptionRaised, brandFilter, modelFilter, capacityFilter, deviceType, 0, 0, availableChecked, limitedStockChecked, sellingFastChecked, outOfStockChecked, organizationID);
                inventoryDashboardVM.regionStores = GetRegionStoreList();
                inventoryDashboardVM.regionStoreName = organizationID != 0 ? GetRegionStoreList().Where(x => x.storeID == organizationID).FirstOrDefault().storeName : string.Empty;
            }
            else 
            {
                inventoryDashboardVM.PhoneVM = WebHelper.Instance.GetFilteredDevice(this.GetType(), out bExceptionRaised, brandFilter, modelFilter, capacityFilter, deviceType, 0, 0, availableChecked, limitedStockChecked, sellingFastChecked, outOfStockChecked, 0);
            }

            if (bExceptionRaised)
                RedirectToAction("StoreError", "Home");

            return View(inventoryDashboardVM);
        }

        private List<regionStore> GetRegionStoreList()
        {
            var listRegion = new CatalogServiceProxy().GetRegion();
            var listOrganization = MasterDataCache.Instance.Organization;
            List<regionStore> regionStores = new List<regionStore>();
            foreach (var region in listRegion)
            {
				List<Organization> storeList = new List<Organization>();
				List<int> storeIDList = new List<int>();

				storeList = listOrganization.Organizations.Where(x => x.Active && !string.IsNullOrEmpty(x.WSDLUrl) && x.WSDLUrl.Contains("http://") && x.OrgTypeID == 1 && x.RegionID == region.ID).ToList();
				foreach (var store in storeList)
                {
                    regionStore regionStore = new regionStore();
                    regionStore.regionID = region.ID;
                    regionStore.regionName = region.Name;
					regionStore.storeID = store.ID;
					regionStore.storeName = store.Name;
                    regionStores.Add(regionStore);
                }
            }
            return regionStores;
        }
    }
}
