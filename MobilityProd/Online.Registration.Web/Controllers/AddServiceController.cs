﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.ViewModels;
using SNT.Utility;
using Online.Registration.Web.CommonEnum;
namespace Online.Registration.Web.Controllers
{
    //[HandleError(View = "GenericError")]
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class AddServiceController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AddServiceController));
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BlacklistChecking()
        {
            ClearServiceSession();

            Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;

            var blacklistCheckReq = new BlacklistCheckRequest()
            {

            };

            return View(new PersonalDetailsVM());
        }

        [HttpPost]
        public ActionResult BlacklistChecking(string idCardNo, string idCardTypeID)
        {
            try
            {
                Session[SessionKey.RegMobileReg_IDCardNo.ToString()] = idCardNo;
                Session[SessionKey.RegMobileReg_IDCardType.ToString()] = idCardTypeID;

                var idCardType = new IDCardType();
                using (var proxy = new RegistrationServiceProxy())
                {
                    idCardType = proxy.IDCardTypeGet(idCardTypeID.ToInt());
                }

                using (var proxy = new KenanServiceProxy())
                {
                    var resp = proxy.BusinessRuleCheck(new BusinessRuleRequest()
                    {
                        RuleNames = Properties.Settings.Default.BusinessRule_MOBILERuleNames.Split('|'),
                        IDCardNo = idCardNo,
                        IDCardType = idCardType.KenanCode
                    });

                    Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] = resp.IsBlacklisted;
                    Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = resp.ResultMessage;
                }
            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }

            return Json(Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()].ToBool());
        }

        public ActionResult BlacklistResult(bool result)
        {
            var accounts = new List<FoundAccount>();
            accounts.Add(new FoundAccount()
            {
                AccountNo="109258111",
                ICNo="801010031234",
                MSISDN="60123456789",
                Name="Tan Kok Wai"
            });

            accounts.Add(new FoundAccount()
            {
                AccountNo = "108472942",
                ICNo = "801010031234",
                MSISDN = "60123456009",
                Name = "Tan Kok Wai"
            });

            accounts.Add(new FoundAccount()
            {
                AccountNo = "103958329",
                ICNo = "801010031234",
                MSISDN = "60123381945",
                Name = "Tan Kok Wai"
            });

            ViewBag.Accounts = accounts;
            ViewBag.isBlacklisted = result;

            var resultMsg = Session[SessionKey.RegMobileReg_ResultMessage.ToString()];

            return View();
        }

        [HttpPost]
        public ActionResult BlacklistResult(FormCollection collection)
        {
            try
            {
                var isBlacklist = collection["isBlacklisted"].ToString2();

                if (collection["submit"].ToString2() == "back")
                {
                    return View("BlacklistChecking");
                }
                else if (collection["submit"].ToString2() == "next" || collection["submit"].ToString() == "skip")
                {
                    if (collection["submit"].ToString() == "skip")
                        Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] = (isBlacklist == "True") ? true : false;

                    return RedirectToAction("SelectPlan");
                }

                Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
                Session[SessionKey.TabNumber.ToString()] = "";
                Session[SessionKey.TabValue.ToString()] = "";

            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return View();
        }

        public ActionResult SelectPlan()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SelectPlan(FormCollection collection)
        {
            try
            {
                var page = collection["Page"].ToString();
                var toURL = "SelectVAS";

                if (page == toURL)
                {
                    Session[SessionKey.TabNumber.ToString()] = collection["TabNumber"].ToString();
                    Session[SessionKey.TabValue.ToString()] = collection["TabValue"].ToString();
                    Session[SessionKey.Bundle.ToString()] = collection["Bundle"].ToString();

                    Session[SessionKey.contract_number.ToString()] = "";
                    Session[SessionKey.contract_value.ToString()] = "";
                    Session[SessionKey.vas_number.ToString()] = "";
                    Session[SessionKey.vas_value.ToString()] = "";

                    return RedirectToAction(toURL);
                }
            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return RedirectToAction("BlacklistResult");
        }

        //[Authorize(Roles = "MREG_W,MREG_C")]
        public ActionResult SelectVAS()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SelectVAS(FormCollection collection)
        {
            try
            {
                var page = collection["Page"].ToString();
                var PD = "SelectMobileNo";
                if (page == PD)
                {
                    Session[SessionKey.contract_number.ToString()] = "";
                    Session[SessionKey.contract_value.ToString()] = "";
                    Session[SessionKey.vas_number.ToString()] = "";
                    Session[SessionKey.vas_value.ToString()] = "";

                    return RedirectToAction("SelectMobileNo");
                }
            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }

            return RedirectToAction("SelectPlan");
        }

        public ActionResult SelectMobileNo(string desireNo = "")
        {
            var mobileNos = new MobileNoSelectionResponse();

            using (var proxy = new KenanServiceProxy())
            {
                mobileNos = proxy.MobileNoSelection(new MobileNoSelectionRequest()
                {
                    DesireNo = desireNo
                });
            }

            var mobileNoVM = new MobileNoVM()
            {
                AvailableMobileNos = mobileNos.MobileNos.ToList(),
                NoOfDesireMobileNo = Properties.Settings.Default.NoOfDesireMobileNo
            };

            return View(mobileNoVM);
        }

        [HttpPost]
        public ActionResult SelectMobileNo(FormCollection collection, MobileNoVM mobileNoVM)
        {
            try
            {
                var page = collection["Page"].ToString();
                var toURL = "PersonalDetails";
                Session["DesiredNo"] = mobileNoVM.SelectedMobileNo;

                if (page == toURL)
                {
                    Session[SessionKey.Title.ToString()] = "";
                    Session[SessionKey.Gender.ToString()] = "";
                    Session[SessionKey.FullName.ToString()] = "";
                    Session[SessionKey.DOB.ToString()] = "";
                    Session[SessionKey.Language.ToString()] = "";
                    Session[SessionKey.AlternateContactNo.ToString()] = "";
                    Session[SessionKey.Nationality.ToString()] = "";
                    Session[SessionKey.Email.ToString()] = "";
                    Session[SessionKey.Race.ToString()] = "";
                    Session[SessionKey.Address.ToString()] = "";
                    Session[SessionKey.addLine2.ToString()] = "";
                    Session[SessionKey.Town.ToString()] = "";
                    Session[SessionKey.PostCode.ToString()] = "";
                    Session[SessionKey.State.ToString()] = "";
                    Session[SessionKey.PaymentMode.ToString()] = "";
                    Session[SessionKey.CardType.ToString()] = "";
                    Session[SessionKey.CardNo.ToString()] = "";
                    Session[SessionKey.CardExpiryDate.ToString()] = "";
                    Session[SessionKey.NameOnCard.ToString()] = "";
                    Session[SessionKey.Deposit.ToString()] = "";

                    return RedirectToAction("PersonalDetails");
                }
            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return RedirectToAction("SelectVAS");
        }

        //[Authorize(Roles = "MREG_W,MREG_C")]
        public ActionResult PersonalDetails()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PersonalDetails(FormCollection collection)
        {
            try
            {
                var page = collection["Page"].ToString();
                var toURL = "Summary";

                if (page == toURL)
                {
                    Session[SessionKey.Title.ToString()] = collection["Title"].ToString();
                    Session[SessionKey.Gender.ToString()] = collection["Gender"].ToString();
                    Session[SessionKey.FullName.ToString()] = collection["FullName"].ToString();
                    Session[SessionKey.DOB.ToString()] = collection["DOB"].ToString();
                    Session[SessionKey.Language.ToString()] = collection["Language"].ToString();
                    Session[SessionKey.AlternateContactNo.ToString()] = collection["AlternateContactNo"].ToString();
                    Session[SessionKey.Nationality.ToString()] = collection["Nationality"].ToString();
                    Session[SessionKey.Email.ToString()] = collection["Email"].ToString();
                    Session[SessionKey.Race.ToString()] = collection["Race"].ToString();
                    Session[SessionKey.Address.ToString()] = collection["Address"].ToString();
                    Session[SessionKey.addLine2.ToString()] = collection["addLine2"].ToString();
                    Session[SessionKey.Town.ToString()] = collection["Town"].ToString();
                    Session[SessionKey.PostCode.ToString()] = collection["PostCode"].ToString();
                    Session[SessionKey.State.ToString()] = collection["State"].ToString();
                    Session[SessionKey.PaymentMode.ToString()] = collection["PaymentMode"].ToString();
                    Session[SessionKey.CardType.ToString()] = collection["CardType"].ToString();
                    Session[SessionKey.CardNo.ToString()] = collection["CardNo"].ToString();
                    Session[SessionKey.CardExpiryDate.ToString()] = collection["CardExpiryDate"].ToString();
                    Session[SessionKey.NameOnCard.ToString()] = collection["NameOnCard"].ToString();
                    Session[SessionKey.Deposit.ToString()] = collection["Deposit"].ToString();

                    return RedirectToAction("Summary");
                }
            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return RedirectToAction("SelectVAS");
        }

        public ActionResult Summary()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Summary(FormCollection collection)
        {
            try
            {
                var page = collection["Page"].ToString();
                return RedirectToAction(page);
            }
            catch (Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
        }

        public ActionResult Success(FormCollection collection)
        {
            return View();
        }

        private void ClearServiceSession()
        {
            Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
             Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegStepNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardType.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;

            Session[SessionKey.TabNumber.ToString()] = null;
            Session[SessionKey.TabValue.ToString()] = null;
            Session[SessionKey.Bundle.ToString()] = null;
            Session[SessionKey.contract_number.ToString()] = null;
            Session[SessionKey.vas_number.ToString()] = null;

            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = null;

             Session[SessionKey.RegMobileReg_SublineVM.ToString()] = null;
            Session[SessionKey.RegMobileReg_portInType.ToString()] = null;
            Session[SessionKey.RegMobileReg_donor.ToString()] = null;
            Session[SessionKey.RegMobileReg_MSISDN.ToString()] = null;

            Session[SessionKey.Title.ToString()] = null;
            Session[SessionKey.Gender.ToString()] = null;
            Session[SessionKey.FullName.ToString()] = null;
            Session[SessionKey.DOB.ToString()] = null;
            Session[SessionKey.Language.ToString()] = null;
            Session[SessionKey.AlternateContactNo.ToString()] = null;
            Session[SessionKey.Nationality.ToString()] = null;
            Session[SessionKey.Email.ToString()] = null;
            Session[SessionKey.Race.ToString()] = null;
            Session[SessionKey.Address.ToString()] = null;
            Session[SessionKey.addLine2.ToString()] = null;
            Session[SessionKey.Town.ToString()] = null;
            Session[SessionKey.PostCode.ToString()] = null;
            Session[SessionKey.State.ToString()] = null;
            Session[SessionKey.PaymentMode.ToString()] = null;
            Session[SessionKey.CardType.ToString()] = null;
            Session[SessionKey.CardNo.ToString()] = null;
            Session[SessionKey.CardExpiryDate.ToString()] = null;
            Session[SessionKey.NameOnCard.ToString()] = null;
            Session[SessionKey.Deposit.ToString()] = null;
        }

        private void LogExceptions(Exception ex)
        {
            /* BEGIN  Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/
            string strErrorMsg = Util.LogException(ex);
            string actionName = this.ControllerContext.RouteData.Values["action"] != null ? this.ControllerContext.RouteData.Values["action"].ToString() : "";
            string controllerName = this.ControllerContext.RouteData.Values["controller"] != null ? this.ControllerContext.RouteData.Values["controller"].ToString() : "";
            Logger.Error("Controller: " + controllerName + "\n" + " Action:" + actionName + "\n" + " Error:" + strErrorMsg, ex);
            /* END Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/

        }
    }

    public class FoundAccount
    {
        public string Name { get; set; }
        public string AccountNo { get; set; }
        public string MSISDN { get; set; }
        public string ICNo { get; set; }
    }

}
