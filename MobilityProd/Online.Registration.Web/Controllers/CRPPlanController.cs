﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.Models;
using Online.Registration.Web.SubscriberICService;
using log4net;
using Online.Registration.Web.CommonEnum;
using SNT.Utility;
namespace Online.Registration.Web.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class CRPPlanController : Controller 
    {
        public ActionResult Index()
        {
            return View();
        }


        //[Authorize]
        public ActionResult GetPlanDetails()
        {
            //for user request removing the registration fee for non malysians also in CRP 18092013 by Nredy
            Session["NoRegistrationForCRP"] = (int)MobileRegType.CRP;
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session["ContractType"] = null;
            Session["CRPPenalty"] = null;
            Session["RebatePenalty"] = null;
            Session["HardStopCRP"] = null;
            Session["ShowDComp"] = null;
            TempData["ShowExtendButton"] = null;

            //Session["AccountHolder"] = null;
            WebHelper.Instance.ResetDepositSessions();
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            List<string> suppAccountMsisdns = new List<string>();
            PrincipalAndSupplementaryDetails obj = new PrincipalAndSupplementaryDetails();
            retrieveAcctListByICResponse AcctListByICResponse = null;
            SupplementaryListAccounts PrincipalListAccounts = null;
            SupplementaryListAccountsDatails SuplementarylListAccountsDetails = null;
            AddSuppInquiryAccount AddSuppInquiryAcc = new AddSuppInquiryAccount();
            AddSuppInquiryAccount AddSuppInquiryChild = new AddSuppInquiryAccount();

            ///BELOW ERROR MIGHT GET POPULATED FROM TARGET PAGE
            if (!ReferenceEquals(TempData["Error"], null))
            {
                ModelState.AddModelError("Error", "Select an account");
            }
            //Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()] = "OS";
            ///CHECK PPID AND PPIDINFO SESSIONS
            if ((!ReferenceEquals(Session[SessionKey.PPID.ToString()], null) && "E".Equals(Convert.ToString(Session[SessionKey.PPID.ToString()]))) && !ReferenceEquals(Session[SessionKey.PPIDInfo.ToString()], null))
            {
                ///GET PPIDInfo FROM SESSION
                AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()];
               
                ///LIST ONLY GSM A/C'S
                if (AcctListByICResponse.itemList.Where(c => c.Account != null && c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob == "POSTGSM").Count() > 0)
                {
                    PrincipalListAccounts = new SupplementaryListAccounts();
                    SuplementarylListAccountsDetails = new SupplementaryListAccountsDatails();

                    foreach (var v in AcctListByICResponse.itemList.Where(c => c.Account != null && c.ServiceInfoResponse != null && c.ServiceInfoResponse.prinSuppInd != "" && c.ServiceInfoResponse.lob == "POSTGSM").ToList())
                    {
                        AddSuppInquiryAcc = new AddSuppInquiryAccount();

                        if (v.ServiceInfoResponse.prinSuppInd != "S" ||  (v.IsMISM == true && v.ServiceInfoResponse.prinSuppInd == "S") )
                        {
                            //Changed by Patanjali to support suggestion by wang on 10-03-2013
                            AddSuppInquiryAcc.AccountNumber = v.AcctExtId;// v.Account.AccountNumber,
                            Session[SessionKey.KenanACNumber.ToString()] = AddSuppInquiryAcc.AccountNumber;
                            //Changed by Patanjali to support suggestion by wang on 10-03-2013
                            AddSuppInquiryAcc.ActiveDate = v.Account.ActiveDate;
                            AddSuppInquiryAcc.Address = v.Account.Address;
                            AddSuppInquiryAcc.Category = v.Account.Category;
                            AddSuppInquiryAcc.CompanyName = v.Account.CompanyName;
                            AddSuppInquiryAcc.Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty;
                            AddSuppInquiryAcc.IDNumber = v.Account.IDNumber;
                            AddSuppInquiryAcc.IDType = v.Account.IDType;
                            AddSuppInquiryAcc.MarketCode = v.Account.MarketCode;
                            if (!string.IsNullOrEmpty(v.Account.Plan))
                                AddSuppInquiryAcc.Plan = v.Account.Plan;
                            else
                                AddSuppInquiryAcc.Plan = "0";
                            AddSuppInquiryAcc.SubscribeNo = v.SusbcrNo.ToString2();
                            AddSuppInquiryAcc.SubscribeNoResets = v.SusbcrNoResets.ToString2();
                            AddSuppInquiryAcc.externalId = v.ExternalId;
                            //AddSuppInquiryAcc.AccountType = HasK2Contract(v.ExternalId).ToString();
                            AddSuppInquiryAcc.accountExtId = v.AcctExtId;
                            AddSuppInquiryAcc.accountIntId = v.AcctIntId;
                            AddSuppInquiryAcc.AccountStatus = v.ServiceInfoResponse.serviceStatus;
                            AddSuppInquiryAcc.IsMISM = v.IsMISM;

                            string idCardNo = string.Empty;
                            string idCardTypeID = string.Empty;
                            idCardNo = Session[SessionKey.IDCardNo.ToString()].ToString2();
                            idCardTypeID = Session[SessionKey.IDCardTypeID.ToString()].ToString2();
                            var idCardType = new IDCardType();
                            var resp = new BusinessRuleResponse();

                            using (var proxy = new RegistrationServiceProxy())
                            {
                                idCardType = proxy.IDCardTypeGet(Convert.ToInt32(idCardTypeID));
                            }

                            var req = new BusinessRuleRequest();
                            req.RuleNames = "OutstandingCreditCheck".Split(' ');
                            req.kenanExtAcctId = v.AcctExtId.ToString2();
                            req.IDCardNo = idCardNo;
                            req.IDCardType = idCardType.KenanCode;

                            resp = BREChecker.validate(req);

                            DateTime dtRes = DateTime.Now;
                            string xmlReq = Util.ConvertObjectToXml(req);
                            string xmlRes = Util.ConvertObjectToXml(resp);

                            AddSuppInquiryAcc.outstandingAmount = resp.IsOutstandingCreditCheckFailed == true ? "1" : "0";

                            if (v.IsMISM)
                            {
                                obj.principalDetails.Add(AddSuppInquiryAcc);

                                if (v.SecondarySimList != null)
                                {
                                    foreach (MsimDetails secon in v.SecondarySimList)
                                    {
                                        AddSuppInquiryAcc = new AddSuppInquiryAccount();
                                        var seconAccount = AcctListByICResponse.itemList.Where(i => i.ExternalId == secon.Msisdn && i.ServiceInfoResponse.prinSuppInd == "S" && i.IsMISM).FirstOrDefault();

                                        AddSuppInquiryAcc.AccountNumber = seconAccount.Account.AccountNumber;
                                        //Changed by Patanjali to support suggestion by wang on 10-03-2013
                                        AddSuppInquiryAcc.ActiveDate = seconAccount.Account.ActiveDate;
                                        AddSuppInquiryAcc.Address = seconAccount.Account.Address;
                                        AddSuppInquiryAcc.Category = seconAccount.Account.Category;
                                        AddSuppInquiryAcc.CompanyName = seconAccount.Account.CompanyName;
                                        AddSuppInquiryAcc.Holder = seconAccount.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty;
                                        AddSuppInquiryAcc.IDNumber = seconAccount.Account.IDNumber;
                                        AddSuppInquiryAcc.IDType = seconAccount.Account.IDType;
                                        AddSuppInquiryAcc.MarketCode = seconAccount.Account.MarketCode;
                                        if (!string.IsNullOrEmpty(seconAccount.Account.Plan))
                                            AddSuppInquiryAcc.Plan = seconAccount.Account.Plan;
                                        else
                                            AddSuppInquiryAcc.Plan = "0";
                                        AddSuppInquiryAcc.SubscribeNo = seconAccount.SusbcrNo.ToString2();
                                        AddSuppInquiryAcc.SubscribeNoResets = seconAccount.SusbcrNoResets.ToString2();
                                        AddSuppInquiryAcc.externalId = seconAccount.ExternalId;
                                        //AddSuppInquiryAcc.AccountType = HasK2Contract(v.ExternalId).ToString();
                                        AddSuppInquiryAcc.accountExtId = seconAccount.AcctExtId;
                                        AddSuppInquiryAcc.accountIntId = seconAccount.AcctIntId;
                                        AddSuppInquiryAcc.AccountStatus = seconAccount.ServiceInfoResponse.serviceStatus;
                                        AddSuppInquiryAcc.IsMISM = seconAccount.IsMISM;

                                        obj.principalDetails.Add(AddSuppInquiryAcc);
                                    }
                                }

                            }
                            else
                            {
                                using (var PrinSupproxy = new Online.Registration.Web.Helper.PrinSupServiceProxy())
                                {

                                    var req2 = new SubscribergetPrinSuppRequest();
                                    retrievegetPrinSuppResponse response = null;
                                    req2.eaiHeader = null;
                                    req2.msisdn = v.ExternalId;

                                    response = PrinSupproxy.getPrinSupplimentarylines(req2);
                                    if (response != null)
                                    {
                                        foreach (var suppline in response.itemList)
                                        {
                                            List<string> lst = new List<string>();
                                            lst = suppAccountMsisdns.Where(m => m.ToString() == suppline.msisdnField).ToList();
                                            if (lst == null || lst.Count <= 0)
                                            {

                                                suppAccountMsisdns.Add(v.ExternalId);

                                                var suppAccount = AcctListByICResponse.itemList.Where(i => i.ExternalId == suppline.msisdnField && i.ServiceInfoResponse.prinSuppInd == "S" && !i.IsMISM).FirstOrDefault();
                                                AddSuppInquiryChild = new AddSuppInquiryAccount();
                                                AddSuppInquiryChild.SubscribeNo = suppline.subscr_noField;

                                                if (suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctIntId))
                                                    AddSuppInquiryChild.accountIntId = suppAccount.AcctIntId;
                                                else
                                                    AddSuppInquiryChild.accountIntId = suppline.acct_noField;

                                                AddSuppInquiryChild.accountExtId = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId;

                                                //Added by Rajender
                                                if (!string.IsNullOrEmpty(suppline.subscr_statusField))
                                                {
                                                    if (suppline.subscr_statusField.ToLower().Contains("suspend"))
                                                        AddSuppInquiryChild.AccountStatus = "S";
                                                    else if (suppline.subscr_statusField.ToLower().Contains("deactive"))
                                                        AddSuppInquiryChild.AccountStatus = "D";
                                                    else if (suppline.subscr_statusField.ToLower().Contains("terminated"))
                                                        AddSuppInquiryChild.AccountStatus = "T";
                                                    else if (suppline.subscr_statusField.ToLower().Contains("barred"))
                                                        AddSuppInquiryChild.AccountStatus = "B";
                                                    else
                                                        AddSuppInquiryChild.AccountStatus = "A";
                                                }
                                                //end

                                                AddSuppInquiryChild.SubscribeNo = suppline.subscr_noField;
                                                AddSuppInquiryChild.SubscribeNoResets = suppline.subscr_no_resetsField;
                                                AddSuppInquiryChild.IDNumber = suppline.msisdnField;
                                                AddSuppInquiryChild.externalId = suppline.acc_AccExtId;
                                                AddSuppInquiryChild.AccountNumber = suppline.acct_noField;
                                                if (!string.IsNullOrEmpty(suppline.cust_nmField))
                                                {
                                                    AddSuppInquiryChild.AccountName = suppline.cust_nmField;
                                                }
                                                else
                                                {
                                                    AddSuppInquiryChild.AccountName = v.Account.Holder;
                                                }
                                                AddSuppInquiryChild.Holder = v.Account.Holder;
                                                AddSuppInquiryChild.Category = v.ExternalId;
                                                AddSuppInquiryChild.IsMISM = suppline.IsMISM;
                                                AddSuppInquiryChild.outstandingAmount = AddSuppInquiryAcc.outstandingAmount;

                                                obj.supplementaryDetails.Add(AddSuppInquiryChild);
                                            }
                                            //SuplementarylListAccountsDetails.SuppListAccountsDetails.Add(AddSuppInquiryAcc);
                                        }
                                        AddSuppInquiryAcc.Plan = response.itemList.Count.ToString2();

                                    }
                                    else
                                    {
                                        AddSuppInquiryAcc.Plan = "0";
                                    }
                                    obj.principalDetails.Add(AddSuppInquiryAcc);
                                }

                                using (var proxy = new UserServiceProxy())
                                {
                                    proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.DeviceCRP), "Change Rate Plan", Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                                }

                                obj.principalDetails.Add(AddSuppInquiryAcc);
                            }

                        }
                    }
                }

            }
            //Session["PrincipalDetails"] = obj.principalDetails;
            //Session["SupplementaryDetails"] = obj.supplementaryDetails;
            Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()] = "ADS";
            return View(obj);
        }

        public string checkCRPHardStop(string pMsisdn, string action, string sMsisdn = "")
        {
            string hardStopCrp = "false";
            bool isValid = WebHelper.Instance.DucklingComponentCheck(pMsisdn, action,sMsisdn);
            if (isValid.ToString() == "False")
            {
                hardStopCrp = Session["HardStopCRP"].ToString2();
            }
            return hardStopCrp;
        }

    }
}
