﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Text;
using System.Web.Routing;
using System.Data.Objects;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;

using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.DAL.Models;
using Online.Registration.Web.SubscriberICService;

using SNT.Utility;

using log4net;
using TCMSCLibrary;
using Online.Registration.Web.breService;
using Online.Registration.Web.Models;
using Online.Registration.Web.CommonEnum;

namespace Online.Registration.Web.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class 
        MNPController : Controller
    {
        #region Members

        private static readonly ILog Logger = LogManager.GetLogger(typeof(MNPController));

        #endregion

        #region Actions

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult BlacklistChecking()
        {
            ClearMNPSession();

            Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.PlanOnly;

            var blacklistCheckReq = new BlacklistCheckRequest()
            {

            };

            return View(new PersonalDetailsVM());
        }



        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult BlacklistResult(bool result)
        {
            ViewBag.isBlacklisted = result;

            var resultMsg = Session[SessionKey.RegMobileReg_ResultMessage.ToString()];

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult BlacklistResult(FormCollection collection)
        {
            try
            {
                var isBlacklist = collection["isBlacklisted"].ToString();

                if (collection["submit"].ToString2() == "back")
                {
                    return View("BlacklistChecking");
                }
                else if (collection["submit"].ToString2() == "next" || collection["submit"].ToString() == "skip")
                {
                    if (collection["submit"].ToString() == "skip")
                        Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] = (isBlacklist == "True") ? true : false;

                    return RedirectToAction("EnterDetails");
                }

                Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;

            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            return View();
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult EnterDetails()
        {

            Session["FromMNP"] = null;
            Session["MNPSupplementaryLines"] = null;
            ViewBag.IsConsumer = Convert.ToString(Properties.Settings.Default.IsConsumer);

            ViewBag.MNPMaxSupplineToPort = Convert.ToInt32(Properties.Settings.Default.MNPMaxSupplineToPort); ///MAXIMUM NUMBER OF SUPPLINE TO BE PORTED
            Session[SessionKey.MenuType.ToString()] = (int)MenuType.MNPStatusTrack;
            return View();
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult EnterDetails(FormCollection collection)
        {
            try
            {
                Session[SessionKey.RegMobileReg_donor.ToString()] = collection["hdnDonorID"].ToString2();
                ///FOLLOWING SETS THE PRESELECTED NUMBER FOR MNP
                Session[SessionKey.RegMobileReg_MSISDN.ToString()] = collection["MSISDN"].ToString2();
                Session[SessionKey.MenuType.ToString()] = (int)MenuType.MNPRegistration;
                ///MNP MULTIPORT RELATED CHANGES
                ///TO AVOID SHARING OF PRIMARY LINE RELATED SESSIONS NEW STRONG TYPE OF DATA INTRODUCED
                ///BELOW CHECKS ENSURES THE REQUEST IS FOR MNP AND SUPPLEMENTARY LINES FOR MULTI PORT IN EXISTS
                bool isMultipleMNP = ((!ReferenceEquals(Session["FromMNP"], null) && Convert.ToBoolean(Session["FromMNP"])) && (!ReferenceEquals(Session["MNPSupplementaryLines"], null) && (!ReferenceEquals(((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"]).SupplimentaryMSISDNs, null) && ((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"]).SupplimentaryMSISDNs.Count > 0)));
                if (isMultipleMNP)
                    Session["MenuType"] = (int)MenuType.MNPPlanWithMultiSuppline;
                Session[SessionKey.TabNumber.ToString()] = string.Empty;
                Session[SessionKey.TabValue.ToString()] = string.Empty;

                Session["IsOlo"] = collection["hdnOlo"].ToString2() == "1" ? 1 : 0;

                string hdnMNPFlowType = collection["hdnMNPFlowType"].ToString2() == string.Empty ? "APL" : collection["hdnMNPFlowType"].ToString2();
                string hdnDealerFlow = collection["hdnDealerFlow"].ToString2() == string.Empty ? "false" : collection["hdnDealerFlow"].ToString2();

                if (Convert.ToBoolean(hdnDealerFlow))
                {
                    switch (hdnMNPFlowType)
                    {
                        case "APL": //add plan only 
                            return RedirectToAction("SelectPlan", "Registration", new { type = 2 });
                            break;
                        case "ASL": // add suppline only
                            return RedirectToAction("AddSuppline", "Suppline", new { type = 8 });
                            break;
                        case "ANL": // add new line only
                            return RedirectToAction("AddNewLine", "Newline", new { type = 9 });
                            break;
                        default:
                            break;
                    }
                }
                return RedirectToAction("SelectPlanNew", "Registration", new { type = 2 });
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
        }

        [HttpPost]
        public string ResubmitMNP(string msisdn, bool isROC)
        {
            Logger.InfoFormat("Entering {0} MSISDN({1}) isROC({2})", "ResubmitMNP", msisdn, isROC.ToString2());
            bool isSuperAdmin = false;
            //CustomizedCustomer customer = null;
            bool isBRN = false;
            StoreKeeperVM storeKeeperVM = new StoreKeeperVM();
			string result = "";

            if (Session[SessionKey.isFromBRNSearch.ToString()].ToString2().ToLower() == "true")
                isBRN = true;

            if (string.IsNullOrEmpty(msisdn))
            {
                Logger.InfoFormat("Entering {0} MSISDN({1}) isROC({2}) MSISDN is blank", "ResubmitMNP", msisdn, isROC.ToString2());
                return "MSISDN is blank";
            }
            else
            {
                // do checking here
                using (var proxy = new retrieveServiceInfoProxy())
                {
                    //customer = proxy.retrieveSubscriberDetls(Msisdn: msisdn, loggedUserName: Request.Cookies["CookieUser"].Value, isSupAdmin: isSuperAdmin);
                }
                try
                {
                    RegistrationFind oReq = new RegistrationFind();
                    oReq.Registration.MSISDN1 = msisdn;
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        List<int> ListIds = new List<int>();
                        // Get Registration for the selected MSISDN
                        // Will return the list of Reg id's
                        var regIDList = proxy.RegistrationFind(oReq);

						if (ReferenceEquals(regIDList, null) || regIDList.Count() == 0)
							return ConfigurationManager.AppSettings["MNP_OrderFromKenanMsg"].ToString2();

                        int maxRegID = regIDList.Max();
                        //var regstatusDetails = proxy.RegStatusGet1(new RegStatusFind() { RegistrationIDs = regIDList.ToList() });
                        var regstatusDetails = proxy.RegStatusGet1(new RegStatusFind() { RegistrationIDs = new List<int> { maxRegID } });

                        if (regstatusDetails.Count() <= 0)
                        {
                            Logger.InfoFormat("Exiting {0} MSISDN({1}) isROC({2}) MSISDN is already in Process", "ResubmitMNP", msisdn, isROC.ToString2());
                            return "MSISDN is already in Process";
                        }
                        /*
                        foreach (RegStatus regSt in regstatusDetails)
                        {
                            ListIds.Add(regSt.ID);
                        }
                        if (ListIds.Count() <= 0)
                        {
                            return "MSISDN is already in Process";
                        }
                        var previousStatusID = ListIds.OrderByDescending(n => n).Take(1);
                        //var lstStatus = regstatusDetails.Where(a => a.ID == previousStatusID.ToInt()).SingleOrDefault();
                        //Session["MNPLastRegStatus"] = lstStatus;

                        var regStatus = regstatusDetails.Where(a => a.ID == ListIds.Max()).SingleOrDefault();
                        */
						var reg = proxy.GetRegistrationFullDetails(maxRegID);
                        if (isROC) // Reject with order cancelled
                        {
                            bool isError = false;
                            string ErrMsg = string.Empty;
                            List<string> supp = new List<string>();
                            try
                            {
                                // query reg, cust, supp
                                int customerID = reg.Customers.FirstOrDefault().ID;
                                var cust = proxy.CustomerGet(new int[] { customerID }).FirstOrDefault();
                                var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = reg.Regs.FirstOrDefault().ID } }).ToList();
                                if (suppLineIDs.Count() > 0)
                                {
                                    var regSuppLinesList = proxy.RegSuppLineGet(suppLineIDs).ToList();
                                    supp = regSuppLinesList.Select(a => a.MSISDN1).ToList();
                                }
                                string donorID = reg.Regs.FirstOrDefault().DonorId;
                                string customerName = cust.FullName;

                                // set session
                                Session[SessionKey.IDCardTypeID.ToString2()] = cust.IDCardTypeID;
                                Session[SessionKey.IDCardNo.ToString2()] = cust.IDCardNo;
                                // prePortInValidation
                                MNPBusinessValidationPreportInChecks(donorID, msisdn, customerName, string.Empty, supp, out isError, out ErrMsg);
                                if (isError)
                                {
                                    Logger.InfoFormat("Exiting {0} MSISDN({1}) isROC({2}) {3}", "ResubmitMNP", msisdn, isROC.ToString2(), ErrMsg);
                                    return ErrMsg;
                                }
                                proxy.RegistrationUpdate(new DAL.Models.Registration
                                {
                                    ID = reg.Regs.FirstOrDefault().ID,
                                    PrePortReqId = Session[SessionKey.Master_PrePortReqId.ToString()].ToString2()
                                });
                                // createCenterOrderMNP
								result = WebHelper.Instance.ReSubmitMNPOrder(reg, maxRegID, isROC);

                                // clear session
                                if (!ReferenceEquals(SessionKey.RegMobileReg_IsPortInDonerCheckFailed.ToString(), null)) Session.Contents.Remove(SessionKey.RegMobileReg_IsPortInDonerCheckFailed.ToString2());
                                if (!ReferenceEquals(SessionKey.RegMobileReg_IsPortInDonerMessage.ToString(),null)) Session.Contents.Remove(SessionKey.RegMobileReg_IsPortInDonerMessage.ToString());
                                if (!ReferenceEquals(SessionKey.RegMobileReg_IsPortInStatusCode.ToString(),null)) Session.Contents.Remove(SessionKey.RegMobileReg_IsPortInStatusCode.ToString());
                                if (!ReferenceEquals(SessionKey.Master_PrePortReqId.ToString(),null)) Session.Contents.Remove(SessionKey.Master_PrePortReqId.ToString());
                                if (!ReferenceEquals(SessionKey.IDCardTypeID.ToString(), null)) Session.Contents.Remove(SessionKey.IDCardTypeID.ToString());
                                if (!ReferenceEquals(SessionKey.IDCardNo.ToString(), null)) Session.Contents.Remove(SessionKey.IDCardNo.ToString());
                            }
                            catch (Exception ex)
                            {
                                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                            }
                        }
                        else // Reject
                        {
							result = WebHelper.Instance.ReSubmitMNPOrder(reg, maxRegID);
                        }
                    }

                    #region "Status Updation"
                    string username = string.Empty;
                    if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                    {
                        username = Request.Cookies["CookieUser"].Value;
                    }
                    #endregion
                    Logger.InfoFormat("Exiting {0} MSISDN({1}) isROC({2})", "ResubmitMNP", msisdn, isROC.ToString2());
					return result;
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
        }

        public ActionResult StatusTracking()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        [Authorize]
        public ActionResult StatusTracking(StatusTrackingVM model)
        {
            var statusTrackModel = model;

            try
            {
                if ((!ReferenceEquals(model, null)) && (model.MSISDN.ToString() != string.Empty))
                {

                    getMNPRequest _request = new getMNPRequest();
                    getMNPRequestResponse _response = new getMNPRequestResponse();

                    // Drop 4
                    _request.reqType = !string.IsNullOrEmpty(Properties.Settings.Default.StatusTrack_RequestType) ? Properties.Settings.Default.StatusTrack_RequestType : "MSISDN";

                    _request.reqValue = model.MSISDN.ToString().Trim();
                    _request.portReqType = !string.IsNullOrEmpty(Properties.Settings.Default.StatusTrack_PortIn) ? Properties.Settings.Default.StatusTrack_PortIn : "I";

                    using (var proxyK = new KenanServiceClient())
                    {
                        _response = proxyK.GetMNPRequest(_request);
                    }

                    if (_response.RequestDetails.Count() == 0)
                    {
                        model = null;
                        throw new Exception(Properties.Settings.Default.StatusTrack_ErrorMessage);
                    }

                    MnpRequestDetails[] mnpResults = _response.RequestDetails;
                    statusTrackModel.MNPReqList = mnpResults;
                }
                else
                {
                    /// need to write logic for msisdn failure
                }
            }
            catch (Exception ex)
            {

                WebHelper.Instance.LogExceptions(this.GetType(), ex);

                ViewBag.ErrorMessage = ex.Message;
            }
            /// this code need to be properly 
            return View(model);
        }



        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult SelectPlan()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult SelectPlan(FormCollection collection)
        {
            try
            {
                var page = collection["Page"].ToString();
                var toURL = "SelectVAS";

                if (page == toURL)
                {
                    Session[SessionKey.TabNumber.ToString()] = collection["TabNumber"].ToString();
                    Session[SessionKey.TabValue.ToString()] = collection["TabValue"].ToString();
                    Session[SessionKey.Bundle.ToString()] = collection["Bundle"].ToString();

                    Session[SessionKey.contract_number.ToString()] = "";
                    Session[SessionKey.contract_value.ToString()] = "";
                    Session[SessionKey.vas_number.ToString()] = "";
                    Session[SessionKey.vas_value.ToString()] = "";

                    return RedirectToAction(toURL);
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            return RedirectToAction("EnterDetails");
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult SelectVAS()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult SelectVAS(FormCollection collection)
        {
            try
            {
                var page = collection["Page"].ToString();
                var PD = "PersonalDetails";
                if (page == PD)
                {
                    Session[SessionKey.contract_number.ToString()] = "";
                    Session[SessionKey.contract_value.ToString()] = "";
                    Session[SessionKey.vas_number.ToString()] = "";
                    Session[SessionKey.vas_value.ToString()] = "";
                    Session[SessionKey.Title.ToString()] = "";
                    Session[SessionKey.Gender.ToString()] = "";
                    Session[SessionKey.FullName.ToString()] = "";
                    Session[SessionKey.DOB.ToString()] = "";
                    Session[SessionKey.Language.ToString()] = "";
                    Session[SessionKey.AlternateContactNo.ToString()] = "";
                    Session[SessionKey.Nationality.ToString()] = "";
                    Session[SessionKey.Email.ToString()] = "";
                    Session[SessionKey.Race.ToString()] = "";
                    Session[SessionKey.Address.ToString()] = "";
                    Session[SessionKey.addLine2.ToString()] = "";
                    Session[SessionKey.Town.ToString()] = "";
                    Session[SessionKey.PostCode.ToString()] = "";
                    Session[SessionKey.State.ToString()] = "";
                    Session[SessionKey.PaymentMode.ToString()] = "";
                    Session[SessionKey.CardType.ToString()] = "";
                    Session[SessionKey.CardNo.ToString()] = "";
                    Session[SessionKey.CardExpiryDate.ToString()] = "";
                    Session[SessionKey.NameOnCard.ToString()] = "";
                    Session[SessionKey.Deposit.ToString()] = "";
                    return RedirectToAction("PersonalDetails");
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

            return RedirectToAction("SelectPlan");
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult PersonalDetails()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult PersonalDetails(FormCollection collection)
        {
            try
            {
                var page = collection["Page"].ToString();
                var toURL = "Summary";

                if (page == toURL)
                {
                    Session[SessionKey.Title.ToString()] = collection["Title"].ToString();
                    Session[SessionKey.Gender.ToString()] = collection["Gender"].ToString();
                    Session[SessionKey.FullName.ToString()] = collection["FullName"].ToString();
                    Session[SessionKey.DOB.ToString()] = collection["DOB"].ToString();
                    Session[SessionKey.Language.ToString()] = collection["Language"].ToString();
                    Session[SessionKey.AlternateContactNo.ToString()] = collection["AlternateContactNo"].ToString();
                    Session[SessionKey.Nationality.ToString()] = collection["Nationality"].ToString();
                    Session[SessionKey.Email.ToString()] = collection["Email"].ToString();
                    Session[SessionKey.Race.ToString()] = collection["Race"].ToString();
                    Session[SessionKey.Address.ToString()] = collection["Address"].ToString();
                    Session[SessionKey.addLine2.ToString()] = collection["addLine2"].ToString();
                    Session[SessionKey.Town.ToString()] = collection["Town"].ToString();
                    Session[SessionKey.PostCode.ToString()] = collection["PostCode"].ToString();
                    Session[SessionKey.State.ToString()] = collection["State"].ToString();
                    Session[SessionKey.PaymentMode.ToString()] = collection["PaymentMode"].ToString();
                    Session[SessionKey.CardType.ToString()] = collection["CardType"].ToString();
                    Session[SessionKey.CardNo.ToString()] = collection["CardNo"].ToString();
                    Session[SessionKey.CardExpiryDate.ToString()] = collection["CardExpiryDate"].ToString();
                    Session[SessionKey.NameOnCard.ToString()] = collection["NameOnCard"].ToString();
                    Session[SessionKey.Deposit.ToString()] = collection["Deposit"].ToString();
                    return RedirectToAction("Summary");
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            return RedirectToAction("SelectVAS");
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult Summary()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult Summary(FormCollection collection)
        {
            try
            {
                var page = collection["Page"].ToString();
                return RedirectToAction(page);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult Success(FormCollection collection)
        {
            return View();
        }

        #endregion

        #region Ajax Json Call

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult BlacklistChecking(string idCardNo, string idCardTypeID)
        {
            try
            {
                Session[SessionKey.RegMobileReg_IDCardNo.ToString()] = idCardNo;
                Session[SessionKey.RegMobileReg_IDCardType.ToString()] = idCardTypeID;

                var idCardType = new IDCardType();
                using (var proxy = new RegistrationServiceProxy())
                {
                    idCardType = proxy.IDCardTypeGet(idCardTypeID.ToInt());
                }

                using (var proxy = new KenanServiceProxy())
                {
                    var resp = proxy.BusinessRuleCheck(new BusinessRuleRequest()
                    {
                        RuleNames = Properties.Settings.Default.BusinessRule_MOBILERuleNames.Split('|'),
                        IDCardNo = idCardNo,
                        IDCardType = idCardType.KenanCode
                    });

                    Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] = resp.IsBlacklisted;
                    Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = resp.ResultMessage;
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

            return Json(Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()].ToBool());
        }

        /// <summary>
        /// New business rule checks relating to MNP MNPServiceIdCheck and PrePortInCheck.
        /// </summary>
        /// <param name="DonorID">The donor ID.</param>
        /// <param name="MSISDN">The MSISDN.</param>
        /// <param name="CustomerName">Name of the customer.</param>
        /// <param name="CompanyName">Name of the company.</param>
        /// <param name="Supp">The supplementary lines.</param>
        /// <returns>JSon</returns>
        [HttpPost]
        [DoNoTCache]
        public ActionResult PortInBusinessRuleChecks(string DonorID, string MSISDN, string CustomerName, string CompanyName, List<string> Supp)
        {
            bool isError = false;
            string ErrorMsg = string.Empty;
            MNPSelectPlanForSuppline Suppline = null;

            var resp = new BusinessRuleResponse();

            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
            {
                Session.Contents.Remove("FromMNP");
            }

            if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
            {
                Session.Contents.Remove("MNPSupplementaryLines");
            }

            ///FOR PRIMARY LINE
            MNPBusinessValidationChecks(DonorID: DonorID, MSISDN: MSISDN, CustomerName: CustomerName, CompanyName: CompanyName, Supp: null, isError: out isError, ErrorMsg: out ErrorMsg);

            ///PLEASE UNCOMENT BELOW WHEN TESTING IS DONE
            ///ERROR WITH MNPServiceIdCheck END THE PROCESS 
            if (isError)
            {
                var jsonData = new { isError, ErrorMsg };
                return Json(jsonData);
            }
            ///FOR SUPPLEMENTARY LINE
            if (!ReferenceEquals(Supp, null))
            {
                MNPBusinessValidationChecks(DonorID: DonorID, MSISDN: MSISDN, CustomerName: CustomerName, CompanyName: CompanyName, Supp: Supp, isError: out isError, ErrorMsg: out ErrorMsg);
            }
            ///PLEASE UNCOMENT BELOW WHEN TESTING IS DONE
            ///ERROR WITH MNPServiceIdCheck END THE PROCESS 
            if (isError)
            {
                var jsonData = new { isError, ErrorMsg };
                return Json(jsonData);
            }

            #region "Preport-In check"
            MNPBusinessValidationPreportInChecks(DonorID: DonorID, MSISDN: MSISDN, CustomerName: CustomerName, CompanyName: CompanyName, Supp: Supp, isError: out isError, ErrorMsg: out ErrorMsg);

            #endregion
            if (isError)
            {
                var jsonData = new { isError, ErrorMsg };
                return Json(jsonData);
            }
            else
            {
                Session[SessionKey.MenuType.ToString()] = (int)MenuType.MNPRegistration;
                Session[SessionKey.FromMNP.ToString()] = true;
                Session[SessionKey.SimType.ToString()] = "Normal";

                ///SUPPLEMENTARY LINE EXISTS
                if (!ReferenceEquals(Supp, null) && Supp.Count > 0)
                {
                    //Session[SessionKey.MNPSupplementaryLines.ToString()] = Supp;

                    ///CONTINUE IF COUNT > 0
                    Suppline = new MNPSelectPlanForSuppline();
                    foreach (string Suppmsisdns in Supp)
                    {
                        Suppline.SupplimentaryMSISDNs.Add(new SupplimentaryMSISDN
                        {
                            Msisdn = Suppmsisdns
                            ///BY DEFAULT status is MNPSupplimentaryDetailsAddState.NOTINITIALIZED
                        });
                    }
                    ///AFTER POPULATING TRANSFER IT TO SESSION
                    Session[SessionKey.MNPSupplementaryLines.ToString()] = Suppline;
                }


            }

            var jsonData2 = new { isError, ErrorMsg };

            return Json(jsonData2);
        }

        #endregion

        #region Private Method

        /// <summary>
        /// MNPs the business validation checks.
        /// </summary>
        /// <param name="DonorID">The donor ID.</param>
        /// <param name="MSISDN">The MSISDN.</param>
        /// <param name="CustomerName">Name of the customer.</param>
        /// <param name="CompanyName">Name of the company.</param>
        /// <param name="Supp">The supp.</param>
        /// <param name="isError">if set to <c>true</c> [is error].</param>
        /// <param name="ErrorMsg">The error MSG.</param>
        private void MNPBusinessValidationChecks(string DonorID, string MSISDN, string CustomerName, string CompanyName, List<string> Supp, out bool isError, out string ErrorMsg)
        {
            Session[SessionKey.DonorId.ToString()] = null;

            HomeController myHomeControl = new HomeController();
            Online.Registration.Web.KenanSvc.businessDataType[] businessDataList = null;
            var resp = new BusinessRuleResponse();
            string idCardNo = string.Empty;
            string idCardTypeID = string.Empty;
            int? GeneratedID = null;
            isError = false;
            ErrorMsg = string.Empty;
            string ServiceErrorMessage = string.Empty;
            Session[SessionKey.DonorId.ToString()] = DonorID;

            ///FOR PRIMARY MSISDN
            if (ReferenceEquals(Supp, null))
            {
                #region MNPServiceIdCheck

                businessDataList = new Online.Registration.Web.KenanSvc.businessDataType[5];

                businessDataList[0] = new Online.Registration.Web.KenanSvc.businessDataType()
                {
                    dataName = "SERVICEEXTERNALID",
                    dataValue = MSISDN
                };

                businessDataList[1] = new Online.Registration.Web.KenanSvc.businessDataType()
                {
                    dataName = "SERVICEEXTERNALIDTYPE",
                    dataValue = Properties.Settings.Default.ServiceExternalIdType
                };

                businessDataList[2] = new Online.Registration.Web.KenanSvc.businessDataType()
                {
                    dataName = "EMFID",
                    dataValue = Properties.Settings.Default.EmfId
                };

                businessDataList[3] = new Online.Registration.Web.KenanSvc.businessDataType()
                {
                    dataName = "ACCTSEG",
                    dataValue = Properties.Settings.Default.AcctSeg
                };

                businessDataList[4] = new Online.Registration.Web.KenanSvc.businessDataType()
                {
                    dataName = "DONORID",
                    dataValue = DonorID
                };

                WebHelper.Instance.LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules :PrePortInCheck", IsBefore: true);

                try
                {
                    //MNPServiceIdCheck
                    resp = BREChecker.validate(new BusinessRuleRequest()

                    {


                        RuleNames = "MNPServiceIdCheck".Split(' '),
                        businessDataList = businessDataList
                    });




                    ServiceErrorMessage = !ReferenceEquals(resp.ResultMessage, null) ? resp.ResultMessage.Replace("business rule check is failed,", string.Empty) : string.Empty;


                    SetMNPServiceIdCheckerSession(resp);

                    resp.ResultMessage = ServiceErrorMessage;



                    isError = resp.IsMNPServiceIdCheckFailed;
                    ErrorMsg = (isError ? MSISDN + " : " + ServiceErrorMessage : string.Empty);

                    WebHelper.Instance.LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules :MNPServiceIdCheck", IsBefore: false, ID: GeneratedID, status: "Success");
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules :MNPServiceIdCheck", IsBefore: false, ID: GeneratedID, status: "Failure : " + ex.Message);
                    Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()] = "MSIE";
                    isError = true;
                    ErrorMsg = ex.Message;

                    WebHelper.Instance.LogExceptions(this.GetType(), ex);


                }
                #endregion MNPServiceIdCheck

                ///PLEASE UNCOMENT BELOW WHEN TESTING IS DONE
                ///ERROR WITH MNPServiceIdCheck END THE PROCESS 
                if (isError)
                {
                    return;
                }
            }
            else
            {
                ///LOOP THROUGH EACH SUPPLEMENTARY LINE
                foreach (string SuppMsisdn in Supp)
                {
                    #region MNPServiceIdCheck

                    businessDataList = new Online.Registration.Web.KenanSvc.businessDataType[5];

                    businessDataList[0] = new Online.Registration.Web.KenanSvc.businessDataType()
                    {
                        dataName = "SERVICEEXTERNALID",
                        dataValue = SuppMsisdn
                    };

                    businessDataList[1] = new Online.Registration.Web.KenanSvc.businessDataType()
                    {
                        dataName = "SERVICEEXTERNALIDTYPE",
                        dataValue = Properties.Settings.Default.ServiceExternalIdType
                    };

                    businessDataList[2] = new Online.Registration.Web.KenanSvc.businessDataType()
                    {
                        dataName = "EMFID",
                        dataValue = Properties.Settings.Default.EmfId
                    };

                    businessDataList[3] = new Online.Registration.Web.KenanSvc.businessDataType()
                    {
                        dataName = "ACCTSEG",
                        dataValue = Properties.Settings.Default.AcctSeg
                    };

                    businessDataList[4] = new Online.Registration.Web.KenanSvc.businessDataType()
                    {
                        dataName = "DONORID",
                        dataValue = DonorID
                    };

                    WebHelper.Instance.LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules :PrePortInCheck", IsBefore: true);

                    try
                    {
                        //MNPServiceIdCheck


                        resp = BREChecker.validate(new BusinessRuleRequest()
                        {
                            RuleNames = "MNPServiceIdCheck".Split(' '),
                            businessDataList = businessDataList
                        });


                        SetMNPServiceIdCheckerSession(resp);






                        isError = resp.IsMNPServiceIdCheckFailed;



                        ErrorMsg = (isError ? SuppMsisdn + " : " + resp.ResultMessage : string.Empty);

                        WebHelper.Instance.LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules :MNPServiceIdCheck", IsBefore: false, ID: GeneratedID, status: "Success");
                    }
                    catch (Exception ex)
                    {
                        WebHelper.Instance.LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules :MNPServiceIdCheck", IsBefore: false, ID: GeneratedID, status: "Failure : " + ex.Message);
                        Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()] = "MSIE";
                        isError = true;
                        ErrorMsg = ex.Message;

                        WebHelper.Instance.LogExceptions(this.GetType(), ex);

                    }
                    #endregion MNPServiceIdCheck
                    if (isError)
                    {
                        return;
                    }




                }
            }
        }

        private void MNPBusinessValidationPreportInChecks(string DonorID, string MSISDN, string CustomerName, string CompanyName, List<string> Supp, out bool isError, out string ErrorMsg)
        {
            Session[SessionKey.Master_PrePortReqId.ToString()] = null;
            HomeController myHomeControl = new HomeController();
            string idCardNo = string.Empty;
            string idCardTypeID = string.Empty;
            int? GeneratedID = null;
            isError = false;
            ErrorMsg = string.Empty;
            string ServiceErrorMessage = string.Empty;
            List<string> _msisdns = new List<string>();
            List<string> _identityType = new List<string>();
            List<string> _identity = new List<string>();

            WebHelper.Instance.LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules :PrePortInCheck", IsBefore: true);
            try
            {

                IDCardType idCardtype = new IDCardType();
                using (var regProxy = new RegistrationServiceProxy())
                {
                    idCardtype = regProxy.IDCardTypeGet(Session[SessionKey.IDCardTypeID.ToString()].ToInt());
                }
                _msisdns.Add(MSISDN);
                _identityType.Add(idCardtype.KenanCode);
                _identity.Add(Session[SessionKey.IDCardNo.ToString()].ToString2());
                if (!ReferenceEquals(Supp, null))
                {
                    foreach (var sup in Supp)
                    {
                        if (!string.IsNullOrEmpty(sup))
                        {
                            _msisdns.Add(sup);
                            _identityType.Add(idCardtype.KenanCode);
                            _identity.Add(Session[SessionKey.IDCardNo.ToString()].ToString2());
                        }
                    }
                }
                //PrePortInCheck
                PreportInRequest _request = null;
                PreportInResponse _response = null;
                using (var proxy = new KenanServiceProxy())
                {
                    _request = new PreportInRequest();
                    _response = new PreportInResponse();
                    _response = proxy.PreportInValidationCheck(new PreportInRequest()
                    {
                        donorId = DonorID,
                        msisdn = _msisdns.ToArray(),
                        customerName = CustomerName,
                        companyName = CompanyName,
                        identityType = _identityType.ToArray(),
                        identity = _identity.ToArray(),
                        isConsumer = "Y"
                    }
                    );
                }
                bool ispreportFailed = true;
                if (!ReferenceEquals(_response, null))
                {
                    if (_response.msgCode.Equals("0")) //success case
                    {
                        ServiceErrorMessage = !ReferenceEquals(_response.msgDesc, null) ? _response.msgDesc.Replace("business rule check is failed,", string.Empty) : string.Empty;
                        //Set sessions
                        Session[SessionKey.RegMobileReg_IsPortInDonerCheckFailed.ToString()] = ispreportFailed = false;
                        Session[SessionKey.RegMobileReg_IsPortInDonerMessage.ToString()] = ServiceErrorMessage;
                        Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()] = _response.msgCode.Equals("0") ? "MPPS" : "MPPF";
                        Session[SessionKey.Master_PrePortReqId.ToString()] = _response.reqId;
                        isError = ispreportFailed;
                        ///ASSIGN THE resp.ResultMessage INTO ErrorMsg IF isError IS TRUE
                        if (ServiceErrorMessage.ToLower() == "success")
                            ServiceErrorMessage = "Business Rule Success";
                        string msisdnlist = string.Empty;
                        foreach (var m in _msisdns)
                        {
                            if (m != string.Empty)
                                msisdnlist += ',' + m;
                        }
                        msisdnlist = msisdnlist.TrimStart(',');
                        _msisdns.ToArray();
                        ErrorMsg = (isError ? msisdnlist + " : " + ServiceErrorMessage : string.Empty);
                    }
                    else //failure case
                    {

                        if (_response.msgCode == "-2")
                            ServiceErrorMessage = "Cannot able to submit portIn Request, Please Contact Administrator";
                        else
                            ServiceErrorMessage = !ReferenceEquals(_response.msgDesc, null) ? _response.msgDesc.Replace("business rule check is failed,", string.Empty) : string.Empty;
                        //Set sessions
                        Session[SessionKey.RegMobileReg_IsPortInDonerCheckFailed.ToString()] = ispreportFailed = true; //always works
                        //Session[SessionKey.RegMobileReg_IsPortInDonerCheckFailed.ToString()] = ispreportFailed = false; //temperory off
                        Session[SessionKey.RegMobileReg_IsPortInDonerMessage.ToString()] = ServiceErrorMessage;
                        Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()] = _response.msgCode.Equals("0") ? "MPPS" : "MPPF";
                        isError = ispreportFailed;
                        ///ASSIGN THE resp.ResultMessage INTO ErrorMsg IF isError IS TRUE   
                        string msisdnlist = string.Empty;
                        foreach (var m in _msisdns)
                        {
                            if (m != string.Empty)
                                msisdnlist += ',' + m;
                        }
                        msisdnlist = msisdnlist.TrimStart(',');
                        _msisdns.ToArray();
                        ErrorMsg = (isError ? msisdnlist + " : " + ServiceErrorMessage : string.Empty);
                    }
                }
                WebHelper.Instance.LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules :PrePortInCheck", IsBefore: false, ID: GeneratedID, status: "Success");

            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules :PrePortInCheck", IsBefore: false, ID: GeneratedID, status: "Failure : " + ex.Message);
                Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()] = "MPPE";
                isError = true;
                ErrorMsg = ex.Message;

                WebHelper.Instance.LogExceptions(this.GetType(), ex);

            }

        }

        private void ClearMNPSession()
        {
            Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegStepNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_IDCardType.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.TabNumber.ToString()] = null;
            Session[SessionKey.TabValue.ToString()] = null;
            Session[SessionKey.Bundle.ToString()] = null;
            Session[SessionKey.contract_number.ToString()] = null;
            Session[SessionKey.vas_number.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = null;
            Session[SessionKey.RegMobileReg_SublineVM.ToString()] = null;
            Session[SessionKey.RegMobileReg_portInType.ToString()] = null;
            Session[SessionKey.RegMobileReg_donor.ToString()] = null;
            Session[SessionKey.RegMobileReg_MSISDN.ToString()] = null;
            Session[SessionKey.Title.ToString()] = null;
            Session[SessionKey.Gender.ToString()] = null;
            Session[SessionKey.FullName.ToString()] = null;
            Session[SessionKey.DOB.ToString()] = null;
            Session[SessionKey.Language.ToString()] = null;
            Session[SessionKey.AlternateContactNo.ToString()] = null;
            Session[SessionKey.Nationality.ToString()] = null;
            Session[SessionKey.Email.ToString()] = null;
            Session[SessionKey.Race.ToString()] = null;
            Session[SessionKey.Address.ToString()] = null;
            Session[SessionKey.addLine2.ToString()] = null;
            Session[SessionKey.Town.ToString()] = null;
            Session[SessionKey.PostCode.ToString()] = null;
            Session[SessionKey.State.ToString()] = null;
            Session[SessionKey.PaymentMode.ToString()] = null;
            Session[SessionKey.CardType.ToString()] = null;
            Session[SessionKey.CardNo.ToString()] = null;
            Session[SessionKey.CardExpiryDate.ToString()] = null;
            Session[SessionKey.NameOnCard.ToString()] = null;
            Session[SessionKey.Deposit.ToString()] = null;

            //GTM e-Billing CR - Ricky - 2014.09.25 - Start
            //Session[SessionKey.billDelivery_msisdnListByAccount.ToString()] = null; //To be cleared during search for new Cust only
            //Session[SessionKey.billDelivery_availableMsisdn.ToString()] = null;     //To be cleared during search for new Cust only
            Session[SessionKey.billDelivery_emailBillSubscription.ToString()] = null;
            Session[SessionKey.billDelivery_emailAddress.ToString()] = null;
            Session[SessionKey.billDelivery_smsAlertFlag.ToString()] = null;
            Session[SessionKey.billDelivery_smsNo.ToString()] = null;
            Session[SessionKey.billDelivery_ableToSubmit.ToString()] = null;
            //GTM e-Billing CR - Ricky - 2014.09.25 - End
        }

        #endregion

        #region Protected Methods

        protected override void Initialize(RequestContext requestContext)
        {
            Util.CheckSessionAccess(requestContext);
            base.Initialize(requestContext);
        }

        #endregion

        private void SetMNPServiceIdCheckerSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckFailed.ToString()] = resp.IsMNPServiceIdCheckFailed;
            Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()] = resp.IsMNPServiceIdCheckFailed ? "MSIF" : "MSIS";
        }

        public ActionResult StatusTrackingNew()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        [Authorize]
        public ActionResult StatusTrackingNew(StatusTrackingVM model)
        {
            var statusTrackModel = model;
            bool isSuperAdmin = (bool)Session[SessionKey.isSuperAdmin.ToString()];

            try
            {
                if ((!ReferenceEquals(model, null)) && (model.MSISDN.ToString() != string.Empty))
                {

                    getMNPRequest _request = new getMNPRequest();
                    getMNPRequestResponse _response = new getMNPRequestResponse();

                    // Drop 4
                    _request.reqType = !string.IsNullOrEmpty(Properties.Settings.Default.StatusTrack_RequestType) ? Properties.Settings.Default.StatusTrack_RequestType : "MSISDN";
                    _request.reqValue = model.MSISDN.ToString().Trim();
                    _request.portReqType = !string.IsNullOrEmpty(Properties.Settings.Default.StatusTrack_PortIn) ? Properties.Settings.Default.StatusTrack_PortIn : "I";
                    using (var proxyK = new KenanServiceClient())
                    {
                        _response = proxyK.GetMNPRequest(_request);
                    }

                    if (_response.RequestDetails.Count() == 0)
                    {
                        model = null;
                        throw new Exception(Properties.Settings.Default.StatusTrack_ErrorMessage);
                    }

                    if (_response.RequestDetails.Count() > 0)
                    {

                        foreach (var item in _response.RequestDetails)
                        {
                            RegistrationFind oReq = new RegistrationFind();
                            oReq.Registration.MSISDN1 = item.Msisdn;

                            using (var proxy = new RegistrationServiceProxy())
                            {
                                
                                var regIDList = proxy.RegistrationFind(oReq);

                                if (regIDList.ToList().IsEmpty())
                                {
                                    //throw new Exception(Properties.Settings.Default.StatusTrack_ErrorMessage);
                                    //[PBI000000009916]RE: Unable to check MNP Status Tracking via iSell - Lus - 27082015
                                    var accountNo = item.AccountNo;
                                    if (!string.IsNullOrEmpty(accountNo))
                                    {
                                        using (var proxySub = new retrieveServiceInfoProxy())
                                        {
                                            CustomizedCustomer subscriberDetails = proxySub.retrieveSubscriberDetlsByAcctNo(accountNo, Request.Cookies["CookieUser"].Value, isSuperAdmin);
                                            model.CustomerName = (!ReferenceEquals(subscriberDetails, null)) ? subscriberDetails.CustomerName : "N/A";
                                        }
                                    }
                                    model.MnpRegDet = new MnpRegDetailsVM();
                                    model.MnpRegDet.RatePlan = "N/A";
                                    model.MnpRegDet.Type = "N/A";
                                }
                                else
                                {
                                    //25052015 Add Customer name and Plan Name information in Status Tracking - Lus
                                    foreach (var ids in regIDList)
                                    {
                                        var custList = proxy.GetRegistrationFullDetails(ids);
                                        model.CustomerName = custList.Customers.Select(y => y.FullName).FirstOrDefault().ToString2();
                                        var result = new MnpRegDetailsVM();
                                        result = proxy.getPlanNamebyRegId(ids);
                                        ///model.MnpRegDet = new MnpRegDetailsVM();
                                        model.MnpRegDet = result;
                                        model.MnpRegDet.RatePlan = result.RatePlan;
                                        model.MnpRegDet.Type = result.Type;
                                    }
                                    
                                    var kenanLog = proxy.KenanLogHistoryDetailsGet(regIDList);
                                    if (!kenanLog.ToList().IsEmpty())
                                    {
                                        for (int i = 0; i < kenanLog.ToList().Count(); i++)
                                        {
                                            var logs = kenanLog.ToList()[i];

                                            if (logs.MethodName.Equals(Properties.Settings.Default.StatusTrack_ResubmissionCreateCenterOrderMNP))
                                            {
                                               int nextValue = 1+i;
                                               
                                                var nextlogs = kenanLog.ToList()[nextValue];

                                                if (nextlogs.MethodName.Equals(Properties.Settings.Default.StatusTrack_ResubmissionUpdateOrderStatus))
                                                {
                                                    if (nextlogs.KenanXmlReq.Contains("<MsgCode>3</MsgCode>"))
                                                    {
                                                        ViewBag.DisplayMessage = "Previous Resubmission Order for this MSISDN is failed.Kindly Reraise Resubmission Order";
                                                        
                                                    }
													// no need to throw exception, as the user still can see the status
													// else 
													//{
													//    throw new Exception(Properties.Settings.Default.StatusTrack_ResubmissionNotAllowed);
													//}

                                                   
                                                }
												// no need to throw exception, as the user still can see the status.
												//if (nextValue == kenanLog.ToList().Count())
												//{
												//    throw new Exception(Properties.Settings.Default.StatusTrack_ResubmissionNotAllowed);
												//}
                                            }  

                                        }
                                    }
                                }
                            }
                        }
                    }

                    
                    MnpRequestDetails[] mnpResults = _response.RequestDetails;
					var orderedDesc = mnpResults.OrderByDescending(x => x.PortInitiatedDt).ToList();
                    
                    statusTrackModel.MNPReqList = mnpResults.OrderByDescending(x=> x.PortInitiatedDt).ToArray();

                }
                else
                {
                    /// need to write logic for msisdn failure
                }
            }
            catch (Exception ex)
            {

                WebHelper.Instance.LogExceptions(this.GetType(), ex);

                ViewBag.ErrorMessage = ex.Message;
            }
            /// this code need to be properly 
            return View(model);
        
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult EnterDetailsNew(int id = 0, string accid = "", string SelectAccountDtls = "")
        {
			// Gerry
			// if SelectAccountDtls not null, means, it's from direct suppline, impact on EnterDetailsNew screen multiple suppline offer
			if (string.IsNullOrWhiteSpace(SelectAccountDtls))
				Session[SessionKey.DedicatedSuppFlow.ToString()] = false;

            Session["MNPName"] = string.Empty;
            Session["FromMNP"] = null;
            Session["MNPSupplementaryLines"] = null;
            Session["OrderType"] = (int)MobileRegType.MNPPlanOnly;
            ViewBag.MNPCreationStatus = id;
            ViewBag.KenanAccount = !string.IsNullOrEmpty(accid) ? accid : SelectAccountDtls;
            ViewBag.IsConsumer = Convert.ToString(Properties.Settings.Default.IsConsumer);
            ViewBag.MNPMaxSupplineToPort = Convert.ToInt32(Properties.Settings.Default.MNPMaxSupplineToPort); ///MAXIMUM NUMBER OF SUPPLINE TO BE PORTED
            Session[SessionKey.MenuType.ToString()] = (int)MenuType.MNPStatusTrack;
            return View();
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC,MREG_DSA")]
        public ActionResult EnterDetailsNew(FormCollection collection)
        {
            try
            { 
                Session["MNPName"] = collection["hdnCustomerName"].ToString2();
                Session[SessionKey.RegMobileReg_donor.ToString()] = collection["hdnDonorID"].ToString2();
                Session[SessionKey.RegMobileReg_MSISDN.ToString()] = collection["MSISDN"].ToString2();
                Session[SessionKey.MenuType.ToString()] = (int)MenuType.MNPRegistration;
                bool isMultipleMNP = ((!ReferenceEquals(Session["FromMNP"], null) && Convert.ToBoolean(Session["FromMNP"])) && (!ReferenceEquals(Session["MNPSupplementaryLines"], null) && (!ReferenceEquals(((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"]).SupplimentaryMSISDNs, null) && ((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"]).SupplimentaryMSISDNs.Count > 0)));
                if (isMultipleMNP)
                {
                    Session["MenuType"] = (int)MenuType.MNPPlanWithMultiSuppline;
                }
                Session[SessionKey.TabNumber.ToString()] = string.Empty;
                Session[SessionKey.TabValue.ToString()] = string.Empty;

                Session["IsOlo"] = collection["hdnOlo"].ToString2() == "1" ? 1 : 0;

                string hdnMNPFlowType = collection["hdnMNPFlowType"].ToString2() == string.Empty ? "APL" : collection["hdnMNPFlowType"].ToString2();
                string hdnDealerFlow = collection["hdnDealerFlow"].ToString2() == string.Empty ? "false" : collection["hdnDealerFlow"].ToString2();
                Session[SessionKey.MNPFlowType.ToString()] = hdnMNPFlowType;

                //GTM e-Billing CR - Ricky - 2014.09.25 - Start
                //To add into session used for Available MSISDN List
                var availMsisdn = new List<string>();
                availMsisdn.Add(collection["MSISDN"].ToString2());
                if (!ReferenceEquals((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"], null))
                {
                    foreach (var x in ((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"]).SupplimentaryMSISDNs)
                        availMsisdn.Add(x.Msisdn);
                }
                Session[SessionKey.billDelivery_availableMsisdn.ToString()] = availMsisdn;
                //GTM e-Billing CR - Ricky - 2014.09.25 - End
                var accStr = collection["hdnMNPKenanAccountID"];
                if (Convert.ToBoolean(hdnDealerFlow))
                {
                    switch (hdnMNPFlowType)
                    {
                        case "APL":
                            return RedirectToAction("SelectPlanNew", "Registration", new { type = 2 });
                            break;
                        case "ASL":
                            {
                                //var accStr = collection["hdnMNPKenanAccountID"];

                                resetSupplineFlow();

                                if (accStr != null)
                                {
                                    string[] strAccountDtls = accStr.Split(',');
                                    Session["FxAccNo"] = strAccountDtls[0].ToString2();
                                    Session["ExternalID"] = strAccountDtls[1].ToString2();
                                    Session["AccExternalID"] = strAccountDtls[2].ToString2();
                                    Session["KenanACNumber"] = strAccountDtls[3].ToString2();
                                    Session[SessionKey.AccountCategory.ToString()] = strAccountDtls[7];
                                    Session[SessionKey.MarketCode.ToString()] = strAccountDtls[8];

                                    using (var proxy = new UserServiceProxy())
                                    {
                                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Select Account", Session["KenanACNumber"].ToString2(), "");
                                    }

                                    return RedirectToAction("SuppLinePlanNew", "SuppLine", new { type = (int)MobileRegType.SuppPlan });
                                }
                            }
                            break;
                        case "ANL":
                            {
                                //var accStr = collection["hdnMNPKenanAccountID"];
                                resetNewlineFlow();
                                if (accStr != null)
                                {
                                    string[] strAccountDtls = accStr.Split(',');
                                    Session[SessionKey.FxAccNo.ToString()] = strAccountDtls[0].ToString();
                                    Session[SessionKey.ExternalID.ToString()] = strAccountDtls[1].ToString();
                                    Session[SessionKey.AccExternalID.ToString()] = strAccountDtls[2].ToString();
                                    Session[SessionKey.KenanACNumber.ToString()] = strAccountDtls[3].ToString();
                                    Session[SessionKey.IsMISM.ToString()] = strAccountDtls[4].ToString();

                                    Session[SessionKey.AccountCategory.ToString()] = strAccountDtls[5];
                                    Session[SessionKey.MarketCode.ToString()] = strAccountDtls[6];
                                    Session["SMEIPARENTID"] = strAccountDtls[7];

                                    using (var proxy = new UserServiceProxy())
                                    {
                                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Select Account", Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                                    }
                                    if (isMultipleMNP)
                                        return RedirectToAction("SelectPlanNew", "Registration", new { type = 2 });
                                    else
                                        return RedirectToAction("SelectPlanNew", "Newline");
                                }
                                break;
                            }
                        default:
                            break;
                    }
                }
                if (!string.IsNullOrEmpty(accStr))
                {
                    resetNewlineFlow();
                    string[] strAccountDtls = accStr.Split(',');
                    Session[SessionKey.FxAccNo.ToString()] = strAccountDtls[0].ToString();
                    Session[SessionKey.ExternalID.ToString()] = strAccountDtls[1].ToString();
                    Session[SessionKey.AccExternalID.ToString()] = strAccountDtls[2].ToString();
                    Session[SessionKey.KenanACNumber.ToString()] = strAccountDtls[3].ToString();
                    Session[SessionKey.IsMISM.ToString()] = strAccountDtls[4].ToString();

                    Session[SessionKey.AccountCategory.ToString()] = strAccountDtls[5];
                    Session[SessionKey.MarketCode.ToString()] = strAccountDtls[6];
                    Session["SMEIPARENTID"] = strAccountDtls[7];

                    Session[SessionKey.isMNPNewlineWithSupplementary.ToString()] = true;//12052015 - Anthony - Add an additional flag to determine the MNP Newline with Supplementary line

                    using (var proxy = new UserServiceProxy())
                    {
                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.NewLine), "Select Account", Session[SessionKey.KenanACNumber.ToString()].ToString(), "");
                    }
                }

                return RedirectToAction("SelectPlanNew", "Registration", new { type = 2 });
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
        }

        private void resetNewlineFlow()
        {
            Session[SessionKey.FromPromoOffer.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session[SessionKey.SelectedPlanID_Seco.ToString()] = null;
            Session[SessionKey.VasGroupIds.ToString()] = null;
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session["vasBackClick"] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] = null;
            Session[SessionKey.RegMobileReg_MobileNo.ToString()] = null;
            Session[SessionKey.SelectedPlanID_Seco.ToString()] = null;
            Session[SessionKey.SimType.ToString()] = null;
            Session[SessionKey.Condition.ToString()] = null;
            Session["AccountHolder"] = null;
            Session["_componentViewModel"] = null;
            Session[SessionKey.AccountCategory.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session["SMEIPARENTID"] = null;
            WebHelper.Instance.ResetDepositSessions();

            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
            {
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            }
        }

        private void resetSupplineFlow()
        {
            Session[SessionKey.AccountCategory.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session["_componentViewModel"] = null;
            Session["FromPromoOffer"] = null;
            Session["MarketCode"] = null;
            Session["VasGroupIds"] = null;
            Session["VasGroupIds_Seco"] = null;
            Session["SelectedPlanID"] = null;
            Session["OfferId"] = null;
            Session["Discount"] = null;
            Session["RegMobileReg_MainDevicePrice"] = null;
            Session["RegMobileReg_OrderSummary"] = null;
            Session["RegMobileReg_OfferDevicePrice"] = null;
            Session["SimType"] = null;
            Session["RegMobileReg_MobileNo"] = null;
            Session["RegMobileReg_VirtualMobileNo_Sec"] = null;
            Session["ExtratenMobilenumbers"] = null;
            Session["Condition"] = null;
            Session["AccountHolder"] = null;

            Session["RegMobileReg_MalyDevAdv"] = null;
            Session["RegMobileReg_MalayPlanAdv"] = null;
            Session["RegMobileReg_MalayDevDeposit"] = null;
            Session["RegMobileReg_MalyPlanDeposit"] = null;
            Session["RegMobileReg_MalAdvDeposit"] = null;
            Session["RegMobileReg_OthDevAdv"] = null;
            Session["RegMobileReg_OthPlanAdv"] = null;
            Session["RegMobileReg_OthDevDeposit"] = null;
            Session["RegMobileReg_OthPlanDeposit"] = null;
            Session["RegMobileReg_OtherAdvDeposit"] = null;

            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session[SessionKey.SelectedComponents.ToString()] = null;
            Session[SessionKey.SpendLimit.ToString()] = null;

            if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
            {
                Session.Contents.Remove("MNPSupplementaryLines");
            }

            if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
            {
                Session.Contents.Remove("mNPPrimaryPlanStorage");
            }
        }
    }
}
