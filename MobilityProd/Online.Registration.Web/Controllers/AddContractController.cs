﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Configuration;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using SNT.Utility;
using Online.Registration.Web.Models;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.ContentRepository;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.UserSvc;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.Properties;
using System.Web.Security;
using Online.Web.Helper;
using System.Web;


namespace Online.Registration.Web.Controllers
{
    [Authorize]
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class AddContractController : Controller
    {
		private bool sharingQuotaFeature = ConfigurationManager.AppSettings["SharingSupplementaryFlag"].ToBool();

        #region Action

        /// <summary>
        /// Display list of available accounts.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            Session["_componentViewModel"] = null;
            Session[SessionKey.ContractDetails.ToString()] = null;
            //Clearing the sessions
            Session["AddedVases"] = null;
            Session["RemovedVases"] = null;
            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = null;
            Session[SessionKey.ExtratenMobilenumbers.ToString()] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session["AddedVasNames"] = null;
            Session["RemovedVasNames"] = null;
            Session["AddedVasPrices"] = null;
            Session["RemovedVasPrices"] = null;
            Session["ARVasGroupIds"] = null;
            Session[SessionKey.VasGroupIds.ToString()] = null;
            Session[SessionKey.Condition.ToString()] = null;
            Session["ChangeFlag"] = null;
            Session["mandatoryPkgCompIds"] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session["RegMobileReg_DataplanID"] = null;
            Session["AccountHolder"] = null;
            Session["VAS"] = null;
            Session["model"] = null;
            Session["MandatoryVasIds"] = null;
            //Clearing the sessions

            retrieveAcctListByICResponse AcctListByICResponse = null;
            SupplementaryListAccounts SuppListAccounts = null;
            var isUserCCCAbove = WebHelper.Instance.IsUserCCCAbove();

            if ((!ReferenceEquals(Session[SessionKey.PPID.ToString()], null) && "E".Equals(Convert.ToString(Session[SessionKey.PPID.ToString()]))) && !ReferenceEquals(Session[SessionKey.PPIDInfo.ToString()], null))
            {
                ///GET PPIDInfo FROM SESSION
                AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()];
                string strMISMEmfConfigId = ConfigurationManager.AppSettings["MISMEmfConfigId"].ToString2();

                string supAccountStatus = string.Empty;

                if (AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse != null && (c.ServiceInfoResponse.lob == "POSTGSM") && (c.ServiceInfoResponse.serviceStatus.ToUpper() == "A" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "S" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "B" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "D" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "T")).Count() > 0)
                {
                    SuppListAccounts = new SupplementaryListAccounts();
                    List<string> lstMsisdnList = new List<string>();


                    foreach (var v in AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse != null && c.ServiceInfoResponse.prinSuppInd != "" && (c.ServiceInfoResponse.lob == "POSTGSM") && c.EmfConfigId != strMISMEmfConfigId && (c.ServiceInfoResponse.serviceStatus.ToUpper() == "A" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "S" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "B")).ToList())
                    {
                        //2015.07.27 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - Start
                        if (Util.SessionAccess.User.isDealer && v.AccountDetails != null && (
                            v.AccountDetails.MktCode == ConfigurationManager.AppSettings["CIParentMarketCode"].ToString2() ||
                            v.AccountDetails.MktCode == ConfigurationManager.AppSettings["SMEIParentMarketCode"].ToString2()))
                        {
                            continue;
                        }
                        //2015.07.27 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - End

                        if (Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString2() == "6" && (Session["IsDealer"].ToBool() || isUserCCCAbove == false)) //MSISDN Search only
                        {
                            #region "MSISDN Search Only Flow "
                            var prinExternalID = string.Empty;
                            if (Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString() == v.ExternalId)
                            {
                                if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "S")
                                {
                                    //Get the MISM primary msisdn 
                                    using (var PrinSupproxy = new PrinSupServiceProxy())
                                    {
                                        retrieveMismDetlsResponse primSecResponse = PrinSupproxy.getPrimSecondaryines(v.ExternalId);
                                        if (primSecResponse != null && primSecResponse.itemList.ToList().Count > 0)
                                        {
                                            prinExternalID = primSecResponse.itemList.Where(a => a.PrimSecInd == "P").FirstOrDefault().Msisdn;
                                        }
                                    }
                                }

                                SuppListAccounts.SuppListAccounts.Add(
                                    new AddSuppInquiryAccount
                                    {
                                        AccountNumber = v.AcctExtId,
                                        ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                        Category = v.Account != null ? v.Account.Category : string.Empty,
                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                        MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                        SubscribeNo = v.SusbcrNo,
                                        SubscribeNoResets = v.SusbcrNoResets,
                                        externalId = v.ExternalId,
                                        accountExtId = v.AcctExtId,
                                        accountIntId = v.AcctIntId,
                                        AccountName = v.ServiceInfoResponse.lob,
                                        AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                        IsMISM = v.IsMISM,
                                        // SimSerial = v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "S" ? GetsimSerial(v.ExternalId) : string.Empty,
                                        AccountType = !string.IsNullOrEmpty(v.ServiceInfoResponse.prinSuppInd) ? v.ServiceInfoResponse.prinSuppInd : "P",
                                        PrinMsisdn = !string.IsNullOrEmpty(prinExternalID) ? prinExternalID : string.Empty,
                                        //IsGreater2GBCompExists = is2GBCompExistOnPkg,
                                        //IsPackagesExistsOnAccountLine = isPkgExists,
                                        //AccountKenanCode = PackageKenanCode.ToString2(),
                                        //AccountPackageName = AccountPackageName,
                                        HasSupplyAccounts = v.PrinSuppResponse != null & v.PrinSuppResponse.itemList.Count > 0 ? true : false

                                    });

                                //if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "P" && v.SecondarySimList != null)
                                //{
                                //    //Get the MISM primary msisdn 
                                //    using (var PrinSupproxy = new PrinSupServiceProxy())
                                //    {
                                //        retrieveMismDetlsResponse primSecResponse = PrinSupproxy.getPrimSecondaryines(v.ExternalId);
                                //        if (primSecResponse != null && primSecResponse.itemList.ToList().Count > 0)
                                //        {
                                //            prinExternalID = primSecResponse.itemList.Where(a => a.PrimSecInd == "P").FirstOrDefault().Msisdn;
                                //            foreach (var supp in v.SecondarySimList)
                                //            {
                                //                SuppListAccounts.SuppListAccounts.Add(
                                //                   new AddSuppInquiryAccount
                                //                   {
                                //                       AccountNumber = v.AcctExtId,
                                //                       ActiveDate = string.Empty,
                                //                       Address = v.Account != null ? v.Account.Address : string.Empty,
                                //                       Category = string.Empty,
                                //                       CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                //                       Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                //                       IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                //                       IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                //                       MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                //                       Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                //                       SubscribeNo = supp.FxSubscrNo,
                                //                       SubscribeNoResets = supp.FxSubscrNoResets,
                                //                       externalId = supp.Msisdn,
                                //                       accountExtId = v.AcctExtId,
                                //                       accountIntId = supp.FxAcctNo,
                                //                       AccountName = v.ServiceInfoResponse.lob,
                                //                       AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                //                       IsMISM = v.IsMISM,
                                //                       //SimSerial = supp.PrimSecInd == "S" ? GetsimSerial(supp.Msisdn) : string.Empty,
                                //                       AccountType = "S",
                                //                       PrinMsisdn = v.ExternalId != supp.Msisdn ? v.ExternalId : string.Empty
                                //                       //IsGreater2GBCompExists = is2GBCompExistOnPkg
                                //                   });
                                //            }
                                //        }
                                //    }
                                //}

                                if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "P" && v.PrinSupplimentaryResponse != null && v.PrinSupplimentaryResponse.itemList.Count > 0)
                                {

                                    foreach (var supp in v.PrinSupplimentaryResponse.itemList)
                                    {
                                        if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                        {
                                            if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                supAccountStatus = "S";
                                            else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                supAccountStatus = "D";
                                            else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                supAccountStatus = "T";
                                            else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                supAccountStatus = "B";
                                            else
                                                supAccountStatus = "A";
                                        }
                                        string accountNumber = string.Empty;
                                        var suppAccount = AcctListByICResponse.itemList.Where(s => s.ExternalId == supp.msisdnField && s.ServiceInfoResponse.prinSuppInd == "S" && !s.IsMISM).FirstOrDefault();
                                        accountNumber = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId;

                                        //SuppListAccounts.SuppListAccounts.Add(
                                        //   new AddSuppInquiryAccount
                                        //   {
                                        //       AccountNumber = accountNumber, // v.AcctExtId,
                                        //       ActiveDate = string.Empty,
                                        //       Address = v.Account != null ? v.Account.Address : string.Empty,
                                        //       Category = string.Empty,
                                        //       CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                        //       Holder = supp.cust_nmField, //v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                        //       IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                        //       IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                        //       MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                        //       Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                        //       SubscribeNo = supp.subscr_noField,
                                        //       SubscribeNoResets = supp.subscr_no_resetsField,
                                        //       externalId = supp.msisdnField,
                                        //       accountExtId = v.AcctExtId,
                                        //       accountIntId = supp.acct_noField,
                                        //       AccountName = v.ServiceInfoResponse.lob,
                                        //       AccountStatus = supAccountStatus, //v.ServiceInfoResponse.serviceStatus,
                                        //       IsMISM = supp.IsMISM,
                                        //       //SimSerial = supp.PrimSecInd == "S" ? GetsimSerial(supp.Msisdn) : string.Empty,
                                        //       AccountType = "S",
                                        //       PrinMsisdn = v.ExternalId // != supp.Msisdn ? v.ExternalId : string.Empty
                                        //       //IsGreater2GBCompExists = is2GBCompExistOnPkg
                                        //   });
                                    }

                                }

                            }
                            #endregion
                        }
                        else
                        {
                            if (!v.IsMISM)
                            {
                                if (v.ServiceInfoResponse.prinSuppInd != "S" && v.ServiceInfoResponse.prinSuppInd != "" && !v.IsMISM)
                                {
                                    if (!lstMsisdnList.Contains(v.ExternalId))
                                    {
                                        lstMsisdnList.Add(v.ExternalId);


                                        //Add only Principal Lines
                                        SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = v.AcctExtId,
                                                        ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        Category = v.Account != null ? v.Account.Category : string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = v.SusbcrNo,
                                                        SubscribeNoResets = v.SusbcrNoResets,
                                                        externalId = v.ExternalId,
                                                        accountExtId = v.AcctExtId,
                                                        accountIntId = v.AcctIntId,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                                        IsMISM = v.IsMISM,
                                                        AccountType = "P",
                                                        PrinMsisdn = string.Empty,
                                                        HasSupplyAccounts = false
                                                    });
                                    }
                                    if (v.PrinSuppResponse != null && v.PrinSuppResponse.itemList != null && v.PrinSuppResponse.itemList.Count > 0)
                                    {
                                        #region "Supplimentary Accounts"
                                        foreach (var supp in v.PrinSuppResponse.itemList)
                                        {
                                            if (v.ExternalId != supp.msisdnField)
                                            {
                                                if (!lstMsisdnList.Contains(supp.msisdnField))
                                                {
                                                    var suppAccount = AcctListByICResponse.itemList.Where(i => i.ExternalId == supp.msisdnField && i.ServiceInfoResponse != null && i.ServiceInfoResponse.prinSuppInd == "S" && !i.IsMISM).FirstOrDefault();
                                                    foreach (var item in SuppListAccounts.SuppListAccounts.Where(a => a.externalId == v.ExternalId))
                                                    {
                                                        item.HasSupplyAccounts = true;
                                                    }
                                                    if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                                    {
                                                        if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                            supAccountStatus = "S";
                                                        else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                            supAccountStatus = "D";
                                                        else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                            supAccountStatus = "T";
                                                        else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                            supAccountStatus = "B";
                                                        else
                                                            supAccountStatus = "A";
                                                    }
                                                    lstMsisdnList.Add(supp.msisdnField);

                                                    SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId,
                                                        ActiveDate = string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        Category = string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = supp.subscr_noField,
                                                        SubscribeNoResets = supp.subscr_no_resetsField,
                                                        externalId = supp.msisdnField,
                                                        accountExtId = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId, 
                                                        accountIntId = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctIntId) ? suppAccount.AcctIntId : supp.acct_noField,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        AccountStatus = supAccountStatus,
                                                        IsMISM = v.IsMISM,
                                                        AccountType = "S",
                                                        PrinMsisdn = v.ExternalId != supp.msisdnField ? v.ExternalId : string.Empty
                                                    });
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }

                            }
                            else
                            {
                                //Listing the MISM LIST
                                #region "MISM Accounts"
                                if ((v.ServiceInfoResponse.prinSuppInd == "P" || v.ServiceInfoResponse.prinSuppInd != string.Empty) && v.IsMISM)
                                {
                                    if (!lstMsisdnList.Contains(v.ExternalId))
                                    {
                                        lstMsisdnList.Add(v.ExternalId);
                                        
                                        SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = v.AcctExtId,
                                                        ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        Category = v.Account != null ? v.Account.Category : string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = v.SusbcrNo,
                                                        SubscribeNoResets = v.SusbcrNoResets,
                                                        externalId = v.ExternalId,
                                                        accountExtId = v.AcctExtId,
                                                        accountIntId = v.AcctIntId,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                                        IsMISM = v.IsMISM,
                                                        AccountType = "P",
                                                        PrinMsisdn = string.Empty
                                                    });
                                    }
                                }
                                

                                if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "P" && v.PrinSupplimentaryResponse != null && v.PrinSupplimentaryResponse.itemList.Count > 0)
                                {

                                    foreach (var supp in v.PrinSupplimentaryResponse.itemList)
                                    {
                                        if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                        {
                                            if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                supAccountStatus = "S";
                                            else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                supAccountStatus = "D";
                                            else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                supAccountStatus = "T";
                                            else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                supAccountStatus = "B";
                                            else
                                                supAccountStatus = "A";
                                        }

                                        string accountNumber = string.Empty;
                                        var suppAccount = AcctListByICResponse.itemList.Where(s => s.ExternalId == supp.msisdnField && s.ServiceInfoResponse.prinSuppInd == "S" && !s.IsMISM).FirstOrDefault();
                                        accountNumber = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId;

                                        SuppListAccounts.SuppListAccounts.Add(
                                           new AddSuppInquiryAccount
                                           {
                                               AccountNumber = accountNumber,
                                               ActiveDate = string.Empty,
                                               Address = v.Account != null ? v.Account.Address : string.Empty,
                                               Category = string.Empty,
                                               CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                               Holder = supp.cust_nmField, 
                                               IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                               IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                               MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                               Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                               SubscribeNo = supp.subscr_noField,
                                               SubscribeNoResets = supp.subscr_no_resetsField,
                                               externalId = supp.msisdnField,
                                               accountExtId = v.AcctExtId,
                                               accountIntId = supp.acct_noField,
                                               AccountName = v.ServiceInfoResponse.lob,
                                               AccountStatus = supAccountStatus, 
                                               IsMISM = supp.IsMISM,
                                               AccountType = "S",
                                               PrinMsisdn = v.ExternalId
                                           });
                                    }

                                }

                            }
                                #endregion
                        }

                    }
                }

            }
            return View(SuppListAccounts);
        }

        /// <summary>
        /// Display the list of components and contracts available for the account.
        /// </summary>
        /// <returns></returns>
        public ActionResult ContractCheck()
        {
            Session["ContractType"] = null;//08042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            Session[SessionKey.PenaltyWaiveOff.ToString()] = null;
            Session["SelectAccountDtls"] = Request.QueryString["SelectAccountDtls"].ToString2();
            Session["OrderType"] = (int)MobileRegType.Contract;
            var contractVM = PrepareContractVM();
            return View(contractVM);
        }

        private ContractVM PrepareContractVM()
        {
            ContractVM contractVM = new ContractVM();
            string SelectAccountDtls = null;
            if (!ReferenceEquals(Session["SelectAccountDtls"], null))
            {
                SelectAccountDtls = Session["SelectAccountDtls"].ToString();

                if (SelectAccountDtls != null)
                {
                    string[] strAccountDtls = SelectAccountDtls.Split(',');

                    contractVM.KenanAccountNo = strAccountDtls[0];
                    contractVM.MSISDN = strAccountDtls[1];
                    contractVM.SubscriberNo = strAccountDtls[2];
                    contractVM.SubscriberNoResets = strAccountDtls[3];
                    contractVM.AccountType = strAccountDtls[6];
                    contractVM.SimType = strAccountDtls[5];
                    Session[SessionKey.MSISDN.ToString()] = Session[SessionKey.ExternalID.ToString()] = strAccountDtls[1];

                    Session[SessionKey.KenanACNumber.ToString()] = strAccountDtls[0];

                    using (var proxy = new UserServiceProxy())
                    {
                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.Contract, "Contract Check", Session["KenanACNumber"].ToString2(), "");
                    }

                    int ContractRemainMonths = 0;
                    Online.Registration.Web.Areas.CRP.CRPViewModels.DateDifference datediff;
                    IList<SubscriberICService.PackageModel> packages;
                    List<SubscriberICService.retrievePenalty> penaltybyMSISDN = new List<SubscriberICService.retrievePenalty>();
                    ContractDetails ContractDet = new ContractDetails();
                    List<ContractDetails> ContractsList = new List<ContractDetails>();
                    List<string> mandatoryPkgIds = new List<string>();
                    string packageId = "";
                    string packageDesc = "";
                    packages = GetPackageDetails(contractVM.MSISDN, ConfigurationManager.AppSettings["KenanMSISDN"]);

                    if (!ReferenceEquals(packages, null))
                    {
                        mandatoryPkgIds = packages.Where(p => p.PackageDesc.ToLower().Contains("mandatory")).Select(p => p.PackageID).ToList();

                        if (mandatoryPkgIds != null && mandatoryPkgIds.Any())
                        {
                            packageId = mandatoryPkgIds[0];
                        }
                        
                        packageDesc = packages.Where(p => p.PackageID == packageId).Select(p => p.PackageDesc).FirstOrDefault();
                    }
                    int planID = 0;
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var response1 = proxy.FindPackageOnKenanCode(packageId);
                        foreach (var item in response1)
                        {
                            if (item.Value.ToLower().Replace(" ", string.Empty).Equals(packageDesc.ToLower().Replace(" ", string.Empty).ToString()))
                            {
                                planID = item.Key.ToInt();
                                break;
                            }
                        }
                    }

                    contractVM.PackageDesc = packageDesc;
                    contractVM.PackageId = planID.ToString();
                    contractVM.Package = packageId;

                    string response = string.Empty;
                    var resp_existing = new SmartGetExistingPackageDetailsResp();
                    List<string> vasNames = new List<string>();
                    List<string> vasNamesList = new List<string>();
                    string price = null;

                    ComponentViewModel lstComponents = new ComponentViewModel();
                    NewComponents component;
                    lstComponents.NewComponentsList = new List<NewComponents>();
                    lstComponents.OldPackagesList = new List<OldPackages>();
                    RegistrationServiceProxy proxyReg = new RegistrationServiceProxy();
					var activeContract = proxyReg.GetContractsList().Where(x => x.Active).ToList();
                    IEnumerable<string> lstContracts = Enumerable.Empty<string>();

                    if (!ReferenceEquals(activeContract, null))
                    {
                        lstContracts = activeContract.Select(c => c.KenanCode);
                    }
                    int packID = 0;

                    if (!ReferenceEquals(packages, null))
                    {
                        foreach (var item in packages)
                        {
                            using (var proxy_kenan = new RegistrationServiceProxy())
                            {
                                resp_existing = proxy_kenan.ExistingPackageDetails(item.PackageID.ToInt());

                                foreach (var vcomponent in item.compList)
                                {
                                    if (!lstContracts.Contains(vcomponent.componentId))
                                    {
                                        component = new NewComponents();

                                        if (resp_existing != null && resp_existing.ExistingPackageDetails.PlanType != null)
                                        {
                                            response = proxy_kenan.GetlnkpgmbdlpkgcompidForKenancomponent(resp_existing.ExistingPackageDetails.PlanType, Convert.ToInt32(item.PackageID), vcomponent.componentId.ToString());
                                            if (response != null && response.Contains(","))
                                            {
                                                if ((vcomponent.componentId.ToString() != ConfigurationSettings.AppSettings["SM30CompID"].ToString2()) && (vcomponent.componentId.ToString() != ConfigurationSettings.AppSettings["MIRewardsCompID"].ToString2()) && vcomponent.componentId.ToString() !="46473" )
                                                {
                                                    vasNames.Add(response.Split(',')[0]);
                                                    price = response.Split(',')[1];
                                                    component.NewComponentId = response.Split(',')[0];
                                                }
                                            }
                                            component.NewComponentKenanCode = vcomponent.componentId;
                                            component.NewComponentName = vcomponent.componentDesc;
                                            component.NewPackageKenanCode = item.PackageID;
                                            component.IsChecked = true;

                                        }
                                        if (price == string.Empty || price == null)
                                            price = "0.00";

                                        vasNamesList.Add(vcomponent.componentDesc + " (RM " + price + ")");
                                        lstComponents.NewComponentsList.Add(component);
                                    }
                                }
                            }
                        }
                    }

                    Session["_componentViewModel"] = lstComponents;

                    contractVM.VasComponents = string.Join(",", vasNames.Distinct().ToList().ToArray());
                    contractVM.VasNames = string.Join(",", vasNamesList.Distinct().ToList().ToArray());

                    List<string> lstIDs = new List<string>();
                    using (RegistrationServiceProxy registrationServiceProxy = new RegistrationServiceProxy())
                        lstIDs = registrationServiceProxy.getRebateContractIDs();
                    using (var Smartproxy = new retrieveSmartServiceInfoProxy())
                    {

                        Session["penaltybyMSISDN"] = contractVM.penaltybyMSISDN = penaltybyMSISDN = Smartproxy.retrievePenalty(contractVM.MSISDN, ConfigurationManager.AppSettings["KenanMSISDN"], contractVM.SubscriberNo, contractVM.SubscriberNoResets, userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
                                 password: Properties.Settings.Default.retrieveAcctListByICResponsePassword).Where(p => !lstIDs.Contains(p.ContractName)).ToList();
                    }

                    Items Item = WebHelper.Instance.PPIDInfo(new WebHelper.PPIDFilter() { ExternalId = contractVM.MSISDN, EliminateDStatusLines = true }).FirstOrDefault();
                    String libforwaiver = Util.getMarketCodeLiberalisationMOC(((retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails")), "LIBERALISATION");
                    List<Online.Registration.DAL.Models.Contracts> lstContractsAll = new List<Contracts>();

                    using (SmartRegistrationServiceProxy Smartproxy = new SmartRegistrationServiceProxy())
                    {
                        lstContractsAll = Smartproxy.GetContractsList().ToList();
                    }

					List<ContractDetails> DFContracts = new List<ContractDetails>();
					List<ContractDetails> AFContracts = new List<ContractDetails>();
					List<ContractDetails> OtherContracts = new List<ContractDetails>();

                    foreach (var item in penaltybyMSISDN)
                    {
                        ContractDetails Contract = new ContractDetails();
                        ContractDet.ActiveDate = DateTime.ParseExact(item.startDate.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        ContractDet.EndDate = DateTime.ParseExact(item.endDate.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        datediff = new Online.Registration.Web.Areas.CRP.CRPViewModels.DateDifference(DateTime.Now, ContractDet.EndDate);
                        ContractDet.ContractDuration = Convert.ToInt16(datediff.Years) * 12 + Convert.ToInt16(datediff.Months);
                        if (ContractDet.ActiveDate > DateTime.Now)
                        {

                            datediff = new Online.Registration.Web.Areas.CRP.CRPViewModels.DateDifference(ContractDet.ActiveDate, ContractDet.EndDate);
                        }
                        else
                        {
                            datediff = new Online.Registration.Web.Areas.CRP.CRPViewModels.DateDifference(DateTime.Now, ContractDet.EndDate);
                        }
                        ContractDet.ContractDuration = Convert.ToInt16(datediff.Years) * 12 + Convert.ToInt16(datediff.Months);

                        if (datediff.Days >= 15)
                        {
                            ContractDet.ContractDuration = ContractDet.ContractDuration + 1;
                        }
                        //Evan-150323 Change for ContractCheck sync with Kenan - Start
                        //ContractDet.ContractRemaining = ContractDet.ContractDuration.ToString2() + " Months";
                        ContractDet.ContractRemaining = Util.getDiffDate(ContractDet.EndDate, ContractDet.ActiveDate).ToString2();
                        //Evan-150323 Change for ContractCheck sync with Kenan - End
                        ContractDet.ComponentDesc = item.ContractName;
                        if (DateTime.Compare(ContractDet.EndDate, DateTime.Now) > 0)
                        {
                            Contract.ComponentDesc = ContractDet.ComponentDesc;
                            Contract.ActiveDate = ContractDet.ActiveDate;
                            if (ContractDet.ActiveDate != null)
                                Contract.ContractStartDate = ContractDet.ActiveDate.ToString("dd-MMM-yyyy");
                            Contract.EndDate = ContractDet.EndDate;
                            if (ContractDet.EndDate != null)
                                Contract.ContractEndDate = ContractDet.EndDate.ToString("dd-MMM-yyyy");
                            Contract.ContractDuration = (ContractDet.EndDate.Subtract(ContractDet.ActiveDate).TotalDays / 30).ToInt();
                            double pendingDays = 0;
                            //if (ContractDet.ActiveDate > DateTime.Now)
                            //{
                            //    pendingDays = ContractDet.EndDate.Subtract(ContractDet.ActiveDate).TotalDays;
                            //}
                            //else
                            //{
                            //    pendingDays = ContractDet.EndDate.Subtract(DateTime.Now).TotalDays;
                            //}

                            
                            Contract.ContractRemaining = ContractDet.ContractRemaining;
                            Regex digitsOnly = new Regex(@"[^\d]");
                            pendingDays = digitsOnly.Replace(Contract.ContractRemaining, "").ToInt();
                            
                            Contract.Penalty = item.penalty.ToDecimal();
                            var pendingMonths = (Math.Round(pendingDays / 30.0, 0));
                            int gpFor12 = 2;
                            int gpFor24 = 4;
                            if (ConfigurationManager.AppSettings["gpFor12"] != null)
                            {
                                int.TryParse(ConfigurationManager.AppSettings["gpFor12"], out gpFor12);
                            }
                            if (ConfigurationManager.AppSettings["gpFor24"] != null)
                            {
                                int.TryParse(ConfigurationManager.AppSettings["gpFor24"], out gpFor24);
                            }
                            //Contract.ContractCustLib = Item != null && Item.Customer != null && !String.IsNullOrEmpty(Item.Customer.RiskCategory.ToString2()) ? Item.Customer.RiskCategory.ToString2() : (!String.IsNullOrEmpty(libforwaiver) ? libforwaiver : "");
                            Contract.ContractCustLib = !String.IsNullOrEmpty(libforwaiver) ? libforwaiver : ""; 
                            bool bPenaltyWaiverForLib = WebHelper.Instance.GetPenaltyWaiveOffForLiberalization(Contract.ContractCustLib, Item.Customer.NationalityID, false);
                            if (!string.IsNullOrWhiteSpace(Contract.ContractCustLib) && bPenaltyWaiverForLib)
                            {
                                if (Contract.ContractDuration == 12)
                                {
                                    Contract.Waived = pendingDays <= gpFor12;
                                }
                                else if (Contract.ContractDuration == 24)
                                {
                                    Contract.Waived = pendingDays <= gpFor24;
                                }
                            }

							Contract.isellContractGroup = Util.checkIsellContractGroup(item.componentId);
							Contract.isellContractGroup = Contract.isellContractGroup == "" ? item.penaltyType : Contract.isellContractGroup;
							Contract.Waived = !string.IsNullOrEmpty(Contract.isellContractGroup) && ("AF,DF").Contains(Contract.isellContractGroup) ? false : Contract.Waived;

							Contract.paramLists = item.paramLists;
							Contract.PenaltyWaiveOff = Contract.Waived ? Contract.Penalty : 0;

							if (Contract.isellContractGroup == "AF")
								AFContracts.Add(Contract);
							else if (Contract.isellContractGroup == "DF")
								DFContracts.Add(Contract);
							else
								OtherContracts.Add(Contract);

                            ContractsList.Add(Contract);
                            ContractRemainMonths += ContractDet.ContractDuration;
                        }

						contractVM.penaltyType = penaltybyMSISDN.FirstOrDefault() != null ? penaltybyMSISDN.Where(x => x.penaltyType == "DF").Any() ? "DF" : penaltybyMSISDN.FirstOrDefault().penaltyType : string.Empty;
                    }
                    contractVM.ContractRemainingMonths = ContractRemainMonths.ToString();

                    if (ContractDet != null)
                    {
                        if (ContractDet.ComponentDesc != null)
                        {

                            contractVM.ErrMsg = string.Empty;
                            if (ContractRemainMonths <= 6)
                            {
                                ContractDet.ContractExtend = "Y";
                                ContractDet.ContractExtendType = "B";
                            }
                            else if (ContractRemainMonths <= 18)
                            {
                                ContractDet.ContractExtend = "Y";
                                ContractDet.ContractExtendType = "12";
                            }
                            else if (ContractRemainMonths > 18)
                                ContractDet.ContractExtend = "N";
                        }
                        else
                        {
                            contractVM.ErrMsg = ConfigurationManager.AppSettings["NoContracts"];
                            contractVM.ErrMsg = contractVM.ErrMsg.ToString().Replace("[MSISDN]", contractVM.MSISDN);
                        }
                    }

                    else
                    {
                        contractVM.ErrMsg = ConfigurationManager.AppSettings["NoContracts"];
                        contractVM.ErrMsg = contractVM.ErrMsg.ToString().Replace("[MSISDN]", contractVM.MSISDN);
                    }

                    bool hasivalueplan = false;
                    if (ContractDet != null)
                    {
                        if (ContractDet.ContractExtend != null)
                        {
                            if (ContractDet.ContractExtend.ToUpper() == "Y")
                            {
                                foreach (var contract in packages)
                                {
                                    if (contract.PackageDesc.ToLower().IndexOf("ivalue") > 0 && contract.PackageDesc.ToLower().IndexOf("mandatory") > 0)
                                    {
                                        hasivalueplan = true;
                                        if (!string.IsNullOrEmpty(contract.PackageID))
                                            ContractDet.PackageID = contract.PackageID;
                                    }
                                }
                            }
                        }

                    }
                    if (!hasivalueplan)
                    {
                        if (ContractDet != null)
                            ContractDet.ContractExtend = "N";
                    }

                    #region Extend Based on MOC status
                    int ActiveContractsCount = 0;
                    if (ContractsList != null && ContractsList.Count() > 0)
                    {
                        ActiveContractsCount = ContractsList.Count;
                    }
                    if (Item != null && Item.Customer != null && Item.Customer.Moc != "" && !ReferenceEquals(Item.Customer.Moc, null))
                    {
                        if (ActiveContractsCount >= 3)
                        {
                            if (ContractDet != null)
                                ContractDet.ContractExtend = "N";
                        }
                    }
                    else
                    {
                        if (ActiveContractsCount >= 2)
                        {
                            if (ContractDet != null)
                                ContractDet.ContractExtend = "N";
                        }
                    }
                    #endregion

                    if (ContractsList.Count() == 0)
                    {
                        ContractDet = null;
                    }

                    if (strAccountDtls[4] == "no devices")
                    {
                        contractVM.ErrMsg = ConfigurationManager.AppSettings["NoDevices"];
                    }

                    contractVM.ContractDetailsList = ContractsList;

                    string compId = "";

                    foreach (ContractDetails cd in ContractsList)
                    {
                        List<RebateDataContractComponents> lstRebateContracts = new List<RebateDataContractComponents>();
                        lstRebateContracts = MasterDataCache.Instance.RebateContractComponents.ToList();
                        compId = lstRebateContracts.Where(c => c.ComponentName == cd.ComponentDesc).Select(a => a.ComponentID).FirstOrDefault();

                    }
                    Session["compId"] = compId;


                    contractVM.Packages = packages;
                    Session["Package"] = packages;
                    contractVM.contractExtend = ContractDet != null ? ContractDet.ContractExtend.ToString2() : string.Empty;
                    Session[SessionKey.ContractDetails.ToString()] = contractVM.ContractDetailsList ?? new List<ContractDetails>();

					checkContractGroup(contractVM);

					Session[SessionKey.ContractVM.ToString()] = contractVM;
                }
            }
            return contractVM;
        }

		private ContractVM checkContractGroup(ContractVM contractsVM)
		{
			if (contractsVM.ContractDetailsList != null && contractsVM.ContractDetailsList.Count() > 0)
			{
				if (contractsVM.ContractDetailsList.Where(x => x.isellContractGroup == "DF") != null && contractsVM.ContractDetailsList.Where(x => x.isellContractGroup == "DF").Any())
				{
					ContractGroupDetails contractGroupDetails = new ContractGroupDetails();
					contractGroupDetails.contractGroup = "DF";
					contractGroupDetails.contracts = contractsVM.ContractDetailsList.Where(x => x.isellContractGroup == "DF").ToList();
					contractsVM.contractGroupDetails.Add(contractGroupDetails);
				}
				if (contractsVM.ContractDetailsList.Where(x => x.isellContractGroup == "AF") != null && contractsVM.ContractDetailsList.Where(x => x.isellContractGroup == "AF").Any())
				{
					ContractGroupDetails contractGroupDetails = new ContractGroupDetails();
					contractGroupDetails.contractGroup = "AF";
					contractGroupDetails.contracts = contractsVM.ContractDetailsList.Where(x => x.isellContractGroup == "AF").ToList();
					contractsVM.contractGroupDetails.Add(contractGroupDetails);
				}
				if (contractsVM.ContractDetailsList.Where(x => !"AF,DF".Contains(x.isellContractGroup)) != null && contractsVM.ContractDetailsList.Where(x => !"AF,DF".Contains(x.isellContractGroup)).Any())
				{
					ContractGroupDetails contractGroupDetails = new ContractGroupDetails();
					contractGroupDetails.contractGroup = "";
					contractGroupDetails.contracts = contractsVM.ContractDetailsList.Where(x => !"AF,DF".Contains(x.isellContractGroup)).ToList();
					contractsVM.contractGroupDetails.Add(contractGroupDetails);
				}
			}

			return contractsVM;
		}

		public ActionResult SelectAccessory(int? type)
		{
			DeviceVM deviceVM = new DeviceVM();
			if (Session["model"] != null)
				deviceVM = (DeviceVM)Session["model"];

			ViewBag.GSTMark = Settings.Default.GSTMark;
			List<String> LabelReplace = new List<String>();
			LabelReplace.Add("GSTMark");
			ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
			var accessoriesVM = new AccessoriesVM();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            Session["af_showOffer"] = null;
            var msisdn = Session[SessionKey.ExternalID.ToString()].ToString2();
            var isMandatoryForAF = deviceVM != null && (deviceVM.subAction == Constants.TERMINATEASSIGN_ACC_ONLY || deviceVM.subAction == Constants.TERMINATEASSIGN_ALL);
			ContractVM contractVMFromSession = (ContractVM)Session[SessionKey.ContractVM.ToString()];
			if (contractVMFromSession != null)
			{
				var contractAFExist = contractVMFromSession.contractGroupDetails.Where(x => x.contractGroup == "AF").Any();
				isMandatoryForAF = (contractAFExist == false && deviceVM.subAction != Constants.TERMINATEASSIGN_ACC_ONLY) ? false : isMandatoryForAF;
			}

			if (deviceVM.subAction == Constants.TERMINATEASSIGN_ACC_ONLY)
				Session["_componentViewModel"] = null;

			Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(deviceVM.PackageId, deviceVM.ModelId, deviceVM.VasComponents, deviceVM.VasNames, Session["RegMobileReg_ContractID"].ToString2(), null);
			ViewBag.imageURLForAccessory = WebHelper.Instance.GetImageURLFromModelID(dropObj);
            ViewBag.isMandatoryForAF = isMandatoryForAF;

            var afEligibilityCheck = Session[SessionKey.AF_Eligibility.ToString()].ToBool();
            var dfException = Session["af_showOffer"].ToString2() == "show" ? true : false;
            var isDealer = Session["IsDealer"].ToBool();

            var allServiceDetails = (retrieveAcctListByICResponse)Session["AllServiceDetails"];
            var hasAccessoryFinancingContract = false;
            //var contractDetails = WebHelper.Instance.retrieveContractDetails(allServiceDetails, out hasAccessoryFinancingContract);
            hasAccessoryFinancingContract = WebHelper.Instance.CheckCurrentAFContract(msisdn);

            if (isDealer && Util.isCDPUUser() == false && afEligibilityCheck == false)
            {
                ViewBag.ErrorMessage = "Customer is not eligible for Accessories Financing. ";
                ViewBag.HardStopMessage = "Please click \"Next\" to continue to the next page";
            }
            else if (hasAccessoryFinancingContract && isMandatoryForAF == false)
            {
                ViewBag.ErrorMessage = "Customer has an existing active Accessories Financing contract for this MSISDN. Accessories Financing offer will not be displayed. ";
                ViewBag.HardStopMessage = "Please click \"Next\" to continue to the next page";
            }

			return View(accessoriesVM);
		}

		[HttpPost]
		public ActionResult SelectAccessory(FormCollection collection, AccessoriesVM accessoriesVM)
		{
			DeviceVM deviceVM = new DeviceVM();
			if (Session["model"] != null)
				deviceVM = (DeviceVM)Session["model"];

			var tabNumber = collection["TabNumber"].ToInt();
			var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
			Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(deviceVM.PackageId, deviceVM.ModelId, deviceVM.VasComponents, deviceVM.VasNames, Session["RegMobileReg_ContractID"].ToString2(), null);

			if (collection["af_showOffer"].ToString2() == "show")
			{
				Session["af_showOffer"] = collection["af_showOffer"].ToString2();
				Session[SessionKey.AF_Eligibility.ToString()] = true;
				return RedirectToAction("SelectAccessory");
			}

			// back button
			if (!string.IsNullOrEmpty(collection["hdnBack"].ToString2()) && collection["hdnBack"].ToString2() == "true")
			{
				if (deviceVM.subAction == Constants.TERMINATEASSIGN_ACC_ONLY)
				{
					string SelectAccountDtls = Session["SelectAccountDtls"].ToString2();
					return RedirectToAction("ContractCheck", new { SelectAccountDtls = SelectAccountDtls });
				}
				else
					return RedirectToAction(GetRegActionStep_AddContract((int)AddContractRegSteps.DevicePlan));
			}

			if (deviceVM.subAction == Constants.TERMINATEASSIGN_ACC_ONLY) {
				deviceVM.VasComponents = autoAddAccFinancingPackageComponents(deviceVM.VasComponents);
				Session["model"] = deviceVM;
			}

			var selectedAccessory = Session[SessionKey.Selected_Accessory.ToString()] as List<AccessoryVM> ?? new List<AccessoryVM>();
			DropFourHelpers.UpdateSelectedAccessory(dropObj, selectedAccessory);

			Session[SessionKey.DropFourObj.ToString()] = dropObj;

            if (deviceVM != null && deviceVM.subAction == Constants.TERMINATEASSIGN_ACC_ONLY)
				return RedirectToAction(GetRegActionStep_AddContract((int)AddContractRegSteps.PersonalDetails));
			else
				return RedirectToAction(GetRegActionStep_AddContract((int)AddContractRegSteps.VAS));
		}

        /// <summary>
        /// Display list of available devices based on existing package.
        /// </summary>
        /// <returns></returns>
        public ActionResult SelectDevice(int? type)
        {
            DropFourObj dropObj = new DropFourObj();
            Session[SessionKey.DedicatedSuppFlow.ToString()] = false;
            Session[SessionKey.Selected_Accessory.ToString()] = null;
            List<String> LabelReplace = new List<String>();
            ViewBag.GSTMark = Settings.Default.GSTMark;
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session["OrderType"] = (int)MobileRegType.Contract;

            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW && dropObj.mainFlow.RegType == -1)
            {
                dropObj.mainFlow.RegType = MobileRegType.Contract.ToInt();
            }

            Session[SessionKey.DropFourObj.ToString()] = dropObj;

            Session["_componentViewModel"] = null;
            Session["AddedVases"] = null;
            Session["RemovedVases"] = null;
            Session["AddedVasNames"] = null;
            Session["RemovedVasNames"] = null;
            Session["AddedVasPrices"] = null;
            Session["RemovedVasPrices"] = null;
            Session["ARVasGroupIds"] = null;
            Session[SessionKey.FromPromoOffer.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.TradeUpAmount.ToString()] = 0;   //RST GST
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;

            WebHelper.Instance.ClearSessions();
            WebHelper.Instance.ResetDepositSessions();
            Session[SessionKey.DeviceLowestContractPrice.ToString2()] = null;
            Session[SessionKey.KenanACNumber.ToString()] = null;
            Session["mdp_decision"] = null;
            Session["fromBackbutton"] = false;

            Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;

            DeviceVM deviceVM = new DeviceVM();

            try
            {
                if (Session["model"] != null)
                    deviceVM = (DeviceVM)Session["model"];
                deviceVM.Phone = GetAvailableModelImage(deviceVM);

                // get lowest price
                Session[SessionKey.DeviceLowestContractPrice.ToString2()] = WebHelper.Instance.GetLowestHighestPlanAndPrice(deviceVM.Phone.ModelImages, useArticleID: false);
                
                string SelectAccountDtls = null;
                if (deviceVM.Phone.ModelImages != null && !deviceVM.Phone.ModelImages.Any())
                {
                    SelectAccountDtls = deviceVM.KenanAccountNo + "," + deviceVM.MSISDN + "," + deviceVM.SubscriberNo + "," + deviceVM.SubscriberNoResets + "," + "no devices" + "," + deviceVM.AccountType + "," + deviceVM.AccountType + ",";
                    return RedirectToAction(GetRegActionStep_AddContract(AddContractRegSteps.ContractCheck.ToInt()), new { SelectAccountDtls = SelectAccountDtls });
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
            }

            dropObj.mainFlow.orderSummary = ConstructOrderSummary(deviceVM.PackageId, deviceVM.ModelId, deviceVM.VasComponents, deviceVM.VasNames, null, deviceVM.Penality);
            resetAdvancePaymentPrice(ref dropObj);
            Session[SessionKey.DropFourObj.ToString()] = dropObj;

            return View(deviceVM);
        }

        /// <summary>
        /// Display list of offers.
        /// </summary>
        /// <returns></returns>
        public ActionResult SelectVAS()
        {
            DeviceVM deviceVM = new DeviceVM();
            if (Session["model"] != null)
                deviceVM = (DeviceVM)Session["model"];
            //Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            //Session["df_showOffer"] = null;
            Session["contractName"] = null;
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var contractVM = PrepareContractVM();
            string vasComponents = contractVM.VasComponents;
            //string vasComponents = deviceVM.VasComponents;
            //deviceVM.VasComponents = string.Empty;
			var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

            string[] components = vasComponents.Split(',');

            // to prevent reset VasComponent value if it's coming from back button and df exceptional
            if (Session["df_showOffer"].ToString2() != "show" && !Session["fromBackbutton"].ToBool())
            {
                deviceVM.VasComponents = string.Empty;
                foreach (string component in components)
                {
                    if (!string.IsNullOrEmpty(component))
                        if (!string.Equals(component, deviceVM.OfferId))
                            deviceVM.VasComponents = deviceVM.VasComponents + component + ",";
                }
            }

            //to remove selected Offer that create double contract in one transaction: 
            if (!string.IsNullOrEmpty(deviceVM.VasComponents) && !string.IsNullOrEmpty(deviceVM.OfferId) && Session["fromBackbutton"].ToBool())
            {
                if (deviceVM.VasComponents.Contains(deviceVM.OfferId))
                {
                    deviceVM.VasComponents = deviceVM.VasComponents.Replace(deviceVM.OfferId, String.Empty);
                }
            }

            Session["df_showOffer"] = null;
            Session["reachVAS"] = null;

            ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();

            if ((Session[SessionKey.PPID.ToString()].ToString() != "") && (Session[SessionKey.PPID.ToString()].ToString() != null))
            {
                if (!ReferenceEquals(Session[SessionKey.RedirectURL.ToString()], null))
                {
                    Session.Contents.Remove("RedirectURL");
                    Session[SessionKey.MenuType.ToString()] = "1";
                }

                //Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = deviceVM.OrderSummary = ConstructOrderSummary(deviceVM.PackageId, deviceVM.ModelId, deviceVM.VasComponents, deviceVM.VasNames, Session["RegMobileReg_ContractID"].ToString2(), null);


                Lstvam = GetAvailablePackageComponents(deviceVM.PackageId.ToInt(), deviceVM.ContractType, deviceVM.ModelId, isMandatory: false);

                string articleid = Session[SessionKey.ArticleId.ToString()].ToString();
                string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
				string planID = deviceVM.Package;
				string contractRemainingMonths = deviceVM.ContractRemainingMonths.ToString2();
				string contractType = deviceVM.ContractType.ToString2();

				// Drop 5
				Lstvam.ContractOffers = WebHelper.Instance.GetContractOffersCRP(Lstvam, articleid,planID, MOCStatus, contractRemainingMonths, contractType) ;

                if (Lstvam.ContractVASes != null)
                    Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);
                if (Lstvam.AvailableVASes != null)
                    Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
                if (Lstvam.ContractOffers != null)
                {
                    Lstvam2.ContractOffers.AddRange(Lstvam.ContractOffers);
                    //Session["OfferId"] = Lstvam2.ContractOffers[Lstvam2.ContractOffers.Count() - 1].Id;
                }

                //RST GST
                foreach (var tradeUpAmountList in Lstvam2.ContractOffers)
                {
                    if (tradeUpAmountList.isTradeUp.ToBool())
                    {
                        var OfferID = tradeUpAmountList.Id;
                        var tradeUpAmounts = tradeUpAmountList.TradeUpAmount.ToString().Split(',').ToList();

                        foreach (var tradeUpAmount in tradeUpAmounts)
                        {
                            var tradeUpList = new TradeUpComponent();
                            var price = tradeUpAmount.ToInt();
                            tradeUpList.OfferId = OfferID;
                            tradeUpList.Name = "RM " + price;
                            tradeUpList.Price = price * -1;
                            //tradeUpList.isSelected = false;
                            Lstvam2.TradeUpList.Add(tradeUpList);
                        }
                    }

                }
                //End RST

                using (var proxy = new CatalogServiceProxy())
                {
                    var masterDFList = proxy.GetAllDeviceFinancingInfo();
                    BundlepackageResp respon = new BundlepackageResp();
					int pbpcID = Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]);
					respon = proxy.GetMandatoryVas(pbpcID);

					for (int i = 0; i < respon.values.Count(); i++)
					{
						if (respon.values[i].Plan_Type == Settings.Default.Device_Financing_Component)
						{
							#region device financing
							retrieveAcctListByICResponse allsServiceDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
							var dfEligibilityCheck = Session[SessionKey.DF_Eligibility.ToString()].ToBool();
							var dfException = Session["df_showOffer"].ToString2() == "show" ? true : false;
                            dropObj.mainFlow.isDeviceFinancingExceptionalOrder = dfException;

                            if (dfEligibilityCheck || dfException || Roles.IsUserInRole("MREG_CUSR"))
							{
                                string dfarticleid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ArticleId.ToString2();
                                string uomCode = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).VasVM.uomCode.ToString2();
								Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

								if (Session["RegMobileReg_Type"].ToInt() == 1 && Lstvam.AvailableVASes.Any())
								{
									var deviceFinancingInfo = proxy.GetDeviceFinancingInfoByArticleID(articleid, pbpcID);
									if (deviceFinancingInfo.Any())
									{
										var contractListDF = Lstvam.AvailableVASes.Where(x => !string.IsNullOrWhiteSpace(x.Value)).ToList();
										var upgradeFeeKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_UPGRADEFEE);
										var mocWaiverKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_MOC_WAIVER);
										var deviceFinancingVAS = Lstvam.AvailableVASes.Where(x => x.isHidden == false).ToList();
										var mocVAS = Lstvam.AvailableVASes.Where(x => x.KenanCode == mocWaiverKenanCode).ToList();

										if (deviceFinancingVAS.Any())
										{
											deviceFinancingVAS.FirstOrDefault(x => x.KenanCode == upgradeFeeKenanCode).Price = deviceFinancingInfo.FirstOrDefault().UpgradeFee;
											deviceFinancingVAS.FirstOrDefault(x => x.KenanCode == upgradeFeeKenanCode).oneTimeUpgrade = deviceFinancingInfo.FirstOrDefault().OneTimeUpgrade;
											Lstvam2.DeviceFinancingVAS.AddRange(deviceFinancingVAS);

											if (mocVAS.Any())
												Lstvam2.DeviceFinancingVAS.AddRange(mocVAS);

											//var DFplanID = proxy.PgmBdlPckComponentGet(new int[] { respon.values[i].BdlDataPkgId.ToInt() }).SingleOrDefault().KenanCode;
											foreach (var df in contractListDF)
											{
												var df_contractOffer = WebHelper.Instance.GetContractOffersDF(new string[] { df.KenanCode }.ToList(), articleid, planID, string.Empty);

												Lstvam2.ContractOffers.AddRange(df_contractOffer);
												foreach (var offer in df_contractOffer)
												{
													var oneTimeFee = masterDFList.Where(x => x.ArticleID == articleid && x.UOMCode.Contains(uomCode) && x.PlanID == pbpcID).FirstOrDefault().OneTimeUpgrade;
													if (!Lstvam2.df_offerOneTimeFee.ContainsKey(offer.UOMCode))
													{
														Lstvam2.df_offerOneTimeFee.Add(offer.UOMCode, oneTimeFee);
													}
												}
											}
                                            if (Lstvam2.ContractOffers.Any() && !Session["fromBackbutton"].ToBool())
												Session["OfferId"] = Lstvam2.ContractOffers[Lstvam2.ContractOffers.Count() - 1].Id;

											string processingMsisdn = string.Empty;
											var dfContract = WebHelper.Instance.GetAvailablePackageComponents(respon.values[i].BdlDataPkgId.ToInt(), ref processingMsisdn, isMandatory: false, isDF: true);

											Lstvam2.ContractVASes.AddRange(dfContract.ContractVASes);
										}
									}
								}
							}
							#endregion
						} // end if loop
					}
                }
				if (dropObj.mainFlow.phone != null && !string.IsNullOrWhiteSpace(dropObj.mainFlow.phone.DeviceArticleId))
				{
					using (var catProxy = new CatalogServiceProxy())
					{
						var anyDFDeviceEligible = catProxy.GetDeviceFinancingInfoByArticleID(dropObj.mainFlow.phone.DeviceArticleId, dropObj.mainFlow.PkgPgmBdlPkgCompID);
						Lstvam2.exceptionalAvailable = anyDFDeviceEligible != null ? anyDFDeviceEligible.Any() : false;
					}
				}

                deviceVM.VAS = Lstvam2;

                deviceVM.Components = LoadComponents();

                Session["VAS"] = Lstvam2;
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = deviceVM.OrderSummary = ConstructOrderSummary(deviceVM.PackageId, deviceVM.ModelId, deviceVM.VasComponents, deviceVM.VasNames, Session["RegMobileReg_ContractID"].ToString2(), null);
                Session["model"] = deviceVM;


                return View(deviceVM);

            }
            else
            {
                Session[SessionKey.RedirectURL.ToString()] = "Registration/SelectVAS";
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// Displays customer information.
        /// </summary>
        /// <returns></returns>
        public ActionResult PersonalDetails()
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            DeviceVM deviceVM = new DeviceVM();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//09042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow
            var idDocumentFront = string.Empty;
            var idDocumentBack = string.Empty;
            var idDocumentOther = string.Empty;
            var idDocumentFrontName = string.Empty;
            var idDocumentBackName = string.Empty;
            var idDocumentOtherName = string.Empty;
            var inputJustification = string.Empty;
            var waiverInfo = string.Empty;
            var surveyfeedback = string.Empty;
            var surveyanswer = string.Empty;
            var surveyanswerrate = string.Empty;
            var ssoValue = string.Empty;
            var ssoEmailAddress = string.Empty;
            var QueueNo = string.Empty;
            var personalDetailsVM = new PersonalDetailsVM();

            personalDetailsVM = Util.FormPersonalDetailsandCustomer(personalDetailsVM);
            personalDetailsVM = Util.CheckDisablePersonalDetailsPage(personalDetailsVM);
            //Drop 5 B18 - Ricky - 2015.05.12 - Populate the default value for Outlet/Sales Code
            WebHelper.Instance.PopulateDealerInfo(personalDetailsVM);

            //GTM e-Billing CR - Ricky - 2014.09.25 - Start
            string SelectAccountDtls = Session[SessionKey.KenanACNumber.ToString()].ToString2();
            var availableMsisdn = WebHelper.Instance.GetAvailableMsisdnForSmsNotification(SelectAccountDtls);
            var billDeliveryVM = new BillDeliveryVM();
            billDeliveryVM.AcctExtId = SelectAccountDtls;
            
            if (string.IsNullOrEmpty(Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2()))
            {
                //set default value for first landing, which is to be retrieved from EBPS
                WebHelper.Instance.BillDeliveryRetrieve(billDeliveryVM);
                if (!billDeliveryVM.Result)
                {
                    TempData["ErrorMessage"] = string.Format(ConfigurationManager.AppSettings["BillDelivery_ErrorMessage_EBPSNotification"].ToString() + billDeliveryVM.ErrorMessage);
                }
                personalDetailsVM.AvailableMsisdn = availableMsisdn;
                personalDetailsVM.EmailBillSubscription = billDeliveryVM.BillDeliveryDetails.EmailBillSubscription;
                personalDetailsVM.EmailAddress = billDeliveryVM.BillDeliveryDetails.EmailAddress;
                personalDetailsVM.SmsAlertFlag = billDeliveryVM.BillDeliveryDetails.SmsAlertFlag;
                personalDetailsVM.SmsNo = billDeliveryVM.BillDeliveryDetails.SmsNo;
            }
            else
            {
                //retrieve previous value from session if user press the back button
                personalDetailsVM.AvailableMsisdn = availableMsisdn;
                personalDetailsVM.EmailBillSubscription = Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2();
                personalDetailsVM.EmailAddress = Session[SessionKey.billDelivery_emailAddress.ToString()].ToString2();
                personalDetailsVM.SmsAlertFlag = Session[SessionKey.billDelivery_smsAlertFlag.ToString()].ToString2();
                personalDetailsVM.SmsNo = Session[SessionKey.billDelivery_smsNo.ToString()].ToString2();
            }
            //GTM e-Billing CR - Ricky - 2014.09.25 - End

            if (Session["model"] != null)
                deviceVM = (DeviceVM)Session["model"];

            #region 06052015 - Anthony - [Paperless CR] Customer Order Acknowledgement
			if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.frontDocumentName))
            {
                idDocumentFront = deviceVM.PersonalDetails.CustomerPhoto;
                idDocumentFrontName = deviceVM.PersonalDetails.frontDocumentName;
            }

			if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.backDocumentName))
            {
                idDocumentBack = deviceVM.PersonalDetails.AltCustomerPhoto;
                idDocumentBackName = deviceVM.PersonalDetails.backDocumentName;
            }

			if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.othDocumentName))
            {
                idDocumentOther = deviceVM.PersonalDetails.Photo;
                idDocumentOtherName = deviceVM.PersonalDetails.othDocumentName;
            }

            if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.Justification))
            {
                inputJustification = deviceVM.PersonalDetails.Justification;
            }

            if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.hdnWaiverInfo))
            {
                waiverInfo = deviceVM.PersonalDetails.hdnWaiverInfo;
            }

            if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.SurveyFeedBack))
            {
                surveyfeedback = deviceVM.PersonalDetails.SurveyFeedBack;
            }

            if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.SurveyAnswer))
            {
                surveyanswer = deviceVM.PersonalDetails.SurveyAnswer;
            }

            if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.SurveyAnswerRate))
            {
                surveyanswerrate = deviceVM.PersonalDetails.SurveyAnswerRate;
            }

            if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.SingleSignOnValue))
            {
                ssoValue = deviceVM.PersonalDetails.SingleSignOnValue;
            }

            if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.SingleSignOnEmailAddress))
            {
                ssoEmailAddress = deviceVM.PersonalDetails.SingleSignOnEmailAddress;
            }

            if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.QueueNo))
            {
                QueueNo = deviceVM.PersonalDetails.QueueNo;
            }

            #endregion

            deviceVM.PersonalDetails = new PersonalDetailsVM();
            deviceVM.PersonalDetails = personalDetailsVM;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = deviceVM.OrderSummary = ConstructOrderSummary(deviceVM.PackageId, deviceVM.ModelId, deviceVM.VasComponents, deviceVM.VasNames, deviceVM.OfferId, null);

            //var idCardTypeID = Session[SessionKey.RegMobileReg_IDCardType.ToString()] != null ? Session[SessionKey.RegMobileReg_IDCardType.ToString()] : null;
            
            #region populate idCardType and idCardNo - moved from _customer.cshtml
            int idCardTypeID = 0;
            string idCardNo = string.Empty;
            if (!ReferenceEquals(Session["PPIDInfo"], null))
            {
                retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                if (!ReferenceEquals(AcctListByICResponse, null) && !ReferenceEquals(AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList(), null))
                {
                    CustomizedCustomer CustomerPersonalInfo = WebHelper.Instance.constructPersonalInformationForView(Session["ExternalID"] != null ? Session["ExternalID"].ToString() : "0");
                    if (!String.IsNullOrEmpty(CustomerPersonalInfo.NewIC))
                    {
                        idCardTypeID = 1; idCardNo = CustomerPersonalInfo.NewIC;
                    }
                    else if (!String.IsNullOrEmpty(CustomerPersonalInfo.PassportNo))
                    {
                        idCardTypeID = 2; idCardNo = CustomerPersonalInfo.PassportNo;
                    }
                    else if (!String.IsNullOrEmpty(CustomerPersonalInfo.OldIC))
                    {
                        idCardTypeID = 3; idCardNo = CustomerPersonalInfo.OldIC;
                    }
                    else if (!String.IsNullOrEmpty(CustomerPersonalInfo.OtherIC))
                    {
                        idCardTypeID = 4; idCardNo = CustomerPersonalInfo.OtherIC;
                    }

                    deviceVM.PersonalDetails.Customer.NationalityID = CustomerPersonalInfo.NationalityID;
                    deviceVM.PersonalDetails.InputNationalityID = CustomerPersonalInfo.NationalityID;

                    #region Drop 5: Waiver for non-Drop 4 Flow
                    //09042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow - Start
                    if (deviceVM.PersonalDetails.Customer.NationalityID == 0 || deviceVM.PersonalDetails.InputNationalityID == 0)
                    {
                        deviceVM.PersonalDetails.Customer.NationalityID = CustomerPersonalInfo.Nation.ToInt();
                        deviceVM.PersonalDetails.InputNationalityID = CustomerPersonalInfo.Nation.ToInt();
                    }
                    if (dropObj.mainFlow != null && dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
                    {
                        var orderSummary = dropObj.mainFlow.orderSummary;
                        deviceVM.OrderSummary.MalayPlanAdv = orderSummary.MalayPlanAdv;
                        deviceVM.OrderSummary.OthPlanAdv = orderSummary.OthPlanAdv;
                        deviceVM.OrderSummary.MalyPlanDeposit = orderSummary.MalyPlanDeposit;
                        deviceVM.OrderSummary.OthPlanDeposit = orderSummary.OthPlanDeposit;
                        deviceVM.OrderSummary.MalyDevAdv = orderSummary.MalyDevAdv;
                        deviceVM.OrderSummary.OthDevAdv = orderSummary.OthDevAdv;
                        deviceVM.OrderSummary.MalayDevDeposit = orderSummary.MalayDevDeposit;
                        deviceVM.OrderSummary.OthDevDeposit = orderSummary.OthDevDeposit;
                    }
                    dropObj.personalInformation = deviceVM.PersonalDetails;
                    Session[SessionKey.DropFourObj.ToString()] = dropObj;
                    //09042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow - End
                    #endregion
                }
                
            }
            #endregion
            

            deviceVM.PersonalDetails.Customer.IDCardTypeID = idCardTypeID;
            deviceVM.PersonalDetails.Customer.IDCardNo = idCardNo;
            //deviceVM.PersonalDetails.Customer.IDCardNo = Session["RegMobileReg_IDCardNo"] == null ? "" : Session["RegMobileReg_IDCardNo"].ToString2();
            deviceVM.PersonalDetails.MSISDN1 = deviceVM.MSISDN;

            #region 06052015 - Anthony - [Paperless CR] Customer Order Acknowledgement
            deviceVM.PersonalDetails.CustomerPhoto = idDocumentFront;
            deviceVM.PersonalDetails.AltCustomerPhoto = idDocumentBack;
            deviceVM.PersonalDetails.Photo = idDocumentOther;
            deviceVM.PersonalDetails.frontDocumentName = idDocumentFrontName;
            deviceVM.PersonalDetails.backDocumentName = idDocumentBackName;
            deviceVM.PersonalDetails.othDocumentName = idDocumentOtherName;
            deviceVM.PersonalDetails.Justification = inputJustification;
            deviceVM.PersonalDetails.hdnWaiverInfo = waiverInfo;
            deviceVM.PersonalDetails.SurveyFeedBack = surveyfeedback;
            deviceVM.PersonalDetails.SurveyAnswer = surveyanswer;
            deviceVM.PersonalDetails.SurveyAnswerRate = surveyanswerrate;
            deviceVM.PersonalDetails.SingleSignOnValue = ssoValue;
            deviceVM.PersonalDetails.SingleSignOnEmailAddress = ssoEmailAddress;
            deviceVM.PersonalDetails.QueueNo = QueueNo;
            #endregion

            deviceVM.PersonalDetails.TacSmsMsisdn = WebHelper.Instance.retrieveCurrentMsisdnByController(this.GetType()); // 20151119 - w.loon - TAC Validation

            return View(deviceVM);
        }

        /// <summary>
        /// Displays the summary information.
        /// </summary>
        /// <param name="regID">Registration Id</param>
        /// <returns></returns>
        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSV,MREG_DSA,MREG_DAI,MREG_DSK,MREG_DCH,MREG_DIC,MREG_DAC")]
        [DoNoTCache]
        public ActionResult MobileRegSummary(int? regID)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var dropObj = Session[SessionKey.DropFourObj.ToString()] as DropFourObj != null 
                            ? (DropFourObj)Session[SessionKey.DropFourObj.ToString()]
                            : new DropFourObj();//09042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow
            DeviceVM deviceVM = new DeviceVM();
            if (Session["model"] != null)
                deviceVM = (DeviceVM)Session["model"];
            var personalDetailsVM = new PersonalDetailsVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            int paymentstatus = -1;
			var allRegDetails = new Online.Registration.DAL.RegistrationDetails();
            #region VLT ADDED CODE 21stFEB for Email functionality
            Session["RegID"] = regID;
            #endregion VLD ADDED CODE 21stFEB for Email functionality
            var KenanLogDetails = new DAL.Models.KenanaLogDetails();
            if (regID.ToInt() > 0)
            {

                ClearRegistrationSession();
                using (var proxy = new RegistrationServiceProxy())
                {
                    allRegDetails = proxy.GetRegistrationFullDetails(regID.Value);
                    if (allRegDetails.Customers.FirstOrDefault().IsVIP == 1 && !Roles.IsUserInRole("MREG_SV"))
                    {
                        Session["VIPFailMsg"] = true;
                        return RedirectToAction("StoreKeeper", "Registration");
                    }

                    WebHelper.Instance.MapRegistrationDetailsToVM(false, regID.Value.ToInt(), ref allRegDetails, out personalDetailsVM, isPDPA: false);

                    reg = allRegDetails.Regs.SingleOrDefault();

                    if (allRegDetails.lnkregdetails != null)
                    {
                        if (allRegDetails.lnkregdetails.SimType != null)
                            deviceVM.SimType = allRegDetails.lnkregdetails.SimType;
                        else
                            deviceVM.SimType = "Normal";
                    }
                    else
                    {
                        deviceVM.SimType = "Normal";
                    }

                    // Offer Name - Lindy
                    var addConOfferID = reg.OfferID;
                    var addConOfferName = "";

                    if (addConOfferID > 0)
                    {
                        var addConOffID = proxy.GetOffersById(addConOfferID.ToInt());
                        addConOfferName = addConOffID.OfferName;
                    }

                    Session["contractName"] = addConOfferName;

                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);
                    Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;

                    // Customer
                    personalDetailsVM.Customer = proxy.CustomerGet(new int[] { reg.CustomerID }).FirstOrDefault();

                    // Address
                    var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                    {
                        Active = true,
                        Address = new Address()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();
                    personalDetailsVM.Address = proxy.RegAddressGet(regAddrIDs).FirstOrDefault();

                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    // RegMdlGrpModels
                    var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                    {
                        RegMdlGrpModel = new RegMdlGrpModel()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();
                    if (regMdlGrpModelIDs.Count() > 0)
                        regMdlGrpModel = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).FirstOrDefault();

                    if (regMdlGrpModel.ID == 0)
                    {
                        Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
                    }
                    else
                    {
                        Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = regMdlGrpModel.Price.ToDecimal();
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                        deviceVM.DevicePrice = regMdlGrpModel.Price.ToString();
                        deviceVM.ModelId = regMdlGrpModel.ModelImageID.ToString();
                    }

                    // RegPgmBdlPkgComponent
                    var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                    {
                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();

                    // Main Line PgmBdlPkgComponent
                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    if (regPgmBdlPkgCompIDs.Count() > 0)
                        regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind()
                    {
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();

                    if (suppLineIDs.Count() > 0)
                    {
                        suppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    }

                    //<Start> - 20150914 - Samuel - display sales code in summary page
                    //personalDetailsVM.Registration = proxy.RegistrationGet(personalDetailsVM.RegID);
                    personalDetailsVM.RegAttributes = proxy.RegAttributesGetByRegID(personalDetailsVM.RegID);
                    /*using (var proxyOrg = new OrganizationServiceProxy())
                    {
                        if (!ReferenceEquals(personalDetailsVM.Registration, null) && !ReferenceEquals(personalDetailsVM.Registration.CenterOrgID, null) && personalDetailsVM.Registration.CenterOrgID != 0)
                        {
                            personalDetailsVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { personalDetailsVM.Registration.CenterOrgID }).FirstOrDefault();
                        }
                    }*/
                    var salesPerson = allRegDetails.Regs.FirstOrDefault().SalesPerson;
                    var orgID = allRegDetails.Regs.FirstOrDefault().CenterOrgID;
                    var outletCode = string.Empty;
                    var salesCode = string.Empty;

                    WebHelper.Instance.generateOutletAndSalesCode(orgID, salesPerson, personalDetailsVM.RegAttributes, out outletCode, out salesCode);
                    personalDetailsVM.OutletCode = outletCode;
                    personalDetailsVM.SalesCode = salesCode;
                    //<End> - 20150914 - Samuel - display sales code in summary page
                }

                if (suppLines.Count() > 0)
                    ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

                using (var proxy = new CatalogServiceProxy())
                {
                    string userICNO = Util.SessionAccess.User.IDCardNo;

                    if (userICNO != null && userICNO.Length > 0)
                    {
                        objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                    }
                    if (objWhiteListDetailsResp != null)
                    {
                        if (objWhiteListDetailsResp.Description != null)
                        {
                            personalDetailsVM._WhiteListDetails.Description = objWhiteListDetailsResp.Description;
                        }
                    }

                    if (regMdlGrpModel.ID > 0)
                        Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;


                    var mainLinePBPCs = new List<PgmBdlPckComponent>();
                    if (mainLinePBPCIDs.Count() > 0)
                        mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var DPCode = Properties.Settings.Default.RegType_DevicePlan;
                    var MAcode = Properties.Settings.Default.Master_Component;
                    var MPcode = Properties.Settings.Default.Main_package;
                    var CPCode = Properties.Settings.Default.ComplimentaryPlan;
                    var DCCode = Properties.Settings.Default.CompType_DataCon;
                    var ECCode = Properties.Settings.Default.Extra_Component;
                    var SPCode = Properties.Settings.Default.SupplimentaryPlan;
                    var OCCode = Properties.Settings.Default.Other_Component;


                    if (mainLinePBPCs.Count() > 0)
                    {
                        if (reg.OfferID != 0 && !string.IsNullOrEmpty(reg.ArticleID))
                        {
                            if (personalDetailsVM.CRPType == "S")
                            {
                                if (mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == SPCode)).Any())
                                    deviceVM.PackageId = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == SPCode)).FirstOrDefault().ID.ToString2();
                            }
                            else
                            {
                                if (mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode)).Any())
                                    deviceVM.PackageId = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode)).FirstOrDefault().ID.ToString2();
                            }
                        }

                        var vasIDs = string.Empty;
                        var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode || a.LinkType == SPCode || a.LinkType == OCCode || a.LinkType == "RC" || a.LinkType == "IC" || a.LinkType == "MD" || a.LinkType == "DF" || a.LinkType == "AF").Select(a => a.ID).ToList();

                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        vasIDs = vasIDs + ",";
                        Session[SessionKey.RegMobileReg_VasIDs.ToString()] = vasIDs;
                        deviceVM.VasComponents = vasIDs;
                    }
                }
                deviceVM.PersonalDetails = personalDetailsVM;

                dropObj.mainFlow.isDeviceFinancingOrder = WebHelper.Instance.determineDFOrderbyRegID(regID.ToInt());

            }

            if (reg.OfferID != null && reg.OfferID > 0)
            {
                deviceVM.OfferId = reg.OfferID.ToString2();
            }
            deviceVM.OrderSummary = ConstructOrderSummary(deviceVM.PackageId, deviceVM.ModelId, deviceVM.VasComponents, deviceVM.VasNames, deviceVM.OfferId, null, showVASAliasName: false);
            deviceVM.OrderSummary.IMEINumber = reg.IMEINumber;
            deviceVM.OrderSummary.SIMSerial = reg.SIMSerial;
            deviceVM.OrderSummary.PenaltyWaiveOffAmount = reg.PenaltyWaivedAmt;

            List<RegAttributes> objRegAttributes = new List<RegAttributes>();
            using (var proxyReg = new RegistrationServiceProxy())
            {
                objRegAttributes = proxyReg.RegAttributesGetByRegID(regID.ToInt());
                if (objRegAttributes != null)
                {
                    deviceVM.OrderSummary.DFRegAttribute = objRegAttributes;
                }
            }


            

            var inputJustification = string.Empty;
            if (regID.ToInt() > 0)
            {
                //RST GST
                if (!ReferenceEquals(regMdlGrpModel, null))
                {
                    deviceVM.OrderSummary.SelectedTradeUpComp = new TradeUpComponent()
                    {
                        Name = regMdlGrpModel.IsTradeUp.ToString(),
                        Price = regMdlGrpModel.TradeUpAmount.ToInt()
                    };
                }
                //End RST GST
            }
            else
            {
                if (dropObj.mainFlow != null && dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
                {
                    var orderSummary = dropObj.mainFlow.orderSummary;
                    deviceVM.OrderSummary.MalayPlanAdv = orderSummary.MalayPlanAdv;
                    deviceVM.OrderSummary.OthPlanAdv = orderSummary.OthPlanAdv;
                    deviceVM.OrderSummary.MalyPlanDeposit = orderSummary.MalyPlanDeposit;
                    deviceVM.OrderSummary.OthPlanDeposit = orderSummary.OthPlanDeposit;
                    deviceVM.OrderSummary.MalyDevAdv = orderSummary.MalyDevAdv;
                    deviceVM.OrderSummary.OthDevAdv = orderSummary.OthDevAdv;
                    deviceVM.OrderSummary.MalayDevDeposit = orderSummary.MalayDevDeposit;
                    deviceVM.OrderSummary.OthDevDeposit = orderSummary.OthDevDeposit;
                }
                
                #region 06052015 - Anthony - [Paperless CR] Customer Order Acknowledgement
                var idDocumentFront = deviceVM.PersonalDetails.CustomerPhoto;
                var idDocumentBack = deviceVM.PersonalDetails.AltCustomerPhoto;
                var idDocumentOther = deviceVM.PersonalDetails.Photo;
                var idDocumentFrontName = deviceVM.PersonalDetails.frontDocumentName;
                var idDocumentBackName = deviceVM.PersonalDetails.backDocumentName;
                var idDocumentOtherName = deviceVM.PersonalDetails.othDocumentName;
                var waiverInfo = deviceVM.PersonalDetails.hdnWaiverInfo;
				var surveyAnswer = deviceVM.PersonalDetails.SurveyAnswer;
                var surveyfeedback = deviceVM.PersonalDetails.SurveyFeedBack;
                var surveyAnswerRate = deviceVM.PersonalDetails.SurveyAnswerRate;
                var ssoValue = deviceVM.PersonalDetails.SingleSignOnValue;
                var ssoEmailAddress = deviceVM.PersonalDetails.SingleSignOnEmailAddress;
                inputJustification = deviceVM.PersonalDetails.Justification;
                #endregion

                #region 06052015 - Anthony - [Paperless CR] Customer Order Acknowledgement
                deviceVM.PersonalDetails.CustomerPhoto = idDocumentFront;
                deviceVM.PersonalDetails.AltCustomerPhoto = idDocumentBack;
                deviceVM.PersonalDetails.Photo = idDocumentOther;
                deviceVM.PersonalDetails.frontDocumentName = idDocumentFrontName;
                deviceVM.PersonalDetails.backDocumentName = idDocumentBackName;
                deviceVM.PersonalDetails.othDocumentName = idDocumentOtherName;
                deviceVM.PersonalDetails.hdnWaiverInfo = waiverInfo;
                deviceVM.PersonalDetails.Justification = inputJustification;
				deviceVM.PersonalDetails.SurveyAnswer = surveyAnswer;
				deviceVM.PersonalDetails.SurveyFeedBack = surveyfeedback;
                deviceVM.PersonalDetails.SurveyAnswerRate = surveyAnswerRate;
                deviceVM.PersonalDetails.SingleSignOnValue = ssoValue;
                deviceVM.PersonalDetails.SingleSignOnEmailAddress = ssoEmailAddress;
                #endregion

                //deviceVM.PersonalDetails.Customer.Gender = "M";
                //deviceVM.SimType = deviceVM.SimType.ToLower() == "true" ? "MISM" : "Normal";  //2015.11.16 Karyne - redundant logic of line --> objRegDetails.SimType = deviceVM.SimType.ToLower() == "true" ? "MISM" : "Normal";

                //Drop 5 B18 - Ricky - 2015.05.12 - Populate the default value for Outlet/Sales Code
                //WebHelper.Instance.PopulateDealerInfo(deviceVM.PersonalDetails);//reskin CR - move to personaldetails page

            }

            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = deviceVM.OrderSummary;

            #region CDPU Approval
            // lnkRegBREStatus
            using (var proxy = new RegistrationServiceProxy())
            {
                var details = proxy.GetCDPUDetails(new RegBREStatus() { RegId = regID.ToInt() });
                if (!ReferenceEquals(details, null))
                {
                    deviceVM.PersonalDetails.CDPUStatus = details.TransactionStatus;
                    deviceVM.PersonalDetails.CDPUDescription = details.TransactionReason;
                    deviceVM.PersonalDetails.IsLockedBy = details.IsLockedBy;
                }
            }
            #endregion

            #region MOC_Status and Liberization status

            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(deviceVM.MSISDN) ? string.Empty : deviceVM.MSISDN, regID.ToInt());
            deviceVM.PersonalDetails.Liberlization_Status = Result[0];
            deviceVM.PersonalDetails.MOCStatus = Result[1];
            deviceVM.PersonalDetails.Justification = !string.IsNullOrEmpty(inputJustification)
                                                            ? inputJustification
                                                            : Result[2].ToString2();//06052015 - Anthony - [Paperless CR] Customer Order Acknowledgement
            //28052015 - Anthony - to retrieve the liberalization based on the customer level - Start
            if (regID.ToInt() == 0)
            {
                var allCustomerDetails = SessionManager.Get("AllServiceDetails") as retrieveAcctListByICResponse;
                if (!ReferenceEquals(allCustomerDetails, null))
                {
                    deviceVM.PersonalDetails.Liberlization_Status = Util.getMarketCodeLiberalisationMOC(allCustomerDetails, "LIBERALISATION");
                }

                deviceVM.PersonalDetails = Util.ConstructWaiverPrice(deviceVM.PersonalDetails, dropObj);
            }
            //28052015 - Anthony - to retrieve the liberalization based on the customer level - End

            Session["RegMobileReg_PersonalDetailsVM"] = deviceVM.PersonalDetails;
            dropObj.personalInformation = deviceVM.PersonalDetails;
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            #endregion

            if (!ReferenceEquals(((List<string>)TempData[SessionKey.errorValidationList.ToString()]), null) && ((List<string>)TempData[SessionKey.errorValidationList.ToString()]).Any())
            {
                string inventoryValidation = "Please re-insert the ";

                if (((List<string>)TempData[SessionKey.errorValidationList.ToString()]).Count() > 1)
                {
                    foreach (var errorMsg in ((List<string>)TempData[SessionKey.errorValidationList.ToString()]))
                    {
                        switch (errorMsg)
                        {
                            case "Sim serial": ModelState.Remove("RegSIMSerial"); break;
                            case "IMEI number": ModelState.Remove("RegIMEINumber"); break;
                        }
                        inventoryValidation += errorMsg + " and ";
                    }
                }
                else
                { // only 1 error
                    inventoryValidation += ((List<string>)TempData[SessionKey.errorValidationList.ToString()]).FirstOrDefault();
                }
                if (inventoryValidation.EndsWith(" and "))
                {
                    inventoryValidation = inventoryValidation.Substring(0, inventoryValidation.Length - 5);
                }
                ModelState.AddModelError(string.Empty, inventoryValidation);
            }
            if (regID.ToInt() > 0)
            {
                deviceVM.PersonalDetails = Util.FormPersonalDetailsandCustomer(deviceVM.PersonalDetails);

                //CR 29JAN2016 - display bill cycle
                if (deviceVM.PersonalDetails.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE).FirstOrDefault() != null)
                {
                    deviceVM.PersonalDetails.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE).FirstOrDefault().ATT_Value;
                }
                else
                {
                    deviceVM.PersonalDetails.BillingCycle = "N/A";
                }
            }
            else
            {
                //CR 29JAN2016 - display bill cycle
                var selectedMsisdn = Session["MSISDN"].ToString2();
                retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                var selectedAccount = AcctListByICResponse.itemList.Where(c => c.ExternalId == selectedMsisdn).FirstOrDefault();
                var billCycle = (selectedAccount != null && selectedAccount.AccountDetails != null ? selectedAccount.AccountDetails.BillCycle : "N/A");
                deviceVM.PersonalDetails.BillingCycle = (!string.IsNullOrWhiteSpace(billCycle) ? billCycle : "N/A");
            }
            if (regID.ToInt() > 0)
                return View(deviceVM);
            else
                return View("MobileRegSummaryNew", deviceVM);
        }

        /// <summary>
        /// Displays the details in print page in sales agent.
        /// </summary>
        /// <param name="id">Registration Id</param>
        /// <returns></returns>
        public ActionResult PrintMobileRF(int id)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var regFormVM = new RegistrationFormVM();
            var modelImageIDs = new List<int>();
            var pbpcIDs = new List<int>();
            var suppLineVASIDs = new List<int>();
            var suppLineForms = new List<SuppLineForm>();
            var regMdlGrpModels = new List<RegMdlGrpModel>();
            string PlanKenanCode = string.Empty;
            using (var proxy = new RegistrationServiceProxy())
            {
                // Registration
                regFormVM.Registration = proxy.RegistrationGet(id);

                // Customer
                regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

				// DF
				regFormVM.DFRegAttribute = proxy.RegAttributesGetByRegID(id);

                // Address
                var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                {
                    Active = true,
                    Address = new Address()
                    {
                        RegID = id
                    }
                }).ToList();
                regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

                // RegMdlGrpModels
                var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                {
                    RegMdlGrpModel = new RegMdlGrpModel()
                    {
                        RegID = id
                    }
                }).ToList();
                if (regMdlGrpModelIDs.Count() > 0)
                {
                    regMdlGrpModels = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
                    modelImageIDs = regMdlGrpModels.Select(a => a.ModelImageID).ToList();
                }
                // RegPgmBdlPkgComponent
                var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                {
                    RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                    {
                        RegID = id,
                    }
                }).ToList();

                var pbpc = new List<RegPgmBdlPkgComp>();
                if (regPgmBdlPkgCompIDs.Count() > 0)
                    pbpc = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                //main line
                if (pbpc.Count() > 0)
                    pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();
                // Reg Supp Line

                var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = id } }).ToList();
                if (suppLineIDs.Count() > 0)
                {
                    var regSuppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    foreach (var regSuppLine in regSuppLines)
                    {
                        suppLineForms.Add(new SuppLineForm()
                        {
                            // supp line pbpc ID
                            SuppLinePBPCID = regSuppLine.PgmBdlPckComponentID,

                            // supp line vas ID
                            SuppLineVASIDs = pbpc.Where(a => a.RegSuppLineID == regSuppLine.ID).Select(a => a.PgmBdlPckComponentID).ToList(),

                            MSISDN = regSuppLine.MSISDN1
                        });
                    }
                }

            }

            using (var proxy = new CatalogServiceProxy())
            {
                // ModelGroupModel
                if (modelImageIDs.Count() > 0)
                {
                    var modelImages = proxy.BrandArticleModelImageGet(modelImageIDs).ToList();
                    var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());

					bool isDeviceFinancingorder = WebHelper.Instance.determineDFOrderbyRegID(id);

					decimal offerPrice = 0;
					if (isDeviceFinancingorder)
					{
						string totalprice = regFormVM.DFRegAttribute.Where(a => a.ATT_Name == RegAttributes.ATTRIB_DF_TOTAL_PRICE && a.SuppLineID == 0).FirstOrDefault().ATT_Value;
						string discountprice = regFormVM.DFRegAttribute.Where(a => a.ATT_Name == RegAttributes.ATTRIB_DF_DISCOUNT && a.SuppLineID == 0).FirstOrDefault().ATT_Value;
						offerPrice = Convert.ToDecimal(totalprice) - Convert.ToDecimal(discountprice);
					}	

                    foreach (var regMdlGrpModel in regMdlGrpModels)
                    {
                        var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
						if (!isDeviceFinancingorder)
						{
							offerPrice = regMdlGrpModel.Price.ToDecimal();
						}

                        regFormVM.DeviceForms.Add(new DeviceForm()
                        {
                            Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
                            Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
							Price = offerPrice,
                            //RST GST
                            TradeUpAmount = regMdlGrpModel.TradeUpAmount,
							DeviceFinancingOrder = isDeviceFinancingorder
                        });
                    }
                }
                var vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
                // BundlePackage, PackageComponents
                if (pbpcIDs.Count() > 0)
                {
                    var pgmBdlPkgComps = MasterDataCache.Instance.FilterComponents(pbpcIDs).Distinct().ToList();
                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var spCode = Properties.Settings.Default.SecondaryPlan;
                    var cpCode = Properties.Settings.Default.ComplimentaryPlan;
                    regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode).ToList();
                    if (regFormVM.BundlePackages != null && regFormVM.BundlePackages.Count <= 0)
                    {
                        regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode && (a.PlanType == "CP" || a.PlanType == "SP")).ToList();
                    }
                    else
                    {
                        regFormVM.PackageComponents = pgmBdlPkgComps.Where((a => a.LinkType.ToLower() == "pc" || a.LinkType.ToLower() == "ec" || a.LinkType.ToLower() == "dc" || a.LinkType.ToLower() == "ma" || a.LinkType.ToLower() == "oc" || a.LinkType.ToLower() == "rc" || a.LinkType.ToLower() == "cr" || a.LinkType.ToLower() == "md" || a.LinkType.ToLower() == "dm" || a.LinkType.ToLower() == "ic")).ToList();
                    }                   

                }

                if (regFormVM.BundlePackages.Count > 0)  
                PlanKenanCode = regFormVM.BundlePackages[0].KenanCode;

                foreach (var suppLineForm in suppLineForms)
                {
                    //supp line package name
                    suppLineForm.SuppLineBdlPkgName = proxy.PackageGet(MasterDataCache.Instance.FilterComponents(new int[] { suppLineForm.SuppLinePBPCID })
                                                                .Select(a => a.ChildID)).SingleOrDefault().Name;
                    //supp line vas names
                    suppLineForm.SuppLineVASNames = proxy.ComponentGet(MasterDataCache.Instance.FilterComponents(suppLineForm.SuppLineVASIDs).Select(a => a.ChildID).ToList()).Select(a => a.Name).ToList();
                }
                regFormVM.SuppLineForms = suppLineForms;

                List<RegSmartComponents> listRegSmartComponents = new List<RegSmartComponents>();

                listRegSmartComponents = WebHelper.Instance.GetSelectedComponents(id.ToString2());
                if (listRegSmartComponents.Count > 0)
                {
                    regFormVM.RegSmartComponents = listRegSmartComponents;
                }

                //Contract Name Dispaly
                if (regFormVM.Registration.RegTypeID == Util.GetIDByCode(RefType.RegType, Settings.Default.RegType_AddContract))
                {
                    List<DAL.Models.Component> componentsList = Util.GetContractsBYRegId(id);
                    if (componentsList != null && componentsList.Count > 0)
                    {
                        Session["SelectedContractName"] = componentsList.Select(vas => vas.Name).ToList();
                        int Duration = 0;
                        foreach (var item in componentsList)
                        {
                            using (var RSproxy = new CatalogServiceProxy())
                            {
                                string strValue = RSproxy.GetContractByCode(componentsList[0].Code);
                                if (Duration < strValue.ToInt())
                                {
                                    Duration = strValue.ToInt();
                                }
                            }

                        }
                        Session["SelectedContractValue"] = Duration;
                    }
                }
            }

            using (var proxy = new RegistrationServiceProxy())
            {
                DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(id);
                Online.Registration.DAL.Models.PrintVersion objPrintVersionDetails = new Online.Registration.DAL.Models.PrintVersion();

                //Condition added by ravi on nov 1 2013 
                if (lnkregdetails != null)
                {
                    
                    objPrintVersionDetails = proxy.GetPrintTsCsInfo(lnkregdetails.PrintVersionNo.ToString2(), 24, regFormVM.Registration.ArticleID == null ? string.Empty : regFormVM.Registration.ArticleID, PlanKenanCode,"");
                    if (objPrintVersionDetails != null)
                    {
                        Session["TsCs"] = objPrintVersionDetails.TsCs;
                    }
                    else
                    {
                        Session["TsCs"] = null;
                    }
                }

            }
            using (var proxyOrg = new OrganizationServiceProxy())
            {
                if (!ReferenceEquals(regFormVM.Registration, null) && !ReferenceEquals(regFormVM.Registration.CenterOrgID, null) && regFormVM.Registration.CenterOrgID != 0)
                {
                    regFormVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { regFormVM.Registration.CenterOrgID }).FirstOrDefault();
                }
            }

            WebHelper.Instance.MapRegAttributesByRegID(regFormVM, !ReferenceEquals(regFormVM.Registration, null) ? regFormVM.Registration.ID : id);

            return View(regFormVM);
        }

        /// <summary>
        /// Displays the details in print page in cashier or store keeper.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult PrintContractDetails(int id)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            return View(WebHelper.Instance.GetContractDetailsViewModel_AddContract(id));
        }

        /// <summary>
        /// Displays this page after service activated in store keeper.
        /// </summary>
        /// <param name="regID">Registration Id</param>
        /// <returns></returns>
        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_DSV,MREG_DSA,MREG_DSK,MREG_DAI,MREG_DIC,MREG_DAC,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegSvcActivated(int regID)
        {
            return View(regID);
        }

        /// <summary>
        /// Displays this page after account is created.
        /// </summary>
        /// <param name="regID">Registration Id</param>
        /// <returns></returns>
        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_DSV,MREG_DSA,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegAccCreated(int regID)
        {
            return View(regID);
        }

        /// <summary>
        /// Display the registration close page.
        /// </summary>
        /// <param name="regID">Registration Id</param>
        /// <returns></returns>
        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_CH,MREG_DSV,MREG_DSA,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegClosed(int regID)
        {
            return View(regID);
        }

        /// <summary>
        /// Registration has been closed incase BRE check failed.
        /// </summary>
        /// <param name="regID">Registration Id</param>
        /// <returns></returns>
        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_SK,MREG_DSV,MREG_DSA,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegBreFail(int regID)
        {
            return View(regID);
        }

        /// <summary>
        /// Display this page after payment recived in cashier.
        /// </summary>
        /// <returns></returns>
        public ActionResult PaymntReceived()
        {
            return View();
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_CH,MREG_DSV,MREG_DSA,MREG_DIC")]
        [DoNoTCache]
        public ActionResult PlanPaymntReceived(int regID)
        {
            return View(regID);
        }

        /// <summary>
        /// Displays the success page after submitting the registration.
        /// </summary>
        /// <param name="regID">Registration Id</param>
        /// <returns></returns>
        [DoNoTCache]
        public ActionResult MobileRegSuccess(int regID)
        {
            //Save the User TransactionLog
            WebHelper.Instance.SaveUserTransactionLogs(regID);
            RegDetailsObjVM regDetailsObj = new RegDetailsObjVM();
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];
            regDetailsObj.RegID = regID;
            using (var regProxy = new RegistrationServiceProxy())
            {
                regDetailsObj.LnkRegDetailsReq = regProxy.GetLnkRegistrationDetails(regID);
                regDetailsObj.reg = regProxy.RegistrationGet(regID);
            }
			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var proxy = new RegistrationServiceProxy())
				{
					regDetailsObj.PegaVM = new PegaRecommendationsVM();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = proxy.GetPegaRecommendationbyMSISDN(regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria);
					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = regDetailsObj.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();
				}
			}

            return View(regDetailsObj);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MobileRegSuccess(FormCollection collection, RegDetailsObjVM regDetailsObj)
        {
            DropFourObj dropObj = new DropFourObj();
            dropObj.personalInformation = new PersonalDetailsVM();
			dropObj.personalInformation.rfDocumentsFile = collection["frontPhoto"].ToString2() == "" ? Session["frontImage"].ToString2() : collection["frontPhoto"].ToString2();
            dropObj.personalInformation.rfDocumentsFileName = collection["frontPhotoFileName"];
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];
            WebHelper.Instance.SaveDocumentToTable(dropObj, regDetailsObj.reg.ID);
            ViewBag.documentUploaded = true;
            using (var regProxy = new RegistrationServiceProxy())
            {
                regDetailsObj.LnkRegDetailsReq = regProxy.GetLnkRegistrationDetails(regDetailsObj.RegID);
                regDetailsObj.reg = regProxy.RegistrationGet(regDetailsObj.RegID);
            }
            using (var proxy = new WebPOSCallBackSvc.WebPOSCallBackServiceClient())
            {
                proxy.sendDocsToIcontract(regDetailsObj.RegID, true);
            }
			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var proxy = new RegistrationServiceProxy())
				{
					regDetailsObj.PegaVM = new PegaRecommendationsVM();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = proxy.GetPegaRecommendationbyMSISDN(regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria);
					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = regDetailsObj.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();
				}
			}

            return View(regDetailsObj);
        }

        /// <summary>
        /// Displays the failure page after submitting the registration.
        /// </summary>
        /// <param name="regID">Registration Id</param>
        /// <returns></returns>
        [DoNoTCache]
        public ActionResult MobileRegFail(int regID)
        {
            return View(regID);
        }

        /// <summary>
        /// Display the cancel page after cancelling the registration.
        /// </summary>
        /// <param name="regID"></param>
        /// <returns></returns>
        [DoNoTCache]
        public ActionResult MobileRegCanceled(int regID)
        {
            return View("MobileRegCanceled", regID);
        }

        [HttpPost]
        public ActionResult Index(string SelectAccountDtls = null)
        {
            Session["SelectAccountDtls"] = null;
            if (SelectAccountDtls != null)
            {
                Session["SelectAccountDtls"] = SelectAccountDtls;
                return RedirectToAction("ContractCheck", new { SelectAccountDtls = SelectAccountDtls });
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ContractCheck(ContractVM contractVM, FormCollection Collection)
        {
            #region Drop 5: BRE Check for non-Drop 4 Flow
            //06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - Start
            var dropObj = new DropFourObj();

            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW && dropObj.mainFlow.RegType == -1)
            {
                dropObj.mainFlow.RegType = MobileRegType.Contract.ToInt();
            }

            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            //06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - End
            #endregion

            List<SubscriberICService.retrievePenalty> penaltybyMSISDN = new List<SubscriberICService.retrievePenalty>();
            if (Session[SessionKey.PPIDInfo.ToString()] != null)
            {
                List<Items> itemList = ((retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()]).itemList.Where(a => a.ExternalId == (contractVM.MSISDN)).ToList();
                if (itemList != null && itemList.Any())
                    contractVM.AccountNumber = itemList[0].AcctIntId;
            }

            if (Collection["TabNumber"].ToInt() == AddContractRegSteps.AccountList.ToInt())
            {
                return RedirectToAction(GetRegActionStep_AddContract(AddContractRegSteps.AccountList.ToInt()));
            }
            else if (Collection["TabNumber"].ToInt() == AddContractRegSteps.DevicePlan.ToInt())
            {
                if (contractVM.ContractType == null)
                {
                    contractVM.ErrMsg = ConfigurationManager.AppSettings["ContractErrMsg"].ToString();
					checkContractGroup(contractVM);
                    return View(contractVM);
                }
                else
                {
                    contractVM.ErrMsg = string.Empty;
                    Session["penaltybyMSISDN"] = null;

                    WebHelper.Instance.ReCheckPenaltyWaiveOffEligibility();
                    if (contractVM.ContractType == "Extend")
                    {
                        //Penalty and Waive offs are calculated in Get method are calculated only by considering the pending contract length
                        // In case of Extend, there will be no penalty and waive off, so reset these values
                        if (Session[SessionKey.ContractDetails.ToString()] != null)
                        {
                            ((List<ContractDetails>)Session[SessionKey.ContractDetails.ToString()]).ForEach(c =>
                            {
                                c.PenaltyWaiveOff = 0; c.Waived = false;
                            });
                        }
                    }

                    if (contractVM != null && !string.IsNullOrEmpty(contractVM.SubAction))
                    {
                        var contractDetails = Session[SessionKey.ContractDetails.ToString()] as List<ContractDetails>;
                        var penaltyAmount = 0M;
                        var penaltyWaiveOffAmount = 0M;

                        if (contractDetails != null && contractDetails.Count > 0 && contractDetails.Where(x => x.Penalty > 0).Any())
                        {
                            WebHelper.Instance.CalculatePenaltyAmount(contractDetails, contractVM.SubAction, ref penaltyAmount, ref penaltyWaiveOffAmount);

                            if (penaltyAmount > 0)
                            {
                                Session["Penalty"] = penaltyAmount;
                                Session["ActualPenalty"] = penaltyAmount;
                                contractVM.Penality = penaltyAmount.ToString("0.00");
                            }

                            if (penaltyWaiveOffAmount > 0)
                                Session[SessionKey.PenaltyWaiveOff.ToString()] = penaltyWaiveOffAmount;
                        }
                    }

					Session["model"] = populateDeviceVM(contractVM);
                    return RedirectToAction(GetRegActionStep_AddContract((int)AddContractRegSteps.DevicePlan));
                }
			}

			#region Extend function, commented
			//For Action Button selection
			/*
			else if (Collection["Extend"] != null)
			{
				contractVM.ErrMsg = string.Empty;
				contractVM.Penality = null;

				if (contractVM.ContractType == "Extend")
				{
					contractVM.ContractType = null;
				}
				else
				{
					contractVM.ContractType = "Extend";
				}
			}
			*/
			#endregion
			
			else if (Collection["TabNumber"].ToInt() == AddContractRegSteps.Accessory.ToInt())
			{
				if (contractVM.ContractType == null)
				{
					contractVM.ErrMsg = ConfigurationManager.AppSettings["ContractErrMsg"].ToString();
					checkContractGroup(contractVM);
					return View(contractVM);
				}
				else
				{
                    if (contractVM != null && !string.IsNullOrEmpty(contractVM.SubAction))
                    {
                        var contractDetails = Session[SessionKey.ContractDetails.ToString()] as List<ContractDetails>;
                        var penaltyAmount = 0M;
                        var penaltyWaiveOffAmount = 0M;

                        if (contractDetails != null && contractDetails.Count > 0 && contractDetails.Where(x => x.Penalty > 0).Any())
                        {
                            WebHelper.Instance.CalculatePenaltyAmount(contractDetails, contractVM.SubAction, ref penaltyAmount, ref penaltyWaiveOffAmount);

                            if (penaltyAmount > 0)
                            {
                                Session["Penalty"] = penaltyAmount;
                                Session["ActualPenalty"] = penaltyAmount;
                            }

                            if (penaltyWaiveOffAmount > 0)
                                Session[SessionKey.PenaltyWaiveOff.ToString()] = penaltyWaiveOffAmount;
                        }
                    } 
                    
                    contractVM.ErrMsg = string.Empty;
					Session["model"] = populateDeviceVM(contractVM);
                    Session[SessionKey.Selected_Accessory.ToString()] = null;
                    retrieveAcctListByICResponse allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
                    var checkCustEligibilityAF = WebHelper.Instance.checkCustEligibilityAF(allCustomerDetails, contractVM.MSISDN);
					return RedirectToAction(GetRegActionStep_AddContract((int)AddContractRegSteps.Accessory));
				}
			}
			else if (Collection["FormAction"].ToString2() == "Terminate")
			{
				if (contractVM.ContractType == "Terminate")
				{
					contractVM.Penality = null;
					contractVM.ContractType = null;
					//contractVM.ParentAction = null;
				}
                //else
                //{
					contractVM.ContractType = "Terminate";
                //}
			}

			if (!string.IsNullOrEmpty(contractVM.ContractType))
				populateContractExtendedData(contractVM);

            Session["ContractType"] = contractVM.ContractType;//08042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
			checkContractGroup(contractVM);
            return View(contractVM);
        }

		private DeviceVM populateDeviceVM(ContractVM contractVM)
		{
			DeviceVM deviceVM = new DeviceVM();
			deviceVM.AccountType = contractVM.AccountType;
			deviceVM.AccountNumber = contractVM.AccountNumber;
			deviceVM.ContractType = !string.IsNullOrEmpty(contractVM.ContractType) ? contractVM.ContractType : "Terminate";
			deviceVM.MSISDN = contractVM.MSISDN;
			deviceVM.ContractRemainingMonths = contractVM.ContractRemainingMonths;
			deviceVM.SubscriberNo = contractVM.SubscriberNo;
			deviceVM.SubscriberNoResets = contractVM.SubscriberNoResets;
			deviceVM.AccountNumber = contractVM.AccountNumber;
			deviceVM.KenanAccountNo = contractVM.KenanAccountNo;
			deviceVM.Penality = contractVM.Penality;
			deviceVM.SimType = contractVM.SimType;
			deviceVM.subAction = contractVM.SubAction;
			if (deviceVM.subAction == Constants.TERMINATEASSIGN_ACC_ONLY)
			{
				deviceVM.PackageDesc = "";
				deviceVM.PackageId = "";
				deviceVM.VasComponents = "";
				deviceVM.Package = "";
				deviceVM.VasNames = "";
			}
			else
			{
				deviceVM.PackageDesc = contractVM.PackageDesc;
				deviceVM.PackageId = contractVM.PackageId;
				deviceVM.VasComponents = contractVM.VasComponents;
				deviceVM.Package = contractVM.Package;
				deviceVM.VasNames = contractVM.VasNames;
			}
			
			
			return deviceVM;
		}

		private ContractVM populateContractExtendedData (ContractVM contractVM){
			var penaltybyMSISDN = (List<SubscriberICService.retrievePenalty>)Session["penaltybyMSISDN"];
			contractVM.penaltyType = penaltybyMSISDN.FirstOrDefault() != null ? penaltybyMSISDN.Where(x => x.penaltyType == "DF").Any() ? "DF" : penaltybyMSISDN.FirstOrDefault().penaltyType : string.Empty;
            var contracts = contractVM.ContractDetailsList;
            decimal penalty = 0;
            decimal penaltyWaiveOff = 0;
            if (penaltybyMSISDN != null)
            {
                foreach (var item in penaltybyMSISDN)
                {
                    contractVM.PenaltyAdminFee = item.adminFee;
                    contractVM.PenaltyRRP = item.rrp;
                    contractVM.PenaltyPhoneModel = item.phoneModel;
                    contractVM.Upfrontpayment = item.UpfrontPayment;
                    contractVM.PenaltyDeviceModel = item.DeviceModel;
                    contractVM.PenaltyDeviceIMEI = item.DeviceIMEI;
                    contractVM.PenaltyUpgradeFee = item.UpgradeFee;
                    penalty += item.penalty.ToDecimal();
                    penaltyWaiveOff += contracts.FirstOrDefault(c => c.ComponentDesc.Equals(item.ContractName)).PenaltyWaiveOff;

                }
                contractVM.Penality = penalty.ToString();
                Session[SessionKey.PenaltyWaiveOff.ToString()] = penaltyWaiveOff;
            }
            else
            {
                contractVM.Penality = "0";
                Session[SessionKey.PenaltyWaiveOff.ToString()] = "0";
            }

			return contractVM;
		}

        [HttpPost]
        public ActionResult SelectDevice(FormCollection collection, DeviceVM deviceVM)
        {
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            Session[SessionKey.Selected_Accessory.ToString()] = null;
            var MSISDN = Session[SessionKey.ExternalID.ToString()].ToString2();
            retrieveAcctListByICResponse allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
            var checkCustEligibilityAF = WebHelper.Instance.checkCustEligibilityAF(allCustomerDetails, MSISDN);
            
            #region back button
            if (collection["submitType"].ToInt() == AddContractRegSteps.ContractCheck.ToInt())
            {
                if (Session["model"] != null)
                    deviceVM = (DeviceVM)Session["model"];

                string SelectAccountDtls = Session["SelectAccountDtls"].ToString2();
                return RedirectToAction("ContractCheck", new { SelectAccountDtls = SelectAccountDtls });
            }
            #endregion
            
            #region filter
            if (collection["submitType"].ToString2() == "filter")
            {
                List<Online.Registration.Web.CatalogSvc.GetDeviceImageDetailsResp> deviceDetails = new List<GetDeviceImageDetailsResp>();
                int brandFilter = collection["brandFilter"].ToInt();
                string modelFilter = collection["modelFilter"].ToString2();
                int capacityFilter = collection["capacityFilter"].ToInt();
                string priceFilter = collection["priceFilter"].ToString2();
                int deviceType = collection["deviceTypeSelected"].ToInt();
                // stock filter
                bool availableChecked = collection["availableChecked"].ToBool();
                bool limitedStockChecked = collection["limitedStockChecked"].ToBool();
                bool sellingFastChecked = collection["sellingFastChecked"].ToBool();
                bool outOfStockChecked = collection["outOfStockChecked"].ToBool();

                //19052015 - Add brand, model, and capacity selected to the VM
                List<SelectListItem> listOfAvailableBrand = Util.GetList(RefType.Brand, defaultValue: string.Empty, defaultText: "All Brand");
                deviceVM.BrandSelected = listOfAvailableBrand.Where(x => x.Value == brandFilter.ToString()).Select(y => y.Text).FirstOrDefault();
                List<SelectListItem> listOfModel = Util.GetList(RefType.Model, defaultValue: string.Empty, defaultText: "All Model");
                deviceVM.ModelSelected = listOfModel.Where(x => x.Value == modelFilter).FirstOrDefault().Text;
                deviceVM.ModelSelected = System.Text.RegularExpressions.Regex.Replace(deviceVM.ModelSelected, @"\S*GB", string.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                List<SelectListItem> listOfCapacity = Util.GetList(RefType.Capacity, defaultValue: string.Empty, defaultText: "All");
                deviceVM.CapacitySelected = listOfCapacity.Where(x => x.Value == capacityFilter.ToString()).Select(y => y.Text).FirstOrDefault();

                deviceVM.PriceRangeValue = priceFilter;
                deviceVM.inputBrandSelected = brandFilter;
                deviceVM.inputCapacitySelected = capacityFilter;
                deviceVM.inputModelSelected = modelFilter;
                deviceVM.inputDeviceTypeSelected = deviceType;

                int priceFrom = 0;
                int priceTo = 0;
                if (priceFilter.Contains("#"))
                {
                    string[] priceArray = priceFilter.Trim().Split('#');
                    priceFrom = int.Parse(priceArray[0]);
                    priceTo = int.Parse(priceArray[1]);

                    string firstword = string.Empty; string secondWord = string.Empty;
                    firstword = priceArray[0] == "0" ? "0" : "RM " + priceArray[0];
                    secondWord = priceArray[1] == "0" ? "0" : "RM " + priceArray[1];

                    deviceVM.PriceRangeSelected = string.Format("{0} - {1}", firstword, secondWord);
                }

                //devicePlanVM.Phone = WebHelper.Instance.GetFilteredDevice(this.GetType(), out bExceptionRaised, brandFilter, modelFilter, capacityFilter, deviceType, priceFrom, priceTo, availableChecked, limitedStockChecked, sellingFastChecked, outOfStockChecked, 0);
                deviceVM.Phone = GetAvailableModelImage(deviceVM, priceFrom, priceTo, availableChecked, limitedStockChecked, sellingFastChecked, outOfStockChecked);

                List<PackageVM> packageListFilterDevice = new List<PackageVM>();

                List<String> LabelReplace = new List<String>();
                ViewBag.GSTMark = Settings.Default.GSTMark;
                LabelReplace.Add("GSTMark");
                ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);

                Session["DeviceLowestContractPrice"] = WebHelper.Instance.GetLowestHighestPlanAndPrice(deviceVM.Phone.ModelImages, useArticleID: false);
                return View(deviceVM);
            }
            #endregion
            
            #region submit
            if (collection["submitType"].ToInt() == AddContractRegSteps.VAS.ToInt())
            {
                Session["ArticleId"] = collection["selectedArticleID"].ToString().Trim();
                Session["RegMobileReg_OfferDevicePrice"] = collection["deviceOfferPrice"].ToDecimal();
                Session["RegMobileReg_RRPDevicePrice"] = collection["deviceRRP"].ToDecimal();
                Session["selectedContractLength"] = collection["selectedContractLength"].ToString2();
                deviceVM.isSimRequired = collection["isSimRequired"].ToBool();
                deviceVM.SimSize = collection["SimSize"].ToString2();

                dropObj.mainFlow.isSimRequired = deviceVM.isSimRequired;
                dropObj.mainFlow.simSize = deviceVM.isSimRequired ? deviceVM.SimSize : "";

                var allArticles = MasterDataCache.Instance.Article;
                //deviceVM.ModelId = deviceVM.Phone.SelectedModelImageID;
                deviceVM.Phone.DeviceArticleId = collection["selectedArticleID"].ToString().Trim();
                deviceVM.Phone.ColourID = allArticles.Where(x => x.ArticleID.Equals(deviceVM.Phone.DeviceArticleId)).SingleOrDefault().ColourID;
                deviceVM.Phone.ModelID = allArticles.Where(x => x.ArticleID.Equals(deviceVM.Phone.DeviceArticleId)).SingleOrDefault().ModelID;
                deviceVM.Phone.SelectedModelImageID = allArticles.Where(x => x.ArticleID.Equals(deviceVM.Phone.DeviceArticleId)).SingleOrDefault().ID.ToString2();
                deviceVM.ModelId = deviceVM.Phone.SelectedModelImageID;
                Session["RegMobileReg_SelectedModelImageID"] = deviceVM.Phone.SelectedModelImageID;
                //Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = deviceVM.OrderSummary = ConstructOrderSummary(deviceVM.PackageId, deviceVM.ModelId, deviceVM.VasComponents, deviceVM.VasNames, null, null);
                //Session["model"] = deviceVM;
                //dropObj.mainFlow.PkgPgmBdlPkgCompID = dropObj.mainFlow.orderSummary.SelectedPgmBdlPkgCompID;
                
                var selectedContractLength = Session["selectedContractLength"].ToString2();
                string planID = deviceVM.Package;
                string articleid = Session[SessionKey.ArticleId.ToString()].ToString2();
                string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString2() : string.Empty;
                string contractRemainingMonths = deviceVM.ContractRemainingMonths.ToString2();
                string contractType = deviceVM.ContractType.ToString2();

                var Lstvam = GetAvailablePackageComponents(deviceVM.PackageId.ToInt(), deviceVM.ContractType, deviceVM.ModelId, isMandatory: false);
                Lstvam.ContractOffers = WebHelper.Instance.GetContractOffersCRP(Lstvam, articleid, planID, MOCStatus, contractRemainingMonths, contractType);

                foreach (var contract in Lstvam.ContractOffers)
                {
                    if (contract.OfferName.Contains(selectedContractLength))
                    {
                        var PBPCPlan = 0;

                        try
                        {
                            PBPCPlan = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
                            {
                                PgmBdlPckComponent = new PgmBdlPckComponent()
                                {
                                    KenanCode = planID,
                                    LinkType = Properties.Settings.Default.Bundle_Package
                                },
                                Active = true
                            }).FirstOrDefault().ChildID;
                        }
                        catch (Exception e)
                        {
                            PBPCPlan = 0;
                        }

                        if (PBPCPlan != 0)
                        {
                            var findCriteria = new PgmBdlPckComponentFind()
                            {
                                PgmBdlPckComponent = new PgmBdlPckComponent()
                                {
                                    ParentID = PBPCPlan,
                                    LinkType = Properties.Settings.Default.Package_Component
                                },
                                Active = true
                            };

                            if (!string.IsNullOrEmpty(contract.KenanComponent1Id) && !string.IsNullOrEmpty(selectedContractLength))
                            {
                                var ContractID = MasterDataCache.Instance.FilterComponents(findCriteria).Where(x => x.KenanCode.Equals(contract.KenanComponent1Id) && x.Value == selectedContractLength.ToInt()).FirstOrDefault().ID;
                                Session["RegMobileReg_ContractID"] = ContractID;
                                Session["selectedKenanComponent1Id"] = contract.KenanComponent1Id; 
                                Session["OfferID"] = contract.Id;
                                Session["selectedOfferID"] = contract.Id;
                                Session["RegMobileReg_OfferDevicePrice"] = contract.Price;
                                dropObj.mainFlow.UOMCode = contract.UOMCode;
                            }
                        }

                        //Session["selectedKenanComponent1Id"] = contract.KenanComponent1Id;
                        //Session["selectedOfferID"] = contract.Id;
                        //Session["RegMobileReg_ContractID"] = contract.Id;
                        //Session["OfferID"] = contract.Id;//14012015 - Anthony - GST Trade-Up
                        Session[SessionKey.TradeUpAmount.ToString()] = null;//14012015 - Anthony - GST Trade-Up
                        break;
                    }
                }

                DropFourHelpers.UpdatePhone(dropObj, deviceVM.Phone);
				DropFourHelpers.UpdatePackage(dropObj, new PackageVM());
                Session[SessionKey.DropFourObj.ToString()] = dropObj;
                deviceVM.OrderSummary = WebHelper.Instance.ConstructOrderSummary(string.Empty, this.GetType().Name, false);
                deviceVM.OrderSummary.ModelID = deviceVM.Phone.ModelID;
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = deviceVM.OrderSummary;
				DropFourHelpers.UpdateOrderSummaryOnly(dropObj);

                Session["model"] = deviceVM;
            }

			return RedirectToAction(GetRegActionStep_AddContract(AddContractRegSteps.Accessory.ToInt()));
			

            #endregion submit
        }

        [HttpPost]
        public ActionResult SelectVAS(DeviceVM deviceVM, FormCollection objfrm)
        {
            
            var tabNumber = objfrm["submitType"].ToInt();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            //dropObj.SetAdditionalContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            
            var deviceVMFromSession = new DeviceVM();
            deviceVMFromSession = (DeviceVM)Session["model"];
            deviceVM.Package = deviceVMFromSession.Package;
            deviceVM.ContractRemainingMonths = deviceVMFromSession.ContractRemainingMonths;
            deviceVM.ContractType = deviceVMFromSession.ContractType;
            deviceVM.PackageId = deviceVMFromSession.PackageId;
            deviceVM.ModelId = deviceVMFromSession.ModelId;
            deviceVM.SimType = deviceVMFromSession.SimType;
            deviceVM.Penality = deviceVMFromSession.Penality;
            deviceVM.isSimRequired = deviceVMFromSession.isSimRequired;
            deviceVM.SimSize = deviceVMFromSession.SimSize;
			deviceVM.subAction = deviceVMFromSession.subAction;
            deviceVM.Components.SubscribedComponentIds = deviceVMFromSession.Components.SubscribedComponentIds;

            if (objfrm["hdnback"].ToString2() == "true")
            {
                Session["model"] = deviceVM;
                return RedirectToAction(GetRegActionStep_AddContract(tabNumber));
            }

            Session["AddedVases"] = objfrm["AddedVases"].ToString2();
            Session["RemovedVases"] = objfrm["RemovedVases"].ToString2();
            Session["RemovedVasNames"] = objfrm["RemovedVasNames"].ToString2();
            Session["RemovedVasPrices"] = objfrm["RemovedVasPrices"].ToString2();
            Session["AddedVasNames"] = objfrm["AddedVasNames"].ToString2();
            Session["AddedVasPrices"] = objfrm["AddedVasPrices"].ToString2();
            Session["ARVasGroupIds"] = objfrm["GroupIds"].ToString2();

			var selectedKenanCodes = objfrm["AddedVasesKenanCodes"].ToString2();

            if (objfrm["df_showOffer"].ToString2() == "show")
            {
                Session["df_showOffer"] = objfrm["df_showOffer"].ToString2();
                Session["DF_Eligibility"] = true;
                Session["mdp_decision"] = objfrm["mdp_decision"].ToString2();
                Session["model"] = deviceVM;
                return RedirectToAction("SelectVAS");
            }

            Session["mdp_decision"] = null;
            
            Session["RegMobileReg_ContractID"] = deviceVM.VAS.SelectedContractID;
            Session["contractName"] = deviceVM.VAS.SelectedContractName;
			string contractSelectedKenanCode = objfrm["hdnContractkenanVASID"].ToString2();
            dropObj.mainFlow.OfferName = objfrm["OfferName"].ToString2();
            #region fxDependency Check
            using (var proxy = new CatalogServiceProxy())
            {
                List<int> lstContracts = WebHelper.Instance.CommaSplitToList(Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2());
                Session[SessionKey.RegMobileReg_DataplanID.ToString()] = proxy.GettllnkContractDataPlan(lstContracts);
            }
            List<FxDependChkComps> Comps = new List<FxDependChkComps>();
            using (var Proxy = new RegistrationServiceProxy())
            {
                string strCompIds = string.Empty;
                if (deviceVM.VasComponents.ToString2().Length > 0)
                {
                    strCompIds = deviceVM.VasComponents.Replace("[object XMLDocument]", string.Empty);
                }
                if (objfrm["AddedVases"].ToString2().Length > 0)
                    strCompIds = strCompIds + "," + objfrm["AddedVases"].ToString().Replace("[object XMLDocument]", string.Empty);
                if (Session["MandatoryVasIds"].ToString2().Length > 0)
                {
                    strCompIds = strCompIds + "," + Session["MandatoryVasIds"].ToString().Replace("[object XMLDocument]", string.Empty);
                }
                strCompIds += "," + Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
                if (deviceVM.VAS.SelectedContractID.ToString2().Length > 0)
                    strCompIds += "," + deviceVM.VAS.SelectedContractID.Replace("[object XMLDocument]", string.Empty);
                //added bite components 
                if (!ReferenceEquals(Session[SessionKey.RegMobileReg_DataplanID.ToString()], null))
                {
                    int[] arrDpIds = (int[])Session[SessionKey.RegMobileReg_DataplanID.ToString()];
                    foreach (int id in arrDpIds)
                    {
                        strCompIds += "," + id.ToString2();
                    }
                    List<int> selcompIds = new List<int>();
                    foreach (var item in strCompIds.Split(','))
                    {
                        if (!selcompIds.Contains(item.ToInt()))
                            selcompIds.Add(item.ToInt());
                    }
                    if (selcompIds.Count() > 0)
                    {
                        strCompIds = string.Empty;
                        strCompIds = string.Join(",", selcompIds);
                    }
                }
                Comps = Proxy.GetAllComponentsbyPlanId(Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(), strCompIds, "BP");

				string dummyString = "";
				if (dropObj.mainFlow.isAccFinancingOrder)
					WebHelper.Instance.AutoAddAccessoryFinancingComponents(ref Comps, ref strCompIds, ref dummyString);

            }
            List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();

            dropObj.mainFlow.isDeviceFinancingOrder = WebHelper.Instance.determineDeviceFinancingOrder(Comps);
			
			List<string> existingKenanCodeList = new List<string>();
			foreach (var comp in Comps)
            {
				if (comp.ComponentKenanCode != "46043")
				{
					Online.Registration.Web.SubscriberICService.componentList component = new componentList();
					component.componentIdField = comp.ComponentKenanCode;
					component.packageIdField = comp.KenanCode;
					componentList.Add(component);
					if (comp.DependentKenanCode != -1)
					{
						component = new componentList();
						component.componentIdField = comp.DependentKenanCode.ToString();
						component.packageIdField = comp.KenanCode;
						componentList.Add(component);
					}
					existingKenanCodeList.Add(comp.ComponentKenanCode);
				}
            }
            Session["selectedValsSession"] = existingKenanCodeList;

			string pbpcID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();

            if (componentList.Count > 0)
            {
				DeviceVM vas = (DeviceVM)Session["model"];

                vas.VasComponents = deviceVM.VasComponents;

				List<string> removedVases = WebHelper.Instance.convertPBPCIDtoKenanCode(objfrm["RemovedVases"].ToString2());
				List<string> addedVasess = WebHelper.Instance.convertPBPCIDtoKenanCode(objfrm["AddedVases"].ToString2());
				if (sharingQuotaFeature)
				{
					//existingKenanCodeList
					vas.Components.ShareSuppErrorMessage = WebHelper.Instance.MutualDependencyheckForARS(removedVases, addedVasess, existingKenanCodeList);

					if (!string.IsNullOrEmpty(vas.Components.ShareSuppErrorMessage))
					{
						return View(vas);
					}
				}

				// Drop 5
				retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
				
				typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, pbpcID: pbpcID);

                typeChkFxVasDependencyResponse response = _retrieveServiceInfoProxy.ChkFxVasDependency(request);
                ViewBag.GSTMark = Settings.Default.GSTMark;
                List<String> LabelReplace = new List<String>();
                LabelReplace.Add("GSTMark");
                ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);

                if (response.mxsRuleMessagesField != null && response.mxsRuleMessagesField.Count > 0)
                {
                    foreach (string errmsg in response.mxsRuleMessagesField)
                    {
                        ModelState.AddModelError(string.Empty, errmsg);
                    }
                    return View(vas);
                }
                else if (response.msgCodeField == "3")
                {
                    ModelState.AddModelError(string.Empty, ConfigurationManager.AppSettings["FXDependencyDownMessage1"].ToString());   // Need to change msg
                    return View(vas);
                }
                else if (response.msgCodeField != "0")
                {
                    ModelState.AddModelError(string.Empty, ConfigurationManager.AppSettings["FXDependencyDownMessage"].ToString());   // Need to change msg
                    return View(vas);
                }
				string errorDependent = WebHelper.Instance.checkVASDependentDF_AddContract(removedVases, addedVasess, vas.Components.SubscribedComponentIds, contractSelectedKenanCode);
				if (!string.IsNullOrWhiteSpace(errorDependent))
				{
					ModelState.AddModelError("", errorDependent);
					return View(vas);
				}

                // iphone 6 and iphone 6 plus data booster - Start
                if (null != Session["ArticleId"])
                {
                    string ArticleID = Session[SessionKey.ArticleId.ToString()].ToString();
                    List<int> selectedComponent = new List<int>();

                    foreach (var component in deviceVM.VasComponents.ToString2().Split(','))
                    {
                        if (!selectedComponent.Contains(component.ToInt()) && (component != null || component.ToInt() == 0))
                            selectedComponent.Add(component.ToInt());
                    }

                    string addedVases = Session["AddedVases"].ToString();
                    foreach (var component in addedVases.Split(','))
                    {
                        if (!selectedComponent.Contains(component.ToInt()) && (component != null || component.ToInt() == 0))
                            selectedComponent.Add(component.ToInt());
                    }

                    if (null != selectedComponent)
                    {
                        int consist = Util.checkDataBooster(selectedComponent, deviceVM.Package, ArticleID);




                        if (consist == 3)
                        {
                            ModelState.AddModelError(string.Empty, ConfigurationManager.AppSettings["maxisDataBoosterError"].ToString());
                            return View(vas);
                        }

                    }
                }
                // iphone 6 and iphone 6 plus data booster - End


                if (Session["_componentViewModel"] != null)
                {
                    ComponentViewModel sessionVal = (ComponentViewModel)Session["_componentViewModel"];



                    NewComponents objCurAddedComps;
                    int loopCnt = 0;
					foreach (string addedVas in selectedKenanCodes.ToString2().Split(','))
                    {
                        if (string.IsNullOrEmpty(addedVas) == false)
                        {
                            var curAdddedVasId = componentList.Where(compId => compId.componentIdField == addedVas).ToList().FirstOrDefault();
                            if (curAdddedVasId != null)
                            {
                                string curVasName = objfrm["AddedVasNames"].ToString2().Split(',')[loopCnt];
                                string curPrice = objfrm["AddedVasPrices"].ToString2().Split(',')[loopCnt];
                                objCurAddedComps = new NewComponents();
                                //objCurAddedComps.NewComponentId = curAdddedVasId.componentIdField;
                                objCurAddedComps.NewComponentKenanCode = curAdddedVasId.componentIdField;
                                if (curVasName.ToString2().Length > 0 && curPrice.ToString2().Length > 0)
                                {
                                    objCurAddedComps.NewComponentName = curVasName + " - RM" + curPrice;
                                }
                                objCurAddedComps.NewPackageKenanCode = curAdddedVasId.packageIdField;
                                objCurAddedComps.IsChecked = true;
                                sessionVal.NewComponentsList.Add(objCurAddedComps);
                                loopCnt++;
                            }

                        }
                    }

                    Session["_componentViewModel"] = sessionVal;
                }

                #region validate device RRP and MDP component
                var deviceModelId = dropObj.mainFlow.phone.ModelID.ToString2();
                int mdpID = objfrm["selectedMDP"].ToInt();

                if (mdpID != 0)
                {
                    string MDPresult = ValidateRRPandMDP(deviceModelId, mdpID);

                    if (MDPresult == "0")
                    {
                        ModelState.AddModelError(string.Empty, "Please select appropriate MDP component based on device RRP");
                        return View(vas);
                    }
                }
                #endregion

            }

            #endregion

            //RST GST - this is server validation for tradeUp LOV
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
            var tradeUpList = new TradeUpComponent();

            if (!ReferenceEquals(Session["VAS"], null))
            {
                Lstvam2 = (ValueAddedServicesVM)Session["VAS"];
                if (!string.IsNullOrEmpty(deviceVM.VAS.hdnTradeUpAmount))
                {
                    if (deviceVM.VAS.hdnTradeUpAmount.Contains("btnTradeUp"))
                    {
                        var offerIDSelected = deviceVM.VAS.hdnTradeUpAmount.Split('_')[1].ToInt();
                        var tradeUpAmountSelected = deviceVM.VAS.hdnTradeUpAmount.Split('-')[1].ToString2();
                        Session[SessionKey.TradeUpAmount.ToString()] = deviceVM.VAS.hdnTradeUpAmount.Split('_')[2].ToInt();

                        tradeUpList.OfferId = offerIDSelected;
                        tradeUpList.Name = "RM " + tradeUpAmountSelected.ToInt();
                        tradeUpList.Price = tradeUpAmountSelected.ToInt() * -1;

                        foreach (var cOffers in Lstvam2.ContractOffers)
                        {
                            if (offerIDSelected == cOffers.Id && !cOffers.TradeUpAmount.Contains(tradeUpAmountSelected))
                            {
                                Session["TradeUpAmount"] = 0;
                            }
                        }
                    }
                    else
                    {
                        Session[SessionKey.TradeUpAmount.ToString()] = deviceVM.VAS.hdnTradeUpAmount;
                    }
                }
            }
            //End RST GST

            deviceVM.OfferId = deviceVM.VAS.SelectedContractID;
            string vasComponents = deviceVM.VasComponents;
            var vasComponentList = new List<string>();

            if (!string.IsNullOrEmpty(vasComponents))
            {
                vasComponentList = WebHelper.Instance.CommaSplitToListString(vasComponents);

                if (vasComponentList.Contains(deviceVM.VAS.SelectedContractID) == false)
                {
                    if (vasComponents.EndsWith(","))
                    {
                        vasComponents = vasComponents + deviceVM.VAS.SelectedContractID + ",";
                    }
                    else
                    {
                        vasComponents = vasComponents + "," + deviceVM.VAS.SelectedContractID;
                    }
                }
            }
            else
            {
                vasComponents = vasComponents + deviceVM.VAS.SelectedContractID + ",";
            }

            //FOR ADD/REmove vas integration
            var currentAddedVases = objfrm["AddedVases"].ToString2().Replace("[object XMLDocument]", string.Empty);
            var addedVasesList = new List<string>();

            if(!string.IsNullOrEmpty(currentAddedVases))
            {
                addedVasesList = WebHelper.Instance.CommaSplitToListString(currentAddedVases);

                foreach (var addedVas in addedVasesList)
                {
                    if (vasComponentList.Contains(addedVas) == false)
                    {
                        if (vasComponents.EndsWith(","))
                        {
                            vasComponents = vasComponents + addedVas + ",";
                        }
                        else
                        {
                            vasComponents = vasComponents + "," + addedVas;
                        }
                    }
                }

                if (currentAddedVases.EndsWith(","))
                {
                    vasComponents = vasComponents + ",";
                }
            }

            //vasComponents = vasComponents + objfrm["AddedVases"].ToString2().Replace("[object XMLDocument]", string.Empty);

            if (Session["_componentViewModel"] != null)
            {
                ComponentViewModel sessionVal = (ComponentViewModel)Session["_componentViewModel"];
                foreach (var c in sessionVal.NewComponentsList)
                {
                    if (c.IsChecked)
                    {

                        if (c.NewComponentId.ToString2().Length > 0 && objfrm["RemovedVases"].ToString2().Contains(c.NewComponentId) == true)
                        {
                            c.IsChecked = false;
                        }

						if (!dropObj.mainFlow.isDeviceFinancingOrder)
						{
							var _MOCWaiverEligibleList = MasterDataCache.Instance.GetAllLovList.Where(x => x.Type == "DF_CHARGE").Select(x => x.Value).ToList();

							if (_MOCWaiverEligibleList.Contains(c.NewComponentKenanCode) && c.IsChecked)
								c.IsChecked = false;
						}


                    }
                }
                Session["_componentViewModel"] = sessionVal;
            }

			

			if (dropObj.mainFlow.isDeviceFinancingOrder)
			{
				DeviceVM deviceVMSession = (DeviceVM)Session["model"];
				string ArticleID = deviceVM.Phone != null ? deviceVM.Phone.DeviceArticleId : "";
				ArticleID = !string.IsNullOrEmpty(ArticleID) ? ArticleID : deviceVMSession.Phone.DeviceArticleId;
				var addedKenanCodes = selectedKenanCodes.ToString2().Split(',').ToList();
				vasComponents = WebHelper.Instance.autoAddWaiverComponent(vasComponents, ArticleID, pbpcID.ToInt(), "DF");
				
				var addedKenanCodeWaiver = Session["addedKenanCodeDF"].ToString2();
				if (!string.IsNullOrEmpty(addedKenanCodeWaiver))
				{
					selectedKenanCodes = selectedKenanCodes.EndsWith(",") ? selectedKenanCodes + addedKenanCodeWaiver : selectedKenanCodes + "," + addedKenanCodeWaiver;
				}
				
				// dropObj.DFcustomerEligibility <<< customer eligibility
				dropObj.mainFlow.isDeviceFinancingExceptionalOrder = !dropObj.DFcustomerEligibility ? true : false;

                //add monthly fee component for DF
                var vasIDList = new List<int>();

                if (deviceVM.VAS.SelectedContractID.Contains(','))
                {
                    try
                    {
                        vasIDList.AddRange(deviceVM.VAS.SelectedContractID.Split(',').Select(int.Parse).ToList());
                    }
                    catch (Exception ex)
                    {
                        vasIDList = new List<int>();
                    }
                }
                else
                {
                    vasIDList.Add(deviceVM.VAS.SelectedContractID.ToInt());
                }

                if (vasIDList.Count > 0)
                {
                    var dependentComponentIDs = WebHelper.Instance.GetDepenedencyComponents(vasIDList);

                    if (dependentComponentIDs != null && dependentComponentIDs.Count > 0)
                    {
                        var dependentCompString = string.Join(",", dependentComponentIDs);

                        if (vasComponents.Substring(vasComponents.Length - 1) != ",")
                        {
                            vasComponents += "," + dependentCompString + ",";
                        }
                        else
                        {
                            vasComponents += dependentCompString + ",";
                        }

                        Session[SessionKey.RegMobileReg_VasIDs.ToString()] = vasComponents;
                    }
                }
			}
			else
			{
				vasComponents = WebHelper.Instance.ReconstructVasComponents(vasComponents);
				
			}

			if (dropObj.mainFlow.isAccFinancingOrder)
				vasComponents = autoAddAccFinancingPackageComponents(vasComponents);

			deviceVM.VasComponents = vasComponents;
            List<int> planIds = new List<int>();
            planIds.Add(Convert.ToInt32(deviceVM.PackageId));

            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = deviceVM.VAS.price;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = deviceVM.VAS.price;
            deviceVM.DevicePrice = deviceVM.VAS.price;
            deviceVM.UOMCode = deviceVM.VAS.uomCode;

            Session[SessionKey.OfferId.ToString()] = deviceVM.VAS.OfferId.ToString();

            PgmBdlPckComponent package = new PgmBdlPckComponent();

            using (var proxy = new CatalogServiceProxy())
            {
                package = MasterDataCache.Instance.FilterComponents(planIds).FirstOrDefault();
                var respOffers = proxy.GETMOCOfferDetails(deviceVM.VAS.OfferId);
                MOCOfferDetails mocOffer = new MOCOfferDetails();
                if (respOffers != null)
                {
                    mocOffer.DiscountAmount = respOffers.DiscountAmount;
                    mocOffer.ID = respOffers.ID;
                    mocOffer.Name = respOffers.Name;
                    mocOffer.OfferDescription = respOffers.OfferDescription;
                    Session[SessionKey.Discount.ToString()] = mocOffer.DiscountAmount;
                }
            }

            using (var proxy = new CatalogServiceProxy())
            {
                List<int> lstContracts = WebHelper.Instance.CommaSplitToList(deviceVM.VAS.SelectedContractID);
                int[] contracts = proxy.GettllnkContractDataPlan(lstContracts);
                deviceVM.DataPlanId = string.Join(",", contracts);
            }

            // DF
            //if (dropObj.mainFlow.phone != null)
            //{
            //    string df_articleID = dropObj.mainFlow.phone.DeviceArticleId;
            //    int df_pbpcID = dropObj.mainFlow.PkgPgmBdlPkgCompID;
            //    deviceVM.VAS = WebHelper.Instance.autoAddWaiverComponent(deviceVM.VAS, df_articleID, df_pbpcID, "DF");
            //}


			

            #region Drop 5: BRE Check for non-Drop 4 Flow
            //06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - Start
            Session["RegMobileReg_UOMCode"] = deviceVM.VAS.uomCode;
            DropFourHelpers.UpdateVAS(dropObj, deviceVM.VAS);

			Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = deviceVM.OrderSummary = ConstructOrderSummary(deviceVM.PackageId, deviceVM.ModelId, deviceVM.VasComponents, deviceVM.VasNames, null, null);
			Session["model"] = deviceVM;

            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW)
            {
                if (dropObj.mainFlow != null && dropObj.mainFlow.orderSummary != null)
                {
                    if (dropObj.mainFlow.orderSummary.unwaivedAdvancePaymentDeposit != null)
                    {
                        dropObj.mainFlow.orderSummary.unwaivedAdvancePaymentDeposit.contractID = deviceVM.VAS.SelectedContractID.ToInt();//10072015 - Anthony - to cater the missing contractID in unwaivedAdvancePaymentDeposit during ConstructOrderSummary
                    }
                }
            }
			DropFourHelpers.UpdateDeviceVM(dropObj, deviceVM);
			dropObj.mainFlow.vas.SelectedKenanCodes = selectedKenanCodes;

            if (tradeUpList != null && !string.IsNullOrEmpty(tradeUpList.Name))
            {
                dropObj.mainFlow.orderSummary.SelectedTradeUpComp = tradeUpList;
                dropObj.mainFlow.isTradeUp = true;
            }

            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            //06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - End
            #endregion

            return RedirectToAction(GetRegActionStep_AddContract(tabNumber));
        }

		private string autoAddAccFinancingPackageComponents(string _selectedVASIDs)
		{
			List<PgmBdlPckComponent> parentPackages = null;
			List<PgmBdlPckComponent> childPackages = null;

			using (var proxy = new CatalogServiceProxy())
			{
				parentPackages = MasterDataCache.Instance.PackageComponents.Where(x => x.LinkType == Properties.Settings.Default.Bundle_Package && x.PlanType == Properties.Settings.Default.Accessory_Financing_Component && x.Active == true).ToList();

				if (parentPackages != null && parentPackages.Count > 0)
				{
					Session[SessionKey.AF_PackageID.ToString()] = parentPackages.FirstOrDefault().ID;

					childPackages = MasterDataCache.Instance.PackageComponents.Where(x => x.ParentID == parentPackages.FirstOrDefault().ChildID && x.LinkType == Properties.Settings.Default.Accessory_Financing_Component && x.IsMandatory == true && x.Active == true).ToList();

					if (childPackages != null && childPackages.Count > 0)
					{
						foreach (var childPackage in childPackages)
						{
							if (_selectedVASIDs.EndsWith(","))
							{
								_selectedVASIDs = _selectedVASIDs + childPackage.ID + ",";
							}
							else
							{
								_selectedVASIDs = _selectedVASIDs + "," + childPackage.ID;
							}
						}
					}
				}
			}

			return _selectedVASIDs;

		}

        [HttpPost]
        public ActionResult PersonalDetails(DeviceVM deviceVM, FormCollection objfrm)
        {
            var deviceVMFromSession = new DeviceVM();
            deviceVMFromSession = (DeviceVM)Session["model"];
            deviceVM.Package = deviceVMFromSession.Package;
            deviceVM.ContractRemainingMonths = deviceVMFromSession.ContractRemainingMonths;
            deviceVM.ContractType = deviceVMFromSession.ContractType;
            deviceVM.PackageId = deviceVMFromSession.PackageId;
            deviceVM.ModelId = deviceVMFromSession.ModelId;
            deviceVM.SimType = deviceVMFromSession.SimType;
            deviceVM.Penality = deviceVMFromSession.Penality;
            deviceVM.isSimRequired = deviceVMFromSession.isSimRequired;
            deviceVM.SimSize = deviceVMFromSession.SimSize;
			deviceVM.subAction = deviceVMFromSession.subAction;
			
            deviceVM.Components.SubscribedComponentIds = deviceVMFromSession.Components.SubscribedComponentIds;

            if (objfrm["hdnback"].ToString2() == "true")
            {
                Session["model"] = deviceVM;
                Session["fromBackbutton"] = true;
                if (deviceVM.subAction == Constants.TERMINATEASSIGN_ACC_ONLY)
                    return RedirectToAction(GetRegActionStep_AddContract((int)AddContractRegSteps.Accessory));
                else
                    return RedirectToAction(GetRegActionStep_AddContract((int)AddContractRegSteps.VAS));
            }

            try
            {
                var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
                //dropObj.SetAdditionalContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
                bool isValid = false;
                string ErrorMessage = string.Empty;

                if (deviceVM.PersonalDetails.TabNumber == (int)AddContractRegSteps.Summary)
                {
                    #region 06052015 - Anthony - [Paperless CR] Customer Order Acknowledgement
                    var idDocumentFront = objfrm["frontPhoto"].ToString2();
                    var idDocumentBack = objfrm["backPhoto"].ToString2();
                    var idDocumentOther = objfrm["otherPhoto"].ToString2();
                    var idDocumentFrontName = objfrm["frontPhotoFileName"].ToString2();
                    var idDocumentBackName = objfrm["backPhotoFileName"].ToString2();
                    var idDocumentOtherName = objfrm["otherFileName"].ToString2();
                    var inputJustification = objfrm["Justification"].ToString2().Trim();
                    var waiverInfo = objfrm["hdnWaiverInfo"].ToString2();
                    #endregion

                    deviceVM.PersonalDetails = Util.FormPersonalDetailsandCustomer(deviceVM.PersonalDetails);
                    deviceVM.PersonalDetails.Customer.ContactNo = deviceVM.MSISDN;
                    //deviceVM.PersonalDetails.Customer.Gender = "M";

					// MyMaxis app survey 
					deviceVM.PersonalDetails.SurveyFeedBack = objfrm["surveyFeedback"].ToString2();
					//deviceVM.PersonalDetails.SurveyAnswer = !string.IsNullOrWhiteSpace(deviceVM.PersonalDetails.SurveyFeedBack) ? "Yes" : "No";
                    deviceVM.PersonalDetails.SurveyAnswerRate = objfrm["surveyAnswerRate"].ToString2();
                    deviceVM.PersonalDetails.SurveyAnswer = objfrm["surveyAnswer"].ToString2();

                    deviceVM.PersonalDetails.SingleSignOnValue = objfrm["SingleSignOnValue"].ToString2();
                    deviceVM.PersonalDetails.SingleSignOnEmailAddress = objfrm["SingleSignOnEmailAddress"].ToString2();

                    deviceVM.PersonalDetails.QueueNo = objfrm["QueueNo"].ToString2();
                    
                    #region 06052015 - Anthony - [Paperless CR] Customer Order Acknowledgement
                    deviceVM.PersonalDetails.CustomerPhoto = idDocumentFront;
                    deviceVM.PersonalDetails.AltCustomerPhoto = idDocumentBack;
                    deviceVM.PersonalDetails.Photo = idDocumentOther;
                    deviceVM.PersonalDetails.frontDocumentName = idDocumentFrontName;
                    deviceVM.PersonalDetails.backDocumentName = idDocumentBackName;
                    deviceVM.PersonalDetails.othDocumentName = idDocumentOtherName;
                    deviceVM.PersonalDetails.Justification = inputJustification;
                    deviceVM.PersonalDetails.hdnWaiverInfo = waiverInfo;

                    var waiverComponents = waiverInfo.Split('+');
                    var waiverChecked = new List<string>();

                    foreach(var waiverComp in waiverComponents)
                    {
                        if (!string.IsNullOrEmpty(waiverComp) && waiverComp.Contains('~'))
                        {
                            var isChecked = waiverComp.Split('~')[4];

                            if (isChecked == "1")
                            {
                                waiverChecked.Add(waiverComp);
                            }
                        }
                    }

                    deviceVM.PersonalDetails.cbWaiver = waiverChecked.ToArray();

					//clearing session if filename not exist 
					if (string.IsNullOrEmpty(deviceVM.PersonalDetails.frontDocumentName)) Session["frontImage"] = null;
					if (string.IsNullOrEmpty(deviceVM.PersonalDetails.backDocumentName)) Session["backImage"] = null;
					if (string.IsNullOrEmpty(deviceVM.PersonalDetails.othDocumentName)) Session["otherImage"] = null;
                    #endregion

                    //GTM e-Billing CR - Ricky - 2014.09.25 - Start
                    string SelectAccountDtls = Session[SessionKey.KenanACNumber.ToString()].ToString2();
                    deviceVM.PersonalDetails.AvailableMsisdn = WebHelper.Instance.GetAvailableMsisdnForSmsNotification(SelectAccountDtls);
                    deviceVM.PersonalDetails.EmailBillSubscription = objfrm["EmailBillSubscription"].ToString2();
                    deviceVM.PersonalDetails.EmailAddress = objfrm["EmailAddress"].ToString2();
                    deviceVM.PersonalDetails.SmsAlertFlag = objfrm["SmsAlertFlag"].ToString2();
                    deviceVM.PersonalDetails.SmsNo = objfrm["SmsNo"].ToString2();
                    deviceVM.PersonalDetails.OutletCode = objfrm["OutletCode"].ToString2();
                    deviceVM.PersonalDetails.OutletChannelId = objfrm["OutletChannelId"].ToString2();
                    deviceVM.PersonalDetails.SalesCode = objfrm["SalesCode"].ToString2();
                    deviceVM.PersonalDetails.SalesChannelId = objfrm["SalesChannelId"].ToString2();
                    deviceVM.PersonalDetails.SalesStaffCode = objfrm["SalesStaffCode"].ToString2();
                    deviceVM.PersonalDetails.SalesStaffChannelId = objfrm["SalesStaffChannelId"].ToString2();

                    WebHelper.Instance.BillDeliveryValidate(deviceVM.PersonalDetails.EmailBillSubscription, deviceVM.PersonalDetails.EmailAddress, deviceVM.PersonalDetails.SmsAlertFlag, deviceVM.PersonalDetails.SmsNo, out isValid, out ErrorMessage);
                    if (!isValid)
                    {
                        ModelState.AddModelError(string.Empty, ErrorMessage);
                        return View(deviceVM);
                    }
                    //if valid, store into Session
                    Session[SessionKey.billDelivery_emailBillSubscription.ToString()] = deviceVM.PersonalDetails.EmailBillSubscription;
                    Session[SessionKey.billDelivery_emailAddress.ToString()] = deviceVM.PersonalDetails.EmailAddress;
                    Session[SessionKey.billDelivery_smsAlertFlag.ToString()] = deviceVM.PersonalDetails.SmsAlertFlag;
                    Session[SessionKey.billDelivery_smsNo.ToString()] = deviceVM.PersonalDetails.SmsNo;
                    //GTM e-Billing CR - Ricky - 2014.09.25 - End

                    //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - Start
                    WebHelper.Instance.ValidateDealerInfo(deviceVM.PersonalDetails, out isValid, out ErrorMessage);
                    if (!isValid)
                    {
                        ModelState.AddModelError(string.Empty, ErrorMessage);
                        return View(deviceVM);
                    }
                    //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - End

                    #region Drop 5: BRE Check for non-Drop 4 Flow
                    //07042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - Start
                    string JustificationMessage = string.Empty;
                    string HardStopMessage = string.Empty;
                    string ApprovalMessage = string.Empty;
                    bool isBREFail = false;
                    WebHelper.Instance.BRECheckMessage(out HardStopMessage, out JustificationMessage, out ApprovalMessage, out isBREFail);

                    if (!string.IsNullOrEmpty(HardStopMessage) || (!string.IsNullOrEmpty(JustificationMessage) && string.IsNullOrEmpty(inputJustification)))
                    {
                        ViewBag.GSTMark = Settings.Default.GSTMark;
                        List<String> LabelReplace = new List<String>();
                        LabelReplace.Add("GSTMark");
                        ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
                    }

                    if (!string.IsNullOrEmpty(HardStopMessage))
                    {
                        ModelState.AddModelError(string.Empty, HardStopMessage);
                        return View(deviceVM);
                    }
                    else if (!string.IsNullOrEmpty(JustificationMessage) && string.IsNullOrEmpty(inputJustification))
                    {
                        ModelState.AddModelError(string.Empty, "Please Enter Justification for - " + JustificationMessage);
                        TempData["ErrorMessage"] = "Please Enter Justification for - " + JustificationMessage;
                        return View(deviceVM);
                    }
                    //07042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - End
                    #endregion

                    #region TAC Validation
                    // 20151117 - [w.loon] - TAC Validation - start
                    if (WebHelper.Instance.IsTacRequired() && !Online.Registration.Web.Properties.Settings.Default.DevelopmentModeOnForTacCheck && !Session["IsTacExceptional"].ToBool())
                    {
                        string submittedTac = objfrm["tacInput"].ToString2();
                        string EmptyTacError = "Please enter TAC.";
                        string ExpiredTacError = "TAC has expired. Please request a new TAC.";
                        string TacNotRequestedError = "TAC has not been requested. Please request a TAC.";
                        string InvalidTacError = "TAC is invalid. Please enter a valid TAC.";
                        deviceVM.PersonalDetails.TacSmsMsisdn = WebHelper.Instance.retrieveCurrentMsisdnByController(this.GetType());

                        // TempData["ErrorMessage"] is for New Layout (_PersonalDetailsNewPV.cshtml); ModelState.AddModelError() is for Old Layout (PersonalDetails.cshtml)
                        if (submittedTac == "" && objfrm["tacRequested"].ToString2() == "true")
                        {
                            //TempData["ErrorMessage"] = EmptyTacError;
                            ModelState.AddModelError(string.Empty, EmptyTacError);
                            return View(deviceVM);
                        }
                        else
                        {
                            HttpCookie tacCookie = Request.Cookies.Get("lastTac");
                            HttpCookie tacCookieDt = Request.Cookies.Get("lastTacRequestDt");
                            if (tacCookie == null && tacCookieDt == null)
                            {
                                if (objfrm["tacRequested"].ToString2() == "true")
                                {
                                    //TempData["ErrorMessage"] = ExpiredTacError;
                                    ModelState.AddModelError(string.Empty, ExpiredTacError);
                                }
                                else
                                {
                                    //TempData["ErrorMessage"] = TacNotRequestedError;
                                    ModelState.AddModelError(string.Empty, TacNotRequestedError);
                                }
                                return View(deviceVM);
                            }
                            else
                            {
                                if (submittedTac != tacCookie.Value.ToString())
                                {
                                    //TempData["ErrorMessage"] = InvalidTacError;
                                    ModelState.AddModelError(string.Empty, InvalidTacError);
                                    return View(deviceVM);
                                }
                                else
                                {
                                    // clear TAC cookies
                                    tacCookie.Expires = DateTime.Now.AddDays(-1);
                                    tacCookieDt.Expires = DateTime.Now.AddDays(-1);
                                    Response.Cookies.Add(tacCookie);
                                    Response.Cookies.Add(tacCookieDt);
                                }
                            }
                        }
                    }
                    // 20151117 - [w.loon] - TAC Validation - end
                    #endregion

                    deviceVM.PersonalDetails.TabNumber = (int)AddContractRegSteps.Summary;
                    #region VLT ADDED CODE
                    //Check the business validations
                    //BusinessValidationChecks(deviceVM.PersonalDetails);//05062015 - Anthony - Use BRECheckMessage method instead of BusinessValidationChecks
                    #endregion VLT ADDED CODE

                    if (deviceVM.PersonalDetails.Customer.PayModeID == Util.GetPaymentModeID(PAYMENTMODE.AutoBilling))
                    {
                        if (!ValidateCardDetails(deviceVM.PersonalDetails.Customer))
                        {
                            return View(deviceVM);
                        }
                    }
                }
                else
                {
					if (deviceVM.subAction != Constants.TERMINATEASSIGN_ACC_ONLY)
						deviceVM.PersonalDetails.TabNumber = (int)AddContractRegSteps.VAS;
					else
						deviceVM.PersonalDetails.TabNumber = (int)AddContractRegSteps.Accessory;
                }
            }
            catch (Exception ex)
            {

                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex; ;
            }
            Session["RegMobileReg_PersonalDetailsVM"] = deviceVM.PersonalDetails;
            Session["model"] = deviceVM;
            return RedirectToAction(GetRegActionStep_AddContract(deviceVM.PersonalDetails.TabNumber));
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MobileRegSummary(FormCollection collection, DeviceVM deviceVM)
        {
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            //dropObj.SetAdditionalContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            DAL.Models.Registration registration = new DAL.Models.Registration();
            var resp = new RegistrationCreateResp();
            var lstRebatePenality = (List<RegRebateDatacontractPenalty>)Session["lstRebatePenality"];
            var deviceVMFromSession = new DeviceVM();

            if (deviceVM.PersonalDetails.RegID.ToInt() == 0)
            {
                deviceVMFromSession = (DeviceVM)Session["model"];
                deviceVM.Package = deviceVMFromSession.Package;
                deviceVM.ContractRemainingMonths = deviceVMFromSession.ContractRemainingMonths;
                deviceVM.ContractType = deviceVMFromSession.ContractType;
                deviceVM.PackageId = deviceVMFromSession.PackageId;
                deviceVM.ModelId = deviceVMFromSession.ModelId;
                deviceVM.SimType = deviceVMFromSession.SimType;
                deviceVM.Penality = deviceVMFromSession.Penality;
                deviceVM.isSimRequired = deviceVMFromSession.isSimRequired;
                deviceVM.SimSize = deviceVMFromSession.SimSize;
                deviceVM.Components.SubscribedComponentIds = deviceVMFromSession.Components.SubscribedComponentIds;
                deviceVM.PersonalDetails.QueueNo = deviceVMFromSession.PersonalDetails.QueueNo;
                deviceVM.PersonalDetails.EmailBillSubscription = deviceVMFromSession.PersonalDetails.EmailBillSubscription;
                deviceVM.PersonalDetails.EmailAddress = deviceVMFromSession.PersonalDetails.EmailAddress;
                deviceVM.PersonalDetails.SmsAlertFlag = deviceVMFromSession.PersonalDetails.SmsAlertFlag;
                deviceVM.PersonalDetails.SmsNo = deviceVMFromSession.PersonalDetails.SmsNo;
                deviceVM.PersonalDetails.BillingCycle = deviceVMFromSession.PersonalDetails.BillingCycle;
                deviceVM.subAction = deviceVMFromSession.subAction;
            }

            try
            {

                #region PaymentRecived
                if (collection["submit1"].ToString() == "PaymentRecived")
                {
                    //Creating Package
                    #region CenterOrderContractCreation

                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = deviceVM.PersonalDetails.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                        });
                    }

                    bool isCreated = false;
                    using (var proxy_createacc = new KenanServiceProxy())
                    {
                        CenterOrderContractRequest Req = new CenterOrderContractRequest();
                        Req.orderId = deviceVM.PersonalDetails.RegID.ToString();
                        if (Request.Cookies["CookieUser"] != null)
                            Req.UserName = Request.Cookies["CookieUser"].Value;

                        isCreated = proxy_createacc.CenterOrderContractCreationDeviceCRP(Req);

                    }
                    if (isCreated)
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = deviceVM.PersonalDetails.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_SvcPendingActivate)
                            });
                        }
                        string username = string.Empty;
                        if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                        {
                            username = Request.Cookies["CookieUser"].Value;
                        }
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationUpdate(new Online.Registration.DAL.Models.Registration()
                            {
                                ID = deviceVM.PersonalDetails.RegID,
                                SalesPerson = username,
                                LastAccessID = Util.SessionAccess.UserName,
                                PenaltyAmount = -1,

                            });
                        }


                    }

                    #endregion


                    /*1.If device+plan genarate IMPOS file place in Shared path then redirect to new page with file name
                     */
                    if (Session[SessionKey.RegMobileReg_Type.ToString()] != null)
                    {
                        if (Convert.ToInt32(Session[SessionKey.RegMobileReg_Type.ToString()].ToString()) == (int)MobileRegType.DevicePlan)
                            using (var proxys = new CatalogServiceProxy())
                            {
                                try
                                {
                                    CRPIMPOSDetailsResp res = proxys.GetIMOPSDetails(deviceVM.PersonalDetails.RegID);
                                    if (res != null)
                                    {
                                        //string fileName = Util.SessionAccess.User.Org.OrganisationId + "_" + res.ModelID + "_" + res.RegID + "_" + DateTime.Now.Date.ToString("yyyyMMdd");
                                        return RedirectToAction("PlanPaymntReceived", new { regID = deviceVM.PersonalDetails.RegID });

                                    }
                                }
                                catch (Exception ex)
                                {
                                    WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                    return RedirectToAction("PlanPaymntReceived", new { regID = deviceVM.PersonalDetails.RegID });
                                }


                            }
                        else
                        {
                            return RedirectToAction("PaymntReceived", new { regID = deviceVM.PersonalDetails.RegID });
                        }
                    }
                    else
                    {
                        return RedirectToAction("MobileRegCanceled", new { regID = deviceVM.PersonalDetails.RegID });
                    }
                }
                #endregion

                #region close
                if (collection["submit1"].ToString() == "close")
                {
                    if (deviceVM.PersonalDetails.RegID == 0)
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resp = proxy.RegistrationCreate(ConstructRegistration(deviceVM,collection["QueueNo"].ToString2(), collection["Remarks"].ToString2(), deviceVM.ContractType, deviceVM.AccountType, deviceVM.MSISDN, deviceVM.SubscriberNo, deviceVM.SubscriberNoResets, deviceVM.AccountNumber, deviceVM.KenanAccountNo, deviceVM.OfferId, deviceVM.UOMCode, deviceVM.Penality, deviceVM.PersonalDetails.SignatureSVG, deviceVM.PersonalDetails.CustomerPhoto, deviceVM.PersonalDetails.AltCustomerPhoto, deviceVM.PersonalDetails.Photo, (collection["ISSIMRequired"] != null ? collection["ISSIMRequired"].ToString() : "false"), (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString() : "false")),
                            ConstructCustomer(), ConstructRegMdlGroupModel(deviceVM.ModelId, deviceVM.PackageId, deviceVM.DevicePrice), ConstructRegAddress(),
                            ConstructRegPgmBdlPkgComponent(deviceVM.PackageId, deviceVM.VasComponents, deviceVM.ContractType, deviceVM.DataPlanId), ConstructRegStatus(), ConstructRegSuppLines(),
                            ConstructRegSuppLineVASes(), null, null);
                            deviceVM.PersonalDetails.RegID = resp.ID;
                        }
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = deviceVM.PersonalDetails.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegBreFail)
                            });
                        }

                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegBreFail", new { regID = deviceVM.PersonalDetails.RegID });
                    }
                    else
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = deviceVM.PersonalDetails.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                            });
                        }
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegClosed", new { regID = deviceVM.PersonalDetails.RegID });
                    }

                }
                #endregion

                #region cancel
                if (collection["submit1"].ToString() == "cancel")
                {
                    string reason = string.Empty;
                    int prevStatus = WebHelper.Instance.GetRegStatus(deviceVM.PersonalDetails.RegID);
                    reason = ReferenceEquals(reason, null) ? "No reason provided." : collection["hdnReasonCancel"].ToString();
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = deviceVM.PersonalDetails.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)
                        });

                        proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                        {
                            RegID = deviceVM.PersonalDetails.RegID,
                            CancelReason = reason,
                            CreateDT = DateTime.Now,
                            LastUpdateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });

                        var totalPrice = Util.getPrinSuppTotalPrice(deviceVM.PersonalDetails.RegID);
                        if (prevStatus == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment) && WebHelper.Instance.GetRegStatus(deviceVM.PersonalDetails.RegID) == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan) && totalPrice > 0)
                        {
                            WebHelper.Instance.SendDetailsToWebPos(deviceVM.PersonalDetails.RegID, Constants.StatusWebPosToCancel);
                        }
                    }
                    Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                    return RedirectToAction("MobileRegCanceled", new { regID = deviceVM.PersonalDetails.RegID });
                }
                #endregion

                #region createAcc
                if (collection["submit1"].ToString() == "createAcc")
                {
                    try
                    {
                        string username = string.Empty;
                        if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                        {
                            username = Request.Cookies["CookieUser"].Value;
                        }
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationUpdate(new DAL.Models.Registration()
                            {
                                ID = deviceVM.PersonalDetails.RegID,
                                SalesPerson = username,
                                LastAccessID = Util.SessionAccess.UserName,
                                PenaltyAmount = deviceVM.PersonalDetails.PenaltyAmount,
                                
                            });
                        }

                        CreateKenanAccount(deviceVM.PersonalDetails.RegID);
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegAccCreated", new { regID = deviceVM.PersonalDetails.RegID });

                    }
                    catch (Exception ex)
                    {

                        WebHelper.Instance.LogExceptions(this.GetType(), ex);
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = deviceVM.PersonalDetails.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegFail", new { regID = deviceVM.PersonalDetails.RegID });
                    }
                }
                #endregion

                #region activateSvc
                if (collection["submit1"].ToString() == "activateSvc")
                {
                    try
                    {
                        FulfillKenanAccount(deviceVM.PersonalDetails.RegID);
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegSvcActivated", new { regID = deviceVM.PersonalDetails.RegID });
                    }
                    catch (Exception ex)
                    {

                        WebHelper.Instance.LogExceptions(this.GetType(), ex);
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = deviceVM.PersonalDetails.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                            });
                        }
                        Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                        return RedirectToAction("MobileRegFail", new { regID = deviceVM.PersonalDetails.RegID });
                    }
                }
                #endregion

                #region submit
                if (deviceVM.PersonalDetails.TabNumber == (int)AddContractRegSteps.Submit)
                {
                    //bool isValid = false;
                    //string ErrorMessage = string.Empty;

                    #region Drop 5: BRE Check for non-Drop 4 Flow
                    //07042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - Start
                    string JustificationMessage = string.Empty;
                    string HardStopMessage = string.Empty;
                    string ApprovalMessage = string.Empty;
                    bool isBREFail = false;
                    WebHelper.Instance.BRECheckMessage(out HardStopMessage, out JustificationMessage, out ApprovalMessage, out isBREFail);
                    deviceVM.PersonalDetails.isBREFail = isBREFail;
                    //07042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - End
                    #endregion

                    // Drop 5 - Maintain DOB and Nationality #Bugzilla 558 :p
                    deviceVM.PersonalDetails.DOBDay = collection["PersonalDetails.DOBDay"].ToInt();
                    deviceVM.PersonalDetails.DOBMonth = collection["PersonalDetails.DOBMonth"].ToInt();
                    deviceVM.PersonalDetails.DOBYear = collection["PersonalDetails.DOBYear"].ToInt();
                    deviceVM.PersonalDetails.Customer.DateOfBirth = DateTime.Parse(collection["PersonalDetails.Customer.DateOfBirth"]);
                    deviceVM.PersonalDetails.Customer.NationalityID = collection["PersonalDetails.Customer.NationalityID"].ToInt();
                    deviceVM.PersonalDetails.Address.Line1 = collection["PersonalDetails.Address.Line1"].ToString2();
                    deviceVM.PersonalDetails.Address.Line2 = collection["PersonalDetails.Address.Line2"].ToString2();
                    deviceVM.PersonalDetails.Address.Line3 = collection["PersonalDetails.Address.Line3"].ToString2();
                    deviceVM.PersonalDetails.Address.Postcode = collection["PersonalDetails.Address.Postcode"].ToString2();
                    deviceVM.PersonalDetails.Address.StateID = collection["PersonalDetails.Address.StateID"].ToInt();
                    deviceVM.PersonalDetails.Address.Town = collection["PersonalDetails.Address.Town"].ToString2();
                    deviceVM.PersonalDetails.SignatureSVG = collection["SignatureSVG"].ToString2();
                    // end :p

                    //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - Start
                    //deviceVM.PersonalDetails.OutletCode = collection["OutletCode"].ToString2();
                    //deviceVM.PersonalDetails.OutletChannelId = collection["OutletChannelId"].ToString2();
                    //deviceVM.PersonalDetails.SalesCode = collection["SalesCode"].ToString2();
                    //deviceVM.PersonalDetails.SalesChannelId = collection["SalesChannelId"].ToString2();
                    //deviceVM.PersonalDetails.SalesStaffCode = collection["SalesStaffCode"].ToString2();
                    //deviceVM.PersonalDetails.SalesStaffChannelId = collection["SalesStaffChannelId"].ToString2();

                    //WebHelper.Instance.ValidateDealerInfo(deviceVM.PersonalDetails, out isValid, out ErrorMessage);
                    //if (!isValid)
                    //{
                    //    ModelState.AddModelError(string.Empty, ErrorMessage);
                    //    return View(deviceVM);
                    //}
                    //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - End

                    // 2015.12.03 - w.loon - BUG 1367 To change the treatment for non-NRIC to follow IC - Biometric Failed/Not Performed
                    bool IsNonNric = !(Session[SessionKey.IDCardTypeName.ToString2()].ToString2() == "NEWIC" || Session["IDCardTypeID"].ToString2() == "1");
                    //bool routeToCDPU = (isBREFail && Session["IsDealer"].ToBool()) || (dropObj.mainFlow.isDeviceFinancingExceptionalOrder && dropObj.mainFlow.isDeviceFinancingOrder && Roles.IsUserInRole("MREG_CUSR"));
                    bool routeToCDPU = WebHelper.Instance.routeToCDPU();
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        var waiverInfo = deviceVM.PersonalDetails.hdnWaiverInfo.ToString2();
                        WebHelper.Instance.ConstructWaiverComponentsForSubmission(deviceVM.PersonalDetails, deviceVM.OrderSummary);//07052015 - Anthony - [Paperless CR] Customer Order Acknowledgement
                        
                        List<DAL.Models.WaiverComponents> objWaiverComp = Util.ConstructWaiverComponents(waiverInfo, orderSummary: dropObj.mainFlow.orderSummary);//21042015 - Anthony - Store the correct information to DB
                        var cCustomer = ConstructCustomer();//10042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow
                        Session[SessionKey.QueueNo.ToString()] = deviceVM.PersonalDetails.QueueNo;

                        resp = proxy.RegistrationCreate_CRP(ConstructRegistration(deviceVM,
                                                                deviceVM.PersonalDetails.QueueNo.ToString2(), 
                                                                collection["Remarks"].ToString2(), 
							                                    deviceVM.ContractType, 
                                                                deviceVM.AccountType, 
                                                                deviceVM.MSISDN, 
                                                                deviceVM.SubscriberNo, 
                                                                deviceVM.SubscriberNoResets, 
							                                    deviceVM.AccountNumber, 
                                                                deviceVM.KenanAccountNo, 
                                                                deviceVM.OfferId, 
                                                                deviceVM.UOMCode, 
                                                                deviceVM.Penality, 
                                                                deviceVM.PersonalDetails.SignatureSVG, 
							                                    string.Empty, 
                                                                string.Empty, 
                                                                string.Empty,
                                                                deviceVM.isSimRequired.ToString2(),
                                                                deviceVM.SimSize.ToString2()
                                                            ),
                                                            cCustomer, 
                                                            ConstructRegMdlGroupModel(deviceVM.ModelId, deviceVM.PackageId, deviceVM.DevicePrice), 
                                                            ConstructRegAddress(),
                                                            ConstructRegPgmBdlPkgComponent(deviceVM.PackageId, deviceVM.VasComponents, deviceVM.ContractType, deviceVM.DataPlanId), 
                                                            ConstructRegStatus(), 
                                                            ConstructRegSuppLines(),
                                                            ConstructRegSuppLineVASes(), 
                                                            ConstructPackage(deviceVM.MSISDN), 
                                                            ConstructCMSSID(), 
                                                            ConstructSmartComponents(deviceVM.PackageId, deviceVM.DataPlanId), 
                                                            objWaiverComp,
                                                            isBREFail);

						if (resp.ID > 0)
						{
							using (var updateProxy = new UpdateServiceProxy())
							{
								deviceVM.PersonalDetails.Photo = Session["otherImage"].ToString2();
								deviceVM.PersonalDetails.CustomerPhoto = Session["frontImage"].ToString2();
								deviceVM.PersonalDetails.AltCustomerPhoto = Session["backImage"].ToString2();

								if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.Photo))
								{
									updateProxy.SavePhotoToDB(deviceVM.PersonalDetails.Photo, "", "", resp.ID);
								}
								if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.CustomerPhoto))
								{
									updateProxy.SavePhotoToDB("", deviceVM.PersonalDetails.CustomerPhoto, "", resp.ID);
								}
								if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.AltCustomerPhoto))
								{
									updateProxy.SavePhotoToDB("", "", deviceVM.PersonalDetails.AltCustomerPhoto, resp.ID);
								}

                                #region Insert into trnRegAttributes
                                var regAttribList = new List<RegAttributes>();
                                if (!string.IsNullOrWhiteSpace(deviceVM.PersonalDetails.OutletCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCODE, ATT_Value = deviceVM.PersonalDetails.OutletCode });
                                if (!string.IsNullOrWhiteSpace(deviceVM.PersonalDetails.OutletChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCHANNELID, ATT_Value = deviceVM.PersonalDetails.OutletChannelId });
                                if (!string.IsNullOrWhiteSpace(deviceVM.PersonalDetails.SalesCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCODE, ATT_Value = deviceVM.PersonalDetails.SalesCode });
                                if (!string.IsNullOrWhiteSpace(deviceVM.PersonalDetails.SalesChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCHANNELID, ATT_Value = deviceVM.PersonalDetails.SalesChannelId });
                                if (!string.IsNullOrWhiteSpace(deviceVM.PersonalDetails.SalesStaffCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCODE, ATT_Value = deviceVM.PersonalDetails.SalesStaffCode });
                                if (!string.IsNullOrWhiteSpace(deviceVM.PersonalDetails.SalesStaffChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCHANNELID, ATT_Value = deviceVM.PersonalDetails.SalesStaffChannelId });

                                if (!string.IsNullOrWhiteSpace(deviceVM.PersonalDetails.SingleSignOnValue))
                                {
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_SINGLESIGNON_STATUS, ATT_Value = deviceVM.PersonalDetails.SingleSignOnValue });

                                    if (deviceVM.PersonalDetails.SingleSignOnValue.ToUpper() == "YES")
                                    {
                                        regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_SINGLESIGNON_EMAILADDRESS, ATT_Value = deviceVM.PersonalDetails.SingleSignOnEmailAddress });
                                    }
                                }

                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_PAYMENT_METHOD, ATT_Value = Util.getPaymentMethodName() });

                                //Samuel - KFC bucket phase 2
                                var pMSISDN = WebHelper.Instance.getPrincipalNoFromSuppNo(deviceVM.MSISDN);
                                if (!string.IsNullOrEmpty(pMSISDN))
                                {
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.PRIN_MSISDN, ATT_Value = pMSISDN.ToString2() });
                                }
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_USER_DEVICE_AGENT, ATT_Value = Request.UserAgent });

                                //proxy.SaveListRegAttributes(regAttribList);

                                #region Accessory Financing Penalty Amount
                                var contractDetails = (List<ContractDetails>)Session[SessionKey.ContractDetails.ToString()];

                                if (contractDetails != null && contractDetails.Count > 0 && contractDetails.Where(x => x.isellContractGroup == "AF").Any())
                                {
                                    var AFPenaltyAmount = contractDetails.Where(x => x.isellContractGroup == "AF" && x.Penalty > 0).Select(y => y.Penalty).Sum();

                                    if (AFPenaltyAmount > 0)
                                        regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_AF_PENALTY_AMOUNT, ATT_Value = AFPenaltyAmount.ToString2() });
                                }
								regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_TERMINATE_TYPE, ATT_Value = deviceVM.subAction });
                                #endregion

                                WebHelper.Instance.ConstructRegAttributesForDF_AddContract(resp.ID, ref regAttribList, out regAttribList);
                                WebHelper.Instance.ConstructRegAttributesForAF(resp.ID, ref regAttribList);
                                if (!string.IsNullOrWhiteSpace(deviceVM.PersonalDetails.BillingCycle))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_ACC_BILL_CYCLE, ATT_Value = deviceVM.PersonalDetails.BillingCycle });

                                proxy.SaveListRegAttributes(regAttribList);
                                #endregion

								#region Insert into trnRegJustification
								WebHelper.Instance.saveTrnRegJustification(resp.ID);
                                #endregion

                                #region Insert into trnRegBreFailTreatment
                                WebHelper.Instance.saveTrnRegBreFailTreatment(resp.ID);
                                #endregion

								#region Insert into trnSurveyResponse
								WebHelper.Instance.SaveSurveyResult(deviceVM.PersonalDetails, RegSurveyResponse.MAXIS_APP, resp.ID);
								#endregion

								#region Insert into trnRegAccessory & trnRegAccessoryDetails
                                var serialNumberList = new Dictionary<int, string>();
								WebHelper.Instance.SaveTrnRegAccessory(dropObj, resp.ID, ref serialNumberList);
                                WebHelper.Instance.SaveTrnRegAccessoryDetails(resp.ID, serialNumberList);
								#endregion

                            }

                            //29042015 - Anthony - [Paperless CR] Save document to tblreguploaddoc for iContract - Start
                            bool iContractActive = ConfigurationManager.AppSettings["iContractActive"].ToBool();
                            if (iContractActive)
                            {
                                WebHelper.Instance.SaveDocumentToTable(dropObj, resp.ID);
                            }
                            //29042015 - Anthony - Save document to tblreguploaddoc for iContract - End
                            
                            // Drop 5
                            if (routeToCDPU)
                            {
                                try
                                {
                                    proxy.UpsertBRE(new RegBREStatus()
                                    {
                                        RegId = resp.ID,
                                        TransactionStatus = Constants.StatusCDPUPending
                                    });
                                }
                                catch
                                {

                                }
                            }
						}


                       //Joshi added this code
                        deviceVM.PersonalDetails.RegID = resp.ID;
                        if ((Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("MREG_DAI")) && false)
                        {
                            string username = string.Empty;
                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                            {
                                username = Request.Cookies["CookieUser"].Value;
                            }
                            using (var proxyobj = new RegistrationServiceProxy())
                            {
                                proxyobj.RegistrationUpdate(new DAL.Models.Registration()
                                {
                                    ID = deviceVM.PersonalDetails.RegID,
                                    SalesPerson = username,
                                    LastAccessID = Util.SessionAccess.UserName,
                                    PenaltyAmount = deviceVM.PersonalDetails.PenaltyAmount,
                                    
                                });
                                if (!routeToCDPU)
                                {
                                    proxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = deviceVM.PersonalDetails.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RFP)
                                    });
                                }
                            }

                            if (!routeToCDPU)
                            {
                                using (var kenanProxy = new KenanServiceProxy())
                                {
                                    String Action = WebHelper.Instance.WebPosAuto(deviceVM.PersonalDetails.RegID);
                                    if (!ReferenceEquals(Action, String.Empty))
                                        return RedirectToAction(Action, new { regID = deviceVM.PersonalDetails.RegID });
                                    //kenanProxy.CreateIMPOSFile(deviceVM.PersonalDetails.RegID);
                                }
                            }

                        }
                      

                        //End of joshi code
                        
                        if (resp.ID > 0)
                        {
                            //commented as disabling pos file feature
                            //#region Saving POS FileName in DB for Dealers
                            //if ((Session["IsDealer"].ToBool() == true))
                            //{
                            //    string posFileName = WebHelper.Instance.CreatePOSFileName(resp.ID);
                            //    using (var catLogProxy = new CatalogServiceProxy())
                            //    {
                            //        catLogProxy.UpdateIMPOSDetails(resp.ID, posFileName, 1);
                            //    }
                            //}
                            //#endregion

                            //saving rebate penalty in database
                            if (lstRebatePenality != null)
                            {
                                List<RegRebateDatacontractPenalty> objlstRebatePenality = new List<RegRebateDatacontractPenalty>();
                                RegRebateDatacontractPenalty objRebateDatacontractPenalty = new RegRebateDatacontractPenalty();
                                LnkLnkRegRebateDatacontractPenaltyReq objRebatePenaltyReq = new LnkLnkRegRebateDatacontractPenaltyReq();
                                foreach (var v in lstRebatePenality)
                                {
                                    objRebateDatacontractPenalty.nrcID = v.nrcID;
                                    objRebateDatacontractPenalty.Penalty = v.Penalty;
                                    objRebateDatacontractPenalty.RegID = resp.ID;
                                    objRebateDatacontractPenalty.Active = v.Active;
                                    objRebateDatacontractPenalty.ComponetID = v.ComponetID;
                                    objRebateDatacontractPenalty.CreateDT = v.CreateDT;
                                    objlstRebatePenality.Add(objRebateDatacontractPenalty);
                                }
                                objRebatePenaltyReq.rebatePenaltyDetails = objlstRebatePenality.ToArray();
                                using (var regProxy = new RegistrationServiceProxy())
                                {

                                    int value = regProxy.SaveLnkRegRebateDatacontractPenalty(objRebatePenaltyReq);
                                }
                            }
                            int regTypeID = Util.GetRegTypeID(REGTYPE.DeviceCRP);

							string PlanKenanCode = "";
							string latestPrintVersion = "";
							// for acc only.
							if (deviceVM.subAction != Constants.TERMINATEASSIGN_ACC_ONLY)
							{
								PlanKenanCode = MasterDataCache.Instance.PackageComponents.Where(a => a.ID == Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"])).Select(a => a.KenanCode).FirstOrDefault().ToString2();
								latestPrintVersion = Util.GetPrintVersionNum(regTypeID, !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty, PlanKenanCode);
							}
							
							Online.Registration.Web.SubscriberICService.retrieveSimDetlsResponse objRetrieveSimDetlsResponse = new Online.Registration.Web.SubscriberICService.retrieveSimDetlsResponse();
                            if (!ReferenceEquals(deviceVM.MSISDN, null) && deviceVM.isSimRequired)
                            {
                                using (var _proxy = new retrieveServiceInfoProxy())
                                {
                                    objRetrieveSimDetlsResponse = _proxy.retreiveSIMDetls(deviceVM.MSISDN);
                                }
                            }
                            else
                            {
                                objRetrieveSimDetlsResponse = null;
                            }
                            LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
                            Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                            dropObj.getExistingContractCount(dropObj);
                            int CurrentTotalContract = (dropObj.additionalContract + dropObj.existingContract);
                            if (objRetrieveSimDetlsResponse != null)
                            {
                                List<Online.Registration.Web.SubscriberICService.SimDetails> simDetlsFiltered = objRetrieveSimDetlsResponse.itemList.Where(c => c.InactiveDt == null || c.InactiveDt == string.Empty).ToList();
                                if (!ReferenceEquals(simDetlsFiltered, null) && simDetlsFiltered.Count > 0)
                                {
                                    if (resp.ID > 0 && !string.IsNullOrEmpty(simDetlsFiltered[0].ExternalID))
                                    {
                                        objRegDetails.CreatedDate = DateTime.Now;
                                        objRegDetails.SimType = deviceVM.SimType.ToLower() == "true" ? "MISM" : "Normal"; //"Normal";///GET THE SimType INFORMATION FROM simType VARIABLE FOUND ABOVE // Session[SessionKey.SimType.ToString()].ToString();
                                        objRegDetails.UserName = Util.SessionAccess.UserName;
                                        objRegDetails.RegId = resp.ID;
                                        objRegDetails.OldSimSerial = simDetlsFiltered[0].ExternalID;
                                        objRegDetails.isDeviceCRP = false;
                                        objRegDetails.IsSuppNewAc = false;
                                        objRegDetails.MOC_Status = deviceVM.PersonalDetails.MOCStatus;
                                        objRegDetails.Liberlization_Status = deviceVM.PersonalDetails.Liberlization_Status;

                                        //*****Added for sim card type cr*******//
                                        if (!string.IsNullOrEmpty(deviceVM.SimSize))
                                            objRegDetails.SimCardType = deviceVM.SimSize;
										//*****Added for sim card type cr*******//


                                        //Insert justifaication
                                        //if (collection["txtJustfication"].ToString2().Length > 0)
                                        //{
                                        //    objRegDetails.Justification = collection["txtJustfication"].ToString();
                                        //}
                                        if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.Justification))
                                        {
                                            objRegDetails.Justification = deviceVM.PersonalDetails.Justification.ToString2();
                                        }

                                        if (!string.IsNullOrEmpty(latestPrintVersion))
                                        {
                                            objRegDetails.PrintVersionNo = latestPrintVersion;
                                        }
                                        //objRegDetails.ContractCheckCount = Session["AccountswithContractCount"].ToInt();
                                        objRegDetails.ContractCheckCount = CurrentTotalContract;
                                        bool IsWriteOff = false;
                                        string acc = string.Empty;
                                        WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
                                        objRegDetails.WriteOffDetails = acc;

                                        objRegDetailsReq.LnkDetails = objRegDetails;
                                    }
                                }
                            }
                            else
                            {
                                objRegDetails.CreatedDate = DateTime.Now;
                                objRegDetails.SimType = deviceVM.SimType.ToLower() == "true" ? "MISM" : "Normal";  //"Normal";///GET THE SimType INFORMATION FROM simType VARIABLE FOUND ABOVE // Session[SessionKey.SimType.ToString()].ToString();
                                objRegDetails.UserName = Util.SessionAccess.UserName;
                                objRegDetails.RegId = resp.ID;
                                objRegDetails.isDeviceCRP = false;
                                objRegDetails.IsSuppNewAc = false;
                                objRegDetails.MOC_Status = deviceVM.PersonalDetails.MOCStatus;
                                objRegDetails.Liberlization_Status = deviceVM.PersonalDetails.Liberlization_Status;
                                objRegDetails.TotalLineCheckCount = Session["existingTotalLine"].ToInt();
                                //Insert justifaication
                                //if (collection["txtJustfication"].ToString2().Length > 0)
                                //{
                                //    objRegDetails.Justification = collection["txtJustfication"].ToString();
                                //}
                                if (!string.IsNullOrEmpty(deviceVM.PersonalDetails.Justification))
                                {
                                    objRegDetails.Justification = deviceVM.PersonalDetails.Justification.ToString2();
                                }

								//*****Added for sim card type cr*******//
								if (!string.IsNullOrEmpty(deviceVM.SimSize))
									objRegDetails.SimCardType = deviceVM.SimSize;
								//*****Added for sim card type cr*******//


                                if (!string.IsNullOrEmpty(latestPrintVersion))
                                {
                                    objRegDetails.PrintVersionNo = latestPrintVersion;
                                }
                                //objRegDetails.ContractCheckCount = Session["AccountswithContractCount"].ToInt();
                                objRegDetails.ContractCheckCount = CurrentTotalContract;

                                objRegDetailsReq.LnkDetails = objRegDetails;
                            }
                            using (var proxy1 = new RegistrationServiceProxy())
                            {
                                proxy1.SaveLnkRegistrationDetails(objRegDetailsReq);
                            }
                        }

                        var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
                        if (biometricID != 0)
                        {
                            var biometric = proxy.BiometricGet(new int[] { biometricID }).FirstOrDefault();
                            biometric.RegID = resp.ID;

                            proxy.BiometricUpdate(biometric);
                        }
                    }

                    #region ADDED BY Rajeswari on 15th March
                    if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
                    {
                        Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
                    }
                    #endregion
                    #region VLT ADDED CODE on 21stFEB for email functionality
                    if (resp.ID > 0)
                    {
                        int regTypeID = 0;
                        if (Session[SessionKey.RegMobileReg_Type.ToString()] != null)
                        {
                            regTypeID = Session[SessionKey.RegMobileReg_Type.ToString()].ToInt();
                        }

						//string PlanKenanCode = MasterDataCache.Instance.PackageComponents.Where(a => a.ID == Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"])).Select(a => a.KenanCode).FirstOrDefault().ToString2();
						//string latestPrintVersion = Util.GetPrintVersionNum(regTypeID, !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty, PlanKenanCode);
						/* BEGIN Added by VLT 5 Apr 2013 to add kenaninfo to database*/
                        if (Session["KenanCustomerInfo"] != null)
                        {
                            string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString().Split('╚');
                            KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
                            kenanInfo.FullName = strArrKenanInfo[0];
                            kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
                            kenanInfo.IDCardNo = strArrKenanInfo[2];
                            kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
                            kenanInfo.Address1 = strArrKenanInfo[4];
                            kenanInfo.Address2 = strArrKenanInfo[5];
                            kenanInfo.Address3 = strArrKenanInfo[6];
                            kenanInfo.Postcode = strArrKenanInfo[8];
                            kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
                            kenanInfo.RegID = resp.ID;
                            using (var proxys = new RegistrationServiceProxy())
                            {
                                var kenanResp = new KenanCustomerInfoCreateResp();

                                kenanResp = proxys.KenanCustomerInfoCreate(kenanInfo);
                            }
                        }
                        Session["KenanCustomerInfo"] = null;
                        /* END Added by VLT 5 Apr 2013 to add kenaninfo to database*/

                        ClearRegistrationSession();
                        //RST GST
                        Session[SessionKey.TradeUpAmount.ToString()] = 0;

                        if ((Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("MREG_DAI")) && false)
                        {
                            return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });
                        }
                        else
                        {
                            return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                        }
                    }
                    #endregion VLT ADDED CODE on 21stFEB for email functionality
                }
                #endregion

                #region createPackage/personalDetails
                if (collection["submit1"].ToString() == "createpackage")
                {
                    var reg = new DAL.Models.Registration();
                    //var regMdlGrpModels = new List<RegMdlGrpModel>();
                    OrderSummaryVM orderSummaryVM = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        reg = proxy.RegistrationGet(deviceVM.PersonalDetails.RegID);
                    }

                    List<string> errorValidationList = new List<string>();

                    if (string.IsNullOrEmpty(reg.SIMSerial) && reg.IsSImRequired_Smart)
                    {
                        // fill error msg
                        errorValidationList.Add("Sim serial");
                    }
                    if (string.IsNullOrEmpty(reg.IMEINumber) && !string.IsNullOrEmpty(orderSummaryVM.ModelImageUrl))
                    {
                        // fill error msg
                        errorValidationList.Add("IMEI number");
                    }
                    if (errorValidationList.Any())
                    {
                        TempData[SessionKey.errorValidationList.ToString()] = errorValidationList;
                        using (var proxy = new UserServiceProxy())
                        {
                            String errorTrace = "fail insert inventory" + (String)(errorValidationList.Contains("Sim serial") ? " SIM(" + deviceVM.PersonalDetails.RegSIMSerial + ") " : "") + (String)(errorValidationList.Contains("IMEI number") ? " IMEI(" + deviceVM.PersonalDetails.RegIMEINumber + ") " : "") + "for regID: " + reg.ID;
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.AddContract), errorTrace, "", "");
                        }
                        return RedirectToAction("MobileRegSummary", new { regID = deviceVM.PersonalDetails.RegID });
                    }
                    #region CDPU Approval - status to pending cdpu approval/cdpu rejected
                    if (Session["IsDealer"].ToBool() || Roles.IsUserInRole("MREG_DAPP"))
                    {
                        if (collection["CDPUStatus"].ToString2() == Constants.StatusCDPUPending)  // have bre failed
                        {
                            //Update Order Status
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                proxy.RegistrationCancel(new RegStatus()
                                {
                                    RegID = deviceVM.PersonalDetails.RegID,
                                    Active = true,
                                    CreateDT = DateTime.Now,
                                    StartDate = DateTime.Now,
                                    LastAccessID = Util.SessionAccess.UserName,
                                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCDPUPendingApp)
                                });
                            }
                            return RedirectToAction("MobileRegSummary", new { regID = deviceVM.PersonalDetails.RegID });
                        }
                        if (collection["CDPUStatus"].ToString2() == Constants.StatusCDPURejected)
                        {
                            GeneralResult result = WebHelper.Instance.UnreserveNumberDealerNet(deviceVM.PersonalDetails.RegID);
                            if (!ReferenceEquals(result, null))
                            {
                                if (result.ResultCode == 0 && result.ResultMessage == "success")
                                {
                                    using (var proxy = new RegistrationServiceProxy())
                                    {
                                        proxy.RegistrationCancel(new RegStatus()
                                        {
                                            RegID = deviceVM.PersonalDetails.RegID,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            StartDate = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCDPURejected)
                                        });

                                        proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                                        {
                                            RegID = deviceVM.PersonalDetails.RegID,
                                            CancelReason = Properties.Settings.Default.Status_RegCDPURejected,
                                            CreateDT = DateTime.Now,
                                            LastUpdateDT = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName
                                        });

                                        proxy.UpsertBRE(new RegBREStatus()
                                        {
                                            RegId = deviceVM.PersonalDetails.RegID,
                                            TransactionStatus = Constants.StatusCDPURejected,
                                            TransactionReason = collection["CDPUDescription"],
                                            TransactionDT = DateTime.Now,
                                            TransactionBy = Util.SessionAccess.UserName
                                        });
                                    }
                                    if (Roles.IsUserInRole("MREG_DAPP"))
                                    {
                                        return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                    }
                                    return RedirectToAction("MobileRegCanceled", new { regID = deviceVM.PersonalDetails.RegID });
                                }
                            }
                            else
                            {
                                return RedirectToAction("MobileRegSummary", new { regID = deviceVM.PersonalDetails.RegID });
                            }
                        }
                        if (collection["CDPUStatus"].ToString2() == Constants.StatusCDPUApprove)
                        {
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                proxy.UpsertBRE(new RegBREStatus()
                                {
                                    RegId = deviceVM.PersonalDetails.RegID,
                                    TransactionStatus = Constants.StatusCDPUApprove,
                                    TransactionReason = collection["CDPUDescription"],
                                    TransactionDT = DateTime.Now,
                                    TransactionBy = Util.SessionAccess.UserName
                                });
                            }
                        }
                    }
                    #endregion

                    //using (var proxy = new RegistrationServiceProxy())
                    //{
                    //    proxy.RegistrationClose(new RegStatus()
                    //    {
                    //        RegID = deviceVM.PersonalDetails.RegID,
                    //        Active = true,
                    //        CreateDT = DateTime.Now,
                    //        StartDate = DateTime.Now,
                    //        LastAccessID = Util.SessionAccess.UserName,
                    //        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RFP)
                    //    });
                    //}

                    using (var proxy = new KenanServiceProxy())
                    {
                        String Action = WebHelper.Instance.WebPosAuto(deviceVM.PersonalDetails.RegID);
                        bool iscdpuorder = WebHelper.Instance.isCDPUorder(deviceVM.PersonalDetails.RegID);

                        if (ReferenceEquals(Action, String.Empty) && iscdpuorder)
                        {
                            using (var autoProxy = new AutoActivationServiceProxy())
                            {
                                autoProxy.KenanAccountFulfill((int)MobileRegType.Contract, deviceVM.PersonalDetails.RegID.ToString());
                            }
                        }
                        //if (!ReferenceEquals(Action, String.Empty))
                        //    return RedirectToAction(Action, new { regID = deviceVM.PersonalDetails.RegID });
                        //proxy.CreateIMPOSFile(deviceVM.PersonalDetails.RegID);
                    }
                    Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();

                    bool isCDPUorderRecs = WebHelper.Instance.isCDPUorder(deviceVM.PersonalDetails.RegID);
                    if (!isCDPUorderRecs)
                    {
                        #region 11052015 - Anthony - Generate RF for iContract
                        try
                        {
                            using (var proxy = new UpdateServiceProxy())
                            {
								if (!proxy.FindDocument(Constants.CONTRACT, deviceVM.PersonalDetails.RegID))
								{
									String contractHTMLSource = string.Empty;
									contractHTMLSource = Util.RenderPartialViewToString(this, "PrintContractDetails", WebHelper.Instance.GetContractDetailsViewModel_AddContract(deviceVM.PersonalDetails.RegID, false));
									contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
									contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
									contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide");
									if (!string.IsNullOrEmpty(contractHTMLSource))
									{
										if (!string.IsNullOrEmpty(contractHTMLSource))
										{
											Byte[] PDFBytes = Util.convertHTMLStringToPDFByteArray(contractHTMLSource, deviceVM.PersonalDetails.RegID);
										}
									}
								}
                            }
                        }
                        catch (Exception ex)
                        {
                            //Logger.Error("ICONTRACT EXCEPTION = " + ex);
                            WebHelper.Instance.LogExceptions(this.GetType(), ex);
                        }
                        #endregion
                    }
                    else
                    {
                        bool isFilecreated = WebHelper.Instance.GenerateCDPUcontractFile(deviceVM.PersonalDetails.RegID);
                    }

                    if (Roles.IsUserInRole("MREG_DAPP"))
                    {
                        return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                    }
                    return RedirectToAction("MobileRegSvcActivated", new { regID = deviceVM.PersonalDetails.RegID });
                }
                else if (deviceVM.PersonalDetails.TabNumber == (int)AddContractRegSteps.PersonalDetails)
                {
                    Session["model"] = deviceVM;
                    Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                    return RedirectToAction("PersonalDetails");
                }
                #endregion
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex; ;
            }
            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
            if (Roles.IsUserInRole("MREG_DAPP"))
            {
                return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
            }
            return RedirectToAction("MobileRegFail", new { regID = resp.ID });
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PrintMobileRegSummary(FormCollection collection, RegistrationFormVM personalDetailsVM)
        {
            Util.CommonPrintMobileRegSummary(collection);

            return RedirectToAction("MobileRegSummary", new { id = collection[1].ToString2() });

        }

        #endregion

        [HttpPost]
        public string GetContractsForOffer(string OfferKenanCodes, string offerid)
        {
            string retrivedContractIds = string.Empty;
            retrivedContractIds = WebHelper.Instance.GetContractsForOffer(OfferKenanCodes, offerid);
            return retrivedContractIds;
        }


        #region Private Methods

        /// <summary>
        /// Get the package details.
        /// </summary>
        /// <param name="MSISDN">MSISDN</param>
        /// <param name="Kenancode">Kenan code</param>
        /// <returns></returns>
        private IList<SubscriberICService.PackageModel> GetPackageDetails(string MSISDN, string Kenancode)
        {


            #region Retrieve Rebate Contract Id
            RetrieveComponentInfoResponse objRetrieveComponentInfoRes = null;
            using (var proxy = new KenanServiceProxy())
            {
                RetrieveComponentInfoRequest objRetrieveComponentInfoReq = new RetrieveComponentInfoRequest();
                objRetrieveComponentInfoReq.ExternalID = Session[SessionKey.ExternalID.ToString()].ToString2();
                Session["ComponentInfo"] = objRetrieveComponentInfoRes = proxy.RetrieveComponentInfo(objRetrieveComponentInfoReq);
            }
            #endregion

            string str = Properties.Settings.Default.retrievePkgCompInfoService.ToString();
            Online.Registration.Web.SubscriberICService.SubscriberICServiceClient icserviceobj = new Online.Registration.Web.SubscriberICService.SubscriberICServiceClient();
            IList<Online.Registration.Web.SubscriberICService.PackageModel> packages =
                icserviceobj.retrievePackageDetls(MSISDN, Kenancode,
                userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName, password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword, postUrl: "");


            return packages;

        }

        #region CR#1234 - Re-skinning
        /*
        /// <summary>
        /// Display list of available brands based on existing package.
        /// </summary>
        /// <returns></returns>
        public ActionResult SelectDevice()
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session[SessionKey.OfferId.ToString()] = null;

            Session["AddedVases"] = null;
            Session["RemovedVases"] = null;
            Session["AddedVasNames"] = null;
            Session["RemovedVasNames"] = null;
            Session["AddedVasPrices"] = null;
            Session["RemovedVasPrices"] = null;
            Session["ARVasGroupIds"] = null;

            DeviceVM deviceVM = new DeviceVM();
            if (Session["model"] != null)
                deviceVM = (DeviceVM)Session["model"];

            ResetAllSession();

            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;

            var deviceOptVM = GetAvailableDeviceOptions(deviceVM.AccountType, deviceVM.ContractType, deviceVM.Package);

            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = deviceOptVM.OrderSummary = ConstructOrderSummary(deviceVM.PackageId, null, deviceVM.VasComponents, deviceVM.VasNames, null, deviceVM.Penality);

            deviceOptVM.AccountType = deviceVM.AccountType;
            deviceOptVM.ContractType = deviceVM.ContractType;
            deviceOptVM.MSISDN = deviceVM.MSISDN;
            deviceOptVM.PackageDesc = deviceVM.PackageDesc;
            deviceOptVM.PackageId = deviceVM.PackageId;
            deviceOptVM.ContractRemainingMonths = deviceVM.ContractRemainingMonths;
            deviceOptVM.VasComponents = deviceVM.VasComponents;
            deviceOptVM.AccountNumber = deviceVM.AccountNumber;
            deviceOptVM.AccountNumber = deviceVM.AccountNumber;
            deviceOptVM.KenanAccountNo = deviceVM.KenanAccountNo;
            deviceOptVM.Penality = deviceVM.Penality;
            deviceOptVM.VasNames = deviceVM.VasNames;
            deviceOptVM.Package = deviceVM.Package;
            deviceOptVM.SimType = deviceVM.SimType;
            deviceOptVM.SubscriberNo = deviceVM.SubscriberNo;
            deviceOptVM.SubscriberNoResets = deviceVM.SubscriberNoResets;

            //28042015 - Anthony - to validate the BRE check message in order summary if no contract added - Start
            string JustificationMessage = string.Empty;
            string HardStopMessage = string.Empty;
            bool isBREFail = false;
            WebHelper.Instance.BRECheckMessage(out HardStopMessage, out JustificationMessage, out isBREFail);
            //28042015 - Anthony - to validate the BRE check message in order summary if no contract added - End

            if (deviceOptVM != null && deviceOptVM.DeviceOption != null)
            {
                if (deviceOptVM.DeviceOption.AvailableBrands.Any())
                {
                    return View(deviceOptVM);
                }
                else
                {
                    string SelectAccountDtls = null;
                    if (deviceVM != null)
                        SelectAccountDtls = deviceVM.KenanAccountNo + "," + deviceVM.MSISDN + "," + deviceVM.SubscriberNo + "," + deviceVM.SubscriberNoResets + "," + "no devices" + "," + deviceVM.AccountType + "," + deviceVM.AccountType + ",";

                    return RedirectToAction("ContractCheck", new { SelectAccountDtls = SelectAccountDtls });
                }
            }
            else
                return View(deviceOptVM);
        }

        /// <summary>
        /// Display list of available devices based on existing package and selected brand.
        /// </summary>
        /// <returns></returns>
        public ActionResult SelectDeviceCatalog()
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.TradeUpAmount.ToString()] = 0;   //RST GST
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;

            DeviceVM deviceVM = new DeviceVM();
            if (Session["model"] != null)
                deviceVM = (DeviceVM)Session["model"];

            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;

            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = deviceVM.OrderSummary = ConstructOrderSummary(deviceVM.PackageId, null, deviceVM.VasComponents, deviceVM.VasNames, null, null);

            deviceVM.Phone = GetAvailableModelImage(deviceVM.ContractType, deviceVM.AccountType, deviceVM.SelectedOptionType, deviceVM.SelectedOptionId, deviceVM.Package, deviceVM.PackageId);

            if (deviceVM != null)
                deviceVM.Phone.SelectedModelImageID = deviceVM.ModelId;
            return View(deviceVM);
        }
        
        
        [HttpPost]
        public ActionResult SelectDevice(DeviceVM deviceVM, FormCollection Collection)
        {
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            //dropObj.SetAdditionalContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow

            if (Collection["TabNumber"].ToInt() == (int)AddContractRegSteps.AccountList)
            {
                return RedirectToAction("Index");
            } 
            
            var options = (!string.IsNullOrEmpty(deviceVM.DeviceOption.SelectedDeviceOption)) ? deviceVM.DeviceOption.SelectedDeviceOption.Split('_').ToList() : null;
            if (options != null)
            {
                try
                {
                    if (deviceVM.DeviceOption.TabNumber == (int)AddContractRegSteps.DevicePlan)
                    {
                        if (deviceVM.SelectedOptionType.ToInt() != options[1].ToInt() || deviceVM.SelectedOptionId.ToInt() != options[2].ToInt())
                        {
                            deviceVM.SelectedOptionType = options[1];
                            deviceVM.SelectedOptionId = options[2];
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                Session["model"] = deviceVM;
                Session["DeviceSelected"] = 1;
                return RedirectToAction(GetRegActionStep_AddContract(deviceVM.DeviceOption.TabNumber));
            }
            else
            {
                return View(GetRegActionStep_AddContract(1));
            }
        }

        [HttpPost]
        public ActionResult SelectDeviceCatalog(DeviceVM deviceVM, FormCollection Collection)
        {
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            //dropObj.SetAdditionalContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            int count = -1;
            try
            {
                if (deviceVM.Phone.TabNumber > 0)
                {
                    deviceVM.ModelId = deviceVM.Phone.SelectedModelImageID;

                    var brandArticleImage = new BrandArticle();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        brandArticleImage = proxy.BrandArticleImageGet(deviceVM.Phone.SelectedModelImageID.ToInt());
                    }
                    if (brandArticleImage != null)
                    {
                        string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                        deviceVM.Phone.DeviceArticleId = brandArticleImage.ArticleID;
						deviceVM.Phone.ColourID = brandArticleImage.ColourID;
						deviceVM.Phone.ModelID = brandArticleImage.ModelID;
						
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
			Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = deviceVM.OrderSummary = ConstructOrderSummary(deviceVM.PackageId, deviceVM.ModelId, deviceVM.VasComponents, deviceVM.VasNames, null, null);
            Session["model"] = deviceVM;
            dropObj.mainFlow.PkgPgmBdlPkgCompID = dropObj.mainFlow.orderSummary.SelectedPgmBdlPkgCompID;
            DropFourHelpers.UpdatePhone(dropObj, deviceVM.Phone);
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            return RedirectToAction(GetRegActionStep_AddContract(deviceVM.Phone.TabNumber));
        }
        
        /// <summary>
        /// Gets the available device options.
        /// </summary>
        /// <param name="accountType">Account Type</param>
        /// <param name="contractType">Contract Type</param>
        /// <param name="package">Package Id</param>
        /// <returns></returns>
        private DeviceVM GetAvailableDeviceOptions(string accountType, string contractType, string package)
        {

            var deviceOptVM = new DeviceVM();

            if (accountType != null)
            {
                if (contractType.ToUpper() == "ASSIGNNEW" || contractType.ToUpper() == "TERMINATE")
                {
                    using (var proxy = new CatalogServiceProxy())
                    {
                        List<Brand> brandIDs = new List<Brand>();

                        if (accountType == "S")
                            brandIDs = proxy.GetBrandsForPackage(package, "SP");
                        else
                            brandIDs = proxy.GetBrandsForPackage(package, "CP");

                        deviceOptVM.brands = brandIDs.Select(c => c.Code).ToList();
                        deviceOptVM.DeviceOption.AvailableBrands = brandIDs.ToList();
                    }

                }
                else if (contractType.ToUpper() == "EXTEND")
                {
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var brandIDs = proxy.BrandFind(new BrandFind()
                        {
                            Brand = new Brand() { },
                            Active = true
                        });

                        deviceOptVM.DeviceOption.AvailableBrands = proxy.BrandGet(brandIDs).ToList();
                        deviceOptVM.brands = deviceOptVM.DeviceOption.AvailableBrands.Select(c => c.Code).ToList();
                    }
                    deviceOptVM.brands = GetExtendContracts();
                }

            }

            return deviceOptVM;
        }
        
        /// <summary>
        /// Get available devices.
        /// </summary>
        /// <param name="contractType">contract type</param>
        /// <param name="accountType">Account type</param>
        /// <param name="selectedOptionType"></param>
        /// <param name="selectedOptionId"></param>
        /// <param name="packageId"></param>
        /// <returns></returns>
        private PhoneVM GetAvailableModelImage(string contractType, string accountType, string selectedOptionType, string selectedOptionId, string packageId, string planId)
        {
            var modelIDs = new List<int>();
            var models = new List<Model>();
            var phoneVM = new PhoneVM();
            var articleID = new List<BrandArticle>();
            var availableModelImages = new List<AvailableModelImage>();
			List<string> listArticleID = new List<string>();

            // select all device images
            using (var proxy = new CatalogServiceProxy())
            {
                if (selectedOptionType.ToInt() == (int)MobileRegDeviceOption.Brand)
                {
                    if (contractType.ToUpper() == "TERMINATE")
                    {
                        if (accountType == "S")
                            models = proxy.GetModelsForPackage(selectedOptionId.ToInt(), packageId, "SP").ToList();
                        else
                            models = proxy.GetModelsForPackage(selectedOptionId.ToInt(), packageId, "CP").ToList();

                        modelIDs = models.Select(c => c.ID).ToList();
                    }

                    else
                    {
                        if (accountType == "S")
                            models = proxy.GetModelsForExtenstionForPackage(selectedOptionId.ToInt(), packageId, "SP");
                        else
                            models = proxy.GetModelsForExtenstionForPackage(selectedOptionId.ToInt(), packageId, "CP");

                        modelIDs = models.Select(c => c.ID).ToList();
                    }
                }

                #region Code for geting the count from the service by using dynamic proxy and geting the article id
                List<ClassArticleId> lstarticleId = new List<ClassArticleId>();
				Dictionary<string, int> stockCount = new Dictionary<string, int>();

                int counter = 0;
                using (var catalogProxy = new CatalogServiceClient())
                {
                    for (int i = 0; i < modelIDs.Count; i++)
                    {
                        articleID = proxy.GetArticleID(modelIDs[i].ToInt()).ArticleIDs.ToList();

                        if (articleID.Count > 0)
                        {

                            for (int j = 0; j < articleID.Count; j++)
                            {
                                ClassArticleId ClassArticleId = new ClassArticleId()
                                {
                                    id = articleID[j].ID,
                                    articleId = articleID[j].ArticleID.Trim(),
                                    modelId = articleID[j].ModelID,
                                    imagePath = ReferenceEquals(articleID[j].ImagePath, null) ? string.Empty : articleID[j].ImagePath.ToString().Trim(),
                                    colourID = articleID[j].ColourID,

                                };
                                lstarticleId.Add(ClassArticleId);
                            }
                            counter++;
                        }
                    }
                    if (counter <= 0)
                    {
                        lstarticleId = null;
                    }
                }

                string wsdlUrl = string.Empty;
                if (Util.SessionAccess.User.Org.WSDLUrl != null)
                {
                    wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
                }
                int storeId = 0;
                string[] maxisStoreIds = ConfigurationManager.AppSettings["OrganizationsIds"].ToString2().Split(",".ToCharArray());
                if (!maxisStoreIds.Contains(Util.SessionAccess.User.Org.OrganisationId))
                {
                    storeId = Convert.ToInt32(Util.SessionAccess.User.Org.OrganisationId);
                }
                try
                {
                    if (!ReferenceEquals(lstarticleId, null))
                    {
                        using (var dsproxy = new DynamicServiceProxy())
                        {
							listArticleID = lstarticleId.Select(x => x.articleId).Distinct().ToList();
							if (!Util.isCDPUUser())
								stockCount = dsproxy.GetStockCounts(listArticleID, wsdlUrl, storeId);
                            //lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = (int)(dsproxy.GetStockCount(i.articleId, wsdlUrl,storeId)));
                        }
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogExceptions(this.GetType(), ex);
                    throw ex;
                }
                //END HERE

                string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
                List<lnkofferuomarticleprice> lstOffers = new List<lnkofferuomarticleprice>();
                ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();

                Lstvam = GetAvailablePackageComponents(planId.ToInt(), contractType, null, isMandatory: false, isDeviceFinancing: true);

                if (!ReferenceEquals(lstarticleId, null))
                {
                    foreach (var item in lstarticleId)
                    {
                        if (contractType.ToUpper() != "EXTEND")
                        {
                            lstOffers = new List<lnkofferuomarticleprice>();
                            
							// drop 5
							lstOffers = WebHelper.Instance.GetContractOffers(Lstvam, item.articleId, packageId, MOCStatus);

                            if (lstOffers.Count() > 0)
                            {
                                availableModelImages.Add(new AvailableModelImage()
                                {
                                    BrandArticle = new BrandArticle()
                                    {
                                        ID = item.id,
                                        ColourID = item.colourID,
                                        ImagePath = item.imagePath,
                                        ModelID = item.modelId,
                                        ArticleID = item.articleId,
                                    },
                                    ModelCode = models.Where(a => a.ID == item.modelId).SingleOrDefault().Code,
                                    //Count = item.count
									Count = !Util.isCDPUUser() ? (stockCount.ContainsKey(item.articleId) ? stockCount[item.articleId] : 0) : 100,
                                });
                            }
                        }
                        else
                        {
                            availableModelImages.Add(new AvailableModelImage()
                            {
                                BrandArticle = new BrandArticle()
                                {
                                    ID = item.id,
                                    ColourID = item.colourID,
                                    ImagePath = item.imagePath,
                                    ModelID = item.modelId,
                                    ArticleID = item.articleId,
                                },
                                ModelCode = models.Where(a => a.ID == item.modelId).SingleOrDefault().Code,
                                //Count = item.count
								Count = !Util.isCDPUUser() ? (stockCount.ContainsKey(item.articleId) ? stockCount[item.articleId] : 0) : 100,
                            });
                        }
                    }
                }
                #endregion
            }

            phoneVM.ModelImages = availableModelImages;

            #region 28052015 - Add flag for device financing eligibility - Lus
            retrieveAcctListByICResponse allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
            DeviceVM selectedsvc = (DeviceVM)Session["model"];
            //string SelectAccountDtls = Session["SelectAccountDtls"].ToString();
            string selectedmsisdn = selectedsvc.MSISDN;

            bool dfCustEligibility = false;
            dfCustEligibility = WebHelper.Instance.checkCustEligibilityDF(allCustomerDetails, selectedmsisdn);
			using (var catalogProxy = new CatalogServiceProxy())
			{
				List<DeviceFinancingInfo> dfProdEligibility = catalogProxy.GetDeviceFinancingInfoByArticleIDs(listArticleID);

				foreach (var device in phoneVM.ModelImages)
				{
					if (dfProdEligibility.Where(x => x.ArticleID == device.BrandArticle.ArticleID).Any())
					{
						if (dfCustEligibility)
						{
							device.dfEligibility = true;	// for eligibility flag based on Customer and Product rule
							device.dfdeviceEligibility = true;
						}
						else
						{
							device.dfdeviceEligibility = true; // for eligibility flag based on Product rule
						}
					}
				}
			}
            #endregion

            return phoneVM;
        }
        */
        #endregion

        /// <summary>
        /// Get available devices.
        /// </summary>
        /// <param name="contractType">contract type</param>
        /// <param name="accountType">Account type</param>
        /// <param name="selectedOptionType"></param>
        /// <param name="selectedOptionId"></param>
        /// <param name="packageId"></param>
        /// <returns></returns>
        private PhoneVM GetAvailableModelImage(DeviceVM deviceVm, int priceFrom = 0, int priceTo = 0, bool availableChecked = true, bool limitedStockChecked = true, bool sellingFastChecked = true, bool outOfStockChecked = false)
        {
            string contractType = deviceVm.ContractType;
            string packageId = deviceVm.Package;
            string planId = deviceVm.PackageId;

            var phoneVM = new PhoneVM();
            var modelList = new List<Model>();
            var colourList = new List<Colour>();
            var brandArticleList = new List<BrandArticle>();
            var availableModelImages = new List<AvailableModelImage>();
            var articleIDList = new List<string>();
            deviceVm.ContractList = new Dictionary<string,List<string>>();

            // select all device images
            using (var proxy = new CatalogServiceProxy())
            {
                modelList = MasterDataCache.Instance.Model.Models.ToList();
                colourList = MasterDataCache.Instance.Colour.Colours.ToList();
                brandArticleList = proxy.GetDeviceListForPackage(packageId, "32", "4").ToList();
                
                #region Code for geting the count from the service by using dynamic proxy and geting the article id
                Dictionary<string, int> stockCount = new Dictionary<string, int>();

                string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl.ToString2();
                int storeId = Convert.ToInt32(Util.SessionAccess.User.Org.OrganisationId);

                try
                {
                    if (brandArticleList.Count > 0)
                    {
                        using (var dsproxy = new DynamicServiceProxy())
                        {
                            articleIDList = brandArticleList.Select(x => x.ArticleID).Distinct().ToList();
							if (!Util.isCDPUUser())
                                stockCount = dsproxy.GetStockCounts(articleIDList, wsdlUrl, storeId);
                            //lstarticleId.Where(i => i.articleId == i.articleId).ToList().ForEach(i => i.count = (int)(dsproxy.GetStockCount(i.articleId, wsdlUrl,storeId)));
                        }
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogExceptions(this.GetType(), ex);
                    throw ex;
                }
                //END HERE

                string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
                List<lnkofferuomarticleprice> lstOffers = new List<lnkofferuomarticleprice>();
                ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
                var lstContracts = new List<string>();

                Lstvam = GetAvailablePackageComponents(planId.ToInt(), contractType, null, isMandatory: false, isDeviceFinancing: true);

                if (brandArticleList.Count > 0)
                {
                    foreach (var item in brandArticleList)
                    {
                        var tempModel = modelList.Where(a => a.ID == item.ModelID).SingleOrDefault();
                        var tempColour = colourList.Where(a => a.ID == item.ColourID).SingleOrDefault();

                        //Filter condition
                        if (deviceVm.inputBrandSelected > 0 && tempModel.BrandID != deviceVm.inputBrandSelected) continue;
                        if (!string.IsNullOrWhiteSpace(deviceVm.inputModelSelected) && tempModel.ID.ToString2() != deviceVm.inputModelSelected) continue;
                        if (deviceVm.inputCapacitySelected > 0 && tempModel.DeviceCapacityID != deviceVm.inputCapacitySelected) continue;
                        if (deviceVm.inputDeviceTypeSelected > 0 && tempModel.DeviceTypeID != deviceVm.inputDeviceTypeSelected) continue;

                        // drop 5
						lstOffers = new List<lnkofferuomarticleprice>();
                        lstOffers = WebHelper.Instance.GetContractOffers(Lstvam, item.ArticleID, packageId, MOCStatus);
                        lstContracts = proxy.GetContractsForArticleId(item.ArticleID, packageId, MOCStatus);

                        //Filter price
                        if ((priceFrom == 0 && priceTo > 0) || priceFrom > 0)
                            lstOffers = lstOffers.Where(x => x.Price <= priceTo && x.Price >= priceFrom).ToList();

                        if (lstOffers.Count() > 0)
                        {
                            availableModelImages.Add(new AvailableModelImage()
                            {
                                BrandArticle = item,
                                ModelCode = tempModel.Code,
                                ColorDescription = tempColour.Description,
                                RetailPrice = tempModel.RetailPrice ?? 0,
                                Count = !Util.isCDPUUser() ? (stockCount.ContainsKey(item.ArticleID) ? stockCount[item.ArticleID] : 0) : 100,
                            });

                            if (lstContracts.Count > 0)
                                //Dictinary<articleid, List<contractduration>>
                                deviceVm.ContractList.Add(item.ArticleID, Lstvam.ContractVASes.Where(a => string.Join(",", lstContracts).Contains(a.KenanCode)).GroupBy(c => c.Value).Select(b => b.Key).ToList());
                        }
                    }
                }
                #endregion

                phoneVM.ModelImages = WebHelper.Instance.checkStockLevel(availableModelImages, availableChecked, limitedStockChecked, sellingFastChecked, outOfStockChecked);
                
                #region 28052015 - Add flag for device financing eligibility - Lus
                retrieveAcctListByICResponse allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
                DeviceVM selectedsvc = (DeviceVM)Session["model"];
                //string SelectAccountDtls = Session["SelectAccountDtls"].ToString();
                string selectedmsisdn = selectedsvc.MSISDN;

                bool dfCustEligibility = false;
                dfCustEligibility = WebHelper.Instance.checkCustEligibilityDF(allCustomerDetails, selectedmsisdn);
                List<DeviceFinancingInfo> dfProdEligibility = proxy.GetDeviceFinancingInfoByArticleIDs(articleIDList);

			    foreach (var device in phoneVM.ModelImages)
			    {
				    if (dfProdEligibility.Where(x => x.ArticleID == device.BrandArticle.ArticleID).Any())
				    {
					    if (dfCustEligibility)
					    {
						    device.dfEligibility = true;	// for eligibility flag based on Customer and Product rule
						    device.dfdeviceEligibility = true;
					    }
					    else
					    {
						    device.dfdeviceEligibility = true; // for eligibility flag based on Product rule
					    }
				    }
			    }
                #endregion

            }
            return phoneVM;
        }

        /// <summary>
        /// Gets the steps for Add Contract.
        /// </summary>
        /// <param name="stepNo">stepno.</param>
        /// <returns></returns>
        private string GetRegActionStep_AddContract(int stepNo)
        {
            if ((int)AddContractRegSteps.AccountList == stepNo)
            {
                return "Index";
            }
            if ((int)AddContractRegSteps.ContractCheck == stepNo)
            {
                return "ContractCheck";
            }
            if ((int)AddContractRegSteps.DevicePlan == stepNo)
            {
                return "SelectDevice";
            }
            else if ((int)AddContractRegSteps.Accessory == stepNo)
            {
                return "SelectAccessory";
            }
            else if ((int)AddContractRegSteps.VAS == stepNo)
            {
                return "SelectVAS";
            }
            else if ((int)AddContractRegSteps.PersonalDetails == stepNo)
            {
                return "PersonalDetails";
            }
            else if ((int)AddContractRegSteps.Summary == stepNo)
            {
                return "MobileRegSummary";
            }

            return "";
        }

        /// <summary>
        /// Gets the extended contracts.
        /// </summary>
        /// <returns></returns>
        private List<string> GetExtendContracts()
        {
            using (var proxy = new CatalogServiceProxy())
            {
                ContractBundleModels model = new ContractBundleModels();
                IEnumerable<string> list = proxy.ExtendContract(model);
                return list.ToList();
            }
        }

        /// <summary>
        /// Constructing the order summary.
        /// </summary>
        /// <param name="packageId">PackageId</param>
        /// <param name="brandArticleId">Brand Article Id</param>
        /// <param name="vasComponents">VAS components</param>
        /// <param name="vasNames"></param>
        /// <param name="contractId">Contract Id</param>
        /// <param name="penality">Penality</param>
        /// <param name="fromSubline">from subline or not.</param>
        /// <returns></returns>
        private OrderSummaryVM ConstructOrderSummary(string packageId, string brandArticleId, string vasComponents, string vasNames, string contractId, string penality, bool fromSubline = false, bool showVASAliasName = true)
        {
            Session["RegMobileReg_SelectedModelImageID"] = brandArticleId;
            Session["RegMobileReg_PkgPgmBdlPkgCompID"] = packageId;
            Session["RegMobileReg_ContractID"] = contractId;
            Session["RegMobileReg_VasIDs"] = vasComponents;
            if (!string.IsNullOrEmpty(penality))
                Session["Penalty"] = penality;

            if (vasNames != null)
                Session[SessionKey.RegMobileReg_VasNames.ToString()] = vasNames.Split(',').ToList();

            Session["Condition"] = "1";

            if (!string.IsNullOrEmpty(brandArticleId))
                Session["RegMobileReg_Type"] = 1;

            OrderSummaryVM SummaryData = WebHelper.Instance.ConstructOrderSummary(string.Empty, this.GetType().Name, fromSubline, showVASAliasName: showVASAliasName);

            if (SummaryData.VasVM != null && SummaryData.VasVM.VASesName != null)
                Session["RegMobileReg_SelectedContracts"] = SummaryData.VasVM.VASesName.Distinct().ToList();
            return SummaryData;
        }

        /// <summary>
        /// Gets the customer details.
        /// </summary>
        /// <returns></returns>
        private PersonalDetailsVM GetPersonalDetails()
        {
            PersonalDetailsVM personalDetails = new PersonalDetailsVM();
            retrieveAcctListByICResponse AcctListByICResponse = null;
            CustomizedCustomer CustomerPersonalInfo = null;
            if (!ReferenceEquals(Session["PPIDInfo"], null))
            {
                ///CHECK IF EXISTING USER THEN ONLY RETRIEVE CUSTOMER INFORMATION
                AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                ///CHECK IF CUSTOMER INFORMATION IS THERE OR NOT
                if (!ReferenceEquals(AcctListByICResponse, null) && !ReferenceEquals(AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList(), null))
                {
                    // CustomerPersonalInfo = AcctListByICResponse.itemList.Where(c => c.Customer != null).ToList().FirstOrDefault().Customer;
                    CustomerPersonalInfo = Util.GetCustomerList(AcctListByICResponse, Session["ExternalID"] != null ? Session["ExternalID"].ToString() : "0");
                }
            }
            if (CustomerPersonalInfo != null)
            {
                //if (CustomerPersonalInfo.Title != "")
                if (!string.IsNullOrWhiteSpace(CustomerPersonalInfo.Title))
                {
                    if (CustomerPersonalInfo.CustomerTitleID != null)
                    {
                        Online.Registration.Web.RegistrationSvc.RegistrationServiceClient client = new Online.Registration.Web.RegistrationSvc.RegistrationServiceClient();
                        int titleID = client.GetIdByTitleName(CustomerPersonalInfo.Title);
                        personalDetails.Customer.CustomerTitleID = titleID;
                        
                    }
                }
                else
                {
                    personalDetails.Customer.CustomerTitleID = 0;
                }
                personalDetails.Customer.FullName = CustomerPersonalInfo.CustomerName;

				if (!string.IsNullOrEmpty(CustomerPersonalInfo.Cbr))
                {
					CustomerPersonalInfo.Cbr = Util.FormatContactNumber(CustomerPersonalInfo.Cbr);

                }
                else
                {
                    CustomerPersonalInfo.Cbr = Session["MSISDN"].ToString2();
                }

                personalDetails.Customer.ContactNo = CustomerPersonalInfo.Cbr.ToString2();

                var idCardTypeID = Session["IDCardTypeID"] != null ? Session["IDCardTypeID"].ToString() : null;
                personalDetails.Customer.IDCardTypeID = idCardTypeID.ToInt();

                personalDetails.Customer.IDCardNo = CustomerPersonalInfo.NewIC != string.Empty ? CustomerPersonalInfo.NewIC : CustomerPersonalInfo.OldIC;
                if ((CustomerPersonalInfo.NewIC).ToString2() != string.Empty)
                {
                    personalDetails.Customer.IDCardNo = CustomerPersonalInfo.NewIC;
                }
                else if ((CustomerPersonalInfo.OldIC).ToString2() != string.Empty)
                {
                    personalDetails.Customer.IDCardNo = CustomerPersonalInfo.OldIC;
                }
                else if ((CustomerPersonalInfo.PassportNo).ToString2() != string.Empty)
                {
                    personalDetails.Customer.IDCardNo = CustomerPersonalInfo.PassportNo;
                }
                else if ((CustomerPersonalInfo.OtherIC).ToString2() != string.Empty)
                {
                    personalDetails.Customer.IDCardNo = CustomerPersonalInfo.OtherIC;
                }
                personalDetails.Customer.NationalityID = (!string.IsNullOrEmpty(CustomerPersonalInfo.NewIC)) ? 1 : 132;
                if (CustomerPersonalInfo.Dob.Length >= 8)
                {
                    personalDetails.DOBDay = (CustomerPersonalInfo.Dob.Substring(6, 2)).ToInt();
                    personalDetails.DOBMonth = (CustomerPersonalInfo.Dob.Substring(4, 2)).ToInt();
                    personalDetails.DOBYear = (CustomerPersonalInfo.Dob.Substring(0, 4)).ToInt();


                    CustomerPersonalInfo.DateOfBirth = new DateTime(personalDetails.DOBYear, personalDetails.DOBMonth, personalDetails.DOBDay);
                    if (personalDetails.Customer != null)
                    {
                        personalDetails.Customer.DateOfBirth = CustomerPersonalInfo.DateOfBirth;
                    }
                }
                personalDetails.Address.Line1 = CustomerPersonalInfo.Address1;
                personalDetails.Address.Line2 = CustomerPersonalInfo.Address2;
                personalDetails.Address.Line3 = CustomerPersonalInfo.Address3;
                personalDetails.Address.Postcode = CustomerPersonalInfo.PostCode;
                personalDetails.Address.Town = CustomerPersonalInfo.City;
             
       
                if (!string.IsNullOrEmpty(CustomerPersonalInfo.State))
                {
                    List<SelectListItem> objStateList = Util.GetList(RefType.State);
                    List<SelectListItem> curStateId = objStateList.Where(e => e.Text == CustomerPersonalInfo.State).ToList();
                    if (curStateId.Count() > 0)
                    {
                        personalDetails.Address.StateID = Convert.ToInt32(curStateId[0].Value);
                    }
                    else
                    {
                        personalDetails.Address.StateID = 20;
                    }
                }
                else
                {
                    personalDetails.Address.StateID = 20;
                }
            }
            return personalDetails;
        }

        private ValueAddedServicesVM GetAvailablePackageComponents(int pbpcID, string contractType, string modelId, bool fromSubline = false, bool isMandatory = false, bool isDeviceFinancing = false)
        {
            var vasVM = new ValueAddedServicesVM();
            var retrievedVases = new List<AvailableVAS>();
            var contractVases = new List<AvailableVAS>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var components = new List<Component>();
            var componentTypes = new List<ComponentType>();

            using (var proxy = new CatalogServiceProxy())
            {
                var packageID = 0;
                if (MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }) != null)
                    packageID = (pbpcID != null && pbpcID != 0) ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().ChildID : 0;
                if (packageID != 0)
                {
                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            LinkType = Properties.Settings.Default.Package_Component,
                            ParentID = packageID,
                        },
                        IsMandatory = isMandatory,
                        Active = true
                    })).ToList();
					if (isDeviceFinancing)
					{
						var pbpcAdd = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
						{
							PgmBdlPckComponent = new PgmBdlPckComponent()
							{
								LinkType = Properties.Settings.Default.Device_Financing_Component
							},
							IsMandatory = isMandatory,
							Active = true
						})).ToList();

						pgmBdlPckComponents.AddRange(pbpcAdd);
					}
                }
                if (pgmBdlPckComponents.Count() > 0)
                {
                    if (contractType != null)
                    {

                        if (contractType.ToUpper() == "EXTEND")
                        {
                            OrderSummaryVM orderVM = new OrderSummaryVM();
                            if ((OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()] != null)
                                orderVM = (OrderSummaryVM)Session[SessionKey.RegMobileReg_OrderSummary.ToString()];
                            if (Session["Value"].ToInt() == 1)
                            {
                                pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.contract_type != "E").ToList();
                                Session["Value"] = 0;
                            }
                            else
                            {
                                pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.contract_type == "E").ToList();
                                if (modelId != null)
                                {
                                    string[] mobileIDs = orderVM.ModelID.ToString2().Split(',');

                                    if (mobileIDs.Length > 0 && mobileIDs[0].ToString() == "0")
                                        mobileIDs[0] = orderVM.ModelID.ToString();


                                    for (int mobIDCnt = 0; mobIDCnt < mobileIDs.Length; mobIDCnt++)
                                    {
                                        pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.ModelId != null).ToList();
                                        pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.ModelId.Contains(mobileIDs[mobIDCnt])).ToList();
                                    }
                                }
                            }
                        }
                        else
                        {
                            pgmBdlPckComponents = pgmBdlPckComponents.Where(a => a.contract_type != "E").ToList();
                        }
                    }
                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();

                    componentTypes = proxy.ComponentTypeGet(components.Select(a => a.ComponentTypeID).Distinct()).ToList();
                }
            }

            foreach (var pbpc in pgmBdlPckComponents)
            {
                var component = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault();
                var compTypeCode = componentTypes.Where(a => a.ID == component.ComponentTypeID).SingleOrDefault().Code;

                if (compTypeCode == Properties.Settings.Default.CompType_DataCon || compTypeCode == Properties.Settings.Default.CompType_VoiceCon || (compTypeCode == Properties.Settings.Default.Device_Financing_Component && isDeviceFinancing) )
                {
                    contractVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        KenanCode = pbpc.KenanCode
                    });
                }
                else
                {
					if ((compTypeCode == Properties.Settings.Default.Device_Financing_Component && !isDeviceFinancing))
					{
						//
					}
					else {
						retrievedVases.Add(new AvailableVAS
						{
							PgmBdlPkgCompID = pbpc.ID,
							ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
							Value = pbpc.Value.ToString2(),
							Price = pbpc.Price
						});
					}
                }
            }

            if (isMandatory)
            {
                vasVM.MandatoryVASes = retrievedVases;
            }
            else
            {
                vasVM.AvailableVASes = retrievedVases;
            }

            //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
            if (Session[SessionKey.ArticleId.ToString()] != null)
            {
                if (contractVases != null && contractVases.Count > 0)
                {
                    string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;

                    List<string> objContracts = new List<string>();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var kenanPackageID = (pbpcID != null && pbpcID != 0) ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().KenanCode : string.Empty;

                        objContracts = proxy.GetContractsForArticleId(Session[SessionKey.ArticleId.ToString()].ToString(), kenanPackageID, MOCStatus);


                    }
                    if (contractType != null)
                    {
                        if (contractType.ToUpper() != "EXTEND")
                        {
                            if (objContracts != null && objContracts.Count > 0)
                                vasVM.ContractVASes = contractVases.Where(a => string.Join(",", objContracts).Contains(a.KenanCode)).ToList();
                        }
                        else
                            vasVM.ContractVASes = contractVases;
                    }
                    else
                        vasVM.ContractVASes = contractVases;
                }
                else
                    vasVM.ContractVASes = contractVases;
            }
            else
                vasVM.ContractVASes = contractVases;
            //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid

            vasVM.SelectedVasIDs = fromSubline ? Session["RegMobileSub_VasIDs"].ToString2() : Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2();

            return vasVM;
        }

        [HttpPost]
        public string ValidateRRPandMDP(string ModelId, int MDPid)
        {
            var resp = new BusinessRuleResponse();
            List<int> Finalintvalues = null;

            try
            {
                using (var proxy2 = new CatalogServiceProxy())
                {
                    List<PkgDataPlanId> values = proxy2.getFilterplanvalues(1).values.ToList();
                    Finalintvalues = values.Where(mid => mid.Modelid == ModelId).Select(a => a.BdlDataPkgId).ToList();
                }

                if (Finalintvalues.Contains(MDPid))
                {
                    resp.ResultMessage = "1";
                }
                else
                {
                    resp.ResultMessage = "0";
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                resp.ResultMessage = "0";
                throw ex;
            }

            return resp.ResultMessage.ToString2();
        }

        /// <summary>
        /// Doing bussiness validation checks.
        /// </summary>
        /// <param name="personalDetailsVM"></param>
        private void BusinessValidationChecks(PersonalDetailsVM personalDetailsVM)
        {
            string idCardNo = string.Empty;
            string idCardTypeID = string.Empty;
            string dob = string.Empty;
            string state = string.Empty;
            string postalCode = string.Empty;
            int? GeneratedID = null;

            var resp = new BusinessRuleResponse();
            if (!ReferenceEquals(personalDetailsVM, null))
            {
                idCardNo = personalDetailsVM.Customer.IDCardNo;
                idCardTypeID = Convert.ToString(personalDetailsVM.Customer.IDCardTypeID);

                dob = personalDetailsVM.DOBYear.ToString() + "";
                if (personalDetailsVM.DOBMonth < 10)
                {
                    dob = dob + "0" + personalDetailsVM.DOBMonth.ToString() + "";
                }
                else
                {
                    dob = dob + personalDetailsVM.DOBMonth.ToString() + "";
                }
                if (personalDetailsVM.DOBDay < 10)
                {
                    dob = dob + "0" + personalDetailsVM.DOBDay.ToString() + "";
                }
                else
                {
                    dob = dob + personalDetailsVM.DOBDay.ToString() + "";
                }
                state = Util.GetNameByID(RefType.State, personalDetailsVM.Address.StateID).ToString();
                postalCode = personalDetailsVM.Address.Postcode.ToString();



                if (!ReferenceEquals(Session[SessionKey.PPID.ToString()], null) && !string.IsNullOrWhiteSpace(Convert.ToString(Session[SessionKey.PPID.ToString()])))
                {
                    try
                    {
                        var idCardType = new IDCardType();

                        using (var proxy = new RegistrationServiceProxy())
                        {
                            idCardType = proxy.IDCardTypeGet(idCardTypeID.ToInt());
                        }
                        //For new users checking
                        if (Session[SessionKey.PPID.ToString()].ToString() == "N")
                        {
                            #region Age Check

                            try
                            {
                                var req = new BusinessRuleRequest();
                                req.RuleNames = "AgeCheck".Split(' ');
                                req.DateOfBirth = dob;
                                req.IDCardNo = idCardNo;
                                req.IDCardType = idCardType.KenanCode;
                                DateTime dtReq = DateTime.Now;

                                //AgeCheck
                                resp = BREChecker.validate(req);

                                DateTime dtRes = DateTime.Now;
                                string xmlReq = Util.ConvertObjectToXml(req);
                                string xmlRes = Util.ConvertObjectToXml(resp);

                                SetAgeSession(resp);
                                personalDetailsVM.AgeCheckStatus = Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]);

                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                            }
                            catch (Exception ex)
                            {
                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);
                                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()] = "AE";
                                personalDetailsVM.AgeCheckStatus = "AE";
                            }

                            #endregion

                            #region DDMFCheck
                            try
                            {
                                //DDMFCheck
                                var req = new BusinessRuleRequest();
                                req.RuleNames = "DDMFCheck".Split(' ');
                                req.IDCardNo = idCardNo;
                                req.IDCardType = idCardType.KenanCode;
                                DateTime dtReq = DateTime.Now;

                                resp = BREChecker.validate(req);

                                DateTime dtRes = DateTime.Now;
                                string xmlReq = Util.ConvertObjectToXml(req);
                                string xmlRes = Util.ConvertObjectToXml(resp);

                                SetDDMFSession(resp);
                                personalDetailsVM.DDMFCheckStatus = Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]);

                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                            }
                            catch (Exception ex)
                            {
                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);
                                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()] = "DE";
                                personalDetailsVM.DDMFCheckStatus = "DE";
                            }
                            #endregion

                            #region AddressCheck
                            try
                            {
                                var req = new BusinessRuleRequest();
                                req.RuleNames = "AddressCheck".Split(' ');
                                req.IDCardNo = idCardNo;
                                req.IDCardType = idCardType.KenanCode;
                                req.state = state;
                                req.postalCode = postalCode;
                                DateTime dtReq = DateTime.Now;

                                //AddressCheck
                                resp = BREChecker.validate(req);

                                DateTime dtRes = DateTime.Now;
                                string xmlReq = Util.ConvertObjectToXml(req);
                                string xmlRes = Util.ConvertObjectToXml(resp);

                                SetAddressSession(resp);
                                personalDetailsVM.AddressCheckStatus = Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]);

                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                            }
                            catch (Exception ex)
                            {
                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);
                                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()] = "ADE";
                                personalDetailsVM.AddressCheckStatus = "ADE";
                            }
                            #endregion
                        }
                        else if (Session[SessionKey.PPID.ToString()].ToString() == "E")
                        {
                            //For existing users checking
                            // 06052015 Remove Age check, DDMF Check, and Address check for existing customer - Lus
                            /*#region Age Check
                            try
                            {
                                var req = new BusinessRuleRequest();
                                req.RuleNames = "AgeCheck".Split(' ');
                                req.DateOfBirth = dob;
                                req.IDCardNo = idCardNo;
                                req.IDCardType = idCardType.KenanCode;
                                DateTime dtReq = DateTime.Now;

                                //AgeCheck
                                resp = BREChecker.validate(req);

                                DateTime dtRes = DateTime.Now;
                                string xmlReq = Util.ConvertObjectToXml(req);
                                string xmlRes = Util.ConvertObjectToXml(resp);

                                SetAgeSession(resp);
                                personalDetailsVM.AgeCheckStatus = Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]);

                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                            }
                            catch (Exception ex)
                            {
                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);
                                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()] = "AE";
                                personalDetailsVM.AgeCheckStatus = "AE";
                            }
                            #endregion

                            #region Address Check
                            try
                            {
                                var req = new BusinessRuleRequest();
                                req.RuleNames = "AddressCheck".Split(' ');
                                req.IDCardNo = idCardNo;
                                req.IDCardType = idCardType.KenanCode;
                                req.state = state;
                                req.postalCode = postalCode;
                                DateTime dtReq = DateTime.Now;

                                //AddressCheck
                                resp = BREChecker.validate(req);

                                DateTime dtRes = DateTime.Now;
                                string xmlReq = Util.ConvertObjectToXml(req);
                                string xmlRes = Util.ConvertObjectToXml(resp);

                                SetAddressSession(resp);
                                personalDetailsVM.AddressCheckStatus = Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]);

                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                            }
                            catch (Exception ex)
                            {
                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);
                                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()] = "ADE";
                                personalDetailsVM.AddressCheckStatus = "ADE";
                            }
                            #endregion

                            #region DDMF Check
                            try
                            {
                                //DDMFCheck
                                var req = new BusinessRuleRequest();
                                req.RuleNames = "DDMFCheck".Split(' ');
                                req.IDCardNo = idCardNo;
                                req.IDCardType = idCardType.KenanCode;
                                DateTime dtReq = DateTime.Now;
                                
                                //DDMFCheck
                                resp = BREChecker.validate(req);

                                DateTime dtRes = DateTime.Now;
                                string xmlReq = Util.ConvertObjectToXml(req);
                                string xmlRes = Util.ConvertObjectToXml(resp);

                                SetDDMFSession(resp);
                                personalDetailsVM.DDMFCheckStatus = Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]);

                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                            }
                            catch (Exception ex)
                            {
                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);
                                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()] = "DE";
                                personalDetailsVM.DDMFCheckStatus = "DE";
                            }
                            #endregion */

                            #region TotalLine Check
                            try
                            {
                                var req = new BusinessRuleRequest();
                                req.RuleNames = "TotalLineCheck".Split(' ');
                                req.totalLinesAllowed = ConfigurationManager.AppSettings["totalLinesAllowed"].ToString();
                                req.IDCardNo = idCardNo;
                                req.IDCardType = idCardType.KenanCode;
                                DateTime dtReq = DateTime.Now;

                                //TotalLineCheck 9 allowed
                                resp = BREChecker.validate(req);

                                DateTime dtRes = DateTime.Now;
                                string xmlReq = Util.ConvertObjectToXml(req);
                                string xmlRes = Util.ConvertObjectToXml(resp);

                                //Set sessions
                                SetTotalLineCheckSession(resp);
                                personalDetailsVM.TotalLineCheckStatus = Convert.ToString(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()]);

                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                            }
                            catch (Exception ex)
                            {
                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);
                                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()] = "TLE";
                                personalDetailsVM.TotalLineCheckStatus = "TLE";
                            }
                            #endregion

                            /* 06052015 Remove Principle Line check for existing customer - Lus
                            #region Principal Line Check
                            try
                            {
                                var req = new BusinessRuleRequest();
                                req.RuleNames = "PrincipleLineCheck".Split(' ');
                                req.totalPrincipleLinesAllowed = ConfigurationManager.AppSettings["totalPrincipleLinesAllowed"].ToString();
                                req.IDCardNo = idCardNo;
                                req.IDCardType = idCardType.KenanCode;
                                DateTime dtReq = DateTime.Now;

                                //PrincipleLineCheck 5 allowed
                                resp = BREChecker.validate(req);

                                DateTime dtRes = DateTime.Now;
                                string xmlReq = Util.ConvertObjectToXml(req);
                                string xmlRes = Util.ConvertObjectToXml(resp);

                                SetPrincipalLineCheckSession(resp);
                                personalDetailsVM.PrincipleLineCheckStatus = Convert.ToString(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()]);

                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);

                            }
                            catch (Exception ex)
                            {
                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);
                                Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()] = "PLE";
                                personalDetailsVM.PrincipleLineCheckStatus = "PLE";
                                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                            }
                            #endregion */

                            #region Contract Check
                            //Contract Check
                            try
                            {
                                var req = new BusinessRuleRequest();
                                req.RuleNames = "ContractCheck".Split(' ');
                                //req.totalContractsAllowed = ConfigurationManager.AppSettings["totalContractsAllowed"].ToString();//30042015 - Anthony - To count the total contract based on customer tenure
                                req.totalContractsAllowed = (WebHelper.Instance.checkTotalContract().ToInt() + 1).ToString2();//30042015 - Anthony - To count the total contract based on customer tenure
                                req.IDCardNo = idCardNo;
                                req.IDCardType = idCardType.KenanCode;
                                DateTime dtReq = DateTime.Now;

                                //ContractCheck 2 allowed
                                resp = BREChecker.validate(req);

                                DateTime dtRes = DateTime.Now;
                                string xmlReq = Util.ConvertObjectToXml(req);
                                string xmlRes = Util.ConvertObjectToXml(resp);

                                SetContractCheckSession(resp);
                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Success", xmlReq: xmlReq, xmlRes: xmlRes, idCardNo: idCardNo, dtReq: dtReq, dtRes: dtRes);
                            }
                            catch (Exception ex)
                            {
                                LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);
                                Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()] = "CE";
                                personalDetailsVM.ContractCheckStatus = "CE";
                                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                            }
                            #endregion

                        }
                    }
                    catch (Exception ex)
                    {
                        WebHelper.Instance.LogExceptions(this.GetType(), ex);
                        LogService(APIname: "KenanServiceProxy", LastGeneratedID: out GeneratedID, MethodName: "checkBusinessRules", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);

                    }

                }
            }

        }

        private void LogService(string APIname, string MethodName, out int? LastGeneratedID, bool IsBefore = true, int? ID = 0, string status = "", string xmlReq = "", string xmlRes = "", string idCardNo = "", System.DateTime? dtReq = null, System.DateTime? dtRes = null)
        {
            APISaveCallStatusResp responseFromBusinessValidation = null;
            LastGeneratedID = null;

            string currentLoggedInUserName = string.Empty;

            //Get username from cookie
            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
            {
                currentLoggedInUserName = Request.Cookies["CookieUser"].Value;
            }
            else
            {
                return;
            }

            if (IsBefore)
            {
                using (var proxy = new UserServiceProxy())
                {
                    responseFromBusinessValidation = new APISaveCallStatusResp();

                    responseFromBusinessValidation = proxy.APISaveCallStatus(new APISaveCallStatusReq
                    {
                        tblAPICallStatusMessagesval = new tblAPICallStatus
                        {
                            APIName = APIname,
                            MethodName = MethodName,
                            RequestTime = (dtReq == null ? System.DateTime.Now : Convert.ToDateTime(dtReq)),
                            UserName = currentLoggedInUserName,
                            BreXmlReq = xmlReq,
                            BreXmlRes = xmlRes,
                            IdCardNo = idCardNo,
                            ResponseTime = (dtRes == null ? System.DateTime.Now : Convert.ToDateTime(dtRes)),
                            Status = status
                        }
                    });
                }
                LastGeneratedID = responseFromBusinessValidation.APIID;
            }
            else
            {
                using (var proxy = new UserServiceProxy())
                {
                    var response = proxy.APIUpdateCallStatus(new APISaveCallStatusReq
                    {
                        tblAPICallStatusMessagesval = new tblAPICallStatus
                        {
                            ID = ID == null ? 0 : (int)ID,
                            ResponseTime = DateTime.Now,
                            Status = status
                        }
                    });
                }
            }

        }

        /// <summary>
        /// Validating card details.
        /// </summary>
        /// <param name="customer">customer information</param>
        /// <returns></returns>
        private bool ValidateCardDetails(Customer customer)
        {
            if (!IsCardDetailsEntered(customer))
            {
                ModelState.AddModelError(string.Empty, "Please enter all Card details.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Verify for the card details.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        private bool IsCardDetailsEntered(Customer customer)
        {
            if (!customer.CardTypeID.HasValue || customer.CardTypeID.Value <= 0)
                return false;

            if (string.IsNullOrEmpty(customer.CardNo))
                return false;

            if (string.IsNullOrEmpty(customer.CardExpiryDate))
                return false;

            if (string.IsNullOrEmpty(customer.NameOnCard))
                return false;

            return true;
        }

        private void ConstructSuppLineSummary(List<RegSuppLine> suppLines, List<RegPgmBdlPkgComp> regPBPCs)
        {
            var strVAS = "";
            var MSISDNs = new List<string>();
            var sublineVM = new List<SublineVM>();
            var suppLinePBPCs = new List<PgmBdlPckComponent>();

            using (var proxy = new CatalogServiceProxy())
            {
                suppLinePBPCs = MasterDataCache.Instance.FilterComponents(regPBPCs.Select(a => a.PgmBdlPckComponentID).ToList()).ToList();
            }

            foreach (var suppLine in suppLines)
            {
                ClearSubLineSession();

                var pbpcIDs = regPBPCs.Where(a => a.RegSuppLineID == suppLine.ID).Select(a => a.PgmBdlPckComponentID).ToList();
                var PBPCs = suppLinePBPCs.Where(a => pbpcIDs.Contains(a.ID)).ToList();

                var bpCode = Properties.Settings.Default.Bundle_Package;
                var pcCode = Properties.Settings.Default.Package_Component;

                Session["RegMobileSub_PkgPgmBdlPkgCompID"] = suppLine.PgmBdlPckComponentID;

                var vasIDs = PBPCs.Where(a => a.LinkType == pcCode).Select(a => a.ID).ToList();
                if (vasIDs.Count() > 0)
                {
                    strVAS = "";
                    foreach (var vasID in vasIDs)
                    {
                        strVAS = strVAS + vasID + ",";
                    }
                }
                Session["RegMobileSub_VasIDs"] = strVAS;

                if (!string.IsNullOrEmpty(suppLine.MSISDN1) || !string.IsNullOrEmpty(suppLine.MSISDN1))
                {
                    MSISDNs = new List<string>();
                    MSISDNs.Add(suppLine.MSISDN1);
                    MSISDNs.Add(suppLine.MSISDN2);
                }
                Session["RegMobileSub_MobileNo"] = MSISDNs;

                ConstructSubLineVM(false);
            }
        }

        /// <summary>
        /// Clear registration sessions.
        /// </summary>
        private void ClearRegistrationSession()
        {
            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
            Session["RegMobileReg_BiometricID"] = null;
            Session["RegMobileReg_SelectedModelID"] = null;
            Session[SessionKey.RegMobileReg_ContractID.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasNames.ToString()] = null;
            Session[SessionKey.RegMobileReg_MobileNo.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session[SessionKey.RegMobileReg_IDCardType.ToString()] = null;
            Session[SessionKey.RegMobileReg_SublineVM.ToString()] = null;
            Session[SessionKey.ArticleId.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session["RebatePenalty"] = null;
            Session["PrimarySim"] = null;
            Session[SessionKey.CDPU_onBehalf.ToString()] = string.Empty;
            ClearSubLineSession();
        }

        private void ClearSubLineSession()
        {
            Session["RegMobileSub_PkgPgmBdlPkgCompID"] = null;
            Session["RegMobileSub_MobileNo"] = null;
            Session["RegMobileSub_VasIDs"] = null;
        }

        private void ConstructSubLineVM(bool canRemove = true)
        {
            var package = new Package();
            var bundle = new Bundle();
            var sublineVMs = new List<SublineVM>();
            var vasNames = new List<string>();
            var selectedCompIDs = new List<int>();

            var vasIDs = Session["RegMobileSub_VasIDs"].ToString2().Split(',').ToList();
            foreach (var id in vasIDs)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    selectedCompIDs.Add(id.ToInt());
                }
            }

            using (var proxy = new CatalogServiceProxy())
            {
                var bundlePackage = MasterDataCache.Instance.FilterComponents(new int[] 
                    { 
                        (int)Session["RegMobileSub_PkgPgmBdlPkgCompID"] 
                    }).SingleOrDefault();

                package = proxy.PackageGet(new int[] 
                    { 
                        bundlePackage.ChildID 
                    }).SingleOrDefault();

                bundle = proxy.BundleGet(new int[] 
                    { 
                        bundlePackage.ParentID 
                    }).SingleOrDefault();

                vasNames = proxy.ComponentGet(
                    MasterDataCache.Instance.FilterComponents
                    (
                        selectedCompIDs
                    ).Select(a => a.ChildID)
                ).Select(a => a.Name).ToList();
            }

            var sessionSublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];
            if (sessionSublines != null)
            {
                if (sessionSublines.Count() > 0)
                    sublineVMs = sessionSublines;
            }

            sublineVMs.Add(new SublineVM()
            {
                CanRemove = canRemove,
                SequenceNo = sublineVMs.Count() + 1,
                PkgPgmBdlPkgCompID = (int)Session["RegMobileSub_PkgPgmBdlPkgCompID"],
                SelectedMobileNos = (List<string>)Session["RegMobileSub_MobileNo"],
                SelectedVasIDs = selectedCompIDs,
                SelectedVasNames = vasNames,
                BundleName = bundle.Name,
                PackageName = package.Name
            });

            Session[SessionKey.RegMobileReg_SublineVM.ToString()] = sublineVMs;
        }

        /// <summary>
        /// Constructing registration object.
        /// </summary>
        /// <param name="queueNo"></param>
        /// <param name="remarks"></param>
        /// <param name="ContractType"></param>
        /// <param name="AccountType"></param>
        /// <param name="MSISDN"></param>
        /// <param name="subscriberNo"></param>
        /// <param name="subscriberNoReset"></param>
        /// <param name="accountNo"></param>
        /// <param name="KenanAccountNo"></param>
        /// <param name="OfferId"></param>
        /// <param name="UOMCode"></param>
        /// <param name="penality"></param>
        /// <param name="signatureSVG"></param>
        /// <param name="custPhoto"></param>
        /// <param name="altCustPhoto"></param>
        /// <param name="photo"></param>
        /// <param name="isSimRequired"></param>
        /// <param name="SIMModelSelected"></param>
        /// <returns></returns>
        private DAL.Models.Registration ConstructRegistration(DeviceVM deviceVM,string queueNo, string remarks, string ContractType, string AccountType, string MSISDN, string subscriberNo, string subscriberNoReset, string accountNo, string KenanAccountNo, string OfferId, string UOMCode, string penality, string signatureSVG = "", string custPhoto = "", string altCustPhoto = "", string photo = "", string isSimRequired = "", string SIMModelSelected = "")
        {
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//10042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow - Start
            var regTypeID = 0;
            if (Session[SessionKey.RegMobileReg_Type.ToString()] != null)
            {
                Session[SessionKey.RegMobileReg_Type.ToString()] = (int)MobileRegType.Contract;
            }
            
                regTypeID = Util.GetRegTypeID(REGTYPE.AddContract);
            
            DAL.Models.Registration registration = new DAL.Models.Registration();

            //we set this always TRUE why because based on this we are storing the ApproveBlacklistCheck  in trnregistration this we are sending to kenan as sales code
            var isBlacklisted = true;
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            registration.K2_Status = Session[SessionKey.RegK2_Status.ToString()].ToString2();

            registration = new DAL.Models.Registration();
            registration.CenterOrgID = Util.SessionAccess.User.OrgID;
            registration.RegTypeID = regTypeID;
            registration.Remarks = remarks;
            registration.QueueNo = queueNo;
			registration.SignatureSVG = signatureSVG;
            registration.CustomerPhoto = string.Empty;
            registration.AltCustomerPhoto = string.Empty;
            registration.Photo = string.Empty;
            registration.AdditionalCharges = personalDetailsVM.Deposite;
            registration.InternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;
            registration.ExternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;

            // w.loon - Is Justification/Supervisor Approval Required
            string ListOfRequiredBreValidation = WebHelper.Instance.ListOfRequiredBreValidation()["ListOfRequiredBreValidation"];

            bool IsWriteOff = false;
            string acc = string.Empty;
            WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
            registration.WriteOffCheckStatus = ListOfRequiredBreValidation.Contains("Writeoff Check") ?
                                                    !IsWriteOff ? "WOS" : "WOF"
                                                    : "WON";
            registration.AgeCheckStatus = ListOfRequiredBreValidation.Contains("AgeCheck") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]) : "AF"
                                                    : "AN";
            registration.DDMFCheckStatus = ListOfRequiredBreValidation.Contains("DDMF Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]) : "DF"
                                                    : "DN";
            registration.AddressCheckStatus = ListOfRequiredBreValidation.Contains("Address Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]) : "ADF"
                                                    : "ADN";
            registration.OutStandingCheckStatus = ListOfRequiredBreValidation.Contains("Outstanding Credit Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()]) : "OF"
                                                    : "ON";
            registration.TotalLineCheckStatus = ListOfRequiredBreValidation.Contains("Total Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()]) : "TLF"
                                                    : "TLN";
            registration.PrincipleLineCheckStatus = ListOfRequiredBreValidation.Contains("Principle Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()]) : "PLF"
                                                    : "PLN";
            registration.BiometricVerify = ListOfRequiredBreValidation.Contains("Biometric") ?
                                                    ((Session["RegMobileReg_BiometricVerify"].ToBool() != null) ? (Session["RegMobileReg_BiometricVerify"].ToBool()) : false)
                                                    : false;
            registration.IsVerified = !ReferenceEquals(Session["RegMobileReg_BiometricVerify"], null) ? true : false;
            registration.ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
            registration.RFSalesDT = DateTime.Now;
            registration.MSISDN1 = MSISDN;
            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
            {
                registration.SalesPerson = Request.Cookies["CookieUser"].Value;
            }
            if (Session[SessionKey.OfferId.ToString()] != null)
                registration.OfferID = Convert.ToDecimal(Session[SessionKey.OfferId.ToString()]);
            else
                registration.OfferID = 0;
            // registration.OfferID = Convert.ToDecimal(OfferId);
            registration.externalId = MSISDN;

            //Added by VLT on 1st May 2013 to add K2Type in Registration
            using (var proxy = new CatalogServiceProxy())
            {
                string[] contractId = null;
                List<int> contractsList = new List<int>();
                List<Component> componentsList = new List<Component>();


                if (Session[SessionKey.RegMobileReg_ContractID.ToString()] != null)
                {
                    contractId = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string crtId in contractId)
                    {
                        contractsList.Add(Convert.ToInt32(crtId));
                    }


                    var vases = MasterDataCache.Instance.FilterComponents
                    (
                    (IEnumerable<int>)contractsList
                    ).ToList();

                    componentsList = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();
                    if (componentsList != null && componentsList.Count > 0)
                    {
                        List<string> selectedContract = new List<string>();

                        List<string> lstComponentNames = new List<string>();
                        lstComponentNames = componentsList.Where(vas => vas.ID == vases[0].ChildID).Select(vas => vas.Name.ToLower()).ToList();
                        if (lstComponentNames != null && lstComponentNames.Count > 0)
                        {
                            selectedContract = lstComponentNames;
                        }
                        if (ConfigurationManager.AppSettings["K2ContractName"] != null)
                        {
                            foreach (var s in selectedContract)
                            {
                                if (s.Contains(ConfigurationManager.AppSettings["K2ContractName"].ToString().ToLower()) || s.Contains("iphone"))
                                {
                                    registration.K2Type = true;
                                    break;
                                }
                                else
                                {
                                    registration.K2Type = false;
                                }
                            }
                        }
                    }

                }
            }
            registration.CreateDT = DateTime.Now;
            registration.LastAccessID = Util.SessionAccess.UserName;
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "0";
            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
            registration.ContractCheckStatus = ListOfRequiredBreValidation.Contains("Contract Check") ?
                                                !ReferenceEquals(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()]) : "CF"
                                                : "CN";
            registration.UserType = Session[SessionKey.PPID.ToString()].ToString() == null ? "N" : Session[SessionKey.PPID.ToString()].ToString();
            //registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()].ToString()) : 0);
            //registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthDevAdv.ToString()].ToString()) : 0);
            //registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalyPlanDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyPlanDeposit"].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()].ToString()) : 0);
            //registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalayDevDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayDevDeposit"].ToString()) : 0) : ((Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] != null) ? Convert.ToDecimal(Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()].ToString()) : 0);
            #region Drop 5: Waiver for non-Drop 4 Flow
            //10042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow - Start
            registration.DeviceAdvance = (dropObj.personalInformation.Customer.NationalityID == 1)
                                                ? dropObj.mainFlow.orderSummary.MalyDevAdv
                                                : dropObj.mainFlow.orderSummary.OthDevAdv;
            registration.DeviceDeposit = (dropObj.personalInformation.Customer.NationalityID == 1)
                                                ? dropObj.mainFlow.orderSummary.MalayDevDeposit
                                                : dropObj.mainFlow.orderSummary.OthDevDeposit;
            //10042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow - End
            #endregion
            registration.OutstandingAcs = Session[SessionKey.FailedAcctIds.ToString()] == null ? string.Empty : Session[SessionKey.FailedAcctIds.ToString()].ToString();
            registration.PrePortInCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()]) : "MPPF";
            registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()]) : "MSIF";
            registration.ArticleID = !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty;
            registration.upfrontPayment = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_UpfrontPayment"] != null) ? Convert.ToDecimal(Session["RegMobileReg_UpfrontPayment"].ToString2()) : 0) : ((Session["RegMobileReg_UpfrontPayment"] != null) ? Convert.ToDecimal(Session["RegMobileReg_UpfrontPayment"].ToString2()) : 0);
            if (UOMCode != null)
            {
                registration.UOMCode = UOMCode;
            }
            else
            {
                if (Session[SessionKey.OfferId.ToString()] != null && Session[SessionKey.OfferId.ToString()].ToString2() != string.Empty)
                {
                    using (var pro = new RegistrationServiceProxy())
                    {
                        var Offerresponse = pro.GetOffersById(Session[SessionKey.OfferId.ToString()].ToInt());
                        registration.UOMCode = Offerresponse == null || Offerresponse.UOMCode == null ? string.Empty : Offerresponse.UOMCode;
                    }
                }
                else
                {
                    registration.UOMCode = string.Empty;
                }
            }

            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : "No";
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "No";
            registration.OrganisationId = Util.SessionAccess.User.Org.OrganisationId;

            registration.Trn_Type = ConfigurationManager.AppSettings["Trn_Type"];
            registration.IMPOSStatus = 0;
            registration.SimModelId = 0;


            //Code Added by VLT Check for International Roaming
            if (Session[SessionKey.RegMobileReg_VasNames.ToString()] != null)
            {
                var vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
                string strVasArray = string.Join(",", vasNames.ToArray());
                if (strVasArray.Contains(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["InternationalRoaming"])) == true)
                {
                    if (WebHelper.Instance.CheckInternationRoaminFor(registration.MSISDN1) == false)
                    {
                        registration.InternationalRoaming = true;
                    }
                }
            }

            //End of International Roaming Check
            if (ContractType.ToUpper() == "TERMINATE")
                registration.SM_ContractType = "T";
            else if (ContractType.ToUpper() == "EXTEND")
                registration.SM_ContractType = "E";

            registration.KenanAccountNo = KenanAccountNo;
            registration.fxAcctNo = accountNo;
            registration.fxSubscrNo = subscriberNo;
            registration.fxSubScrNoResets = subscriberNoReset;
            if (isSimRequired.ToLower() == "true")
                registration.IsSImRequired_Smart = true;
            else
                registration.IsSImRequired_Smart = false;

            if (!string.IsNullOrEmpty(SIMModelSelected.Split(',')[0]) && SIMModelSelected.Split(',')[0].ToLower() != "false")
                registration.SimModelId = SIMModelSelected.Split(',')[0].ToInt();

            if (deviceVM.subAction.ToString2() != Constants.TERMINATEASSIGN_ACC_ONLY)
                registration.PenaltyAmount = penality == null ? 0 : penality == "" ? 0 : Convert.ToDecimal(Convert.ToDouble(penality));
            else
                registration.PenaltyAmount = 0;

            registration.PenaltyWaivedAmt = Session[SessionKey.PenaltyWaiveOff.ToString()].ToString2() == string.Empty ? 0 : Convert.ToDecimal(Session[SessionKey.PenaltyWaiveOff.ToString()]);
            registration.Discount_ID = "0";
            registration.IsK2 = ((!ReferenceEquals(Session["IsExistingK2"], null) && Session["IsExistingK2"].ToString() == "1") ? true : false);

            // S - for supplines and other type is principle line
            registration.CRPType = AccountType;
            registration.IMEINumber = Session["IMEIForDevice"].ToString2();

            //GTM e-Billing CR - Ricky - 2014.09.25
            registration.billDeliveryViaEmail = deviceVM.PersonalDetails.EmailBillSubscription == "Y" ? true : false;
            registration.billDeliveryEmailAddress = deviceVM.PersonalDetails.EmailBillSubscription == "Y" ? deviceVM.PersonalDetails.EmailAddress : "";
            registration.billDeliverySmsNotif = deviceVM.PersonalDetails.SmsAlertFlag == "Y" ? true : false;
            registration.billDeliverySmsNo = deviceVM.PersonalDetails.SmsAlertFlag == "Y" ? deviceVM.PersonalDetails.SmsNo : "";
            registration.billDeliverySubmissionStatus = "Pending";

            return registration;
        }

        /// <summary>
        /// Constructing customer object.
        /// </summary>
        /// <returns></returns>
        private Customer ConstructCustomer()
        {
            var allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");//10042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow
            var CustomerDetails = WebHelper.Instance.getCustomerDetails(allCustomerDetails);//10042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            var customerTitleIDforNA = ConfigurationManager.AppSettings["CustomerTitleID"].ToInt();

            personalDetailsVM.Customer.PayModeID = 1;
            personalDetailsVM.Customer.LanguageID = 1;
            personalDetailsVM.Customer.EmailAddr = CustomerDetails.EmailAddr; //11-12-2015 Karyne:E-Reciept email send to POS, adapt to OCT drop
            personalDetailsVM.Customer.RaceID = 4;

            if (personalDetailsVM.Customer.DateOfBirth.Equals(DateTime.MinValue))
                personalDetailsVM.Customer.DateOfBirth = DateTime.MinValue;
            else
                personalDetailsVM.Customer.DateOfBirth = Convert.ToDateTime(personalDetailsVM.DOBYear + "-" + personalDetailsVM.DOBMonth + "-" + personalDetailsVM.DOBDay);

           
            if (string.IsNullOrEmpty(personalDetailsVM.Customer.ContactNo))
            {
                personalDetailsVM.Customer.ContactNo = "60123456789";
            }
            else if (personalDetailsVM.Customer.ContactNo.Length == 10)
                personalDetailsVM.Customer.ContactNo = "6" + personalDetailsVM.Customer.ContactNo;

            var customerTitleID = personalDetailsVM.Customer.CustomerTitleID > 0
                                    ? personalDetailsVM.Customer.CustomerTitleID
                                    : customerTitleIDforNA;
            var customerTitle = Util.GetNameByID(RefType.CustomerTitle, customerTitleID);

            var cust = new Customer()
            {
                FullName = personalDetailsVM.Customer.FullName,
                //10042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow - Start
                Gender = !string.IsNullOrEmpty(CustomerDetails.Gender)
                            ? CustomerDetails.Gender
                            : personalDetailsVM.Customer.Gender,
                DateOfBirth = DateTime.Compare(CustomerDetails.DateOfBirth, DateTime.MinValue) > 0
                                ? CustomerDetails.DateOfBirth
                                : personalDetailsVM.Customer.DateOfBirth,
                CustomerTitleID = customerTitleID,
                RaceID = CustomerDetails.RaceID > 0
                            ? CustomerDetails.RaceID
                            : personalDetailsVM.Customer.RaceID,
                NationalityID = CustomerDetails.NationalityID > 0
                                    ? CustomerDetails.NationalityID
                                    : personalDetailsVM.Customer.NationalityID,
                //10042015 - Anthony - Drop 5: Waiver for non-Drop 4 Flow - End
                LanguageID = personalDetailsVM.Customer.LanguageID,
                IDCardTypeID = personalDetailsVM.Customer.IDCardTypeID,
                IDCardNo = personalDetailsVM.Customer.IDCardNo,
                ContactNo = personalDetailsVM.Customer.ContactNo,
                AlternateContactNo = personalDetailsVM.Customer.AlternateContactNo,
                EmailAddr = personalDetailsVM.Customer.EmailAddr,
                PayModeID = personalDetailsVM.Customer.PayModeID,
                CardTypeID = personalDetailsVM.Customer.CardTypeID,
                NameOnCard = personalDetailsVM.Customer.NameOnCard,
                CardNo = personalDetailsVM.Customer.CardNo,
                CardExpiryDate = personalDetailsVM.Customer.CardExpiryDate,
                IsVIP = Session["isVIP_Masking"].ToBool() ? 1 : 0,
                Active = true,
                LastAccessID = Util.SessionAccess.UserName,
                CreateDT = DateTime.Now
            };

            Session[SessionKey.FullName.ToString()] = string.Format("{0} {1}", customerTitle, personalDetailsVM.Customer.FullName);
            Session[SessionKey.Email.ToString()] = personalDetailsVM.Customer.EmailAddr;

            return cust;
        }

        /// <summary>
        /// Constructing device object.
        /// </summary>
        /// <param name="modelId">Model Id</param>
        /// <param name="packageId">Package Id</param>
        /// <param name="devicePrice">Model price</param>
        /// <returns></returns>
        private RegMdlGrpModel ConstructRegMdlGroupModel(string modelId, string packageId, string devicePrice)
        {
            int? mdlGrpModelID = null;

            using (var proxy = new CatalogServiceProxy())
            {
                var modelGroupIDs = proxy.ModelGroupFind(new ModelGroupFind()
                {
                    ModelGroup = new ModelGroup()
                    {
                        PgmBdlPckComponentID = packageId.ToInt()
                    },
                    
                    Active = true
                });

                var modelGroupModels = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                {
                    ModelGroupModel = new ModelGroupModel()
                    {
                        ModelID = Session["RegMobileReg_SelectedModelID"].ToInt()
                    }
                })).ToList();

                mdlGrpModelID = modelGroupModels.Where(a => modelGroupIDs.Contains(a.ModelGroupID)).Select(a => a.ID).FirstOrDefault();
            }

            var regMdlGrpModel = new RegMdlGrpModel()
            {
                ModelImageID = modelId.ToInt(),
                ModelGroupModelID = mdlGrpModelID != null ? mdlGrpModelID : 0,
                Price = devicePrice != null ? Convert.ToDecimal(devicePrice) : 0,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName,
                //RST GST
				TradeUpAmount = !ReferenceEquals(Session[SessionKey.TradeUpAmount.ToString2()],null) ? Convert.ToDecimal(Session[SessionKey.TradeUpAmount.ToString2()]) : 0,
				IsTradeUp = !ReferenceEquals(Session[SessionKey.TradeUpAmount.ToString2()], null) ? Convert.ToDecimal(Session[SessionKey.TradeUpAmount.ToString2()]) != 0 ? true : false :false
                //End RST GST
            };

            return regMdlGrpModel;

        }

        /// <summary>
        /// Constructing customer address object.
        /// </summary>
        /// <returns></returns>
        private List<Address> ConstructRegAddress()
        {
            var addresses = new List<Address>();
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

            addresses.Add(new Address()
            {
                AddressTypeID = 1,
                Line1 = personalDetailsVM.Address.Line1,
                Line2 = personalDetailsVM.Address.Line2,
                Line3 = personalDetailsVM.Address.Line3,
                Postcode = personalDetailsVM.Address.Postcode,
                StateID = personalDetailsVM.Address.StateID,
                Town = personalDetailsVM.Address.Town,
                Street = "",
                BuildingNo = "",
                CountryID = Util.GetIDByCode(RefType.Country, Properties.Settings.Default.Country_MY),
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });

            return addresses;
        }

        /// <summary>
        /// Constructing components object.
        /// </summary>
        /// <param name="planId">Plan Id</param>
        /// <param name="vasComponents">Vas components</param>
        /// <param name="contractType">Contract type</param>
        /// <param name="dataPlanId">Data PlanId</param>
        /// <returns></returns>
        private List<RegPgmBdlPkgComp> ConstructRegPgmBdlPkgComponent(string planId, string vasComponents, string contractType, string dataPlanId)
        {
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var regPBPCs = new List<RegPgmBdlPkgComp>();

            // Plan PgmBdlPkgComponent
            regPBPCs.Add(new RegPgmBdlPkgComp()
            {
                RegSuppLineID = null,
                PgmBdlPckComponentID = planId.ToInt(),
                IsNewAccount = true,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });

            /*data plan*/
            if (Session["intdataidsval"] != null && Session["intdataidsval"] != "" && Convert.ToInt32(Session["intdataidsval"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session["intdataidsval"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            /*extra plan*/
            if (Session["intextraidsval"] != null && Session["intextraidsval"].ToString() != "" && Convert.ToInt32(Session["intextraidsval"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session["intextraidsval"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            /*Insurance package*/
            if (Session["intinsuranceval"] != null && Session["intinsuranceval"] != "" && Convert.ToInt32(Session["intinsuranceval"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session["intinsuranceval"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            /*Promotional package*/
            if (Session["intpramotionidsval"] != null && Session["intpramotionidsval"] != "" && Convert.ToInt32(Session["intpramotionidsval"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session["intpramotionidsval"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            
			/*Promotional package*/
			if (Session["intDevFinancingIds"] != null && Session["intDevFinancingIds"] != "" && Convert.ToInt32(Session["intDevFinancingIds"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
					PgmBdlPckComponentID = Session["intDevFinancingIds"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }

			/*AF Package*/
			if (Session[SessionKey.AF_PackageID.ToString()] != null && Session[SessionKey.AF_PackageID.ToString()] != "" && Convert.ToInt32(Session[SessionKey.AF_PackageID.ToString()]) != 0)
			{
				regPBPCs.Add(new RegPgmBdlPkgComp()
				{
					PgmBdlPckComponentID = Session[SessionKey.AF_PackageID.ToString()].ToInt(),
					IsNewAccount = true,
					Active = true,
					CreateDT = DateTime.Now,
					LastAccessID = Util.SessionAccess.UserName
				});
			}

            // Optional VAS PgmBdlPkgComponent
            var vasIDs = vasComponents.ToString2().Split(',').Distinct();
			List<string> tempVasIDs = new List<string>();
			
            foreach (var vasID in vasIDs)
            {
                if (!string.IsNullOrEmpty(vasID.ToString()))
                {
					if (!tempVasIDs.Contains(vasID))
					{
						regPBPCs.Add(new RegPgmBdlPkgComp()
						{
							PgmBdlPckComponentID = vasID.ToInt(),
							IsNewAccount = true,
							Active = true,
							CreateDT = DateTime.Now,
							LastAccessID = Util.SessionAccess.UserName
						});
						tempVasIDs.Add(vasID);
					}
                    
                }
            }

            List<int> lstDPIds = new List<int>();
            string depcompIds = string.Empty;
            try
            {
                int[] ids = (int[])dataPlanId.Split(',').Select(s => int.Parse(s)).ToArray();
                foreach (int id in ids)
                    lstDPIds.Add(id);
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                lstDPIds.Add(Convert.ToInt32(dataPlanId));
            }
            depcompIds = WebHelper.Instance.GetDepenedencyComponents(string.Join(",", lstDPIds), false);

            List<string> depListCompIds = new List<string>();
            depListCompIds = depcompIds.Split(',').ToList();

            foreach (string compid in depListCompIds)
            {

                if (compid != string.Empty && Convert.ToInt32(compid) > 0)
                {
                    regPBPCs.Add(new RegPgmBdlPkgComp()
                    {
                        PgmBdlPckComponentID = compid.ToInt(),
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }

            }

            return regPBPCs;
        }

        /// <summary>
        /// Constructing registration status object.
        /// </summary>
        /// <returns></returns>
        private RegStatus ConstructRegStatus()
        {
            var regStatus = new RegStatus()
            {
                Active = true,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName,
                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegNew)
            };

            return regStatus;
        }

        private List<RegSuppLine> ConstructRegSuppLines()
        {
            var regSuppLines = new List<RegSuppLine>();

            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];

            if (sublines != null)
            {
                foreach (var subline in sublines)
                {
                    // SuppLine Plan
                    regSuppLines.Add(new RegSuppLine()
                    {
                        SequenceNo = subline.SequenceNo,
                        PgmBdlPckComponentID = subline.PkgPgmBdlPkgCompID,
                        IsNewAccount = true,
                        MSISDN1 = subline.SelectedMobileNos[0].ToString2(),
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }

            return regSuppLines;
        }

        private List<RegPgmBdlPkgComp> ConstructRegSuppLineVASes()
        {
            var regSuppLinePBPCs = new List<RegPgmBdlPkgComp>();
            var sublines = (List<SublineVM>)Session[SessionKey.RegMobileReg_SublineVM.ToString()];

            if (sublines != null)
            {
                foreach (var subline in sublines)
                {
                    // Mandatory SuppLine VAS
                    var suppLineVasIDs = new List<int>();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var pkgID = MasterDataCache.Instance.FilterComponents(new int[] { subline.PkgPgmBdlPkgCompID }).SingleOrDefault().ChildID;

                        suppLineVasIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                        {
                            IsMandatory = true,
                            PgmBdlPckComponent = new PgmBdlPckComponent()
                            {
                                ParentID = pkgID,
                                LinkType = Properties.Settings.Default.Package_Component
                            }
                        }).ToList();
                    }

                    foreach (var suppLineVasID in suppLineVasIDs)
                    {
                        regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
                        {
                            SequenceNo = subline.SequenceNo,
                            PgmBdlPckComponentID = suppLineVasID,
                            IsNewAccount = true,
                            Active = true,
                            CreateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }

                    // Optional SuppLine VAS
                    foreach (var vasID in subline.SelectedVasIDs)
                    {
                        regSuppLinePBPCs.Add(new RegPgmBdlPkgComp()
                        {
                            SequenceNo = subline.SequenceNo,
                            PgmBdlPckComponentID = vasID.ToInt(),
                            IsNewAccount = true,
                            Active = true,
                            CreateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }
                }
            }
            return regSuppLinePBPCs;
        }

        /// <summary>
        /// Constructing package object.
        /// </summary>
        /// <param name="MSISDN">MSISDN</param>
        /// <returns></returns>
        private List<PackageComponents> ConstructPackage(string MSISDN)
        {
            IList<Online.Registration.Web.SubscriberICService.PackageModel> packages = GetPackageDetails(MSISDN, ConfigurationManager.AppSettings["KenanMSISDN"]);
            List<PackageComponents> pckg = new List<PackageComponents>();
            foreach (var contract in packages)
            {
                for (int i = 0; i < contract.compList.Count; i++)
                {
                    pckg.Add(new PackageComponents()
                    {
                        PackageId = contract.PackageID,
                        packageInstId = contract.packageInstId,
                        packageInstIdServ = contract.packageInstIdServ,
                        packageDesc = contract.PackageDesc,
                        componentId = contract.compList[i].componentId,
                        componentInstId = contract.compList[i].componentInstId,
                        componentInstIdServ = contract.compList[i].componentInstIdServ,
                        componentActiveDt = contract.compList[i].componentActiveDt,
                        componentInactiveDt = contract.compList[i].componentInactiveDt,
                        componentDesc = contract.compList[i].componentDesc,
                        componentShortDisplay = contract.compList[i].componentShortDisplay
                    });
                }
            }
            return pckg;

        }

        private CMSSComplain ConstructCMSSID()
        {
            CMSSComplain cms = new CMSSComplain();
            cms.CMSSID = Session["CMSSID"] == null ? string.Empty : Session["CMSSID"].ToString();
            return cms;
        }

        /// <summary>
        /// Constructing smart components object.
        /// </summary>
        /// <param name="planId">Plan Id</param>
        /// <param name="dataPlanId">Data planId</param>
        /// <returns></returns>
        private List<RegSmartComponents> ConstructSmartComponents(string planId, string dataPlanId)
        {
            List<RegSmartComponents> regSmartComponents = new List<RegSmartComponents>();
            if (Session["_componentViewModel"] != null)
            {
				var _MOCWaiverEligibleList = MasterDataCache.Instance.GetAllLovList.Where(x => x.Type == "DF_CHARGE").Select(x => x.Value).ToList();
                ComponentViewModel sessionVal = (ComponentViewModel)Session["_componentViewModel"];
                RegSmartComponents smartcomp = new RegSmartComponents();

                foreach (var c in sessionVal.NewComponentsList)
                {
					if (c.IsChecked && !_MOCWaiverEligibleList.Contains(c.NewComponentKenanCode))
                    {
                        smartcomp = new RegSmartComponents();
                        smartcomp.PackageID = c.NewPackageKenanCode;
                        smartcomp.ComponentID = c.NewComponentKenanCode;
                        smartcomp.ComponentDesc = c.NewComponentName;
                        smartcomp.PackageDesc = string.Empty;
                        regSmartComponents.Add(smartcomp);
                    }
                }

                // Adding dependent VAS list           
                if (sessionVal.DependentVASList != null && sessionVal.DependentVASList.Count > 0)
                {
                    foreach (var c in sessionVal.DependentVASList)
                    {
                        if (c.IsChecked)
                        {
                            smartcomp = new RegSmartComponents();
                            smartcomp.PackageID = c.NewPackageKenanCode;
                            smartcomp.ComponentID = c.NewComponentKenanCode;
                            smartcomp.ComponentDesc = c.NewComponentName;
                            smartcomp.PackageDesc = string.Empty;
                            regSmartComponents.Add(smartcomp);
                        }
                    }
                }

                //Added for Contract dependent 
                if (dataPlanId.ToString2() != string.Empty)
                {
                    List<int> depCompIds = new List<int>();
                    List<int> tempdepCompIds = new List<int>();
                    if (dataPlanId.ToString2() != string.Empty)
                    {
                        try
                        {
                            int[] dataplanid = (int[])dataPlanId.Split(',').Select(s => int.Parse(s)).ToArray();

                            foreach (int pid in dataplanid)
                            {
                                depCompIds.Add(pid.ToInt());
                            }
                            tempdepCompIds.AddRange(WebHelper.Instance.GetDepenedencyComponents(dataplanid.ToList()));
                            depCompIds.AddRange(tempdepCompIds);
                        }
                        catch (Exception ex)
                        {
                            WebHelper.Instance.LogExceptions(this.GetType(), ex);
                            depCompIds.Add(dataPlanId.ToInt());
                        }

                    }

                    var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
                    var components = new List<Component>();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        if (depCompIds != null && depCompIds.Count > 0)
                        {
                            depCompIds = depCompIds.Distinct().ToList();

                            List<FxDependChkComps> Comps = new List<FxDependChkComps>();
                            string strCompIds = string.Empty;
                            foreach (var item in depCompIds)
                            {
                                strCompIds += item.ToString2() + ",";
                            }

                            using (var Proxy = new RegistrationServiceProxy())
                            {
                                Comps = Proxy.GetAllComponentsbyPlanId(planId.ToInt(), strCompIds, "BP");
                            }
                            List<Component> components1 = new List<Component>();
                            pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(depCompIds).ToList();
                            components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID)).ToList();
                            components = components.Where(cnt => cnt.RestricttoKenan == false).ToList();
                            if (components != null && components.Count > 0)
                            {
                                for (int cnt = 0; cnt < components.Count(); cnt++)
                                {
                                    smartcomp = new RegSmartComponents();
                                    if (Comps != null && Comps.Count > 0 && Comps[0].KenanCode != "0")
                                    {
                                        smartcomp.PackageID = Comps[0].KenanCode;
                                    }
                                    smartcomp.ComponentID = pgmBdlPckComponents[cnt].KenanCode;
                                    smartcomp.ComponentDesc = components[cnt].Description + " (RM " + pgmBdlPckComponents[cnt].Price.ToString() + ")";
                                    smartcomp.PackageDesc = string.Empty;
                                    regSmartComponents.Add(smartcomp);
                                }
                                Session["ContractPkgKenanCode"] = null;
                            }

                        }
                    }
                }

                if (regSmartComponents != null && regSmartComponents.Count > 0)
                {
                    regSmartComponents = regSmartComponents.GroupBy(x => x.ComponentID).Select(y => y.First()).ToList();
                }

                return regSmartComponents;
            }
            else
            {
                return regSmartComponents;
            }


        }

        /// <summary>
        /// Creating Kenan account.
        /// </summary>
        /// <param name="regID"></param>
        private void CreateKenanAccount(int regID)
        {
            var reg = new DAL.Models.Registration();
            var cust = new Customer();
            var billAddr = new Address();

            try
            {
                using (var proxy = new RegistrationServiceProxy())
                {
                    // ********** Update Registration Status **********
                    proxy.RegStatusCreate(WebHelper.Instance.ConstructRegStatus(regID, Properties.Settings.Default.Status_AccPendingCreate), null);

                    // ********** Construct Kenan object **********
                    reg = proxy.RegistrationGet(regID);
                    cust = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    var billAddrID = proxy.RegAddressFind(new AddressFind()
                    {
                        Address = new Address()
                        {
                            RegID = reg.ID,
                            AddressTypeID = Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Billing)
                        }
                    }).SingleOrDefault();

                    billAddr = proxy.RegAddressGet(new int[] { billAddrID }).SingleOrDefault();
                }

                using (var proxy = new KenanServiceProxy())
                {
                    if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                    {

                        proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
                        {
                            orderId = regID.ToString(),
                            externalId = reg.MSISDN1,
                            extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
                            Customer = WebHelper.Instance.ConstructKenanCustomerMNP(cust, billAddr),
                            Account = WebHelper.Instance.ConstructKenanAccountMNP(cust),
                            UserName = reg.SalesPerson,
                        });
                    }
                    else
                    {
                        if (reg.RegTypeID == (int)MobileRegType.MNPPlanOnly)
                        {
                            proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
                            {
                                orderId = regID.ToString(),
                                externalId = reg.MSISDN1,
                                extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
                                Customer = WebHelper.Instance.ConstructKenanCustomerMNP(cust, billAddr),
                                Account = WebHelper.Instance.ConstructKenanAccountMNP(cust),
                                UserName = reg.SalesPerson,
                            });
                        }
                        else
                        {
                            try
                            {
                                proxy.KenanAccountCreate(new OrderCreationRequest()
                                {
                                    OrderID = regID.ToString(),
                                    MarketCodeID = Util.GetIDByCode(RefType.Market, Properties.Settings.Default.MarketCode_Individual),
                                    AccCategoryID = Util.GetIDByCode(RefType.AccountCategory, Properties.Settings.Default.AccountCategory_Consumer),
									//VIPCodeID = Util.GetIDByCode(RefType.VIPCode, Properties.Settings.Default.VIPCode_Individal),
									// Add in Check For Wrong Collection Tagging
									VIPCodeID = Util.custVIPCheck(cust),
                                    ExternalID = reg.MSISDN1,
                                    ExtIDTypeID = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN),
                                    Customer = WebHelper.Instance.ConstructKenanCustomer(cust),
                                    BillingAddress = WebHelper.Instance.ConstructBillAddress(billAddr),
                                    UserName = reg.SalesPerson,
                                    UserSession = Util.SessionAccess.UserName,
                                });
                            }
                            catch (Exception ex)
                            {
                                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                RedirectToAction("MobileRegClosed");
                            }
                        }
                    }


                }
            }
            catch (Exception ex)
            {

                WebHelper.Instance.LogExceptions(this.GetType(), ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regID"></param>
        private void FulfillKenanAccount(int regID)
        {
            var reg = new DAL.Models.Registration();
            var cust = new Customer();
            var billAddr = new Address();

            try
            {

                var request = new OrderFulfillRequest()
                {
                    OrderID = regID.ToString(),
                    UserSession = Util.SessionAccess.UserName,
                };

                using (var proxy = new KenanServiceProxy())
                {
                    proxy.KenanAccountFulfill(request);
                }
            }
            catch (Exception ex)
            {

                WebHelper.Instance.LogExceptions(this.GetType(), ex);
            }

        }

        /// <summary>
        /// Reset all sessions.
        /// </summary>
        private void ResetAllSession()
        {
            Session["RegMobileReg_FromSearch"] = null;
            Session[SessionKey.RegMobileReg_MalyDevAdv.ToString()] = null;
            Session[SessionKey.RegMobileReg_MalayPlanAdv.ToString()] = null;
            Session[SessionKey.RegMobileReg_MalayDevDeposit.ToString()] = null;
            Session[SessionKey.RegMobileReg_MalyPlanDeposit.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthDevAdv.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthPlanAdv.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthDevDeposit.ToString()] = null;
            Session[SessionKey.RegMobileReg_OthPlanDeposit.ToString()] = null;
            ClearRegistrationSession();
        }
        private ValueAddedServicesVM GetAvailablePackageComponents(int pbpcID, bool fromSubline = false, bool isMandatory = false, bool isDeviceFinancing = false)
        {
            var vasVM = new ValueAddedServicesVM();
            var retrievedVases = new List<AvailableVAS>();
            var contractVases = new List<AvailableVAS>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var components = new List<Component>();
            var componentTypes = new List<ComponentType>();

            using (var proxy = new CatalogServiceProxy())
            {
                var packageID = (pbpcID != null && pbpcID != 0) ? ((MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().ChildID) != null ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().ChildID : 0) : 0;
                if (packageID != 0)
                {
                    pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            LinkType = Properties.Settings.Default.Package_Component,
                            ParentID = packageID,
                        },
                        IsMandatory = isMandatory,
                        Active = true
                    })).ToList();
                }
                if (pgmBdlPckComponents.Count() > 0)
                {
                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();

                    componentTypes = proxy.ComponentTypeGet(components.Select(a => a.ComponentTypeID).Distinct()).ToList();
                }
            }

            foreach (var pbpc in pgmBdlPckComponents)
            {
                var component = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault();
                var compTypeCode = componentTypes.Where(a => a.ID == component.ComponentTypeID).SingleOrDefault().Code;

                if (compTypeCode == Properties.Settings.Default.CompType_DataCon || compTypeCode == Properties.Settings.Default.CompType_VoiceCon || (compTypeCode == Properties.Settings.Default.Device_Financing_Component && isDeviceFinancing) )
                {
                    contractVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
                        KenanCode = pbpc.KenanCode,
                        RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false

                    });
                }
                else
                {
					if (compTypeCode == Properties.Settings.Default.Device_Financing_Component && !isDeviceFinancing)
					{
						//
					}
					else
					{
						retrievedVases.Add(new AvailableVAS
						{
							PgmBdlPkgCompID = pbpc.ID,
							ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
							Value = pbpc.Value.ToString2(),
							Price = pbpc.Price,
							KenanCode = pbpc.KenanCode,
							RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false
						});
					}
                   
                }
            }

            if (isMandatory)
            {
                vasVM.MandatoryVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }
            else
            {
                vasVM.AvailableVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }

            //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
            if (Session["ArticleId"] != null)
            {
                if (contractVases != null && contractVases.Count > 0)
                {
                    string MOCStatus = !ReferenceEquals(Session["MOCIDs"], null) ? Session["MOCIDs"].ToString() : string.Empty;

                    List<string> objContracts = new List<string>();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var kenanPackageID = (pbpcID != null && pbpcID != 0) ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().KenanCode : string.Empty;

                        objContracts = proxy.GetContractsForArticleId(Session["ArticleId"].ToString(), kenanPackageID, MOCStatus);

                    }
                    if (objContracts != null && objContracts.Count > 0)
                        vasVM.ContractVASes = contractVases.Where(a => string.Join(",", objContracts).Contains(a.KenanCode)).ToList();

                }
                else
                    vasVM.ContractVASes = contractVases;
            }
            else
                vasVM.ContractVASes = contractVases;

            vasVM.SelectedContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2();
            vasVM.SelectedVasIDs = fromSubline ? Session["RegMobileSub_VasIDs"].ToString2() : Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2();

            return vasVM;
        }
        private ValueAddedServicesVM GetAvailablePackageComponents_PC_MC_DC(int pbpcID, string listtype, bool fromSubline = false, bool isMandatory = false)
        {
            var vasVM = new ValueAddedServicesVM();
            var retrievedVases = new List<AvailableVAS>();
            var contractVases = new List<AvailableVAS>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var components = new List<Component>();
            var componentTypes = new List<ComponentType>();


            int packageID = 0;
            using (var proxy = new CatalogServiceProxy())
            {

                packageID = (pbpcID != null) ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().ChildID : 0;

                if (packageID != 0)
                {

                    pgmBdlPckComponents.AddRange(MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            LinkType = listtype,
                            ParentID = packageID,
                        },
                        IsMandatory = isMandatory,
                        Active = true
                    })).ToList());



                }
                if (pgmBdlPckComponents.Count() > 0)
                {
                    components = proxy.ComponentGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();

                    componentTypes = proxy.ComponentTypeGet(components.Select(a => a.ComponentTypeID).Distinct()).ToList();
                }
            }


            foreach (var pbpc in pgmBdlPckComponents)
            {
                var component = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault();
                var compTypeCode = componentTypes.Where(a => a.ID == component.ComponentTypeID).SingleOrDefault().Code;

                if (compTypeCode == Properties.Settings.Default.CompType_DataCon || compTypeCode == Properties.Settings.Default.CompType_VoiceCon)
                {
                    contractVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
                        KenanCode = pbpc.KenanCode,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        GroupId = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().GroupId,
                        GroupName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Contains("╚") ? components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Split('╚')[1] : "",
                        RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false
                    });
                }
                else
                {
                    retrievedVases.Add(new AvailableVAS
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        KenanCode = pbpc.KenanCode,
                        ComponentName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        Value = pbpc.Value.ToString2(),
                        Price = pbpc.Price,
                        GroupId = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().GroupId,
                        GroupName = components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Contains("╚") ? components.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Description.Split('╚')[1] : "",
                        RestricttoKenan = component.RestricttoKenan.HasValue ? component.RestricttoKenan.Value : false
                    });
                }
            }

            if (isMandatory)
            {
                vasVM.MandatoryVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }
            else
            {
                vasVM.AvailableVASes = retrievedVases.Where(a => a.RestricttoKenan == false).ToList();
            }

            //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
            if (Session["ArticleId"] != null)
            {
                if (contractVases != null && contractVases.Count > 0)
                {
                    string MOCStatus = !ReferenceEquals(Session["MOCIDs"], null) ? Session["MOCIDs"].ToString() : string.Empty;

                    List<string> objContracts = new List<string>();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        var kenanPackageID = (pbpcID != null && pbpcID != 0) ? MasterDataCache.Instance.FilterComponents(new int[] { pbpcID }).SingleOrDefault().KenanCode : string.Empty;

                        objContracts = proxy.GetContractsForArticleId(Session["ArticleId"].ToString(), kenanPackageID, MOCStatus);

                    }
                    if (objContracts != null && objContracts.Count > 0)
                        vasVM.ContractVASes = contractVases.Where(a => string.Join(",", objContracts).Contains(a.KenanCode)).ToList();

                }
                else
                    vasVM.ContractVASes = contractVases;
            }
            else
                vasVM.ContractVASes = contractVases;
            //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid



            vasVM.SelectedContractID = Session[SessionKey.RegMobileReg_ContractID.ToString()].ToString2();
            vasVM.SelectedVasIDs = fromSubline ? Session["RegMobileSub_VasIDs"].ToString2() : Session[SessionKey.RegMobileReg_VasIDs.ToString()].ToString2();

            return vasVM;
        }

        public ValueAddedServicesVM GetMandatoryVasDependencies(List<string> subscribedComponents, out  List<int> mandatoryIds, ref List<string> kenanMandatoryVasIds)
        {

            int selectedPlan = 0;
            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] != null)
            {
                selectedPlan = Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]);

            }
            //Get the dependent components of the mandatory vases

            List<int> depCompIds = new List<int>();
            List<int> mandatoryVasIds = new List<int>();
            List<string> mandatoryVasKenanCodes = new List<string>();
            mandatoryIds = new List<int>();
            List<string> mandatoryDepCompIds = new List<string>();

            List<int> mandatoryPkgCompIds = new List<int>();

            ValueAddedServicesVM vasVM = new ValueAddedServicesVM();
            if (selectedPlan > 0)
            {
                vasVM = GetAvailablePackageComponents(selectedPlan, isMandatory: true);
                mandatoryVasIds = vasVM.MandatoryVASes.Select(cas => cas.PgmBdlPkgCompID).ToList();
                mandatoryVasKenanCodes = vasVM.MandatoryVASes.Select(cas => cas.KenanCode).ToList();
                kenanMandatoryVasIds = mandatoryVasKenanCodes;
                if (mandatoryVasIds != null && mandatoryVasIds.Count > 0)
                {
                    using (var proxy = new CatalogServiceProxy())
                    {
                        int loop = 0;
                        foreach (var vasId in mandatoryVasIds)
                        {

                            if (subscribedComponents.Contains(mandatoryVasKenanCodes[loop]))
                            {
                                mandatoryIds.Add(vasId);
                                List<PgmBdlPckComponent> pgmComponents = new List<PgmBdlPckComponent>();
                                //depCompIds = GetDepenedencyComponents(vasId.ToString());
                                List<Component> components = new List<Component>();
                                //pgmComponents = proxy.PgmBdlPckComponentGet(depCompIds).ToList();
                                pgmComponents = proxy.PgmBdlPckComponentGet2(Convert.ToInt32(vasId), 0).ToList();
                                foreach (var component in pgmComponents)
                                {
                                    mandatoryDepCompIds.Add(component.KenanCode);
                                    mandatoryPkgCompIds.Add(component.ID);
                                }
                            }
                            loop++;
                        }
                    }

                }
            }

            if (mandatoryDepCompIds != null && mandatoryDepCompIds.Count > 0)
            {

                Session["MandatoryDepCompIds"] = mandatoryDepCompIds;

                Session["mandatoryPkgCompIds"] = mandatoryPkgCompIds;

            }
            return vasVM;
        }
        private ValueAddedServicesVM GetVasComponents(List<string> subscribedComponents, out BundlepackageResp bundlePackages, out  List<int> mandatoryVasIds, ref List<string> KenanMandatoryVasIds)
        {
            RetrieveComponentInfoRequest objRetrieveComponentInfoReq = null;
            RetrieveComponentInfoResponse objRetrieveComponentInfoRes = null;
            using (var proxy = new KenanServiceProxy())
            {
                if (Session["ComponentInfo"] != null)
                {
                    objRetrieveComponentInfoRes = (RetrieveComponentInfoResponse)Session["ComponentInfo"];

                }
                else
                {
                    objRetrieveComponentInfoReq = new RetrieveComponentInfoRequest();
                    objRetrieveComponentInfoReq.ExternalID = Session[SessionKey.ExternalID.ToString()].ToString2();
                    objRetrieveComponentInfoRes = proxy.RetrieveComponentInfo(objRetrieveComponentInfoReq);
                }
            }

            ValueAddedServicesVM mandatoryVasVM = new ValueAddedServicesVM();
            List<int> mandatoryVasIDs = new List<int>();
            mandatoryVasVM = GetMandatoryVasDependencies(subscribedComponents, out mandatoryVasIDs, ref KenanMandatoryVasIds);
            mandatoryVasIds = mandatoryVasIDs;
            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] == null || (Convert.ToString(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]) == String.Empty))
            {
                Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = Session["GuidedSales_PkgPgmBdlPkgCompID"];
            }
            ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();

            string MobileRegType = string.Empty;
            Lstvam = GetAvailablePackageComponents(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt(), isMandatory: false);
            if (!ReferenceEquals(Session["MobileRegType"], null))
            {
                MobileRegType = Session[SessionKey.MobileRegType.ToString()].ToString();
            }
            Session[SessionKey.MobileRegType.ToString()] = "13";

            if (Lstvam.ContractVASes != null)
                Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);

            if (Lstvam.AvailableVASes != null)
                Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);

            /* For Mandatory packages */
            Session["vasmandatoryids"] = "";
            using (var proxy = new CatalogServiceProxy())
            {

                BundlepackageResp respon = new BundlepackageResp();
                List<int> bundles = new List<int>();
                List<PgmBdlPckComponent> bundleComponents = new List<PgmBdlPckComponent>();
                respon = proxy.GetMandatoryVas(Convert.ToInt32(Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()]));
                if (respon.values != null)
                {
                    for (int i = 0; i < respon.values.Count(); i++)
                    {

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Master_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            Session["intmandatoryidsval"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
                            bundles = new List<int>();
                            bundleComponents = new List<PgmBdlPckComponent>();
                            bundles.Add(Convert.ToInt32(respon.values[i].BdlDataPkgId));
                            bundleComponents = MasterDataCache.Instance.FilterComponents(bundles).ToList();
                            if (bundleComponents.Count > 0)
                                respon.values[i].BdlDataPkgId = bundleComponents[0].KenanCode;
                        }

                        if (respon.values[i].Plan_Type == Properties.Settings.Default.CompType_DataCon || respon.values[i].Plan_Type == Properties.Settings.Default.Mandatory_DataComponent)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;

                            List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                            Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode
                                                             };


                            Session["intdataidsval"] = respon.values[i].BdlDataPkgId;
                            Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts);

                            bundles = new List<int>();
                            bundleComponents = new List<PgmBdlPckComponent>();
                            bundles.Add(Convert.ToInt32(respon.values[i].BdlDataPkgId));
                            bundleComponents = MasterDataCache.Instance.FilterComponents(bundles).ToList();
                            if (bundleComponents.Count > 0)
                                respon.values[i].BdlDataPkgId = bundleComponents[0].KenanCode;
                        }
                        /*Extra packages*/
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Extra_Component)
                        {
                            Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            List<int> Finalintvalues = null;

                            List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                            Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();



                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS()
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode
                                                             };


                            Session["intextraidsval"] = respon.values[i].BdlDataPkgId;

                            Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);
                            bundles = new List<int>();
                            bundleComponents = new List<PgmBdlPckComponent>();
                            bundles.Add(Convert.ToInt32(respon.values[i].BdlDataPkgId));
                            bundleComponents = MasterDataCache.Instance.FilterComponents(bundles).ToList();
                            if (bundleComponents.Count > 0)
                                respon.values[i].BdlDataPkgId = bundleComponents[0].KenanCode;
                        }
                        //Pramotional Packages
                        if (respon.values[i].Plan_Type == Properties.Settings.Default.Pramotional_Component)
                        {
							Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            #region COmmented filterComponents for Pramotional components
                            //List<int> Finalintvalues = null;
                            //if (Session["RegMobileReg_Type"].ToInt() == 1)
                            //{
                            //    List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();
                            //    string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();
                            //    Finalintvalues = values.Select(a => a.BdlDataPkgId).Distinct().ToList();
                            //    Lstvam.DeviceType = "Seco_NoDevice";
                            //}
                            //else if (Session["RegMobileReg_Type"].ToInt() == 2)
                            //{
                            //    List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
                            //    Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
                            //}
                            //var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                            //                                 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                            //                                 select new AvailableVAS
                            //                                 {
                            //                                     ComponentName = e.ComponentName,
                            //                                     PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                            //                                     Price = e.Price,
                            //                                     Value = e.Value,
                            //                                     GroupId = e.GroupId,
                            //                                     GroupName = e.GroupName,
                            //                                     KenanCode = e.KenanCode
                            //                                 }; 
                            #endregion


                            Session["intpramotionidsval"] = respon.values[i].BdlDataPkgId;
                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intpramotionidsval = respon.values[i].BdlDataPkgId;
                                }
                            }
                            bundles = new List<int>();
                            bundleComponents = new List<PgmBdlPckComponent>();
                            bundles.Add(Convert.ToInt32(respon.values[i].BdlDataPkgId));
                            bundleComponents = MasterDataCache.Instance.FilterComponents(bundles).ToList();
                            if (bundleComponents.Count > 0)
                                respon.values[i].BdlDataPkgId = bundleComponents[0].KenanCode;
                            if (Lstvam.AvailableVASes != null && Lstvam.AvailableVASes.Count > 0)
                            {
                                for (int j = 0; j < Lstvam.AvailableVASes.Count; j++)
                                {
                                    Lstvam.AvailableVASes[j].PgmBdlPkgID = bundleComponents[0].KenanCode;
                                }
                            }
                            Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);

                        }
                        //insurance Components
                        if (respon.values[i].Plan_Type == Settings.Default.Device_Insurecomponent)
                        {
							Lstvam = GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;
                            if (Session["RegMobileReg_Type"].ToInt() == 1)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

                                string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

                                Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

                            }
                            else if (Session["RegMobileReg_Type"].ToInt() == 2)
                            {
                                List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

                                Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            }
                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

                                                             select new AvailableVAS
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault
                                                             };
                            Session["intinsuranceval"] = respon.values[i].BdlDataPkgId;
                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intinsuranceval = respon.values[i].BdlDataPkgId;
                                }
                            }

                            Lstvam2.InsuranceComponents.AddRange(finalfiltereddatacontracts);
                        }

                        #region device financing
                        retrieveAcctListByICResponse allsServiceDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
                        var dfEligibilityCheck = Session[SessionKey.DF_Eligibility.ToString()].ToBool();
                        var dfException = Session["df_showOffer"].ToString2() == "show" ? true : false;

                        if (dfEligibilityCheck || dfException || Roles.IsUserInRole("MREG_CUSR"))
                        {
                            var masterDFList = proxy.GetAllDeviceFinancingInfo();
                            if (respon.values[i].Plan_Type == Settings.Default.Device_Financing_Component)
                            {
								Session["intDevFinancingIds"] = respon.values[i].BdlDataPkgId;
                                //string articleid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ArticleId.ToString2();
                                string articleid = Session[SessionKey.ArticleId.ToString()].ToString2();
                                string existingPlanID = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).SelectedPgmBdlPkgCompID.ToString2();
                                string uomCode = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).VasVM.uomCode.ToString2();
                                int pbpcID = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).SelectedPgmBdlPkgCompID.ToInt();
								Lstvam = WebHelper.Instance.GetAvailablePackageComponentsCompName_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                                if (Session["RegMobileReg_Type"].ToInt() == 1 && Lstvam.AvailableVASes.Any())
                                {
                                    var deviceFinancingInfo = proxy.GetDeviceFinancingInfoByArticleID(articleid, Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));
                                    if (deviceFinancingInfo.Any())
                                    {
                                        var contractListDF = Lstvam.AvailableVASes.Where(x => !string.IsNullOrWhiteSpace(x.Value)).ToList();
                                        var upgradeFeeKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_UPGRADEFEE);
										var mocWaiverKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_MOC_WAIVER);
										var mocVAS = Lstvam.AvailableVASes.Where(x => x.KenanCode == mocWaiverKenanCode).ToList();
										var deviceFinancingVAS = Lstvam.AvailableVASes.Where(x => x.isHidden == false).ToList();

                                        if (deviceFinancingVAS.Any())
                                        {
											deviceFinancingVAS.FirstOrDefault(x => x.KenanCode == upgradeFeeKenanCode).Price = deviceFinancingInfo.FirstOrDefault().UpgradeFee;
											deviceFinancingVAS.FirstOrDefault(x => x.KenanCode == upgradeFeeKenanCode).oneTimeUpgrade = deviceFinancingInfo.FirstOrDefault().OneTimeUpgrade;
											Lstvam2.DeviceFinancingVAS.AddRange(deviceFinancingVAS);
											
											if (mocVAS.Any())
												Lstvam2.DeviceFinancingVAS.AddRange(mocVAS);

                                            //var DFplanID = proxy.PgmBdlPckComponentGet(new int[] { respon.values[i].BdlDataPkgId.ToInt() }).SingleOrDefault().KenanCode;
                                            foreach (var df in contractListDF)
                                            {
                                                var df_contractOffer = WebHelper.Instance.GetContractOffersDF(new string[] { df.KenanCode }.ToList(), articleid, existingPlanID, string.Empty);

                                                Lstvam2.ContractOffers.AddRange(df_contractOffer);
                                                foreach (var offer in df_contractOffer)
                                                {
                                                    var oneTimeFee = masterDFList.Where(x => x.ArticleID == articleid && x.UOMCode.Contains(uomCode) && x.PlanID == pbpcID).FirstOrDefault().OneTimeUpgrade;
                                                    if (!Lstvam2.df_offerOneTimeFee.ContainsKey(offer.UOMCode))
                                                    {
                                                        Lstvam2.df_offerOneTimeFee.Add(offer.UOMCode, oneTimeFee);
                                                    }
                                                }
                                            }
                                            if (Lstvam2.ContractOffers.Any() && !Session["fromBackbutton"].ToBool())
                                                Session["OfferId"] = Lstvam2.ContractOffers[Lstvam2.ContractOffers.Count() - 1].Id;

                                            string processingMsisdn = string.Empty;
                                            var dfContract = WebHelper.Instance.GetAvailablePackageComponents(respon.values[i].BdlDataPkgId.ToInt(), ref processingMsisdn, isMandatory: false, isDF: true);

                                            Lstvam2.ContractVASes.AddRange(dfContract.ContractVASes);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                    }

                }
                bundlePackages = respon;


            }


            if (MobileRegType == "Planonly")
            {

                Lstvam.ContractVASes.Clear();

            }
            if (subscribedComponents != null && subscribedComponents.Count > 0)
            {
                List<AvailableVAS> availableVases = new List<AvailableVAS>();
                List<AvailableVAS> dataVases = new List<AvailableVAS>();
                List<AvailableVAS> extraVases = new List<AvailableVAS>();
                List<AvailableVAS> mandatoryVASes = new List<AvailableVAS>();
                List<AvailableVAS> DeviceInsuranceVASes = new List<AvailableVAS>();
                List<AvailableVAS> pramotionalVASes = new List<AvailableVAS>();
                List<AvailableVAS> devicefinancingVASes = new List<AvailableVAS>();
                foreach (var vas in Lstvam2.AvailableVASes)
                {
                    List<string> checkVases = new List<string>();
                    checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                    if (checkVases == null || checkVases.Count == 0)
                        availableVases.Add(vas);
                }
                foreach (var vas in Lstvam2.DataVASes)
                {
                    List<string> checkVases = new List<string>();
                    checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                    if (checkVases == null || checkVases.Count == 0)
                        dataVases.Add(vas);
                }
                foreach (var vas in Lstvam2.ExtraComponents)
                {
                    List<string> checkVases = new List<string>();
                    checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                    if (checkVases == null || checkVases.Count == 0)
                        extraVases.Add(vas);
                }
                foreach (var vas in mandatoryVasVM.MandatoryVASes)
                {
                    List<string> checkVases = new List<string>();
                    checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                    if (checkVases == null || checkVases.Count == 0)
                        mandatoryVASes.Add(vas);
                }
                foreach (var vas in Lstvam2.InsuranceComponents)
                {
                    List<string> checkVases = new List<string>();
                    checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                    if (checkVases == null || checkVases.Count == 0)
                        DeviceInsuranceVASes.Add(vas);
                }
                foreach (var vas in Lstvam2.PramotionalComponents)
                {
                    List<string> lstContractTypes = null;
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        lstContractTypes = proxy.getRebateContractIDbyComponentID(vas.KenanCode);
                    }
                    if (lstContractTypes != null && lstContractTypes.Any())
                    {
                        foreach (var i in lstContractTypes)
                        {
                            List<Online.Registration.Web.KenanSvc.Contract> objResNew = new List<KenanSvc.Contract>();
                            if (objRetrieveComponentInfoRes.Contracts != null && objRetrieveComponentInfoRes.Contracts.Any())
                            {
                                objResNew = objRetrieveComponentInfoRes.Contracts.Where(a => a.ContractType == i && a.EndDate > DateTime.Now.Date).ToList();
                            }
                            if (objResNew != null && objResNew.Any())
                            {
                                var item = from e in objResNew
                                           where e.EndDate.Date < DateTime.Now.Date
                                           select e;
                                if (item != null && item.Count() > 0)
                                    pramotionalVASes.Add(vas);
                            }
                            else
                            {
                                List<string> checkVases = new List<string>();
                                checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                                if (checkVases == null || checkVases.Count == 0)
                                    pramotionalVASes.Add(vas);
                            }
                        }
                    }
                    else
                    {
                        List<string> checkVases = new List<string>();
                        checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();
                        if (checkVases == null || checkVases.Count == 0)
                            pramotionalVASes.Add(vas);
                    }

                   
                }
                foreach (var vas in Lstvam2.DeviceFinancingVAS)
                {
					List<string> checkVases = new List<string>();
					checkVases = subscribedComponents.Where(c => c.ToString() == vas.KenanCode).ToList();

					if (checkVases == null || checkVases.Count == 0)
						devicefinancingVASes.Add(vas);

                }
                Lstvam2.AvailableVASes = availableVases;
                Lstvam2.DataVASes = dataVases;
                Lstvam2.ExtraComponents = extraVases;
                Lstvam2.MandatoryVASes = mandatoryVASes;
                Lstvam2.InsuranceComponents = DeviceInsuranceVASes;
                Lstvam2.PramotionalComponents = pramotionalVASes;
                Lstvam2.DeviceFinancingComponents = devicefinancingVASes;
            }
            return Lstvam2;

        }
        private ComponentViewModel LoadComponents()
        {
            ComponentViewModel model = new ComponentViewModel();
            List<string> subscribedComponents = new List<string>();
            decimal price = 0;
            List<List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents>> packages = new List<List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents>>();
            List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();

            model.SubscribedComponents = GetPackageDetails(Session[SessionKey.ExternalID.ToString()].ToString2(), "23", out componentList, ref subscribedComponents);
            if (subscribedComponents != null && subscribedComponents.Count > 0)
            {
                model.SubscribedComponentIds = string.Join(",", subscribedComponents);
            }
            List<string> KenanMandatoryVasIds = new List<string>();
            BundlepackageResp bundlePackages = new BundlepackageResp();
            List<int> mandatoryVasIDs = new List<int>();
            model.Allowedcomponents = GetVasComponents(subscribedComponents, out bundlePackages, out mandatoryVasIDs, ref KenanMandatoryVasIds);
            model.KenanMandatoryVasIds = KenanMandatoryVasIds;
            if (Session[SessionKey.RegMobileReg_OrderSummary.ToString()] == null)
            {
                OrderSummaryVM orderSummary = new OrderSummaryVM();
                //orderSummary = ConstructOrderSummary();
                if (orderSummary.PlanName == null)
                {
                    List<string> plans = new List<string>();
                    plans = model.SubscribedComponents.Where(p => p.PackageDesc.ToLower().Contains("mandatory")).Select(p => p.PackageDesc).ToList();

                    if (plans != null && plans.Count > 0)
                    {
                        orderSummary.PlanName = plans[0];

                    }

                }
                packages = model.SubscribedComponents.Select(p => p.compList).ToList();
                if (packages != null && packages.Count > 0)
                {
                    foreach (var package in packages)
                    {
                        price = price + package.Select(c => Convert.ToDecimal(c.Price)).Sum();
                    }
                    orderSummary.MonthlySubscription = price;
                }
                model.MonthlySubscription = orderSummary.MonthlySubscription;
                if (orderSummary.VasVM.VASesName == null || orderSummary.VasVM.VASesName.Count == 0)
                {
                    orderSummary.VasVM.VASesName = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];

                }

                if (Session["mandatoryPkgCompIds"] != null)
                {
                    var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
                    List<Component> components = new List<Component>();
                    using (var proxy = new CatalogServiceProxy())
                    {
                        pgmBdlPckComponents = MasterDataCache.Instance.FilterComponents((List<int>)Session["mandatoryPkgCompIds"]).ToList();
                        components = proxy.ComponentGet(pgmBdlPckComponents.Select(c => c.ChildID)).ToList();
                    }
                    List<string> curVases = new List<string>();
                    if (Session[SessionKey.RegMobileReg_VasNames.ToString()] != null)
                    {
                        curVases = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
                    }

                    for (int compCnt = 0; compCnt < components.Count; compCnt++)
                    {
                        decimal curPrice = 0;
                        if (pgmBdlPckComponents[compCnt] != null)
                        {
                            curPrice = pgmBdlPckComponents[compCnt].Price;
                            orderSummary.MonthlySubscription += curPrice;
                        }
                        curVases.Add(components[compCnt].Name + " (RM " + curPrice.ToString() + ")");

                    }



                    Session[SessionKey.RegMobileReg_VasNames.ToString()] = curVases.Distinct().ToList();
                }

                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderSummary;
            }
            model.MainPackage = "Main Components";
            model.ExtraPackage = "Extra Components";
            model.DataPackage = "Data Components";
            model.PramotionalPackage = "Promotional Components";
            //model.InsurancePackage = "Device Insurance Components";
			model.InsurancePackage = "Mobile SafeDevice";
            model.DeviceFinancingPackage = "Device Zerolution Components";

            return model;
        }


        private List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel> GetPackageDetails(string MSISDN, string Kenancode, out List<Online.Registration.Web.SubscriberICService.componentList> vasComponentList, ref List<string> subscribedComponents)
        {
            Session["OCCmpntList"] = null;
            Online.Registration.Web.SubscriberICService.componentList component = new SubscriberICService.componentList();
            List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<SubscriberICService.componentList>();
            var resp_existing = new SmartGetExistingPackageDetailsResp();
            List<PackageComponentModel> packageComponentModel = new List<PackageComponentModel>();
            List<string> ExistingKenanIDs = new List<string>();
            PackageModel package;
            List<SubscriberICService.Components> components;
            List<SubscriberICService.PackageModel> packages = new List<PackageModel>();

            List<SubscriberICService.PackageModel> retrievedPackages = new List<PackageModel>();
            int tempAdd = 0;

            

            #region Retrieve Rebate Contract Id
            RetrieveComponentInfoResponse objRetrieveComponentInfoRes = null;
            using (var proxy = new KenanServiceProxy())
            {
                RetrieveComponentInfoRequest objRetrieveComponentInfoReq = new RetrieveComponentInfoRequest();
                objRetrieveComponentInfoReq.ExternalID = Session[SessionKey.ExternalID.ToString()].ToString2();
                Session["ComponentInfo"] = objRetrieveComponentInfoRes = proxy.RetrieveComponentInfo(objRetrieveComponentInfoReq);
            }
            #endregion
            using (var proxy = new retrieveServiceInfoProxy())
            {
                packages = proxy.retrievePackageDetls(MSISDN, "23", userName: "", password: "", postUrl: "").ToList();

                IEnumerable<IGrouping<string, PackageModel>> query = packages.GroupBy(k => k.PackageDesc);
                foreach (IGrouping<string, PackageModel> groupItem in query)
                {
                    package = new PackageModel();
                    components = new List<SubscriberICService.Components>();
                    //addPackage.PackageDesc = removePackage.PackageDesc = groupItem.Key;
                    foreach (PackageModel vasItem in groupItem)
                    {
                        tempAdd++;
                        foreach (var packageComp in vasItem.compList)
                        {
                            components.Add(packageComp);
                            component = new componentList();
                            component.packageIdField = vasItem.PackageID;
                            component.componentIdField = packageComp.componentId;
                            componentList.Add(component);
                            if (Session["compId"].ToString2() != packageComp.componentId.ToString())
                            {
                                if (Session["OCCmpntList"].ToString2().Length > 0)
                                {
                                    Session["OCCmpntList"] = Session["OCCmpntList"].ToString2() + "," + packageComp.componentId.ToString();
                                }
                                else
                                {
                                    Session["OCCmpntList"] = packageComp.componentId.ToString();
                                }
                            }
                        }
                        package.PackageDesc = vasItem.PackageDesc;
                        package.PackageID = vasItem.PackageID;

                    }
                    package.compList = components;
                    if (tempAdd > 0)
                        retrievedPackages.Add(package);

                    tempAdd = 0;

                }
            }
            packages = retrievedPackages;
            List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel> listPakcagecomponents = new List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel>();
            Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel compModel = null;
            List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents> vascomponents = new List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents>();
            Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents comp = null;
            long planId = 0;
            long respPlanid = 0;
            string response = string.Empty;
            List<string> mandatoryPkgIds = new List<string>();
            string packageId = "";
            mandatoryPkgIds = packages.Where(p => p.PackageDesc.ToLower().Contains("mandatory")).Select(p => p.PackageID).ToList();
            if (mandatoryPkgIds != null && mandatoryPkgIds.Count > 0)
            {

                packageId = mandatoryPkgIds[0];

            }
            string mandatoryKenancode = string.Empty;
            string planType = "CP";
            foreach (var item in packages)
            {
                using (var proxy_kenan = new RegistrationServiceProxy())
                {
                    if (Session["AccountType"] != null && Session["AccountType"].ToString() == "MISMSec")
                    {
                        planId = proxy_kenan.GetLnkPgmBdlPkgCompIdByKenanCode(packageId, "CP", "SP");
                        if (planId > 0)
                        {
                            //mandatoryKenancode = item.PackageID;
                            respPlanid = planId;
                            planType = "CP";
                        }
                    }
                    if (Session["AccountType"] != null && Session["AccountType"].ToString() == "Suppline")
                    {
                        planId = proxy_kenan.GetLnkPgmBdlPkgCompIdByKenanCode(packageId, "SP", "BP");
                        if (planId > 0)
                        {
                            //mandatoryKenancode = item.PackageID;
                            respPlanid = planId;
                            planType = "CP";
                        }
                    }
                    if (Session["AccountType"] != null && Session["AccountType"].ToString() == "Primary" || Session["AccountType"] != null && Session["AccountType"].ToString() == "MISMPrimary")
                    {

                        planId = proxy_kenan.GetLnkPgmBdlPkgCompIdByKenanCode(packageId, "CP", "BP");
                        planType = "CP";
                        if (planId > 0)
                        {
                            //mandatoryKenancode = item.PackageID;
                            respPlanid = planId;

                        }
                    }
                }
            }
            List<string> groupIds = new List<string>();
            List<string> vasNames = new List<string>();
            if (respPlanid > 0)
                Session["RegMobileReg_PkgPgmBdlPkgCompID"] = respPlanid;
            foreach (var item in packages)
            {
                compModel = new Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel();
                vascomponents = new List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents>();
                compModel.packageActiveDt = item.packageActiveDt;
                compModel.PackageDesc = item.PackageDesc;
                compModel.PackageID = item.PackageID;
                compModel.packageInstId = item.packageInstId;
                compModel.packageInstIdServ = item.packageInstIdServ;
                ExistingKenanIDs.Add(item.PackageID);
                using (var proxy_kenan = new RegistrationServiceProxy())
                {
                    resp_existing = proxy_kenan.ExistingPackageDetails(item.PackageID.ToInt());

                    foreach (var vcomponent in item.compList)
                    {
                        comp =  new Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents();
                        if (resp_existing != null && resp_existing.ExistingPackageDetails != null && resp_existing.ExistingPackageDetails.PlanType != null && resp_existing.ExistingPackageDetails.PlanType.ToUpper() == "PP")
                        {
                            string strRebateExpDate = string.Empty;
                            List<string> lstContractTypes = null;
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                lstContractTypes = proxy.getRebateContractIDbyComponentID(vcomponent.componentId);
                            }
                            if (lstContractTypes != null && lstContractTypes.Count > 0)
                            {
                                foreach (var i in lstContractTypes)
                                {
                                    if (objRetrieveComponentInfoRes != null && objRetrieveComponentInfoRes.Contracts != null && objRetrieveComponentInfoRes.Contracts.Count() > 0)
                                    {
                                        foreach (var contract in objRetrieveComponentInfoRes.Contracts.Where(a => a.ContractType == i))
                                        {
                                            if (contract.EndDate != DateTime.MinValue && contract.EndDate >= DateTime.Now.Date)
                                                strRebateExpDate = "<b style='color:red'>" + "(" + "Rebate Exp Date :" + contract.EndDate.ToString("dd-MMM-yy") + ")" + "</b>";
                                        }

                                    }

                                }
                            }

                            comp = new Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents();
                            comp.componentActiveDt = vcomponent.componentActiveDt;
                            comp.componentDesc = vcomponent.componentDesc + strRebateExpDate;
                            comp.componentId = vcomponent.componentId;
                            comp.componentInstId = vcomponent.componentInstId;
                            comp.componentInstIdServ = vcomponent.componentInstIdServ;
                            comp.componentShortDisplay = vcomponent.componentShortDisplay;
                            subscribedComponents.Add(comp.componentId);

                        }
                        else
                        {
                            comp = new Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents();
                            comp.componentActiveDt = vcomponent.componentActiveDt;
                            comp.componentDesc = vcomponent.componentDesc;
                            comp.componentId = vcomponent.componentId;
                            comp.componentInstId = vcomponent.componentInstId;
                            comp.componentInstIdServ = vcomponent.componentInstIdServ;
                            comp.componentShortDisplay = vcomponent.componentShortDisplay;
                            subscribedComponents.Add(comp.componentId);
                        }
                        if (resp_existing != null && resp_existing.ExistingPackageDetails.PlanType != null)
                        {

                            response = proxy_kenan.GetlnkpgmbdlpkgcompidForKenancomponent(resp_existing.ExistingPackageDetails.PlanType, Convert.ToInt32(item.PackageID), vcomponent.componentId.ToString());
                            if (response != null && response.Contains(","))
                            {
                                comp.GroupId = response.Split(',')[2];
                                comp.Price = response.Split(',')[1];
                                comp.lnkpgmbdlpkgcompId = response.Split(',')[0];
                                comp.Value = Convert.ToInt32(response.Split(',')[3]);
                            }

                        }
                        if (comp.Price == string.Empty || comp.Price == null)
                            comp.Price = "0.00";
						
						//int[] DFKenanCodeNotDisplay = new int[] { 46039, 46040, 46043 };
						
						vascomponents.Add(comp);
						vasNames.Add(vcomponent.componentDesc + " (RM " + comp.Price + ")");

                        //}

                    }
                    compModel.compList = vascomponents;


                }
                listPakcagecomponents.Add(compModel);
                
            }
            Session["existingKenanIDs"] = ExistingKenanIDs;
            if (Session["RegMobileReg_VasNames"] == null)
                Session["RegMobileReg_VasNames"] = vasNames.Distinct().ToList();
            
            vasComponentList = componentList;
            return listPakcagecomponents;
        }

        private List<int> GetDepenedencyComponents(string componentIds)
        {
            List<int> depCompIds = new List<int>();

            using (var proxy = new CatalogServiceProxy())
            {
                depCompIds = proxy.GetAllDependentComponents(Convert.ToInt32(componentIds), 0);
            }

            return depCompIds;
        }

        private List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel> GetRemovedVases(string MSISDN, string Kenancode, string removedVases, ref List<string> removedComponents)
        {

            var resp_existing = new SmartGetExistingPackageDetailsResp();
            List<PackageComponentModel> packageComponentModel = new List<PackageComponentModel>();
            List<string> ExistingKenanIDs = new List<string>();
            List<SubscriberICService.PackageModel> packages = new List<PackageModel>();
            List<SubscriberICService.Components> components;
            int tempAdd = 0;
            PackageModel package;
            List<PackageModel> retrievedPackages = new List<PackageModel>();
            using (var proxy = new retrieveServiceInfoProxy())
            {
                packages = proxy.retrievePackageDetls(MSISDN, "23", userName: string.Empty, password: string.Empty, postUrl: string.Empty).ToList();

                IEnumerable<IGrouping<string, PackageModel>> query = packages.GroupBy(k => k.PackageDesc);
                foreach (IGrouping<string, PackageModel> groupItem in query)
                {
                    package = new PackageModel();
                    components = new List<SubscriberICService.Components>();
                    //addPackage.PackageDesc = removePackage.PackageDesc = groupItem.Key;
                    foreach (PackageModel vasItem in groupItem)
                    {
                        tempAdd++;
                        foreach (var packageComp in vasItem.compList)
                        {
                            components.Add(packageComp);
                        }
                        package.PackageDesc = vasItem.PackageDesc;
                        package.PackageID = vasItem.PackageID;

                    }
                    package.compList = components;
                    if (tempAdd > 0)
                        retrievedPackages.Add(package);

                    tempAdd = 0;

                }
            }
            long planId = 0;
            long respPlanid = 0;
            using (var proxy_kenan = new RegistrationServiceProxy())
            {
                foreach (var item in packages)
                {
                    resp_existing = proxy_kenan.ExistingPackageDetails(item.PackageID.ToInt());

                    planId = proxy_kenan.GetLnkPgmBdlPkgCompIdByKenanCode(item.PackageID, "CP");
                    if (planId > 0)
                    {

                        respPlanid = planId;

                    }
                    else
                    {

                        planId = proxy_kenan.GetLnkPgmBdlPkgCompIdByKenanCode(item.PackageID, "SP");

                        if (planId > 0)
                        {

                            respPlanid = planId;

                        }
                    }
                }
            }
            List<string> vasNames = new List<string>();
            if (Session[SessionKey.RegMobileReg_VasNames.ToString()] != null)
            {
                vasNames = (List<string>)Session[SessionKey.RegMobileReg_VasNames.ToString()];
            }
            if (respPlanid > 0)
                Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = respPlanid;
            List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel> listPakcagecomponents = new List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel>();
            Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel compModel = null;
            List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents> vascomponents = new List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents>();
            Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents comp = null;
            string response = string.Empty;
            foreach (var vas in removedVases.Split(','))
            {
                if (vas != string.Empty)
                {



                    foreach (var item in retrievedPackages)
                    {
                        compModel = new Online.Registration.Web.Controllers.AddRemoveVASController.ARVASPackageComponentModel();
                        vascomponents = new List<Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents>();
                        compModel.packageActiveDt = item.packageActiveDt;
                        compModel.PackageDesc = item.PackageDesc;
                        compModel.PackageID = item.PackageID;
                        compModel.packageInstId = item.packageInstId;
                        compModel.packageInstIdServ = item.packageInstIdServ;
                        ExistingKenanIDs.Add(item.PackageID);
                        using (var proxy_kenan = new RegistrationServiceProxy())
                        {
                            resp_existing = proxy_kenan.ExistingPackageDetails(item.PackageID.ToInt());

                            foreach (var component in item.compList)
                            {
                                if (component.componentId == vas)
                                {
                                    comp = new Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents();
                                    comp.componentActiveDt = component.componentActiveDt;
                                    comp.componentDesc = component.componentDesc;
                                    comp.componentId = component.componentId;
                                    comp.componentInstId = component.componentInstId;
                                    comp.componentInstIdServ = component.componentInstIdServ;
                                    comp.componentShortDisplay = component.componentShortDisplay;
                                    removedComponents.Add(component.componentId);
                                    if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] != null && resp_existing != null && resp_existing.ExistingPackageDetails != null)
                                    {
                                        if (resp_existing.ExistingPackageDetails.PlanType != null)
                                        {
                                            response = proxy_kenan.GetlnkpgmbdlpkgcompidForKenancomponent(resp_existing.ExistingPackageDetails.PlanType, Convert.ToInt32(item.PackageID), component.componentId.ToString());
                                            if (response != null && response.Contains(","))
                                            {
                                                comp.GroupId = response.Split(',')[2];
                                                comp.Price = response.Split(',')[1];
                                                comp.lnkpgmbdlpkgcompId = response.Split(',')[0];
                                            }
                                        }
                                    }
                                    vascomponents.Add(comp);

                                    //get dependency components for each of the unselected components and remove them
                                    using (var catProxy = new CatalogServiceProxy())
                                    {
                                        List<int> depCompIds = new List<int>();

                                        List<PgmBdlPckComponent> pgmComponents = new List<PgmBdlPckComponent>();
                                        if (comp.lnkpgmbdlpkgcompId != null)
                                        {
                                            var depcomp = new Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents();
                                            depCompIds = GetDepenedencyComponents(comp.lnkpgmbdlpkgcompId.ToString());
                                            
                                            if (depCompIds != null && depCompIds.Count > 0)
                                            {
                                                pgmComponents = MasterDataCache.Instance.FilterComponents(depCompIds).ToList();
                                                List<Component> comps = new List<Component>();
                                                comps = catProxy.ComponentGet(pgmComponents.Select(a => a.ChildID)).ToList();
                                                int i = 0;
                                                foreach (var dComp in pgmComponents)
                                                {
                                                    //commented to fix issue, when data component is removed its associtaed dependency component has to be removed
                                                    if (item.compList.Select(c => c.componentId).ToList().Contains(dComp.KenanCode))
                                                    {

                                                        if (comps != null && comps.Count > 0)
                                                        {
                                                            depcomp = new Online.Registration.Web.Controllers.AddRemoveVASController.ARVASComponents();
                                                            depcomp.componentId = dComp.KenanCode;
                                                            depcomp.componentDesc = comps[i].Description;
                                                            depcomp.Price = dComp.Price.ToString();
                                                            if (!removedComponents.Contains(dComp.KenanCode))
                                                            {
                                                                vascomponents.Add(depcomp);
                                                                vasNames.Remove(depcomp.componentDesc + " (RM " + depcomp.Price + ")");
                                                                removedComponents.Add(dComp.KenanCode);
                                                            }
                                                        }

                                                    }

                                                    i++;

                                                }



                                            }
                                        }
                                    }
                                }
                            }
                            if (vascomponents != null && vascomponents.Count > 0)
                                compModel.compList = vascomponents;


                        }
                        if (vascomponents != null && vascomponents.Count > 0)
                            listPakcagecomponents.Add(compModel);
                    }
                }
            }

            Session[SessionKey.RegMobileReg_VasNames.ToString()] = vasNames;
            return listPakcagecomponents;
        }

        #endregion Private Methods

        [HttpPost]
        public string retrievePenaltyCRP(string addedVas, string removedVas, bool changeFlag = true)
        {
            decimal strPenalty = 0.00M;
            var lstRebateContracts = new List<RebateDataContractComponents>();
            RetrieveComponentInfoResponse onjRes = new RetrieveComponentInfoResponse();
            var lstRebatePenality = new List<RegRebateDatacontractPenalty>();
            var RegRebatePenalty = new RegRebateDatacontractPenalty();
            string[] strAccountDtls = null;
            using (var proxy = new KenanServiceProxy())
            {
                if (Session["SelectAccountDtls"] != null)
                {
                    strAccountDtls = Session["SelectAccountDtls"].ToString().Split(',');
                    RetrieveComponentInfoRequest objReq = new RetrieveComponentInfoRequest();
                    objReq.ExternalID = strAccountDtls[1].ToString2();
                    onjRes = proxy.RetrieveComponentInfo(objReq);
                }
            }

            if (onjRes.Contracts != null && onjRes.Contracts.Count() > 0)
            {
                var itemFilter = from e in onjRes.Contracts
                                 where e.EndDate.Date > DateTime.Now.Date
                                 select e;
                lstRebateContracts = MasterDataCache.Instance.RebateContractComponents.ToList();
                if (itemFilter != null && itemFilter.Count() > 0)
                {
                    foreach (var i in itemFilter)
                    {
                        if (lstRebateContracts.Where(a => a.ContractType == i.ContractType && a.Type.ToUpper() == "DC") != null && lstRebateContracts.Where(a => a.ContractType == i.ContractType && a.Type.ToUpper() == "DC").Count() > 0)
                        {
                            var item = lstRebateContracts.Where(a => a.ContractType == i.ContractType && a.Type == "DC").SingleOrDefault();
                            if (item != null)
                            {
                                RegRebatePenalty.ID = item.ID;
                                RegRebatePenalty.ComponetID = item.ComponentID;
                                RegRebatePenalty.RegID = 0;
                                RegRebatePenalty.Penalty = Convert.ToDecimal(item.penalty);
                                RegRebatePenalty.Active = item.Status;
                                RegRebatePenalty.nrcID = item.nrcID;
                                RegRebatePenalty.CreateDT = DateTime.Now;
                                lstRebatePenality.Add(RegRebatePenalty);
                                if (!string.IsNullOrEmpty(item.penalty) && Convert.ToDecimal(item.penalty) > 0)
                                {
                                    lstRebatePenality.Add(RegRebatePenalty);
                                    strPenalty = strPenalty + Convert.ToDecimal(item.penalty);
                                }
                            }

                        }
                        //getting penalty for Reducing contracts type
                        else if (lstRebateContracts.Where(a => a.ContractType == i.ContractType && a.Type.ToUpper() == "RP") != null && lstRebateContracts.Where(a => a.ContractType == i.ContractType && a.Type.ToUpper() == "RP").Count() > 0)
                        {
                            #region getting Penalty for contract type 6
                            string strCompID = string.Empty;
                            string strNrcID = string.Empty;
                            List<SubscriberICService.retrievePenalty> penaltybyMSISDN = new List<SubscriberICService.retrievePenalty>();
                            using (var Smartproxy = new retrieveSmartServiceInfoProxy())
                            {
                                penaltybyMSISDN = Smartproxy.retrievePenalty(strAccountDtls[1].ToString2(), ConfigurationManager.AppSettings["KenanMSISDN"], strAccountDtls[2].ToString2(), strAccountDtls[3].ToString2(), userName: Properties.Settings.Default.retrieveAcctListByICResponseUserName,
                                    password: Properties.Settings.Default.retrieveAcctListByICResponsePassword);
                            }
                            if (penaltybyMSISDN != null && penaltybyMSISDN.Count > 0)
                            {
                                foreach (var item in penaltybyMSISDN)
                                {
                                    RegRebatePenalty = new RegRebateDatacontractPenalty();
                                    if (item != null)
                                    {
                                        if (!string.IsNullOrEmpty(item.ContractName))
                                            strCompID = lstRebateContracts.Where(a => a.ContractName == item.ContractName).Select(a => a.ComponentID).SingleOrDefault();
                                        if (!string.IsNullOrEmpty(item.ContractName))
                                            strNrcID = lstRebateContracts.Where(a => a.ContractName == item.ContractName).Select(a => a.nrcID).SingleOrDefault();
                                        // RegRebatePenalty.ID = item.ID;
                                        RegRebatePenalty.ComponetID = strCompID;
                                        RegRebatePenalty.RegID = 0;
                                        RegRebatePenalty.Penalty = Convert.ToDecimal(item.penalty);
                                        RegRebatePenalty.Active = true;
                                        RegRebatePenalty.nrcID = strNrcID;
                                        RegRebatePenalty.CreateDT = DateTime.Now;
                                        if (!string.IsNullOrEmpty(strNrcID) && !string.IsNullOrEmpty(item.penalty) && Convert.ToDecimal(item.penalty) > 0)
                                        {
                                            lstRebatePenality.Add(RegRebatePenalty);
                                            strPenalty = strPenalty + Convert.ToDecimal(item.penalty);
                                        }
                                    }
                                }

                            }
                            #endregion

                        }
                    }
                }
            }
            Session["lstRebatePenality"] = lstRebatePenality;
            Session["RebatePenalty"] = strPenalty;
            return strPenalty.ToString();
        }

        public int PrintDMEOrder(int regId)
        {
            var UserDMEPrint = new UserDMEPrint();
            UserDMEPrint.RegID = regId;
            UserDMEPrint.UserID = Util.SessionAccess.UserID;
            var status = Util.SaveUserDMEPrint(UserDMEPrint);
            return status;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IMEI"></param>
        /// <param name="regID"></param>
        /// <Author>Chetan Channe</Author>
        /// <returns></returns>
        [HttpPost]
        public string CheckIMEIStatus(string IMEI, string regID)
        {
            //Code by chetan moved the dynamic proxy call to integration service

            string resStatus = string.Empty;

            string regszArticleID = string.Empty;

            string regszStatus = string.Empty;

            string dbArticleId = string.Empty;

            var reg = new DAL.Models.Registration();

            string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
            string storeId = Util.SessionAccess.User.Org.OrganisationId;

            try
            {

                using (var proxy = new DynamicServiceProxy())
                {

                    resStatus = proxy.GetSerialNumberStatus(storeId, IMEI, wsdlUrl);

                    //Should Uncomment after POS updation in production

                    if (!string.IsNullOrEmpty(resStatus))
                    {

                        regszArticleID = resStatus.Split('|')[0];

                        regszStatus = resStatus.Split('|')[1];

                        using (var regproxy = new RegistrationServiceProxy())
                        {

                            reg = regproxy.RegistrationGet(Convert.ToInt32(regID));

                            dbArticleId = reg.ArticleID;

                        }

                    }

                }

            }

            catch (Exception ex)
            {

                WebHelper.Instance.LogExceptions(this.GetType(), ex);

                RedirectToAction("StoreError", "HOME");

            }

            //END HERE

            //Should Uncomment after POS updation in production

            if ((!string.IsNullOrEmpty(regszStatus) || regszStatus != "") && (regszStatus == ConfigurationManager.AppSettings["IMEIStatus"]) && (dbArticleId == regszArticleID))

            //Should comment after POS updation in production

            //if ((!string.IsNullOrEmpty(resStatus) || resStatus != "") && (resStatus == ConfigurationManager.AppSettings["IMEIStatus"]))
            {

                resStatus = "True";

                return resStatus;

            }

            //Please don't assign true value for resStatus always as true

            //it can leads to order failure in kenan bacause of the wrong IMEI Number

            //Comment added and resStatus changed by chetan channe

            resStatus = "False";

            return resStatus;

        }

        private void resetAdvancePaymentPrice(ref DropFourObj dropObj)
        {
            dropObj.mainFlow.orderSummary.MalayPlanAdv = 0;
            dropObj.mainFlow.orderSummary.MalyPlanDeposit = 0;
            dropObj.mainFlow.orderSummary.MalyDevAdv = 0;
            dropObj.mainFlow.orderSummary.MalayDevDeposit = 0;
            dropObj.mainFlow.orderSummary.OthPlanAdv = 0;
            dropObj.mainFlow.orderSummary.OthPlanDeposit = 0;
            dropObj.mainFlow.orderSummary.OthDevAdv = 0;
            dropObj.mainFlow.orderSummary.OthDevDeposit = 0;
            dropObj.mainFlow.orderSummary.unwaivedAdvancePaymentDeposit = new AdvDepositPrice();
        }

        private void SetAgeSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsAgeCheckFailed.ToString()] = resp.IsAgeCheckFailed;
            Session[SessionKey.RegMobileReg_AgeCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()] = resp.IsAgeCheckFailed ? "AF" : "AS";
        }

        private void SetDDMFSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsDDMFCheckFailed.ToString()] = resp.IsDDMFCheckFailed;
            Session[SessionKey.RegMobileReg_DDMFCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()] = resp.IsDDMFCheckFailed ? "DF" : "DS";
        }

        private void SetAddressSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsAddressCheckFailed.ToString()] = resp.IsAddressCheckFailed;
            Session[SessionKey.RegMobileReg_AddressCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()] = resp.IsAddressCheckFailed ? "ADF" : "ADS";
        }

        private void SetTotalLineCheckSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsTotalLineCheckFailed.ToString()] = resp.IsTotalLineCheckFailed;
            Session[SessionKey.RegMobileReg_TotalLineCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()] = resp.IsTotalLineCheckFailed ? "TLF" : "TLS";
        }

        private void SetPrincipalLineCheckSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsPrincipalLineCheckFailed.ToString()] = resp.IsPrincipleLineCheckFailed;
            Session[SessionKey.RegMobileReg_PricinpalLineCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()] = resp.IsPrincipleLineCheckFailed ? "PLF" : "PLS";
        }

        private void SetContractCheckSession(BusinessRuleResponse resp)
        {
            Session[SessionKey.RegMobileReg_IsContractCheckFailed.ToString()] = resp.IsContractCheckFailed;
            Session[SessionKey.RegMobileReg_ContractCheckResultMessage.ToString()] = resp.ResultMessage;
            Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()] = resp.IsContractCheckFailed ? "CF" : "CS";
        }

        public JsonResult SearchAccessory(int searchBy, string searchText, bool fromOutrightPurchase = false)
        {
            var result = new Dictionary<string, List<AccessoryVM>>();
            var jsonData = new List<AccessoryVM>();
            var deviceVM = Session["model"] as DeviceVM;
            var msisdn = Session[SessionKey.ExternalID.ToString()].ToString2();
            var hasAccessoryFinancingContract = WebHelper.Instance.CheckCurrentAFContract(msisdn);
            //var showAccFinancingOffer = hasAccessoryFinancingContract
            //                                ? deviceVM != null && (deviceVM.subAction == Constants.TERMINATEASSIGN_ACC_ONLY || deviceVM.subAction == Constants.TERMINATEASSIGN_ALL)
            //                                : true;
            var showAccFinancingOffer = !hasAccessoryFinancingContract || (hasAccessoryFinancingContract && deviceVM != null && deviceVM.subAction != Constants.TERMINATEASSIGN_DEVICE_ONLY);

            if ((searchBy == Constants.EAN_ID.ToInt() || searchBy == Constants.ARTICLE_ID.ToInt() || searchBy == Constants.NAME.ToInt() || searchBy == Constants.SERIAL.ToInt()) && !string.IsNullOrEmpty(searchText.Trim()))
            {
                jsonData = WebHelper.Instance.getAccessoryData(searchBy, searchText, showAccFinancingOffer: showAccFinancingOffer, fromOutrightPurchase: fromOutrightPurchase);
                Session[SessionKey.Available_Accessory.ToString()] = jsonData;
            }

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}
