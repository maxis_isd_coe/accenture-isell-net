﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.RegistrationSvc;
using SNT.Utility;
using System.Configuration;
using Online.Registration.Web.CommonEnum;
using log4net;
using System.Web.Script.Serialization;


namespace Online.Registration.Web.Controllers
{
    [SessionAuthenticationAttribute]
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class ExtraTenController : Controller
    {
        #region Member Declaration
        private static readonly ILog Logger = LogManager.GetLogger(typeof(RegistrationController));
        #endregion
        [Authorize]
        [HttpGet]
        [DoNoTCache]
        public ActionResult AccountsList()
        {
            ExtraTenAccounts Accounts = new ExtraTenAccounts();
            retrieveAcctListByICResponse AcctListByICResponse = null;
            Online.Registration.Web.SubscriberICService.SubscriberICServiceClient icserviceobj = null;
            IList<Online.Registration.Web.SubscriberICService.PackageModel> packages = null;
            ExtraTenVM objExt = null;
            try
            {
                #region "code commented for "
                if (TempData["Result"] == null ) Session["ExtraTenUpdate"] = "false";
                if (SessionManager.Get("PPIDInfo") != null)
                {
                    AcctListByICResponse = (retrieveAcctListByICResponse)SessionManager.Get("PPIDInfo");


                    using (var proxy = new UserServiceProxy())
                    {
                        proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.ManageExtraTen, "Manage Extra Ten", "", "");
                    }

                    foreach (string acctExtID in AcctListByICResponse.itemList.Where(x => x.ServiceInfoResponse != null).Select(x => x.AcctExtId).Distinct())
                    {
                        foreach (var v in AcctListByICResponse.itemList.Where(x => x.AcctExtId == acctExtID))
                        {
                            icserviceobj = new Online.Registration.Web.SubscriberICService.SubscriberICServiceClient();
                            packages = icserviceobj.retrievePackageDetls(v.ExternalId
                                        , System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"]
                                        , userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName
                                        , password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword
                                        , postUrl: "");
                            foreach (var _package in packages)
                            {
                                foreach (var comp in _package.compList)
                                {
                                    if (comp.componentId == ConfigurationManager.AppSettings.Get("ExtraTenKenanCode").ToString2()) //"41389" extraten kenan code
                                    {
                                        objExt = new ExtraTenVM();
                                        objExt.Account = v.Account;
                                        objExt.Customer = v.Customer;
                                        objExt.Components.componentActiveDt = string.IsNullOrEmpty(comp.componentActiveDt) ? string.Empty : comp.componentActiveDt;
                                        objExt.Components.componentDesc = string.IsNullOrEmpty(comp.componentDesc) ? string.Empty : comp.componentDesc;
                                        objExt.Components.componentId = string.IsNullOrEmpty(comp.componentId) ? string.Empty : comp.componentId;
                                        objExt.Components.componentInactiveDt = string.IsNullOrEmpty(comp.componentInactiveDt) ? string.Empty : comp.componentInactiveDt;
                                        objExt.Components.componentInstId = string.IsNullOrEmpty(comp.componentInstId) ? string.Empty : comp.componentInstId;
                                        objExt.Components.componentInstIdServ = string.IsNullOrEmpty(comp.componentInstIdServ) ? string.Empty : comp.componentInstIdServ;
                                        objExt.Components.componentShortDisplay = string.IsNullOrEmpty(comp.componentShortDisplay) ? string.Empty : comp.componentShortDisplay;
                                        //objExt.CorriderID = "40102";// THIS IS THE CONFIGURATION VALUE
                                        objExt.CorriderID = ConfigurationManager.AppSettings.Get("ExtraTenCorridorID");
                                        objExt.AcctExtId = v.AcctExtId;
                                        objExt.ExternalId = v.ExternalId;
                                        #region "retrive ExtraTenNumbers from Kenan Service Call"
                                        using (var proxy = new retrieveServiceInfoProxy())
                                        {
                                            Online.Registration.Web.SubscriberICService.ExtraTenRetreivalResponse resp = proxy.RetrieveExtraTenForAddEdit(CorridorId: objExt.CorriderID, ExternalId: objExt.ExternalId);
                                            if (!ReferenceEquals(resp, null) && !(ReferenceEquals(resp.ExtraTenInfo, null)))
                                            {
                                                if (!ReferenceEquals(resp.Code, null) && resp.Code == "0")
                                                {
                                                    objExt.ExtraTenMSISDNDetails.ChargeAmt = !ReferenceEquals(resp.ExtraTenInfo.chargeAmtField, null) ? resp.ExtraTenInfo.chargeAmtField : string.Empty;
                                                    objExt.ExtraTenMSISDNDetails.Counter = !ReferenceEquals(resp.ExtraTenInfo.counterField, null) ? resp.ExtraTenInfo.counterField.ToString() : string.Empty;
                                                    objExt.ExtraTenMSISDNDetails.MsgCode = !ReferenceEquals(resp.ExtraTenInfo.msgCodeField, null) ? resp.ExtraTenInfo.msgCodeField.ToString() : string.Empty;
                                                    objExt.ExtraTenMSISDNDetails.MsgDesc = !ReferenceEquals(resp.ExtraTenInfo.msgDescField, null) ? resp.ExtraTenInfo.msgDescField.ToString() : string.Empty;
                                                    objExt.ExtraTenMSISDNDetails.RetrivedMSISDNList = new List<string>();
                                                    for (int i = 0; i < resp.ExtraTenInfo.fnfDocField.Count(); i++)
                                                    {
                                                        objExt.ExtraTenMSISDNDetails.RetrivedMSISDNList.Add(!ReferenceEquals(resp.ExtraTenInfo.fnfDocField[i], null) ? resp.ExtraTenInfo.fnfDocField[i].msisdnField.ToString() : string.Empty);
                                                    }
                                                }
                                            }
                                         
                                        }
                                        #endregion
                                        Accounts.ExtraTenAccounts1.Add(objExt);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(ex);

            }
            finally
            {
                icserviceobj = null;
                AcctListByICResponse = null;
            }

            return View(Accounts);
        }

        [HttpPost]
        [Authorize]
        [DoNoTCache]
        public ActionResult UpdateExtraTenDetails()
        {
            bool isError = false;
            string ErrorMsg = string.Empty;
            string kenanComponentID = ConfigurationManager.AppSettings.Get("ExtraTenKenanCode");
            int registrationID = 0;
            Session["ExtraTenUpdate"] = "true";
            string corridorID = ConfigurationManager.AppSettings.Get("ExtraTenCorridorID");
            Online.Registration.Web.SubscriberICService.ExtraTenRetreivalResponse _ServiceRetriveResp = null;
            string rawMsisdn = Request.Form["newMSISDN"];
            List<extraTenObject> extraTDetails = new List<extraTenObject>();
            extraTDetails.Add(new extraTenObject() { ExtraTenMSISDN = Request.Form["extMsisdn"], OperationType = Request.Form["OperationType"], OldMSISDN = Request.Form["oldMSISDN"], NewMSISDN = string.Join("", Request.Form["newMSISDN"].Split('-')) });
            try
            {
                
                if (!ReferenceEquals(extraTDetails, null) && extraTDetails.Count > 0)
                {
                    using (var proxy = new retrieveServiceInfoProxy())
                    {
                        Online.Registration.Web.SubscriberICService.typeProcessExtraTenRequest request = new Online.Registration.Web.SubscriberICService.typeProcessExtraTenRequest();

                        foreach (var item in extraTDetails)
                        {
                            if (string.IsNullOrEmpty(item.ExtraTenMSISDN.Trim()) && string.IsNullOrEmpty(item.OldMSISDN.Trim()) && string.IsNullOrEmpty(item.NewMSISDN.Trim()) && (item.OperationType.ToUpper().Contains("UPDATE")))
                            {
                                isError = true;

                                Logger.InfoFormat("Controller{0}:Method{1}### ExternalMSISDN:{2} OldMSISDN:{3} NewMSISDN:{4} ", "ExtraTen", "UpdateExtraTenDetails", item.ExtraTenMSISDN == null ? "null" : item.ExtraTenMSISDN, item.OldMSISDN == null ? "null" : item.OldMSISDN, item.NewMSISDN == null ? "null" : item.NewMSISDN);
                                if (item.ExtraTenMSISDN.Trim() == string.Empty)
                                    ErrorMsg = "External MSISDN cannot be Empty";
                                if (item.OldMSISDN.Trim() == string.Empty)
                                    ErrorMsg = "Old MSISDN cannot be Empty";
                                if (item.NewMSISDN.Trim() == string.Empty)
                                    ErrorMsg = "New MSISDN cannot be Empty";
                            }
                            else if (string.IsNullOrEmpty(item.ExtraTenMSISDN.Trim()) && string.IsNullOrEmpty(item.OldMSISDN.Trim()) && (item.OperationType.ToUpper().Contains("DELETE")))
                            {
                                isError = true;
                                Logger.InfoFormat("Controller{0}:Method{1}### ExternalMSISDN:{2} OldMSISDN:{3}  ", "ExtraTen", "UpdateExtraTenDetails", item.ExtraTenMSISDN == null ? "null" : item.ExtraTenMSISDN, item.OldMSISDN == null ? "null" : item.OldMSISDN);
                                if (item.ExtraTenMSISDN.Trim() == string.Empty)
                                    ErrorMsg = "External MSISDN cannot be Empty";
                                if (item.OldMSISDN.Trim() == string.Empty)
                                    ErrorMsg = "MSISDN cannot be Empty";
                            }
                            else if (string.IsNullOrEmpty(item.ExtraTenMSISDN.Trim()) && string.IsNullOrEmpty(item.NewMSISDN.Trim()) && item.OperationType.ToUpper().Contains("NEW"))
                            {
                                isError = true;
                                Logger.InfoFormat("Controller{0}:Method{1}### ExternalMSISDN:{2}  NewMSISDN:{3} ", "ExtraTen", "UpdateExtraTenDetails", item.ExtraTenMSISDN == null ? "null" : item.ExtraTenMSISDN, item.NewMSISDN == null ? "null" : item.NewMSISDN);
                                if (item.ExtraTenMSISDN.Trim() == string.Empty)
                                    ErrorMsg = "External MSISDN cannot be Empty";
                                if (item.NewMSISDN.Trim() == string.Empty)
                                    ErrorMsg = "New MSISDN cannot be Empty";
                            }
                            if (!isError)
                            {
                                if (item.OperationType.ToUpper() == "UPDATE") //update operation
                                {
                                    request.actionTypeField = "REPLACE";
                                    request.componentIdField = kenanComponentID;
                                    request.oldFnfMsisdnField = item.OldMSISDN.Trim();
                                    request.newFnfMsisdnField = item.NewMSISDN.Trim();
                                    request.reqTypeField = "EXTRA5"; //need clarification on this**************
                                    request.externalIdField = item.ExtraTenMSISDN.Trim();
                                }
                                else if (item.OperationType.ToUpper() == "DELETE") //delete operation
                                {
                                    request.actionTypeField = "DELETE";
                                    request.componentIdField = kenanComponentID;
                                    request.externalIdField = item.ExtraTenMSISDN.Trim();
                                    request.reqTypeField = "EXTRA5"; //need clarification on this**************
                                    request.oldFnfMsisdnField = item.OldMSISDN;
                                }
                                else// ADD operation
                                {
                                    _ServiceRetriveResp = proxy.RetrieveExtraTenForAddEdit(CorridorId: corridorID, ExternalId: item.ExtraTenMSISDN);
                                    Int32 recordsCounter = 0;
                                    try
                                    {
                                        if (_ServiceRetriveResp != null)
                                            recordsCounter = _ServiceRetriveResp.ExtraTenInfo == null ? 0 : Convert.ToInt32(_ServiceRetriveResp.ExtraTenInfo.fnfDocField.Count());
                                    }
                                    catch
                                    {
                                        recordsCounter = 0;
                                    }
                                    if (recordsCounter < ConfigurationManager.AppSettings.Get("ExtraTenMaxMSISDNs").ToInt())
                                    {
                                        request.actionTypeField = "ADD";
                                        request.componentIdField = kenanComponentID;
                                        request.reqTypeField = "EXTRA5"; //need clarification on this**************
                                        request.externalIdField = item.ExtraTenMSISDN.Trim();
                                        request.newFnfMsisdnField = item.NewMSISDN;
                                    }
                                    else
                                    {
                                        isError = true;
                                        ErrorMsg = string.Format("{0} New MSISDN allows only {1} numbers, Additional Numbers are not allowed", item.OperationType, ConfigurationManager.AppSettings.Get("ExtraTenMaxMSISDNs").ToInt());
                                        Logger.InfoFormat("Controller{0}:Method{1} Maximum {2} Add New MSISDN counter Exited", "ExtraTenController", "UpdateExtraTenDetails", recordsCounter);
                                    }

                                }
                                if (!isError)
                                {
                                    Logger.InfoFormat("Controller{0}:Method{1} Request for ProcessExtraTenForAddEdit {2}", "ExtraTenController", "UpdateExtraTenDetails", request);
                                    var response = proxy.ProcessExtraTenForAddEdit(request, registrationID);
                                    if (response.Success)
                                    {
                                        item.RegID = registrationID.ToString2();
                                        item.Status = response.Code;// need to check which field to add for success response status
                                        item.Status = response.ExtraTenProcessResponse.msgCodeField; // need to check which field to add for success response status
                                        item.StatusReason = response.ExtraTenProcessResponse.msgDescField; // need to check which field to add for success response status
                                        item.CreateDate = System.DateTime.Now;
                                        item.CreatedyBy = Util.SessionAccess.UserName;
                                        item.UpdateDate = System.DateTime.Now;
                                        item.UpdatedBy = Util.SessionAccess.UserName;
                                        ExtraTenAddEditReq reqForm = new ExtraTenAddEditReq();
                                        reqForm.ExtraTenAddEditDetails = new lnkExtraTenAddEditDetails()
                                        {
                                            CreateDate = item.CreateDate,
                                            CreatedyBy = item.CreatedyBy,
                                            RegID = Convert.ToInt32(item.RegID),
                                            Status = item.Status,
                                            StatusReason = item.StatusReason,
                                            ExtraTenMSISDN = item.ExtraTenMSISDN,
                                            NewMSISDN = item.NewMSISDN,
                                            OldMSISDN = item.OldMSISDN,
                                            OperationType = item.OperationType,
                                            OperationDesc = item.OperationType,
                                            UpdateDate = item.UpdateDate,
                                            UpdatedBy = item.UpdatedBy
                                        };
                                        using (var regproxy = new RegistrationServiceProxy())
                                        {
                                            item.ID = regproxy.SaveExtraTenAddEditDetails(reqForm).ToString2();
                                        }
                                        var serResponse = proxy.RetrieveExtraTenForAddEdit(corridorID, item.ExtraTenMSISDN);
                                        if (serResponse != null)
                                        {
                                            item.CounterValue = serResponse.ExtraTenInfo == null ? "0" : serResponse.ExtraTenInfo.counterField;
                                        }
                                    }
                                    else
                                    {
                                        isError = true;
                                        if (!ReferenceEquals(response, null))
                                        {
                                            ErrorMsg = string.Format("{0} Operation Failed, Error Message: {1}.", item.OperationType,response.Message);
                                        }
                                        else
                                        {
                                            ErrorMsg = string.Format("{0} Operation failed.", item.OperationType);
                                        }
                                        Logger.ErrorFormat("Controller{0}:Method{1}### Response Object returned NUll", "ExtraTenController", "UPdateExtraTenDetails");
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    isError = true;
                    Logger.Info("Extra Ten Details are empty or Null");
                    ErrorMsg = "No details to update or delete";
                }
            }
            catch (Exception ex)
            {
                isError = true;
                ErrorMsg = "Could not able to submit details, Please try later or contact administrator";
                Logger.ErrorFormat("Controller{0}:Method{1}### Error:{2}", "ExtraTenController", "UPdateExtraTenDetails", ex);
                //var jsonErrObject = new { isError, ErrorMsg, extraTDetails };
                //return Json(jsonErrObject);
            }

            string status = extraTDetails.Select(a => a.Status).SingleOrDefault();
            string statusReason = extraTDetails.Select(a => a.StatusReason).SingleOrDefault();
            string ExternalMsisdn = extraTDetails.Select(a => a.ExtraTenMSISDN).SingleOrDefault();
            string OldMSISDN = extraTDetails.Select(a => a.OldMSISDN).SingleOrDefault();
            string NewMSISDN = rawMsisdn; //extraTDetails.Select(a => a.NewMSISDN).SingleOrDefault();
            string OperationType = extraTDetails.Select(a => a.OperationType).SingleOrDefault();
            if (isError || status != "0")
            {
                status = isError ? "-1" : status;
                statusReason = ErrorMsg.Length > 0 ? ErrorMsg : statusReason;
                statusReason = statusReason.Replace("ERR_MSG =", string.Empty);
                if (OperationType.ToUpper() == "DELETE")
                    statusReason = statusReason.Replace("%value reqTypeName%", OldMSISDN);
                else if (OperationType.ToUpper() == "UPDATE")
                    statusReason = statusReason.Replace("%value reqTypeName%", NewMSISDN);
                else
                    statusReason = statusReason.Replace("%value reqTypeName%", NewMSISDN);

            }

            string ETCounter = extraTDetails.Select(a => a.CounterValue).SingleOrDefault();
            var responseObject = new ExtraTenResponseVM() { 
            isError = isError,
            ErrorMsg = statusReason,
            status = status,
            statusReason = statusReason,
            ExternalMsisdn = ExternalMsisdn,
            OldMSISDN = OldMSISDN,
            NewMSISDN = NewMSISDN,
            OperationType = OperationType,
            ETCounter = ETCounter 
            };
            TempData["Result"] = responseObject;
            System.Threading.Thread.Sleep(10000);
            return RedirectToAction("AccountsList", "ExtraTen");
            //var jsonobject = new { isError, ErrorMsg, extraTDetails };
            //return Json(jsonobject);
        }
    }

    [Serializable]
    public class ExtraTenResponseVM
    {
        public bool isError { get; set; }
        public string ErrorMsg { get; set; }
        public string status { get; set; }
        public string statusReason { get; set; }
        public string ExternalMsisdn { get; set; }
        public string OldMSISDN { get; set; }
        public string NewMSISDN { get; set; }
        public string OperationType { get; set; }
        public string ETCounter { get; set; }
    }
}
