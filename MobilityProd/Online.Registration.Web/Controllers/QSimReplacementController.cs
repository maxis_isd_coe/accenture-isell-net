﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.CMSSSvc;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.ContentRepository;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.Models;
using Online.Registration.Web.Properties;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.ViewModels;
using SNT.Utility;
using Online.Registration.Web.KenanSvc;
using CMSS = Online.Registration.Web.CMSSSvc;
using log4net.Repository.Hierarchy;
using log4net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.IO;

namespace Online.Registration.Web.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class QSimReplacementController : Controller
    {
        #region Member Declaration
        private static readonly ILog Logger = LogManager.GetLogger(typeof(RegistrationController));
        #endregion

        #region Actions
        [Authorize]
        public ActionResult AccountDetails()
        {
            string msisdn = string.Empty;
            string parameter = Request.QueryString["type"].ToString2();
            string[] array = parameter.Split(',');
            msisdn = array.Count() > 1 ? array[1] : string.Empty;

            Session[SessionKey.AccountCategory.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session["_componentViewModel"] = null;
            Session["Comp2GBExists"] = null;
            Session["RegMobileReg_PkgPgmBdlPkgCompID"] = null;
            Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session["SimReplacement_WaiverDetails"] = null;
            Session["RegMobileReg_SimReplacementReasons"] = null;
            Session["RegMobileReg_VasIDs"] = null;
            Session["RegMobileReg_VasIDs_Seco"] = null;
            Session[SessionKey.SimType.ToString()] = null;
            Session["Old_SimDetails"] = null;
            Session[SessionKey.Condition.ToString()] = null;
            Session[SessionKey.FromPromoOffer.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            WebHelper.Instance.ResetDepositSessions();
            Session["SimReplacementType"] = null;
            Session[SessionKey.SelectedComponents_Seco.ToString()] = null;
            Session["SelectedComponents"] = null;
            Session["RebatePenalty"] = null;
            Session["OrderType"] = (int)MobileRegType.SimReplacement;
            Session["ContractType"] = null;//08042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();//10042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
            //20151007 - Samuel - Third Party SIM CR - Start
            Session[SessionKey.ThirdParty_BiometricVerify.ToString2()] = null;
            Session[SessionKey.ThirdParty_BiometricDesc.ToString()] = null;
            //20151007 - Samuel - Third Party SIM CR - End
            Session["frontImage"] = null;
            Session["backImage"] = null;
            Session["otherImage"] = null;
            Session["thirdImage"] = null;

            #region Drop 5: BRE Check for non-Drop 4 Flow
            //07042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - Start
            var dropObj = new DropFourObj();

            if (dropObj.currentFlow == DropFourConstant.MAIN_FLOW && dropObj.mainFlow.RegType == -1)
            {
                dropObj.mainFlow.RegType = MobileRegType.SimReplacement.ToInt();
            }

            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            //07042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - End
            #endregion

            if (!ReferenceEquals(TempData["Error"], null))
            {
                ModelState.AddModelError("Error", "Select an account");
            }
            retrieveAcctListByICResponse AcctListByICResponse = null;
            SupplementaryListAccounts SuppListAccounts = null;
            Online.Registration.Web.SubscriberICService.SubscriberICServiceClient
                                                                  icserviceobj = new
                                                                  Online.Registration.Web.SubscriberICService.SubscriberICServiceClient();
            //Added by Rajender SIMReplacement Issue
            string supAccountStatus = string.Empty;
            var isUserCCCAbove = WebHelper.Instance.IsUserCCCAbove();

            if (!ReferenceEquals(Session[SessionKey.SR_PPID.ToString()], null) && Session[SessionKey.SR_PPID.ToString()].GetType() == typeof(retrieveAcctListByICResponse))
            {
                AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.SR_PPID.ToString()];
                ///LIST ONLY GSM A/C'S
                ///

                if (AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse != null && (c.ServiceInfoResponse.lob == "POSTGSM" && (c.ServiceInfoResponse.serviceStatus.ToUpper() == "A" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "S" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "B" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "D" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "T") || c.ServiceInfoResponse.lob == "PREGSM" || c.ServiceInfoResponse.lob == "FWBB" || c.ServiceInfoResponse.lob == "HSDPA")).Count() > 0)
                {
                    SuppListAccounts = new SupplementaryListAccounts();
                    List<string> lstMsisdnList = new List<string>();

                    foreach (var v in AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse != null && c.IsActive == true && (c.ServiceInfoResponse.prinSuppInd != "" && ((c.ServiceInfoResponse.lob == "POSTGSM" && (c.ServiceInfoResponse.serviceStatus.ToUpper() == "A" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "S" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "B"))) || c.ServiceInfoResponse.lob == "PREGSM" || c.ServiceInfoResponse.lob == "FWBB" || c.ServiceInfoResponse.lob == "HSDPA")).ToList())
                    {
                        bool is2GBCompExistOnPkg = false;
                        int PackageKenanCode = 0;
                        bool isPkgExists = false;
                        string AccountPackageName = string.Empty;
                        bool isDataCompsExists = false;

                        //2015.07.27 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - Start
                        if (Util.SessionAccess.User.isDealer && v.AccountDetails != null && (
                            v.AccountDetails.MktCode == ConfigurationManager.AppSettings["CIParentMarketCode"].ToString2() ||
                            v.AccountDetails.MktCode == ConfigurationManager.AppSettings["SMEIParentMarketCode"].ToString2()))
                        {
                            continue;
                        }
                        //2015.07.27 - Ricky - Bugzilla 1152 - To block SMEI/CI for MEPS - End

                        if (Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString2() == "6" && (Session["IsDealer"].ToBool() || isUserCCCAbove == false)) //MSISDN Search only
                        {
                            #region "MSISDN Search Only Flow "
                            var prinExternalID = string.Empty;
                            if (Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString() == v.ExternalId)
                            {
                                if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "S")
                                {
                                    //Get the MISM primary msisdn 
                                    using (var PrinSupproxy = new PrinSupServiceProxy())
                                    {
                                        retrieveMismDetlsResponse primSecResponse = PrinSupproxy.getPrimSecondaryines(v.ExternalId);
                                        if (primSecResponse != null && primSecResponse.itemList.ToList().Count > 0)
                                        {
                                            prinExternalID = primSecResponse.itemList.Where(a => a.PrimSecInd == "P").FirstOrDefault().Msisdn;
                                        }
                                    }
                                }

                                #region :::check whether any greater than 2gb components exists on current line.:::

                                if (v.ServiceInfoResponse.lob.ToLower() != "PREGSM".ToLower() && !v.IsMISM)
                                {
                                    IList<Online.Registration.Web.SubscriberICService.PackageModel>
                                    packages = icserviceobj.retrievePackageDetls(v.ExternalId
                                    , System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"]
                                    , userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName
                                    , password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword
                                    , postUrl: "");
                                    if (!ReferenceEquals(packages, null) && packages.Count > 0)
                                    {
                                        List<string> UniqCompIds = new List<string>();
                                        foreach (var item1 in packages)
                                        {
                                            for (int i = 0; i < item1.compList.Count; i++)
                                            {
                                                UniqCompIds.Add(item1.compList[i].componentId.ToString2());
                                            }
                                        }
                                        Dictionary<string, string> DcList = MasterDataCache.Instance.DataComps2GB;
                                        if (DcList != null && DcList.Count > 0)
                                        {
                                            foreach (var item in DcList.Values)
                                            {
                                                if (item != string.Empty && UniqCompIds.Contains(item))
                                                {
                                                    is2GBCompExistOnPkg = true;
                                                    break;
                                                }
                                            }
                                        }

                                        IList<PackageModel> unifiedPackages = packages.GroupBy(a => a.PackageID).Select(a => a.Last()).ToList();
                                        if (unifiedPackages != null & unifiedPackages.Count > 0)
                                        {
                                            isPkgExists = true;
                                            PackageKenanCode = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageID).FirstOrDefault().ToInt();
                                            AccountPackageName = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageDesc).FirstOrDefault().ToString2();
                                        }
                                    }

                                    #region "Retrive Plan ID based on AccountKenanCode"
                                    if (!ReferenceEquals(PackageKenanCode, null) && PackageKenanCode > 0 && !string.IsNullOrEmpty(AccountPackageName) && !is2GBCompExistOnPkg)
                                    {
                                        int planID = 0;
                                        using (var proxy = new CatalogServiceProxy())
                                        {
                                            var response = proxy.FindPackageOnKenanCode(PackageKenanCode.ToString2() == "" ? "0" : PackageKenanCode.ToString());
                                            foreach (var item in response)
                                            {
                                                if (item.Value.ToLower().Replace(" ", string.Empty).Equals(AccountPackageName.ToLower().Replace(" ", string.Empty).ToString()))
                                                {
                                                    planID = item.Key.ToInt();
                                                    break;
                                                }
                                            }
                                        }
                                        if (planID > 0)
                                        {
                                            isDataCompsExists = CheckDataComponentsOnPlanId(planID);
                                        }
                                    }
                                    #endregion
                                }

                                #endregion

                                SuppListAccounts.SuppListAccounts.Add(
                                    new AddSuppInquiryAccount
                                    {
                                        AccountNumber = v.AcctExtId,// v.Account.AccountNumber,
                                        ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                        Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                                        MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                        // MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                        SubscribeNo = v.SusbcrNo,//v.Account.SubscribeNo  Himansu Changed
                                        SubscribeNoResets = v.SusbcrNoResets,//v.Account.SubscribeNoResets himansu
                                        externalId = v.ExternalId,
                                        accountExtId = v.AcctExtId,
                                        accountIntId = v.AcctIntId,
                                        AccountName = v.ServiceInfoResponse.lob,
                                        AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                        IsMISM = v.IsMISM,
                                        SimSerial = v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "S" ? GetsimSerial(v.ExternalId) : string.Empty,
                                        AccountType = !string.IsNullOrEmpty(v.ServiceInfoResponse.prinSuppInd) ? v.ServiceInfoResponse.prinSuppInd : "P",
                                        PrinMsisdn = !string.IsNullOrEmpty(prinExternalID) ? prinExternalID : string.Empty,
                                        IsGreater2GBCompExists = is2GBCompExistOnPkg,
                                        IsPackagesExistsOnAccountLine = isPkgExists,
                                        AccountKenanCode = PackageKenanCode.ToString2(),
                                        AccountPackageName = AccountPackageName,
                                        HasSupplyAccounts = v.PrinSuppResponse != null & v.PrinSuppResponse.itemList.Count > 0 ? true : false,
                                        WriteOffAmount = (v.Customer != null ? v.Customer.TotalWriteOff.ToInt() : 0),
                                        DataCompsExistOnAccount = isDataCompsExists

                                    });

                                if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "P" && v.SecondarySimList != null)
                                {
                                    //Get the MISM primary msisdn 
                                    using (var PrinSupproxy = new PrinSupServiceProxy())
                                    {
                                        retrieveMismDetlsResponse primSecResponse = PrinSupproxy.getPrimSecondaryines(v.ExternalId);
                                        if (primSecResponse != null && primSecResponse.itemList.ToList().Count > 0)
                                        {
                                            prinExternalID = primSecResponse.itemList.Where(a => a.PrimSecInd == "P").FirstOrDefault().Msisdn;
                                            foreach (var supp in v.SecondarySimList)
                                            {
                                                SuppListAccounts.SuppListAccounts.Add(
                                                   new AddSuppInquiryAccount
                                                   {
                                                       AccountNumber = v.AcctExtId,
                                                       ActiveDate = string.Empty,
                                                       Address = v.Account != null ? v.Account.Address : string.Empty,
                                                       Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                                                       MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                                                       CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                       Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                       IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                       IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                       // MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                       Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                       SubscribeNo = supp.FxSubscrNo,
                                                       SubscribeNoResets = supp.FxSubscrNoResets,
                                                       externalId = supp.Msisdn,
                                                       accountExtId = v.AcctExtId,
                                                       accountIntId = supp.FxAcctNo,
                                                       AccountName = v.ServiceInfoResponse.lob,
                                                       AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                                       IsMISM = v.IsMISM,
                                                       SimSerial = supp.PrimSecInd == "S" ? GetsimSerial(supp.Msisdn) : string.Empty,
                                                       AccountType = "S",
                                                       PrinMsisdn = v.ExternalId != supp.Msisdn ? v.ExternalId : string.Empty,
                                                       IsGreater2GBCompExists = is2GBCompExistOnPkg,
                                                       WriteOffAmount = (v.Customer != null ? v.Customer.TotalWriteOff.ToInt() : 0),
                                                       DataCompsExistOnAccount = isDataCompsExists
                                                   });
                                            }
                                        }
                                    }
                                }

                                if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "P" && v.PrinSupplimentaryResponse != null && v.PrinSupplimentaryResponse.itemList.Count > 0)
                                {

                                    foreach (var supp in v.PrinSupplimentaryResponse.itemList)
                                    {
                                        if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                        {
                                            if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                supAccountStatus = "S";
                                            else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                supAccountStatus = "D";
                                            else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                supAccountStatus = "T";
                                            else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                supAccountStatus = "B";
                                            else
                                                supAccountStatus = "A";
                                        }

                                        string accountNumber = string.Empty;
                                        //var AcctListByICRespAct = new retrieveAcctListByICResponse();
                                        //AcctListByICRespAct.itemList.AddRange((List<SubscriberICService.Items>)AcctListByICResponse.itemList.Where(s => !string.IsNullOrEmpty(s.ExternalId) && !ReferenceEquals(s.ServiceInfoResponse, null) && !ReferenceEquals(s.IsMISM, null)));
                                        var AcctListByICRespListAct = AcctListByICResponse.itemList.Where(s => !ReferenceEquals(s.ServiceInfoResponse, null));
                                        var suppAccount = AcctListByICRespListAct.Where(s => s.ExternalId == supp.msisdnField && s.ServiceInfoResponse.prinSuppInd == "S" && !s.IsMISM).FirstOrDefault();
                                        accountNumber = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId;

                                        SuppListAccounts.SuppListAccounts.Add(
                                           new AddSuppInquiryAccount
                                           {
                                               AccountNumber = accountNumber,// v.AcctExtId,
                                               ActiveDate = string.Empty,
                                               Address = v.Account != null ? v.Account.Address : string.Empty,
                                               Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                                               MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                                               CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                               Holder = supp.cust_nmField, //v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                               IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                               IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                               //MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                               Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                               SubscribeNo = supp.subscr_noField,
                                               SubscribeNoResets = supp.subscr_no_resetsField,
                                               externalId = supp.msisdnField,
                                               accountExtId = v.AcctExtId,
                                               accountIntId = supp.acct_noField,
                                               AccountName = v.ServiceInfoResponse.lob,
                                               AccountStatus = supAccountStatus, //v.ServiceInfoResponse.serviceStatus,
                                               IsMISM = supp.IsMISM,
                                               //SimSerial = supp.PrimSecInd == "S" ? GetsimSerial(supp.Msisdn) : string.Empty,
                                               AccountType = "S",
                                               PrinMsisdn = v.ExternalId, // != supp.Msisdn ? v.ExternalId : string.Empty
                                               //IsGreater2GBCompExists = is2GBCompExistOnPkg
                                               WriteOffAmount = (v.Customer != null ? v.Customer.TotalWriteOff.ToInt() : 0),
                                               DataCompsExistOnAccount = isDataCompsExists
                                           });
                                    }

                                }

                            }
                            #endregion
                        }
                        else
                        {
                            if (!v.IsMISM)
                            {
                                if ((v.ServiceInfoResponse.prinSuppInd != "S" && v.ServiceInfoResponse.prinSuppInd != "") || (v.ServiceInfoResponse.prinSuppInd == "" && (v.ServiceInfoResponse.lob == "HSDPA" || v.ServiceInfoResponse.lob == "FWBB")))
                                {
                                    if (!lstMsisdnList.Contains(v.ExternalId))
                                    {
                                        lstMsisdnList.Add(v.ExternalId);

                                        #region :::check whether any greater than 2gb components exists on current line.:::

                                        if (v.ServiceInfoResponse.lob.ToLower() != "PREGSM".ToLower())
                                        {
                                            IList<Online.Registration.Web.SubscriberICService.PackageModel>
                                            packages = icserviceobj.retrievePackageDetls(v.ExternalId
                                            , System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"]
                                            , userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName
                                            , password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword
                                            , postUrl: "");
                                            if (!ReferenceEquals(packages, null) && packages.Count > 0)
                                            {
                                                List<string> UniqCompIds = new List<string>();
                                                foreach (var item1 in packages)
                                                {
                                                    for (int i = 0; i < item1.compList.Count; i++)
                                                    {
                                                        UniqCompIds.Add(item1.compList[i].componentId.ToString2());
                                                    }
                                                }
                                                Dictionary<string, string> DcList = MasterDataCache.Instance.DataComps2GB;
                                                if (DcList != null && DcList.Count > 0)
                                                {
                                                    foreach (var item in DcList.Values)
                                                    {
                                                        if (item != string.Empty && UniqCompIds.Contains(item))
                                                        {
                                                            is2GBCompExistOnPkg = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                IList<PackageModel> unifiedPackages = packages.GroupBy(a => a.PackageID).Select(a => a.Last()).ToList();
                                                if (unifiedPackages != null & unifiedPackages.Count > 0)
                                                {
                                                    isPkgExists = true;
                                                    PackageKenanCode = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageID).FirstOrDefault().ToInt();
                                                    AccountPackageName = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageDesc).FirstOrDefault().ToString2();
                                                }

                                                #region "Retrive Plan ID based on AccountKenanCode"
                                                if (!ReferenceEquals(PackageKenanCode, null) && PackageKenanCode > 0 && !string.IsNullOrEmpty(AccountPackageName))
                                                {
                                                    int planID = 0;
                                                    using (var proxy = new CatalogServiceProxy())
                                                    {
                                                        var response = proxy.FindPackageOnKenanCode(PackageKenanCode.ToString2() == "" ? "0" : PackageKenanCode.ToString());
                                                        foreach (var item in response)
                                                        {
                                                            if (item.Value.ToLower().Replace(" ", string.Empty).Equals(AccountPackageName.ToLower().Replace(" ", string.Empty).ToString()))
                                                            {
                                                                planID = item.Key.ToInt();
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    if (planID > 0)
                                                    {
                                                        isDataCompsExists = CheckDataComponentsOnPlanId(planID);
                                                    }
                                                }
                                                #endregion
                                            }
                                        }

                                        #endregion
                                        //Add only Principal Lines
                                        SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = v.AcctExtId,
                                                        ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        // Category = v.Account != null ? v.Account.Category : string.Empty,
                                                        Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                                                        MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        //Holder = v.Account != null ? v.Account.Holder : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        //MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = v.SusbcrNo,
                                                        SubscribeNoResets = v.SusbcrNoResets,
                                                        externalId = v.ExternalId,
                                                        accountExtId = v.AcctExtId,
                                                        accountIntId = v.AcctIntId,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                                        IsMISM = v.IsMISM,
                                                        SimSerial = string.Empty,
                                                        AccountType = "P",
                                                        PrinMsisdn = string.Empty,
                                                        IsGreater2GBCompExists = is2GBCompExistOnPkg,
                                                        AccountKenanCode = PackageKenanCode.ToString2(),
                                                        IsPackagesExistsOnAccountLine = isPkgExists,
                                                        AccountPackageName = AccountPackageName,
                                                        HasSupplyAccounts = false,
                                                        WriteOffAmount = (v.Customer != null ? v.Customer.TotalWriteOff.ToInt() : 0),
                                                        DataCompsExistOnAccount = isDataCompsExists
                                                    });
                                    }
                                    if (v.PrinSuppResponse != null && v.PrinSuppResponse.itemList != null && v.PrinSuppResponse.itemList.Count > 0)
                                    {
                                        #region "Supplimentary Accounts"
                                        foreach (var supp in v.PrinSuppResponse.itemList)
                                        {
                                            if (v.ExternalId != supp.msisdnField)
                                            {
                                                if (!lstMsisdnList.Contains(supp.msisdnField))
                                                {
                                                    var suppAccount = AcctListByICResponse.itemList.Where(i => i.ExternalId == supp.msisdnField && i.ServiceInfoResponse != null && i.ServiceInfoResponse.prinSuppInd == "S" && !i.IsMISM).FirstOrDefault();
                                                    //SuppListAccounts.SuppListAccounts.Where(A => A.externalId == v.ExternalId) = SuppListAccounts.SuppListAccounts.Where(a => a.externalId == v.ExternalId).Select(s => { s.HasSupplyAccounts = true; return s; }).ToList();
                                                    foreach (var item in SuppListAccounts.SuppListAccounts.Where(a => a.externalId == v.ExternalId))
                                                    {
                                                        item.HasSupplyAccounts = true;
                                                    }
                                                    if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                                    {
                                                        if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                            supAccountStatus = "S";
                                                        else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                            supAccountStatus = "D";
                                                        else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                            supAccountStatus = "T";
                                                        else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                            supAccountStatus = "B";
                                                        else
                                                            supAccountStatus = "A";
                                                    }
                                                    lstMsisdnList.Add(supp.msisdnField);
                                                    #region :::check whether any greater than 2gb components exists on current line.:::

                                                    if (v.ServiceInfoResponse.lob.ToLower() != "PREGSM".ToLower())
                                                    {
                                                        //here we are sending external id of supplimenary MSISDN, keep remember below point.
                                                        IList<Online.Registration.Web.SubscriberICService.PackageModel>
                                                              packages = icserviceobj.retrievePackageDetls(supp.msisdnField
                                                       , System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"]
                                                       , userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName
                                                       , password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword
                                                       , postUrl: "");

                                                        if (!ReferenceEquals(packages, null) && packages.Count > 0)
                                                        {
                                                            List<string> UniqCompIds = new List<string>();
                                                            foreach (var item1 in packages)
                                                            {
                                                                for (int i = 0; i < item1.compList.Count; i++)
                                                                {
                                                                    UniqCompIds.Add(item1.compList[i].ToString());
                                                                }
                                                            }
                                                            Dictionary<string, string> DcList = MasterDataCache.Instance.DataComps2GB;
                                                            if (DcList != null && DcList.Count > 0)
                                                            {
                                                                foreach (var item in DcList.Values)
                                                                {
                                                                    if (item != string.Empty && UniqCompIds.Contains(item))
                                                                    {
                                                                        is2GBCompExistOnPkg = true;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        IList<PackageModel> unifiedPackages = packages.GroupBy(a => a.PackageID).Select(a => a.Last()).ToList();
                                                        if (unifiedPackages != null & unifiedPackages.Count > 0)
                                                        {
                                                            isPkgExists = true;
                                                            PackageKenanCode = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageID).FirstOrDefault().ToInt();
                                                            AccountPackageName = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageDesc).FirstOrDefault().ToString2();
                                                        }
                                                    }

                                                    #endregion
                                                    SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId,  //string.IsNullOrEmpty(supp.acc_AccExtId)? v.AcctExtId :supp.acc_AccExtId,
                                                        ActiveDate = string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                                                        MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        //  Holder = v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        // MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = supp.subscr_noField,
                                                        SubscribeNoResets = supp.subscr_no_resetsField,
                                                        externalId = supp.msisdnField,
                                                        accountExtId = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId, //string.IsNullOrEmpty(supp.acc_AccExtId) ? v.AcctExtId : supp.acc_AccExtId,
                                                        accountIntId = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctIntId) ? suppAccount.AcctIntId : supp.acct_noField,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        //Added by Rajender SIMReplacement Issue SIMReplacement Issue
                                                        AccountStatus = supAccountStatus, //v.ServiceInfoResponse.serviceStatus,
                                                        IsMISM = v.IsMISM,
                                                        SimSerial = string.Empty,
                                                        AccountType = "S",
                                                        PrinMsisdn = v.ExternalId != supp.msisdnField ? v.ExternalId : string.Empty,
                                                        IsGreater2GBCompExists = is2GBCompExistOnPkg,
                                                        AccountKenanCode = PackageKenanCode.ToString2(),
                                                        IsPackagesExistsOnAccountLine = isPkgExists,
                                                        AccountPackageName = AccountPackageName,
                                                        WriteOffAmount = (v.Customer != null ? v.Customer.TotalWriteOff.ToInt() : 0),
                                                        DataCompsExistOnAccount = isDataCompsExists
                                                    });
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }

                            }
                            else
                            {
                                //Listing the MISM LIST
                                #region "MISM Accounts"
                                if ((v.ServiceInfoResponse.prinSuppInd == "P") && v.IsMISM)
                                {
                                    if (!lstMsisdnList.Contains(v.ExternalId))
                                    {
                                        lstMsisdnList.Add(v.ExternalId);
                                        //Adding primary  line for MISM

                                        IList<Online.Registration.Web.SubscriberICService.PackageModel>
                                                              packages = icserviceobj.retrievePackageDetls(v.ExternalId
                                                       , System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"]
                                                       , userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName
                                                       , password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword
                                                       , postUrl: "");

                                        IList<PackageModel> unifiedPackages = packages.GroupBy(a => a.PackageID).Select(a => a.Last()).ToList();
                                        if (unifiedPackages != null & unifiedPackages.Count > 0)
                                        {
                                            isPkgExists = true;
                                            PackageKenanCode = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageID).FirstOrDefault().ToInt();
                                            AccountPackageName = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageDesc).FirstOrDefault().ToString2();
                                        }
                                        SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = v.AcctExtId,
                                                        ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                                                        MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        //   Holder = v.Account != null ? v.Account.Holder : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        //MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = v.SusbcrNo,
                                                        SubscribeNoResets = v.SusbcrNoResets,
                                                        externalId = v.ExternalId,
                                                        accountExtId = v.AcctExtId,
                                                        accountIntId = v.AcctIntId,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                                        IsMISM = v.IsMISM,
                                                        SimSerial = string.Empty,
                                                        AccountType = "P",
                                                        PrinMsisdn = string.Empty,
                                                        IsGreater2GBCompExists = false,
                                                        AccountKenanCode = PackageKenanCode.ToString2(),
                                                        IsPackagesExistsOnAccountLine = isPkgExists,
                                                        AccountPackageName = AccountPackageName,
                                                        WriteOffAmount = (v.Customer != null ? v.Customer.TotalWriteOff.ToInt() : 0),
                                                        DataCompsExistOnAccount = isDataCompsExists
                                                    });
                                    }
                                }
                                if (v.ServiceInfoResponse.prinSuppInd == "P" && v.IsMISM && v.SecondarySimList != null)
                                {

                                    //Adding primary and Secondary line for MISM
                                    foreach (var supp in v.SecondarySimList)
                                    {
                                        PackageKenanCode = 0;
                                        if (!lstMsisdnList.Contains(supp.Msisdn))
                                        {
                                            IList<Online.Registration.Web.SubscriberICService.PackageModel>
                                                              packages = icserviceobj.retrievePackageDetls(supp.Msisdn
                                                       , System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"]
                                                       , userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName
                                                       , password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword
                                                       , postUrl: "");

                                            IList<PackageModel> unifiedPackages = packages.GroupBy(a => a.PackageID).Select(a => a.Last()).ToList();
                                            if (unifiedPackages != null & unifiedPackages.Count > 0)
                                            {
                                                isPkgExists = true;
                                                PackageKenanCode = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageID).FirstOrDefault().ToInt();
                                                AccountPackageName = unifiedPackages.Where(a => a.PackageDesc.ToLower().Contains("mandatory package")).Select(a => a.PackageDesc).FirstOrDefault().ToString2();
                                            }
                                            lstMsisdnList.Add(supp.Msisdn);
                                            SuppListAccounts.SuppListAccounts.Add(
                                            new AddSuppInquiryAccount
                                            {
                                                AccountNumber = v.AcctExtId,
                                                ActiveDate = string.Empty,
                                                Address = v.Account != null ? v.Account.Address : string.Empty,
                                                Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                                                MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                                                CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                // Holder = v.Account != null ? v.Account.Holder : string.Empty,
                                                Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                // MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                SubscribeNo = supp.FxSubscrNo,
                                                SubscribeNoResets = supp.FxSubscrNoResets,
                                                externalId = supp.Msisdn,
                                                accountExtId = v.AcctExtId,
                                                accountIntId = supp.FxAcctNo,
                                                AccountName = v.ServiceInfoResponse.lob,
                                                AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                                IsMISM = v.IsMISM,
                                                SimSerial = supp.PrimSecInd == "S" ? GetsimSerial(supp.Msisdn) : string.Empty,
                                                //AccountType = v.ExternalId != supp.Msisdn ? "P" : "S",
                                                AccountType = "S",
                                                PrinMsisdn = v.ExternalId != supp.Msisdn ? v.ExternalId : string.Empty,
                                                IsGreater2GBCompExists = false,
                                                AccountKenanCode = PackageKenanCode.ToString2(),
                                                IsPackagesExistsOnAccountLine = isPkgExists,
                                                AccountPackageName = AccountPackageName,
                                                WriteOffAmount = (v.Customer != null ? v.Customer.TotalWriteOff.ToInt() : 0),
                                                DataCompsExistOnAccount = isDataCompsExists
                                            });
                                        }
                                    }

                                }

                                if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "P" && v.PrinSupplimentaryResponse != null && v.PrinSupplimentaryResponse.itemList.Count > 0)
                                {

                                    foreach (var supp in v.PrinSupplimentaryResponse.itemList)
                                    {
                                        if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                        {
                                            if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                supAccountStatus = "S";
                                            else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                supAccountStatus = "D";
                                            else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                supAccountStatus = "T";
                                            else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                supAccountStatus = "B";
                                            else
                                                supAccountStatus = "A";
                                        }

                                        string accountNumber = string.Empty;
                                        //var AcctListByICRespAct = new retrieveAcctListByICResponse();
                                        //AcctListByICRespAct.itemList.AddRange((List<SubscriberICService.Items>)AcctListByICResponse.itemList.Where(s => !string.IsNullOrEmpty(s.ExternalId) && !ReferenceEquals(s.ServiceInfoResponse, null) && !ReferenceEquals(s.IsMISM, null)));
                                        var AcctListByICRespListAct = AcctListByICResponse.itemList.Where(s => !ReferenceEquals(s.ServiceInfoResponse, null));
                                        var suppAccount = AcctListByICRespListAct.Where(s => s.ExternalId == supp.msisdnField && s.ServiceInfoResponse.prinSuppInd == "S" && !s.IsMISM).FirstOrDefault();
                                        accountNumber = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId;

                                        SuppListAccounts.SuppListAccounts.Add(
                                           new AddSuppInquiryAccount
                                           {
                                               AccountNumber = accountNumber,// v.AcctExtId,
                                               ActiveDate = string.Empty,
                                               Address = v.Account != null ? v.Account.Address : string.Empty,
                                               Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                                               MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                                               CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                               Holder = supp.cust_nmField, //v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                               IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                               IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                               //MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                               Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                               SubscribeNo = supp.subscr_noField,
                                               SubscribeNoResets = supp.subscr_no_resetsField,
                                               externalId = supp.msisdnField,
                                               accountExtId = v.AcctExtId,
                                               accountIntId = supp.acct_noField,
                                               AccountName = v.ServiceInfoResponse.lob,
                                               AccountStatus = supAccountStatus, //v.ServiceInfoResponse.serviceStatus,
                                               IsMISM = supp.IsMISM,
                                               //SimSerial = supp.PrimSecInd == "S" ? GetsimSerial(supp.Msisdn) : string.Empty,
                                               AccountType = "S",
                                               PrinMsisdn = v.ExternalId,
                                               WriteOffAmount = (v.Customer != null ? v.Customer.TotalWriteOff.ToInt() : 0),
                                               DataCompsExistOnAccount = isDataCompsExists
                                           });
                                    }

                                }

                                #endregion
                            }

                        }
                    }
                }
            }
            Session["SuppListAccounts"] = SuppListAccounts;
            ViewBag.MSISDN = !string.IsNullOrEmpty(msisdn) ? msisdn : string.Empty;
            return View(SuppListAccounts);
        }

        [HttpPost]
        public ActionResult AccountDetails(FormCollection collection)
        {
            bool backbutton = false;
            if (backbutton)
            {
                RedirectToAction("IndexNew");
            }

            DropFourObj Drop4Obj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            PersonalDetailsVM PersonalDetailVM = new PersonalDetailsVM();
            string SelectedAccountDetails = collection["SelectedAccountDtls"].ToString2();
            string[] SplitData = SelectedAccountDetails.Split(',');
            PersonalDetailVM.SimReplacementsVM.externalId = SplitData[0];
            PersonalDetailVM.SimReplacementsVM.AccountType = SplitData[4];
            PersonalDetailVM.SimReplacementsVM.KenanACNumber = SplitData[5];
            PersonalDetailVM.SimReplacementsVM.SubscribeNo = SplitData[6];
            PersonalDetailVM.SimReplacementsVM.SubscribeNoResets = SplitData[7];
            PersonalDetailVM.SimReplacementsVM.Comp2GBExist = SplitData[8].ToString2() == "True" ? true : false;
            PersonalDetailVM.SimReplacementsVM.KenanCode = SplitData[9];
            PersonalDetailVM.SimReplacementsVM.fxAcctNo = SplitData[17];
            PersonalDetailVM.SimReplacementsVM.AcctPackageName = SplitData[10];
            PersonalDetailVM.SimReplacementsVM.AcctPkgsExists = SplitData[11];
            PersonalDetailVM.SimReplacementsVM.HasSupplyAccounts = SplitData[12];
            PersonalDetailVM.SimReplacementsVM.DataCompsExistOnAccount = SplitData[14];
            PersonalDetailVM.SimReplacementsVM.AccountCategory = SplitData[15];
            PersonalDetailVM.SimReplacementsVM.MarketCode = SplitData[16];
            PersonalDetailVM.SimReplacementsVM.Type = !ReferenceEquals(collection["SimReplacementType"], null) ? collection["SimReplacementType"].ToInt() : 0;
            
            Session["Comp2GBExists"] = null;
            Session[SessionKey.SelectedAccountNumber.ToString()] = collection["SelectedAccountDtls"].ToString2();

            if (collection["SelectedAccountDtls"] == null || collection["SelectedAccountDtls"] == string.Empty)
            {
                ModelState.AddModelError("CustomError", "please select the account line");
                Logger.InfoFormat("{0}:{1},Error{2}", "QSimReplacement", "AccountDetails(Form Collection)", "select the account line");
                return View((SupplementaryListAccounts)Session["SuppListAccounts"]);
            }

            Session["SimReplacementType"] = !ReferenceEquals(collection["SimReplacementType"], null) ? collection["SimReplacementType"] : "0";
            Session[SessionKey.RegMobileReg_Type.ToString()] = MobileRegType.SimReplacement.ToInt();
            if (Session[SessionKey.SelectedAccountNumber.ToString()].ToString2().Length > 0)
            {
                string SelectAccountDtls = Session[SessionKey.SelectedAccountNumber.ToString()].ToString();
                string[] strAccountDtls = SelectAccountDtls.Split(',');
                Session[SessionKey.FxAccNo.ToString()] = PersonalDetailVM.SimReplacementsVM.fxAcctNo; //Acd.accountIntId
                Session[SessionKey.MSISDN.ToString()] = Session[SessionKey.ExternalID.ToString()] = PersonalDetailVM.SimReplacementsVM.externalId; //Acd.externalId
                Session[SessionKey.AccExternalID.ToString()] = PersonalDetailVM.SimReplacementsVM.KenanACNumber; //Acd.accountExtId   
                Session[SessionKey.KenanACNumber.ToString()] = PersonalDetailVM.SimReplacementsVM.KenanACNumber; //Acd.AccountNumber  v.AcctExtId
                var mobileNos = new List<string>();
                mobileNos.Add(PersonalDetailVM.SimReplacementsVM.externalId); //Acd.externalId
                Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;
                Session["SubscribeNo"] = PersonalDetailVM.SimReplacementsVM.SubscribeNo; //Acd.SubscribeNo
                Session["SubscribeNoResets"] = PersonalDetailVM.SimReplacementsVM.SubscribeNoResets; //Acd.SubscribeNoResets
                Session["AccountType"] = PersonalDetailVM.SimReplacementsVM.AccountType;
                Session[SessionKey.AccountCategory.ToString()] = PersonalDetailVM.SimReplacementsVM.AccountCategory;
                Session[SessionKey.MarketCode.ToString()] = PersonalDetailVM.SimReplacementsVM.MarketCode;
                retrieveSimDetlsResponse objRetrieveSimDetlsResponse = new retrieveSimDetlsResponse();

                using (var proxy = new UserServiceProxy())
                {
                    proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SimReplacement, "Sim Replacement Account Details", Session["KenanACNumber"].ToString(), "");
                }

                //Get Sim Details
                using (var proxy = new retrieveServiceInfoProxy())
                {
                    objRetrieveSimDetlsResponse = proxy.retreiveSIMDetls(PersonalDetailVM.SimReplacementsVM.externalId);
                }
                SimDetlsList objSimDetlsList = null;
                if (objRetrieveSimDetlsResponse != null)
                {
                    List<Online.Registration.Web.SubscriberICService.SimDetails> simDetlsFiltered = objRetrieveSimDetlsResponse.itemList.Where(c => c.InactiveDt == null || c.InactiveDt == string.Empty).ToList();
                    if (!ReferenceEquals(simDetlsFiltered, null) && simDetlsFiltered.Count > 0)
                    {
                        objSimDetlsList = new SimDetlsList(
                        simDetlsFiltered[0].ExternalID.ToString2(),
                        simDetlsFiltered[0].ExternalIDType.ToString2(),
                        simDetlsFiltered[0].InventoryTypeID.ToString2(),
                        simDetlsFiltered[0].InventoryDisplayValue,
                        simDetlsFiltered[0].Reason,
                        simDetlsFiltered[0].ActiveDt,
                        simDetlsFiltered[0].InactiveDt
                       );
                        PersonalDetailVM.SimReplacementsVM.OldSIMDetail = objSimDetlsList;
                        Session["Old_SimDetails"] = objSimDetlsList;
                    }
                    else
                    {
                        ModelState.AddModelError("CustomError", "Inventory details not found in Kenan, please refer to Kenan for details");
                        Logger.InfoFormat("{0}:{1},Error{2}", "QSimReplacement", "AccountDetails(Form Collection)", "null Kenan inventory response");
                        return View((SupplementaryListAccounts)Session["SuppListAccounts"]);
                    }
                }
            }

            Drop4Obj.personalInformation = new PersonalDetailsVM();
            Drop4Obj.personalInformation.TabNumber = SIMReplacementStep.AccountDetails.ToInt();
            Drop4Obj.personalInformation.SimReplacementsVM = new SIMReplacementVM();
            Drop4Obj.personalInformation.SimReplacementsVM = PersonalDetailVM.SimReplacementsVM;
            Session[SessionKey.DropFourObj.ToString()] = Drop4Obj;
            Session["SimType"] = Util.SIMReplacementSIMType();
            if (!string.IsNullOrEmpty(Session["SimReplacementType"].ToString2()))
            {
                if ((Convert.ToInt16(Session["SimReplacementType"]) != Convert.ToInt16(SimReplacementType.N2M)))
                {
                    return RedirectToAction("CustomerSummaryNew", new { regID = 0 });
                }
                else
                {
                    int planID = 0;
                    if (!PersonalDetailVM.SimReplacementsVM.Comp2GBExist) // Incase of 2GB Component not exists on this Plan
                    {
                        Session["Comp2GBExists"] = 1;
                        ///todo: retrive planid based on KenanCode
                        #region "Retrive Plan ID based on AccountKenanCode"
                        if (!string.IsNullOrEmpty(PersonalDetailVM.SimReplacementsVM.KenanCode) && !string.IsNullOrEmpty(PersonalDetailVM.SimReplacementsVM.AcctPackageName))
                        {
                            using (var proxy = new CatalogServiceProxy())
                            {
                                var response = proxy.FindPackageOnKenanCode(PersonalDetailVM.SimReplacementsVM.KenanCode);
                                foreach (var item in response)
                                {
                                    if (item.Value.ToLower().Replace(" ", string.Empty).Equals(PersonalDetailVM.SimReplacementsVM.AcctPackageName.ToLower().Replace(" ", string.Empty).ToString()))
                                    {
                                        planID = item.Key.ToInt();
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion
                        if (planID > 0)
                        {
                            return RedirectToAction("SelectVas", new { planid = planID });
                        }
                        else
                        {
                            ModelState.AddModelError("CustomError", "Selected Line cannot convert to MISM, Due to Line details doesn`t exists.");
                            Logger.InfoFormat("{0}:{1} PlanID:{2} , AccountKenanCode:{3}, AcctPackageName:{4}, Comp2GBExists:{5}", "QSimReplacement", "AccountDetails(Form Collection)", planID, collection["AccountKenanCode"].ToString2(), collection["AcctPackageName"].ToString2(), collection["Comp2GBExists"].ToString2());
                            return View((SupplementaryListAccounts)Session["SuppListAccounts"]);
                        }
                    }
                    else
                    {
                        //return RedirectToAction("SelectSecondaryPlan", new { planid = planID });
                        return RedirectToAction("CustomerSummaryNew", new { regID = 0 });
                    }
                }
            }
            return RedirectToAction("CustomerSummaryNew", new { regID = 0 });
        }

        public int LiberisationTenureStatus()
        {
            int years = 4;
            return years;
        }

        [Authorize(Roles = "MREG_DAPP,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DAI,MREG_DAC")]
        [DoNoTCache]
        public ActionResult MobileRegSuccess(int regID)
        {
            //LnkRegDetailsReq objLnkRegDetails = new LnkRegDetailsReq();
            //using (var regProxy = new RegistrationServiceProxy())
            //{
            //    objLnkRegDetails = regProxy.GetLnkRegistrationDetails(regID);
            //}
            //return View(objLnkRegDetails);

            RegDetailsObjVM regDetailsObj = new RegDetailsObjVM();
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];
            regDetailsObj.RegID = regID;

            using (var regProxy = new RegistrationServiceProxy())
            {
                regDetailsObj.LnkRegDetailsReq = regProxy.GetLnkRegistrationDetails(regID);
            }
			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var proxy = new RegistrationServiceProxy())
				{
					regDetailsObj.PegaVM = new PegaRecommendationsVM();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = proxy.GetPegaRecommendationbyMSISDN(regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria);
					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = regDetailsObj.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();
				}
			}
            

            return View(regDetailsObj);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DAC,MREG_DAI,MREG_DC")]
        [DoNoTCache]
        public ActionResult MobileRegFail(int regID)
        {
            return View(regID);
        }
        [Authorize(Roles = "MREG_DAPP,MREG_DSV,MREG_DSA,,MREG_C,MREG_CH,MREG_DC")]
        [DoNoTCache]
        public ActionResult PlanPaymntReceived(SimReplacementPaymentRecvd paymentReceived)
        {
            return View(paymentReceived);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_DAC,MREG_DAI,MREG_DC")]
        [DoNoTCache]
        public ActionResult MobileRegAccCreated(int regID)
        {
            LnkRegDetailsReq objLnkRegDetails = new LnkRegDetailsReq();
            using (var regProxy = new RegistrationServiceProxy())
            {
                objLnkRegDetails = regProxy.GetLnkRegistrationDetails(regID);
            }

            //Save the User TransactionLog
            WebHelper.Instance.SaveUserTransactionLogs(regID);
            return View(objLnkRegDetails);
        }
        [HttpPost]
        public ActionResult MobileRegAccCreated(FormCollection collection)
        {
            LnkRegDetailsReq request = new LnkRegDetailsReq();
            int orderID = collection["OrderId"].ToInt();
            return View(request);
        }

        #region "Deactivated due to auto send transaction to webpos w/o login"
        //[Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DSK,MREG_U,MREG_SK,MREG_DC,MREG_DN")]
        //[DoNoTCache]
        //[HttpGet]
        //public ActionResult WebPOS(int regID)
        //{
        //    WEBPosVM webPOSVM = WebHelper.Instance.ConstructWebPOS(regID);
        //    return View(webPOSVM);
        //} 
        #endregion



        [Authorize(Roles = "MREG_DAPP,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DAC,MREG_DAI,MREG_DC,MREG_DSK,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegCanceled(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_U,MREG_CH,MREG_DC,MREG_DAI,MREG_DAC,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegSvcActivated(int regID)
        {
            RegDetailsObjVM regDetailsObj = new RegDetailsObjVM();
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];

			using (var regProxy = new RegistrationServiceProxy())
			{
				regDetailsObj.LnkRegDetailsReq = regProxy.GetLnkRegistrationDetails(regID);
				regDetailsObj.reg = regProxy.RegistrationGet(regID);
			}

			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var regProxy = new RegistrationServiceProxy())
				{
					regDetailsObj.PegaVM = new PegaRecommendationsVM();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = regProxy.GetPegaRecommendationbyMSISDN(regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria);
					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = regDetailsObj.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();

				}
			}
            
            return View(regDetailsObj);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MobileRegSvcActivated(FormCollection collection, RegDetailsObjVM regDetailsObj)
        {
            DropFourObj dropObj = new DropFourObj();
            dropObj.personalInformation = new PersonalDetailsVM();
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];
			dropObj.personalInformation.rfDocumentsFile = collection["frontPhoto"].ToString2() == "" ? Session["frontImage"].ToString2() : collection["frontPhoto"].ToString2();
			dropObj.personalInformation.rfDocumentsFileName = collection["frontPhotoFileName"];
            WebHelper.Instance.SaveDocumentToTable(dropObj,regDetailsObj.reg.ID);
            
            using (var regProxy = new RegistrationServiceProxy())
            {
                regDetailsObj.LnkRegDetailsReq = regProxy.GetLnkRegistrationDetails(regDetailsObj.RegID);
                regDetailsObj.reg = regProxy.RegistrationGet(regDetailsObj.RegID);
            }

            using (var proxy = new WebPOSCallBackSvc.WebPOSCallBackServiceClient())
            {
                proxy.sendDocsToIcontract(regDetailsObj.RegID, true);
            }
			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var Proxy = new RegistrationServiceProxy())
				{
					regDetailsObj.PegaVM = new PegaRecommendationsVM();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = Proxy.GetPegaRecommendationbyMSISDN(regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria);
					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = regDetailsObj.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();

				}
			}
            
            ViewBag.documentUploaded = true;

            return View(regDetailsObj);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_CH,MREG_DC,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegClosed(int regID)
        {
            LnkRegDetailsReq objLnkRegDetails = new LnkRegDetailsReq();
            using (var regProxy = new RegistrationServiceProxy())
            {
                objLnkRegDetails = regProxy.GetLnkRegistrationDetails(regID);
            }
            return View(objLnkRegDetails);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_HQ,MREG_DAB,MREG_DAI,MREG_DAC,MREG_DSK,MREG_DCH,MREG_DC,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult CustomerSummaryNew(int? regID)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var personalDetailsVM = new PersonalDetailsVM();
            var orderSummary = new OrderSummaryVM();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            var mainLinePBPCIDsSec = new List<int>();
            var regSec = new DAL.Models.RegistrationSec();
            var suppLineRegPBPCsSec = new List<RegPgmBdlPkgCompSec>();
            DAL.Models.RegistrationSec registrationSec = new DAL.Models.RegistrationSec();
            var allRegDetails = new Online.Registration.DAL.RegistrationDetails();
            int paymentstatus = -1;
            if (!Session["IsDealer"].ToBool() && Session["SimReplacementType"].ToInt() != (int)SimReplacementType.PREPAID)
            {
                personalDetailsVM.ThirdPartyAllowed = true;
            }
            else
            {
                personalDetailsVM.ThirdPartyAllowed = false;
            }

            try
            {
                var KenanLogDetails = new DAL.Models.KenanaLogDetails();
                if (regID.ToInt() == 0)
                {
                    #region regID = 0
                    DropFourObj DropForObj = !ReferenceEquals(Session[SessionKey.DropFourObj.ToString()], null) ? (DropFourObj)Session[SessionKey.DropFourObj.ToString()] : new DropFourObj();
                    DropForObj.getExistingLineCount();
                    personalDetailsVM = Util.FormPersonalDetailsandCustomer(personalDetailsVM);
                    personalDetailsVM.RegTypeID = MobileRegType.SimReplacement.ToInt();
                    string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session[SessionKey.ExternalID.ToString()].ToString2()) ? string.Empty : Session[SessionKey.ExternalID.ToString()].ToString2(), regID.ToInt());
                    personalDetailsVM.Liberlization_Status = Result[0];
                    personalDetailsVM.MOCStatus = Result[1];
                    personalDetailsVM.TabNumber = DropForObj.personalInformation.TabNumber;
                    //28052015 - Anthony - to retrieve the liberalization based on the customer level - Start
                    var allCustomerDetails = SessionManager.Get("AllServiceDetails") as retrieveAcctListByICResponse;
                    if (!ReferenceEquals(allCustomerDetails, null))
                    {
                        personalDetailsVM.Liberlization_Status = Util.getMarketCodeLiberalisationMOC(allCustomerDetails, "LIBERALISATION");
                    }
                    //28052015 - Anthony - to retrieve the liberalization based on the customer level - End

                    /*Added by Himansu For PDPA implementation*/
                    using (var registrationProxy = new RegistrationServiceProxy())
                    {
                        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                        personalDetailsVM.PDPDVersion = resultdata.Version;
                    }
                    CustomizedCustomer CustomerPersonalInfo = (CustomizedCustomer)Session["CustPersonalInfo"];
                    List<SelectListItem> lstNationality = Util.GetList(RefType.Nationality, defaultValue: string.Empty);

                    if (!ReferenceEquals(CustomerPersonalInfo, null))
                    {
                        if (!string.IsNullOrEmpty(CustomerPersonalInfo.PassportNo))
                        {
                            personalDetailsVM.Customer.NationalityID = lstNationality.Where(n => n.Text.ToUpper() == NATIONALITY.NonMalaysian.ToString().ToUpper()).SingleOrDefault().Value.ToInt();
                        }
                    }

                    WebHelper.Instance.PopulateDealerInfo(personalDetailsVM);
                    #endregion
                }

                Session[SessionKey.SimType.ToString()] = Util.SIMReplacementSIMType();
                personalDetailsVM.SimType = Session[SessionKey.SimType.ToString()].ToString2();
                Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;


                string planID = string.Empty;
                var masterComponents = MasterDataCache.Instance.PackageComponents;
                masterComponents = masterComponents.Where(x => x.LinkType.Equals(Properties.Settings.Default.SecondaryPlan) && x.PlanType.Equals(Properties.Settings.Default.ComplimentaryPlan) && x.Active).ToList();
                planID = masterComponents.Where(x => x.KenanCode.Equals(Properties.Settings.Default.MISMDefaultPlan)).SingleOrDefault().ID.ToString2();
                var unetPBPCComponentID = ConfigurationManager.AppSettings["UNETPBPCID"].ToString2();
                string vasComponentID = WebHelper.Instance.GetDepenedencyComponents(unetPBPCComponentID);
                Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"] = planID;

                Session["RegMobileReg_VasIDs_Seco"] = vasComponentID;

                //Bugzila #1421 - take logic from SelectSecondaryVAS page (reskin)
                using (var proxy = new CatalogServiceProxy())
                {
                    var respon = new BundlepackageResp();
                    respon = proxy.GetMandatoryVas(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"]));
                    for (int i = 0; i < respon.values.Count(); i++)
                    {
                        if (respon.values[i].Plan_Type == Settings.Default.Master_Component)
                        {
                            Session["intmandatoryidsvalSec"] = respon.values[i].BdlDataPkgId;
                        }

                        if (respon.values[i].Plan_Type == Settings.Default.CompType_DataCon)
                        {
                            Session["intdataidsvalSec"] = respon.values[i].BdlDataPkgId;
                        }
                        
                        if (respon.values[i].Plan_Type == Settings.Default.Extra_Component)
                        {
                           Session["intextraidsvalSec"] = respon.values[i].BdlDataPkgId;
                        }

                        if (respon.values[i].Plan_Type == Settings.Default.Pramotional_Component)
                        {
                            Session["intpramotionidsval"] = respon.values[i].BdlDataPkgId;
                        }
                    }
                }

                var orderSummaryVM = ConstructOrderSummary(regID.ToInt(), personalDetailsVM, (Session["SimReplacementType"].ToInt() == Convert.ToInt32(SimReplacementType.N2M) ? true : false));
                //assigning waiver details to order summary
                if (orderSummary.WaiverComponent != null)
                    orderSummaryVM.WaiverComponent = orderSummary.WaiverComponent;


                Session["KenanAccountNo"] = !ReferenceEquals(reg.KenanAccountNo, null) ? reg.KenanAccountNo : "0";

                if (regID.ToInt() == 0)
                {
                    if (Session[SessionKey.KenanACNumber.ToString()] != null)
                    {
                        Session["KenanAccountNo"] = Session[SessionKey.KenanACNumber.ToString()];
                    }
                }
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderSummaryVM;
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;

            }

            //CR 29JAN2016 - display bill cycle
            if (regID.ToInt() > 0)
            {
                if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault() != null)
                {
                    personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault().ATT_Value;
                }
                else
                {
                    personalDetailsVM.BillingCycle = "N/A";
                }
            }
            else
            {
                var selectedMsisdn = personalDetailsVM.MSISDN1;
                retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                var selectedAccount = AcctListByICResponse.itemList.Where(c => c.ExternalId == selectedMsisdn).FirstOrDefault();
                var billCycle = (selectedAccount != null && selectedAccount.AccountDetails != null ? selectedAccount.AccountDetails.BillCycle : "N/A");
                personalDetailsVM.BillingCycle = (!string.IsNullOrWhiteSpace(billCycle) ? billCycle : "N/A");
            }
            return View(personalDetailsVM);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CustomerSummaryNew(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            var cmssCaseId = string.Empty;
            var cmssCaseStatus = string.Empty;
            var resp = new RegistrationCreateResp();
            var reasonIdStr = collection["SimReplReasonCode"].ToString2();
            SimReplacementReasons simReplacementReasons = new SimReplacementReasons();
            SimReplacementReasonResp simReplacementReasonResp = new SimReplacementReasonResp();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            try
            {
                switch (collection["submit1"].ToString2())
                {

                    case "SubmitReg":
                        #region "Registration Submit"
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            bool isValid = false;
                            string ErrorMessage = string.Empty;

                            int simModelId = 0;
                            int articleId = 0;

                            simModelId = string.IsNullOrEmpty(collection["hdnSimModel"]) ? 0 : int.Parse(collection["hdnSimModel"].Split(',')[0]);
                            articleId = string.IsNullOrEmpty(collection["hdnSimModel"]) ? 0 : int.Parse(collection["hdnSimModel"].Split(',')[1]);

                            var accountTypeStr = Session["AccountType"] != null ? (string)Session["AccountType"] : "";
                            
                            if (!string.IsNullOrEmpty(accountTypeStr) && accountTypeStr.ToLower() == "pregsm")
                            {
                                // gry UOM code problem for replacement reason for prepaid.
                                if (!string.IsNullOrEmpty(reasonIdStr) && (reasonIdStr == "25" || reasonIdStr == "35"))
                                {
                                    var tempModel = CheckSimModelName(articleId.ToString(), "EA");
                                    if (!string.IsNullOrEmpty(tempModel))
                                    {
                                        var splitModel = tempModel.Split('ä');
                                        if (splitModel != null && splitModel.Length > 1)
                                        {
                                            var modelnsim = splitModel[1].Split(',');
                                            if (modelnsim != null && modelnsim.Length > 0)
                                            {
                                                simModelId = modelnsim[0].ToInt();
                                            }
                                        }
                                    }
                                }
                            }

                            //}

                            //16032015 - Anthony - to avoid getting null value from the Session["RegMobileReg_SimReplacementReasons"] - Start
                            using (var catalogService = new CatalogServiceProxy())
                            {
                                simReplacementReasonResp = catalogService.GetSimReplacementReasonDetails(reasonIdStr.ToInt());

                                if (!ReferenceEquals(simReplacementReasonResp, null))
                                {
                                    simReplacementReasons = simReplacementReasonResp.ReplacementReasonsDetails;
                                }
                            }
                            //16032015 - Anthony - to avoid getting null value from the Session["RegMobileReg_SimReplacementReasons"] - End

                            decimal waiverAmt = string.IsNullOrEmpty(collection["hdnwaiverAmount"]) ? 0 : Convert.ToDecimal(collection["hdnwaiverAmount"].ToString2());
                            bool iswaiverOpted = string.IsNullOrEmpty(collection["hdniswaiver"].ToString2()) ? false : Convert.ToBoolean(collection["hdniswaiver"].ToString2());
                            int simRplcType = Session["SimReplacementType"].ToInt();
                            List<MsimDetails> lstMISMSupplies = new List<MsimDetails>();
                            if (Session["RegMobileReg_PersonalDetailsVM"] != null)
                            {
                                personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
                            }
                            #region "Forming Sim Replacement Reason Details"
                            SimReplacementReasonDetails objSimReplacementReasonDet = new SimReplacementReasonDetails();
                            objSimReplacementReasonDet.CmssId = "0";
                            objSimReplacementReasonDet.ActiveDt = string.IsNullOrEmpty(collection["hdnActiveDt"]) ? string.Empty : collection["hdnActiveDt"];
                            objSimReplacementReasonDet.SimModelId = simModelId;
                            objSimReplacementReasonDet.SimArticalId = articleId;
                            objSimReplacementReasonDet.SimType = string.IsNullOrEmpty(collection["hdnSelectedSimType"]) ? string.Empty : collection["hdnSelectedSimType"];
                            if (collection["hdnisPartialwaiver"] == "No")
                            {
                                if (!string.IsNullOrEmpty(collection["hdnInitialPenaltyCharges"]))
                                {

                                    objSimReplacementReasonDet.WaiverAmount = waiverAmt;
                                    objSimReplacementReasonDet.PenaltyAmount = Decimal.Parse(collection["hdnInitialPenaltyCharges"]);
                                }
                                else
                                {
                                    objSimReplacementReasonDet.PenaltyAmount = 0;
                                }
                                objSimReplacementReasonDet.IsWaiver = false;

                            }
                            else
                            {
                                objSimReplacementReasonDet.IsWaiver = true;
                                objSimReplacementReasonDet.PenaltyAmount = 0;
                            }

                            Session["SimReplacement_WaiverDetails"] = objSimReplacementReasonDet;
                            Session["price"] = (objSimReplacementReasonDet.PenaltyAmount.ToString("0.00"));
                            #endregion

                            //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - Start
                            //Retrieve from collection due to personalDetailsVM object was replaced from Session by unknown person
                            personalDetailsVM.OutletCode = collection["OutletCode"].ToString2();
                            personalDetailsVM.OutletChannelId = collection["OutletChannelId"].ToString2();
                            personalDetailsVM.SalesCode = collection["SalesCode"].ToString2();
                            personalDetailsVM.SalesChannelId = collection["SalesChannelId"].ToString2();
                            personalDetailsVM.SalesStaffCode = collection["SalesStaffCode"].ToString2();
                            personalDetailsVM.SalesStaffChannelId = collection["SalesStaffChannelId"].ToString2();

                            // [w.loon] BUG #1583 - retaining SIM serials
                            personalDetailsVM.SimReplacementsVM.PrimarySimSerial = collection["simSerial"].ToString2();
                            personalDetailsVM.SimReplacementsVM.SecondarySimSerial = collection["simSerialSec"].ToString2();

                            WebHelper.Instance.ValidateDealerInfo(personalDetailsVM, out isValid, out ErrorMessage);
                            if (!isValid)
                            {
                                ModelState.AddModelError(string.Empty, ErrorMessage);
                                return View(personalDetailsVM);
                            }
                            //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - End

                            //Constrct waiver components by ravi on mar 04 2014 by ravi
                            List<DAL.Models.WaiverComponents> objWaiverComp = new List<DAL.Models.WaiverComponents>();
                            DAL.Models.RegistrationSec objRegSec = null;
                            List<RegPgmBdlPkgCompSec> objRegSecComps = null;
                            List<RegPgmBdlPkgComp> objRegComps = null;
                            if (Session["SimReplacementType"].ToInt() != Convert.ToInt32(SimReplacementType.PREPAID))
                            {
                                if (Session["SimReplacementType"].ToInt() == Convert.ToInt32(SimReplacementType.N2M))
                                {
                                    objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                                    objRegSec = ConstructRegistrationForSec(collection["QueueNo"].ToString2(), collection["Remarks"].ToString2(), personalDetailsVM.Customer.NationalityID, string.IsNullOrEmpty(collection["hdnSimModelSec"]) ? 0 : int.Parse(collection["hdnSimModelSec"].Split(',')[0]), string.IsNullOrEmpty(collection["hdnSimModelSec"]) ? string.Empty : Convert.ToString(collection["hdnSimModelSec"].Split(',')[1]), Session["IsDealer"].ToBool() ? collection["DealerSecondarySimSerial"].ToString2() : collection["simSerialSec"].ToString2());
                                    objRegSecComps = ConstructRegPgmBdlPkgSecComponent();

                                    if (Session["Comp2GBExists"].ToInt() == 1 && Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt() > 0)
                                    {
                                        objRegComps = ConstructRegPgmBdlPkgComponent();
                                    }

                                }
                                //Constrct waiver components by ravi on mar 04 2014 by ravi ends here

                                objWaiverComp.AddRange(ConstructWaiverDetails(waiverAmt, iswaiverOpted));
                            }

                            #region
                            if (simRplcType == (int)SimReplacementType.M2N)
                            {
                                var AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.SR_PPID.ToString()];
                                foreach (var item in AcctListByICResponse.itemList.Where(v => v.ExternalId == Session["ExternalID"].ToString2() && v.IsMISM == true && (v.ServiceInfoResponse.prinSuppInd == "P" || v.ServiceInfoResponse.prinSuppInd == string.Empty)))
                                {
                                    if (item.SecondarySimList != null && item.SecondarySimList.Count > 0)
                                    {
                                        lstMISMSupplies.AddRange(item.SecondarySimList);
                                        break;
                                    }
                                }
                            }
                            #endregion

                            #region Update BRE Fail Section
                            string JustificationMessage = string.Empty;
                            string HardStopMessage = string.Empty;
                            string ApprovalMessage = string.Empty;
                            bool isBREFail = false;
                            bool ThirdPartyBiometric = false;
                            string ruleOverride = string.Empty;
                            string ThirdParty_BiometricDesc = string.Empty;
                            //if (Session["SimReplacementType"].ToInt() != Convert.ToInt32(SimReplacementType.PREPAID))
                            //{
                                if (collection["ThirdPartyCheckBox"].ToBool())
                                {
                                    ThirdParty_BiometricDesc = Session[SessionKey.ThirdParty_BiometricDesc.ToString()].ToString2();
                                    if (!string.IsNullOrEmpty(ThirdParty_BiometricDesc))
                                    {
                                        ThirdPartyBiometric = true;
                                    }
                                    ruleOverride = "ThirdPartySIM";
                                }
                                WebHelper.Instance.BRECheckMessage(out HardStopMessage, out JustificationMessage, out ApprovalMessage, out isBREFail, ThirdPartyBiometric, ruleOverride);
                                if (!string.IsNullOrEmpty(HardStopMessage))
                                {
                                    ModelState.AddModelError(string.Empty, HardStopMessage);
                                    return View(personalDetailsVM);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(JustificationMessage) && (string.IsNullOrEmpty(collection["Justification"].ToString2())))
                                    {
                                        ModelState.AddModelError(string.Empty, "Please Enter Justification for - " + JustificationMessage);
                                        return View(personalDetailsVM);
                                    }
                                }
                            //}
                            
                            #endregion

                            resp = proxy.RegistrationCreateWithSec(ConstructRegistration(personalDetailsVM, collection["QueueNo"].ToString2(), collection["Remarks"].ToString2(),
                                                                                         simModelId, articleId.ToString(),
                                                                                         objSimReplacementReasonDet.PenaltyAmount,
                                                                                         collection["simSerial"].ToString2(),
                                                                                         collection["SignatureSVG"].ToString2(),
                                                                                         string.Empty,
                                                                                         string.Empty,
                                                                                         string.Empty),
                                                                   WebHelper.Instance.ConstructCustomer(personalDetailsVM),
                                                                   null, WebHelper.Instance.ConstructRegAddress(personalDetailsVM),
                                                                   objRegComps,
                                                                   WebHelper.Instance.ConstructRegStatus_SIMReplacement(),
                                                                   null, null, objRegSec, objRegSecComps, null, null,
                                                                   objWaiverComp, isBREFail);

                            if (resp.ID > 0)
                            {
                                //clearing session if upload image file not exist 
                                if (string.IsNullOrEmpty(collection["idDocumentFront"])) Session["frontImage"] = null;
                                if (string.IsNullOrEmpty(collection["idDocumentBack"])) Session["backImage"] = null;
                                if (string.IsNullOrEmpty(collection["otherDocuments"])) Session["otherImage"] = null;
                                if (string.IsNullOrEmpty(collection["thirdPartyFile"])) Session["thirdImage"] = null;

                                personalDetailsVM.ThirdParty_ApplicantName = collection["authorizedName"].ToString2();
                                personalDetailsVM.ThirdParty_TypeID = collection["ThirdSIMTypeID"].ToString2();
                                personalDetailsVM.ThirdParty_Id = collection["authorizedId"].ToString2();
                                personalDetailsVM.ThirdParty_Contact = collection["authorizedContact"].ToString2();
                                if (!collection["ThirdPartyCheckBox"].ToBool())
                                {
                                    Session[SessionKey.ThirdParty_BiometricVerify.ToString2()] = false;
                                }
                                personalDetailsVM.ThirdParty_Letter = Session["thirdImage"].ToString2();
                                if (personalDetailsVM.ThirdParty_TypeID == "1")
                                {
                                    personalDetailsVM.ThirdParty_BiometricVerify = Session[SessionKey.ThirdParty_BiometricVerify.ToString()].ToBool() ?
                                        Constants.OPTION_YES : Constants.OPTION_NO;
                                }
                                else if (personalDetailsVM.ThirdParty_TypeID == "2" || personalDetailsVM.ThirdParty_TypeID == "4")
                                {
                                    personalDetailsVM.ThirdParty_BiometricVerify = Constants.THIRDPARTY_BIOMETRICVERIFY_NA;
                                }
                                
                                personalDetailsVM.ThirdPartySIM = collection["ThirdPartyCheckBox"].ToBool();

                                personalDetailsVM.Photo = Session["otherImage"].ToString2();
                                personalDetailsVM.CustomerPhoto = Session["frontImage"].ToString2();
                                personalDetailsVM.AltCustomerPhoto = Session["backImage"].ToString2();
                                dropObj.personalInformation = personalDetailsVM;

                                using (var updateProxy = new UpdateServiceProxy())
                                {
                                    if (!string.IsNullOrEmpty(personalDetailsVM.Photo))
                                    {
                                        updateProxy.SavePhotoToDB(personalDetailsVM.Photo, "", "", resp.ID);
                                    }
                                    if (!string.IsNullOrEmpty(personalDetailsVM.CustomerPhoto))
                                    {
                                        updateProxy.SavePhotoToDB("", personalDetailsVM.CustomerPhoto, "", resp.ID);
                                    }
                                    if (!string.IsNullOrEmpty(personalDetailsVM.AltCustomerPhoto))
                                    {
                                        updateProxy.SavePhotoToDB("", "", personalDetailsVM.AltCustomerPhoto, resp.ID);
                                    }
                                }
                                #region 14052015 - Anthony - Save document to tblreguploaddoc for iContract
                                bool iContractActive = ConfigurationManager.AppSettings["iContractActive"].ToBool();
                                if (iContractActive)
                                {
                                    var firstBase64index = 0;
                                    var lastBase64index = 0;

                                    if (!string.IsNullOrEmpty(personalDetailsVM.CustomerPhoto))
                                    {
                                        firstBase64index = personalDetailsVM.CustomerPhoto.IndexOf("data:");
                                        lastBase64index = personalDetailsVM.CustomerPhoto.IndexOf(',');
                                        dropObj.personalInformation.idDocumentFront = personalDetailsVM.CustomerPhoto.Remove(firstBase64index, lastBase64index + 1);
                                    }

                                    if (!string.IsNullOrEmpty(personalDetailsVM.AltCustomerPhoto))
                                    {
                                        firstBase64index = personalDetailsVM.AltCustomerPhoto.IndexOf("data:");
                                        lastBase64index = personalDetailsVM.AltCustomerPhoto.IndexOf(',');
                                        dropObj.personalInformation.idDocumentBack = personalDetailsVM.AltCustomerPhoto.Remove(firstBase64index, lastBase64index + 1);
                                    }

                                    if (!string.IsNullOrEmpty(personalDetailsVM.Photo))
                                    {
                                        firstBase64index = personalDetailsVM.Photo.IndexOf("data:");
                                        lastBase64index = personalDetailsVM.Photo.IndexOf(',');
                                        dropObj.personalInformation.otherDocuments = personalDetailsVM.Photo.Remove(firstBase64index, lastBase64index + 1);
                                    }

                                    if (!string.IsNullOrEmpty(personalDetailsVM.ThirdParty_Letter))
                                    {
                                        firstBase64index = personalDetailsVM.ThirdParty_Letter.IndexOf("data:");
                                        lastBase64index = personalDetailsVM.ThirdParty_Letter.IndexOf(',');
                                        dropObj.personalInformation.otherDocuments = personalDetailsVM.ThirdParty_Letter.Remove(firstBase64index, lastBase64index + 1);
                                    }
                                    WebHelper.Instance.SaveDocumentToTable(dropObj, resp.ID);
                                }
                                #endregion

                                SimReplacementVm objSimReplacementVm = new SimReplacementVm();
                                if (!ReferenceEquals(simReplacementReasons, null))
                                {
                                    if (Session["price"] != null)
                                    {
                                        simReplacementReasons.Price = Session["price"].ToDecimal();
                                    }
                                    objSimReplacementVm.SimReplacementReasons = simReplacementReasons;
                                }
                                else
                                {
                                    objSimReplacementVm.SimReplacementReasons = (SimReplacementReasons)Session["RegMobileReg_SimReplacementReasons"];
                                }
                                objSimReplacementVm.simDetlsList = (SimDetlsList)Session["Old_SimDetails"];
                                objSimReplacementVm.SimReplacementReasonDetail = objSimReplacementReasonDet;

                                #region Insert into lnkRegDetails
                                LnkRegDetailsReq objRegDetailsReqobj = new LnkRegDetailsReq();
                                Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                                objRegDetails.CreatedDate = DateTime.Now;
                                objRegDetails.IsSuppNewAc = true;
                                objRegDetails.UserName = Util.SessionAccess.UserName;
                                objRegDetails.RegId = resp.ID;
                                objRegDetails.CmssId = objSimReplacementReasonDet.CmssId;
                                objRegDetails.ActiveDt = objSimReplacementReasonDet.ActiveDt;
                                objRegDetails.SimModelId = objSimReplacementReasonDet.SimModelId;
                                objRegDetails.SimArticalId = objSimReplacementReasonDet.SimArticalId;
                                objRegDetails.SimType = objSimReplacementReasonDet.SimType;
                                objRegDetails.OldSimSerial = objSimReplacementVm.simDetlsList.ExteranalId;
                                objRegDetails.NewSimSerial = collection["simSerial"].ToString2();
                                objRegDetails.SimReplReasonCode = objSimReplacementVm.SimReplacementReasons == null ? 0 : objSimReplacementVm.SimReplacementReasons.DISCONNECT_REASON;
                                objRegDetails.SimReplReason = ReferenceEquals(objSimReplacementVm.SimReplacementReasons, null) ? string.Empty : objSimReplacementVm.SimReplacementReasons.DISPLAY_VALUE;
                                objRegDetails.SIMModel = objSimReplacementVm.simDetlsList.ExteranalIdType;
                                objRegDetails.AccountType = Session["AccountType"].ToString2();
                                objRegDetails.NrcId = objSimReplacementVm.SimReplacementReasons == null ? string.Empty : objSimReplacementVm.SimReplacementReasons.NRC.ToString();
                                objRegDetails.TotalLineCheckCount = Session["existingTotalLine"].ToInt();
                                //Insert justification
                                if (collection["Justification"].ToString2().Length > 0)
                                    objRegDetails.Justification = collection["Justification"].ToString();
                                if (Session["AccountType"].ToString2().ToUpper() == "PREGSM" && collection["tutSimSerial"].ToString2().Length > 0)
                                {
                                    objRegDetails.TUTSerial = collection["tutSimSerial"].ToString().Trim();

                                    int tutSimModelId = 0;
                                    string strTUTArtucleID = ReferenceEquals(Session["TUTArticleID"], null) ? string.Empty : Session["TUTArticleID"].ToString();
                                    if (!string.IsNullOrEmpty(strTUTArtucleID))
                                    {
                                        tutSimModelId = proxy.GetSIMModelIDByArticleID(strTUTArtucleID);
                                    }
                                    objRegDetails.TUTSimModelId = tutSimModelId;
                                }
                                objRegDetails.SimReplacementType = Session["SimReplacementType"].ToInt();
                                objRegDetails.MOC_Status = personalDetailsVM.MOCStatus;
                                objRegDetails.PdpaVersion = personalDetailsVM.PDPDVersion;
                                objRegDetails.Liberlization_Status = personalDetailsVM.Liberlization_Status;

                                bool IsWriteOff = false;
                                string acc = string.Empty;
                                WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
                                objRegDetails.WriteOffDetails = acc;

                                objRegDetailsReqobj.LnkDetails = objRegDetails;
                                // display the old sim serial number in success page print.
                                Session["OldSimSerial"] = objSimReplacementVm.simDetlsList.ExteranalId.ToString2();
                                proxy.SaveLnkRegistrationDetails(objRegDetailsReqobj);
                                    
                                if (simRplcType == (int)SimReplacementType.M2N && lstMISMSupplies.Count > 0)
                                {
                                    List<lnkSecondaryAcctDetailsSimRplc> lstSecondarAccts = new List<lnkSecondaryAcctDetailsSimRplc>();
                                    foreach (var item in lstMISMSupplies)
                                    {
                                        lnkSecondaryAcctDetailsSimRplc item1 = new lnkSecondaryAcctDetailsSimRplc();
                                        item1.REGID = resp.ID;
                                        item1.CREATEDATE = DateTime.Now;
                                        item1.DESCRIPTION = Session["ExternalID"].ToString2() + "_" + item.Msisdn;
                                        item1.EXTERNALID = Session["ExternalID"].ToString2();
                                        item1.FxAcctNo = item.FxAcctNo;
                                        item1.FxSubscrNo = item.FxSubscrNo;
                                        item1.FxSubscrNoResets = item.FxSubscrNoResets;
                                        item1.PrimSecInd = item.PrimSecInd;
                                        item1.STATUS = true;
                                        item1.SUPP_Msisdn = item.Msisdn;
                                        lstSecondarAccts.Add(item1);
                                    }
                                    if (lstSecondarAccts.Count > 0)
                                        proxy.RegSecondaryAccountsForSimRplc(lstSecondarAccts);
                                }
                                #region created by Raj on 4/14/2014 for save data componets in lnkpackagecomponets for disconnect only for normal to mism flow

                                if (simRplcType == (int)SimReplacementType.N2M)
                                {
                                    List<PackageComponents> pckg = ConstructDisconnectComponents(Session[SessionKey.RegMobileReg_MobileNo.ToString()] == null ? null : ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2());
                                    proxy.SaveLnkPackageComponet(resp.ID, pckg, 13);
                                }

                                #endregion
                                
                                #endregion

                                #region Insert into lnkRegAttributes
                                var regAttribList = new List<RegAttributes>();
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCODE, ATT_Value = personalDetailsVM.OutletCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCHANNELID, ATT_Value = personalDetailsVM.OutletChannelId });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCODE, ATT_Value = personalDetailsVM.SalesCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCHANNELID, ATT_Value = personalDetailsVM.SalesChannelId });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCODE, ATT_Value = personalDetailsVM.SalesStaffCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCHANNELID, ATT_Value = personalDetailsVM.SalesStaffChannelId });
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_PAYMENT_METHOD, ATT_Value = Util.getPaymentMethodName() });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.BillingCycle))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_ACC_BILL_CYCLE, ATT_Value = personalDetailsVM.BillingCycle });


                                if(personalDetailsVM.ThirdPartySIM == true)
                                {
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_3RDPARTY_ORDER, ATT_Value = personalDetailsVM.ThirdPartySIM.ToString2() });
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_3RDPARTY_NAME, ATT_Value = personalDetailsVM.ThirdParty_ApplicantName });
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_3RDPARTY_TYPE_ID, ATT_Value = personalDetailsVM.ThirdParty_TypeID });
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_3RDPARTY_ID, ATT_Value = personalDetailsVM.ThirdParty_Id });
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_3RDPARTY_CONTACT, ATT_Value = personalDetailsVM.ThirdParty_Contact });
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_3RDPARTY_BIOMETRIC_VERIFY, ATT_Value = personalDetailsVM.ThirdParty_BiometricVerify });
                                }
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_USER_DEVICE_AGENT, ATT_Value = Request.UserAgent });
                                proxy.SaveListRegAttributes(regAttribList);
                                #endregion

                                #region Insert into trnRegJustification
                                WebHelper.Instance.saveTrnRegJustification(resp.ID);
                                #endregion

                                #region Insert into trnRegBreFailTreatment
                                WebHelper.Instance.saveTrnRegBreFailTreatment(resp.ID);
                                #endregion

                                var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
                                if (biometricID != 0)
                                {
                                    var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
                                    biometric.RegID = resp.ID;
                                    proxy.BiometricUpdate(biometric);
                                }

                                if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
                                {
                                    Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
                                }

                                using (var proxytransaction = new UserServiceProxy())
                                {
                                    proxytransaction.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SimReplacement, "Registration Done with RegId " + resp.ID, Session["KenanACNumber"].ToString(), "");
                                }


                                #region Drop 5 - update Status to PendingCDPU and save to lnkRegBREStatus
                                // all BRE Failed from Dealer route to CDPU
                                if (isBREFail && Session["IsDealer"].ToBool())
                                {
                                    try
                                    {
                                        proxy.UpsertBRE(new RegBREStatus()
                                        {
                                            RegId = resp.ID,
                                            TransactionStatus = Constants.StatusCDPUPending
                                        });// have bre failed
                                        personalDetailsVM.CDPUStatus = Constants.StatusCDPUPending;

                                        proxy.RegistrationCancel(new RegStatus()
                                        {
                                            RegID = resp.ID,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            StartDate = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCDPUPendingApp)
                                        });
                                        return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });

                                    }
                                    catch
                                    {

                                    }
                                }
                                else
                                {
                                    WebHelper.Instance.WebPosAuto(resp.ID);
                                }
                                #endregion

                                #region email functionality

                                if (Session["KenanCustomerInfo"] != null)
                                {
                                    string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString().Split('╚');
                                    KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
                                    kenanInfo.FullName = strArrKenanInfo[0];
                                    kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
                                    kenanInfo.IDCardNo = strArrKenanInfo[2];
                                    kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
                                    kenanInfo.Address1 = strArrKenanInfo[4];
                                    kenanInfo.Address2 = strArrKenanInfo[5];
                                    kenanInfo.Address3 = strArrKenanInfo[6];
                                    kenanInfo.Postcode = strArrKenanInfo[8];
                                    kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
                                    kenanInfo.RegID = resp.ID;
                                    var kenanResp = new KenanCustomerInfoCreateResp();
                                    kenanResp = proxy.KenanCustomerInfoCreate(kenanInfo);

                                }
                                Session["KenanCustomerInfo"] = null;
                                WebHelper.Instance.ClearRegistrationSession();
                                Session["Comp2GBExists"] = null;
                                #region "Generating Impos File and Moving the status to ready for Payment"
                                try
                                {
                                    
                                    #region Code for SRWWO by chetan
                                    if (Session["AllowSimReplacement"].ToString2() == "True")
                                    {
                                        string _selectedexternalID = !ReferenceEquals(Session[SessionKey.ExternalID.ToString()], null) ? Session[SessionKey.ExternalID.ToString()].ToString() : null;
                                        var custIdentity = string.Empty;
                                        if (Session[SessionKey.PPID.ToString()] != null)
                                        {
                                            var AcctListByICResponse = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                                            SubscriberICService.CustomizedCustomer customerInfo = new CustomizedCustomer();
                                            SubscriberICService.Items selectedAccount = new Items();
                                            selectedAccount = AcctListByICResponse.itemList.Where(v => v.ExternalId == _selectedexternalID).FirstOrDefault();

                                            if (selectedAccount.Account != null)
                                            {
                                                customerInfo = selectedAccount.Customer;
                                                if (customerInfo.NewIC != string.Empty)
                                                    custIdentity = customerInfo.NewIC;
                                                else if (customerInfo.PassportNo != string.Empty)
                                                    custIdentity = customerInfo.PassportNo;
                                                else if (customerInfo.OldIC != string.Empty)
                                                    custIdentity = customerInfo.OldIC;
                                                else if (customerInfo.OtherIC != string.Empty)
                                                    custIdentity = customerInfo.OtherIC;
                                                CreateInteractionCaseResponse Cresp = new CreateInteractionCaseResponse();
                                                try
                                                {
                                                    if (WebHelper.Instance.CompareCMSSDtForAcct(custIdentity) > 14.0)
                                                    {
                                                        Cresp = WebHelper.Instance.CreateCaseInCMSS(selectedAccount.AcctExtId.ToString2(),
                                                           Constants.CMSS_CASE_COMPLEXITY,
                                                           ConfigurationManager.AppSettings["CMSSQueue"].ToString2(),
                                                           "59",
                                                           ConfigurationManager.AppSettings["CMSSDirection"].ToString2(),
                                                           selectedAccount.Account != null ? selectedAccount.Account.AccountNumber.ToString2() : "",
                                                           selectedAccount.ExternalId.ToString2(),
                                                           (collection["NewCBR"].ToString().Length > 0 ? collection["NewCBR"].ToString2() : string.Empty),
                                                           ConfigurationManager.AppSettings["CMSSProduct"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSReason1"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSReason2"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSReason3"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSSysName"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSTitle"].ToString2(),
                                                           Constants.CMSS_TYPE,
                                                           Constants.CMSS_CASE_TOPROCEEDWITH,
                                                            resp.ID.ToInt(),
                                                            custIdentity.ToString2());

                                                        ViewBag.AllowSubmit = "True";
                                                        if (Cresp != null && !string.IsNullOrEmpty(Cresp.Id))
                                                        {
                                                            cmssCaseStatus = "Success";
                                                            cmssCaseId = Cresp.Id;
                                                        }
                                                        else
                                                            cmssCaseStatus = "Fail";
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    cmssCaseStatus = "Fail";
                                                    Logger.Info("Exception while CMSS Case Creation : ExternalID -" + selectedAccount.AcctExtId.ToString2() + " : Message - " + ex.Message + ": InnerException - " + ex.InnerException);
                                                    string xmlReq = Util.ConvertObjectToXml(selectedAccount);
                                                    Logger.Info("Exception while CMSS Case Creation for Account Selected : XML -" + xmlReq.ToString2());
                                                }
                                            }

                                        }
                                    }
                                    #endregion upto here code for SRWWO
                                    TempData["CMSSCaseID"] = cmssCaseId;
                                    TempData["CMSSCaseStatus"] = cmssCaseStatus;

                                    decimal simReplacementTotalPayable = !string.IsNullOrWhiteSpace(Session["SIMReplacementTotalPayable"].ToString2()) ? Session["SIMReplacementTotalPayable"].ToDecimal() : 0;
                                    decimal hdnTotalPayable = !string.IsNullOrWhiteSpace(collection["hdnTotalPayable"].ToString2()) ? collection["hdnTotalPayable"].ToDecimal() : 0;

                                    #region 11052015 - Anthony - Generate RF for iContract
                                    try
                                    {
                                        using (var updateProxy = new UpdateServiceProxy())
                                        {
                                            if (!updateProxy.FindDocument(Constants.CONTRACT, resp.ID))
                                            {
                                                String contractHTMLSource = string.Empty;

                                                if (simRplcType == SimReplacementType.PREPAID.ToInt())
                                                {
                                                    contractHTMLSource = Util.RenderPartialViewToString(this, "SimReplacementPrint", WebHelper.Instance.GetContractDetailsViewModel_PrepaidSIMReplacement(resp.ID, false));
                                                }
                                                else
                                                {
                                                    contractHTMLSource = Util.RenderPartialViewToString(this, "CofServiceFormPrint", WebHelper.Instance.GetContractDetailsViewModel_SIMReplacement(resp.ID, false));
                                                }
                                                contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
                                                contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
                                                contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide");
                                                if (!string.IsNullOrEmpty(contractHTMLSource))
                                                {
                                                    Byte[] PDFBytes = Util.convertHTMLStringToPDFByteArray(contractHTMLSource, resp.ID);
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        //Logger.Error("ICONTRACT EXCEPTION = " + ex);
                                        WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                    }
                                    #endregion

                                    // Gerry - comment this because auto knock off not only for dealer
                                    //if (Session["ISDealer"].ToBool
                                    if (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove)
                                    {
                                        #region Auto activation/Ready for Payment redirection for Dealer

                                        if (simReplacementTotalPayable == 0 && hdnTotalPayable == 0)
                                        {
                                            if (resp.ID > 0)
                                            {
                                                personalDetailsVM.RegID = resp.ID;
                                            }

                                            #region "Cashier Payment Received Section for Dealer"

                                            int _simReplacementTypeAuto = 0;
                                            bool isPrimaryLineCompsAddedAuto = false;
                                            var requestAuto = new OrderFulfillRequest()
                                            {
                                                OrderID = personalDetailsVM.RegID.ToString(),
                                                UserSession = Util.SessionAccess.UserName,

                                            };
                                            UpdateRegStatus(new RegStatus()
                                            {
                                                RegID = personalDetailsVM.RegID,
                                                Active = true,
                                                CreateDT = DateTime.Now,
                                                StartDate = DateTime.Now,
                                                LastAccessID = Util.SessionAccess.UserName,
                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                                            });


                                            string usernameAuto = string.Empty;
                                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                                            {
                                                usernameAuto = Request.Cookies["CookieUser"].Value;
                                            }
                                            eaiResponseType objeaiResponseTypeAuto = new eaiResponseType();
                                            LnkRegDetailsReq objRegDetailsReqAuto = new LnkRegDetailsReq();
                                            if (personalDetailsVM.RegID > 0)
                                            {
                                                using (var regProxy = new RegistrationServiceProxy())
                                                {
                                                    objRegDetailsReqAuto = regProxy.GetLnkRegistrationDetails(personalDetailsVM.RegID);
                                                    _simReplacementTypeAuto = objRegDetailsReqAuto.LnkDetails.SimReplacementType.ToInt();
                                                }
                                            }

                                            #region "Normal to MISM Convertion Flow"
                                            if (_simReplacementTypeAuto > 0 && _simReplacementTypeAuto == (int)SimReplacementType.N2M)
                                            {
                                                #region "Primary Line Details"
                                                DAL.Models.Registration reg = null;
                                                bool isPrimaryCompAdded = false;
                                                using (var prxy = new RegistrationServiceProxy())
                                                {
                                                    reg = prxy.RegistrationGet(personalDetailsVM.RegID);

                                                    var regPgmBdlPkgCompIDs = prxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                                                    {
                                                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                                                        {
                                                            RegID = reg.ID,
                                                        }
                                                    }).ToList();

                                                    #region "Add Greater than 2GB Components functional logic"
                                                    if (regPgmBdlPkgCompIDs.Count() > 0)
                                                    {
                                                        using (var proxyKenan = new KenanServiceProxy())
                                                        {
                                                            if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                                                            {
                                                                List<int> Ids = new List<int>();
                                                                Ids.Add(reg.ID);
                                                                CenterOrderContractRequest Req = new CenterOrderContractRequest();
                                                                Req.orderId = personalDetailsVM.RegID.ToString();
                                                                if (Request.Cookies["CookieUser"] != null)
                                                                    Req.UserName = Request.Cookies["CookieUser"].Value;
                                                                isPrimaryCompAdded = proxyKenan.CenterOrderContractCreationPrimaryLine(Req);
                                                                isPrimaryLineCompsAddedAuto = true;
                                                                if (!isPrimaryCompAdded)
                                                                {
                                                                    Logger.InfoFormat("{0}:{1}# Primary Line Components registration Failed, response{2}", "QSimReplacement", "CustomerSummary", isPrimaryCompAdded);
                                                                    UpdateRegStatus(new RegStatus()
                                                                    {
                                                                        RegID = personalDetailsVM.RegID,
                                                                        Active = true,
                                                                        CreateDT = DateTime.Now,
                                                                        StartDate = DateTime.Now,
                                                                        LastAccessID = Util.SessionAccess.UserName,
                                                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_ADDCOMP_FAIL)
                                                                    });
                                                                    return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });

                                                                }

                                                                using (var regProxy = new RegistrationServiceProxy())
                                                                {
                                                                    regProxy.RegistrationCancel(new RegStatus()
                                                                    {
                                                                        RegID = personalDetailsVM.RegID,
                                                                        Active = true,
                                                                        CreateDT = DateTime.Now,
                                                                        StartDate = DateTime.Now,
                                                                        LastAccessID = Util.SessionAccess.UserName,
                                                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_SvcPendingActivate)
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                }
                                                #endregion

                                                #region "Secondary Line Details"
                                                if (!isPrimaryLineCompsAddedAuto) // in case of primary line components not exists or created and only secondary line should be created.
                                                {
                                                    using (var proxySec = new RegistrationServiceProxy())
                                                    {
                                                        var RegSec = proxySec.RegistrationSecGet(personalDetailsVM.RegID);
                                                        var pbpcIDsSec = new List<int>();
                                                        if (RegSec != null)
                                                        {
                                                            var regPgmBdlPkgCompIDsSec = proxySec.RegPgmBdlPkgCompSecFind(new RegPgmBdlPkgCompSecFind()
                                                            {
                                                                RegPgmBdlPkgCompSec = new RegPgmBdlPkgCompSec()
                                                                {
                                                                    RegID = RegSec.ID,
                                                                }
                                                            }).ToList();
                                                            var pbpcSec = new List<RegPgmBdlPkgCompSec>();
                                                            if (regPgmBdlPkgCompIDsSec.Count() > 0)
                                                                pbpcSec = proxySec.RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec).ToList();
                                                            //main line
                                                            if (pbpcSec.Count() > 0)
                                                                pbpcIDsSec = pbpcSec.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();
                                                            if (!ReferenceEquals(objRegDetailsReqAuto, null) && !ReferenceEquals(objRegDetailsReqAuto.LnkDetails.SimType, null) && objRegDetailsReqAuto.LnkDetails.SimType == "MISM")
                                                            {
                                                                // Added the functionality of registering the additional secondary line for MISM
                                                                var regSecondary = RegSec;
                                                                var isMISM = regSecondary == null ? false : true;
                                                                var extid = new List<KenanTypeExternalId>();
                                                                var extidnew = new List<KenanTypeExternalId>();
                                                                extidnew.Add(new KenanTypeExternalId() { ExternalId = string.Empty, ExternalIdType = string.Empty });
                                                                extid.Add(new KenanTypeExternalId() { ExternalId = reg.MSISDN1 == null ? "" : reg.MSISDN1, ExternalIdType = (reg.AccExternalID == null ? "" : reg.AccExternalID) });
                                                                if (isMISM == true && regSecondary.IsSecondaryRequestSent == false)
                                                                {
                                                                    var regAccount = new DAL.Models.RegAccount();
                                                                    regSecondary.IsSecondaryRequestSent = true;
                                                                    using (var reproxy = new RegistrationServiceProxy())
                                                                    {
                                                                        regSecondary.IsSecondaryRequestSent = true;
                                                                        int result = reproxy.UpdateSecondaryRequestSent(regSecondary.ID);
                                                                    }
                                                                    using (var kProxy = new KenanServiceProxy())
                                                                    {
                                                                        var request2 = new KenanTypeAdditionLineRegistration()
                                                                        {
                                                                            OrderId = "ISELLMISM" + reg.ID.ToString() + "SIM",
                                                                            FxAcctNo = (reg.fxAcctNo == null) ? "" : reg.fxAcctNo,
                                                                            ExternalId = (extid.ToList().ToArray() == null) ? extidnew.ToList().ToArray() : extid.ToList().ToArray(),
                                                                        };
                                                                        var response = kProxy.KenanAdditionLineRegistrationMISMSecondary(request2);
                                                                        if (response.Code != "0")
                                                                        {
                                                                            Logger.InfoFormat("{0}:{1} Secondary Components Registration Failed at Kenan , Response{2}:{3}", "QSimReplacement", "CustomerSummary", response.Code.ToString2(), response.Message.ToString2());
                                                                            UpdateRegStatus(new RegStatus()
                                                                            {
                                                                                RegID = personalDetailsVM.RegID,
                                                                                Active = true,
                                                                                CreateDT = DateTime.Now,
                                                                                StartDate = DateTime.Now,
                                                                                LastAccessID = Util.SessionAccess.UserName,
                                                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SECCT_FAIL)
                                                                            });
                                                                            return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                                                        }
                                                                        else
                                                                        {
                                                                            UpdateRegStatus(new RegStatus()
                                                                            {
                                                                                RegID = personalDetailsVM.RegID,
                                                                                Active = true,
                                                                                CreateDT = DateTime.Now,
                                                                                StartDate = DateTime.Now,
                                                                                LastAccessID = Util.SessionAccess.UserName,
                                                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SECCT)
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion

                                            }
                                            #endregion
                                            // for normal to MISM flow, we should call the SIMSWAP from FullFillORder Success for SecondaryLine Creation.
                                            if (_simReplacementTypeAuto > 0 && _simReplacementTypeAuto != (int)SimReplacementType.N2M)
                                            {
                                                #region "Incase of Mism(primary) to Normal flow , need to disconnect MISM secondary Lines"
                                                if (_simReplacementTypeAuto == (int)SimReplacementType.M2N)
                                                {
                                                    List<lnkSecondaryAcctDetailsSimRplc> SecAccountLines = new List<lnkSecondaryAcctDetailsSimRplc>();
                                                    using (var regproxy = new RegistrationServiceProxy())
                                                    {
                                                        SecAccountLines = regproxy.GetSecondaryAccountLinesForSimRplc(personalDetailsVM.RegID);
                                                    }
                                                    //TODO: 1. NEED TO CHECK WHETHER WE ARE PROCEEDING WITH MISM-P OR MISM-S
                                                    //TODO: 2. NEED TO CONSTRUCT THE SECONDALY LINES FOR MISM-P
                                                    if (SecAccountLines.Count > 0)
                                                    {
                                                        using (var kenanProxy = new KenanServiceProxy())
                                                        {
                                                            int indexval = 0;
                                                            foreach (var itemval in SecAccountLines)
                                                            {
                                                                indexval++;
                                                                kenanProxy.DisconnectComponents(new DisconnectCenterServiceRequest()
                                                                {
                                                                    orderId = "ISELL" + personalDetailsVM.RegID + indexval + "D",
                                                                    newOrderInd = "Y",
                                                                    fxAcctNo = itemval.FxAcctNo,
                                                                    fxSubscrNo = itemval.FxSubscrNo,
                                                                    fxSubscrNoResets = itemval.FxSubscrNoResets,
                                                                    disconnectReason = "1",
                                                                    inpF01 = Util.SessionAccess.UserName,
                                                                    inpF02 = personalDetailsVM.OrganisationId
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion

                                                using (var proxyKenan = new KenanServiceProxy())
                                                {
                                                    objeaiResponseTypeAuto = proxyKenan.SwapCenterInventory(personalDetailsVM.RegID);
                                                }
                                                if (!string.IsNullOrEmpty(objeaiResponseTypeAuto.msgCodeField))
                                                {
                                                    if (int.Parse(objeaiResponseTypeAuto.msgCodeField) > 0)
                                                    {
                                                        UpdateRegStatus(new RegStatus()
                                                        {
                                                            RegID = personalDetailsVM.RegID,
                                                            Active = true,
                                                            CreateDT = DateTime.Now,
                                                            StartDate = DateTime.Now,
                                                            LastAccessID = Util.SessionAccess.UserName,
                                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SWAP_FAIL)
                                                        });
                                                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                                    }
                                                }
                                                else
                                                {
                                                    UpdateRegStatus(new RegStatus()
                                                    {
                                                        RegID = personalDetailsVM.RegID,
                                                        Active = true,
                                                        CreateDT = DateTime.Now,
                                                        StartDate = DateTime.Now,
                                                        LastAccessID = Util.SessionAccess.UserName,
                                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SWAP_FAIL)
                                                    });
                                                    return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                                }
                                            }
                                            #endregion

                                            Session["SIMReplacementTotalPayable"] = null;

                                            return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });

                                        }
                                        else
                                        {
                                            Session["SIMReplacementTotalPayable"] = null;
                                            if (Roles.IsUserInRole("MREG_DSV") && false)
                                                return RedirectToAction("MobileRegAccCreated", new { regID = resp.ID });
                                            else
                                                return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                        return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                                    }


                                }
                                catch (Exception ex)
                                {
                                    WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                    using (var regProxy = new RegistrationServiceProxy())
                                    {
                                        regProxy.RegistrationCancel(new RegStatus()
                                        {
                                            RegID = resp.ID,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            StartDate = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                                        });
                                    }
                                    return RedirectToAction("MobileRegFail", "QSimReplacement", new { regID = resp.ID });
                                }

                                #endregion

                            }

                                #endregion


                        }
                        #endregion
                        break;

                    default:
                        #region BackButton
                        if (personalDetailsVM.TabNumber == (int)SIMReplacementStep.AccountDetails)
                        {
                            return RedirectToAction(WebHelper.Instance.GetSimReplacementActionStep(personalDetailsVM.TabNumber));
                        }
                        if (personalDetailsVM.TabNumber == (int)SIMReplacementStep.VAS)
                        {
                            return RedirectToAction(WebHelper.Instance.GetSimReplacementActionStep(personalDetailsVM.TabNumber), new { planid = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()].ToInt() });
                        }
                        #endregion
                        break;
                }


            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

            return RedirectToAction("MobileRegFail", new { regID = resp.ID });

        }

        [Authorize(Roles = "MREG_DAPP,MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_HQ,MREG_DAB,MREG_DAI,MREG_DAC,MREG_DSK,MREG_DCH,MREG_DC,MREG_DN,MREG_DIC")]
        [DoNoTCache]
        public ActionResult CustomerSummary(int? regID)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var personalDetailsVM = new PersonalDetailsVM();
            var orderSummary = new OrderSummaryVM();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            var mainLinePBPCIDsSec = new List<int>();
            var regSec = new DAL.Models.RegistrationSec();
            var suppLineRegPBPCsSec = new List<RegPgmBdlPkgCompSec>();
            DAL.Models.RegistrationSec registrationSec = new DAL.Models.RegistrationSec();
            var allRegDetails = new Online.Registration.DAL.RegistrationDetails();
            int paymentstatus = -1;

            try
            {
                var KenanLogDetails = new DAL.Models.KenanaLogDetails();
                if (regID.ToInt() > 0)
                {
                    #region regID > 0
                    WebHelper.Instance.ClearRegistrationSession();

                    Session["RegMobileReg_PhoneVM"] = null;

                    using (var proxy = new RegistrationServiceProxy())
                    {
                        allRegDetails = proxy.GetRegistrationFullDetails(regID.ToInt());

                        if (allRegDetails.Customers.FirstOrDefault().IsVIP == 1 && !Roles.IsUserInRole("MREG_SV"))
                        {
                            Session["VIPFailMsg"] = true;
                            return RedirectToAction("StoreKeeper", "Registration");
                        }

                        WebHelper.Instance.MapRegistrationDetailsToVM(false, regID.Value.ToInt(), ref allRegDetails, out personalDetailsVM);
                        reg = allRegDetails.Regs.SingleOrDefault();

                        SimReplacementReasonDetails objSimReplacementReasonDetails = new SimReplacementReasonDetails();
                        objSimReplacementReasonDetails.PenaltyAmount = personalDetailsVM.PenaltyAmount;
                        if (!ReferenceEquals(allRegDetails.lnkregdetails, null))
                        {
                            objSimReplacementReasonDetails.CmssId = allRegDetails.lnkregdetails.CmssId.ToString2();
                            objSimReplacementReasonDetails.ActiveDt = allRegDetails.lnkregdetails.ActiveDt;
                            objSimReplacementReasonDetails.SimModelId = allRegDetails.lnkregdetails.SimModelId.GetValueOrDefault();
                            objSimReplacementReasonDetails.SimArticalId = allRegDetails.lnkregdetails.SimArticalId.GetValueOrDefault();
                            Online.Registration.DAL.Models.SimReplacementReasons objSimReplacementReasons = new SimReplacementReasons();
                            objSimReplacementReasons.DISPLAY_VALUE = allRegDetails.lnkregdetails != null ? allRegDetails.lnkregdetails.SimReplReason.ToString2() : string.Empty;
                            Session["RegMobileReg_SimReplacementReasons"] = objSimReplacementReasons;
                            Session["SimReplacementType"] = allRegDetails.lnkregdetails.SimReplacementType.ToInt();
                        }
                        else
                        {
                            objSimReplacementReasonDetails.CmssId = string.Empty;
                            personalDetailsVM.AccountType = string.Empty;
                        }

                        objSimReplacementReasonDetails.IsWaiver = personalDetailsVM.PenaltyAmount > 0 ? false : true;
                        Session["SimReplacement_WaiverDetails"] = objSimReplacementReasonDetails;

                        if (string.IsNullOrEmpty(personalDetailsVM.RegSIMSerial))
                        {
                            Online.Registration.Web.SubscriberICService.retrieveSimDetlsResponse objRetrieveSimDetlsResponse = new Online.Registration.Web.SubscriberICService.retrieveSimDetlsResponse();
                            using (var Serviceproxy = new retrieveServiceInfoProxy())
                            {
                                objRetrieveSimDetlsResponse = Serviceproxy.retreiveSIMDetls(personalDetailsVM.MSISDN1);
                            }

                            SimDetlsList objSimDetlsList = null;
                            if (objRetrieveSimDetlsResponse != null)
                            {
                                List<Online.Registration.Web.SubscriberICService.SimDetails> simDetlsFiltered = objRetrieveSimDetlsResponse.itemList.Where(c => c.InactiveDt == null || c.InactiveDt == string.Empty).ToList();
                                if (!ReferenceEquals(simDetlsFiltered, null) && simDetlsFiltered.Count > 0)
                                {
                                    objSimDetlsList = new SimDetlsList(
                                    simDetlsFiltered[0].ExternalID.ToString2(),
                                    simDetlsFiltered[0].ExternalIDType.ToString2(),
                                    simDetlsFiltered[0].InventoryTypeID.ToString2(),
                                    simDetlsFiltered[0].InventoryDisplayValue,
                                    simDetlsFiltered[0].Reason,
                                    simDetlsFiltered[0].ActiveDt,
                                    simDetlsFiltered[0].InactiveDt
                                   );
                                    Session["Old_SimDetails"] = objSimDetlsList;
                                }


                            }
                        }

                        // MobileNo
                        var mobileNos = new List<string>();
                        mobileNos.Add(reg.MSISDN1);
                        mobileNos.Add(reg.MSISDN2);
                        Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;

                        // RegPgmBdlPkgComponent
                        var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                        {
                            RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                            {
                                RegID = regID.Value
                            },
                            Active = true
                        }).ToList();

                        // Main Line PgmBdlPkgComponent
                        var regPBPCs = new List<RegPgmBdlPkgComp>();
                        if (regPgmBdlPkgCompIDs.Count() > 0)
                        {
                            regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                        }

                        if (regPBPCs.Count() > 0)
                        {
                            mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();
                        }

                        var details = proxy.GetCDPUDetails(new RegBREStatus() { RegId = regID.ToInt() });
                        if (!ReferenceEquals(details, null))
                        {
                            personalDetailsVM.CDPUStatus = details.TransactionStatus;
                            personalDetailsVM.CDPUDescription = details.TransactionReason;
                            personalDetailsVM.IsLockedBy = details.IsLockedBy;
                        }

                        #region "SimReplacement Waiver Off"
                        if (personalDetailsVM.AccountType == "POSTGSM")
                        {
                            List<DAL.Models.WaiverComponents> objWaiverComponents = proxy.GetWaiverComponentsbyRegID(regID.ToInt());
                            if (!ReferenceEquals(objWaiverComponents, null) && objWaiverComponents.Count > 0)
                            {
                                for (int i = 0; i < objWaiverComponents.Count; i++)
                                {
                                    if (objWaiverComponents[i].MismType.ToLower() == "sim")
                                    {
                                        orderSummary.WaiverComponent = new WaiverComponents();
                                        orderSummary.WaiverComponent.ComponentID = objWaiverComponents[i].ComponentID;
                                        orderSummary.WaiverComponent.ComponentName = objWaiverComponents[i].ComponentName;
                                        orderSummary.WaiverComponent.ID = objWaiverComponents[i].ID;
                                        orderSummary.WaiverComponent.IsWaived = objWaiverComponents[i].IsWaived;
                                        personalDetailsVM.isWaiverOff = objWaiverComponents[i].IsWaived;
                                        orderSummary.WaiverComponent.MismType = objWaiverComponents[i].MismType;
                                        orderSummary.WaiverComponent.price = objWaiverComponents[i].price;
                                        orderSummary.WaiverComponent.RegID = objWaiverComponents[i].RegID;
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion

                        #region	moved to WebHelper.MapRegistrationDetailsToVM - Drop 5
                        /*
						var lnkregdetails = allRegDetails.lnkregdetails;
						if (!ReferenceEquals(lnkregdetails, null))
                        {
                            #region MOC_Status and Liberization status

                            personalDetailsVM.Liberlization_Status = lnkregdetails.Liberlization_Status;
                            personalDetailsVM.MOCStatus = lnkregdetails.MOC_Status;

                            #endregion

                            Session["SimReplacementType"] = lnkregdetails.SimReplacementType.ToInt();
                            personalDetailsVM.Justification = lnkregdetails.Justification;
                            Session["AccountType"] = lnkregdetails.AccountType.ToString2();
                            personalDetailsVM.CapturedTUTSerial = lnkregdetails.TUTSerial.ToString2();
                            // Added by Himansu for PDPA implementation
                            personalDetailsVM.PDPAVersionStatus = lnkregdetails.PdpaVersion;
                            if (!ReferenceEquals(lnkregdetails.PdpaVersion, null))
                            {
                                using (var registrationProxy = new RegistrationServiceProxy())
                                {
                                    var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", lnkregdetails.PdpaVersion);
                                    personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                                }
                            }
						}
						*/
                        #endregion

                        if (personalDetailsVM.SimReplacementType == Convert.ToInt32(SimReplacementType.N2M))
                        {
                            regSec = proxy.RegistrationSecGet(regID.Value);
                            if (regSec != null)
                            {
                                personalDetailsVM.RegIDSeco = regSec.ID;
                                personalDetailsVM.MSISDNSeco = regSec.MSISDN1;
                                personalDetailsVM.PlanAdvanceSeco = regSec.PlanAdvance;
                                personalDetailsVM.PlanDepositSeco = regSec.PlanDeposit;
                                personalDetailsVM.DeviceAdvanceSeco = regSec.DeviceAdvance;
                                personalDetailsVM.DeviceDepositSeco = regSec.DeviceDeposit;
                                personalDetailsVM.RegTypeIDSeco = regSec.RegTypeID;
                                personalDetailsVM.RegSIMSerialSeco = regSec.SIMSerial;
                                personalDetailsVM.MISMSIMModel = regSec.SimModelId.ToString2() == string.Empty ? "0" : regSec.SimModelId.ToString();
                                // MobileNo
                                var SecmobileNos = new List<string>();
                                SecmobileNos.Add(regSec.MSISDN1);
                                SecmobileNos.Add(regSec.MSISDN2);
                                Session[SessionKey.RegMobileReg_VirtualMobileNo_Sec.ToString()] = string.Empty;

                                // RegMdlGrpModels for Secondary
                                var regMdlGrpModelIDs = proxy.RegMdlGrpModelSecFind(new RegMdlGrpModelSecFind()
                                {
                                    RegMdlGrpModelSec = new RegMdlGrpModelSec()
                                    {
                                        SecRegID = regSec.ID
                                    },
                                    Active = true
                                }).ToList();

                                // RegPgmBdlPkgComponent for Secondary
                                var regPgmBdlPkgCompIDsSec = proxy.RegPgmBdlPkgCompSecFind(new RegPgmBdlPkgCompSecFind()
                                {
                                    RegPgmBdlPkgCompSec = new RegPgmBdlPkgCompSec()
                                    {
                                        RegID = regSec.ID
                                    },
                                    Active = true
                                }).ToList();

                                // Secondary Line PgmBdlPkgComponent
                                var regPBPCsSec = new List<RegPgmBdlPkgCompSec>();
                                if (regPgmBdlPkgCompIDsSec.Count() > 0)
                                {
                                    regPBPCsSec = proxy.RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec).ToList();
                                }

                                if (regPBPCsSec.Count() > 0)
                                {
                                    mainLinePBPCIDsSec = regPBPCsSec.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();
                                }
                            }
                        }

                        //<Start> - 20150914 - Samuel - display sales code in summary page
                        //personalDetailsVM.Registration = proxy.RegistrationGet(personalDetailsVM.RegID);
                        personalDetailsVM.RegAttributes = proxy.RegAttributesGetByRegID(personalDetailsVM.RegID);
                        /*using (var proxyOrg = new OrganizationServiceProxy())
                        {
                            if (!ReferenceEquals(personalDetailsVM.Registration, null) && !ReferenceEquals(personalDetailsVM.Registration.CenterOrgID, null) && personalDetailsVM.Registration.CenterOrgID != 0)
                            {
                                personalDetailsVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { personalDetailsVM.Registration.CenterOrgID }).FirstOrDefault();
                            }
                        }*/
                        var salesPerson = allRegDetails.Regs.FirstOrDefault().SalesPerson;
                        var orgID = allRegDetails.Regs.FirstOrDefault().CenterOrgID;
                        var outletCode = string.Empty;
                        var salesCode = string.Empty;

                        WebHelper.Instance.generateOutletAndSalesCode(orgID, salesPerson, personalDetailsVM.RegAttributes, out outletCode, out salesCode);
                        personalDetailsVM.OutletCode = outletCode;
                        personalDetailsVM.SalesCode = salesCode;
                        //<End> - 20150914 - Samuel - display sales code in summary page
                    }

                    if (personalDetailsVM.RegAttributes != null && 
                        personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ORDER).Any() && 
                        personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ID).Any())
                    {
                        personalDetailsVM.ThirdPartySIM = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ORDER).FirstOrDefault().ATT_Value.ToBool();
                        personalDetailsVM.ThirdParty_ApplicantName = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_NAME).FirstOrDefault().ATT_Value;
                        personalDetailsVM.ThirdParty_Id = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ID).FirstOrDefault().ATT_Value;
                        personalDetailsVM.ThirdParty_Contact = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_CONTACT).FirstOrDefault().ATT_Value;
                        personalDetailsVM.ThirdParty_TypeID = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_TYPE_ID).FirstOrDefault().ATT_Value;
                        personalDetailsVM.ThirdParty_BiometricVerify = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_BIOMETRIC_VERIFY).FirstOrDefault().ATT_Value;

                        if (!string.IsNullOrEmpty(personalDetailsVM.ThirdParty_TypeID))
                        {
                            personalDetailsVM.ThirdParty_TypeID = Util.GetNameByID(RefType.IDCardType, personalDetailsVM.ThirdParty_TypeID.ToInt());
                        }
                    }
                    

                    using (var proxy = new CatalogServiceProxy())
                    {
                        string userICNO = string.Empty;
                        try
                        {
                            userICNO = Util.SessionAccess.User.IDCardNo;
                        }
                        catch
                        {
                        }

                        if (userICNO != null && userICNO.Length > 0)
                        {
                            objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                        }
                        if (objWhiteListDetailsResp != null)
                        {
                            WhiteListDetails objWhiteListDetails = new WhiteListDetails();
                            objWhiteListDetails.Description = objWhiteListDetailsResp.Description;
                            if (objWhiteListDetails.Description != null && objWhiteListDetails.Description.Length > 0)
                            {
                                personalDetailsVM._WhiteListDetails = objWhiteListDetails;
                            }
                        }

                        var mainLinePBPCs = new List<PgmBdlPckComponent>();
                        if (mainLinePBPCIDs.Count() > 0)
                        {
                            mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();
                        }

                        var bpCode = Properties.Settings.Default.Bundle_Package;
                        var pcCode = Properties.Settings.Default.Package_Component;
                        var DPCode = Properties.Settings.Default.RegType_DevicePlan;
                        var MAcode = Properties.Settings.Default.Master_Component;
                        var MPcode = Properties.Settings.Default.Main_package;
                        var CPCode = Properties.Settings.Default.ComplimentaryPlan;
                        var DCCode = Properties.Settings.Default.CompType_DataCon;
                        var ECCode = Properties.Settings.Default.Extra_Component;
                        var spCode = Properties.Settings.Default.SupplimentaryPlan;
                        var OCCode = Properties.Settings.Default.Other_Component;
                        var RCCode = Properties.Settings.Default.Pramotional_Component;
                        if (mainLinePBPCs.Count() > 0)
                        {
                            Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID.ToString()] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode || a.PlanType == spCode)).FirstOrDefault().ID;

                            var vasIDs = string.Empty;
                            //Filters OC AND RC Added by ravi on 20 may 2014 for TFS Bug Id:2501
                            var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode || a.LinkType == OCCode || a.LinkType == RCCode).Select(a => a.ID).ToList();
                            foreach (var id in pkgCompIDs)
                            {
                                vasIDs = vasIDs + "," + id;
                            }
                            Session[SessionKey.RegMobileReg_VasIDs.ToString()] = vasIDs;
                        }

                        var mainLinePBPCsSec = new List<PgmBdlPckComponent>();
                        if (mainLinePBPCIDsSec.Count() > 0)
                        {
                            mainLinePBPCsSec = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDsSec).ToList();
                        }

                        if (mainLinePBPCsSec.Count() > 0)
                        {
                            var SPCode = Properties.Settings.Default.SecondaryPlan;
                            var componentList = mainLinePBPCsSec.Where(a => (a.LinkType == bpCode || a.LinkType == SPCode) && (a.PlanType == CPCode || a.PlanType == SPCode)).ToList();
                            if (componentList != null && componentList.Count > 0)
                                Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = mainLinePBPCsSec.Where(a => (a.LinkType == bpCode || a.LinkType == SPCode) && (a.PlanType == CPCode || a.PlanType == SPCode)).FirstOrDefault().ID;
                            else
                                Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] = mainLinePBPCsSec.Where(a => a.LinkType == SPCode && (a.PlanType == MPcode)).FirstOrDefault().ID;

                            var vasIDs = string.Empty;
                            //Filters OC AND RC Added by ravi on 20 may 2014 for TFS Bug Id:2501
                            var pkgCompIDs = mainLinePBPCsSec.Where(a => a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode || a.LinkType == OCCode || a.LinkType == RCCode).Select(a => a.ID).ToList();

                            foreach (var id in pkgCompIDs)
                            {
                                vasIDs = vasIDs + "," + id;
                            }
                            Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = vasIDs;
                        }

                    }
                    #endregion
                }

                else
                {
                    #region regID = 0
                    DropFourObj DropForObj = new DropFourObj();
                    DropForObj.getExistingLineCount();
                    personalDetailsVM = Util.FormPersonalDetailsandCustomer(personalDetailsVM);
                    personalDetailsVM.RegTypeID = MobileRegType.SimReplacement.ToInt();
                    string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session[SessionKey.ExternalID.ToString()].ToString2()) ? string.Empty : Session[SessionKey.ExternalID.ToString()].ToString2(), regID.ToInt());
                    personalDetailsVM.Liberlization_Status = Result[0];
                    personalDetailsVM.MOCStatus = Result[1];
                    //28052015 - Anthony - to retrieve the liberalization based on the customer level - Start
                    var allCustomerDetails = SessionManager.Get("AllServiceDetails") as retrieveAcctListByICResponse;
                    if (!ReferenceEquals(allCustomerDetails, null))
                    {
                        personalDetailsVM.Liberlization_Status = Util.getMarketCodeLiberalisationMOC(allCustomerDetails, "LIBERALISATION");
                    }
                    //28052015 - Anthony - to retrieve the liberalization based on the customer level - End

                    /*Added by Himansu For PDPA implementation*/
                    using (var registrationProxy = new RegistrationServiceProxy())
                    {
                        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                        personalDetailsVM.PDPDVersion = resultdata.Version;
                    }
                    CustomizedCustomer CustomerPersonalInfo = (CustomizedCustomer)Session["CustPersonalInfo"];
                    List<SelectListItem> lstNationality = Util.GetList(RefType.Nationality, defaultValue: string.Empty);

                    if (!ReferenceEquals(CustomerPersonalInfo, null))
                    {
                        if (!string.IsNullOrEmpty(CustomerPersonalInfo.PassportNo))
                        {
                            personalDetailsVM.Customer.NationalityID = lstNationality.Where(n => n.Text.ToUpper() == NATIONALITY.NonMalaysian.ToString().ToUpper()).SingleOrDefault().Value.ToInt();
                        }
                    }
                    #endregion
                }

                Session[SessionKey.SimType.ToString()] = Util.SIMReplacementSIMType();
                personalDetailsVM.SimType = Session[SessionKey.SimType.ToString()].ToString2();
                Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;
                var orderSummaryVM = ConstructOrderSummary(regID.ToInt(), personalDetailsVM, (Session["SimReplacementType"].ToInt() == Convert.ToInt32(SimReplacementType.N2M) ? true : false));
                #region "assigning waiver details to order summary"
                if (orderSummary.WaiverComponent != null)
                    orderSummaryVM.WaiverComponent = orderSummary.WaiverComponent;
                #endregion

                if (reg.KenanAccountNo != null)
                { Session["KenanAccountNo"] = reg.KenanAccountNo; }
                else
                { Session["KenanAccountNo"] = "0"; }
                //displaying the kenan account before submiting For SA
                if (regID.ToInt() == 0)
                {
                    if (Session[SessionKey.KenanACNumber.ToString()] != null)
                    {
                        Session["KenanAccountNo"] = Session[SessionKey.KenanACNumber.ToString()];
                    }
                }
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = orderSummaryVM;

                #region Added by VLT on 21 June 2013 for getting Sim Type

                #endregion
                if (regID.ToInt() == 0)
                {
                    //Drop 5 B18 - Ricky - 2015.05.12 - Populate the default value for Outlet/Sales Code
                    WebHelper.Instance.PopulateDealerInfo(personalDetailsVM);
                }

                if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault() != null)
                {
                    personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault().ATT_Value;
                }
                else
                {
                    personalDetailsVM.BillingCycle = "N/A";
                }

            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;

            }
            return View(personalDetailsVM);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CustomerSummary(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            var cmssCaseId = string.Empty;
            var cmssCaseStatus = string.Empty;
            var resp = new RegistrationCreateResp();
            var reasonIdStr = collection["SimReplacementReasons_DISPLAY_VALUE"].ToString2();
            SimReplacementReasons simReplacementReasons = new SimReplacementReasons();
            SimReplacementReasonResp simReplacementReasonResp = new SimReplacementReasonResp();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            try
            {
                switch (collection["submit1"].ToString2())
                {

                    case "SubmitReg":
                        #region "Registration Submit"
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            bool isValid = false;
                            string ErrorMessage = string.Empty;

                            //int simModelId = string.IsNullOrEmpty(collection["hdnSimModel"]) ? 0 : int.Parse(collection["hdnSimModel"].Split(',')[0]);
                            //int articleId = string.IsNullOrEmpty(collection["hdnSimModel"]) ? 0 : int.Parse(collection["hdnSimModel"].Split(',')[1]);

                            int simModelId = 0;
                            int articleId = 0;

                            //if (!Session["IsDealer"].ToBool())
                            //{
								simModelId = string.IsNullOrEmpty(collection["hdnSimModel"]) ? 0 : int.Parse(collection["hdnSimModel"].Split(',')[0]);
								articleId = string.IsNullOrEmpty(collection["hdnSimModel"]) ? 0 : int.Parse(collection["hdnSimModel"].Split(',')[1]);

                                var accountTypeStr = Session["AccountType"] != null ? (string)Session["AccountType"] : "";
                                //var reasonIdStr = collection["SimReplacementReasons_DISPLAY_VALUE"].ToString2();//16032015 - Anthony - Move upside, because this value should be available for MC & Dealer
                                if (!string.IsNullOrEmpty(accountTypeStr) && accountTypeStr.ToLower() == "pregsm")
                                {
                                    // gry UOM code problem for replacement reason for prepaid.
                                    if (!string.IsNullOrEmpty(reasonIdStr) && (reasonIdStr == "25" || reasonIdStr == "35"))
                                    {
                                        var tempModel = CheckSimModelName(articleId.ToString(), "EA");
                                        if (!string.IsNullOrEmpty(tempModel))
                                        {
                                            var splitModel = tempModel.Split('ä');
                                            if (splitModel != null && splitModel.Length > 1)
                                            {
                                                var modelnsim = splitModel[1].Split(',');
                                                if (modelnsim != null && modelnsim.Length > 0)
                                                {
                                                    simModelId = modelnsim[0].ToInt();
                                                }
                                            }
                                        }
                                    }
                                }

                            //}

                            //16032015 - Anthony - to avoid getting null value from the Session["RegMobileReg_SimReplacementReasons"] - Start
                            using (var catalogService = new CatalogServiceProxy())
                            {
                                simReplacementReasonResp = catalogService.GetSimReplacementReasonDetails(reasonIdStr.ToInt());

                                if (!ReferenceEquals(simReplacementReasonResp, null))
                                {
                                    simReplacementReasons = simReplacementReasonResp.ReplacementReasonsDetails;
                                }
                            }
                            //16032015 - Anthony - to avoid getting null value from the Session["RegMobileReg_SimReplacementReasons"] - End

                            decimal waiverAmt = string.IsNullOrEmpty(collection["hdnwaiverAmount"]) ? 0 : Convert.ToDecimal(collection["hdnwaiverAmount"].ToString2());
                            bool iswaiverOpted = string.IsNullOrEmpty(collection["hdniswaiver"].ToString2()) ? false : Convert.ToBoolean(collection["hdniswaiver"].ToString2());
                            int simRplcType = Session["SimReplacementType"].ToInt();
                            List<MsimDetails> lstMISMSupplies = new List<MsimDetails>();
                            if (Session["RegMobileReg_PersonalDetailsVM"] != null)
                            {
                                personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
                            }
                            #region "Forming Sim Replacement Reason Details"
                            SimReplacementReasonDetails objSimReplacementReasonDet = new SimReplacementReasonDetails();
                            objSimReplacementReasonDet.CmssId = "0";
                            objSimReplacementReasonDet.ActiveDt = string.IsNullOrEmpty(collection["hdnActiveDt"]) ? string.Empty : collection["hdnActiveDt"];
                            objSimReplacementReasonDet.SimModelId = simModelId;
                            objSimReplacementReasonDet.SimArticalId = articleId;
                            objSimReplacementReasonDet.SimType = string.IsNullOrEmpty(collection["hdnSelectedSimType"]) ? string.Empty : collection["hdnSelectedSimType"];
                            if (collection["hdnisPartialwaiver"] == "No")
                            {
                                if (!string.IsNullOrEmpty(collection["hdnInitialPenaltyCharges"]))
                                {

                                    objSimReplacementReasonDet.WaiverAmount = waiverAmt;
                                    objSimReplacementReasonDet.PenaltyAmount = Decimal.Parse(collection["hdnInitialPenaltyCharges"]);
                                }
                                else
                                {
                                    objSimReplacementReasonDet.PenaltyAmount = 0;
                                }
                                objSimReplacementReasonDet.IsWaiver = false;

                            }
                            else
                            {
                                objSimReplacementReasonDet.IsWaiver = true;
                                objSimReplacementReasonDet.PenaltyAmount = 0;
                            }

                            Session["SimReplacement_WaiverDetails"] = objSimReplacementReasonDet;
                            Session["price"] = (objSimReplacementReasonDet.PenaltyAmount.ToString("0.00"));
                            #endregion

                            //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - Start
                            //Retrieve from collection due to personalDetailsVM object was replaced from Session by unknown person
                            personalDetailsVM.OutletCode = collection["OutletCode"].ToString2();
                            personalDetailsVM.OutletChannelId = collection["OutletChannelId"].ToString2();
                            personalDetailsVM.SalesCode = collection["SalesCode"].ToString2();
                            personalDetailsVM.SalesChannelId = collection["SalesChannelId"].ToString2();
                            personalDetailsVM.SalesStaffCode = collection["SalesStaffCode"].ToString2();
                            personalDetailsVM.SalesStaffChannelId = collection["SalesStaffChannelId"].ToString2();

                            WebHelper.Instance.ValidateDealerInfo(personalDetailsVM, out isValid, out ErrorMessage);
                            if (!isValid)
                            {
                                ModelState.AddModelError(string.Empty, ErrorMessage);
                                return View(personalDetailsVM);
                            }
                            //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - End

                            //Constrct waiver components by ravi on mar 04 2014 by ravi
                            List<DAL.Models.WaiverComponents> objWaiverComp = new List<DAL.Models.WaiverComponents>();
                            DAL.Models.RegistrationSec objRegSec = null;
                            List<RegPgmBdlPkgCompSec> objRegSecComps = null;
                            List<RegPgmBdlPkgComp> objRegComps = null;
                            if (Session["SimReplacementType"].ToInt() != Convert.ToInt32(SimReplacementType.PREPAID))
                            {
                                if (Session["SimReplacementType"].ToInt() == Convert.ToInt32(SimReplacementType.N2M))
                                {
                                    objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                                    objRegSec = ConstructRegistrationForSec(collection["QueueNo"].ToString2(), collection["Remarks"].ToString2(), personalDetailsVM.Customer.NationalityID, string.IsNullOrEmpty(collection["hdnSimModelSec"]) ? 0 : int.Parse(collection["hdnSimModelSec"].Split(',')[0]), string.IsNullOrEmpty(collection["hdnSimModelSec"]) ? string.Empty : Convert.ToString(collection["hdnSimModelSec"].Split(',')[1]), Session["IsDealer"].ToBool() ? collection["DealerSecondarySimSerial"].ToString2() : collection["simSerialSec"].ToString2());
                                    objRegSecComps = ConstructRegPgmBdlPkgSecComponent();

                                    if (Session["Comp2GBExists"].ToInt() == 1 && Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt() > 0)
                                    {
                                        objRegComps = ConstructRegPgmBdlPkgComponent();
                                    }

                                }
                                //Constrct waiver components by ravi on mar 04 2014 by ravi ends here

                                objWaiverComp.AddRange(ConstructWaiverDetails(waiverAmt, iswaiverOpted));
                            }

                            #region
                            if (simRplcType == (int)SimReplacementType.M2N)
                            {
                                var AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.SR_PPID.ToString()];
                                foreach (var item in AcctListByICResponse.itemList.Where(v => v.ExternalId == Session["ExternalID"].ToString2() && v.IsMISM == true && (v.ServiceInfoResponse.prinSuppInd == "P" || v.ServiceInfoResponse.prinSuppInd == string.Empty)))
                                {
                                    if (item.SecondarySimList != null && item.SecondarySimList.Count > 0)
                                    {
                                        lstMISMSupplies.AddRange(item.SecondarySimList);
                                        break;
                                    }
                                }
                            }
                            #endregion

                            #region Update BRE Fail Section
                            string JustificationMessage = string.Empty;
                            string HardStopMessage = string.Empty;
                            string ApprovalMessage = string.Empty;
                            bool isBREFail = false;
                            if (Session["SimReplacementType"].ToInt() != Convert.ToInt32(SimReplacementType.PREPAID))
                            {
                                WebHelper.Instance.BRECheckMessage(out HardStopMessage, out JustificationMessage, out ApprovalMessage, out isBREFail);
                            }
                            #endregion

                            resp = proxy.RegistrationCreateWithSec(ConstructRegistration(personalDetailsVM,collection["QueueNo"].ToString2(), collection["Remarks"].ToString2(),
                                                                                         simModelId, articleId.ToString(),
                                                                                         objSimReplacementReasonDet.PenaltyAmount,
                                                                                         collection["simSerial"].ToString2(),
                                                                                         collection["SignatureSVG"].ToString2(),
                                                                                         string.Empty,
                                                                                         string.Empty,
                                                                                         string.Empty),
                                                                   WebHelper.Instance.ConstructCustomer(personalDetailsVM),
                                                                   null, WebHelper.Instance.ConstructRegAddress(personalDetailsVM),
                                                                   objRegComps,
                                                                   WebHelper.Instance.ConstructRegStatus_SIMReplacement(),
                                                                   null, null, objRegSec, objRegSecComps, null, null,
                                                                   objWaiverComp, isBREFail);

                            if (resp.ID > 0)
                            {
								//personalDetailsVM.CustomerPhoto = collection["CustomerPhoto"].ToString2();
								//personalDetailsVM.AltCustomerPhoto = collection["AltCustomerPhoto"].ToString2();
								//personalDetailsVM.Photo = collection["Photo"].ToString2();
								personalDetailsVM.Photo = Session["otherImage"].ToString2();
								personalDetailsVM.CustomerPhoto = Session["frontImage"].ToString2();
								personalDetailsVM.AltCustomerPhoto = Session["backImage"].ToString2();
                                dropObj.personalInformation = personalDetailsVM;

                                using (var updateProxy = new UpdateServiceProxy())
                                {
                                    if (!string.IsNullOrEmpty(personalDetailsVM.Photo))
                                    {
                                        updateProxy.SavePhotoToDB(personalDetailsVM.Photo, "", "", resp.ID);
                                    }
                                    if (!string.IsNullOrEmpty(personalDetailsVM.CustomerPhoto))
                                    {
                                        updateProxy.SavePhotoToDB("", personalDetailsVM.CustomerPhoto, "", resp.ID);
                                    }
                                    if (!string.IsNullOrEmpty(personalDetailsVM.AltCustomerPhoto))
                                    {
                                        updateProxy.SavePhotoToDB("", "", personalDetailsVM.AltCustomerPhoto, resp.ID);
                                    }
                                }
                                #region 14052015 - Anthony - Save document to tblreguploaddoc for iContract
                                bool iContractActive = ConfigurationManager.AppSettings["iContractActive"].ToBool();
                                if (iContractActive)
                                {
                                    var firstBase64index = 0;
                                    var lastBase64index = 0;

                                    if (!string.IsNullOrEmpty(personalDetailsVM.CustomerPhoto))
                                    {
                                        firstBase64index = personalDetailsVM.CustomerPhoto.IndexOf("data:");
                                        lastBase64index = personalDetailsVM.CustomerPhoto.IndexOf(',');
                                        dropObj.personalInformation.idDocumentFront = personalDetailsVM.CustomerPhoto.Remove(firstBase64index, lastBase64index + 1);
                                    }

                                    if (!string.IsNullOrEmpty(personalDetailsVM.AltCustomerPhoto))
                                    {
                                        firstBase64index = personalDetailsVM.AltCustomerPhoto.IndexOf("data:");
                                        lastBase64index = personalDetailsVM.AltCustomerPhoto.IndexOf(',');
                                        dropObj.personalInformation.idDocumentBack = personalDetailsVM.AltCustomerPhoto.Remove(firstBase64index, lastBase64index + 1);
                                    }

                                    if (!string.IsNullOrEmpty(personalDetailsVM.Photo))
                                    {
                                        firstBase64index = personalDetailsVM.Photo.IndexOf("data:");
                                        lastBase64index = personalDetailsVM.Photo.IndexOf(',');
                                        dropObj.personalInformation.otherDocuments = personalDetailsVM.Photo.Remove(firstBase64index, lastBase64index + 1);
                                    }
                                    WebHelper.Instance.SaveDocumentToTable(dropObj, resp.ID);
                                }
                                #endregion
                                
                                SimReplacementVm objSimReplacementVm = new SimReplacementVm();
                                //objSimReplacementVm.SimReplacementReasons = (SimReplacementReasons)Session["RegMobileReg_SimReplacementReasons"];//16032015 - Anthony - to avoid getting null value from the Session["RegMobileReg_SimReplacementReasons"]
                                if (!ReferenceEquals(simReplacementReasons, null))
                                {
                                    if (Session["price"] != null)
                                    {
                                        simReplacementReasons.Price = Session["price"].ToDecimal();
                                    }
                                    objSimReplacementVm.SimReplacementReasons = simReplacementReasons;
                                }
                                else
                                {
                                    objSimReplacementVm.SimReplacementReasons = (SimReplacementReasons)Session["RegMobileReg_SimReplacementReasons"];
                                }
                                objSimReplacementVm.simDetlsList = (SimDetlsList)Session["Old_SimDetails"];
                                objSimReplacementVm.SimReplacementReasonDetail = objSimReplacementReasonDet;

                                #region Insert into lnkRegDetails
                                LnkRegDetailsReq objRegDetailsReqobj = new LnkRegDetailsReq();
                                Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                                objRegDetails.CreatedDate = DateTime.Now;
                                objRegDetails.IsSuppNewAc = true;
                                objRegDetails.UserName = Util.SessionAccess.UserName;
                                objRegDetails.RegId = resp.ID;
                                objRegDetails.CmssId = objSimReplacementReasonDet.CmssId;
                                objRegDetails.ActiveDt = objSimReplacementReasonDet.ActiveDt;
                                objRegDetails.SimModelId = objSimReplacementReasonDet.SimModelId;
                                objRegDetails.SimArticalId = objSimReplacementReasonDet.SimArticalId;
                                objRegDetails.SimType = objSimReplacementReasonDet.SimType;
                                objRegDetails.OldSimSerial = objSimReplacementVm.simDetlsList.ExteranalId;
                                objRegDetails.NewSimSerial = collection["simSerial"].ToString2();
                                objRegDetails.SimReplReasonCode = objSimReplacementVm.SimReplacementReasons == null ? 0 : objSimReplacementVm.SimReplacementReasons.DISCONNECT_REASON;
                                objRegDetails.SimReplReason = ReferenceEquals(objSimReplacementVm.SimReplacementReasons, null) ? string.Empty : objSimReplacementVm.SimReplacementReasons.DISPLAY_VALUE;
                                objRegDetails.SIMModel = objSimReplacementVm.simDetlsList.ExteranalIdType;
                                objRegDetails.AccountType = Session["AccountType"].ToString2();
                                objRegDetails.NrcId = objSimReplacementVm.SimReplacementReasons == null ? string.Empty : objSimReplacementVm.SimReplacementReasons.NRC.ToString();
                                objRegDetails.TotalLineCheckCount = Session["existingTotalLine"].ToInt();
                                //Insert justification
                                if (collection["txtJustfication"].ToString2().Length > 0)
                                    objRegDetails.Justification = collection["txtJustfication"].ToString();
                                //Added by ravi for tut sim serial on Feb 18 2014
                                if (Session["AccountType"].ToString2().ToUpper() == "PREGSM" && collection["tutSimSerial"].ToString2().Length > 0)
                                {
                                    objRegDetails.TUTSerial = collection["tutSimSerial"].ToString().Trim();

                                    //TUT SIM Model Id saving by ravi on Mar 24 2014
                                    int tutSimModelId = 0;
                                    string strTUTArtucleID = ReferenceEquals(Session["TUTArticleID"], null) ? string.Empty : Session["TUTArticleID"].ToString();
                                    if (!string.IsNullOrEmpty(strTUTArtucleID))
                                    {
                                        using (var regProxy = new RegistrationServiceProxy())
                                        {
                                            tutSimModelId = regProxy.GetSIMModelIDByArticleID(strTUTArtucleID);
                                        }

                                    }
                                    objRegDetails.TUTSimModelId = tutSimModelId;
                                    //TUT SIM Model Id saving by ravi on Mar 24 2014
                                }


                                //Added by ravi on Mar 04 2014 for sim replacement type
                                objRegDetails.SimReplacementType = Session["SimReplacementType"].ToInt();
                                //Added by ravi on mar 05 2014 for moc and libersation status
                                objRegDetails.MOC_Status = personalDetailsVM.MOCStatus;
                                //Added By Himansu for PPDA 
                                objRegDetails.PdpaVersion = personalDetailsVM.PDPDVersion;
                                objRegDetails.Liberlization_Status = personalDetailsVM.Liberlization_Status;

                                bool IsWriteOff = false;
                                string acc = string.Empty;
                                WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
                                objRegDetails.WriteOffDetails = acc;

                                objRegDetailsReqobj.LnkDetails = objRegDetails;
                                // display the old sim serial number in success page print.
                                Session["OldSimSerial"] = objSimReplacementVm.simDetlsList.ExteranalId.ToString2();
                                using (var regProxy = new RegistrationServiceProxy())
                                {
                                    regProxy.SaveLnkRegistrationDetails(objRegDetailsReqobj);
                                    //#region save PosFile
                                    //try
                                    //{
                                    //    using (var kenanProxy = new KenanServiceProxy())
                                    //    {
                                    //        kenanProxy.CreateSIMReplacementPOSFile(resp.ID);
                                    //    }
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //    using (var proxytransaction = new UserServiceProxy())
                                    //    {
                                    //        proxytransaction.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SimReplacement, "Failed CreateSIMReplacementPOSFile RegId " + resp.ID, Session["KenanACNumber"].ToString(), "");
                                    //    }
                                    //    Logger.Error(ex);
                                    //}
                                    //#endregion
                                    if (simRplcType == (int)SimReplacementType.M2N && lstMISMSupplies.Count > 0)
                                    {
                                        List<lnkSecondaryAcctDetailsSimRplc> lstSecondarAccts = new List<lnkSecondaryAcctDetailsSimRplc>();
                                        foreach (var item in lstMISMSupplies)
                                        {
                                            lnkSecondaryAcctDetailsSimRplc item1 = new lnkSecondaryAcctDetailsSimRplc();
                                            item1.REGID = resp.ID;
                                            item1.CREATEDATE = DateTime.Now;
                                            item1.DESCRIPTION = Session["ExternalID"].ToString2() + "_" + item.Msisdn;
                                            item1.EXTERNALID = Session["ExternalID"].ToString2();
                                            item1.FxAcctNo = item.FxAcctNo;
                                            item1.FxSubscrNo = item.FxSubscrNo;
                                            item1.FxSubscrNoResets = item.FxSubscrNoResets;
                                            item1.PrimSecInd = item.PrimSecInd;
                                            item1.STATUS = true;
                                            item1.SUPP_Msisdn = item.Msisdn;
                                            lstSecondarAccts.Add(item1);
                                        }
                                        if (lstSecondarAccts.Count > 0)
                                            proxy.RegSecondaryAccountsForSimRplc(lstSecondarAccts);
                                    }
                                    #region created by Raj on 4/14/2014 for save data componets in lnkpackagecomponets for disconnect only for normal to mism flow

                                    if (simRplcType == (int)SimReplacementType.N2M)
                                    {
                                        List<PackageComponents> pckg = ConstructDisconnectComponents(Session[SessionKey.RegMobileReg_MobileNo.ToString()] == null ? null : ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2());
                                        proxy.SaveLnkPackageComponet(resp.ID, pckg, 13);
                                    }

                                    #endregion
                                }
                                #endregion

                                #region Insert into lnkRegAttributes
                                var regAttribList = new List<RegAttributes>();
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCODE, ATT_Value = personalDetailsVM.OutletCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCHANNELID, ATT_Value = personalDetailsVM.OutletChannelId });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCODE, ATT_Value = personalDetailsVM.SalesCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCHANNELID, ATT_Value = personalDetailsVM.SalesChannelId });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCODE, ATT_Value = personalDetailsVM.SalesStaffCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCHANNELID, ATT_Value = personalDetailsVM.SalesStaffChannelId });
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_PAYMENT_METHOD, ATT_Value = Util.getPaymentMethodName() });

                                proxy.SaveListRegAttributes(regAttribList);
                                #endregion

								#region Insert into trnRegJustification
								WebHelper.Instance.saveTrnRegJustification(resp.ID);
                                #endregion

                                #region Insert into trnRegBreFailTreatment
                                WebHelper.Instance.saveTrnRegBreFailTreatment(resp.ID);
                                #endregion

                                var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
                                if (biometricID != 0)
                                {
                                    var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
                                    biometric.RegID = resp.ID;
                                    proxy.BiometricUpdate(biometric);
                                }

                                if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
                                {
                                    Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
                                }

                                using (var proxytransaction = new UserServiceProxy())
                                {
                                    proxytransaction.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SimReplacement, "Registration Done with RegId " + resp.ID, Session["KenanACNumber"].ToString(), "");
                                }


                                #region Drop 5 - update Status to PendingCDPU and save to lnkRegBREStatus
                                // all BRE Failed from Dealer route to CDPU
                                if (isBREFail && Session["IsDealer"].ToBool())
                                {
                                    try
                                    {
                                        proxy.UpsertBRE(new RegBREStatus()
                                        {
                                            RegId = resp.ID,
                                            TransactionStatus = Constants.StatusCDPUPending
                                        });// have bre failed
                                        personalDetailsVM.CDPUStatus = Constants.StatusCDPUPending;
                                       
                                        proxy.RegistrationCancel(new RegStatus()
                                        {
                                            RegID = resp.ID,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            StartDate = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCDPUPendingApp)
                                        });
                                        return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });
                                        
                                    }
                                    catch
                                    {

                                    }
                                }
                                else
                                {
                                    WebHelper.Instance.WebPosAuto(resp.ID);
                                }
                                #endregion

                                //#region Saving POS FileName in DB for Dealers
                                //if ((Session["IsDealer"].ToBool() == true && collection["hdnTotalPayable"].ToString2() != string.Empty && collection["hdnTotalPayable"].ToDecimal() > 0))
                                //{
                                //    string posFileName = WebHelper.Instance.CreatePOSFileName(resp.ID);
                                //    using (var catLogProxy = new CatalogServiceProxy())
                                //    {
                                //        catLogProxy.UpdateIMPOSDetails(resp.ID, posFileName, 1);
                                //    }
                                //}
                                //#endregion


                                #region email functionality

                                if (Session["KenanCustomerInfo"] != null)
                                {
                                    string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString().Split('╚');
                                    KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
                                    kenanInfo.FullName = strArrKenanInfo[0];
                                    kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
                                    kenanInfo.IDCardNo = strArrKenanInfo[2];
                                    kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
                                    kenanInfo.Address1 = strArrKenanInfo[4];
                                    kenanInfo.Address2 = strArrKenanInfo[5];
                                    kenanInfo.Address3 = strArrKenanInfo[6];
                                    kenanInfo.Postcode = strArrKenanInfo[8];
                                    kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
                                    kenanInfo.RegID = resp.ID;
                                    var kenanResp = new KenanCustomerInfoCreateResp();
                                    kenanResp = proxy.KenanCustomerInfoCreate(kenanInfo);

                                }
                                Session["KenanCustomerInfo"] = null;
                                WebHelper.Instance.ClearRegistrationSession();
                                Session["Comp2GBExists"] = null;
                                #region "Generating Impos File and Moving the status to ready for Payment"
                                try
                                {
                                    //for genarate IMPOS file
                                    /*
                                    using (var kenanProxy = new KenanServiceProxy())
                                    {
                                        kenanProxy.CreateSIMReplacementPOSFile(resp.ID);
                                    }
                                     * */
                                    //Update the account status in lnkregstatus
                                    //proxy.RegStatusCreate(WebHelper.Instance.ConstructRegStatus(resp.ID, Properties.Settings.Default.Status_ReadyForPayment), null);

                                    #region Code for SRWWO by chetan
                                    if (Session["AllowSimReplacement"].ToString2() == "True")
                                    {
                                        string _selectedexternalID = !ReferenceEquals(Session[SessionKey.ExternalID.ToString()], null) ? Session[SessionKey.ExternalID.ToString()].ToString() : null;
                                        var custIdentity = string.Empty;
                                        if (Session[SessionKey.PPID.ToString()] != null)
                                        {
                                            var AcctListByICResponse = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                                            SubscriberICService.CustomizedCustomer customerInfo = new CustomizedCustomer();
                                            SubscriberICService.Items selectedAccount = new Items();
                                            selectedAccount = AcctListByICResponse.itemList.Where(v => v.ExternalId == _selectedexternalID).FirstOrDefault();

                                            if (selectedAccount.Account != null)
                                            {
                                                customerInfo = selectedAccount.Customer;
                                                if (customerInfo.NewIC != string.Empty)
                                                    custIdentity = customerInfo.NewIC;
                                                else if (customerInfo.PassportNo != string.Empty)
                                                    custIdentity = customerInfo.PassportNo;
                                                else if (customerInfo.OldIC != string.Empty)
                                                    custIdentity = customerInfo.OldIC;
                                                else if (customerInfo.OtherIC != string.Empty)
                                                    custIdentity = customerInfo.OtherIC;
                                                CreateInteractionCaseResponse Cresp = new CreateInteractionCaseResponse();
                                                try
                                                {
                                                    if (WebHelper.Instance.CompareCMSSDtForAcct(custIdentity) > 14.0)
                                                    {
                                                        Cresp = WebHelper.Instance.CreateCaseInCMSS(selectedAccount.AcctExtId.ToString2(),
                                                           Constants.CMSS_CASE_COMPLEXITY,
                                                           ConfigurationManager.AppSettings["CMSSQueue"].ToString2(),
                                                           "59",
                                                           ConfigurationManager.AppSettings["CMSSDirection"].ToString2(),
                                                           selectedAccount.Account != null ? selectedAccount.Account.AccountNumber.ToString2() : "",
                                                           selectedAccount.ExternalId.ToString2(),
                                                           (collection["NewCBR"].ToString().Length > 0 ? collection["NewCBR"].ToString2() : string.Empty),
                                                           ConfigurationManager.AppSettings["CMSSProduct"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSReason1"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSReason2"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSReason3"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSSysName"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSTitle"].ToString2(),
														   Constants.CMSS_TYPE, 
														   Constants.CMSS_CASE_TOPROCEEDWITH,
                                                            resp.ID.ToInt(),
                                                            custIdentity.ToString2());

                                                        ViewBag.AllowSubmit = "True";
                                                        if (Cresp != null && !string.IsNullOrEmpty(Cresp.Id))
                                                        {
                                                            cmssCaseStatus = "Success";
                                                            cmssCaseId = Cresp.Id;
                                                        }
                                                        else
                                                            cmssCaseStatus = "Fail";
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    cmssCaseStatus = "Fail";
                                                    Logger.Info("Exception while CMSS Case Creation : ExternalID -" + selectedAccount.AcctExtId.ToString2() + " : Message - " + ex.Message + ": InnerException - " + ex.InnerException);
                                                    string xmlReq = Util.ConvertObjectToXml(selectedAccount);
                                                    Logger.Info("Exception while CMSS Case Creation for Account Selected : XML -" + xmlReq.ToString2());
                                                }
                                            }

                                        }
                                    }
                                    #endregion upto here code for SRWWO
                                    TempData["CMSSCaseID"] = cmssCaseId;
                                    TempData["CMSSCaseStatus"] = cmssCaseStatus;

									decimal simReplacementTotalPayable = !string.IsNullOrWhiteSpace(Session["SIMReplacementTotalPayable"].ToString2()) ? Session["SIMReplacementTotalPayable"].ToDecimal() : 0;
									decimal hdnTotalPayable = !string.IsNullOrWhiteSpace(collection["hdnTotalPayable"].ToString2()) ? collection["hdnTotalPayable"].ToDecimal() : 0;

                                    #region 11052015 - Anthony - Generate RF for iContract
                                    try
                                    {
                                        using (var updateProxy = new UpdateServiceProxy())
                                        {
                                            if (!updateProxy.FindDocument(Constants.CONTRACT, resp.ID))
                                            {
                                                String contractHTMLSource = string.Empty;

                                                if (simRplcType == SimReplacementType.PREPAID.ToInt())
                                                {
                                                    contractHTMLSource = Util.RenderPartialViewToString(this, "SimReplacementPrint", WebHelper.Instance.GetContractDetailsViewModel_PrepaidSIMReplacement(resp.ID, false));
                                                }
                                                else
                                                {
                                                    contractHTMLSource = Util.RenderPartialViewToString(this, "CofServiceFormPrint", WebHelper.Instance.GetContractDetailsViewModel_SIMReplacement(resp.ID, false));
                                                }
                                                contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
                                                contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
                                                contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide"); 
                                                if (!string.IsNullOrEmpty(contractHTMLSource))
                                                {
                                                    Byte[] PDFBytes = Util.convertHTMLStringToPDFByteArray(contractHTMLSource, resp.ID);
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        //Logger.Error("ICONTRACT EXCEPTION = " + ex);
                                        WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                    }
                                    #endregion

									// Gerry - comment this because auto knock off not only for dealer
                                    //if (Session["ISDealer"].ToBool
                                    if (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove)
									{
                                        #region Auto activation/Ready for Payment redirection for Dealer

                                        if (simReplacementTotalPayable == 0 && hdnTotalPayable == 0)
                                        {
                                            if (resp.ID > 0)
                                            {
                                                personalDetailsVM.RegID = resp.ID;
                                            }

                                            #region "Cashier Payment Received Section for Dealer"

                                            int _simReplacementTypeAuto = 0;
                                            bool isPrimaryLineCompsAddedAuto = false;
                                            var requestAuto = new OrderFulfillRequest()
                                            {
                                                OrderID = personalDetailsVM.RegID.ToString(),
                                                UserSession = Util.SessionAccess.UserName,

                                            };
                                            UpdateRegStatus(new RegStatus()
                                            {
                                                RegID = personalDetailsVM.RegID,
                                                Active = true,
                                                CreateDT = DateTime.Now,
                                                StartDate = DateTime.Now,
                                                LastAccessID = Util.SessionAccess.UserName,
                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                                            });


                                            string usernameAuto = string.Empty;
                                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                                            {
                                                usernameAuto = Request.Cookies["CookieUser"].Value;
                                            }
                                            eaiResponseType objeaiResponseTypeAuto = new eaiResponseType();
                                            LnkRegDetailsReq objRegDetailsReqAuto = new LnkRegDetailsReq();
                                            if (personalDetailsVM.RegID > 0)
                                            {
                                                using (var regProxy = new RegistrationServiceProxy())
                                                {
                                                    objRegDetailsReqAuto = regProxy.GetLnkRegistrationDetails(personalDetailsVM.RegID);
                                                    _simReplacementTypeAuto = objRegDetailsReqAuto.LnkDetails.SimReplacementType.ToInt();
                                                }
                                            }

                                            #region "Normal to MISM Convertion Flow"
                                            if (_simReplacementTypeAuto > 0 && _simReplacementTypeAuto == (int)SimReplacementType.N2M)
                                            {
                                                #region "Primary Line Details"
                                                DAL.Models.Registration reg = null;
                                                bool isPrimaryCompAdded = false;
                                                using (var prxy = new RegistrationServiceProxy())
                                                {
                                                    reg = prxy.RegistrationGet(personalDetailsVM.RegID);

                                                    var regPgmBdlPkgCompIDs = prxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                                                    {
                                                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                                                        {
                                                            RegID = reg.ID,
                                                        }
                                                    }).ToList();

                                                    #region "Add Greater than 2GB Components functional logic"
                                                    if (regPgmBdlPkgCompIDs.Count() > 0)
                                                    {
                                                        using (var proxyKenan = new KenanServiceProxy())
                                                        {
                                                            if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                                                            {
                                                                List<int> Ids = new List<int>();
                                                                Ids.Add(reg.ID);
                                                                CenterOrderContractRequest Req = new CenterOrderContractRequest();
                                                                Req.orderId = personalDetailsVM.RegID.ToString();
                                                                if (Request.Cookies["CookieUser"] != null)
                                                                    Req.UserName = Request.Cookies["CookieUser"].Value;
                                                                isPrimaryCompAdded = proxyKenan.CenterOrderContractCreationPrimaryLine(Req);
                                                                isPrimaryLineCompsAddedAuto = true;
                                                                if (!isPrimaryCompAdded)
                                                                {
                                                                    Logger.InfoFormat("{0}:{1}# Primary Line Components registration Failed, response{2}", "QSimReplacement", "CustomerSummary", isPrimaryCompAdded);
                                                                    UpdateRegStatus(new RegStatus()
                                                                    {
                                                                        RegID = personalDetailsVM.RegID,
                                                                        Active = true,
                                                                        CreateDT = DateTime.Now,
                                                                        StartDate = DateTime.Now,
                                                                        LastAccessID = Util.SessionAccess.UserName,
                                                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_ADDCOMP_FAIL)
                                                                    });
                                                                    return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });

                                                                }

                                                                using (var regProxy = new RegistrationServiceProxy())
                                                                {
                                                                    regProxy.RegistrationCancel(new RegStatus()
                                                                    {
                                                                        RegID = personalDetailsVM.RegID,
                                                                        Active = true,
                                                                        CreateDT = DateTime.Now,
                                                                        StartDate = DateTime.Now,
                                                                        LastAccessID = Util.SessionAccess.UserName,
                                                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_SvcPendingActivate)
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                }
                                                #endregion

                                                #region "Secondary Line Details"
                                                if (!isPrimaryLineCompsAddedAuto) // in case of primary line components not exists or created and only secondary line should be created.
                                                {
                                                    using (var proxySec = new RegistrationServiceProxy())
                                                    {
                                                        var RegSec = proxySec.RegistrationSecGet(personalDetailsVM.RegID);
                                                        var pbpcIDsSec = new List<int>();
                                                        if (RegSec != null)
                                                        {
                                                            var regPgmBdlPkgCompIDsSec = proxySec.RegPgmBdlPkgCompSecFind(new RegPgmBdlPkgCompSecFind()
                                                            {
                                                                RegPgmBdlPkgCompSec = new RegPgmBdlPkgCompSec()
                                                                {
                                                                    RegID = RegSec.ID,
                                                                }
                                                            }).ToList();
                                                            var pbpcSec = new List<RegPgmBdlPkgCompSec>();
                                                            if (regPgmBdlPkgCompIDsSec.Count() > 0)
                                                                pbpcSec = proxySec.RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec).ToList();
                                                            //main line
                                                            if (pbpcSec.Count() > 0)
                                                                pbpcIDsSec = pbpcSec.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();
                                                            if (!ReferenceEquals(objRegDetailsReqAuto, null) && !ReferenceEquals(objRegDetailsReqAuto.LnkDetails.SimType, null) && objRegDetailsReqAuto.LnkDetails.SimType == "MISM")
                                                            {
                                                                // Added the functionality of registering the additional secondary line for MISM
                                                                var regSecondary = RegSec;
                                                                var isMISM = regSecondary == null ? false : true;
                                                                var extid = new List<KenanTypeExternalId>();
                                                                var extidnew = new List<KenanTypeExternalId>();
                                                                extidnew.Add(new KenanTypeExternalId() { ExternalId = string.Empty, ExternalIdType = string.Empty });
                                                                extid.Add(new KenanTypeExternalId() { ExternalId = reg.MSISDN1 == null ? "" : reg.MSISDN1, ExternalIdType = (reg.AccExternalID == null ? "" : reg.AccExternalID) });
                                                                if (isMISM == true && regSecondary.IsSecondaryRequestSent == false)
                                                                {
                                                                    var regAccount = new DAL.Models.RegAccount();
                                                                    regSecondary.IsSecondaryRequestSent = true;
                                                                    using (var reproxy = new RegistrationServiceProxy())
                                                                    {
                                                                        regSecondary.IsSecondaryRequestSent = true;
                                                                        int result = reproxy.UpdateSecondaryRequestSent(regSecondary.ID);
                                                                    }
                                                                    using (var kProxy = new KenanServiceProxy())
                                                                    {
                                                                        var request2 = new KenanTypeAdditionLineRegistration()
                                                                        {
                                                                            OrderId = "ISELLMISM" + reg.ID.ToString() + "SIM",
                                                                            FxAcctNo = (reg.fxAcctNo == null) ? "" : reg.fxAcctNo,
                                                                            ExternalId = (extid.ToList().ToArray() == null) ? extidnew.ToList().ToArray() : extid.ToList().ToArray(),
                                                                        };
                                                                        var response = kProxy.KenanAdditionLineRegistrationMISMSecondary(request2);
                                                                        if (response.Code != "0")
                                                                        {
                                                                            Logger.InfoFormat("{0}:{1} Secondary Components Registration Failed at Kenan , Response{2}:{3}", "QSimReplacement", "CustomerSummary", response.Code.ToString2(), response.Message.ToString2());
                                                                            UpdateRegStatus(new RegStatus()
                                                                            {
                                                                                RegID = personalDetailsVM.RegID,
                                                                                Active = true,
                                                                                CreateDT = DateTime.Now,
                                                                                StartDate = DateTime.Now,
                                                                                LastAccessID = Util.SessionAccess.UserName,
                                                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SECCT_FAIL)
                                                                            });
                                                                            return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                                                        }
                                                                        else
                                                                        {
                                                                            UpdateRegStatus(new RegStatus()
                                                                            {
                                                                                RegID = personalDetailsVM.RegID,
                                                                                Active = true,
                                                                                CreateDT = DateTime.Now,
                                                                                StartDate = DateTime.Now,
                                                                                LastAccessID = Util.SessionAccess.UserName,
                                                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SECCT)
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion

                                            }
                                            #endregion
                                            // for normal to MISM flow, we should call the SIMSWAP from FullFillORder Success for SecondaryLine Creation.
                                            if (_simReplacementTypeAuto > 0 && _simReplacementTypeAuto != (int)SimReplacementType.N2M)
                                            {
                                                #region "Incase of Mism(primary) to Normal flow , need to disconnect MISM secondary Lines"
                                                if (_simReplacementTypeAuto == (int)SimReplacementType.M2N)
                                                {
                                                    List<lnkSecondaryAcctDetailsSimRplc> SecAccountLines = new List<lnkSecondaryAcctDetailsSimRplc>();
                                                    using (var regproxy = new RegistrationServiceProxy())
                                                    {
                                                        SecAccountLines = regproxy.GetSecondaryAccountLinesForSimRplc(personalDetailsVM.RegID);
                                                    }
                                                    //TODO: 1. NEED TO CHECK WHETHER WE ARE PROCEEDING WITH MISM-P OR MISM-S
                                                    //TODO: 2. NEED TO CONSTRUCT THE SECONDALY LINES FOR MISM-P
                                                    if (SecAccountLines.Count > 0)
                                                    {
                                                        using (var kenanProxy = new KenanServiceProxy())
                                                        {
                                                            int indexval = 0;
                                                            foreach (var itemval in SecAccountLines)
                                                            {
                                                                indexval++;
                                                                kenanProxy.DisconnectComponents(new DisconnectCenterServiceRequest()
                                                                {
                                                                    orderId = "ISELL" + personalDetailsVM.RegID + indexval + "D",
                                                                    newOrderInd = "Y",
                                                                    fxAcctNo = itemval.FxAcctNo,
                                                                    fxSubscrNo = itemval.FxSubscrNo,
                                                                    fxSubscrNoResets = itemval.FxSubscrNoResets,
                                                                    disconnectReason = "1",
                                                                    inpF01 = Util.SessionAccess.UserName,
                                                                    inpF02 = personalDetailsVM.OrganisationId
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion

                                                using (var proxyKenan = new KenanServiceProxy())
                                                {
                                                    objeaiResponseTypeAuto = proxyKenan.SwapCenterInventory(personalDetailsVM.RegID);
                                                }
                                                if (!string.IsNullOrEmpty(objeaiResponseTypeAuto.msgCodeField))
                                                {
                                                    if (int.Parse(objeaiResponseTypeAuto.msgCodeField) > 0)
                                                    {
                                                        UpdateRegStatus(new RegStatus()
                                                        {
                                                            RegID = personalDetailsVM.RegID,
                                                            Active = true,
                                                            CreateDT = DateTime.Now,
                                                            StartDate = DateTime.Now,
                                                            LastAccessID = Util.SessionAccess.UserName,
                                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SWAP_FAIL)
                                                        });
                                                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                                    }
                                                }
                                                else
                                                {
                                                    UpdateRegStatus(new RegStatus()
                                                    {
                                                        RegID = personalDetailsVM.RegID,
                                                        Active = true,
                                                        CreateDT = DateTime.Now,
                                                        StartDate = DateTime.Now,
                                                        LastAccessID = Util.SessionAccess.UserName,
                                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SWAP_FAIL)
                                                    });
                                                    return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                                }
                                            }
                                            #endregion

                                            Session["SIMReplacementTotalPayable"] = null;

                                            return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });

                                        }
                                        else
                                        {
                                            Session["SIMReplacementTotalPayable"] = null;
                                            if (Roles.IsUserInRole("MREG_DSV") && false)
                                                return RedirectToAction("MobileRegAccCreated", new { regID = resp.ID });
                                            else
                                                return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                        return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                                    }


                                }
                                catch (Exception ex)
                                {
                                    WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                    using (var regProxy = new RegistrationServiceProxy())
                                    {
                                        regProxy.RegistrationCancel(new RegStatus()
                                        {
                                            RegID = resp.ID,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            StartDate = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                                        });
                                    }
                                    return RedirectToAction("MobileRegFail", "QSimReplacement", new { regID = resp.ID });
                                }

                                #endregion

                            }

                                #endregion


                        }
                        #endregion
                        break;

                    case "cancel":
                        #region "Cancel Registration"
                        string reason = string.Empty;
                        int prevStatus = WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID);
                        reason = ReferenceEquals(reason, null) ? "No reason provided." : collection["hdnReasonCancel"].ToString();
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationCancel(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)
                            });

                            proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                            {
                                RegID = personalDetailsVM.RegID,
                                CancelReason = reason,
                                CreateDT = DateTime.Now,
                                LastUpdateDT = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName
                            });

                            var totalPrice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID, IMEInumberStored:false);
                            if (prevStatus == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment) && WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID) == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan) && totalPrice > 0)
                            {
                                WebHelper.Instance.SendDetailsToWebPos(personalDetailsVM.RegID, Constants.StatusWebPosToCancel);
                            }
                        }
                        return RedirectToAction("MobileRegCanceled", new { regID = personalDetailsVM.RegID });
                        #endregion
                        break;
                    case "activateSvc":
                        #region "Activate SVC"
                        try
                        {
                            #region CDPU Approval - status to pending cdpu approval/cdpu rejected
                            if (Session["IsDealer"].ToBool() || Roles.IsUserInRole("MREG_DAPP"))
                            {
                                
                                if (personalDetailsVM.CDPUStatus == Constants.StatusCDPURejected)
                                {
                                    GeneralResult result = WebHelper.Instance.UnreserveNumberDealerNet(personalDetailsVM.RegID);
                                    if (!ReferenceEquals(result, null))
                                    {
                                        if (result.ResultCode == 0 && result.ResultMessage == "success")
                                        {
                                            using (var proxy = new RegistrationServiceProxy())
                                            {
                                                proxy.RegistrationCancel(new RegStatus()
                                                {
                                                    RegID = personalDetailsVM.RegID,
                                                    Active = true,
                                                    CreateDT = DateTime.Now,
                                                    StartDate = DateTime.Now,
                                                    LastAccessID = Util.SessionAccess.UserName,
                                                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCDPURejected)
                                                });

                                                proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                                                {
                                                    RegID = personalDetailsVM.RegID,
                                                    CancelReason = Properties.Settings.Default.Status_RegCDPURejected,
                                                    CreateDT = DateTime.Now,
                                                    LastUpdateDT = DateTime.Now,
                                                    LastAccessID = Util.SessionAccess.UserName
                                                });

                                                proxy.UpsertBRE(new RegBREStatus()
                                                {
                                                    RegId = personalDetailsVM.RegID,
                                                    TransactionStatus = Constants.StatusCDPURejected,
                                                    TransactionReason = personalDetailsVM.CDPUDescription,
                                                    TransactionDT = DateTime.Now,
                                                    TransactionBy = Util.SessionAccess.UserName
                                                });
                                            }
                                            return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                            
                                        }
                                    }
                                    else
                                    {
                                        return RedirectToAction("CustomerSummary", new { regID = personalDetailsVM.RegID });
                                    }
                                }
                                if (personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove)
                                {
                                    using (var proxy = new RegistrationServiceProxy())
                                    {
                                        proxy.UpsertBRE(new RegBREStatus()
                                        {
                                            RegId = personalDetailsVM.RegID,
                                            TransactionStatus = Constants.StatusCDPUApprove,
                                            TransactionReason = personalDetailsVM.CDPUDescription,
                                            TransactionDT = DateTime.Now,
                                            TransactionBy = Util.SessionAccess.UserName
                                        });
                                    }
                                }
                            }
                            #endregion

                            #region comment out
                            /*
                            using (var proxy = new KenanServiceProxy())
                            {
                                if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                                {
                                    proxy.CreateIMPOSFile(personalDetailsVM.RegID);
                                }
                            }
                            */
                            #endregion
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                #region Saving POS FileName in DB for Dealers
                                if ((Session["IsDealer"].ToBool() == true && collection["hdnTotalPayable"].ToString2() != string.Empty && collection["hdnTotalPayable"].ToDecimal() > 0))
                                {
                                    string posFileName = WebHelper.Instance.CreatePOSFileName(resp.ID);
                                    using (var catLogProxy = new CatalogServiceProxy())
                                    {
                                        catLogProxy.UpdateIMPOSDetails(resp.ID, posFileName, 1);
                                    }
                                }
                                #endregion


                                #region email functionality

                                if (Session["KenanCustomerInfo"] != null)
                                {
                                    string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString().Split('╚');
                                    KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
                                    kenanInfo.FullName = strArrKenanInfo[0];
                                    kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
                                    kenanInfo.IDCardNo = strArrKenanInfo[2];
                                    kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
                                    kenanInfo.Address1 = strArrKenanInfo[4];
                                    kenanInfo.Address2 = strArrKenanInfo[5];
                                    kenanInfo.Address3 = strArrKenanInfo[6];
                                    kenanInfo.Postcode = strArrKenanInfo[8];
                                    kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
                                    kenanInfo.RegID = resp.ID;
                                    var kenanResp = new KenanCustomerInfoCreateResp();
                                    kenanResp = proxy.KenanCustomerInfoCreate(kenanInfo);

                                }
                                Session["KenanCustomerInfo"] = null;
                                WebHelper.Instance.ClearRegistrationSession();
                                Session["Comp2GBExists"] = null;
                                #region "Generating Impos File and Moving the status to ready for Payment"
                                try
                                {
                                    //for genarate IMPOS file
                                    /*
                                    using (var kenanProxy = new KenanServiceProxy())
                                    {
                                        kenanProxy.CreateSIMReplacementPOSFile(resp.ID);
                                    }
                                     * */
                                    //Update the account status in lnkregstatus
                                    //proxy.RegStatusCreate(WebHelper.Instance.ConstructRegStatus(resp.ID, Properties.Settings.Default.Status_ReadyForPayment), null);

                                    #region Code for SRWWO by chetan
                                    if (Session["AllowSimReplacement"].ToString2() == "True")
                                    {
                                        string _selectedexternalID = !ReferenceEquals(Session[SessionKey.ExternalID.ToString()], null) ? Session[SessionKey.ExternalID.ToString()].ToString() : null;
                                        var custIdentity = string.Empty;
                                        if (Session[SessionKey.PPID.ToString()] != null)
                                        {
                                            var AcctListByICResponse = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                                            SubscriberICService.CustomizedCustomer customerInfo = new CustomizedCustomer();
                                            SubscriberICService.Items selectedAccount = new Items();
                                            selectedAccount = AcctListByICResponse.itemList.Where(v => v.ExternalId == _selectedexternalID).FirstOrDefault();

                                            if (selectedAccount.Account != null)
                                            {
                                                customerInfo = selectedAccount.Customer;
                                                if (customerInfo.NewIC != string.Empty)
                                                    custIdentity = customerInfo.NewIC;
                                                else if (customerInfo.PassportNo != string.Empty)
                                                    custIdentity = customerInfo.PassportNo;
                                                else if (customerInfo.OldIC != string.Empty)
                                                    custIdentity = customerInfo.OldIC;
                                                else if (customerInfo.OtherIC != string.Empty)
                                                    custIdentity = customerInfo.OtherIC;
                                                CreateInteractionCaseResponse Cresp = new CreateInteractionCaseResponse();
                                                try
                                                {
                                                    if (WebHelper.Instance.CompareCMSSDtForAcct(custIdentity) > 14.0)
                                                    {
                                                        Cresp = WebHelper.Instance.CreateCaseInCMSS(selectedAccount.AcctExtId.ToString2(),
														   Constants.CMSS_CASE_COMPLEXITY,
                                                           ConfigurationManager.AppSettings["CMSSQueue"].ToString2(),
                                                           "59",
                                                           ConfigurationManager.AppSettings["CMSSDirection"].ToString2(),
                                                           selectedAccount.Account != null ? selectedAccount.Account.AccountNumber.ToString2() : "",
                                                           selectedAccount.ExternalId.ToString2(),
                                                           (collection["NewCBR"].ToString().Length > 0 ? collection["NewCBR"].ToString2() : string.Empty),
                                                           ConfigurationManager.AppSettings["CMSSProduct"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSReason1"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSReason2"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSReason3"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSSysName"].ToString2(),
                                                           ConfigurationManager.AppSettings["CMSSTitle"].ToString2(),
														   Constants.CMSS_TYPE, 
														   Constants.CMSS_CASE_TOPROCEEDWITH,
                                                            resp.ID.ToInt(),
                                                            custIdentity.ToString2());

                                                        ViewBag.AllowSubmit = "True";
                                                        if (Cresp != null && !string.IsNullOrEmpty(Cresp.Id))
                                                        {
                                                            cmssCaseStatus = "Success";
                                                            cmssCaseId = Cresp.Id;
                                                        }
                                                        else
                                                            cmssCaseStatus = "Fail";
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    cmssCaseStatus = "Fail";
                                                    Logger.Info("Exception while CMSS Case Creation : ExternalID -" + selectedAccount.AcctExtId.ToString2() + " : Message - " + ex.Message + ": InnerException - " + ex.InnerException);
                                                    string xmlReq = Util.ConvertObjectToXml(selectedAccount);
                                                    Logger.Info("Exception while CMSS Case Creation for Account Selected : XML -" + xmlReq.ToString2());
                                                }
                                            }

                                        }
                                    }
                                    #endregion upto here code for SRWWO
                                    TempData["CMSSCaseID"] = cmssCaseId;
                                    TempData["CMSSCaseStatus"] = cmssCaseStatus;

									decimal simReplacementTotalPayable = !string.IsNullOrWhiteSpace(Session["SIMReplacementTotalPayable"].ToString2()) ? Session["SIMReplacementTotalPayable"].ToDecimal() : 0;
									decimal hdnTotalPayable = !string.IsNullOrWhiteSpace(collection["hdnTotalPayable"].ToString2()) ? collection["hdnTotalPayable"].ToDecimal() : 0;

									// Gerry - comment this because auto knock off not only for dealer
                                    //if (Session["ISDealer"].ToBool
                                    if (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus.Trim()) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove)
									{
                                        
                                        
                                        #region Auto activation/Ready for Payment redirection for Dealer

                                        if ((simReplacementTotalPayable > 0 || hdnTotalPayable > 0) && Session["IsDealer"].ToBool())
                                        {
                                            WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                        }
										if (simReplacementTotalPayable == 0 && hdnTotalPayable == 0)
                                        {
                                            if (resp.ID > 0)
                                            {                                           
                                                personalDetailsVM.RegID = resp.ID;
                                            }

                                            #region "Cashier Payment Received Section for Dealer"

                                            int _simReplacementTypeAuto = 0;
                                            bool isPrimaryLineCompsAddedAuto = false;
                                            var requestAuto = new OrderFulfillRequest()
                                            {
                                                OrderID = personalDetailsVM.RegID.ToString(),
                                                UserSession = Util.SessionAccess.UserName,

                                            };
                                            UpdateRegStatus(new RegStatus()
                                            {
                                                RegID = personalDetailsVM.RegID,
                                                Active = true,
                                                CreateDT = DateTime.Now,
                                                StartDate = DateTime.Now,
                                                LastAccessID = Util.SessionAccess.UserName,
                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                                            });


                                            string usernameAuto = string.Empty;
                                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                                            {
                                                usernameAuto = Request.Cookies["CookieUser"].Value;
                                            }
                                            eaiResponseType objeaiResponseTypeAuto = new eaiResponseType();
                                            LnkRegDetailsReq objRegDetailsReqAuto = new LnkRegDetailsReq();
                                            if (personalDetailsVM.RegID > 0)
                                            {
                                                using (var regProxy = new RegistrationServiceProxy())
                                                {
                                                    objRegDetailsReqAuto = regProxy.GetLnkRegistrationDetails(personalDetailsVM.RegID);
                                                    _simReplacementTypeAuto = objRegDetailsReqAuto.LnkDetails.SimReplacementType.ToInt();
                                                }

                                                #region 11052015 - Anthony - Generate RF for iContract
                                                try
                                                {
                                                    using (var updateProxy = new UpdateServiceProxy())
                                                    {
                                                        if (!updateProxy.FindDocument(Constants.CONTRACT, personalDetailsVM.RegID))
                                                        {
                                                            String contractHTMLSource = string.Empty;

                                                            if (_simReplacementTypeAuto == SimReplacementType.PREPAID.ToInt())
                                                            {
                                                                contractHTMLSource = Util.RenderPartialViewToString(this, "SimReplacementPrint", WebHelper.Instance.GetContractDetailsViewModel_PrepaidSIMReplacement(personalDetailsVM.RegID, false));
                                                            }
                                                            else
                                                            {
                                                                contractHTMLSource = Util.RenderPartialViewToString(this, "CofServiceFormPrint", WebHelper.Instance.GetContractDetailsViewModel_SIMReplacement(personalDetailsVM.RegID, false));
                                                            }
                                                            contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
                                                            contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
                                                            contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide");
                                                            if (!string.IsNullOrEmpty(contractHTMLSource))
                                                            {
                                                                Byte[] PDFBytes = Util.convertHTMLStringToPDFByteArray(contractHTMLSource, personalDetailsVM.RegID);
                                                            }
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    //Logger.Error("ICONTRACT EXCEPTION = " + ex);
                                                    WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                                }
                                                #endregion
                                            }

                                            #region "Normal to MISM Convertion Flow"
                                            if (_simReplacementTypeAuto > 0 && _simReplacementTypeAuto == (int)SimReplacementType.N2M)
                                            {
                                                #region "Primary Line Details"
                                                DAL.Models.Registration reg = null;
                                                bool isPrimaryCompAdded = false;
                                                using (var prxy = new RegistrationServiceProxy())
                                                {
                                                    reg = prxy.RegistrationGet(personalDetailsVM.RegID);

                                                    var regPgmBdlPkgCompIDs = prxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                                                    {
                                                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                                                        {
                                                            RegID = reg.ID,
                                                        }
                                                    }).ToList();

                                                    #region "Add Greater than 2GB Components functional logic"
                                                    if (regPgmBdlPkgCompIDs.Count() > 0)
                                                    {
                                                        using (var proxyKenan = new KenanServiceProxy())
                                                        {
                                                            if (!ReferenceEquals(personalDetailsVM.RegID, null) && personalDetailsVM.RegID > 0)
                                                            {
                                                                List<int> Ids = new List<int>();
                                                                Ids.Add(reg.ID);
                                                                CenterOrderContractRequest Req = new CenterOrderContractRequest();
                                                                Req.orderId = personalDetailsVM.RegID.ToString();
                                                                if (Request.Cookies["CookieUser"] != null)
                                                                    Req.UserName = Request.Cookies["CookieUser"].Value;
                                                                isPrimaryCompAdded = proxyKenan.CenterOrderContractCreationPrimaryLine(Req);
                                                                isPrimaryLineCompsAddedAuto = true;
                                                                if (!isPrimaryCompAdded)
                                                                {
                                                                    Logger.InfoFormat("{0}:{1}# Primary Line Components registration Failed, response{2}", "QSimReplacement", "CustomerSummary", isPrimaryCompAdded);
                                                                    UpdateRegStatus(new RegStatus()
                                                                    {
                                                                        RegID = personalDetailsVM.RegID,
                                                                        Active = true,
                                                                        CreateDT = DateTime.Now,
                                                                        StartDate = DateTime.Now,
                                                                        LastAccessID = Util.SessionAccess.UserName,
                                                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_ADDCOMP_FAIL)
                                                                    });
                                                                    if (Roles.IsUserInRole("MREG_DAPP"))
                                                                    {
                                                                        return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                                                    }
                                                                    return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });

                                                                }

                                                                using (var regProxy = new RegistrationServiceProxy())
                                                                {
                                                                    regProxy.RegistrationCancel(new RegStatus()
                                                                    {
                                                                        RegID = personalDetailsVM.RegID,
                                                                        Active = true,
                                                                        CreateDT = DateTime.Now,
                                                                        StartDate = DateTime.Now,
                                                                        LastAccessID = Util.SessionAccess.UserName,
                                                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_SvcPendingActivate)
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                }
                                                #endregion

                                                #region "Secondary Line Details"
                                                if (!isPrimaryLineCompsAddedAuto) // in case of primary line components not exists or created and only secondary line should be created.
                                                {
                                                    using (var proxySec = new RegistrationServiceProxy())
                                                    {
                                                        var RegSec = proxySec.RegistrationSecGet(personalDetailsVM.RegID);
                                                        var pbpcIDsSec = new List<int>();
                                                        if (RegSec != null)
                                                        {
                                                            var regPgmBdlPkgCompIDsSec = proxySec.RegPgmBdlPkgCompSecFind(new RegPgmBdlPkgCompSecFind()
                                                            {
                                                                RegPgmBdlPkgCompSec = new RegPgmBdlPkgCompSec()
                                                                {
                                                                    RegID = RegSec.ID,
                                                                }
                                                            }).ToList();
                                                            var pbpcSec = new List<RegPgmBdlPkgCompSec>();
                                                            if (regPgmBdlPkgCompIDsSec.Count() > 0)
                                                                pbpcSec = proxySec.RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec).ToList();
                                                            //main line
                                                            if (pbpcSec.Count() > 0)
                                                                pbpcIDsSec = pbpcSec.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();
                                                            if (!ReferenceEquals(objRegDetailsReqAuto, null) && !ReferenceEquals(objRegDetailsReqAuto.LnkDetails.SimType, null) && objRegDetailsReqAuto.LnkDetails.SimType == "MISM")
                                                            {
                                                                // Added the functionality of registering the additional secondary line for MISM
                                                                var regSecondary = RegSec;
                                                                var isMISM = regSecondary == null ? false : true;
                                                                var extid = new List<KenanTypeExternalId>();
                                                                var extidnew = new List<KenanTypeExternalId>();
                                                                extidnew.Add(new KenanTypeExternalId() { ExternalId = string.Empty, ExternalIdType = string.Empty });
                                                                extid.Add(new KenanTypeExternalId() { ExternalId = reg.MSISDN1 == null ? "" : reg.MSISDN1, ExternalIdType = (reg.AccExternalID == null ? "" : reg.AccExternalID) });
                                                                if (isMISM == true && regSecondary.IsSecondaryRequestSent == false)
                                                                {
                                                                    var regAccount = new DAL.Models.RegAccount();
                                                                    regSecondary.IsSecondaryRequestSent = true;
                                                                    using (var reproxy = new RegistrationServiceProxy())
                                                                    {
                                                                        regSecondary.IsSecondaryRequestSent = true;
                                                                        int result = reproxy.UpdateSecondaryRequestSent(regSecondary.ID);
                                                                    }
                                                                    using (var kProxy = new KenanServiceProxy())
                                                                    {
                                                                        var request2 = new KenanTypeAdditionLineRegistration()
                                                                        {
                                                                            OrderId = "ISELLMISM" + reg.ID.ToString() + "SIM",
                                                                            FxAcctNo = (reg.fxAcctNo == null) ? "" : reg.fxAcctNo,
                                                                            ExternalId = (extid.ToList().ToArray() == null) ? extidnew.ToList().ToArray() : extid.ToList().ToArray(),
                                                                        };
                                                                        var response = kProxy.KenanAdditionLineRegistrationMISMSecondary(request2);
                                                                        if (response.Code != "0")
                                                                        {
                                                                            Logger.InfoFormat("{0}:{1} Secondary Components Registration Failed at Kenan , Response{2}:{3}", "QSimReplacement", "CustomerSummary", response.Code.ToString2(), response.Message.ToString2());
                                                                            UpdateRegStatus(new RegStatus()
                                                                            {
                                                                                RegID = personalDetailsVM.RegID,
                                                                                Active = true,
                                                                                CreateDT = DateTime.Now,
                                                                                StartDate = DateTime.Now,
                                                                                LastAccessID = Util.SessionAccess.UserName,
                                                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SECCT_FAIL)
                                                                            });
                                                                            if (Roles.IsUserInRole("MREG_DAPP"))
                                                                            {
                                                                                return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                                                            }
                                                                            return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                                                        }
                                                                        else
                                                                        {
                                                                            UpdateRegStatus(new RegStatus()
                                                                            {
                                                                                RegID = personalDetailsVM.RegID,
                                                                                Active = true,
                                                                                CreateDT = DateTime.Now,
                                                                                StartDate = DateTime.Now,
                                                                                LastAccessID = Util.SessionAccess.UserName,
                                                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SECCT)
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion

                                            }
                                            #endregion
                                            // for normal to MISM flow, we should call the SIMSWAP from FullFillORder Success for SecondaryLine Creation.
                                            if (_simReplacementTypeAuto > 0 && _simReplacementTypeAuto != (int)SimReplacementType.N2M)
                                            {
                                                #region "Incase of Mism(primary) to Normal flow , need to disconnect MISM secondary Lines"
                                                if (_simReplacementTypeAuto == (int)SimReplacementType.M2N)
                                                {
                                                    List<lnkSecondaryAcctDetailsSimRplc> SecAccountLines = new List<lnkSecondaryAcctDetailsSimRplc>();
                                                    using (var regproxy = new RegistrationServiceProxy())
                                                    {
                                                        SecAccountLines = regproxy.GetSecondaryAccountLinesForSimRplc(personalDetailsVM.RegID);
                                                    }
                                                    //TODO: 1. NEED TO CHECK WHETHER WE ARE PROCEEDING WITH MISM-P OR MISM-S
                                                    //TODO: 2. NEED TO CONSTRUCT THE SECONDALY LINES FOR MISM-P
                                                    if (SecAccountLines.Count > 0)
                                                    {
                                                        using (var kenanProxy = new KenanServiceProxy())
                                                        {
                                                            int indexval = 0;
                                                            foreach (var itemval in SecAccountLines)
                                                            {
                                                                indexval++;
                                                                kenanProxy.DisconnectComponents(new DisconnectCenterServiceRequest()
                                                                {
                                                                    orderId = "ISELL" + personalDetailsVM.RegID + indexval + "D",
                                                                    newOrderInd = "Y",
                                                                    fxAcctNo = itemval.FxAcctNo,
                                                                    fxSubscrNo = itemval.FxSubscrNo,
                                                                    fxSubscrNoResets = itemval.FxSubscrNoResets,
                                                                    disconnectReason = "1",
                                                                    inpF01 = Util.SessionAccess.UserName,
                                                                    inpF02 = personalDetailsVM.OrganisationId
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion

                                                using (var proxyKenan = new KenanServiceProxy())
                                                {
                                                    objeaiResponseTypeAuto = proxyKenan.SwapCenterInventory(personalDetailsVM.RegID);
                                                }
                                                if (!string.IsNullOrEmpty(objeaiResponseTypeAuto.msgCodeField))
                                                {
                                                    if (int.Parse(objeaiResponseTypeAuto.msgCodeField) > 0)
                                                    {
                                                        UpdateRegStatus(new RegStatus()
                                                        {
                                                            RegID = personalDetailsVM.RegID,
                                                            Active = true,
                                                            CreateDT = DateTime.Now,
                                                            StartDate = DateTime.Now,
                                                            LastAccessID = Util.SessionAccess.UserName,
                                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SWAP_FAIL)
                                                        });
                                                        if (Roles.IsUserInRole("MREG_DAPP"))
                                                        {
                                                            return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                                        }
                                                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                                    }
                                                }
                                                else
                                                {
                                                    UpdateRegStatus(new RegStatus()
                                                    {
                                                        RegID = personalDetailsVM.RegID,
                                                        Active = true,
                                                        CreateDT = DateTime.Now,
                                                        StartDate = DateTime.Now,
                                                        LastAccessID = Util.SessionAccess.UserName,
                                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.REG_SR_SWAP_FAIL)
                                                    });
                                                    if (Roles.IsUserInRole("MREG_DAPP"))
                                                    {
                                                        return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                                    }
                                                    return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                                }
                                            }
                                            #endregion

                                            Session["SIMReplacementTotalPayable"] = null; 
                                            if (Roles.IsUserInRole("MREG_DAPP"))
                                            {
                                                return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                            }
                                            return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });
                                        }
                                        else {
                                            Session["SIMReplacementTotalPayable"] = null;
                                            if (Roles.IsUserInRole("MREG_DAPP"))
                                                return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                            else if (Roles.IsUserInRole("MREG_DSV") && false)
                                                return RedirectToAction("MobileRegAccCreated", new { regID = resp.ID });
                                            else
                                                return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                                        }
                                        
                                        #endregion
                                    }
                                    else
                                    {
                                        if (Roles.IsUserInRole("MREG_DAPP"))
                                        {
                                            return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                        }
                                        else
                                        {
                                            return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
                                        }
                                    }                                   


                                }
                                catch (Exception ex)
                                {
                                    WebHelper.Instance.LogExceptions(this.GetType(), ex);
                                    using (var regProxy = new RegistrationServiceProxy())
                                    {
                                        regProxy.RegistrationCancel(new RegStatus()
                                        {
                                            RegID = resp.ID,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            StartDate = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                                        });
                                    }
                                    return RedirectToAction("MobileRegFail", "QSimReplacement", new { regID = resp.ID });
                                }

                                #endregion

                            }

                                #endregion
                        
                            return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });

                        }
                        catch (Exception ex)
                        {
                            WebHelper.Instance.LogExceptions(this.GetType(), ex);
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                proxy.RegistrationCancel(new RegStatus()
                                {
                                    RegID = personalDetailsVM.RegID,
                                    Active = true,
                                    CreateDT = DateTime.Now,
                                    StartDate = DateTime.Now,
                                    LastAccessID = Util.SessionAccess.UserName,
                                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
                                });
                            }
                            return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                        }
                        #endregion
                        break;
                    default: break;
                }


            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

            return RedirectToAction("MobileRegFail", new { regID = resp.ID });

        }



        [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
        [DoNoTCache]
        public ActionResult SelectVAS(int? planid)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session["RegMobileReg_VasNames"] = null;
            Session["RegMobileReg_VasIDs"] = null;
            Session["MandatoryVasIds"] = null;
            Session["RegMobileReg_PkgPgmBdlPkgCompID"] = planid.ToInt();
            Session["RegMobileReg_DataPkgID"] = null;
            Session["ExtratenMobilenumbers"] = null;
            ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
            if (Session["PPID"].ToString() != string.Empty)
            {
                string processingMsisdn = string.Empty;
                Lstvam = WebHelper.Instance.GetAvailablePackageComponents(planid.ToInt(), ref processingMsisdn, isMandatory: false);
                Session["vasmandatoryids"] = string.Empty;
                using (var proxy = new CatalogServiceProxy())
                {
                    BundlepackageResp respon = new BundlepackageResp();
                    respon = proxy.GetMandatoryVas(planid.ToInt());
                    for (int i = 0; i < respon.values.Count(); i++)
                    {
                        if (respon.values[i].Plan_Type == Settings.Default.CompType_DataCon)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            Session["RegMobileReg_DataPkgID"] = respon.values[i].BdlDataPkgId;
                            List<int> Finalintvalues = null;
                            List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
                            Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes
                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)
                                                             select new AvailableVAS
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode
                                                             };
                            Session["intdataidsval"] = respon.values[i].BdlDataPkgId;
                            Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts);
                            break;
                        }
                    }
                }
                Session["VAS"] = Lstvam2;
                PersonalDetailsVM objPersondetails = new PersonalDetailsVM();
                Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(0, objPersondetails, true);
                if (Lstvam2 != null)
                {
                    return View(Lstvam2);
                }
                Lstvam2 = (ValueAddedServicesVM)Session["VAS"];

                return View(Lstvam2);
            }
            else
            {
                return RedirectToAction("SelectVAS", "QSimReplacement");
            }
        }

        [HttpPost]
        [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
        [DoNoTCache]
        public ActionResult SelectVAS(FormCollection collection, ValueAddedServicesVM vasVM)
        {
            if (collection["TabNumber"].ToInt() == SIMReplacementStep.AccountDetails.ToInt())
            {
                return RedirectToAction(WebHelper.Instance.GetSimReplacementActionStep(collection["TabNumber"].ToInt()));
            }
            if (vasVM.SelectedVasIDs.ToString2().Length > 0)
            {
                vasVM.SelectedVasIDs = vasVM.SelectedVasIDs.ToString2().Replace("[object XMLDocument]", string.Empty);
                Session["SelectedComponents"] = vasVM.SelectedVasIDs;
            }
            #region fxDependency Check
            List<FxDependChkComps> Comps = new List<FxDependChkComps>();
            using (var Proxy = new RegistrationServiceProxy())
            {
                string strCompIds = string.Empty;
                if (vasVM.SelectedVasIDs.ToString2() != string.Empty)
                {
                    strCompIds = vasVM.SelectedVasIDs;
                    Session[SessionKey.SelectedKenanCodes.ToString()] = vasVM.SelectedKenanCodes.ToString2().Replace("[object XMLDocument]", string.Empty);
                }
                // need to add the dataplanID as compIds Here
                strCompIds += "," + Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
                //added bite components 
                if (!ReferenceEquals(Session[SessionKey.RegMobileReg_DataplanID.ToString()], null))
                {
                    int[] arrDpIds = (int[])Session[SessionKey.RegMobileReg_DataplanID.ToString()];
                    foreach (int id in arrDpIds)
                    {
                        strCompIds += "," + id.ToString2();
                    }
                }

            }
            List<Online.Registration.Web.SubscriberICService.componentList> componentList =
                new List<Online.Registration.Web.SubscriberICService.componentList>();
            foreach (var comp in Comps)
            {
                Online.Registration.Web.SubscriberICService.componentList component = new componentList();
                component.componentIdField = comp.ComponentKenanCode;
                component.packageIdField = comp.KenanCode;
                componentList.Add(component);
                if (comp.DependentKenanCode != -1)
                {
                    component = new componentList();
                    component.componentIdField = comp.DependentKenanCode.ToString();
                    component.packageIdField = comp.KenanCode;
                    componentList.Add(component);
                }
            }
            if (componentList.Count > 0)
            {
                // Drop 5
                retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
                string pbpcID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
                typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, pbpcID: pbpcID);

                typeChkFxVasDependencyResponse response = _retrieveServiceInfoProxy.ChkFxVasDependency(request);
                ValueAddedServicesVM vas = (ValueAddedServicesVM)Session["VAS"];
                if (response.mxsRuleMessagesField != null && response.mxsRuleMessagesField.Count > 0)
                {
                    foreach (string errmsg in response.mxsRuleMessagesField)
                    {
                        ModelState.AddModelError(string.Empty, errmsg);
                    }
                    return View(vas);
                }
                else if (response.msgCodeField == "3")
                {
                    ModelState.AddModelError(string.Empty, ConfigurationManager.AppSettings["FXDependencyDownMessage1"].ToString2());   // Need to change msg
                    return View(vas);
                }
                else if (response.msgCodeField != "0")
                {
                    ModelState.AddModelError(string.Empty, ConfigurationManager.AppSettings["FXDependencyDownMessage"].ToString2());   // Need to change msg
                    return View(vas);
                }

            }

            #endregion

            PgmBdlPckComponent package = new PgmBdlPckComponent();
            var selectedVasIDs = string.Empty;
            if (vasVM.SelectedVasIDs != null)
            {
                if (vasVM.SelectedVasIDs != null && !Regex.IsMatch(vasVM.SelectedVasIDs, @"^,+$") && vasVM.SelectedVasIDs != "[object XMLDocument]")
                {
                    /********************* need to revalidate below condition*****************************/
                    selectedVasIDs = vasVM.SelectedVasIDs;
                    Session["RegMobileReg_DataPkgID"] = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
                }
            }
            else
            {
                if (!ReferenceEquals(Session["RegMobileReg_DataPkgID"], null))
                {
                    Session.Contents.Remove("RegMobileReg_DataPkgID");
                }
                if (!ReferenceEquals(Session["RegMobileReg_VasIDs"], null))
                {
                    Session.Contents.Remove("RegMobileReg_VasIDs");
                }
                Session[SessionKey.SelectedComponents.ToString()] = null;
            }
            if (selectedVasIDs != "[object XMLDocument]")
            {
                if (package != null)
                {
                    if (package.ParentID == 2 || package.ParentID == 1)
                    {
                        string defCompId = string.Empty;
                        if (defCompId != string.Empty)
                        {
                            if (selectedVasIDs == null || selectedVasIDs == string.Empty)
                                selectedVasIDs = defCompId;
                            else
                                selectedVasIDs = selectedVasIDs + "," + defCompId;

                            if (selectedVasIDs != string.Empty && selectedVasIDs.Contains(","))
                                selectedVasIDs = string.Join(",", selectedVasIDs.TrimEnd(',').Split(',').ToList<string>().Select(id => id.ToString()).Distinct()) + ",";
                        }
                    }
                }
            }
            try
            {
                if ((Convert.ToString(Session["SimType"]) == "MISM") && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs)))
                {
                    vasVM.SelectedVasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
                    Session["RegMobileReg_VasIDs"] = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            DropFourObj DropForObj = !ReferenceEquals(Session[SessionKey.DropFourObj.ToString()], null) ? (DropFourObj)Session[SessionKey.DropFourObj.ToString()] : new DropFourObj();
            DropForObj.personalInformation.TabNumber = SIMReplacementStep.VAS.ToInt();
            Session[SessionKey.DropFourObj.ToString()] = DropForObj;

            //return RedirectToAction("SelectSecondaryPlan", "QSimReplacement");
            return RedirectToAction("CustomerSummaryNew", "QSimReplacement");
        }

        [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
        public ActionResult SelectSecondaryPlan(int type = 0)
        {
            DropFourObj DropForObj = new DropFourObj();
            DropForObj.getExistingLineCount();
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            PersonalDetailsVM objPersondetails = new PersonalDetailsVM();
            Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(0, objPersondetails, true, isLevel1Call: true);
            var packageVM = GetAvailablePackages(false, true);
            if (Session["MISMCount"] == null)
            {
                Session["MISMCount"] = "0";
            }
            packageVM.MISMCount = Convert.ToInt32(Session["MISMCount"].ToString());

            PersonalDetailsVM objCurDetails = new PersonalDetailsVM();
            packageVM.PersonalDetailsVM = objCurDetails;
            return View(packageVM);
        }

        [HttpPost]
        [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
        public ActionResult SelectSecondaryPlan(PackageVM packageVM)
        {
            //VLT ADDED CODE to check for PPID verification
            if (Session["PPID"].ToString() != string.Empty)
            {
                if (!ReferenceEquals(Session["RedirectURL"], null))
                {
                    Session.Contents.Remove("RedirectURL");
                }

                Session["RegMobileReg_TabIndex"] = packageVM.TabNumber;

                try
                {
                    // selected different Plan
                    if (Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"].ToInt() != packageVM.SelectedPgmBdlPkgCompID)
                    {
                        Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"] = packageVM.SelectedPgmBdlPkgCompID;
                        Session["SelectedPlanID_Seco"] = packageVM.SelectedPgmBdlPkgCompID;
                        // Retrieve Plan Min Age
                        using (var proxy = new CatalogServiceProxy())
                        {
                            var pbpc = MasterDataCache.Instance.FilterComponents(new int[] { packageVM.SelectedPgmBdlPkgCompID }).SingleOrDefault();

                            //Chnages made by chetan for SKMM
                            Session["UOMPlanID_Seco"] = pbpc.KenanCode;

                            var bundleID = pbpc.ParentID;
                            var programID = MasterDataCache.Instance.FilterComponents(new PgmBdlPckComponentFind()
                            {
                                PgmBdlPckComponent = new PgmBdlPckComponent()
                                {
                                    ChildID = bundleID,
                                    LinkType = Properties.Settings.Default.Program_Bundle
                                },
                                Active = true
                            }).SingleOrDefault().ParentID;

                            var minAge = proxy.ProgramGet(new int[] { programID }).SingleOrDefault().MinAge;
                            Session["RegMobileReg_ProgramMinAge_Seco"] = minAge;
                        }

                        // reset session
                        Session["RegMobileReg_ContractID_Seco"] = null;
                        Session["RegMobileReg_DataplanID_Seco"] = null;
                        Session["RegMobileReg_VasIDs_Seco"] = null;
                        Session["RegMobileReg_VasNames_Seco"] = null;
                        Session["VasGroupIds_Seco"] = null;
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogExceptions(this.GetType(), ex);
                    throw ex;
                }

                return RedirectToAction("SelectSecondaryVAS");
            }
            else
            {
                Session["RedirectURL"] = "GuidedSales/PlanSearch";
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
        public ActionResult SelectSecondaryVAS()
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            Session["RegMobileReg_MainDevicePrice_Seco"] = null;
            Session["RegMobileReg_OfferDevicePrice_Seco"] = null;
            Session[SessionKey.RegMobileReg_VasNames_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()] = null;
            WebHelper.Instance.ResetDepositSessions();
            Session["MandatoryVasIds"] = null;
            ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
            ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
            if (Session["VasGroupIds_Seco"].ToString2() != string.Empty)
            {
                Lstvam2.hdnGroupIds = Session["VasGroupIds_Seco"].ToString2();
            }

            if (Session["PPID"].ToString() != string.Empty)
            {
                if (!ReferenceEquals(Session["RedirectURL"], null))
                {
                    Session.Contents.Remove("RedirectURL");
                    Session[SessionKey.MenuType.ToString()] = "1";
                }
                if (Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"] == null || (Convert.ToString(Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"]) == String.Empty))
                {
                    Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"] = Session["GuidedSales_PkgPgmBdlPkgCompID_Seco"];
                }
                var orderVM = new OrderSummaryVM();
                if ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"] != null)
                    orderVM = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];
                orderVM.SelectedModelImageIDSeco = 0;
                Session["RegMobileReg_OrderSummary"] = orderVM;
                SimReplacementVm objQSimReplacementVM = new SimReplacementVm();
                PersonalDetailsVM objPersonalDetailsVM = new PersonalDetailsVM();
                if (Session["RegMobileReg_PersonalDetailsVM"] != null)
                {
                    objPersonalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
                }
                Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(0, objPersonalDetailsVM, true);
                string processingMsisdn = string.Empty;
                Lstvam = WebHelper.Instance.GetAvailablePackageComponents(Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"].ToInt(), ref processingMsisdn, isMandatory: false);
                Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);
                Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
                var Ivalexists = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).PlanBundle;
                /* For Mandatory packages */
                Session["vasmandatoryids"] = string.Empty;
                using (var proxy = new CatalogServiceProxy())
                {
                    var respon = new BundlepackageResp();
                    respon = proxy.GetMandatoryVas(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"]));
                    for (int i = 0; i < respon.values.Count(); i++)
                    {
                        if (respon.values[i].Plan_Type == Settings.Default.Master_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            Session["intmandatoryidsvalSec"] = respon.values[i].BdlDataPkgId;
                            Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
                        }

                        if (respon.values[i].Plan_Type == Settings.Default.CompType_DataCon)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;

                            List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
                            Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes
                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)
                                                             select new AvailableVAS
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode
                                                             };
                            Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts);
                            Session["intdataidsvalSec"] = respon.values[i].BdlDataPkgId;
                        }
                        /*Extra packages*/
                        if (respon.values[i].Plan_Type == Settings.Default.Extra_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                            List<int> Finalintvalues = null;

                            List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
                            Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

                            var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes
                                                             join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)
                                                             select new AvailableVAS
                                                             {
                                                                 ComponentName = e.ComponentName,
                                                                 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                                 Price = e.Price,
                                                                 Value = e.Value,
                                                                 GroupId = e.GroupId,
                                                                 GroupName = e.GroupName,
                                                                 KenanCode = e.KenanCode
                                                             };
                            Session["intextraidsvalSec"] = respon.values[i].BdlDataPkgId;
                            Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);
                        }

                        if (respon.values[i].Plan_Type == Settings.Default.Pramotional_Component)
                        {
                            Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

                            Session["intpramotionidsval"] = respon.values[i].BdlDataPkgId;
                            //MNP MULTIPORT RELATED CHANGES
                            if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                            {
                                if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                                {
                                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intpramotionidsval = respon.values[i].BdlDataPkgId;
                                }
                            }

                            Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
                        }

                    }

                }


                Session["VAS"] = Lstvam2;
                if (Lstvam2 != null)
                {
                    return View(Lstvam2);
                }
                Lstvam2 = (ValueAddedServicesVM)Session["VAS"];
                return View(Lstvam2);
            }
            else
            {
                Session["RedirectURL"] = "Registration/SelectVAS";
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
        public ActionResult SelectSecondaryVAS(FormCollection collection, ValueAddedServicesVM vasVM)
        {
            //added by ravi on jan 23 2014 as it is giving exception
            if (vasVM.SelectedVasIDs.ToString2().Length > 0)
            {
                vasVM.SelectedVasIDs = vasVM.SelectedVasIDs.Replace("[object XMLDocument]", string.Empty);
                Session["SelectedComponents_Seco"] = vasVM.SelectedVasIDs;
            }
            //added by ravi on jan 23 2014 as it is giving exception ends here.

            #region fxDependency Check
            List<FxDependChkComps> Comps = new List<FxDependChkComps>();
            using (var Proxy = new RegistrationServiceProxy())
            {
                string strCompIds = string.Empty;
                if (vasVM.SelectedVasIDs.ToString2() != string.Empty)
                {
                    strCompIds = vasVM.SelectedVasIDs;
                    Session[SessionKey.SelectedComponents_Seco.ToString()] = vasVM.SelectedVasIDs;
                }
                if (Session["MandatoryVasIds"].ToString2().Length > 0)
                {
                    strCompIds = strCompIds + "," + Session["MandatoryVasIds"].ToString();
                }
                strCompIds += "," + Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"].ToString2();
                Comps = Proxy.GetAllComponentsbySecondaryPlanId(Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"].ToInt(), strCompIds);

                #region workaround for MISM component, since MMS need GPRS, but somehow product config table not catered this.
                /*
				foreach (var _comp in Comps)
				{
					if (_comp.ComponentKenanCode == "40032")
					{

						string[] _mismNeededComp = new string[] { "40082", "40090" };
						foreach (var _mismComponent in _mismNeededComp)
						{
							FxDependChkComps _fxComp = new FxDependChkComps();
							_fxComp.DependentKenanCode = 13;
							_fxComp.ComponentKenanCode = _mismComponent;
							_fxComp.KenanCode = _comp.KenanCode;
							Comps.Add(_fxComp);
						}
						break;
					}

				}*/
                #endregion

            }
            List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();

            foreach (var comp in Comps)
            {
                Online.Registration.Web.SubscriberICService.componentList component = new componentList();
                component.componentIdField = comp.ComponentKenanCode;
                component.packageIdField = comp.KenanCode;
                componentList.Add(component);
                if (comp.DependentKenanCode != -1)
                {
                    component = new componentList();
                    component.componentIdField = comp.DependentKenanCode.ToString();
                    component.packageIdField = comp.KenanCode;
                    componentList.Add(component);
                }
            }
            if (componentList.Count > 0)
            {
                // Drop 5
                retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
                string pbpcID = Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"].ToString2();
                typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, pbpcID: pbpcID);

                typeChkFxVasDependencyResponse response = _retrieveServiceInfoProxy.ChkFxVasDependency(request);
                ValueAddedServicesVM vas = (ValueAddedServicesVM)Session["VAS"];
                if (response.mxsRuleMessagesField != null && response.mxsRuleMessagesField.Count > 0)
                {
                    foreach (string errmsg in response.mxsRuleMessagesField)
                    {
                        ModelState.AddModelError(string.Empty, errmsg);
                    }
                    return View(vas);
                }
                else if (response.msgCodeField == "3")
                {
                    ModelState.AddModelError(string.Empty, "OPF system error - Unable to fetch data.");
                    return View(vas);
                }
                else if (response.msgCodeField != "0")
                {
                    ModelState.AddModelError(string.Empty, "Selected components are not available.");
                    return View(vas);
                }

            }
            #endregion
            Session["Secondaryvas"] = "Secondaryvas";
            Session["VasGroupIds_Seco"] = vasVM.hdnGroupIds;
            int selectedPlan = 0;
            List<int> planIds = new List<int>();
            if (Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"] != null)
            {
                selectedPlan = Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"]);
                planIds.Add(selectedPlan);
            }
            PgmBdlPckComponent package = new PgmBdlPckComponent();
            var selectedVasIDs = string.Empty;
            if (vasVM.SelectedVasIDs != null)
            {
                if (vasVM.SelectedVasIDs != null && !Regex.IsMatch(vasVM.SelectedVasIDs, @"^,+$") && vasVM.SelectedVasIDs != "[object XMLDocument]")
                {
                    selectedVasIDs = vasVM.SelectedVasIDs;
                    Session["RegMobileReg_DataPkgID_Seco"] = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
                }
                else
                {
                    Session["SelectedComponents_Seco"] = null;
                }
            }
            else
            {
                if (!ReferenceEquals(Session["RegMobileReg_DataPkgID_Seco"], null))
                {
                    Session.Contents.Remove("RegMobileReg_DataPkgID_Seco");
                }
                if (!ReferenceEquals(Session["RegMobileReg_VasIDs_Seco"], null))
                {
                    Session.Contents.Remove("RegMobileReg_VasIDs_Seco");
                }
            }
            if (selectedVasIDs != "[object XMLDocument]")
            {
                if (package.ParentID == 2 || package.ParentID == 1)
                {
                    string defCompId = string.Empty;

                    if (defCompId != string.Empty)
                    {
                        if (selectedVasIDs == null || selectedVasIDs == string.Empty)
                            selectedVasIDs = defCompId;
                        else
                            selectedVasIDs = selectedVasIDs + "," + defCompId;
                        if (selectedVasIDs != string.Empty && selectedVasIDs.Contains(","))
                            selectedVasIDs = string.Join(",", selectedVasIDs.TrimEnd(',').Split(',').ToList<string>().Select(id => id.ToString()).Distinct()) + ",";
                    }
                }
            }
            try
            {
                if (((Convert.ToString(Session["SimType"]) == "MISM") && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs))))
                {
                    selectedVasIDs = selectedVasIDs.Replace("[object XMLDocument]", string.Empty);
                    vasVM.SelectedVasIDs = selectedVasIDs;
                    Session["RegMobileReg_VasIDs_Seco"] = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

            return RedirectToAction("CustomerSummary", "QSimReplacement");
        }

        /// <summary>
        /// New Action Method for Sim Replacement Print Page
        /// </summary>
        /// <param name="id">Registration Id</param>
        /// <returns>Personal Detials View Model</returns>
        //[Authorize(Roles = "MREG_CH")]
        public ActionResult CofServiceFormPrint(int id)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var regFormVM = new RegistrationFormVM();
            //Joshi Added for DME Printer
            //if (HttpContext.Current.Request.UserAgent.ToLower().Contains("ipad"))
            if (Request.Headers["User-Agent"].ToLower().Contains("ipad"))
            {
                var UserDMEPrint = new UserDMEPrint();
                UserDMEPrint.RegID = id.ToInt();
                UserDMEPrint.UserID = Util.SessionAccess.UserID;
                var status = Util.SaveUserDMEPrint(UserDMEPrint);
                Session["StatusDME"] = status;
            }
            using (var proxy = new RegistrationServiceProxy())
            {
                //Bill Cycle
                regFormVM.RegAttributes = proxy.RegAttributesGetByRegID(id);

                #region Primary Registration
                var pbpcIDs = new List<int>();

                regFormVM.Registration = proxy.RegistrationGet(id);

                var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                {
                    RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                    {
                        RegID = id,
                    }
                }).ToList();

                var pbpc = new List<RegPgmBdlPkgComp>();
                if (regPgmBdlPkgCompIDs.Count() > 0)
                    pbpc = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();


                //main line
                if (pbpc.Count() > 0)
                    pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();

                if (pbpcIDs.Count() > 0)
                {
                    int NewPackageId = 0;
                    var pgmBdlPkgComps = MasterDataCache.Instance.FilterComponents(pbpcIDs).Distinct().ToList();
                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var spCode = Properties.Settings.Default.SecondaryPlan;
                    regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode && (a.PlanType == "CP" || a.PlanType == "SP")).ToList();
                    if (regFormVM.BundlePackages.Count > 0)
                    {
                        regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode && (a.PlanType == "CP" || a.PlanType == "SP")).ToList();
                    }
                    else
                    {
                        regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == spCode).ToList();
                    }
                    if (regFormVM.BundlePackages.Count > 0)
                        NewPackageId = regFormVM.BundlePackages[0].ID;

                    regFormVM.PackageComponents = pgmBdlPkgComps.Where((a => a.LinkType.ToLower() == "pc" || a.LinkType.ToLower() == "ec" || a.LinkType.ToLower() == "dc" || a.LinkType.ToLower() == "ma" || a.LinkType.ToLower() == "oc")).ToList();
                }

                #endregion

                #region Customer
                regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();
                #endregion

                #region Address
                var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                {
                    Active = true,
                    Address = new Address()
                    {
                        RegID = id
                    }
                }).ToList();

                IEnumerable<Address> addresses = proxy.RegAddressGet(regAddrIDs);
                if (addresses != null)
                    regFormVM.Addresses = addresses.ToList();
                #endregion

                #region "Lnkk Registration Details"
                LnkRegDetailsReq objRegDetailsReq = proxy.GetLnkRegistrationDetails(id);
                regFormVM.LnkRegistrationDetails = new LnkRegDetails();

                if (objRegDetailsReq != null && objRegDetailsReq.LnkDetails != null)
                {
                    #region "LnkRegistrationDetails"
                    regFormVM.LnkRegistrationDetails.Id = objRegDetailsReq.LnkDetails.Id;
                    regFormVM.LnkRegistrationDetails.AccountType = objRegDetailsReq.LnkDetails.AccountType.ToString2();
                    regFormVM.LnkRegistrationDetails.RegId = objRegDetailsReq.LnkDetails.RegId;
                    regFormVM.LnkRegistrationDetails.IsSuppNewAc = objRegDetailsReq.LnkDetails.IsSuppNewAc;
                    regFormVM.LnkRegistrationDetails.UserName = objRegDetailsReq.LnkDetails.UserName.ToString2();
                    regFormVM.LnkRegistrationDetails.CreatedDate = objRegDetailsReq.LnkDetails.CreatedDate;
                    regFormVM.LnkRegistrationDetails.isDeviceCRP = objRegDetailsReq.LnkDetails.isDeviceCRP;
                    regFormVM.LnkRegistrationDetails.SimType = objRegDetailsReq.LnkDetails.SimType.ToString2();
                    regFormVM.LnkRegistrationDetails.CmssId = objRegDetailsReq.LnkDetails.CmssId.ToString2();
                    regFormVM.LnkRegistrationDetails.OldSimSerial = objRegDetailsReq.LnkDetails.OldSimSerial.ToString2();
                    regFormVM.LnkRegistrationDetails.NewSimSerial = objRegDetailsReq.LnkDetails.NewSimSerial.ToString2();
                    regFormVM.LnkRegistrationDetails.SimReplReasonCode = objRegDetailsReq.LnkDetails.SimReplReasonCode;
                    regFormVM.LnkRegistrationDetails.SimReplReason = objRegDetailsReq.LnkDetails.SimReplReason.ToString2();
                    regFormVM.LnkRegistrationDetails.SIMModel = objRegDetailsReq.LnkDetails.SIMModel.ToString2();
                    regFormVM.LnkRegistrationDetails.NrcId = objRegDetailsReq.LnkDetails.NrcId.ToString2();
                    regFormVM.LnkRegistrationDetails.SimModelId = objRegDetailsReq.LnkDetails.SimModelId;
                    regFormVM.LnkRegistrationDetails.SimArticalId = objRegDetailsReq.LnkDetails.SimArticalId;
                    regFormVM.LnkRegistrationDetails.ActiveDt = objRegDetailsReq.LnkDetails.ActiveDt.ToString2();
                    regFormVM.LnkRegistrationDetails.PrintVersionNo = objRegDetailsReq.LnkDetails.PrintVersionNo.ToString2();
                    regFormVM.LnkRegistrationDetails.MarketCode = objRegDetailsReq.LnkDetails.MarketCode;
                    regFormVM.LnkRegistrationDetails.Justification = objRegDetailsReq.LnkDetails.Justification.ToString2();
                    regFormVM.LnkRegistrationDetails.Liberlization_Status = objRegDetailsReq.LnkDetails.Liberlization_Status.ToString2();
                    regFormVM.LnkRegistrationDetails.MOC_Status = objRegDetailsReq.LnkDetails.MOC_Status.ToString2();
                    regFormVM.LnkRegistrationDetails.TUTSerial = objRegDetailsReq.LnkDetails.TUTSerial.ToString2();
                    regFormVM.LnkRegistrationDetails.PdpaVersion = objRegDetailsReq.LnkDetails.PdpaVersion.ToString2();
                    regFormVM.LnkRegistrationDetails.SimReplacementType = objRegDetailsReq.LnkDetails.SimReplacementType;
                    #endregion
                }
                #endregion

                #region "Waiver Details"
                List<DAL.Models.WaiverComponents> objWaiverComponents = proxy.GetWaiverComponentsbyRegID(id);
                regFormVM.waiverComponentsList = objWaiverComponents;
                #endregion

                #region "Secondary Line Details"
                regFormVM.RegistrationSec = proxy.RegistrationSecGet(id);

                var pbpcIDsSec = new List<int>();
                if (regFormVM.RegistrationSec != null)
                {
                    var regPgmBdlPkgCompIDsSec = proxy.RegPgmBdlPkgCompSecFind(new RegPgmBdlPkgCompSecFind()
                    {
                        RegPgmBdlPkgCompSec = new RegPgmBdlPkgCompSec()
                        {
                            RegID = regFormVM.RegistrationSec.ID,
                        }
                    }).ToList();

                    var pbpcSec = new List<RegPgmBdlPkgCompSec>();
                    if (regPgmBdlPkgCompIDsSec.Count() > 0)
                        pbpcSec = proxy.RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec).ToList();

                    //main line
                    if (pbpcSec.Count() > 0)
                        pbpcIDsSec = pbpcSec.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();

                    if (pbpcIDsSec.Count() > 0)
                    {
                        var pgmBdlPkgCompsSec = MasterDataCache.Instance.FilterComponents(pbpcIDsSec).Distinct().ToList();
                        var bpCode = Properties.Settings.Default.Bundle_Package;
                        var pcCode = Properties.Settings.Default.Package_Component;
                        var OCCode = Properties.Settings.Default.Other_Component;
                        var RCCode = Properties.Settings.Default.Pramotional_Component;
                        regFormVM.BundlePackagesSec = pgmBdlPkgCompsSec.Where(a => (a.LinkType == bpCode || a.LinkType == "SP") && (a.PlanType == "CP" || a.PlanType == "SP")).ToList();
                        //Filters OC AND RC Added by ravi on 20 may 2014 for TFS Bug Id:2501
                        regFormVM.PackageComponentsSec = pgmBdlPkgCompsSec.Where((a => a.LinkType.ToLower() == "pc" || a.LinkType.ToLower() == "ec" || a.LinkType.ToLower() == "dc" || a.LinkType.ToLower() == "ma" || a.LinkType == OCCode || a.LinkType == RCCode)).ToList();
                    }
                }
                #endregion

                #region Third Party Sim
                regFormVM.DFRegAttribute = proxy.RegAttributesGetByRegID(id);
                if (regFormVM.DFRegAttribute != null)
                {
                    if (regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ORDER).FirstOrDefault() != null && regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ID).FirstOrDefault() != null)
                    {
                        regFormVM.ThirdPartySim = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ORDER).FirstOrDefault().ATT_Value.ToBool();
                        regFormVM.ThirdParty_ApplicantName = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_NAME).FirstOrDefault().ATT_Value;
                        regFormVM.ThirdParty_TypeID = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_TYPE_ID).FirstOrDefault().ATT_Value;
                        regFormVM.ThirdParty_Id = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ID).FirstOrDefault().ATT_Value;
                        regFormVM.ThirdParty_Contact = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_CONTACT).FirstOrDefault().ATT_Value;
                        regFormVM.ThirdParty_BiometricVerify = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_BIOMETRIC_VERIFY).FirstOrDefault().ATT_Value;
                        
                        if (!string.IsNullOrEmpty(regFormVM.ThirdParty_TypeID))
                        {
                            regFormVM.ThirdParty_TypeID = Util.GetNameByID(RefType.IDCardType, regFormVM.ThirdParty_TypeID.ToInt());
                        }
                    }
                }
                #endregion

            }
            using (var proxyOrg = new OrganizationServiceProxy())
            {
                if (!ReferenceEquals(regFormVM.Registration, null) && !ReferenceEquals(regFormVM.Registration.CenterOrgID, null) && regFormVM.Registration.CenterOrgID != 0)
                {
                    regFormVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { regFormVM.Registration.CenterOrgID }).FirstOrDefault();
                }
            }

            if (regFormVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == id).FirstOrDefault() != null)
            {
                regFormVM.BillCycle = regFormVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == id).FirstOrDefault().ATT_Value;
            }
            else
            {
                regFormVM.BillCycle = "N/A";
            }

            WebHelper.Instance.MapRegAttributesByRegID(regFormVM, !ReferenceEquals(regFormVM.Registration, null) ? regFormVM.Registration.ID : id);
            return View(regFormVM);
        }

        /// <summary>
        /// New Action Method for Sim Replacement Print Page
        /// </summary>
        /// <param name="id">Registration Id</param>
        /// <returns>Personal Detials View Model</returns>
        //[Authorize(Roles = "MREG_CH")]
        public ActionResult SimReplacementPrint(int id)
        {
            var regFormVM = new RegistrationFormVM();

            //Joshi Added for DME Printer
            //if (HttpContext.Current.Request.UserAgent.ToLower().Contains("ipad"))
            if (Request.Headers["User-Agent"].ToLower().Contains("ipad"))
            {
                var UserDMEPrint = new UserDMEPrint();
                UserDMEPrint.RegID = id;
                UserDMEPrint.UserID = Util.SessionAccess.UserID;
                var status = Util.SaveUserDMEPrint(UserDMEPrint);
                Session["StatusDME"] = status;
            }
            using (var proxy = new RegistrationServiceProxy())
            {
                // Registration
                regFormVM.Registration = proxy.RegistrationGet(id);

                // Customer
                regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

                // Address
                var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                {
                    Active = true,
                    Address = new Address()
                    {
                        RegID = id
                    }
                }).ToList();
                IEnumerable<Address> addresses = proxy.RegAddressGet(regAddrIDs);
                if (addresses != null)
                    regFormVM.Addresses = addresses.ToList();

                LnkRegDetailsReq objLnkRegDetails = new LnkRegDetailsReq();
                objLnkRegDetails = proxy.GetLnkRegistrationDetails(id);
                regFormVM.LnkRegistrationDetails = objLnkRegDetails.LnkDetails;

                if (objLnkRegDetails.LnkDetails != null)
                    if (objLnkRegDetails.LnkDetails.AccountType == "POSTGSM" && objLnkRegDetails.LnkDetails.SimType != "MISM-S")//&& lnkregdetails.SimType != "MISM"
                        Session[SessionKey.SimType.ToString()] = "Normal";
                    //FWBB and HSDPA include Postpaid
                    else if ((objLnkRegDetails.LnkDetails.AccountType == "HSDPA" || objLnkRegDetails.LnkDetails.AccountType == "FWBB") && objLnkRegDetails.LnkDetails.SimType != "MISM-S")
                        Session[SessionKey.SimType.ToString()] = "Normal";
                    else if (objLnkRegDetails.LnkDetails.AccountType == "PREGSM" && objLnkRegDetails.LnkDetails.SimType != "MISM")
                        Session[SessionKey.SimType.ToString()] = "Prepaid";
                    else if (objLnkRegDetails.LnkDetails.SimType == "MISM-S")
                        Session[SessionKey.SimType.ToString()] = "MISM";
                    else
                        Session[SessionKey.SimType.ToString()] = "Prepaid";

                else
                    Session[SessionKey.SimType.ToString()] = "Prepaid";

                #region Third Party Sim
                regFormVM.DFRegAttribute = proxy.RegAttributesGetByRegID(id);
                if (regFormVM.DFRegAttribute != null)
                {
                    if (regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ORDER).FirstOrDefault() != null && regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ID).FirstOrDefault() != null)
                    {
                        regFormVM.ThirdPartySim = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ORDER).FirstOrDefault().ATT_Value.ToBool();
                        regFormVM.ThirdParty_ApplicantName = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_NAME).FirstOrDefault().ATT_Value;
                        regFormVM.ThirdParty_TypeID = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_TYPE_ID).FirstOrDefault().ATT_Value;
                        regFormVM.ThirdParty_Id = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_ID).FirstOrDefault().ATT_Value;
                        regFormVM.ThirdParty_Contact = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_CONTACT).FirstOrDefault().ATT_Value;
                        regFormVM.ThirdParty_BiometricVerify = regFormVM.DFRegAttribute.Where(x => x.ATT_Name == RegAttributes.ATTRIB_3RDPARTY_BIOMETRIC_VERIFY).FirstOrDefault().ATT_Value;
                        
                        if (!string.IsNullOrEmpty(regFormVM.ThirdParty_TypeID))
                        {
                            regFormVM.ThirdParty_TypeID = Util.GetNameByID(RefType.IDCardType, regFormVM.ThirdParty_TypeID.ToInt());
                        }
                    }
                }
                #endregion
            }
            using (var proxyOrg = new OrganizationServiceProxy())
            {
                if (!ReferenceEquals(regFormVM.Registration, null) && !ReferenceEquals(regFormVM.Registration.CenterOrgID, null) && regFormVM.Registration.CenterOrgID != 0)
                {
                    regFormVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { regFormVM.Registration.CenterOrgID }).FirstOrDefault();
                }
            }

            WebHelper.Instance.MapRegAttributesByRegID(regFormVM, !ReferenceEquals(regFormVM.Registration, null) ? regFormVM.Registration.ID : id);

            return View(regFormVM);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult PrintMobileRegSummary(FormCollection collection, RegistrationFormVM personalDetailsVM)
        {
            Util.CommonPrintMobileRegSummary(collection);
            return RedirectToAction("CustomerSummary", new { id = collection[1].ToString2() });
        }

        [HttpPost]
        public ActionResult UploadImage()
        {
            ///Get the upload and resize locations from settings
            ///PLEASE CREATE 'Uploads' AND 'Resized' FOLDERS IN THE PROJECT
            ///FOR EXAMPLE Online.Registration.Web/Uploads AND Online.Registration.Web/Resized
            string imageUploadLocation = Server.MapPath(Properties.Settings.Default.imageUploadLocation);
            string imageResizeLocation = Server.MapPath(Properties.Settings.Default.imageResizeLocation);
            string fitImage = Properties.Settings.Default.fitImage;
            string usedIC = Session[SessionKey.IDCardNo.ToString()].ToString2();
            string timeStamp = string.Format("{0:yyyyMMddhhmmssffff}", System.DateTime.Now);

            bool isResized = false;

            string SourceImageName, ActualImageName, onlyFileExtension, imageUrl, imageType = string.Empty;

            System.Drawing.Image srcimage = null;
            imageUrl = string.Empty;

            ///CHECKS WHETHER THE Request.Files
            if (!ReferenceEquals(Request.Files, null) && Request.Files.Count > 0)
            {
                ///RETRIEVES THE FIRST KEY IN CASE THERE WOULD BE MULTIPLE FOUND.
                string PostedFileName = Request.Files.AllKeys.FirstOrDefault();
                ///TYPECASTING TO HttpPostedFileBase
                HttpPostedFileBase hpf = Request.Files[PostedFileName] as HttpPostedFileBase;

                ///ONLY PROCESS IF LENGTH >0
                if (hpf.ContentLength > 0)
                {
                    //Keep the actual imagename for future reference
                    //ActualImageName = SourceImageName = Path.GetFileName(hpf.FileName);
                    ActualImageName = SourceImageName = string.Format("{0}-{1}-{2}", usedIC, timeStamp, Path.GetFileName(hpf.FileName));
                    imageType = hpf.ContentType;
                    ///Check whether there is already an image in the folder with the same name
                    Util.checkFileExistance(imageUploadLocation, ref SourceImageName, out onlyFileExtension);
                    imageUploadLocation = Path.Combine(imageUploadLocation + @"\", SourceImageName);
                    ///Save the image
                    hpf.SaveAs(imageUploadLocation);

                    srcimage = System.Drawing.Image.FromFile(imageUploadLocation);
                    ///CALL TO CHECK AND LOWER THE RESOLUTION OF HIGH RESOULTION IMAGE
                    //isResized = Util.ResizeImage(srcimage, 73, imageResizeLocation + SourceImageName, 540, 365, fitImage);
                    isResized = Util.AlwaysResizeImage(srcimage, 23, imageResizeLocation + SourceImageName, 540, 365, fitImage);

                    ///Return the processed or unprocessed image as url
                    if (isResized)
                    {
                        imageResizeLocation = Path.Combine(imageResizeLocation + @"\", SourceImageName);
                        imageUrl = string.Format(@"data:{0};base64,", imageType) + Convert.ToBase64String(System.IO.File.ReadAllBytes(imageResizeLocation));
                    }
                    else
                    {
                        imageUrl = string.Format(@"data:{0};base64,", imageType) + Convert.ToBase64String(System.IO.File.ReadAllBytes(imageUploadLocation));
                    }
                }
            }
            ///RETURNS THE IMAGE IN BASE 64 ENCODED FORMAT
            var jsonData = new { imageUrl = imageUrl };

            var serializer = new JavaScriptSerializer();

            serializer.MaxJsonLength = Int32.MaxValue;

            var data = new ContentResult
            {
                Content = serializer.Serialize(jsonData),
                ContentType = "application/json"
            };

            return data;
        }

        [Authorize]
        public string CheckSimSerialAndTUT(string sim, string simmodel, string simtut)
        {

            #region Variable declaretion
            string resStatus = string.Empty;
            string regszArticleID = string.Empty;
            string regszStatus = string.Empty;
            string SimArticleId = string.Empty;
            if (simmodel.Contains(","))
                SimArticleId = simmodel.Split(',')[1];
            string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
            string storeId = Util.SessionAccess.User.Org.OrganisationId;
            string checkval = string.Empty;
            bool imposStatus = false;
            bool TUTStatus = false;
            bool kenanStatus = false;
            #endregion

            #region TUT validation
            if (!string.IsNullOrEmpty(simtut) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["TUTArticleID"].ToString2()))
            {
                using (var proxy = new DynamicServiceProxy())
                {
                    resStatus = proxy.GetSerialNumberStatus(storeId, simtut.Trim(), wsdlUrl);
                    //Should Uncomment after POS updation in production
                    if (!string.IsNullOrEmpty(resStatus) && resStatus.Contains("|"))
                    {
                        regszArticleID = resStatus.Split('|')[0];
                        regszStatus = resStatus.Split('|')[1];
                    }
                    else
                    {
                        return checkval = "TUT";
                    }
                }

                //Code Changes made by ravi on 24/03/2014 for tur sim serial id
                if (!string.IsNullOrEmpty(regszStatus) && regszStatus == ConfigurationManager.AppSettings["IMEIStatus"])
                {
                    string[] strArray = ConfigurationManager.AppSettings["TUTArticleID"].Split('|');
                    for (int i = 0; i < strArray.Count(); i++)
                    {
                        if (strArray[i].ToString() == regszArticleID.Trim())
                        {
                            Session["TUTArticleID"] = regszArticleID;
                            TUTStatus = true;
                            break;
                        }
                    }
                    if (Session["TUTArticleID"] == null)
                    {
                        return checkval = "TUT";
                    }
                }

            }
            #endregion

            #region Store Service validation for SIM SERIAL
            if (Session["IsDealer"].ToBool())
            {

            }
            else
            {
                using (var proxy = new DynamicServiceProxy())
                {
                    resStatus = proxy.GetSerialNumberStatus(storeId, sim.Trim().Length > 18 ? sim.Trim().Substring(0, 18) : sim.Trim(), wsdlUrl);
                    //Should Uncomment after POS updation in production
                    if (!string.IsNullOrEmpty(resStatus) && resStatus.Contains("|"))
                    {
                        regszArticleID = resStatus.Split('|')[0];
                        regszStatus = resStatus.Split('|')[1];
                    }
                    if (!regszStatus.Equals("ONSTOCK"))
                    {
                        return string.Empty;
                    }
                }
                //Should Uncomment after POS updation in production
                if ((!string.IsNullOrEmpty(regszStatus) || regszStatus != "") && (regszStatus == ConfigurationManager.AppSettings["IMEIStatus"]) && (SimArticleId == regszArticleID))
                {
                    imposStatus = true;
                }
            }
            #endregion

            #region Kenan Inventry Validation
            using (var proxy = new KenanServiceProxy())
            {
                kenanStatus = proxy.validateInventoryExtIdService(new TypeValidateInventoryExtIdRequest
                {
                    externalId = sim
                });
            }
            #endregion

            #region DEPENDING ON BOTH THE RESULT WE ARE RETURNING THE RESULT TO FRONTEND
            if (Session["IsDealer"].ToBool())
            {
                if (kenanStatus)
                {
                    return checkval = "TRUE";
                }
                else
                {
                    return checkval = "SIM";
                }

            }
            else
            {
                if (kenanStatus && imposStatus && TUTStatus)
                {
                    return checkval = "TRUE";
                }
                else if (!TUTStatus)
                {
                    return checkval = "TUT";
                }
                else
                {
                    //Please don't assign true value for checkval always as true
                    //it can leads to order failure in kenan bacause of the wrong SIM Serial Number
                    return checkval = "SIM";
                }
            }
            #endregion
        }
        [HttpPost]
        public JsonResult prepaidStatusCheck(string Details)
        {
            var result = "";
            if (!string.IsNullOrEmpty(Details))
            {
                string[] strSelectAccountDtls = Details.Split(',');
                string msisdn = strSelectAccountDtls[0];
                retrieveAcctListByICResponse AcctListByICResponse = null;

                if (!ReferenceEquals(Session[SessionKey.SR_PPID.ToString()], null))
                {
                    AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.SR_PPID.ToString()];
                }

                try
                {
                    //AcctListByICResponse.itemList = (from x in AcctListByICResponse.itemList where x.ExternalId == strSelectAccountDtls[1].ToString() && x.ServiceInfoResponse.lob == "PREGSM" select x).ToList();
                    if (AcctListByICResponse != null && AcctListByICResponse.itemList != null && AcctListByICResponse.itemList.Any())
                    {
                        var selectAcctDetails = AcctListByICResponse.itemList.Where(c => c.ExternalId == msisdn).ToList();

                        var prepaidServiceInfo = selectAcctDetails.Where(x => x.ServiceInfoResponse != null && x.ServiceInfoResponse.lob == "PREGSM").ToList();
                        if (!ReferenceEquals(prepaidServiceInfo, null))
                        {
                            if (prepaidServiceInfo.Any())
                            {
                                using (var Proxy = new KenanServiceProxy())
                                {
                                    retrievePrepaidSubscriberResponse respone = Proxy.retrievePrepaidSubscriberInfo(msisdn);
                                    if (respone.msgCode == "0")
                                    {
                                        if (respone.acctStatus == "Active" || respone.acctStatus == "Grace")
                                        {
                                            RedirectToAction("QSimReplacement", new { SelectAccountDtls = Details });
                                        }
                                        else
                                        {
                                            result = "0";
                                        }
                                    }
                                    else
                                    {
                                        result = "1";
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = "2";
                }
            }
            else
            {
                result = "2";
            }
            return Json(result);
        }

        #endregion

        #region Private method

        #region Order summary Moved to WebHelper

        private DAL.Models.Registration ConstructRegistration(PersonalDetailsVM personalDetailsVM,string queueNo, string remarks, int simModelId, string articleId, decimal penaltyAmount, string simSerial, string signature, string custPhoto, string altCustPhoto, string photo)
        {
            DAL.Models.Registration registration = new DAL.Models.Registration();
            var isBlacklisted = true;
            registration.K2_Status = Session["RegK2_Status"].ToString2();
            registration = new DAL.Models.Registration();
            registration.CenterOrgID = Util.SessionAccess.User.OrgID;
            registration.RegTypeID = MobileRegType.SimReplacement.ToInt();
            registration.Remarks = remarks;
            registration.QueueNo = queueNo;
            registration.SignatureSVG = signature;
            registration.CustomerPhoto = custPhoto;
            registration.AltCustomerPhoto = altCustPhoto;
            registration.Photo = photo;
            registration.AdditionalCharges = 0;
            registration.InternalBlacklisted = Session["RegMobileReg_IsBlacklisted"] != null ? (bool)Session["RegMobileReg_IsBlacklisted"] : true;
            registration.ExternalBlacklisted = Session["RegMobileReg_IsBlacklisted"] != null ? (bool)Session["RegMobileReg_IsBlacklisted"] : true;

            // w.loon - Is Justification/Supervisor Approval Required
            string ListOfRequiredBreValidation = WebHelper.Instance.ListOfRequiredBreValidation()["ListOfRequiredBreValidation"];

            if (!ReferenceEquals(Session["RegMobileReg_AgeCheckStatusCode"], null) && Session["RegMobileReg_AgeCheckStatusCode"].ToString2() == "AN")
            {
                registration.AgeCheckStatus = "DN";
            }
            else
            {
                registration.AgeCheckStatus = ListOfRequiredBreValidation.Contains("AgeCheck") ?
                                                        !ReferenceEquals(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]) : "AF"
                                                        : "AN";
            }

            registration.DDMFCheckStatus = ListOfRequiredBreValidation.Contains("DDMF Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]) : "DF"
                                                    : "DN";

            if (!ReferenceEquals(Session["RegMobileReg_AddressCheckStatusCode"], null) && Session["RegMobileReg_AddressCheckStatusCode"].ToString2() == "ADN")
            {
                registration.AddressCheckStatus = "DN";
            }
            else
            {
                registration.AddressCheckStatus = ListOfRequiredBreValidation.Contains("Address Check") ?
                                                        !ReferenceEquals(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]) : "ADF"
                                                        : "ADN";
            }
            bool IsWriteOff = false;
            string acc = string.Empty;
            WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
            registration.WriteOffCheckStatus = ListOfRequiredBreValidation.Contains("Writeoff Check") ?
                                                    !IsWriteOff ? "WOS" : "WOF"
                                                    : "WON";
            registration.OutStandingCheckStatus = ListOfRequiredBreValidation.Contains("Outstanding Credit Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()]) : "OF"
                                                    : "ON";
            registration.TotalLineCheckStatus = ListOfRequiredBreValidation.Contains("Total Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()]) : "TLF"
                                                    : "TLN";
            registration.PrincipleLineCheckStatus = ListOfRequiredBreValidation.Contains("Principle Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()]) : "PLF"
                                                    : "PLN";
            registration.BiometricVerify = ListOfRequiredBreValidation.Contains("Biometric") ?
                                                    (!ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? Session[SessionKey.RegMobileReg_BiometricVerify.ToString()].ToBool() : false)
                                                    : false;

            registration.IsVerified = !ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? true : false;
            registration.ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
            registration.RFSalesDT = DateTime.Now;
            registration.PenaltyAmount = penaltyAmount;
            registration.fxAcctNo = Session[SessionKey.FxAccNo.ToString()] == null ? null : Convert.ToString(Session[SessionKey.FxAccNo.ToString()]);
            registration.fxSubscrNo = Session["SubscribeNo"] == null ? null : Convert.ToString(Session["SubscribeNo"]);
            registration.fxSubScrNoResets = Session["SubscribeNoResets"] == null ? null : Convert.ToString(Session["SubscribeNoResets"]);
            registration.IsK2 = false;
            registration.MSISDN1 = Session[SessionKey.RegMobileReg_MobileNo.ToString()] == null ? null : ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2();
            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
            {
                registration.SalesPerson = Request.Cookies["CookieUser"].Value;
            }
            registration.OfferID = 0;
            registration.K2Type = false;
            registration.CreateDT = DateTime.Now;
            registration.LastAccessID = Util.SessionAccess.UserName;
            registration.ContractCheckStatus = ListOfRequiredBreValidation.Contains("Contract Check") ?
                                                !ReferenceEquals(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()]) : "CF"
                                                : "CN";
            registration.UserType = Session[SessionKey.PPID.ToString()] == null ? "N" : Session[SessionKey.PPID.ToString()].ToString();
            registration.PlanAdvance = 0;
            registration.DeviceAdvance = 0;
            registration.PlanDeposit = 0;
            registration.DeviceDeposit = 0;
            registration.OutstandingAcs = Session["FailedAcctIds"] == null ? string.Empty : Session["FailedAcctIds"].ToString();
            registration.PrePortInCheckStatus = !ReferenceEquals(Session["RegMobileReg_IsPortInStatusCode"], null) ? Convert.ToString(Session["RegMobileReg_IsPortInStatusCode"]) : "MPPF";
            registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session["RegMobileReg_IsMNPServiceIdCheckStatusCode"], null) ? Convert.ToString(Session["RegMobileReg_IsMNPServiceIdCheckStatusCode"]) : "MSIF";
            string price = Session["price"].ToString2();
            decimal totalPrice = price.ToDecimal();
            //if (penaltyAmount > 0)
            //{
            //    registration.UOMCode = "ZA1";
            //}
            //else
            //{
            //    registration.UOMCode = "EA";
            //}

            registration.MOC_Status = !ReferenceEquals(Session["MOCIDs"], null) ? Session["MOCIDs"].ToString() : "No";
            registration.Whitelist_Status = !ReferenceEquals(Session["MocandwhitelistSstatus"], null) ? Session["MocandwhitelistSstatus"].ToString() : "No";
            registration.OrganisationId = Util.SessionAccess.User.Org.OrganisationId;
            registration.KenanAccountNo = !ReferenceEquals(Session[SessionKey.KenanACNumber.ToString()], null) ? Session[SessionKey.KenanACNumber.ToString()].ToString() : "";
            registration.Trn_Type = ConfigurationManager.AppSettings["Trn_Type"].ToString2();
            registration.IMPOSStatus = 0;
            registration.SimModelId = simModelId;
            registration.ArticleID = articleId;
            registration.InternationalRoaming = false;
            registration.SIMSerial = simSerial;

			// need to know how dealer is being configured for UOMcode, since simModel cannot be checked
			if (Session["IsDealer"].ToBool())
			{
				registration.UOMCode = "EA";
			}
			else {
				using (var cat_proxy = new CatalogServiceProxy())
				{
					registration.UOMCode = cat_proxy.GetSimModelDetails(simModelId, int.Parse(articleId)).UOM;
				}
			}

            return registration;
        }


        private OrderSummaryVM ConstructOrderSummary(int regid, PersonalDetailsVM objPersonalDetailsVM, bool isCallPlanandComps = false, bool isLevel1Call = false)
        {
            return WebHelper.Instance.ConstructQSIMReplacementOrderSummary(regid, objPersonalDetailsVM, isCallPlanandComps, isLevel1Call: isLevel1Call);
        }

        private List<WaiverComponents> ConstructWaiverDetails(decimal WaiverCharge, bool isWaived)
        {
            List<WaiverComponents> _Waivers = new List<WaiverComponents>();

            WaiverComponents _WaiverComp = new WaiverComponents();
            _WaiverComp.ComponentID = 0;
            _WaiverComp.ComponentName = "Sim Replacement Charge";
            _WaiverComp.price = WaiverCharge;
            _WaiverComp.IsWaived = isWaived;
            _WaiverComp.MismType = "SIM";
            _Waivers.Add(_WaiverComp);
            return _Waivers;
        }
        #endregion

        private string GetsimSerial(string msisdn)
        {
            string SIMSerial = string.Empty;
            Online.Registration.Web.SubscriberICService.retrieveSimDetlsResponse objRetrieveSimDetlsResponse = null;
            SimDetlsList objSimDetlsList = null;
            objRetrieveSimDetlsResponse = new Online.Registration.Web.SubscriberICService.retrieveSimDetlsResponse();
            using (var proxy = new retrieveServiceInfoProxy())
            {
                objRetrieveSimDetlsResponse = proxy.retreiveSIMDetls(msisdn);
            }

            if (objRetrieveSimDetlsResponse != null)
            {
                List<Online.Registration.Web.SubscriberICService.SimDetails> simDetlsFiltered = objRetrieveSimDetlsResponse.itemList.Where(c => c.InactiveDt == null || c.InactiveDt == string.Empty).ToList();
                if (!ReferenceEquals(simDetlsFiltered, null) && simDetlsFiltered.Count > 0)
                {
                    objSimDetlsList = new SimDetlsList(
                    simDetlsFiltered[0].ExternalID.ToString2(),
                    simDetlsFiltered[0].ExternalIDType.ToString2(),
                    simDetlsFiltered[0].InventoryTypeID.ToString2(),
                    simDetlsFiltered[0].InventoryDisplayValue,
                    simDetlsFiltered[0].Reason,
                    simDetlsFiltered[0].ActiveDt,
                    simDetlsFiltered[0].InactiveDt
                   );

                }
            }
            SIMSerial = objSimDetlsList.ExteranalId;
            return SIMSerial;
        }

        private DAL.Models.RegistrationSec ConstructRegistrationForSec(string queueNo, string remarks, int nationalityId, int simModelId, string articleId, string simSerial)
        {
            DAL.Models.RegistrationSec registration;
            registration = new DAL.Models.RegistrationSec();
            var isBlacklisted = true;
            registration.K2_Status = string.Empty;
            registration = new DAL.Models.RegistrationSec();
            registration.CenterOrgID = Util.SessionAccess.User.OrgID;
            registration.RegTypeID = (int)MobileRegType.SimReplacement;
            registration.Remarks = remarks;
            registration.QueueNo = queueNo;
            registration.SignatureSVG = string.Empty;
            registration.CustomerPhoto = string.Empty;
            registration.AltCustomerPhoto = string.Empty;
            registration.Photo = string.Empty;
            registration.AdditionalCharges = 0;
            registration.InternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;
            registration.ExternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;

            // w.loon - Is Justification/Supervisor Approval Required
            string ListOfRequiredBreValidation = WebHelper.Instance.ListOfRequiredBreValidation()["ListOfRequiredBreValidation"];

            registration.AgeCheckStatus = ListOfRequiredBreValidation.Contains("AgeCheck") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]) : "AF"
                                                    : "AN";
            registration.DDMFCheckStatus = ListOfRequiredBreValidation.Contains("DDMF Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]) : "DF"
                                                    : "DN";
            registration.AddressCheckStatus = ListOfRequiredBreValidation.Contains("Address Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]) : "ADF"
                                                    : "ADN";
            registration.OutStandingCheckStatus = ListOfRequiredBreValidation.Contains("Outstanding Credit Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()]) : "OF"
                                                    : "ON";
            registration.TotalLineCheckStatus = ListOfRequiredBreValidation.Contains("Total Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()]) : "TLF"
                                                    : "TLN";
            registration.PrincipleLineCheckStatus = ListOfRequiredBreValidation.Contains("Principle Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()]) : "PLF"
                                                    : "PLN";
            registration.BiometricVerify = ListOfRequiredBreValidation.Contains("Biometric") ?
                                                    (!ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? Session[SessionKey.RegMobileReg_BiometricVerify.ToString()].ToBool() : false)
                                                    : false;

            registration.IsVerified = !ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? true : false;
            registration.ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
            registration.RFSalesDT = DateTime.Now;
            //registration.MSISDN1 = Session[SessionKey.AccExternalID.ToString()] == null ? null : Session[SessionKey.AccExternalID.ToString()].ToString();
            registration.MSISDN1 = WebHelper.Instance.retriveVirtualNumbers(isRegistraton: true);
            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
            {
                registration.SalesPerson = Request.Cookies["CookieUser"].Value;
            }
            registration.K2Type = false;
            registration.CreateDT = DateTime.Now;
            registration.LastAccessID = Util.SessionAccess.UserName;
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "0";
            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty; registration.ContractCheckStatus = ListOfRequiredBreValidation.Contains("Contract Check") ?
                                     !ReferenceEquals(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()]) : "CF"
                                     : "CN";
            registration.UserType = Session[SessionKey.PPID.ToString()].ToString() == null ? "N" : Session[SessionKey.PPID.ToString()].ToString();
            registration.PlanAdvance = (nationalityId == 1) ? ((Session["RegMobileReg_MalayPlanAdv_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayPlanAdv_Seco"].ToString()) : 0) : ((Session["RegMobileReg_OthPlanAdv_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthPlanAdv_Seco"].ToString()) : 0);
            registration.DeviceAdvance = (nationalityId == 1) ? ((Session["RegMobileReg_MalyDevAdv_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyDevAdv_Seco"].ToString()) : 0) : ((Session["RegMobileReg_OthDevAdv_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthDevAdv_Seco"].ToString()) : 0);
            registration.PlanDeposit = (nationalityId == 1) ? ((Session["RegMobileReg_MalyPlanDeposit_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyPlanDeposit_Seco"].ToString()) : 0) : ((Session["RegMobileReg_OthPlanDeposit_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthPlanDeposit_Seco"].ToString()) : 0);
            registration.DeviceDeposit = (nationalityId == 1) ? ((Session["RegMobileReg_MalayDevDeposit_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayDevDeposit_Seco"].ToString()) : 0) : ((Session["RegMobileReg_OthDevDeposit_Seco"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthDevDeposit_Seco"].ToString()) : 0);
            registration.fxAcctNo = Session[SessionKey.FxAccNo.ToString()] == null ? null : Session[SessionKey.FxAccNo.ToString()].ToString();
            registration.externalId = Session[SessionKey.ExternalID.ToString()] == null ? null : Session[SessionKey.ExternalID.ToString()].ToString();
            registration.AccExternalID = Session[SessionKey.AccExternalID.ToString()] == null ? null : Session[SessionKey.AccExternalID.ToString()].ToString();
            registration.OutstandingAcs = Session[SessionKey.FailedAcctIds.ToString()] == null ? string.Empty : Session[SessionKey.FailedAcctIds.ToString()].ToString();
            registration.PrePortInCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()]) : "MPPF";
            registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()]) : "MSIF";
            registration.OfferID = 0;
            registration.UOMCode = "EA";
            registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : "No";
            registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString() : "No";
            registration.OrganisationId = Util.SessionAccess.User.Org.OrganisationId;
            registration.Trn_Type = ConfigurationManager.AppSettings["Trn_Type"].ToString2();
            registration.IMPOSStatus = 0;
            registration.SimModelId = simModelId;
            registration.ArticleID = articleId;
            registration.SIMSerial = simSerial;
            registration.InternationalRoaming = false;

            return registration;
        }


        private List<RegPgmBdlPkgComp> ConstructRegPgmBdlPkgComponent()
        {
            var regPBPCs = new List<RegPgmBdlPkgComp>();
            // Plan PgmBdlPkgComponent
            regPBPCs.Add(new RegPgmBdlPkgComp()
            {
                RegSuppLineID = null,
                PgmBdlPckComponentID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(),
                IsNewAccount = true,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });

            var vasIDs = Session["RegMobileReg_VasIDs"].ToString2().Split(',');
            foreach (var vasID in vasIDs)
            {
                if (!string.IsNullOrEmpty(vasID.ToString()))
                {
                    regPBPCs.Add(new RegPgmBdlPkgComp()
                    {
                        PgmBdlPckComponentID = vasID.ToInt(),
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }

            /*data plan*/
            if (Session["intdataidsval"] != null && Convert.ToInt32(Session["intdataidsval"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    PgmBdlPckComponentID = Session["intdataidsval"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }


            return regPBPCs;
        }

        private List<RegPgmBdlPkgCompSec> ConstructRegPgmBdlPkgSecComponent()
        {

            var regPBPCs = new List<RegPgmBdlPkgCompSec>();

            if (Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()] == null)
                return regPBPCs;


            // Plan PgmBdlPkgComponent
            regPBPCs.Add(new RegPgmBdlPkgCompSec()
            {
                RegSuppLineID = null,
                PgmBdlPckComponentID = Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt(),
                IsNewAccount = true,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });
            // Plan PgmBdlPkgComponent
            regPBPCs.Add(new RegPgmBdlPkgCompSec()
            {
                RegSuppLineID = null,
                PgmBdlPckComponentID = ConfigurationManager.AppSettings["IncludeMandatoryPackageMISM"].ToInt(),
                IsNewAccount = true,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });
            // Mandatory VAS PgmBdlPkgComponent
            var vasPBPCIDs = new List<int>();
            using (var proxy = new CatalogServiceProxy())
            {
                var pkgID = MasterDataCache.Instance.FilterComponents(new int[] { Session[SessionKey.RegMobileReg_PkgPgmBdlPkgCompID_Seco.ToString()].ToInt() }).SingleOrDefault().ChildID;

                vasPBPCIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    IsMandatory = true,
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        ParentID = pkgID,
                        LinkType = Properties.Settings.Default.Package_Component
                    },
                    Active = true
                }).ToList();

                foreach (var vasPBPCID in vasPBPCIDs)
                {
                    regPBPCs.Add(new RegPgmBdlPkgCompSec()
                    {
                        RegSuppLineID = null,
                        PgmBdlPckComponentID = vasPBPCID,
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }
            regPBPCs.Add(new RegPgmBdlPkgCompSec()
            {
                PgmBdlPckComponentID = ConfigurationManager.AppSettings["IncludeMandatoryComponentMISM"].ToInt(),
                IsNewAccount = true,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });
            // Optional VAS PgmBdlPkgComponent
            var vasIDs = Session[SessionKey.RegMobileReg_VasIDs_Seco.ToString()].ToString2().Split(',');

            foreach (var vasID in vasIDs)
            {
                if (!string.IsNullOrEmpty(vasID.ToString()))
                {
                    regPBPCs.Add(new RegPgmBdlPkgCompSec()
                    {
                        PgmBdlPckComponentID = vasID.ToInt(),
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }

            string depcompIds = string.Empty;
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_DataplanID.ToString()], null))
            {
                depcompIds = WebHelper.Instance.GetDepenedencyComponents(Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToString());
            }
            List<string> depListCompIds = new List<string>();
            depListCompIds = depcompIds.Split(',').ToList();

            foreach (string compid in depListCompIds)
            {

                if (compid != string.Empty && Convert.ToInt32(compid) > 0)
                {
                    regPBPCs.Add(new RegPgmBdlPkgCompSec()
                    {
                        PgmBdlPckComponentID = compid.ToInt(),
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }

            }

            /*Mandatory package*/
            if (Session["intmandatoryidsvalSec"] != null && Convert.ToInt32(Session["intmandatoryidsvalSec"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgCompSec()
                {
                    PgmBdlPckComponentID = Session["intmandatoryidsvalSec"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            /*data plan*/
            if (Session["intdataidsvalSec"] != null && Convert.ToInt32(Session["intdataidsvalSec"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgCompSec()
                {
                    PgmBdlPckComponentID = Session["intdataidsvalSec"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }
            /*extra plan*/
            if (Session["intextraidsvalSec"] != null && Convert.ToInt32(Session["intextraidsvalSec"]) != 0)
            {
                regPBPCs.Add(new RegPgmBdlPkgCompSec()
                {
                    PgmBdlPckComponentID = Session["intextraidsvalSec"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });
            }

            return regPBPCs;
        }
        private OrderSummaryVM ConstructOrderSummary(bool fromSubline = false)
        {
            return WebHelper.Instance.ConstructOrderSummary(string.Empty, this.GetType().Name, fromSubline);
        }

        private PackageVM GetAvailablePackages(bool fromSubline = false, bool iSecondary = false)
        {
            var packageVM = new PackageVM();
            var availablePackages = new List<AvailablePackage>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            IEnumerable<PgmBdlPckComponent> programBundles = null;
            var packages = new List<Package>();
            var bundles = new List<Bundle>();
            var bundleIDs = new List<int>();
            var programs = new List<Program>();
            var filterBdlIDs = new List<int>();
            var bundlePackages = new List<PgmBdlPckComponent>();
            var packageIDs = new List<int>();
            var orderVM = new OrderSummaryVM();
            string linktype = string.Empty;
            var masterComponents = MasterDataCache.Instance.PackageComponents;
            using (var proxy = new CatalogServiceProxy())
            {
                var cmpFilter = new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.Program_Bundle,
                        FilterOrgType = Util.SessionOrgTypeCode
                    },
                    Active = true
                };
                filterBdlIDs = MasterDataCache.Instance.FilterComponents(cmpFilter).Select(a => a.ChildID).ToList(); // pckComponents.Select(a => a.ChildID).ToList();
                bundleIDs = filterBdlIDs;


                cmpFilter = new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.SecondaryPlan,
                        PlanType = fromSubline ? Properties.Settings.Default.SupplimentaryPlan : Properties.Settings.Default.ComplimentaryPlan,
                    },
                    ParentIDs = bundleIDs,
                    ChildIDs = packageIDs,
                    Active = true
                };

                var query = MasterDataCache.Instance.FilterComponents(cmpFilter);
                // Main Line
                if (!fromSubline)
                {

                    cmpFilter = new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            LinkType = Properties.Settings.Default.SecondaryPlan,
                            PlanType = Properties.Settings.Default.MandatoryPlan,
                        },
                        ParentIDs = bundleIDs,
                        ChildIDs = packageIDs,
                        Active = true
                    };
                    var querySeco = MasterDataCache.Instance.FilterComponents(cmpFilter);
                    pgmBdlPckComponents = query.ToList();
                    pgmBdlPckComponents.AddRange(querySeco);

                }
                else //Sub Line
                {
                    pgmBdlPckComponents = query.ToList();
                }

                if (pgmBdlPckComponents.Count() > 0)
                {
                    packages = proxy.PackageGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();
                    bundles = proxy.BundleGet(pgmBdlPckComponents.Select(a => a.ParentID).Distinct()).ToList();
                }

                cmpFilter = new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.Program_Bundle,
                    },
                    ChildIDs = pgmBdlPckComponents.Select(a => a.ParentID).ToList()
                };
                programBundles = MasterDataCache.Instance.FilterComponents(cmpFilter).Distinct();

                var programIDs = programBundles.Select(a => a.ParentID).ToList();

                if (programIDs != null)
                {
                    if (programIDs.Count() > 0)
                    {
                        programs = proxy.ProgramGet(programIDs).ToList();
                    }
                }
            }

            foreach (var pbpc in pgmBdlPckComponents)
            {
                availablePackages.Add(new AvailablePackage()
                {
                    PgmBdlPkgCompID = pbpc.ID,
                    ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
                    BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
                    PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                    PackageCode = pbpc.Code,
                    KenanCode = pbpc.KenanCode,
                });
            }

            if (Session["SimType"] != null && Session["SimType"].ToString() == "MISM")
            {
                if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan && Session["DeviceType"].ToString2() == "Seco_WithDevice")
                {
                    if (Session["ArticleId_Seco"] != null)
                    {
                        string MOCStatus = !ReferenceEquals(Session["MOCIDs"], null) ? Session["MOCIDs"].ToString() : string.Empty;
                        List<string> objPackages = new List<string>();
                        using (var proxy = new CatalogServiceProxy())
                        {
                            objPackages = proxy.GetPackagesForArticleId(Session["ArticleId_Seco"].ToString(), MOCStatus);
                        }
                        if (objPackages != null && objPackages.Count > 0)
                            packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).Where(a => string.Join(",", objPackages).Contains(a.KenanCode)).ToList();
                    }
                    else
                        packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();

                }
                else
                    packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();
            }
            else
            {
                if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
                {
                    if (Session["ArticleId"] != null)
                    {
                        string MOCStatus = !ReferenceEquals(Session["MOCIDs"], null) ? Session["MOCIDs"].ToString() : string.Empty;
                        List<string> objPackages = new List<string>();
                        using (var proxy = new CatalogServiceProxy())
                        {
                            objPackages = proxy.GetPackagesForArticleId(Session["ArticleId"].ToString(), MOCStatus);
                        }
                        if (objPackages != null && objPackages.Count > 0)
                            packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).Where(a => string.Join(",", objPackages).Contains(a.KenanCode)).ToList();
                    }
                    else
                        packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();
                }
                else
                    packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();
            }
            // Main Line
            if (!fromSubline)
            {
                packageVM.SelectedPgmBdlPkgCompID = iSecondary ? Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"].ToInt() : Session["RegMobileReg_PkgPgmBdlPkgCompID_Seco"].ToInt();
            }
            else
                packageVM.SelectedPgmBdlPkgCompID = Session["RegMobileSub_PkgPgmBdlPkgCompID_Seco"].ToInt();

            return packageVM;
        }

        [Authorize]
        [HttpPost]
        public string CheckSimModel(string sim)
        {
            #region Variable declaretion
            string resStatus = string.Empty;
            string regszArticleID = string.Empty;
            string regszStatus = string.Empty;
            string wsdlUrl = Util.SessionAccess.User.Org.WSDLUrl;
            string storeId = Util.SessionAccess.User.Org.OrganisationId;
            bool kenanStatus = false;
            #endregion
            #region ValidationLogic for sim model
            // commened as we dont want to check the model validation as per discussion with sree
            if (Session["IsDealer"].ToBool())
            {

            }
            else
            {
                using (var proxy = new DynamicServiceProxy())
                {
                    resStatus = proxy.GetSerialNumberStatus(storeId, sim.Trim().Length > 18 ? sim.Trim().Substring(0, 18) : sim.Trim(), wsdlUrl);
                    //Should Uncomment after POS updation in production
                    if (!string.IsNullOrEmpty(resStatus) && resStatus.Contains("|"))
                    {
                        regszArticleID = resStatus.Split('|')[0];
                        regszStatus = resStatus.Split('|')[1];
                    }
                    if (!regszStatus.Equals("ONSTOCK"))
                    {
                        return string.Empty;
                    }
                }
            }

            #region Kenan Inventry Validation
            using (var proxy = new KenanServiceProxy())
            {
                kenanStatus = proxy.validateInventoryExtIdService(new TypeValidateInventoryExtIdRequest
                {
                    externalId = sim
                });

                if (!kenanStatus)
                    return string.Empty;
            }
            #endregion
            return CheckSimModelName(regszArticleID);
            #endregion
        }

        private string CheckSimModelName(string articleId, string uomCode = "")
        {
            List<SelectListItem> objSimModelsList = new List<SelectListItem>();
            objSimModelsList = Util.GetList(RefType.SimModelType, simType: Util.SIMReplacementSIMType(), uomCode: uomCode);
            List<SelectListItem> objSimModels = new List<SelectListItem>();
            foreach (var item in objSimModelsList)
            {
                SelectListItem obj = new SelectListItem();
                string[] SplitarticleId = item.Value.Split(',');
                if (item.Value != Convert.ToString(0))
                    obj.Value = SplitarticleId[1].ToString();
                else
                    obj.Value = item.Value;
                obj.Text = item.Text;
                obj.Selected = item.Selected;
                objSimModels.Add(obj);
            }
            var simModelName = (from x in objSimModels
                                where x.Value == articleId
                                select x.Text).ToList();

			if (!Session["IsDealer"].ToBool())
			{
				if (simModelName.Count > 0)
				{
					var simArticleId = (from x in objSimModelsList
										where x.Text == Convert.ToString(simModelName[0])
										select x.Value).ToList();
					return Convert.ToString(simModelName[0] + "ä" + simArticleId[0]);
				}
				else
					return string.Empty;
			}
			else
			{
				return Convert.ToString("128K U-SIM - HLR 1" + "ä" + "15,20019198");    // Given dummy data for Dearler test. Need to change  
			}

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strReasonId"></param>
        /// <param name="activateDate"></param>
        /// <param name="strSimModelDtls"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetReasonDetails(string strReasonId, string activateDate, string strSimModelDtls)
        {
            int id = 0;
            int articalId = 0;
            if (Session["IsDealer"].ToBool())
            {

            }
            else
            {
                if (strSimModelDtls != null)
                {
                    string[] strSimModelDetls = strSimModelDtls.Split(',');
                    id = int.Parse(strSimModelDetls[0].ToString());
                    articalId = int.Parse(strSimModelDetls[1].ToString());
                }
            }
            var activDate = !string.IsNullOrEmpty(activateDate) && activateDate != "/" ? activateDate : (DateTime.Now).AddDays(-10).ToString();
            var actDate = Convert.ToDateTime(activDate);
            var nowDate = Convert.ToDateTime(DateTime.Now.ToString());
            int months = ((nowDate.Year - actDate.Year) * 12) + (nowDate.Month - actDate.Month);

            if (nowDate.Day < actDate.Day)
            {
                months--;
            }
            CatalogServiceProxy objCatalogService = new CatalogServiceProxy();
            SimReplacementReasons objSimReplacementReasons = new SimReplacementReasons();
            SimReplacementReasonResp objRes = objCatalogService.GetSimReplacementReasonDetails(int.Parse(strReasonId));
            SimModels objSimModels = new SimModels();
            if (Session["IsDealer"].ToBool())
            {
                //SimModels objSimModels = objCatalogService.GetSimModelDetails(id, articalId);
                if (objRes.ReplacementReasonsDetails != null)
                    objSimReplacementReasons.Price = objRes.ReplacementReasonsDetails.Price;
            }
            else
            {
                objSimModels = objCatalogService.GetSimModelDetails(id, articalId);
            }
            objSimReplacementReasons = objRes.ReplacementReasonsDetails;
            string[] strReasonCode = (System.Configuration.ConfigurationManager.AppSettings["FullWaiverReasonCodes"].ToString2()).Split(',');

			//if (strReasonCode.Contains(objSimReplacementReasons.DISCONNECT_REASON.ToString()) && Session["AccountType"].ToString2() != "POSTGSM")
			// Bug 1453 - Charge and Waive components are not found in Kenan 
			// below rule are applicable for prepaid only, since isell introduced wbb / hsdpa sim replacement, need to be very specific on the condition.
			if (strReasonCode.Contains(objSimReplacementReasons.DISCONNECT_REASON.ToString()) && Session["AccountType"].ToString2() == "PREGSM")
            {
                if (months >= 12)
                    objSimReplacementReasons.Price = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PenaltyAmountForMoreOneYear"].ToString());
                else
                {
                    objSimReplacementReasons.Price = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PenaltyAmountForLessOneYear"].ToString());
                }
            }
            else
            {
                if (!Session["IsDealer"].ToBool())
                {
                    objSimReplacementReasons.Price = Decimal.Parse(objSimModels.Price.ToString2());
                }
            }


            Session["RegMobileReg_SimReplacementReasons"] = objSimReplacementReasons;
            return Json(objSimReplacementReasons);
        }

        private void UpdateRegStatus(RegStatus request)
        {
            using (var regProxy = new RegistrationServiceProxy())
            {
                regProxy.RegistrationCancel(request);
            }
        }

        private List<PackageComponents> ConstructDisconnectComponents(string MSISDN)
        {
            List<PackageComponents> pckg = new List<PackageComponents>();
            try
            {
                retrieveAcctListByICResponse AcctListByICResponse = null;
                if (SessionManager.Get("PPIDInfo") != null)
                {
                    AcctListByICResponse = (retrieveAcctListByICResponse)SessionManager.Get("PPIDInfo");
                }
                List<PackageModel> packages = AcctListByICResponse.itemList.Where(c => c.ExternalId == MSISDN).SingleOrDefault().Packages;

                List<string> DCKenanCodes = MasterDataCache.Instance.PackageComponents.Where(a => a.LinkType == "DC" && a.Active == true).Select(d => d.KenanCode).Distinct().ToList();

                foreach (var contract in packages)
                {
                    for (int i = 0; i < contract.compList.Count; i++)
                    {
                        if (DCKenanCodes.Contains(contract.compList[i].componentId))
                        {
                            pckg.Add(new PackageComponents()
                            {
                                PackageId = contract.PackageID,
                                packageInstId = contract.packageInstId,
                                packageInstIdServ = contract.packageInstIdServ,
                                packageDesc = contract.PackageDesc,
                                componentId = contract.compList[i].componentId,
                                componentInstId = contract.compList[i].componentInstId,
                                componentInstIdServ = contract.compList[i].componentInstIdServ,
                                componentActiveDt = contract.compList[i].componentActiveDt,
                                componentInactiveDt = contract.compList[i].componentInactiveDt,
                                componentDesc = contract.compList[i].componentDesc,
                                componentShortDisplay = contract.compList[i].componentShortDisplay
                            }
                            );
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the ConstructDisconnectComponents method:" + ex);
            }

            return pckg;
        }

        private bool CheckDataComponentsOnPlanId(int planId)
        {
            bool isExists = false;
            using (var proxy = new CatalogServiceProxy())
            {
                BundlepackageResp respon = new BundlepackageResp();
                respon = proxy.GetMandatoryVas(planId.ToInt());
                for (int i = 0; i < respon.values.Count(); i++)
                {
                    if (respon.values[i].Plan_Type == Settings.Default.CompType_DataCon)
                    {
                        var Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
                        List<int> Finalintvalues = null;
                        List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
                        Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
                        var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes
                                                         join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)
                                                         select new AvailableVAS
                                                         {
                                                             ComponentName = e.ComponentName,
                                                             PgmBdlPkgCompID = e.PgmBdlPkgCompID,
                                                             Price = e.Price,
                                                             Value = e.Value,
                                                             GroupId = e.GroupId,
                                                             GroupName = e.GroupName,
                                                             KenanCode = e.KenanCode
                                                         };
                        if (!ReferenceEquals(finalfiltereddatacontracts, null) && finalfiltereddatacontracts.Count() > 0)
                        {
                            Dictionary<string, string> DcList = MasterDataCache.Instance.DataComps2GB;

                            if (!(ReferenceEquals(DcList, null)) && DcList.Count > 0)
                            {
                                foreach (var item in finalfiltereddatacontracts)
                                {
                                    if (DcList.Values.Contains(item.KenanCode))
                                    {
                                        isExists = true;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
            }
            return isExists;
        }
        #endregion
        public int PrintDMEOrder(int regId)
        {
            var UserDMEPrint = new UserDMEPrint();
            UserDMEPrint.RegID = regId;
            UserDMEPrint.UserID = Util.SessionAccess.UserID;
            var status = Util.SaveUserDMEPrint(UserDMEPrint);
            // Session["StatusDME"] = status;
            return status;
        }

        [HttpPost]
		public JsonResult getBreValidation(bool thirdParty = false, string ruleOverride = "")
        {
            string ThirdParty_BiometricDesc = string.Empty;
			var validationResult = WebHelper.Instance.breValidate_list(thirdParty, ruleOverride: ruleOverride);
            foreach (var _result in validationResult)
            {
                ThirdParty_BiometricDesc = (_result.Action == "J") ?
                string.IsNullOrEmpty(ThirdParty_BiometricDesc) ? _result.Code : ThirdParty_BiometricDesc + ", " + _result.Code :
                ThirdParty_BiometricDesc;
            }

            return Json(ThirdParty_BiometricDesc);
        }

        [HttpPost]
        public ActionResult performThirdPartyBiometric(string IDCardNo, int IDType, bool thirdParty)
        {
            string ThirdParty_ICValidationError = string.Empty;
            if (IDType == 1)
            {
                if (Util.isValidNewNRIC(IDCardNo))
                {
                    if (!Util.isUnderAgeCustomer(IDCardNo))
                    {
                        return RedirectToAction("RetrieveMyKadInfo", "Registration", new { IDCardNo = IDCardNo, strKenanCustomerInfo = "", thirdParty = true });
                    }
                    else
                    {
                        ThirdParty_ICValidationError = "Age Check Failed, Customer should be at least 18 years to do registration";
                    }
                }
                else
                {
                    ThirdParty_ICValidationError = "Please enter a valid IC number";
                }
            }
            return Json(ThirdParty_ICValidationError);
        }
    }
}

