﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.Helper;

namespace Online.Registration.Web.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class MISMController : Controller
    {
        //
        // GET: /MISM/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MISMRegSummary()
        {
            return View();
        }
    }
}
