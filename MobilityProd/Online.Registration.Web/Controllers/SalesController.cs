﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Online.Registration.Web.Helper;
using Online.Registration.Web.Models;

using log4net;

namespace Online.Registration.Web.Controllers
{
    //[HandleError(View = "GenericError")]
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class SalesController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SalesController));

        //UNUSED METHODS
        //private void LogException(Exception ex)
        //{
        //    Logger.DebugFormat("Exception: {0}", ex.ToString());
        //}

        //UNUSED METHODS
        //private RedirectToRouteResult RedirectException(Exception ex)
        //{
        //    Util.SetSessionErrMsg(ex);
        //    return RedirectToAction("Error", "Home");
        //}

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult New()
        {
            return View(new SalesNewModel());
        }

        [HttpPost]
        public ActionResult New(RegistrationSearchModel searchCriteria)
        {
            var viewModel = new SalesNewModel { SearchCriteria = searchCriteria };

            viewModel.SearchResults = new List<SalesNewRegSearchResult>();
            viewModel.SearchResults.Add(new SalesNewRegSearchResult()
                {
                    CreatedDate = "20/06/2012",
                    CustomerID = 1,
                    FirstName = "Tester",
                    ICNumber = "123345",
                    LastName = "Tan",
                    MSISDN = "60123435",
                    RegID = 1
                });

            viewModel.SearchResults.Add(new SalesNewRegSearchResult()
            {
                CreatedDate = "21/06/2012",
                CustomerID = 2,
                FirstName = "Tester2",
                ICNumber = "124253345",
                LastName = "Lee",
                MSISDN = "601355535",
                RegID = 2
            });

            return View(viewModel);
        }

        public ActionResult Create(int regID)
        {
            var viewModel = new SalesCreateModel()
            {
                RegID = regID,
                Address = "",
                ICNumber = "",
                MSISDN = "",
                Name = ""
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Create(SalesCreateModel viewModel)
        {
            return View();
        }
    }
}
