﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Caching;
using Online.Registration.Web.Helper;
using log4net;
using Online.Web.Helper;

namespace Online.Registration.Web.Controllers
{
   [CustomErrorHandlerAttr(View = "GenericError")]
   
   public class ClearCacheController : Controller
    {
        //
        // GET: /ClearCache/
       private static readonly ILog Logger = LogManager.GetLogger(typeof(ClearCacheController));
       
        public ActionResult Index()
        {
            try
            {
                if (HttpRuntime.Cache["packageVM"] != null)
                {
                    HttpRuntime.Cache.Remove("packageVM");
                    ViewData["ClearCache"] = ErrorMsg.Cache.ClearCacheSuccess;
                }
                else
                    ViewData["ClearCache"] = ErrorMsg.Cache.NoCache;
            }
            catch (Exception ex)
            {


               LogExceptions(ex);
                ViewData["ClearCache"] = ErrorMsg.Cache.ClearCacheFail;
            }
            return View();
        }


        private void LogExceptions(Exception ex)
        {
            /* BEGIN  Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/
            string strErrorMsg = Util.LogException(ex);
            string actionName = this.ControllerContext.RouteData.Values["action"] != null ? this.ControllerContext.RouteData.Values["action"].ToString() : "";
            string controllerName = this.ControllerContext.RouteData.Values["controller"] != null ? this.ControllerContext.RouteData.Values["controller"].ToString() : "";
            Logger.Error("Controller: " + controllerName + "\n" + " Action:" + actionName + "\n" + " Error:" + strErrorMsg, ex);
            /* END Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/

        }
    }
}
