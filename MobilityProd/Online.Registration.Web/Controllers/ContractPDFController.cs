﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using SNT.Utility;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.ViewModels;
namespace Online.Registration.Web.Controllers
{
    public class ContractPDFController : Controller
    {
        //
        // GET: /ContractPDF/
        private static readonly ILog Logger = LogManager.GetLogger(typeof(RegistrationController));

        public ActionResult ContractPDF()
        {
            ViewBag.Sucessmessage = "";
            return View();
        }

        [HttpPost]
        public ActionResult ContractPDF(FormCollection collection)
        {
            try
            {
                int regID = Convert.ToInt32(collection["regID"]);
           
                if (null == regID && regID == 0)
                    return View();

                String contractHTMLSource = string.Empty;
          
                var registration = new DAL.Models.Registration();
                var CRPCnt = new CRPController();

                using (var serviceA = new RegistrationServiceProxy())
                {
                    registration = serviceA.RegistrationGet(regID);
                }
                using (var service = new UpdateServiceProxy())
                {
                    if (registration.RegTypeID == 2 && registration.Trn_Type == "S")
                    {
                        contractHTMLSource = Util.RenderPartialViewToString(this, "Smart_PrintContractDetails", WebHelper.Instance.GetContractDetailsViewModel_SMART(regID, false));
                        contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
                        contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
                        contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide");
                    }
                    else if(registration.RegTypeID == 36)
                    {
                        contractHTMLSource = Util.RenderPartialViewToString(this, "Addcont_PrintContractDetails", WebHelper.Instance.GetContractDetailsViewModel_AddContract(regID, false));
						contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
						contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
						contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide");
                    }
                    else if(registration.RegTypeID == 14)
                    {
                        RegDetailsObjVM regDetailsObj = new RegDetailsObjVM();
                        using (var regProxy = new RegistrationServiceProxy())
                        {
                            regDetailsObj.LnkRegDetailsReq = regProxy.GetLnkRegistrationDetails(regID);
                        }
                        if (regDetailsObj.LnkRegDetailsReq.LnkDetails.SimReplacementType == SimReplacementType.PREPAID.ToInt())
                        {
                            contractHTMLSource = Util.RenderPartialViewToString(this, "SimReplacementPrint", WebHelper.Instance.GetContractDetailsViewModel_PrepaidSIMReplacement(regID, false));
                        }
                        else
                        {
                            contractHTMLSource = Util.RenderPartialViewToString(this, "CofServiceFormPrint", WebHelper.Instance.GetContractDetailsViewModel_SIMReplacement(regID, false));
                        }
                        contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
                        contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
                        contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide");
                    }
                    else if (registration.RegTypeID == 24 || (registration.RegTypeID == 2 && (registration.CRPType == "1" || registration.CRPType == "2" || registration.CRPType == "3" || registration.CRPType == "4")))
                    {
                        contractHTMLSource = Util.RenderPartialViewToString(this, "CRP_PrintContractDetails", WebHelper.Instance.GetContractDetailsViewModel_CRP(regID, false));
                        contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
                        contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
                        contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide");
                    }
                    else
                        contractHTMLSource = Util.RenderPartialViewToString(this, "PrintContractDetails", WebHelper.Instance.GetContractDetailsViewModel(regID, false));
                    if (!string.IsNullOrEmpty(contractHTMLSource))
                    {
                        int? GeneratedID = null;
                        try
                        {
                            Byte[] PDFBytes = Util.convertHTMLStringToPDFByteArray(contractHTMLSource, regID);
                            if (PDFBytes != null)
                            {
                                ViewBag.Sucessmessage = "PDF Generated Successfully.";
                            }
                        }
                        catch (Exception ex)
                        {
                            WebHelper.Instance.LogService(APIname: "ICONTRACT", LastGeneratedID: out GeneratedID, MethodName: "generatePDF", IsBefore: true, ID: GeneratedID, status: "Failure : " + ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                Logger.Error(ex.Message + " (" + ex.StackTrace + ")");
                throw ex;
            }
            return View();
        }


    }
}
