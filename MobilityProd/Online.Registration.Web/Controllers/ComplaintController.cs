﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;
using Online.Registration.Web.Models;
using Online.Registration.Web.SubscriberICService;
using SNT.Utility;
using CMSS = Online.Registration.Web.CMSSSvc;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.CMSSSvc;
namespace Online.Registration.Web.Controllers
{
    //[HandleError(View = "GenericError")]
     [CustomErrorHandlerAttr(View = "GenericError")]
     [SessionAuthenticationAttribute]
    public class ComplaintController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ComplaintController));
        protected override void Initialize(RequestContext requestContext)
        {
            Util.CheckSessionAccess(requestContext);
            base.Initialize(requestContext);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View(new ComplaintCreateModel());
        }

        public ActionResult Success(string caseID)
        {
            ViewBag.CaseID = caseID;
            return View();
        }
        public ActionResult Fail(string Resson)
        {
            ViewBag.CaseID = Resson;
            return View();
        }

        [HttpGet]
        public JsonResult GetCustomerInfo(string msisdn)
        {
            var customerInfo = new ComplaintCreateModel()
            {
                MSISDN = msisdn,
                Name = "John Smith",
                AccountNo = "10491123",
                Address = "Level 22, Menara Maxis Kuala Lumpur City Centre 50088 KL",
                IDNumber = "890301051233",
                IDType = "New IC"
            };
            retrieveAcctListByICResponse AcctListByICResponse = null;
            if (Session[SessionKey.PPIDInfo.ToString()] != null)
            {
                AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()];
            }
            List<Items> itemList = AcctListByICResponse.itemList.Where(a => a.ExternalId == (msisdn)).ToList();
            if (itemList != null)
            {
                if (itemList.Count > 0)
                {
                    customerInfo = new ComplaintCreateModel()
                    {
                        MSISDN = msisdn,
                        Name = itemList[0].Account.Holder,
                        AccountNo = itemList[0].Account.AccountNumber,
                        Address = itemList[0].Account.Address,
                        IDNumber = itemList[0].Account.IDNumber,
                        IDType = itemList[0].Account.IDType
                    };
                }
            }
            try
            {
                if (!Properties.Settings.Default.DevelopmentModeOn)
                {
                    //var account = M2KServiceProxy.GetAccount(msisdn);
                    //var addressDetails = Util.ConcatenateStrings(" ", account.City, account.Zip, account.State);

                    //customerInfo.AccountNo = account.AccountNo.ToString();
                    //customerInfo.Address = Util.ConcatenateStrings(", ", account.Address1, account.Address2, account.Address3, addressDetails);
                    //customerInfo.IDNumber = account.extendedData[0].Id;
                    //customerInfo.IDType = account.extendedData[0].DisVal;
                    //customerInfo.Name = Util.ConcatenateStrings(" - ", account.CompanyName, account.LastName);
                }
            }
            catch (Exception ex)
            {
                // LogException(ex);
                LogExceptions(ex);
                throw ex;
            }

            return Json(customerInfo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetReason1ByIssue(int productID)
        {
            var reason1 = new List<int>();
            var reason1List = new List<ComplaintIssues>();

            using (var proxy = new ComplainServiceProxy())
            {
                var reason1Find = proxy.ComplaintIssuesGroupFind(new ComplaintIssuesGroupFind()
                {
                    ComplaintIssuesGroup = new ComplaintIssuesGroup()
                    {
                        ProductID = productID
                    }
                });
                if (reason1Find.Count() > 0)
                {
                    reason1 = proxy.ComplaintIssuesGroupGet(reason1Find).Select(a => a.Reason1ID).ToList();

                    reason1List = proxy.ComplaintIssuesGet(reason1).OrderBy(o => o.Name).ToList();
                }
            }

            reason1List.Insert(0, new ComplaintIssues() { ID = 0, Name = " -- Select Reason 1 --" });

            return Json(new SelectList(reason1List, "ID", "Name"));
        }

        [HttpPost]
        public ActionResult GetReason2ByReason1(int reason1ID, int productID)
        {
            var reason2 = new List<int>();
            var reason2List = new List<ComplaintIssues>();

            using (var proxy = new ComplainServiceProxy())
            {
                var reason2Find = proxy.ComplaintIssuesGroupFind(new ComplaintIssuesGroupFind()
                {
                    ComplaintIssuesGroup = new ComplaintIssuesGroup()
                    {
                        ProductID = productID,
                        Reason1ID = reason1ID
                    }
                });
                if (reason2Find.Count() > 0)
                {
                    reason2 = proxy.ComplaintIssuesGroupGet(reason2Find).Select(a => a.Reason2ID).ToList();

                    reason2List = proxy.ComplaintIssuesGet(reason2).OrderBy(o => o.Name).ToList();
                }
            }

            reason2List.Insert(0, new ComplaintIssues() { ID = 0, Name = " -- Select Reason 2 --" });

            return Json(new SelectList(reason2List, "ID", "Name"));
        }

        [HttpPost]
        public ActionResult GetReason3ByReason2(int productID, int reason1ID, int reason2ID)
        {
            var reason3 = new List<int>();
            var reason3List = new List<ComplaintIssues>();

            using (var proxy = new ComplainServiceProxy())
            {
                var reason3Find = proxy.ComplaintIssuesGroupFind(new ComplaintIssuesGroupFind()
                {
                    ComplaintIssuesGroup = new ComplaintIssuesGroup()
                    {
                        ProductID = productID,
                        Reason1ID = reason1ID,
                        Reason2ID = reason2ID
                    }
                });
                if (reason3Find.Count() > 0)
                {
                    reason3 = proxy.ComplaintIssuesGroupGet(reason3Find).Select(a => a.Reason3ID).ToList();

                    reason3List = proxy.ComplaintIssuesGet(reason3).OrderBy(o => o.Name).ToList();
                }
            }

            reason3List.Insert(0, new ComplaintIssues() { ID = 0, Name = " -- Select Reason 3 -- " });

            return Json(new SelectList(reason3List, "ID", "Name"));
        }





        [HttpPost]
        public ActionResult GetDynamicText(int productID, int reason1ID, int reason2ID, int reason3ID)
        {
            var DynamicTextIDs = new List<int>();
            string DynamicText = string.Empty;

            using (var proxy = new ComplainServiceProxy())
            {
                var DynamicTextFind = proxy.ComplaintIssuesGroupFind(new ComplaintIssuesGroupFind()
                {
                    ComplaintIssuesGroup = new ComplaintIssuesGroup()
                    {
                        ProductID = productID,
                        Reason1ID = reason1ID,
                        Reason2ID = reason2ID,
                        Reason3ID = reason3ID
                    }
                });
                if (DynamicTextFind.Count() > 0)
                {
                    DynamicTextIDs = proxy.ComplaintIssuesGroupGet(DynamicTextFind).Select(a => a.DynamicFormTextID).ToList();

                    DynamicText = proxy.ComplaintIssuesGet(DynamicTextIDs).Select(a => a.Name).SingleOrDefault();
                }
            }
            return Json(DynamicText);
        }

        #region commented and converted this is to get from ComplaintIssuesGroup

        //[HttpPost]
        //public ActionResult GetDispatchQByReason3(int productID, int reason1ID, int reason2ID, int reason3ID)
        //{
        //    var issueGrpDispatchQList = new List<int>();
        //    var complaintIssuesGrp = new List<int>();
        //    var dispatchQ = new List<int>();
        //    var dispatchQList = new List<ComplaintIssues>();
        //    Session["defaultQuee"] = null;
        //    using (var proxy = new ComplainServiceProxy())
        //    {
        //        // find complaint issues group by 4 items
        //        var complaintIssuesGrpFind = proxy.ComplaintIssuesGroupFind(new ComplaintIssuesGroupFind()
        //        {
        //            ComplaintIssuesGroup = new ComplaintIssuesGroup()
        //            {
        //                ProductID = productID,
        //                Reason1ID = reason1ID,
        //                Reason2ID = reason2ID,
        //                Reason3ID = reason3ID
        //            }
        //        });
        //        if (complaintIssuesGrpFind.Count() > 0)
        //        {
        //            complaintIssuesGrp = proxy.ComplaintIssuesGroupGet(complaintIssuesGrpFind).Select(a => a.ID).ToList();

        //            var dispatchQFind = proxy.IssueGrpDispatchQFind(new IssueGrpDispatchQFind()
        //                {
        //                    ComplaintIssuesGrpIDs = complaintIssuesGrp
        //                });

        //            issueGrpDispatchQList = proxy.IssueGrpDispatchQGet(dispatchQFind).Select(a => a.CaseDispatchQID).ToList();
        //            var dispatchQListFind = proxy.ComplaintIssuesFind(new ComplaintIssuesFind()
        //            {
        //                CaseDispatchQIDs = issueGrpDispatchQList
        //            });

        //            dispatchQList = proxy.ComplaintIssuesGet(dispatchQListFind).ToList();
        //            string defaultQuee = dispatchQList[0].Name.ToString2();
        //            var listComplainIssue =new ComplainServiceProxy().FindComplaintIssues(new ComplaintIssuesFind() { ComplaintIssues = new ComplaintIssues() { Type = "DQ" }, Active = true, CaseDispatchQIDs = new List<int>() });
        //            if (listComplainIssue != null)
        //            {
        //                dispatchQList = listComplainIssue.Select(a => new ComplaintIssues()
        //                {
        //                    ID = a.ID,
        //                    Name = a.Name,
        //                }).OrderBy(o => o.Name).ToList();
        //            }
        //            var resultList = listComplainIssue.OrderBy(o => o.Name).ToList();
        //            var result = resultList.Select((item, index) => new { index, item });

        //            foreach (var test in result)
        //            {
        //                if (test.item.Name.ToString2() == defaultQuee)
        //                {
        //                    Session["defaultQuee"] = test.index;
        //                }

        //            }
        //        }
        //    }

        //    dispatchQList.Insert(0, new ComplaintIssues() { ID = 0, Name = "--Select Group Dispatch Queue--" });

        //    return Json(new SelectList(dispatchQList, "ID", "Name", new {ID = 420}));
        //}

        #endregion

        public ActionResult GetDispatchQ(int productID, int reason1ID, int reason2ID, int reason3ID)
        {
            var dispatchQ = new List<int>();
            var dispatchQList = new List<ComplaintIssues>();

            using (var proxy = new ComplainServiceProxy())
            {
                var dispatchQFind = proxy.ComplaintIssuesGroupFind(new ComplaintIssuesGroupFind()
                {
                    ComplaintIssuesGroup = new ComplaintIssuesGroup()
                    {
                        ProductID = productID,
                        Reason1ID = reason1ID,
                        Reason2ID = reason2ID,
                        Reason3ID = reason3ID
                    }
                });
                if (dispatchQFind.Count() > 0)
                {
                    dispatchQ = proxy.ComplaintIssuesGroupGet(dispatchQFind).Select(a => a.QueueID).ToList();

                    dispatchQList = proxy.ComplaintIssuesGet(dispatchQ).OrderBy(o => o.Name).ToList();
                }
            }

            dispatchQList.Insert(0, new ComplaintIssues() { ID = 0, Name = " -- Select DispatchQ -- " });

            return Json(new SelectList(dispatchQList, "ID", "Name"));
        }

        [HttpPost]
        public ActionResult Create(ComplaintCreateModel viewModel)
        {
            if (!ModelState.IsValid)
                return View(viewModel);

            //if (Properties.Settings.Default.DevelopmentModeOn)
            //    return RedirectToAction("Success", new { caseID = "C11991751" });

            try
            {
                var intTranxID = 0;
                var tranxID = 0;
                var complaintIssuesGrp = new List<int>();
                string CaseComplexity = string.Empty, CasePriority = string.Empty, Direction = string.Empty, SysName = string.Empty, Title = string.Empty, Type = string.Empty;

                List<ComplaintIssues> complaintIssuesFind = null;

                using (var proxy = new ComplainServiceProxy())
                {
                    complaintIssuesFind = proxy.GetRequredXMLInputs(new ComplaintIssuesGroupFind()
                    {
                        ComplaintIssuesGroup = new ComplaintIssuesGroup()
                        {
                            ProductID = viewModel.ComplaintIssuesID,
                            Reason1ID = viewModel.Reason1ID,
                            Reason2ID = viewModel.Reason2ID,
                            Reason3ID = viewModel.Reason3ID,
                            QueueID = viewModel.DispatchQID,
                        }
                    }).ToList();

                }
                //    var reason1 = new List<int>();
                //    if (complaintIssuesGrpFind.Count() > 0)
                //    {
                //        reason1 = proxy.ComplaintIssuesGroupGet(complaintIssuesGrpFind).Select(a => a.Title).ToList();
                //        reason1List = proxy.ComplaintIssuesGet(reason1).FirstOrDefault()!=null?
                //    }

                //   dispatchQList = proxy.ComplaintIssuesGet(complaintIssuesGrpFind).OrderBy(o => o.Name).ToList();
                //}

                if (complaintIssuesFind != null && complaintIssuesFind.Any())
                {
                    CaseComplexity = complaintIssuesFind.Where(a => a.Type == "CASEC").FirstOrDefault() != null ? complaintIssuesFind.Where(a => a.Type == "CASEC").FirstOrDefault().Name.ToString2() : string.Empty;
                    CasePriority = complaintIssuesFind.Where(a => a.Type == "CASEP").FirstOrDefault() != null ? complaintIssuesFind.Where(a => a.Type == "CASEP").FirstOrDefault().Name.ToString2() : string.Empty;
                    Direction = complaintIssuesFind.Where(a => a.Type == "DIR").FirstOrDefault() != null ? complaintIssuesFind.Where(a => a.Type == "DIR").FirstOrDefault().Name.ToString2() : string.Empty;
                    SysName = complaintIssuesFind.Where(a => a.Type == "SYSN").FirstOrDefault() != null ? complaintIssuesFind.Where(a => a.Type == "SYSN").FirstOrDefault().Name.ToString2() : string.Empty;
                    Title = complaintIssuesFind.Where(a => a.Type == "TTL").FirstOrDefault() != null ? complaintIssuesFind.Where(a => a.Type == "TTL").FirstOrDefault().Name.ToString2() : string.Empty;
                    Type = complaintIssuesFind.Where(a => a.Type == "INTRT").FirstOrDefault() != null ? complaintIssuesFind.Where(a => a.Type == "INTRT").FirstOrDefault().Name.ToString2() : string.Empty;
                }

                var complain = new Complain()
                {
                    AccountNo = viewModel.MSISDN,
                    CustomerName = viewModel.Name.Trim(),
                    MSISDN = viewModel.MSISDN,
                    Remark = viewModel.Comments.Trim(),
                    Address = viewModel.Address.Trim(),
                    IDNumber = viewModel.IDNumber,
                    IDType = viewModel.IDType,
                    TranxID = 182,
                    IntTranxID = 182,
                    LastAccessID = Util.SessionAccess.UserName,
                    //LastAccessID = "Developer",
                    CreateDT = DateTime.Now,
                    ComplaintGroupID = complaintIssuesGrp.FirstOrDefault()
                };

                ComplainServiceProxy.CreateComplain(complain);

                var resp = CreateCaseInCMSS(complain);
                if (resp.Success)
                {
                    complain.LastUpdateDT = DateTime.Now;
                    ComplainServiceProxy.UpdateComplain(complain);

                    return RedirectToAction("Success", new { caseID = complain.CMSSCaseID });
                }
                else
                {
                    ModelState.AddModelError("", resp.Message);
                }
            }
            catch (Exception ex)
            {
                //LogException(ex);

                LogExceptions(ex);
                ModelState.AddModelError("", "Exception occur: " + ex.Message);
            }

            return View(viewModel);
        }

        //[HttpPost]
        //public ActionResult GetReason1ByProduct(int productID)
        //{
        //    var reason1 = new List<ComplaintReason1>();
        //    using (var proxy = new ComplainServiceProxy())
        //    {
        //        var productReason1IDs = proxy.ProductReason1Find(new ProductReason1Find()
        //        {
        //            Active = true,
        //            ProductReason1 = new ProductReason1() { ComplaintProductID = productID }
        //        });
        //        var productReason1Resp = proxy.ProductReason1Get(productReason1IDs).ToList();
        //        var reason1IDs = productReason1Resp.Select(a => a.Reason1ID).ToList();

        //        if (reason1IDs.Count() > 0)
        //        {
        //            reason1 = proxy.ComplaintReason1Get(reason1IDs.ToList()).ToList();
        //        }
        //    }

        //    reason1.Insert(0, new ComplaintReason1() { ID = 0, Name = "Reason 1" });

        //    return Json(new SelectList(reason1, "ID", "Name"));
        //}

        //[HttpPost]
        //public ActionResult GetReason2ByReason1(int reason1ID)
        //{
        //    var reason2 = new List<ComplaintReason2>();
        //    using (var proxy = new ComplainServiceProxy())
        //    {
        //        var complaintReason12IDs = proxy.ComplaintReason12Find(new ComplaintReason12Find()
        //        {
        //            Active = true,
        //            ComplaintReason12 = new ComplaintReason12() { Reason1ID = reason1ID }
        //        });
        //        var complaintReason12Resp = proxy.ComplaintReason12Get(complaintReason12IDs).ToList();
        //        var reason2IDs = complaintReason12Resp.Select(a => a.Reason2ID).ToList();

        //        if (reason2IDs.Count() > 0)
        //        {
        //            reason2 = proxy.ComplaintReason2Get(reason2IDs.ToList()).ToList();
        //        }
        //    }

        //    reason2.Insert(0, new ComplaintReason2() { ID = 0, Name = "Reason 2" });

        //    return Json(new SelectList(reason2, "ID", "Name"));
        //}

        //[HttpPost]
        //public ActionResult GetReason3ByReason3(int reason2ID)
        //{
        //    var reason3 = new List<ComplaintReason3>();
        //    using (var proxy = new ComplainServiceProxy())
        //    {
        //        var complaintReason23IDs = proxy.ComplaintReason23Find(new ComplaintReason23Find()
        //        {
        //            Active = true,
        //            ComplaintReason23 = new ComplaintReason23() { Reason2ID = reason2ID }
        //        });
        //        var complaintReason23Resp = proxy.ComplaintReason23Get(complaintReason23IDs).ToList();
        //        var reason3IDs = complaintReason23Resp.Select(a => a.Reason3ID).ToList();

        //        if (reason3IDs.Count() > 0)
        //        {
        //            reason3 = proxy.ComplaintReason3Get(reason3IDs.ToList()).ToList();
        //        }
        //    }

        //    reason3.Insert(0, new ComplaintReason3() { ID = 0, Name = "Reason 3" });

        //    return Json(new SelectList(reason3, "ID", "Name"));
        //}

        private CMSS.CreateInteractionCaseResponse CreateCaseInCMSS(Complain complain)
        {
            var resp = new CMSS.CreateInteractionCaseResponse();

            var request = new CMSS.CreateInteractionCaseRequest
            {
                AccountExternalID = complain.AccountNo,
                MSISDN = complain.MSISDN,
                //TransactionID = complain.TranxID.ToString(),
            };

            if (complain.IntTranxID != 0)
            {
                request.TransactionID = complain.IntTranxID.ToString2();

                request.Notes = string.Format("Name: {0}\nTelephone (alt): {1}\nIntTranxID: {2}\nComments/Feedback: {3}",
                complain.CustomerName,
                complain.MSISDN,
                complain.IntTranxID.ToString(),
                complain.Remark);

                var response = CMSSServiceProxy.CreateInteractionCase(request);
                if (response.Success)
                {
                    complain.CMSSInteractionID = response.InteractionID;
                }
                else
                {
                    return response;
                }
            }

            request.TransactionID = complain.TranxID.ToString2();

            request.Notes = string.Format("Name: {0}\nTelephone (alt): {1}\nTranxID: {2}\nComments/Feedback: {3}", 
                complain.CustomerName, 
                complain.TelNo,
                complain.TranxID.ToString(),
                complain.Remark);

            var caseResponse = CMSSServiceProxy.CreateInteractionCase(request);
            if (caseResponse.Success)
            {
                complain.CMSSCaseID = caseResponse.CaseID;
            }
            else
            {
                return caseResponse;
            }

            return caseResponse;
        }

        [HttpGet]
        public ActionResult GetCmssListPage()
        {
            Online.Registration.Web.ComplainSvc.ComplaintGetResp objCmssGetResp = GetComplaintIssues(string.Empty, string.Empty);


            return View(objCmssGetResp);
        }

        private Online.Registration.Web.ComplainSvc.ComplaintGetResp GetComplaintIssues(string msisdn, string cmssCaseId)
        {
            TempData["SearchMsisdn"] = msisdn;
            TempData["SearchCmssCaseId"] = cmssCaseId;
            Online.Registration.Web.ComplainSvc.ComplaintGetResp objCmssGetResp = new Online.Registration.Web.ComplainSvc.ComplaintGetResp();
            using (var proxy = new ComplainServiceProxy())
            {
                objCmssGetResp = proxy.GetCmssList(new CMSSComplainGet()
                {
                    MSISDN = msisdn,
                    CMSSCaseID = cmssCaseId
                });
            }
            return objCmssGetResp;
        }

        [HttpPost]
        public ActionResult GetCmssListPage(FormCollection objFrmCol)
        {
            Online.Registration.Web.ComplainSvc.ComplaintGetResp objCmssGetResp = GetComplaintIssues(objFrmCol["txtMsisdn"].ToString2(), objFrmCol["txtCmssCaseId"].ToString2());


            return View(objCmssGetResp);
        }

        private void LogExceptions(Exception ex)
        {
            /* BEGIN  Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/
            string strErrorMsg = Util.LogException(ex);
            string actionName = this.ControllerContext.RouteData.Values["action"] != null ? this.ControllerContext.RouteData.Values["action"].ToString() : "";
            string controllerName = this.ControllerContext.RouteData.Values["controller"] != null ? this.ControllerContext.RouteData.Values["controller"].ToString() : "";
            Logger.Error("Controller: " + controllerName + "\n" + " Action:" + actionName + "\n" + " Error:" + strErrorMsg, ex);
            /* END Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/

        }
    }
}
