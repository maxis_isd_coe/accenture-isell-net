﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using log4net;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;
using Online.Registration.Web.Models;
using Online.Registration.Web.UserSvc;
using Online.Registration.Web.CommonEnum;
using System.Net;
using System.Net.Sockets;
using Online.Web.Helper;
using SNT.Utility.Helper;

namespace Online.Registration.Web.Controllers
{
    [CustomErrorHandlerAttr(View = "GenericError")]
    public class AccountController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AccountController));
       
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        public ActionResult GenericError()
        {
            if (Request.Url != null)
            {
                ILog Logger = LogManager.GetLogger(typeof(Online.Registration.Web.Controllers.AccountController));
                Logger.Error("404 :: " + Request.Url);
            }
            return View();
        }

        //GET: /Account/LogOn
        public ActionResult LogOn()
        {
            FormsAuthentication.SignOut();
            Util.SessionAccess = null;
            //**remove user name cookie**
            if (ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("CookieUser"))
            {
                HttpCookie cookieUser = ControllerContext.HttpContext.Request.Cookies["CookieUser"];
                if (cookieUser != null)
                {
                    cookieUser.Expires = DateTime.Now.AddDays(-1);
                    ControllerContext.HttpContext.Response.Cookies.Add(cookieUser);
                }
            }
            //**remove user name cookie** 
            Session.RemoveAll();
            Session.Abandon();
            Session.Clear();

            return View();
        }
        
        public ActionResult CreateCache()
        {
             
            bool bExceptionRaised = false;
            try
            {

                var stockCount = WebHelper.Instance.GetStockCounts(out bExceptionRaised);
                
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogStockExceptions(this.GetType(), ex);
            }
            if (bExceptionRaised)
                ViewBag.Caching = "Cache Creation failed";
            else
                ViewBag.Caching = "Cache Cretaed Successfully";
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var access = UserServiceProxy.GetAccess(model.UserName);
                    if (access == null)
                    {
                        ModelState.AddModelError("", "The user name does not exist.");
                        return View(model);
                    }
                    Session["LoggedInUserID"] = access.UserID;
                    access.User = UserServiceProxy.GetUser(access.UserID);

                    using (var userProxy = new UserServiceProxy())
                    {
                        var getData = userProxy.UserGroupAccessGet(userProxy.UserGroupAccessFind(new UserGrpAccessFind()
                        {
                            UserGrpAccess = new UserGrpAccess()
                            {
                                AccessID = access.ID
                            },
                            Active = true
                        })).ToList();

                        var currentGroupId = userProxy.UserGroupGet(getData.Select(a => a.UserGrpID));

                        Session["UserGroupId"] = currentGroupId.Select(a => a.ID).ToList();
                    }

                    Session["IsDealer"] = access.User.isDealer;

                    Util.SessionAccess = access;

                    using (var orgProxy = new OrganizationServiceProxy())
                    {
                        var org = orgProxy.OrganizationGet(new[] { access.User.OrgID }).Single();
                        access.User.Org = new Organization
                        {
                            Active = org.Active,
                            AddrLine1 = org.AddrLine1,
                            AddrLine2 = org.AddrLine2,
                            Code = org.Code,
                            DealerCode = org.DealerCode,
                            ContactNo = org.ContactNo,
                            CountryID = org.CountryID,
                            CreateDT = org.CreateDT,
                            Description = org.Description,
                            EmailAddr = org.EmailAddr,
                            FaxNo = org.FaxNo,
                            ID = org.ID,
                            IsHQ = org.IsHQ,
                            KenanRegCode = org.KenanRegCode,
                            KenanServiceProviderID = org.KenanServiceProviderID,
                            LastAccessID = org.LastAccessID,
                            LastUpdateDT = org.LastUpdateDT,
                            MobileNo = org.MobileNo,
                            Name = org.Name,
                            OrgTypeID = org.OrgTypeID,
                            PersonInCharge = org.PersonInCharge,
                            Postcode = org.Postcode,
                            SAPBranchCode = org.SAPBranchCode,
                            StateID = org.StateID,
                            TownCity = org.TownCity,
                            WSDLUrl = org.WSDLUrl,
                            OrganisationId = org.OrganisationId,
                            Inventry_OutOfStockLower = org.Inventry_OutOfStockLower,
                            Inventry_OutOfStockHigher = org.Inventry_OutOfStockHigher,
                            Inventry_LimitedStockLower = org.Inventry_LimitedStockLower,
                            Inventry_LimitedStockHigher = org.Inventry_LimitedStockHigher,
                            Inventry_AvailableStockLower = org.Inventry_AvailableStockLower,
                            Inventry_AvailableStockHigher = org.Inventry_AvailableStockHigher,
                            SalesChannelID = org.SalesChannelID,
							// added by Gerry
							SMARTTeamID = org.SMARTTeamID
                        };

                        Util.SessionOrgTypeCode = orgProxy.OrgTypeGet(new[] { access.User.Org.OrgTypeID }).Single().Code;

                    }

                    UserTransactionLogResp responseFromBusinessValidation = null;
                    using (var proxy = new UserServiceProxy())
                    {
                        responseFromBusinessValidation = new UserTransactionLogResp();

                        responseFromBusinessValidation = proxy.UserTransactionLogCreate(new UserTransactionLogReq
                        {
                            UserInfoVal = new UserTransactionLog
                            {
                                Username = Util.SessionAccess.UserName,
                                Action = "Log On",
                            }
                        });

                        var ipAddress = Dns.GetHostAddresses(Dns.GetHostName()).Where(address => address.AddressFamily == AddressFamily.InterNetwork).First();
                        int logID = 0;
                        logID = proxy.SaveUserAuditLog(Util.SessionAccess.UserName, Session.SessionID, ipAddress.ToString(), 0);
                        Session["CurrentUserLogId"] = logID;
                    }

                    if (access.IsLockedOut)
                    {
                        ModelState.AddModelError("", "This account has been Locked out.");
                        return View(model);
                    }
                    if (!access.Active)
                    {
                        ModelState.AddModelError("", "This account has been Suspended.");
                        return View(model);
                    }

                    Logger.Info(string.Format("LogOn SessionOrgType({0})", Util.SessionOrgTypeCode));

                    var orgTypes = Properties.Settings.Default.LDAP_OrgType.Split(',');

                    if (orgTypes.Contains(Util.SessionOrgTypeCode))
                    {
                        if (!Util.SessionAccess.LDAPLogin)
                        {
                            string sha1Pswd = Util.GetMD5Hash(model.Password);

                            if (!sha1Pswd.Equals(access.Password))
                            {
                                ModelState.AddModelError("", "The user name or password provided is incorrect.");
                                return View(model);
                            }
                        }
                        else
                        {
                            if (!Properties.Settings.Default.DevelopmentModeOn)
                            {
                                using (var ldapProxy = new LDAPServiceProxy())
                                {
                                    var resp = ldapProxy.ValidateCredential(new LDAPSvc.LDAPAccessRequest
                                    {
                                        Username = model.UserName,
                                        Password = model.Password
                                    }, Session["IsDealer"].ToString());

                                    if (!resp.IsCredentialValidated)
                                    {
                                        ModelState.AddModelError("", "The user name or password provided is incorrect.");
                                        return View(model);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        string sha1Pswd = Util.GetMD5Hash(model.Password);

                        if (!sha1Pswd.Equals(access.Password))
                        {
                            ModelState.AddModelError("", "The user name or password provided is incorrect.");
                            return View(model);
                        }
                    }

                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);

                    if (access.FailedPasswordAttemptCount > 0)
                        UserServiceProxy.ResetAccessFailedPasswordAttempt(access.ID);

                    Session["LoginTime"] = Util.GetLoginDateTime();
                    HttpCookie cookieUser = new HttpCookie("CookieUser");
                    cookieUser.Value = model.UserName;
                    cookieUser.Expires = DateTime.Now.AddDays(Session.Timeout);
                    ControllerContext.HttpContext.Response.Cookies.Add(cookieUser);
                    Session[SessionKey.PPID.ToString()] = "";

                    GetAPIStatusMessagesResp ApiMessages = null;
                    using (var proxy = new UserServiceProxy())
                    {
                        var resp = proxy.GetAPIStatusMessages(new GetAPIStatusMessagesReq
                        {
                            tblAPIStatusMessagesval = new tblAPIStatusMessages
                            {
                                APIName = "breService",
                                MethodName = "checkBusinessRules"
                            }
                        });
                        ApiMessages = resp;
                    }

                    Session["ApiMessages"] = ApiMessages;
                    return RedirectToAction("IndexNew", "Home");
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(GetType(), ex);
                ModelState.AddModelError("", System.Configuration.ConfigurationManager.AppSettings["UserConnStatus"]);

                return View(model);
            }
            return View(model);
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            try
            {
                #region VLT ADDED CODE by Rajeswari on Feb 25th for logging user information

                UserTransactionLogResp responseFromBusinessValidation = null;

                using (var proxy = new UserServiceProxy())
                {
                    responseFromBusinessValidation = new UserTransactionLogResp();

                    responseFromBusinessValidation = proxy.UserTransactionLogCreate(new UserTransactionLogReq
                    {
                        UserInfoVal = new UserTransactionLog
                        {
                            Username = Util.SessionAccess != null ? Util.SessionAccess.UserName : "",
                            Action = "Logged Out"
                        }
                    });
                }

                #endregion VLT ADDED CODE

                FormsAuthentication.SignOut();
                Util.SessionAccess = null;
                //Session["Access"] = null;

                //**remove user name cookie**
                if (ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("CookieUser"))
                {
                    HttpCookie cookieUser = ControllerContext.HttpContext.Request.Cookies["CookieUser"];
                    if (cookieUser != null)
                    {
                        cookieUser.Expires = DateTime.Now.AddDays(-1);
                        ControllerContext.HttpContext.Response.Cookies.Add(cookieUser);
                    }
                }
                //**remove user name cookie**   
                Session.Abandon();
                return RedirectToAction("LogOut", new { type = "loggedout" });
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(GetType(), ex);
                Session.Abandon();
                return RedirectToAction("LogOut", new { type = "loggedout" });
            }
        }

        // GET: /Account/Register
        public ActionResult Register()
        {
            return View();
        }

        // POST: /Account/Register
        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", ErrorCodeToString(createStatus));
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePassword
        //[Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        //[Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    changePasswordSucceeded = ChangePassword(model.Username, model.OldPassword, model.NewPassword);
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogExceptions(GetType(), ex);
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
            }

            return View(model);
        }
        private bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            Logger.InfoFormat("ChangePassword({0}) called!", username);
            bool status = false;
            try
            {
                using (var proxy = new UserServiceProxy())
                {
                    var access = proxy.GetAccessByUsername(username, GetMD5Hash(oldPassword));
                    if (access == null) { status = false; }
                    else
                    {
                        access.Password = GetMD5Hash(newPassword);
                        access.ChangePassword = false;
                        status = proxy.AccessPasswordUpdate(access);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ChangePassword({0}):{1}!", username, ex.Message), ex);
                WebHelper.Instance.LogExceptions(GetType(), ex);
                return status;
            }
            Logger.InfoFormat("ChangePassword({0}):{1} returned!", username, status);
            return status;
        }
        public static string GetMD5Hash(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        //
        // GET: /Account/ChangePasswordSuccess
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePasswordSuccess(FormCollection collection)
        {
            return RedirectToAction("LogOn");
        }
        public ActionResult LogOut(string type = "")
        {
            if (type == "loggedout")
            {
                ViewData["logged"] = "Logged out successfully.";
            }
            using (var proxy = new UserServiceProxy())
            {
                proxy.SaveUserAuditLog("", Session.SessionID, "", 1);
            }
            Session.Clear();
            Session.Abandon();

            return View();
        }
        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return ErrorMsg.Account.DuplicateUserName;

                case MembershipCreateStatus.DuplicateEmail:
                    return ErrorMsg.Account.DuplicateEmail;

                case MembershipCreateStatus.InvalidPassword:
                    return ErrorMsg.Account.InvalidPassword;

                case MembershipCreateStatus.InvalidEmail:
                    return ErrorMsg.Account.InvalidEmail;

                case MembershipCreateStatus.InvalidAnswer:
                    return ErrorMsg.Account.InvalidAnswer;

                case MembershipCreateStatus.InvalidQuestion:
                    return ErrorMsg.Account.InvalidQuestion;

                case MembershipCreateStatus.InvalidUserName:
                    return ErrorMsg.Account.InvalidUserName;

                case MembershipCreateStatus.ProviderError:
                    return ErrorMsg.Account.ProviderError;

                case MembershipCreateStatus.UserRejected:
                    return ErrorMsg.Account.UserRejected;

                default:
                    return ErrorMsg.Account.DefaultMembershipCreateStatus;
            }
        }
        #endregion

    }
}