﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Text;
using System.Web.Routing;

using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Properties;
using SNT.Utility;
using log4net;
using TCMSCLibrary;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.ContentRepository;

namespace Online.Registration.Web.Controllers
{
    //[HandleError(View = "GenericError")]
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class HomeRegistrationController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(HomeRegistrationController));
        protected override void Initialize(RequestContext requestContext)
        {
            Util.CheckSessionAccess(requestContext);
            base.Initialize(requestContext);
        }

        //UNUSED METHODS
        //private void LogException(Exception ex)
        //{
        //    Logger.DebugFormat("Exception: {0}", ex.ToString());
        //}

        //UNUSED METHODS
        //private RedirectToRouteResult RedirectException(Exception ex)
        //{
        //    Util.SetSessionErrMsg(ex);
        //    return RedirectToAction("Error", "Home");
        //}

        // Dictionary
        // Session["RegHomeReg_TabIndex"] - Action No
        // Session["RegHomeReg_MobileNo"] - selected MobileNo
        // Session["RegHomeReg_OrderSummary"] - OrderSummaryVM
        // Session["RegHomeReg_custDetailsVM"] - custDetailsVM
        // Session["RegHomeReg_PhoneVM"] - PhoneVM
        // Session["RegHomeReg_PkgPgmBdlPkgCompID"] - selected PgmBdlPkgComponent.ID (BP)
        // Session["RegHomeReg_ProgramMinAge"] - minimum age for selected program
        // Session["RegHomeReg_SelectedModelID"] - selected ModelID
        // Session["RegHomeReg_SelectedModelImageID"] - selected ModelImage.ID
        // Session["RegHomeStepNo"] - Registration Step No
        // Session["RegHomeReg_Addresses"] - Installation address
        // Session["RegHomeReg_InstallationDT"] - selected InstallationDT
        // Session["RegHomeReg_OptPkgPgmBdlPkgCompID"] - selected optional PgmBdlPkgComponent.ID (BP)
        // Session["RegHomeReg_OptProgramMinAge"] - minimum age for selected optional program
        // Session["RegHomeReg_IsBlacklisted"] - indicate wheter IC is blacklisted
        // Session["RegHomeReg_ResultMsg"] - result message returned from blacklist checking
        // Session["RegHomeReg_ICNo"] - IC no entered for blacklist checking
        // Session["RegHomeReg_ExternalBlacklist"] - External blacklist
        // Session["RegHomeReg_InternalBlacklist"] - Internal blacklist
        // Session["RegHomeReg_SkipDuplicate"] - indicate the registration is skipped for duplicate reg
        // Session["RegHomeReg_IDCardType"] - ID card type selected

        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult CustomerDetails()
        {
            SetRegHomeStepNo(HomeRegistrationSteps.CustDetails);
            Session["RegHomeReg_OrderSummary"] = ConstructOrderSummary();

            var icNo = (Session["RegHomeReg_ICNo"] != null) ? (string)Session["RegHomeReg_ICNo"] : null;
            var idCardTypeID = Session["RegHomeReg_IDCardType"] != null ? Session["RegHomeReg_IDCardType"] : null;
            //var isBlacklisted = (bool)Session["RegHomeReg_IsBlacklisted"];
            var custDetailsVM = new CustomerDetailsVM();
            if ((CustomerDetailsVM)Session["RegHomeReg_custDetailsVM"] != null)
            {
                custDetailsVM = (CustomerDetailsVM)Session["RegHomeReg_custDetailsVM"];
            }

            var addresses = (List<Address>)Session["RegHomeReg_Addresses"];
            var address = addresses.Where(a => a.AddressTypeID == Util.GetIDByCode(RefType.AddressType, Settings.Default.AddressType_Installation)).SingleOrDefault();

            InstallationAddress instAddr = new InstallationAddress();

            instAddr = new InstallationAddress()
                {
                    AddressTypeID = address.AddressTypeID,
                    UnitNo = address.UnitNo,
                    Street = address.Street,
                    BuildingNo = address.BuildingNo,
                    Line1 = address.Line1,
                    Line2 = address.Line2,
                    Town = address.Town,
                    Postcode = address.Postcode,
                    StateID = address.StateID,
                };

            custDetailsVM.AddressVM.InstallationAddress = instAddr;
            custDetailsVM.Customer.IDCardNo = icNo;
            custDetailsVM.Customer.IDCardTypeID = idCardTypeID.ToInt();
            custDetailsVM.RegTypeID = Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_HomePersonal);
            //custDetailsVM.AddressVM.Registration.IsBlacklisted = isBlacklisted;

            DateTime? installDT = null;
            if (Session["RegHomeReg_InstallationDT"] != null)
            {
                installDT = (DateTime)Session["RegHomeReg_InstallationDT"];
            }
            custDetailsVM.AddressVM.Registration.InstallationDT = installDT;

            custDetailsVM.AddressVM.Registration.SalesPerson = Util.SessionAccess == null ? "" : Util.SessionAccess.UserName;

            return View(custDetailsVM);
        }
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult SelectPlan(bool? fromMenu)
        {
            //if (fromMenu.HasValue)
            //{
            //    if (fromMenu.Value)
            //    {
            //        ClearRegistrationSession();
            //        //Session["RegHomeReg_Type"] = (int)MobileRegType.PlanOnly;
            //    }
            //}
            SetRegHomeStepNo(HomeRegistrationSteps.Plan);

            var custDetailsVM = (CustomerDetailsVM)Session["RegHomeReg_custDetailsVM"];
            var aa = custDetailsVM.Customer.IDCardNo;
            var bb = custDetailsVM.Customer.IDCardTypeID;
            Session["RegHomeReg_OrderSummary"] = ConstructOrderSummary();
            Session["RegHomeReg_custDetailsVM"] = custDetailsVM;
            var packageVM = GetAvailablePackages(true);

            return View(packageVM);
        }
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult SelectWBBPlan()
        {
            //if (fromMenu.HasValue)
            //{
            //    if (fromMenu.Value)
            //    {
            //        ClearRegistrationSession();
            //        Session["RegHomeReg_Type"] = (int)MobileRegType.PlanOnly;
            //    }
            //}
            SetRegHomeStepNo(HomeRegistrationSteps.WBBPlan);

            Session["RegHomeReg_OrderSummary"] = ConstructOrderSummary();
            Session["RegHomeReg_custDetailsVM"] = (CustomerDetailsVM)Session["RegHomeReg_custDetailsVM"]; ;
            var packageVM = GetAvailablePackages(false);

            return View(packageVM);
        }
        //[Authorize(Roles = "HREG_W,HREG_C")]
        //public ActionResult SelectMobileNo()
        //{
        //    SetRegHomeStepNo(HomeRegistrationSteps.NumberSelection);
        //    Session["RegHomeReg_OrderSummary"] = ConstructOrderSummary();
        //    var aa = Session["RegHomeReg_OptPkgPgmBdlPkgCompID"];

        //    var homeMobileNoVM = new HomeMobileNoVM()
        //    {
        //        NoOfDesireMobileNo = Properties.Settings.Default.NoOfDesireMobileNo
        //    };

        //    if (((List<string>)Session["RegHomeReg_MobileNo"]) != null)
        //    {
        //        homeMobileNoVM.SelectedMobileNumber = ((List<string>)Session["RegHomeReg_MobileNo"]);
        //    }

        //    return View(homeMobileNoVM);
        //}
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult BlacklistChecking()
        {
            Session["RegHomeReg_FromSearch"] = null;
            Session["RegHomeReg_ResultMessage"] = null;
            ClearRegistrationSession();

            //if (type == (int)MobileRegType.DevicePlan)
            //    Session["RegHomeReg_Type"] = (int)MobileRegType.DevicePlan;
            //else if (type == (int)MobileRegType.PlanOnly)
            //    Session["RegHomeReg_Type"] = (int)MobileRegType.PlanOnly;

            return View(new CustomerDetailsVM());
        }
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult BlacklistResult(bool isBlacklisted, bool isDuplicate)
        {
            ViewBag.isBlacklisted = isBlacklisted;
            ViewBag.isDuplicated = isDuplicate;

            var resultMsg = Session["RegHomeReg_ResultMessage"];

            return View();
        }
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult InstallationAddress()
        {
            var ss = (bool)Session["RegHomeReg_SkipDuplicate"];
            SetRegHomeStepNo(HomeRegistrationSteps.InstallationAddr);
            Session["RegHomeReg_OrderSummary"] = ConstructOrderSummary();

            var custDetailsVM = new CustomerDetailsVM();
            var icNo = (Session["RegHomeReg_ICNo"] != null) ? (string)Session["RegHomeReg_ICNo"] : null;


            if ((CustomerDetailsVM)Session["RegHomeReg_custDetailsVM"] != null)
            {
                custDetailsVM = (CustomerDetailsVM)Session["RegHomeReg_custDetailsVM"];
                custDetailsVM.AddressVM.Address = custDetailsVM.AddressVM.InstallationAddress;
                //custDetailsVM.Customer.IDCardNo = icNo;

                return View(custDetailsVM.AddressVM);
            }

            return View(new AddressVM());
        }
        private static string ConstructInstAddrLine1(Address address)
        {
            string line1 = string.Empty;
            if (address != null)
            {
                if (!string.IsNullOrEmpty(address.UnitNo))
                    line1 += address.UnitNo + ", ";
                if (!string.IsNullOrEmpty(address.BuildingNo))
                {
                    line1 += address.BuildingNo + ", ";
                }
            }
            return line1;
        }
        [Authorize(Roles = "HREG_W,HREG_C,MREG_U")]
        public ActionResult HomeRegSummary(int? regID)
        {
            var custDetailsVM = (CustomerDetailsVM)Session["RegHomeReg_custDetailsVM"];
            var homeOrderSummary = new HomeOrderSummaryVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var pbpcIDs = new List<int>();
            var regStatus = new List<int>();
            var isSearch = Session["RegHomeReg_FromSearch"] == null ? false : (bool)Session["RegHomeReg_FromSearch"];
            //var orderSummaryVM = (HomeOrderSummaryVM)Session["RegHomeReg_OrderSummary"];


            if (regID.ToInt() > 0)
            {
                ClearRegistrationSession();
                custDetailsVM = new CustomerDetailsVM()
                {
                    RegID = regID.Value.ToInt(),
                };

                Session["RegHomeReg_PhoneVM"] = null;

                using (var proxy = new RegistrationServiceProxy())
                {
                    var reg = proxy.RegistrationGet(regID.Value);
                    //custDetailsVM.QueueNo = reg.QueueNo;
                    custDetailsVM.BiometricVerify = reg.BiometricVerify;
                    custDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
                    custDetailsVM.BlacklistInternal = reg.InternalBlacklisted;

                    // Registration Status
                    var regStatusID = proxy.RegStatusFind(new RegStatusFind()
                    {
                        RegStatus = new RegStatus()
                        {
                            RegID = reg.ID
                        },
                        Active = true
                    }).SingleOrDefault();

                    custDetailsVM.StatusID = proxy.RegStatusGet(new int[] { regStatusID }).SingleOrDefault().StatusID;
                    if (custDetailsVM.StatusID == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed))
                    {
                        custDetailsVM.IsRegClosed = true;
                    }

                    custDetailsVM.AddressVM.Registration.SalesPerson = reg.SalesPerson;
                    custDetailsVM.AddressVM.Registration.RFSalesDT = reg.RFSalesDT;

                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    //mobileNos.Add(reg.MSISDN2);
                    Session["RegHomeReg_MobileNo"] = mobileNos;

                    // Customer
                    custDetailsVM.Customer = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    // Address
                    var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                    {
                        Active = true,
                        Address = new Address()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();
                    custDetailsVM.AddressVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

                    // Personal Details
                    //if (isSearch)
                    //{
                    var billingAddr = Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Billing);
                    var installationAddr = Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Installation);
                    var permanentAddr = Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Residential);

                    foreach (var addr in custDetailsVM.AddressVM.Addresses)
                    {
                        if (addr.AddressTypeID == billingAddr)
                            custDetailsVM.AddressVM.BillingAddress = ContructBillingAddress(addr);
                        if (addr.AddressTypeID == installationAddr)
                        {
                            custDetailsVM.AddressVM.InstallationAddress = ContructInstallationAddress(addr);
                            custDetailsVM.AddressVM.InstallationFullAddress = ContructInstallationFullAddress(addr);
                        }
                        if (addr.AddressTypeID == permanentAddr)
                            custDetailsVM.AddressVM.PermanentAddress = ContructPermanentAddress(addr);

                    }
                    //}

                    Session["RegHomeReg_custDetailsVM"] = custDetailsVM;

                    // RegPgmBdlPkgComponent
                    var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                    {
                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();
                    pbpcIDs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Reg status list
                    if (isSearch == true)
                    {
                        var regStatusIDs = proxy.RegStatusFind(new RegStatusFind()
                        {
                            RegStatus = new RegStatus()
                            {
                                RegID = regID.ToInt()
                            }
                        }).ToList();

                        var regStatuses = proxy.RegStatusGet(regStatusIDs).ToList();
                        var regStatusList = new List<RegStatusResult>();
                        //var reasonCodeIDs = regStatuses.Where(a => a.ReasonCodeID.HasValue && a.ReasonCodeID.Value != 0).Select(a => a.ReasonCodeID.Value).Distinct().ToList();
                        //var reasonCodes = new List<StatusReason>();

                        //if (reasonCodeIDs.Count() > 0)
                        //{
                        //    using (var proxy1 = new ConfigServiceProxy())
                        //    {
                        //        reasonCodes = proxy1.StatusReasonGet(reasonCodeIDs).ToList();
                        //    }
                        //}

                        //foreach (var status in regStatuses)
                        //{
                        //    regStatusList.Add(new RegStatusResult()
                        //    {
                        //        RegStatusID = status.StatusID,
                        //        ReasonCode = reasonCodeIDs.Count > 0 ? (status.ReasonCodeID.HasValue ? reasonCodes.Where(a => a.ID == status.ReasonCodeID.Value).SingleOrDefault().Name : "") : "",
                        //        RegID = status.RegID,
                        //        RegStatus = Util.GetNameByID(RefType.Status, status.StatusID),
                        //        Remark = status.Remark,
                        //        StatusLogDT = status.StatusLogDT,
                        //        LastAccessID = status.LastAccessID,
                        //        CreateDT = status.CreateDT
                        //    });
                        //}
                        regStatusList = proxy.RegStatusLogGet(regStatusIDs).ToList();

                        custDetailsVM.RegStatusResult = regStatusList;
                        // Statuses for Reason code
                        custDetailsVM.StatusReasonIDs = ConfigServiceProxy.FindStatusReasonCodeIDs().ToList();
                    }
                }

                using (var proxy = new CatalogServiceProxy())
                {
                    if (regMdlGrpModel.ID > 0)
                        Session["RegHomeReg_SelectedModelID"] = proxy.ModelGroupModelGet(new int[] { regMdlGrpModel.ModelGroupModelID.ToInt() }).SingleOrDefault().ModelID;

                    var pgmBdlPkgComps = MasterDataCache.Instance.FilterComponents(pbpcIDs).ToList();

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    Session["RegHomeReg_PkgPgmBdlPkgCompID"] = pgmBdlPkgComps.Where(a => a.LinkType == bpCode && a.IsMandatory).SingleOrDefault().ID;

                    var wbb = pgmBdlPkgComps.Where(a => a.LinkType == bpCode && a.IsMandatory == false).SingleOrDefault();
                    if (wbb != null)
                        Session["RegHomeReg_OptPkgPgmBdlPkgCompID"] = wbb.ID;
                }

                Session["RegHomeReg_OrderSummary"] = ConstructOrderSummary();
            }

            return View(custDetailsVM);
        }
        [Authorize(Roles = "HREG_W,HREG_C,MREG_U")]
        public ActionResult PrintHomeRF(int id)
        {
            var regFormVM = new RegistrationFormVM();
            var mdlGrpModelIDs = new List<int>();
            var pbpcIDs = new List<int>();

            using (var proxy = new RegistrationServiceProxy())
            {
                // Registration
                regFormVM.Registration = proxy.RegistrationGet(id);

                // Customer
                regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

                // Address
                var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                {
                    Active = true,
                    Address = new Address()
                    {
                        RegID = id
                    }
                }).ToList();
                regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

                // RegMdlGrpModels
                var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                {
                    RegMdlGrpModel = new RegMdlGrpModel()
                    {
                        RegID = id
                    }
                }).ToList();
                if (regMdlGrpModelIDs.Count() > 0)
                    mdlGrpModelIDs = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).Distinct().Where(a => a.ModelGroupModelID != null).Select(a => a.ModelGroupModelID.Value).ToList();

                // RegPgmBdlPkgComponent
                var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                {
                    RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                    {
                        RegID = id
                    }
                }).ToList();
                pbpcIDs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();
            }

            using (var proxy = new CatalogServiceProxy())
            {
                // ModelGroupModel
                if (mdlGrpModelIDs.Count() > 0)
                {
                    regFormVM.ModelGroupModels = proxy.ModelGroupModelGet(mdlGrpModelIDs).ToList();
                }
                // BundlePackage, PackageComponents
                var pgmBdlPkgComps = MasterDataCache.Instance.FilterComponents(pbpcIDs).Distinct().ToList();
                var bpCode = Properties.Settings.Default.Bundle_Package;
                var pcCode = Properties.Settings.Default.Package_Component;
                regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode).ToList();
                regFormVM.PackageComponents = pgmBdlPkgComps.Where(a => a.LinkType == pcCode).ToList();
            }

            return View(regFormVM);
        }
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult HomeRegSuccess(int regID)
        {
            return View(regID);
        }
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult HomeRegClosed(int regID)
        {
            return View(regID);
        }
        [Authorize(Roles = "HREG_W,HREG_C,MREG_U")]
        public ActionResult SearchRegistration()
        {
            var searchRegVM = new SearchHomeRegistrationVM();

            using (var proxy = new RegistrationServiceProxy())
            {
                searchRegVM.RegistrationSearchResults = proxy.RegistrationSearch(new RegistrationSearchCriteria()
                {
                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_HomeSubmitted),
                    UserID = Util.SessionAccess == null ? 0 : Util.SessionAccess.UserID,
                    PageSize = Properties.Settings.Default.PageSize
                });
            }

            Session["RegHomeReg_FromSearch"] = true;

            return View(searchRegVM);
        }
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult CoverageCheck()
        {
            return View();
        }

        private BillingAddress ContructBillingAddress(Address addr)
        {
            return new BillingAddress()
            {
                Line1 = addr.Line1,
                Line2 = addr.Line2,
                Town = addr.Town,
                Postcode = addr.Postcode,
                StateID = addr.StateID,
                Active = addr.Active
            };
        }
        private InstallationAddress ContructInstallationAddress(Address addr)
        {
            return new InstallationAddress()
            {
                UnitNo = addr.UnitNo,
                Street = addr.Street,
                BuildingNo = addr.BuildingNo,
                Line1 = ConstructInstAddrLine1(addr),
                Line2 = addr.Street,
                Town = addr.Town,
                Postcode = addr.Postcode,
                StateID = addr.StateID,
                Active = addr.Active
            };
        }
        private PermanentAddress ContructPermanentAddress(Address addr)
        {
            return new PermanentAddress()
            {
                Line1 = addr.Line1,
                Line2 = addr.Line2,
                Town = addr.Town,
                Postcode = addr.Postcode,
                StateID = addr.StateID,
                Active = addr.Active
            };
        }
        private string ContructInstallationFullAddress(Address addr)
        {
            var fullAddress = string.Empty;

            if (!string.IsNullOrEmpty(addr.UnitNo))
                fullAddress += addr.UnitNo + ", ";
            if (!string.IsNullOrEmpty(addr.BuildingNo))
                fullAddress += addr.BuildingNo + ", ";
            if (!string.IsNullOrEmpty(addr.Street))
                fullAddress += addr.Street + ", ";
            if (!string.IsNullOrEmpty(addr.Town))
                fullAddress += addr.Town + ", ";
            if (!string.IsNullOrEmpty(addr.Postcode))
                fullAddress += addr.Postcode + ", ";
            if (addr.StateID != 0)
                fullAddress += Util.GetNameByID(RefType.State, addr.StateID);

            return fullAddress;
        }

        [HttpPost]
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult CustomerDetails(CustomerDetailsVM custDetailsVM)
        {
            custDetailsVM.AddressVM.BillingAddress.AddressTypeID = Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Billing);
            custDetailsVM.AddressVM.PermanentAddress.AddressTypeID = Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Residential);

            if (custDetailsVM.AddressVM.BillingAddress.StateID == 0)
                custDetailsVM.AddressVM.BillingAddress.StateID = custDetailsVM.AddressVM.BillingAddress.AlternateStateID;

            if (custDetailsVM.AddressVM.PermanentAddress.StateID == 0)
                custDetailsVM.AddressVM.PermanentAddress.StateID = custDetailsVM.AddressVM.PermanentAddress.AlternateStateID;

            Session["RegHomeReg_custDetailsVM"] = custDetailsVM;

            if (custDetailsVM.TabNumber == (int)HomeRegistrationSteps.Plan)
            {
                Session["RegHomeReg_TabIndex"] = custDetailsVM.TabNumber;

                if (custDetailsVM.Customer.PayModeID == (int)PAYMENTMODE.AutoBilling)
                {
                    if (!ValidateCardDetails(custDetailsVM.Customer))
                    {
                        return View(custDetailsVM);
                    }
                }

                //if (custDetailsVM.AddressVM.Registration.RFSalesDT.Date > DateTime.Now.Date)
                //{
                //    ModelState.AddModelError(string.Empty, "Customer Signed Date must not later than current date.");
                //    return View(custDetailsVM);
                //}
            }

            return RedirectToAction(GetRegActionStep(custDetailsVM.TabNumber));
        }
        [HttpPost]
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult SelectPlan(HomePackageVM homePackageVM)
        {
            Session["RegHomeReg_TabIndex"] = homePackageVM.TabNumber;

            //if (packageVM.TabNumber == (int)HomeRegistrationSteps.Vas)
            //{
            // selected different Plan
            if (Session["RegHomeReg_PkgPgmBdlPkgCompID"].ToInt() != homePackageVM.SelectedPgmBdlPkgCompID)
            {
                Session["RegHomeReg_PkgPgmBdlPkgCompID"] = homePackageVM.SelectedPgmBdlPkgCompID;

                // Retrieve Plan Min Age
                using (var proxy = new CatalogServiceProxy())
                {
                    var bundleID = MasterDataCache.Instance.FilterComponents(new int[] { homePackageVM.SelectedPgmBdlPkgCompID }).SingleOrDefault().ParentID;
                    var programID = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            ChildID = bundleID,
                            LinkType = Properties.Settings.Default.Program_Bundle
                        },
                        Active = true
                    })).SingleOrDefault().ParentID;

                    var minAge = proxy.ProgramGet(new int[] { programID }).SingleOrDefault().MinAge;
                    Session["RegHomeReg_ProgramMinAge"] = minAge;
                }

                // reset session
                //Session["RegHomeReg_VasIDs"] = null;
                //Session["RegHomeReg_VasNames"] = null;
            }
            //}

            return RedirectToAction(GetRegActionStep(homePackageVM.TabNumber));
        }
        [HttpPost]
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult SelectWBBPlan(HomePackageVM homePackageVM)
        {
            Session["RegHomeReg_TabIndex"] = homePackageVM.TabNumber;

            //if (packageVM.TabNumber == (int)HomeRegistrationSteps.Vas)
            //{
            // selected different Plan
            if (Session["RegHomeReg_OptPkgPgmBdlPkgCompID"].ToInt() != homePackageVM.SelectedOptPgmBdlPkgCompID)
            {
                Session["RegHomeReg_OptPkgPgmBdlPkgCompID"] = homePackageVM.SelectedOptPgmBdlPkgCompID;

                // Retrieve Plan Min Age
                using (var proxy = new CatalogServiceProxy())
                {
                    var bundleID = MasterDataCache.Instance.FilterComponents(new int[] { homePackageVM.SelectedOptPgmBdlPkgCompID }).SingleOrDefault().ParentID;
                    var programID = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            ChildID = bundleID,
                            LinkType = Properties.Settings.Default.Program_Bundle
                        },
                        Active = true
                    })).SingleOrDefault().ParentID;

                    var minAge = proxy.ProgramGet(new int[] { programID }).SingleOrDefault().MinAge;
                    Session["RegHomeReg_OptProgramMinAge"] = minAge;
                }

                // reset session
                //Session["RegHomeReg_VasIDs"] = null;
                //Session["RegHomeReg_VasNames"] = null;
            }
            //}

            return RedirectToAction(GetRegActionStep(homePackageVM.TabNumber));
        }
        //[HttpPost]
        //public ActionResult SelectMobileNo(FormCollection collection, HomeMobileNoVM homeMobileNoVM)
        //{
        //    try
        //    {
        //        Session["RegHomeReg_TabIndex"] = homeMobileNoVM.TabNumber;
        //        var orderSummaryVM = (HomeOrderSummaryVM)Session["RegHomeReg_OrderSummary"];

        //        var mobileNos = new List<string>();
        //        for (int i = 0; i < Properties.Settings.Default.NoOfDesireMobileNo; i++)
        //        {
        //            //if (collection["txtMobileNo_" + i].ToString2() != "")
        //                mobileNos.Add(collection["txtMobileNo_" + i].ToString2());
        //        }
        //        if ((int)HomeRegistrationSteps.Summary == homeMobileNoVM.TabNumber)
        //        {
        //            Session["RegHomeReg_MobileNo"] = mobileNos;
        //        }
        //        orderSummaryVM.MobileNumbers = mobileNos;
        //        Session["RegHomeReg_OrderSummary"] = orderSummaryVM;
        //    }
        //    catch (Exception ex)
        //    {
        //        LogException(ex);
        //        return RedirectException(ex);
        //    }
        //    return RedirectToAction(GetRegActionStep(homeMobileNoVM.TabNumber));
        //}
        [HttpPost]
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult BlacklistChecking(string idCardNo, string idCardTypeID)
        {
            var isBlacklisted = false;
            var isDuplicate = false;
            //var idCardNo = custDetailsVM.Customer.IDCardNo;
            //Session["RegHomeReg_ICNo"] = custDetailsVM.Customer.IDCardNo;
            Session["RegHomeReg_ICNo"] = idCardNo;
            Session["RegHomeReg_IDCardType"] = idCardTypeID;

            try
            {
                var idCardType = new IDCardType();
                using (var proxy = new RegistrationServiceProxy())
                {
                    idCardType = proxy.IDCardTypeGet(idCardTypeID.ToInt());
                }

                using (var proxy = new KenanServiceProxy())
                {
                    var resp = proxy.BusinessRuleCheck(new BusinessRuleRequest()
                    {
                        RuleNames = Properties.Settings.Default.BusinessRule_HOMERuleNames.Split('|'),
                        IDCardNo = idCardNo,
                        IDCardType = idCardType.KenanCode
                    });

                    Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] = resp.IsBlacklisted;
                    Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = resp.ResultMessage;
                }

                #region BlacklistChecking Commented on 20121205 by Ben
                //var blacklistCheckReq = new BlacklistCheckRequest()
                //{
                //    CreditCheckRequest = ConstructInternalCheck(idCardNo, idCardType.KenanCode),
                //    ExternalCheckXML = ConstructExternalCheck(new ExternalCheck()
                //    {
                //        ExternalIdValue = idCardNo,
                //        //ExternalIdType = custDetailsVM.Customer.IDCardTypeID.ToString()
                //        ExternalIdType = idCardType.KenanCode
                //    })
                //};

                //using (var proxy = new KenanServiceProxy())
                //{
                //    var resp = proxy.BlacklistCheck(blacklistCheckReq);

                //    if (resp.IsExternalBlacklisted || resp.IsInternalBlacklisted)
                //    {
                //        isBlacklisted = true;
                //        Session["RegHomeReg_ResultMessage"] = resp.ResultMessage;
                //    }

                //    Session["RegHomeReg_ExternalBlacklist"] = resp.IsExternalBlacklisted;
                //    Session["RegHomeReg_InternalBlacklist"] = resp.IsInternalBlacklisted;

                //    // For testing
                //    //isBlacklisted = true;
                //    //Session["RegHomeReg_ResultMessage"] = "IC number is blacklisted";
                //}
                #endregion

                using (var proxy = new RegistrationServiceProxy())
                {
                    //check ic no duplicate with new status reg
                    var IDs = proxy.CustomerFind(new CustomerFind()
                    {
                        Customer = new Customer()
                        {
                            IDCardNo = idCardNo
                        }
                    });

                    if (IDs.Count() > 0)
                    {
                        var regIDs = proxy.RegistrationFind(new RegistrationFind()
                        {
                            CustomerIDs = IDs.ToList()
                        });

                        if (regIDs.Count() > 0)
                        {
                            var regStatusIDs = proxy.RegStatusFind(new RegStatusFind()
                            {
                                RegistrationIDs = regIDs.ToList(),
                                Active = true,
                                //RegStatus = new RegStatus()
                                //{
                                //    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_HomeSubmitted),
                                //}
                            });

                            var regStatuses = proxy.RegStatusGet(regStatusIDs).ToList();
                            var statusIDs = regStatuses.Select(a => a.StatusID).Distinct().ToList();
                            var rejectStatus = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_HomeReject);
                            var cancelStatus = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_HomeCancelled);
                            statusIDs.Remove(rejectStatus);
                            statusIDs.Remove(cancelStatus);

                            if (statusIDs.Count() > 0)
                                isDuplicate = true;
                        }
                    }
                }
                // For testing purpose
                //isDuplicate = true;
            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            Session["RegHomeReg_IsBlacklisted"] = isBlacklisted;
            Session["RegHomeReg_SkipDuplicate"] = isDuplicate;
            return Json(new { isBlacklisted = isBlacklisted, isDuplicate = isDuplicate });
        }

        [HttpPost]
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult BlacklistResult(FormCollection collection)
        {
            if (collection["submit"].ToString2() == "back")
            {
                return RedirectToAction("BlacklistChecking");
            }
            else if (collection["submit"].ToString() == "skip")
            {
                return RedirectToAction("InstallationAddress");
            }
            else if (collection["submit"].ToString2() == "next")
            {
                return RedirectToAction("InstallationAddress");
            }

            Session["RegHomeReg_ResultMessage"] = null;

            return View();
        }
        [HttpPost]
        [Authorize(Roles = "HREG_W,HREG_C")]
        public ActionResult InstallationAddress(AddressVM addressVM)
        {
            if (addressVM.Registration.InstallationDT.HasValue)
            {
                var installDate = addressVM.Registration.InstallationDT.Value.Date.ToString("yyyyMMdd");
                var installTime = addressVM.PreferredInstallationTime.ToString("HHmmss");

                addressVM.Registration.InstallationDT = DateTime.ParseExact(installDate + installTime, "yyyyMMddHHmmss", null);
            }

            Session["RegHomeReg_TabIndex"] = addressVM.TabNumber;

            //addressVM.Address.AddressTypeID = Util.GetIDByCode(RefType.AddressType, Settings.Default.AddressType_Installation);
            var addresses = new List<Address>();

            addressVM.Address = new Address()
            {
                AddressTypeID = Util.GetIDByCode(RefType.AddressType, Settings.Default.AddressType_Installation),
                Line1 = ConstructInstAddrLine1(addressVM.InstallationAddress),
                Line2 = (!string.IsNullOrEmpty(addressVM.InstallationAddress.Street)) ? (addressVM.InstallationAddress.Street) : null,
                Postcode = addressVM.InstallationAddress.Postcode,
                StateID = addressVM.InstallationAddress.StateID,
                Town = addressVM.InstallationAddress.Town,
                UnitNo = addressVM.InstallationAddress.UnitNo,
                BuildingNo = addressVM.InstallationAddress.InstallBuildingNo,
                Street = addressVM.InstallationAddress.InstallStreet
            };

            addressVM.Address.Line1 = ConstructInstAddrLine1(addressVM.Address);
            addressVM.Address.Line2 = (!string.IsNullOrEmpty(addressVM.Address.Street)) ? (addressVM.Address.Street) : null;

            addresses.Add(addressVM.Address);
            //addresses.Add(addressVM.InstallationAddress);

            Session["RegHomeReg_Addresses"] = addresses;
            Session["RegHomeReg_InstallationDT"] = addressVM.Registration.InstallationDT;

            //var addresses = (List<Address>)Session["RegHomeReg_Addresses"];
            //addresses.Where(a => a.AddressTypeID == Util.GetIDByCode(RefType.AddressType, Settings.Default.AddressType_INS));

            return RedirectToAction(GetRegActionStep(addressVM.TabNumber));
        }
        [HttpPost]
        [Authorize(Roles = "HREG_W,HREG_C,MREG_U")]
        public ActionResult SearchRegistration(SearchHomeRegistrationVM searchRegVM)
        {
            using (var proxy = new RegistrationServiceProxy())
            {
                searchRegVM.RegistrationSearchResults = proxy.RegistrationSearch(new RegistrationSearchCriteria()
                {
                    RegID = searchRegVM.RegistrationSearchCriteria.RegID,
                    IDCardNo = searchRegVM.RegistrationSearchCriteria.IDCardNo,
                    StatusID = searchRegVM.RegistrationSearchCriteria.StatusID,
                    CustName = searchRegVM.RegistrationSearchCriteria.CustName,
                    CenterOrgID = searchRegVM.RegistrationSearchCriteria.CenterOrgID,
                    RegDateFrom = searchRegVM.RegistrationSearchCriteria.RegDateFrom,
                    RegDateTo = searchRegVM.RegistrationSearchCriteria.RegDateTo,
                    MSISDN = searchRegVM.RegistrationSearchCriteria.MSISDN,
                    UserID = Util.SessionAccess == null ? 0 : Util.SessionAccess.UserID
                    //PageSize = Properties.Settings.Default.PageSize
                });
            }

            return View(searchRegVM);
        }
        [HttpPost]
        [Authorize(Roles = "HREG_W,HREG_C,MREG_U")]
        public ActionResult HomeRegSummary(FormCollection collection, CustomerDetailsVM custDetailsVM)
        {
            var resp = new HomeRegistrationCreateResp();
            Session["RegHomeReg_TabIndex"] = custDetailsVM.TabNumber;
            Session["RegHomeReg_CustomerDetailsVM"] = custDetailsVM;

            try
            {
                if (collection["submit1"].ToString() == "close")
                {
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationClose(new RegStatus()
                        {
                            RegID = custDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                        });
                    }

                    return RedirectToAction("HomeRegClosed", new { regID = custDetailsVM.RegID });
                }

                if (collection["submit1"].ToString() == "addStatus")
                {
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        var reg = new DAL.Models.Registration();
                        var regStatus = new RegStatus()
                        {
                            StatusID = custDetailsVM.RegStatus.StatusID,
                            RegID = custDetailsVM.RegStatus.RegID,
                            Remark = custDetailsVM.RegStatus.Remark,
                            StartDate = DateTime.Now,
                            CreateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            Active = true
                        };
                        proxy.RegStatusCreate(regStatus, reg);

                        return RedirectToAction("HomeRegSummary", new { regID = custDetailsVM.RegID });
                    }
                }

                if (custDetailsVM.TabNumber == (int)HomeRegistrationSteps.Submit)
                {
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        // submit registration
                        resp = proxy.HomeRegistrationCreate(ConstructRegistration(), ConstructCustomer(), ConstructRegAddress(), ConstructRegPgmBdlPkgComponent(), ConstructRegStatus());

                        var biometricID = Session["RegHomeReg_BiometricID"].ToInt();
                        if (biometricID != 0)
                        {
                            var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
                            biometric.RegID = resp.ID;

                            proxy.BiometricUpdate(biometric);
                        }
                    }

                    ClearRegistrationSession();
                }
                else if (custDetailsVM.TabNumber == (int)HomeRegistrationSteps.WBBPlan)
                {
                    return RedirectToAction("SelectWBBPlan");
                }

            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }
            return RedirectToAction("HomeRegSuccess", new { regID = resp.ID });
        }
        private DAL.Models.Registration ConstructRegistration()
        {

            var custDetailsVM = (CustomerDetailsVM)Session["RegHomeReg_CustomerDetailsVM"];
            var isBlacklisted = (bool)Session["RegHomeReg_IsBlacklisted"];
            var isduplicate = (bool)Session["RegHomeReg_SkipDuplicate"];

            var reg = new DAL.Models.Registration()
            {
                CenterOrgID = Util.SessionAccess.User.OrgID,
                RegTypeID = custDetailsVM.RegTypeID,
                InstallationDT = custDetailsVM.AddressVM.Registration.InstallationDT,
                //IsBlacklisted = (bool)Session["RegHomeReg_IsBlacklisted"],
                ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null,
                InternalBlacklisted = (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()],
                ExternalBlacklisted = (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()],
                //MSISDN1 = ((List<string>)Session["RegHomeReg_MobileNo"])[0].ToString2(),
                //MSISDN2 = ((List<string>)Session["RegHomeReg_MobileNo"])[1].ToString2(),
                SalesPerson = custDetailsVM.AddressVM.Registration.SalesPerson,
                RFSalesDT = custDetailsVM.AddressVM.Registration.RFSalesDT,
                SkipDuplicate = (bool)Session["RegHomeReg_SkipDuplicate"],
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName,
            };

            return reg;
        }
        private Customer ConstructCustomer()
        {
            var custDetailsVM = (CustomerDetailsVM)Session["RegHomeReg_CustomerDetailsVM"];

            var cust = new Customer()
            {
                FullName = custDetailsVM.Customer.FullName,
                Gender = custDetailsVM.Customer.Gender,
                DateOfBirth = custDetailsVM.Customer.DateOfBirth,
                CustomerTitleID = custDetailsVM.Customer.CustomerTitleID,
                RaceID = custDetailsVM.Customer.RaceID,
                LanguageID = custDetailsVM.Customer.LanguageID,
                NationalityID = custDetailsVM.Customer.NationalityID,
                IDCardTypeID = custDetailsVM.Customer.IDCardTypeID,
                IDCardNo = custDetailsVM.Customer.IDCardNo,
                ContactNo = custDetailsVM.Customer.ContactNo,
                AlternateContactNo = custDetailsVM.Customer.AlternateContactNo,
                PostpaidMobileNo = custDetailsVM.Customer.PostpaidMobileNo,
                EmailAddr = custDetailsVM.Customer.EmailAddr,
                PayModeID = custDetailsVM.Customer.PayModeID,
                CardTypeID = custDetailsVM.Customer.CardTypeID,
                NameOnCard = custDetailsVM.Customer.NameOnCard,
                CardNo = custDetailsVM.Customer.CardNo,
                CardExpiryDate = custDetailsVM.Customer.CardExpiryDate,
                BizRegName = custDetailsVM.Customer.BizRegName,
                BizRegNo = custDetailsVM.Customer.BizRegNo,
                SignatoryName = custDetailsVM.Customer.SignatoryName,
                Active = true,
                LastAccessID = Util.SessionAccess.UserName,
                CreateDT = DateTime.Now
            };

            return cust;
        }
        //private RegMdlGrpModel ConstructRegMdlGroupModel()
        //{
        //    if (Session["RegHomeReg_Type"].ToInt() == (int)MobileRegType.PlanOnly)
        //        return new RegMdlGrpModel();

        //    var mdlGrpModelID = 0;

        //    using (var proxy = new CatalogServiceProxy())
        //    {
        //        var bundleID = proxy.PgmBdlPckComponentGet(new int[] { Session["RegHomeReg_PkgPgmBdlPkgCompID"].ToInt() }).SingleOrDefault().ParentID;
        //        var pbPgmBdlPkgCompIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
        //        {
        //            PgmBdlPckComponent = new PgmBdlPckComponent()
        //            {
        //                ChildID = bundleID,
        //                LinkType = Properties.Settings.Default.Program_Bundle
        //            }
        //        }).ToList();

        //        var modelGroupIDs = proxy.ModelGroupFind(new ModelGroupFind()
        //        {
        //            ModelGroup = new ModelGroup(),
        //            PgmBdlPkgCompIDs = pbPgmBdlPkgCompIDs
        //        });

        //        var modelGroupModels = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
        //        {
        //            ModelGroupModel = new ModelGroupModel()
        //            {
        //                ModelID = Session["RegHomeReg_SelectedModelID"].ToInt()
        //            }
        //        })).ToList();

        //        mdlGrpModelID = modelGroupModels.Where(a => modelGroupIDs.Contains(a.ModelGroupID)).Select(a => a.ID).SingleOrDefault();
        //    }

        //    var regMdlGrpModel = new RegMdlGrpModel()
        //    {
        //        ModelImageID = Session["RegHomeReg_SelectedModelImageID"].ToInt(),
        //        ModelGroupModelID = mdlGrpModelID,
        //        Active = true,
        //        CreateDT = DateTime.Now,
        //        LastAccessID = "Developer"
        //    };

        //    return regMdlGrpModel;

        //}
        private List<Address> ConstructRegAddress()
        {
            var addresses = new List<Address>();
            var custDetailsVM = (CustomerDetailsVM)Session["RegHomeReg_CustomerDetailsVM"];

            // Billing address
            addresses.Add(new Address()
            {
                AddressTypeID = custDetailsVM.AddressVM.BillingAddress.AddressTypeID,
                Line1 = custDetailsVM.AddressVM.BillingAddress.Line1,
                Line2 = custDetailsVM.AddressVM.BillingAddress.Line2,
                Postcode = custDetailsVM.AddressVM.BillingAddress.Postcode,
                StateID = custDetailsVM.AddressVM.BillingAddress.StateID,
                Town = custDetailsVM.AddressVM.BillingAddress.Town,
                CountryID = Util.GetIDByCode(RefType.Country, Properties.Settings.Default.Country_MY),
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });
            // Installation address
            addresses.Add(new Address()
            {
                AddressTypeID = custDetailsVM.AddressVM.InstallationAddress.AddressTypeID,
                UnitNo = custDetailsVM.AddressVM.InstallationAddress.UnitNo,
                Street = custDetailsVM.AddressVM.InstallationAddress.Street,
                BuildingNo = custDetailsVM.AddressVM.InstallationAddress.BuildingNo,
                Line1 = custDetailsVM.AddressVM.InstallationAddress.Line1,
                Line2 = custDetailsVM.AddressVM.InstallationAddress.Line2,
                Postcode = custDetailsVM.AddressVM.InstallationAddress.Postcode,
                StateID = custDetailsVM.AddressVM.InstallationAddress.StateID,
                Town = custDetailsVM.AddressVM.InstallationAddress.Town,
                CountryID = Util.GetIDByCode(RefType.Country, Properties.Settings.Default.Country_MY),
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });
            // Permanent address
            addresses.Add(new Address()
            {
                AddressTypeID = custDetailsVM.AddressVM.PermanentAddress.AddressTypeID,
                Line1 = custDetailsVM.AddressVM.PermanentAddress.Line1,
                Line2 = custDetailsVM.AddressVM.PermanentAddress.Line2,
                Postcode = custDetailsVM.AddressVM.PermanentAddress.Postcode,
                StateID = custDetailsVM.AddressVM.PermanentAddress.StateID,
                Town = custDetailsVM.AddressVM.PermanentAddress.Town,
                CountryID = Util.GetIDByCode(RefType.Country, Properties.Settings.Default.Country_MY),
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });

            return addresses;
        }
        private List<RegPgmBdlPkgComp> ConstructRegPgmBdlPkgComponent()
        {
            var regPBPCs = new List<RegPgmBdlPkgComp>();

            // Plan PgmBdlPkgComponent
            regPBPCs.Add(new RegPgmBdlPkgComp()
            {
                RegSuppLineID = null,
                PgmBdlPckComponentID = Session["RegHomeReg_PkgPgmBdlPkgCompID"].ToInt(),
                IsNewAccount = true,
                Active = true,
                CreateDT = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName
            });

            // Mandatory VAS PgmBdlPkgComponent
            var vasPBPCIDs = new List<int>();
            using (var proxy = new CatalogServiceProxy())
            {
                var pkgID = MasterDataCache.Instance.FilterComponents(new int[] { Session["RegHomeReg_PkgPgmBdlPkgCompID"].ToInt() }).SingleOrDefault().ChildID;

                vasPBPCIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    IsMandatory = true,
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        ParentID = pkgID,
                        LinkType = Properties.Settings.Default.Package_Component
                    }
                }).ToList();

                foreach (var vasPBPCID in vasPBPCIDs)
                {
                    regPBPCs.Add(new RegPgmBdlPkgComp()
                    {
                        RegSuppLineID = null,
                        PgmBdlPckComponentID = vasPBPCID,
                        IsNewAccount = true,
                        Active = true,
                        CreateDT = DateTime.Now,
                        LastAccessID = Util.SessionAccess.UserName
                    });
                }
            }

            if (Session["RegHomeReg_OptPkgPgmBdlPkgCompID"] != null)
            {
                regPBPCs.Add(new RegPgmBdlPkgComp()
                {
                    RegSuppLineID = null,
                    PgmBdlPckComponentID = Session["RegHomeReg_OptPkgPgmBdlPkgCompID"].ToInt(),
                    IsNewAccount = true,
                    Active = true,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName
                });

                // Non Mandatory VAS PgmBdlPkgComponent
                var vasOptPBPCIDs = new List<int>();
                using (var proxy = new CatalogServiceProxy())
                {
                    var pkgID = MasterDataCache.Instance.FilterComponents(new int[] { Session["RegHomeReg_OptPkgPgmBdlPkgCompID"].ToInt() }).SingleOrDefault().ChildID;

                    vasOptPBPCIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                    {
                        IsMandatory = true,
                        PgmBdlPckComponent = new PgmBdlPckComponent()
                        {
                            ParentID = pkgID,
                            LinkType = Properties.Settings.Default.Package_Component
                        }
                    }).ToList();

                    foreach (var vasOptPBPCID in vasOptPBPCIDs)
                    {
                        regPBPCs.Add(new RegPgmBdlPkgComp()
                        {
                            RegSuppLineID = null,
                            PgmBdlPckComponentID = vasOptPBPCID,
                            IsNewAccount = true,
                            Active = true,
                            CreateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });
                    }
                }
            }

            //// Optional VAS PgmBdlPkgComponent
            //var vasIDs = Session["RegHomeReg_VasIDs"].ToString2().Split(',');
            //foreach (var vasID in vasIDs)
            //{
            //    if (!string.IsNullOrEmpty(vasID))
            //    {
            //        regPBPCs.Add(new RegPgmBdlPkgComp()
            //        {
            //            PgmBdlPckComponentID = vasID.ToInt(),
            //            IsNewAccount = true,
            //            Active = true,
            //            CreateDT = DateTime.Now,
            //            LastAccessID = "Developer"
            //        });
            //    }
            //}

            return regPBPCs;
        }
        private RegStatus ConstructRegStatus()
        {
            var regStatus = new RegStatus()
            {
                Active = true,
                CreateDT = DateTime.Now,
                StartDate = DateTime.Now,
                LastAccessID = Util.SessionAccess.UserName,
                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_HomeSubmitted)
            };

            return regStatus;
        }

        [HttpPost]
        public ActionResult GetReasonCodeByStatus(int statusID)
        {
            var reasonCode = new List<int>();
            var reasonCodeList = new List<StatusReason>();

            using (var proxy = new ConfigServiceProxy())
            {
                var reasonCodeFind = proxy.StatusReasonCodeFind(new StatusReasonCodeFind()
                {
                    StatusReasonCode = new StatusReasonCode()
                    {
                        StatusID = statusID
                    }
                });
                if (reasonCodeFind.Count() > 0)
                {
                    reasonCode = proxy.StatusReasonCodeGet(reasonCodeFind).Select(a => a.ReasonCodeID).ToList();

                    reasonCodeList = proxy.StatusReasonGet(reasonCode).ToList();
                }
            }

            reasonCodeList.Insert(0, new StatusReason() { ID = 0, Name = "Non Applicable" });

            return Json(new SelectList(reasonCodeList, "ID", "Name"));
        }
        [HttpPost]
        public ActionResult GetHasStatusDate(int statusID)
        {
            using (var proxy = new ConfigServiceProxy())
            {
                var status = proxy.StatusGet(statusID);

                return Json(status.HasStatusDate);
            }
        }

        private void SetRegHomeStepNo(HomeRegistrationSteps regStep)
        {
            switch (regStep)
            {
                case HomeRegistrationSteps.InstallationAddr:
                    Session["RegHomeStepNo"] = (int)HomeRegistrationSteps.InstallationAddr + 1;
                    break;
                case HomeRegistrationSteps.CustDetails:
                    Session["RegHomeStepNo"] = (int)HomeRegistrationSteps.CustDetails + 1;
                    break;
                case HomeRegistrationSteps.Plan:
                    Session["RegHomeStepNo"] = (int)HomeRegistrationSteps.Plan + 1;
                    break;
                case HomeRegistrationSteps.WBBPlan:
                    Session["RegHomeStepNo"] = (int)HomeRegistrationSteps.WBBPlan + 1;
                    break;
                //case HomeRegistrationSteps.NumberSelection:
                //    Session["RegHomeStepNo"] = (int)HomeRegistrationSteps.NumberSelection + 1;
                //    break;
                case HomeRegistrationSteps.Summary:
                    Session["RegHomeStepNo"] = (int)HomeRegistrationSteps.Summary + 1;
                    break;
                case HomeRegistrationSteps.Submit:
                    Session["RegHomeStepNo"] = (int)HomeRegistrationSteps.Submit + 1;
                    break;
                default:
                    break;
            }

            //if (Session["RegHomeReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
            //{
            //    if (regStep == HomeRegistrationSteps.Device)
            //        Session["RegHomeStepNo"] = Session["RegHomeStepNo"].ToInt() + 1;
            //}
            //else if (Session["RegHomeReg_Type"].ToInt() == (int)MobileRegType.PlanOnly)
            //{
            //    Session["RegHomeStepNo"] = Session["RegHomeStepNo"].ToInt() - 1;
            //}
        }
        private string GetRegActionStep(int stepNo)
        {
            if ((int)HomeRegistrationSteps.InstallationAddr == stepNo)
            {
                return "InstallationAddress";
            }
            if ((int)HomeRegistrationSteps.CustDetails == stepNo)
            {
                return "CustomerDetails";
            }
            else if ((int)HomeRegistrationSteps.Plan == stepNo)
            {
                return "SelectPlan";
            }
            else if ((int)HomeRegistrationSteps.WBBPlan == stepNo)
            {
                return "SelectWBBPlan";
            }
            //else if ((int)HomeRegistrationSteps.NumberSelection == stepNo)
            //{
            //    return "SelectMobileNo";
            //}
            else if ((int)HomeRegistrationSteps.Summary == stepNo)
            {
                return "HomeRegSummary";
            }

            return "";
        }
        private bool ValidateCardDetails(Customer customer)
        {
            if (!IsCardDetailsEntered(customer))
            {
                ModelState.AddModelError(string.Empty, "Please enter all Card details.");
                return false;
            }

            if (customer.CardNo.Length != 16)
            {
                ModelState.AddModelError(string.Empty, "Credit Card No must be 16 digit.");
                return false;
            }

            if (customer.CardExpiryDate.Length != 4)
            {
                ModelState.AddModelError(string.Empty, "Credit Card Expiry Date must be 4 digit.");
                return false;
            }

            return true;
        }
        private bool IsCardDetailsEntered(Customer customer)
        {
            if (!customer.CardTypeID.HasValue || customer.CardTypeID.Value <= 0)
                return false;

            if (string.IsNullOrEmpty(customer.CardNo))
                return false;

            if (string.IsNullOrEmpty(customer.CardExpiryDate))
                return false;

            if (string.IsNullOrEmpty(customer.NameOnCard))
                return false;

            return true;
        }
        private HomePackageVM GetAvailablePackages(bool isMandatory)
        {
            var homePackageVM = new HomePackageVM();
            var availablePackages = new List<AvailableHomePackage>();
            var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
            var programBundles = new List<PgmBdlPckComponent>();
            var packages = new List<Package>();
            var bundles = new List<Bundle>();
            var bundleIDs = new List<int>();
            var programs = new List<Program>();

            using (var proxy = new CatalogServiceProxy())
            {
                //if (Session["RegHomeReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
                //{
                //    var modelID = proxy.ModelImageGet(Session["RegHomeReg_SelectedModelImageID"].ToInt()).ModelID;

                //    var modelGroupIDs = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
                //    {
                //        ModelGroupModel = new ModelGroupModel()
                //        {
                //            ModelID = modelID
                //        },
                //        Active = true
                //    })).Select(a => a.ModelGroupID).Distinct().ToList();

                //    // program - bundle
                //    var pgmBdlPkgCompIDs = proxy.ModelGroupGet(modelGroupIDs).Select(a => a.PgmBdlPckComponentID).ToList();
                //    bundleIDs = proxy.PgmBdlPckComponentGet(pgmBdlPkgCompIDs).Where(a => a.Active == true).Select(a => a.ChildID).ToList();
                //}

                var homeProgramBundleIDs = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.Program_Bundle,
                        ParentID = Util.GetIDByCode(RefType.Program, Properties.Settings.Default.Program_Home),
                        FilterOrgType = Util.SessionOrgTypeCode
                    },
                    Active = true
                })).Distinct().Select(a => new { ID = a.ID, ProgramID = a.ParentID, BundleID = a.ChildID }).ToList();

                var query = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                {
                    PgmBdlPckComponent = new PgmBdlPckComponent()
                    {
                        LinkType = Properties.Settings.Default.Bundle_Package
                    },
                    ParentIDs = homeProgramBundleIDs.Select(a => a.BundleID).ToList(),
                    Active = true,
                    IsMandatory = isMandatory
                }));

                //if (Session["RegHomeReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
                //{
                //    pgmBdlPckComponents = query.Where(a => bundleIDs.Contains(a.ParentID)).ToList();
                //}
                //else if (Session["RegHomeReg_Type"].ToInt() == (int)MobileRegType.PlanOnly)
                //{
                pgmBdlPckComponents = query.ToList();
                //}

                if (pgmBdlPckComponents.Count() > 0)
                {
                    packages = proxy.PackageGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();
                    bundles = proxy.BundleGet(pgmBdlPckComponents.Select(a => a.ParentID).Distinct()).ToList();
                }

                //var pgmBdlIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                //{
                //    PgmBdlPckComponent = new PgmBdlPckComponent()
                //    {
                //        LinkType = Properties.Settings.Default.Program_Bundle,
                //    },
                //    ChildIDs = pgmBdlPckComponents.Select(a => a.ChildID).ToList()
                //});

                programBundles = MasterDataCache.Instance.FilterComponents(homeProgramBundleIDs.Select(a => a.ID).ToList()).Distinct().ToList();
                programs = proxy.ProgramGet(programBundles.Select(a => a.ParentID).ToList()).ToList();
            }

            if (programs.Count() > 0)
            {
                foreach (var pbpc in pgmBdlPckComponents)
                {
                    availablePackages.Add(new AvailableHomePackage()
                    {
                        PgmBdlPkgCompID = pbpc.ID,
                        ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
                        BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
                        PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
                        PackageCode = pbpc.Code
                    });
                }
            }

            homePackageVM.AvailableHomePackages = availablePackages;
            homePackageVM.SelectedPgmBdlPkgCompID = Session["RegHomeReg_PkgPgmBdlPkgCompID"].ToInt();
            homePackageVM.SelectedOptPgmBdlPkgCompID = Session["RegHomeReg_OptPkgPgmBdlPkgCompID"].ToInt();

            return homePackageVM;
        }
        private HomeOrderSummaryVM ConstructOrderSummary()
        {
            var components = new List<Component>();
            var modelImage = new ModelImage();
            var package = new Package();
            var bundle = new Bundle();
            var orderVM = new HomeOrderSummaryVM();
            var phoneVM = (PhoneVM)Session["RegHomeReg_PhoneVM"];

            if ((HomeOrderSummaryVM)Session["RegHomeReg_OrderSummary"] != null)
                orderVM = (HomeOrderSummaryVM)Session["RegHomeReg_OrderSummary"];

            //Package Details
            if (Session["RegHomeReg_PkgPgmBdlPkgCompID"].ToInt() != orderVM.SelectedPgmBdlPkgCompID)
            {
                var bundlePackage = new PgmBdlPckComponent();
                orderVM.SelectedPgmBdlPkgCompID = Session["RegHomeReg_PkgPgmBdlPkgCompID"].ToInt();
                using (var proxy = new CatalogServiceProxy())
                {
                    bundlePackage = MasterDataCache.Instance.FilterComponents(new int[] 
                    { 
                        Session["RegHomeReg_PkgPgmBdlPkgCompID"].ToInt() 
                    }).SingleOrDefault();

                    package = proxy.PackageGet(new int[] 
                    { 
                        bundlePackage.ChildID 
                    }).SingleOrDefault();

                    bundle = proxy.BundleGet(new int[] 
                    { 
                        bundlePackage.ParentID 
                    }).SingleOrDefault();
                }

                orderVM.PlanBundle = bundle.Name;
                orderVM.PlanName = package.Name;
                orderVM.MonthlySubscription = bundlePackage.Price;
                orderVM.TotalPrice += bundlePackage.Price;
            }

            //WBB Package Details
            if (Session["RegHomeReg_OptPkgPgmBdlPkgCompID"].ToInt() != orderVM.SelectedOptPgmBdlPkgCompID)
            {
                orderVM.SelectedOptPgmBdlPkgCompID = Session["RegHomeReg_OptPkgPgmBdlPkgCompID"].ToInt();
                using (var proxy = new CatalogServiceProxy())
                {
                    var bundlePackage = MasterDataCache.Instance.FilterComponents(new int[] 
                        { 
                            Session["RegHomeReg_OptPkgPgmBdlPkgCompID"].ToInt() 
                        }).SingleOrDefault();

                    package = proxy.PackageGet(new int[] 
                    { 
                        bundlePackage.ChildID 
                    }).SingleOrDefault();

                    bundle = proxy.BundleGet(new int[] 
                    { 
                        bundlePackage.ParentID 
                    }).SingleOrDefault();
                }

                orderVM.OptPlanBundle = bundle.Name;
                orderVM.OptPlanName = package.Name;
            }

            //VAS details
            //var selectedCompIDs = new List<int>();
            //if (!string.IsNullOrEmpty(Session["RegHomeReg_VasIDs"].ToString2()) ||
            //    (orderVM.VasVM.SelectedVasIDs != Session["RegHomeReg_VasIDs"].ToString2()))
            //{
            //    orderVM.VasVM.SelectedVasIDs = Session["RegHomeReg_VasIDs"].ToString2();

            //    var vasIDs = orderVM.VasVM.SelectedVasIDs.Split(',').ToList();
            //    foreach (var id in vasIDs)
            //    {
            //        if (!string.IsNullOrEmpty(id))
            //        {
            //            selectedCompIDs.Add(id.ToInt());
            //        }
            //    }

            //    using (var proxy = new CatalogServiceProxy())
            //    {
            //        orderVM.VasVM.VASesName = proxy.ComponentGet(
            //            proxy.PgmBdlPckComponentGet
            //            (
            //                selectedCompIDs
            //            ).Select(a => a.ChildID)
            //        ).Select(a => a.Name).ToList();
            //    }
            //    Session["RegHomeReg_VasNames"] = orderVM.VasVM.VASesName;
            //}

            //MobileNo
            //orderVM.MobileNumbers = Session["RegHomeReg_MobileNo"] == null ? new List<string>() : (List<string>)Session["RegHomeReg_MobileNo"];
            orderVM.MobileNumbers = Session["RegHomeReg_MobileNo"] == null ? new List<string>() : (List<string>)Session["RegHomeReg_MobileNo"];

            Session["RegHomeReg_OrderSummary"] = orderVM;

            return orderVM;
        }
        //UNUSED METHODS
        //private creditCheckOperation ConstructInternalCheck(string idCardNo, string idCardTypeID)
        //{
        //    var userRoles = new List<string>();
        //    userRoles.Add(Properties.Settings.Default.IntChk_Role);

        //    var orgUnit = new List<string>();
        //    orgUnit.Add(Properties.Settings.Default.IntChk_Org);

        //    return new creditCheckOperation
        //    {
        //        creditCheckRequestField = new CreditCheck_RequestType
        //        {
        //            serviceHeaderField = new ServiceHeader
        //            {
        //                securityInfoField = new SecurityHeaderType
        //                {
        //                    orgUnitsField = orgUnit.ToArray(),
        //                    userRolesField = userRoles.ToArray(),
        //                    userNameTokenField = new UserNameTokenType()
        //                    {
        //                        idField = Properties.Settings.Default.IntChk_UserTokenID,
        //                        userNameField = Properties.Settings.Default.IntChk_UserTokenName,
        //                        userTypeField = Properties.Settings.Default.IntChk_UserTokenType
        //                    },
        //                },
        //                serviceInfoField = new ServiceRequestHeaderType
        //                {
        //                    operationNameField = Properties.Settings.Default.IntChk_Operation,
        //                    serviceNameField = new ServiceRequestHeaderTypeServiceName()
        //                    {
        //                        serviceIdField = Properties.Settings.Default.IntChk_ServiceID,
        //                        valueField = Properties.Settings.Default.IntChk_ServiceValue,
        //                        versionField = Properties.Settings.Default.IntChk_ServiceVersion
        //                    },
        //                    serviceRequesterInfoField = new ServiceRequesterInfoType()
        //                    {
        //                        applicationNameField = new ServiceRequesterInfoTypeApplicationName()
        //                        {
        //                            appIdField = Properties.Settings.Default.IntChk_AppID,
        //                            valueField = Properties.Settings.Default.IntChk_AppValue
        //                        },
        //                    },
        //                    transactionInfoField = new ServiceTransactionInfoType()
        //                    {
        //                        majorTransactionIdField = Properties.Settings.Default.IntChk_TransMajorID,
        //                        minorTransactionIdField = Properties.Settings.Default.IntChk_TransMinorID,
        //                        messageIdField = Properties.Settings.Default.IntChk_TransMsgID
        //                    }
        //                }
        //            },
        //            serviceRequestField = new _CreditCheckRequest
        //            {
        //                iD_TypeField = idCardTypeID, //Util.GetIDCardTypeID(IDCARDTYPE.NRIC).ToString2(),
        //                iD_ValueField = idCardNo
        //            }
        //        }
        //    };

        //    //String XmlizedString = null;
        //    //MemoryStream memoryStream = new MemoryStream();
        //    //XmlSerializer xs = new XmlSerializer(typeof(InternalCheck));
        //    //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, null);

        //    //xs.Serialize(xmlTextWriter, intCheck);
        //    //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
        //    //UTF8Encoding encode = new UTF8Encoding();
        //    //XmlizedString = encode.GetString(memoryStream.ToArray());

        //    //return XmlizedString;
        //}
        //UNUSED METHODS
        //private string ConstructExternalCheck(ExternalCheck extCheck)

        //{
        //    String XmlizedString = null;
        //    //MemoryStream memoryStream = new MemoryStream();
        //    //XmlSerializer xs = new XmlSerializer(typeof(ExternalCheck));
        //    //XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, null);

        //    //xs.Serialize(xmlTextWriter, extCheck);
        //    //memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
        //    //UTF8Encoding encode = new UTF8Encoding();
        //    //XmlizedString = encode.GetString(memoryStream.ToArray());

        //    XmlizedString = StringXMLSerializer<ExternalCheck>.Convert2XML(extCheck);

        //    XmlDocument xmlDoc = new XmlDocument();
        //    xmlDoc.LoadXml(XmlizedString);

        //    xmlDoc.RemoveChild(xmlDoc.FirstChild);

        //    StringWriter sw = new StringWriter();
        //    XmlTextWriter tx = new XmlTextWriter(sw);
        //    xmlDoc.WriteTo(tx);

        //    XmlizedString = sw.ToString();

        //    //return "<![CDATA[" + XmlizedString + "]]>";
        //    return XmlizedString;
        //}
        private void ClearRegistrationSession()
        {
            Session["RegHomeReg_IsBlacklisted"] = null;
            Session["RegHomeReg_ResultMessage"] = null;
            Session["RegHomeReg_TabIndex"] = null;
            Session["RegHomeReg_SelectedModelImageID"] = null;
            Session["RegHomeReg_SelectedModelID"] = null;
            Session["RegHomeReg_PkgPgmBdlPkgCompID"] = null;
            Session["RegHomeReg_OptPkgPgmBdlPkgCompID"] = null;
            Session["RegHomeReg_ProgramMinAge"] = null;
            Session["RegHomeReg_OptProgramMinAge"] = null;
            Session["RegHomeReg_MobileNo"] = null;
            Session["RegHomeReg_OrderSummary"] = null;
            Session["RegHomeReg_PhoneVM"] = null;
            Session["RegHomeReg_custDetailsVM"] = null;
            Session["RegHomeStepNo"] = null;
            Session["RegHomeReg_Address"] = null;
            Session["RegHomeReg_InstallDT"] = null;
            Session["RegHomeReg_ICNo"] = null;
            Session["RegHomeReg_SelectedOptionID"] = null;
            Session["RegHomeReg_SelectedOptionType"] = null;
            Session["RegHomeReg_SkipDuplicate"] = null;
            Session["RegHomeReg_IDCardType"] = null;
        }

        [HttpPost]
        public ActionResult PostcodeValidation(string postcode)
        {
            string result = "";

            using (var proxy = new KenanServiceProxy())
            {
                var response = proxy.PostcodeValidation(new PostcodeValidationRequest()
                {
                    InputXML = ConstructPostcodeXML(new PostcodeValidate()
                    {
                        Postcode = postcode
                    })
                });

                result = response.Message;
            }

            return Json(result);
        }
        private string ConstructPostcodeXML(PostcodeValidate postcodeValidate)
        {
            String XmlizedString = null;

            XmlizedString = StringXMLSerializer<PostcodeValidate>.Convert2XML(postcodeValidate);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XmlizedString);

            xmlDoc.RemoveChild(xmlDoc.FirstChild);

            StringWriter sw = new StringWriter();
            XmlTextWriter tx = new XmlTextWriter(sw);
            xmlDoc.WriteTo(tx);

            XmlizedString = sw.ToString();

            return XmlizedString;

        }

        [HttpPost]
        [Authorize]
        public ActionResult StatusLog(CustomerDetailsVM custDetailsVM)
        {
            using (var proxy = new RegistrationServiceProxy())
            {
                var regStatus = new RegStatus()
                {
                    StatusID = custDetailsVM.RegStatusID,
                    ReasonCodeID = custDetailsVM.RegStatus.ReasonCodeID == 0 ? null : custDetailsVM.RegStatus.ReasonCodeID,
                    RegID = custDetailsVM.RegID,
                    Remark = custDetailsVM.RegStatus.Remark,
                    StatusLogDT = custDetailsVM.RegStatus.StatusLogDT,
                    CreateDT = DateTime.Now,
                    LastAccessID = Util.SessionAccess.UserName,
                    Active = true
                };

                var registration = new DAL.Models.Registration();
                var statusID = Util.GetIDByCode(RefType.Status, Settings.Default.Status_HomeConfirmed);

                if (custDetailsVM.RegStatusID == statusID)
                {
                    registration = proxy.RegistrationGet(custDetailsVM.RegID);
                    registration.ModemID = custDetailsVM.ModemID;
                    registration.KenanAccountNo = custDetailsVM.KenanAccountNo;
                }


                // Create Reg status
                proxy.RegStatusCreate(regStatus, registration);

                return RedirectToAction("HomeRegSummary", new { regID = custDetailsVM.RegID });
            }
        }

        [HttpPost]
        public ActionResult RetrieveMyKadInfo(string icno)
        {
            var resp = new AMRSvc.RetrieveMyKadInfoRp();

            //var xmlString = @"<amr_mykad_response><amr_result>0</amr_result><amr_message>MyKad Info Retrieved Succesfully</amr_message><mykad_info><name>Tan Kok Wai</name><new_ic>701031101234</new_ic><old_ic>A12345678</old_ic><gender>Lelaki</gender><dob>31-10-1970</dob><nationality>Malaysian</nationality><race>Cina</race><religion>Buddhist</religion><address1>Jalan SS 6/16A</address1><address2>Dataran Glomac, Kelana Jaya</address2><address3>Petaling Jaya</address3><postcode>47301</postcode><city>Petaling Jaya</city><state>Selangor</state><finger_match>Y</finger_match><finger_thumb>1</finger_thumb></mykad_info></amr_mykad_response>";
            //resp = ConstructMyKadInfoRp(xmlString);

            using (var proxy = new AMRServiceProxy())
            {
                resp = proxy.RetrieveMyKadInfo(new AMRSvc.RetrieveMyKadInfoRq()
                {
                    ApplicationID = 3,
                    DealerCode = Properties.Settings.Default.DealerCode,
                    NewIC = icno
                });
            }

            if (string.IsNullOrEmpty(resp.Name))
            {
                Session["RegHomeReg_BiometricVerify"] = false;
            }
            else
            {
                Session["RegHomeReg_BiometricVerify"] = true;

                using (var proxy = new RegistrationServiceProxy())
                {
                    Session["RegHomeReg_BiometricID"] = proxy.BiometricCreate(ConstructBriometri(resp));
                }
            }

            return Json(resp);
        }
        private void LogExceptions(Exception ex)
        {
            /* BEGIN  Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/
            string strErrorMsg = Util.LogException(ex);
            string actionName = this.ControllerContext.RouteData.Values["action"] != null ? this.ControllerContext.RouteData.Values["action"].ToString() : "";
            string controllerName = this.ControllerContext.RouteData.Values["controller"] != null ? this.ControllerContext.RouteData.Values["controller"].ToString() : "";
            Logger.Error("Controller: " + controllerName + "\n" + " Action:" + actionName + "\n" + " Error:" + strErrorMsg, ex);
            /* END Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/

        }
        private Biometrics ConstructBriometri(AMRSvc.RetrieveMyKadInfoRp resp)
        {
            return new Biometrics()
            {
                Name = resp.Name,
                NewIC = resp.NewIC,
                OldIC = resp.OldIC,
                Gender = resp.Gender,
                DOB = resp.DOB,
                Nationality = resp.Nationality,
                Race = resp.Race,
                Religion = resp.Religion,
                Address1 = resp.Address1,
                Address2 = resp.Address2,
                Address3 = resp.Address3,
                Postcode = resp.Postcode,
                City = resp.City,
                State = resp.State,
                FingerMatch = resp.FingerMatch ? "Y" : "N",
                FingerThumb = resp.FingerThumb,
                LastAccessID = Util.SessionAccess.UserName,
                CreateDT = DateTime.Now
            };
        }

        //UNUSED METHODS
        //private AMRSvc.RetrieveMyKadInfoRp ConstructMyKadInfoRp(string xmlString)
        //{
        //    var resp = new AMRSvc.RetrieveMyKadInfoRp();

        //    string str = string.Empty;

        //    using (XmlReader xmlReader = XmlReader.Create(new StringReader(xmlString)))
        //    {
        //        while (xmlReader.Read())
        //        {
        //            XmlNodeType nType = xmlReader.NodeType;

        //            if (nType == XmlNodeType.Element && str != xmlReader.Name.ToString())
        //            {
        //                str = xmlReader.Name.ToString();
        //            }

        //            if (nType == XmlNodeType.Text)
        //            {
        //                switch (str)
        //                {
        //                    case "name":
        //                        resp.Name = xmlReader.Value;
        //                        break;
        //                    case "new_ic":
        //                        resp.NewIC = xmlReader.Value;
        //                        break;
        //                    case "old_ic":
        //                        resp.OldIC = xmlReader.Value;
        //                        break;
        //                    case "gender":
        //                        if (xmlReader.Value == "Lelaki")
        //                            resp.Gender = "M";
        //                        else if (xmlReader.Value == "Perempuan")
        //                            resp.Gender = "F";
        //                        break;
        //                    case "dob":
        //                        resp.DOB = xmlReader.Value.Replace('-', '/');
        //                        break;
        //                    case "nationality":
        //                        resp.Nationality = xmlReader.Value;
        //                        break;
        //                    case "race":
        //                        resp.Race = xmlReader.Value;
        //                        break;
        //                    case "religion":
        //                        resp.Religion = xmlReader.Value;
        //                        break;
        //                    case "address1":
        //                        resp.Address1 = xmlReader.Value;
        //                        break;
        //                    case "address2":
        //                        resp.Address2 = xmlReader.Value;
        //                        break;
        //                    case "address3":
        //                        resp.Address3 = xmlReader.Value;
        //                        break;
        //                    case "postcode":
        //                        resp.Postcode = xmlReader.Value;
        //                        break;
        //                    case "city":
        //                        resp.City = xmlReader.Value;
        //                        break;
        //                    case "state":
        //                        resp.State = xmlReader.Value;
        //                        break;
        //                    case "finger_match":
        //                        if (xmlReader.Value == "Y")
        //                            resp.FingerMatch = true;
        //                        else
        //                            resp.FingerMatch = false;
        //                        break;
        //                    case "finger_thumb":
        //                        resp.FingerThumb = xmlReader.Value;
        //                        break;
        //                    default:
        //                        break;
        //                }
        //            }
        //        }
        //    }

        //    return resp;
        //}

    }
}
