﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Online.Registration.Web.Models;
using Online.Registration.Web.Helper;
using Online.Registration.Web.Properties;
using SNT.Utility;

using log4net;
using System.Web.Routing;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.CommonEnum;

namespace Online.Registration.Web.Controllers
{
    ///[HandleError(View = "GenericError")]
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class InquiryController : Controller
    {
        private string dateFormat = "d MMM yyyy";

        private static readonly ILog Logger = LogManager.GetLogger(typeof(InquiryController));
        protected override void Initialize(RequestContext requestContext)
        {
            Util.CheckSessionAccess(requestContext);
            base.Initialize(requestContext);
        }

        //private void LogException(Exception ex)
        //{
        //    Logger.DebugFormat("Exception: {0}", ex.ToString());
        //}

        //private RedirectToRouteResult RedirectException(Exception ex)
        //{
        //    Util.SetSessionErrMsg(ex);
        //    return RedirectToAction("Error", "Home");
        //}

        public ActionResult Index()
        {
            return View(new InquiryIndexModel());
        }

        public ActionResult AccountSummary()
        {
            string msisdn = Session[SessionKey.MSISDN.ToString()].ToString2();
            var viewModel = new InquiryAccountSummaryModel { MSISDN = msisdn };

            try
            {
                PopulateAccountInfo(viewModel);
                PopulateBillingInfo(viewModel);
                PopulateEBPSLink(viewModel);
            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }

            return View(viewModel);
        }

        private void PopulateAccountInfo(InquiryAccountSummaryModel model)
        {
            //M2KSvc.Account account = null;

            //if(Settings.Default.DevelopmentModeOn)
            //    account = GetTestingAccount();
            //else
            //    account = M2KServiceProxy.GetAccount(model.MSISDN);

            //var modelAccountInfo = model.AccountInfo;
            //var accountCategoryID = Util.GetIDByKenanCode(RefType.AccountCategory, account.AccountCategory.ToString());
            //var marketID = Util.GetIDByKenanCode(RefType.Market, account.MarketCode.ToString());
            //var addressDetails = Util.ConcatenateStrings(" ", account.City, account.Zip, account.State);

            //modelAccountInfo.AccountNumber = account.AccountNo.ToString();
            //modelAccountInfo.Holder = account.LastName;
            //modelAccountInfo.CompanyName = account.CompanyName;
            //modelAccountInfo.Category = Util.GetNameByID(RefType.AccountCategory, accountCategoryID);
            //modelAccountInfo.MarketCode = Util.GetNameByID(RefType.Market, marketID);
            //modelAccountInfo.Address = Util.ConcatenateStrings(", ", account.Address1, account.Address2, account.Address3, addressDetails);
            //modelAccountInfo.SubscribeNo = account.SubscribeNo.ToString();
            //modelAccountInfo.SubscribeNoResets = account.SubscribeNoResets.ToString();
            //modelAccountInfo.ActiveDate = ""; //TODO: Find out what api needed for this
            //modelAccountInfo.Plan = ""; //TODO: Find out what api needed for this

            //if (!account.extendedData.IsEmpty())
            //{
            //    var extendedData = account.extendedData[0];
            //    modelAccountInfo.IDNumber = extendedData.Id;
            //    modelAccountInfo.IDType = extendedData.DisVal;
            //}
            retrieveAcctListByICResponse AcctListByICResponse = null;
            if ( Session[SessionKey.PPIDInfo.ToString()] != null)
            {
                AcctListByICResponse = (retrieveAcctListByICResponse) Session[SessionKey.PPIDInfo.ToString()];
            }
            List<Items> itemList = AcctListByICResponse.itemList.Where(a => a.ExternalId == (model.MSISDN)).ToList();
            var modelAccountInfo = model.AccountInfo;
            if (itemList != null)
            {
                if (itemList.Count > 0)
                {
                    modelAccountInfo.AccountNumber = itemList[0].AcctIntId;//.Account.AccountNumber;
                    modelAccountInfo.Holder = itemList[0].Account.Holder;
                    modelAccountInfo.CompanyName = itemList[0].Account.CompanyName;
                    modelAccountInfo.Category = itemList[0].Account.Category;
                    modelAccountInfo.MarketCode = itemList[0].Account.MarketCode;
                    modelAccountInfo.Address = itemList[0].Account.Address;
                    modelAccountInfo.SubscribeNo = itemList[0].Account.SubscribeNo;
                    modelAccountInfo.SubscribeNoResets = itemList[0].Account.SubscribeNoResets;
                    modelAccountInfo.ActiveDate = itemList[0].Account.ActiveDate; //TODO: Find out what api needed for this
                    modelAccountInfo.Plan = itemList[0].Account.Plan; //TODO: Find out what api needed for this
                    modelAccountInfo.IDNumber = itemList[0].Account.IDNumber;
                    modelAccountInfo.IDType = itemList[0].Account.IDType;

                }
            }
        }
        private void PopulateBillingInfo(InquiryAccountSummaryModel model)
        {
            var billingInfo = model.BillingInfo;
            var billingDetails = GetKenanBillingDetails(model.MSISDN);
            
            billingInfo.TotalOutstanding = billingDetails.TotalDueAmount.FormatCurrency();
            billingInfo.Unbilled = billingDetails.UnbilledAmount.FormatCurrency();
            billingInfo.CurrentBilled = billingDetails.CurrentBillAmount.FormatCurrency();
            billingInfo.DueDate = billingDetails.CurrentBillDueDate == DateTime.MinValue ? "" : billingDetails.CurrentBillDueDate.ToString(dateFormat);
            billingInfo.CreditLimit = billingDetails.CreditLimit.FormatCurrency();
            billingInfo.BalanceForward = billingDetails.BalanceBringForwardAmount.FormatCurrency();
            PopulateLastPayment(billingInfo, billingDetails.LastPayments);
        }

        //UNUSED METHODS
        //private void PopulateComponentInfo(InquiryAccountSummaryModel model)
        //{
        //    var componentInfo = GetKenanComponentInfo(model.MSISDN);
        //}

        private void PopulateEBPSLink(InquiryAccountSummaryModel model)
        {
            var extAcctNo = "";
            //using (var proxy = new KenanServiceProxy())
            //{
            //    Logger.DebugFormat("PopulateEBPSLink: ServiceNo({0})", model.MSISDN);
            //    var resp = proxy.RetrieveExternalAccount(new KenanSvc.RetrieveExtAccDetailsRequest()
            //    {
            //        ServiceNo = model.MSISDN
            //    });

            //    extAcctNo = resp.ExtAcctNo;
            //}

             retrieveAcctListByICResponse AcctListByICResponse = null;
            if ( Session[SessionKey.PPIDInfo.ToString()] != null)
            {
                AcctListByICResponse = (retrieveAcctListByICResponse) Session[SessionKey.PPIDInfo.ToString()];
            }
            List<Items> itemList = AcctListByICResponse.itemList.Where(a => a.ExternalId == (model.MSISDN)).ToList();
            var modelAccountInfo = model.AccountInfo;
            if (itemList != null)
            {
                if (itemList.Count > 0)
                {
                    extAcctNo = itemList[0].AcctExtId;
                }
            }
            var ebpsUrl = Settings.Default.EBPSUrl;
            var csrUserId = Util.SessionAccess.UserName; //"IcgAgeUser";
            var hash = Util.GetMD5Hash(extAcctNo + csrUserId + Settings.Default.EBPSHashSalt);
            //var hash = Util.GetMD5Hash("814242186" + csrUserId + Settings.Default.EBPSHashSalt);

            model.EBPSLinkUrl = string.Format("{0}?accNo={1}&userId={2}&hash={3}",
                ebpsUrl,
                extAcctNo,
                //"814242186",
                csrUserId,
                hash
                );

            Logger.Info("LaunchEBPSUrl : " + model.EBPSLinkUrl);
        }

        
      

        private KenanSvc.RetrieveBillingDetailsResponse GetKenanBillingDetails(string msisdn)
        {
            KenanSvc.RetrieveBillingDetailsResponse billingDetails = null;

            if (Settings.Default.DevelopmentModeOn)
            {
                billingDetails = GetTestingBillingDetails();
            }
            else
            {
                using (var proxy = new KenanServiceProxy())
                {
                    var request = new KenanSvc.RetrieveBillingDetailsRequest
                    {
                        MSISDN = msisdn,
                        TransactionType = KenanSvc.TransactionType.Msisdn,
                    };

                    billingDetails = proxy.RetrieveBillingDetails(request);
                    if (!billingDetails.Success)
                        throw new ApplicationException(billingDetails.Message);
                }
            }

            return billingDetails;
        }

        //UNUSED METHODS
        //private KenanSvc.RetrieveComponentInfoResponse GetKenanComponentInfo(string msisdn)
        //{
        //    KenanSvc.RetrieveComponentInfoResponse componentInfo = null;

        //    if (Settings.Default.DevelopmentModeOn)
        //    {
        //        componentInfo = GetTestingComponentInfo();
        //    }
        //    else
        //    {
        //        using (var proxy = new KenanServiceProxy())
        //        {
        //            var request = new KenanSvc.RetrieveComponentInfoRequest
        //            {
        //                ExternalID = msisdn,
        //                SubscriberNo = "",
        //                SubscriberNoResets = ""
        //            };

        //            componentInfo = proxy.RetrieveComponentInfo(request);
        //            //if (!componentInfo.Success)
        //            //    throw new ApplicationException(componentInfo.Message);
        //        }
        //    }

        //    return componentInfo;
        //}

        private void PopulateLastPayment(InquiryBilling billingInfo, IEnumerable<KenanSvc.PaymentDetail> payments)
        {
            var orderedPayments = payments.OrderByDescending(a => a.Date).ToArray();

            for (int i = 0; i < 3 && i < orderedPayments.Length; i++)
            {
                var lastPayment = orderedPayments[i];
                var paymentHistory = new InquiryPaymentHistory
                {
                    Amount = lastPayment.Amount.FormatCurrency(),
                    TransactionDate = lastPayment.Date == DateTime.MinValue ? "" : lastPayment.Date.ToString(dateFormat)
                };

                switch (i)
                {
                    case 0:
                        billingInfo.LastPayment = paymentHistory;
                        break;

                    case 1:
                        billingInfo.Last2ndPayment = paymentHistory;
                        break;

                    case 2:
                        billingInfo.Last3rdPayment = paymentHistory;
                        break;

                    default:
                        break;
                }
            }
        }

        //UNUSED METHODS
        //private M2KSvc.Account GetTestingAccount()
        //{
        //    var account = new M2KSvc.Account
        //    {
        //        AccountCategory = 4,
        //        MarketCode = 32,
        //        AccountNo = 146424614,
        //        LastName = "NOR AZRI BIN NOR AZIZAN",
        //        Address1 = "NO 22 JALAN CONGKAK 11/1D",
        //        Address2 = "SEKSYEN 11",
        //        City = "Shah Alam",
        //        State = "Selangor",
        //        Zip = "40100",
        //        extendedData = new M2KSvc.ExtendedData[] 
        //        { 
        //            new M2KSvc.ExtendedData
        //            {
        //                Id = "930826055237",
        //                DisVal = "New IC",
        //                Type = 9
        //            }
        //        }
        //    };

        //    return account;
        //}

        private KenanSvc.RetrieveBillingDetailsResponse GetTestingBillingDetails()
        {
            var billingDetails = new KenanSvc.RetrieveBillingDetailsResponse
            {
                BalanceBringForwardAmount = 0.50,
                CreditLimit = 200,
                CurrentBillAmount = 160,
                CurrentBillDueDate = DateTime.ParseExact("20120901", "yyyyMMdd", null),
                LastPayments = new KenanSvc.PaymentDetail[] 
                { 
                    new KenanSvc.PaymentDetail
                    {
                        Date = DateTime.ParseExact("20120716", "yyyyMMdd", null),
                        Amount = 120.50
                    }, 
                    new KenanSvc.PaymentDetail
                    {
                        Date = DateTime.ParseExact("20120620", "yyyyMMdd", null),
                        Amount = 98.50
                    },
                    new KenanSvc.PaymentDetail
                    {
                        Date = DateTime.ParseExact("20120518", "yyyyMMdd", null),
                        Amount = 103.60
                    }
                },
                TotalDueAmount = 98.50,
                UnbilledAmount = 98.50
            };

            return billingDetails;
        }

        //UNUSED METHODS
        //private KenanSvc.RetrieveComponentInfoResponse GetTestingComponentInfo()
        //{
        //    return new KenanSvc.RetrieveComponentInfoResponse();
        //}

        public ActionResult Services(string msisdn, string subscribeNo, string subscribeNoResets)
        {
            var viewModel = new InquiryServicesModel
            {
                MSISDN = msisdn,
                SubscribeNo = subscribeNo,
                SubscribeNoResets = subscribeNoResets
            };

            try
            {
                PopulateViewModel(viewModel);
            }
            catch(Exception ex)
            {
                LogExceptions(ex);
                throw ex;
            }

            return View(viewModel);
        }

        private void PopulateViewModel(InquiryServicesModel model)
        {
            //var componentInfo = GetComponentInfo(model);
            //if (componentInfo.Success)
            //{
            //    model.Contracts = componentInfo.Contracts.Select(a =>
            //        new InquiryContract
            //        {
            //            ContractID = a.ContractID,
            //            ContractName = a.ContractName,
            //            ContractType = a.ContractType,
            //            StartDate = a.StartDate.ToString(dateFormat),
            //            EndDate = a.EndDate.ToString(dateFormat)
            //        }).ToArray();

            //    model.VAS = componentInfo.Components.Select(a =>
            //        new InquiryVAS
            //        {
            //            ActiveDate = a.ActivationDate.ToString(dateFormat),
            //            ComponentDescription = a.ComponentDesc,
            //            ComponentID = a.ComponentID
            //        }).ToArray();
            //}
            IList<Online.Registration.Web.SubscriberICService.PackageModel> listComponentInfo = GetComponentInformation(model);
            SmartRegistrationServiceProxy proxy = new SmartRegistrationServiceProxy();
            IEnumerable<string> lstContracts = proxy.GetContractsList().ToList().Where(cd => cd.IsDeviceContract == 1).Select(c => c.KenanCode);

            List<Online.Registration.Web.SubscriberICService.PackageModel> componentInfo = new List<Online.Registration.Web.SubscriberICService.PackageModel>();

            if (listComponentInfo != null && listComponentInfo.Count > 0)
            {
                if (listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("mandatory")).FirstOrDefault() != null)
                    componentInfo.Add(listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("mandatory")).FirstOrDefault());
                if (listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("main")).FirstOrDefault() != null)
                    componentInfo.Add(listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("main")).FirstOrDefault());
                if (listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("3g")).FirstOrDefault() != null)
                    componentInfo.Add(listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("3g")).FirstOrDefault());
                if (listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("discount")).FirstOrDefault() != null)
                    componentInfo.Add(listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("discount")).FirstOrDefault());
                if (listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("penalty")).FirstOrDefault() != null)
                    componentInfo.Add(listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("penalty")).FirstOrDefault());

                foreach (var pkginfo in componentInfo)
                {
                    listComponentInfo.Remove(pkginfo);
                }
            }

            //listComponentInfo.Remove(listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("mandatory")).FirstOrDefault());
            //listComponentInfo.Remove(listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("main")).FirstOrDefault());
            //listComponentInfo.Remove(listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("3g")).FirstOrDefault());
            //listComponentInfo.Remove(listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("discount")).FirstOrDefault());
            //listComponentInfo.Remove(listComponentInfo.Where(p => p.PackageDesc.ToLower().Contains("penalty")).FirstOrDefault());
            if (listComponentInfo!=null && listComponentInfo.Count > 0)
            {
                foreach (Online.Registration.Web.SubscriberICService.PackageModel pkg in listComponentInfo)
                {
                    componentInfo.Add(pkg);
                }
            }

            if (componentInfo!=null && componentInfo.Count > 0)
            {
                List<Components> UniqComponents = new List<Components>();
                foreach (var item1 in componentInfo)
                        {
                            for (int i = 0; i < item1.compList.Count; i++)
                            {
                                UniqComponents.Add(item1.compList[i]);

                            }

                        }
                        UniqComponents = UniqComponents.GroupBy(a => a.componentId).Select(a => a.Last()).ToList();
               IList<PackageModel> uniqeList1 = componentInfo.GroupBy(a => a.PackageID).Select(a => a.Last()).ToList();
               List<InquiryContract> contracts = new List<InquiryContract>();
               foreach (var contract in uniqeList1)
               {

                   for (int i = 0; i < contract.compList.Count; i++)
                   {
                       if (lstContracts.Contains(contract.compList[i].componentId))
                       {
                           InquiryContract a =
                            new InquiryContract
                            {
                                ContractID = contract.compList[i].componentId,
                                ContractName = contract.compList[i].componentDesc,
                                //ContractType = contract.compList[i].ContractType,
                                StartDate = DateTime.ParseExact(contract.compList[i].componentActiveDt.Substring(0, 8), "yyyyMMdd",
System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy"),
                                EndDate = DateTime.ParseExact(contract.compList[i].componentActiveDt.Substring(0, 8), "yyyyMMdd",
System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy")
                            };
                           contracts.Add(a);
                       }
                   }
               }
               model.Contracts = contracts;

               List<InquiryVAS> vases = new List<InquiryVAS>();
               foreach (var item3 in UniqComponents)
               {
                   if (!lstContracts.Contains(item3.componentId))
                   {
                       InquiryVAS a = new InquiryVAS
                    {
                        ActiveDate = DateTime.ParseExact(item3.componentActiveDt.Substring(0, 8), "yyyyMMdd",
System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy"),
                        ComponentDescription = item3.componentDesc,
                        ComponentID = item3.componentId
                    };
                       vases.Add(a);
                   }
               }
               model.VAS = vases;
            }
        }

        //Unused Methods

        //private KenanSvc.RetrieveComponentInfoResponse GetComponentInfo(InquiryServicesModel model)
        //{
        //    KenanSvc.RetrieveComponentInfoResponse response = null;

        //    if (Settings.Default.DevelopmentModeOn)
        //    {
        //        response = new KenanSvc.RetrieveComponentInfoResponse
        //        {
        //            Components = new KenanSvc.Vas[] 
        //            { 
        //                new KenanSvc.Vas
        //                {
        //                    ActivationDate = DateTime.ParseExact("20120712", "yyyyMMdd", null),
        //                    ComponentDesc = "Data Component ABC",
        //                    ComponentID = "12345"
        //                },
        //                new KenanSvc.Vas
        //                {
        //                    ActivationDate = DateTime.ParseExact("20120712", "yyyyMMdd", null),
        //                    ComponentDesc = "MMS Component",
        //                    ComponentID = "23456"
        //                }
        //            },
        //            Contracts = new KenanSvc.Contract[] 
        //            { 
        //                new KenanSvc.Contract
        //                {
        //                    ContractID = "54321",
        //                    ContractName = "iData 55 x 12",
        //                    ContractType = "Data Contract",
        //                    StartDate = DateTime.ParseExact("20120712", "yyyyMMdd", null),
        //                }
        //            }
        //        };
        //    }
        //    else
        //    {
        //        using (var proxy = new KenanServiceProxy())
        //        {
        //            var request = new KenanSvc.RetrieveComponentInfoRequest
        //            {
        //                ExternalID = model.MSISDN,
        //                SubscriberNo = model.SubscribeNo,
        //                SubscriberNoResets = model.SubscribeNoResets
        //            };
        //            response = proxy.RetrieveComponentInfo(request);

        //            //if (!response.Success)
        //            //    throw new ApplicationException(response.Message);
        //        }
        //    }

        //    return response;
        //}

        private IList<Online.Registration.Web.SubscriberICService.PackageModel> GetComponentInformation(InquiryServicesModel model)
        {
            IList<Online.Registration.Web.SubscriberICService.PackageModel> packages=null;
            if (Settings.Default.DevelopmentModeOn)
            {
                //response = new KenanSvc.RetrieveComponentInfoResponse
                //{
                //    Components = new KenanSvc.Vas[] 
                //    { 
                //        new KenanSvc.Vas
                //        {
                //            ActivationDate = DateTime.ParseExact("20120712", "yyyyMMdd", null),
                //            ComponentDesc = "Data Component ABC",
                //            ComponentID = "12345"
                //        },
                //        new KenanSvc.Vas
                //        {
                //            ActivationDate = DateTime.ParseExact("20120712", "yyyyMMdd", null),
                //            ComponentDesc = "MMS Component",
                //            ComponentID = "23456"
                //        }
                //    },
                //    Contracts = new KenanSvc.Contract[] 
                //    { 
                //        new KenanSvc.Contract
                //        {
                //            ContractID = "54321",
                //            ContractName = "iData 55 x 12",
                //            ContractType = "Data Contract",
                //            StartDate = DateTime.ParseExact("20120712", "yyyyMMdd", null),
                //        }
                //    }
                //};
            }
            else
            {
                using (var icserviceobj = new SubscriberICServiceClient())
                {
                               packages = icserviceobj.retrievePackageDetls(model.MSISDN,
                               System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"],
                               userName:
                               Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName, password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword, postUrl:
                               "");
                }
            }
            return packages;
           // return response;
        }

        private void LogExceptions(Exception ex)
        {
            /* BEGIN  Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/
            string strErrorMsg = Util.LogException(ex);
            string actionName = this.ControllerContext.RouteData.Values["action"] != null ? this.ControllerContext.RouteData.Values["action"].ToString() : "";
            string controllerName = this.ControllerContext.RouteData.Values["controller"] != null ? this.ControllerContext.RouteData.Values["controller"].ToString() : "";
            Logger.Error("Controller: " + controllerName + "\n" + " Action:" + actionName + "\n" + " Error:" + strErrorMsg, ex);
            /* END Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/

        }
    }
}
