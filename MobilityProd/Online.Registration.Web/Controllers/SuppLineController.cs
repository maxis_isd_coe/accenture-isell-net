﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using log4net;
using Online.Registration.DAL;
using Online.Registration.DAL.Models;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.ContentRepository;
using Online.Registration.Web.Helper;
using Online.Registration.Web.KenanSvc;
using Online.Registration.Web.Models;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.Properties;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.ViewModels;
using Online.Web.Helper;
using SNT.Utility;

namespace Online.Registration.Web.Controllers
{
	[CustomErrorHandlerAttr(View = "GenericError")]
	[SessionAuthenticationAttribute]
	public class SuppLineController : Controller
	{
		#region Member Declarations
		private static readonly ILog Logger = LogManager.GetLogger(typeof(RegistrationController));
		private string ProcessingMsisdn = string.Empty;
		private bool sharingQuotaFeature = ConfigurationManager.AppSettings["SharingSupplementaryFlag"].ToBool();
		#endregion


		#region Action Methods


		[Authorize]
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public ActionResult CheckMISMLines(string msisdn)
		{
			string Message = string.Empty;
			PrinSupEaiHeader header = null;
			retrievegetPrinSuppResponse response = null;
			header = null;
			response = null;
			bool value = false;
			string ExternalID = string.Empty;
			string tblId = string.Empty;
			if (msisdn != null)
			{
				string[] strAccountDtls = msisdn.Split(',');
				ExternalID = strAccountDtls[1].ToString2();
				tblId = strAccountDtls[4].ToString2();
			}
			if (tblId == "SeconList")
			{
				using (var PrinSupproxy = new PrinSupServiceProxy())
				{

					var req = new SubscribergetPrinSuppRequest();
					req.eaiHeader = header;
					req.msisdn = ExternalID;
					DateTime dtReq = DateTime.Now;

					response = PrinSupproxy.getPrinSupplimentarylines(req);
					var count = response.itemList.ToList().Count();
					if (count != 0)
					{
						value = true;
						Message = "This customer unable to add supplementory Lines";
					}
				}
			}
			else
			{
				using (var PrinSupproxy = new PrinSupServiceProxy())
				{

					var req = new SubscribergetPrinSuppRequest();
					req.eaiHeader = header;
					req.msisdn = ExternalID;
					DateTime dtReq = DateTime.Now;

					response = PrinSupproxy.getPrinSupplimentarylines(req);
					var count = response.itemList.ToList().Count();
					if (count != 0)
					{
						value = true;
						Message = "This customer unable to add supplementory Lines";
					}
				}
			}


			return Json(new { Success = value, Message });
		}

		[Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
		public ActionResult SecondaryLinePlan(int? type, bool isBack = false)
		{
			Session["PrimarySim"] = null;
			Session[SessionKey.SelectedComponents.ToString()] = null;
			Session["MobileRegType"] = "Planonly";
			if (type > 0)
				ResetAllSession(type);
			if (!isBack)
				WebHelper.Instance.ClearSubLineSession();
			Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(fromSubline: false);
			var sublinePkgVM = new SublinePackageVM();
			sublinePkgVM.PackageVM = GetAvailablePackagesForSecondaryLine(false);
			sublinePkgVM.SelectedMobileNumber = Session["RegMobileSub_MobileNo"] == null ? new List<string>() : (List<string>)Session["RegMobileSub_MobileNo"];
			sublinePkgVM.NoOfDesireMobileNo = Properties.Settings.Default.NoOfDesireMobileNo;

			Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(false);
			Session["VasGroupIds_Seco"] = null;
			PersonalDetailsVM objCurDetails = new PersonalDetailsVM();

			sublinePkgVM.PersonalDetailsVM = objCurDetails;
			return View(sublinePkgVM);
		}

		[Authorize]
		[HttpPost]
		public ActionResult SecondaryLinePlan(FormCollection collection, SublinePackageVM sublinePkgVM)
		{
			try
			{
				if (collection["submit1"].ToString2().ToUpper() == "NEXT")
				{
					// selected different Sub Plan
					if (Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt() != sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID)
					{
						Session["RegMobileReg_PkgPgmBdlPkgCompID"] = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
						Session["SelectedPlanID"] = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
					}

					var mobileNos = new List<string>();

					for (int i = 0; i < Properties.Settings.Default.NoOfDesireMobileNo; i++)
					{
						mobileNos.Add(collection["txtMobileNo_" + i].ToString2());
					}

					Session["RegMobileSub_MobileNo"] = mobileNos;
				}
				else if (collection["submit1"].ToString2().ToLower() == "back")
				{
					return RedirectToAction("AddSuppLine");
				}
				else
				{
					return RedirectToAction("SecondaryLineVAS");
				}
			}
			catch (Exception ex)
			{
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
				throw ex;
			}
			return RedirectToAction("SecondaryLineVAS");
		}


		[Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DC,MREG_DAI,MREG_DAC")]
		public ActionResult SecondaryLineVAS()
		{

			ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
			ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
			Session["RegMobileReg_OrderSummary"] = null;
			Session["RegMobileReg_SublineSummary"] = null;
			Session["RegMobileReg_VasNames"] = null;
			Session["RegMobileReg_VasIDs"] = null;
			Session["RegMobileReg_SublineVM"] = null;
			Session["RegMobileReg_VirtualMobileNo_Sec"] = null;

			if (Session["PPID"].ToString2() != string.Empty)
			{
				if (!ReferenceEquals(Session["RedirectURL"], null))
				{
					Session.Contents.Remove("RedirectURL");
				}

				GetRegActionStepForSecondaryPlan(SecondaryLineRegistrationSteps.Vas.ToInt());
				if (Session["RegMobileReg_PkgPgmBdlPkgCompID"] == null || Session["RegMobileReg_PkgPgmBdlPkgCompID"] == string.Empty)
				{
					Session["RegMobileReg_PkgPgmBdlPkgCompID"] = Session["GuidedSales_PkgPgmBdlPkgCompID"];
				}
				Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(fromSubline: false);
				string MobileRegType = "planonly";

				OrderSummaryVM orderVM = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];

				Int32 Selecpgmid = (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.SelectedPgmBdlPkgCompID != null) ? orderVM.SelectedPgmBdlPkgCompID : Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt();

				Lstvam = WebHelper.Instance.GetAvailablePackageComponents(Selecpgmid, ref ProcessingMsisdn, isMandatory: false, fromSuppline: true);

				Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(fromSubline: false);
				var selectedCompIDs = new List<int>();
				List<AvailableVAS> lstvas = new List<AvailableVAS>();


				lstvas.AddRange(orderVM.VasVM.MandatoryVASes);

				for (int i = 0; i < orderVM.Sublines.Count(); i++)
				{
					for (int j = 0; j < orderVM.Sublines[i].SelectedVasNames.Count; j++)
					{
						lstvas.Add(new AvailableVAS() { PgmBdlPkgCompID = orderVM.Sublines[i].SelectedVasIDs[j], ComponentName = orderVM.Sublines[i].SelectedVasNames[j].ToString2() });
					}

				}


				for (int i = 0; i < lstvas.Count(); i++)
				{
					selectedCompIDs.Add(lstvas[i].PgmBdlPkgCompID);
				}



				Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);

				Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);

				Session["RegMobileReg_VasNames"] = orderVM.VasVM.MandatoryVASes;

				/* For Mandatory packages */
				Session["vasmandatoryids"] = string.Empty;
				using (var proxy = new CatalogServiceProxy())
				{

					BundlepackageResp respon = new BundlepackageResp();

					respon = proxy.GetMandatoryVas(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));

					for (int i = 0; i < respon.values.Count(); i++)
					{

						if (respon.values[i].Plan_Type == Properties.Settings.Default.Master_Component)
						{
							Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false, fromSuppLine: true);

							Session["intmandatoryidsval"] = respon.values[i].BdlDataPkgId;

							Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
						}

						if (respon.values[i].Plan_Type == Properties.Settings.Default.CompType_DataCon)
						{
							Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false, fromSuppLine: true);
							List<int> Finalintvalues = null;
							if (Session["RegMobileReg_Type"].ToInt() == 1)
							{
								List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

								string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

								Finalintvalues = values.Where(mid => mid.Modelid == "0").Select(a => a.BdlDataPkgId).ToList();
                                if (Finalintvalues.Count() == 0)
                                {
                                    Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();
                                }

							}

							List<PkgDataPlanId> valuesplan = proxy.getFilterplanvalues(2).values.ToList();

							Finalintvalues = valuesplan.Select(a => a.BdlDataPkgId).ToList();

							var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

															 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

															 select new AvailableVAS()
															 {
																 ComponentName = e.ComponentName,
																 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
																 Price = e.Price,
																 Value = e.Value,
																 GroupId = e.GroupId,
																 GroupName = e.GroupName,
																 KenanCode = e.KenanCode
															 };


							Session["intdataidsval"] = respon.values[i].BdlDataPkgId;

							Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts);
						}
						/*Extra packages*/
						if (respon.values[i].Plan_Type == Properties.Settings.Default.Extra_Component)
						{
							Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false, fromSuppLine: true);

							List<int> Finalintvalues = null;
							if (Session["RegMobileReg_Type"].ToInt() == 1)
							{
								List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

								string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

								Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

							}

							List<PkgDataPlanId> valuesplan = proxy.getFilterplanvalues(2).values.ToList();

							Finalintvalues = valuesplan.Select(a => a.BdlDataPkgId).ToList();

							var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

															 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

															 select new AvailableVAS()
															 {
																 ComponentName = e.ComponentName,
																 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
																 Price = e.Price,
																 Value = e.Value,
																 GroupId = e.GroupId,
																 GroupName = e.GroupName,
																 KenanCode = e.KenanCode
															 };


							Session["intextraidsval"] = respon.values[i].BdlDataPkgId;

							Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);

						}
						//Pramotional Packages
						if (respon.values[i].Plan_Type == Properties.Settings.Default.Pramotional_Component)
						{
							Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
							#region COmmented filterComponents for Pramotional components
							//List<int> Finalintvalues = null;
							//if (Session["RegMobileReg_Type"].ToInt() == 1)
							//{
							//    List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();
							//    string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();
							//    Finalintvalues = values.Select(a => a.BdlDataPkgId).Distinct().ToList();
							//    Lstvam.DeviceType = "Seco_NoDevice";
							//}
							//else if (Session["RegMobileReg_Type"].ToInt() == 2)
							//{
							//    List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
							//    Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
							//}
							//var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

							//                                 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

							//                                 select new AvailableVAS
							//                                 {
							//                                     ComponentName = e.ComponentName,
							//                                     PgmBdlPkgCompID = e.PgmBdlPkgCompID,
							//                                     Price = e.Price,
							//                                     Value = e.Value,
							//                                     GroupId = e.GroupId,
							//                                     GroupName = e.GroupName,
							//                                     KenanCode = e.KenanCode
							//                                 }; 
							#endregion
							Session["intpramotionidsval"] = respon.values[i].BdlDataPkgId;
							//MNP MULTIPORT RELATED CHANGES
							if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
							{
								if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
								{
									((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intpramotionidsval = respon.values[i].BdlDataPkgId;
								}
							}

							Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
						}

					}

				}


				if (MobileRegType == "Planonly")
				{
					Lstvam.ContractVASes.Clear();

				}
				Session["SecondaryLineVas"] = Lstvam2;

				if (Lstvam2 != null)
				{
					return View(Lstvam2);
				}
				else
				{
					Lstvam2 = (ValueAddedServicesVM)Session["SecondaryLineVas"];
					return View(Lstvam2);
				}

			}
			else
			{
				Session["RedirectURL"] = "Registration/SelectVAS";
				return RedirectToAction("Index", "Home");
			}
		}


		[Authorize]
		[HttpPost]
		public ActionResult SecondaryLineVAS(FormCollection collection, ValueAddedServicesVM vasVM)
		{
			Session["ExtratenMobilenumbers"] = vasVM.ExtraTenMobileNumbers;
			Session["RegMobileReg_TabIndex"] = vasVM.TabNumber;
			var selectedVasIDs = string.Empty;

			#region fxDependency Check
			if (vasVM.TabNumber == 3)
			{
				List<FxDependChkComps> Comps = new List<FxDependChkComps>();
				using (var Proxy = new RegistrationServiceProxy())
				{
					string strCompIds = string.Empty;
					if (vasVM.SelectedVasIDs.ToString2() != string.Empty)
					{
						strCompIds = vasVM.SelectedVasIDs;
						Session[SessionKey.SelectedComponents.ToString()] = vasVM.SelectedVasIDs;

					}
					if (Session["MandatoryVasIds"].ToString2().Length > 0)
					{
						strCompIds = strCompIds + "," + Session["MandatoryVasIds"].ToString2();
					}
					strCompIds += "," + Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
					if (vasVM.OfferId != null)
					{
						Session["OfferId"] = vasVM.OfferId.ToString();
					}
					//for adding contract id
					if (vasVM.SelectedContractID.ToString2().Length > 0)
					{
						strCompIds += "," + vasVM.SelectedContractID;
					}
					// added by Nreddy by pass the dependece check method when click on back button
					if (collection["TabNumber"] != "2")
					{
						Comps = Proxy.GetAllComponentsbySecondaryPlanId(Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(), strCompIds);
					}
				}


				List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();

				foreach (var comp in Comps)
				{
					Online.Registration.Web.SubscriberICService.componentList component = new componentList();
					component.componentIdField = comp.ComponentKenanCode;
					component.packageIdField = comp.KenanCode;
					componentList.Add(component);
					if (comp.DependentKenanCode != -1)
					{
						component = new componentList();
						component.componentIdField = comp.DependentKenanCode.ToString();
						component.packageIdField = comp.KenanCode;

						componentList.Add(component);
					}
				}
				if (componentList.Count > 0)
				{
					// Drop 5
					retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
					string pbpcID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
					typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, pbpcID: pbpcID);

					typeChkFxVasDependencyResponse response = _retrieveServiceInfoProxy.ChkFxVasDependency(request);
					ValueAddedServicesVM vas = (ValueAddedServicesVM)Session["SecondaryLineVas"];
					if (response.mxsRuleMessagesField != null && response.mxsRuleMessagesField.Count > 0)
					{
						foreach (string errmsg in response.mxsRuleMessagesField)
						{
							ModelState.AddModelError(string.Empty, errmsg);
						}
						return View(vas);
					}
					else if (response.msgCodeField == "3")
					{
						ModelState.AddModelError(string.Empty, "OPF system error - Unable to fetch data.");   // Need to change msg
						return View(vas);
					}
					else if (response.msgCodeField != "0")
					{
						ModelState.AddModelError(string.Empty, "Selected components are not available.");   // Need to change msg
						return View(vas);
					}
					// }
				}
			}
			#endregion
			Session["VasGroupIds_Seco"] = vasVM.hdnGroupIds;
			if (vasVM.SelectedVasIDs != null)
			{
				if (vasVM.SelectedVasIDs != null && !Regex.IsMatch(vasVM.SelectedVasIDs, @"^,+$") && vasVM.SelectedVasIDs != "[object XMLDocument]")
				{
					selectedVasIDs = vasVM.SelectedVasIDs;
					Session["RegMobileReg_DataPkgID"] = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
					Session[SessionKey.SelectedComponents.ToString()] = vasVM.SelectedVasIDs;
				}
				else
				{
					if (!ReferenceEquals(Session["RegMobileReg_DataPkgID"], null))
					{
						Session.Contents.Remove("RegMobileReg_DataPkgID");
					}
					if (!ReferenceEquals(Session["RegMobileReg_VasIDs"], null))
					{
						Session.Contents.Remove("RegMobileReg_VasIDs");
					}
				}
			}
			try
			{
				if (vasVM.TabNumber == (int)SecondaryLineRegistrationSteps.PersonalDetails && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) || !string.IsNullOrEmpty(vasVM.SelectedContractID)))
				{
					if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
					{
						#region VLT ADDED CODE
						//Contract check for existing users
						//Retrieve data from session
						#endregion VLT ADDED CODE
						if (!string.IsNullOrEmpty(Session["RegMobileReg_ContractID"].ToString2()))
							if (vasVM.SelectedVasIDs != null)
								selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session["RegMobileReg_ContractID"].ToString2()), string.Empty);

						/*Condition added by chetan on 27042013*/
						if (selectedVasIDs != string.Empty)
						{
							if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
							{
								selectedVasIDs += "," + vasVM.SelectedContractID + ",";
							}
						}
						else
						{
							selectedVasIDs += vasVM.SelectedContractID + ",";
						}
					}

					vasVM.SelectedVasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
					Session["RegMobileReg_VasIDs"] = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
					Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID;

					using (var proxy = new CatalogServiceProxy())
					{
						List<int> lstContracts = WebHelper.Instance.CommaSplitToList(Session["RegMobileReg_ContractID"].ToString2());
						Session[SessionKey.RegMobileReg_DataplanID.ToString()] = proxy.GettllnkContractDataPlan(lstContracts);
					}


				}
			}
			catch (Exception ex)
			{
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
				throw ex;
			}

			if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
			{
				if (!string.IsNullOrEmpty(Session["RegMobileReg_ContractID"].ToString2()))
					selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session["RegMobileReg_ContractID"].ToString2()), string.Empty);

				/*Condition added by chetan on 27042013*/
				if (selectedVasIDs != string.Empty)
				{
					if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
					{
						selectedVasIDs += "," + vasVM.SelectedContractID + ",";
					}
				}
				else
				{
					selectedVasIDs += vasVM.SelectedContractID + ",";
				}
			}

			vasVM.SelectedVasIDs = selectedVasIDs;

			Session["RegMobileReg_VasIDs"] = selectedVasIDs;
			Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID;

			var mobileNosResponse = new MobileNoSelectionResponse();
			List<string> mobileNos = new List<string>();
			return RedirectToAction(GetRegActionStepForSecondaryPlan(vasVM.TabNumber.ToInt()));
		}

		[Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
		public ActionResult SecondaryLinePersonalDetails()
		{

			Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(fromSubline: false);

			OrderSummaryVM orderVM = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];

			var idCardTypeID = Session[SessionKey.RegMobileReg_IDCardType.ToString()] != null ? Session[SessionKey.RegMobileReg_IDCardType.ToString()] : null;
			var personalDetailsVM = new PersonalDetailsVM();
			if ((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"] != null)
			{
				personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
			}

			personalDetailsVM.Customer.IDCardTypeID = idCardTypeID.ToInt();
			personalDetailsVM.Customer.IDCardNo = Session[SessionKey.RegMobileReg_IDCardNo.ToString()] == null ? string.Empty : Session[SessionKey.RegMobileReg_IDCardNo.ToString()].ToString2();

			return View(personalDetailsVM);
		}

		[HttpPost]
		[Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
		public ActionResult SecondaryLinePersonalDetails(PersonalDetailsVM personalDetailsVM)
		{

			try
			{
				//if (!ModelState.IsValid)
				//    return View(personalDetailsVM);

				if (personalDetailsVM.TabNumber == (int)SecondaryLineRegistrationSteps.Summary)
				{
					personalDetailsVM.Customer.IDCardTypeID = personalDetailsVM.InputIDCardTypeID.ToInt();
					personalDetailsVM.Customer.IDCardNo = personalDetailsVM.InputIDCardNo;


					personalDetailsVM.Customer.NationalityID = personalDetailsVM.InputNationalityID;
					Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;
					Session["RegMobileReg_TabIndex"] = personalDetailsVM.TabNumber;
					Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

					//WebHelper.Instance.BusinessValidationChecks(personalDetailsVM, this.GetType());
				   // string validationResult = WebHelper.Instance.breValidate();
					//if (!string.IsNullOrEmpty(validationResult))
					//{
					//    TempData["ErrorMessage"] = validationResult;
						return View(personalDetailsVM);
					//}

					Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(fromSubline: false);

					OrderSummaryVM orderVM = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];

					//if (personalDetailsVM.Customer.PayModeID == Util.GetPaymentModeID(PAYMENTMODE.AutoBilling))
					//{
					//    if (!ValidateCardDetails(personalDetailsVM.Customer))
					//    {
							return View(personalDetailsVM);
						//}
					//}
				}
				else
				{
					personalDetailsVM.TabNumber = (int)SecondaryLineRegistrationSteps.Vas;
				}

			}
			catch (Exception ex)
			{
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
				throw ex;
			}
			return RedirectToAction(GetRegActionStepForSecondaryPlan(personalDetailsVM.TabNumber));
		}


        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_HQ,MREG_DAB,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DSK,MREG_DCH,MREG_DIC,MREG_DAI,MREG_DAC")]
		[DoNoTCache]
		public ActionResult SecondaryLineRegSummary(int? regID)
		{
			Session["SelectedPlanID"] = null;

			if (ReferenceEquals(Session["RegMobileReg_PersonalDetailsVM"], null))
			{
                if (!Request.UrlReferrer.AbsolutePath.Contains("/Registration/CDPUDashBoard") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/CDPUDashBoard") && !Request.UrlReferrer.AbsolutePath.Contains("SecondaryLineRegAccCreated") && !Request.UrlReferrer.AbsolutePath.Contains("Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("StoreKeeper") && !Request.UrlReferrer.AbsolutePath.Contains("SecondaryLineRegSuccess") &&
                    !Request.UrlReferrer.AbsolutePath.Contains("/Registration/SuperVisorForWaiveOff") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/OrderStatusReport"))
				{
					return RedirectToAction("Index", "Home");
				}
			}

			var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            var regDetails = new Online.Registration.DAL.RegistrationDetails();
			var orderSummary = new OrderSummaryVM();
			var regMdlGrpModel = new RegMdlGrpModel();
			var mdlGrpModelIDs = new List<int>();
			var mainLinePBPCIDs = new List<int>();
			var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
			var suppLines = new List<RegSuppLine>();
			var reg = new DAL.Models.Registration();
			var objWhiteListDetailsResp = new WhiteListDetailsResp();
			int paymentstatus = -1;
			#region VLT ADDED CODE 21stFEB for Email functionality
			Session["RegID"] = regID;
			#endregion VLD ADDED CODE 21stFEB for Email functionality
			/*Chetan added for displaying the reson for failed transcation*/
			var KenanLogDetails = new DAL.Models.KenanaLogDetails();
			if (regID != null && regID.ToInt() > 0)
			{
				WebHelper.Instance.ClearRegistrationSession();
				personalDetailsVM = new PersonalDetailsVM()
				{
					RegID = regID.Value.ToInt(),
				};

				Session["RegMobileReg_PhoneVM"] = null;

				using (var proxy = new RegistrationServiceProxy())
				{
                    regDetails = proxy.GetRegistrationFullDetails(personalDetailsVM.RegID);

                    if (regDetails.Customers.FirstOrDefault().IsVIP == 1 && !Roles.IsUserInRole("MREG_SV"))
                    {
                        Session["VIPFailMsg"] = true;
                        return RedirectToAction("StoreKeeper", "Registration");
                    }
                    
                    reg = proxy.RegistrationGet(regID.Value);


					personalDetailsVM.MSISDN1 = reg.MSISDN1.ToString2();
					/*Chetan added for displaying the reson for failed transcation*/
					KenanLogDetails = proxy.KenanLogDetailsGet(regID.Value);
					if (!ReferenceEquals(KenanLogDetails, null))
					{
						personalDetailsVM.MessageCode = KenanLogDetails.MessageCode.ToInt();
						personalDetailsVM.MessageDesc = KenanLogDetails.MessageDesc.ToString();
					}

					paymentstatus = proxy.PaymentStatusGet(regID.Value);
					if (paymentstatus != -1)
					{
						if (paymentstatus == 0)
						{
							personalDetailsVM.PaymentStatus = "0";
						}
						else
						{
							personalDetailsVM.PaymentStatus = "1";
						}
					}
					else
					{
						personalDetailsVM.PaymentStatus = "0";
					}


					personalDetailsVM.QueueNo = reg.QueueNo;

					personalDetailsVM.Remarks = reg.Remarks;

					personalDetailsVM.BiometricVerify = reg.BiometricVerify;
					personalDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
					personalDetailsVM.BlacklistInternal = reg.InternalBlacklisted;
					personalDetailsVM.SignatureSVG = reg.SignatureSVG;
					personalDetailsVM.CustomerPhoto = reg.CustomerPhoto;
					personalDetailsVM.AltCustomerPhoto = reg.AltCustomerPhoto;
                    personalDetailsVM.RFSalesDT = reg.RFSalesDT;
					personalDetailsVM.Photo = reg.Photo;
					personalDetailsVM.RegSIMSerial = reg.SIMSerial;
					personalDetailsVM.RegIMEINumber = reg.IMEINumber;
                    personalDetailsVM.CapturedIMEINumber = reg.IMEINumber;
					personalDetailsVM.RegTypeID = reg.RegTypeID;
					personalDetailsVM.AgeCheckStatus = reg.AgeCheckStatus;
					personalDetailsVM.DDMFCheckStatus = reg.DDMFCheckStatus;
					personalDetailsVM.AddressCheckStatus = reg.AddressCheckStatus;
					personalDetailsVM.OutStandingCheckStatus = reg.OutStandingCheckStatus;
					personalDetailsVM.TotalLineCheckStatus = reg.TotalLineCheckStatus;
					personalDetailsVM.PrincipleLineCheckStatus = reg.PrincipleLineCheckStatus;
					personalDetailsVM.ContractCheckStatus = reg.ContractCheckStatus;
					personalDetailsVM.UserType = reg.UserType;
					personalDetailsVM.PlanAdvance = reg.PlanAdvance;
					personalDetailsVM.PlanDeposit = reg.PlanDeposit;
					personalDetailsVM.DeviceAdvance = reg.DeviceAdvance;
					personalDetailsVM.DeviceDeposit = reg.DeviceDeposit;
					personalDetailsVM.Discount = reg.Discount;
					personalDetailsVM.KenanAccountNo = reg.KenanAccountNo;
					personalDetailsVM.OutstandingAcs = reg.OutstandingAcs;
					personalDetailsVM.PrePortInCheckStatus = reg.PrePortInCheckStatus;
					personalDetailsVM.MNPServiceIdCheckStatus = reg.MNPServiceIdCheckStatus;
					personalDetailsVM.MOC_Status = reg.MOC_Status;
					personalDetailsVM.Whitelist_Status = reg.Whitelist_Status;

					//Added by ravikiran for getting sim model on 30 jan 2014
					personalDetailsVM.SIMModel = reg.SimModelId.ToString2() == string.Empty ? "0" : reg.SimModelId.ToString();
					//Added by ravikiran for getting sim model on 30 jan 2014 Ends here

					personalDetailsVM.fxAcctNo = reg.fxAcctNo;
					personalDetailsVM.FxSubscrNo = reg.fxSubscrNo;
					personalDetailsVM.FxSubScrNoResets = reg.fxSubScrNoResets;
					personalDetailsVM.CRPType = reg.CRPType;
					Session["Condition"] = personalDetailsVM.CRPType.ToString2();
					//Added by VLT on 13 Apr 2013
					personalDetailsVM.IMPOSFileName = reg.IMPOSFileName;
					//Added by VLT on 13 Apr 2013 ends here

					// RegAccount
					var regAccount = proxy.RegAccountGet(new int[] { reg.ID }).SingleOrDefault();
					if (regAccount != null)
					{
						personalDetailsVM.ServiceActivateErrMsg = regAccount.KenanActivateServiceErrMessage;
					}

					// Registration Status
					var regStatusID = proxy.RegStatusFind(new RegStatusFind()
					{
						RegStatus = new RegStatus()
						{
							RegID = reg.ID
						},
						Active = true
					}).SingleOrDefault();

					personalDetailsVM.Deposite = reg.AdditionalCharges;
					personalDetailsVM.StatusID = proxy.RegStatusGet(new int[] { regStatusID }).SingleOrDefault().StatusID;


					using (var configProxy = new ConfigServiceProxy())
					{
						personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;

					}

					//for cancel reason
					if (personalDetailsVM.StatusID == 21)
					{

						using (var regProxy = new RegistrationServiceProxy())
						{
							string reason = regProxy.CancelReasonGet(personalDetailsVM.RegID);
							personalDetailsVM.MessageDesc = reason;

						}
					}

					ExtraTenGetResp objExtraTenDetails = new ExtraTenGetResp();
					objExtraTenDetails = proxy.GetExtraTenDetails(personalDetailsVM.RegID);
					List<lnkExtraTenDetails> modListExtraTen = new List<lnkExtraTenDetails>();
					personalDetailsVM.ExtraTenDetails = objExtraTenDetails.ExtraTenDetails.ToList();
					if (personalDetailsVM.ExtraTenDetails != null && personalDetailsVM.ExtraTenDetails.Count > 0)
					{
						foreach (var extraTen in personalDetailsVM.ExtraTenDetails)
						{
							if (string.IsNullOrEmpty(extraTen.MsgCode) == true)
							{
								extraTen.MsgCode = "Processing";
							}
							else if (extraTen.MsgCode == "0")
							{
								extraTen.MsgCode = "Success";
							}
							else
							{
								extraTen.MsgCode = "Fail";
							}
							modListExtraTen.Add(extraTen);

						}
						personalDetailsVM.ExtraTenDetails = modListExtraTen;
					}
					personalDetailsVM.ExtraTenMobileNumbers = string.Join(",", objExtraTenDetails.ExtraTenDetails.Where(e => e.Status == "A").Select(e => e.Msisdn).ToList());
					Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;

					var mobileNos = new List<string>();
					mobileNos.Add(reg.MSISDN1);
					mobileNos.Add(reg.MSISDN2);
					Session["RegMobileReg_VirtualMobileNo_Sec"] = string.Empty;
					Session["RegMobileReg_MobileNo"] = null;
					// Customer
					personalDetailsVM.Customer = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

					// Address
					var regAddrIDs = proxy.RegAddressFind(new AddressFind()
					{
						Active = true,
						Address = new Address()
						{
							RegID = regID.Value
						}
					}).ToList();
					personalDetailsVM.Address = proxy.RegAddressGet(regAddrIDs).SingleOrDefault();

					// Personal Details
					Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

					// RegMdlGrpModels
					var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
					{
						RegMdlGrpModel = new RegMdlGrpModel()
						{
							RegID = regID.Value
						},
						Active = true
					}).ToList();
					if (regMdlGrpModelIDs.Count() > 0)
						regMdlGrpModel = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).SingleOrDefault();

					if (regMdlGrpModel != null && regMdlGrpModel.ID == 0)
					{
						Session["RegMobileReg_Type"] = (int)MobileRegType.SecPlan;
					}
					else
					{
						Session["RegMobileReg_MainDevicePrice"] = regMdlGrpModel.Price.ToDecimal();
						Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
						Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;
					}

					if (personalDetailsVM.RegTypeID == (int)MobileRegType.CRP)
						Session["RegMobileReg_Type"] = personalDetailsVM.RegTypeID;

					// RegPgmBdlPkgComponent
					var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
					{
						RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
						{
							RegID = regID.Value
						},
						Active = true
					}).ToList();

					// Main Line PgmBdlPkgComponent
					var regPBPCs = new List<RegPgmBdlPkgComp>();
					if (regPgmBdlPkgCompIDs.Count() > 0)
						regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

					if (regPBPCs.Count() > 0)
						mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

					// Sub Line RegPgmBdlPkgComponent
					suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

					// RegSuppLine
					var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind()
					{
						RegSuppLine = new RegSuppLine()
						{
							RegID = regID.Value
						}
					}).ToList();

					if (suppLineIDs.Count() > 0)
					{
						suppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
					}

                    //<Start> - 20150914 - Samuel - display sales code in summary page
                    //personalDetailsVM.Registration = proxy.RegistrationGet(personalDetailsVM.RegID);
                    personalDetailsVM.RegAttributes = proxy.RegAttributesGetByRegID(personalDetailsVM.RegID);
                    /*using (var proxyOrg = new OrganizationServiceProxy())
                    {
                        if (!ReferenceEquals(personalDetailsVM.Registration, null) && !ReferenceEquals(personalDetailsVM.Registration.CenterOrgID, null) && personalDetailsVM.Registration.CenterOrgID != 0)
                        {
                            personalDetailsVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { personalDetailsVM.Registration.CenterOrgID }).FirstOrDefault();
                        }
                    }*/
                    var salesPerson = regDetails.Regs.FirstOrDefault().SalesPerson;
                    var orgID = regDetails.Regs.FirstOrDefault().CenterOrgID;
                    var outletCode = string.Empty;
                    var salesCode = string.Empty;

                    WebHelper.Instance.generateOutletAndSalesCode(orgID, salesPerson, personalDetailsVM.RegAttributes, out outletCode, out salesCode);
                    personalDetailsVM.OutletCode = outletCode;
                    personalDetailsVM.SalesCode = salesCode;
                    //<End> - 20150914 - Samuel - display sales code in summary page
				}

				if (suppLines.Count() > 0)
					WebHelper.Instance.ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

				using (var proxy = new CatalogServiceProxy())
				{
					string userICNO = Util.SessionAccess.User.IDCardNo;

					if (userICNO != null && userICNO.Length > 0)
					{
						objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
					}
					if (objWhiteListDetailsResp != null)
					{
						if (objWhiteListDetailsResp.Description != null)
						{
							personalDetailsVM._WhiteListDetails.Description = objWhiteListDetailsResp.Description;
						}
					}

					if (regMdlGrpModel.ID > 0)
						Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;


					var mainLinePBPCs = new List<PgmBdlPckComponent>();
					if (mainLinePBPCIDs.Count() > 0)
						mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

					var bpCode = Properties.Settings.Default.Bundle_Package;
					var pcCode = Properties.Settings.Default.Package_Component;
					var DPCode = Properties.Settings.Default.RegType_DevicePlan;
					var MAcode = Properties.Settings.Default.Master_Component;
					var MPcode = Properties.Settings.Default.Main_package;
					var CPCode = Properties.Settings.Default.ComplimentaryPlan;
					var DCCode = Properties.Settings.Default.CompType_DataCon;
					var ECCode = Properties.Settings.Default.Extra_Component;
					var SPCode = Properties.Settings.Default.SecondaryPlan;
					var RCCode = Properties.Settings.Default.Pramotional_Component;
					var CRCode = Properties.Settings.Default.DiscountDataContract_Component;
					if (mainLinePBPCs.Count() > 0)
					{
						var componentList = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode)).ToList();
						if (componentList != null && componentList.Count > 0)
							Session["RegMobileReg_PkgPgmBdlPkgCompID"] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode)).FirstOrDefault().ID;
						else
							Session["RegMobileReg_PkgPgmBdlPkgCompID"] = mainLinePBPCs.Where(a => a.LinkType == SPCode && (a.PlanType == CPCode)).FirstOrDefault().ID;


						var vasIDs = string.Empty;
						//Added by chetan to exclusively check DiscountDataContract_Component as CRCode & || RCCode
						var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == CRCode || a.LinkType == RCCode || a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode).Select(a => a.ID).ToList();

						foreach (var id in pkgCompIDs)
						{
							vasIDs = vasIDs + "," + id;
						}
						Session["RegMobileReg_VasIDs"] = vasIDs;


					}
				}
			}
			if (reg.OfferID != null && reg.OfferID > 0)
			{
				Session["OfferId"] = reg.OfferID;
			}

			var orderSummaryVM = ConstructOrderSummary();


			if (Session["RegMobileReg_PkgPgmBdlPkgCompID"] != null)
			{
				if (Session["regid"] != null)
				{
					if (regID.Value > 0)
					{
						CatalogServiceProxy objCSproxy = new CatalogServiceProxy();
						var deviceproxys = objCSproxy.GetDiscountPriceDetails(orderSummaryVM.ModelID, Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(), ReferenceEquals(Session["RegMobileReg_ContractID"], null) ? 0 : Session["RegMobileReg_ContractID"].ToInt(), 0);
						if (deviceproxys.NewPlanName != null)
						{
							orderSummaryVM.DiscountPrice = deviceproxys.DiscountPrice.ToInt();
							orderSummaryVM.PlanName = deviceproxys.NewPlanName;
							orderSummaryVM.MonthlySubscription = orderSummaryVM.MonthlySubscription - orderSummaryVM.DiscountPrice;
						}
					}

				}
			}


			#region Added by VLT on 21 June 2013 for getting Sim Type
			using (var proxy = new RegistrationServiceProxy())
			{
				if (regID != null && regID.ToInt() > 0)
				{
					DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(regID.ToInt());
					if (lnkregdetails != null)
					{
						//Justification by ravi for waiver cr on sept 26
						personalDetailsVM.Justification = lnkregdetails.Justification.ToString2();
						//Justification by ravi for waiver cr on sept 26 ends here
						personalDetailsVM.ContractCheckCount = lnkregdetails.ContractCheckCount;
                        personalDetailsVM.TotalLineCheckCount = lnkregdetails.TotalLineCheckCount;
						if (lnkregdetails.MismSimCardType != null)
						{
							personalDetailsVM.SecondarySIMType = lnkregdetails.MismSimCardType;
						}

						if (lnkregdetails.SimType != null)
						{
							personalDetailsVM.SimType = lnkregdetails.SimType;

						}
						else
						{
							personalDetailsVM.SimType = "Normal";
						}
					}
					else
					{
						personalDetailsVM.SimType = "Normal";
					}

					/*Active PDPA Commented*/
					/*Changes by chetan related to PDPA implementation*/
					string documentversion = lnkregdetails.PdpaVersion;
					personalDetailsVM.PDPAVersionStatus = documentversion;

					if (!ReferenceEquals(documentversion, null))
					{
						using (var registrationProxy = new RegistrationServiceProxy())
						{
							var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", documentversion);
							personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
						}
					}

				}
				/*Active PDPA Commented*/
				else
				{
					/*Changes by chetan related to PDPA implementation*/
					using (var registrationProxy = new RegistrationServiceProxy())
					{
						var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
						personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
						personalDetailsVM.PDPDVersion = resultdata.Version;
					}
				}
			}
			#endregion



			orderSummaryVM.IMEINumber = reg.IMEINumber;
			orderSummaryVM.SIMSerial = reg.SIMSerial;

			if (regID != null && regID.ToInt() > 0)
			{
				orderSummaryVM.SimType = personalDetailsVM.SimType;
				Session["SimType"] = personalDetailsVM.SimType;
			}

			if (reg.KenanAccountNo != null)
			{ Session["KenanAccountNo"] = reg.KenanAccountNo; }
			else
			{ Session["KenanAccountNo"] = "0"; }
			Session["RegMobileReg_OrderSummary"] = orderSummaryVM;

			#region MOC_Status and Liberization status

			string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session["ExternalID"].ToString2()) ? string.Empty : Session["ExternalID"].ToString2(), regID.ToInt());
			personalDetailsVM.Liberlization_Status = Result[0];
			personalDetailsVM.MOCStatus = Result[1];

			#endregion

            // 29JAN2016 - display bill cycle
            if (regID.ToInt() > 0)
            {
                if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault() != null)
                {
                    personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault().ATT_Value;
                }
                else
                {
                    personalDetailsVM.BillingCycle = "N/A";
                }
            }
            else
            {
                var selectedMsisdn = Session["SelectedMsisdn"].ToString2();
                retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                var selectedAccount = AcctListByICResponse.itemList.Where(c => c.ExternalId == selectedMsisdn).FirstOrDefault();
                var billCycle = (selectedAccount != null && selectedAccount.AccountDetails != null ? selectedAccount.AccountDetails.BillCycle : "N/A");
                personalDetailsVM.BillingCycle = (!string.IsNullOrWhiteSpace(billCycle) ? billCycle : "N/A");
            }

			return View(personalDetailsVM);
		}


		[Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_HQ,MREG_DAB,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DSK,MREG_DCH,MREG_DIC,MREG_DAI,MREG_DAC")]
		[DoNoTCache]
		public ActionResult SecondaryLineRegSummaryNew(int? regID)
		{
            var dropObj = new DropFourObj();
            dropObj.msimIndex = 1;
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
			Session["SelectedPlanID"] = null;
            Session["OrderType"] = (int)MobileRegType.SecPlan;

            if (Session["MobileRegType"] == null)
            {
                Session["MobileRegType"] = MobileRegType.SecPlan;
            }

			retrieveAcctListByICResponse AcctListByICResponse = null;
            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            string selectedMsisdn = string.Empty;

            if (Session["SelectedMsisdn"] != null
                && (Request.UrlReferrer.AbsolutePath.Contains("/Home/IndexNew") || Request.UrlReferrer.AbsolutePath.Contains("/SuppLine/AccountsListMISM"))
                && (regID == 0 || regID == null))
            {
                selectedMsisdn = Session["SelectedMsisdn"].ToString2();
            
                if (SessionManager.Get("PPIDInfo") != null)
                {
                    AcctListByICResponse = (retrieveAcctListByICResponse)SessionManager.Get("PPIDInfo");
                }
                if (AcctListByICResponse != null && AcctListByICResponse.itemList != null && AcctListByICResponse.itemList.Count() > 0)
                {
                    WebHelper.Instance.ClearRegistrationSession();
                    personalDetailsVM = WebHelper.Instance.ConstructPersonalDetailsForMism(AcctListByICResponse, selectedMsisdn);
                    personalDetailsVM.changeMISM = 3;
                    personalDetailsVM.Customer.ContactNo = selectedMsisdn;
					// clear this session, this is impacting the ordertype
					Session["IsDeviceRequired"] = string.Empty;

                    //CR 29JAN2016 - display bill cycle
                    var selectedAccount = AcctListByICResponse.itemList.Where(c => c.ExternalId == selectedMsisdn).FirstOrDefault();
                    var billCycle = (selectedAccount != null && selectedAccount.AccountDetails != null ? selectedAccount.AccountDetails.BillCycle : "N/A");
                    personalDetailsVM.BillingCycle = (!string.IsNullOrWhiteSpace(billCycle) ? billCycle : "N/A");
                    return View(personalDetailsVM);
                }
            }


            if (ReferenceEquals(Session["RegMobileReg_PersonalDetailsVM"], null))
            {
                if (!Request.UrlReferrer.AbsolutePath.Contains("SecondaryLineRegAccCreated") && !Request.UrlReferrer.AbsolutePath.Contains("Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("StoreKeeper") && !Request.UrlReferrer.AbsolutePath.Contains("SecondaryLineRegSuccess") &&
                    !Request.UrlReferrer.AbsolutePath.Contains("/Registration/SuperVisorForWaiveOff") && !Request.UrlReferrer.AbsolutePath.Contains("/Home/IndexNew"))
                {
                    return RedirectToAction("IndexNew", "Home");
                }
            }


            var orderSummary = new OrderSummaryVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            int paymentstatus = -1;
            #region VLT ADDED CODE 21stFEB for Email functionality
            Session["RegID"] = regID;
            #endregion VLD ADDED CODE 21stFEB for Email functionality
            /*Chetan added for displaying the reson for failed transcation*/
            var KenanLogDetails = new DAL.Models.KenanaLogDetails();


            if (regID != null && regID.ToInt() > 0)
            {
                WebHelper.Instance.ClearRegistrationSession();
                personalDetailsVM = new PersonalDetailsVM()
                {
                    RegID = regID.Value.ToInt(),
                };

                Session["RegMobileReg_PhoneVM"] = null;

                using (var proxy = new RegistrationServiceProxy())
                {
                    reg = proxy.RegistrationGet(regID.Value);



                    personalDetailsVM.MSISDN1 = reg.MSISDN1.ToString2();
                    /*Chetan added for displaying the reson for failed transcation*/
                    KenanLogDetails = proxy.KenanLogDetailsGet(regID.Value);
                    if (!ReferenceEquals(KenanLogDetails, null))
                    {
                        personalDetailsVM.MessageCode = KenanLogDetails.MessageCode.ToInt();
                        personalDetailsVM.MessageDesc = KenanLogDetails.MessageDesc.ToString();
                    }

                    paymentstatus = proxy.PaymentStatusGet(regID.Value);
                    if (paymentstatus != -1)
                    {
                        if (paymentstatus == 0)
                        {
                            personalDetailsVM.PaymentStatus = "0";
                        }
                        else
                        {
                            personalDetailsVM.PaymentStatus = "1";
                        }
                    }
                    else
                    {
                        personalDetailsVM.PaymentStatus = "0";
                    }


                    personalDetailsVM.QueueNo = reg.QueueNo;

                    personalDetailsVM.Remarks = reg.Remarks;

                    personalDetailsVM.BiometricVerify = reg.BiometricVerify;
                    personalDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
                    personalDetailsVM.BlacklistInternal = reg.InternalBlacklisted;
                    personalDetailsVM.SignatureSVG = reg.SignatureSVG;
                    personalDetailsVM.CustomerPhoto = reg.CustomerPhoto;
                    personalDetailsVM.AltCustomerPhoto = reg.AltCustomerPhoto;
                    personalDetailsVM.RFSalesDT = reg.RFSalesDT;
                    personalDetailsVM.Photo = reg.Photo;
                    personalDetailsVM.RegSIMSerial = reg.SIMSerial;
                    personalDetailsVM.RegIMEINumber = reg.IMEINumber;
                    personalDetailsVM.CapturedIMEINumber = reg.IMEINumber;
                    personalDetailsVM.RegTypeID = reg.RegTypeID;
                    personalDetailsVM.AgeCheckStatus = reg.AgeCheckStatus;
                    personalDetailsVM.DDMFCheckStatus = reg.DDMFCheckStatus;
                    personalDetailsVM.AddressCheckStatus = reg.AddressCheckStatus;
                    personalDetailsVM.OutStandingCheckStatus = reg.OutStandingCheckStatus;
                    personalDetailsVM.TotalLineCheckStatus = reg.TotalLineCheckStatus;
                    personalDetailsVM.PrincipleLineCheckStatus = reg.PrincipleLineCheckStatus;
                    personalDetailsVM.ContractCheckStatus = reg.ContractCheckStatus;
                    personalDetailsVM.UserType = reg.UserType;
                    personalDetailsVM.PlanAdvance = reg.PlanAdvance;
                    personalDetailsVM.PlanDeposit = reg.PlanDeposit;
                    personalDetailsVM.DeviceAdvance = reg.DeviceAdvance;
                    personalDetailsVM.DeviceDeposit = reg.DeviceDeposit;
                    personalDetailsVM.Discount = reg.Discount;
                    personalDetailsVM.KenanAccountNo = reg.KenanAccountNo;
                    personalDetailsVM.OutstandingAcs = reg.OutstandingAcs;
                    personalDetailsVM.PrePortInCheckStatus = reg.PrePortInCheckStatus;
                    personalDetailsVM.MNPServiceIdCheckStatus = reg.MNPServiceIdCheckStatus;
                    personalDetailsVM.MOC_Status = reg.MOC_Status;
                    personalDetailsVM.Whitelist_Status = reg.Whitelist_Status;

                    //Added by ravikiran for getting sim model on 30 jan 2014
                    personalDetailsVM.SIMModel = reg.SimModelId.ToString2() == string.Empty ? "0" : reg.SimModelId.ToString();
                    //Added by ravikiran for getting sim model on 30 jan 2014 Ends here

                    personalDetailsVM.fxAcctNo = reg.fxAcctNo;
                    personalDetailsVM.FxSubscrNo = reg.fxSubscrNo;
                    personalDetailsVM.FxSubScrNoResets = reg.fxSubScrNoResets;
                    personalDetailsVM.CRPType = reg.CRPType;
                    Session["Condition"] = personalDetailsVM.CRPType.ToString2();
                    //Added by VLT on 13 Apr 2013
                    personalDetailsVM.IMPOSFileName = reg.IMPOSFileName;
                    //Added by VLT on 13 Apr 2013 ends here

                    // RegAccount
                    var regAccount = proxy.RegAccountGet(new int[] { reg.ID }).SingleOrDefault();
                    if (regAccount != null)
                    {
                        personalDetailsVM.ServiceActivateErrMsg = regAccount.KenanActivateServiceErrMessage;
                    }

                    // Registration Status
                    var regStatusID = proxy.RegStatusFind(new RegStatusFind()
                    {
                        RegStatus = new RegStatus()
                        {
                            RegID = reg.ID
                        },
                        Active = true
                    }).SingleOrDefault();

                    personalDetailsVM.Deposite = reg.AdditionalCharges;
                    personalDetailsVM.StatusID = proxy.RegStatusGet(new int[] { regStatusID }).SingleOrDefault().StatusID;


                    using (var configProxy = new ConfigServiceProxy())
                    {
                        personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;

                    }

                    //for cancel reason
                    if (personalDetailsVM.StatusID == 21)
                    {

                        using (var regProxy = new RegistrationServiceProxy())
                        {
                            string reason = regProxy.CancelReasonGet(personalDetailsVM.RegID);
                            personalDetailsVM.MessageDesc = reason;

                        }
                    }

                    ExtraTenGetResp objExtraTenDetails = new ExtraTenGetResp();
                    objExtraTenDetails = proxy.GetExtraTenDetails(personalDetailsVM.RegID);
                    List<lnkExtraTenDetails> modListExtraTen = new List<lnkExtraTenDetails>();
                    personalDetailsVM.ExtraTenDetails = objExtraTenDetails.ExtraTenDetails.ToList();
                    if (personalDetailsVM.ExtraTenDetails != null && personalDetailsVM.ExtraTenDetails.Count > 0)
                    {
                        foreach (var extraTen in personalDetailsVM.ExtraTenDetails)
                        {
                            if (string.IsNullOrEmpty(extraTen.MsgCode) == true)
                            {
                                extraTen.MsgCode = "Processing";
                            }
                            else if (extraTen.MsgCode == "0")
                            {
                                extraTen.MsgCode = "Success";
                            }
                            else
                            {
                                extraTen.MsgCode = "Fail";
                            }
                            modListExtraTen.Add(extraTen);

                        }
                        personalDetailsVM.ExtraTenDetails = modListExtraTen;
                    }
                    personalDetailsVM.ExtraTenMobileNumbers = string.Join(",", objExtraTenDetails.ExtraTenDetails.Where(e => e.Status == "A").Select(e => e.Msisdn).ToList());
                    Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;

                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);
                    Session["RegMobileReg_VirtualMobileNo_Sec"] = string.Empty;
                    Session["RegMobileReg_MobileNo"] = null;
                    // Customer
                    personalDetailsVM.Customer = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    // Address
                    var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                    {
                        Active = true,
                        Address = new Address()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();
                    personalDetailsVM.Address = proxy.RegAddressGet(regAddrIDs).SingleOrDefault();

                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    // RegMdlGrpModels
                    var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                    {
                        RegMdlGrpModel = new RegMdlGrpModel()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();
                    if (regMdlGrpModelIDs.Count() > 0)
                        regMdlGrpModel = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).SingleOrDefault();

                    if (regMdlGrpModel != null && regMdlGrpModel.ID == 0)
                    {
                        Session["RegMobileReg_Type"] = (int)MobileRegType.SecPlan;
                    }
                    else
                    {
                        Session["RegMobileReg_MainDevicePrice"] = regMdlGrpModel.Price.ToDecimal();
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                        Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;
                    }

                    if (personalDetailsVM.RegTypeID == (int)MobileRegType.CRP)
                        Session["RegMobileReg_Type"] = personalDetailsVM.RegTypeID;

                    // RegPgmBdlPkgComponent
                    var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                    {
                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();

                    // Main Line PgmBdlPkgComponent
                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    if (regPgmBdlPkgCompIDs.Count() > 0)
                        regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind()
                    {
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();

                    if (suppLineIDs.Count() > 0)
                    {
                        suppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    }
                }

                if (suppLines.Count() > 0)
                    WebHelper.Instance.ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

                using (var proxy = new CatalogServiceProxy())
                {
                    string userICNO = Util.SessionAccess.User.IDCardNo;

                    if (userICNO != null && userICNO.Length > 0)
                    {
                        objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                    }
                    if (objWhiteListDetailsResp != null)
                    {
                        if (objWhiteListDetailsResp.Description != null)
                        {
                            personalDetailsVM._WhiteListDetails.Description = objWhiteListDetailsResp.Description;
                        }
                    }

                    if (regMdlGrpModel.ID > 0)
                        Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;


                    var mainLinePBPCs = new List<PgmBdlPckComponent>();
                    if (mainLinePBPCIDs.Count() > 0)
                        mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var DPCode = Properties.Settings.Default.RegType_DevicePlan;
                    var MAcode = Properties.Settings.Default.Master_Component;
                    var MPcode = Properties.Settings.Default.Main_package;
                    var CPCode = Properties.Settings.Default.ComplimentaryPlan;
                    var DCCode = Properties.Settings.Default.CompType_DataCon;
                    var ECCode = Properties.Settings.Default.Extra_Component;
                    var SPCode = Properties.Settings.Default.SecondaryPlan;
                    var RCCode = Properties.Settings.Default.Pramotional_Component;
                    var CRCode = Properties.Settings.Default.DiscountDataContract_Component;
                    if (mainLinePBPCs.Count() > 0)
                    {
                        var componentList = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode)).ToList();
                        if (componentList != null && componentList.Count > 0)
                            Session["RegMobileReg_PkgPgmBdlPkgCompID"] = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == CPCode)).FirstOrDefault().ID;
                        else
                            Session["RegMobileReg_PkgPgmBdlPkgCompID"] = mainLinePBPCs.Where(a => a.LinkType == SPCode && (a.PlanType == CPCode)).FirstOrDefault().ID;


                        var vasIDs = string.Empty;
                        //Added by chetan to exclusively check DiscountDataContract_Component as CRCode & || RCCode
                        var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == CRCode || a.LinkType == RCCode || a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode).Select(a => a.ID).ToList();

                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session["RegMobileReg_VasIDs"] = vasIDs;


                    }
                }
            }
            if (reg.OfferID != null && reg.OfferID > 0)
            {
                Session["OfferId"] = reg.OfferID;
            }

            var orderSummaryVM = ConstructOrderSummary();


            if (Session["RegMobileReg_PkgPgmBdlPkgCompID"] != null)
            {
                if (Session["regid"] != null)
                {
                    if (regID.Value > 0)
                    {
                        CatalogServiceProxy objCSproxy = new CatalogServiceProxy();
                        var deviceproxys = objCSproxy.GetDiscountPriceDetails(orderSummaryVM.ModelID, Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(), ReferenceEquals(Session["RegMobileReg_ContractID"], null) ? 0 : Session["RegMobileReg_ContractID"].ToInt(), 0);
                        if (deviceproxys.NewPlanName != null)
                        {
                            orderSummaryVM.DiscountPrice = deviceproxys.DiscountPrice.ToInt();
                            orderSummaryVM.PlanName = deviceproxys.NewPlanName;
                            orderSummaryVM.MonthlySubscription = orderSummaryVM.MonthlySubscription - orderSummaryVM.DiscountPrice;
                        }
                    }

                }
            }


            #region Added by VLT on 21 June 2013 for getting Sim Type
            using (var proxy = new RegistrationServiceProxy())
            {
                if (regID != null && regID.ToInt() > 0)
                {
                    DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(regID.ToInt());
                    if (lnkregdetails != null)
                    {
                        //Justification by ravi for waiver cr on sept 26
                        personalDetailsVM.Justification = lnkregdetails.Justification.ToString2();
                        //Justification by ravi for waiver cr on sept 26 ends here
                        personalDetailsVM.ContractCheckCount = lnkregdetails.ContractCheckCount;
                        if (lnkregdetails.MismSimCardType != null)
                        {
                            personalDetailsVM.SecondarySIMType = lnkregdetails.MismSimCardType;
                        }

                        if (lnkregdetails.SimType != null)
                        {
                            personalDetailsVM.SimType = lnkregdetails.SimType;

                        }
                        else
                        {
                            personalDetailsVM.SimType = "Normal";
                        }
                    }
                    else
                    {
                        personalDetailsVM.SimType = "Normal";
                    }

                    /*Active PDPA Commented*/
                    /*Changes by chetan related to PDPA implementation*/
                    string documentversion = lnkregdetails.PdpaVersion;
                    personalDetailsVM.PDPAVersionStatus = documentversion;

                    if (!ReferenceEquals(documentversion, null))
                    {
                        using (var registrationProxy = new RegistrationServiceProxy())
                        {
                            var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", documentversion);
                            personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                        }
                    }

                }
                /*Active PDPA Commented*/
                else
                {
                    /*Changes by chetan related to PDPA implementation*/
                    using (var registrationProxy = new RegistrationServiceProxy())
                    {
                        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                        personalDetailsVM.PDPDVersion = resultdata.Version;
                    }
                }
            }
            #endregion



            orderSummaryVM.IMEINumber = reg.IMEINumber;
            orderSummaryVM.SIMSerial = reg.SIMSerial;

            if (regID != null && regID.ToInt() > 0)
            {
                orderSummaryVM.SimType = personalDetailsVM.SimType;
                Session["SimType"] = personalDetailsVM.SimType;
            }

            if (reg.KenanAccountNo != null)
            { Session["KenanAccountNo"] = reg.KenanAccountNo; }
            else
            { Session["KenanAccountNo"] = "0"; }
            Session["RegMobileReg_OrderSummary"] = orderSummaryVM;

            #region MOC_Status and Liberization status

            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session["ExternalID"].ToString2()) ? string.Empty : Session["ExternalID"].ToString2(), regID.ToInt());
            personalDetailsVM.Liberlization_Status = Result[0];
            personalDetailsVM.MOCStatus = Result[1];

            #endregion

            // 29JAN2016 - display bill cycle
            if (regID.ToInt() > 0)
            {
                if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault() != null)
                {
                    personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault().ATT_Value;
                }
                else
                {
                    personalDetailsVM.BillingCycle = "N/A";
                }
            }
            else
            {
                var selectedAccount = AcctListByICResponse.itemList.Where(c => c.ExternalId == selectedMsisdn).FirstOrDefault();
                var billCycle = (selectedAccount != null && selectedAccount.AccountDetails != null ? selectedAccount.AccountDetails.BillCycle : "N/A");
                personalDetailsVM.BillingCycle = (!string.IsNullOrWhiteSpace(billCycle) ? billCycle : "N/A");
            }
            
            return View(personalDetailsVM);
        }

        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DCH,MREG_DSK,MREG_DIC,MREG_DAI,MREG_DAC,MREG_DIC,MREG_DAC")]
        public ActionResult SecondaryLineRegSummaryNew(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            bool isSuppAsNew = false;
            Session["SelectedPlanID"] = null;
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var resp = new RegistrationCreateResp();
            var response = new KenanNewSuppLineResponse();
            bool isSuccess = false;
            Session["RegMobileReg_TabIndex"] = personalDetailsVM.TabNumber;
            Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;
            try
            {
                #region code commented for implementing the epetrol changes
                if (collection["submit1"].ToString2() == "PaymentRecived")
                {
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                        });
                    }

                    #region Added  by Nreddy for service activation

                    using (var proxy = new RegistrationServiceProxy())
                    {
                        isSuppAsNew = proxy.getSupplineDetails(personalDetailsVM.RegID);

                    }
                    if (isSuppAsNew)
                    {
                        var request = new OrderFulfillRequest()
                        {
                            OrderID = personalDetailsVM.RegID.ToString(),
                            UserSession = Util.SessionAccess.UserName,

                        };
                        using (var proxy = new KenanServiceProxy())
                        {
                            isSuccess = proxy.KenanAccountFulfill(request);
                        }
                        if (isSuccess)
                        {
                            return RedirectToAction("SecondaryLineRegSvcActivated", new { regID = personalDetailsVM.RegID });
                        }
                        else
                        {
                            return RedirectToAction("SecondaryLineRegClosed", new { regID = personalDetailsVM.RegID });
                        }
                    }
                    else
                    {

                        if (Convert.ToString(Session["SimType"]) == "MISM")
                        {

                            List<string> mobileNos = new List<string>();
                            mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers(true));
                            using (var proxyobj = new RegistrationServiceProxy())
                            {
                                if (mobileNos[0] == string.Empty)
                                    mobileNos[0] = "60179900033";
                                int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
                                if (res == 1)
                                {
                                    proxyobj.RegistrationUpdate(new DAL.Models.Registration()
                                    {
                                        ID = personalDetailsVM.RegID,

                                        MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,

                                        LastAccessID = Util.SessionAccess.UserName

                                    });
                                }
                            }
                        }
                        response = FulfillKenanAccount(personalDetailsVM.RegID);

                        if (!ReferenceEquals(response, null) && !ReferenceEquals(response.Message, null))
                        {
                            if (response.Message == "success")
                            {
                                return RedirectToAction("SecondaryLineRegSvcActivated", new { regID = personalDetailsVM.RegID });
                            }
                            else
                            {
                                if (response.Code != "0")
                                {
                                    using (var regproxy = new RegistrationServiceProxy())
                                    {
                                        regproxy.RegistrationClose(new RegStatus()
                                        {
                                            RegID = personalDetailsVM.RegID,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            StartDate = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegNew),
                                            ResponseCode = response.Code,
                                            ResponseDescription = response.Message,
                                            MethodName = "KenanAddNewSuppLine"
                                        });
                                    }
                                    TempData["KenanMethod"] = "KenanAddNewSuppLine";
                                    TempData["ResponseCode"] = response.Code;
                                    if (response.Code == "1")
                                        TempData["ResponseDescription"] = "SI/Account Creation failed";

                                    if (response.Code == "2")
                                        TempData["ResponseDescription"] = "Account/SI Creation failed due to Communication problem with Downstream services (Kenan/NCR)";
                                    if (response.Code == "3")
                                        RedirectToAction("MobileRegWMFail", new { regID = personalDetailsVM.RegID });
                                    else
                                        Response.Redirect("~/Home/Error");


                                }

                            }
                        }
                        else
                        {
                            return RedirectToAction("SecondaryLineRegWMFail", new { regID = personalDetailsVM.RegID });
                        }
                    #endregion

                    }
                }
                #endregion

                #region close
                if (collection["submit1"].ToString2() == "close")
                {
                    if (personalDetailsVM.RegID == 0)
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString2() : "false")),
                                                        WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]),
                                                        WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
                                                        WebHelper.Instance.ConstructRegSuppLineVASes(), null, null, personalDetailsVM.isBREFail);
                            personalDetailsVM.RegID = resp.ID;
                        }

                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegBreFail)
                            });
                        }
                        return RedirectToAction("SecondaryLineRegBreFail", new { regID = personalDetailsVM.RegID });
                    }
                    else
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                            });
                        }
                        return RedirectToAction("SecondaryLineRegClosed", new { regID = personalDetailsVM.RegID });
                    }
                }
                #endregion

                #region cancel
                if (collection["submit1"].ToString2() == "cancel")
                {
                    string reason = string.Empty;
                    int prevStatus = WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID);
                    reason = ReferenceEquals(reason, null) ? "No reason provided." : collection["hdnReasonCancel"].ToString2();
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = personalDetailsVM.RegID,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)

                        });

                        proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                        {
                            RegID = personalDetailsVM.RegID,
                            CancelReason = reason,
                            CreateDT = DateTime.Now,
                            LastUpdateDT = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName
                        });

                        var totalPrice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID);
                        if (prevStatus == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment) && WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID) == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan) && totalPrice > 0)
                        {
                            WebHelper.Instance.SendDetailsToWebPos(personalDetailsVM.RegID, Constants.StatusWebPosToCancel);
                        }
                    }

                    return RedirectToAction("SecondaryLineRegCanceled", new { regID = personalDetailsVM.RegID });
                }
                #endregion

                #region createAcc
                if (collection["submit1"].ToString2() == "createAcc")
                {
                    #region Added by Nreddy for support suppline as new account

                    using (var proxy = new RegistrationServiceProxy())
                    {
                        isSuppAsNew = proxy.getSupplineDetails(personalDetailsVM.RegID);
                    }
                    if (isSuppAsNew)
                    {
                        string username = string.Empty;
                        if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                        {
                            username = Request.Cookies["CookieUser"].Value;
                        }
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationUpdate(new DAL.Models.Registration()
                            {
                                ID = personalDetailsVM.RegID,
                                SalesPerson = username,
                                LastAccessID = Util.SessionAccess.UserName,



                            });
                        }

                        WebHelper.Instance.CreateKenanAccount(personalDetailsVM.RegID);
                        //30042015 - Anthony - Generate RF for iContract - Start
                        try
                        {
                            using (var proxy = new UpdateServiceProxy())
                            {
                                if (!proxy.FindDocument(Constants.CONTRACT, personalDetailsVM.RegID))
                                {
                                    String contractHTMLSource = string.Empty;
                                    contractHTMLSource = Util.RenderPartialViewToString(this, "PrintContractDetails", WebHelper.Instance.GetContractDetailsViewModel(personalDetailsVM.RegID, false));
                                    contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
                                    contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
                                    contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide"); 
                                    if (!string.IsNullOrEmpty(contractHTMLSource))
                                    {
                                        Byte[] PDFBytes = Util.convertHTMLStringToPDFByteArray(contractHTMLSource, personalDetailsVM.RegID);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //Logger.Error("ICONTRACT EXCEPTION = " + ex);
                            WebHelper.Instance.LogExceptions(this.GetType(), ex);
                        }
                        //30042015 - Anthony - Generate RF for iContract - End
                    }

                    else
                    {
                        using (var proxy = new KenanServiceProxy())
                        {
                            String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                            if (!ReferenceEquals(Action, String.Empty))
                                return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                            //proxy.CreateIMPOSFile(personalDetailsVM.RegID);
                        }
                        using (var regproxy = new RegistrationServiceProxy())
                        {
                            regproxy.RegistrationClose(new RegStatus()
                            {
                                RegID = personalDetailsVM.RegID,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                ResponseCode = response.Code,
                                ResponseDescription = response.Message,
                                MethodName = "KenanAdditionLineRegistration"
                            });
                        }
                    }
                    return RedirectToAction("SecondaryLineRegAccCreated", new { regID = personalDetailsVM.RegID });
                    #endregion
                }
                #endregion

                #region Activate service
                if (collection["submit1"].ToString2() == "activateSvc")
                {
                    response = FulfillKenanAccount(personalDetailsVM.RegID);

                    if (response.Message == "success")
                    {

                        using (var proxy = new RegistrationServiceProxy())
                        {
                            if (personalDetailsVM.ExtraTenLimitConfirmation == "true")
                            {

                                if (personalDetailsVM.hdnExtraTenCount != null && personalDetailsVM.hdnExtraTenCount != string.Empty)
                                {
                                    proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                    {
                                        ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                        {
                                            ConfirmedBy = Util.SessionAccess.UserName,
                                            ConfirmedStatus = "Y",
                                            Count = Convert.ToInt32(personalDetailsVM.hdnExtraTenCount),
                                            CreatedDate = System.DateTime.Now,
                                            RegId = personalDetailsVM.RegID
                                        }
                                    });
                                }
                            }

                            if (personalDetailsVM.ExtraTenConfirmation == "true")
                            {

                                proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                {
                                    ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                    {
                                        ConfirmedBy = Util.SessionAccess.UserName,
                                        ConfirmedStatus = "Y",
                                        CreatedDate = System.DateTime.Now,
                                        RegId = personalDetailsVM.RegID
                                    }
                                });

                            }
                            else
                            {
                                proxy.SaveExtraTenLogs(new ExtraTenLogReq
                                {
                                    ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
                                    {
                                        ConfirmedBy = Util.SessionAccess.UserName,
                                        ConfirmedStatus = "N",
                                        CreatedDate = System.DateTime.Now,
                                        RegId = personalDetailsVM.RegID
                                    }
                                });
                            }

                            proxy.DeleteExtraTenDetails(personalDetailsVM.RegID);
                            if (personalDetailsVM.ExtraTenConfirmation == "true")
                            {
                                foreach (var extratenMSISDN in personalDetailsVM.ExtraTenMobileNumbers.Split(','))
                                {
                                    if (extratenMSISDN != string.Empty)
                                    {
                                        proxy.SaveExtraTen(new ExtraTenReq
                                        {

                                            ExtraTenDetails = new lnkExtraTenDetails { CreatedBy = Util.SessionAccess.UserName, CreatedDate = System.DateTime.Now, Msisdn = extratenMSISDN, RegId = personalDetailsVM.RegID, Status = "A" }


                                        });
                                    }
                                }
                            }
                        }
                        //END   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database

                        return RedirectToAction("SecondaryLineRegSvcActivated", new { regID = personalDetailsVM.RegID });
                    }
                    else
                    {
                        return RedirectToAction("SecondaryLineRegClosed", new { regID = personalDetailsVM.RegID });
                    }
                }
                #endregion


                if (personalDetailsVM.TabNumber == (int)SecondaryLineRegistrationSteps.Submit)
                {

                    Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(false);


                    // 20141204 - Read the waiver components from the dropFourObj - start
                    var dropFourObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                    if (dropFourObj.msimIndex > 0)
                    {
                        var allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
                        var currentAccountNumber = Session[SessionKey.KenanACNumber.ToString()].ToString2();
                        var mobileRegType = MobileRegType.SecPlan.ToString();

                        dropFourObj.mainFlow.orderSummary = WebHelper.Instance.initializeAutoWaiverValues(dropFourObj.mainFlow.orderSummary, mobileRegType, personalDetailsVM, allCustomerDetails, currentAccountNumber);
                    }
                    List<DAL.Models.WaiverComponents> objWaiverComp = Util.constructWaiverComponentList(dropFourObj);

                    if (objWaiverComp == null || objWaiverComp.Count == 0)
                    {
                        //Waiver formation cr by ravi kiran on 26 sept
                        objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                        //Waiver formation cr by ravi kiran on 26 sept ends here
                    }
                    // 20141204 - Read the waiver components from the dropFourObj - start

                    // submit registration
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        var cRegistration = ConstructRegistration(
                            personalDetailsVM.QueueNo,
                            personalDetailsVM.Remarks,
                            personalDetailsVM.SignatureSVG,
                            personalDetailsVM.CustomerPhoto,
                            personalDetailsVM.AltCustomerPhoto,
                            personalDetailsVM.Photo,
                            (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString2() : "false"));

                        var cCustomer = WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]);
                        var cGroupModel = new RegMdlGrpModel();
                        var cAddress = WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]);
                        var cComponent = WebHelper.Instance.ConstructRegPgmBdlPkgComponent();
                        var cStatus = WebHelper.Instance.ConstructRegStatus();
                        var cSupplines = WebHelper.Instance.ConstructRegSuppLines();
                        var cSuppVas = WebHelper.Instance.ConstructRegSuppLineVASes();
                        var cModelSec = WebHelper.Instance.ConstructRegMdlGroupModelSec();

                        resp = proxy.RegistrationCreate(
                            cRegistration,
                            cCustomer,
                            cGroupModel,
                            cAddress,
                            cComponent,
                            cStatus,
                            cSupplines,
                            cSuppVas,
                            null,
                            objWaiverComp,
                            personalDetailsVM.isBREFail);
                        //Added sim model id by ravikiran on 30 jan 2014 as it was not saving
                        //resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString2() : "false")),
                        //                                WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]),
                        //                                WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
                        //                                WebHelper.Instance.ConstructRegSuppLineVASes(), null, objWaiverComp, personalDetailsVM.isBREFail);



                        if (Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("MREG_DAI"))
                        {
                            string username = string.Empty;
                            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                            {
                                username = Request.Cookies["CookieUser"].Value;
                            }
                            using (var objproxys = new RegistrationServiceProxy())
                            {
                                objproxys.RegistrationUpdate(new DAL.Models.Registration()
                                {
                                    ID = resp.ID,
                                    SalesPerson = username,
                                    LastAccessID = Util.SessionAccess.UserName,



                                });
                            }

                            WebHelper.Instance.CreateKenanAccount(personalDetailsVM.RegID);

                        }


                        if (resp.ID > 0)
                        {
                            ///BYPASS THE BELOW CODE FOR Extra Ten IF REQUEST FROM Multiple MNP
                            bool isMultipleMNP = personalDetailsVM.RegTypeID == (int)MobileRegType.RegType_MNPPlanWithMultiSuppline ? true : false;
                            if (!isMultipleMNP)
                                Util.SaveExtraTen(personalDetailsVM.ExtraTenMobileNumbers, resp.ID);

                            #region Insert into lnkRegAttributes
                            var regAttribList = new List<RegAttributes>();
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.BillingCycle))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_ACC_BILL_CYCLE, ATT_Value = personalDetailsVM.BillingCycle });
                            regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_PAYMENT_METHOD, ATT_Value = Util.getPaymentMethodName() });
                            regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_USER_DEVICE_AGENT, ATT_Value = Request.UserAgent });
                            proxy.SaveListRegAttributes(regAttribList);
                            #endregion
                        }

                        var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
                        if (biometricID != 0)
                        {
                            var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
                            biometric.RegID = resp.ID;

                            proxy.BiometricUpdate(biometric);
                        }
                    }



                    #region VLT ADDED CODE on 21stFEB for email functionality
                    if (resp.ID > 0)
                    {
                        bool routeToCDPU = personalDetailsVM.isBREFail && Session["IsDealer"].ToBool();
                        if (routeToCDPU)
                        {
                            try
                            {
                                using (var proxy = new RegistrationServiceProxy())
                                {
                                    proxy.UpsertBRE(new RegBREStatus()
                                    {
                                        RegId = resp.ID,
                                        TransactionStatus = Constants.StatusCDPUPending
                                    });
                                    personalDetailsVM.CDPUStatus = Constants.StatusCDPUPending;
                                }
                            }
                            catch
                            {

                            }
                        }

                        /* BEGIN Added by VLT 5 Apr 2013 to add kenaninfo to database*/
                        if (Session["KenanCustomerInfo"] != null)
                        {
                            string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString2().Split('╚');
                            KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
                            kenanInfo.FullName = strArrKenanInfo[0];
                            kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
                            kenanInfo.IDCardNo = strArrKenanInfo[2];
                            kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
                            kenanInfo.Address1 = strArrKenanInfo[4];
                            kenanInfo.Address2 = strArrKenanInfo[5];
                            kenanInfo.Address3 = strArrKenanInfo[6];
                            kenanInfo.Postcode = strArrKenanInfo[8];
                            kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
                            kenanInfo.RegID = resp.ID;
                            using (var proxys = new RegistrationServiceProxy())
                            {
                                var kenanResp = new KenanCustomerInfoCreateResp();

                                kenanResp = proxys.KenanCustomerInfoCreate(kenanInfo);
                            }
                        }
                        Session["KenanCustomerInfo"] = null;
                        /* END Added by VLT 5 Apr 2013 to add kenaninfo to database*/

                        int regTypeID = 0;
                        if (Session["RegMobileReg_Type"] != null)
                        {
                            regTypeID = Session["RegMobileReg_Type"].ToInt();
                        }

                        //commented as disabling pos file feature
                        //#region Saving POS FileName in DB for Dealers
                        //if (Session["IsDealer"].ToBool() == true && collection["hdnSecLineTotalPayable"].ToString2() != string.Empty && collection["hdnSecLineTotalPayable"].ToDecimal() > 0)
                        //{
                        //    string posFileName = WebHelper.Instance.CreatePOSFileName(resp.ID);
                        //    using (var proxy = new CatalogServiceProxy())
                        //    {
                        //        proxy.UpdateIMPOSDetails(resp.ID, posFileName, 1);
                        //    }
                        //}
                        //#endregion

                        string PlanKenanCode = MasterDataCache.Instance.PackageComponents.Where(a => a.ID == Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"])).Select(a => a.KenanCode).SingleOrDefault().ToString2();
                        string latestPrintVersion = Util.GetPrintVersionNum(regTypeID, string.Empty, PlanKenanCode);

						#region Insert into trnRegJustification
						WebHelper.Instance.saveTrnRegJustification(resp.ID);
                        #endregion

                        #region Insert into trnRegBreFailTreatment
                        WebHelper.Instance.saveTrnRegBreFailTreatment(resp.ID);
                        #endregion

                        #region "SecondaryLine New Account Feature"
                        if (Util.IsSecLineNewAccount == true || collection["txtJustfication"].ToString2().Length > 0)
                            if (Util.IsSecLineNewAccount == true || (!string.IsNullOrEmpty(latestPrintVersion)))
                            {
                                LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
                                Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                                objRegDetails.CreatedDate = DateTime.Now;
                                objRegDetails.UserName = Util.SessionAccess.UserName;
                                objRegDetails.RegId = resp.ID;
                                objRegDetails.Liberlization_Status = !ReferenceEquals(Session["CustomerLiberalisation"], null) ? Session["CustomerLiberalisation"].ToString2() : personalDetailsVM.Liberlization_Status;
                                if (Util.IsSecLineNewAccount == true)
                                {
                                    objRegDetails.IsSuppNewAc = false;
                                    objRegDetails.SimType = "MISM";
                                }
                                objRegDetails.Justification = collection["txtJustfication"].ToString2();

                                if (!string.IsNullOrEmpty(latestPrintVersion))
                                {
                                    objRegDetails.PrintVersionNo = latestPrintVersion;
                                }
                                else
                                {
                                    objRegDetails.PrintVersionNo = string.Empty;
                                }
                                //******selected sim card type is assigned*****//
                                if (!string.IsNullOrEmpty(collection["SecondarySimCardTypeSelected"]))
                                    objRegDetails.MismSimCardType = collection["SecondarySimCardTypeSelected"].ToString2();
                                //******selected sim card type is assigned*****//
                                /*Active PDPA Commented*/
                                /*Changes by chetan related to PDPA implementation*/
                                //using (var registrationProxy = new RegistrationServiceProxy())
                                //{
                                //    var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                                //    objRegDetails.PdpaVersion = resultdata.Version;
                                //}

                                //objRegDetails.ContractCheckCount = Session["AccountswithContractCount"].ToInt();
                                dropFourObj.getExistingContractCount(dropFourObj);
                                objRegDetails.ContractCheckCount = dropFourObj.existingContract;
                                objRegDetails.TotalLineCheckCount = Session["existingTotalLine"].ToInt();
                                bool IsWriteOff = false;
                                string acc = string.Empty;
                                WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
                                objRegDetails.WriteOffDetails = acc;

                                /*Recode Active PDPA Commented*/
                                objRegDetails.PdpaVersion = personalDetailsVM.PDPDVersion;

                                objRegDetailsReq.LnkDetails = objRegDetails;
                                using (var proxy = new RegistrationServiceProxy())
                                {
                                    proxy.SaveLnkRegistrationDetails(objRegDetailsReq);
                                }
                            }
                        if (Roles.IsUserInRole("MREG_DSV"))
                        {
                            #region Added by Nreddy for support suppline as new account
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                isSuppAsNew = proxy.getSupplineDetails(resp.ID);
                                if (isSuppAsNew)
                                {
                                    string username = string.Empty;
                                    if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                                    {
                                        username = Request.Cookies["CookieUser"].Value;
                                    }

                                    proxy.RegistrationUpdate(new DAL.Models.Registration()
                                    {
                                        ID = resp.ID,
                                        SalesPerson = username,
                                        LastAccessID = Util.SessionAccess.UserName,
                                    });

                                    WebHelper.Instance.CreateKenanAccount(resp.ID);

                                }

                                else
                                {
                                    if (!routeToCDPU)
                                    {
                                        using (var kenanProxy = new KenanServiceProxy())
                                        {
                                            String Action = WebHelper.Instance.WebPosAuto(resp.ID);
                                            if (!ReferenceEquals(Action, String.Empty))
                                                return RedirectToAction(Action, new { regID = resp.ID });
                                            //kenanProxy.CreateIMPOSFile(resp.ID);
                                        }
                                        using (var regproxy = new RegistrationServiceProxy())
                                        {
                                            regproxy.RegistrationClose(new RegStatus()
                                            {
                                                RegID = resp.ID,
                                                Active = true,
                                                CreateDT = DateTime.Now,
                                                StartDate = DateTime.Now,
                                                LastAccessID = Util.SessionAccess.UserName,
                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                                ResponseCode = response.Code,
                                                ResponseDescription = response.Message,
                                                MethodName = "KenanAdditionLineRegistration"
                                            });
                                        }
                                    }
                                }
                            }
                            return RedirectToAction("SecondaryLineRegAccCreated", new { regID = resp.ID });
                            #endregion
                        }
                        //Assingn back to Default Value to Util
                        Util.IsSecLineNewAccount = false;
                        #endregion
                        WebHelper.Instance.ClearRegistrationSession();

                        return RedirectToAction("SecondaryLineRegSuccessNew", new { regID = resp.ID });

                    }
                    #endregion VLT ADDED CODE on 21stFEB for email functionality
                }
                else if (personalDetailsVM.TabNumber == (int)SecondaryLineRegistrationSteps.PersonalDetails)
                {
                    return RedirectToAction("IndexNew", "Home", collection);
				}


			}
			catch (Exception ex)
			{
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
				throw ex;
			}

			return RedirectToAction("SecondaryLineRegFail", new { regID = personalDetailsVM.RegID });
		}



		[HttpPost]
		[ValidateInput(false)]
        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DCH,MREG_DSK,MREG_DIC,MREG_DAI,MREG_DAC,MREG_DIC,MREG_DAC")]
		public ActionResult SecondaryLineRegSummary(FormCollection collection, PersonalDetailsVM personalDetailsVM)
		{
			bool isSuppAsNew = false;
			Session["SelectedPlanID"] = null;
			var resp = new RegistrationCreateResp();
			var response = new KenanNewSuppLineResponse();
			bool isSuccess = false;
			Session["RegMobileReg_TabIndex"] = personalDetailsVM.TabNumber;
			Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;
			try
			{
				#region code commented for implementing the epetrol changes
				if (collection["submit1"].ToString2() == "PaymentRecived")
				{
					using (var proxy = new RegistrationServiceProxy())
					{
						proxy.RegistrationCancel(new RegStatus()
						{
							RegID = personalDetailsVM.RegID,
							Active = true,
							CreateDT = DateTime.Now,
							StartDate = DateTime.Now,
							LastAccessID = Util.SessionAccess.UserName,
							StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
						});
					}

					#region Added  by Nreddy for service activation

					using (var proxy = new RegistrationServiceProxy())
					{
						isSuppAsNew = proxy.getSupplineDetails(personalDetailsVM.RegID);

					}
					if (isSuppAsNew)
					{
						var request = new OrderFulfillRequest()
						{
							OrderID = personalDetailsVM.RegID.ToString(),
							UserSession = Util.SessionAccess.UserName,

						};
						using (var proxy = new KenanServiceProxy())
						{
							isSuccess = proxy.KenanAccountFulfill(request);
						}
						if (isSuccess)
						{
							return RedirectToAction("SecondaryLineRegSvcActivated", new { regID = personalDetailsVM.RegID });
						}
						else
						{
							return RedirectToAction("SecondaryLineRegClosed", new { regID = personalDetailsVM.RegID });
						}
					}
					else
					{

						if (Convert.ToString(Session["SimType"]) == "MISM")
						{

							List<string> mobileNos = new List<string>();
							mobileNos.Add(WebHelper.Instance.retriveVirtualNumbers(true));
							using (var proxyobj = new RegistrationServiceProxy())
							{
								if (mobileNos[0] == string.Empty)
									mobileNos[0] = "60179900033";
								int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(mobileNos[0]), 1);
								if (res == 1)
								{
									proxyobj.RegistrationUpdate(new DAL.Models.Registration()
									{
										ID = personalDetailsVM.RegID,

										MSISDN1 = (mobileNos != null) ? mobileNos[0] : null,

										LastAccessID = Util.SessionAccess.UserName

									});
								}
							}
						}
						response = FulfillKenanAccount(personalDetailsVM.RegID);

						if (!ReferenceEquals(response, null) && !ReferenceEquals(response.Message, null))
						{
							if (response.Message == "success")
							{
								return RedirectToAction("SecondaryLineRegSvcActivated", new { regID = personalDetailsVM.RegID });
							}
							else
							{
								if (response.Code != "0")
								{
									using (var regproxy = new RegistrationServiceProxy())
									{
										regproxy.RegistrationClose(new RegStatus()
										{
											RegID = personalDetailsVM.RegID,
											Active = true,
											CreateDT = DateTime.Now,
											StartDate = DateTime.Now,
											LastAccessID = Util.SessionAccess.UserName,
											StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegNew),
											ResponseCode = response.Code,
											ResponseDescription = response.Message,
											MethodName = "KenanAddNewSuppLine"
										});
									}
									TempData["KenanMethod"] = "KenanAddNewSuppLine";
									TempData["ResponseCode"] = response.Code;
									if (response.Code == "1")
										TempData["ResponseDescription"] = "SI/Account Creation failed";

									if (response.Code == "2")
										TempData["ResponseDescription"] = "Account/SI Creation failed due to Communication problem with Downstream services (Kenan/NCR)";
									if (response.Code == "3")
										RedirectToAction("MobileRegWMFail", new { regID = personalDetailsVM.RegID });
									else
										Response.Redirect("~/Home/Error");


								}

							}
						}
						else
						{
							return RedirectToAction("SecondaryLineRegWMFail", new { regID = personalDetailsVM.RegID });
						}
					#endregion

					}
				}
				#endregion

				#region close
				if (collection["submit1"].ToString2() == "close")
				{
					if (personalDetailsVM.RegID == 0)
					{
						using (var proxy = new RegistrationServiceProxy())
						{
							resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString2() : "false")),
														WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]),
														WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
														WebHelper.Instance.ConstructRegSuppLineVASes(), null, null, personalDetailsVM.isBREFail);
							personalDetailsVM.RegID = resp.ID;
						}

						using (var proxy = new RegistrationServiceProxy())
						{
							proxy.RegistrationClose(new RegStatus()
							{
								RegID = personalDetailsVM.RegID,
								Active = true,
								CreateDT = DateTime.Now,
								StartDate = DateTime.Now,
								LastAccessID = Util.SessionAccess.UserName,
								StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegBreFail)
							});
						}
						return RedirectToAction("SecondaryLineRegBreFail", new { regID = personalDetailsVM.RegID });
					}
					else
					{
						using (var proxy = new RegistrationServiceProxy())
						{
							proxy.RegistrationClose(new RegStatus()
							{
								RegID = personalDetailsVM.RegID,
								Active = true,
								CreateDT = DateTime.Now,
								StartDate = DateTime.Now,
								LastAccessID = Util.SessionAccess.UserName,
								StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
							});
						}
						return RedirectToAction("SecondaryLineRegClosed", new { regID = personalDetailsVM.RegID });
					}
				}
				#endregion

				#region cancel
				if (collection["submit1"].ToString2() == "cancel")
				{
					string reason = string.Empty;
                    int prevStatus = WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID);
					reason = ReferenceEquals(reason, null) ? "No reason provided." : collection["hdnReasonCancel"].ToString2();
					using (var proxy = new RegistrationServiceProxy())
					{
						proxy.RegistrationCancel(new RegStatus()
						{
							RegID = personalDetailsVM.RegID,
							Active = true,
							CreateDT = DateTime.Now,
							StartDate = DateTime.Now,
							LastAccessID = Util.SessionAccess.UserName,
							StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)

						});

						proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
						{
							RegID = personalDetailsVM.RegID,
							CancelReason = reason,
							CreateDT = DateTime.Now,
							LastUpdateDT = DateTime.Now,
							LastAccessID = Util.SessionAccess.UserName
						});

                        //var totalPrice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID,IMEInumberStored:false);
                        if (prevStatus == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment) && WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID) == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan))
                        {
                            WebHelper.Instance.SendDetailsToWebPos(personalDetailsVM.RegID, Constants.StatusWebPosToCancel);
                        }
					}

					return RedirectToAction("SecondaryLineRegCanceled", new { regID = personalDetailsVM.RegID });
				}
				#endregion

				#region createAcc
				if (collection["submit1"].ToString2() == "createAcc")
				{
					#region 11052015 - Anthony - Generate RF for iContract
                    try
                    {
                        using (var updateProxy = new UpdateServiceProxy())
                        {
                            if (!updateProxy.FindDocument(Constants.CONTRACT, personalDetailsVM.RegID))
                            {
                                String contractHTMLSource = string.Empty;
                                contractHTMLSource = Util.RenderPartialViewToString(this, "PrintContractDetails", WebHelper.Instance.GetContractDetailsViewModel(personalDetailsVM.RegID, false));
                                contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
                                contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
                                contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide"); 
                                if (!string.IsNullOrEmpty(contractHTMLSource))
                                {
                                    Byte[] PDFBytes = Util.convertHTMLStringToPDFByteArray(contractHTMLSource, personalDetailsVM.RegID);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //Logger.Error("ICONTRACT EXCEPTION = " + ex);
                        WebHelper.Instance.LogExceptions(this.GetType(), ex);
                    }
                    #endregion

                    String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);

                    // this is for add mism auto-knockoff
                    // drop 4 - Gry
                    using (var autoProxy = new AutoActivationServiceProxy())
                    {
                        autoProxy.KenanAccountFulfill((int)MobileRegType.SecPlan, personalDetailsVM.RegID.ToString2());

                        if (Roles.IsUserInRole("MREG_DAPP"))
                        {
                            return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                        }
                        else { return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID }); }
                    }
				}
				#endregion

				#region Activate service
				if (collection["submit1"].ToString2() == "activateSvc")
				{
					response = FulfillKenanAccount(personalDetailsVM.RegID);

					if (response.Message == "success")
					{

						using (var proxy = new RegistrationServiceProxy())
						{
							if (personalDetailsVM.ExtraTenLimitConfirmation == "true")
							{

								if (personalDetailsVM.hdnExtraTenCount != null && personalDetailsVM.hdnExtraTenCount != string.Empty)
								{
									proxy.SaveExtraTenLogs(new ExtraTenLogReq
									{
										ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
										{
											ConfirmedBy = Util.SessionAccess.UserName,
											ConfirmedStatus = "Y",
											Count = Convert.ToInt32(personalDetailsVM.hdnExtraTenCount),
											CreatedDate = System.DateTime.Now,
											RegId = personalDetailsVM.RegID
										}
									});
								}
							}

							if (personalDetailsVM.ExtraTenConfirmation == "true")
							{

								proxy.SaveExtraTenLogs(new ExtraTenLogReq
								{
									ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
									{
										ConfirmedBy = Util.SessionAccess.UserName,
										ConfirmedStatus = "Y",
										CreatedDate = System.DateTime.Now,
										RegId = personalDetailsVM.RegID
									}
								});

							}
							else
							{
								proxy.SaveExtraTenLogs(new ExtraTenLogReq
								{
									ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
									{
										ConfirmedBy = Util.SessionAccess.UserName,
										ConfirmedStatus = "N",
										CreatedDate = System.DateTime.Now,
										RegId = personalDetailsVM.RegID
									}
								});
							}

							proxy.DeleteExtraTenDetails(personalDetailsVM.RegID);
							if (personalDetailsVM.ExtraTenConfirmation == "true")
							{
								foreach (var extratenMSISDN in personalDetailsVM.ExtraTenMobileNumbers.Split(','))
								{
									if (extratenMSISDN != string.Empty)
									{
										proxy.SaveExtraTen(new ExtraTenReq
										{

											ExtraTenDetails = new lnkExtraTenDetails { CreatedBy = Util.SessionAccess.UserName, CreatedDate = System.DateTime.Now, Msisdn = extratenMSISDN, RegId = personalDetailsVM.RegID, Status = "A" }


										});
									}
								}
							}
						}
						//END   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database

						return RedirectToAction("SecondaryLineRegSvcActivated", new { regID = personalDetailsVM.RegID });
					}
					else
					{
						return RedirectToAction("SecondaryLineRegClosed", new { regID = personalDetailsVM.RegID });
					}
				}
				#endregion


				if (personalDetailsVM.TabNumber == (int)SecondaryLineRegistrationSteps.Submit)
				{

					Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(false);





                    // 20141204 - Read the waiver components from the dropFourObj - start

                    /*
                    //Waiver formation cr by ravi kiran on 26 sept
                    List<DAL.Models.WaiverComponents> objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                    //Waiver formation cr by ravi kiran on 26 sept ends here
                    */

                    var dropFourObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                    List<DAL.Models.WaiverComponents> objWaiverComp = Util.constructWaiverComponentList(dropFourObj);

                    if (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove)
                    {
                        WebHelper.Instance.PaymentReceived(personalDetailsVM);
                    }

                    if (objWaiverComp == null || objWaiverComp.Count == 0)
                    {
                        //Waiver formation cr by ravi kiran on 26 sept
                        objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                        //Waiver formation cr by ravi kiran on 26 sept ends here
                    }
                    // 20141204 - Read the waiver components from the dropFourObj - start
                         

					// submit registration
					using (var proxy = new RegistrationServiceProxy())
					{
						//Added sim model id by ravikiran on 30 jan 2014 as it was not saving
						resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString2() : "false")),
														WebHelper.Instance.ConstructCustomer((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"]),
														WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
														WebHelper.Instance.ConstructRegSuppLineVASes(), null, objWaiverComp, personalDetailsVM.isBREFail);



						if (Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("MREG_DAI"))
						{
							string username = string.Empty;
							if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
							{
								username = Request.Cookies["CookieUser"].Value;
							}
							using (var objproxys = new RegistrationServiceProxy())
							{
								objproxys.RegistrationUpdate(new DAL.Models.Registration()
								{
									ID = resp.ID,
									SalesPerson = username,
									LastAccessID = Util.SessionAccess.UserName,



								});
							}

							WebHelper.Instance.CreateKenanAccount(personalDetailsVM.RegID);

						}


						if (resp.ID > 0)
						{
							///BYPASS THE BELOW CODE FOR Extra Ten IF REQUEST FROM Multiple MNP
							bool isMultipleMNP = personalDetailsVM.RegTypeID == (int)MobileRegType.RegType_MNPPlanWithMultiSuppline ? true : false;
							if (!isMultipleMNP)
								Util.SaveExtraTen(personalDetailsVM.ExtraTenMobileNumbers, resp.ID);
						}

						var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
						if (biometricID != 0)
						{
							var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
							biometric.RegID = resp.ID;

							proxy.BiometricUpdate(biometric);
						}
					}



					#region VLT ADDED CODE on 21stFEB for email functionality
					if (resp.ID > 0)
					{
						/* BEGIN Added by VLT 5 Apr 2013 to add kenaninfo to database*/
						if (Session["KenanCustomerInfo"] != null)
						{
							string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString2().Split('╚');
							KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
							kenanInfo.FullName = strArrKenanInfo[0];
							kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
							kenanInfo.IDCardNo = strArrKenanInfo[2];
							kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
							kenanInfo.Address1 = strArrKenanInfo[4];
							kenanInfo.Address2 = strArrKenanInfo[5];
							kenanInfo.Address3 = strArrKenanInfo[6];
							kenanInfo.Postcode = strArrKenanInfo[8];
							kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
							kenanInfo.RegID = resp.ID;
							using (var proxys = new RegistrationServiceProxy())
							{
								var kenanResp = new KenanCustomerInfoCreateResp();

								kenanResp = proxys.KenanCustomerInfoCreate(kenanInfo);
							}
						}
						Session["KenanCustomerInfo"] = null;
						/* END Added by VLT 5 Apr 2013 to add kenaninfo to database*/
						
						int regTypeID = 0;
						if (Session["RegMobileReg_Type"] != null)
						{
							regTypeID = Session["RegMobileReg_Type"].ToInt();
						}

                        //commented as disabling pos file feature
                        //#region Saving POS FileName in DB for Dealers
                        //if (Session["IsDealer"].ToBool() == true && collection["hdnSecLineTotalPayable"].ToString2() != string.Empty && collection["hdnSecLineTotalPayable"].ToDecimal() > 0)
                        //{
                        //    string posFileName = WebHelper.Instance.CreatePOSFileName(resp.ID);
                        //    using (var proxy = new CatalogServiceProxy())
                        //    {
                        //        proxy.UpdateIMPOSDetails(resp.ID, posFileName, 1);
                        //    }
                        //}
                        //#endregion

						string PlanKenanCode = MasterDataCache.Instance.PackageComponents.Where(a => a.ID == Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"])).Select(a => a.KenanCode).SingleOrDefault().ToString2();
						string latestPrintVersion = Util.GetPrintVersionNum(regTypeID, string.Empty, PlanKenanCode);
						

						#region "SecondaryLine New Account Feature"
						if (Util.IsSecLineNewAccount == true || collection["txtJustfication"].ToString2().Length > 0)
							if (Util.IsSecLineNewAccount == true || (!string.IsNullOrEmpty(latestPrintVersion)))
							{
								LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
								Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
								objRegDetails.CreatedDate = DateTime.Now;
								objRegDetails.UserName = Util.SessionAccess.UserName;
								objRegDetails.RegId = resp.ID;
								objRegDetails.Liberlization_Status = personalDetailsVM.Liberlization_Status;
								if (Util.IsSecLineNewAccount == true)
								{
									objRegDetails.IsSuppNewAc = false;
									objRegDetails.SimType = "MISM";
								}
								objRegDetails.Justification = collection["txtJustfication"].ToString2();
								if (!string.IsNullOrEmpty(latestPrintVersion))
								{
									objRegDetails.PrintVersionNo = latestPrintVersion; 
								}
								else
								{
									objRegDetails.PrintVersionNo = string.Empty;
								}
								//******selected sim card type is assigned*****//
								if (!string.IsNullOrEmpty(collection["SecondarySimCardTypeSelected"]))
									objRegDetails.MismSimCardType = collection["SecondarySimCardTypeSelected"].ToString2();
								//******selected sim card type is assigned*****//
								/*Active PDPA Commented*/
								/*Changes by chetan related to PDPA implementation*/
								//using (var registrationProxy = new RegistrationServiceProxy())
								//{
								//    var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
								//    objRegDetails.PdpaVersion = resultdata.Version;
								//}
								objRegDetails.ContractCheckCount = Session["AccountswithContractCount"].ToInt();
								/*Recode Active PDPA Commented*/
								objRegDetails.PdpaVersion = personalDetailsVM.PDPDVersion;

								// Gerry - PN 3967 - Reserve Number
								Logger.Debug("SecondaryLine skipkenan = " + Session["SkipKenanReservation"].ToBool());
								objRegDetails.IsMSISDNReserved = Session["SkipKenanReservation"].ToBool() ? true : false;

								objRegDetailsReq.LnkDetails = objRegDetails;
								using (var proxy = new RegistrationServiceProxy())
								{
									proxy.SaveLnkRegistrationDetails(objRegDetailsReq);
								}
							}
						if (Roles.IsUserInRole("MREG_DSV"))
						{
							#region Added by Nreddy for support suppline as new account
							using (var proxy = new RegistrationServiceProxy())
							{
								isSuppAsNew = proxy.getSupplineDetails(resp.ID);
								if (isSuppAsNew)
								{
									string username = string.Empty;
									if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
									{
										username = Request.Cookies["CookieUser"].Value;
									}

									proxy.RegistrationUpdate(new DAL.Models.Registration()
									{
										ID = resp.ID,
										SalesPerson = username,
										LastAccessID = Util.SessionAccess.UserName,
									});

									WebHelper.Instance.CreateKenanAccount(resp.ID);

								}

								else
								{
									using (var kenanProxy = new KenanServiceProxy())
									{
                                        String Action = WebHelper.Instance.WebPosAuto(resp.ID);
                                        //if (!ReferenceEquals(Action, String.Empty))
                                        //    return RedirectToAction(Action, new { regID = resp.ID });
                                        //kenanProxy.CreateIMPOSFile(resp.ID);
									}
                                    //using (var regproxy = new RegistrationServiceProxy())
                                    //{
                                    //    regproxy.RegistrationClose(new RegStatus()
                                    //    {
                                    //        RegID = resp.ID,
                                    //        Active = true,
                                    //        CreateDT = DateTime.Now,
                                    //        StartDate = DateTime.Now,
                                    //        LastAccessID = Util.SessionAccess.UserName,
                                    //        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                    //        ResponseCode = response.Code,
                                    //        ResponseDescription = response.Message,
                                    //        MethodName = "KenanAdditionLineRegistration"
                                    //    });
                                    //}
								}
							}
							return RedirectToAction("SecondaryLineRegAccCreated", new { regID = resp.ID });
							#endregion
						}
						//Assingn back to Default Value to Util
						Util.IsSecLineNewAccount = false;
						#endregion
						WebHelper.Instance.ClearRegistrationSession();

						return RedirectToAction("SecondaryLineRegSuccess", new { regID = resp.ID });
					}
					#endregion VLT ADDED CODE on 21stFEB for email functionality
				}
				else if (personalDetailsVM.TabNumber == (int)SecondaryLineRegistrationSteps.PersonalDetails)
				{
					return RedirectToAction("SecondaryLinePersonalDetails");
				}
			}
			catch (Exception ex)
			{
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
				throw ex;
			}

			return RedirectToAction("SecondaryLineRegFail", new { regID = personalDetailsVM.RegID });
		}

		[Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
		[DoNoTCache]
		public ActionResult SecondaryLineRegSuccess(int regID)
		{
			//Save the User TransactionLog
			WebHelper.Instance.SaveUserTransactionLogs(regID);

			return View(regID);
		}

		[Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
		[DoNoTCache]
		public ActionResult SecondaryLineRegSuccessNew(int regID)
		{
			//Save the User TransactionLog
			WebHelper.Instance.SaveUserTransactionLogs(regID);
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];
            RegDetailsObjVM regDetailsObj = new RegDetailsObjVM();

			using (var regProxy = new RegistrationServiceProxy())
			{
				regDetailsObj.LnkRegDetailsReq = regProxy.GetLnkRegistrationDetails(regID);
				regDetailsObj.reg = regProxy.RegistrationGet(regID);
			}

			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var regProxy = new RegistrationServiceProxy())
				{

					regDetailsObj.PegaVM = new PegaRecommendationsVM();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = regProxy.GetPegaRecommendationbyMSISDN(regDetailsObj.PegaVM.PegaRecommendationsSearchCriteria);
					regDetailsObj.PegaVM.PegaRecommendationsSearchResult = regDetailsObj.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();

				}
			}

            
            return View(regDetailsObj);
		}

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SecondaryLineRegSuccessNew(FormCollection collection, RegDetailsObjVM regDetailsObj)
        {
            DropFourObj dropObj = new DropFourObj();
            dropObj.personalInformation = new PersonalDetailsVM();
            dropObj.personalInformation.rfDocumentsFile = collection["frontPhoto"];
            dropObj.personalInformation.rfDocumentsFileName = collection["frontPhotoFileName"];
            WebHelper.Instance.SaveDocumentToTable(dropObj, regDetailsObj.RegID);
            ViewBag.documentUploaded = true;
            using (var proxy = new WebPOSCallBackSvc.WebPOSCallBackServiceClient())
            {
                proxy.sendDocsToIcontract(regDetailsObj.RegID, true);
            }
            return View(regDetailsObj);
        }

		[Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DSK,MREG_DAI,MREG_DAC,MREG_DIC")]
		[DoNoTCache]
		public ActionResult SecondaryLineRegCanceled(int regID)
		{
			return View(regID);
		}

		[Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DCH,MREG_DSK,MREG_DAI,MREG_DAC,MREG_DIC")]
		[DoNoTCache]
		public ActionResult SecondaryLineRegAccCreated(int regID)
		{
            TempData["ReadyForPaymentStatusID"] = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment);
            TempData["RegFailStatusID"] = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail);
            TempData["SvcActivatedStatusID"] = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_SvcActivated);
			return View(regID);
		}


		[HttpPost]
		public ActionResult SecondaryLineRegAccCreated(FormCollection collection)
		{
            int orderID = (collection["OrderId"]).ToInt(); 
            return View(orderID);
            //return RedirectToAction("WebPos", new { regid = (collection["OrderId"]).ToInt() });
		}


		[Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_CH,MREG_SK,MREG_DSV,MREG_DSA,MREG_DC,MREG_DCH,MREG_DAC,MREG_DIC,MREG_DAI,MREG_DAC")]
		[DoNoTCache]
		public ActionResult SecondaryLineRegSvcActivated(int regID)
		{

			return View(regID);
		}

		[Authorize(Roles = "MREG_W,MREG_C,MREG_CH,MREG_SK,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
		[DoNoTCache]
		public ActionResult SecondaryLineRegClosed(int regID)
		{
			return View(regID);
		}

		[Authorize(Roles = "MREG_W,MREG_C,MREG_CH,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
		public ActionResult SecondaryLineRegBreFail(int regID)
		{
			return View(regID);
		}

		[Authorize(Roles = "MREG_W,MREG_C,MREG_CH,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DC,MREG_DIC")]
		[DoNoTCache]
		public ActionResult SecondaryLineRegFail(int regID)
		{
			return View(regID);
		}

		[HttpPost]
		public ActionResult SecondaryLineRegFail(FormCollection collection)
		{
			return RedirectToAction("SecondaryLineRegSummary");
		}

        [Authorize]
        [HttpGet]
        [DoNoTCache]
        public ActionResult AccountsListMISM()
        {
            var dropObj = new DropFourObj();
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            Session[SessionKey.AccountCategory.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session["_componentViewModel"] = null;
            Session["FromPromoOffer"] = null;
            Session["MarketCode"] = null;
            Session["VasGroupIds"] = null;
            Session["VasGroupIds_Seco"] = null;
            Session["SelectedPlanID"] = null;
            Session["OfferId"] = null;
            Session["Discount"] = null;
            Session["RegMobileReg_MainDevicePrice"] = null;
            Session["RegMobileReg_OrderSummary"] = null;
            Session["RegMobileReg_OfferDevicePrice"] = null;
            Session["SimType"] = null;
            Session["RegMobileReg_MobileNo"] = null;
            Session["RegMobileReg_VirtualMobileNo_Sec"] = null;
            Session["ExtratenMobilenumbers"] = null;
            Session["Condition"] = null;
            Session["AccountHolder"] = null;
            ResetDepositSessions();
            //set the sessions to null when we are starting with adding new supplien by ravi on 15/07/2013
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session[SessionKey.SelectedComponents.ToString()] = null;
            Session[SessionKey.SpendLimit.ToString()] = null;

            ///REMOVE KEY FROM SESSION AS WHEN USER CLICKS Add Supp/Secon Line MULTIPORT IN SHOULD BE DISABLED
            if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
            {
                Session.Contents.Remove("MNPSupplementaryLines");
            }

            if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
            {
                Session.Contents.Remove("mNPPrimaryPlanStorage");
            }
            retrieveAcctListByICResponse acctListByICResponse = null;
            //SupplementaryListAccounts SuppListAccounts = null;                                    
            var supplementaryListAccounts = new Models.SupplementaryListAccounts();

            ///BELOW ERROR MIGHT GET POPULATED FROM TARGET PAGE
            if (!ReferenceEquals(TempData["Error"], null))
            {
                ModelState.AddModelError("Error", "Select an account");
            }


            ///CHECK PPID AND PPIDINFO SESSIONS
            if ((!ReferenceEquals(Session["PPID"], null) && "E".Equals(Convert.ToString(Session["PPID"]))) && !ReferenceEquals(Session["PPIDInfo"], null))
            {
                ///GET PPIDInfo FROM SESSION
                acctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                if (acctListByICResponse != null)
                {
                    var itemList = acctListByICResponse.itemList.Where(c => c.Account != null && c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob == "POSTGSM");
                    Online.Registration.Web.Helper.PrinSupServiceProxy _proxyObj = new PrinSupServiceProxy();
                    //LIST ONLY GSM A/C'S
                    if (itemList != null)
                    {
                        supplementaryListAccounts.SuppListAccounts = itemList.Where(i => i.ServiceInfoResponse.prinSuppInd == "P").Select(v => new AddSuppInquiryAccount
                        {
                            AccountNumber = v.AcctExtId,
                            ActiveDate = v.Account.ActiveDate,
                            Address = v.Account.Address,
                            Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                            MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                            CompanyName = v.Account.CompanyName,
                            Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                            IDNumber = v.Account.IDNumber,
                            IDType = v.Account.IDType,
                            // MarketCode = v.Account.MarketCode,
                            Plan = v.Account.Plan,
                            SubscribeNo = v.Account.SubscribeNo,
                            SubscribeNoResets = v.Account.SubscribeNoResets,
                            AccountStatus = v.ServiceInfoResponse.serviceStatus,
                            AccountName = v.ServiceInfoResponse.lob,
                            AccountType = "P",
                            externalId = v.ExternalId,
                            accountExtId = v.AcctExtId,
                            IsMISM = v.IsMISM,
                            accountIntId = v.AcctIntId,
                            SecondarySimList = v.SecondarySimList,
                            SupplinesCount = _proxyObj.getPrinSupplimentarylines(new SubscribergetPrinSuppRequest() { msisdn = v.ExternalId }).itemList.Count
                        }).ToList();
                    }
                }

                var mismPrimaryList = supplementaryListAccounts.SuppListAccounts.Where(i => i.IsMISM == true).ToList();

                if (mismPrimaryList.Any() && mismPrimaryList.Count() > 0)
                {
                    using (var subscriberProxy = new retrieveServiceInfoProxy())
                    {
                        foreach (var mism in mismPrimaryList)
                        {
                            var mismPackageDetails = subscriberProxy.retrievePackageDetls(
                                mism.externalId,
                                System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"],
                                userName: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName,
                                password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword,
                                postUrl: string.Empty
                                );
                            supplementaryListAccounts.packageModelDictionary.Add(mism.externalId, mismPackageDetails);

                        }

                    }
                    using (var regProxy = new RegistrationServiceProxy())
                    {
                        supplementaryListAccounts.dataComponentIDMISM = regProxy.GetDataPlanComponentsForMISM();
                    }
                }
            }

            Session["supplementaryListAccounts"] = supplementaryListAccounts;
            return View(supplementaryListAccounts);
        }

        [HttpPost]
        [Authorize]
        [DoNoTCache]
        public ActionResult AccountsListMISM(SupplementaryListAccounts suppListAccounts, FormCollection collection)
        {
            retrieveAcctListByICResponse AcctListByIC = null;
            List<Online.Registration.Web.SubscriberICService.Items> AccntList = null;
            Session[SessionKey.DedicatedSuppFlow.ToString()] = true;
            string[] input = collection["SelectSecoAccountDtls"].ToString2().Split(',');
            string selectedExternalId = string.Empty;

            try
            {
                if (input.Count() > 0)
                    selectedExternalId = input[1];

                if (Session["AcctAllDetails"] != null)
                {
                    AcctListByIC = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                    AccntList = AcctListByIC != null ? AcctListByIC.itemList != null ? AcctListByIC.itemList.Where(c => c.ExternalId == selectedExternalId && c.IsActive).ToList() : null : null;
                    if (AccntList.Count > 0)
                    {
                        Util.IsSecLineNewAccount = (collection["hdnIsSeclineNew"].ToString2() == "1" ? false : true);
                        Session["FxAccNo"] = AccntList[0].AcctIntId;
                        Session["ExternalID"] = AccntList[0].ExternalId;
                        Session["AccExternalID"] = AccntList[0].AcctExtId;
                        Session["KenanACNumber"] = AccntList[0].AcctExtId;
                        Session["Secondary"] = "SecondaryPlan";
                        Session["SelectedMsisdn"] = selectedExternalId;
                        Session[SessionKey.AccountCategory.ToString()] = AccntList[0].AccountDetails != null ? AccntList[0].AccountDetails.AcctCategory.ToString2() : string.Empty;
                        Session[SessionKey.MarketCode.ToString()] = AccntList[0].AccountDetails != null ? AccntList[0].AccountDetails.MktCode.ToString2() : string.Empty;
                    }
                }
                return RedirectToAction("SecondaryLineRegSummaryNew", "SuppLine");
            }
            catch
            {
                ModelState.AddModelError("", "There was a technical error while processing customer details. Please try again");
                suppListAccounts = (SupplementaryListAccounts)Session["supplementaryListAccounts"];

                return View(suppListAccounts);
            }
        }

        [Authorize]
        [HttpGet]
        [DoNoTCache]
        public ActionResult AccountsList()
        {
            var dropObj = new DropFourObj();
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            Session[SessionKey.AccountCategory.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session["_componentViewModel"] = null;
            Session["FromPromoOffer"] = null;
            Session["MarketCode"] = null;
            Session["VasGroupIds"] = null;
            Session["VasGroupIds_Seco"] = null;
            Session["SelectedPlanID"] = null;
            Session["OfferId"] = null;
            Session["Discount"] = null;
            Session["RegMobileReg_MainDevicePrice"] = null;
            Session["RegMobileReg_OrderSummary"] = null;
            Session["RegMobileReg_OfferDevicePrice"] = null;
            Session["SimType"] = null;
            Session["RegMobileReg_MobileNo"] = null;
            Session["RegMobileReg_VirtualMobileNo_Sec"] = null;
            Session["ExtratenMobilenumbers"] = null;
            Session["Condition"] = null;
            Session["AccountHolder"] = null;
            ResetDepositSessions();
            //set the sessions to null when we are starting with adding new supplien by ravi on 15/07/2013
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session[SessionKey.SelectedComponents.ToString()] = null;
            Session[SessionKey.SpendLimit.ToString()] = null;

            ///REMOVE KEY FROM SESSION AS WHEN USER CLICKS Add Supp/Secon Line MULTIPORT IN SHOULD BE DISABLED
            if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
            {
                Session.Contents.Remove("MNPSupplementaryLines");
            }

            if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
            {
                Session.Contents.Remove("mNPPrimaryPlanStorage");
            }
            retrieveAcctListByICResponse acctListByICResponse = null;
            //SupplementaryListAccounts SuppListAccounts = null;                                    
            var supplementaryListAccounts = new Models.SupplementaryListAccounts();

            ///BELOW ERROR MIGHT GET POPULATED FROM TARGET PAGE
            if (!ReferenceEquals(TempData["Error"], null))
            {
                ModelState.AddModelError("Error", "Select an account");
            }


            ///CHECK PPID AND PPIDINFO SESSIONS
            if ((!ReferenceEquals(Session["PPID"], null) && "E".Equals(Convert.ToString(Session["PPID"]))) && !ReferenceEquals(Session["PPIDInfo"], null))
            {
                ///GET PPIDInfo FROM SESSION
                acctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                if (acctListByICResponse != null)
                {
                    var itemList = acctListByICResponse.itemList.Where(c => c.Account != null && c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob == "POSTGSM");
                    Online.Registration.Web.Helper.PrinSupServiceProxy _proxyObj = new PrinSupServiceProxy();
                    //LIST ONLY GSM A/C'S
                    if (itemList != null)
                    {
                        supplementaryListAccounts.SuppListAccounts = itemList.Where(i => i.ServiceInfoResponse.prinSuppInd == "P").Select(v => new AddSuppInquiryAccount
                        {
                            AccountNumber = v.AcctExtId,
                            ActiveDate = v.Account.ActiveDate,
                            Address = v.Account.Address,
                            Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
                            MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
                            CompanyName = v.Account.CompanyName,
                            Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                            IDNumber = v.Account.IDNumber,
                            IDType = v.Account.IDType,
                            // MarketCode = v.Account.MarketCode,
                            Plan = v.Account.Plan,
                            SubscribeNo = v.Account.SubscribeNo,
                            SubscribeNoResets = v.Account.SubscribeNoResets,
                            AccountStatus = v.ServiceInfoResponse.serviceStatus,
                            AccountName = v.ServiceInfoResponse.lob,
                            AccountType = "P",
                            externalId = v.ExternalId,
                            accountExtId = v.AcctExtId,
                            IsMISM = v.IsMISM,
                            accountIntId = v.AcctIntId,
                            SecondarySimList = v.SecondarySimList,
                            SupplinesCount = _proxyObj.getPrinSupplimentarylines(new SubscribergetPrinSuppRequest() { msisdn = v.ExternalId }).itemList.Count
                        }).ToList();
                    }
                }
            }
            Session["supplementaryListAccounts"] = supplementaryListAccounts;
            return View(supplementaryListAccounts);
        }

        [HttpPost]
        [Authorize]
        [DoNoTCache]
        public ActionResult AccountsList(SupplementaryListAccounts suppListAccounts, FormCollection collection)
        {
            retrieveAcctListByICResponse AcctListByIC = null;
            List<Online.Registration.Web.SubscriberICService.Items> AccntList = null;
            Session[SessionKey.DedicatedSuppFlow.ToString()] = true;
            string[] input = collection["SelectAccountDtls"].ToString2().Split(',');
            string selectedExternalId = string.Empty;

            try
            {
                if (input.Count() > 0)
                {
                    selectedExternalId = input[1];
                    Session["SelectedMsisdn"] = selectedExternalId;
                }

                if (Session["AcctAllDetails"] != null)
                {
                    AcctListByIC = (retrieveAcctListByICResponse)Session["AcctAllDetails"];
                    AccntList = AcctListByIC != null ? AcctListByIC.itemList != null ? AcctListByIC.itemList.Where(c => c.ExternalId == selectedExternalId && c.IsActive).ToList() : null : null;
                    if (AccntList.Count > 0)
                    {
                        Session["FxAccNo"] = AccntList[0].AcctIntId;
                        Session["ExternalID"] = AccntList[0].ExternalId;
                        Session["AccExternalID"] = AccntList[0].AcctExtId;
                        Session["KenanACNumber"] = AccntList[0].AcctExtId;
                        Session[SessionKey.AccountCategory.ToString()] = AccntList[0].AccountDetails != null ? AccntList[0].AccountDetails.AcctCategory.ToString2() : string.Empty;
                        Session[SessionKey.MarketCode.ToString()] = AccntList[0].AccountDetails != null ? AccntList[0].AccountDetails.MktCode.ToString2() : string.Empty;
                    }
                }

                Session["IsDeviceRequired"] = collection["hdnIsDeviceRequired"].ToString2();

                if (Session["IsDeviceRequired"].ToString2() == "Yes")
                {
                    return RedirectToAction("SelectDevicePlan", "SuppLine", new { type = (int)MobileRegType.DevicePlan });
                }
                else
                {
                    return RedirectToAction("SuppLinePlanNew", "SuppLine", new { type = (int)MobileRegType.SuppPlan });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "There was a technical error while processing customer details. Please try again");
                suppListAccounts = (SupplementaryListAccounts)Session["supplementaryListAccounts"];

                return View(suppListAccounts);
            }
        }

		[Authorize]
		public ActionResult AddSuppLine()
		{
			Session[SessionKey.AccountCategory.ToString()] = null;
			Session[SessionKey.MarketCode.ToString()] = null;
			Session["_componentViewModel"] = null;
			Session["FromPromoOffer"] = null;
			Session["MarketCode"] = null;
			Session["VasGroupIds"] = null;
			Session["VasGroupIds_Seco"] = null;
			Session["SelectedPlanID"] = null;
			Session["OfferId"] = null;
			Session["Discount"] = null;
			Session["RegMobileReg_MainDevicePrice"] = null;
			Session["RegMobileReg_OrderSummary"] = null;
			Session["RegMobileReg_OfferDevicePrice"] = null;
			Session["SimType"] = null;
			Session["RegMobileReg_MobileNo"] = null;
			Session["RegMobileReg_VirtualMobileNo_Sec"] = null;
			Session["ExtratenMobilenumbers"] = null;
			Session["Condition"] = null;
			Session["AccountHolder"] = null;
			ResetDepositSessions();
			//set the sessions to null when we are starting with adding new supplien by ravi on 15/07/2013
			Session["RegMobileReg_PersonalDetailsVM"] = null;
			Session[SessionKey.SelectedComponents.ToString()] = null;
			Session[SessionKey.SpendLimit.ToString()] = null;



			///REMOVE KEY FROM SESSION AS WHEN USER CLICKS Add Supp/Secon Line MULTIPORT IN SHOULD BE DISABLED
			if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
			{
				Session.Contents.Remove("MNPSupplementaryLines");
			}

			if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
			{
				Session.Contents.Remove("mNPPrimaryPlanStorage");
			}
			retrieveAcctListByICResponse acctListByICResponse = null;
			//SupplementaryListAccounts SuppListAccounts = null;                                    
			var supplementaryListAccounts = new Models.SupplementaryListAccounts();

			///BELOW ERROR MIGHT GET POPULATED FROM TARGET PAGE
			if (!ReferenceEquals(TempData["Error"], null))
			{
				ModelState.AddModelError("Error", "Select an account");
			}


			///CHECK PPID AND PPIDINFO SESSIONS
			if ((!ReferenceEquals(Session["PPID"], null) && "E".Equals(Convert.ToString(Session["PPID"]))) && !ReferenceEquals(Session["PPIDInfo"], null))
			{
				///GET PPIDInfo FROM SESSION
				acctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
				if (acctListByICResponse != null)
				{
					var itemList = acctListByICResponse.itemList.Where(c => c.Account != null && c.ServiceInfoResponse != null && c.ServiceInfoResponse.lob == "POSTGSM");
					Online.Registration.Web.Helper.PrinSupServiceProxy _proxyObj = new PrinSupServiceProxy();
					//LIST ONLY GSM A/C'S
					if (itemList != null)
					{
						supplementaryListAccounts.SuppListAccounts = itemList.Where(i => i.ServiceInfoResponse.prinSuppInd == "P").Select(v => new AddSuppInquiryAccount
						{
							AccountNumber = v.AcctExtId,
							ActiveDate = v.Account.ActiveDate,
							Address = v.Account.Address,
							Category = v.AccountDetails != null ? v.AccountDetails.AcctCategory.ToString2() : string.Empty,
							MarketCode = v.AccountDetails != null ? v.AccountDetails.MktCode.ToString2() : string.Empty,
							CompanyName = v.Account.CompanyName,
							Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
							IDNumber = v.Account.IDNumber,
							IDType = v.Account.IDType,
							// MarketCode = v.Account.MarketCode,
							Plan = v.Account.Plan,
							SubscribeNo = v.Account.SubscribeNo,
							SubscribeNoResets = v.Account.SubscribeNoResets,
							AccountStatus = v.ServiceInfoResponse.serviceStatus,
							AccountName = v.ServiceInfoResponse.lob,
							AccountType = "P",
							externalId = v.ExternalId,
							accountExtId = v.AcctExtId,
							IsMISM = v.IsMISM,
							accountIntId = v.AcctIntId,
							SecondarySimList = v.SecondarySimList,
							SupplinesCount = _proxyObj.getPrinSupplimentarylines(new SubscribergetPrinSuppRequest() { msisdn = v.ExternalId }).itemList.Count
						}).ToList();
					}
				}
			}
			Session["supplementaryListAccounts"] = supplementaryListAccounts;
			return View(supplementaryListAccounts);
		}

		[HttpPost]
		public ActionResult AddSuppLine(FormCollection collection, string SelectAccountDtls = null, string SelectSecoAccountDtls = null)
		{
			int supliAcctConfig = System.Configuration.ConfigurationManager.AppSettings.Get("totalSupplimentaryLinesAllowed").ToInt();

			if (SelectSecoAccountDtls != null)
			{
				string[] strSecoAccountDtls = SelectSecoAccountDtls.Split(',');
				if (strSecoAccountDtls[4].ToString2() == "True")
				{
					Session["IsNewBillingAddress"] = "ExistingBillingAddress";
				}
			}
			else
			{
				if (collection[4].ToString2() == "1")
				{
					Session["IsNewBillingAddress"] = "IsNewBillingAddress";
				}
				else
				{
					Session["IsNewBillingAddress"] = "ExistingBillingAddress";
				}
			}


			if (SelectAccountDtls != null)
			{

				// New Changes supplementary New or Existing Account
				Session[SessionKey.IsSuppleNewAccount.ToString()] = (collection["hdnIsSupplineNew"].ToString2() == "1" ? true : false);
				Session["IsDeviceRequired"] = collection["hdnIsDeviceRequired"].ToString2();
				//End of New Changes
				string tblId = string.Empty;
				string[] strAccountDtls = SelectAccountDtls.Split(',');
				Session["FxAccNo"] = strAccountDtls[0].ToString2();
				Session["ExternalID"] = strAccountDtls[1].ToString2();
				Session["AccExternalID"] = strAccountDtls[2].ToString2();
				Session["KenanACNumber"] = strAccountDtls[3].ToString2();
				Session[SessionKey.AccountCategory.ToString()] = strAccountDtls[7];
				Session[SessionKey.MarketCode.ToString()] = strAccountDtls[8];
				if (supliAcctConfig > 0)
				{
					int supplineAccCount = !ReferenceEquals(strAccountDtls[6], null) && !string.IsNullOrEmpty(strAccountDtls[6]) ? strAccountDtls[6].ToInt() : 0;
					if ((supplineAccCount > 0) && (supplineAccCount >= supliAcctConfig))
					{
						ModelState.AddModelError("CustomError", "Selected Principal Account has more than or equal to " + supliAcctConfig + " supplimentary Account Lines, you cannot proceed further.");
						return View((SupplementaryListAccounts)Session["supplementaryListAccounts"]);
					}
				}

				using (var proxy = new UserServiceProxy())
				{
					proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Select Account", Session["KenanACNumber"].ToString2(), "");
				}

				if (Session["IsDeviceRequired"].ToString2() == "Yes")
				{
					return RedirectToAction("SelectDevice", "SuppLine", new { type = (int)MobileRegType.DevicePlan });
				}
				else
				{
					return RedirectToAction("SuppLinePlan", "SuppLine", new { type = (int)MobileRegType.SuppPlan });
				}

			}
			if (SelectSecoAccountDtls != null)
			{

				// New Changes secondary line New or Existing Account
				Util.IsSecLineNewAccount = (collection["hdnIsSeclineNew"].ToString2() == "1" ? false : true);
				//End of New Changes
				string tblId = string.Empty;
				string[] strAccountDtls = SelectSecoAccountDtls.Split(',');
				Session["FxAccNo"] = strAccountDtls[0].ToString2();
				Session["ExternalID"] = strAccountDtls[1].ToString2();
				Session["AccExternalID"] = strAccountDtls[2].ToString2();
				Session["KenanACNumber"] = strAccountDtls[3].ToString2();
				Session[SessionKey.AccountCategory.ToString()] = strAccountDtls[7];
				Session[SessionKey.MarketCode.ToString()] = strAccountDtls[8];
				Session["Secondary"] = "SecondaryPlan";
				return RedirectToAction("SecondaryLinePlan", "SuppLine", new { type = (int)MobileRegType.SecPlan });

			}

			///BELOW WILL CONTAIN THE SELECTED ACCOUNTNUMBER
			///PLEASE REDIRECT TO APPROPRIATE PAGE FROM THIS POINT ONWARDS. DO NOT FORGET TO EXPIRE THE SESSION WHEN YOU ARE DONE WITH IT.
			Session["SelectedAccountNumber"] = SelectAccountDtls;
			///REPLACE THE BELOW REDIRECTION TO YOUR PAGE
			return RedirectToAction("AddSuppLine");
		}

		[Authorize]
		[HttpPost]
		public ActionResult SuppLinePlan(FormCollection collection, SublinePackageVM sublinePkgVM)
		{
			try
			{

				MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;
				///INSTANCE WILL CONTAIN LIST OF SUPPLEMENTARY LINES HAVING DIFFERENT STATES SUCH AS 
				///MNPSupplimentaryDetailsAddState.NOTINITIALIZED
				///MNPSupplimentaryDetailsAddState.PROCESSING
				///MNPSupplimentaryDetailsAddState.PROCESSED
				///ONLY ONE MSISDN CAN BE IN PROCESSING STATE AT ANY POINT OF TIME
				MNPSelectPlanForSuppline SuppleMentaryLine = null;
				MNPSupplinePlanStorage MNPSupplinePlanStorage = null;

				//List of Suppline attached to primary line changes
				SelectPlanForSuppline selectPlanForSuppline = null;
				List<SupplinePlanStorage> supplinePlanStorageList;
				SupplinePlanStorage supplinePlanStorage = null;
				Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]); 

				if (IsMNPWithMultpleLines)
				{
					///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
					MNPPrimaryPlanStorage = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];

					if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
					{
						///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
						SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()];

						///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
						if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
						{
							ProcessingMsisdn = SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
							///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
							MNPSupplinePlanStorage = (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
						}
					}
				}


				if (primarySupplimentLines)
				{
					supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : null;

					if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
					{
						///GET ALL SUPPLEMENTARY LINES STORED IN SESSION
						selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString()];

						///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
						if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
						{
							ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
							///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
							supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
						}
					}
				}


				using (var proxy = new UserServiceProxy())
				{
					if (Session["IsDeviceRequired"].ToString2() == "Yes")
					{
						proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Select Supplementary Plan", Session["KenanACNumber"].ToString2(), "");
					}
					else
					{
						proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SuppPlan, "Select Supplementary Plan", Session["KenanACNumber"].ToString2(), "");
					}
				}
				if (collection["submit1"].ToString2().ToUpper() == "NEXT")
				{
					using (var proxy = new CatalogServiceProxy())
					{
						var pbpc = MasterDataCache.Instance.FilterComponents(new int[] { sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID }).SingleOrDefault();

						if (pbpc != null && !ReferenceEquals(pbpc.KenanCode, null))
						{
						   

							if (primarySupplimentLines)
							{
								supplinePlanStorage.RegMobileReg_UOMCode = pbpc.KenanCode;
							}
							else
							{
								Session["UOMPlanID"] = pbpc.KenanCode;
							}

						}

					}
					// selected different Sub Plan
					///MNP MULTIPORT RELATED CHANGES
					///IF IsMNPWithMultpleLines TRUE THEN
					if (IsMNPWithMultpleLines)
					{
						if (MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID != sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID)
						{
							MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
							MNPSupplinePlanStorage.SelectedPlanID = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
							Session["RegMobileReg_PkgPgmBdlPkgCompID"] = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
						}
					}
					else if (primarySupplimentLines)
					{
						if (supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID != sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID)
						{
							supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
							supplinePlanStorage.SelectedPlanID = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
							Session["RegMobileReg_PkgPgmBdlPkgCompID"] = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
						}
					}
					else
					{
						if (Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt() != sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID)
						{
							//previous components are getting selected
							if (sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID != Session["SelectedPlanID"].ToInt())
							{
								Session[SessionKey.SelectedComponents.ToString()] = null;
								Session["ExtratenMobilenumbers"] = null;

							}
							//components are getting selected Ends here

							Session["RegMobileReg_PkgPgmBdlPkgCompID"] = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
							Session["SelectedPlanID"] = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
						}
					}

					var mobileNos = new List<string>();


					for (int i = 0; i < Properties.Settings.Default.NoOfDesireMobileNo; i++)
					{
						mobileNos.Add(collection["txtMobileNo_" + i].ToString2());
					}

					///MNP MULTIPORT RELATED CHANGES
					if (IsMNPWithMultpleLines)
					{
						MNPSupplinePlanStorage.RegMobileSub_MobileNo = mobileNos;
					}
					else if (primarySupplimentLines)
					{
						supplinePlanStorage.RegMobileSub_MobileNo = mobileNos;
					}
					else
					{
						Session["RegMobileSub_MobileNo"] = mobileNos;
					}


					//Spend Limit Commented by Ravi As per New Flow on June 15 2013
					//Spend Limit
					if (collection["hdnSpendLimit"].ToString2()!="")
						Session[SessionKey.SpendLimit.ToString()] = collection["hdnSpendLimit"].ToString2();

					//Spend Limit
				}
				else if (collection["submit1"].ToString2().ToLower() == "back")
				{
					if (Session["IsDeviceRequired"].ToString2() == "Yes")
					{
						return RedirectToAction("SelectDeviceCatalog");
					}
					else
					{
						return RedirectToAction("AddSuppLine");
					}
				}
				else if (collection["submit1"].ToString2().ToLower() == "mnpselectsuppline")
				{
					///MNP MULTIPORT RELATED CHANGES
					///WILL FIRE WHEN BACK BUTTON PRESSED FROM SUPPLINEPLAN PAGE
					return RedirectToAction("MNPSelectSuppLine", "Registration");
				}
				else
				{
					return RedirectToAction("SuppLineVAS");
				}
			}
			catch (Exception ex)
			{
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
				throw ex;
			}

			return RedirectToAction("SuppLineVAS");
		}

		[Authorize]
		[HttpPost]
		public ActionResult SuppLinePlanNew(FormCollection collection, SublinePackageVM sublinePkgVM)
		{
			bool isDedicatedSuppFlow = Session[SessionKey.DedicatedSuppFlow.ToString()].ToBool();
			string selectedMSISDN = Session["ExternalID"].ToString2();
			retrieveAcctListByICResponse allServiceDetails = (retrieveAcctListByICResponse)Session["AllServiceDetails"];
			var sublineVM = new SublinePackageVM();
			sublineVM.PackageVM = GetAvailablePackages(true);
			sublineVM.PersonalDetailsVM = new PersonalDetailsVM();

			try
			{
				MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;
				///INSTANCE WILL CONTAIN LIST OF SUPPLEMENTARY LINES HAVING DIFFERENT STATES SUCH AS 
				///MNPSupplimentaryDetailsAddState.NOTINITIALIZED
				///MNPSupplimentaryDetailsAddState.PROCESSING
				///MNPSupplimentaryDetailsAddState.PROCESSED
				///ONLY ONE MSISDN CAN BE IN PROCESSING STATE AT ANY POINT OF TIME
				MNPSelectPlanForSuppline SuppleMentaryLine = null;
				MNPSupplinePlanStorage MNPSupplinePlanStorage = null;

				//List of Suppline attached to primary line changes
				SelectPlanForSuppline selectPlanForSuppline = null;
				List<SupplinePlanStorage> supplinePlanStorageList;
				SupplinePlanStorage supplinePlanStorage = null;
				Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]);

				if (IsMNPWithMultpleLines)
				{
					///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
					MNPPrimaryPlanStorage = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];

					if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
					{
						///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
						SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()];

						///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
						if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
						{
							ProcessingMsisdn = SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
							///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
							MNPSupplinePlanStorage = (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
						}
					}
				}


				if (primarySupplimentLines)
				{
					supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : null;

					if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
					{
						///GET ALL SUPPLEMENTARY LINES STORED IN SESSION
						selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString()];

						///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
						if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
						{
							ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
							///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
							supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
						}
					}
				}


				using (var proxy = new UserServiceProxy())
				{
					if (Session["IsDeviceRequired"].ToString2() == "Yes")
					{
						proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Select Supplementary Plan", Session["KenanACNumber"].ToString2(), "");
					}
					else
					{
						proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SuppPlan, "Select Supplementary Plan", Session["KenanACNumber"].ToString2(), "");
					}
				}
				if (collection["submit1"].ToString2().ToUpper() == "NEXT")
				{
					using (var proxy = new CatalogServiceProxy())
					{
						var pbpc = MasterDataCache.Instance.FilterComponents(new int[] { sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID }).SingleOrDefault();

						if (pbpc != null && !ReferenceEquals(pbpc.KenanCode, null))
						{


							if (primarySupplimentLines)
							{
								supplinePlanStorage.RegMobileReg_UOMCode = pbpc.KenanCode;
							}
							else
							{
								Session["UOMPlanID"] = pbpc.KenanCode;
							}

						}

					}
					// selected different Sub Plan
					///MNP MULTIPORT RELATED CHANGES
					///IF IsMNPWithMultpleLines TRUE THEN
					if (IsMNPWithMultpleLines)
					{
						if (ReferenceEquals(MNPSupplinePlanStorage,null) ||  MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID != sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID)
						{
							MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
							MNPSupplinePlanStorage.SelectedPlanID = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
							Session["RegMobileReg_PkgPgmBdlPkgCompID"] = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
						}
					}
					else if (primarySupplimentLines)
					{
						if (supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID != sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID)
						{
							supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
							supplinePlanStorage.SelectedPlanID = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
							Session["RegMobileReg_PkgPgmBdlPkgCompID"] = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
						}
					}
					else
					{
						if (Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt() != sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID)
						{
							//previous components are getting selected
							if (sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID != Session["SelectedPlanID"].ToInt())
							{
								Session[SessionKey.SelectedComponents.ToString()] = null;
								Session["ExtratenMobilenumbers"] = null;

							}
							//components are getting selected Ends here

							Session["RegMobileReg_PkgPgmBdlPkgCompID"] = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
							Session["SelectedPlanID"] = sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID;
						}
					}

					var mobileNos = new List<string>();


					for (int i = 0; i < Properties.Settings.Default.NoOfDesireMobileNo; i++)
					{
						mobileNos.Add(collection["txtMobileNo_" + i].ToString2());
					}

					///MNP MULTIPORT RELATED CHANGES
					if (IsMNPWithMultpleLines)
					{
						MNPSupplinePlanStorage.RegMobileSub_MobileNo = mobileNos;
					}
					else if (primarySupplimentLines)
					{
						supplinePlanStorage.RegMobileSub_MobileNo = mobileNos;
					}
					else
					{
						Session["RegMobileSub_MobileNo"] = mobileNos;
					}


					//Spend Limit Commented by Ravi As per New Flow on June 15 2013
					//Spend Limit
					if (collection["hdnSpendLimit"].ToString2() != "")
						Session[SessionKey.SpendLimit.ToString()] = collection["hdnSpendLimit"].ToString2();

					//Spend Limit
				}
				else if (collection["submit1"].ToString2().ToLower() == "back")
				{
					if (Session["IsDeviceRequired"].ToString2() == "Yes")
					{
						

						return RedirectToAction("SelectDevicePlan");
					}
					else
					{
						return RedirectToAction("IndexNew","Home");
					}
				}
				else if (collection["submit1"].ToString2().ToLower() == "mnpselectsuppline")
				{
					///MNP MULTIPORT RELATED CHANGES
					///WILL FIRE WHEN BACK BUTTON PRESSED FROM SUPPLINEPLAN PAGE
					return RedirectToAction("MNPSelectSuppLine", "Registration");
				}
				else
				{

					if (IsMNPWithMultpleLines)
					{
						MNPSupplinePlanStorage.RegMobileReg_SublineSummary = ConstructOrderSummary(fromSubline: true);
					}
					else if (primarySupplimentLines)
					{
						supplinePlanStorage.RegMobileReg_SublineSummary = ConstructOrderSummary(fromSubline: true);
					}
					else
					{
						Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(fromSubline: true);
					}

					var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

					// validation for quotaShare
					#region QuotaShare

					if (sharingQuotaFeature)
					{
						sublineVM.errorMsg = isDedicatedSuppFlow ? WebHelper.Instance.quotaShareCheckingQty(selectedMSISDN, allServiceDetails, null)
								: WebHelper.Instance.quotaShareCheckingQty(string.Empty, null, dropObj);

						if (!string.IsNullOrEmpty(sublineVM.errorMsg))
						{
							return View(sublineVM);
						}
					}

					#endregion

					DropFourHelpers.UpdatePackage(dropObj, sublinePkgVM.PackageVM);
					Session[SessionKey.DropFourObj.ToString()] = dropObj;
					return RedirectToAction("SuppLineVASNew");
				}
			}
			catch (Exception ex)
			{
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
				throw ex;
			}


			Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(fromSubline: true);

			var dropObj1 = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

			DropFourHelpers.UpdatePackage(dropObj1, sublinePkgVM.PackageVM);

			#region QuotaShare

			if (sharingQuotaFeature)
			{

				sublineVM.errorMsg = isDedicatedSuppFlow ? WebHelper.Instance.quotaShareCheckingQty(selectedMSISDN, allServiceDetails, null, selectedPBPCID: sublinePkgVM.PackageVM.SelectedPgmBdlPkgCompID.ToString2())
						: WebHelper.Instance.quotaShareCheckingQty(string.Empty, null, dropObj1);

				if (!string.IsNullOrEmpty(sublineVM.errorMsg))
				{
					return View(sublineVM);
				}
			}

			#endregion
			
            //Anthony: New IC (search id type: 1) and Other IC (search id type: 3) will be tagged as Malaysian
            /*if (Session["SearchBy"].ToString2().ToUpper() == "NRIC" || Session["SearchBy"].ToString2().ToUpper() == "NEWIC" || Session["SearchBy"].ToString2().ToUpper() == "OTHERIC")
            {
                if (!ReferenceEquals(dropObj1.personalInformation, null))
                    dropObj1.personalInformation.Customer.NationalityID = 1;
            }
            else */
            if (Session[SessionKey.IDCardTypeID.ToString()].ToInt() != 2)
            {//other than passport, the nationality id = 1
                if (!ReferenceEquals(dropObj1.personalInformation, null))
                    dropObj1.personalInformation.Customer.NationalityID = 1;
            }

			Session[SessionKey.DropFourObj.ToString()] = dropObj1;

			return RedirectToAction("SuppLineVASNew");
		}



		/// <summary>
		/// Method for Getting Registration Status
		/// </summary>
		/// <param name="id">RegId</param>
		/// <returns></returns>
		[HttpPost]
		public JsonResult GetRegStatus(int regId)
		{
		  
			bool isSuppAsNew = false;
			DAL.Models.Registration reg = new DAL.Models.Registration();
			List<DAL.Models.WaiverComponents> objWaiverComponents = new List<WaiverComponents>();
			int statusID = 0;
			bool isSuccess = false;

			// this is for supplementary flag, true for standalone, false for non-standalone
			bool isStandAloneSupp = false;

			//if ((Roles.IsUserInRole("MREG_DIC") || Roles.IsUserInRole("MREG_DAI") || Roles.IsUserInRole("MREG_DSV")))
			//{
				using (var regProxy = new RegistrationServiceProxy())
				{
					isSuppAsNew = regProxy.getSupplineDetails(regId);

					reg = regProxy.RegistrationGet(regId);

					objWaiverComponents = regProxy.GetWaiverComponentsbyRegID(regId);
				}
			//}
			statusID = WebHelper.Instance.GetRegStatus(regId);
			bool isWaived = true;

			//if ((Roles.IsUserInRole("MREG_DIC") || Roles.IsUserInRole("MREG_DAI") || Roles.IsUserInRole("MREG_DSV")))
			//{
            //if (Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate).ToInt() == statusID)
            //{
            //    if (Roles.IsUserInRole("MREG_DAPP") && Session["isSuppAsNew"].ToBool())
            //    {
            //        String Action = WebHelper.Instance.WebPosAuto(reg.ID);
            //    }
            //}

            if (ConfigurationManager.AppSettings["ReadyForPaymentStatusID"].ToInt() == statusID && reg != null && (reg.RegTypeID.ToInt() == (int)MobileRegType.SuppPlan || reg.RegTypeID.ToInt() == (int)MobileRegType.MNPSuppPlan))
				{
					foreach (var waiverComp in objWaiverComponents)
					{
						if (waiverComp.IsWaived == false)
						{
							isWaived = false;
							break;
						}
					}
				}
			//}

            //Ricky - implement workaround due to waiver issue - PN2046
            decimal totalPayable = reg.PlanAdvance + reg.PlanDeposit + reg.DeviceAdvance + reg.DeviceDeposit;
            //if (isWaived == true)

            if (totalPayable == 0)
            {
                if (reg.RegTypeID.ToInt() == (int)MobileRegType.MNPSuppPlan)
                {
                    #region For MNP Flow

                    using (var proxy = new KenanServiceProxy())
                    {

                        var request1 = new MNPtypeFullfillCenterOrderMNP()
                        {
                            orderId = regId.ToString(),
                        };
                        bool statusMNPSup = proxy.KenanMNPAccountFulfill(request1);
                        if (statusMNPSup)
                        {
                            return new JsonResult() { Data = "AutoActivation" };
                        }

                    }

                    #endregion
                }
                else
                {
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        // adding condition for standalone supplementary
                        isStandAloneSupp = proxy.getSupplineDetails(reg.ID);

                        proxy.RegistrationCancel(new RegStatus()
                        {
                            RegID = regId,
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                        });
                    }
                    var request = new OrderFulfillRequest()
                    {
                        OrderID = regId.ToString(),
                        UserSession = Util.SessionAccess.UserName,

                    };
                    using (var proxy = new KenanServiceProxy())
                    {
                        // adding condition for standalone supp
                        if (!isStandAloneSupp)
                        {
                            //isSuccess = proxy.KenanAccountFulfill(request);
                            KenanNewSuppLineResponse response = FulfillKenanAccount(regId);
                        }
                        else
                        {
                            var standAloneFulFill = proxy.KenanAccountFulfill(request);
                        }
                    }
                    return new JsonResult() { Data = "AutoActivation" };
                }
            }
            
            return new JsonResult() { Data = statusID.ToString()};
		}

        [HttpPost]
        public String ReconstructOrderSummary(String isSuppline = "True", String isStandalone = "False")
        {
            bool fromSubline = isSuppline.ToLower().ToBool();
            bool standalone = isStandalone.ToLower().ToBool();
            OrderSummaryVM orderVM = (OrderSummaryVM)WebHelper.Instance.ConstructOrderSummary(string.Empty, this.GetType().Name, fromSubline, standalone);
            var personalDetailsVM = !ReferenceEquals(orderVM, null) ? (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"] : new PersonalDetailsVM();
            String partialHTMLString = string.Empty;
            partialHTMLString = Util.RenderPartialViewToString(this, "_ExtraCharges", personalDetailsVM);
            return partialHTMLString;
            //return PartialView("_ExtraCharges",personalDetailsVM);
        }

		public ActionResult PrintContractDetails(int id)
		{
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
			var regFormVM = new RegistrationFormVM();
			var modelImageIDs = new List<int>();
			var pbpcIDs = new List<int>();
			var suppLineVASIDs = new List<int>();
			var suppLineForms = new List<SuppLineForm>();
			var regMdlGrpModels = new List<RegMdlGrpModel>();
			var regMdlGrpModelsSec = new List<RegMdlGrpModelSec>();
			var modelImageIDsSec = new List<int>();
			var pbpcIDsSec = new List<int>();
			var suppLineFormsSec = new List<SuppLineForm>();
			var printVersionInfo = new List<PrintVersionDetails>();

			//Joshi Added for DME Printer
			//if (HttpContext.Current.Request.UserAgent.ToLower().Contains("ipad"))
			if (HttpContext.Request.Headers["User-Agent"].ToLower().Contains("ipad"))
			{
				var UserDMEPrint = new UserDMEPrint();
				UserDMEPrint.RegID = id;
				UserDMEPrint.UserID = Util.SessionAccess.UserID;
				var status = Util.SaveUserDMEPrint(UserDMEPrint);
				Session["StatusDME"] = status;
			}
			//End of Joshi


			var PlanKenanCode = string.Empty;
			using (var proxy = new RegistrationServiceProxy())
			{
				// Registration
				regFormVM.Registration = proxy.RegistrationGet(id);

                // Device Financing Attribute Details
                regFormVM.DFRegAttribute = proxy.RegAttributesGetByRegID(id);

				// Registration Secondary
				regFormVM.RegistrationSec = proxy.RegistrationSecGet(id);

				// Customer
				regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

				// Address
				var regAddrIDs = proxy.RegAddressFind(new AddressFind()
				{
					Active = true,
					Address = new Address()
					{
						RegID = id
					}
				}).ToList();
				regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

				// RegMdlGrpModels
				var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
				{
					RegMdlGrpModel = new RegMdlGrpModel()
					{
						RegID = id
					}
				}).ToList();
				if (regMdlGrpModelIDs.Count() > 0)
				{
					regMdlGrpModels = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
					modelImageIDs = regMdlGrpModels.Select(a => a.ModelImageID).ToList();
				}


				// RegMdlGrpModels for Secondary
				if (regFormVM.RegistrationSec != null)
				{
					var regMdlGrpModelIDsSec = proxy.RegMdlGrpModelSecFind(new RegMdlGrpModelSecFind()
					{
						RegMdlGrpModelSec = new RegMdlGrpModelSec()
						{
							SecRegID = regFormVM.RegistrationSec.ID
						}
					}).ToList();


					if (regMdlGrpModelIDsSec.Count() > 0)
					{
						regMdlGrpModelsSec = proxy.RegMdlGrpModelSecGet(regMdlGrpModelIDsSec).ToList();
						modelImageIDsSec = regMdlGrpModelsSec.Select(a => a.ModelImageID).ToList();
					}

				}


				//Commented by VLT on 11 Apr 2013
				// RegPgmBdlPkgComponent
				var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
				{
					RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
					{
						RegID = id,
					}
				}).ToList();

				var pbpc = new List<RegPgmBdlPkgComp>();
				if (regPgmBdlPkgCompIDs.Count() > 0)
					pbpc = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();


				//main line
				if (pbpc.Count() > 0)
					pbpcIDs = pbpc.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();

				// RegPgmBdlPkgComponent for Secondary
				if (regFormVM.RegistrationSec != null)
				{
					var regPgmBdlPkgCompIDsSec = proxy.RegPgmBdlPkgCompSecFind(new RegPgmBdlPkgCompSecFind()
					{
						RegPgmBdlPkgCompSec = new RegPgmBdlPkgCompSec()
						{
							RegID = regFormVM.RegistrationSec.ID,
						}
					}).ToList();

					var pbpcSec = new List<RegPgmBdlPkgCompSec>();
					if (regPgmBdlPkgCompIDsSec.Count() > 0)
						pbpcSec = proxy.RegPgmBdlPkgCompSecGet(regPgmBdlPkgCompIDsSec).ToList();

					//main line
					if (pbpcSec.Count() > 0)
						pbpcIDsSec = pbpcSec.Where(a => a.RegSuppLineID == null).Select(a => a.PgmBdlPckComponentID).ToList();


				}


				var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind() { RegSuppLine = new RegSuppLine() { RegID = id } }).ToList();
				if (suppLineIDs.Count() > 0)
				{
					var regSuppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
					foreach (var regSuppLine in regSuppLines)
					{
						regFormVM.Registration.PlanAdvance += regSuppLine.planadvance;
						regFormVM.Registration.PlanDeposit += regSuppLine.plandeposit;

						suppLineForms.Add(new SuppLineForm()
						{
							SuppLinePBPCID = regSuppLine.PgmBdlPckComponentID,
							SuppLineVASIDs = pbpc.Where(a => a.RegSuppLineID == regSuppLine.ID && a.PgmBdlPckComponentID != regSuppLine.PgmBdlPckComponentID).Select(a => a.PgmBdlPckComponentID).ToList(),
							SIMSerial = regSuppLine.SIMSerial,
							MSISDN = regSuppLine.MSISDN1,
							SimModelId = regSuppLine.SimModelId
						});
					}
				}

				ExtraTenGetResp objExtraTenDetails = new ExtraTenGetResp();
				objExtraTenDetails = proxy.GetExtraTenDetails(id);
				List<lnkExtraTenDetails> modListExtraTen = new List<lnkExtraTenDetails>();
				regFormVM.ExtraTenDetails = objExtraTenDetails.ExtraTenDetails.ToList();

			}

			using (var proxy = new CatalogServiceProxy())
			{

				// ModelGroupModel
				if (modelImageIDs.Count() > 0)
				{
					var modelImages = proxy.BrandArticleModelImageGet(modelImageIDs).ToList();
					var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());
					bool deviceFinancingOrder = WebHelper.Instance.determineDFOrderbyRegID(id);
					foreach (var regMdlGrpModel in regMdlGrpModels)
					{
						var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
						decimal offerPrice = 0;
						if (deviceFinancingOrder)
						{
							string totalprice = regFormVM.DFRegAttribute.Where(a => a.ATT_Name == RegAttributes.ATTRIB_DF_TOTAL_PRICE && a.SuppLineID == 0).FirstOrDefault().ATT_Value;
							string discountprice = regFormVM.DFRegAttribute.Where(a => a.ATT_Name == RegAttributes.ATTRIB_DF_DISCOUNT && a.SuppLineID == 0).FirstOrDefault().ATT_Value;
							offerPrice = Convert.ToDecimal(totalprice) - Convert.ToDecimal(discountprice);
						}
						else
						{
							offerPrice = regMdlGrpModel.Price.ToDecimal();
						}

						regFormVM.DeviceForms.Add(new DeviceForm()
						{
							Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
							Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
							Price = offerPrice
						});

						var colour = Util.GetNameByID(RefType.Colour, modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ColourID);
						Session["RegMobileReg_DeviceColour"] = null;
						Session["RegMobileReg_DeviceColour"] = colour;
					}
				}
				var vasNames = (List<string>)Session["RegMobileReg_VasNames"];
				int NewPackageId = 0;

				// ModelGroupModel for Secondary
				if (modelImageIDsSec.Count() > 0)
				{
					var modelImages = proxy.BrandArticleModelImageGet(modelImageIDsSec).ToList();
					var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());

					foreach (var regMdlGrpModel in regMdlGrpModelsSec)
					{
						var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
						regFormVM.DeviceFormsSec.Add(new DeviceForm()
						{
							Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
							Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
							Price = regMdlGrpModel.Price.ToDecimal()
						});
					}
				}

				var vasNamesSec = (List<string>)Session["RegMobileReg_VasNames_Seco"];

				// BundlePackage, PackageComponents
				if (pbpcIDs.Count() > 0)
				{
					var pgmBdlPkgComps = MasterDataCache.Instance.FilterComponents(pbpcIDs).Distinct().ToList();
					var bpCode = Properties.Settings.Default.Bundle_Package;
					var pcCode = Properties.Settings.Default.Package_Component;
					var spCode = Properties.Settings.Default.SecondaryPlan;
					regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode && (a.PlanType == "CP" || a.PlanType == "SP")).ToList();
					if (regFormVM.BundlePackages.Count > 0)
					{
						regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == bpCode && (a.PlanType == "CP" || a.PlanType == "SP")).ToList();
					}
					else
					{
						regFormVM.BundlePackages = pgmBdlPkgComps.Where(a => a.LinkType == spCode).ToList();
					}
					if (regFormVM.BundlePackages.Count > 0)
						NewPackageId = regFormVM.BundlePackages[0].ID;
					PlanKenanCode = regFormVM.BundlePackages[0].KenanCode;//Added for T&C implementation

					regFormVM.PackageComponents = pgmBdlPkgComps.Where((a => a.LinkType.ToLower() == "pc" || a.LinkType.ToLower() == "ec" || a.LinkType.ToLower() == "dc" || a.LinkType.ToLower() == "ma" || a.LinkType.ToLower() == "oc" || a.LinkType.ToLower() == "rc" || a.LinkType.ToLower() == "cr" || a.LinkType.ToLower() == "md" || a.LinkType.ToLower() == "dm" || a.LinkType.ToLower() == "ic")).ToList();
				}
				List<int> vasids = new List<int>();
				if ((regFormVM.Registration.CRPType == "1") || (regFormVM.Registration.CRPType == "3"))
				{
					List<RegSmartComponents> selectedcomps = WebHelper.Instance.GetSelectedComponents(regFormVM.Registration.ID.ToString());
					List<string> compkenancodes = selectedcomps.Select(a => a.ComponentID).ToList();
					ComponentViewModel Componentresponse = GetSelectedPackageInfo_smart(0, NewPackageId, "");
					List<string> vasnames = new List<string>();

					if (Componentresponse.NewComponentsList.Count > 0)
					{
						for (int cnt = 0; cnt < Componentresponse.NewComponentsList.Count; cnt++)
						{
							if (compkenancodes.Contains(Componentresponse.NewComponentsList[cnt].NewComponentKenanCode))
								vasids.Add(Componentresponse.NewComponentsList[cnt].NewComponentId.ToInt());

						}

						// getting dependency ids
						List<int> depCompIds = new List<int>();
						foreach (var vasid in vasids)
						{
							if (vasid > 0)
							{

								depCompIds.AddRange(WebHelper.Instance.GetDepenedencyComponentsCRPPlan(vasid.ToString()));

							}
						}
						foreach (var depid in depCompIds)
						{
							if (depid > 0)
							{
								vasids.Add(depid.ToInt());
							}

						}

						Session["RegMobileReg_VasNames"] = proxy.ComponentGet(MasterDataCache.Instance.FilterComponents(vasids).Select(a => a.ChildID).ToList()).Select(a => a.Name).ToList();
					}
				}
				Session["RegMobileReg_VasNamesWithPackages"] = null;
				//Getting Vas names with Packages for Change Rate Plan
				if (regFormVM.Registration.RegTypeID == 19)
				{
					if ((regFormVM.Registration.CRPType == "2") || (regFormVM.Registration.CRPType == "4"))
					{
						// p2s and s2p                         
						vasids = (from cmp in regFormVM.PackageComponents select cmp.ID).ToList();
					}

					var components = MasterDataCache.Instance.FilterComponents(vasids);
					List<ComponentWithPkg> compswithpackages = (from ct in components select new ComponentWithPkg(ct.ChildID, ct.LinkType)).ToList();

					if (compswithpackages != null && compswithpackages.Count > 0)
					{
						compswithpackages = compswithpackages.GroupBy(p => new { p.ComponentId, p.LinkType }).Select(g => g.FirstOrDefault()).ToList();
					}

					Session["RegMobileReg_VasNamesWithPackages"] = proxy.ComponentGetWithPackage(compswithpackages);
				}

				// BundlePackage, PackageComponents for Secondary
				if (pbpcIDsSec.Count() > 0)
				{
					var pgmBdlPkgCompsSec = MasterDataCache.Instance.FilterComponents(pbpcIDsSec).Distinct().ToList();
					var bpCode = Properties.Settings.Default.Bundle_Package;
					var pcCode = Properties.Settings.Default.Package_Component;

					//regFormVM.BundlePackagesSec = pgmBdlPkgCompsSec.Where(a => a.LinkType == bpCode).ToList();
					regFormVM.BundlePackagesSec = pgmBdlPkgCompsSec.Where(a => (a.LinkType == bpCode || a.LinkType == "SP") && (a.PlanType == "CP" || a.PlanType == "SP")).ToList();
					regFormVM.PackageComponentsSec = pgmBdlPkgCompsSec.Where((a => a.LinkType.ToLower() == "pc" || a.LinkType.ToLower() == "ec" || a.LinkType.ToLower() == "dc" || a.LinkType.ToLower() == "ma" || a.LinkType.ToLower() == "rc" || a.LinkType.ToLower() == "cr" || a.LinkType.ToLower() == "md" || a.LinkType == "ic")).ToList();
				}

				foreach (var suppLineForm in suppLineForms)
				{
					//supp line package name
					suppLineForm.SuppLineBdlPkgName = proxy.PackageGet(MasterDataCache.Instance.FilterComponents(new int[] { suppLineForm.SuppLinePBPCID })
																.Select(a => a.ChildID)).SingleOrDefault().Name;
					//supp line vas names
					suppLineForm.SuppLineVASNames = proxy.ComponentGet(MasterDataCache.Instance.FilterComponents(suppLineForm.SuppLineVASIDs).Where(a => a.LinkType.ToLower() != "bp").Select(a => a.ChildID).ToList()).Select(a => a.Name).ToList();
				}
				regFormVM.SuppLineForms = suppLineForms;

				if (regFormVM.Registration.OfferID > 0)
				{
					string offerName = proxy.GetOfferNameById(Convert.ToInt32(regFormVM.Registration.OfferID));
					Session["offerNamebyId"] = offerName;

				}
				if (regFormVM.Registration.RegTypeID == Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_DevicePlan))
				{
					List<DAL.Models.Component> componentsList = Util.GetContractsBYRegId(id);
					if (componentsList != null && componentsList.Count > 0)
					{
						Session["SelectedContractName"] = componentsList.Select(vas => vas.Name).ToList();
						int Duration = 0;
						foreach (var item in componentsList)
						{
							using (var RSproxy = new CatalogServiceProxy())
							{
								string strValue = RSproxy.GetContractByCode(item.Code);
								if (Duration < strValue.ToInt())
								{
									Duration = strValue.ToInt();
								}
							}

						}
						Session["SelectedContractValue"] = Duration;
					}

					List<VoiceContractDetails> lstVoiceContractAddendums = Util.GetVoiceContractsBYRegId(id);
					if (lstVoiceContractAddendums != null && lstVoiceContractAddendums.Count > 0)
					{
						Session["VoiceAddendums"] = lstVoiceContractAddendums[0].ContractDetails;
					}
				}
				//secondary Offer Name
				if (regFormVM.RegistrationSec != null)
				{
					if (regFormVM.RegistrationSec.RegistrationID > 0)
					{
						if (regFormVM.RegistrationSec.OfferID > 0)
						{
							string offerName = proxy.GetOfferNameById(Convert.ToInt32(regFormVM.RegistrationSec.OfferID));
							Session["offerNamebyId_Seco"] = offerName;

						}
					}
				}
				//Contract Name Dispaly for Secondary
				if (regFormVM.RegistrationSec != null)

					if (regFormVM.RegistrationSec.RegTypeID == Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_DevicePlan))
					{

						int contractId = proxy.GetContractIdByRegIDSeco(regFormVM.RegistrationSec.ID);
						if (contractId > 0)
						{
							List<int> contractsList = new List<int>();
							List<Component> componentsList = new List<Component>();
							contractsList.Add(contractId);

							var vases = MasterDataCache.Instance.FilterComponents
							(
							(IEnumerable<int>)contractsList
							).ToList();

							componentsList = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();
							if (componentsList != null && componentsList.Count > 0)
							{
								string selectedContract = string.Empty;
								string strValue = proxy.GetContractByCode(componentsList[0].Code);
								Session["SelectedContractValue_Seco"] = strValue;
								List<string> lstComponentNames = new List<string>();
								lstComponentNames = componentsList.Where(vas => vas.ID == vases[0].ChildID).Select(vas => vas.Name).ToList();
								if (lstComponentNames != null && lstComponentNames.Count > 0)
								{
									selectedContract = lstComponentNames[0];
									Session["SelectedContractName_Seco"] = selectedContract;
								}

							}
						}
					}

			}

			using (var proxy = new RegistrationServiceProxy())
			{
				DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(id);
				Online.Registration.DAL.Models.PrintVersion objPrintVersionDetails = new Online.Registration.DAL.Models.PrintVersion();

				//Condition added by ravi on nov 1 2013 as it was throwing exception.
				if (lnkregdetails != null)
				{
					objPrintVersionDetails = proxy.GetPrintTsCsInfo(lnkregdetails.PrintVersionNo.ToString2(), regFormVM.Registration.RegTypeID, regFormVM.Registration.ArticleID == null ? string.Empty : regFormVM.Registration.ArticleID, PlanKenanCode, "");
					if (objPrintVersionDetails != null)
					{
						if (objPrintVersionDetails.ConTC1ModelIds != null && objPrintVersionDetails.ConTC1ModelIds != null)
						{
							string BrandModelID = proxy.GetBrandModelIDbyArticleID(regFormVM.Registration.ArticleID == null ? string.Empty : regFormVM.Registration.ArticleID);
							if (!string.IsNullOrEmpty(BrandModelID))
							{
								string[] arrModelIDs = objPrintVersionDetails.ConTC1ModelIds.Split('|');
								if (arrModelIDs.Contains(BrandModelID))
								{
									Session["TsCs"] = objPrintVersionDetails.TsCs;
									Session["ConTsCs"] = objPrintVersionDetails.ConTC1;
								}
								else
								{
									Session["TsCs"] = objPrintVersionDetails.TsCs;
									Session["ConTsCs"] = objPrintVersionDetails.ConTC;
								}
							}
							else
							{
								Session["TsCs"] = objPrintVersionDetails.TsCs;
								Session["ConTsCs"] = objPrintVersionDetails.ConTC;
							}

						}
						else
						{

							Session["TsCs"] = objPrintVersionDetails.TsCs;
							Session["ConTsCs"] = objPrintVersionDetails.ConTC;
						}
					}
					else
					{
						Session["TsCs"] = null;
					}
					regFormVM.LnkRegistrationDetails = lnkregdetails;
				}
			}

            using (var proxyOrg = new OrganizationServiceProxy())
            {
                if (!ReferenceEquals(regFormVM.Registration, null) && !ReferenceEquals(regFormVM.Registration.CenterOrgID, null) && regFormVM.Registration.CenterOrgID != 0)
                {
                    regFormVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { regFormVM.Registration.CenterOrgID }).FirstOrDefault();
                }
            }
            WebHelper.Instance.MapRegAttributesByRegID(regFormVM, !ReferenceEquals(regFormVM.Registration, null) ? regFormVM.Registration.ID : id);
			//Added to display terms and condition for MI rewards/ Added by Sumit start
			regFormVM.VoiceContractDetails = Util.GetRebateDataContractsBYRegId(id);
			//Added to display terms and condition for MI rewards/ Added by Sumit end

			return View(regFormVM);
		}



		[HttpPost, ValidateInput(false)]
		public ActionResult PrintMobileRegSummary(FormCollection collection, RegistrationFormVM personalDetailsVM)
		{
			Util.CommonPrintMobileRegSummary(collection);
			return RedirectToAction("MobileRegSummary", new { id = collection[1].ToString2() });

		}
		
		[Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DN")]
		public ActionResult SuppLineVASNew()
		{
			var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

			ViewBag.BackButton = GetVasBackButton();
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
			Session["MandatoryVasIds"] = null;
            Session["VasGroupIds"] = null;
            Session["df_showOffer"] = null;
			ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
			ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
			MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;
			///INSTANCE WILL CONTAIN LIST OF SUPPLEMENTARY LINES HAVING DIFFERENT STATES SUCH AS 
			///MNPSupplimentaryDetailsAddState.NOTINITIALIZED
			///MNPSupplimentaryDetailsAddState.PROCESSING
			///MNPSupplimentaryDetailsAddState.PROCESSED
			///ONLY ONE MSISDN CAN BE IN PROCESSING STATE AT ANY POINT OF TIME
			List<MOCOfferDetails> offers = new List<MOCOfferDetails>();
			MNPSelectPlanForSuppline SuppleMentaryLine = null;
			MNPSupplinePlanStorage MNPSupplinePlanStorage = null;

			//List of Suppline attached to primary line changes
			SelectPlanForSuppline selectPlanForSuppline = null;
			List<SupplinePlanStorage> supplinePlanStorageList;
			SupplinePlanStorage supplinePlanStorage = null;
			Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]);



			if (IsMNPWithMultpleLines)
			{
				///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
				MNPPrimaryPlanStorage = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];

				if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
				{
					///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
					SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()];

					///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
					if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
					{
						ProcessingMsisdn = SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
						///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
						MNPSupplinePlanStorage = (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
					}
				}
			}
			else if (primarySupplimentLines)
			{
				///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
				supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : null;


				if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
				{
					///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
					selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString()];

					///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
					if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
					{
						ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
						///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
						supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
					}
				}
			}
			else
			{
				Session["RegMobileReg_OrderSummary"] = null;
				Session["RegMobileReg_SublineSummary"] = null;
				Session["RegMobileReg_VasNames"] = null;
				Session["RegMobileReg_VasIDs"] = null;
				Session["RegMobileReg_SublineVM"] = null;
			}
			// Gerry - Drop5
			offers = WebHelper.Instance.GetMOCOfferByArticleID(Session[SessionKey.ArticleId.ToString()].ToString2());

			if (offers != null)
				Lstvam2.Offers = offers;
			if (Session["PPID"].ToString2() != string.Empty)
			{
				if (!ReferenceEquals(Session["RedirectURL"], null))
				{
					Session.Contents.Remove("RedirectURL");
				}
				SetRegStepNo(MobileRegistrationSteps.Vas);

				///MNP MULTIPORT RELATED CHANGES
				if (IsMNPWithMultpleLines)
				{
					if (ReferenceEquals(MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID, null) || string.IsNullOrWhiteSpace(MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID.ToString()))
					{
						MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID = MNPSupplinePlanStorage.GuidedSales_PkgPgmBdlPkgCompID;
					}

					MNPSupplinePlanStorage.RegMobileReg_OrderSummary = ConstructOrderSummary(fromSubline: true);
				}
				else if (primarySupplimentLines)
				{
					if (ReferenceEquals(supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID, null) || string.IsNullOrWhiteSpace(supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID.ToString()))
					{
						supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID = supplinePlanStorage.GuidedSales_PkgPgmBdlPkgCompID;
					}

					supplinePlanStorage.RegMobileReg_OrderSummary = ConstructOrderSummary(fromSubline: true);
				}
				else
				{
					if (Session["RegMobileReg_PkgPgmBdlPkgCompID"] == null || Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2() == string.Empty)
					{
						Session["RegMobileReg_PkgPgmBdlPkgCompID"] = Session["GuidedSales_PkgPgmBdlPkgCompID"];
					}

					Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(fromSubline: true);
				}

				///MNP MULTIPORT RELATED CHANGES
				OrderSummaryVM orderVM = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_OrderSummary : (OrderSummaryVM)Session["RegMobileReg_SublineSummary"];
				Int32 Selecpgmid;

				///MNP MULTIPORT RELATED CHANGES
				if (IsMNPWithMultpleLines)
				{
					Selecpgmid = (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.SelectedPgmBdlPkgCompID != null) ? orderVM.SelectedPgmBdlPkgCompID : MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID;
				}
				else if (primarySupplimentLines)
				{
					Selecpgmid = (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.SelectedPgmBdlPkgCompID != null) ? orderVM.SelectedPgmBdlPkgCompID : supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID;
				}
				else
				{
					Selecpgmid = (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.SelectedPgmBdlPkgCompID != null) ? orderVM.SelectedPgmBdlPkgCompID : Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt();
				}


				//Lstvam = WebHelper.Instance.GetAvailablePackageComponents_old(Selecpgmid, ref ProcessingMsisdn, isMandatory: false, fromSuppline: true);
				Lstvam = WebHelper.Instance.GetAvailablePackageComponents(Selecpgmid, ref ProcessingMsisdn, isMandatory: false, fromSuppline: true);



				// for Device+Plans, get the offers to be listed in VAS components
				string _mobileRegType = Session["MobileRegType"].ToString2();
				string _regMobileReg_Type = Session["RegMobileReg_Type"].ToString2();
				if (_regMobileReg_Type.ToNullableInt() == (int)MobileRegType.DevicePlan)
				{
					string offernames = string.Empty;
					string planid = primarySupplimentLines ? (supplinePlanStorage != null ? supplinePlanStorage.RegMobileReg_UOMCode : Session["UOMPlanID"].ToString2()) : Session["UOMPlanID"].ToString2();
					string articleid = Session[SessionKey.ArticleId.ToString()].ToString2();
					string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString2() : string.Empty;
					// drop 5
					Lstvam.ContractOffers = WebHelper.Instance.GetContractOffers(Lstvam, articleid, planid, MOCStatus);
				}


				var selectedCompIDs = new List<int>();
				List<AvailableVAS> lstvas = new List<AvailableVAS>();


				lstvas.AddRange(orderVM.VasVM.MandatoryVASes);

				for (int i = 0; i < orderVM.Sublines.Count(); i++)
				{
					for (int j = 0; j < orderVM.Sublines[i].SelectedVasNames.Count; j++)
					{
						lstvas.Add(new AvailableVAS() { PgmBdlPkgCompID = orderVM.Sublines[i].SelectedVasIDs[j], ComponentName = orderVM.Sublines[i].SelectedVasNames[j].ToString2() });
					}

				}


				for (int i = 0; i < lstvas.Count(); i++)
				{
					selectedCompIDs.Add(lstvas[i].PgmBdlPkgCompID);
				}



				Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);

				Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
				Lstvam2.ContractOffers.AddRange(Lstvam.ContractOffers);

                //14012015 - Anthony - GST Trade Up - Start
                foreach (var tradeUpAmountList in Lstvam2.ContractOffers)
                {
                    if (tradeUpAmountList.isTradeUp.ToBool())
                    {
                        var OfferID = tradeUpAmountList.Id;
                        var tradeUpAmounts = tradeUpAmountList.TradeUpAmount.ToString().Split(',').ToList();

                        foreach (var tradeUpAmount in tradeUpAmounts)
                        {
                            var tradeUpList = new TradeUpComponent();
                            var price = tradeUpAmount.ToInt();
                            tradeUpList.OfferId = OfferID;
                            tradeUpList.Name = "RM " + price;
                            tradeUpList.Price = price * -1;
                            Lstvam2.TradeUpList.Add(tradeUpList);
                        }
                    }
                }

                if (string.IsNullOrEmpty(Session[SessionKey.TradeUpAmount.ToString()].ToString2()) || Session[SessionKey.TradeUpAmount.ToString()].ToString2().Equals("0"))
                {
                    Session[SessionKey.TradeUpAmount.ToString()] = "empty";
                }
                //14012015 - Anthony - GST Trade Up - Start

				///MNP MULTIPORT RELATED CHANGES
				if (IsMNPWithMultpleLines)
				{
					MNPSupplinePlanStorage.RegMobileReg_VasNames = orderVM.VasVM.MandatoryVASes;
				}
				else if (primarySupplimentLines)
				{
					supplinePlanStorage.RegMobileReg_VasNames = orderVM.VasVM.MandatoryVASes;
				}
				else
				{
					Session["RegMobileReg_VasNames"] = orderVM.VasVM.MandatoryVASes;
				}
				/* For Mandatory packages */
				///MNP MULTIPORT RELATED CHANGES
				if (IsMNPWithMultpleLines)
				{
					MNPSupplinePlanStorage.vasmandatoryids = string.Empty;
				}
				else if (primarySupplimentLines)
				{
					supplinePlanStorage.vasmandatoryids = string.Empty;
				}
				else
				{
					Session["vasmandatoryids"] = string.Empty;
				}

				using (var proxy = new CatalogServiceProxy())
				{

					BundlepackageResp respon = new BundlepackageResp();

					///MNP MULTIPORT RELATED CHANGES
					respon = proxy.GetMandatoryVas(IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID : Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));

					for (int i = 0; i < respon.values.Count(); i++)
					{

						Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false, fromSuppLine: true);
						if (respon.values[i].Plan_Type == Properties.Settings.Default.Master_Component)
						{

							///MNP MULTIPORT RELATED CHANGES
							if (IsMNPWithMultpleLines)
							{
								MNPSupplinePlanStorage.intmandatoryidsval = respon.values[i].BdlDataPkgId;
							}
							else if (primarySupplimentLines)
							{
								supplinePlanStorage.intmandatoryidsval = respon.values[i].BdlDataPkgId;
							}
							else
							{
								Session["intmandatoryidsval"] = respon.values[i].BdlDataPkgId;
								dropObj.suppFlow[dropObj.suppIndex - 1].MandatoryPackage = respon.values[i].BdlDataPkgId;
							}

							Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
						}

						if (respon.values[i].Plan_Type == Properties.Settings.Default.CompType_DataCon)
						{

							List<int> Finalintvalues = null;

							///MNP MULTIPORT RELATED CHANGES
							//if (IsMNPWithMultpleLines)
							//{
							//    if (MNPSupplinePlanStorage.RegMobileReg_Type == 1)
							//    {
							//        List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

							//        string modelid = ((OrderSummaryVM)(MNPSupplinePlanStorage.RegMobileReg_OrderSummary)).ModelID.ToString();

							//        Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

							//    }
							//}
							//else
							//{
							//    if (Session["RegMobileReg_Type"].ToInt() == 1)
							//    {
							//        List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

							//        string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

							//        Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

							//    }
							//}

							List<PkgDataPlanId> valuesplan = proxy.getFilterplanvalues(2).values.ToList();

							Finalintvalues = valuesplan.Select(a => a.BdlDataPkgId).ToList();


							var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

															 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

															 select new AvailableVAS()
															 {
																 ComponentName = e.ComponentName,
																 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
																 Price = e.Price,
																 Value = e.Value,
																 GroupId = e.GroupId,
																 GroupName = e.GroupName,
																 KenanCode = e.KenanCode,
																 isSharing = !ReferenceEquals(e.isSharing, null) ? e.isSharing : false,
																 isVRCDataBooster = !ReferenceEquals(e.isVRCDataBooster, null) ? e.isVRCDataBooster : false
															 };

							///MNP MULTIPORT RELATED CHANGES
							if (IsMNPWithMultpleLines)
							{
								MNPSupplinePlanStorage.intdataidsval = respon.values[i].BdlDataPkgId;
							}
							else if (primarySupplimentLines)
							{
								supplinePlanStorage.intdataidsval = respon.values[i].BdlDataPkgId;
							}
							else
							{
								Session["intdataidsval"] = respon.values[i].BdlDataPkgId;
								dropObj.suppFlow[dropObj.suppIndex - 1].DataPackage = respon.values[i].BdlDataPkgId;
							}
							finalfiltereddatacontracts = finalfiltereddatacontracts.Where(v => v.GroupId != null);

							// normal dataComponent will not be as Databooster or sharing
							Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts.Where(x => !x.isVRCDataBooster && !x.isSharing));

							Lstvam2.NonShareVRCDataBooster.AddRange(finalfiltereddatacontracts.Where(x => x.isVRCDataBooster == true && x.isSharing == false));
							Lstvam2.ShareVRCDataBooster.AddRange(finalfiltereddatacontracts.Where(x => x.isVRCDataBooster == true && x.isSharing == true));
						}
						/*Extra packages*/
						if (respon.values[i].Plan_Type == Properties.Settings.Default.Extra_Component)
						{


							List<int> Finalintvalues = null;
							///MNP MULTIPORT RELATED CHANGES
							//if (IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_Type == 1 : Session["RegMobileReg_Type"].ToInt() == 1)
							//{
							//    List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();
							//    ///MNP MULTIPORT RELATED CHANGES
							//    string modelid = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_OrderSummary.ModelID.ToString() : ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

							//    Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

							//}

							List<PkgDataPlanId> valuesplan = proxy.getFilterplanvalues(2).values.ToList();

							Finalintvalues = valuesplan.Select(a => a.BdlDataPkgId).ToList();



							var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

															 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

															 select new AvailableVAS()
															 {
																 ComponentName = e.ComponentName,
																 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
																 Price = e.Price,
																 Value = e.Value,
																 GroupId = e.GroupId,
																 GroupName = e.GroupName,
																 KenanCode = e.KenanCode

															 };

							///MNP MULTIPORT RELATED CHANGES
							if (IsMNPWithMultpleLines)
							{
								MNPSupplinePlanStorage.intextraidsval = respon.values[i].BdlDataPkgId;
							}
							else if (primarySupplimentLines)
							{
								supplinePlanStorage.intextraidsval = respon.values[i].BdlDataPkgId;
							}
							else
							{
								Session["intextraidsval"] = respon.values[i].BdlDataPkgId;
								dropObj.suppFlow[dropObj.suppIndex - 1].ExtraPackage = respon.values[i].BdlDataPkgId;

							}

							Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);


						}
						//Pramotional Packages
						if (respon.values[i].Plan_Type == Properties.Settings.Default.Pramotional_Component)
						{
							Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
							#region COmmented filterComponents for Pramotional components
							//List<int> Finalintvalues = null;
							//if (Session["RegMobileReg_Type"].ToInt() == 1)
							//{
							//    List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();
							//    string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();
							//    Finalintvalues = values.Select(a => a.BdlDataPkgId).Distinct().ToList();
							//    Lstvam.DeviceType = "Seco_NoDevice";
							//}
							//else if (Session["RegMobileReg_Type"].ToInt() == 2)
							//{
							//    List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
							//    Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
							//}
							//var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

							//                                 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

							//                                 select new AvailableVAS
							//                                 {
							//                                     ComponentName = e.ComponentName,
							//                                     PgmBdlPkgCompID = e.PgmBdlPkgCompID,
							//                                     Price = e.Price,
							//                                     Value = e.Value,
							//                                     GroupId = e.GroupId,
							//                                     GroupName = e.GroupName,
							//                                     KenanCode = e.KenanCode
							//                                 }; 
							#endregion
							Session["intpramotionidsval"] = respon.values[i].BdlDataPkgId;
							dropObj.suppFlow[dropObj.suppIndex - 1].PromotionPackage = respon.values[i].BdlDataPkgId;

							//MNP MULTIPORT RELATED CHANGES
							if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
							{
								if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
								{
									((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intpramotionidsval = respon.values[i].BdlDataPkgId;
								}
							}

							//Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
							//Condition added for SM30 by sumit start
							if (Session["IsNewBillingAddress"].ToString2() != "IsNewBillingAddress" && Session["IsNewBillingAddress"].ToString2() != "")
							{
								using (var PrinSupproxy = new Online.Registration.Web.Helper.PrinSupServiceProxy())
								{
									var req = new SubscribergetPrinSuppRequest();
									retrievegetPrinSuppResponse response = null;
									req.eaiHeader = null;
									if (Session["ExternalID"] != null)
									{
										req.msisdn = Session["ExternalID"].ToString2();
									}//Need to add condition for mnp / sumit
									DateTime dtReq = DateTime.Now;
									//response = PrinSupproxy.getPrinSupplimentarylines(new SubscribergetPrinSuppRequest
									//{
									// eaiHeader = header,
									// msisdn = v.ExternalId
									//});
									response = PrinSupproxy.getPrinSupplimentarylines(req);
									if (response != null)
									{
										if (response.itemList.Count > 0)
										{
											foreach (var v in response.itemList)
											{
												retrieveAcctListByICResponse AcctListByICResponse = null;
												if (!ReferenceEquals(Session[SessionKey.PPIDInfo.ToString()], null))
												{
													AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()];
												}
												//var result = AcctListByICResponse.itemList.Where(a => a.AcctIntId == v.acct_noField.ToString() && a.ExternalId == req.msisdn).Select(a => a.AcctExtId).FirstOrDefault();

												var result = AcctListByICResponse.itemList.Where(a => a.ExternalId == v.msisdnField && v.prinsupp_indField == "S").Select(a => a.AcctExtId).FirstOrDefault();

												if (result == Session["KenanACNumber"].ToString2())
												{
													Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
													break;
												}
												else
												{

													Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
													Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
													break;

												}
											}
										}
										else
										{
											Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
											Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
										}
									}
									else
									{
										Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
										Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
									}
								}
							}
							else if (primarySupplimentLines)
							{
								if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
								{
									if (((SelectPlanForSuppline)(Session[SessionKey.PrimarySupplimentaryLines.ToString()])).PrimarySupplimentaryMSISDNs.Count > 1)
									{
										if (Session["FirstSupplimentaryMSISDN"] != null)
										{
											if (ProcessingMsisdn != Session["FirstSupplimentaryMSISDN"].ToString2())
											{
												Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
											}
											else
											{
												Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
												Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
											}
										}
										else
										{
											Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
										}
									}
									else
									{
										Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
										Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
									}
								}
								else
								{
									Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
									Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
								}
							}
							else
							{
								if (Session["MNPSupplementaryLines"] != null)
								{
									if (((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"]).SupplimentaryMSISDNs.Count > 1)
									{
										if (Session["FirstSupplimentaryMSISDN"] != null)
										{
											if (ProcessingMsisdn != Session["FirstSupplimentaryMSISDN"].ToString2())
											{
												Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
											}
											else
											{
												Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
												Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
											}
										}
										else
										{
											Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
										}
									}
									else
									{
										Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
										Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
									}
								}
								else
								{
									Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
									Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
								}
							}
							//Condition added for SM30 by sumit end


						}
						//insurance Components
						if (respon.values[i].Plan_Type == Properties.Settings.Default.Device_Insurecomponent)
						{
							Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
							List<int> Finalintvalues = null;
							string modelid = ((OrderSummaryVM)(Session["RegMobileReg_SublineSummary"])).ModelID.ToString();

							if (Session["RegMobileReg_Type"].ToInt() == 1 || (Session["RegMobileReg_Type"].ToInt() == 8 && modelid != "0"))
							{
								List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();
								
								Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

							}
							else if (Session["RegMobileReg_Type"].ToInt() == 2 || (Session["RegMobileReg_Type"].ToInt() == 8 && modelid == "0"))
							{
								List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

								Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

							}
							var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

															 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

															 select new AvailableVAS
															 {
																 ComponentName = e.ComponentName,
																 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
																 Price = e.Price,
																 Value = e.Value,
																 GroupId = e.GroupId,
																 GroupName = e.GroupName,
																 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault,
                                                                 isHidden = e.isHidden
															 };
							Session["intinsuranceval"] = respon.values[i].BdlDataPkgId;
							dropObj.suppFlow[dropObj.suppIndex - 1].InsurancePackage = respon.values[i].BdlDataPkgId;

							//MNP MULTIPORT RELATED CHANGES
							if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
							{
								if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
								{
									((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intinsuranceval = respon.values[i].BdlDataPkgId;
								}
							}

							Lstvam2.InsuranceComponents.AddRange(finalfiltereddatacontracts);
						}

						#region device financing
						retrieveAcctListByICResponse allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
						var dfEligibilityCheck = Session[SessionKey.DF_Eligibility.ToString()].ToBool();
						var dfException = Session["df_showOffer"].ToString2() == "show" ? true : false;

						dropObj.suppFlow[dropObj.suppIndex - 1].isDeviceFinancingExceptionalOrder = dfException;

						if (!dfEligibilityCheck && !dfException)
						{
							if (respon.values[i].Plan_Type == Settings.Default.Device_Financing_Component)
							{
								Session["intDeviceFinancingVal"] = respon.values[i].BdlDataPkgId;
								dropObj.suppFlow[dropObj.suppIndex - 1].DeviceFinancingPackage = respon.values[i].BdlDataPkgId;
								string articleid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ArticleId.ToString2();
								Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

								//current contract
								var currentContractList = Lstvam2.ContractOffers;
								List<lnkofferuomarticleprice> newContractList = new List<lnkofferuomarticleprice>();
								if (Session["RegMobileReg_Type"].ToInt() == 1 && Lstvam.AvailableVASes.Any())
								{
									var deviceFinancingInfo = proxy.GetDeviceFinancingInfoByArticleID(articleid, Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));
									if (deviceFinancingInfo.Any())
									{
										foreach (var curCtr in currentContractList)
										{
											if (!curCtr.isDeviceFinancing)
												newContractList.Add(curCtr);
										}
										Lstvam2.ContractOffers = newContractList;
									}
								}
							}
						}
						
                        if (dfEligibilityCheck || dfException || Roles.IsUserInRole("MREG_CUSR"))
						{
							if (respon.values[i].Plan_Type == Settings.Default.Device_Financing_Component)
							{
                                var pbpcCriteria = new PgmBdlPckComponentFind()
                                {
                                    PgmBdlPckComponent = new PgmBdlPckComponent()
                                    {
                                        LinkType = "DF"
                                    },
                                    Active = true
                                };

                                var contractPBPCs = MasterDataCache.Instance.FilterComponents(pbpcCriteria);

                                List<int> contractDFDuration = new List<int>();

                                if (contractPBPCs.Any())
                                {
                                    using (var SmartCatProxy = new SmartCatelogServiceProxy())
                                    {
                                        foreach (var contractPBPC in contractPBPCs)
                                        {
                                            if (!string.IsNullOrEmpty(contractPBPC.KenanCode))
                                            {
                                                contractDFDuration.Add(SmartCatProxy.GetContractDurationByKenanCode(contractPBPC.KenanCode));
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    contractDFDuration.Clear();
                                }
								Session["intDevFinancingIds"] = respon.values[i].BdlDataPkgId;
								string articleid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ArticleId.ToString2();
								Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);

								if (Session["RegMobileReg_Type"].ToInt() == 1 && Lstvam.AvailableVASes.Any())
								{
									var deviceFinancingInfo = proxy.GetDeviceFinancingInfoByArticleID(articleid, Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));
									if (deviceFinancingInfo.Any())
									{
										var contractListDF = Lstvam.AvailableVASes.Where(x => !string.IsNullOrWhiteSpace(x.Value)).ToList();
										var upgradeFeeKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_UPGRADEFEE);
										var deviceFinancingVAS = Lstvam.AvailableVASes.Where(x => x.isHidden == false).ToList();

										if (deviceFinancingVAS.Any())
										{
											deviceFinancingVAS.FirstOrDefault(x => x.KenanCode == upgradeFeeKenanCode).Price = deviceFinancingInfo.FirstOrDefault().UpgradeFee;
											deviceFinancingVAS.FirstOrDefault(x => x.KenanCode == upgradeFeeKenanCode).oneTimeUpgrade = deviceFinancingInfo.FirstOrDefault().OneTimeUpgrade;
											Lstvam2.DeviceFinancingVAS.AddRange(deviceFinancingVAS);
											
											//var planID = proxy.PgmBdlPckComponentGet(new int[] { respon.values[i].BdlDataPkgId.ToInt() }).SingleOrDefault().KenanCode;
											string planID = primarySupplimentLines ? supplinePlanStorage.RegMobileReg_UOMCode : Session["UOMPlanID"].ToString2();
											foreach (var df in contractListDF)
											{
												var contractDF = WebHelper.Instance.GetContractOffersDF(new string[] { df.KenanCode }.ToList(), articleid, planID, string.Empty);

												foreach (var ctr in contractDF)
												{
													if (!Lstvam2.ContractOffers.Where(x => x.UOMCode == ctr.UOMCode && x.KenanPackageId == ctr.KenanPackageId).Any())
													{
														Lstvam2.ContractOffers.Add(ctr);	
													}
												}
											}

                                            if (((contractDFDuration.Any() && !string.IsNullOrEmpty(Session["selectedContractLength"].ToString2())) ? contractDFDuration.Contains(Session["selectedContractLength"].ToInt()) : false) && ReferenceEquals(Session["RegMobileReg_UOMCode"], null))
                                                Session["OfferId"] = Lstvam2.ContractOffers[Lstvam2.ContractOffers.Count() - 1].Id;

                                            //if (Session["selectedContractLength"].ToInt() == 24 && ReferenceEquals(Session["RegMobileReg_UOMCode"], null))
                                            //	Session["OfferId"] = Lstvam2.ContractOffers[Lstvam2.ContractOffers.Count() - 1].Id;

											//if (Lstvam2.ContractOffers.Any())
											//    Session["OfferId"] = Lstvam2.ContractOffers[Lstvam2.ContractOffers.Count() - 1].Id;

											var dfContract = WebHelper.Instance.GetAvailablePackageComponents(respon.values[i].BdlDataPkgId.ToInt(), ref ProcessingMsisdn, isMandatory: false, isDF: true);

											Lstvam2.ContractVASes.AddRange(dfContract.ContractVASes);
										}
									}
								}
							}
						}
						#endregion
					
                    }

                    //GTM e-Billing CR - Ricky - 2014.09.25
                    //To retrieve the component relation
                    Lstvam2.MutualExclusiveComponents = proxy.ComponentRelationGetMutualExclusive(Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));

				}

				dropObj.suppFlow[dropObj.suppIndex - 1].MandatoryVAS = (String)Session["MandatoryVasIds"];
				
				Session[SessionKey.DropFourObj.ToString()] = dropObj;

				if (_mobileRegType == "Planonly")
				{
					Lstvam.ContractVASes.Clear();
                    Session["selectedOfferID"] = null;
                    Session["OfferId"] = null;
                    Session["RegMobileReg_UOMCode"] = null;
				}

				Session["supplineVas"] = Lstvam2;

                if (Session["selectedOfferID"].ToString2() != null && Session["selectedOfferID"].ToString2() != "")
                {
                    var check1 = (!Session["OfferId"].ToString2().IsEmpty()) ? Session["OfferId"].ToString2() : Session["selectedOfferID"].ToString2();
                    foreach (var item in Lstvam2.ContractOffers)
                    {
                        if (check1.Equals(item.Id.ToString()))
                        {
                            dropObj.suppFlow[dropObj.suppIndex - 1].UOMCode = item.UOMCode;
							dropObj.suppFlow[dropObj.suppIndex - 1].OfferDevicePrice = Convert.ToDouble(item.Price);//11032015 - Anthony - Fix for Device Offer Price is not sync with the offer
                            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = Convert.ToDouble(item.Price);//02092015 - Anthony - Fix for inconsistent device price
                            Session[SessionKey.DropFourObj.ToString()] = dropObj;
                            Session["RegMobileReg_UOMCode"] = item.UOMCode;
                            Lstvam2.uomCode = item.UOMCode;
                        }
                    }
                }


                var sessionUomCode = Session["RegMobileReg_UOMCode"].ToString2();
                //24022015 - Anthony - fix for inconsistent price in order summary - Start
                Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(true);

                for (var i = 0; i < dropObj.suppIndex; i++)
                {
                    if (i == dropObj.suppIndex - 1)
                    {
                        if (!Session[SessionKey.FromMNP.ToString()].ToBool() && dropObj.suppFlow[i].orderSummary.ModelID > 0)
                        {
                            dropObj.suppFlow[i].orderSummary.MalayPlanAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalayPlanAdv;
                            dropObj.suppFlow[i].orderSummary.MalyPlanDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalyPlanDeposit;
                            dropObj.suppFlow[i].orderSummary.MalyDevAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalyDevAdv;
                            dropObj.suppFlow[i].orderSummary.MalayDevDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalayDevDeposit;
                            dropObj.suppFlow[i].orderSummary.OthPlanAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthPlanAdv;
                            dropObj.suppFlow[i].orderSummary.OthPlanDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthPlanDeposit;
                            dropObj.suppFlow[i].orderSummary.OthDevAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthDevAdv;
                            dropObj.suppFlow[i].orderSummary.OthDevDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthDevDeposit;
                        }
                    }
                }

                //foreach (var suppFlows in dropObj.suppFlow)
                //{
                //    if (!Session[SessionKey.FromMNP.ToString()].ToBool() && suppFlows.orderSummary.ModelID > 0)
                //    {
                //        suppFlows.orderSummary.MalayPlanAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalayPlanAdv;
                //        suppFlows.orderSummary.MalyPlanDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalyPlanDeposit;
                //        suppFlows.orderSummary.MalyDevAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalyDevAdv;
                //        suppFlows.orderSummary.MalayDevDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).MalayDevDeposit;
                //        suppFlows.orderSummary.OthPlanAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthPlanAdv;
                //        suppFlows.orderSummary.OthPlanDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthPlanDeposit;
                //        suppFlows.orderSummary.OthDevAdv = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthDevAdv;
                //        suppFlows.orderSummary.OthDevDeposit = ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"]).OthDevDeposit;
                //    }
                //}

				if (dropObj.suppFlow[dropObj.suppIndex - 1].phone != null && !string.IsNullOrWhiteSpace(dropObj.suppFlow[dropObj.suppIndex - 1].phone.DeviceArticleId))
				{
					using (var catProxy = new CatalogServiceProxy())
					{
						var anyDFDeviceEligible = catProxy.GetDeviceFinancingInfoByArticleID(dropObj.suppFlow[dropObj.suppIndex - 1].phone.DeviceArticleId, dropObj.suppFlow[dropObj.suppIndex - 1].PkgPgmBdlPkgCompID);
						Lstvam2.exceptionalAvailable = anyDFDeviceEligible != null ? anyDFDeviceEligible.Any() : false;
					}
				}

                Session["RegMobileReg_OrderSummary"] = Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary();
                Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(fromSubline: true);

                //24022015 - Anthony - fix for inconsistent price in order summary - End
				if (Lstvam2 != null)
				{
					return View(Lstvam2);
				}
				else
				{
					Lstvam2 = (ValueAddedServicesVM)Session["supplineVas"];
					return View(Lstvam2);
				}
			}
			else
			{
				Session["RedirectURL"] = "SuppLine/SuppLineVASNew";
				return RedirectToAction("IndexNew", "Home");
			}
		}



		[Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DN")]
		public ActionResult SuppLineVAS()
		{
			Session["MandatoryVasIds"] = null;

			ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
			ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
			MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;
			///INSTANCE WILL CONTAIN LIST OF SUPPLEMENTARY LINES HAVING DIFFERENT STATES SUCH AS 
			///MNPSupplimentaryDetailsAddState.NOTINITIALIZED
			///MNPSupplimentaryDetailsAddState.PROCESSING
			///MNPSupplimentaryDetailsAddState.PROCESSED
			///ONLY ONE MSISDN CAN BE IN PROCESSING STATE AT ANY POINT OF TIME
			List<MOCOfferDetails> offers = new List<MOCOfferDetails>();
			MNPSelectPlanForSuppline SuppleMentaryLine = null;
			MNPSupplinePlanStorage MNPSupplinePlanStorage = null;

			//List of Suppline attached to primary line changes
			SelectPlanForSuppline selectPlanForSuppline = null;
			List<SupplinePlanStorage> supplinePlanStorageList;
			SupplinePlanStorage supplinePlanStorage = null;
			Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]); 
		  


			if (IsMNPWithMultpleLines)
			{
				///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
				MNPPrimaryPlanStorage = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];

				if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
				{
					///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
					SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()];

					///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
					if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
					{
						ProcessingMsisdn = SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
						///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
						MNPSupplinePlanStorage = (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
					}
				}
			}
			else if(primarySupplimentLines)
			{
				  ///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
				supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : null;


				if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
				{
					///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
					selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString()];

					///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
					if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
					{
						ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
						///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
						supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
					}
				}
			}
			else
			{
				Session["RegMobileReg_OrderSummary"] = null;
				Session["RegMobileReg_SublineSummary"] = null;
				Session["RegMobileReg_VasNames"] = null;
				Session["RegMobileReg_VasIDs"] = null;
				Session["RegMobileReg_SublineVM"] = null;
			}

			// Gerry - Drop5
			offers = WebHelper.Instance.GetMOCOfferByArticleID(Session[SessionKey.ArticleId.ToString()].ToString2());
			
			if (offers != null)
				Lstvam2.Offers = offers;
			if (Session["PPID"].ToString2() != string.Empty)
			{
				if (!ReferenceEquals(Session["RedirectURL"], null))
				{
					Session.Contents.Remove("RedirectURL");
				}
				SetRegStepNo(MobileRegistrationSteps.Vas);

				///MNP MULTIPORT RELATED CHANGES
				if (IsMNPWithMultpleLines)
				{
					if (ReferenceEquals(MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID, null) || string.IsNullOrWhiteSpace(MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID.ToString()))
					{
						MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID = MNPSupplinePlanStorage.GuidedSales_PkgPgmBdlPkgCompID;
					}

					MNPSupplinePlanStorage.RegMobileReg_OrderSummary = ConstructOrderSummary(fromSubline: true);
				}
				else if (primarySupplimentLines)
				{
					if (ReferenceEquals(supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID, null) || string.IsNullOrWhiteSpace(supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID.ToString()))
					{
						supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID = supplinePlanStorage.GuidedSales_PkgPgmBdlPkgCompID;
					}

					supplinePlanStorage.RegMobileReg_OrderSummary = ConstructOrderSummary(fromSubline: true);
				}
				else
				{
					if (Session["RegMobileReg_PkgPgmBdlPkgCompID"] == null || Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2() == string.Empty)
					{
						Session["RegMobileReg_PkgPgmBdlPkgCompID"] = Session["GuidedSales_PkgPgmBdlPkgCompID"];
					}

					Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(fromSubline: true);
				}

				///MNP MULTIPORT RELATED CHANGES
				OrderSummaryVM orderVM = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_OrderSummary : (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];
				Int32 Selecpgmid;

				///MNP MULTIPORT RELATED CHANGES
				if (IsMNPWithMultpleLines)
				{
					Selecpgmid = (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.SelectedPgmBdlPkgCompID != null) ? orderVM.SelectedPgmBdlPkgCompID : MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID;
				}
				else if(primarySupplimentLines)
				{
					Selecpgmid = (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.SelectedPgmBdlPkgCompID != null) ? orderVM.SelectedPgmBdlPkgCompID : supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID;
				}
				else
				{
					Selecpgmid = (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.SelectedPgmBdlPkgCompID != null) ? orderVM.SelectedPgmBdlPkgCompID : Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt();
				}


				//Lstvam = WebHelper.Instance.GetAvailablePackageComponents_old(Selecpgmid, ref ProcessingMsisdn, isMandatory: false, fromSuppline: true);
				Lstvam = WebHelper.Instance.GetAvailablePackageComponents(Selecpgmid, ref ProcessingMsisdn, isMandatory: false, fromSuppline: true);



				// for Device+Plans, get the offers to be listed in VAS components
				string _mobileRegType = Session["MobileRegType"].ToString2();
				string _regMobileReg_Type = Session["RegMobileReg_Type"].ToString2();
				if (_regMobileReg_Type.ToNullableInt() == (int)MobileRegType.DevicePlan)
				{
					string offernames = string.Empty;
					string planid = primarySupplimentLines ? supplinePlanStorage.RegMobileReg_UOMCode : Session["UOMPlanID"].ToString2();
					string articleid = Session[SessionKey.ArticleId.ToString()].ToString2();
					string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString2() : string.Empty;
					// drop 5
					Lstvam.ContractOffers = WebHelper.Instance.GetContractOffers(Lstvam, articleid, planid, MOCStatus);
				}


				var selectedCompIDs = new List<int>();
				List<AvailableVAS> lstvas = new List<AvailableVAS>();


				lstvas.AddRange(orderVM.VasVM.MandatoryVASes);

				for (int i = 0; i < orderVM.Sublines.Count(); i++)
				{
					for (int j = 0; j < orderVM.Sublines[i].SelectedVasNames.Count; j++)
					{
						lstvas.Add(new AvailableVAS() { PgmBdlPkgCompID = orderVM.Sublines[i].SelectedVasIDs[j], ComponentName = orderVM.Sublines[i].SelectedVasNames[j].ToString2() });
					}

				}


				for (int i = 0; i < lstvas.Count(); i++)
				{
					selectedCompIDs.Add(lstvas[i].PgmBdlPkgCompID);
				}



				Lstvam2.ContractVASes.AddRange(Lstvam.ContractVASes);

				Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
				Lstvam2.ContractOffers.AddRange(Lstvam.ContractOffers);

				///MNP MULTIPORT RELATED CHANGES
				if (IsMNPWithMultpleLines)
				{
					MNPSupplinePlanStorage.RegMobileReg_VasNames = orderVM.VasVM.MandatoryVASes;
				}
				else if(primarySupplimentLines)
				{
					supplinePlanStorage.RegMobileReg_VasNames = orderVM.VasVM.MandatoryVASes;
				}
				else
				{
					Session["RegMobileReg_VasNames"] = orderVM.VasVM.MandatoryVASes;
				}
				/* For Mandatory packages */
				///MNP MULTIPORT RELATED CHANGES
				if (IsMNPWithMultpleLines)
				{
					MNPSupplinePlanStorage.vasmandatoryids = string.Empty;
				}
				else if (primarySupplimentLines)
				{
					supplinePlanStorage.vasmandatoryids = string.Empty;
				}
				else
				{
					Session["vasmandatoryids"] = string.Empty;
				}

				using (var proxy = new CatalogServiceProxy())
				{

					BundlepackageResp respon = new BundlepackageResp();

					///MNP MULTIPORT RELATED CHANGES
					respon = proxy.GetMandatoryVas(IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID : Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));

					for (int i = 0; i < respon.values.Count(); i++)
					{

						Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false, fromSuppLine: true);
						if (respon.values[i].Plan_Type == Properties.Settings.Default.Master_Component)
						{

							///MNP MULTIPORT RELATED CHANGES
							if (IsMNPWithMultpleLines)
							{
								MNPSupplinePlanStorage.intmandatoryidsval = respon.values[i].BdlDataPkgId;
							}
							else if(primarySupplimentLines)
							{
								supplinePlanStorage.intmandatoryidsval = respon.values[i].BdlDataPkgId;
							}
							else
							{
								Session["intmandatoryidsval"] = respon.values[i].BdlDataPkgId;
							}

							Lstvam2.AvailableVASes.AddRange(Lstvam.AvailableVASes);
						}

						if (respon.values[i].Plan_Type == Properties.Settings.Default.CompType_DataCon)
						{

							List<int> Finalintvalues = null;

							///MNP MULTIPORT RELATED CHANGES
							//if (IsMNPWithMultpleLines)
							//{
							//    if (MNPSupplinePlanStorage.RegMobileReg_Type == 1)
							//    {
							//        List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

							//        string modelid = ((OrderSummaryVM)(MNPSupplinePlanStorage.RegMobileReg_OrderSummary)).ModelID.ToString();

							//        Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

							//    }
							//}
							//else
							//{
							//    if (Session["RegMobileReg_Type"].ToInt() == 1)
							//    {
							//        List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

							//        string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

							//        Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

							//    }
							//}

							List<PkgDataPlanId> valuesplan = proxy.getFilterplanvalues(2).values.ToList();

							Finalintvalues = valuesplan.Select(a => a.BdlDataPkgId).ToList();


							var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

															 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

															 select new AvailableVAS()
															 {
																 ComponentName = e.ComponentName,
																 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
																 Price = e.Price,
																 Value = e.Value,
																 GroupId = e.GroupId,
																 GroupName = e.GroupName,
																 KenanCode = e.KenanCode
															 };

							///MNP MULTIPORT RELATED CHANGES
							if (IsMNPWithMultpleLines)
							{
								MNPSupplinePlanStorage.intdataidsval = respon.values[i].BdlDataPkgId;
							}
							else if(primarySupplimentLines)
							{
								supplinePlanStorage.intdataidsval = respon.values[i].BdlDataPkgId;
							}
							else
							{
								Session["intdataidsval"] = respon.values[i].BdlDataPkgId;
							}
							finalfiltereddatacontracts = finalfiltereddatacontracts.Where(v => v.GroupId != null);
							Lstvam2.DataVASes.AddRange(finalfiltereddatacontracts);
						}
						/*Extra packages*/
						if (respon.values[i].Plan_Type == Properties.Settings.Default.Extra_Component)
						{


							List<int> Finalintvalues = null;
							///MNP MULTIPORT RELATED CHANGES
							//if (IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_Type == 1 : Session["RegMobileReg_Type"].ToInt() == 1)
							//{
							//    List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();
							//    ///MNP MULTIPORT RELATED CHANGES
							//    string modelid = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_OrderSummary.ModelID.ToString() : ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

							//    Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

							//}

							List<PkgDataPlanId> valuesplan = proxy.getFilterplanvalues(2).values.ToList();

							Finalintvalues = valuesplan.Select(a => a.BdlDataPkgId).ToList();



							var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

															 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

															 select new AvailableVAS()
															 {
																 ComponentName = e.ComponentName,
																 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
																 Price = e.Price,
																 Value = e.Value,
																 GroupId = e.GroupId,
																 GroupName = e.GroupName,
																 KenanCode = e.KenanCode

															 };

							///MNP MULTIPORT RELATED CHANGES
							if (IsMNPWithMultpleLines)
							{
								MNPSupplinePlanStorage.intextraidsval = respon.values[i].BdlDataPkgId;
							}
							else if(primarySupplimentLines)
							{
								supplinePlanStorage.intextraidsval = respon.values[i].BdlDataPkgId;
							}
							else
							{
								Session["intextraidsval"] = respon.values[i].BdlDataPkgId;
							}

							Lstvam2.ExtraComponents.AddRange(finalfiltereddatacontracts);


						}
						//Pramotional Packages
						if (respon.values[i].Plan_Type == Properties.Settings.Default.Pramotional_Component)
						{
							Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
							#region COmmented filterComponents for Pramotional components
							//List<int> Finalintvalues = null;
							//if (Session["RegMobileReg_Type"].ToInt() == 1)
							//{
							//    List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();
							//    string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();
							//    Finalintvalues = values.Select(a => a.BdlDataPkgId).Distinct().ToList();
							//    Lstvam.DeviceType = "Seco_NoDevice";
							//}
							//else if (Session["RegMobileReg_Type"].ToInt() == 2)
							//{
							//    List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();
							//    Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();
							//}
							//var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

							//                                 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

							//                                 select new AvailableVAS
							//                                 {
							//                                     ComponentName = e.ComponentName,
							//                                     PgmBdlPkgCompID = e.PgmBdlPkgCompID,
							//                                     Price = e.Price,
							//                                     Value = e.Value,
							//                                     GroupId = e.GroupId,
							//                                     GroupName = e.GroupName,
							//                                     KenanCode = e.KenanCode
							//                                 }; 
							#endregion
							Session["intpramotionidsval"] = respon.values[i].BdlDataPkgId;
							//MNP MULTIPORT RELATED CHANGES
							if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
							{
								if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
								{
									((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intpramotionidsval = respon.values[i].BdlDataPkgId;
								}
							}

							//Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
							//Condition added for SM30 by sumit start
							if (Session["IsNewBillingAddress"].ToString2() != "IsNewBillingAddress" && Session["IsNewBillingAddress"].ToString2() != "")
							{
								using (var PrinSupproxy = new Online.Registration.Web.Helper.PrinSupServiceProxy())
								{
									var req = new SubscribergetPrinSuppRequest();
									retrievegetPrinSuppResponse response = null;
									req.eaiHeader = null;
									if (Session["ExternalID"] != null)
									{
										req.msisdn = Session["ExternalID"].ToString2();
									}//Need to add condition for mnp / sumit
									DateTime dtReq = DateTime.Now;
									//response = PrinSupproxy.getPrinSupplimentarylines(new SubscribergetPrinSuppRequest
									//{
									// eaiHeader = header,
									// msisdn = v.ExternalId
									//});
									response = PrinSupproxy.getPrinSupplimentarylines(req);
									if (response != null)
									{
										if (response.itemList.Count > 0)
										{
											foreach (var v in response.itemList)
											{
												retrieveAcctListByICResponse AcctListByICResponse = null;
												if (!ReferenceEquals(Session[SessionKey.PPIDInfo.ToString()], null))
												{
													AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.PPIDInfo.ToString()];
												}
												//var result = AcctListByICResponse.itemList.Where(a => a.AcctIntId == v.acct_noField.ToString() && a.ExternalId == req.msisdn).Select(a => a.AcctExtId).FirstOrDefault();

												var result = AcctListByICResponse.itemList.Where(a => a.ExternalId == v.msisdnField && v.prinsupp_indField == "S").Select(a => a.AcctExtId).FirstOrDefault();

												if (result == Session["KenanACNumber"].ToString2())
												{
													Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
													break;
												}
												else
												{

													Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
													Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
													break;

												}
											}
										}
										else
										{
											Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
											Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
										}
									}
									else
									{
										Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
										Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
									}
								}
							}
							else if(primarySupplimentLines)
							{
								if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
								{
									if (((SelectPlanForSuppline)(Session[SessionKey.PrimarySupplimentaryLines.ToString()])).PrimarySupplimentaryMSISDNs.Count > 1)
									{
										if (Session["FirstSupplimentaryMSISDN"] != null)
										{
											if (ProcessingMsisdn != Session["FirstSupplimentaryMSISDN"].ToString2())
											{
												Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
											}
											else
											{
												Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
												Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
											}
										}
										else
										{
											Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
										}
									}
									else
									{
										Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
										Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
									}
								}
								else
								{
									Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
									Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
								}
							}
							else
							{
								if (Session["MNPSupplementaryLines"] != null)
								{
									if (((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"]).SupplimentaryMSISDNs.Count > 1)
									{
										if (Session["FirstSupplimentaryMSISDN"] != null)
										{
											if (ProcessingMsisdn != Session["FirstSupplimentaryMSISDN"].ToString2())
											{
												Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
											}
											else
											{
												Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
												Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
											}
										}
										else
										{
											Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
										}
									}
									else
									{
										Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
										Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
									}
								}
								else
								{
									Lstvam.AvailableVASes = (from x in Lstvam.AvailableVASes where x.KenanCode != ConfigurationManager.AppSettings["ExclueSM30KenanId"].ToString2() select x).ToList();
									Lstvam2.PramotionalComponents.AddRange(Lstvam.AvailableVASes);
								}
							}
							//Condition added for SM30 by sumit end


						}
						//insurance Components
						if (respon.values[i].Plan_Type == Properties.Settings.Default.Device_Insurecomponent)
						{
							Lstvam = WebHelper.Instance.GetAvailablePackageComponents_PC_MC_DC(Convert.ToInt32(respon.values[i].BdlDataPkgId), respon.values[i].Plan_Type, isMandatory: false);
							List<int> Finalintvalues = null;
							if (Session["RegMobileReg_Type"].ToInt() == 1 || Session["RegMobileReg_Type"].ToInt() == 8)
							{
								List<PkgDataPlanId> values = proxy.getFilterplanvalues(1).values.ToList();

								string modelid = ((OrderSummaryVM)(Session["RegMobileReg_OrderSummary"])).ModelID.ToString();

								Finalintvalues = values.Where(mid => mid.Modelid == modelid).Select(a => a.BdlDataPkgId).ToList();

							}
							else if (Session["RegMobileReg_Type"].ToInt() == 2)
							{
								List<PkgDataPlanId> values = proxy.getFilterplanvalues(2).values.ToList();

								Finalintvalues = values.Select(a => a.BdlDataPkgId).ToList();

							}
							var finalfiltereddatacontracts = from e in Lstvam.AvailableVASes

															 join m in Finalintvalues on Convert.ToInt32(e.PgmBdlPkgCompID) equals Convert.ToInt32(m)

															 select new AvailableVAS
															 {
																 ComponentName = e.ComponentName,
																 PgmBdlPkgCompID = e.PgmBdlPkgCompID,
																 Price = e.Price,
																 Value = e.Value,
																 GroupId = e.GroupId,
																 GroupName = e.GroupName,
																 KenanCode = e.KenanCode,
                                                                 isDefault = e.isDefault,
                                                                 isHidden = e.isHidden
															 };
							Session["intinsuranceval"] = respon.values[i].BdlDataPkgId;
							//MNP MULTIPORT RELATED CHANGES
							if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
							{
								if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
								{
									((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).intinsuranceval = respon.values[i].BdlDataPkgId;
								}
							}

							Lstvam2.InsuranceComponents.AddRange(finalfiltereddatacontracts);
						}

					}




				}


				if (_mobileRegType == "Planonly")
				{

					Lstvam.ContractVASes.Clear();

				}

				Session["supplineVas"] = Lstvam2;
				if (Lstvam2 != null)
				{
					return View(Lstvam2);
				}
				else
				{
					Lstvam2 = (ValueAddedServicesVM)Session["supplineVas"];
					return View(Lstvam2);
				}
			}
			else
			{
				Session["RedirectURL"] = "SuppLine/SuppLineVAS";
				return RedirectToAction("Index", "Home");
			}
		}

		[Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DN")]
		[HttpPost]
		public ActionResult SuppLineVAS(FormCollection collection, ValueAddedServicesVM vasVM)
		{
			//added by ravi on oct 21 2013 as it is giving exception
			if (vasVM.SelectedVasIDs != null && vasVM.SelectedVasIDs != string.Empty)
			{
				vasVM.SelectedVasIDs = vasVM.SelectedVasIDs.Replace("[object XMLDocument]", string.Empty);
			}
			//added by ravi on oct 21 2013 as it is giving exception ends here.
			// for bite CR
			List<int> depCompIds1 = new List<int>();
			List<int> tempdepCompIds = new List<int>();
			using (var proxy = new CatalogServiceProxy())
			{

				List<int> lstContracts = WebHelper.Instance.CommaSplitToList(vasVM.SelectedContractID.ToString2());
				Session[SessionKey.RegMobileReg_DataplanID.ToString()] = proxy.GettllnkContractDataPlan(lstContracts);
			}
			//Changed by ravikiran on Feb 13 2014 for vas dependency Check components highlighting
			MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;
			MNPSelectPlanForSuppline SuppleMentaryLine = null;
			MNPSupplinePlanStorage MNPSupplinePlanStorage = null;

			//List of Suppline attached to primary line changes
			SelectPlanForSuppline selectPlanForSuppline = null;
			List<SupplinePlanStorage> supplinePlanStorageList;
			SupplinePlanStorage supplinePlanStorage = null;
			Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]); 

			if (IsMNPWithMultpleLines)
			{
				///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
				MNPPrimaryPlanStorage = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];
				if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
				{
					///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
					SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()];
					///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
					if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
					{
						ProcessingMsisdn = SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
						///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
						MNPSupplinePlanStorage = (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
					}
				}
			}
			//Changed by ravikiran on Feb 13 2014 for vas dependency Check components highlighting ends here

			if (primarySupplimentLines)
			{
				supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : null;

				if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
				{
					///GET ALL SUPPLEMENTARY LINES STORED IN SESSION 
					selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString()];
					///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
					if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
					{
						ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
						///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
						supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
					}
				}
			}


			#region fxDependency Check
			if (vasVM.TabNumber == 4 || vasVM.TabNumber == 5)
			{
				//Added by chetan on request from Pavan
				Session["RegMobileReg_MainDevicePrice"] = vasVM.price;
				Session["RegMobileReg_OfferDevicePrice"] = vasVM.price;
				//Up to here
				List<FxDependChkComps> Comps = new List<FxDependChkComps>();
				using (var Proxy = new RegistrationServiceProxy())
				{
					string strCompIds = string.Empty;
					if (vasVM.SelectedVasIDs.ToString2() != string.Empty)
					{
						strCompIds = vasVM.SelectedVasIDs;
						//Changed by ravikiran on Feb 13 2014 for vas dependency Check components highlighting
						if (IsMNPWithMultpleLines == true)
						{
							MNPSupplinePlanStorage.SelectedComponents = vasVM.SelectedVasIDs;
							if (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) && vasVM.SelectedVasIDs.Split(',').Any())
							{
								List<int> selectedids = new List<int>();
								foreach (string item in vasVM.SelectedVasIDs.Split(','))
								{
									if (item != string.Empty)
									{
										selectedids.Add(item.ToInt());
									}
								}
								//List<int> selectedids = vasVM.SelectedVasIDs.Split(',').Select(int.Parse).ToList();
								if (selectedids.Any())
								{
									List<int> depids = WebHelper.Instance.GetDepenedencyComponents(selectedids);
									if (depids != null && depids.Any())
									{
										foreach (var item in depids)
										{
											MNPSupplinePlanStorage.SelectedComponents += "," + item;
										}
									}
								}
							}
						}
						else if(primarySupplimentLines)
						{
							supplinePlanStorage.SelectedComponents = vasVM.SelectedVasIDs;
							if (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) && vasVM.SelectedVasIDs.Split(',').Any())
							{
								List<int> selectedids = new List<int>();
								foreach (string item in vasVM.SelectedVasIDs.Split(','))
								{
									if (item != string.Empty)
									{
										selectedids.Add(item.ToInt());
									}
								}
								if (selectedids.Any())
								{
									List<int> depids = WebHelper.Instance.GetDepenedencyComponents(selectedids);
									if (depids != null && depids.Any())
									{
										foreach (var item in depids)
										{
											supplinePlanStorage.SelectedComponents += "," + item;
										}
									}
								}
							}
						}
						else
						{
							Session[SessionKey.SelectedComponents.ToString()] = vasVM.SelectedVasIDs;
						}
						//Changed by ravikiran on Feb 13 2014 for vas dependency Check components highlighting ends here

					}
					if (Session["MandatoryVasIds"].ToString2().Length > 0)
					{
						strCompIds = strCompIds + "," + Session["MandatoryVasIds"].ToString2();
					}
					strCompIds += "," + Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
					if (vasVM.OfferId != null)
					{
						Session["OfferId"] = vasVM.OfferId.ToString();
					}
					//for adding contract id
					if (vasVM.SelectedContractID.ToString2().Length > 0)
					{
						strCompIds += "," + vasVM.SelectedContractID;
					}

					if (Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToString2() != string.Empty)
					{
						try
						{
							int[] dataplanid = (int[])Session[SessionKey.RegMobileReg_DataplanID.ToString()];
							foreach (int pid in dataplanid)
							{
								depCompIds1.Add(pid.ToInt());
								tempdepCompIds = GetDepenedencyComponents(pid.ToString2());
								foreach (var item in tempdepCompIds)
								{
									depCompIds1.Add(item);
								}
							}

						}
						catch (Exception ex)
						{
							depCompIds1.Add(Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt());
							WebHelper.Instance.LogExceptions(this.GetType(), ex);
						}
						if (depCompIds1 != null && depCompIds1.Count > 0)
						{
							depCompIds1 = depCompIds1.Distinct().ToList();
							foreach (var item in depCompIds1)
							{
								strCompIds += "," + item.ToString2();
							}
						}
					}

					// added by Nreddy by pass the dependece check method when click on back button
					if (collection["TabNumber"] != "2")
					{
						Comps = Proxy.GetAllComponentsbyPlanId(Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(), strCompIds, "BP");
					}
				}
				List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();
				foreach (var comp in Comps)
				{
					Online.Registration.Web.SubscriberICService.componentList component = new componentList();
					component.componentIdField = comp.ComponentKenanCode;
					component.packageIdField = comp.KenanCode;
					componentList.Add(component);
					if (comp.DependentKenanCode != -1)
					{
						component = new componentList();
						component.componentIdField = comp.DependentKenanCode.ToString();
						component.packageIdField = comp.KenanCode;
						if (IsMNPWithMultpleLines || primarySupplimentLines)
						{
							using (var prox = new CatalogServiceProxy())
							{
								var comps = prox.PgmBdlPckComponentGetForSmartByKenanCode(new string[] { comp.DependentKenanCode.ToString() }.ToList()).ToList();
								if (comps != null & comps.Count > 0)
								{
									foreach (var item in comps.OrderBy(a => a.ID))
									{
										if (item.KenanCode == comp.DependentKenanCode.ToString())
										{
											if(primarySupplimentLines)
											{
												if (ReferenceEquals(supplinePlanStorage.SelectedComponents, null))
													supplinePlanStorage.SelectedComponents = string.Empty;
												if (!supplinePlanStorage.SelectedComponents.Contains(item.ID.ToString()))
													supplinePlanStorage.SelectedComponents += "," + item.ID.ToString();
												break;
											}
											else{
											if (ReferenceEquals(MNPSupplinePlanStorage.SelectedComponents, null))
												MNPSupplinePlanStorage.SelectedComponents = string.Empty;
											if (!MNPSupplinePlanStorage.SelectedComponents.Contains(item.ID.ToString()))
												MNPSupplinePlanStorage.SelectedComponents += "," + item.ID.ToString();
											break;
											}
										}
									}

								}
								componentList.Add(component);
							}
						}
						else
						{
							componentList.Add(component);
						}
					}
				}
				if (componentList.Count > 0)
				{
					// Drop 5
					retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
					string pbpcID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
					typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, pbpcID: pbpcID);
					
					var FxReqXML = Util.ConvertObjectToXml(request);
					typeChkFxVasDependencyResponse response = _retrieveServiceInfoProxy.ChkFxVasDependency(request);
					ValueAddedServicesVM vas = (ValueAddedServicesVM)Session["supplineVas"];
					if (response.mxsRuleMessagesField != null && response.mxsRuleMessagesField.Count > 0)
					{
						foreach (string errmsg in response.mxsRuleMessagesField)
						{
							ModelState.AddModelError(string.Empty, errmsg);
						}
						return View(vas);
					}
					else if (response.msgCodeField == "3")
					{
						ModelState.AddModelError(string.Empty, "OPF system error - Unable to fetch data.");   // Need to change msg
						return View(vas);
					}
					else if (response.msgCodeField != "0")
					{
						ModelState.AddModelError(string.Empty, "Selected components are not available.");   // Need to change msg
						return View(vas);
					}
				}
			}
			#endregion
			Session["VasGroupIds"] = vasVM.hdnGroupIds;
			if (IsMNPWithMultpleLines)
			{
				if (vasVM.ExtraTenMobileNumbers.ToString2().Length > 0)
				{
					MNPSupplinePlanStorage.ExtratenMobilenumbers = vasVM.ExtraTenMobileNumbers;
				}
				MNPSupplinePlanStorage.RegMobileReg_TabIndex = vasVM.TabNumber;
			}
			else if(primarySupplimentLines)
			{
				if (vasVM.ExtraTenMobileNumbers.ToString2().Length > 0)
				{
					supplinePlanStorage.ExtratenMobilenumbers = vasVM.ExtraTenMobileNumbers;
				}
				supplinePlanStorage.RegMobileReg_TabIndex = vasVM.TabNumber;
			}
			else
			{
				Session["ExtratenMobilenumbers"] = vasVM.ExtraTenMobileNumbers;
			}
			List<int> planIds = new List<int>();
			PgmBdlPckComponent package = new PgmBdlPckComponent();
			Session["RegMobileReg_TabIndex"] = vasVM.TabNumber;
			Session["RegMobileReg_MainDevicePrice"] = vasVM.price;
			Session["RegMobileReg_OfferDevicePrice"] = vasVM.price;
			Session[SessionKey.RegMobileReg_UOMCode.ToString()] = vasVM.uomCode;
			Session["RegMobileReg_TabIndex"] = vasVM.TabNumber;
			using (var proxy = new CatalogServiceProxy())
			{
				package = MasterDataCache.Instance.FilterComponents(planIds).FirstOrDefault();
				var respOffers = proxy.GETMOCOfferDetails(vasVM.OfferId);
				MOCOfferDetails mocOffer = new MOCOfferDetails();
				if (respOffers != null)
				{
					mocOffer.DiscountAmount = respOffers.DiscountAmount;
					mocOffer.ID = respOffers.ID;
					mocOffer.Name = respOffers.Name;
					mocOffer.OfferDescription = respOffers.OfferDescription;
					Session["Discount"] = mocOffer.DiscountAmount;

					///MNP MULTIPORT RELATED CHANGES
					if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
					{
						if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
						{
							((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).Discount = mocOffer.DiscountAmount;
						}
					}
				}
			}
			//modefied by chetan
			var selectedVasIDs = string.Empty;
			if (vasVM.SelectedVasIDs != null)
			{
				if (vasVM.SelectedVasIDs != null && !Regex.IsMatch(vasVM.SelectedVasIDs, @"^,+$") && vasVM.SelectedVasIDs != "[object XMLDocument]")
				{
					selectedVasIDs = vasVM.SelectedVasIDs;
					///MNP MULTIPORT RELATED CHANGES
					if (IsMNPWithMultpleLines)
					{
						MNPSupplinePlanStorage.RegMobileReg_DataPkgID = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
					}
					else if(primarySupplimentLines)
					{
						supplinePlanStorage.RegMobileReg_DataPkgID = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
					}
					else
					{
						selectedVasIDs = vasVM.SelectedVasIDs;
						Session["RegMobileReg_DataPkgID"] = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
					}
				}
				else
				{
					///MNP MULTIPORT RELATED CHANGES
					if (IsMNPWithMultpleLines)
					{
						MNPSupplinePlanStorage.RegMobileReg_DataPkgID = null;
						MNPSupplinePlanStorage.RegMobileReg_VasIDs = null;
					}
					else if(primarySupplimentLines)
					{
						supplinePlanStorage.RegMobileReg_DataPkgID = null;
						supplinePlanStorage.RegMobileReg_VasIDs = null;
					}
					else
					{
						if (!ReferenceEquals(Session["RegMobileReg_DataPkgID"], null))
						{
							Session.Contents.Remove("RegMobileReg_DataPkgID");
						}
						if (!ReferenceEquals(Session["RegMobileReg_VasIDs"], null))
						{
							Session.Contents.Remove("RegMobileReg_VasIDs");
						}
					}

				}
			}
			else
			{
				Session[SessionKey.SelectedComponents.ToString()] = null;
			}
			try
			{
				if (vasVM.TabNumber == (int)MobileRegistrationSteps.MobileNo && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) || !string.IsNullOrEmpty(vasVM.SelectedContractID)))
				{
					//Commented by ravi as Vases are not binding in supline on 21/07/2013

					{
						#region VLT ADDED CODE
						//Contract check for existing users
						//Retrieve data from session
						#endregion VLT ADDED CODE
						///MNP MULTIPORT RELATED CHANGES
						if (IsMNPWithMultpleLines)
						{
							if (!string.IsNullOrEmpty(MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2()))
							{
								if (vasVM.SelectedVasIDs != null)
								{
									selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2()), string.Empty);
								}
							}
						}
						else if(primarySupplimentLines)
						{
							if (!string.IsNullOrEmpty(supplinePlanStorage.RegMobileReg_ContractID.ToString2()))
							{
								if (vasVM.SelectedVasIDs != null)
								{
									selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", supplinePlanStorage.RegMobileReg_ContractID.ToString2()), string.Empty);
								}
							}
						}
						else
						{
							if (!string.IsNullOrEmpty(Session["RegMobileReg_ContractID"].ToString2()))
							{
								if (vasVM.SelectedVasIDs != null)
								{
									selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session["RegMobileReg_ContractID"].ToString2()), string.Empty);
								}
							}
							/*Condition added by chetan on 27042013*/
							if (selectedVasIDs != string.Empty)
							{
								if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
								{
									selectedVasIDs += "," + vasVM.SelectedContractID + ",";
								}
							}
							else
							{
								selectedVasIDs += vasVM.SelectedContractID + ",";
							}
						}




						/*CHENGED BY CHETAN*/
						vasVM.SelectedVasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;

						///MNP MULTIPORT RELATED CHANGES
						if (IsMNPWithMultpleLines)
						{
							MNPSupplinePlanStorage.RegMobileReg_VasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
							MNPSupplinePlanStorage.RegMobileReg_ContractID = vasVM.SelectedContractID;
						}
						else if(primarySupplimentLines)
						{
							supplinePlanStorage.RegMobileReg_VasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
							supplinePlanStorage.RegMobileReg_ContractID = vasVM.SelectedContractID;
						}
						else
						{
							Session["RegMobileReg_VasIDs"] = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
							Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID;
						}
						/*END HERE*/




						using (var proxy = new CatalogServiceProxy())
						{
							///MNP MULTIPORT RELATED CHANGES
							if (IsMNPWithMultpleLines)
							{
								MNPSupplinePlanStorage.RegMobileReg_DataplanID = proxy.GettllnkContractDataPlan(Convert.ToInt32(MNPSupplinePlanStorage.RegMobileReg_ContractID));
							}
							else if(primarySupplimentLines)
							{
								supplinePlanStorage.RegMobileReg_DataplanID = proxy.GettllnkContractDataPlan(Convert.ToInt32(supplinePlanStorage.RegMobileReg_ContractID));

							}
							else
							{

							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
				throw ex;
			}

			using (var proxy = new UserServiceProxy())
			{
				if (Session["IsDeviceRequired"].ToString2() == "Yes")
				{
					proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Select Supplementary VAS", Session["KenanACNumber"].ToString2(), "");
				}
				else
				{
					proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SuppPlan, "Select Supplementary VAS", Session["KenanACNumber"].ToString2(), "");
				}
			}

			if (ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
			{
				if ((Session["MobileRegType"].ToString2() != "") && (Session["MobileRegType"].ToString2() != null))
				{
					string MobileRegType = Session["MobileRegType"].ToString2();
					if (MobileRegType == "Planonly")
					{
						return RedirectToAction(GetRegActionStep(vasVM.TabNumber), new { type = 8 });
					}

					return RedirectToAction(GetRegActionStep(vasVM.TabNumber));

				}
				else
				{

					return RedirectToAction(GetRegActionStep(vasVM.TabNumber));

				}
			}
			else
			{
				///REQUEST IS FROM MNP
				///MNP MULTIPORT RELATED CHANGES
				if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
				{
					if (IsMNPWithMultpleLines)
					{
						if (!string.IsNullOrEmpty(MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2()))
						{
							selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session["RegMobileReg_ContractID"].ToString2()), string.Empty);
						}
					}
					else
					{
						if (!string.IsNullOrEmpty(MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2()))
						{
							selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2()), string.Empty);
						}
					}

					if (primarySupplimentLines)
					{
						if (!string.IsNullOrEmpty(supplinePlanStorage.RegMobileReg_ContractID.ToString2()))
						{
							selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session["RegMobileReg_ContractID"].ToString2()), string.Empty);
						}
					}
					else
					{
						if (!string.IsNullOrEmpty(supplinePlanStorage.RegMobileReg_ContractID.ToString2()))
						{
							selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", supplinePlanStorage.RegMobileReg_ContractID.ToString2()), string.Empty);
						}
					}

					/*Condition added by chetan on 27042013*/
					if (selectedVasIDs != string.Empty)
					{
						if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
						{
							selectedVasIDs += "," + vasVM.SelectedContractID + ",";
						}
					}
					else
					{
						selectedVasIDs += vasVM.SelectedContractID + ",";
					}
				}
				vasVM.SelectedVasIDs = selectedVasIDs;
				///MNP MULTIPORT RELATED CHANGES
				if (IsMNPWithMultpleLines)
				{
					MNPSupplinePlanStorage.RegMobileReg_VasIDs = selectedVasIDs;
					MNPSupplinePlanStorage.RegMobileReg_ContractID = vasVM.SelectedContractID;
				}
				else if(primarySupplimentLines)
				{
					 supplinePlanStorage.RegMobileReg_VasIDs = selectedVasIDs;
					 supplinePlanStorage.RegMobileReg_ContractID = vasVM.SelectedContractID;
				}
				else
				{

					Session["RegMobileReg_VasIDs"] = selectedVasIDs;
					Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID;
				}


				///CHECK IF SUPPLEMENATRY LINES FOR MNP CONTAINS ANY OR MORE MSISDN'S TO PORT IN
				///IF THERE ARE THEN TRANSFER THE CONTROL TO MNPSelectSuppLine.cshtml                
				if (IsMNPWithMultpleLines)
				{
					///CHECK IF THE BACK BUTTON CLCKED THEN REDIRECT
					if (vasVM.TabNumber == (int)MobileRegistrationSteps.Plan)
					{
						return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
					}

					///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
					SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()];

					///CHECK FOR PROCESSING MSISDN
					if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
					{
						///UPDATE THE STATUS WITH PROCESSED
						SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Status = MNPSupplimentaryDetailsAddState.PROCESSED.ToInt();


						Session["mNPPrimaryPlanStorage"] = MNPPrimaryPlanStorage;
					}
					return RedirectToAction(GetRegActionStep(vasVM.TabNumber + 6), "Registration");
				}

				// redirecting to personal details page incase of MNP(Hope this case only when we take XTRATEN in MNP) 
				if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && vasVM.TabNumber == 4)
				{
					vasVM.TabNumber = 5;
				}
				//

                var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                DropFourHelpers.UpdateVAS(dropObj, vasVM);
                Session[SessionKey.DropFourObj.ToString()] = dropObj;

                //return RedirectToAction("SuppLineVASNew");

                return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
            }
        }


        [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DN")]
        [HttpPost]
        public ActionResult SuppLineVASNew(FormCollection collection, ValueAddedServicesVM vasVM)
        {
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

            //added by ravi on oct 21 2013 as it is giving exception
            if (vasVM.SelectedVasIDs != null && vasVM.SelectedVasIDs != string.Empty)
            {
                vasVM.SelectedVasIDs = vasVM.SelectedVasIDs.Replace("[object XMLDocument]", string.Empty);
            }
            //added by ravi on oct 21 2013 as it is giving exception ends here.
            // for bite CR
            List<int> depCompIds1 = new List<int>();
            List<int> tempdepCompIds = new List<int>();
            Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID.ToString2();

            string componentToCheckWithMISM = string.Empty;

            using (var proxy = new CatalogServiceProxy())
            {

                List<int> lstContracts = WebHelper.Instance.CommaSplitToList(vasVM.SelectedContractID.ToString2());
                Session[SessionKey.RegMobileReg_DataplanID.ToString()] = proxy.GettllnkContractDataPlan(lstContracts);
                dropObj.suppFlow[dropObj.suppIndex - 1].SelectedDataPlanID = (int[])Session[SessionKey.RegMobileReg_DataplanID.ToString()];
            }

            //20141205 - Refresh the contract details here since the contractId is moved here - Habs - start
            OrderSummaryVM orderVM = dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary;
            PersonalDetailsVM personalDetailsVM = dropObj.personalInformation;

            orderVM = WebHelper.Instance.ConstructPrimaryContractDetails(orderVM, personalDetailsVM, Session["RegMobileReg_ContractID"].ToString2(), Session["RegMobileReg_DataPkgID"].ToString2(), Session["MobileRegType"].ToString2());

            dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary = orderVM;
            //20141205 - Refresh the contract details here since the contractId is moved here - Habs - end


            //Changed by ravikiran on Feb 13 2014 for vas dependency Check components highlighting
            MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;
            MNPSelectPlanForSuppline SuppleMentaryLine = null;
            MNPSupplinePlanStorage MNPSupplinePlanStorage = null;

            //List of Suppline attached to primary line changes
            SelectPlanForSuppline selectPlanForSuppline = null;
            List<SupplinePlanStorage> supplinePlanStorageList;
            SupplinePlanStorage supplinePlanStorage = null;
            Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]);

            if (IsMNPWithMultpleLines)
            {
                ///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
                MNPPrimaryPlanStorage = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];
                if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
                {
                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
                    SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()];
                    ///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
                    if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        ProcessingMsisdn = SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
                        ///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
                        MNPSupplinePlanStorage = (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());



                        string addVAS = string.Empty;
                        foreach (var item in MNPSupplinePlanStorage.RegMobileReg_OrderSummary.VasVM.MandatoryVASes)
                        {
                            addVAS = (item.PgmBdlPkgCompID).ToString();
                            Session["MandatoryVasIds"] = Session["MandatoryVasIds"].ToString() + "," + addVAS;
                        }

                        string check = Session["MandatoryVasIds"].ToString();
                    }
                }
            }
            //Changed by ravikiran on Feb 13 2014 for vas dependency Check components highlighting ends here

            if (primarySupplimentLines)
            {
                supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : null;

                if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
                {
                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION 
                    selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString()];
                    ///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
                    if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
                        ///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
                        supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                    }
                }
            }

			if (collection["df_showOffer"].ToString2() == "show")
			{
				Session["df_showOffer"] = collection["df_showOffer"].ToString2();
                Session["DF_Eligibility"] = true;
				return RedirectToAction("SuppLineVASNew");
			}

			// DF
			if (dropObj.suppFlow[dropObj.suppIndex - 1].phone != null)
			{
				string df_articleID = dropObj.suppFlow[dropObj.suppIndex - 1].phone.DeviceArticleId;
				int df_pbpcID = dropObj.suppFlow[dropObj.suppIndex - 1].PkgPgmBdlPkgCompID;
				vasVM = WebHelper.Instance.autoAddWaiverComponent(vasVM, df_articleID, df_pbpcID, "DF");
			}
            #region fxDependency Check
            if (vasVM.TabNumber == 4 || vasVM.TabNumber == 5)
            {
                //Added by chetan on request from Pavan
                Session["RegMobileReg_MainDevicePrice"] = vasVM.price;
                Session["RegMobileReg_OfferDevicePrice"] = vasVM.price;
                //Up to here
                List<FxDependChkComps> Comps = new List<FxDependChkComps>();
                using (var Proxy = new RegistrationServiceProxy())
                {
                    string strCompIds = string.Empty;
                    if (vasVM.SelectedVasIDs.ToString2() != string.Empty)
                    {
                        strCompIds = vasVM.SelectedVasIDs;
                        //Changed by ravikiran on Feb 13 2014 for vas dependency Check components highlighting
                        if (IsMNPWithMultpleLines == true)
                        {
                            MNPSupplinePlanStorage.SelectedComponents = vasVM.SelectedVasIDs;
                            if (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) && vasVM.SelectedVasIDs.Split(',').Any())
                            {
                                List<int> selectedids = new List<int>();
                                foreach (string item in vasVM.SelectedVasIDs.Split(','))
                                {
                                    if (item != string.Empty)
                                    {
                                        selectedids.Add(item.ToInt());
                                    }
                                }
                                //List<int> selectedids = vasVM.SelectedVasIDs.Split(',').Select(int.Parse).ToList();
                                if (selectedids.Any())
                                {
                                    List<int> depids = WebHelper.Instance.GetDepenedencyComponents(selectedids);
                                    if (depids != null && depids.Any())
                                    {
                                        foreach (var item in depids)
                                        {
                                            MNPSupplinePlanStorage.SelectedComponents += "," + item;
                                        }
                                    }
                                }
                            }
                        }
                        else if (primarySupplimentLines)
                        {
                            supplinePlanStorage.SelectedComponents = vasVM.SelectedVasIDs;
                            if (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) && vasVM.SelectedVasIDs.Split(',').Any())
                            {
                                List<int> selectedids = new List<int>();
                                foreach (string item in vasVM.SelectedVasIDs.Split(','))
                                {
                                    if (item != string.Empty)
                                    {
                                        selectedids.Add(item.ToInt());
                                    }
                                }
                                if (selectedids.Any())
                                {
                                    List<int> depids = WebHelper.Instance.GetDepenedencyComponents(selectedids);
                                    if (depids != null && depids.Any())
                                    {
                                        foreach (var item in depids)
                                        {
                                            supplinePlanStorage.SelectedComponents += "," + item;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Session[SessionKey.SelectedComponents.ToString()] = vasVM.SelectedVasIDs;
                        }
                        //Changed by ravikiran on Feb 13 2014 for vas dependency Check components highlighting ends here

                    }
                    if (Session["MandatoryVasIds"].ToString2().Length > 0)
                    {
                        strCompIds = strCompIds + "," + Session["MandatoryVasIds"].ToString2();
                    }
                    strCompIds += "," + Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
                    if (vasVM.OfferId != null)
                    {
                        Session["OfferId"] = vasVM.OfferId.ToString();
                    }
                    //for adding contract id
                    if (vasVM.SelectedContractID.ToString2().Length > 0)
                    {
                        strCompIds += "," + vasVM.SelectedContractID;
                    }

                    if (Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToString2() != string.Empty)
                    {
                        try
                        {
                            int[] dataplanid = (int[])Session[SessionKey.RegMobileReg_DataplanID.ToString()];
                            foreach (int pid in dataplanid)
                            {
                                depCompIds1.Add(pid.ToInt());
                                tempdepCompIds = GetDepenedencyComponents(pid.ToString2());
                                foreach (var item in tempdepCompIds)
                                {
                                    depCompIds1.Add(item);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            depCompIds1.Add(Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt());
                            WebHelper.Instance.LogExceptions(this.GetType(), ex);
                        }
                        if (depCompIds1 != null && depCompIds1.Count > 0)
                        {
                            depCompIds1 = depCompIds1.Distinct().ToList();
                            foreach (var item in depCompIds1)
                            {
                                strCompIds += "," + item.ToString2();
                            }
                        }
                    }

                    // added by Nreddy by pass the dependece check method when click on back button
                    if (collection["TabNumber"] != "2")
                    {
                        Comps = Proxy.GetAllComponentsbyPlanId(Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(), strCompIds, "BP");
                    }
                }
                List<Online.Registration.Web.SubscriberICService.componentList> componentList = new List<Online.Registration.Web.SubscriberICService.componentList>();

				dropObj.suppFlow[dropObj.suppIndex - 1].isDeviceFinancingOrder = WebHelper.Instance.determineDeviceFinancingOrder(Comps);

				if (dropObj.suppFlow[dropObj.suppIndex - 1].isDeviceFinancingOrder)
				{
					// dropObj.DFcustomerEligibility <<< customer eligibility
					dropObj.suppFlow[dropObj.suppIndex - 1].isDeviceFinancingExceptionalOrder = !dropObj.DFcustomerEligibility ? true : false;
				}

                if (dropObj.suppFlow[dropObj.suppIndex - 1].isAccFinancingOrder)
                {
                    var selectedVASIDs = vasVM.SelectedVasIDs;
                    var selectedKenanCodes = vasVM.SelectedKenanCodes;
                    WebHelper.Instance.AutoAddAccessoryFinancingComponents(ref Comps, ref selectedVASIDs, ref selectedKenanCodes);

                    vasVM.SelectedVasIDs = selectedVASIDs;
                    vasVM.SelectedKenanCodes = selectedKenanCodes;
                    Session[SessionKey.SelectedComponents.ToString()] = selectedVASIDs;
                    //Session[SessionKey.SelectedKenanCodes.ToString()] = selectedKenanCodes;
                }

				foreach (var comp in Comps)
                {
                    componentToCheckWithMISM += comp.ComponentKenanCode + ",";
                    Online.Registration.Web.SubscriberICService.componentList component = new componentList();
                    component.componentIdField = comp.ComponentKenanCode;
                    component.packageIdField = comp.KenanCode;
                    componentList.Add(component);
                    if (comp.DependentKenanCode != -1)
                    {
                        component = new componentList();
                        component.componentIdField = comp.DependentKenanCode.ToString();
                        component.packageIdField = comp.KenanCode;
                        if (IsMNPWithMultpleLines || primarySupplimentLines)
                        {
                            using (var prox = new CatalogServiceProxy())
                            {
                                var comps = prox.PgmBdlPckComponentGetForSmartByKenanCode(new string[] { comp.DependentKenanCode.ToString() }.ToList()).ToList();
                                if (comps != null & comps.Count > 0)
                                {
                                    foreach (var item in comps.OrderBy(a => a.ID))
                                    {
                                        if (item.KenanCode == comp.DependentKenanCode.ToString())
                                        {
                                            if (primarySupplimentLines)
                                            {
                                                if (ReferenceEquals(supplinePlanStorage.SelectedComponents, null))
                                                    supplinePlanStorage.SelectedComponents = string.Empty;
                                                if (!supplinePlanStorage.SelectedComponents.Contains(item.ID.ToString()))
                                                    supplinePlanStorage.SelectedComponents += "," + item.ID.ToString();
                                                break;
                                            }
                                            else
                                            {
                                                if (ReferenceEquals(MNPSupplinePlanStorage.SelectedComponents, null))
                                                    MNPSupplinePlanStorage.SelectedComponents = string.Empty;
                                                if (!MNPSupplinePlanStorage.SelectedComponents.Contains(item.ID.ToString()))
                                                    MNPSupplinePlanStorage.SelectedComponents += "," + item.ID.ToString();
                                                break;
                                            }
                                        }
                                    }

                                }
                                componentList.Add(component);
                            }
                        }
                        else
                        {
                            componentList.Add(component);
                        }
                    }
                }
                if (componentList.Count > 0)
                {
					// Drop 5
					retrieveServiceInfoProxy _retrieveServiceInfoProxy = new retrieveServiceInfoProxy();
					string pbpcID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToString2();
					typeChkFxVasDependencyRequest request = WebHelper.Instance.ConstructVASDependencyCheckRequest(componentList, pbpcID: pbpcID);
					
                    var FxReqXML = Util.ConvertObjectToXml(request);
                    typeChkFxVasDependencyResponse response = _retrieveServiceInfoProxy.ChkFxVasDependency(request);
                    ValueAddedServicesVM vas = (ValueAddedServicesVM)Session["supplineVas"];
                    //Evan - assign GST Notif start
                    ViewBag.GSTMark = Settings.Default.GSTMark;
                    List<String> LabelReplace = new List<String>();
                    LabelReplace.Add("GSTMark");
                    ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
                    //Evan - assign GST Notif end
                    if (response.mxsRuleMessagesField != null && response.mxsRuleMessagesField.Count > 0)
                    {
                        foreach (string errmsg in response.mxsRuleMessagesField)
                        {
                            ModelState.AddModelError(string.Empty, errmsg);
                        }
                        return View(vas);
                    }
                    else if (response.msgCodeField == "3")
                    {
                        ModelState.AddModelError(string.Empty, "OPF system error - Unable to fetch data.");   // Need to change msg
                        return View(vas);
                    }
                    else if (response.msgCodeField != "0")
                    {
                        ModelState.AddModelError(string.Empty, "Selected components are not available.");   // Need to change msg
                        return View(vas);
                    }

					string errorDependent = WebHelper.Instance.checkVASDependentDF(Comps);
					if (!string.IsNullOrWhiteSpace(errorDependent))
					{
						ModelState.AddModelError("", errorDependent);
						return View(vas);
					}
                }
            }
            #endregion
			// df
			

            Session["VasGroupIds"] = vasVM.hdnGroupIds;
            if (IsMNPWithMultpleLines)
            {
                if (vasVM.ExtraTenMobileNumbers.ToString2().Length > 0)
                {
                    MNPSupplinePlanStorage.ExtratenMobilenumbers = vasVM.ExtraTenMobileNumbers;
                }
                MNPSupplinePlanStorage.RegMobileReg_TabIndex = vasVM.TabNumber;
            }
            else if (primarySupplimentLines)
            {
                if (vasVM.ExtraTenMobileNumbers.ToString2().Length > 0)
                {
                    supplinePlanStorage.ExtratenMobilenumbers = vasVM.ExtraTenMobileNumbers;
                }
                supplinePlanStorage.RegMobileReg_TabIndex = vasVM.TabNumber;
            }
            else
            {
                Session["ExtratenMobilenumbers"] = vasVM.ExtraTenMobileNumbers;
            }
            List<int> planIds = new List<int>();
            PgmBdlPckComponent package = new PgmBdlPckComponent();
            Session["RegMobileReg_TabIndex"] = vasVM.TabNumber;
            Session["RegMobileReg_MainDevicePrice"] = vasVM.price;
            Session["RegMobileReg_OfferDevicePrice"] = vasVM.price;
            Session[SessionKey.RegMobileReg_UOMCode.ToString()] = vasVM.uomCode;


            Session["RegMobileReg_TabIndex"] = vasVM.TabNumber;
            using (var proxy = new CatalogServiceProxy())
            {
                package = MasterDataCache.Instance.FilterComponents(planIds).FirstOrDefault();
                var respOffers = proxy.GETMOCOfferDetails(vasVM.OfferId);
                MOCOfferDetails mocOffer = new MOCOfferDetails();
                if (respOffers != null)
                {
                    mocOffer.DiscountAmount = respOffers.DiscountAmount;
                    mocOffer.ID = respOffers.ID;
                    mocOffer.Name = respOffers.Name;
                    mocOffer.OfferDescription = respOffers.OfferDescription;
                    Session["Discount"] = mocOffer.DiscountAmount;

                    ///MNP MULTIPORT RELATED CHANGES
                    if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
                    {
                        if (!ReferenceEquals(Session["mNPPrimaryPlanStorage"], null))
                        {
                            ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).Discount = mocOffer.DiscountAmount;
                        }
                    }
                }
            }
            //modefied by chetan
            var selectedVasIDs = string.Empty;
            if (vasVM.SelectedVasIDs != null)
            {
                if (vasVM.SelectedVasIDs != null && !Regex.IsMatch(vasVM.SelectedVasIDs, @"^,+$") && vasVM.SelectedVasIDs != "[object XMLDocument]")
                {
                    selectedVasIDs = vasVM.SelectedVasIDs;
                    ///MNP MULTIPORT RELATED CHANGES
                    if (IsMNPWithMultpleLines)
                    {
                        MNPSupplinePlanStorage.RegMobileReg_DataPkgID = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
                    }
                    else if (primarySupplimentLines)
                    {
                        supplinePlanStorage.RegMobileReg_DataPkgID = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
                    }
                    else
                    {
                        selectedVasIDs = vasVM.SelectedVasIDs;
                        Session["RegMobileReg_DataPkgID"] = selectedVasIDs.Length > 4 ? selectedVasIDs.Remove(4) : selectedVasIDs;
                    }
                }
                else
                {
                    ///MNP MULTIPORT RELATED CHANGES
                    if (IsMNPWithMultpleLines)
                    {
                        MNPSupplinePlanStorage.RegMobileReg_DataPkgID = null;
                        MNPSupplinePlanStorage.RegMobileReg_VasIDs = null;
                    }
                    else if (primarySupplimentLines)
                    {
                        supplinePlanStorage.RegMobileReg_DataPkgID = null;
                        supplinePlanStorage.RegMobileReg_VasIDs = null;
                    }
                    else
                    {
                        if (!ReferenceEquals(Session["RegMobileReg_DataPkgID"], null))
                        {
                            Session.Contents.Remove("RegMobileReg_DataPkgID");
                        }
                        if (!ReferenceEquals(Session["RegMobileReg_VasIDs"], null))
                        {
                            Session.Contents.Remove("RegMobileReg_VasIDs");
                        }
                    }

                }
            }
            else
            {
                Session[SessionKey.SelectedComponents.ToString()] = null;
            }
            try
            {
                if ((vasVM.TabNumber == (int)MobileRegistrationSteps.MobileNo || vasVM.TabNumber == (int)MobileRegistrationSteps.PersonalDetails) && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) || !string.IsNullOrEmpty(vasVM.SelectedContractID)))
                {
                    //Commented by ravi as Vases are not binding in supline on 21/07/2013

                    {
                        #region VLT ADDED CODE
                        //Contract check for existing users
                        //Retrieve data from session
                        #endregion VLT ADDED CODE
                        ///MNP MULTIPORT RELATED CHANGES
                        if (IsMNPWithMultpleLines)
                        {
                            if (!string.IsNullOrEmpty(MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2()))
                            {
                                if (vasVM.SelectedVasIDs != null)
                                {
                                    selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2()), string.Empty);
                                }
                            }
                        }
                        else if (primarySupplimentLines)
                        {
                            if (!string.IsNullOrEmpty(supplinePlanStorage.RegMobileReg_ContractID.ToString2()))
                            {
                                if (vasVM.SelectedVasIDs != null)
                                {
                                    selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", supplinePlanStorage.RegMobileReg_ContractID.ToString2()), string.Empty);
                                }
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Session["RegMobileReg_ContractID"].ToString2()))
                            {
                                if (vasVM.SelectedVasIDs != null)
                                {
                                    selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session["RegMobileReg_ContractID"].ToString2()), string.Empty);
                                }
                            }
                            /*Condition added by chetan on 27042013*/
                            if (selectedVasIDs != string.Empty)
                            {
                                if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
                                {
                                    selectedVasIDs += "," + vasVM.SelectedContractID + ",";
                                }
                            }
                            else
                            {
                                selectedVasIDs += vasVM.SelectedContractID + ",";
                            }

                            //add monthly fee component for DF
                            if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                            {
                                if (dropObj.suppFlow[dropObj.suppIndex - 1].isDeviceFinancingOrder)
                                {
                                    var vasIDList = new List<int>();

                                    if (vasVM.SelectedContractID.Contains(','))
                                    {
                                        try
                                        {
                                            vasIDList.AddRange(vasVM.SelectedContractID.Split(',').Select(int.Parse).ToList());
                                        }
                                        catch (Exception ex)
                                        {
                                            vasIDList = new List<int>();
                                        }
                                    }
                                    else
                                    {
                                        vasIDList.Add(vasVM.SelectedContractID.ToInt());
                                    }

                                    if (vasIDList.Count > 0)
                                    {
                                        var dependentComponentIDs = WebHelper.Instance.GetDepenedencyComponents(vasIDList);

                                        if (dependentComponentIDs != null && dependentComponentIDs.Count > 0)
                                        {
                                            var dependentCompString = string.Join(",", dependentComponentIDs);

                                            if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
                                            {
                                                selectedVasIDs += "," + dependentCompString + ",";
                                            }
                                            else
                                            {
                                                selectedVasIDs += dependentCompString + ",";
                                            }
                                        }
                                    }
                                }
                            }
                        }




                        /*CHENGED BY CHETAN*/
                        vasVM.SelectedVasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;

                        ///MNP MULTIPORT RELATED CHANGES
                        if (IsMNPWithMultpleLines)
                        {
                            Session["RegMobileReg_VasIDs"] = MNPSupplinePlanStorage.RegMobileReg_VasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
                            MNPSupplinePlanStorage.RegMobileReg_ContractID = vasVM.SelectedContractID;
                        }
                        else if (primarySupplimentLines)
                        {
                            supplinePlanStorage.RegMobileReg_VasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
                            supplinePlanStorage.RegMobileReg_ContractID = vasVM.SelectedContractID;
                        }
                        else
                        {
                            Session["RegMobileReg_VasIDs"] = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;
                            Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID;
                        }
                        /*END HERE*/




                        using (var proxy = new CatalogServiceProxy())
                        {
                            ///MNP MULTIPORT RELATED CHANGES
                            if (IsMNPWithMultpleLines)
                            {
                                MNPSupplinePlanStorage.RegMobileReg_DataplanID = proxy.GettllnkContractDataPlan(Convert.ToInt32(MNPSupplinePlanStorage.RegMobileReg_ContractID));
                            }
                            else if (primarySupplimentLines)
                            {
                                supplinePlanStorage.RegMobileReg_DataplanID = proxy.GettllnkContractDataPlan(Convert.ToInt32(supplinePlanStorage.RegMobileReg_ContractID));

                            }
                            //else
                            //{

                            //}
                        }
                    }
                }

                if (dropObj.currentFlow == DropFourConstant.SUPP_FLOW)
                {
                    dropObj.suppFlow[dropObj.suppIndex - 1].OfferName = collection["OfferName"];
                }

				DropFourHelpers.UpdateVAS(dropObj, vasVM);
				Session["RegMobileReg_SublineSummary"] = Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();

                Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(fromSubline: true);
                DropFourHelpers.UpdateVAS(dropObj, vasVM);

                //14012015 - Anthony - GST Trade Up - Start
                if (!IsMNPWithMultpleLines)
                {
                    Session[SessionKey.TradeUpAmount.ToString()] = vasVM.hdnTradeUpAmount;

                    if (!string.IsNullOrEmpty(vasVM.hdnTradeUpAmount))
                    {
                        //ValueAddedServicesVM lstOffers = (ValueAddedServicesVM)Session["VAS"];
                        ValueAddedServicesVM lstOffers = (ValueAddedServicesVM)Session["supplineVas"];
                        var offerIDSelected = vasVM.hdnTradeUpAmount.Split('_')[1].ToInt();
                        var tradeUpAmountSelected = vasVM.hdnTradeUpAmount.Split('_')[2].ToInt();//negative value
                        var deviceOfferPrice = vasVM.price.ToInt();
                        var tradeUpList = new TradeUpComponent();

                        foreach (var tradeUpAmountList in lstOffers.TradeUpList)
                        {
                            if (tradeUpAmountList.OfferId == offerIDSelected && tradeUpAmountList.Price == tradeUpAmountSelected)
                            {
                                tradeUpList.OfferId = offerIDSelected;
                                tradeUpList.Name = "RM " + tradeUpAmountSelected * -1;
                                tradeUpList.Price = tradeUpAmountSelected;

                                Session[SessionKey.TradeUpAmount.ToString()] = vasVM.hdnTradeUpAmount;

                                dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.SelectedTradeUpComp = tradeUpList;
                                dropObj.suppFlow[dropObj.suppIndex - 1].isTradeUp = true;
                                break;
                            }
                            else
                            {
                                Session[SessionKey.TradeUpAmount.ToString()] = 0;
                            }
                        }
                    }
                }
                //14012015 - Anthony - GST Trade Up - End

                Session[SessionKey.DropFourObj.ToString()] = dropObj;
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

            using (var proxy = new UserServiceProxy())
            {
                if (Session["IsDeviceRequired"].ToString2() == "Yes")
                {
                    proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Select Supplementary VAS", Session["KenanACNumber"].ToString2(), "");
                }
                else
                {
                    proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SuppPlan, "Select Supplementary VAS", Session["KenanACNumber"].ToString2(), "");
                }
            }

            if (ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
            {
                if ((Session["MobileRegType"].ToString2() != "") && (Session["MobileRegType"].ToString2() != null))
                {
                    string MobileRegType = Session["MobileRegType"].ToString2();
                    if (MobileRegType == "Planonly")
                    {
                        if (vasVM.TabNumber == (int)MobileRegistrationSteps.Plan)
                        {
                            dropObj.suppIndex = dropObj.suppIndex - 1;
                            dropObj.suppFlow.Remove(dropObj.suppFlow[dropObj.suppIndex]);
                            Session[SessionKey.DropFourObj.ToString()] = dropObj;
                        }
                        return RedirectToAction(GetRegActionStep(vasVM.TabNumber), new { type = 8 });
                    }
                    else
                    {
                        if (vasVM.TabNumber == (int)MobileRegistrationSteps.DevicePlan)
                        {
                            dropObj.suppIndex = dropObj.suppIndex - 1;
                            dropObj.suppFlow.Remove(dropObj.suppFlow[dropObj.suppIndex]);
                            Session[SessionKey.DropFourObj.ToString()] = dropObj;
                        }
                    }
                    return RedirectToAction(GetRegActionStep(vasVM.TabNumber));

                }
                else
                {

                    return RedirectToAction(GetRegActionStep(vasVM.TabNumber));

                }
            }
            else
            {
                ///REQUEST IS FROM MNP
                ///MNP MULTIPORT RELATED CHANGES
                if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                {
                    if (IsMNPWithMultpleLines)
                    {
                        if (!string.IsNullOrEmpty(MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2()))
                        {
                            selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session["RegMobileReg_ContractID"].ToString2()), string.Empty);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2()))
                        {
                            selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2()), string.Empty);
                        }
                    }

                    if (primarySupplimentLines)
                    {
                        if (!string.IsNullOrEmpty(supplinePlanStorage.RegMobileReg_ContractID.ToString2()))
                        {
                            selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session["RegMobileReg_ContractID"].ToString2()), string.Empty);
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(supplinePlanStorage.RegMobileReg_ContractID.ToString2()))
                        {
                            selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", supplinePlanStorage.RegMobileReg_ContractID.ToString2()), string.Empty);
                        }
                    }

                    /*Condition added by chetan on 27042013*/
                    if (selectedVasIDs != string.Empty)
                    {
                        if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
                        {
                            selectedVasIDs += "," + vasVM.SelectedContractID + ",";
                        }
                    }
                    else
                    {
                        selectedVasIDs += vasVM.SelectedContractID + ",";
                    }
                }
                vasVM.SelectedVasIDs = selectedVasIDs;
                ///MNP MULTIPORT RELATED CHANGES
                if (IsMNPWithMultpleLines)
                {
                    MNPSupplinePlanStorage.RegMobileReg_VasIDs = selectedVasIDs;
                    MNPSupplinePlanStorage.RegMobileReg_ContractID = vasVM.SelectedContractID;
                }
                else if (primarySupplimentLines)
                {
                    supplinePlanStorage.RegMobileReg_VasIDs = selectedVasIDs;
                    supplinePlanStorage.RegMobileReg_ContractID = vasVM.SelectedContractID;
                }
                else
                {

                    Session["RegMobileReg_VasIDs"] = selectedVasIDs;
                    Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID;
                }


                ///CHECK IF SUPPLEMENATRY LINES FOR MNP CONTAINS ANY OR MORE MSISDN'S TO PORT IN
                ///IF THERE ARE THEN TRANSFER THE CONTROL TO MNPSelectSuppLine.cshtml                
                if (IsMNPWithMultpleLines)
                {
                    ///CHECK IF THE BACK BUTTON CLCKED THEN REDIRECT
                    if (vasVM.TabNumber == (int)MobileRegistrationSteps.Plan || vasVM.TabNumber == (int)MobileRegistrationSteps.DevicePlan)
                    {
                        dropObj.suppIndex = dropObj.suppIndex - 1;
                        dropObj.suppFlow.Remove(dropObj.suppFlow[dropObj.suppIndex]);
                        Session[SessionKey.DropFourObj.ToString()] = dropObj;

                        return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
                    }

                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
                    SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()];

                    ///CHECK FOR PROCESSING MSISDN
                    if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        ///UPDATE THE STATUS WITH PROCESSED
                        SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Status = MNPSupplimentaryDetailsAddState.PROCESSED.ToInt();


                        Session["mNPPrimaryPlanStorage"] = MNPPrimaryPlanStorage;
                    }

                    int MSISNDNstatus = 2;

                    foreach (var item in SuppleMentaryLine.SupplimentaryMSISDNs)
                    {
                        if (item.Status == 0)
                        {
                            MSISNDNstatus = 1;
                            break;
                        }
                    }

                    if (dropObj.suppIndex > 0)
                        dropObj.suppFlow[dropObj.suppIndex - 1]._flowCompleted = true;

                    if (MSISNDNstatus == 1)
                    {
                        return RedirectToAction(GetRegActionStep(vasVM.TabNumber + 6), "Registration");
                    }
                    else
                    {
                        return RedirectToAction(GetRegActionStep(vasVM.TabNumber), "Registration");

                    }

                }

                // redirecting to personal details page incase of MNP(Hope this case only when we take XTRATEN in MNP) 
                if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && vasVM.TabNumber == 4)
                {
                    vasVM.TabNumber = 5;
                }
                return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
            }
        }



        [Authorize]
        public ActionResult BlacklistChecking()
        {
            WebHelper.Instance.ClearRegistrationSession();
            Session["RegMobileReg_Type"] = (int)MobileRegType.SuppPlan;
            return View(new PersonalDetailsVM());
        }

        [Authorize]
        [HttpPost]
        public ActionResult BlacklistChecking(string idCardNo, string idCardTypeID)
        {
            try
            {
                Session[SessionKey.RegMobileReg_IDCardNo.ToString()] = idCardNo;
                Session[SessionKey.RegMobileReg_IDCardType.ToString()] = idCardTypeID;

                var idCardType = new IDCardType();
                using (var proxy = new RegistrationServiceProxy())
                {
                    idCardType = proxy.IDCardTypeGet(idCardTypeID.ToInt());
                }

                using (var proxy = new KenanServiceProxy())
                {
                    var resp = proxy.BusinessRuleCheck(new BusinessRuleRequest()
                    {
                        RuleNames = Properties.Settings.Default.BusinessRule_MOBILERuleNames.Split('|'),
                        IDCardNo = idCardNo,
                        IDCardType = idCardType.KenanCode
                    });

                    Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] = resp.IsBlacklisted;
                    Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = resp.ResultMessage;
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

            return Json(Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()].ToBool());
        }

        [Authorize]
        public ActionResult BlacklistResult(bool result)
        {
            var accounts = new List<FoundAccounts>();
            accounts.Add(new FoundAccounts()
            {
                AccountNo = "109258111",
                ICNo = "801010031234",
                MSISDN = "60123456789",
                Name = "Tan Kok Wai"
            });

            accounts.Add(new FoundAccounts()
            {
                AccountNo = "108472942",
                ICNo = "801010031234",
                MSISDN = "60123456009",
                Name = "Tan Kok Wai"
            });

            accounts.Add(new FoundAccounts()
            {
                AccountNo = "103958329",
                ICNo = "801010031234",
                MSISDN = "60123381945",
                Name = "Tan Kok Wai"
            });

            ViewBag.Accounts = accounts;
            ViewBag.isBlacklisted = result;

            var resultMsg = Session[SessionKey.RegMobileReg_ResultMessage.ToString()];

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult BlacklistResult(FormCollection collection)
        {
            try
            {
                var isBlacklist = collection["isBlacklisted"].ToString2();

                if (collection["submit"].ToString2() == "back")
                {
                    return View("BlacklistChecking");
                }
                else if (collection["submit"].ToString2() == "next" || collection["submit"].ToString2() == "skip")
                {
                    if (collection["submit"].ToString2() == "skip")
                        Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] = (isBlacklist == "True") ? true : false;

                    return RedirectToAction("SelectPlan");
                }

                Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
                Session[SessionKey.TabNumber.ToString()] = string.Empty;
                Session[SessionKey.TabValue.ToString()] = string.Empty;

            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            return View();
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DAC,MREG_DC,MREG_DAI,MREG_DAC")]
        public ActionResult SelectPlan(int type = 0)
        {
            Session["ExtratenMobilenumbers"] = null;
            if (type > 0)
            {
                ResetAllSession(type);
            }

            SetRegStepNo(MobileRegistrationSteps.Plan);

            Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();
            var packageVM = GetAvailablePackages();

            //Dealer specific plans            
            var dealerPkgs = Util.GetDealerPlans(packageVM.AvailablePackages);
            if (dealerPkgs.Count > 0)
            {
                packageVM.AvailablePackages = dealerPkgs;
            }
            // EOF Dealer specific plans

            return View(packageVM);
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DAC,MREG_DC,MREG_DAI,MREG_DAC")]
        public ActionResult SelectPlan(PackageVM packageVM)
        {
            //VLT ADDED CODE to check for PPID verification
            if (Session["PPID"].ToString2() != string.Empty)
            {
                if (!ReferenceEquals(Session["RedirectURL"], null))
                {
                    Session.Contents.Remove("RedirectURL");
                }

                Session["RegMobileReg_TabIndex"] = packageVM.TabNumber;
                try
                {
                    if (packageVM.TabNumber == (int)MobileRegistrationSteps.Vas)
                    {
                        // selected different Plan
                        if (Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt() != packageVM.SelectedPgmBdlPkgCompID)
                        {
                            Session["RegMobileReg_PkgPgmBdlPkgCompID"] = packageVM.SelectedPgmBdlPkgCompID;

                            // Retrieve Plan Min Age
                            using (var proxy = new CatalogServiceProxy())
                            {
                                var pbpc = MasterDataCache.Instance.FilterComponents(new int[] { packageVM.SelectedPgmBdlPkgCompID }).SingleOrDefault();
                                //Chnages made by chetan for SKMM
                                Session["UOMPlanID"] = pbpc.KenanCode;

                                var bundleID = pbpc.ParentID;
                                var programID = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
                                {
                                    PgmBdlPckComponent = new PgmBdlPckComponent()
                                    {
                                        ChildID = bundleID,
                                        LinkType = Properties.Settings.Default.Program_Bundle
                                    },
                                    Active = true
                                })).SingleOrDefault().ParentID;

                                var minAge = proxy.ProgramGet(new int[] { programID }).SingleOrDefault().MinAge;
                                Session["RegMobileReg_ProgramMinAge"] = minAge;
                            }

                            // reset session
                            Session["RegMobileReg_ContractID"] = null;
                            Session["RegMobileReg_VasIDs"] = null;
                            Session["RegMobileReg_VasNames"] = null;
                        }
                    }

                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogExceptions(this.GetType(), ex);
                    throw ex;
                }
                return RedirectToAction(GetRegActionStep(packageVM.TabNumber));
            }
            else
            {
                Session["RedirectURL"] = "GuidedSales/PlanSearch";
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize]
        public ActionResult SelectVAS()
        {

            ValueAddedServicesVM vasVM = null;
            #region MNP MULTIPORT RELATED CHANGES
            MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;

            ///INSTANCE WILL CONTAIN LIST OF SUPPLEMENTARY LINES HAVING DIFFERENT STATES SUCH AS 
            ///MNPSupplimentaryDetailsAddState.NOTINITIALIZED
            ///MNPSupplimentaryDetailsAddState.PROCESSING
            ///MNPSupplimentaryDetailsAddState.PROCESSED
            ///ONLY ONE MSISDN CAN BE IN PROCESSING STATE AT ANY POINT OF TIME
            MNPSelectPlanForSuppline SuppleMentaryLine = null;
            MNPSupplinePlanStorage MNPSupplinePlanStorage = null;

            if (IsMNPWithMultpleLines)
            {
                ///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
                MNPPrimaryPlanStorage = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];

                if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
                {
                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
                    SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()];

                    ///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
                    if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        ProcessingMsisdn = SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
                        ///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
                        MNPSupplinePlanStorage = (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                    }
                }
            }
            #endregion MNP MULTIPORT RELATED CHANGES
            SetRegStepNo(MobileRegistrationSteps.Vas);
            ///MNP MULTIPORT RELATED CHANGES
            if (IsMNPWithMultpleLines)
            {
                MNPSupplinePlanStorage.RegMobileReg_OrderSummary = ConstructOrderSummary();
                vasVM = WebHelper.Instance.GetAvailablePackageComponents(MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID, ref ProcessingMsisdn, fromSuppline: true);

            }
            else
            {
                Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();
                vasVM = WebHelper.Instance.GetAvailablePackageComponents(Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(), ref ProcessingMsisdn, fromSuppline: true);
            }


            return View(vasVM);
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DAC,MREG_DC,MREG_DAI,MREG_DAC")]
        public ActionResult SelectVAS(ValueAddedServicesVM vasVM)
        {
            Session["RegMobileReg_TabIndex"] = vasVM.TabNumber;
            var selectedVasIDs = vasVM.SelectedVasIDs;

            try
            {
                if (vasVM.TabNumber == (int)MobileRegistrationSteps.MobileNo && (!string.IsNullOrEmpty(vasVM.SelectedVasIDs) || !string.IsNullOrEmpty(vasVM.SelectedContractID)))
                {
                    if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                    {
                        #region VLT ADDED CODE
                        //Contract check for existing users
                        //Retrieve data from session                         
                        #endregion VLT ADDED CODE
                        if (!string.IsNullOrEmpty(Session["RegMobileReg_ContractID"].ToString2()))
                            selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session["RegMobileReg_ContractID"].ToString2()), string.Empty);

                        /*Condition added by chetan on 27042013*/
                        /*Condition added by chetan on 27042013*/
                        if (selectedVasIDs != string.Empty)
                        {
                            if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
                            {
                                selectedVasIDs += "," + vasVM.SelectedContractID + ",";
                            }
                        }
                        else
                        {
                            selectedVasIDs += vasVM.SelectedContractID + ",";
                        }
                    }

                    vasVM.SelectedVasIDs = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;

                    Session["RegMobileReg_VasIDs"] = selectedVasIDs == "[object XMLDocument]" ? null : selectedVasIDs;

                    Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID;
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
            if (ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
            {
                if (!string.IsNullOrEmpty(Session["MobileRegType"].ToString2()))
                {
                    string MobileRegType = Session["MobileRegType"].ToString2();
                    if (MobileRegType == "Planonly")
                    {
                        return RedirectToAction(GetRegActionStep(vasVM.TabNumber), new { type = 2 });
                    }
                    return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
                }
                else
                {
                    return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(vasVM.SelectedContractID))
                {
                    if (!string.IsNullOrEmpty(Session["RegMobileReg_ContractID"].ToString2()))
                        selectedVasIDs = vasVM.SelectedVasIDs.Replace(string.Format("{0},", Session["RegMobileReg_ContractID"].ToString2()), string.Empty);

                    /*Condition added by chetan on 27042013*/
                    if (selectedVasIDs != string.Empty)
                    {
                        if (selectedVasIDs.Substring(selectedVasIDs.Length - 1) != ",")
                        {
                            selectedVasIDs += "," + vasVM.SelectedContractID + ",";
                        }
                    }
                    else
                    {
                        selectedVasIDs += vasVM.SelectedContractID + ",";
                    }
                }

                vasVM.SelectedVasIDs = selectedVasIDs;

                Session["RegMobileReg_VasIDs"] = selectedVasIDs;
                Session["RegMobileReg_ContractID"] = vasVM.SelectedContractID;
                return RedirectToAction(GetRegActionStep(vasVM.TabNumber));
            }
        }



        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        public ActionResult SelectMobileNo(string desireNo = "", string numberType = "n", string prefix = "")
        {
            var mobileNoVM = new MobileNoVM();
            var mobileNos = new MobileNoSelectionResponse();
            List<string> objMobileNos = new List<string>();

            //Primary Supplementary Line Changes
            //List of Suppline attached to primary line changes
            SelectPlanForSuppline selectPlanForSuppline = null;
            List<SupplinePlanStorage> supplinePlanStorageList;
            SupplinePlanStorage supplinePlanStorage = null;
            Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]);

            if (primarySupplimentLines)
            {
                supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : null;

                if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
                {
                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION 
                    selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString()];
                    ///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
                    if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
                        ///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
                        supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                    }
                }
            }

            try
            {

                if (numberType == "n")
                {
                    // **************** Dealer Number ******************
                    if (prefix == "-1")
                    {
                        prefix = string.Empty;
                    }
                    using (var proxy = new KenanServiceProxy())
                    {
                        mobileNos = proxy.MobileNoSelection(new MobileNoSelectionRequest()
                        {
                            DesireNo = desireNo,
                            MobileNoPrefix = prefix
                        });
                    }
                    // **************************************************

                    objMobileNos = mobileNos.MobileNos.ToList();
                }
                else
                {
                    var mobileNosResp = WebHelper.Instance.RetriveMobileNumbers(desireNo, numberType);

                    if (mobileNosResp.Status == false)
                    {
                        ModelState.AddModelError(string.Empty, mobileNosResp.Message);
                        return View(mobileNoVM);
                    }
                    objMobileNos = mobileNosResp.Data;
                }

                mobileNoVM = new MobileNoVM()
                {
                    //AvailableMobileNos = mobileNos.MobileNos.ToList(),
                    AvailableMobileNos = objMobileNos != null ? objMobileNos : new List<string>(),
                    NoOfDesireMobileNo = Properties.Settings.Default.NoOfDesireMobileNo
                };

                if (primarySupplimentLines)
                {
                    supplinePlanStorage.RegMobileReg_SublineSummary = ConstructOrderSummary(fromSubline: true);
                }
                else
                {
                    Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(true);
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            return View(mobileNoVM);
        }


        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
		public ActionResult SelectMobileNoNew(string desireNo = "", string numberType = "n", string prefix = "", bool validateAgentCode = false, string salesAgentCode = "")
        {
            var mobileNoVM = new MobileNoVM();
            var mobileNos = new MobileNoSelectionResponse();
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            List<string> objMobileNos = new List<string>();

            //Primary Supplementary Line Changes
            //List of Suppline attached to primary line changes
            SelectPlanForSuppline selectPlanForSuppline = null;
            List<SupplinePlanStorage> supplinePlanStorageList;
            SupplinePlanStorage supplinePlanStorage = null;
            Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]);

            if (primarySupplimentLines)
            {
                supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : null;

                if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
                {
                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION 
                    selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString()];
                    ///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
                    if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
                        ///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
                        supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                    }
                }
            }

            try
            {

                if (numberType == "n")
                {
                    // **************** Dealer Number ******************
                    if (prefix == "-1")
                    {
                        prefix = string.Empty;
                    }
                    using (var proxy = new KenanServiceProxy())
                    {
                        mobileNos = proxy.MobileNoSelection(new MobileNoSelectionRequest()
                        {
                            DesireNo = desireNo,
                            MobileNoPrefix = prefix
                        });
                    }
                    // **************************************************

                    // since Drop 4, got multi suppline, in mobile number selection should remove the selected number before.
                    var tempMobileNos = mobileNos.MobileNos.ToList();
                    var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                    if (!ReferenceEquals(dropObj, null))
                    {
                        if (!ReferenceEquals(dropObj.selectedMobileNoinFlow, null) && dropObj.selectedMobileNoinFlow.Count() > 0)
                        {
                            var newList = tempMobileNos.Except(dropObj.selectedMobileNoinFlow).ToList();
                            tempMobileNos.Clear();
                            tempMobileNos.AddRange(newList);
                        }
                    }

                    objMobileNos = tempMobileNos;

                }
                else
                {
					var mobileNosResp = WebHelper.Instance.RetriveMobileNumbers(desireNo, numberType, validateAgentCode, salesAgentCode);

                    if (mobileNosResp.Status == false)
                    {
                        if (Equals(mobileNosResp.Message, Constants.InfentoryNotExist))
                        {
                            mobileNosResp.Message = Constants.ReservedNotFound;
                        }
                        ViewBag.errReserve = mobileNosResp.Message;
                        ModelState.AddModelError(string.Empty, mobileNosResp.Message);
                        return View(mobileNoVM);
                    }
                    objMobileNos = mobileNosResp.Data;
                }

                mobileNoVM = new MobileNoVM()
                {
                    //AvailableMobileNos = mobileNos.MobileNos.ToList(),
                    AvailableMobileNos = objMobileNos != null ? objMobileNos : new List<string>(),
                    NoOfDesireMobileNo = Properties.Settings.Default.NoOfDesireMobileNo
                };

                if (primarySupplimentLines)
                {
                    supplinePlanStorage.RegMobileReg_SublineSummary = ConstructOrderSummary(fromSubline: true);
                }
                else
                {
                    Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(true);
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            return View(mobileNoVM);
        }



        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        public ActionResult SuppLinePlan(int? type, bool isBack = false)
        {
            if (!IsMNPWithMultpleLines)
            {
                Session["PrimarySim"] = null;
            }
            Session[SessionKey.SelectedComponents.ToString()] = null;

            // set session as plan
            Session["RegMobileReg_Type"] = (int)MobileRegType.PlanOnly;

            ///MNP MULTIPORT RELATED CHANGES            
            MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;
            ///INSTANCE WILL CONTAIN LIST OF SUPPLEMENTARY LINES HAVING DIFFERENT STATES SUCH AS 
            ///MNPSupplimentaryDetailsAddState.NOTINITIALIZED
            ///MNPSupplimentaryDetailsAddState.PROCESSING
            ///MNPSupplimentaryDetailsAddState.PROCESSED
            ///ONLY ONE MSISDN CAN BE IN PROCESSING STATE AT ANY POINT OF TIME
            MNPSelectPlanForSuppline SuppleMentaryLine = null;
            MNPSupplinePlanStorage MNPSupplinePlanStorage = null;

            //List of Suppline attached to primary line changes
            SelectPlanForSuppline selectPlanForSuppline = null;
            List<SupplinePlanStorage> supplinePlanStorageList;
            SupplinePlanStorage supplinePlanStorage = null;
            Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]);


            if (IsMNPWithMultpleLines)
            {
                ///WHILE IN MNP SUPPLINE MODE BACK BUTTON SHOULD REDIRECT THE USER TO MNPSelectSuppLine PAGE
                ViewBag.setBackValue = "MNPSelectSuppLine";
            }
            if (type > 0)
            {
                //Changed by ravi in order to maintain persistance on 13/07/2013
                PersonalDetailsVM personalDetailsVM = null;
                if (Session["RegMobileReg_PersonalDetailsVM"] != null)
                {
                    personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
                }
                if (!primarySupplimentLines)
                {
                    //Changed by ravi in order to maintain persistance on 13/07/2013 Ends here
                    ResetAllSession(type);
                    //Changed by ravi in order to maintain persistance on 13/07/2013
                }
                if (type == 8)
                {
                    Session["RegMobileReg_PersonalDetailsVM"] = null;
                    Session["ExtratenMobilenumbers"] = null;
                }
                Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;
                //Changed by ravi in order to maintain persistance on 13/07/2013 Ends here
            }

            if (primarySupplimentLines)
            {
                if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString2()], null))
                {
                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION 
                    selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString2()];

                    ///CHECK IF THERE ARE ANY IN PROCESS MSISDN'S
                    if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        //  ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
                        ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
                    }
                }

                ///CHECK WHETHER THE ProcessingMsisdn CONTAINS A SUPPLEMENTARY NUMBER
                if (!string.IsNullOrWhiteSpace(ProcessingMsisdn))
                {
                    ///CHECK IF SupplinePlanStorageList IS NULL THEN
                    ///SupplinePlanStorageList SUPPLEMENTARY LINE LIST IS NOT BLANK
                    ///ASSIGN A NEW OBJECT. THIS MIGHT HAPPEN FOR THE FIRST TIME

                    supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : new List<SupplinePlanStorage>();


                    ///CHECK IF MATCHING SUPPLEMENTARY MSISDN IS ALREADY ASSOCIATED WITH ANY OBJECT 
                    if (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().Count > 0)
                    {
                        ///GET IT IF EXISTS
                        // supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                        supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                        ///REMOVE THE ITEM FROM LIST
                        supplinePlanStorageList.Remove(supplinePlanStorage);
                    }

                    ///NOW ASSIGN THE SUPPLEMENTARY MSISDN TO MNPSupplinePlanStorage
                    supplinePlanStorage = new ViewModels.SupplinePlanStorage()
                    {
                        // RegMobileReg_MSISDN = ProcessingMsisdn
                        RegMobileReg_ProcessingId = ProcessingMsisdn
                    };
                    supplinePlanStorageList.Add(supplinePlanStorage);
                    // Session.Remove("supplinePlanStorageList");
                    Session["supplinePlanStorageList"] = supplinePlanStorageList;
                }
            }



            if (IsMNPWithMultpleLines)
            {
                ///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
                MNPPrimaryPlanStorage = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];

                if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString2()], null))
                {
                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
                    SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString2()];

                    ///CHECK IF THERE ARE ANY IN PROCESS MSISDN'S
                    if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        ProcessingMsisdn = SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
                    }
                }

                ///CHECK WHETHER THE ProcessingMsisdn CONTAINS A SUPPLEMENTARY NUMBER, MNPPrimaryPlanStorage THE MAIN PLAN STORAGE UNDER WHICH SUPPLEMENTARY MSISDN'S ARE GOING TO BE ADDED IS NOT NULL
                if (!string.IsNullOrWhiteSpace(ProcessingMsisdn) && (!ReferenceEquals(MNPPrimaryPlanStorage, null)))
                {
                    ///CHECK IF MNPPrimaryPlanStorage.MNPSupplinePlanStorageList IS NULL THEN
                    ///MNPSupplinePlanStorageList SUPPLEMENTARY LINE LIST UNDER MNPPrimaryPlanStorage IS NOT BLANK
                    ///ASSIGN A NEW OBJECT. THIS MIGHT HAPPEN FOR THE FIRST TIME
                    if (ReferenceEquals(MNPPrimaryPlanStorage.MNPSupplinePlanStorageList, null))
                    {
                        MNPPrimaryPlanStorage.MNPSupplinePlanStorageList = new List<MNPSupplinePlanStorage>();
                    }
                    else
                    {
                        ///CHECK IF MATCHING SUPPLEMENTARY MSISDN IS ALREADY ASSOCIATED WITH ANY OBJECT UNDER MNPPrimaryPlanStorage
                        if (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().Count > 0)
                        {
                            ///GET IT IF EXISTS
                            MNPSupplinePlanStorage = (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                            ///REMOVE THE ITEM FROM LIST
                            MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Remove(MNPSupplinePlanStorage);
                        }
                    }
                    ///NOW ASSIGN THE SUPPLEMENTARY MSISDN TO MNPSupplinePlanStorage
                    MNPSupplinePlanStorage = new ViewModels.MNPSupplinePlanStorage()
                    {
                        RegMobileReg_MSISDN = ProcessingMsisdn
                    };
                    MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Add(MNPSupplinePlanStorage);
                }
            }

            ResetDepositSessions();
            //Spend Limit Commented by Ravi As per New Flow on June 15 2013
            //Spend Limit
            //Util.SpendLimit = string.Empty;
            //Session[SessionKey.SpendLimit.ToString()] = null;
            Session["VasGroupIds"] = null;
            //Spend Limit

            if (!isBack)
            {
                ///IF FALSE THEN CLEAR SUBLINE SESSIONS
                WebHelper.Instance.ClearSubLineSession();
            }
            #region Added by Chetan for hiding the contrats & vas in planonly
            if (type == 8)
            {
                Session["MobileRegType"] = "Planonly";
                ///MNP MULTIPORT RELATED CHANGES
                if (IsMNPWithMultpleLines)
                {
                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).MobileRegType = "Planonly";
                }
            }
            else
            {
                Session["MobileRegType"] = "Other";
                ///MNP MULTIPORT RELATED CHANGES
                if (IsMNPWithMultpleLines)
                {
                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).MobileRegType = "Other";
                }
            }
            #endregion
            if (IsMNPWithMultpleLines)
            {

                MNPSupplinePlanStorage.RegMobileReg_SublineSummary = ConstructOrderSummary(fromSubline: true);
            }
            else if (primarySupplimentLines)
            {
                supplinePlanStorage.RegMobileReg_SublineSummary = ConstructOrderSummary(fromSubline: true);
            }
            else
            {
                Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(fromSubline: true);
            }
            var sublinePkgVM = new SublinePackageVM();
            sublinePkgVM.PackageVM = GetAvailablePackages(true);


            ///MNP MULTIPORT RELATED CHANGES
            if (IsMNPWithMultpleLines)
            {
                sublinePkgVM.SelectedMobileNumber = ReferenceEquals(MNPSupplinePlanStorage.RegMobileSub_MobileNo, null) ? new List<string>() : (List<string>)MNPSupplinePlanStorage.RegMobileSub_MobileNo;
            }
            else if (primarySupplimentLines)
            {
                sublinePkgVM.SelectedMobileNumber = ReferenceEquals(supplinePlanStorage.RegMobileSub_MobileNo, null) ? new List<string>() : (List<string>)supplinePlanStorage.RegMobileSub_MobileNo;

            }
            else
            {
                sublinePkgVM.SelectedMobileNumber = Session["RegMobileSub_MobileNo"] == null ? new List<string>() : (List<string>)Session["RegMobileSub_MobileNo"];
            }

            sublinePkgVM.NoOfDesireMobileNo = Properties.Settings.Default.NoOfDesireMobileNo;

            PersonalDetailsVM objCurDetails = new PersonalDetailsVM();

            sublinePkgVM.PersonalDetailsVM = objCurDetails;

            return View(sublinePkgVM);
        }

        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        public ActionResult SuppLinePlanNew(int? type, bool isBack = false)
        {
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            //clear selected plan for 1st land on page
            //Session["SelectedPlanID"] = null;
            Session["OrderType"] = (int)MobileRegType.SuppPlan;
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);

            dropObj.currentFlow = DropFourConstant.SUPP_FLOW;
            if (dropObj.suppIndex == 0)
            {
                dropObj.suppIndex = dropObj.suppIndex + 1;
                if (dropObj.suppIndex > dropObj.suppFlow.Count)
                {
                    dropObj.suppFlow.Add(new Flow());
                }
            }
            else
            {
                // it's set to true in mobile number selection Registration / New Line
                if (dropObj.suppFlow[dropObj.suppIndex - 1]._flowCompleted == true)
                {
                    dropObj.suppIndex = dropObj.suppIndex + 1;
                    if (dropObj.suppIndex > dropObj.suppFlow.Count)
                    {
                        dropObj.suppFlow.Add(new Flow());
                    }
                }
            }

            dropObj.suppFlow[dropObj.suppIndex - 1].RegType = (int)MobileRegType.PlanOnly;
            Session["RegMobileReg_Type"] = (int)MobileRegType.PlanOnly;
            Session["RegMobileReg_OrderSummary"] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session["RegMobileReg_SublineSummary"] = null;
            Session["RegMobileReg_VasIDs"] = null;
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
			Session["selectedContractLength"] = null;
			Session[SessionKey.MSISDN.ToString()] = string.Empty;

            if (!ReferenceEquals(Session[SessionKey.MNPFlowType.ToString()], null))
            {
                if (Session[SessionKey.MNPFlowType.ToString()].ToString() == "ASL")
                {
                    MobileNoVM mobile = new MobileNoVM();
                    mobile.SelectedMobileNo = Session[SessionKey.RegMobileReg_MSISDN.ToString()].ToString();

                    dropObj.suppFlow[0].mobile = mobile;
                }
            }

            if (!IsMNPWithMultpleLines)
            {
                Session["PrimarySim"] = null;
            }
            Session[SessionKey.SelectedComponents.ToString()] = null;

            ///MNP MULTIPORT RELATED CHANGES            
            MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;
            ///INSTANCE WILL CONTAIN LIST OF SUPPLEMENTARY LINES HAVING DIFFERENT STATES SUCH AS 
            ///MNPSupplimentaryDetailsAddState.NOTINITIALIZED
            ///MNPSupplimentaryDetailsAddState.PROCESSING
            ///MNPSupplimentaryDetailsAddState.PROCESSED
            ///ONLY ONE MSISDN CAN BE IN PROCESSING STATE AT ANY POINT OF TIME
            MNPSelectPlanForSuppline SuppleMentaryLine = null;
            MNPSupplinePlanStorage MNPSupplinePlanStorage = null;

            //List of Suppline attached to primary line changes
            SelectPlanForSuppline selectPlanForSuppline = null;
            List<SupplinePlanStorage> supplinePlanStorageList;
            SupplinePlanStorage supplinePlanStorage = null;
            Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]);


            if (IsMNPWithMultpleLines)
            {
                ///WHILE IN MNP SUPPLINE MODE BACK BUTTON SHOULD REDIRECT THE USER TO MNPSelectSuppLine PAGE
                ViewBag.setBackValue = "MNPSelectSuppLine";
            }
            if (type > 0)
            {
                //Changed by ravi in order to maintain persistance on 13/07/2013
                PersonalDetailsVM personalDetailsVM = null;
                if (Session["RegMobileReg_PersonalDetailsVM"] != null)
                {
                    personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
                }
                if (!primarySupplimentLines)
                {
                    //Changed by ravi in order to maintain persistance on 13/07/2013 Ends here
                    ResetAllSession(type);
                    //Changed by ravi in order to maintain persistance on 13/07/2013
                }
                if (type == 8)
                {
                    Session["RegMobileReg_PersonalDetailsVM"] = null;
                    Session["ExtratenMobilenumbers"] = null;
                }
                Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;
                //Changed by ravi in order to maintain persistance on 13/07/2013 Ends here
            }

            if (primarySupplimentLines)
            {
                if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString2()], null))
                {
                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION 
                    selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString2()];

                    ///CHECK IF THERE ARE ANY IN PROCESS MSISDN'S
                    if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        //  ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
                        ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
                    }
                }

                ///CHECK WHETHER THE ProcessingMsisdn CONTAINS A SUPPLEMENTARY NUMBER
                if (!string.IsNullOrWhiteSpace(ProcessingMsisdn))
                {
                    ///CHECK IF SupplinePlanStorageList IS NULL THEN
                    ///SupplinePlanStorageList SUPPLEMENTARY LINE LIST IS NOT BLANK
                    ///ASSIGN A NEW OBJECT. THIS MIGHT HAPPEN FOR THE FIRST TIME

                    supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : new List<SupplinePlanStorage>();


                    ///CHECK IF MATCHING SUPPLEMENTARY MSISDN IS ALREADY ASSOCIATED WITH ANY OBJECT 
                    if (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().Count > 0)
                    {
                        ///GET IT IF EXISTS
                        // supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                        supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                        ///REMOVE THE ITEM FROM LIST
                        supplinePlanStorageList.Remove(supplinePlanStorage);
                    }

                    ///NOW ASSIGN THE SUPPLEMENTARY MSISDN TO MNPSupplinePlanStorage
                    supplinePlanStorage = new ViewModels.SupplinePlanStorage()
                    {
                        // RegMobileReg_MSISDN = ProcessingMsisdn
                        RegMobileReg_ProcessingId = ProcessingMsisdn
                    };
                    supplinePlanStorageList.Add(supplinePlanStorage);
                    // Session.Remove("supplinePlanStorageList");
                    Session["supplinePlanStorageList"] = supplinePlanStorageList;
                }
            }



            if (IsMNPWithMultpleLines)
            {
                ///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
                MNPPrimaryPlanStorage = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];

                if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString2()], null))
                {
                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
                    SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString2()];

                    ///CHECK IF THERE ARE ANY IN PROCESS MSISDN'S
                    if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        ProcessingMsisdn = SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
                    }
                }

                ///CHECK WHETHER THE ProcessingMsisdn CONTAINS A SUPPLEMENTARY NUMBER, MNPPrimaryPlanStorage THE MAIN PLAN STORAGE UNDER WHICH SUPPLEMENTARY MSISDN'S ARE GOING TO BE ADDED IS NOT NULL
                if (!string.IsNullOrWhiteSpace(ProcessingMsisdn) && (!ReferenceEquals(MNPPrimaryPlanStorage, null)))
                {
                    ///CHECK IF MNPPrimaryPlanStorage.MNPSupplinePlanStorageList IS NULL THEN
                    ///MNPSupplinePlanStorageList SUPPLEMENTARY LINE LIST UNDER MNPPrimaryPlanStorage IS NOT BLANK
                    ///ASSIGN A NEW OBJECT. THIS MIGHT HAPPEN FOR THE FIRST TIME
                    if (ReferenceEquals(MNPPrimaryPlanStorage.MNPSupplinePlanStorageList, null))
                    {
                        MNPPrimaryPlanStorage.MNPSupplinePlanStorageList = new List<MNPSupplinePlanStorage>();
                    }
                    else
                    {
                        ///CHECK IF MATCHING SUPPLEMENTARY MSISDN IS ALREADY ASSOCIATED WITH ANY OBJECT UNDER MNPPrimaryPlanStorage
                        if (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().Count > 0)
                        {
                            ///GET IT IF EXISTS
                            MNPSupplinePlanStorage = (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                            ///REMOVE THE ITEM FROM LIST
                            MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Remove(MNPSupplinePlanStorage);
                        }
                    }
                    ///NOW ASSIGN THE SUPPLEMENTARY MSISDN TO MNPSupplinePlanStorage
                    MNPSupplinePlanStorage = new ViewModels.MNPSupplinePlanStorage()
                    {
                        RegMobileReg_MSISDN = ProcessingMsisdn
                    };
                    MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Add(MNPSupplinePlanStorage);
                }
            }

            ResetDepositSessions();
            //Spend Limit Commented by Ravi As per New Flow on June 15 2013
            //Spend Limit
            //Util.SpendLimit = string.Empty;
            //Session[SessionKey.SpendLimit.ToString()] = null;
            Session["VasGroupIds"] = null;
            //Spend Limit

            if (!isBack)
            {
                ///IF FALSE THEN CLEAR SUBLINE SESSIONS
                WebHelper.Instance.ClearSubLineSession();
            }
            #region Added by Chetan for hiding the contrats & vas in planonly
            if (type == 8)
            {
                Session["MobileRegType"] = "Planonly";
                ///MNP MULTIPORT RELATED CHANGES
                if (IsMNPWithMultpleLines)
                {
                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).MobileRegType = "Planonly";
                }
            }
            else
            {
                Session["MobileRegType"] = "Other";
                ///MNP MULTIPORT RELATED CHANGES
                if (IsMNPWithMultpleLines)
                {
                    ((MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"]).MobileRegType = "Other";
                }
            }
            #endregion
            if (IsMNPWithMultpleLines)
            {

                MNPSupplinePlanStorage.RegMobileReg_SublineSummary = ConstructOrderSummary(fromSubline: true);
            }
            else if (primarySupplimentLines)
            {
                supplinePlanStorage.RegMobileReg_SublineSummary = ConstructOrderSummary(fromSubline: true);
            }
            else
            {
                Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(fromSubline: true);
            }
            var sublinePkgVM = new SublinePackageVM();
            sublinePkgVM.PackageVM = GetAvailablePackages(true);

			if (sharingQuotaFeature)
			{
				sublinePkgVM.PackageVM = WebHelper.Instance.checkPrinSuppPlanEligibility(sublinePkgVM.PackageVM, dropObj);
			}

            ///MNP MULTIPORT RELATED CHANGES
            if (IsMNPWithMultpleLines)
            {
                sublinePkgVM.SelectedMobileNumber = ReferenceEquals(MNPSupplinePlanStorage.RegMobileSub_MobileNo, null) ? new List<string>() : (List<string>)MNPSupplinePlanStorage.RegMobileSub_MobileNo;
            }
            else if (primarySupplimentLines)
            {
                sublinePkgVM.SelectedMobileNumber = ReferenceEquals(supplinePlanStorage.RegMobileSub_MobileNo, null) ? new List<string>() : (List<string>)supplinePlanStorage.RegMobileSub_MobileNo;

            }
            else
            {
                sublinePkgVM.SelectedMobileNumber = Session["RegMobileSub_MobileNo"] == null ? new List<string>() : (List<string>)Session["RegMobileSub_MobileNo"];
            }

            sublinePkgVM.NoOfDesireMobileNo = Properties.Settings.Default.NoOfDesireMobileNo;

            PersonalDetailsVM objCurDetails = new PersonalDetailsVM();

            sublinePkgVM.PersonalDetailsVM = objCurDetails;

            //Anthony: New IC (search id type: 1) and Other IC (search id type: 3) will be tagged as Malaysian
            /*
            if (Session["SearchBy"].ToString2().ToUpper() == "NRIC" || Session["SearchBy"].ToString2().ToUpper() == "NEWIC" || Session["SearchBy"].ToString2().ToUpper() == "OTHERIC")
            {
                if (!ReferenceEquals(dropObj.personalInformation, null))
                    dropObj.personalInformation.Customer.NationalityID = 1;
            }
            else 
            */
            if (Session[SessionKey.IDCardTypeID.ToString()].ToInt() != 2)
            {//other than passport, the nationality id = 1
                if (!ReferenceEquals(dropObj.personalInformation, null))
                    dropObj.personalInformation.Customer.NationalityID = 1;
            }
            return View(sublinePkgVM);
        }


        public string ClearSpendlimitSession()
        {
            Session[SessionKey.SpendLimit.ToString()] = null;
            return string.Empty;
        }

        /// <summary>        
        /// 1.Extracts the file information passed from Request collection. 
        /// 2.Uploads the actual image and resized image in temporary location.
        /// 3.Returns the image in base 64 encoded format.
        /// </summary>
        /// <Author>Sutan Dan</Author>
        /// <returns>TypeOf(Json)</returns>
        [HttpPost]
        public ActionResult UploadImage()
        {
            ///Get the upload and resize locations from settings
            ///PLEASE CREATE 'Uploads' AND 'Resized' FOLDERS IN THE PROJECT
            ///FOR EXAMPLE Online.Registration.Web/Uploads AND Online.Registration.Web/Resized
            string imageUploadLocation = Server.MapPath(Properties.Settings.Default.imageUploadLocation);
            string imageResizeLocation = Server.MapPath(Properties.Settings.Default.imageResizeLocation);
            string fitImage = Properties.Settings.Default.fitImage;

            bool isResized = false;

            string SourceImageName, ActualImageName, onlyFileExtension, imageUrl, imageType = string.Empty;

            System.Drawing.Image srcimage = null;
            imageUrl = string.Empty;

            ///CHECKS WHETHER THE Request.Files
            if (!ReferenceEquals(Request.Files, null) && Request.Files.Count > 0)
            {
                ///RETRIEVES THE FIRST KEY IN CASE THERE WOULD BE MULTIPLE FOUND.
                string PostedFileName = Request.Files.AllKeys.FirstOrDefault();
                ///TYPECASTING TO HttpPostedFileBase
                HttpPostedFileBase hpf = Request.Files[PostedFileName] as HttpPostedFileBase;

                ///ONLY PROCESS IF LENGTH >0
                if (hpf.ContentLength > 0)
                {
					//Keep the actual imagename for future reference
					ActualImageName = SourceImageName = Path.GetFileName(hpf.FileName);
					imageType = hpf.ContentType;
					///Check whether there is already an image in the folder with the same name
					Util.checkFileExistance(imageUploadLocation, ref SourceImageName, out onlyFileExtension);
					imageUploadLocation = Path.Combine(imageUploadLocation + @"\", SourceImageName);
					///Save the image
					hpf.SaveAs(imageUploadLocation);

					srcimage = System.Drawing.Image.FromFile(imageUploadLocation);
					///CALL TO CHECK AND LOWER THE RESOLUTION OF HIGH RESOULTION IMAGE
					//isResized = Util.ResizeImage(srcimage, 73, imageResizeLocation + SourceImageName, 540, 365, fitImage);
					isResized = Util.AlwaysResizeImage(srcimage, 23, imageResizeLocation + SourceImageName, 540, 365, fitImage);

					///Return the processed or unprocessed image as url
					if (isResized)
					{
						imageResizeLocation = Path.Combine(imageResizeLocation + @"\", SourceImageName);
						imageUrl = string.Format(@"data:{0};base64,", imageType) + Convert.ToBase64String(System.IO.File.ReadAllBytes(imageResizeLocation));
					}
					else
					{
						imageUrl = string.Format(@"data:{0};base64,", imageType) + Convert.ToBase64String(System.IO.File.ReadAllBytes(imageUploadLocation));
					}
                }
            }
            ///RETURNS THE IMAGE IN BASE 64 ENCODED FORMAT
            var jsonData = new { imageUrl = imageUrl };

            var serializer = new JavaScriptSerializer();

            serializer.MaxJsonLength = Int32.MaxValue;

            var data = new ContentResult
            {
                Content = serializer.Serialize(jsonData),
                ContentType = "application/json"
            };

            return data;
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        public ActionResult SelectMobileNo(FormCollection collection, MobileNoVM mobileNoVM)
        {
            //List of Suppline attached to primary line changes
            SelectPlanForSuppline selectPlanForSuppline = null;
            List<SupplinePlanStorage> supplinePlanStorageList;
            SupplinePlanStorage supplinePlanStorage = null;
            Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]);

            if (primarySupplimentLines)
            {
                supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : null;

                if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
                {
                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION 
                    selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString()];
                    ///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
                    if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
                        ///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
                        supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                    }
                }
            }

            try
            {
                //Added by ravi on Apr 10 2014 for by passing mobile no update 
                Session[SessionKey.NumberType.ToString2()] = collection["hdnNumberType"].ToString2();
                //Added by ravi on Apr 10 2014 for by passing mobile no update  ends here

                if (primarySupplimentLines)
                {
                    supplinePlanStorage.ExtratenMobilenumbers = mobileNoVM.ExtraTenMobileNumbers;
                }
                else
                {
                    Session["ExtratenMobilenumbers"] = mobileNoVM.ExtraTenMobileNumbers;
                }
                if (collection["submit1"].ToString2() == "subline")
                {
                    WebHelper.Instance.ClearSubLineSession();
                    return RedirectToAction("SubLinePlan");
                }
                else if (collection["submit1"].ToString2() == "search")
                {
                    return RedirectToAction("SelectMobileNo", new { desireNo = mobileNoVM.DesireNo, numberType = collection["hdnNumberType"].ToString2(), prefix = collection["ddlPrefix"].ToString2() });
                }
                else if (collection["submit1"].ToInt() > 0)
                {
                    var squeenceNo = collection["submit1"].ToInt();
                    var subLines = (List<SublineVM>)Session["RegMobileReg_SublineVM"];

                    if (primarySupplimentLines)
                    {
                        subLines = supplinePlanStorage.RegMobileReg_SublineVM;
                    }

                    var removeItem = subLines.Where(a => a.SequenceNo == squeenceNo).SingleOrDefault();
                    subLines.Remove(removeItem);

                    for (int i = 1; i <= subLines.Count(); i++)
                    {
                        subLines[i - 1].SequenceNo = i;
                    }

                    Session["RegMobileReg_SublineVM"] = subLines;

                    using (var proxy = new UserServiceProxy())
                    {
                        if (Session["IsDeviceRequired"].ToString2() == "Yes")
                        {
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Select Mobile No", Session["KenanACNumber"].ToString2(), "");
                        }
                        else
                        {
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SuppPlan, "Select Mobile No", Session["KenanACNumber"].ToString2(), "");
                        }
                    }
                    if (primarySupplimentLines)
                    {
                        //return RedirectToAction("SelectPrimarySuppLine", "Registration");
                        return RedirectToAction("IsSupplimentaryRequired", "Registration");
                    }

                    return RedirectToAction(GetRegActionStep((int)Session["RegMobileReg_TabIndex"]));
                }
                else
                {

                    if (primarySupplimentLines)
                    {
                        supplinePlanStorage.RegMobileReg_TabIndex = mobileNoVM.TabNumber;
                    }
                    else
                    {
                        Session["RegMobileReg_TabIndex"] = mobileNoVM.TabNumber;
                    }
                    var mobileNos = new List<string>();
                    if (mobileNoVM != null && mobileNoVM.SelectedMobileNo != null)
                    {
                        if (!mobileNoVM.SelectedMobileNo.StartsWith("60"))
                        {
                            mobileNoVM.SelectedMobileNo = "60" + mobileNoVM.SelectedMobileNo;
                        }
                        mobileNos.Add(mobileNoVM.SelectedMobileNo);
                    }
                    //Changes for single primary and multiple supplimentary line
                    if ((int)MobileRegistrationSteps.PersonalDetails == mobileNoVM.TabNumber && !primarySupplimentLines)
                    {
                        Session["RegMobileReg_MobileNo"] = mobileNos;
                    }
                    if (primarySupplimentLines)
                    {
                        supplinePlanStorage.RegMobileReg_MobileNo = mobileNos;
                        supplinePlanStorage.RegMobileReg_MSISDN = mobileNoVM.SelectedMobileNo;

                        selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn = supplinePlanStorage.RegMobileReg_MSISDN;
                        ///UPDATE THE STATUS WITH PROCESSED
                        selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Status = PrimarySupplimentaryDetailsAddState.PROCESSED.ToInt();

                    }
                }
                if (mobileNoVM != null && mobileNoVM.SelectedMobileNo != null)
                {
                    /*For checking the MSISDN whether valid or not valid by chetan*/
                    bool checkval = false;
                    using (var proxy = new KenanServiceProxy())
                    {
                        checkval = proxy.validateInventoryExtIdService(new TypeValidateInventoryExtIdRequest { externalId = mobileNoVM.SelectedMobileNo });
                        //checkval = false;
                    }
                    if (checkval && primarySupplimentLines)
                    {
                        // return RedirectToAction("SelectPrimarySuppLine", "Registration");
                        return RedirectToAction("IsSupplimentaryRequired", "Registration");
                    }
                    else if (checkval)
                    {
                        return RedirectToAction(GetRegActionStep(mobileNoVM.TabNumber));
                    }
                    else
                    {
                        return RedirectToAction("SelectMobileNo", new { invchk = "False" });
                    }
                }
                else if (primarySupplimentLines)
                {
                    // return RedirectToAction("SelectPrimarySuppLine", "Registration");
                    return RedirectToAction("IsSupplimentaryRequired", "Registration");
                }
                else
                {
                    return RedirectToAction(GetRegActionStep(mobileNoVM.TabNumber));
                }

            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

        }


        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        public ActionResult SelectMobileNoNew(FormCollection collection, MobileNoVM mobileNoVM)
        {
            //RST - identify back button
            var flow_hiden = collection["flowHidden"].ToString2();
            var flowID = string.Empty;
            if (flow_hiden.Contains(';'))
            {
                var isBackButton = collection["flowHidden"].ToString2().Split(';');
                flowID = isBackButton[1];
                if (flowID == "backbutton")
                {
                    return RedirectToAction("SuppLineVASNew", "SuppLine");
                }
            }
            else
            {
                flowID = flow_hiden;
            }
            //end RST

            bool isDedicatedSuppFlow = Session[SessionKey.DedicatedSuppFlow.ToString()].ToBool();
            bool newLineSupplementary = Session[SessionKey.NewLineSupplementary.ToString()].ToBool();
            ActionResult redirectPage = null;
            string selectedNumber = string.Empty;
            selectedNumber = mobileNoVM.SelectedMobileNo;

            //List of Suppline attached to primary line changes
            SelectPlanForSuppline selectPlanForSuppline = null;
            List<SupplinePlanStorage> supplinePlanStorageList;
            SupplinePlanStorage supplinePlanStorage = null;
            Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]);

            if (primarySupplimentLines)
            {
                supplinePlanStorageList = (Session["supplinePlanStorageList"] != null) ? ((List<SupplinePlanStorage>)Session["supplinePlanStorageList"]) : null;

                if (!ReferenceEquals(Session[SessionKey.PrimarySupplimentaryLines.ToString()], null))
                {
                    ///GET ALL SUPPLEMENTARY LINES STORED IN SESSION 
                    selectPlanForSuppline = (SelectPlanForSuppline)Session[SessionKey.PrimarySupplimentaryLines.ToString()];
                    ///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
                    if (selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
                    {
                        ProcessingMsisdn = selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().ProcessingId;
                        ///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
                        supplinePlanStorage = (supplinePlanStorageList.Where(i => i.RegMobileReg_ProcessingId.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
                    }
                }
            }

            try
            {
                //Added by ravi on Apr 10 2014 for by passing mobile no update 
                Session[SessionKey.NumberType.ToString2()] = collection["hdnNumberType"].ToString2();
                //Added by ravi on Apr 10 2014 for by passing mobile no update  ends here
                String simSize = collection["SimSize"].ToString2();
                simSize = collection["SimSize"].ToString2();
                if (primarySupplimentLines)
                {
                    supplinePlanStorage.ExtratenMobilenumbers = mobileNoVM.ExtraTenMobileNumbers;
                }
                else
                {
                    Session["ExtratenMobilenumbers"] = mobileNoVM.ExtraTenMobileNumbers;
                }
                if (collection["submit1"].ToString2() == "subline")
                {
                    WebHelper.Instance.ClearSubLineSession();
                    redirectPage = RedirectToAction("SubLinePlan");
                }
                else if (collection["submit1"].ToString2() == "search")
                {
                    //redirectPage = RedirectToAction("SelectMobileNoNew", new { desireNo = mobileNoVM.DesireNo, numberType = collection["hdnNumberType"].ToString2(), prefix = collection["ddlPrefix"].ToString2() });
					redirectPage = RedirectToAction("SelectMobileNoNew", new
					{
						desireNo = mobileNoVM.DesireNo,
						numberType = collection["hdnNumberType"].ToString2(),
						prefix = collection["ddlPrefix"].ToString2(),
						validateAgentCode = collection["validateAgentCode"].ToBool(),
						salesAgentCode = collection["salesAgentCode"].ToString2()
					});
				}
                else if (collection["submit1"].ToInt() > 0)
                {
                    var squeenceNo = collection["submit1"].ToInt();
                    var subLines = (List<SublineVM>)Session["RegMobileReg_SublineVM"];

                    if (primarySupplimentLines)
                    {
                        subLines = supplinePlanStorage.RegMobileReg_SublineVM;
                    }

                    var removeItem = subLines.Where(a => a.SequenceNo == squeenceNo).SingleOrDefault();
                    subLines.Remove(removeItem);

                    for (int i = 1; i <= subLines.Count(); i++)
                    {
                        subLines[i - 1].SequenceNo = i;
                    }

                    Session["RegMobileReg_SublineVM"] = subLines;

                    using (var proxy = new UserServiceProxy())
                    {
                        if (Session["IsDeviceRequired"].ToString2() == "Yes")
                        {
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Select Mobile No", Session["KenanACNumber"].ToString2(), "");
                        }
                        else
                        {
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SuppPlan, "Select Mobile No", Session["KenanACNumber"].ToString2(), "");
                        }
                    }
                    if (primarySupplimentLines)
                    {
                        //return RedirectToAction("SelectPrimarySuppLine", "Registration");
                        redirectPage = RedirectToAction("IsSupplimentaryRequired", "Registration");
                    }

                    redirectPage = RedirectToAction(GetRegActionStep((int)Session["RegMobileReg_TabIndex"]));
                }
                else
                {

                    if (primarySupplimentLines)
                    {
                        supplinePlanStorage.RegMobileReg_TabIndex = mobileNoVM.TabNumber;
                    }
                    else
                    {
                        Session["RegMobileReg_TabIndex"] = mobileNoVM.TabNumber;
                    }
                    var mobileNos = new List<string>();
                    if (mobileNoVM != null && mobileNoVM.SelectedMobileNo != null)
                    {
                        mobileNoVM.SelectedMobileNo = Util.FormatContactNumber(mobileNoVM.SelectedMobileNo);
                        mobileNos.Add(mobileNoVM.SelectedMobileNo);
                    }

                    mobileNoVM.SelectedMobileNumber = mobileNos;

                    //Changes for single primary and multiple supplimentary line
                    if ((int)MobileRegistrationSteps.PersonalDetails == mobileNoVM.TabNumber && !primarySupplimentLines)
                    {
                        Session["RegMobileReg_MobileNo"] = mobileNos;
                    }
                    if (primarySupplimentLines)
                    {
                        supplinePlanStorage.RegMobileReg_MobileNo = mobileNos;
                        supplinePlanStorage.RegMobileReg_MSISDN = mobileNoVM.SelectedMobileNo;

                        selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn = supplinePlanStorage.RegMobileReg_MSISDN;
                        ///UPDATE THE STATUS WITH PROCESSED
                        selectPlanForSuppline.PrimarySupplimentaryMSISDNs.Where(i => i.Status == PrimarySupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Status = PrimarySupplimentaryDetailsAddState.PROCESSED.ToInt();

                    }
                }
                var msismType = collection["MsismType"];

                if (!string.IsNullOrEmpty(msismType))
                {
                    Session["SimType"] = "MISM";
                    mobileNoVM.SIMCardType = "MISM";
                }
                else
                {
                    Session["SimType"] = "Normal";
                    mobileNoVM.SIMCardType = "Normal";
                }

                var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

                if (!dropObj.selectedMobileNoinFlow.Contains(selectedNumber))
                    dropObj.selectedMobileNoinFlow.Add(selectedNumber);

                mobileNoVM.NumberType = collection["hdnNumberType"].ToString2();//20032015 - Anthony - Fix for reserved number type is null

                DropFourHelpers.UpdateMobile(dropObj, mobileNoVM);
                DropFourHelpers.SimSize(dropObj, simSize);

                if (!isDedicatedSuppFlow)
                {

                    if (!string.IsNullOrEmpty(msismType))
                    {
                        dropObj.mainFlow.mobile.SIMCardType = "MISM";

                        if (dropObj.msimFlow.Count() == 0)
                        {
                            dropObj.msimIndex = dropObj.msimIndex + 1;
                            dropObj.msimFlow.Add(new Flow());
                            dropObj.msimFlow[0].simSize = msismType;
                        }
                    }
                    else
                    {
                        dropObj.msimIndex = 0;
                        dropObj.msimFlow = new List<Flow>();
                        dropObj.mainFlow.mobile.SIMCardType = "Normal";
                    }
                }
                //var flowID = collection["flowHidden"].ToString2();
                if (flowID != "None")
                {
                    switch (flowID)
                    {
                        case "PlanNDevice":
                            Session["IsNewBillingAddress"] = "ExistingBillingAddress";
                            Session[SessionKey.IsSuppleNewAccount.ToString()] = false;
                            Session["IsDeviceRequired"] = "Yes";
                            //clear selected plan
                            Session["SelectedPlanID"] = null;
                            // Gry - move to supp controller
                            //dropObj.currentFlow = DropFourConstant.SUPP_FLOW;
                            //dropObj.suppIndex = dropObj.suppIndex + 1;
                            //dropObj.suppFlow.Add(new Flow());
                            //dropObj.suppFlow[dropObj.suppIndex - 1].RegType = (int)MobileRegType.DevicePlan;
                            //redirectPage = RedirectToAction("SelectDevicePlan", "Registration");
                            if (dropObj.suppIndex > 0)
                                dropObj.suppFlow[dropObj.suppIndex - 1]._flowCompleted = true;

                            redirectPage = RedirectToAction("SelectDevicePlan", "SuppLine", new { type = 1 });
                            break;
                        case "Plan":
                            Session["IsNewBillingAddress"] = "ExistingBillingAddress";
                            Session[SessionKey.IsSuppleNewAccount.ToString()] = false;
                            Session["IsDeviceRequired"] = "No";
                            //clear selected plan
                            Session["SelectedPlanID"] = null;
                            // Gry - move to supp controller
                            //dropObj.currentFlow = DropFourConstant.SUPP_FLOW;
                            //dropObj.suppIndex = dropObj.suppIndex + 1;
                            //dropObj.suppFlow.Add(new Flow());
                            //dropObj.suppFlow[dropObj.suppIndex - 1].RegType = (int)MobileRegType.PlanOnly;
                            //redirectPage = RedirectToAction("SelectPlanNew", "Registration");
                            if (dropObj.suppIndex > 0)
                                dropObj.suppFlow[dropObj.suppIndex - 1]._flowCompleted = true;
                            redirectPage = RedirectToAction("SuppLinePlanNew", "SuppLine", new { type = 8 });
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    if (mobileNoVM != null && mobileNoVM.SelectedMobileNo != null)
                    {
                        /*For checking the MSISDN whether valid or not valid by chetan*/
                        bool checkval = false;
                        using (var proxy = new KenanServiceProxy())
                        {
                            checkval = proxy.validateInventoryExtIdService(new TypeValidateInventoryExtIdRequest { externalId = mobileNoVM.SelectedMobileNo });
                            //checkval = false;
                        }
                        if (checkval && primarySupplimentLines)
                        {
                            // return RedirectToAction("SelectPrimarySuppLine", "Registration");
                            if (isDedicatedSuppFlow)
                            {
                                redirectPage = RedirectToAction("IsSupplimentaryRequired", "SuppLine");
                            }
                            else if (newLineSupplementary)
                            {
                                redirectPage = RedirectToAction("IsSupplimentaryRequired", "NewLine");
                            }
                            else
                            {
                                redirectPage = RedirectToAction("IsSupplimentaryRequired", "Registration");
                            }

                        }
                        else if (checkval)
                        {
                            //redirectPage = RedirectToAction(GetRegActionStep(mobileNoVM.TabNumber));
                            if (isDedicatedSuppFlow)
                            {
                                redirectPage = RedirectToAction("PersonalDetailsNew", "SuppLine");
                            }
                            else if (newLineSupplementary)
                            {
                                redirectPage = RedirectToAction("PersonalDetailsNew", "NewLine");
                            }
                            else
                            {
                                redirectPage = RedirectToAction("PersonalDetailsNew", "Registration");
                            }

                        }
                        else
                        {
                            redirectPage = RedirectToAction("SelectMobileNoNew", new { invchk = "False" });
                        }
                    }
                    else if (primarySupplimentLines)
                    {
                        // return RedirectToAction("SelectPrimarySuppLine", "Registration");
                        redirectPage = RedirectToAction("IsSupplimentaryRequired", "Registration");
                    }
                    else
                    {
                        if (collection["submit1"].ToString2() != "search")
                        {
                            redirectPage = RedirectToAction(GetRegActionStep(mobileNoVM.TabNumber));
                        }
                    }
                }
                Session[SessionKey.DropFourObj.ToString()] = dropObj;
                return redirectPage;
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

        }



        //PersonalDetails() original code .Commented by sumit ##########################

        //[Authorize(Roles = "MREG_W,MREG_C")]
        //public ActionResult PersonalDetails()
        //{
        //    SetRegStepNo(MobileRegistrationSteps.PersonalDetails);
        //    MNPPrimaryPlanStorage _mnpPlan = null;
        //    if (IsMNPWithMultpleLines)
        //    {
        //        _mnpPlan = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];
        //    }
        //    else
        //    {
        //        _mnpPlan = new MNPPrimaryPlanStorage();
        //    }
        //    if (IsMNPWithMultpleLines)
        //    {
        //        _mnpPlan.RegMobileReg_OrderSummary = ConstructOrderSummary(fromSubline: true);
        //    }
        //    else
        //    {
        //        Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(fromSubline: true);
        //    }


        //    var idCardTypeID = Session[SessionKey.RegMobileReg_IDCardType.ToString2()] != null ? Session[SessionKey.RegMobileReg_IDCardType.ToString2()] : null;
        //    var personalDetailsVM = new PersonalDetailsVM();
        //    if (IsMNPWithMultpleLines)
        //    {
        //        if (_mnpPlan.RegMobileReg_PersonalDetailsVM != null)
        //        {
        //            personalDetailsVM = _mnpPlan.RegMobileReg_PersonalDetailsVM;
        //        }
        //    }
        //    else
        //    {
        //        if ((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"] != null)
        //        {
        //            personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
        //        }
        //    }
        //    personalDetailsVM.Customer.IDCardTypeID = idCardTypeID.ToInt();
        //    if (IsMNPWithMultpleLines)
        //        personalDetailsVM.Customer.IDCardNo = _mnpPlan.RegMobileReg_IDCardNo.ToString2();
        //    else
        //        personalDetailsVM.Customer.IDCardNo = Session[SessionKey.RegMobileReg_IDCardNo.ToString2()] == null ? "" : Session[SessionKey.RegMobileReg_IDCardNo.ToString2()].ToString2();

        //    _mnpPlan.RegMobileReg_PersonalDetailsVM = personalDetailsVM;
        //    Session["mNPPrimaryPlanStorage"] = _mnpPlan;
        //    return View(personalDetailsVM);
        //}


        //end PersonalDetails() original code .Commented by sumit ############################

        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        public ActionResult PersonalDetails()
        {
            SetRegStepNo(MobileRegistrationSteps.PersonalDetails);
            MNPPrimaryPlanStorage _mnpPlan = null;
            var personalDetailsVM = new PersonalDetailsVM();
            if (IsMNPWithMultpleLines)
            {
                _mnpPlan = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];
                _mnpPlan.RegMobileReg_OrderSummary = ConstructOrderSummary(fromSubline: true);
                if (_mnpPlan.RegMobileReg_PersonalDetailsVM != null)
                {
                    personalDetailsVM = _mnpPlan.RegMobileReg_PersonalDetailsVM;
                }
                personalDetailsVM.Customer.IDCardNo = _mnpPlan.RegMobileReg_IDCardNo.ToString2();
            }
            else
            {
                _mnpPlan = new MNPPrimaryPlanStorage();
                Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(fromSubline: true);
                if ((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"] != null)
                {
                    personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
                }
                personalDetailsVM.Customer.IDCardNo = Session[SessionKey.RegMobileReg_IDCardNo.ToString()] == null ? "" : Session[SessionKey.RegMobileReg_IDCardNo.ToString()].ToString2();
            }



            var idCardTypeID = Session[SessionKey.RegMobileReg_IDCardType.ToString()] != null ? Session[SessionKey.RegMobileReg_IDCardType.ToString()] : null;
            personalDetailsVM.Customer.IDCardTypeID = idCardTypeID.ToInt();
            _mnpPlan.RegMobileReg_PersonalDetailsVM = personalDetailsVM;
            Session["mNPPrimaryPlanStorage"] = _mnpPlan;

            if (Session["RegMobileReg_UpfrontPayment"] != null)
                personalDetailsVM.UpfrontPayment = (decimal)Session["RegMobileReg_UpfrontPayment"];

            return View(personalDetailsVM);
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        public ActionResult PersonalDetails(PersonalDetailsVM personalDetailsVM)
        {
            try
            {
                if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.Summary)
                {
                    personalDetailsVM.Customer.IDCardTypeID = personalDetailsVM.InputIDCardTypeID.ToInt();
                    personalDetailsVM.Customer.IDCardNo = personalDetailsVM.InputIDCardNo;

                    /*Added by Rajeswari on 24th March to disable nationality*/
                    personalDetailsVM.Customer.NationalityID = personalDetailsVM.InputNationalityID;
                    Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;
                    Session["RegMobileReg_TabIndex"] = personalDetailsVM.TabNumber;
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    WebHelper.Instance.BusinessValidationChecks(personalDetailsVM, this.GetType());

                    Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(fromSubline: true);

                    OrderSummaryVM orderVM = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];


                    using (var proxy = new UserServiceProxy())
                    {
                        if (Session["IsDeviceRequired"].ToString2() == "Yes")
                        {
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Personal Details", Session["KenanACNumber"].ToString2(), "");
                        }
                        else
                        {
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SuppPlan, "Personal Details", Session["KenanACNumber"].ToString2(), "");
                        }
                    }

                    if (personalDetailsVM.Customer.PayModeID == Util.GetPaymentModeID(PAYMENTMODE.AutoBilling))
                    {
                        if (!ValidateCardDetails(personalDetailsVM.Customer))
                        {
                            return View(personalDetailsVM);
                        }
                    }
                }
                else
                {
                    if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DeviceOnly)
                    {
                        Session["RegMobileReg_TabIndex"] = (int)MobileRegistrationSteps.DeviceCatalog;
                        personalDetailsVM.TabNumber = (int)MobileRegistrationSteps.DeviceCatalog;
                    }
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            if (Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_SK") || Roles.IsUserInRole("MREG_DAI"))
            {
                //return RedirectToAction("DMobileRegSummary");
                return RedirectToAction(GetRegActionStep(personalDetailsVM.TabNumber));
            }
            else
            {
                return RedirectToAction(GetRegActionStep(personalDetailsVM.TabNumber));
            }
        }

        [Authorize(Roles = "MREG_W,MREG_C,DSupervisor,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        public ActionResult PersonalDetailsNew()
        {
            SetRegStepNo(MobileRegistrationSteps.PersonalDetails);
            ViewBag.BackButton = GetPersonalDetailsBackButton();
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            MNPPrimaryPlanStorage _mnpPlan = null;
            var personalDetailsVM = new PersonalDetailsVM();
            if (IsMNPWithMultpleLines)
            {
                _mnpPlan = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];
                _mnpPlan.RegMobileReg_OrderSummary = ConstructOrderSummary(fromSubline: true);
                if (_mnpPlan.RegMobileReg_PersonalDetailsVM != null)
                {
                    personalDetailsVM = _mnpPlan.RegMobileReg_PersonalDetailsVM;
                }
                personalDetailsVM.Customer.IDCardNo = _mnpPlan.RegMobileReg_IDCardNo.ToString2();
                string IDCardTypeID = Session[SessionKey.RegMobileReg_IDCardType.ToString()] == null ? "" : Session[SessionKey.RegMobileReg_IDCardType.ToString()].ToString2();
            }
            else
            {
                _mnpPlan = new MNPPrimaryPlanStorage();
                Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(fromSubline: true);
                if ((PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"] != null)
                {
                    personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
                }
                personalDetailsVM.Customer.IDCardNo = Session[SessionKey.RegMobileReg_IDCardNo.ToString()] == null ? "" : Session[SessionKey.RegMobileReg_IDCardNo.ToString()].ToString2();
                string IDCardTypeID = Session[SessionKey.RegMobileReg_IDCardType.ToString()] == null ? "" : Session[SessionKey.RegMobileReg_IDCardType.ToString()].ToString2();
            }

            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            if (null != dropObj.suppFlow[dropObj.suppIndex - 1].mobile)
            {
                if (dropObj.suppIndex > 0)
                {
                    personalDetailsVM.Customer.ContactNo = dropObj.suppFlow[dropObj.suppIndex - 1].mobile.SelectedMobileNo;
                }
            }

            _mnpPlan.RegMobileReg_PersonalDetailsVM = personalDetailsVM;
            Session["mNPPrimaryPlanStorage"] = _mnpPlan;

            if (Session["RegMobileReg_UpfrontPayment"] != null)
                personalDetailsVM.UpfrontPayment = (decimal)Session["RegMobileReg_UpfrontPayment"];

            // to default all personal details from response
            personalDetailsVM = Util.FormPersonalDetailsandCustomer(personalDetailsVM, _fromSuppLine: true, _isStandalone: Session[SessionKey.IsSuppleNewAccount.ToString()].ToBool());
            personalDetailsVM = Util.CheckDisablePersonalDetailsPage(personalDetailsVM);

            if (Roles.IsUserInRole("MREG_HQ") || Roles.IsUserInRole("MREG_DAB") || Roles.IsUserInRole("MREG_SV"))
            {
                personalDetailsVM.Supervisor = true;
            }

            //GTM e-Billing CR - Ricky - 2014.09.25 - Start
            string SelectAccountDtls = Session[SessionKey.AccExternalID.ToString()].ToString2();
            var availableMsisdn = WebHelper.Instance.GetAvailableMsisdnForSmsNotification(SelectAccountDtls);
            var billDeliveryVM = new BillDeliveryVM();
            billDeliveryVM.AcctExtId = SelectAccountDtls;

            if (string.IsNullOrEmpty(Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2()))
            {
                //set default value for first landing, which is to be retrieved from EBPS
                WebHelper.Instance.BillDeliveryRetrieve(billDeliveryVM);
                if (!billDeliveryVM.Result)
                {
                    TempData["ErrorMessage"] = string.Format(ConfigurationManager.AppSettings["BillDelivery_ErrorMessage_EBPSNotification"].ToString() + billDeliveryVM.ErrorMessage);
                }
                personalDetailsVM.AvailableMsisdn = availableMsisdn;
                personalDetailsVM.EmailBillSubscription = billDeliveryVM.BillDeliveryDetails.EmailBillSubscription;
                personalDetailsVM.EmailAddress = billDeliveryVM.BillDeliveryDetails.EmailAddress;
                personalDetailsVM.SmsAlertFlag = billDeliveryVM.BillDeliveryDetails.SmsAlertFlag;
                personalDetailsVM.SmsNo = billDeliveryVM.BillDeliveryDetails.SmsNo;
            }
            else
            {
                //retrieve previous value from session if user press the back button
                personalDetailsVM.AvailableMsisdn = availableMsisdn;
                personalDetailsVM.EmailBillSubscription = Session[SessionKey.billDelivery_emailBillSubscription.ToString()].ToString2();
                personalDetailsVM.EmailAddress = Session[SessionKey.billDelivery_emailAddress.ToString()].ToString2();
                personalDetailsVM.SmsAlertFlag = Session[SessionKey.billDelivery_smsAlertFlag.ToString()].ToString2();
                personalDetailsVM.SmsNo = Session[SessionKey.billDelivery_smsNo.ToString()].ToString2();
            }
            //GTM e-Billing CR - Ricky - 2014.09.25 - End

            //Drop 5 B18 - Ricky - 2015.05.12 - Populate the default value for Outlet/Sales Code
            WebHelper.Instance.PopulateDealerInfo(personalDetailsVM);

			if (sharingQuotaFeature)
			{
				personalDetailsVM.sharingSuppErrorMsg = ConfigurationManager.AppSettings["sharingSuppErrorMsgSameAccount"].ToString2();
				if (!string.IsNullOrEmpty(personalDetailsVM.sharingSuppErrorMsg))
					personalDetailsVM.sharingSuppAdded = WebHelper.Instance.sharingSuppCount(dropObj) > 0 ? true : false;
			}

            retrieveAcctListByICResponse allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
            personalDetailsVM.Liberlization_Status = WebHelper.Instance.determineRiskCategory(allCustomerDetails);

            Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

            //Drop 5 - Karyne - 2015.09.08 - Validate BRE on page load - Start
            string JustificationMessage = string.Empty;
            string HardStopMessage = string.Empty;
            string ApprovalMessage = string.Empty;
            bool isBREFail = false;
            WebHelper.Instance.BRECheckMessage(out HardStopMessage, out JustificationMessage, out ApprovalMessage, out isBREFail);
            //Drop 5 - Karyne - 2015.09.08 - Validate BRE on page load - End

            return View(personalDetailsVM);
        }

        [HttpPost]
        [Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        public ActionResult PersonalDetailsNew(PersonalDetailsVM personalDetailsVM, FormCollection collection)
        {
            bool isDedicatedSuppFlow = Session[SessionKey.DedicatedSuppFlow.ToString()].ToBool();
            string addressType = collection["addressType"];
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

            // to default all personal details from response -[w.loon] BUG #1525 retain new input values and disable pre-populated fields
            Util.FormPersonalDetailsandCustomer(personalDetailsVM);
            Util.CheckDisablePersonalDetailsPage(personalDetailsVM);
            if (personalDetailsVM.Customer.AlternateContactNo == "")
                personalDetailsVM.Customer.AlternateContactNo = collection["Customer.AlternateContactNo"];
            if (personalDetailsVM.Customer.EmailAddr == "")
                personalDetailsVM.Customer.EmailAddr = collection["Customer.EmailAddr"];
            if (personalDetailsVM.Customer.RaceID == 0)
                personalDetailsVM.Customer.RaceID = collection["Customer.RaceID"].ToInt();

            // tempstore card number
            dropObj.tempCardNumber = personalDetailsVM.Customer.CardNo;
            string test = collection["hdnWaiverInfo"];

            if (isDedicatedSuppFlow)
            {
                if (addressType.Equals("new-address"))
                {
                    Session[SessionKey.IsSuppleNewAccount.ToString()] = true;
                    Session["SimType"] = "Normal";
                }
                else
                {
                    Session[SessionKey.IsSuppleNewAccount.ToString()] = false;
                    Session["SimType"] = "Normal";
                }
            }

            if (Session["RegMobileReg_PersonalDetailsVM"] != null)
            {
                var personalDetailsVMSession = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

                if ((personalDetailsVM.idDocumentFront == null || personalDetailsVM.idDocumentFront[0] == null) && personalDetailsVMSession.idDocumentFront != null)
                {
                    personalDetailsVM.idDocumentFront = personalDetailsVMSession.idDocumentFront;
                }

                if ((personalDetailsVM.idDocumentBack == null || personalDetailsVM.idDocumentBack[0] == null) && personalDetailsVMSession.idDocumentBack != null)
                {
                    personalDetailsVM.idDocumentBack = personalDetailsVMSession.idDocumentBack;
                }
            }

			if (!ReferenceEquals(Session["CustPersonalInfo"],null))
			{
				var customerPersonalInfo = (CustomizedCustomer)Session["CustPersonalInfo"];

				personalDetailsVM.Customer.EmailAddr = !string.IsNullOrEmpty(personalDetailsVM.Customer.EmailAddr) ? personalDetailsVM.Customer.EmailAddr :
																	customerPersonalInfo.EmailAddrs;
				personalDetailsVM.Customer.ContactNo = Util.FormatContactNumber(customerPersonalInfo.Cbr);
				personalDetailsVM.Customer.AlternateContactNo = customerPersonalInfo.AlternateContactNo;
			}

            try
            {
                if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.Summary)
                {
                    if (null == Session["RegMobileReg_IDCardType"])
                    {
                        Session["RegMobileReg_IDCardType"] = Session["IDCardTypeID"].ToString();
                    }
                    personalDetailsVM.InputIDCardTypeID = Session["RegMobileReg_IDCardType"].ToString();
                    personalDetailsVM.Customer.IDCardTypeID = Session["RegMobileReg_IDCardType"].ToInt();
                    personalDetailsVM.Customer.IDCardNo = personalDetailsVM.InputIDCardNo;

                    personalDetailsVM.Customer.NationalityID = personalDetailsVM.InputNationalityID;
                    Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;
                    Session["RegMobileReg_TabIndex"] = personalDetailsVM.TabNumber;

					personalDetailsVM.idDocumentFront = collection["frontPhoto"];
					personalDetailsVM.frontDocumentName = collection["frontPhotoFileName"];
					personalDetailsVM.idDocumentBack = collection["backPhoto"];
					personalDetailsVM.backDocumentName = collection["backPhotoFileName"];
					personalDetailsVM.otherDocuments = collection["otherPhoto"];
					personalDetailsVM.othDocumentName = collection["otherFileName"];
					personalDetailsVM.thirdPartyFile = collection["thirdPartyPhoto"];
					personalDetailsVM.thirdPartyDocumentName = collection["thirdPartyFileName"];

					//clearing session if filename not exist 
					if (string.IsNullOrEmpty(personalDetailsVM.frontDocumentName)) Session["frontImage"] = null;
					if (string.IsNullOrEmpty(personalDetailsVM.backDocumentName)) Session["backImage"] = null;
					if (string.IsNullOrEmpty(personalDetailsVM.othDocumentName)) Session["otherImage"] = null;
					if (string.IsNullOrEmpty(personalDetailsVM.thirdPartyDocumentName)) Session["thirdImage"] = null;

					string SelectAccountDtls = Session[SessionKey.AccExternalID.ToString()].ToString2();
					personalDetailsVM.AvailableMsisdn = WebHelper.Instance.GetAvailableMsisdnForSmsNotification(SelectAccountDtls);

                    //if (Session["RegMobileReg_PersonalDetailsVM"] != null)
                    //{
                    //    var personalDetailsVMSession = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];

                    //    if (string.IsNullOrEmpty(personalDetailsVM.idDocumentFront))
                    //    {
                    //        personalDetailsVM.idDocumentFront = personalDetailsVMSession.idDocumentFront;
                    //        personalDetailsVM.frontDocumentName = personalDetailsVMSession.frontDocumentName;
                    //    }
                    //    if (string.IsNullOrEmpty(personalDetailsVM.idDocumentBack))
                    //    {
                    //        personalDetailsVM.idDocumentBack = personalDetailsVMSession.idDocumentBack;
                    //        personalDetailsVM.backDocumentName = personalDetailsVMSession.backDocumentName;
                    //    }
                    //    if (string.IsNullOrEmpty(personalDetailsVM.otherDocuments))
                    //    {
                    //        personalDetailsVM.otherDocuments = personalDetailsVMSession.otherDocuments;
                    //        personalDetailsVM.othDocumentName = personalDetailsVMSession.othDocumentName;
                    //    }
                    //    if (string.IsNullOrEmpty(personalDetailsVM.thirdPartyFile))
                    //    {
                    //        personalDetailsVM.thirdPartyFile = personalDetailsVMSession.thirdPartyFile;
                    //        personalDetailsVM.thirdPartyDocumentName = personalDetailsVMSession.thirdPartyDocumentName;
                    //    }
                    //}

					// MyMaxis app survey 
					personalDetailsVM.SurveyFeedBack = collection["surveyFeedback"].ToString2();
                    //personalDetailsVM.SurveyAnswer = !string.IsNullOrWhiteSpace(personalDetailsVM.SurveyFeedBack) ? "Yes" : "No";
                    personalDetailsVM.SurveyAnswerRate = collection["surveyAnswerRate"].ToString2();
                    personalDetailsVM.SurveyAnswer = collection["surveyAnswer"].ToString2();

                    personalDetailsVM.SingleSignOnValue = collection["SingleSignOnValue"].ToString2();
                    personalDetailsVM.SingleSignOnEmailAddress = collection["SingleSignOnEmailAddress"].ToString2();

                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    #region RST BRE CR - comment out - Moved to webhelper breValidate region Contract Check
                    /*
                    //Anthony - Add condition for Contract Check
                    //if (!ReferenceEquals(Session["FromMNP"], null) && !Convert.ToBoolean(Session["FromMNP"]))
                    if (ReferenceEquals(Session["FromMNP"], null) || (!ReferenceEquals(Session["FromMNP"], null) && !Convert.ToBoolean(Session["FromMNP"])))
					{
						//20122014 - Anthony - to prevent page crash on order type add supplementary
						//if (!isDedicatedSuppFlow)
						//{
						**************** Contract Check ***********************
						dropObj = dropObj.getExistingContractCount(dropObj);
						**************** Contract Check ***********************
						//}
                        
						// if contract check qty is exceed the maximum number allowed then do hardstop
						// do hardstop for MEPS & justification for MC
						if ((dropObj.additionalContract + dropObj.existingContract) > Convert.ToInt32(ConfigurationManager.AppSettings["MaxOrderTotalContract"]))
						{
							if (Util.SessionAccess.User.isDealer)
							{
								TempData["ErrorMessage_2"] = ConfigurationManager.AppSettings["ContractCheckHSMsg"].ToString2();
								return View(personalDetailsVM);
							}
							else
							{
								if (string.IsNullOrEmpty(validationResult))
								{
									validationResult = "Contract Check";
								}
								else
								{
									if (!validationResult.Contains("Contract Check"))
									{
										validationResult = validationResult + " , " + "Contract Check";
									}
								}
							}
						}
					}*/
                    #endregion

                    //Check the business validations sharvin
                    WebHelper.Instance.BusinessValidationChecks(personalDetailsVM, this.GetType());
                    var validationResult = WebHelper.Instance.breValidate_list();

                    string JustificationMessage = string.Empty;
                    string HardStopMessage = string.Empty;
                    foreach (var _result in validationResult)
                    {
                        JustificationMessage = (_result.Action == "J") ?
                            string.IsNullOrEmpty(JustificationMessage) ? _result.Code : JustificationMessage + ", " + _result.Code :
                            JustificationMessage;
                        
                        if (_result.Action == "HS")
                        {
                            switch(_result.Code)
                            {
                                case "Contract Check":
                                    HardStopMessage = string.IsNullOrEmpty(HardStopMessage) ? ErrorMsg.Registration.ContractFail : HardStopMessage + ", " + ErrorMsg.Registration.ContractFail;
                                    break;
                                case "Age Check":
                                    HardStopMessage = string.IsNullOrEmpty(HardStopMessage) ? ErrorMsg.Registration.AgeFail : HardStopMessage + ", " + ErrorMsg.Registration.AgeFail;
                                    break;
                                case "DDMF Check":
                                    HardStopMessage = string.IsNullOrEmpty(HardStopMessage) ? ErrorMsg.Registration.DDMFFail : HardStopMessage + ", " + ErrorMsg.Registration.DDMFFail;
                                    break;
                                case "Address Check":
                                    HardStopMessage = string.IsNullOrEmpty(HardStopMessage) ? ErrorMsg.Registration.AddressFail : HardStopMessage + ", " + ErrorMsg.Registration.AddressFail;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(HardStopMessage))
                    {
                        TempData["ErrorMessage"] = HardStopMessage;
                        return View(personalDetailsVM);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(JustificationMessage) && (string.IsNullOrEmpty(personalDetailsVM.Justification)))
                        {
                            TempData["ErrorMessage"] = "Please Enter Justification for - " + JustificationMessage;
                            return View(personalDetailsVM);
                        }
                    }

                    #region TAC Validation
                    // 20151117 - [w.loon] - TAC Validation - start
                    if (WebHelper.Instance.IsTacRequired() && !Online.Registration.Web.Properties.Settings.Default.DevelopmentModeOnForTacCheck && !Session["IsTacExceptional"].ToBool())
                    {
                        string submittedTac = collection["tacInput"].ToString2();
                        string EmptyTacError = "Please enter TAC.";
                        string ExpiredTacError = "TAC has expired. Please request a new TAC.";
                        string TacNotRequestedError = "TAC has not been requested. Please request a TAC.";
                        string InvalidTacError = "TAC is invalid. Please enter a valid TAC.";

                        // TempData["ErrorMessage"] is for New Layout (_PersonalDetailsNewPV.cshtml); ModelState.AddModelError() is for Old Layout (PersonalDetails.cshtml)
                        if (submittedTac == "" && collection["tacRequested"].ToString2() == "true")
                        {
                            TempData["ErrorMessage"] = EmptyTacError;
                            ModelState.AddModelError(string.Empty, EmptyTacError);
                            return View(personalDetailsVM);
                        }
                        else
                        {
                            HttpCookie tacCookie = Request.Cookies.Get("lastTac");
                            HttpCookie tacCookieDt = Request.Cookies.Get("lastTacRequestDt");
                            if (tacCookie == null && tacCookieDt == null)
                            {
                                if (collection["tacRequested"].ToString2() == "true")
                                {
                                    TempData["ErrorMessage"] = ExpiredTacError;
                                    ModelState.AddModelError(string.Empty, ExpiredTacError);
                                }
                                else
                                {
                                    TempData["ErrorMessage"] = TacNotRequestedError;
                                    ModelState.AddModelError(string.Empty, TacNotRequestedError);
                                }
                                return View(personalDetailsVM);
                            }
                            else
                            {
                                if (submittedTac != tacCookie.Value.ToString())
                                {
                                    TempData["ErrorMessage"] = InvalidTacError;
                                    ModelState.AddModelError(string.Empty, InvalidTacError);
                                    return View(personalDetailsVM);
                                }
                                else
                                {
                                    // clear TAC cookies
                                    tacCookie.Expires = DateTime.Now.AddDays(-1);
                                    tacCookieDt.Expires = DateTime.Now.AddDays(-1);
                                    Response.Cookies.Add(tacCookie);
                                    Response.Cookies.Add(tacCookieDt);
                                }
                            }
                        }
                    }
                    // 20151117 - [w.loon] - TAC Validation - end
                    #endregion

                    //drop 5
                    if (!string.IsNullOrEmpty(HardStopMessage) || !string.IsNullOrEmpty(JustificationMessage))
                    {
                        personalDetailsVM.isBREFail = true;
                    }
                    string postcode = personalDetailsVM.Address.Postcode;
                    var result = WebHelper.Instance.checkStateCityByPostcode(postcode);
                    if (!ReferenceEquals(result, null))
                    {
                        if (!string.IsNullOrEmpty(result.ErrorMessage))
                        {
                            TempData["ErrorMessage"] = result.ErrorMessage;
                            return View(personalDetailsVM);
                        }
                    }

                    #region comment out - breCheck hardstop/justy logic
                    /*
                    if (!string.IsNullOrEmpty(validationResult))
                    {
						//Gregory PN 2546 add conditional for justification not null
						if (string.IsNullOrEmpty(personalDetailsVM.Justification) || personalDetailsVM.Justification.Trim().Length == 0)
                        {
                            TempData["ErrorMessage"] = validationResult;
                            return View(personalDetailsVM);
                        }

                        if (personalDetailsVM.Justification != null)
                        {
                            /*RST BRE CR - 20150126
                            if (validationResult == ErrorMsg.Registration.ContractFail || validationResult.Contains(ErrorMsg.Registration.ContractFail))
                            {
                                TempData["ErrorMessage"] = validationResult;
                                return View(personalDetailsVM);
                            }
                            //end RST*
                            if (validationResult == ErrorMsg.Registration.DDMFFail)
                            {
                                TempData["ErrorMessage"] = validationResult;
                                return View(personalDetailsVM);
                            }

                            if (validationResult == ErrorMsg.Registration.AgeFail)
                            {
                                TempData["ErrorMessage"] = validationResult;
                                return View(personalDetailsVM);
                            }

                            if (validationResult == ErrorMsg.Registration.AddressFail)
                            {
                                TempData["ErrorMessage"] = validationResult;
                                return View(personalDetailsVM);
                            }
                        }
                    }*/
                    #endregion
                    Session["RegMobileReg_IsAddressCheckFailed"] = false;
                    Session["RegMobileReg_IsAgeCheckFailed"] = false;
                    Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary(fromSubline: true, standalone: Session[SessionKey.IsSuppleNewAccount.ToString()].ToBool());

                    OrderSummaryVM orderVM = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];


                    using (var proxy = new UserServiceProxy())
                    {
                        if (Session["IsDeviceRequired"].ToString2() == "Yes")
                        {
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Personal Details", Session["KenanACNumber"].ToString2(), "");
                        }
                        else
                        {
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SuppPlan, "Personal Details", Session["KenanACNumber"].ToString2(), "");
                        }
                    }

                    if (personalDetailsVM.Customer.PayModeID == Util.GetPaymentModeID(PAYMENTMODE.AutoBilling))
                    {
                        if (!ValidateCardDetails(personalDetailsVM.Customer))
                        {
                            return View(personalDetailsVM);
                        }
                    }

                    //GTM e-Billing CR - Ricky - 2014.09.25 - Start
                    bool isValid = false;
                    string ErrorMessage = string.Empty;
                    WebHelper.Instance.BillDeliveryValidate(personalDetailsVM.EmailBillSubscription, personalDetailsVM.EmailAddress, personalDetailsVM.SmsAlertFlag, personalDetailsVM.SmsNo, out isValid, out ErrorMessage);
					if (!isValid)
                    {
                        TempData["ErrorMessage"] = ErrorMessage;
                        return View(personalDetailsVM);
                    }
                    //if valid, store into Session
                    Session[SessionKey.billDelivery_emailBillSubscription.ToString()] = personalDetailsVM.EmailBillSubscription;
                    Session[SessionKey.billDelivery_emailAddress.ToString()] = personalDetailsVM.EmailAddress;
                    Session[SessionKey.billDelivery_smsAlertFlag.ToString()] = personalDetailsVM.SmsAlertFlag;
                    Session[SessionKey.billDelivery_smsNo.ToString()] = personalDetailsVM.SmsNo;
                    //GTM e-Billing CR - Ricky - 2014.09.25 - End

					if (sharingQuotaFeature)
					{
						if (addressType.Equals("new-address"))
						{
                            personalDetailsVM.sharingSuppAdded = WebHelper.Instance.sharingSuppCount(dropObj) > 0 ? true : false;
                            //string sharingSuppErrorMsgSameAccount = ConfigurationManager.AppSettings["sharingSuppErrorMsgSameAccount"].ToString2();
							//if (!string.IsNullOrEmpty(sharingSuppErrorMsgSameAccount))
                            if (personalDetailsVM.sharingSuppAdded)
                            {
								//personalDetailsVM.sharingSuppAdded = true;
                                personalDetailsVM.sharingSuppErrorMsg = ConfigurationManager.AppSettings["sharingSuppErrorMsgSameAccount"].ToString2(); ;
                                TempData["ErrorMessage"] = personalDetailsVM.sharingSuppErrorMsg;
								return View(personalDetailsVM);
							}
						}
					}

                    //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - Start
                    WebHelper.Instance.ValidateDealerInfo(personalDetailsVM, out isValid, out ErrorMessage);
                    if (!isValid)
                    {
                        TempData["ErrorMessage"] = ErrorMessage;
                        return View(personalDetailsVM);
                    }
                    //Drop-5 B18 - Dealer Code CR - Ricky - 2015.05.05 - End
                }
                else
                {
                    if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DeviceOnly)
                    {
                        Session["RegMobileReg_TabIndex"] = (int)MobileRegistrationSteps.DeviceCatalog;
                        personalDetailsVM.TabNumber = (int)MobileRegistrationSteps.DeviceCatalog;
                    }
                }

                dropObj.personalInformation = personalDetailsVM;
                //for back button scenario, undo/remove the last selected number from the selectedMobileNo bucket 
                if (dropObj.selectedMobileNoinFlow.Count() > 0)
                    dropObj.selectedMobileNoinFlow.RemoveAt(dropObj.selectedMobileNoinFlow.Count() - 1);

				dropObj.FromMobileDevice = !string.IsNullOrEmpty(collection["fromMobileDevice"]) ? collection["fromMobileDevice"].ToBool() : false;
                dropObj.ApproverID = collection["HdnApproverID"].ToInt();
                Session[SessionKey.DropFourObj.ToString()] = dropObj;
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            Session[SessionKey.Email.ToString()] = personalDetailsVM.Customer.EmailAddr;
            if (Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_SK") || Roles.IsUserInRole("MREG_DAI"))
            {
                //return RedirectToAction("DMobileRegSummary");
                return RedirectToAction(GetRegActionStep(personalDetailsVM.TabNumber));
            }
            else
            {
                return RedirectToAction(GetRegActionStep(personalDetailsVM.TabNumber));
            }
        }

        [Authorize(Roles = "MREG_W,MREG_C,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
        [DoNoTCache]
        public ActionResult MobileRegSuccess(int regID)
        {
            //Save the User TransactionLog
            WebHelper.Instance.SaveUserTransactionLogs(regID);
            var reg = new DAL.Models.Registration();
            PersonalDetailsVM personalDetails = new PersonalDetailsVM();
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];
            Session["RegID"] = regID;

			using (var regProxy = new RegistrationServiceProxy())
			{
				reg = regProxy.RegistrationGet(regID);
			}

			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var regProxy = new RegistrationServiceProxy())
				{
					personalDetails.PegaVM = new PegaRecommendationsVM();
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					personalDetails.PegaVM.PegaRecommendationsSearchResult = regProxy.GetPegaRecommendationbyMSISDN(personalDetails.PegaVM.PegaRecommendationsSearchCriteria);
					personalDetails.PegaVM.PegaRecommendationsSearchResult = personalDetails.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();

				}
			}
            
            personalDetails.RegID = regID;
            personalDetails.SignatureSVG = reg.SignatureSVG;

            
            return View(personalDetails);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MobileRegSuccess(FormCollection collection, PersonalDetailsVM personalDetails)
        {
            DropFourObj dropObj = new DropFourObj();
            dropObj.personalInformation = new ViewModels.PersonalDetailsVM();
			dropObj.personalInformation.rfDocumentsFile = collection["frontPhoto"].ToString2() == "" ? Session["frontImage"].ToString2() : collection["frontPhoto"].ToString2();
            dropObj.personalInformation.rfDocumentsFileName = collection["frontPhotoFileName"];
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];
            WebHelper.Instance.SaveDocumentToTable(dropObj, personalDetails.RegID);
            ViewBag.documentUploaded = true;
            using (var proxy = new WebPOSCallBackSvc.WebPOSCallBackServiceClient())
            {
                proxy.sendDocsToIcontract(personalDetails.RegID, true);
            }

			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var proxy = new RegistrationServiceProxy())
				{
					personalDetails.PegaVM = new PegaRecommendationsVM();
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					personalDetails.PegaVM.PegaRecommendationsSearchResult = proxy.GetPegaRecommendationbyMSISDN(personalDetails.PegaVM.PegaRecommendationsSearchCriteria);
					personalDetails.PegaVM.PegaRecommendationsSearchResult = personalDetails.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();
				}
			}

            return View(personalDetails);
        }

        [Authorize(Roles = "MREG_CH,MREG_W,MREG_C,MREG_U,MREG_SK,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSK,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegCanceled(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,DREG_N,DREG_C,MREG_DAI,MREG_DSV,MREG_DAC,MREG_DC,MREG_DSK,MREG_DIC")]
        [DoNoTCache]
        public ActionResult MobileRegAccCreated(int regID)
        {
            //if (!ReferenceEquals(Action, String.Empty))
            //    return RedirectToAction(Action, new { regID = regID });
            
            return View(regID);
        }

        [HttpPost]
        public ActionResult MobileRegAccCreated(FormCollection collection)
        {
            int orderID = collection["OrderId"].ToInt();
            return View(orderID);
        }

        #region "Deactivated due to auto send transaction to webpos w/o login"
        //[Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_W,MREG_C,MREG_DSK,MREG_U,MREG_SK,MREG_DC,MREG_DN,MREG_DCH,MREG_DIC,MREG_DAC")]
        //[DoNoTCache]
        //[HttpGet]
        //public ActionResult WebPOS(int regID)
        //{
        //    WEBPosVM webPOSVM = WebHelper.Instance.ConstructWebPOS(regID);
        //    return View(webPOSVM);
        //}
        #endregion

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_CH,DREG_N,DREG_C,DREG_CH,MREG_DC,MREG_DCH,MREG_DAC,MREG_DIC,MREG_DSV,MREG_DAI,MREG_DAC")]
        [DoNoTCache]
        public ActionResult MobileRegSvcActivated(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_CH,DREG_N,DREG_C,DREG_CH,MREG_DC,MREG_DCH,MREG_DAC,MREG_DIC,MREG_DSV")]
        [DoNoTCache]
        public ActionResult MobileRegClosed(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_CH,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
        public ActionResult MobileRegBreFail(int regID)
        {
            return View(regID);
        }

        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_CH,DREG_N,DREG_C,MREG_DC,MREG_DAI,MREG_DAC")]
        [DoNoTCache]
        public ActionResult MobileRegFail(int regID)
        {
            return View(regID);
        }

        [HttpPost]
        public ActionResult MobileRegFail(FormCollection collection)
        {
            return RedirectToAction(GetRegActionStep(collection["TabNumber"].ToInt()));
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_CH,MREG_SK,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DAI,MREG_DAC")]
        [DoNoTCache]
        public ActionResult MobileRegWMFail(int regID)
        {
            return View(regID);
        }
        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,SMRT_MANAGER,MREG_HQ,MREG_DAB")]
        [DoNoTCache]
        public ActionResult MobileRegSummary_old(int? regID)
        {
            bool isSuppAsNew = false;

            Session["isSuppAsNew"] = isSuppAsNew;

            if (ReferenceEquals(Session["RegMobileReg_PersonalDetailsVM"], null))
            {
                if (!Request.UrlReferrer.AbsolutePath.Contains("Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("StoreKeeper") && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegSuccess") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/SuperVisorForWaiveOff"))
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            var orderSummary = new OrderSummaryVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            #region VLT ADDED CODE 21stFEB for Email functionality
            Session["RegID"] = regID;
            #endregion VLD ADDED CODE 21stFEB for Email functionality
            /*Chetan added for displaying the reson for failed transcation*/
            var KenanLogDetails = new DAL.Models.KenanaLogDetails();
            if (regID.ToInt() > 0)
            {
                WebHelper.Instance.ClearRegistrationSession();
                personalDetailsVM = new PersonalDetailsVM()
                {
                    RegID = regID.Value.ToInt(),
                };

                Session["RegMobileReg_PhoneVM"] = null;
                int paymentstatus = -1;
                using (var proxy = new RegistrationServiceProxy())
                {
                    reg = proxy.RegistrationGet(regID.Value);
                    /*Chetan added for displaying the reson for failed transcation*/
                    KenanLogDetails = proxy.KenanLogDetailsGet(regID.Value);
                    if (!ReferenceEquals(KenanLogDetails, null))
                    {
                        personalDetailsVM.MessageCode = KenanLogDetails.MessageCode.ToInt();
                        personalDetailsVM.MessageDesc = KenanLogDetails.MessageDesc.ToString2();

                        //Added by ravi in order to track in which method it is failing on 19 july 2013
                        personalDetailsVM.ServiceMethodName = KenanLogDetails.MethodName.ToString2();
                        //Added by ravi in order to track in which method it is failing on 19 july 2013 Ends Here
                    }
                    paymentstatus = proxy.PaymentStatusGet(regID.Value);
                    if (paymentstatus != -1)
                    {
                        if (paymentstatus == 0)
                        {
                            personalDetailsVM.PaymentStatus = "0";
                        }
                        else
                        {
                            personalDetailsVM.PaymentStatus = "1";
                        }
                    }
                    else
                    {
                        personalDetailsVM.PaymentStatus = "0";
                    }
                    personalDetailsVM.QueueNo = reg.QueueNo;
                    personalDetailsVM.Remarks = reg.Remarks;
                    personalDetailsVM.BiometricVerify = reg.BiometricVerify;
                    personalDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
                    personalDetailsVM.BlacklistInternal = reg.InternalBlacklisted;
                    personalDetailsVM.SignatureSVG = reg.SignatureSVG;
                    personalDetailsVM.CustomerPhoto = reg.CustomerPhoto;
                    personalDetailsVM.AltCustomerPhoto = reg.AltCustomerPhoto;
                    personalDetailsVM.Photo = reg.Photo;
                    personalDetailsVM.RegSIMSerial = reg.SIMSerial;
                    personalDetailsVM.RegIMEINumber = reg.IMEINumber;
                    personalDetailsVM.CapturedIMEINumber = reg.IMEINumber;
                    personalDetailsVM.RegTypeID = reg.RegTypeID;
                    personalDetailsVM.AgeCheckStatus = reg.AgeCheckStatus;
                    personalDetailsVM.DDMFCheckStatus = reg.DDMFCheckStatus;
                    personalDetailsVM.AddressCheckStatus = reg.AddressCheckStatus;
                    personalDetailsVM.OutStandingCheckStatus = reg.OutStandingCheckStatus;
                    personalDetailsVM.TotalLineCheckStatus = reg.TotalLineCheckStatus;
                    personalDetailsVM.PrincipleLineCheckStatus = reg.PrincipleLineCheckStatus;
                    personalDetailsVM.ContractCheckStatus = reg.ContractCheckStatus;
                    personalDetailsVM.UserType = reg.UserType;
                    personalDetailsVM.PlanAdvance = reg.PlanAdvance;
                    personalDetailsVM.PlanDeposit = reg.PlanDeposit;
                    personalDetailsVM.DeviceAdvance = reg.DeviceAdvance;
                    personalDetailsVM.DeviceDeposit = reg.DeviceDeposit;
                    personalDetailsVM.Discount = reg.Discount;
                    personalDetailsVM.PrePortInCheckStatus = reg.PrePortInCheckStatus;
                    personalDetailsVM.MNPServiceIdCheckStatus = reg.MNPServiceIdCheckStatus;
                    personalDetailsVM.MOC_Status = reg.MOC_Status;
                    personalDetailsVM.Whitelist_Status = reg.Whitelist_Status;

                    //Added by ravi on 22/07/2013 for checking principal msisdn 
                    personalDetailsVM.externalId = reg.externalId;
                    //Added by ravi on 22/07/2013 for checking principal msisdn  Ends Here
                    // RegAccount
                    var regAccount = proxy.RegAccountGet(new int[] { reg.ID }).SingleOrDefault();
                    if (regAccount != null)
                    {
                        personalDetailsVM.ServiceActivateErrMsg = regAccount.KenanActivateServiceErrMessage;
                    }

                    //checking suppline 
                    isSuppAsNew = proxy.getSupplineDetails(personalDetailsVM.RegID);
                    Session["isSuppAsNew"] = isSuppAsNew;

                    // Registration Status
                    var regStatusID = proxy.RegStatusFind(new RegStatusFind()
                    {
                        RegStatus = new RegStatus()
                        {
                            RegID = reg.ID
                        },
                        Active = true
                    }).SingleOrDefault();

                    personalDetailsVM.Deposite = reg.AdditionalCharges;
                    personalDetailsVM.StatusID = proxy.RegStatusGet(new int[] { regStatusID }).SingleOrDefault().StatusID;

                    using (var configProxy = new ConfigServiceProxy())
                    {
                        personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;
                    }

                    //for cancel reason
                    if (personalDetailsVM.StatusID == 21)
                    {

                        using (var regProxy = new RegistrationServiceProxy())
                        {
                            string reason = regProxy.CancelReasonGet(personalDetailsVM.RegID);
                            personalDetailsVM.MessageDesc = reason;

                        }
                    }

                    ExtraTenGetResp objExtraTenDetails = new ExtraTenGetResp();
                    objExtraTenDetails = proxy.GetExtraTenDetails(personalDetailsVM.RegID);
                    List<lnkExtraTenDetails> modListExtraTen = new List<lnkExtraTenDetails>();
                    personalDetailsVM.ExtraTenDetails = objExtraTenDetails.ExtraTenDetails.ToList();
                    if (personalDetailsVM.ExtraTenDetails != null && personalDetailsVM.ExtraTenDetails.Count > 0)
                    {
                        foreach (var extraTen in personalDetailsVM.ExtraTenDetails)
                        {
                            if (string.IsNullOrEmpty(extraTen.MsgCode) == true)
                            {
                                extraTen.MsgCode = "Processing";
                            }
                            else if (extraTen.MsgCode == "0")
                            {
                                extraTen.MsgCode = "Success";
                            }
                            else
                            {
                                extraTen.MsgCode = "Fail";
                            }
                            modListExtraTen.Add(extraTen);

                        }
                        personalDetailsVM.ExtraTenDetails = modListExtraTen;
                    }
                    personalDetailsVM.ExtraTenMobileNumbers = string.Join(",", objExtraTenDetails.ExtraTenDetails.Where(e => e.Status == "A").Select(e => e.Msisdn).ToList());
                    Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;
                    //}

                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);
                    Session["RegMobileReg_MobileNo"] = mobileNos;
                    Session["RegMobileReg_VirtualMobileNo_Sec"] = null;
                    // Customer
                    personalDetailsVM.Customer = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

                    // Address
                    var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                    {
                        Active = true,
                        Address = new Address()
                        {
                            RegID = regID.Value
                        }
                    }).ToList();
                    personalDetailsVM.Address = proxy.RegAddressGet(regAddrIDs).SingleOrDefault();

                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    // RegMdlGrpModels
                    var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                    {
                        RegMdlGrpModel = new RegMdlGrpModel()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();
                    if (regMdlGrpModelIDs.Count() > 0)
                        regMdlGrpModel = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).SingleOrDefault();

                    if (regMdlGrpModel != null && regMdlGrpModel.ID == 0)
                    {
                        Session["RegMobileReg_Type"] = (int)MobileRegType.SuppPlan;
                    }
                    else
                    {
                        Session["RegMobileReg_MainDevicePrice"] = regMdlGrpModel.Price.ToDecimal();
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                        Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;
                    }

                    // RegPgmBdlPkgComponent
                    var regPgmBdlPkgCompIDs = proxy.RegPgmBdlPkgCompFind(new RegPgmBdlPkgCompFind()
                    {
                        RegPgmBdlPkgComp = new RegPgmBdlPkgComp()
                        {
                            RegID = regID.Value
                        },
                        Active = true
                    }).ToList();

                    // Main Line PgmBdlPkgComponent
                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    if (regPgmBdlPkgCompIDs.Count() > 0)
                        regPBPCs = proxy.RegPgmBdlPkgCompGet(regPgmBdlPkgCompIDs).ToList();

                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var suppLineIDs = proxy.RegSuppLineFind(new RegSuppLineFind()
                    {
                        Active = true,
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value,
                            Active = true
                        }
                    }).ToList();

                    if (suppLineIDs.Count() > 0)
                    {
                        suppLines = proxy.RegSuppLineGet(suppLineIDs).ToList();
                    }
                }

                if (suppLines.Count() > 0)
                    WebHelper.Instance.ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

                using (var proxy = new CatalogServiceProxy())
                {
                    string userICNO = Util.SessionAccess.User.IDCardNo;

                    if (userICNO != null && userICNO.Length > 0)
                    {

                        objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                    }
                    if (objWhiteListDetailsResp != null)
                    {
                        WhiteListDetails objWhiteListDetails = new WhiteListDetails();
                        objWhiteListDetails.Description = objWhiteListDetailsResp.Description;
                        if (objWhiteListDetails.Description != null && objWhiteListDetails.Description.Length > 0)
                        {
                            personalDetailsVM._WhiteListDetails = objWhiteListDetails;
                        }
                    }
                    if (regMdlGrpModel.ID > 0)
                        Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;

                    var mainLinePBPCs = new List<PgmBdlPckComponent>();
                    if (mainLinePBPCIDs.Count() > 0)
                        mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var DCCode = Properties.Settings.Default.CompType_DataCon;
                    var ECCode = Properties.Settings.Default.Extra_Component;
                    var MAcode = Properties.Settings.Default.Master_Component;
                    var SPcode = Properties.Settings.Default.SupplimentaryPlan;

                    if (mainLinePBPCs.Count() > 0)
                    {
                        var pkgBdlDetails = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == SPcode)).SingleOrDefault();
                        if (pkgBdlDetails != null)
                            Session["RegMobileReg_PkgPgmBdlPkgCompID"] = pkgBdlDetails.ID;



                        var vasIDs = "";

                        var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode).Select(a => a.ID).ToList();
                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session["RegMobileReg_VasIDs"] = vasIDs;
                    }

                }
            }

            var orderSummaryVM = ConstructOrderSummary(fromSubline: true, standalone: Session[SessionKey.IsSuppleNewAccount.ToString()].ToBool());
            orderSummaryVM.IMEINumber = reg.IMEINumber;
            orderSummaryVM.SIMSerial = reg.SIMSerial;


            if (reg.KenanAccountNo != null)
            { Session["KenanAccountNo"] = reg.KenanAccountNo; }
            else
            { Session["KenanAccountNo"] = "0"; }
            Session["RegMobileReg_OrderSummary"] = orderSummaryVM;

            #region Added by VLT on 21 June 2013 for getting Sim Type
            using (var proxy = new RegistrationServiceProxy())
            {
                if (regID.ToInt() > 0)
                {
                    DAL.Models.LnkRegDetails lnkregdetails = proxy.LnkRegistrationGetByRegId(regID.ToInt());
                    if (lnkregdetails != null)
                    {
                        // Added by ravi on 25 sep for waiver Justification
                        personalDetailsVM.Justification = lnkregdetails.Justification.ToString2();
                        // Added by ravi on 25 Sep for waiver Justification Ends Here
                        personalDetailsVM.ContractCheckCount = lnkregdetails.ContractCheckCount;
                        if (lnkregdetails.SimType != null)
                        {
                            personalDetailsVM.SimType = lnkregdetails.SimType;
                        }
                        else
                        {
                            personalDetailsVM.SimType = "Normal";
                        }
                    }
                    else
                    {
                        personalDetailsVM.SimType = "Normal";
                    }
                    Session["SimType"] = personalDetailsVM.SimType;

                    /*Active PDPA Commented*/
                    /*Changes by chetan related to PDPA implementation*/
                    string documentversion = lnkregdetails.PdpaVersion;
                    personalDetailsVM.PDPAVersionStatus = documentversion;

                    if (!ReferenceEquals(documentversion, null))
                    {
                        using (var registrationProxy = new RegistrationServiceProxy())
                        {
                            var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", documentversion);
                            personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                        }
                    }
                }
                /*Active PDPA Commented*/
                else
                {
                    /*Changes by chetan related to PDPA implementation*/
                    using (var registrationProxy = new RegistrationServiceProxy())
                    {
                        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                        personalDetailsVM.PDPDVersion = resultdata.Version;
                    }
                }
            }
            #endregion

            #region MOC_Status and Liberization status

            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session["ExternalID"].ToString2()) ? string.Empty : Session["ExternalID"].ToString2(), regID.ToInt());
            personalDetailsVM.Liberlization_Status = Result[0];
            personalDetailsVM.MOCStatus = Result[1];

            #endregion

            return View(personalDetailsVM);
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,SMRT_MANAGER,MREG_HQ,MREG_DAB,MREG_DSV,MREG_DSA,MREG_DAI,MREG_DIC,MREG_DAC,MREG_DC,MREG_DSK,MREG_DCH")]
        [DoNoTCache]
        public ActionResult MobileRegSummaryNew(int? regID)
        {
            ViewBag.BackButton = GetOrderSummaryBackButton();
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            {
                bool isSuppAsNew = false;
                if (regID > 0)
                    Session["RegMobileReg_PersonalDetailsVM"] = null;
                Session["isSuppAsNew"] = isSuppAsNew;

                if (ReferenceEquals(Session["RegMobileReg_PersonalDetailsVM"], null) && !ReferenceEquals(Request.UrlReferrer, null))
                {
                    if (!(Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_C") || Roles.IsUserInRole("MREG_DAI")))
                    {
                        if (!Request.UrlReferrer.AbsolutePath.Contains("Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("StoreKeeper") && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegSuccess")
                            && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegSvcActivated") && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegAccCreated")
                            && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/SuperVisorForWaiveOff") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/OrderStatusReport"))
                        {
                            Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }

                var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
                var orderSummary = new OrderSummaryVM();
                var regMdlGrpModel = new RegMdlGrpModel();
                var mdlGrpModelIDs = new List<int>();
                var mainLinePBPCIDs = new List<int>();
                var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
                var suppLines = new List<RegSuppLine>();
                var reg = new DAL.Models.Registration();
                var objWhiteListDetailsResp = new WhiteListDetailsResp();
                Session["RegID"] = regID;
                var KenanLogDetails = new DAL.Models.KenanaLogDetails();
                RegistrationDetails regDetails = new RegistrationDetails();
                if (regID != null && regID.ToInt() > 0)
                {
                    WebHelper.Instance.ClearRegistrationSession();
                    personalDetailsVM = new PersonalDetailsVM()
                    {
                        RegID = regID.Value.ToInt(),
                    };

                    Session["RegMobileReg_PhoneVM"] = null;
                    int paymentstatus = -1;
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        regDetails = proxy.GetRegistrationFullDetails(personalDetailsVM.RegID);

                        reg = regDetails.Regs.SingleOrDefault();
                        KenanLogDetails = regDetails.LogDetails.FirstOrDefault();
                        if (!ReferenceEquals(KenanLogDetails, null))
                        {
                            personalDetailsVM.MessageCode = KenanLogDetails.MessageCode.ToInt();
                            personalDetailsVM.MessageDesc = KenanLogDetails.MessageDesc.ToString2();
                            personalDetailsVM.ServiceMethodName = KenanLogDetails.MethodName.ToString2();
                        }
                        paymentstatus = regDetails.PaymentStatus;
                        if (paymentstatus != -1)
                        {
                            if (paymentstatus == 0)
                            {
                                personalDetailsVM.PaymentStatus = "0";
                            }
                            else
                            {
                                personalDetailsVM.PaymentStatus = "1";
                            }
                        }
                        else
                        {
                            personalDetailsVM.PaymentStatus = "0";
                        }
                        personalDetailsVM.QueueNo = reg.QueueNo;
                        personalDetailsVM.Remarks = reg.Remarks;
                        personalDetailsVM.BiometricVerify = reg.BiometricVerify;
                        personalDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
                        personalDetailsVM.BlacklistInternal = reg.InternalBlacklisted;
                        personalDetailsVM.SignatureSVG = reg.SignatureSVG;
                        personalDetailsVM.CustomerPhoto = reg.CustomerPhoto;
                        personalDetailsVM.AltCustomerPhoto = reg.AltCustomerPhoto;
                        personalDetailsVM.RFSalesDT = reg.RFSalesDT;
                        personalDetailsVM.Photo = reg.Photo;
                        personalDetailsVM.RegSIMSerial = reg.SIMSerial;
                        personalDetailsVM.RegIMEINumber = reg.IMEINumber;
                        personalDetailsVM.CapturedIMEINumber = reg.IMEINumber;
                        personalDetailsVM.RegTypeID = reg.RegTypeID;
                        personalDetailsVM.AgeCheckStatus = reg.AgeCheckStatus;
                        personalDetailsVM.DDMFCheckStatus = reg.DDMFCheckStatus;
                        personalDetailsVM.AddressCheckStatus = reg.AddressCheckStatus;
                        personalDetailsVM.OutStandingCheckStatus = reg.OutStandingCheckStatus;
                        personalDetailsVM.TotalLineCheckStatus = reg.TotalLineCheckStatus;
                        personalDetailsVM.PrincipleLineCheckStatus = reg.PrincipleLineCheckStatus;
                        personalDetailsVM.ContractCheckStatus = reg.ContractCheckStatus;
                        personalDetailsVM.UserType = reg.UserType;
                        personalDetailsVM.PlanAdvance = reg.PlanAdvance;
                        personalDetailsVM.PlanDeposit = reg.PlanDeposit;
                        personalDetailsVM.DeviceAdvance = reg.DeviceAdvance;
                        personalDetailsVM.DeviceDeposit = reg.DeviceDeposit;
                        personalDetailsVM.Discount = reg.Discount;
                        personalDetailsVM.PrePortInCheckStatus = reg.PrePortInCheckStatus;
                        personalDetailsVM.MNPServiceIdCheckStatus = reg.MNPServiceIdCheckStatus;
                        personalDetailsVM.MOC_Status = reg.MOC_Status;
                        personalDetailsVM.Whitelist_Status = reg.Whitelist_Status;
                        personalDetailsVM.externalId = reg.externalId;
                        personalDetailsVM.SIMModel = reg.SimModelId.ToString2() == string.Empty ? "0" : reg.SimModelId.ToString();
                        personalDetailsVM.IsVerified = reg.IsVerified;
                        //Bill Delivery Options
                        //GTM e-Billing CR - Ricky - 2014.09.25
                        personalDetailsVM.EmailBillSubscription = reg.billDeliveryViaEmail ? "Y" : "N";
                        personalDetailsVM.EmailAddress = reg.billDeliveryViaEmail ? reg.billDeliveryEmailAddress : "";
                        personalDetailsVM.SmsAlertFlag = reg.billDeliverySmsNotif ? "Y" : "N";
                        personalDetailsVM.SmsNo = reg.billDeliverySmsNotif ? reg.billDeliverySmsNo : "";
                        personalDetailsVM.BillSubmissionStatus = reg.billDeliverySubmissionStatus != null ? reg.billDeliverySubmissionStatus : "";

                        if (regDetails.lnkregdetails != null)
                        {
                            personalDetailsVM.SIMCardType = regDetails.lnkregdetails.SimCardType;
                            personalDetailsVM.SecondarySIMType = regDetails.lnkregdetails.MismSimCardType;
                        }

                        var regAccount = regDetails.RegAcc;
                        if (regAccount != null)
                        {
                            personalDetailsVM.ServiceActivateErrMsg = regAccount.KenanActivateServiceErrMessage;
                        }

                        isSuppAsNew = regDetails.IsSuppNewAc;
                        Session["isSuppAsNew"] = isSuppAsNew;

                        personalDetailsVM.UpfrontPayment = reg.upfrontPayment;
                        personalDetailsVM.Deposite = reg.AdditionalCharges;
                        personalDetailsVM.StatusID = regDetails.RegStatus.SingleOrDefault().StatusID;

                        using (var configProxy = new ConfigServiceProxy())
                        {
                            personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;
                        }

                        //for cancel reason
                        if (personalDetailsVM.StatusID == 21)
                        {
                            personalDetailsVM.MessageDesc = regDetails.CancelReason;
                        }

                        ExtraTenGetResp objExtraTenDetails = new ExtraTenGetResp();
                        objExtraTenDetails.ExtraTenDetails = regDetails.ExtraTenDetails.ToArray();
                        List<lnkExtraTenDetails> modListExtraTen = new List<lnkExtraTenDetails>();
                        personalDetailsVM.ExtraTenDetails = objExtraTenDetails.ExtraTenDetails.ToList();
                        if (personalDetailsVM.ExtraTenDetails != null && personalDetailsVM.ExtraTenDetails.Count > 0)
                        {
                            foreach (var extraTen in personalDetailsVM.ExtraTenDetails)
                            {
                                if (string.IsNullOrEmpty(extraTen.MsgCode) == true)
                                {
                                    extraTen.MsgCode = "Processing";
                                }
                                else if (extraTen.MsgCode == "0")
                                {
                                    extraTen.MsgCode = "Success";
                                }
                                else
                                {
                                    extraTen.MsgCode = "Fail";
                                }
                                modListExtraTen.Add(extraTen);

                            }
                            personalDetailsVM.ExtraTenDetails = modListExtraTen;
                        }
                        personalDetailsVM.ExtraTenMobileNumbers = string.Join(",", objExtraTenDetails.ExtraTenDetails.Where(e => e.Status == "A").Select(e => e.Msisdn).ToList());
                        Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;

                        // MobileNo
                        var mobileNos = new List<string>();
                        mobileNos.Add(reg.MSISDN1);
                        mobileNos.Add(reg.MSISDN2);
                        Session["RegMobileReg_MobileNo"] = mobileNos;
                        Session["RegMobileReg_VirtualMobileNo_Sec"] = null;
                        // Customer
                        personalDetailsVM.Customer = regDetails.Customers.SingleOrDefault();

                        personalDetailsVM.Address = regDetails.RegAddresses.SingleOrDefault();

                        // Personal Details
                        Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                        regMdlGrpModel = regDetails.RegMdlGrpMdls.SingleOrDefault();
                        if (regMdlGrpModel == null)
                        {
                            Session["RegMobileReg_Type"] = (int)MobileRegType.SuppPlan;
                        }
                        else
                        {
                            Session["RegMobileReg_MainDevicePrice"] = regMdlGrpModel.Price.ToDecimal();
                            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                            Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;
                        }

                        var regPBPCs = new List<RegPgmBdlPkgComp>();
                        regPBPCs = regDetails.RegPgmBdlPkgComps.ToList();
                        if (regPBPCs.Count() > 0)
                            mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                        suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                        var regSuppLineFindCriteria = new RegSuppLineFind()
                        {
                            Active = true,
                            RegSuppLine = new RegSuppLine()
                            {
                                RegID = regID.Value,
                                Active = true
                            }
                        };
                        suppLines = regDetails.SuppLines.ToList();
                    }

                    if (suppLines.Count() > 0)
                        WebHelper.Instance.ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

                    using (var proxy = new CatalogServiceProxy())
                    {
                        string userICNO = string.Empty;
                        if (Util.SessionAccess.User.IDCardNo != null)
                        {
                            userICNO = Util.SessionAccess.User.IDCardNo;
                        }

                        if (userICNO != null && userICNO.Length > 0)
                        {

                            objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                        }
                        if (objWhiteListDetailsResp != null)
                        {
                            WhiteListDetails objWhiteListDetails = new WhiteListDetails();
                            objWhiteListDetails.Description = objWhiteListDetailsResp.Description;
                            if (objWhiteListDetails.Description != null && objWhiteListDetails.Description.Length > 0)
                            {
                                personalDetailsVM._WhiteListDetails = objWhiteListDetails;
                            }
                        }
                        if (regMdlGrpModel != null)
                            Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;

                        var mainLinePBPCs = new List<PgmBdlPckComponent>();
                        if (mainLinePBPCIDs.Count() > 0)
                            mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

                        var bpCode = Properties.Settings.Default.Bundle_Package;
                        var pcCode = Properties.Settings.Default.Package_Component;
                        var DCCode = Properties.Settings.Default.CompType_DataCon;
                        var ECCode = Properties.Settings.Default.Extra_Component;
                        var MAcode = Properties.Settings.Default.Master_Component;
                        var SPcode = Properties.Settings.Default.SupplimentaryPlan;
                        var RCcode = Properties.Settings.Default.Pramotional_Component;
                        var CRCode = Properties.Settings.Default.DiscountDataContract_Component;
                        var ICcCode = Properties.Settings.Default.Device_Insurecomponent;

                        if (mainLinePBPCs.Count() > 0)
                        {
                            var pkgBdlDetails = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == SPcode)).SingleOrDefault();
                            if (pkgBdlDetails != null)
                                Session["RegMobileReg_PkgPgmBdlPkgCompID"] = pkgBdlDetails.ID;

                            var vasIDs = "";
                            var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == CRCode || a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode || a.LinkType == RCcode || a.LinkType == ICcCode).Select(a => a.ID).ToList();
                            foreach (var id in pkgCompIDs)
                            {
                                vasIDs = vasIDs + "," + id;
                            }
                            Session["RegMobileReg_VasIDs"] = vasIDs;
                        }

                    }
                }

                var orderSummaryVM = ConstructOrderSummary(fromSubline: true, standalone: Session[SessionKey.IsSuppleNewAccount.ToString()].ToBool());
                orderSummaryVM.IMEINumber = reg.IMEINumber;
                orderSummaryVM.SIMSerial = reg.SIMSerial;


                if (reg.KenanAccountNo != null)
                { Session["KenanAccountNo"] = reg.KenanAccountNo; }
                else
                { Session["KenanAccountNo"] = "0"; }
                Session["RegMobileReg_OrderSummary"] = orderSummaryVM;

                using (var proxy = new RegistrationServiceProxy())
                {
                    if (regID != null && regID.ToInt() > 0)
                    {
                        DAL.Models.LnkRegDetails lnkregdetails = regDetails.lnkregdetails;

                        if (lnkregdetails != null)
                        {
                            // Added by ravi on 25 sep for waiver Justification
                            personalDetailsVM.Justification = lnkregdetails.Justification.ToString2();
                            // Added by ravi on 25 Sep for waiver Justification Ends Here

                            if (lnkregdetails.SimType != null)
                            {
                                personalDetailsVM.SimType = lnkregdetails.SimType;
                            }
                            else
                            {
                                personalDetailsVM.SimType = "Normal";
                            }

                            personalDetailsVM.ContractCheckCount = regDetails.lnkregdetails.ContractCheckCount.ToInt();

                            /*Active PDPA Commented*/
                            /*Changes by chetan related to PDPA implementation*/
                            string documentversion = lnkregdetails.PdpaVersion;
                            personalDetailsVM.PDPAVersionStatus = documentversion;

                            if (!ReferenceEquals(documentversion, null))
                            {
                                using (var registrationProxy = new RegistrationServiceProxy())
                                {
                                    var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", documentversion);
                                    personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                                }
                            }
                        }
                        else
                        {
                            personalDetailsVM.SimType = "Normal";
                        }
                        Session["SimType"] = personalDetailsVM.SimType;


                    }
                    /*Active PDPA Commented*/
                    else
                    {
                        /*Changes by chetan related to PDPA implementation*/
                        using (var registrationProxy = new RegistrationServiceProxy())
                        {
                            var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                            personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                            personalDetailsVM.PDPDVersion = resultdata.Version;
                        }
                    }
                }

                string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session["ExternalID"].ToString2()) ? string.Empty : Session["ExternalID"].ToString2(), regID.ToInt());
                personalDetailsVM.Liberlization_Status = Result[0];
                personalDetailsVM.MOCStatus = Result[1];
                var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];//Anthony-[#2068]
                //personalDetailsVM.SIMCardType = dropObj.mainFlow.simSize;//Anthony-[#2068]
                if (string.IsNullOrEmpty(personalDetailsVM.SIMCardType))
                {
                    if (!string.IsNullOrEmpty(dropObj.mainFlow.simSize))
                    {
                        personalDetailsVM.SIMCardType = dropObj.mainFlow.simSize;
                    }
                    else
                    {
                        personalDetailsVM.SIMCardType = dropObj.suppFlow[dropObj.suppIndex - 1].simSize;
                    }
                }

                // construct waiver price
                personalDetailsVM = Util.ConstructWaiverPrice(personalDetailsVM, dropObj);

                using (var registrationProxy = new RegistrationServiceClient())
                {
                    personalDetailsVM.CDPUStatus = registrationProxy.GetCDPUStatus(regID.ToInt());
                }

                //CR 29JAN2016 - display bill cycle
                if (regID.ToInt() > 0)
                {
                    if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault() != null)
                    {
                        personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault().ATT_Value;
                    }
                    else
                    {
                        personalDetailsVM.BillingCycle = "N/A";
                    }
                }
                else
                {
                    var selectedMsisdn = Session["SelectedMsisdn"].ToString2();
                    retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                    var selectedAccount = AcctListByICResponse.itemList.Where(c => c.ExternalId == selectedMsisdn).FirstOrDefault();
                    var billCycle = (selectedAccount != null && selectedAccount.AccountDetails != null ? selectedAccount.AccountDetails.BillCycle : "N/A");
                    personalDetailsVM.BillingCycle = (!string.IsNullOrWhiteSpace(billCycle) ? billCycle : "N/A");
                }

                return View(personalDetailsVM);
            }

        }


        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,SMRT_MANAGER,MREG_HQ,MREG_DAB,MREG_DSV,MREG_DSA,MREG_DAI,MREG_DIC,MREG_DAC,MREG_DC,MREG_DSK,MREG_DCH")]
        [DoNoTCache]
        public ActionResult MobileRegSummary(int? regID)
        {
            bool isSuppAsNew = false;
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            if (regID > 0)
                Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session["isSuppAsNew"] = isSuppAsNew;

            //if (ReferenceEquals(Session["RegMobileReg_PersonalDetailsVM"], null) && !ReferenceEquals(Request.UrlReferrer, null))
            //{
            //    if (!(Roles.IsUserInRole("DSupervisor") || Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_C") || Roles.IsUserInRole("MREG_DAI")))
            //    {
            //        if (!Request.UrlReferrer.AbsolutePath.Contains("Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("StoreKeeper") && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegSuccess")
            //            && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegSvcActivated") && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegAccCreated")
            //            && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/SuperVisorForWaiveOff"))
            //        {
            //            return RedirectToAction("Index", "Home");
            //        }
            //    }
            //}

            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            var orderSummary = new OrderSummaryVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            #region VLT ADDED CODE 21stFEB for Email functionality
            Session["RegID"] = regID;
            #endregion VLD ADDED CODE 21stFEB for Email functionality
            var KenanLogDetails = new DAL.Models.KenanaLogDetails();
            RegistrationDetails regDetails = new RegistrationDetails();
			var mainLinePBPCs = new List<PgmBdlPckComponent>();
            if (regID != null && regID.ToInt() > 0)
            {
                WebHelper.Instance.ClearRegistrationSession();

                Session["RegMobileReg_PhoneVM"] = null;
                int paymentstatus = -1;
                using (var proxy = new RegistrationServiceProxy())
                {
                    //Added by Pradeep - Combines multiple service calls to Registration proxy
					regDetails = proxy.GetRegistrationFullDetails(regID.Value);

                    if (regDetails.Customers.FirstOrDefault().IsVIP == 1 && !Roles.IsUserInRole("MREG_SV"))
                    {
                        Session["VIPFailMsg"] = true;
                        return RedirectToAction("StoreKeeper", "Registration");
                    }

					WebHelper.Instance.MapRegistrationDetailsToVM(IsMNPWithMultpleLines, regID.Value.ToInt(), ref regDetails, out personalDetailsVM);

                    reg = regDetails.Regs.SingleOrDefault();

                    // Offer Name - Lindy
                    var suppOfferID = reg.OfferID;
                    var suppOfferName = "";

                    if (suppOfferID > 0)
                    {
                        var suppOffID = proxy.GetOffersById(suppOfferID.ToInt());
                        suppOfferName = suppOffID.OfferName;
                    }

                    Session["suppOfferName"] = suppOfferName;
					
					// this indicator for standalone supp
                    isSuppAsNew = regDetails.IsSuppNewAc;
                    Session["isSuppAsNew"] = isSuppAsNew;
					
					Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;

                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);

                    Session["RegMobileReg_MobileNo"] = mobileNos;
                    Session["RegMobileReg_VirtualMobileNo_Sec"] = null;
                    
                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    regMdlGrpModel = regDetails.RegMdlGrpMdls.SingleOrDefault();
                    if (regMdlGrpModel == null)
                    {
                        Session["RegMobileReg_Type"] = (int)MobileRegType.SuppPlan;
                    }
                    else
                    {
                        Session["RegMobileReg_MainDevicePrice"] = regMdlGrpModel.Price.ToDecimal();
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                        Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;
                    }

                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    regPBPCs = regDetails.RegPgmBdlPkgComps.ToList();
                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var regSuppLineFindCriteria = new RegSuppLineFind()
                    {
                        Active = true,
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value,
                            Active = true
                        }
                    };
                    suppLines = regDetails.SuppLines.ToList();

                    //<Start> - 20150914 - Samuel - display sales code in summary page
                    //personalDetailsVM.Registration = proxy.RegistrationGet(personalDetailsVM.RegID);
                    personalDetailsVM.RegAttributes = proxy.RegAttributesGetByRegID(personalDetailsVM.RegID);
                    /*using (var proxyOrg = new OrganizationServiceProxy())
                    {
                        if (!ReferenceEquals(personalDetailsVM.Registration, null) && !ReferenceEquals(personalDetailsVM.Registration.CenterOrgID, null) && personalDetailsVM.Registration.CenterOrgID != 0)
                        {
                            personalDetailsVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { personalDetailsVM.Registration.CenterOrgID }).FirstOrDefault();
                        }
                    }*/
                    var salesPerson = regDetails.Regs.FirstOrDefault().SalesPerson;
                    var orgID = regDetails.Regs.FirstOrDefault().CenterOrgID;
                    var outletCode = string.Empty;
                    var salesCode = string.Empty;

                    WebHelper.Instance.generateOutletAndSalesCode(orgID, salesPerson, personalDetailsVM.RegAttributes, out outletCode, out salesCode);
                    personalDetailsVM.OutletCode = outletCode;
                    personalDetailsVM.SalesCode = salesCode;
                    //<End> - 20150914 - Samuel - display sales code in summary page
                }

                if (suppLines.Count() > 0)
                    WebHelper.Instance.ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

                using (var proxy = new CatalogServiceProxy())
                {
                    string userICNO = string.Empty;
                    if (Util.SessionAccess.User.IDCardNo != null)
                    {
                        userICNO = Util.SessionAccess.User.IDCardNo;
                    }

                    if (userICNO != null && userICNO.Length > 0)
                    {

                        objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                    }
                    if (objWhiteListDetailsResp != null)
                    {
                        WhiteListDetails objWhiteListDetails = new WhiteListDetails();
                        objWhiteListDetails.Description = objWhiteListDetailsResp.Description;
                        if (objWhiteListDetails.Description != null && objWhiteListDetails.Description.Length > 0)
                        {
                            personalDetailsVM._WhiteListDetails = objWhiteListDetails;
                        }
                    }
                    if (regMdlGrpModel != null)
                        Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;

                    
                    if (mainLinePBPCIDs.Count() > 0)
                        mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var DCCode = Properties.Settings.Default.CompType_DataCon;
                    var ECCode = Properties.Settings.Default.Extra_Component;
                    var MAcode = Properties.Settings.Default.Master_Component;
                    var SPcode = Properties.Settings.Default.SupplimentaryPlan;
                    var RCcode = Properties.Settings.Default.Pramotional_Component;
                    var CRCode = Properties.Settings.Default.DiscountDataContract_Component;
                    var ICcCode = Properties.Settings.Default.Device_Insurecomponent;
					var DFCode = Properties.Settings.Default.Device_Financing_Component;

                    if (mainLinePBPCs.Count() > 0)
                    {
                        var pkgBdlDetails = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == SPcode)).SingleOrDefault();
                        if (pkgBdlDetails != null)
                            Session["RegMobileReg_PkgPgmBdlPkgCompID"] = pkgBdlDetails.ID;
						
                        var vasIDs = "";
                        //Added by chetan to exclusively check DiscountDataContract_Component as CRCode & RCCode
						var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == CRCode || a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode || a.LinkType == RCcode || a.LinkType == ICcCode || a.LinkType == DFCode).Select(a => a.ID).ToList();
                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session["RegMobileReg_VasIDs"] = vasIDs;
                    }

                }
            }

            var orderSummaryVM = ConstructOrderSummary(fromSubline: true);


			#region monthly subscription charge for principal device financing

			//if (regID.ToInt() > 0)
			//{
			//    var upgradeFeeKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_UPGRADEFEE);
			//    var mocWaiverKenanCode = WebHelper.Instance.getDFLOV(Constants.DF_MOC_WAIVER);

			//    if (mainLinePBPCs.Where(x => x.KenanCode == upgradeFeeKenanCode) != null && mainLinePBPCs.Where(x => x.KenanCode == upgradeFeeKenanCode).Any())
			//    {
			//        using (var regProxy = new RegistrationServiceProxy())
			//        {
			//            var objRegAttributes = regProxy.RegAttributesGetByRegID(regID.ToInt());
			//            orderSummaryVM.DFRegAttribute = objRegAttributes;

			//            try
			//            {
			//                bool dfWaiverExist = mainLinePBPCs.Where(x => x.KenanCode == mocWaiverKenanCode) != null ? mainLinePBPCs.Where(x => x.KenanCode == mocWaiverKenanCode).Any() : false;
			//                if (!dfWaiverExist)
			//                {
			//                    orderSummaryVM.MonthlySubscription += objRegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_DF_UPGRADE_FEE).FirstOrDefault().ATT_Value.ToDecimal();
			//                }
			//            }
			//            catch (Exception ex)
			//            {
			//                //
			//            }
			//        }
			//    }

			//}

			#endregion
			
			using (var regProxy = new RegistrationServiceProxy())
			{
				var objRegAttributes = regProxy.RegAttributesGetByRegID(regID.ToInt());
				orderSummaryVM.DFRegAttribute = objRegAttributes;
			}

            orderSummaryVM.IMEINumber = reg.IMEINumber;
            orderSummaryVM.SIMSerial = reg.SIMSerial;

            //14012015 - Anthony - GST Trade Up - Start
            if (!ReferenceEquals(regMdlGrpModel, null))
            {
                orderSummaryVM.SelectedTradeUpComp = new TradeUpComponent()
                {
                    Name = regMdlGrpModel.IsTradeUp.ToString(),
                    Price = regMdlGrpModel.TradeUpAmount.ToInt()
                };
            }

            //14012015 - Anthony - GST Trade Up - End

            if (reg.KenanAccountNo != null)
            { Session["KenanAccountNo"] = reg.KenanAccountNo; }
            else
            { Session["KenanAccountNo"] = "0"; }
            Session["RegMobileReg_OrderSummary"] = orderSummaryVM;

            #region Added by VLT on 21 June 2013 for getting Sim Type
            using (var proxy = new RegistrationServiceProxy())
            {
                if (regID != null && regID.ToInt() > 0)
				{
					#region moved to WebHelper.MapRegistrationDetailsToVM - Drop 5
					/*
					DAL.Models.LnkRegDetails lnkregdetails = regDetails.lnkregdetails;

                    if (lnkregdetails != null)
                    {
                        // Added by ravi on 25 sep for waiver Justification
                        personalDetailsVM.Justification = lnkregdetails.Justification.ToString2();
                        // Added by ravi on 25 Sep for waiver Justification Ends Here

                        if (lnkregdetails.SimType != null)
                        {
                            personalDetailsVM.SimType = lnkregdetails.SimType;
                        }
                        else
                        {
                            personalDetailsVM.SimType = "Normal";
                        }

                        personalDetailsVM.ContractCheckCount = regDetails.lnkregdetails.ContractCheckCount.ToInt();

                        // Active PDPA Commented
                        // Changes by chetan related to PDPA implementation
                        string documentversion = lnkregdetails.PdpaVersion;
                        personalDetailsVM.PDPAVersionStatus = documentversion;

                        if (!ReferenceEquals(documentversion, null))
                        {
                            using (var registrationProxy = new RegistrationServiceProxy())
                            {
                                var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", documentversion);
                                personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                            }
                        }
                    }
                    else
                    {
                        personalDetailsVM.SimType = "Normal";
					}
					*/
					#endregion
					Session["SimType"] = personalDetailsVM.SimType;
                }
                /*Active PDPA Commented*/
                else
                {
                    /*Changes by chetan related to PDPA implementation*/
                    using (var registrationProxy = new RegistrationServiceProxy())
                    {
                        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                        personalDetailsVM.PDPDVersion = resultdata.Version;
                    }
                }
            }
            #endregion

            #region MOC_Status and Liberization status

            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session["ExternalID"].ToString2()) ? string.Empty : Session["ExternalID"].ToString2(), regID.ToInt());
            personalDetailsVM.Liberlization_Status = Result[0];
            personalDetailsVM.MOCStatus = Result[1];

            #endregion
            
            //using (var registrationProxy = new RegistrationServiceClient())
            //{
            //    personalDetailsVM.CDPUStatus = registrationProxy.GetCDPUStatus(regID.ToInt());
            //}

            #region CDPU Approval - drop 5
            using (var registrationProxy = new RegistrationServiceClient())
            {
                if (regID > 0)
                {
                    var details = registrationProxy.GetCDPUDetails(new RegBREStatus() { RegId = regID.ToInt() });
                    if (!ReferenceEquals(details, null))
                    {
                        personalDetailsVM.CDPUStatus = details.TransactionStatus;
                        personalDetailsVM.CDPUDescription = details.TransactionReason;
                        personalDetailsVM.IsLockedBy = details.IsLockedBy;
                    }
                }

            }
            #endregion

            if (!ReferenceEquals(((List<string>)TempData[SessionKey.errorValidationList.ToString()]), null) && ((List<string>)TempData[SessionKey.errorValidationList.ToString()]).Any())
            {
                string inventoryValidation = "Please re-insert the ";

                if (((List<string>)TempData[SessionKey.errorValidationList.ToString()]).Count() > 1)
                {
                    foreach (var errorMsg in ((List<string>)TempData[SessionKey.errorValidationList.ToString()]))
                    {
                        switch (errorMsg)
                        {
                            case "Sim serial": ModelState.Remove("RegSIMSerial"); break;
                            case "IMEI number": ModelState.Remove("RegIMEINumber"); break;
                            //case "MISM Sim serial": ModelState.Remove("RegSIMSerialSeco"); break;
                        }

                        inventoryValidation += errorMsg + " and ";
                    }
                }
                else
                { // only 1 error
                    inventoryValidation += ((List<string>)TempData[SessionKey.errorValidationList.ToString()]).FirstOrDefault();
                }
                if (inventoryValidation.EndsWith(" and "))
                {
                    inventoryValidation = inventoryValidation.Substring(0, inventoryValidation.Length - 5);
                }
                ModelState.AddModelError(string.Empty, inventoryValidation);
            }

            //CR 29JAN2016 - display bill cycle
            if (regID.ToInt() > 0)
            {
                if (personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault() != null)
                {
                    personalDetailsVM.BillingCycle = personalDetailsVM.RegAttributes.Where(x => x.ATT_Name == RegAttributes.ATTRIB_ACC_BILL_CYCLE && x.RegID == regID).FirstOrDefault().ATT_Value;
                }
                else
                {
                    personalDetailsVM.BillingCycle = "N/A";
                }
            }
            else
            {
                var selectedMsisdn = Session["SelectedMsisdn"].ToString2();
                retrieveAcctListByICResponse AcctListByICResponse = (retrieveAcctListByICResponse)Session["PPIDInfo"];
                var selectedAccount = AcctListByICResponse.itemList.Where(c => c.ExternalId == selectedMsisdn).FirstOrDefault();
                var billCycle = (selectedAccount != null && selectedAccount.AccountDetails != null ? selectedAccount.AccountDetails.BillCycle : "N/A");
                personalDetailsVM.BillingCycle = (!string.IsNullOrWhiteSpace(billCycle) ? billCycle : "N/A");
            }

            return View(personalDetailsVM);
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,SMRT_MANAGER,MREG_HQ,MREG_DAB,DSupervisor,DAgent,MREG_DSV,MREG_DSA,DREG_N,DREG_C,DREG_CH,MREG_DAI,MREG_DIC,MREG_DAC")]
        [DoNoTCache]
        public ActionResult DMobileRegSummary(int? regID)
        {
            bool isSuppAsNew = false;
            if (regID > 0)
                Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session["isSuppAsNew"] = isSuppAsNew;

            if (ReferenceEquals(Session["RegMobileReg_PersonalDetailsVM"], null) && !ReferenceEquals(Request.UrlReferrer, null))
            {
                if (!(Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_C") || Roles.IsUserInRole("MREG_DAI")))
                {
                    if (!Request.UrlReferrer.AbsolutePath.Contains("/Registration/CDPUDashBoard") && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/OrderStatusReport") && !Request.UrlReferrer.AbsolutePath.Contains("Cashier") && !Request.UrlReferrer.AbsolutePath.Contains("StoreKeeper") && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegSuccess")
                        && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegSvcActivated") && !Request.UrlReferrer.AbsolutePath.Contains("MobileRegAccCreated")
                        && !Request.UrlReferrer.AbsolutePath.Contains("/Registration/SuperVisorForWaiveOff"))
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            var personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            var orderSummary = new OrderSummaryVM();
            var regMdlGrpModel = new RegMdlGrpModel();
            var mdlGrpModelIDs = new List<int>();
            var mainLinePBPCIDs = new List<int>();
            var suppLineRegPBPCs = new List<RegPgmBdlPkgComp>();
            var suppLines = new List<RegSuppLine>();
            var reg = new DAL.Models.Registration();
            var objWhiteListDetailsResp = new WhiteListDetailsResp();
            #region VLT ADDED CODE 21stFEB for Email functionality
            Session["RegID"] = regID;
            #endregion VLD ADDED CODE 21stFEB for Email functionality
            var KenanLogDetails = new DAL.Models.KenanaLogDetails();
            RegistrationDetails regDetails = new RegistrationDetails();
            if (regID != null && regID.ToInt() > 0)
            {
                WebHelper.Instance.ClearRegistrationSession();
                personalDetailsVM = new PersonalDetailsVM()
                {
                    RegID = regID.Value.ToInt(),
                };

                Session["RegMobileReg_PhoneVM"] = null;
                int paymentstatus = -1;
                using (var proxy = new RegistrationServiceProxy())
                {
                    //Added by Pradeep - Combines multiple service calls to Registration proxy
                    regDetails = proxy.GetRegistrationFullDetails(personalDetailsVM.RegID);

                    reg = regDetails.Regs.SingleOrDefault();
                    KenanLogDetails = regDetails.LogDetails.FirstOrDefault();
                    if (!ReferenceEquals(KenanLogDetails, null))
                    {
                        personalDetailsVM.MessageCode = KenanLogDetails.MessageCode.ToInt();
                        personalDetailsVM.MessageDesc = KenanLogDetails.MessageDesc.ToString2();
                        personalDetailsVM.ServiceMethodName = KenanLogDetails.MethodName.ToString2();
                    }
                    paymentstatus = regDetails.PaymentStatus;
                    if (paymentstatus != -1)
                    {
                        if (paymentstatus == 0)
                        {
                            personalDetailsVM.PaymentStatus = "0";
                        }
                        else
                        {
                            personalDetailsVM.PaymentStatus = "1";
                        }
                    }
                    else
                    {
                        personalDetailsVM.PaymentStatus = "0";
                    }
                    personalDetailsVM.QueueNo = reg.QueueNo;
                    personalDetailsVM.Remarks = reg.Remarks;
                    personalDetailsVM.BiometricVerify = reg.BiometricVerify;
                    personalDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
                    personalDetailsVM.BlacklistInternal = reg.InternalBlacklisted;
                    personalDetailsVM.SignatureSVG = reg.SignatureSVG;
                    personalDetailsVM.CustomerPhoto = reg.CustomerPhoto;
                    personalDetailsVM.AltCustomerPhoto = reg.AltCustomerPhoto;
                    personalDetailsVM.Photo = reg.Photo;
                    personalDetailsVM.RegSIMSerial = reg.SIMSerial;
                    personalDetailsVM.RegIMEINumber = reg.IMEINumber;
                    personalDetailsVM.CapturedIMEINumber = reg.IMEINumber;
                    personalDetailsVM.RegTypeID = reg.RegTypeID;
                    personalDetailsVM.AgeCheckStatus = reg.AgeCheckStatus;
                    personalDetailsVM.DDMFCheckStatus = reg.DDMFCheckStatus;
                    personalDetailsVM.AddressCheckStatus = reg.AddressCheckStatus;
                    personalDetailsVM.OutStandingCheckStatus = reg.OutStandingCheckStatus;
                    personalDetailsVM.TotalLineCheckStatus = reg.TotalLineCheckStatus;
                    personalDetailsVM.PrincipleLineCheckStatus = reg.PrincipleLineCheckStatus;
                    personalDetailsVM.ContractCheckStatus = reg.ContractCheckStatus;
                    personalDetailsVM.UserType = reg.UserType;
                    personalDetailsVM.PlanAdvance = reg.PlanAdvance;
                    personalDetailsVM.PlanDeposit = reg.PlanDeposit;
                    personalDetailsVM.DeviceAdvance = reg.DeviceAdvance;
                    personalDetailsVM.DeviceDeposit = reg.DeviceDeposit;
                    personalDetailsVM.Discount = reg.Discount;
                    personalDetailsVM.PrePortInCheckStatus = reg.PrePortInCheckStatus;
                    personalDetailsVM.MNPServiceIdCheckStatus = reg.MNPServiceIdCheckStatus;
                    personalDetailsVM.MOC_Status = reg.MOC_Status;
                    personalDetailsVM.Whitelist_Status = reg.Whitelist_Status;
                    personalDetailsVM.externalId = reg.externalId;
                    personalDetailsVM.SIMModel = reg.SimModelId.ToString2() == string.Empty ? "0" : reg.SimModelId.ToString();
                    personalDetailsVM.IsVerified = reg.IsVerified;

                    if (regDetails.lnkregdetails != null)
                    {
                        //**getting sim card type start**//
                        personalDetailsVM.SIMCardType = regDetails.lnkregdetails.SimCardType;
                        personalDetailsVM.SecondarySIMType = regDetails.lnkregdetails.MismSimCardType;
                    }
                    //**getting sim card type end**//

                    var regAccount = regDetails.RegAcc;
                    if (regAccount != null)
                    {
                        personalDetailsVM.ServiceActivateErrMsg = regAccount.KenanActivateServiceErrMessage;
                    }

                    isSuppAsNew = regDetails.IsSuppNewAc;
                    Session["isSuppAsNew"] = isSuppAsNew;

                    personalDetailsVM.UpfrontPayment = reg.upfrontPayment;
                    personalDetailsVM.Deposite = reg.AdditionalCharges;
                    personalDetailsVM.StatusID = regDetails.RegStatus.SingleOrDefault().StatusID;

                    using (var configProxy = new ConfigServiceProxy())
                    {
                        personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;
                    }

                    //for cancel reason
                    if (personalDetailsVM.StatusID == 21)
                    {
                        personalDetailsVM.MessageDesc = regDetails.CancelReason;
                    }

                    ExtraTenGetResp objExtraTenDetails = new ExtraTenGetResp();
                    //objExtraTenDetails = proxy.GetExtraTenDetails(personalDetailsVM.RegID);
                    objExtraTenDetails.ExtraTenDetails = regDetails.ExtraTenDetails.ToArray();
                    List<lnkExtraTenDetails> modListExtraTen = new List<lnkExtraTenDetails>();
                    personalDetailsVM.ExtraTenDetails = objExtraTenDetails.ExtraTenDetails.ToList();
                    if (personalDetailsVM.ExtraTenDetails != null && personalDetailsVM.ExtraTenDetails.Count > 0)
                    {
                        foreach (var extraTen in personalDetailsVM.ExtraTenDetails)
                        {
                            if (string.IsNullOrEmpty(extraTen.MsgCode) == true)
                            {
                                extraTen.MsgCode = "Processing";
                            }
                            else if (extraTen.MsgCode == "0")
                            {
                                extraTen.MsgCode = "Success";
                            }
                            else
                            {
                                extraTen.MsgCode = "Fail";
                            }
                            modListExtraTen.Add(extraTen);

                        }
                        personalDetailsVM.ExtraTenDetails = modListExtraTen;
                    }
                    personalDetailsVM.ExtraTenMobileNumbers = string.Join(",", objExtraTenDetails.ExtraTenDetails.Where(e => e.Status == "A").Select(e => e.Msisdn).ToList());
                    Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;
                    //}

                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);
                    Session["RegMobileReg_MobileNo"] = mobileNos;
                    Session["RegMobileReg_VirtualMobileNo_Sec"] = null;
                    // Customer
                    personalDetailsVM.Customer = regDetails.Customers.SingleOrDefault();

                    personalDetailsVM.Address = regDetails.RegAddresses.SingleOrDefault();

                    // Personal Details
                    Session["RegMobileReg_PersonalDetailsVM"] = personalDetailsVM;

                    regMdlGrpModel = regDetails.RegMdlGrpMdls.SingleOrDefault();
                    if (regMdlGrpModel == null)
                    {
                        Session["RegMobileReg_Type"] = (int)MobileRegType.SuppPlan;
                    }
                    else
                    {
                        Session["RegMobileReg_MainDevicePrice"] = regMdlGrpModel.Price.ToDecimal();
                        Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = regMdlGrpModel.ModelImageID;
                        Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;
                    }

                    var regPBPCs = new List<RegPgmBdlPkgComp>();
                    regPBPCs = regDetails.RegPgmBdlPkgComps.ToList();
                    if (regPBPCs.Count() > 0)
                        mainLinePBPCIDs = regPBPCs.Where(a => a.RegSuppLineID == null).Distinct().Select(a => a.PgmBdlPckComponentID).ToList();

                    // Sub Line RegPgmBdlPkgComponent
                    suppLineRegPBPCs = regPBPCs.Where(a => a.RegSuppLineID != null).ToList();

                    // RegSuppLine
                    var regSuppLineFindCriteria = new RegSuppLineFind()
                    {
                        Active = true,
                        RegSuppLine = new RegSuppLine()
                        {
                            RegID = regID.Value,
                            Active = true
                        }
                    };
                    suppLines = regDetails.SuppLines.ToList();

                    //<Start> - 20150914 - Samuel - display sales code in summary page
                    personalDetailsVM.Registration = proxy.RegistrationGet(personalDetailsVM.RegID);
                    personalDetailsVM.RegAttributes = proxy.RegAttributesGetByRegID(personalDetailsVM.RegID);
                    using (var proxyOrg = new OrganizationServiceProxy())
                    {
                        if (!ReferenceEquals(personalDetailsVM.Registration, null) && !ReferenceEquals(personalDetailsVM.Registration.CenterOrgID, null) && personalDetailsVM.Registration.CenterOrgID != 0)
                        {
                            personalDetailsVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { personalDetailsVM.Registration.CenterOrgID }).FirstOrDefault();
                        }
                    }
                    //<End> - 20150914 - Samuel - display sales code in summary page
                }

                if (suppLines.Count() > 0)
                    WebHelper.Instance.ConstructSuppLineSummary(suppLines, suppLineRegPBPCs);

                using (var proxy = new CatalogServiceProxy())
                {
                    string userICNO = string.Empty;
                    if (Util.SessionAccess.User.IDCardNo != null)
                    {
                        userICNO = Util.SessionAccess.User.IDCardNo;
                    }

                    if (userICNO != null && userICNO.Length > 0)
                    {

                        objWhiteListDetailsResp = proxy.GetWhilteListDetails(userICNO);
                    }
                    if (objWhiteListDetailsResp != null)
                    {
                        WhiteListDetails objWhiteListDetails = new WhiteListDetails();
                        objWhiteListDetails.Description = objWhiteListDetailsResp.Description;
                        if (objWhiteListDetails.Description != null && objWhiteListDetails.Description.Length > 0)
                        {
                            personalDetailsVM._WhiteListDetails = objWhiteListDetails;
                        }
                    }
                    if (regMdlGrpModel != null)
                        Session["RegMobileReg_SelectedModelID"] = proxy.BrandArticleImageGet(regMdlGrpModel.ModelImageID).ModelID;

                    var mainLinePBPCs = new List<PgmBdlPckComponent>();
                    if (mainLinePBPCIDs.Count() > 0)
                        mainLinePBPCs = MasterDataCache.Instance.FilterComponents(mainLinePBPCIDs).ToList();

                    var bpCode = Properties.Settings.Default.Bundle_Package;
                    var pcCode = Properties.Settings.Default.Package_Component;
                    var DCCode = Properties.Settings.Default.CompType_DataCon;
                    var ECCode = Properties.Settings.Default.Extra_Component;
                    var MAcode = Properties.Settings.Default.Master_Component;
                    var SPcode = Properties.Settings.Default.SupplimentaryPlan;
                    var RCcode = Properties.Settings.Default.Pramotional_Component;
                    var CRCode = Properties.Settings.Default.DiscountDataContract_Component;
                    var ICcCode = Properties.Settings.Default.Device_Insurecomponent;

                    if (mainLinePBPCs.Count() > 0)
                    {
                        var pkgBdlDetails = mainLinePBPCs.Where(a => a.LinkType == bpCode && (a.PlanType == SPcode)).SingleOrDefault();
                        if (pkgBdlDetails != null)
                            Session["RegMobileReg_PkgPgmBdlPkgCompID"] = pkgBdlDetails.ID;



                        var vasIDs = "";
                        //Added by chetan to exclusively check DiscountDataContract_Component as CRCode & RCCode
                        var pkgCompIDs = mainLinePBPCs.Where(a => a.LinkType == CRCode || a.LinkType == pcCode || a.LinkType == MAcode || a.LinkType == DCCode || a.LinkType == ECCode || a.LinkType == RCcode || a.LinkType == ICcCode).Select(a => a.ID).ToList();
                        foreach (var id in pkgCompIDs)
                        {
                            vasIDs = vasIDs + "," + id;
                        }
                        Session["RegMobileReg_VasIDs"] = vasIDs;
                    }

                }
            }

            var orderSummaryVM = ConstructOrderSummary(fromSubline: true);
            orderSummaryVM.IMEINumber = reg.IMEINumber;
            orderSummaryVM.SIMSerial = reg.SIMSerial;


            if (reg.KenanAccountNo != null)
            { Session["KenanAccountNo"] = reg.KenanAccountNo; }
            else
            { Session["KenanAccountNo"] = "0"; }
            Session["RegMobileReg_OrderSummary"] = orderSummaryVM;

            #region Added by VLT on 21 June 2013 for getting Sim Type
            using (var proxy = new RegistrationServiceProxy())
            {
                if (regID != null && regID.ToInt() > 0)
                {
                    DAL.Models.LnkRegDetails lnkregdetails = regDetails.lnkregdetails;

                    if (lnkregdetails != null)
                    {
                        // Added by ravi on 25 sep for waiver Justification
                        personalDetailsVM.Justification = lnkregdetails.Justification.ToString2();
                        // Added by ravi on 25 Sep for waiver Justification Ends Here

                        if (lnkregdetails.SimType != null)
                        {
                            personalDetailsVM.SimType = lnkregdetails.SimType;
                        }
                        else
                        {
                            personalDetailsVM.SimType = "Normal";
                        }

                        personalDetailsVM.ContractCheckCount = regDetails.lnkregdetails.ContractCheckCount.ToInt();

                        /*Active PDPA Commented*/
                        /*Changes by chetan related to PDPA implementation*/
                        string documentversion = lnkregdetails.PdpaVersion;
                        personalDetailsVM.PDPAVersionStatus = documentversion;

                        if (!ReferenceEquals(documentversion, null))
                        {
                            using (var registrationProxy = new RegistrationServiceProxy())
                            {
                                var resultdata = registrationProxy.GetPDPADataInfoWithVersion("TANDC", "R", documentversion);
                                personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                            }
                        }
                    }
                    else
                    {
                        personalDetailsVM.SimType = "Normal";
                    }
                    Session["SimType"] = personalDetailsVM.SimType;


                }
                /*Active PDPA Commented*/
                else
                {
                    /*Changes by chetan related to PDPA implementation*/
                    using (var registrationProxy = new RegistrationServiceProxy())
                    {
                        var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                        personalDetailsVM.DocumentUrl = resultdata.DocumentUrl;
                        personalDetailsVM.PDPDVersion = resultdata.Version;
                    }
                }
            }
            #endregion

            #region MOC_Status and Liberization status

            string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session["ExternalID"].ToString2()) ? string.Empty : Session["ExternalID"].ToString2(), regID.ToInt());
            personalDetailsVM.Liberlization_Status = Result[0];
            personalDetailsVM.MOCStatus = Result[1];

            #endregion

            using (var registrationProxy = new RegistrationServiceClient())
            {
                personalDetailsVM.CDPUStatus = registrationProxy.GetCDPUStatus(regID.ToInt());
            }

            return View(personalDetailsVM);
        }


        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = "MREG_DAPP,MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSV,MREG_DSA,DREG_N,DREG_C,DREG_CH,MREG_DIC,MREG_DAI,MREG_DAC,MREG_DC,MREG_DSK")]
        public ActionResult MobileRegSummary(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            bool isSuppAsNew = false;
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

            DateTime? TimeApproval = null;
            if (!ReferenceEquals(Session[SessionKey.TimeApproval.ToString()], null))
            {
                TimeApproval = (DateTime)Session[SessionKey.TimeApproval.ToString()];
            }
            var resp = new RegistrationCreateResp();
            var response = new KenanNewSuppLineResponse();
            var reg = new DAL.Models.Registration();
            bool isSuccess = false;
            Session["RegMobileReg_TabIndex"] = personalDetailsVM.TabNumber;
            Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;
            PersonalDetailsVM PersonalDetailsVM_obj = null;

            if (Session["RegMobileReg_PersonalDetailsVM"] != null)
            {
                PersonalDetailsVM_obj = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
            }

            try
            {

                switch (collection["submit1"].ToString2())
                {
                    case "PaymentRecived":
                        {
                            #region PaymentRecived

                            using (var proxy = new RegistrationServiceProxy())
                            {
                                proxy.RegistrationCancel(new RegStatus()
                                {
                                    RegID = personalDetailsVM.RegID,
                                    Active = true,
                                    CreateDT = DateTime.Now,
                                    StartDate = DateTime.Now,
                                    LastAccessID = Util.SessionAccess.UserName,
                                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                                });
                            }

                            using (var proxy = new RegistrationServiceProxy())
                            {
                                isSuppAsNew = proxy.getSupplineDetails(personalDetailsVM.RegID);
                            }
                            if (isSuppAsNew)
                            {
                                var request = new OrderFulfillRequest()
                                {
                                    OrderID = personalDetailsVM.RegID.ToString(),
                                    UserSession = Util.SessionAccess.UserName,

                                };
                                using (var proxy = new KenanServiceProxy())
                                {
                                    isSuccess = proxy.KenanAccountFulfill(request);
                                }
                                if (isSuccess)
                                {
                                    return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                                }
                                else
                                {
                                    return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
                                }
                            }
                            else if (PersonalDetailsVM_obj.RegTypeID == 11)
                            {
                                #region For MNP Flow

                                using (var proxy = new KenanServiceProxy())
                                {

                                    var request1 = new MNPtypeFullfillCenterOrderMNP()
                                    {
                                        orderId = personalDetailsVM.RegID.ToString(),
                                    };
                                    bool statusMNPSup = proxy.KenanMNPAccountFulfill(request1);
                                    if (statusMNPSup)
                                    {
                                        return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                                    }
                                    else
                                    {
                                        using (var regproxy = new RegistrationServiceProxy())
                                        {
                                            regproxy.RegistrationClose(new RegStatus()
                                            {
                                                RegID = personalDetailsVM.RegID,
                                                Active = true,
                                                CreateDT = DateTime.Now,
                                                StartDate = DateTime.Now,
                                                LastAccessID = Util.SessionAccess.UserName,
                                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail),
                                                ResponseCode = response.Code,
                                                ResponseDescription = response.Message,
                                                MethodName = "KenanAdditionLineRegistration"
                                            });
                                        }
                                        return RedirectToAction("MobileRegWMFail", new { regID = personalDetailsVM.RegID });
                                    }
                                }

                                #endregion
                            }
                            else
                            {
                                #region For Normal Flow
                                response = FulfillKenanAccount(personalDetailsVM.RegID);
                                return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });


                                #endregion
                            }

                            #endregion
                        }
                        break;
                    case "close":
                        {
                            #region close
                            if (personalDetailsVM.RegID == 0)
                            {
                                using (var proxy = new RegistrationServiceProxy())
                                {
                                    resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString2() : "false")),
                                    WebHelper.Instance.ConstructCustomer(PersonalDetailsVM_obj), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress(PersonalDetailsVM_obj),
                                    WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
                                    WebHelper.Instance.ConstructRegSuppLineVASes(), null, null, personalDetailsVM.isBREFail, dropObj.ApproverID, TimeApproval);
                                    personalDetailsVM.RegID = resp.ID;
                                }

                                using (var proxy = new RegistrationServiceProxy())
                                {
                                    proxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegBreFail)
                                    });
                                }
                                return RedirectToAction("MobileRegBreFail", new { regID = personalDetailsVM.RegID });
                            }
                            else
                            {
                                using (var proxy = new RegistrationServiceProxy())
                                {
                                    proxy.RegistrationClose(new RegStatus()
                                    {
                                        RegID = personalDetailsVM.RegID,
                                        Active = true,
                                        CreateDT = DateTime.Now,
                                        StartDate = DateTime.Now,
                                        LastAccessID = Util.SessionAccess.UserName,
                                        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                                    });
                                }
                                return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
                            }


                            #endregion
                        }
                        break;
                    case "cancel":
                        {
                            #region cancel

                            string reason = string.Empty;
                            int prevStatus = WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID);
                            reason = ReferenceEquals(reason, null) ? "No reason provided." : collection["hdnReasonCancel"].ToString2();
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                proxy.RegistrationCancel(new RegStatus()
                                {
                                    RegID = personalDetailsVM.RegID,
                                    Active = true,
                                    CreateDT = DateTime.Now,
                                    StartDate = DateTime.Now,
                                    LastAccessID = Util.SessionAccess.UserName,
                                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)

                                });

                                proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                                {
                                    RegID = personalDetailsVM.RegID,
                                    CancelReason = reason,
                                    CreateDT = DateTime.Now,
                                    LastUpdateDT = DateTime.Now,
                                    LastAccessID = Util.SessionAccess.UserName
                                });
                                var totalPrice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID);
                                if (prevStatus == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment) && WebHelper.Instance.GetRegStatus(personalDetailsVM.RegID) == Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan) && totalPrice > 0)
                                {
                                    WebHelper.Instance.SendDetailsToWebPos(personalDetailsVM.RegID, Constants.StatusWebPosToCancel);
                                }
                            }

                            return RedirectToAction("MobileRegCanceled", new { regID = personalDetailsVM.RegID });


                            #endregion
                        }
                        break;
                    case "createAcc":
                        {
                            #region createAcc
                            reg = new DAL.Models.Registration();
                            var regsec = new DAL.Models.RegistrationSec();
                            //var regMdlGrpModels = new List<RegMdlGrpModel>();
                            OrderSummaryVM orderSummaryVM = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                                //regsec = proxy.RegistrationSecGet(personalDetailsVM.RegID);
                            }

                            List<string> errorValidationList = new List<string>();
                            if (string.IsNullOrEmpty(reg.SIMSerial))
                            {
                                // fill error msg
                                errorValidationList.Add("Sim serial");
                            }
                            if (!ReferenceEquals(orderSummaryVM,null))
                            {
                                if (string.IsNullOrEmpty(reg.IMEINumber) && !string.IsNullOrEmpty(orderSummaryVM.ModelImageUrl))
                                {
                                    // fill error msg
                                    errorValidationList.Add("IMEI number");
                                }
                            }
                            if (errorValidationList.Any())
                            {
                                TempData[SessionKey.errorValidationList.ToString()] = errorValidationList;
                                using (var proxy = new UserServiceProxy())
                                {
                                    String errorTrace = "fail insert inventory" + (String)(errorValidationList.Contains("Sim serial") ? " SIM(" + personalDetailsVM.RegSIMSerial + ") " : "") + (String)(errorValidationList.Contains("IMEI number") ? " IMEI(" + personalDetailsVM.RegIMEINumber + ") " : "") + "for regID: " + reg.ID;
                                    proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), Util.GetRegTypeID(REGTYPE.SuppPlan), errorTrace, "", "");
                                }
                                return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                            }
                            string username = string.Empty;
                            #region CDPU Approval - status to pending cdpu approval/cdpu rejected
                            if (Session["IsDealer"].ToBool() || Roles.IsUserInRole("MREG_DAPP"))
                            {
                                if (personalDetailsVM.CDPUStatus == Constants.StatusCDPUPending)  // have bre failed
                                {
                                    //Update Order Status
                                    using (var proxy = new RegistrationServiceProxy())
                                    {
                                        proxy.RegistrationCancel(new RegStatus()
                                        {
                                            RegID = personalDetailsVM.RegID,
                                            Active = true,
                                            CreateDT = DateTime.Now,
                                            StartDate = DateTime.Now,
                                            LastAccessID = Util.SessionAccess.UserName,
                                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCDPUPendingApp)
                                        });
                                    }
                                    return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                                }
                                if (personalDetailsVM.CDPUStatus == Constants.StatusCDPURejected)
                                {
                                    GeneralResult result = WebHelper.Instance.UnreserveNumberDealerNet(personalDetailsVM.RegID);
                                    if (!ReferenceEquals(result, null))
                                    {
                                        if (result.ResultCode == 0 && result.ResultMessage == "success")
                                        {
                                            using (var proxy = new RegistrationServiceProxy())
                                            {
                                                proxy.RegistrationCancel(new RegStatus()
                                                {
                                                    RegID = personalDetailsVM.RegID,
                                                    Active = true,
                                                    CreateDT = DateTime.Now,
                                                    StartDate = DateTime.Now,
                                                    LastAccessID = Util.SessionAccess.UserName,
                                                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCDPURejected)
                                                });

                                                proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
                                                {
                                                    RegID = personalDetailsVM.RegID,
                                                    CancelReason = Properties.Settings.Default.Status_RegCDPURejected,
                                                    CreateDT = DateTime.Now,
                                                    LastUpdateDT = DateTime.Now,
                                                    LastAccessID = Util.SessionAccess.UserName
                                                });

                                                proxy.UpsertBRE(new RegBREStatus()
                                                {
                                                    RegId = personalDetailsVM.RegID,
                                                    TransactionStatus = Constants.StatusCDPURejected,
                                                    TransactionReason = personalDetailsVM.CDPUDescription,
                                                    TransactionDT = DateTime.Now,
                                                    TransactionBy = Util.SessionAccess.UserName
                                                });
                                            }
                                            return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                        }
                                    }
                                    else
                                    {
                                        return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                                    }
                                }
                                if (personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove)
                                {
                                    using (var proxy = new RegistrationServiceProxy())
                                    {
                                        proxy.UpsertBRE(new RegBREStatus()
                                        {
                                            RegId = personalDetailsVM.RegID,
                                            TransactionStatus = Constants.StatusCDPUApprove,
                                            TransactionReason = personalDetailsVM.CDPUDescription,
                                            TransactionDT = DateTime.Now,
                                            TransactionBy = Util.SessionAccess.UserName
                                        });
                                    }
                                }
                            }
                            #endregion

                            #region "sales person update"
                            using (var proxy = new RegistrationServiceProxy())
                            {
                                if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
                                {
                                    username = Request.Cookies["CookieUser"].Value;
                                }

                                proxy.RegistrationUpdate(new DAL.Models.Registration()
                                {
                                    ID = personalDetailsVM.RegID,
                                    //SalesPerson = username,
                                    LastAccessID = Util.SessionAccess.UserName,

                                    //SimModelId = ReferenceEquals(Session["SimModelTypeId"].ToString2(), null) ? 0 : Convert.ToInt32(Session["SimModelTypeId"].ToString2()),

                                });
                            }

                            #endregion


                            using (var proxy = new RegistrationServiceProxy())
                            {
                                reg = proxy.RegistrationGet(personalDetailsVM.RegID);
                                personalDetailsVM.RegTypeID = reg.RegTypeID;
                            }

                            bool isCDPUorderRecs = WebHelper.Instance.isCDPUorder(personalDetailsVM.RegID);

                            if (!isCDPUorderRecs)
                            {
                                try
                                {
                                    using (var proxy = new UpdateServiceProxy())
                                    {
                                        if (!proxy.FindDocument(Constants.CONTRACT, personalDetailsVM.RegID))
                                        {
											// generate PDF and store into db after input sim / imei
											String contractHTMLSource = string.Empty;
											contractHTMLSource = Util.RenderPartialViewToString(this, "PrintContractDetails", WebHelper.Instance.GetContractDetailsViewModel(personalDetailsVM.RegID, false));
											contractHTMLSource = contractHTMLSource.Replace("rfTable rfGapTable", "rfTable");
											contractHTMLSource = contractHTMLSource.Replace("rfTableSk rfGapTable", "rfTableSk");
											contractHTMLSource = contractHTMLSource.Replace("trSectionGap", "trSectionHide");

                                            //06 aug 2015 - Samuel - keep in html if standalone suppline
                                            using (var proxyobj = new RegistrationServiceProxy())
                                            {
                                                //Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                                                DAL.Models.LnkRegDetails lnkregdetails = proxyobj.LnkRegistrationGetByRegId(personalDetailsVM.RegID.ToInt());

                                                if (lnkregdetails.IsSuppNewAc == false)
                                                {
                                                    Byte[] PDFBytes = Util.convertHTMLStringToPDFByteArray(contractHTMLSource, personalDetailsVM.RegID);
                                                }
                                                else if (lnkregdetails.IsSuppNewAc == true)
                                                {
                                                    using (var service = new UpdateServiceProxy())
                                                    {
                                                        byte[] HTMLSource = System.Text.Encoding.ASCII.GetBytes(contractHTMLSource);
                                                        RegUploadDoc objRegUploadDoc = new RegUploadDoc();
                                                        objRegUploadDoc.FileName = personalDetailsVM.RegID + ".html";
                                                        objRegUploadDoc.FileType = "Contract";
                                                        objRegUploadDoc.RegID = personalDetailsVM.RegID;
                                                        objRegUploadDoc.FileStream = HTMLSource;

                                                        try
                                                        {
                                                            service.SaveDocument(objRegUploadDoc);
                                                            Logger.Info(string.Format("HTML SUCCESS {0}-{1}", personalDetailsVM.RegID, "HTML"));
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Logger.Error("Failed to save contract HTML source:" + ex);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error("ICONTRACT EXCEPTION = " + ex);
                                    ;
                                }
                            }
                            else
                            {
                                bool isFilecreated = WebHelper.Instance.GenerateCDPUcontractFile(personalDetailsVM.RegID);
                            }
                            

                            // this is for add mism auto-knockoff
                            // drop 4 - Gry
                            if (personalDetailsVM.RegTypeID == (int)MobileRegType.SecPlan)
                            {
                                using (var autoProxy = new AutoActivationServiceProxy())
                                {
                                    autoProxy.KenanAccountFulfill((int)MobileRegType.SecPlan, personalDetailsVM.RegID.ToString2());


                                    if (Roles.IsUserInRole("MREG_DAPP"))
                                    {
                                        return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                    }
                                    else { return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID }); }
                                }

                            }

							using (var proxy = new RegistrationServiceProxy())
							{
								isSuppAsNew = proxy.getSupplineDetails(personalDetailsVM.RegID);
							}
                            var totalprice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID);
							if (isSuppAsNew)
							{
								WebHelper.Instance.CreateKenanAccount(personalDetailsVM.RegID);
                                if (Roles.IsUserInRole("MREG_DAPP"))
                                {
                                    return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                }
                                return RedirectToAction("StoreKeeper", "Registration");
                                
							}
							else if (personalDetailsVM.RegTypeID == 11)
							{
								#region For MNP Flow
                                response = FulfillKenanAccount(personalDetailsVM.RegID);
								if ((response.Message == "success") && (response.Code == "0"))
								{
									using (var regproxy = new RegistrationServiceProxy())
									{
										regproxy.RegistrationClose(new RegStatus()
										{
											RegID = personalDetailsVM.RegID,
											Active = true,
											CreateDT = DateTime.Now,
											StartDate = DateTime.Now,
											LastAccessID = Util.SessionAccess.UserName,
											StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate),
											ResponseCode = response.Code,
											ResponseDescription = response.Message,
											MethodName = "KenanAdditionLineRegistration"
										});
                                    }
                                    //var totalprice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID);
                                    //if (totalprice == 0 && (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove))
                                    //{
                                    //    WebHelper.Instance.PaymentReceived(personalDetailsVM);
                                    //    WebHelper.Instance.SaveUserTransactionLogs(personalDetailsVM.RegID);
                                    //}
                                    if (Roles.IsUserInRole("MREG_DAPP"))
                                    {
                                        return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                    }
                                    return RedirectToAction("StoreKeeper", "Registration");
								}
								else
								{
									using (var proxy = new RegistrationServiceProxy())
									{
										proxy.RegistrationCancel(new RegStatus()
										{
											RegID = personalDetailsVM.RegID,
											Active = true,
											CreateDT = DateTime.Now,
											StartDate = DateTime.Now,
											LastAccessID = Util.SessionAccess.UserName,
											StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
										});
									}
                                    //if (!Session["isSuppAsNew"].ToBool())
                                    //{
                                    //    if (totalprice == 0 && (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove))
                                    //    {
                                    //        WebHelper.Instance.PaymentReceived(personalDetailsVM);
                                    //        WebHelper.Instance.SaveUserTransactionLogs(personalDetailsVM.RegID);
                                    //    }
                                    //}
                                    if (Roles.IsUserInRole("MREG_DAPP"))
                                    {
                                        return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                    }
                                    else
                                    {
                                        return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
                                    }
								}
								#endregion
							}
							else
							{
								#region For Normal Flow
								using (var proxy = new KenanServiceProxy())
								{
                                    String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    //if (!ReferenceEquals(Action, String.Empty))
                                    //    return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //proxy.CreateIMPOSFile(personalDetailsVM.RegID);
								}
                                //using (var regproxy = new RegistrationServiceProxy())
                                //{
                                //    regproxy.RegistrationClose(new RegStatus()
                                //    {
                                //        RegID = personalDetailsVM.RegID,
                                //        Active = true,
                                //        CreateDT = DateTime.Now,
                                //        StartDate = DateTime.Now,
                                //        LastAccessID = Util.SessionAccess.UserName,
                                //        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                //        ResponseCode = response.Code,
                                //        ResponseDescription = response.Message,
                                //        MethodName = "KenanAdditionLineRegistration"
                                //    });
                                //}
                                if(!Session["isSuppAsNew"].ToBool()){
                                    if (totalprice == 0 && (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove))
                                    {
                                        response = FulfillKenanAccount(personalDetailsVM.RegID);
                                        //WebHelper.Instance.PaymentReceived(personalDetailsVM);
                                        WebHelper.Instance.SaveUserTransactionLogs(personalDetailsVM.RegID);
                                    }
                                }
                                if (Roles.IsUserInRole("MREG_DAPP") && !Roles.IsUserInRole("MREG_CUSR"))
                                {
                                    return RedirectToAction("CDPUDashBoard", "Registration", new { RefreshSession = 1 });
                                }
                                return RedirectToAction("StoreKeeper", "Registration");
								#endregion
                            }                             
                            #endregion
                        }
						break;
                    case "activateSvc":
						{
							#region activateSvc

							#region "sales person update"
							string username = string.Empty;
							using (var proxy = new RegistrationServiceProxy())
							{
								if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
								{
									username = Request.Cookies["CookieUser"].Value;
								}

								proxy.RegistrationUpdate(new DAL.Models.Registration()
								{
									ID = personalDetailsVM.RegID,
									//SalesPerson = username,
									LastAccessID = Util.SessionAccess.UserName,
								});
							}

							#endregion
							response = FulfillKenanAccount(personalDetailsVM.RegID);

							if (response.Message == "success")
							{
								//BEGIN   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database
								using (var proxy = new RegistrationServiceProxy())
								{
									if (personalDetailsVM.ExtraTenLimitConfirmation == "true")
									{

										if (personalDetailsVM.hdnExtraTenCount != null && personalDetailsVM.hdnExtraTenCount != string.Empty)
										{
											proxy.SaveExtraTenLogs(new ExtraTenLogReq
											{
												ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
												{
													ConfirmedBy = Util.SessionAccess.UserName,
													ConfirmedStatus = "Y",
													Count = Convert.ToInt32(personalDetailsVM.hdnExtraTenCount),
													CreatedDate = System.DateTime.Now,
													RegId = personalDetailsVM.RegID
												}
											});
										}
									}

									if (personalDetailsVM.ExtraTenConfirmation == "true")
									{

										proxy.SaveExtraTenLogs(new ExtraTenLogReq
										{
											ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
											{
												ConfirmedBy = Util.SessionAccess.UserName,
												ConfirmedStatus = "Y",
												CreatedDate = System.DateTime.Now,
												RegId = personalDetailsVM.RegID
											}
										});

									}
									else
									{
										proxy.SaveExtraTenLogs(new ExtraTenLogReq
										{
											ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
											{
												ConfirmedBy = Util.SessionAccess.UserName,
												ConfirmedStatus = "N",
												CreatedDate = System.DateTime.Now,
												RegId = personalDetailsVM.RegID
											}
										});
									}


									proxy.DeleteExtraTenDetails(personalDetailsVM.RegID);
									if (personalDetailsVM.ExtraTenConfirmation == "true")
									{
										foreach (var extratenMSISDN in personalDetailsVM.ExtraTenMobileNumbers.Split(','))
										{
											if (extratenMSISDN != string.Empty)
											{
												proxy.SaveExtraTen(new ExtraTenReq
												{

													ExtraTenDetails = new lnkExtraTenDetails { CreatedBy = Util.SessionAccess.UserName, CreatedDate = System.DateTime.Now, Msisdn = extratenMSISDN, RegId = personalDetailsVM.RegID, Status = "A" }


												});
											}
										}
									}
								}
								//END   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database
                                //if (Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSV"))
                                //{
                                //    return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
                                //}
                                //else
                                //{
									return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
                                //}
							}
							else
							{
								return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
							}

							#endregion
						}
						break;
					default:
						break;
				}

                            

				if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.Submit)
				{
					List<DAL.Models.WaiverComponents> objWaiverComp = new List<WaiverComponents>();
					if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == false)
					{
						if (Session[SessionKey.NumberType.ToString()].ToString2() == "n")
						{
							// Update Dealer Mobile No
							var kenanResp = new MobileNoUpdateResponse();
							using (var proxy = new KenanServiceProxy())
							{
								kenanResp = proxy.MobileNoUpdate(WebHelper.Instance.ConstructMobileNoUpdate());
							}

							if (!kenanResp.Success)
							{
								personalDetailsVM = PersonalDetailsVM_obj;

								ModelState.AddModelError(string.Empty, string.Format("Failed to update Dealer Mobile Number ({0}).",
									((List<string>)Session["RegMobileReg_MobileNo"])[0].ToString2()));
								return View("MobileRegSummaryNew", personalDetailsVM);
							}
						}
					}
					if (string.IsNullOrEmpty(personalDetailsVM.QueueNo))
					{
						personalDetailsVM.QueueNo = !ReferenceEquals(dropObj.personalInformation, null) ? dropObj.personalInformation.QueueNo : string.Empty;
					}

					// submit registration
					using (var proxy = new RegistrationServiceProxy())
					{

                        // 20141204 - Read the waiver components from the dropFourObj - start
                        var dropFourObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                        objWaiverComp = Util.constructWaiverComponentList(dropFourObj);

                        // Drop 5 - use for CDPU 
                        personalDetailsVM.isBREFail = dropObj.personalInformation.isBREFail;

                        if (objWaiverComp == null || objWaiverComp.Count == 0)
                        {
                            //Waiver formation cr by ravi kiran on 25 sept
                            //objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
                            objWaiverComp = Util.ConstructWaiverComponents(PersonalDetailsVM_obj.hdnWaiverInfo);
                            //Waiver formation cr by ravi kiran on 25 sept ends here
                        }
                        // 20141204 - Read the waiver components from the dropFourObj - start

						// drop 4 sanitazion
						var cRegistration = ConstructRegistration(
							personalDetailsVM.QueueNo, 
							personalDetailsVM.Remarks, 
							personalDetailsVM.SignatureSVG, 
							personalDetailsVM.CustomerPhoto, 
							personalDetailsVM.AltCustomerPhoto, 
							personalDetailsVM.Photo, 
							(!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString2() : "false"));
						var cCustomer = WebHelper.Instance.ConstructCustomer(PersonalDetailsVM_obj);
						var cGroupModel = WebHelper.Instance.ConstructRegMdlGroupModel();
						var cAddress = WebHelper.Instance.ConstructRegAddress(PersonalDetailsVM_obj);
						var cComponent = WebHelper.Instance.ConstructRegPgmBdlPkgComponent();
						var cStatus = WebHelper.Instance.ConstructRegStatus();
                        var cSupplines = WebHelper.Instance.ConstructRegSuppLinesNew(
                                string.IsNullOrEmpty(collection["InputNationalityID"].ToString2()) ? personalDetailsVM.Customer.NationalityID : Convert.ToInt32(collection["InputNationalityID"])                            
                            );
						var cSuppVas = WebHelper.Instance.ConstructRegSuppLineVASes();

						resp = proxy.RegistrationCreate(
							cRegistration,
							cCustomer,
							cGroupModel,
							cAddress,
							cComponent,
							cStatus,
							cSupplines,
							cSuppVas,
							null,
							objWaiverComp,
							personalDetailsVM.isBREFail,
                            dropObj.ApproverID,
                            TimeApproval);


						//resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString2() : "false")),
						//                                WebHelper.Instance.ConstructCustomer(PersonalDetailsVM_obj), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress(PersonalDetailsVM_obj),
						//                                WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
						//                                WebHelper.Instance.ConstructRegSuppLineVASes(), null, objWaiverComp, personalDetailsVM.isBREFail);

						#region "Spend Limit"
						if (resp.ID > 0)
						{
							//Save the Spend Limit
							if (Session[SessionKey.SpendLimit.ToString()].ToString2().Length > 0)
							{
								lnkRegSpendLimit objSpendLimit = new lnkRegSpendLimit();
								objSpendLimit.PlanId = (Session["RegMobileReg_PkgPgmBdlPkgCompID"] == null ? 0 : Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));
								objSpendLimit.RegId = resp.ID;
								objSpendLimit.SpendLimit = (Session[SessionKey.SpendLimit.ToString()].ToString2() == "" ? 0 : Convert.ToDecimal(Session[SessionKey.SpendLimit.ToString()]));
								objSpendLimit.CreatedDate = DateTime.Now;
								objSpendLimit.UserName = Util.SessionAccess.UserName;
								LnkRegSpendLimitReq objSpendReq = new LnkRegSpendLimitReq();
								objSpendReq.lnkRegSpendLimitDetails = objSpendLimit;
								int outValue = proxy.SaveSpendLimit(objSpendReq);
							}
							///BYPASS THE BELOW CODE FOR Extra Ten IF REQUEST FROM Multiple MNP
							bool isMultipleMNP = personalDetailsVM.RegTypeID == (int)MobileRegType.RegType_MNPPlanWithMultiSuppline ? true : false;
							if (!isMultipleMNP)
								Util.SaveExtraTen(personalDetailsVM.ExtraTenMobileNumbers, resp.ID);
						}
						//end of saving Spend Limit
						#endregion



						// update cardType if existing customer is direct debit, flag get from _BillingPaymentnew
						// if change this need to change in newLine also.
						if (!ReferenceEquals(Session["directDebit"], null))
						{
							var _ddFlag = Session["directDebit"].ToString2();
							if (_ddFlag.Equals("1"))
							{
								var cust = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();
								if (!ReferenceEquals(cust, null))
								{
									using (var _updateProxy = new UpdateServiceProxy())
									{
										cust.CardTypeID = 99;
										_updateProxy.CustomerUpdate(cust);
									}
								}

							}
						}

						var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
						if (biometricID != 0)
						{
							var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
							biometric.RegID = resp.ID;

							proxy.BiometricUpdate(biometric);
						}

						//Spend Limit  by Ravi As per New Flow on June 15 2013

                        Session[SessionKey.TradeUpAmount.ToString()] = null;//14012015 - Anthony - GST Trade Up
                        if (resp.ID > 0)
                        {
                            // Drop 5
                            //if ((personalDetailsVM.isBREFail && Session["IsDealer"].ToBool()) || (dropObj.mainFlow.isDeviceFinancingExceptionalOrder && dropObj.mainFlow.isDeviceFinancingOrder && Roles.IsUserInRole("MREG_CUSR")))
                            var isRouteToCDPU = WebHelper.Instance.routeToCDPU();

                            if (isRouteToCDPU)
                            {
                                try
                                {
                                    proxy.UpsertBRE(new RegBREStatus()
                                    {
                                        RegId = resp.ID,
                                        TransactionStatus = Constants.StatusCDPUPending
                                    });
                                    personalDetailsVM.CDPUStatus = Constants.StatusCDPUPending;
                                }
                                catch
                                {

                                }
                            }
                        }
					}
					using (var proxy = new UserServiceProxy())
					{
						if (Session["IsDeviceRequired"].ToString2() == "Yes")
						{
							proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Registration Done with RegId " + resp.ID, Session["KenanACNumber"].ToString2(), "");
						}
						else
						{
							proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SuppPlan, "Registration Done with RegId " + resp.ID, Session["KenanACNumber"].ToString2(), "");
						}
					}

					#region VLT ADDED CODE on 19th June for Adding SimType
					if (resp.ID > 0)
					{
                        
						int regTypeID = 0;
						if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
						{
							regTypeID = Util.GetRegTypeID(REGTYPE.MNPSuppPlan);
						}
						else
						{
							switch (Session["RegMobileReg_Type"].ToInt())
							{
								case (int)MobileRegType.DevicePlan:
									regTypeID = Util.GetRegTypeID(REGTYPE.DevicePlan);
									break;
								case (int)MobileRegType.PlanOnly:
									regTypeID = 8;
									break;
								case (int)MobileRegType.DeviceOnly:
									regTypeID = Util.GetRegTypeID(REGTYPE.DeviceOnly);
									break;
								case (int)MobileRegType.SuppPlan:
									regTypeID = Util.GetRegTypeID(REGTYPE.SuppPlan);
									break;
								case (int)MobileRegType.SecPlan:
									regTypeID = Util.GetRegTypeID(REGTYPE.SecPlan);
									break;
							}
						}

                        //commented as disabling pos file feature
                        //#region Saving POS FileName in DB for Dealers
                        //if (Session["IsDealer"].ToBool() == true && (collection["hdnTotalPayable"].ToString2() != string.Empty && collection["hdnTotalPayable"].ToDecimal() > 0 || regTypeID == Util.GetRegTypeID(REGTYPE.DevicePlan)))
                        //{
                        //    string posFileName = WebHelper.Instance.CreatePOSFileName(resp.ID);
                        //    using (var proxy = new CatalogServiceProxy())
                        //    {
                        //        proxy.UpdateIMPOSDetails(resp.ID, posFileName, 1);
                        //    }
                        //}
                        //#endregion

                        using (var proxy = new RegistrationServiceProxy())
                        {
                            #region lnkRegDetails
                            string PlanKenanCode = MasterDataCache.Instance.PackageComponents.Where(a => a.ID == Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"])).Select(a => a.KenanCode).SingleOrDefault().ToString2();
                            string latestPrintVersion = Util.GetPrintVersionNum(regTypeID, !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty, PlanKenanCode);

                            if (Session["SimType"] != null || Session[SessionKey.IsSuppleNewAccount.ToString()].ToString2().ToUpper() == "TRUE" || (!string.IsNullOrEmpty(latestPrintVersion)))
                            {
                                LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
                                Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
                                objRegDetails.CreatedDate = DateTime.Now;
                                //Insert SimType 
                                if (Session["SimType"].ToString2().Length > 0)
                                    objRegDetails.SimType = Session["SimType"].ToString2();
                                if (Session[SessionKey.IsSuppleNewAccount.ToString()].ToString2().ToUpper() == "TRUE")
                                    objRegDetails.IsSuppNewAc = true;
                                else
                                    objRegDetails.IsSuppNewAc = false;
                                if (Session["SimType"] != null)
                                    objRegDetails.SimType = Session["SimType"].ToString2();
                                else
                                    objRegDetails.SimType = "Normal";
                                objRegDetails.UserName = Util.SessionAccess.UserName;
                                objRegDetails.RegId = resp.ID;
                                /*Active PDPA Commented*/
                                /*Changes by chetan related to PDPA implementation*/
                                //using (var registrationProxy = new RegistrationServiceProxy())
                                //{
                                //    var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
                                //    objRegDetails.PdpaVersion = resultdata.Version;
                                //}

                                /*Recode Active PDPA Commented*/
                                objRegDetails.PdpaVersion = personalDetailsVM.PDPDVersion;
                                //Insert Justification 
                                //if (collection["txtJustfication"].ToString2().Length > 0)
                                //    objRegDetails.Justification = collection["txtJustfication"].ToString2();

                                if (!ReferenceEquals(dropObj.personalInformation, null) && !string.IsNullOrEmpty(dropObj.personalInformation.Justification))
                                {
                                    objRegDetails.Justification = dropObj.personalInformation.Justification;
                                }


                                //Changed by ravi as already on sep 25 2013 inserting lnkregDetails
                                if (Session[SessionKey.IsSuppleNewAccount.ToString()].ToString2().ToUpper() == "TRUE")
                                    objRegDetails.IsSuppNewAc = true;
                                ///Changed by ravi as already on sep 25 2013 inserting lnkregDetails ends here
                                objRegDetails.MOC_Status = personalDetailsVM.MOCStatus;
                                objRegDetails.Liberlization_Status = personalDetailsVM.Liberlization_Status;
                                if (!string.IsNullOrEmpty(latestPrintVersion))
                                    objRegDetails.PrintVersionNo = latestPrintVersion;
                                else
                                    objRegDetails.PrintVersionNo = string.Empty;
                                // null check condition added
                                /*
							    if (Session["AccountswithContractCount"] != null)
								    objRegDetails.ContractCheckCount = Session["AccountswithContractCount"].ToInt();
                                 */
                                objRegDetails.ContractCheckCount = dropObj.additionalContract + dropObj.existingContract; //RST BRE CR
                                objRegDetails.TotalLineCheckCount = dropObj.getExistingLineCount() + dropObj.getNewLineCount();
                                bool IsWriteOff = false;
                                string acc = string.Empty;
                                WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
                                objRegDetails.WriteOffDetails = acc;

                                if (Session[SessionKey.NumberType.ToString()].ToString2() == "g")
                                    objRegDetails.IsMSISDNReserved = true;
                                else
                                    objRegDetails.IsMSISDNReserved = false;
                                //******selected sim card type is assigned*****//
                                //if (!string.IsNullOrEmpty(collection["SimCardTypeSelected"]))
                                //    objRegDetails.SimCardType = collection["SimCardTypeSelected"].ToString2();

                                if (string.IsNullOrEmpty(objRegDetails.SimCardType))
                                {
                                    if (!string.IsNullOrEmpty(dropObj.mainFlow.simSize))
                                    {
                                        objRegDetails.SimCardType = dropObj.mainFlow.simSize;
                                    }
                                    else
                                    {
                                        objRegDetails.SimCardType = dropObj.suppFlow[dropObj.suppIndex - 1].simSize;
                                    }
                                }

                                if (!string.IsNullOrEmpty(collection["SecondarySimCardTypeSelected"]))
                                    objRegDetails.MismSimCardType = collection["SecondarySimCardTypeSelected"].ToString2();

                                // this is for MNP
                                if (string.IsNullOrEmpty(objRegDetails.SimCardType))
                                    objRegDetails.SimCardType = collection["SimCardTypeSelected"].ToString2();

                                //******selected sim card type is assigned*****//
                                objRegDetailsReq.LnkDetails = objRegDetails;
                                objRegDetails.Liberlization_Status = personalDetailsVM.Liberlization_Status;

                                // Gerry - PN 3967 - Reserve Number
                                Logger.Debug("Suppline skipkenan = " + Session["SkipKenanReservation"].ToBool());
                                objRegDetails.IsMSISDNReserved = Session["SkipKenanReservation"].ToBool() ? true : false;

                                proxy.SaveLnkRegistrationDetails(objRegDetailsReq);

                                Session[SessionKey.NumberType.ToString()] = null;
                            }
                            #endregion

                            #region Insert into trnRegAttributes
                            var regAttribList = new List<RegAttributes>();
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletCode))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCODE, ATT_Value = personalDetailsVM.OutletCode });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletChannelId))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCHANNELID, ATT_Value = personalDetailsVM.OutletChannelId });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesCode))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCODE, ATT_Value = personalDetailsVM.SalesCode });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesChannelId))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCHANNELID, ATT_Value = personalDetailsVM.SalesChannelId });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffCode))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCODE, ATT_Value = personalDetailsVM.SalesStaffCode });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffChannelId))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCHANNELID, ATT_Value = personalDetailsVM.SalesStaffChannelId });
                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.BillingCycle))
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_ACC_BILL_CYCLE, ATT_Value = personalDetailsVM.BillingCycle });

                            if (!string.IsNullOrWhiteSpace(personalDetailsVM.SingleSignOnValue))
                            {
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_SINGLESIGNON_STATUS, ATT_Value = personalDetailsVM.SingleSignOnValue });

                                if (personalDetailsVM.SingleSignOnValue.ToUpper() == "YES")
                                {
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_SINGLESIGNON_EMAILADDRESS, ATT_Value = personalDetailsVM.SingleSignOnEmailAddress });
                                }
                            }

                            regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_PAYMENT_METHOD, ATT_Value = Util.getPaymentMethodName(dropObj.personalInformation.Customer.PayModeID) });
                            regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_USER_DEVICE_AGENT, ATT_Value = Request.UserAgent });

                            int[] mnpOrder = new int[] { 10, 11, 12, 26 };
							if (!mnpOrder.Contains(regTypeID))
							{
                                WebHelper.Instance.ConstructRegAttributesForDF(resp.ID, ref regAttribList, out regAttribList);
                                WebHelper.Instance.ConstructRegAttributesForAF(resp.ID, ref regAttribList);
							}
                            
                            proxy.SaveListRegAttributes(regAttribList);
                            #endregion

							#region Insert into trnRegJustification
							WebHelper.Instance.saveTrnRegJustification(resp.ID);
                            #endregion

                            #region Insert into trnRegBreFailTreatment
                            WebHelper.Instance.saveTrnRegBreFailTreatment(resp.ID);
                            #endregion

							#region Kenan Customer Info
							if (Session["KenanCustomerInfo"] != null)
                            {
                                string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString2().Split('╚');
                                KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
                                kenanInfo.FullName = strArrKenanInfo[0];
                                kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
                                kenanInfo.IDCardNo = strArrKenanInfo[2];
                                kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
                                kenanInfo.Address1 = strArrKenanInfo[4];
                                kenanInfo.Address2 = strArrKenanInfo[5];
                                kenanInfo.Address3 = strArrKenanInfo[6];
                                kenanInfo.Postcode = strArrKenanInfo[8];
                                kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
                                kenanInfo.RegID = resp.ID;
                                var kenanResp = new KenanCustomerInfoCreateResp();
                                kenanResp = proxy.KenanCustomerInfoCreate(kenanInfo);
                            }
                            Session["KenanCustomerInfo"] = null;
                            /* END Added by VLT 5 Apr 2013 to add kenaninfo to database*/
                            #endregion

							#region Insert into trnSurveyResponse
                            WebHelper.Instance.SaveSurveyResult(personalDetailsVM, RegSurveyResponse.MAXIS_APP, resp.ID);
							#endregion

                            #region Insert into trnRegAccessory
                            if (Session["IsDeviceRequired"].ToString2() == "Yes")
                            {
                                var serialNumberList = new Dictionary<int, string>();
                                WebHelper.Instance.SaveTrnRegAccessory(dropObj, resp.ID, ref serialNumberList, true);
                                WebHelper.Instance.SaveTrnRegAccessoryDetails(resp.ID, serialNumberList);
                            }
                            #endregion
                        }

						//Icontract Upload Images.
						bool iContractActive = Convert.ToBoolean(ConfigurationManager.AppSettings["iContractActive"]);
						//bool rollOutCheck = Util.rollOutCheck();
						bool rollOutCheck = true;

						if (iContractActive)
						{
							WebHelper.Instance.SaveDocumentToTable(dropObj, resp.ID);
						}
						

						#region "Total Payable"

                        //if ((Roles.IsUserInRole("MREG_DIC") || Roles.IsUserInRole("MREG_DAI") || Roles.IsUserInRole("MREG_DSV")) && false)
                        //{
                            //if (Session["IsDealer"].ToBool() == true && collection["hdnTotalPayable"].ToString2() != string.Empty && collection["hdnTotalPayable"].ToDecimal() == 0)
                            //var totalprice = Util.getPrinSuppTotalPrice(personalDetailsVM.RegID);
                            //if (totalprice == 0 && (String.IsNullOrEmpty(personalDetailsVM.CDPUStatus) || personalDetailsVM.CDPUStatus == Constants.StatusCDPUApprove))
                            //{
                            //    //Check all the advance/components waived off
                            //    List<DAL.Models.WaiverComponents> objWaivedComp = objWaiverComp.Where(comp => comp.IsWaived == true).ToList();
                            //    if (objWaivedComp != null && objWaiverComp != null && objWaivedComp.Count == objWaiverComp.Count)
                            //    {
                            //        using (var proxyobj = new RegistrationServiceProxy())
                            //        {
                            //            isSuppAsNew = proxyobj.getSupplineDetails(resp.ID);
                            //        }
                            //        if (isSuppAsNew == false)
                            //        {
                            //            using (var proxy = new RegistrationServiceProxy())
                            //            {
                            //                proxy.RegistrationCancel(new RegStatus()
                            //                {
                            //                    RegID = resp.ID,
                            //                    Active = true,
                            //                    CreateDT = DateTime.Now,
                            //                    StartDate = DateTime.Now,
                            //                    LastAccessID = Util.SessionAccess.UserName,
                            //                    StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
                            //                });
                            //            }
                            //        }
                            //        if (isSuppAsNew)
                            //        {
                            //            WebHelper.Instance.CreateKenanAccount(resp.ID);

                            //            return RedirectToAction("MobileRegAccCreated", new { regID = resp.ID });

                            //        }
                            //        else if (PersonalDetailsVM_obj.RegTypeID == 11 || regTypeID==11)
                            //        {
                            //            #region For MNP Flow

                            //            using (var proxy = new KenanServiceProxy())
                            //            {

                            //                var request1 = new MNPtypeFullfillCenterOrderMNP()
                            //                {
                            //                    orderId = resp.ID.ToString(),
                            //                };
                            //                //bool statusMNPSup = proxy.KenanMNPAccountFulfill(request1);
                            //                //if (statusMNPSup)
                            //                //{
                            //                //    return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });
                            //                //}
                            //                response = FulfillKenanAccount(resp.ID);
                            //                if (response.Success)
                            //                {
                            //                    return RedirectToAction("MobileRegAccCreated", new { regID = resp.ID });
                            //                }
                            //                else
                            //                {
                            //                    using (var regproxy = new RegistrationServiceProxy())
                            //                    {
                            //                        regproxy.RegistrationClose(new RegStatus()
                            //                        {
                            //                            RegID = resp.ID,
                            //                            Active = true,
                            //                            CreateDT = DateTime.Now,
                            //                            StartDate = DateTime.Now,
                            //                            LastAccessID = Util.SessionAccess.UserName,
                            //                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail),
                            //                            ResponseCode = response.Code,
                            //                            ResponseDescription = response.Message,
                            //                            MethodName = "KenanAdditionLineRegistration"
                            //                        });
                            //                    }
                            //                    return RedirectToAction("MobileRegWMFail", new { regID = resp.ID });
                            //                }
                            //            }

                            //            #endregion
                            //        }
                            //        else
                            //        {// if have device dont knock off
                            //            if (regTypeID != Util.GetRegTypeID(REGTYPE.DevicePlan))
                            //            {
                            //                #region For Normal Flow
                            //                response = FulfillKenanAccount(resp.ID);
                            //                return RedirectToAction("MobileRegSvcActivated", new { regID = resp.ID });
                            //                #endregion
                            //            }
                            //        }
                            //    }
                            //}
                        //}

						#endregion


						if ((Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("MREG_DAI")) && false)
						{
							string username = string.Empty;
							using (var proxyobj = new RegistrationServiceProxy())
							{
								if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
									username = Request.Cookies["CookieUser"].Value;
								proxyobj.RegistrationUpdate(new DAL.Models.Registration()
								{
									ID = resp.ID,
									//SalesPerson = username,
									LastAccessID = Util.SessionAccess.UserName,
									//SimModelId = ReferenceEquals(Session["SimModelTypeId"].ToString2(), null) ? 0 : Convert.ToInt32(Session["SimModelTypeId"].ToString2()),
								});
							}
							using (var proxyobj = new RegistrationServiceProxy())
							{
								reg = proxyobj.RegistrationGet(resp.ID);
								personalDetailsVM.RegTypeID = reg.RegTypeID;
							}
							using (var proxyobj = new RegistrationServiceProxy())
							{
								isSuppAsNew = proxyobj.getSupplineDetails(resp.ID);
							}
							if (isSuppAsNew)
							{
								WebHelper.Instance.CreateKenanAccount(resp.ID);
							}
							else if (personalDetailsVM.RegTypeID == 11)
							{
								response = FulfillKenanAccount(resp.ID);
								if ((response.Message == "success") && (response.Code == "0"))
								{
									using (var regproxy = new RegistrationServiceProxy())
									{
										regproxy.RegistrationClose(new RegStatus()
										{
											RegID = resp.ID,
											Active = true,
											CreateDT = DateTime.Now,
											StartDate = DateTime.Now,
											LastAccessID = Util.SessionAccess.UserName,
											StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate),
											ResponseCode = response.Code,
											ResponseDescription = response.Message,
											MethodName = "KenanAdditionLineRegistration"
										});
									}
									return RedirectToAction("MobileRegAccCreated", new { regID = resp.ID });
								}
								else
								{
									using (var proxyobj = new RegistrationServiceProxy())
									{
										proxyobj.RegistrationCancel(new RegStatus()
										{
											RegID = resp.ID,
											Active = true,
											CreateDT = DateTime.Now,
											StartDate = DateTime.Now,
											LastAccessID = Util.SessionAccess.UserName,
											StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
										});
									}
									return RedirectToAction("MobileRegFail", new { regID = resp.ID });
								}
							}
							else
							{
								using (var proxyobj = new KenanServiceProxy())
								{
                                    String Action = WebHelper.Instance.WebPosAuto(resp.ID);
                                    //if (!ReferenceEquals(Action, String.Empty))
                                    //    return RedirectToAction(Action, new { regID = resp.ID });
                                    //proxyobj.CreateIMPOSFile(resp.ID);
								}
                                //using (var regproxy = new RegistrationServiceProxy())
                                //{
                                //    regproxy.RegistrationClose(new RegStatus()
                                //    {
                                //        RegID = resp.ID,
                                //        Active = true,
                                //        CreateDT = DateTime.Now,
                                //        StartDate = DateTime.Now,
                                //        LastAccessID = Util.SessionAccess.UserName,
                                //        StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
                                //        ResponseCode = response.Code,
                                //        ResponseDescription = response.Message,
                                //        MethodName = "KenanAdditionLineRegistration"
                                //    });
                                //}
							}
						}

						//dropObj.personalInformation = personalDetailsVM;
                        if (Session[SessionKey.DedicatedSuppFlow.ToString()].ToBool())
                            dropObj.suppFlow[0].orderSummary = ConstructOrderSummary();
                        else
                            dropObj.mainFlow.orderSummary = ConstructOrderSummary();
						Session[SessionKey.DropFourObj.ToString()] = dropObj;

						#region "Supple New Account Feature"


						Session[SessionKey.IsSuppleNewAccount.ToString()] = null;
						Session[SessionKey.SpendLimit.ToString()] = null;
						#endregion
						WebHelper.Instance.ClearRegistrationSession();
                        Session[SessionKey.RegMobileReg_MSISDN.ToString()] = null;

						if (Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("DAgent") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_C") || Roles.IsUserInRole("MREG_DAI"))
						{
							//return RedirectToAction("MobileRegSummary", new { regID = resp.ID });
							//return RedirectToAction("MobileRegAccCreated", new { regID = resp.ID });
                            return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
						}
						else
						{
							return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
						}
					}
					#endregion

				}
				else if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.PersonalDetails)
				{
					return RedirectToAction("PersonalDetailsNew");
				}
			}
			catch (Exception ex)
			{
				Logger.Info("Throw Exception for Order ID:" + resp.ID + "Exception:" + ex);
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
				throw ex;
			}

			return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
		}

		[HttpPost]
		[ValidateInput(false)]
		[Authorize(Roles = "MREG_W,MREG_C,MREG_U,MREG_SK,MREG_CH,MREG_DSV,MREG_DSA,DREG_N,DREG_C,DREG_CH,MREG_DIC,MREG_DAI,MREG_DAC")]
		public ActionResult DMobileRegSummary(FormCollection collection, PersonalDetailsVM personalDetailsVM)
		{
			bool isSuppAsNew = false;

			var resp = new RegistrationCreateResp();
			var response = new KenanNewSuppLineResponse();
			var reg = new DAL.Models.Registration();
			bool isSuccess = false;
			Session["RegMobileReg_TabIndex"] = personalDetailsVM.TabNumber;
			Session["ExtratenMobilenumbers"] = personalDetailsVM.ExtraTenMobileNumbers;
			PersonalDetailsVM PersonalDetailsVM_obj = null;

			if (Session["RegMobileReg_PersonalDetailsVM"] != null)
			{
				PersonalDetailsVM_obj = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
			}

			try
			{

				switch (collection["submit1"].ToString2())
				{
					case "PaymentRecived":
						{
							#region PaymentRecived

							using (var proxy = new RegistrationServiceProxy())
							{
								proxy.RegistrationCancel(new RegStatus()
								{
									RegID = personalDetailsVM.RegID,
									Active = true,
									CreateDT = DateTime.Now,
									StartDate = DateTime.Now,
									LastAccessID = Util.SessionAccess.UserName,
									StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Payment_Recived)
								});
							}

							using (var proxy = new RegistrationServiceProxy())
							{
								isSuppAsNew = proxy.getSupplineDetails(personalDetailsVM.RegID);
							}
							if (isSuppAsNew)
							{
								var request = new OrderFulfillRequest()
								{
									OrderID = personalDetailsVM.RegID.ToString(),
									UserSession = Util.SessionAccess.UserName,

								};
								using (var proxy = new KenanServiceProxy())
								{
									isSuccess = proxy.KenanAccountFulfill(request);
								}
								if (isSuccess)
								{
									return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
								}
								else
								{
									return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
								}
							}
							else if (PersonalDetailsVM_obj.RegTypeID == 11)
							{
								#region For MNP Flow

								using (var proxy = new KenanServiceProxy())
								{

									var request1 = new MNPtypeFullfillCenterOrderMNP()
									{
										orderId = personalDetailsVM.RegID.ToString(),
									};
									bool statusMNPSup = proxy.KenanMNPAccountFulfill(request1);
									if (statusMNPSup)
									{
										return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
									}
									else
									{
										using (var regproxy = new RegistrationServiceProxy())
										{
											regproxy.RegistrationClose(new RegStatus()
											{
												RegID = personalDetailsVM.RegID,
												Active = true,
												CreateDT = DateTime.Now,
												StartDate = DateTime.Now,
												LastAccessID = Util.SessionAccess.UserName,
												StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail),
												ResponseCode = response.Code,
												ResponseDescription = response.Message,
												MethodName = "KenanAdditionLineRegistration"
											});
										}
										return RedirectToAction("MobileRegWMFail", new { regID = personalDetailsVM.RegID });
									}
								}

								#endregion
							}
							else
							{
								#region For Normal Flow
								response = FulfillKenanAccount(personalDetailsVM.RegID);
								return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });


								#endregion
							}

							#endregion
						}
						break;
					case "close":
						{
							#region close



							if (personalDetailsVM.RegID == 0)
							{
								using (var proxy = new RegistrationServiceProxy())
								{
									resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString2() : "false")),
									WebHelper.Instance.ConstructCustomer(PersonalDetailsVM_obj), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress(PersonalDetailsVM_obj),
									WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
									WebHelper.Instance.ConstructRegSuppLineVASes(), null, null, personalDetailsVM.isBREFail);
									personalDetailsVM.RegID = resp.ID;
								}

								using (var proxy = new RegistrationServiceProxy())
								{
									proxy.RegistrationClose(new RegStatus()
									{
										RegID = personalDetailsVM.RegID,
										Active = true,
										CreateDT = DateTime.Now,
										StartDate = DateTime.Now,
										LastAccessID = Util.SessionAccess.UserName,
										StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegBreFail)
									});
								}
								return RedirectToAction("MobileRegBreFail", new { regID = personalDetailsVM.RegID });
							}
							else
							{
								using (var proxy = new RegistrationServiceProxy())
								{
									proxy.RegistrationClose(new RegStatus()
									{
										RegID = personalDetailsVM.RegID,
										Active = true,
										CreateDT = DateTime.Now,
										StartDate = DateTime.Now,
										LastAccessID = Util.SessionAccess.UserName,
										StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
									});
								}
								return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
							}


							#endregion
						}
						break;
					case "cancel":
						{
							#region cancel

							string reason = string.Empty;
							reason = ReferenceEquals(reason, null) ? "No reason provided." : collection["hdnReasonCancel"].ToString2();
							using (var proxy = new RegistrationServiceProxy())
							{
								proxy.RegistrationCancel(new RegStatus()
								{
									RegID = personalDetailsVM.RegID,
									Active = true,
									CreateDT = DateTime.Now,
									StartDate = DateTime.Now,
									LastAccessID = Util.SessionAccess.UserName,
									StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegCan)

								});

								proxy.RegistrationCancelCreate(new DAL.Models.RegistrationCancelReason()
								{
									RegID = personalDetailsVM.RegID,
									CancelReason = reason,
									CreateDT = DateTime.Now,
									LastUpdateDT = DateTime.Now,
									LastAccessID = Util.SessionAccess.UserName
								});
							}

							return RedirectToAction("MobileRegCanceled", new { regID = personalDetailsVM.RegID });


							#endregion
						}
						break;
					case "createAcc":
						{
							#region createAcc

							string username = string.Empty;


							#region "sales person update"
							using (var proxy = new RegistrationServiceProxy())
							{
								if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
								{
									username = Request.Cookies["CookieUser"].Value;
								}

								proxy.RegistrationUpdate(new DAL.Models.Registration()
								{
									ID = personalDetailsVM.RegID,
									SalesPerson = username,
									LastAccessID = Util.SessionAccess.UserName,

									//SimModelId = ReferenceEquals(Session["SimModelTypeId"].ToString2(), null) ? 0 : Convert.ToInt32(Session["SimModelTypeId"].ToString2()),

								});
							}

							#endregion


							using (var proxy = new RegistrationServiceProxy())
							{
								reg = proxy.RegistrationGet(personalDetailsVM.RegID);
								personalDetailsVM.RegTypeID = reg.RegTypeID;
							}


							using (var proxy = new RegistrationServiceProxy())
							{
								isSuppAsNew = proxy.getSupplineDetails(personalDetailsVM.RegID);
							}
							if (isSuppAsNew)
							{
								WebHelper.Instance.CreateKenanAccount(personalDetailsVM.RegID);
								if (Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSV"))
								{
									return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
								}
								else
								{
									return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
								}
							}
							else if (personalDetailsVM.RegTypeID == 11)
							{
								#region For MNP Flow
								response = FulfillKenanAccount(personalDetailsVM.RegID);
								if ((response.Message == "success") && (response.Code == "0"))
								{
									using (var regproxy = new RegistrationServiceProxy())
									{
										regproxy.RegistrationClose(new RegStatus()
										{
											RegID = personalDetailsVM.RegID,
											Active = true,
											CreateDT = DateTime.Now,
											StartDate = DateTime.Now,
											LastAccessID = Util.SessionAccess.UserName,
											StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate),
											ResponseCode = response.Code,
											ResponseDescription = response.Message,
											MethodName = "KenanAdditionLineRegistration"
										});
									}
									return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
								}
								else
								{
									using (var proxy = new RegistrationServiceProxy())
									{
										proxy.RegistrationCancel(new RegStatus()
										{
											RegID = personalDetailsVM.RegID,
											Active = true,
											CreateDT = DateTime.Now,
											StartDate = DateTime.Now,
											LastAccessID = Util.SessionAccess.UserName,
											StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
										});
									}
									return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
								}
								#endregion
							}
							else
							{
								#region For Normal Flow
								using (var proxy = new KenanServiceProxy())
								{
                                    String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    if (!ReferenceEquals(Action, String.Empty))
                                        return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //proxy.CreateIMPOSFile(personalDetailsVM.RegID);
								}
								using (var regproxy = new RegistrationServiceProxy())
								{
									regproxy.RegistrationClose(new RegStatus()
									{
										RegID = personalDetailsVM.RegID,
										Active = true,
										CreateDT = DateTime.Now,
										StartDate = DateTime.Now,
										LastAccessID = Util.SessionAccess.UserName,
										StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
										ResponseCode = response.Code,
										ResponseDescription = response.Message,
										MethodName = "KenanAdditionLineRegistration"
									});
								}
								if (Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSV"))
								{
									return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
								}
								else
								{
									return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
								}
								#endregion
							}


							#endregion
						}
						break;
					case "activateSvc":
						{
							#region activateSvc

							#region "sales person update"
							string username = string.Empty;
							using (var proxy = new RegistrationServiceProxy())
							{
								if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
								{
									username = Request.Cookies["CookieUser"].Value;
								}

								proxy.RegistrationUpdate(new DAL.Models.Registration()
								{
									ID = personalDetailsVM.RegID,
									SalesPerson = username,
									LastAccessID = Util.SessionAccess.UserName,



								});
							}

							#endregion
							response = FulfillKenanAccount(personalDetailsVM.RegID);

							if (response.Message == "success")
							{
								//BEGIN   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database


								using (var proxy = new RegistrationServiceProxy())
								{
									if (personalDetailsVM.ExtraTenLimitConfirmation == "true")
									{

										if (personalDetailsVM.hdnExtraTenCount != null && personalDetailsVM.hdnExtraTenCount != string.Empty)
										{
											proxy.SaveExtraTenLogs(new ExtraTenLogReq
											{
												ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
												{
													ConfirmedBy = Util.SessionAccess.UserName,
													ConfirmedStatus = "Y",
													Count = Convert.ToInt32(personalDetailsVM.hdnExtraTenCount),
													CreatedDate = System.DateTime.Now,
													RegId = personalDetailsVM.RegID
												}
											});
										}
									}

									if (personalDetailsVM.ExtraTenConfirmation == "true")
									{

										proxy.SaveExtraTenLogs(new ExtraTenLogReq
										{
											ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
											{
												ConfirmedBy = Util.SessionAccess.UserName,
												ConfirmedStatus = "Y",
												CreatedDate = System.DateTime.Now,
												RegId = personalDetailsVM.RegID
											}
										});

									}
									else
									{
										proxy.SaveExtraTenLogs(new ExtraTenLogReq
										{
											ExtraTenConfirmationLog = new LnkExtraTenConfirmationLog
											{
												ConfirmedBy = Util.SessionAccess.UserName,
												ConfirmedStatus = "N",
												CreatedDate = System.DateTime.Now,
												RegId = personalDetailsVM.RegID
											}
										});
									}


									proxy.DeleteExtraTenDetails(personalDetailsVM.RegID);
									if (personalDetailsVM.ExtraTenConfirmation == "true")
									{
										foreach (var extratenMSISDN in personalDetailsVM.ExtraTenMobileNumbers.Split(','))
										{
											if (extratenMSISDN != string.Empty)
											{
												proxy.SaveExtraTen(new ExtraTenReq
												{

													ExtraTenDetails = new lnkExtraTenDetails { CreatedBy = Util.SessionAccess.UserName, CreatedDate = System.DateTime.Now, Msisdn = extratenMSISDN, RegId = personalDetailsVM.RegID, Status = "A" }


												});
											}
										}
									}
								}
								//END   Added by Vahini on 10 June 2013 for Adding Extra Ten MSISDNs to Database
								if (Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("MREG_DSV"))
								{
									return RedirectToAction("MobileRegSummary", new { regID = personalDetailsVM.RegID });
								}
								else
								{
									return RedirectToAction("MobileRegSvcActivated", new { regID = personalDetailsVM.RegID });
								}
							}
							else
							{
								return RedirectToAction("MobileRegClosed", new { regID = personalDetailsVM.RegID });
							}

							#endregion
						}
						break;


					default:
						break;
				}

				if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.Submit)
				{
					if (Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]) == false)
					{
						if (Session[SessionKey.NumberType.ToString()].ToString2() == "n")
						{
							// Update Dealer Mobile No
							var kenanResp = new MobileNoUpdateResponse();
							using (var proxy = new KenanServiceProxy())
							{
								kenanResp = proxy.MobileNoUpdate(WebHelper.Instance.ConstructMobileNoUpdate());
							}

							if (!kenanResp.Success)
							{
								personalDetailsVM = PersonalDetailsVM_obj;

								ModelState.AddModelError(string.Empty, string.Format("Failed to update Dealer Mobile Number ({0}).",
									((List<string>)Session["RegMobileReg_MobileNo"])[0].ToString2()));

								return View("MobileRegSummary", personalDetailsVM);
							}
						}
					}
					// submit registration
					using (var proxy = new RegistrationServiceProxy())
					{

						//Waiver formation cr by ravi kiran on 25 sept
						List<DAL.Models.WaiverComponents> objWaiverComp = Util.ConstructWaiverComponents(collection["hdnWaiverInfo"].ToString2());
						//Waiver formation cr by ravi kiran on 25 sept ends here

						resp = proxy.RegistrationCreate(ConstructRegistration(personalDetailsVM.QueueNo, personalDetailsVM.Remarks, personalDetailsVM.SignatureSVG, personalDetailsVM.CustomerPhoto, personalDetailsVM.AltCustomerPhoto, personalDetailsVM.Photo, (!string.IsNullOrEmpty(collection["SIMModelSelected"]) ? collection["SIMModelSelected"].ToString2() : "false")),
														WebHelper.Instance.ConstructCustomer(PersonalDetailsVM_obj), WebHelper.Instance.ConstructRegMdlGroupModel(), WebHelper.Instance.ConstructRegAddress(PersonalDetailsVM_obj),
														WebHelper.Instance.ConstructRegPgmBdlPkgComponent(), WebHelper.Instance.ConstructRegStatus(), WebHelper.Instance.ConstructRegSuppLines(),
														WebHelper.Instance.ConstructRegSuppLineVASes(), null, objWaiverComp, personalDetailsVM.isBREFail);
						personalDetailsVM.RegID = resp.ID;
						//Spend Limit  by Ravi As per New Flow on June 15 2013

						//joshi added to create kenan account for dealers
						if (Roles.IsUserInRole("MREG_DSV"))
						{
							string username = string.Empty;
							using (var proxyobj = new RegistrationServiceProxy())
							{
								if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
								{
									username = Request.Cookies["CookieUser"].Value;
								}

								proxyobj.RegistrationUpdate(new DAL.Models.Registration()
								{
									ID = personalDetailsVM.RegID,
									SalesPerson = username,
									LastAccessID = Util.SessionAccess.UserName,

									//SimModelId = ReferenceEquals(Session["SimModelTypeId"].ToString2(), null) ? 0 : Convert.ToInt32(Session["SimModelTypeId"].ToString2()),

								});
							}

							using (var proxyobj = new RegistrationServiceProxy())
							{
								reg = proxyobj.RegistrationGet(personalDetailsVM.RegID);
								personalDetailsVM.RegTypeID = reg.RegTypeID;
							}


							using (var proxyobj = new RegistrationServiceProxy())
							{
								isSuppAsNew = proxyobj.getSupplineDetails(personalDetailsVM.RegID);
							}
							if (isSuppAsNew)
							{
								WebHelper.Instance.CreateKenanAccount(personalDetailsVM.RegID);
							}
							else if (personalDetailsVM.RegTypeID == 11)
							{
								response = FulfillKenanAccount(personalDetailsVM.RegID);
								if ((response.Message == "success") && (response.Code == "0"))
								{
									using (var regproxy = new RegistrationServiceProxy())
									{
										regproxy.RegistrationClose(new RegStatus()
										{
											RegID = personalDetailsVM.RegID,
											Active = true,
											CreateDT = DateTime.Now,
											StartDate = DateTime.Now,
											LastAccessID = Util.SessionAccess.UserName,
											StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_AccPendingCreate),
											ResponseCode = response.Code,
											ResponseDescription = response.Message,
											MethodName = "KenanAdditionLineRegistration"
										});
									}
									return RedirectToAction("MobileRegAccCreated", new { regID = personalDetailsVM.RegID });
								}
								else
								{
									using (var proxyobj = new RegistrationServiceProxy())
									{
										proxyobj.RegistrationCancel(new RegStatus()
										{
											RegID = personalDetailsVM.RegID,
											Active = true,
											CreateDT = DateTime.Now,
											StartDate = DateTime.Now,
											LastAccessID = Util.SessionAccess.UserName,
											StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegFail)
										});
									}
									return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
								}

							}
							else
							{

								using (var proxyobj = new KenanServiceProxy())
								{
                                    String Action = WebHelper.Instance.WebPosAuto(personalDetailsVM.RegID);
                                    if (!ReferenceEquals(Action, String.Empty))
                                        return RedirectToAction(Action, new { regID = personalDetailsVM.RegID });
                                    //proxyobj.CreateIMPOSFile(personalDetailsVM.RegID);
								}
								using (var regproxy = new RegistrationServiceProxy())
								{
									regproxy.RegistrationClose(new RegStatus()
									{
										RegID = personalDetailsVM.RegID,
										Active = true,
										CreateDT = DateTime.Now,
										StartDate = DateTime.Now,
										LastAccessID = Util.SessionAccess.UserName,
										StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_ReadyForPayment),
										ResponseCode = response.Code,
										ResponseDescription = response.Message,
										MethodName = "KenanAdditionLineRegistration"
									});
								}

							}


						}



						//End joshi added to create kenan account for dealers
						#region "Spend Limit"
						if (resp.ID > 0)
						{
							//Save the Spend Limit
							if (Session[SessionKey.SpendLimit.ToString()].ToString2().Length > 0)
							{
								lnkRegSpendLimit objSpendLimit = new lnkRegSpendLimit();
								objSpendLimit.PlanId = (Session["RegMobileReg_PkgPgmBdlPkgCompID"] == null ? 0 : Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"]));
								objSpendLimit.RegId = resp.ID;
								objSpendLimit.SpendLimit = (Session[SessionKey.SpendLimit.ToString()].ToString2() == "" ? 0 : Convert.ToDecimal(Session[SessionKey.SpendLimit.ToString()]));
								objSpendLimit.CreatedDate = DateTime.Now;
								objSpendLimit.UserName = Util.SessionAccess.UserName;
								LnkRegSpendLimitReq objSpendReq = new LnkRegSpendLimitReq();
								objSpendReq.lnkRegSpendLimitDetails = objSpendLimit;
								int outValue = proxy.SaveSpendLimit(objSpendReq);
							}
							///BYPASS THE BELOW CODE FOR Extra Ten IF REQUEST FROM Multiple MNP
							bool isMultipleMNP = personalDetailsVM.RegTypeID == (int)MobileRegType.RegType_MNPPlanWithMultiSuppline ? true : false;
							if (!isMultipleMNP)
								Util.SaveExtraTen(personalDetailsVM.ExtraTenMobileNumbers, resp.ID);
						}
						//end of saving Spend Limit
						#endregion

						var biometricID = Session["RegMobileReg_BiometricID"].ToInt();
						if (biometricID != 0)
						{
							var biometric = proxy.BiometricGet(new int[] { biometricID }).SingleOrDefault();
							biometric.RegID = resp.ID;

							proxy.BiometricUpdate(biometric);
						}
					}
					using (var proxy = new UserServiceProxy())
					{
						if (Session["IsDeviceRequired"].ToString2() == "Yes")
						{
							proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Registration Done with RegId " + resp.ID, Session["KenanACNumber"].ToString2(), "");
						}
						else
						{
							proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SuppPlan, "Registration Done with RegId " + resp.ID, Session["KenanACNumber"].ToString2(), "");
						}
					}
					#region VLT ADDED CODE on 19th June for Adding SimType
					if (resp.ID > 0)
					{
						
						int regTypeID = 0;
						if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
						{
							regTypeID = Util.GetRegTypeID(REGTYPE.MNPSuppPlan);
						}
						else
						{
							switch (Session["RegMobileReg_Type"].ToInt())
							{
								case (int)MobileRegType.DevicePlan:
									regTypeID = Util.GetRegTypeID(REGTYPE.DevicePlan);
									break;
								case (int)MobileRegType.PlanOnly:
									regTypeID = 8;
									break;
								case (int)MobileRegType.DeviceOnly:
									regTypeID = Util.GetRegTypeID(REGTYPE.DeviceOnly);
									break;
								case (int)MobileRegType.SuppPlan:
									regTypeID = Util.GetRegTypeID(REGTYPE.SuppPlan);
									break;
								case (int)MobileRegType.SecPlan:
									regTypeID = Util.GetRegTypeID(REGTYPE.SecPlan);
									break;
							}
						}
						string PlanKenanCode = MasterDataCache.Instance.PackageComponents.Where(a => a.ID == Convert.ToInt32(Session["RegMobileReg_PkgPgmBdlPkgCompID"])).Select(a => a.KenanCode).SingleOrDefault().ToString2();
						string latestPrintVersion = Util.GetPrintVersionNum(regTypeID, !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty, PlanKenanCode);
						
						if (Session["SimType"] != null || Session[SessionKey.IsSuppleNewAccount.ToString()].ToString2().ToUpper() == "TRUE" || (!string.IsNullOrEmpty(latestPrintVersion)))
						{
							LnkRegDetailsReq objRegDetailsReq = new LnkRegDetailsReq();
							Online.Registration.DAL.Models.LnkRegDetails objRegDetails = new Online.Registration.DAL.Models.LnkRegDetails();
							objRegDetails.CreatedDate = DateTime.Now;





							//Insert SimType 
							if (Session["SimType"].ToString2().Length > 0)
							{
								objRegDetails.SimType = Session["SimType"].ToString2();
							}

							if (Session[SessionKey.IsSuppleNewAccount.ToString()].ToString2().ToUpper() == "TRUE")
							{
								objRegDetails.IsSuppNewAc = true;
							}
							else
							{
								objRegDetails.IsSuppNewAc = false;
							}
							if (Session["SimType"] != null)
							{
								objRegDetails.SimType = Session["SimType"].ToString2();
							}
							else
							{
								objRegDetails.SimType = "Normal";
							}
							objRegDetails.UserName = Util.SessionAccess.UserName;
							objRegDetails.RegId = resp.ID;

							/*Active PDPA Commented*/
							/*Changes by chetan related to PDPA implementation*/
							//using (var registrationProxy = new RegistrationServiceProxy())
							//{
							//    var resultdata = registrationProxy.GetPDPADataInfo("TANDC", "R");
							//    objRegDetails.PdpaVersion = resultdata.Version;
							//}

							/*Recode Active PDPA Commented*/
							objRegDetails.PdpaVersion = personalDetailsVM.PDPDVersion;

							//Insert Justification 
							if (collection["txtJustfication"].ToString2().Length > 0)
							{
								objRegDetails.Justification = collection["txtJustfication"].ToString2();
							}

							//Changed by ravi as already on sep 25 2013 inserting lnkregDetails
							if (Session[SessionKey.IsSuppleNewAccount.ToString()].ToString2().ToUpper() == "TRUE")
							{
								objRegDetails.IsSuppNewAc = true;
							}
							///Changed by ravi as already on sep 25 2013 inserting lnkregDetails ends here

							objRegDetails.MOC_Status = personalDetailsVM.MOCStatus;
							objRegDetails.Liberlization_Status = personalDetailsVM.Liberlization_Status;
							if (!string.IsNullOrEmpty(latestPrintVersion))
							{
								objRegDetails.PrintVersionNo = latestPrintVersion; 
							}
							else
							{
								objRegDetails.PrintVersionNo = string.Empty;
							}
							// null check condition added
							if (Session["AccountswithContractCount"] != null)
							{
								objRegDetails.ContractCheckCount = Session["AccountswithContractCount"].ToInt();
							}
							if (Session[SessionKey.NumberType.ToString()].ToString2() == "g")
							{

								objRegDetails.IsMSISDNReserved = true;
							}
							else
							{
								objRegDetails.IsMSISDNReserved = false;
							}

							//******selected sim card type is assigned*****//
							if (!string.IsNullOrEmpty(collection["SimCardTypeSelected"]))
								objRegDetails.SimCardType = collection["SimCardTypeSelected"].ToString2();

							if (!string.IsNullOrEmpty(collection["SecondarySimCardTypeSelected"]))
								objRegDetails.MismSimCardType = collection["SecondarySimCardTypeSelected"].ToString2();
							//******selected sim card type is assigned*****//

							objRegDetailsReq.LnkDetails = objRegDetails;

							objRegDetails.Liberlization_Status = personalDetailsVM.Liberlization_Status;
							using (var proxy = new RegistrationServiceProxy())
							{
								proxy.SaveLnkRegistrationDetails(objRegDetailsReq);
							}
							Session[SessionKey.NumberType.ToString()] = null;
						}



						if (Session["KenanCustomerInfo"] != null)
						{
							string[] strArrKenanInfo = Session["KenanCustomerInfo"].ToString2().Split('╚');
							KenamCustomerInfo kenanInfo = new KenamCustomerInfo();
							kenanInfo.FullName = strArrKenanInfo[0];
							kenanInfo.IDCardTypeID = Convert.ToInt32(strArrKenanInfo[1]);
							kenanInfo.IDCardNo = strArrKenanInfo[2];
							kenanInfo.NationalityID = Convert.ToInt32(strArrKenanInfo[3]);
							kenanInfo.Address1 = strArrKenanInfo[4];
							kenanInfo.Address2 = strArrKenanInfo[5];
							kenanInfo.Address3 = strArrKenanInfo[6];
							kenanInfo.Postcode = strArrKenanInfo[8];
							kenanInfo.StateID = Convert.ToInt32(strArrKenanInfo[9]);
							kenanInfo.RegID = resp.ID;
							using (var proxys = new RegistrationServiceProxy())
							{
								var kenanResp = new KenanCustomerInfoCreateResp();

								kenanResp = proxys.KenanCustomerInfoCreate(kenanInfo);
							}
						}
						Session["KenanCustomerInfo"] = null;
						/* END Added by VLT 5 Apr 2013 to add kenaninfo to database*/

						#region "Supple New Account Feature"


						Session[SessionKey.IsSuppleNewAccount.ToString()] = null;
						Session[SessionKey.SpendLimit.ToString()] = null;
						#endregion

						WebHelper.Instance.ClearRegistrationSession();
						if (Roles.IsUserInRole("MREG_DSA") || Roles.IsUserInRole("DAgent") || Roles.IsUserInRole("MREG_DSV") || Roles.IsUserInRole("DREG_C") || Roles.IsUserInRole("MREG_DAI") || Roles.IsUserInRole("DREG_SK"))
						{
							return RedirectToAction("DMobileRegSummary", new { regID = resp.ID });
						}
						else
						{
							return RedirectToAction("MobileRegSuccess", new { regID = resp.ID });
						}
					}
					#endregion




				}
				else if (personalDetailsVM.TabNumber == (int)MobileRegistrationSteps.PersonalDetails)
				{
					return RedirectToAction("PersonalDetails");
				}


			}
			catch (Exception ex)
			{
				Logger.Info("Throw Exception for Order ID:" + resp.ID + "Exception:" + ex);
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
				throw ex;
			}

			return RedirectToAction("MobileRegFail", new { regID = personalDetailsVM.RegID });
		}

		public ActionResult PaymntReceived()
		{
			return View();
		}

		[HttpPost]
		public ActionResult RetrieveMyKadInfo(string icno)
		{
			var resp = new AMRSvc.RetrieveMyKadInfoRp();



			using (var proxy = new AMRServiceProxy())
			{

				resp = proxy.RetrieveMyKadInfo(new AMRSvc.RetrieveMyKadInfoRq()
				{
					ApplicationID = 3,
					DealerCode = Util.SessionAccess.User.Org.DealerCode,
					NewIC = icno
				});
			}

			if (string.IsNullOrEmpty(resp.FingerThumb))
			{
				Session["RegMobileReg_BiometricVerify"] = false;
			}
			else
			{
				Session["RegMobileReg_BiometricVerify"] = true;

				using (var proxy = new RegistrationServiceProxy())
				{
					Session["RegMobileReg_BiometricID"] = proxy.BiometricCreate(WebHelper.Instance.ConstructBriometri(resp));
				}
			}

			return Json(resp);
		}



		public ActionResult PaymentDetails(int id)
		{
			Online.Registration.Web.ViewModels.OrderSummaryVM MobileRegOrderSummary = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];
			string Amount = (MobileRegOrderSummary.PersonalDetailsVM.PlanAdvance + MobileRegOrderSummary.PersonalDetailsVM.PlanDeposit + MobileRegOrderSummary.PersonalDetailsVM.DeviceAdvance + MobileRegOrderSummary.PersonalDetailsVM.DeviceDeposit + ((MobileRegOrderSummary.ItemPrice != 0) ? MobileRegOrderSummary.ItemPrice : MobileRegOrderSummary.ModelPrice)).ToString();
			Session["OrderId"] = null;
			Session["Amount"] = null;
			ViewBag.Amount = Amount;
			ViewBag.OrderId = id;
			return View();
		}

		[HttpPost]
		public ActionResult PaymentDetails(FormCollection frm)
		{

			string OrderId = Session["OrderId"].ToString2();
			string Amount = Session["Amount"].ToString2() + "00";
			string HashingSaltValue = "1234567890";
			string HashingSalt = WebHelper.Instance.CalculateMD5Hash(HashingSaltValue + Amount + OrderId + OrderId);

			if (frm["CustomerInfo.PayModeID"] == "1")
			{
				return Redirect("PaymentDetailsSuccess?id=" + OrderId + "&paymentstatus=Success");
			}
			else if (frm["CustomerInfo.PayModeID"] == "2")
			{
				//return Redirect("https://merchantstore/payment.aspx?MerchTxnRef=" + OrderId + "&OrderInfo=" + OrderId + "&Amount=" + Amount + "&Hash=" + HashingSalt);
				return Redirect("http://203.121.86.30/MaxisPaymentISELL/PGGP/doCardPayment.aspx?MerchTxnRef=" + OrderId + "&OrderInfo=" + OrderId + "&Amount=" + Amount + "&Hash=" + HashingSalt);
			}
			return Redirect("PaymentDetailsSuccess?id=" + OrderId + "&paymentstatus=Failed");
		}

		[ValidateInput(false)]
		public ActionResult PaymentDetailsSuccess(string id, string paymentstatus)
		{

			Int32 getId = 0;
			ViewBag.Paymentstatus = paymentstatus;

			if (!Int32.TryParse(id, out getId))
			{
				ViewBag.Paymentstatus = "Failed";
				return View();
			}

			if (!ReferenceEquals(ViewBag.Paymentstatus, null))
			{
				using (var proxy = new RegistrationServiceProxy())
				{
					proxy.RegistrationPaymentSuccess(new RegStatus()
					{
						RegID = Convert.ToInt32(getId),
						Active = true,
						CreateDT = DateTime.Now,
						StartDate = DateTime.Now,
						LastAccessID = Util.SessionAccess.UserName,
						StatusID = "Success".Equals(ViewBag.Paymentstatus) ? Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegPaymentSuccess) : Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegPaymentFailed)
					});
				}
			}
			return RedirectToAction("MobileRegSummary", new { id = getId });

		}

		[HttpPost]
		public ActionResult GetComponentid(string msisdn)
		{
			List<string> depCompIds = new List<string>();
			using (var proxy = new RegistrationServiceProxy())
			{
				depCompIds = proxy.GetDataPlanComponentsForMISM();
			}
			Online.Registration.Web.SubscriberICService.SubscriberICServiceClient
					   icserviceobj = new
					   Online.Registration.Web.SubscriberICService.SubscriberICServiceClient();

			IList<Online.Registration.Web.SubscriberICService.PackageModel>
			packages = icserviceobj.retrievePackageDetls(msisdn,
			System.Configuration.ConfigurationManager.AppSettings["KenanMSISDN"],
			userName:
			Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponseUserName, password: Online.Registration.Web.Properties.Settings.Default.retrieveAcctListByICResponsePassword, postUrl:
			"");
			List<int> UniqComponents = new List<int>();
			foreach (var item1 in packages)
			{
				for (int i = 0; i < item1.compList.Count; i++)
				{
					UniqComponents.Add(item1.compList[i].componentId.ToInt());
				}

			}
			string result = string.Empty;

			foreach (string compid in depCompIds)
			{
				if (UniqComponents.Contains(compid.ToInt()))
				{
					result = "true";
					break;
				}
				else
				{
					result = "false";
				}
			}

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult GetContractbyPlan(string articleID, int modelID, string kenanCode, string packageCode, int pgmBdlPkgCompID, string packageName)
		{
			Session["MandatoryVasIds"] = null;

			ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
			ValueAddedServicesVM Lstvam2 = new ValueAddedServicesVM();
			MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;
			///INSTANCE WILL CONTAIN LIST OF SUPPLEMENTARY LINES HAVING DIFFERENT STATES SUCH AS 
			///MNPSupplimentaryDetailsAddState.NOTINITIALIZED
			///MNPSupplimentaryDetailsAddState.PROCESSING
			///MNPSupplimentaryDetailsAddState.PROCESSED
			///ONLY ONE MSISDN CAN BE IN PROCESSING STATE AT ANY POINT OF TIME
			List<MOCOfferDetails> offers = new List<MOCOfferDetails>();
			MNPSelectPlanForSuppline SuppleMentaryLine = null;
			MNPSupplinePlanStorage MNPSupplinePlanStorage = null;

			//List of Suppline attached to primary line changes
			SelectPlanForSuppline selectPlanForSuppline = null;
			List<SupplinePlanStorage> supplinePlanStorageList;
			SupplinePlanStorage supplinePlanStorage = null;
			Boolean primarySupplimentLines = (Session["IsSupplementaryRequired"] == null) ? false : Convert.ToBoolean(Session["IsSupplementaryRequired"]);
			Online.Registration.Web.CatalogSvc.GetDeviceImageDetailsResp deviceDetail = new GetDeviceImageDetailsResp();
			OrderSummaryVM orderVM = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_OrderSummary : (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];
			orderVM.SelectedPgmBdlPkgCompID = pgmBdlPkgCompID;
			Int32 Selecpgmid;

			Session["RegMobileReg_PkgPgmBdlPkgCompID"] = pgmBdlPkgCompID;

			using (var proxy = new CatalogServiceProxy())
			{
				var pbpc = MasterDataCache.Instance.FilterComponents(new int[] { pgmBdlPkgCompID }).SingleOrDefault();

				if (pbpc != null && !ReferenceEquals(pbpc.KenanCode, null))
				{


					if (primarySupplimentLines)
					{
						supplinePlanStorage.RegMobileReg_UOMCode = pbpc.KenanCode;
					}
					else
					{
						Session["UOMPlanID"] = pbpc.KenanCode;
					}

				}

			}

			///MNP MULTIPORT RELATED CHANGES
			if (IsMNPWithMultpleLines)
			{
				Selecpgmid = (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.SelectedPgmBdlPkgCompID != null) ? orderVM.SelectedPgmBdlPkgCompID : MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID;
			}
			else if (primarySupplimentLines)
			{
				Selecpgmid = (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.SelectedPgmBdlPkgCompID != null) ? orderVM.SelectedPgmBdlPkgCompID : supplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID;
			}
			else
			{
				Selecpgmid = (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.SelectedPgmBdlPkgCompID != null) ? orderVM.SelectedPgmBdlPkgCompID : Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt();
			}
			//using (var catalogProxy = new CatalogServiceClient())
			//{
			//    deviceDetail = catalogProxy.GetDeviceImageDetails(articleID, modelID);
			//}

			List<string> contractList = new List<string>();
			Lstvam = WebHelper.Instance.GetAvailablePackageComponents(Selecpgmid, ref ProcessingMsisdn, isMandatory: false, fromSuppline: true);

			// for supp line with device
			string _mobileRegType = "Other";
			string _regMobileReg_Type = "1";
			
			if (_regMobileReg_Type.ToNullableInt() == (int)MobileRegType.DevicePlan)
			{
				string offernames = string.Empty;
				string planid = primarySupplimentLines ? supplinePlanStorage.RegMobileReg_UOMCode : Session["UOMPlanID"].ToString2();
				//string articleid = Session[SessionKey.ArticleId.ToString()].ToString2();
				string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString2() : string.Empty;
				// drop 5
				Lstvam.ContractOffers = WebHelper.Instance.GetContractOffers(Lstvam, articleID, planid, MOCStatus);
			}

			using (var catalogProxy = new CatalogServiceClient())
			{
				deviceDetail = catalogProxy.GetDeviceImageDetails(articleID, modelID);
			}

			foreach (var v in deviceDetail.deviceDetails.DevicePlanData)
			{
				if (v.PlanName.ToUpper().Contains(packageName.ToUpper()))
				{
					if (!contractList.Contains(v.ContractTerm.ToString2()))
					{
						contractList.Add(v.ContractTerm.ToString2());
					}
				}
			}

			if (!contractList.Any() || contractList.Count() == 0)
			{
				foreach (var contract in Lstvam.ContractOffers)
				{
					if (contract.OfferName.Contains("-"))
					{
						string[] contractArray = contract.OfferName.Trim().Split('-');
						string contractLength = string.Empty;
						Match match = Regex.Match(contract.OfferName, "(12|24)+");
						if (match.Success)
						{
							contractLength = match.Value;
						}
						contractList.Add(contractLength);
					}
					else
					{
						if (contract.OfferName.Contains("+"))
						{
							string[] contractArray = contract.OfferName.Trim().Split('+');
							string contractLength = string.Empty;
							Match match = Regex.Match(contract.OfferName, "(12|24)+");
							if (match.Success)
							{
								contractLength = match.Value;
							}
							contractList.Add(contractLength);
						}
						else
						{
							string contractLength = string.Empty;
							Match match = Regex.Match(contract.OfferName, "(12|24)+");
							if (match.Success)
							{
								contractLength = match.Value;
							}
							contractList.Add(contractLength);
						}
					}
				}
			}

			string processingMsisdn = string.Empty;
			decimal planPrice = 0;
			List<AvailableVAS> MandatoryVASes = WebHelper.Instance.GetAvailablePackageComponents(pgmBdlPkgCompID, ref processingMsisdn, isMandatory: true).MandatoryVASes;
			foreach (var m in MandatoryVASes)
			{
				planPrice += m.Price;
			}

			contractList = contractList.Distinct().ToList();
			//return the JSON to bind the "model" combobox
			return Json(new { ContractList = contractList, PlanPrice = planPrice });
		}


		[Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAI,MREG_DAC,MREG_DC")]
		public ActionResult SelectDevicePlan(int? type)
		{
			var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            Session[SessionKey.Selected_Accessory.ToString()] = null;
			Session["OrderType"] = (int)MobileRegType.SuppPlan;
            List<String> LabelReplace = new List<String>();
            ViewBag.GSTMark = Settings.Default.GSTMark;
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
			dropObj.currentFlow = DropFourConstant.SUPP_FLOW;

			if (dropObj.suppIndex == 0)
			{
				dropObj.suppIndex = dropObj.suppIndex + 1;
				if (dropObj.suppIndex > dropObj.suppFlow.Count)
				{
					dropObj.suppFlow.Add(new Flow());
				}
			}
			else
			{
				// it's set to true in mobile number selection Registration / New Line
				if (dropObj.suppFlow[dropObj.suppIndex - 1]._flowCompleted == true)
				{
					dropObj.suppIndex = dropObj.suppIndex + 1;
					if (dropObj.suppIndex > dropObj.suppFlow.Count)
					{
						dropObj.suppFlow.Add(new Flow());
					}
				}
			}

            //Session["RegMobileReg_OrderSummary"] = null; //Anthony-[#2122]
            //Session["RegMobileReg_PersonalDetailsVM"] = null; //Anthony-[#2122]
			//Session["RegMobileReg_SublineSummary"] = null;
			Session[SessionKey.MSISDN.ToString()] = string.Empty;
			Session["RegMobileReg_VasIDs"] = null;
			dropObj.suppFlow[dropObj.suppIndex - 1].RegType = (int)MobileRegType.DevicePlan;
			Session[SessionKey.DropFourObj.ToString()] = dropObj;

			#region clearing session
			Session["FromPromoOffer"] = null;
			Session["MarketCode"] = null;
			Session["SelectedPlanID"] = null;
			Session["OfferId"] = null;
			Session["Discount"] = null;
			Session["RegMobileReg_MainDevicePrice"] = null;
			Session["RegMobileReg_OrderSummary"] = null;
			Session["RegMobileReg_OfferDevicePrice"] = null;
			Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
			Session["SelectedPlanID_Seco"] = null;
			Session["OfferId_Seco"] = null;
			Session["Discount_Seco"] = null;
			Session["RegMobileReg_MainDevicePrice_Seco"] = null;
			Session["RegMobileReg_OrderSummary_Seco"] = null;
			Session["RegMobileReg_OfferDevicePrice_Seco"] = null;
			Session["RegMobileReg_SelectedModelImageID_Seco"] = null;
			Session["Condition"] = null;
			WebHelper.Instance.ClearSessions();
			ResetDepositSessions();
			ResetAllSession(type);
			Session["ExtratenMobilenumbers"] = null;
			#endregion

			var _regMobilReg = Session["RegMobileReg_Type"];
			// set session as device + plan
			Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;

			#region get device + plan
			SetRegStepNo(MobileRegistrationSteps.Device);
			bool bExceptionRaised = false;
			var devicePlanVM = new DevicePlanPackageVM();

			devicePlanVM.DeviceOptionVM = GetAvailableDeviceOptions();
			// get hero device for the first view
            devicePlanVM.PhoneVM = WebHelper.Instance.GetHeroDevice(this.GetType(), out bExceptionRaised, fromSuppLine:true);
			List<PackageVM> packageList = new List<PackageVM>();

			// get plan based on modelID
			if (!ReferenceEquals(devicePlanVM.PhoneVM, null))
			{
				foreach (var device in devicePlanVM.PhoneVM.ModelImages)
				{
					packageList.Add(WebHelper.Instance.GetDevicePackage(device.BrandArticle.ModelID, device.BrandArticle.ArticleID, isDealer: Util.SessionAccess.User.isDealer, fromSubline: true));
					using (var catalogProxy = new CatalogServiceClient())
					{
						devicePlanVM.deviceDetails.Add(catalogProxy.GetDeviceImageDetails(device.BrandArticle.ArticleID, device.BrandArticle.ModelID));
					}
				}
			}
			devicePlanVM.PackageVMList = packageList.GroupBy(a => a.modelID).Select(grp => grp.First()).ToList();
			#endregion

			if (sharingQuotaFeature)
			{
				List<PackageVM> filteredPackageVM = new List<PackageVM>();

				foreach (var availablepackage in devicePlanVM.PackageVMList)
				{
					PackageVM pkgVM = new PackageVM();
					pkgVM = WebHelper.Instance.checkPrinSuppPlanEligibility(availablepackage, dropObj);
					if (pkgVM != null)
					{
						filteredPackageVM.Add(pkgVM);
					}
				}
				devicePlanVM.PackageVMList = new List<PackageVM>();
				devicePlanVM.PackageVMList.AddRange(filteredPackageVM);
			}


			#region only show for available package
			List<PackageVM> tempPackageVMList = new List<PackageVM>();
			List<int> availableModelID = new List<int>();
			PhoneVM tempPhoneVM = new PhoneVM();
			List<AvailableModelImage> tempModelImages = new List<AvailableModelImage>();

			foreach (var availablepackage in devicePlanVM.PackageVMList)
			{
				if (availablepackage.AvailablePackages.Count > 0)
				{
					tempPackageVMList.Add(availablepackage);
					availableModelID.Add(availablepackage.modelID);
				}
			}
			devicePlanVM.PackageVMList = tempPackageVMList;

			foreach (var phone in devicePlanVM.PhoneVM.ModelImages)
			{
				if (availableModelID.Contains(phone.BrandArticle.ModelID))
				{
					tempModelImages.Add(phone);
				}
			}

			devicePlanVM.PhoneVM.ModelImages = tempModelImages;
			#endregion

			Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary();
			// get lowest price
			Session["DeviceLowestContractPrice"] = WebHelper.Instance.GetLowestHighestPlanAndPrice(devicePlanVM.PhoneVM.ModelImages, useArticleID: false);

			SetRegStepNo(MobileRegistrationSteps.Device);
			// this is for reset vas grouping
			Session["VasGroupIds"] = null;
			ViewBag.BackButton = GetBackButtonDevicePlan();
			Session["supp_devicePlanVM"] = devicePlanVM;
			
			return View(devicePlanVM);
		}

		[HttpPost]
		public ActionResult SelectDevicePlan(FormCollection collection, DevicePlanPackageVM devicePlanPackageVM)
		{
			bool bExceptionRaised = false;
			var devicePlanVM = new DevicePlanPackageVM();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            retrieveAcctListByICResponse allCustomerDetails = (retrieveAcctListByICResponse)SessionManager.Get("AllServiceDetails");
            var checkCustEligibilityAF = WebHelper.Instance.checkCustEligibilityAF(allCustomerDetails, null);

			#region filter
			if (collection["submitType"].ToString2() == "filter")
			{
				devicePlanPackageVM = (DevicePlanPackageVM)Session["supp_devicePlanVM"];
				List<Online.Registration.Web.CatalogSvc.GetDeviceImageDetailsResp> deviceDetails = new List<GetDeviceImageDetailsResp>();
				int brandFilter = collection["brandFilter"].ToInt();
				string modelFilter = collection["modelFilter"].ToString2();
				int capacityFilter = collection["capacityFilter"].ToInt();
				string priceFilter = collection["priceFilter"].ToString2();
				int deviceType = collection["deviceTypeSelected"].ToInt();
				bool availableChecked = collection["availableChecked"].ToBool();
				bool limitedStockChecked = collection["limitedStockChecked"].ToBool();
				bool sellingFastChecked = collection["sellingFastChecked"].ToBool();
				bool outOfStockChecked = collection["outOfStockChecked"].ToBool();

				//19052015 - Add brand, model, and capacity selected to inventoryDashboardVM
				List<SelectListItem> listOfAvailableBrand = Util.GetList(RefType.Brand, defaultValue: string.Empty, defaultText: "All Brand");
				devicePlanVM.BrandSelected = listOfAvailableBrand.Where(x => x.Value == brandFilter.ToString()).Select(y => y.Text).FirstOrDefault();

				//inventoryDashboardVM.ModelSelected = models.Where(x=> x.ID == modelFilter.ToInt()).FirstOrDefault().Name;
				List<SelectListItem> listOfModel = Util.GetList(RefType.Model, defaultValue: string.Empty, defaultText: "All Model");
				devicePlanVM.ModelSelected = listOfModel.Where(x => x.Value == modelFilter).FirstOrDefault().Text;
				devicePlanVM.ModelSelected = System.Text.RegularExpressions.Regex.Replace(devicePlanVM.ModelSelected, @"\S*GB", string.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
				List<SelectListItem> listOfCapacity = Util.GetList(RefType.Capacity, defaultValue: string.Empty, defaultText: "All");
				devicePlanVM.CapacitySelected = listOfCapacity.Where(x => x.Value == capacityFilter.ToString()).Select(y => y.Text).FirstOrDefault();

				devicePlanVM.PriceRangeValue = priceFilter;
				devicePlanVM.inputBrandSelected = brandFilter;
				devicePlanVM.inputCapacitySelected = capacityFilter;
				devicePlanVM.inputModelSelected = modelFilter;
				devicePlanVM.inputDeviceTypeSelected = deviceType;

				int priceFrom = 0;
				int priceTo = 0;
				if (priceFilter.Contains("#"))
				{
					string[] priceArray = priceFilter.Trim().Split('#');
					priceFrom = int.Parse(priceArray[0]);
					priceTo = int.Parse(priceArray[1]);

					string firstword = string.Empty; string secondWord = string.Empty;
					firstword = priceArray[0] == "0" ? "0" : "RM " + priceArray[0];
					secondWord = priceArray[1] == "0" ? "0" : "RM " + priceArray[1];

					devicePlanVM.PriceRangeSelected = string.Format("{0} - {1}", firstword, secondWord);
				}

				devicePlanVM.PhoneVM = WebHelper.Instance.GetFilteredDevice(this.GetType(), out bExceptionRaised, brandFilter, modelFilter, capacityFilter, deviceType, priceFrom, priceTo, availableChecked, limitedStockChecked, sellingFastChecked, outOfStockChecked, 0);

				#region need to increase performance here
				List<PackageVM> packageListFilterDevice = new List<PackageVM>();

				// get plan based on modelID
				if (!ReferenceEquals(devicePlanVM.PhoneVM, null))
				{
					foreach (var device in devicePlanVM.PhoneVM.ModelImages)
					{
						packageListFilterDevice.Add(WebHelper.Instance.GetDevicePackage(device.BrandArticle.ModelID, device.BrandArticle.ArticleID, isDealer: Util.SessionAccess.User.isDealer, fromSubline: true));
						using (var catalogProxy = new CatalogServiceClient())
						{
							devicePlanVM.deviceDetails.Add(catalogProxy.GetDeviceImageDetails(device.BrandArticle.ArticleID, device.BrandArticle.ModelID));
						}
					}
				}
				devicePlanVM.PackageVMList = packageListFilterDevice.GroupBy(a => a.modelID).Select(grp => grp.First()).ToList();

				#region only show for available package
				List<PackageVM> tempPackageVMList = new List<PackageVM>();
				List<int> availableModelID = new List<int>();
				PhoneVM tempPhoneVM = new PhoneVM();
				List<AvailableModelImage> tempModelImages = new List<AvailableModelImage>();

				foreach (var availablepackage in devicePlanVM.PackageVMList)
				{
					if (availablepackage.AvailablePackages.Count > 0)
					{
						tempPackageVMList.Add(availablepackage);
						availableModelID.Add(availablepackage.modelID);
					}
				}
				devicePlanVM.PackageVMList = tempPackageVMList;

				foreach (var phone in devicePlanVM.PhoneVM.ModelImages)
				{
					if (availableModelID.Contains(phone.BrandArticle.ModelID))
					{
						tempModelImages.Add(phone);
					}
				}

				devicePlanVM.PhoneVM.ModelImages = tempModelImages;
				#endregion;

				#endregion

				Session["DeviceLowestContractPrice"] = WebHelper.Instance.GetLowestHighestPlanAndPrice(devicePlanVM.PhoneVM.ModelImages, useArticleID: false);

				devicePlanVM.PhoneVM.SelectedModelImageID = "0";
				devicePlanVM.PhoneVM.SelectedModelImageIDSeco = "0";
				devicePlanVM.PackageVM.SelectedPgmBdlPkgCompID = 0;
				devicePlanVM.PackageVM.SelectedSecondaryPgmBdlPkgCompID = 0;

				if (devicePlanPackageVM != null && devicePlanPackageVM.PackageVMList != null && devicePlanPackageVM.PackageVMList.Count() > 0)
					devicePlanVM.PackageVMList = devicePlanPackageVM.PackageVMList;

				List<String> LabelReplace = new List<String>();
				ViewBag.GSTMark = Settings.Default.GSTMark;
				LabelReplace.Add("GSTMark");
				ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
				
				return View(devicePlanVM);
			}
			#endregion

			#region submit 
			if (collection["submitType"].ToString2() == "")
			{
                //to cleanup VAS session
                Session[SessionKey.SelectedComponents.ToString()] = null;
                Session["ExtratenMobilenumbers"] = null;

				Session["ArticleId"] = collection["selectedArticleID"].ToString().Trim();
				Session["RegMobileReg_OfferDevicePrice"] = collection["deviceOfferPrice"].ToDecimal();
				Session["selectedKenanCode"] = collection["selectedKenanCode"];
				Session["selectedPackageCode"] = collection["selectedPackageCode"];
				string selectedContractLength = collection["selectedContractLength"];
				Session["selectedContractLength"] = selectedContractLength;
				bool isDedicatedSuppFlow = Session[SessionKey.DedicatedSuppFlow.ToString()].ToBool();
				string selectedMSISDN = Session["ExternalID"].ToString2();
				retrieveAcctListByICResponse allServiceDetails = (retrieveAcctListByICResponse)Session["AllServiceDetails"];
                
				devicePlanPackageVM.PhoneVM.DeviceArticleId = collection["selectedArticleID"].ToString().Trim();

				var allArticles = MasterDataCache.Instance.Article;
				devicePlanPackageVM.PhoneVM.SelectedModelImageID = allArticles.Where(x => x.ArticleID.Equals(devicePlanPackageVM.PhoneVM.DeviceArticleId)).SingleOrDefault().ID.ToString2();
				devicePlanPackageVM.PhoneVM.ColourID = allArticles.Where(x => x.ArticleID.Equals(devicePlanPackageVM.PhoneVM.DeviceArticleId)).SingleOrDefault().ColourID;

                Session["RegMobileReg_SelectedModelImageID"] = devicePlanPackageVM.PhoneVM.SelectedModelImageID;//Anthony-[#2122]

				// device
				string selectedModelID = collection["selectedModelID"].ToString2();
				int modelID = int.Parse(selectedModelID);
				devicePlanPackageVM.DeviceOptionVM.TabNumber = 1;
				using (var proxy = new CatalogServiceProxy())
				{
					var modelList = proxy.ModelsList();
					devicePlanPackageVM.DeviceOptionVM.SelectedDeviceOption = modelList.Models.Where(x => x.ID == modelID).Select(y => y.BrandID).SingleOrDefault().ToString();
				}

				// plan + package
				//devicePlanPackageVM.PackageVM.SelectedPgmBdlPkgCompID =

				// phone
				devicePlanPackageVM.PhoneVM.DeviceArticleId = collection["selectedArticleID"].ToString().Trim();

				
				ValueAddedServicesVM Lstvam = new ValueAddedServicesVM();
				string processingMsisdn = string.Empty;
				Lstvam = WebHelper.Instance.GetAvailablePackageComponents(Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(), ref processingMsisdn, isMandatory: false);
				string offernames = string.Empty;
				string planid = Session["UOMPlanID"].ToString();
				string articleid = Session[SessionKey.ArticleId.ToString()].ToString();
				string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString() : string.Empty;
				
				// drop 5
				Lstvam.ContractOffers = WebHelper.Instance.GetContractOffers(Lstvam, articleid, planid, MOCStatus);
            
				foreach (var contract in Lstvam.ContractOffers)
				{
					if (contract.OfferName.Contains(selectedContractLength))
					{
                        //24022015 - Anthony - fix for inconsistent price in order summary - Start
                        var PBPCPlan = 0;
                        try
                        {
                            PBPCPlan = MasterDataCache.Instance.FilterComponents(new int[] { devicePlanPackageVM.PackageVM.SelectedPgmBdlPkgCompID.ToInt() }).FirstOrDefault().ChildID;
                        }
                        catch (Exception e)
                        {
                            PBPCPlan = 0;
                        }

                        if (PBPCPlan != 0)
                        {
                            var findCriteria = new PgmBdlPckComponentFind()
                            {
                                PgmBdlPckComponent = new PgmBdlPckComponent()
                                {
                                    ParentID = PBPCPlan,
                                    LinkType = Properties.Settings.Default.Package_Component
                                },
                                Active = true
                            };

                            if (!string.IsNullOrEmpty(contract.KenanComponent1Id) && !string.IsNullOrEmpty(selectedContractLength))
                            {
                                var ContractID = MasterDataCache.Instance.FilterComponents(findCriteria).Where(x => x.KenanCode.Equals(contract.KenanComponent1Id) && x.Value == selectedContractLength.ToInt()).FirstOrDefault().ID;
                                Session["RegMobileReg_ContractID"] = ContractID;
                            }
                        }
                        //24022015 - Anthony - fix for inconsistent price in order summary - End

						Session["selectedKenanComponent1Id"] = contract.KenanComponent1Id;
						Session["selectedOfferID"] = contract.Id;
                        Session["OfferID"] = contract.Id;//14012015 - Anthony - GST Trade Up
                        Session[SessionKey.TradeUpAmount.ToString()] = null;//14012015 - Anthony - GST Trade Up
						break;
					}
				}

                foreach (var contractVAS in Lstvam.ContractVASes)
                {
                    if (Session["selectedKenanComponent1Id"].ToString2() == contractVAS.KenanCode)
                    {
                        Session["RegMobileReg_ContractID"] = contractVAS.PgmBdlPkgCompID;
                        break;
                    }
                }
          
                //var currFlow = dropObj.currentFlow;
                //var deviceSupp = dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.BrandName;
				Session["MobileRegType"] = "Other";
                OrderSummaryVM odm = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];
                odm.ModelID = modelID;
                Session["RegMobileReg_OrderSummary"] = odm;
                Session["RegMobileReg_OrderSummary"] = Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary();//Anthony-[#2122]
                Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(fromSubline: true);
				DropFourHelpers.UpdateDevice(dropObj, devicePlanPackageVM.DeviceOptionVM);
				DropFourHelpers.UpdatePhone(dropObj, devicePlanPackageVM.PhoneVM);
				DropFourHelpers.UpdatePackage(dropObj, devicePlanPackageVM.PackageVM);

				// validation for quotaShare
				#region QuotaShare

				if (sharingQuotaFeature)
				{
					string errorMsg = isDedicatedSuppFlow ? WebHelper.Instance.quotaShareCheckingQty(selectedMSISDN, allServiceDetails, null, selectedPBPCID: devicePlanPackageVM.PackageVM.SelectedPgmBdlPkgCompID.ToString2())
							: WebHelper.Instance.quotaShareCheckingQty(string.Empty, null, dropObj);

					if (!string.IsNullOrEmpty(errorMsg))
					{
						devicePlanPackageVM = (DevicePlanPackageVM)Session["supp_devicePlanVM"];
						devicePlanPackageVM.errorMsg = errorMsg;
						List<String> LabelReplace = new List<String>();
						ViewBag.GSTMark = Settings.Default.GSTMark;
						LabelReplace.Add("GSTMark");
						ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
						return View(devicePlanPackageVM);
					}
				}

				#endregion

                Session[SessionKey.DropFourObj.ToString()] = dropObj;

				

                //Anthony: New IC (search id type: 1) and Other IC (search id type: 3) will be tagged as Malaysian
                /*if (Session["SearchBy"].ToString2().ToUpper() == "NRIC" || Session["SearchBy"].ToString2().ToUpper() == "NEWIC" || Session["SearchBy"].ToString2().ToUpper() == "OTHERIC")
                {
                    if (!ReferenceEquals(dropObj.personalInformation, null))
                        dropObj.personalInformation.Customer.NationalityID = 1;
                }
                else */
                if (Session[SessionKey.IDCardTypeID.ToString()].ToInt() != 2)
                {//other than passport, the nationality id = 1
                    if (!ReferenceEquals(dropObj.personalInformation, null))
                        dropObj.personalInformation.Customer.NationalityID = 1;
                }
			}
			#endregion

            //if (Session["IsDealer"].ToBool())
            //{
            //    return RedirectToAction("SuppLineVASNew");
            //}
            //else
            //{
                return RedirectToAction("SelectAccessory");
            //}
		}

        //[Authorize(Roles = "MREG_W,MREG_C,DREG_N,DREG_C,MREG_DAI,MREG_DAC")]
        [Authorize(Roles = "MREG_DSV,MREG_DSA,MREG_DSA,MREG_W,MREG_C,MREG_DSV,MREG_DSV,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC,MREG_DN")]
        public ActionResult SelectAccessory(int? type)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var accessoriesVM = new AccessoriesVM();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            Session["af_showOffer"] = null;
            var isDealer = Session["IsDealer"].ToBool();
            Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(fromSubline: true);
            ViewBag.imageURLForAccessory = WebHelper.Instance.GetImageURLFromModelID(dropObj);

            var afEligibilityCheck = Session[SessionKey.AF_Eligibility.ToString()].ToBool();
            var dfException = Session["af_showOffer"].ToString2() == "show" ? true : false;

            var allServiceDetails = (retrieveAcctListByICResponse)Session["AllServiceDetails"];
            //var hasAccessoryFinancingContract = false;
            //var contractDetails = WebHelper.Instance.retrieveContractDetails(allServiceDetails, out hasAccessoryFinancingContract);

            if (isDealer && Util.isCDPUUser() == false && afEligibilityCheck == false)
            {
                ViewBag.ErrorMessage = "Customer is not eligible for Accessories Financing. ";
                ViewBag.HardStopMessage = "Please click \"Next\" to continue to the next page";
            }
			//else if (hasAccessoryFinancingContract)
			//{
			//    ViewBag.ErrorMessage = "Customer has an existing active Accessories Financing contract for this MSISDN. Accessories Financing offer will not be displayed. ";
			//    ViewBag.HardStopMessage = "Please click \"Next\" to continue to the next page";
			//}
            
            Session[SessionKey.DropFourObj.ToString()] = dropObj;

            return View(accessoriesVM);
        }

        [HttpPost]
        public ActionResult SelectAccessory(FormCollection collection, AccessoriesVM accessoriesVM)
        {
            var tabNumber = collection["TabNumber"].ToInt();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();
            Session["RegMobileReg_SublineSummary"] = ConstructOrderSummary(fromSubline: true);

            if (!string.IsNullOrEmpty(collection["TabNumber"].ToString2()) && tabNumber == (int)MobileRegistrationSteps.DevicePlan)
            {
                dropObj.suppIndex = dropObj.suppIndex - 1;
                dropObj.suppFlow.Remove(dropObj.suppFlow[dropObj.suppIndex]);
                Session[SessionKey.DropFourObj.ToString()] = dropObj;
                return RedirectToAction(GetRegActionStep((int)MobileRegistrationSteps.DevicePlan));
            }

            if (collection["af_showOffer"].ToString2() == "show")
            {
                Session["af_showOffer"] = collection["af_showOffer"].ToString2();
                Session[SessionKey.AF_Eligibility.ToString()] = true;
                return RedirectToAction("SelectAccessory");
            }
            
            var selectedAccessory = Session[SessionKey.Selected_Accessory.ToString()] as List<AccessoryVM> ?? new List<AccessoryVM>();
            DropFourHelpers.UpdateSelectedAccessory(dropObj, selectedAccessory);

            Session[SessionKey.DropFourObj.ToString()] = dropObj;

            return RedirectToAction(GetRegActionStep(tabNumber));
        }

		[Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAI,MREG_DAC,MREG_DC")]
		public ActionResult SelectDevice(int? type)
		{
			Session["FromPromoOffer"] = null;
			Session["MarketCode"] = null;
			Session["SelectedPlanID"] = null;
			Session["OfferId"] = null;
			Session["Discount"] = null;
			Session["RegMobileReg_MainDevicePrice"] = null;
			Session["RegMobileReg_OrderSummary"] = null;
			Session["RegMobileReg_OfferDevicePrice"] = null;
			Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
			Session["SelectedPlanID_Seco"] = null;
			Session["OfferId_Seco"] = null;
			Session["Discount_Seco"] = null;
			Session["RegMobileReg_MainDevicePrice_Seco"] = null;
			Session["RegMobileReg_OrderSummary_Seco"] = null;
			Session["RegMobileReg_OfferDevicePrice_Seco"] = null;
			Session["RegMobileReg_SelectedModelImageID_Seco"] = null;
			Session["Condition"] = null;
			WebHelper.Instance.ClearSessions();
			ResetDepositSessions();
			ResetAllSession(type);
			Session["ExtratenMobilenumbers"] = null;

			SetRegStepNo(MobileRegistrationSteps.Device);

			Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();
			var deviceOptVM = GetAvailableDeviceOptions();

			return View(deviceOptVM);
		}


		[HttpPost]
		[Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
		public ActionResult SelectDevice(DeviceOptionVM devOptVM)
		{
			Session["RegMobileReg_TabIndex"] = devOptVM.TabNumber;
			Session["DeviceSelected"] = 1;
			var options = (!string.IsNullOrEmpty(devOptVM.SelectedDeviceOption)) ? devOptVM.SelectedDeviceOption.Split('_').ToList() : null;
			if (options != null)
			{
				try
				{
					if (devOptVM.TabNumber == (int)MobileRegistrationSteps.DeviceCatalog)
					{
						// selected different Device Option
						if (Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToInt() != options[1].ToInt() ||
							Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToInt() != options[2].ToInt())
						{
							Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()] = options[1];
							Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()] = options[2];

							// reset session
							Session["RegMobileReg_PhoneVM"] = null;
							Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
							Session["RegMobileReg_SelectedModelID"] = null;
							Session["RegMobileReg_PkgPgmBdlPkgCompID"] = null;
							Session["RegMobileReg_ProgramMinAge"] = null;
							Session["RegMobileReg_ContractID"] = null;
							Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
							Session["RegMobileReg_VasIDs"] = null;
							Session["RegMobileReg_VasNames"] = null;
							Session["RegMobileReg_MobileNo"] = null;
							Session["RegMobileReg_OrderSummary"] = null;
						}
					}
					using (var proxy = new UserServiceProxy())
					{
						proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Select Brand For Supplementary", Session["KenanACNumber"].ToString2(), "");
					}
				}
				catch (Exception ex)
				{
					WebHelper.Instance.LogExceptions(this.GetType(), ex);
					throw ex;
				}

				return RedirectToAction(GetRegActionStep(devOptVM.TabNumber));
			}
			else
			{
				return RedirectToAction("AddSuppLine");
			}

		}

		[Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
		public ActionResult SelectDeviceCatalog()
		{
			Session["SelectedPlanID"] = null;
			Session["OfferId"] = null;
			Session["Discount"] = null;
			Session["RegMobileReg_MainDevicePrice"] = null;
			Session["RegMobileReg_OrderSummary"] = null;
			Session["RegMobileReg_OfferDevicePrice"] = null;
			Session["RegMobileReg_UpfrontPayment"] = null;
			ResetDepositSessions();
			SetRegStepNo(MobileRegistrationSteps.DeviceCatalog);

			Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();
			bool bExceptionRaised = false;
			var phoneVM = WebHelper.Instance.GetAvailableModelImage(this.GetType(), out bExceptionRaised);
			if (bExceptionRaised)
				RedirectToAction("StoreError", "HOME");

			return View(phoneVM);
		}

		[HttpPost]
		[Authorize(Roles = "MREG_W,MREG_C,MREG_DSV,MREG_DSA,DREG_N,DREG_C,MREG_DAI,MREG_DAC,MREG_DC")]
		public ActionResult SelectDeviceCatalog(PhoneVM phoneVM)
		{
			Session["RegMobileReg_TabIndex"] = phoneVM.TabNumber;
			Session["DeviceCatalogSeco"] = "DeviceCatalogSeco";
			try
			{
				if (phoneVM.TabNumber == (int)MobileRegistrationSteps.Plan)
				{
					// selected different Model Image
					if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToString2() != phoneVM.SelectedModelImageID)
					{
						Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = phoneVM.SelectedModelImageID;

						// reset session
						Session["RegMobileReg_PkgPgmBdlPkgCompID"] = null;
						Session["RegMobileReg_ProgramMinAge"] = null;
						Session["RegMobileReg_ContractID"] = null;
						Session[SessionKey.RegMobileReg_DataplanID.ToString()] = null;
						Session["RegMobileReg_VasIDs"] = null;
						Session["RegMobileReg_VasNames"] = null;
					}
				}

				// Outright Sales
				if (Convert.ToInt32(Session["RegMobileReg_Type"]) == (int)MobileRegType.DeviceOnly)
				{
					Session["RegMobileReg_TabIndex"] = (int)RegistrationSteps.PersonalDetails;
					phoneVM.TabNumber = (int)RegistrationSteps.PersonalDetails;
				}
				using (var proxy = new UserServiceProxy())
				{
					proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.SupplementaryPlanwithDevice, "Select Device for Supplementary", Session["KenanACNumber"].ToString2(), "");
				}
			}
			catch (Exception ex)
			{
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
				throw ex;
			}
			return RedirectToAction(GetRegActionStep(phoneVM.TabNumber));
		}


		#endregion Action Methods


		#region Private Methods

		/// <summary>
		/// Gets a value indicating whether this instance is MNP with multple lines.
		/// </summary>
		/// <value>
		/// <c>true</c> if this instance is MNP with multple lines; otherwise, <c>false</c>.
		/// </value>
		private bool IsMNPWithMultpleLines
		{
			get
			{
				///TO AVOID SHARING OF PRIMARY LINE RELATED SESSIONS NEW STRONG TYPE OF DATA INTRODUCED
				///BELOW CHECKS ENSURES THE REQUEST IS FOR MNP AND SUPPLEMENTARY LINES FOR MULTI PORT IN EXISTS
				return ((!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()])) && (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null) && (!ReferenceEquals(((MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()]).SupplimentaryMSISDNs, null) && ((MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()]).SupplimentaryMSISDNs.Count > 0)));
			}
		}

		private ComponentViewModel GetSelectedPackageInfo_smart(int modelId, int packageId, string kenanCode)
		{
			ComponentViewModel pkgResults = new ComponentViewModel();
			List<ModelPackageInfo> listRes = null;
			using (var proxy = new CatalogServiceProxy())
			{
				listRes = proxy.GetModelPackageInfo_smart(modelId, packageId, kenanCode);
			}

			List<NewPackages> listNewPackages = new List<NewPackages>();
			NewComponents objNewComponents;
			List<NewComponents> listNewComponents = new List<NewComponents>();

			NewPackages objNewPackagesPC = new NewPackages();
			NewPackages objNewPackagesMA = new NewPackages();
			NewPackages objNewPackagesEC = new NewPackages();
			NewPackages objNewPackagesDC = new NewPackages();
			NewPackages objNewPackagesOC = new NewPackages();
			NewPackages objNewPackagesRC = new NewPackages();
			NewPackages objNewPackagesCR = new NewPackages();

			if (listRes != null)
			{
				foreach (var item in listRes)
				{

					if (item.packageType == "PC")
					{
						objNewPackagesPC.NewPackageId = item.PackageId.ToString2();
						objNewPackagesPC.NewPackageName = item.PackageName;
						objNewPackagesPC.Type = item.packageType;

						objNewComponents = new NewComponents();
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.ReassignIsmandatory = item.IsMandatory;
						objNewComponents.Type = item.packageType;
						objNewComponents.isDefault = item.isDefault;
						listNewComponents.Add(objNewComponents);

					}
					if (item.packageType == "MA")
					{
						objNewPackagesMA.NewPackageId = item.PackageId.ToString2();
						objNewPackagesMA.NewPackageName = item.PackageName;
						objNewPackagesMA.Type = item.packageType;

						objNewComponents = new NewComponents();
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.IsChecked = item.IsMandatory;
						objNewComponents.Type = item.packageType;
						objNewComponents.isDefault = item.isDefault;
						listNewComponents.Add(objNewComponents);
						Session["NewMPID"] = item.PackageId;

					}
					if (item.packageType == "EC")
					{
						objNewPackagesEC.NewPackageId = item.PackageId.ToString2();
						objNewPackagesEC.NewPackageName = item.PackageName;
						objNewPackagesEC.Type = item.packageType;

						objNewComponents = new NewComponents();
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.IsChecked = item.IsMandatory;
						objNewComponents.Type = item.packageType;
						objNewComponents.isDefault = item.isDefault;
						listNewComponents.Add(objNewComponents);

					}
					if (item.packageType == "DC" || item.packageType == "DM" || item.packageType == "MD")
					{
						TempData["DataComponentPkgKenancode"] = item.KenanCode;
						TempData["DataComponentPkgId"] = item.PackageId.ToString2();
						objNewPackagesDC.NewPackageId = item.PackageId.ToString2();
						objNewPackagesDC.NewPackageName = item.PackageName;
						objNewPackagesDC.Type = item.packageType;

						objNewComponents = new NewComponents();
						objNewComponents.ComponentGroupId = item.ComponentGroupId;
						objNewComponents.ComponentGroupName = item.packageType;
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.IsChecked = item.IsMandatory;
						objNewComponents.Type = item.packageType;
						objNewComponents.isDefault = item.isDefault;
						listNewComponents.Add(objNewComponents);
						//}
					}

					if (item.packageType == "OC")
					{

						objNewPackagesOC.NewPackageId = item.PackageId.ToString2();
						objNewPackagesOC.NewPackageName = item.PackageName;
						objNewPackagesOC.Type = item.packageType;
						objNewComponents = new NewComponents();
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.isDefault = item.isDefault;
						objNewComponents.Type = item.packageType;
						objNewComponents.IsChecked = item.IsMandatory;
						listNewComponents.Add(objNewComponents);
					}


					if (item.packageType == "RC")
					{


						objNewPackagesRC.NewPackageId = item.PackageId.ToString2();
						objNewPackagesRC.NewPackageName = item.PackageName;
						objNewPackagesPC.Type = item.packageType;

						objNewComponents = new NewComponents();
						objNewComponents.NewComponentId = item.ComponentId.ToString2();
						objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
						objNewComponents.NewComponentName = item.ComponentName;
						objNewComponents.NewPackageId = item.PackageId.ToString2();
						objNewComponents.NewPackageKenanCode = item.KenanCode;
						objNewComponents.ReassignIsmandatory = item.IsMandatory;
						objNewComponents.isCRP = item.isCRP;
						objNewComponents.Type = item.packageType;
						objNewComponents.isDefault = item.isDefault;

						listNewComponents.Add(objNewComponents);




					}
					if (item.packageType == "CR")
					{
						objNewPackagesCR.NewPackageId = item.PackageId.ToString2();
						objNewPackagesCR.NewPackageName = item.PackageName;
						objNewPackagesCR.Type = item.packageType;
						if (!item.isCRP)
						{
							objNewComponents = new NewComponents();
							objNewComponents.NewComponentId = item.ComponentId.ToString2();
							objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
							objNewComponents.NewComponentName = item.ComponentName;
							objNewComponents.NewPackageId = item.PackageId.ToString2();
							objNewComponents.NewPackageKenanCode = item.KenanCode;
							objNewComponents.ReassignIsmandatory = item.IsMandatory;
							objNewComponents.Type = item.packageType;
							objNewComponents.isDefault = item.isDefault;
							objNewComponents.ComponentGroupId = item.ComponentGroupId;

							listNewComponents.Add(objNewComponents);
						}
						else
						{
							if (Session["Package"] != null)
							{
								IList<Online.Registration.Web.SubscriberICService.PackageModel> oldCompPackage = (IList<Online.Registration.Web.SubscriberICService.PackageModel>)Session["Package"];
								foreach (var pkg in oldCompPackage)
								{
									foreach (var comp in pkg.compList)
									{
										if ((comp.componentId == "45310" || comp.componentId == "45308"))
										{
											if (comp.componentId == item.ComponentKenanCode.ToString())
											{
												objNewComponents = new NewComponents();
												objNewComponents.NewComponentId = item.ComponentId.ToString2();
												objNewComponents.NewComponentKenanCode = item.ComponentKenanCode.ToString2();
												objNewComponents.NewComponentName = item.ComponentName;
												objNewComponents.NewPackageId = item.PackageId.ToString2();
												objNewComponents.NewPackageKenanCode = item.KenanCode;
												objNewComponents.ReassignIsmandatory = item.IsMandatory;
												objNewComponents.isCRP = item.isCRP;
												objNewComponents.Type = item.packageType;
												objNewComponents.ComponentGroupId = item.ComponentGroupId;
												listNewComponents.Add(objNewComponents);
											}

										}
									}
								}
							}

						}
					}
					NewPackages pkgGroup = new NewPackages();
					pkgGroup.NewPackageId = item.PackageId.ToString2();
					pkgGroup.NewPackageName = item.PackageName;
					pkgGroup.Type = item.packageType;

					var existingpkg = from pkg in listNewPackages where pkg.NewPackageId == item.PackageId.ToString() select pkg;

					if (existingpkg.Count() <= 0)
					{
						listNewPackages.Add(pkgGroup);
					}
					// end
				}

				if (!ReferenceEquals(objNewPackagesOC, null))
					listNewPackages.Add(objNewPackagesOC);

				pkgResults.NewComponentsList = listNewComponents;
				pkgResults.NewPackagesList = listNewPackages;
			}

			return pkgResults;

		}


		#region New order summary
        private OrderSummaryVM ConstructOrderSummary(bool fromSubline = false, bool standalone = false)
        {
            return WebHelper.Instance.ConstructOrderSummary(string.Empty, this.GetType().Name, fromSubline, standalone);
        }
		#endregion

		private OrderSummaryVM ConstructOrderSummaryMNP(bool fromSubline = false)
		{
			string strDataPlanID;
			string strContractId;
			var components = new List<Component>();
			var modelImage = new ModelImage();
			var package = new Package();
			var bundle = new Bundle();
			var orderVM = new OrderSummaryVM();
			PhoneVM phoneVM = null;
			MNPSelectPlanForSuppline SuppleMentaryLine = null;
			MNPSupplinePlanStorage MNPSupplinePlanStorage = null;
			MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;
			PersonalDetailsVM personalDetailsVM = null;

			///MNP MULTIPORT RELATED CHANGES
			if (IsMNPWithMultpleLines)
			{
				///GET THE MNPPrimaryPlanStorage INSTANCE CONTAINING PRIMARY PLAN STORAGE
				MNPPrimaryPlanStorage = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];
				//orderVM = MNPPrimaryPlanStorage.RegMobileReg_OrderSummary;
				if (!ReferenceEquals(Session[SessionKey.MNPSupplementaryLines.ToString()], null))
				{
					///GET ALL SUPPLEMENTARY LINES STORED IN SESSION WHICH WAS SET FROM EnterDetails PAGE
					SuppleMentaryLine = (MNPSelectPlanForSuppline)Session[SessionKey.MNPSupplementaryLines.ToString()];

					///CHECK IF THERE ARE ANY, IN PROCESS MSISDN'S
					if (SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().Count > 0)
					{
						ProcessingMsisdn = SuppleMentaryLine.SupplimentaryMSISDNs.Where(i => i.Status == MNPSupplimentaryDetailsAddState.PROCESSING.ToInt()).ToList().FirstOrDefault().Msisdn;
						///GET THE SUPPLEMENTARY OBJECT CORRESPONDING TO ProcessingMsisdn
						MNPSupplinePlanStorage = (MNPPrimaryPlanStorage.MNPSupplinePlanStorageList.Where(i => i.RegMobileReg_MSISDN.Equals(ProcessingMsisdn)).ToList().FirstOrDefault());
					}
				}
			}

			///MNP MULTIPORT RELATED CHANGES
			if (IsMNPWithMultpleLines)
			{
				phoneVM = !ReferenceEquals(MNPSupplinePlanStorage.RegMobileReg_PhoneVM, null) ? MNPSupplinePlanStorage.RegMobileReg_PhoneVM : null;
				personalDetailsVM = MNPSupplinePlanStorage.RegMobileReg_PersonalDetailsVM == null ? new PersonalDetailsVM() : (PersonalDetailsVM)MNPSupplinePlanStorage.RegMobileReg_PersonalDetailsVM;
			}
			else
			{
				phoneVM = (PhoneVM)Session["RegMobileReg_PhoneVM"];
				personalDetailsVM = Session["RegMobileReg_PersonalDetailsVM"] == null ? new PersonalDetailsVM() : (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
			}

			int varDataPlanId = 0;

			//personalDetailsVM = Session["RegMobileReg_PersonalDetailsVM"] == null ? new PersonalDetailsVM() : (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];// Main Line
			if (!fromSubline)
			{
				if (IsMNPWithMultpleLines)
				{
					if (MNPSupplinePlanStorage.RegMobileReg_OrderSummary != null)
						orderVM = MNPSupplinePlanStorage.RegMobileReg_OrderSummary;
				}
				else
				{
					if ((OrderSummaryVM)Session["RegMobileReg_OrderSummary"] != null)
						orderVM = (OrderSummaryVM)Session["RegMobileReg_OrderSummary"];
				}

				if (Session["RegMobileReg_DataPkgID"] != null)
				{
					if (IsMNPWithMultpleLines)
						strDataPlanID = MNPSupplinePlanStorage.RegMobileReg_DataPkgID.ToString2();
					else
						strDataPlanID = Session["RegMobileReg_DataPkgID"].ToString2();

					string _validateContractID = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2() : Session["RegMobileReg_ContractID"].ToString2();

					string ValidateMobilePlanType = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_Type.ToString2().ToUpper() : Session["MobileRegType"].ToString2().ToUpper();

					if (string.IsNullOrEmpty(_validateContractID) && (ValidateMobilePlanType == "PLANONLY"))
					{
						if (IsMNPWithMultpleLines)
							strContractId = MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2();
						else
							strContractId = Session["RegMobileReg_ContractID"].ToString2();

						using (var proxys = new CatalogServiceProxy())
						{
							varDataPlanId = proxys.GettllnkContractDataPlan(strContractId.ToInt());
						}

						//added by deepika 10 march
						if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID != 0 && strContractId.ToInt() != 0 && varDataPlanId.ToInt() != 0 || varDataPlanId.ToInt() == 0)
						{
							using (var proxy = new CatalogServiceProxy())
							{
								List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
								varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, strContractId.ToInt(), varDataPlanId.ToInt() }).ToList();
								for (int i = 0; i < varAdvDeposit.Count; i++)
									if (varAdvDeposit[i] != null && varAdvDeposit[i].modelId != 0)
									{
										var Malaytotalprice = varAdvDeposit[i].malyDevAdv + varAdvDeposit[i].malayPlanAdv + varAdvDeposit[i].malayDevDeposit + varAdvDeposit[i].malyPlanDeposit;
										var Othertotalprice = varAdvDeposit[i].othDevAdv + varAdvDeposit[i].othPlanAdv + varAdvDeposit[i].othDevDeposit + varAdvDeposit[i].othPlanDeposit;
										orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
										orderVM.MalayAdvDevPrice = Malaytotalprice;
										orderVM.OthersAdvDevPrice = Othertotalprice;
										Session["RegMobileReg_MalAdvDeposit"] = orderVM.MalayAdvDevPrice;
										Session["RegMobileReg_OtherAdvDeposit"] = orderVM.OthersAdvDevPrice;

										/*Added by chetan for split adv & deposit*/
										orderVM.MalyDevAdv = varAdvDeposit[i].malyDevAdv;
										orderVM.MalayPlanAdv = varAdvDeposit[i].malayPlanAdv;
										orderVM.MalayDevDeposit = varAdvDeposit[i].malayDevDeposit;
										orderVM.MalyPlanDeposit = varAdvDeposit[i].malyPlanDeposit;
										orderVM.OthDevAdv = varAdvDeposit[i].othDevAdv;
										orderVM.OthPlanAdv = varAdvDeposit[i].othPlanAdv;
										orderVM.OthDevDeposit = varAdvDeposit[i].othDevDeposit;
										orderVM.OthPlanDeposit = varAdvDeposit[i].othPlanDeposit;

										Session["RegMobileReg_MalyDevAdv"] = orderVM.MalyDevAdv;
										Session["RegMobileReg_MalayPlanAdv"] = orderVM.MalayPlanAdv;
										Session["RegMobileReg_MalayDevDeposit"] = orderVM.MalayDevDeposit;
										Session["RegMobileReg_MalyPlanDeposit"] = orderVM.MalyPlanDeposit;
										Session["RegMobileReg_OthDevAdv"] = orderVM.OthDevAdv;
										Session["RegMobileReg_OthPlanAdv"] = orderVM.OthPlanAdv;
										Session["RegMobileReg_OthDevDeposit"] = orderVM.OthDevDeposit;
										Session["RegMobileReg_OthPlanDeposit"] = orderVM.OthPlanDeposit;
										/*up to here*/

										if (personalDetailsVM.Customer.NationalityID != 0)
										{
											if (personalDetailsVM.Customer.NationalityID == 1)
											{
												/*Added by chetan for split adv & deposit*/
												Session["RegMobileReg_MalyDevAdv"] = orderVM.MalyDevAdv;
												Session["RegMobileReg_MalayPlanAdv"] = orderVM.MalayPlanAdv;
												Session["RegMobileReg_MalayDevDeposit"] = orderVM.MalayDevDeposit;
												Session["RegMobileReg_MalyPlanDeposit"] = orderVM.MalyPlanDeposit;
												/*up to here*/
												Session["RegMobileReg_MalAdvDeposit"] = orderVM.MalayAdvDevPrice;
												orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
											}
											else
											{
												/*Added by chetan for split adv & deposit*/
												Session["RegMobileReg_OthDevAdv"] = orderVM.OthDevAdv;
												Session["RegMobileReg_OthPlanAdv"] = orderVM.OthPlanAdv;
												Session["RegMobileReg_OthDevDeposit"] = orderVM.OthDevDeposit;
												Session["RegMobileReg_OthPlanDeposit"] = orderVM.OthPlanDeposit;
												/*up to here*/
												Session["RegMobileReg_OtherAdvDeposit"] = orderVM.OthersAdvDevPrice;
												orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
											}
										}
									}
							}
						}

					}
					//ranjeeth 26thJuly2013

					else if (string.IsNullOrEmpty(_validateContractID) && (ValidateMobilePlanType == "Planonly"))
					{
						//added by deepika 10 march
						if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID == 0 && orderVM.ContractID == 0)
						{
							using (var proxy = new CatalogServiceProxy())
							{
								var varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, orderVM.ContractID, 0 }).SingleOrDefault();
								if (varAdvDeposit != null)
								{
									var Malaytotalprice = varAdvDeposit.malyDevAdv + varAdvDeposit.malayPlanAdv + varAdvDeposit.malayDevDeposit + varAdvDeposit.malyPlanDeposit;
									var Othertotalprice = varAdvDeposit.othDevAdv + varAdvDeposit.othPlanAdv + varAdvDeposit.othDevDeposit + varAdvDeposit.othPlanDeposit;
									orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
									orderVM.MalayAdvDevPrice = Malaytotalprice;
									orderVM.OthersAdvDevPrice = Othertotalprice;
									Session["RegMobileReg_MalAdvDeposit"] = orderVM.MalayAdvDevPrice;
									Session["RegMobileReg_OtherAdvDeposit"] = orderVM.OthersAdvDevPrice;

									/*Added by chetan for split adv & deposit*/
									orderVM.MalyDevAdv = varAdvDeposit.malyDevAdv;
									orderVM.MalayPlanAdv = varAdvDeposit.malayPlanAdv;
									orderVM.MalayDevDeposit = varAdvDeposit.malayDevDeposit;
									orderVM.MalyPlanDeposit = varAdvDeposit.malyPlanDeposit;
									orderVM.OthDevAdv = varAdvDeposit.othDevAdv;
									orderVM.OthPlanAdv = varAdvDeposit.othPlanAdv;
									orderVM.OthDevDeposit = varAdvDeposit.othDevDeposit;
									orderVM.OthPlanDeposit = varAdvDeposit.othPlanDeposit;

									Session["RegMobileReg_MalyDevAdv"] = orderVM.MalyDevAdv;
									Session["RegMobileReg_MalayPlanAdv"] = orderVM.MalayPlanAdv;
									Session["RegMobileReg_MalayDevDeposit"] = orderVM.MalayDevDeposit;
									Session["RegMobileReg_MalyPlanDeposit"] = orderVM.MalyPlanDeposit;
									Session["RegMobileReg_OthDevAdv"] = orderVM.OthDevAdv;
									Session["RegMobileReg_OthPlanAdv"] = orderVM.OthPlanAdv;
									Session["RegMobileReg_OthDevDeposit"] = orderVM.OthDevDeposit;
									Session["RegMobileReg_OthPlanDeposit"] = orderVM.OthPlanDeposit;
									/*up to here*/

									if (personalDetailsVM.Customer.NationalityID != 0)
									{
										if (personalDetailsVM.Customer.NationalityID == 1)
										{
											/*Added by chetan for split adv & deposit*/
											Session["RegMobileReg_MalyDevAdv"] = orderVM.MalyDevAdv;
											Session["RegMobileReg_MalayPlanAdv"] = orderVM.MalayPlanAdv;
											Session["RegMobileReg_MalayDevDeposit"] = orderVM.MalayDevDeposit;
											Session["RegMobileReg_MalyPlanDeposit"] = orderVM.MalyPlanDeposit;
											/*up to here*/
											Session["RegMobileReg_MalAdvDeposit"] = orderVM.MalayAdvDevPrice;
											orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
										}
										else
										{
											/*Added by chetan for split adv & deposit*/
											Session["RegMobileReg_OthDevAdv"] = orderVM.OthDevAdv;
											Session["RegMobileReg_OthPlanAdv"] = orderVM.OthPlanAdv;
											Session["RegMobileReg_OthDevDeposit"] = orderVM.OthDevDeposit;
											Session["RegMobileReg_OthPlanDeposit"] = orderVM.OthPlanDeposit;
											/*up to here*/
											Session["RegMobileReg_OtherAdvDeposit"] = orderVM.OthersAdvDevPrice;
											orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
										}
									}
								}
							}
						}
					}
				}
				else
				{
					strDataPlanID = "0";

					string _validateContractID = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString2() : Session["RegMobileReg_ContractID"].ToString2();
					string _ValidateMobilePlanType = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_Type.ToString2().ToUpper() : Session["MobileRegType"].ToString2().ToUpper();

					if (_validateContractID != string.Empty && _ValidateMobilePlanType != "PLANONLY")
					{
						//ranjeeth 26thJuly2013
						if (IsMNPWithMultpleLines)
							strContractId = MNPSupplinePlanStorage.RegMobileReg_ContractID.ToString();
						else
							strContractId = Session["RegMobileReg_ContractID"].ToString2();

						using (var proxys = new CatalogServiceProxy())
						{
							List<int> lstContracts = WebHelper.Instance.CommaSplitToList(strContractId);

							varDataPlanId = proxys.GettllnkContractDataPlan(lstContracts.ToInt());
						}

						//added by deepika 10 march
						if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID != 0 && strContractId.ToInt() != 0 && varDataPlanId.ToInt() != 0 || varDataPlanId.ToInt() == 0)
						{
							using (var proxy = new CatalogServiceProxy())
							{
								List<AdvDepositPrice> varAdvDeposit = new List<AdvDepositPrice>();
								varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, strContractId.ToInt(), varDataPlanId.ToInt() }).ToList();
								for (int i = 0; i < varAdvDeposit.Count; i++)
									if (varAdvDeposit[i] != null && varAdvDeposit[i].modelId != 0)
									{
										var Malaytotalprice = varAdvDeposit[i].malyDevAdv + varAdvDeposit[i].malayPlanAdv + varAdvDeposit[i].malayDevDeposit + varAdvDeposit[i].malyPlanDeposit;
										var Othertotalprice = varAdvDeposit[i].othDevAdv + varAdvDeposit[i].othPlanAdv + varAdvDeposit[i].othDevDeposit + varAdvDeposit[i].othPlanDeposit;
										orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
										orderVM.MalayAdvDevPrice = Malaytotalprice;
										orderVM.OthersAdvDevPrice = Othertotalprice;
										Session["RegMobileReg_MalAdvDeposit"] = orderVM.MalayAdvDevPrice;
										Session["RegMobileReg_OtherAdvDeposit"] = orderVM.OthersAdvDevPrice;

										/*Added by chetan for split adv & deposit*/
										orderVM.MalyDevAdv = varAdvDeposit[i].malyDevAdv;
										orderVM.MalayPlanAdv = varAdvDeposit[i].malayPlanAdv;
										orderVM.MalayDevDeposit = varAdvDeposit[i].malayDevDeposit;
										orderVM.MalyPlanDeposit = varAdvDeposit[i].malyPlanDeposit;
										orderVM.OthDevAdv = varAdvDeposit[i].othDevAdv;
										orderVM.OthPlanAdv = varAdvDeposit[i].othPlanAdv;
										orderVM.OthDevDeposit = varAdvDeposit[i].othDevDeposit;
										orderVM.OthPlanDeposit = varAdvDeposit[i].othPlanDeposit;

										Session["RegMobileReg_MalyDevAdv"] = orderVM.MalyDevAdv;
										Session["RegMobileReg_MalayPlanAdv"] = orderVM.MalayPlanAdv;
										Session["RegMobileReg_MalayDevDeposit"] = orderVM.MalayDevDeposit;
										Session["RegMobileReg_MalyPlanDeposit"] = orderVM.MalyPlanDeposit;
										Session["RegMobileReg_OthDevAdv"] = orderVM.OthDevAdv;
										Session["RegMobileReg_OthPlanAdv"] = orderVM.OthPlanAdv;
										Session["RegMobileReg_OthDevDeposit"] = orderVM.OthDevDeposit;
										Session["RegMobileReg_OthPlanDeposit"] = orderVM.OthPlanDeposit;
										/*up to here*/

										if (personalDetailsVM.Customer.NationalityID != 0)
										{
											if (personalDetailsVM.Customer.NationalityID == 1)
											{
												/*Added by chetan for split adv & deposit*/
												Session["RegMobileReg_MalyDevAdv"] = orderVM.MalyDevAdv;
												Session["RegMobileReg_MalayPlanAdv"] = orderVM.MalayPlanAdv;
												Session["RegMobileReg_MalayDevDeposit"] = orderVM.MalayDevDeposit;
												Session["RegMobileReg_MalyPlanDeposit"] = orderVM.MalyPlanDeposit;
												/*up to here*/
												Session["RegMobileReg_MalAdvDeposit"] = orderVM.MalayAdvDevPrice;
												orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
											}
											else
											{
												/*Added by chetan for split adv & deposit*/
												Session["RegMobileReg_OthDevAdv"] = orderVM.OthDevAdv;
												Session["RegMobileReg_OthPlanAdv"] = orderVM.OthPlanAdv;
												Session["RegMobileReg_OthDevDeposit"] = orderVM.OthDevDeposit;
												Session["RegMobileReg_OthPlanDeposit"] = orderVM.OthPlanDeposit;
												/*up to here*/
												Session["RegMobileReg_OtherAdvDeposit"] = orderVM.OthersAdvDevPrice;
												orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
											}
										}
									}
							}
						}

					}
					else if (string.IsNullOrEmpty(_validateContractID) && _ValidateMobilePlanType == "Planonly")
					{

						if (orderVM.SelectedPgmBdlPkgCompID != 0 && orderVM.ModelID == 0 && orderVM.ContractID == 0)
						{
							using (var proxy = new CatalogServiceProxy())
							{
								var varAdvDeposit = proxy.AdvDepositPricesGet(new int[] { orderVM.ModelID, orderVM.SelectedPgmBdlPkgCompID, orderVM.ContractID, 0 }).SingleOrDefault();
								if (varAdvDeposit != null)
								{
									var Malaytotalprice = varAdvDeposit.malyDevAdv + varAdvDeposit.malayPlanAdv + varAdvDeposit.malayDevDeposit + varAdvDeposit.malyPlanDeposit;
									var Othertotalprice = varAdvDeposit.othDevAdv + varAdvDeposit.othPlanAdv + varAdvDeposit.othDevDeposit + varAdvDeposit.othPlanDeposit;
									orderVM.RaceID = personalDetailsVM.Customer.NationalityID;
									orderVM.MalayAdvDevPrice = Malaytotalprice;
									orderVM.OthersAdvDevPrice = Othertotalprice;
									Session["RegMobileReg_MalAdvDeposit"] = orderVM.MalayAdvDevPrice;
									Session["RegMobileReg_OtherAdvDeposit"] = orderVM.OthersAdvDevPrice;

									/*Added by chetan for split adv & deposit*/
									orderVM.MalyDevAdv = varAdvDeposit.malyDevAdv;
									orderVM.MalayPlanAdv = varAdvDeposit.malayPlanAdv;
									orderVM.MalayDevDeposit = varAdvDeposit.malayDevDeposit;
									orderVM.MalyPlanDeposit = varAdvDeposit.malyPlanDeposit;
									orderVM.OthDevAdv = varAdvDeposit.othDevAdv;
									orderVM.OthPlanAdv = varAdvDeposit.othPlanAdv;
									orderVM.OthDevDeposit = varAdvDeposit.othDevDeposit;
									orderVM.OthPlanDeposit = varAdvDeposit.othPlanDeposit;

									Session["RegMobileReg_MalyDevAdv"] = orderVM.MalyDevAdv;
									Session["RegMobileReg_MalayPlanAdv"] = orderVM.MalayPlanAdv;
									Session["RegMobileReg_MalayDevDeposit"] = orderVM.MalayDevDeposit;
									Session["RegMobileReg_MalyPlanDeposit"] = orderVM.MalyPlanDeposit;
									Session["RegMobileReg_OthDevAdv"] = orderVM.OthDevAdv;
									Session["RegMobileReg_OthPlanAdv"] = orderVM.OthPlanAdv;
									Session["RegMobileReg_OthDevDeposit"] = orderVM.OthDevDeposit;
									Session["RegMobileReg_OthPlanDeposit"] = orderVM.OthPlanDeposit;
									/*up to here*/

									if (personalDetailsVM.Customer.NationalityID != 0)
									{
										if (personalDetailsVM.Customer.NationalityID == 1)
										{
											/*Added by chetan for split adv & deposit*/
											Session["RegMobileReg_MalyDevAdv"] = orderVM.MalyDevAdv;
											Session["RegMobileReg_MalayPlanAdv"] = orderVM.MalayPlanAdv;
											Session["RegMobileReg_MalayDevDeposit"] = orderVM.MalayDevDeposit;
											Session["RegMobileReg_MalyPlanDeposit"] = orderVM.MalyPlanDeposit;
											/*up to here*/
											Session["RegMobileReg_MalAdvDeposit"] = orderVM.MalayAdvDevPrice;
											orderVM.SumPrice = (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MalayAdvDevPrice + orderVM.MonthlySubscription;
										}
										else
										{
											/*Added by chetan for split adv & deposit*/
											Session["RegMobileReg_OthDevAdv"] = orderVM.OthDevAdv;
											Session["RegMobileReg_OthPlanAdv"] = orderVM.OthPlanAdv;
											Session["RegMobileReg_OthDevDeposit"] = orderVM.OthDevDeposit;
											Session["RegMobileReg_OthPlanDeposit"] = orderVM.OthPlanDeposit;
											/*up to here*/
											Session["RegMobileReg_OtherAdvDeposit"] = orderVM.OthersAdvDevPrice;
											orderVM.SumPrice = orderVM.OthersAdvDevPrice + (orderVM.ItemPrice != 0 ? orderVM.ItemPrice : orderVM.ModelPrice) + orderVM.MonthlySubscription;
										}
									}
								}
							}
						}

					}
				}
				// END Deepika


				// Device Details
				if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan || Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DeviceOnly)
				{
					if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt() != orderVM.SelectedModelImageID)
					{
						orderVM.SelectedModelImageID = Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt();

						using (var proxy = new CatalogServiceProxy())
						{
							modelImage = proxy.ModelImageGet(orderVM.SelectedModelImageID);
						}

						Session["RegMobileReg_SelectedModelID"] = modelImage.ModelID;

						var model = new Model();
						using (var proxy = new CatalogServiceProxy())
						{
							model = proxy.ModelGet(modelImage.ModelID);
						}

						orderVM.ModelImageUrl = modelImage.ImagePath;
						orderVM.ModelID = model.ID;
						orderVM.ModelName = model.Name;
						orderVM.ColourName = modelImage.ColourID.HasValue ? Util.GetNameByID(RefType.Colour, modelImage.ColourID.Value) : "";
						orderVM.BrandID = model.BrandID;
						orderVM.BrandName = Util.GetNameByID(RefType.Brand, model.BrandID);

						if (personalDetailsVM.RegID > 0)
						{
							orderVM.TotalPrice = Session["RegMobileReg_MainDevicePrice"].ToDecimal();
							orderVM.ModelPrice = Session["RegMobileReg_MainDevicePrice"].ToDecimal();
							orderVM.ItemPrice = Session["RegMobileReg_MainDevicePrice"].ToDecimal();
						}
						else
						{
							orderVM.DevicePriceType = DevicePriceType.RetailPrice;
							orderVM.TotalPrice -= orderVM.ModelPrice;
							orderVM.TotalPrice += model.RetailPrice.ToInt();
							orderVM.ModelPrice = model.RetailPrice.ToDecimal();

							Session["RegMobileReg_MainDevicePrice"] = orderVM.ModelPrice;
						}
					}
				}
			}
			else //Sub Line
			{

				if (!ReferenceEquals(MNPPrimaryPlanStorage.MNPSupplinePlanStorageList, null))
				{
					int SeqNo = 0;
					List<SublineVM> SubVMs = new List<SublineVM>();
					if (orderVM.Sublines.Count > 0)
					{
						foreach (var sup in orderVM.Sublines)
						{
							SubVMs.Add(sup);
						}
					}

					foreach (MNPSupplinePlanStorage _suppLineClass in MNPPrimaryPlanStorage.MNPSupplinePlanStorageList)
					{
						if (ReferenceEquals(_suppLineClass.SelectedComponents, null) || string.IsNullOrEmpty(_suppLineClass.SelectedComponents) || _suppLineClass.RegMobileReg_MSISDN != ProcessingMsisdn)
							continue;
						SeqNo++;
						var vasNames = new List<string>();
						var selectedCompIDs1 = new List<int>();
						var packagesub = new Package();
						var bundlesub = new Bundle();
						List<string> strSelectedCompIDs = new List<string>();
						strSelectedCompIDs = _suppLineClass.SelectedComponents.ToString2().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

						foreach (string _compId in strSelectedCompIDs)
						{
							selectedCompIDs1.Add(_compId.ToInt());
						}
						using (var proxy = new CatalogServiceProxy())
						{
							var bundlePackage = MasterDataCache.Instance.FilterComponents(new int[] 
								{   
								(int)_suppLineClass.RegMobileReg_PkgPgmBdlPkgCompID
								}).SingleOrDefault();

							packagesub = proxy.PackageGet(new int[] 
								{ 
									bundlePackage.ChildID 
								}).SingleOrDefault();

							bundlesub = proxy.BundleGet(new int[] 
								{ 
									bundlePackage.ParentID 
								}).SingleOrDefault();

							vasNames = proxy.ComponentGet(
								 MasterDataCache.Instance.FilterComponents
										(selectedCompIDs1).Select(a => a.ChildID)).Where(c => c.RestricttoKenan == false).Select(a => a.Name).ToList();

						}

						SubVMs.Add(new SublineVM()
						{

							SequenceNo = SeqNo,

							PkgPgmBdlPkgCompID = (int)_suppLineClass.RegMobileReg_PkgPgmBdlPkgCompID,


							SelectedMobileNos = _suppLineClass.RegMobileSub_MobileNo,
							SelectedVasIDs = selectedCompIDs1,
							SelectedVasNames = vasNames,
							BundleName = bundlesub.Name,
							PackageName = packagesub.Name
						});
					}
					//supline, 
					if (SubVMs.Count > 0)
						orderVM.Sublines = SubVMs;
				}
			}
			int sessionPkgPgmBdlPkgCompID;
			///MNP MULTIPORT RELATED CHANGES
			if (IsMNPWithMultpleLines)
			{
				//Package Details 
				sessionPkgPgmBdlPkgCompID = fromSubline ? MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID.ToInt() : MNPSupplinePlanStorage.RegMobileReg_PkgPgmBdlPkgCompID.ToInt(); //Main or Sub Line
			}
			else
			{
				//Package Details
				sessionPkgPgmBdlPkgCompID = fromSubline ? Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt() : Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt(); //Main or Sub Line
			}
			if (sessionPkgPgmBdlPkgCompID != orderVM.SelectedPgmBdlPkgCompID)
			{
				var bundlePackage = new PgmBdlPckComponent();
				orderVM.SelectedPgmBdlPkgCompID = sessionPkgPgmBdlPkgCompID;

				// Package Details
				if (sessionPkgPgmBdlPkgCompID != 0)
				{
					using (var proxy = new CatalogServiceProxy())
					{
						bundlePackage = MasterDataCache.Instance.FilterComponents(new int[] 
					{ 
						sessionPkgPgmBdlPkgCompID
					}).SingleOrDefault();

						package = proxy.PackageGet(new int[] 
					{ 
						bundlePackage.ChildID 
					}).SingleOrDefault();

						bundle = proxy.BundleGet(new int[] 
					{ 
						bundlePackage.ParentID 
					}).SingleOrDefault();
					}
				}

				orderVM.PlanBundle = bundle.Name;
				orderVM.PlanName = package.Name;

				//Spend Limit
				orderVM.IsSpendLimit = bundlePackage.IsSpendLimit.HasValue ? bundlePackage.IsSpendLimit.Value : false;
				//End Spend Limit

				// Plan Monthly Subscription
				if (orderVM.MonthlySubscription != bundlePackage.Price)
				{
					orderVM.MonthlySubscription = bundlePackage.Price;
				}

				// Mandatory Package Components
				if (sessionPkgPgmBdlPkgCompID > 0)
				{
					orderVM.VasVM.MandatoryVASes = WebHelper.Instance.GetAvailablePackageComponents(orderVM.SelectedPgmBdlPkgCompID, ref ProcessingMsisdn, isMandatory: true, fromSuppline: true).MandatoryVASes;
				}
				else
				{
					orderVM.VasVM = new ValueAddedServicesVM();
				}
			}

			// Advance Payment/ Deposite
			if (orderVM.Deposite != personalDetailsVM.Deposite)
			{
				orderVM.TotalPrice -= orderVM.Deposite;
				orderVM.TotalPrice += personalDetailsVM.Deposite;

				orderVM.Deposite = personalDetailsVM.Deposite;
			}

			// Device Price (cheaper)
			if (IsMNPWithMultpleLines ? (MNPSupplinePlanStorage.RegMobileReg_ContractID != null && orderVM.SelectedPgmBdlPkgCompID.ToInt() != 0 && orderVM.ModelID != 0) : Session["RegMobileReg_ContractID"] != null && orderVM.SelectedPgmBdlPkgCompID.ToInt() != 0 && orderVM.ModelID != 0)
			{
				if (orderVM.ItemPrice != orderVM.ModelPrice)
				{
					var itemPrice = new ItemPrice();
					using (var proxy = new CatalogServiceProxy())
					{
						var itemPriceID = proxy.ItemPriceFind(new ItemPriceFind()
						{
							ItemPrice = new ItemPrice()
							{
								BdlPkgID = orderVM.SelectedPgmBdlPkgCompID.ToInt(),
								PkgCompID = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_ContractID.ToInt() : Session["RegMobileReg_ContractID"].ToInt(),
								ModelID = orderVM.ModelID,
								pkgDataID = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_DataPkgID.ToInt() : Session["RegMobileReg_DataPkgID"].ToInt()
							},
							Active = true
						}).SingleOrDefault();

						if (personalDetailsVM.RegID == 0)
						{
							if (itemPriceID.ToInt() != 0) // item price configured
							{
								itemPrice = proxy.ItemPriceGet(itemPriceID);

								if (orderVM.DevicePriceType == DevicePriceType.RetailPrice)
								{
									orderVM.TotalPrice -= orderVM.ModelPrice;
								}
								else if (orderVM.DevicePriceType == DevicePriceType.ItemPrice)
								{
									orderVM.TotalPrice -= orderVM.ItemPrice;
								}

								if (itemPrice.Price == 0) // RM 0 price for device
								{
									orderVM.DevicePriceType = DevicePriceType.NoPrice;
								}
								else // cheaper price for device
								{
									orderVM.DevicePriceType = DevicePriceType.ItemPrice;
								}

								orderVM.TotalPrice += itemPrice.Price;
								orderVM.ItemPrice = itemPrice.Price.ToInt();

								///MNP MULTIPORT RELATED CHANGES
								if (IsMNPWithMultpleLines)
								{
									MNPSupplinePlanStorage.RegMobileReg_MainDevicePrice = orderVM.ItemPrice.ToDecimal();
								}
								else
								{
									Session["RegMobileReg_MainDevicePrice"] = orderVM.ItemPrice.ToDecimal();
								}
							}
							else // retail price
							{
								if (orderVM.DevicePriceType == DevicePriceType.ItemPrice)
								{
									orderVM.TotalPrice -= orderVM.ItemPrice;

									orderVM.TotalPrice += orderVM.ModelPrice;
									orderVM.DevicePriceType = DevicePriceType.RetailPrice;

									///MNP MULTIPORT RELATED CHANGES
									if (IsMNPWithMultpleLines)
									{
										MNPSupplinePlanStorage.RegMobileReg_MainDevicePrice = orderVM.ModelPrice;
									}
									else
									{
										Session["RegMobileReg_MainDevicePrice"] = orderVM.ModelPrice;
									}
								}
							}
						}
					}
				}
			}
			else //(retail price)
			{
				if (personalDetailsVM.RegID == 0)
				{
					if (orderVM.DevicePriceType != DevicePriceType.RetailPrice)
					{
						orderVM.TotalPrice -= orderVM.ItemPrice;
						orderVM.ItemPrice = 0;
						orderVM.TotalPrice += orderVM.ItemPrice;
					}
				}
			}


			List<string> mobileNos = null;

			///MNP MULTIPORT RELATED CHANGES
			if (IsMNPWithMultpleLines)
			{
				mobileNos = MNPSupplinePlanStorage.RegMobileReg_MobileNo; // Main or Sub Line
			}
			else
			{
				mobileNos = fromSubline ? (List<string>)Session["RegMobileReg_MobileNo"] : (List<string>)Session["RegMobileReg_MobileNo"]; // Main or Sub Line
			}

			orderVM.MobileNumbers = mobileNos == null ? new List<string>() : mobileNos;

			//SecondarymobileNo
			if (Session["RegMobileReg_VirtualMobileNo_Sec"] != null && Session["RegMobileReg_VirtualMobileNo_Sec"] != "")
				orderVM.MobileNumbers = (List<string>)Session["RegMobileReg_VirtualMobileNo_Sec"];

			if (!fromSubline) //main line
			{
				if (IsMNPWithMultpleLines)
					MNPSupplinePlanStorage.RegMobileReg_OrderSummary = orderVM;
				else
					Session["RegMobileReg_OrderSummary"] = orderVM;
			}
			else //sub line
			{
				///MNP MULTIPORT RELATED CHANGES
				if (IsMNPWithMultpleLines)
				{
					MNPSupplinePlanStorage.RegMobileReg_SublineSummary = orderVM;
					//no need of below condition

				}
				else
				{
					Session["RegMobileReg_SublineSummary"] = orderVM;
					orderVM.fromSubline = true;
				}

			}

			// Subline
			List<SublineVM> sublines = null;
			///MNP MULTIPORT RELATED CHANGES
			if (IsMNPWithMultpleLines)
			{
				sublines = (List<SublineVM>)MNPSupplinePlanStorage.RegMobileReg_SublineVM;
				if (sublines != null)
					orderVM.Sublines = (List<SublineVM>)MNPSupplinePlanStorage.RegMobileReg_SublineVM;
			}
			else
			{
				sublines = (List<SublineVM>)Session["RegMobileReg_SublineVM"];
				if (sublines != null)
					orderVM.Sublines = (List<SublineVM>)Session["RegMobileReg_SublineVM"];
			}

			orderVM.MonthlySubscription = 0;
			decimal vasPrice = 0;


			var selectedCompIDs = new List<int>();

			List<AvailableVAS> lstvas = new List<AvailableVAS>();
			///MNP MULTIPORT RELATED CHANGES
			if (IsMNPWithMultpleLines ? !ReferenceEquals(MNPSupplinePlanStorage.RegMobileReg_VasIDs, null) : !ReferenceEquals(Session["RegMobileReg_VasIDs"], null))
			{
				lstvas.AddRange(orderVM.VasVM.MandatoryVASes);

				for (int i = 0; i < orderVM.Sublines.Count(); i++)
				{
					for (int j = 0; j < orderVM.Sublines[i].SelectedVasNames.Count; j++)
					{
						lstvas.Add(new AvailableVAS() { PgmBdlPkgCompID = orderVM.Sublines[i].SelectedVasIDs[j], ComponentName = orderVM.Sublines[i].SelectedVasNames[j].ToString2() });
					}
				}

				for (int i = 0; i < lstvas.Count(); i++)
				{
					selectedCompIDs.Add(lstvas[i].PgmBdlPkgCompID);
				}
			}

			string selectedVasIDs;

			///MNP MULTIPORT RELATED CHANGES
			if (IsMNPWithMultpleLines)
			{
				MNPSupplinePlanStorage.RegMobileReg_VasNames = lstvas;
				selectedVasIDs = MNPSupplinePlanStorage.RegMobileReg_VasIDs.ToString2(); //Main or Sub Line
			}
			else
			{
				Session["RegMobileReg_VasNames"] = lstvas;
				selectedVasIDs = Session["RegMobileReg_VasIDs"].ToString2(); //Main or Sub Line
			}
			//ranjeeth 26thJuly2013


			// Mandatory VASes
			if (orderVM.VasVM.MandatoryVASes.Count() > 0)
			{
				selectedCompIDs.AddRange(orderVM.VasVM.MandatoryVASes.Select(a => a.PgmBdlPkgCompID).ToList());
			}

			if (orderVM.VasVM.SelectedVasIDs != selectedVasIDs || orderVM.VasVM.MandatoryVASes.Count() > 0)
			{

				orderVM.ContractID = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_ContractID.ToInt() : Session["RegMobileReg_ContractID"].ToInt();
				orderVM.DataPlanID = IsMNPWithMultpleLines ? MNPSupplinePlanStorage.RegMobileReg_DataplanID : Session[SessionKey.RegMobileReg_DataplanID.ToString()].ToInt();
				orderVM.VasVM.SelectedVasIDs = selectedVasIDs;


				var vasIDs = orderVM.VasVM.SelectedVasIDs.Split(',').ToList();
				foreach (var id in vasIDs)
				{
					if (!string.IsNullOrEmpty(id))
					{
						selectedCompIDs.Add(id.ToInt());
					}
				}
				List<int> depCompIds = new List<int>();
				foreach (var id in vasIDs)
				{
					if (!string.IsNullOrEmpty(id))
					{
						depCompIds = GetDepenedencyComponentsSupp(id.ToString());

					}
				}
				foreach (var id in depCompIds)
				{
					if (id > 0)
					{
						selectedCompIDs.Add(id.ToInt());
					}

				}


				if (selectedCompIDs != null)
				{
					if (selectedCompIDs.Count() > 0)
					{
						using (var proxy = new CatalogServiceProxy())
						{
							var vases = MasterDataCache.Instance.FilterComponents
							   (
								   selectedCompIDs
							   ).ToList();

							components = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();

							//Added by RAVI on 01 Nov 2013 FOR CONNECT/DISCONNECT CR

							components = components.Where(a => a.RestricttoKenan == false).ToList();

							//Added by RAVI on o1 nov 2013 FOR CONNECT/DISCONNECT CR ENDS HERE

							var query = from selectedVases in vases
										join comps in components on
											selectedVases.ChildID equals comps.ID
										select new { name = comps.Name + " (RM" + selectedVases.Price + ")" };

							orderVM.VasVM.VASesName = query.Select(a => a.name).ToList();

							/*Added by chetan for vasplan price adding*/

							if ((!string.IsNullOrEmpty(selectedVasIDs)) || (orderVM.VasVM.SelectedVasIDs != selectedVasIDs) || (orderVM.VasVM.VASesName.Count > 0))
							{
								var query1 = from selectedVases in vases
											 join comps in components on
												selectedVases.ChildID equals comps.ID
											 select new { selectedVases.Price };
								if (query1 != null)
								{
									orderVM.MonthlySubscription2 = query1.Select(b => b.Price).ToList();
									for (int i = 0; i < orderVM.MonthlySubscription2.Count; i++)
									{
										vasPrice += orderVM.MonthlySubscription2[i];
									}
								}
							}
						}
					}
				}

				orderVM.MonthlySubscription += vasPrice;

				/////MNP MULTIPORT RELATED CHANGES
				if (IsMNPWithMultpleLines)
				{

					MNPSupplinePlanStorage.RegMobileReg_VasNames = orderVM.VasVM.AvailableVASes;
				}
				else
				{
					Session["RegMobileReg_VasNames"] = orderVM.VasVM.VASesName;
				}
			}



			return orderVM;
		}
		private bool ValidateCardDetails(Customer customer)
		{
			string error;
			if (!WebHelper.Instance.IsCardDetailsEntered(customer, out error))
			{
				if (!string.IsNullOrEmpty(error))
				{
					ModelState.AddModelError("CreditCardError", error);
				}
				else
				{
					ModelState.AddModelError("CreditCardError", "Please enter all Card details.");
				}
				return false;
			}
			return true;
		}

		private KenanNewSuppLineResponse FulfillKenanAccount(int regID)
		{
			//var result = "";
			var reg = new DAL.Models.Registration();
			var cust = new Customer();
			var billAddr = new Address();
			var response = new KenanNewSuppLineResponse();

			MNPOrderCreationResponse rs = new MNPOrderCreationResponse();

			try
			{
				var extid = new List<KenanTypeExternalId>();
				var extidnew = new List<KenanTypeExternalId>();
				extidnew.Add(new KenanTypeExternalId() { ExternalId = string.Empty, ExternalIdType = string.Empty });

				using (var proxy = new RegistrationServiceProxy())
				{
					reg = proxy.RegistrationGet(regID);
					cust = proxy.CustomerGet(new int[] { reg.CustomerID }).SingleOrDefault();

					var billAddrID = proxy.RegAddressFind(new AddressFind()
					{
						Address = new Address()
						{
							RegID = reg.ID,
							AddressTypeID = Util.GetIDByCode(RefType.AddressType, Properties.Settings.Default.AddressType_Billing)
						}
					}).SingleOrDefault();

					billAddr = proxy.RegAddressGet(new int[] { billAddrID }).SingleOrDefault();
				}
				extid.Add(new KenanTypeExternalId() { ExternalId = (reg.externalId == null ? "" : reg.externalId), ExternalIdType = (reg.AccExternalID == null ? "" : reg.AccExternalID) });

				var request = new KenanNewSuppLineRequest()
				{

					OrderId = regID.ToString(),
					suppLine = WebHelper.Instance.ConstructSuppPackages(reg.MSISDN1),
					FxAcctNo = reg.fxAcctNo,
					ExternalId = (extid.ToList().ToArray() == null) ? extidnew.ToList().ToArray() : extid.ToList().ToArray(),
					prinLine = WebHelper.Instance.ConstructPrinPackages(reg.externalId)
				};

				var secResponse = new KenanTypeAdditionLineRegistrationResponse();

				string preOrderID = !ReferenceEquals(ConfigurationManager.AppSettings["orderProcessReq_fromMISM"], null) ? ConfigurationManager.AppSettings["orderProcessReq_fromMISM"] : "ISELLMISM";
				var secRequest = new KenanTypeAdditionLineRegistration()
				{
					OrderId = preOrderID + regID,
					FxAcctNo = (reg.fxAcctNo == null) ? "" : reg.fxAcctNo,
					ExternalId = (extid.ToList().ToArray() == null) ? extidnew.ToList().ToArray() : extid.ToList().ToArray(),
				};

				using (var proxy = new KenanServiceProxy())
				{
					if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && Convert.ToBoolean(Session[SessionKey.FromMNP.ToString()]))
					{
						rs = proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
						{
							orderId = regID.ToString(),
							externalId = reg.MSISDN1,
							extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
							Customer = WebHelper.Instance.ConstructKenanCustomerMNP(cust, billAddr),
							Account = WebHelper.Instance.ConstructKenanAccountMNP(cust),
							UserName = reg.SalesPerson,
						});
					}
					else
					{
						if (reg.RegTypeID == (int)MobileRegType.MNPSuppPlan)
						{
							rs = proxy.MNPcreateCenterOrderMNP(new OrderCreationRequestMNP()
							{
								orderId = regID.ToString(),
								externalId = reg.MSISDN1,
								extenalIdType = Util.GetIDByCode(RefType.ExternalIDType, Properties.Settings.Default.ExternalIDType_MSISDN).ToString(),
								Customer = WebHelper.Instance.ConstructKenanCustomerMNP(cust, billAddr),
								Account = WebHelper.Instance.ConstructKenanAccountMNP(cust),
								UserName = reg.SalesPerson,
							});
						}
						else if (reg.RegTypeID == (int)MobileRegType.SecPlan)
						{
							secResponse = proxy.KenanAdditionLineRegistrationMISMSecondary(secRequest);
							if (!ReferenceEquals(secResponse, null))
							{
								response.Message = secResponse.Message;
								response.Success = secResponse.Success;
								response.Code = secResponse.Code;
							}
							return response;
						}
						else
						{	
							response = proxy.KenanAddNewSuppLine(request);
							
							return response;
						}


					}
					/*Added by narayanareddy*/
					if (reg.RegTypeID == (int)MobileRegType.MNPSuppPlan)
					{
						if (!ReferenceEquals(rs, null))
						{
							response.Message = rs.Message;
							response.Success = rs.Success;
							response.Code = rs.Code;
						}
					}
					/*End*/
				}
			}
			catch (Exception ex)
			{
				WebHelper.Instance.LogExceptions(this.GetType(), ex);
			}

			return response;
		}

		private DAL.Models.Registration ConstructRegistration(string queueNo, string remarks, string signatureSVG = "", string custPhoto = "", string altCustPhoto = "", string photo = "", string SIMModelSelected = "")
		{
			MNPPrimaryPlanStorage MNPPrimaryPlanStorage = null;
			MNPPrimaryPlanStorage = (MNPPrimaryPlanStorage)Session["mNPPrimaryPlanStorage"];

			var regTypeID = 0;
			DAL.Models.Registration registration = new DAL.Models.Registration();

            var isBlacklisted = true;
            bool isDirectMISMOrder = false;

			PersonalDetailsVM personalDetailsVM = null;
			if (Session["RegMobileReg_PersonalDetailsVM"] != null)
			{
				personalDetailsVM = (PersonalDetailsVM)Session["RegMobileReg_PersonalDetailsVM"];
			}

			if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null))
			{
				regTypeID = Util.GetRegTypeID(REGTYPE.MNPSuppPlan);
			}
			else
			{
				if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
					regTypeID = Util.GetRegTypeID(REGTYPE.DevicePlan);
				else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.PlanOnly)
					regTypeID = 8;
				else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DeviceOnly)
					regTypeID = Util.GetRegTypeID(REGTYPE.DeviceOnly);
				else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.SuppPlan)
					regTypeID = Util.GetRegTypeID(REGTYPE.SuppPlan);
				else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.SecPlan)
					regTypeID = Util.GetRegTypeID(REGTYPE.SecPlan);
			}


			registration = new DAL.Models.Registration();

			registration.CenterOrgID = Util.SessionAccess.User.OrgID;
			registration.RegTypeID = regTypeID;
			registration.QueueNo = queueNo;
			//Added by Patanjali to support remarks on 30-03-2013
			registration.Remarks = remarks;
			//Added by Patanjali to support remarks on 30-03-2013 ends her
			registration.SignatureSVG = signatureSVG;
			//CustomerPhoto = Uri.EscapeDataString(custPhoto);
			registration.CustomerPhoto = custPhoto;
			registration.AltCustomerPhoto = altCustPhoto;
			registration.Photo = photo;
			registration.AdditionalCharges = !ReferenceEquals(personalDetailsVM.Deposite, 0) ? personalDetailsVM.Deposite : 0;
			registration.InternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;
			registration.ExternalBlacklisted = Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] != null ? (((bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()] == null) ? false : (bool)Session[SessionKey.RegMobileReg_IsBlacklisted.ToString()]) : true;

            // w.loon - Is Justification/Supervisor Approval Required
            string ListOfRequiredBreValidation = WebHelper.Instance.ListOfRequiredBreValidation()["ListOfRequiredBreValidation"];

            registration.AgeCheckStatus = ListOfRequiredBreValidation.Contains("AgeCheck") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AgeCheckStatusCode.ToString()]) : "AF"
                                                    : "AN";
            registration.DDMFCheckStatus = ListOfRequiredBreValidation.Contains("DDMF Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_DDMFCheckStatusCode.ToString()]) : "DF"
                                                    : "DN";
            registration.AddressCheckStatus = ListOfRequiredBreValidation.Contains("Address Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_AddressCheckStatusCode.ToString()]) : "ADF"
                                                    : "ADN";
            registration.OutStandingCheckStatus = ListOfRequiredBreValidation.Contains("Outstanding Credit Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsOutstandingCreditCheckStatusCode.ToString()]) : "OF"
                                                    : "ON";
            registration.TotalLineCheckStatus = ListOfRequiredBreValidation.Contains("Total Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_TotalLineCheckStatusCode.ToString()]) : "TLF"
                                                    : "TLN";
            registration.PrincipleLineCheckStatus = ListOfRequiredBreValidation.Contains("Principle Line Check") ?
                                                    !ReferenceEquals(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_PrincipalLineCheckStatusCode.ToString()]) : "PLF"
                                                    : "PLN";
            registration.BiometricVerify = ListOfRequiredBreValidation.Contains("Biometric") ?
                                                    (!ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? Session[SessionKey.RegMobileReg_BiometricVerify.ToString()].ToBool() : false)
                                                    : false;

			if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
			{
				registration.SalesPerson = Request.Cookies["CookieUser"].Value;
			}
			//added by Nreddy for MyKad
            registration.IsVerified = !ReferenceEquals(Session[SessionKey.RegMobileReg_BiometricVerify.ToString()], null) ? true : false;
			registration.ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
			registration.RFSalesDT = DateTime.Now;


			if (!ReferenceEquals(Session[SessionKey.FromMNP.ToString()], null) && !ReferenceEquals(Session[SessionKey.RegMobileReg_MSISDN.ToString()], null))
			{
				registration.MSISDN1 = Convert.ToString(Session[SessionKey.RegMobileReg_MSISDN.ToString()]);
			}
			else
			{
				registration.MSISDN1 = Session["RegMobileReg_MobileNo"] == null ? null : ((List<string>)Session["RegMobileReg_MobileNo"])[0].ToString2();
			}
			if (Session["RegMobileReg_VirtualMobileNo_Sec"] != null && Session["RegMobileReg_VirtualMobileNo_Sec"] != "")
			{
				if (((List<string>)Session["RegMobileReg_VirtualMobileNo_Sec"]) != null)
				{
					if (((List<string>)Session["RegMobileReg_VirtualMobileNo_Sec"])[0].Trim().Length > 0)
					{
						registration.MSISDN1 = ((List<string>)Session["RegMobileReg_VirtualMobileNo_Sec"])[0];
					}
					else
					{

						registration.MSISDN1 = "";
					}
				}
			}

			if ( !string.IsNullOrEmpty(Session[SessionKey.DirectMISMOrder.ToString2()].ToString2()))
			{
				isDirectMISMOrder = Session[SessionKey.DirectMISMOrder.ToString2()].ToBool();
                if (isDirectMISMOrder)
                {
                    registration.MSISDN1 = WebHelper.Instance.retriveVirtualNumbers(true);
                    if (!string.IsNullOrEmpty(registration.MSISDN1))
                    {
                        using (var proxyobj = new RegistrationServiceProxy())
                        {
                            int res = proxyobj.Insertusedmobilenos(personalDetailsVM.RegID, Convert.ToInt64(registration.MSISDN1), 1);
                        }
                    }
                }
			}

			registration.CreateDT = DateTime.Now;
			registration.LastAccessID = Util.SessionAccess.UserName;
			registration.Whitelist_Status = Session[SessionKey.Mocandwhitelistcuststatus.ToString()] == null ? null : Session[SessionKey.Mocandwhitelistcuststatus.ToString()].ToString2();
            registration.MOC_Status = Session[SessionKey.MocDefault.ToString()] == null ? null : Session[SessionKey.MocDefault.ToString()].ToString2(); registration.ContractCheckStatus = ListOfRequiredBreValidation.Contains("Contract Check") ?
                                     !ReferenceEquals(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_ContractCheckStatusCode.ToString()]) : "CF"
                                     : "CN";
            registration.UserType = "E";
            bool IsWriteOff = false;
            string acc = string.Empty;
            WebHelper.Instance.CheckWriteOffStatus(out IsWriteOff, out acc);
            registration.WriteOffCheckStatus = ListOfRequiredBreValidation.Contains("Writeoff Check") ?
                                                    !IsWriteOff ? "WOS" : "WOF"
                                                    : "WON";

			registration.fxAcctNo = Session["FxAccNo"] == null ? null : Session["FxAccNo"].ToString2();
			registration.externalId = Session["ExternalID"] == null ? null : Session["ExternalID"].ToString2();
			registration.AccExternalID = Session["AccExternalID"] == null ? null : Session["AccExternalID"].ToString2();
			registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalayPlanAdv"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayPlanAdv"].ToString2()) : 0) : ((Session["RegMobileReg_OthPlanAdv"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthPlanAdv"].ToString2()) : 0);
			registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalyDevAdv"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyDevAdv"].ToString2()) : 0) : ((Session["RegMobileReg_OthDevAdv"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthDevAdv"].ToString2()) : 0);
			registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalyPlanDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyPlanDeposit"].ToString2()) : 0) : ((Session["RegMobileReg_OthPlanDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthPlanDeposit"].ToString2()) : 0);
			registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalayDevDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayDevDeposit"].ToString2()) : 0) : ((Session["RegMobileReg_OthDevDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthDevDeposit"].ToString2()) : 0);
			registration.OutstandingAcs = Session[SessionKey.FailedAcctIds.ToString()] == null ? string.Empty : Session[SessionKey.FailedAcctIds.ToString()].ToString2();
			registration.PrePortInCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()]) : "MPPF";
			registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()]) : "MSIF";
			registration.MOC_Status = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString2() : "No";
			registration.Whitelist_Status = !ReferenceEquals(Session[SessionKey.MocandwhitelistSstatus.ToString()], null) ? Session[SessionKey.MocandwhitelistSstatus.ToString()].ToString2() : "NO";
			registration.KenanAccountNo = !ReferenceEquals(Session["KenanACNumber"], null) ? Session["KenanACNumber"].ToString2() : "";
			registration.OrganisationId = Util.SessionAccess.User.Org.OrganisationId;
			registration.Trn_Type = ConfigurationManager.AppSettings["Trn_Type"];
			if (Session["OfferId"] != null)
				registration.OfferID = Convert.ToDecimal(Session["OfferId"]);
			else
				registration.OfferID = 0;
			registration.IMPOSStatus = 0;
			registration.SimModelId = 0;
			registration.upfrontPayment = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_UpfrontPayment"] != null) ? Convert.ToDecimal(Session["RegMobileReg_UpfrontPayment"].ToString2()) : 0) : ((Session["RegMobileReg_UpfrontPayment"] != null) ? Convert.ToDecimal(Session["RegMobileReg_UpfrontPayment"].ToString2()) : 0);
			//Added by VLT on 1st May 2013 to add K2Type in Registration
			using (var proxy = new CatalogServiceProxy())
			{
				string[] contractId = null;
				List<int> contractsList = new List<int>();
				List<Component> componentsList = new List<Component>();

				if (Session["RegMobileReg_ContractID"] != null)
				{


					contractId = IsMNPWithMultpleLines ? ((null != MNPPrimaryPlanStorage.RegMobileReg_ContractID) ? MNPPrimaryPlanStorage.RegMobileReg_ContractID.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries) : Session["RegMobileReg_ContractID"].ToString2().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries) ): Session["RegMobileReg_ContractID"].ToString2().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
					foreach (string crtId in contractId)
					{
						contractsList.Add(Convert.ToInt32(crtId));
					}

					var vases = MasterDataCache.Instance.FilterComponents
					(
					(IEnumerable<int>)contractsList
					).ToList();

					componentsList = proxy.ComponentGet(vases.Select(a => a.ChildID)).ToList();
					if (componentsList != null && componentsList.Count > 0)
					{
						// ranjeeth for islandeviceCR
						List<string> selectedContract = new List<string>();

						List<string> lstComponentNames = new List<string>();
						lstComponentNames = componentsList.Where(vas => vas.ID == vases[0].ChildID).Select(vas => vas.Name.ToLower()).ToList();
						if (lstComponentNames != null && lstComponentNames.Count > 0)
							selectedContract = lstComponentNames;


						if (ConfigurationManager.AppSettings["K2ContractName"] != null)
						{
							foreach (var s in selectedContract)
							{
								if (s.Contains(ConfigurationManager.AppSettings["K2ContractName"].ToString2().ToLower()) || s.Contains("iphone"))
								{
									registration.K2Type = true;
									break;
								}
								else
								{
									registration.K2Type = false;
								}
							}
						}
					}

				}
			}

			//Added by VLT on 1st May 2013 to add K2Type in Registration

			//Code Added by VLT Check for International Roaming
			if (Session["RegMobileReg_VasNames"] != null)
			{
				var vasNames = (List<string>)Session["RegMobileReg_VasNames"];
				string strVasArray = string.Join(",", vasNames.ToArray());
				if (strVasArray.Contains(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["InternationalRoaming"])) == true)
				{
					registration.InternationalRoaming = true;
				}
			}

			//End of International Roaming Check

            //Anthony - [#2439] MISM charges for Plan Advance
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            var isMISM = dropObj.msimIndex;
            
            if (isMISM > 0)
            {
                registration.DeviceAdvance = System.Configuration.ConfigurationManager.AppSettings["MISMDeviceAdvance"].ToInt();
                registration.DeviceDeposit = System.Configuration.ConfigurationManager.AppSettings["MISMDeviceDeposit"].ToInt();
                registration.PlanAdvance = System.Configuration.ConfigurationManager.AppSettings["MISMPlanAdvance"].ToInt();//Anthony - [#2439] need to be configured in web.config
                registration.PlanDeposit = System.Configuration.ConfigurationManager.AppSettings["MISMPlanDeposit"].ToInt();

            }
            else
            {
                if (dropObj != null && dropObj.suppFlow != null && dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary != null)
                {
                    registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.MalayPlanAdv != null) ? Convert.ToDecimal(dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.MalayPlanAdv) : 0) : ((dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.OthPlanAdv != null) ? Convert.ToDecimal(dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.OthPlanAdv) : 0);
                    registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.MalyDevAdv != null) ? Convert.ToDecimal(dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.MalyDevAdv) : 0) : ((dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.OthDevAdv != null) ? Convert.ToDecimal(dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.OthDevAdv) : 0);
                    registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.MalyPlanDeposit != null) ? Convert.ToDecimal(dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.MalyPlanDeposit) : 0) : ((dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.OthPlanDeposit != null) ? Convert.ToDecimal(dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.OthPlanDeposit) : 0);
                    registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.MalayDevDeposit != null) ? Convert.ToDecimal(dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.MalayDevDeposit) : 0) : ((dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.OthDevDeposit != null) ? Convert.ToDecimal(dropObj.suppFlow[dropObj.suppIndex - 1].orderSummary.OthDevDeposit) : 0);
                }
                else
                {
                    registration.PlanAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalayPlanAdv"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayPlanAdv"].ToString2()) : 0) : ((Session["RegMobileReg_OthPlanAdv"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthPlanAdv"].ToString2()) : 0);
                    registration.DeviceAdvance = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalyDevAdv"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyDevAdv"].ToString2()) : 0) : ((Session["RegMobileReg_OthDevAdv"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthDevAdv"].ToString2()) : 0);
                    registration.PlanDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalyPlanDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalyPlanDeposit"].ToString2()) : 0) : ((Session["RegMobileReg_OthPlanDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthPlanDeposit"].ToString2()) : 0);
                    registration.DeviceDeposit = (personalDetailsVM.Customer.NationalityID == 1) ? ((Session["RegMobileReg_MalayDevDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_MalayDevDeposit"].ToString2()) : 0) : ((Session["RegMobileReg_OthDevDeposit"] != null) ? Convert.ToDecimal(Session["RegMobileReg_OthDevDeposit"].ToString2()) : 0);
                }

            } 
            registration.upfrontPayment = !ReferenceEquals(Session["RegMobileReg_UpfrontPayment"], null) ? Convert.ToDecimal(Session["RegMobileReg_UpfrontPayment"]) : 0;
            registration.OutstandingAcs = Session[SessionKey.FailedAcctIds.ToString()] == null ? string.Empty : Session[SessionKey.FailedAcctIds.ToString()].ToString2();
            registration.PrePortInCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsPortInStatusCode.ToString()]) : "MPPF";
            registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()], null) ? Convert.ToString(Session[SessionKey.RegMobileReg_IsMNPServiceIdCheckStatusCode.ToString()]) : "MSIF";
            registration.ArticleID = !ReferenceEquals(Session[SessionKey.ArticleId.ToString()], null) ? Convert.ToString(Session[SessionKey.ArticleId.ToString()]) : string.Empty;
            if (Session["RegMobileReg_UOMCode"] != null)
			{
				registration.UOMCode = Session["RegMobileReg_UOMCode"].ToString2();

			}
			else
			{
				registration.UOMCode = string.Empty;
			}
			// For plan setting UOM code and offerid to generic values -- By Patanjali on 29-04-2013
			if (registration.RegTypeID != 2)
			{
				registration.OfferID = 0;
				registration.UOMCode = "EA";
			}
			//Added by Chetan for saving DonorID & Master PrePortReqId
			registration.PrePortReqId = ReferenceEquals(Session[SessionKey.Master_PrePortReqId.ToString()], null) ? "" : Session[SessionKey.Master_PrePortReqId.ToString()].ToString2();
			registration.DonorId = ReferenceEquals(Session[SessionKey.DonorId.ToString()], null) ? "" : Session[SessionKey.DonorId.ToString()].ToString2();
			registration.CRPType = Session["IsDeviceRequired"].ToString2() == "Yes" ? "DSP" : "";
			Session[SessionKey.Master_PrePortReqId.ToString()] = null;
			Session[SessionKey.DonorId.ToString()] = null;
			//End Here
			if (!string.IsNullOrEmpty(SIMModelSelected.Split(',')[0]) && SIMModelSelected.Split(',')[0].ToLower() != "false")
			{
				registration.SimModelId = SIMModelSelected.Split(',')[0].ToInt();
			}
			registration.SIMSerial = Session["PrimarySim"].ToString2();
			registration.IMEINumber = Session["IMEIForDevice"].ToString2();

            //GTM e-Billing CR - Ricky - 2014.09.25
            //Note: No need to trigger to EBPS for MSIM order
            if (!isDirectMISMOrder)
            {
                registration.billDeliveryViaEmail = personalDetailsVM.EmailBillSubscription == "Y" ? true : false;
                registration.billDeliveryEmailAddress = personalDetailsVM.EmailBillSubscription == "Y" ? personalDetailsVM.EmailAddress : "";
                registration.billDeliverySmsNotif = personalDetailsVM.SmsAlertFlag == "Y" ? true : false;
                registration.billDeliverySmsNo = personalDetailsVM.SmsAlertFlag == "Y" ? personalDetailsVM.SmsNo : "";
                registration.billDeliverySubmissionStatus = "Pending";
            }

			return registration;
		}
		private PackageVM GetAvailablePackages(bool fromSubline = false)
		{
			var packageVM = new PackageVM();
			var availablePackages = new List<AvailablePackage>();
			var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
			var programBundles = new List<PgmBdlPckComponent>();
			var packages = new List<Package>();
			var bundles = new List<Bundle>();
			var bundleIDs = new List<int>();
			var programs = new List<Program>();
			var filterBdlIDs = new List<int>();
			var bundlePackages = new List<PgmBdlPckComponent>();
			var packageIDs = new List<int>();

			using (var proxy = new CatalogServiceProxy())
			{
				// retrieve bundles that assigned to current user
				var findCriteria = new PgmBdlPckComponentFind()
				{
					PgmBdlPckComponent = new PgmBdlPckComponent()
					{
						LinkType = Properties.Settings.Default.Program_Bundle,
						FilterOrgType = Util.SessionOrgTypeCode
					},
					Active = true
				};

				filterBdlIDs = MasterDataCache.Instance.FilterComponents(findCriteria).Select(a => a.ChildID).ToList();
				bundleIDs = filterBdlIDs;

				// Main Line
				if (!fromSubline)
				{
					if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
					{
						var modelID = proxy.ModelImageGet(Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt()).ModelID;

						var modelGroupIDs = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
						{
							ModelGroupIDs = new List<int>(),
							ModelGroupModel = new ModelGroupModel()
							{
								ModelID = modelID
							},
							Active = true
						})).Select(a => a.ModelGroupID).Distinct().ToList();

						// Bundle - Package
						var pbpcIDs = proxy.ModelGroupGet(modelGroupIDs).Select(a => a.PgmBdlPckComponentID).ToList();

						bundlePackages = MasterDataCache.Instance.FilterComponents(pbpcIDs).Where(a => a.Active == true).ToList();
						bundleIDs = filterBdlIDs.Intersect(bundlePackages.Select(a => a.ParentID)).ToList();
						packageIDs = bundlePackages.Select(a => a.ChildID).ToList();
					}
				}

				findCriteria = new PgmBdlPckComponentFind()
				{
					PgmBdlPckComponent = new PgmBdlPckComponent()
					{
						LinkType = Properties.Settings.Default.Bundle_Package,
						PlanType = fromSubline ? Properties.Settings.Default.SupplimentaryPlan : Properties.Settings.Default.ComplimentaryPlan,
					},
					ParentIDs = bundleIDs,
					ChildIDs = packageIDs,
					Active = true
				};
				//var query = proxy.PgmBdlPckComponentGet(proxy.PgmBdlPckComponentFind(findCriteria));
				var query = MasterDataCache.Instance.FilterComponents(findCriteria);

				// Main Line
				if (!fromSubline)
				{
					if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.SecPlan)
					{
						findCriteria = new PgmBdlPckComponentFind()
						{
							PgmBdlPckComponent = new PgmBdlPckComponent()
							{
								LinkType = Properties.Settings.Default.SecondaryPlan,
								PlanType = Properties.Settings.Default.MandatoryPlan,
							},
							ParentIDs = bundleIDs,
							ChildIDs = packageIDs,
							Active = true
						};
						var querySeco = MasterDataCache.Instance.FilterComponents(findCriteria);
						if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
						{
							pgmBdlPckComponents = query.Where(a => bundleIDs.Contains(a.ParentID)).ToList();
							List<PgmBdlPckComponent> seocndary = querySeco.Where(a => bundleIDs.Contains(a.ParentID)).ToList();
							foreach (PgmBdlPckComponent component in seocndary)
								pgmBdlPckComponents.Add(component);
						}
						else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.PlanOnly)
						{
							pgmBdlPckComponents = query.ToList();
							List<PgmBdlPckComponent> seocndary = querySeco.ToList();
							foreach (PgmBdlPckComponent component in seocndary)
								pgmBdlPckComponents.Add(component);
						}
						else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.SuppPlan)
						{
							pgmBdlPckComponents = query.ToList();
							List<PgmBdlPckComponent> seocndary = querySeco.ToList();
							foreach (PgmBdlPckComponent component in seocndary)
								pgmBdlPckComponents.Add(component);
						}
					}
					else
					{
						if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
						{
							pgmBdlPckComponents = query.Where(a => bundleIDs.Contains(a.ParentID)).ToList();
						}
						else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.PlanOnly)
						{
							pgmBdlPckComponents = query.ToList();
						}
						else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.SuppPlan)
						{
							pgmBdlPckComponents = query.ToList();
						}
					}
				}
				else //Sub Line
				{
					if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.SecPlan)
					{
						findCriteria = new PgmBdlPckComponentFind()
						{
							PgmBdlPckComponent = new PgmBdlPckComponent()
							{
								LinkType = Properties.Settings.Default.SecondaryPlan,
								PlanType = Properties.Settings.Default.MandatoryPlan,
							},
							ParentIDs = bundleIDs,
							ChildIDs = packageIDs,
							Active = true
						};
						var querySeco = MasterDataCache.Instance.FilterComponents(findCriteria);
					}
					if (Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt() != 0)
					{
						var modelID = proxy.BrandArticleImageGet(Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt()).ModelID;

						var modelGroupIDs = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
						{
							ModelGroupIDs = new List<int>(),
							ModelGroupModel = new ModelGroupModel()
							{
								ModelID = modelID
							},
							Active = true
						})).Select(a => a.ModelGroupID).Distinct().ToList();


						// Bundle - Package
						var pbpcIDs = proxy.ModelGroupGet(modelGroupIDs).Select(a => a.PgmBdlPckComponentID).ToList();

						bundlePackages = MasterDataCache.Instance.FilterComponents(pbpcIDs).Where(a => a.Active == true).ToList();
						bundleIDs = filterBdlIDs.Intersect(bundlePackages.Select(a => a.ParentID)).ToList();
						packageIDs = bundlePackages.Select(a => a.ChildID).ToList();

					}
					if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
					{
						pgmBdlPckComponents = query.Where(a => bundleIDs.Contains(a.ParentID)).ToList();
					}
					pgmBdlPckComponents = query.ToList();
				}

				if (pgmBdlPckComponents.Count() > 0)
				{
					packages = proxy.PackageGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();
					bundles = proxy.BundleGet(pgmBdlPckComponents.Select(a => a.ParentID).Distinct()).ToList();
				}

				var pgmBdlIDs = new PgmBdlPckComponentFind()
				{
					PgmBdlPckComponent = new PgmBdlPckComponent()
					{
						LinkType = Properties.Settings.Default.Program_Bundle,
					},
					ChildIDs = pgmBdlPckComponents.Select(a => a.ParentID).ToList()
				};

				programBundles = MasterDataCache.Instance.FilterComponents(pgmBdlIDs).Distinct().ToList();

				var programIDs = programBundles.Select(a => a.ParentID).ToList();

				if (programIDs != null)
				{
					if (programIDs.Count() > 0)
					{
						programs = proxy.ProgramGet(programIDs).ToList();
					}
				}
			}

			foreach (var pbpc in pgmBdlPckComponents)
			{
				availablePackages.Add(new AvailablePackage()
				{
					PgmBdlPkgCompID = pbpc.ID,
					ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
					BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
					PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
					PackageCode = pbpc.Code,
					KenanCode = pbpc.KenanCode,
					//Added by VLT on 04 June 2013 to get SpendLimt
					IsSpendLimit = pbpc.IsSpendLimit.HasValue ? pbpc.IsSpendLimit.Value : false

				});
			}
			if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
			{
				if (Session[SessionKey.ArticleId.ToString()] != null)
				{
					string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString2() : string.Empty;
					List<string> objPackages = new List<string>();
					using (var proxy = new CatalogServiceProxy())
					{
						objPackages = proxy.GetPackagesForArticleId(Session[SessionKey.ArticleId.ToString()].ToString2(), MOCStatus);

					}
					if (objPackages != null && objPackages.Count > 0)
						packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).Where(a => string.Join(",", objPackages).Contains(a.KenanCode)).ToList();

				}
				else
					packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();
			}
			else
			{
				packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();
			}

			// Main Line
			if (!fromSubline)
				packageVM.SelectedPgmBdlPkgCompID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt();
			else
				packageVM.SelectedPgmBdlPkgCompID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt();

			return packageVM;
		}
		private string GetRegActionStep(int stepNo)
		{
			if ((int)MobileRegistrationSteps.Device == stepNo)
			{
				return "SelectDevice";
			}
			else if ((int)MobileRegistrationSteps.DeviceCatalog == stepNo)
			{
				return "SelectDeviceCatalog";
			}
			else if ((int)MobileRegistrationSteps.Plan == stepNo)
			{
				return "SuppLinePlanNew";
			}
			else if ((int)MobileRegistrationSteps.Vas == stepNo)
			{
				return "SuppLineVASNew";
			}
			else if ((int)MobileRegistrationSteps.MobileNo == stepNo)
			{
				return "SelectMobileNoNew";
			}
			else if ((int)MobileRegistrationSteps.PersonalDetails == stepNo)
			{
				return "PersonalDetailsNew";
			}
			else if ((int)MobileRegistrationSteps.Summary == stepNo)
			{
				return "MobileRegSummaryNew";
			}
			else if ((int)MobileRegistrationSteps.MNPSelectSuppLine == stepNo)
			{
				return "MNPSelectSuppLine";
            }
            else if ((int)MobileRegistrationSteps.DevicePlan == stepNo)
            {
                return "SelectDevicePlan";
            }
            else if ((int)MobileRegistrationSteps.Accessory == stepNo)
            {
                return "SelectAccessory";
            }
			return "";
		}

		private string GetRegActionStepNew(int stepNo)
		{
			if ((int)MobileRegistrationSteps.Device == stepNo)
			{
				return "SelectDevice";
			}
			else if ((int)MobileRegistrationSteps.DeviceCatalog == stepNo)
			{
				return "SelectDeviceCatalog";
			}
			else if ((int)MobileRegistrationSteps.Plan == stepNo)
			{
				return "SuppLinePlan";
			}
			else if ((int)MobileRegistrationSteps.Vas == stepNo)
			{
				return "SuppLineVASNew";
			}
			else if ((int)MobileRegistrationSteps.MobileNo == stepNo)
			{
				return "SelectMobileNoNew";
			}
			else if ((int)MobileRegistrationSteps.PersonalDetails == stepNo)
			{
				return "PersonalDetails";
			}
			else if ((int)MobileRegistrationSteps.Summary == stepNo)
			{
				return "MobileRegSummary";
			}
			else if ((int)MobileRegistrationSteps.MNPSelectSuppLine == stepNo)
			{
				return "MNPSelectSuppLine";
            }
            else if ((int)MobileRegistrationSteps.DevicePlan == stepNo)
            {
                return "SelectDevicePlan";
            }
			return "";
		}
		private string GetRegActionStepForSecondaryPlan(int stepNo)
		{

			if ((int)SecondaryLineRegistrationSteps.Plan == stepNo)
			{
				return "SecondaryLinePlan";
			}
			else if ((int)SecondaryLineRegistrationSteps.Vas == stepNo)
			{
				return "SecondaryLineVAS";
			}
			else if ((int)SecondaryLineRegistrationSteps.PersonalDetails == stepNo)
			{
				return "SecondaryLinePersonalDetails";
			}
			else if ((int)SecondaryLineRegistrationSteps.Summary == stepNo)
			{
				return "SecondaryLineRegSummary";
			}

			return "";
		}
		private void SetRegStepNo(MobileRegistrationSteps regStep)
		{
			switch (regStep)
			{
				case MobileRegistrationSteps.Device:
					Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Device;
					break;
				case MobileRegistrationSteps.DeviceCatalog:
					Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.DeviceCatalog;
					break;
				case MobileRegistrationSteps.Plan:
					Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Plan;
					break;
				case MobileRegistrationSteps.Vas:
					Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.Vas;
					break;
				case MobileRegistrationSteps.MobileNo:
					Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.MobileNo;
					break;
				case MobileRegistrationSteps.PersonalDetails:
					if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DeviceOnly)
					{
						Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() + 1;
					}
					else
					{
						Session[SessionKey.RegStepNo.ToString()] = (int)MobileRegistrationSteps.PersonalDetails;
					}
					break;
				default:
					break;
			}

			if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan || Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DeviceOnly)
			{
				if (regStep == MobileRegistrationSteps.Device)
					Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() + 1;
			}
			else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.PlanOnly)
			{
				Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() - 1;
			}
			else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.SuppPlan)
			{
				Session[SessionKey.RegStepNo.ToString()] = Session[SessionKey.RegStepNo.ToString()].ToInt() - 1;
			}

			// set Personal Details as step 3 for Device Only
			if (regStep == MobileRegistrationSteps.PersonalDetails)
			{

			}
		}

		private void ResetAllSession(int? type)
		{
			Session["RegMobileReg_FromSearch"] = null;
			Session[SessionKey.RegMobileReg_ResultMessage.ToString()] = null;
			WebHelper.Instance.ClearRegistrationSession();
		   
			switch (type)
			{
				case (int)MobileRegType.DevicePlan:
					Session["RegMobileReg_Type"] = (int)MobileRegType.DevicePlan;
					break;
				case (int)MobileRegType.PlanOnly:
					Session["RegMobileReg_Type"] = (int)MobileRegType.PlanOnly;
					break;
				case (int)MobileRegType.DeviceOnly:
					Session["RegMobileReg_Type"] = (int)MobileRegType.DeviceOnly;
					break;
				case (int)MobileRegType.SuppPlan:
					Session["RegMobileReg_Type"] = (int)MobileRegType.SuppPlan;
					break;
				case (int)MobileRegType.SecPlan:
					Session["RegMobileReg_Type"] = (int)MobileRegType.SecPlan;
					break;


			}
			// From guided sales
			if (Session["GuidedSales_SelectedModelImageID"].ToInt() != 0 && Session["GuidedSales_PkgPgmBdlPkgCompID"].ToInt() != 0)
				WebHelper.Instance.ConstructGuidedSalesDetails();
			else
				WebHelper.Instance.ClearGuidedSalesSession();
		}
		private DeviceOptionVM GetAvailableDeviceOptions()
		{
			var deviceOptVM = new DeviceOptionVM();
			using (var proxy = new CatalogServiceProxy())
			{
				List<Brand> brandIDs = new List<Brand>();
				brandIDs = proxy.GetBrandsForSuppLines();
				Session["list"] = brandIDs.Select(c => c.Code).ToList();
				deviceOptVM.AvailableBrands = brandIDs.ToList();
			}
			if (!string.IsNullOrEmpty(Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToString2()))
			{
				deviceOptVM.SelectedDeviceOption = "btnDvcOpt_" + Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToString2() + "_" + Session[SessionKey.RegMobileReg_SelectedOptionID.ToString()].ToString2();
			}
			if (!string.IsNullOrEmpty(Session["RegMobileReg_SelectedOptionID_Seco"].ToString2()))
			{
				deviceOptVM.SelectedDeviceOptionSeco = "btnDvcOpt_" + Session["RegMobileReg_SelectedOptionType_Seco"].ToString2() + "_" + Session["RegMobileReg_SelectedOptionID_Seco"].ToString2();
			}
			deviceOptVM.Type = Session[SessionKey.RegMobileReg_SelectedOptionType.ToString()].ToString2();
			if (!string.IsNullOrEmpty(Session["RegMobileReg_SelectedOptionID_Seco"].ToString2()))
			{
				deviceOptVM.Type = Session["RegMobileReg_SelectedOptionType_Seco"].ToString2();
			}
			return deviceOptVM;
		}
		public List<int> GetDepenedencyComponents(string componentIds)
		{
			List<int> depCompIds = new List<int>();

			using (var proxy = new CatalogServiceProxy())
			{
				depCompIds = proxy.GetAllDependentComponents(Convert.ToInt32(componentIds), 0);
			}

			return depCompIds;
		}
		private PackageVM GetAvailablePackagesForSecondaryLine(bool fromSubline = false)
		{
			var packageVM = new PackageVM();
			var availablePackages = new List<AvailablePackage>();
			var pgmBdlPckComponents = new List<PgmBdlPckComponent>();
			var programBundles = new List<PgmBdlPckComponent>();
			var packages = new List<Package>();
			var bundles = new List<Bundle>();
			var bundleIDs = new List<int>();
			var programs = new List<Program>();
			var filterBdlIDs = new List<int>();
			var bundlePackages = new List<PgmBdlPckComponent>();
			var packageIDs = new List<int>();
			var orderVM = new OrderSummaryVM();

			using (var proxy = new CatalogServiceProxy())
			{
				// retrieve bundles that assigned to current user
				var filterBdlPkgIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
				{
					PgmBdlPckComponent = new PgmBdlPckComponent()
					{
						LinkType = Properties.Settings.Default.Program_Bundle,
						FilterOrgType = Util.SessionOrgTypeCode
					},
					Active = true
				}).ToList();

				filterBdlIDs = MasterDataCache.Instance.FilterComponents(filterBdlPkgIDs).Select(a => a.ChildID).ToList();
				bundleIDs = filterBdlIDs;

				// Main Line
				if (!fromSubline)
				{
					if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
					{   /*Commented for articleid*/

						var modelID = proxy.BrandArticleImageGet(Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()].ToInt()).ModelID;

						var modelGroupIDs = proxy.ModelGroupModelGet(proxy.ModelGroupModelFind(new ModelGroupModelFind()
						{
							ModelGroupIDs = new List<int>(),
							ModelGroupModel = new ModelGroupModel()
							{
								ModelID = modelID
							},
							Active = true
						})).Select(a => a.ModelGroupID).Distinct().ToList();

						// Bundle - Package
						var pbpcIDs = proxy.ModelGroupGet(modelGroupIDs).Select(a => a.PgmBdlPckComponentID).ToList();

						bundlePackages = MasterDataCache.Instance.FilterComponents(pbpcIDs).Where(a => a.Active == true).ToList();
						bundleIDs = filterBdlIDs.Intersect(bundlePackages.Select(a => a.ParentID)).ToList();
						packageIDs = bundlePackages.Select(a => a.ChildID).ToList();
					}
				}

				var cmpFind = new PgmBdlPckComponentFind()
				 {
					 PgmBdlPckComponent = new PgmBdlPckComponent()
					 {
						 //commented for secondary line packages while implementing the Bite CR  
						 //LinkType = Properties.Settings.Default.Bundle_Package,
						 LinkType = Properties.Settings.Default.SecondaryPlan,
						 PlanType = fromSubline ? Properties.Settings.Default.SupplimentaryPlan : Properties.Settings.Default.ComplimentaryPlan,
					 },
					 ParentIDs = bundleIDs,
					 ChildIDs = packageIDs,
					 Active = true
				 };
				var query = MasterDataCache.Instance.FilterComponents(cmpFind);

				// Main Line
				if (!fromSubline)
				{
					if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
					{
						pgmBdlPckComponents = query.Where(a => bundleIDs.Contains(a.ParentID)).ToList();
					}
					else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.PlanOnly)
					{
						pgmBdlPckComponents = query.ToList();
					}
					else if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.SecPlan)
					{
						var querySeco = MasterDataCache.Instance.FilterComponents(proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
						{
							PgmBdlPckComponent = new PgmBdlPckComponent()
							{
								LinkType = Properties.Settings.Default.SecondaryPlan,
								PlanType = Properties.Settings.Default.MandatoryPlan,
							},
							ParentIDs = bundleIDs,
							ChildIDs = packageIDs,
							Active = true
						}));
						pgmBdlPckComponents = query.ToList();
						List<PgmBdlPckComponent> seocndary = querySeco.ToList();
						foreach (PgmBdlPckComponent component in seocndary)
							pgmBdlPckComponents.Add(component);
					}
				}
				else //Sub Line
				{
					pgmBdlPckComponents = query.ToList();
				}

				if (pgmBdlPckComponents.Count() > 0)
				{
					packages = proxy.PackageGet(pgmBdlPckComponents.Select(a => a.ChildID).Distinct()).ToList();
					bundles = proxy.BundleGet(pgmBdlPckComponents.Select(a => a.ParentID).Distinct()).ToList();
				}

				var pgmBdlIDs = proxy.PgmBdlPckComponentFind(new PgmBdlPckComponentFind()
				{
					PgmBdlPckComponent = new PgmBdlPckComponent()
					{
						LinkType = Properties.Settings.Default.Program_Bundle,
					},
					ChildIDs = pgmBdlPckComponents.Select(a => a.ParentID).ToList()
				});

				programBundles = MasterDataCache.Instance.FilterComponents(pgmBdlIDs).Distinct().ToList();

				var programIDs = programBundles.Select(a => a.ParentID).ToList();

				if (programIDs != null)
				{
					if (programIDs.Count() > 0)
					{
						programs = proxy.ProgramGet(programIDs).ToList();
					}
				}
			}


			foreach (var pbpc in pgmBdlPckComponents)
			{
				availablePackages.Add(new AvailablePackage()
				{
					PgmBdlPkgCompID = pbpc.ID,
					ProgramName = programs.Where(a => a.ID == (programBundles.Where(b => b.ChildID == pbpc.ParentID).SingleOrDefault().ParentID)).SingleOrDefault().Name,
					BundleName = bundles.Where(a => a.ID == pbpc.ParentID).SingleOrDefault().Name,
					PackageName = packages.Where(a => a.ID == pbpc.ChildID).SingleOrDefault().Name,
					PackageCode = pbpc.Code,
					//Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
					KenanCode = pbpc.KenanCode,
				});
			}


			//Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
			if (Session["RegMobileReg_Type"].ToInt() == (int)MobileRegType.DevicePlan)
			{
				if (Session[SessionKey.ArticleId.ToString()] != null)
				{
					string MOCStatus = !ReferenceEquals(Session[SessionKey.MOCIDs.ToString()], null) ? Session[SessionKey.MOCIDs.ToString()].ToString2() : string.Empty;
					List<string> objPackages = new List<string>();
					using (var proxy = new CatalogServiceProxy())
					{
						objPackages = proxy.GetPackagesForArticleId(Session[SessionKey.ArticleId.ToString()].ToString2(), MOCStatus);

					}
					if (objPackages != null && objPackages.Count > 0)
						packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).Where(a => string.Join(",", objPackages).Contains(a.KenanCode)).ToList();

				}
				else
					packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();

			}
			else
			{
				packageVM.AvailablePackages = availablePackages.OrderBy(a => a.BundleName).ThenBy(a => a.PackageName).ToList();
			}
			//Added by VLT on 12 May 2013 to get packages/contracts based on artcileid


			// Main Line
			if (!fromSubline)
				packageVM.SelectedPgmBdlPkgCompID = Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt();
			else
				packageVM.SelectedPgmBdlPkgCompID = Session["RegMobileSub_PkgPgmBdlPkgCompID"].ToInt();

			return packageVM;
		}
		private void ResetDepositSessions()
		{
			Session["RegMobileReg_MalyDevAdv"] = null;
			Session["RegMobileReg_MalayPlanAdv"] = null;
			Session["RegMobileReg_MalayDevDeposit"] = null;
			Session["RegMobileReg_MalyPlanDeposit"] = null;
			Session["RegMobileReg_MalAdvDeposit"] = null;
			Session["RegMobileReg_OthDevAdv"] = null;
			Session["RegMobileReg_OthPlanAdv"] = null;
			Session["RegMobileReg_OthDevDeposit"] = null;
			Session["RegMobileReg_OthPlanDeposit"] = null;
			Session["RegMobileReg_OtherAdvDeposit"] = null;

		}

		private string GetBackButtonDevicePlan()
		{
			string backURL = Url.Content("~/Home/IndexNew");
			var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
			if (dropObj.suppIndex == 0)
			{
				return backURL;
			}
			else
			{
				//will back to main flow last screen
			}

			return backURL;
		}

		
		#endregion Private Methods



		#region Other Public Methods

		public List<int> GetDepenedencyComponentsSupp(string componentIds)
		{
			List<int> depCompIds = new List<int>();

			using (var proxy = new CatalogServiceProxy())
			{
				depCompIds = proxy.GetAllDependentComponents(Convert.ToInt32(componentIds), 0);
			}

			return depCompIds;
		}

		private int GetPersonalDetailsBackButton()
		{
			int tabNumber = 0;

			if (!ReferenceEquals(Session["FromMNP"], null) && Convert.ToBoolean(Session["FromMNP"]))
			{
				bool IsMNPWithMultpleLines = ((!ReferenceEquals(Session["FromMNP"], null) && Convert.ToBoolean(Session["FromMNP"])) && (!ReferenceEquals(Session["MNPSupplementaryLines"], null) && (!ReferenceEquals(((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"]).SupplimentaryMSISDNs, null) && ((MNPSelectPlanForSuppline)Session["MNPSupplementaryLines"]).SupplimentaryMSISDNs.Count > 0)));
				if (IsMNPWithMultpleLines)
				{
					tabNumber = 11;
				}
				else
				{
					tabNumber = 3;
				}
			}
			else
			{
				var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
				if (dropObj.suppIndex == 1)
				{
					tabNumber = (int)MobileRegistrationSteps.MobileNo;
				}
				else
				{
					//will back to main flow last screen
				}

			}
			return tabNumber;
		}

		private int GetVasBackButton()
		{
			int tabNumber = 0;

			if (Session["GuidedSales_Type"].ToString2() != string.Empty)
			{
				if (Convert.ToInt32(Session["GuidedSales_Type"]) != MobileRegType.PlanOnly.ToInt())
				{
					return MobileRegistrationSteps.CustomerPersonalDetails.ToInt();
				}
			}


			var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

            if (dropObj.suppFlow[dropObj.suppIndex - 1].RegType == (int)MobileRegType.DevicePlan)
			{
				//tabNumber = (int)MobileRegistrationSteps.DevicePlan;
                tabNumber = (int)MobileRegistrationSteps.Accessory;
			}
            else if (dropObj.suppFlow[dropObj.suppIndex - 1].RegType == (int)MobileRegType.PlanOnly)
			{
				tabNumber = (int)MobileRegistrationSteps.Plan;
			}

			return tabNumber;
		}

        private int GetOrderSummaryBackButton()
        {
            int tabNumber = 0;
            tabNumber = (int)MobileRegistrationSteps.PersonalDetails;
            return tabNumber;
        }

		#endregion Other Public Methods


		#region AjaxCalls
		/// <summary>
		/// Send Spend Limit
		/// </summary>
		/// <param name="spendLimit">Entered Spend Limit Value</param>
		/// <returns>string</returns>
		public string SendSpendLimt(string spendLimit)
		{


			Session[SessionKey.SpendLimit.ToString()] = spendLimit;

			return string.Empty;
		}
		public string GetContractsForOffer(string OfferKenanCodes, string offerid)
		{
			return WebHelper.Instance.GetContractsForOffer(OfferKenanCodes, offerid, true);
		}

		#endregion AjaxCalls
	}

}

