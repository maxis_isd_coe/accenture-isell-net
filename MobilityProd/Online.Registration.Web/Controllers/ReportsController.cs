﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using log4net.Repository.Hierarchy;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;
using Online.Registration.Web.ReportsSvc;
using Online.Registration.Web.ViewModels;
using Online.Registration.DAL.ReportModels;
using log4net;
using Online.Registration.Web.CommonEnum;

namespace Online.Registration.Web.Controllers
{
    [Authorize]
    //[HandleError(View = "GenericError")]
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class ReportsController : Controller
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ReportsController));

        protected override void Initialize(RequestContext requestContext)
        {
            Util.CheckSessionAccess(requestContext);
            base.Initialize(requestContext);
        }

        #region Aging Report

        ReportsServiceClient proxy = new ReportsServiceClient();

        [Authorize(Roles = "RPT_R")]
        public ActionResult ReportsMain()
        {
            return View();
        }

        public ActionResult AgingReport()
        {
            //var centerOrgID = Util.SessionAccess.User.OrgID;
            //bool isInstaller = Util.CheckIsInstaller(centerOrgID);

            //if (isInstaller)
            //{
            //    ViewBag.CenterOrgID = centerOrgID;
            //}
            return View(new SearchViewAgingReportModel());
        }

        [HttpPost]
        public ActionResult AgingReport(SearchViewAgingReportModel svAgingRpt)
        {
            try
            {
                ViewBag.Format = svAgingRpt.Format;
                //int orgID = svOrderRpt.SearchCriterias.CenterOrgID.HasValue ? svOrderRpt.SearchCriterias.CenterOrgID.Value : 0;
                //var centerOrgID = Util.SessionAccess.User.OrgID;
                //bool isInstaller = Util.CheckIsInstaller(centerOrgID);

                var regTypeID = Util.GetIDByCode(RefType.RegType, Properties.Settings.Default.RegType_HomePersonal);

                var criteria = new AgingReportCriteria()
                {
                    RegID = svAgingRpt.SearchCriterias.RegID.HasValue ? svAgingRpt.SearchCriterias.RegID.Value : 0,
                    StatusID = svAgingRpt.SearchCriterias.StatusID.Value,
                    //CenterTypeID = svAgingRpt.SearchCriterias.CenterTypeID,
                    //CenterOrgID = svAgingRpt.SearchCriterias.CenterOrgID.Value,
                    CenterOrgID = 4,
                    CenterTypeID = 1,
                    RegTypeID = regTypeID,
                    MSISDN1 = svAgingRpt.SearchCriterias.MSISDN,
                    IDCardNo = svAgingRpt.SearchCriterias.ICNo,
                    CreatedBy = svAgingRpt.SearchCriterias.CreatedBy,
                    LastUpdatedBy = svAgingRpt.SearchCriterias.LastUpdatedBy,
                    RegDateFrom = svAgingRpt.SearchCriterias.RegDateFrom,
                    RegDateTo = svAgingRpt.SearchCriterias.RegDateTo
                };

                using (var proxy = new ReportsServiceProxy())
                {
                    svAgingRpt.SearchResults = proxy.AgingReportGet(new AgingReportGetReq() { Criteria = criteria }).AgingReports.ToList();
                }

                return View("AgingReportResult", svAgingRpt.SearchResults);

            }
            catch (Exception ex)
            {
               // return RedirectToException(ex);
                LogExceptions(ex);
                throw ex;
            }
        }

        #endregion

        #region Biometrics
        public ActionResult BiometricsReport()
        {
            return View(new SearchViewBiometricsReportModel());
        }

        [HttpPost]
        public ActionResult BiometricsReport(SearchViewBiometricsReportModel svBiomtrcsRpt)
        {
            try
            {
                ViewBag.Format = svBiomtrcsRpt.Format;
                var searchCriteria = new BiometricsReportCriteria()
                {
                    IDCardNo = svBiomtrcsRpt.SearchCriterias.IDCardNo,
                    Name = svBiomtrcsRpt.SearchCriterias.Name
                };

                using (var proxy = new ReportsServiceProxy())
                {
                    svBiomtrcsRpt.SearchResult = proxy.BiometricsReportGet(new BiometricsReportGetReq()
                        {
                            Criteria = searchCriteria
                        }).BiometricsReports.ToList();
                }

                return View("BiometricsReportResult", svBiomtrcsRpt.SearchResult);
            }
            catch (Exception ex)
            {
                //return RedirectToException(e);
                LogExceptions(ex);
                throw ex;
            }
        }
        #endregion

        #region
        [Authorize(Roles = "RPT_R")]
        public ActionResult FailTransctionReport()
        {
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            var failTranVM = new SearchViewFailedTranReportModel();

            using (var proxy = new ReportsServiceProxy())
            {
                failTranVM.FailTransactionReportSearchResults = proxy.FailTransactionSearch(new FailTransactionsSearchCriteria()
                {
                    StatusID = 0,//Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegNew),
                    IsMobilePlan = true,
                    UserID = Util.SessionAccess == null ? 0 : Util.SessionAccess.UserID,
                    PageSize = Properties.Settings.Default.PageSize
                });
            }

            Session["RegMobileReg_FromSearch"] = true;

            return View(failTranVM);
        }

        [HttpPost]
        [Authorize(Roles = "RPT_R")]
        public ActionResult FailTransctionReport(SearchViewFailedTranReportModel svFailTranRpt)
        {
            try
            {
                using (var proxy = new ReportsServiceProxy())
                {
                    svFailTranRpt.FailTransactionReportSearchResults = proxy.FailTransactionSearch(new FailTransactionsSearchCriteria()
                    {
                        RegID = svFailTranRpt.FailTransactionReportSearchCriteria.RegID,
                        IDCardNo = svFailTranRpt.FailTransactionReportSearchCriteria.IDCardNo,
                        StatusID = svFailTranRpt.FailTransactionReportSearchCriteria.StatusID,
                        QueueNo = svFailTranRpt.FailTransactionReportSearchCriteria.QueueNo,
                        RegTypeID = svFailTranRpt.FailTransactionReportSearchCriteria.RegTypeID,
                        RegDateFrom = svFailTranRpt.FailTransactionReportSearchCriteria.RegDateFrom,
                        IsMobilePlan = true,
                        UserID = Util.SessionAccess.UserID,
                        PageSize = Properties.Settings.Default.PageSize
                    });
                }

            }
            catch (Exception ex)
            {
                //return RedirectToException(ex);
                LogExceptions(ex);
                throw ex;
            }
            return View(svFailTranRpt);
        }

        #endregion


        #region
        [Authorize(Roles = "RPT_R")]
        public ActionResult RegistrationHistory(int regID)
        {
            RegistrationHistory objRegistrationHistory = new RegistrationHistory();
            ReportsServiceProxy reportsProxy = new ReportsServiceProxy();

            List<int> objRegistrationIDs = new List<int>();
            objRegistrationIDs.Add(regID);

            objRegistrationHistory.RegIstrationHistoryDetail = reportsProxy.RegHistoryDetails(regID);

            IEnumerable<int> registrationIDs = (IEnumerable<int>)objRegistrationIDs;
            List<KenanaLogDetails> objKenanHistory = new List<KenanaLogDetails>();
             try
            {
                using (var proxy = new RegistrationServiceProxy())
                {

                    objKenanHistory=proxy.KenanLogHistoryDetailsGet(registrationIDs).ToList();
                    if (objKenanHistory != null && objKenanHistory.Count > 0)
                        objRegistrationHistory.KenanCallHistory = objKenanHistory;
                }

            }
            catch (Exception ex)
            {
                //return RedirectToException(ex);
                LogExceptions(ex);
                throw;
            }
             return View(objRegistrationHistory);
        }
        private void LogExceptions(Exception ex)
        {
            /* BEGIN  Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/
            string strErrorMsg = Util.LogException(ex);
            string actionName = this.ControllerContext.RouteData.Values["action"] != null ? this.ControllerContext.RouteData.Values["action"].ToString() : "";
            string controllerName = this.ControllerContext.RouteData.Values["controller"] != null ? this.ControllerContext.RouteData.Values["controller"].ToString() : "";
            Logger.Error("Controller: " + controllerName + "\n" + " Action:" + actionName + "\n" + " Error:" + strErrorMsg, ex);
            /* END Added By Performance Tuning Team on 15 Nov 2013 to implement Exception handling*/

        }
        #endregion
    }
}
