﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Routing;

using Online.Registration.Web.Helper;

namespace Online.Registration.Web.Controllers
{
    //[HandleError(View = "GenericError")]
    [CustomErrorHandlerAttr(View = "GenericError")]
    [SessionAuthenticationAttribute]
    public class RawDumpReportController : Controller
    {
        protected override void Initialize(RequestContext requestContext)
        {
            Util.CheckSessionAccess(requestContext);
            base.Initialize(requestContext);
        }

        [Authorize(Roles = "RPT_R")]
        public ActionResult Index()
        {
            // uat file path - E:\IceAge\Application\RawReports\
            string[] filePaths = Directory.GetFiles(@Properties.Settings.Default.FilePath, "*.xls");
            //string[] filePaths = Directory.GetFiles(@"D:\IceAgeReports\", "*.xls");
            var fileNames = new List<string>();

            foreach (var path in filePaths)
            {
                fileNames.Add(Path.GetFileName(path));
            }

            ViewBag.FileNames = fileNames;

            return View();
        }

        public void DownloadReport(string fileName = "")
        {
            Response.AppendHeader("content-disposition", "attachement; filename=" + fileName);

            FileInfo fileDownload = new FileInfo(Properties.Settings.Default.FilePath + fileName);
            //FileInfo fileDownload = new FileInfo(@"D:\IceAgeReports\" + fileName);
            Response.Flush();
            Response.WriteFile(fileDownload.FullName);
        }

    }
}
