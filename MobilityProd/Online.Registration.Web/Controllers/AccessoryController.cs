﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using SNT.Utility;
using Online.Registration.DAL.Models;
using Online.Registration.Web.Helper;
using Online.Registration.Web.Models;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.CommonEnum;
using Online.Registration.Web.Properties;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.ContentRepository;
using Online.Registration.Web.SubscriberICService;
using System.Web.Security;

namespace Online.Registration.Web.Controllers
{
    public class AccessoryController : Controller
    {
        //
        // GET: /Accessory/

        public ActionResult Accounts()
        {
            WebHelper.Instance.ClearSessions();
            WebHelper.Instance.ResetDepositSessions();
            Session["Condition"] = null;
            Session["RebatePenalty"] = null;
            Session["Old_SimDetails"] = null;
            Session["_componentViewModel"] = null;
            Session["ExtratenMobilenumbers"] = null;
            Session["SimReplacement_WaiverDetails"] = null;
            Session["RegMobileReg_SelectedOptionID"] = null;
            Session["RegMobileReg_PersonalDetailsVM"] = null;
            Session["RegMobileReg_SimReplacementReasons"] = null;
            Session[SessionKey.SimType.ToString()] = null;
            Session[SessionKey.OfferId.ToString()] = null;
            Session[SessionKey.Discount.ToString()] = null;
            Session[SessionKey.Condition.ToString()] = null;
            Session[SessionKey.MarketCode.ToString()] = null;
            Session[SessionKey.FromPromoOffer.ToString()] = null;
            Session[SessionKey.SelectedPlanID.ToString()] = null;
            Session[SessionKey.SelectedPlanID_Seco.ToString()] = null;
            Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            Session[SessionKey.RegMobileReg_MainDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_OfferDevicePrice.ToString()] = null;
            Session[SessionKey.RegMobileReg_SelectedModelImageID.ToString()] = null;
            Session[SessionKey.Selected_Accessory.ToString()] = null;
            if (!ReferenceEquals(Session[SessionKey.RegMobileReg_OrderSummary.ToString()], null))
            {
                Session[SessionKey.RegMobileReg_OrderSummary.ToString()] = null;
            }
            if (!ReferenceEquals(TempData["Error"], null))
            {
                ModelState.AddModelError("Error", "Select an account");
            }
            retrieveAcctListByICResponse AcctListByICResponse = null;
            SupplementaryListAccounts SuppListAccounts = null;
            string supAccountStatus = string.Empty;
            try
            {
                if (Session["PPID"].ToString2() == "N" || Session["PPID"].ToString2() == "E")
                {
                    return RedirectToAction("SelectAccessory");
                }

                if (!ReferenceEquals(Session[SessionKey.SR_PPID.ToString()], null))
                {
                    AcctListByICResponse = (retrieveAcctListByICResponse)Session[SessionKey.SR_PPID.ToString()];
                    ///LIST ONLY GSM A/C'S
                    //if (AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse != null && (c.ServiceInfoResponse.lob == "POSTGSM" || c.ServiceInfoResponse.lob == "PREGSM") && (c.ServiceInfoResponse.serviceStatus.ToUpper() == "A" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "S" || c.ServiceInfoResponse.serviceStatus.ToUpper() == "B")).Count() > 0)
                    if (AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse != null && (c.ServiceInfoResponse.serviceStatus.ToUpper() != "T")).Count() > 0)
                    {
                        SuppListAccounts = new SupplementaryListAccounts();

                        List<string> lstMsisdnList = new List<string>();

                        foreach (var v in AcctListByICResponse.itemList.Where(c => c.ServiceInfoResponse != null && (c.ServiceInfoResponse.serviceStatus.ToUpper() != "T")).ToList())
                        {

                            Session["KenanACNumber"] = v.AcctExtId;//Anthony - [Regression-2]
                            if (Session[SessionKey.IDCardTypeIDMSiSDNNew.ToString()].ToString2() == "6") //MSISDN Search only
                            {
                                var prinExternalID = string.Empty;
                                if (Session[SessionKey.IDCardMSiSDNNew.ToString()].ToString() == v.ExternalId)
                                {
                                    if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "S")
                                    {
                                        //Get the MISM primary msisdn 
                                        using (var PrinSupproxy = new PrinSupServiceProxy())
                                        {
                                            retrieveMismDetlsResponse primSecResponse = PrinSupproxy.getPrimSecondaryines(v.ExternalId);
                                            if (primSecResponse != null && primSecResponse.itemList.ToList().Count > 0)
                                            {
                                                prinExternalID = primSecResponse.itemList.Where(a => a.PrimSecInd == "P").FirstOrDefault().Msisdn;
                                            }
                                        }
                                    }
                                    SuppListAccounts.SuppListAccounts.Add(
                                        new AddSuppInquiryAccount
                                        {
                                            AccountNumber = v.AcctExtId,// v.Account.AccountNumber,
                                            ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                            Address = v.Account != null ? v.Account.Address : string.Empty,
                                            Category = v.Account != null ? v.Account.Category : string.Empty,
                                            CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,

                                            Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,

                                            IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                            IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                            MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                            Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                            SubscribeNo = v.SusbcrNo,//v.Account.SubscribeNo  Himansu Changed
                                            SubscribeNoResets = v.SusbcrNoResets,//v.Account.SubscribeNoResets himansu
                                            externalId = v.ExternalId,
                                            accountExtId = v.AcctExtId,
                                            accountIntId = v.AcctIntId,
                                            AccountName = v.ServiceInfoResponse.lob,
                                            AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                            IsMISM = v.IsMISM,
                                            SimSerial = v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "S" ? GetsimSerial(v.ExternalId) : string.Empty,
                                            AccountType = v.ServiceInfoResponse.prinSuppInd,
                                            PrinMsisdn = !string.IsNullOrEmpty(prinExternalID) ? prinExternalID : string.Empty,
                                        });

                                    if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "P" && v.SecondarySimList != null)
                                    {
                                        //Get the MISM primary msisdn 
                                        using (var PrinSupproxy = new PrinSupServiceProxy())
                                        {
                                            retrieveMismDetlsResponse primSecResponse = PrinSupproxy.getPrimSecondaryines(v.ExternalId);
                                            if (primSecResponse != null && primSecResponse.itemList.ToList().Count > 0)
                                            {
                                                prinExternalID = primSecResponse.itemList.Where(a => a.PrimSecInd == "P").FirstOrDefault().Msisdn;
                                                foreach (var supp in v.SecondarySimList)
                                                {
                                                    SuppListAccounts.SuppListAccounts.Add(
                                           new AddSuppInquiryAccount
                                           {

                                               AccountNumber = v.AcctExtId,
                                               ActiveDate = string.Empty,
                                               Address = v.Account != null ? v.Account.Address : string.Empty,
                                               Category = string.Empty,
                                               CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                               Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,

                                               IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                               IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                               MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                               Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                               SubscribeNo = supp.FxSubscrNo,
                                               SubscribeNoResets = supp.FxSubscrNoResets,
                                               externalId = supp.Msisdn,
                                               accountExtId = v.AcctExtId,
                                               accountIntId = supp.FxAcctNo,
                                               AccountName = v.ServiceInfoResponse.lob,

                                               AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                               IsMISM = v.IsMISM,
                                               SimSerial = supp.PrimSecInd == "S" ? GetsimSerial(supp.Msisdn) : string.Empty,
                                               AccountType = "S",
                                               PrinMsisdn = v.ExternalId != supp.Msisdn ? v.ExternalId : string.Empty,


                                           });
                                                }
                                            }
                                        }
                                    }

                                    if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "P" && v.PrinSupplimentaryResponse != null && v.PrinSupplimentaryResponse.itemList.Count > 0)
                                    {

                                        foreach (var supp in v.PrinSupplimentaryResponse.itemList)
                                        {
                                            if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                            {
                                                if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                    supAccountStatus = "S";
                                                else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                    supAccountStatus = "D";
                                                else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                    supAccountStatus = "T";
                                                else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                    supAccountStatus = "B";
                                                else
                                                    supAccountStatus = "A";
                                            }

                                            string accountNumber = string.Empty;
                                            var suppAccount = AcctListByICResponse.itemList.Where(s => s.ExternalId == supp.msisdnField && s.ServiceInfoResponse.prinSuppInd == "S" && !s.IsMISM).FirstOrDefault();
                                            accountNumber = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId;

                                            SuppListAccounts.SuppListAccounts.Add(
                                               new AddSuppInquiryAccount
                                               {
                                                   AccountNumber = accountNumber,// v.AcctExtId,
                                                   ActiveDate = string.Empty,
                                                   Address = v.Account != null ? v.Account.Address : string.Empty,
                                                   Category = string.Empty,
                                                   CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                   Holder = supp.cust_nmField, //v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                   IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                   IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                   MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                   Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                   SubscribeNo = supp.subscr_noField,
                                                   SubscribeNoResets = supp.subscr_no_resetsField,
                                                   externalId = supp.msisdnField,
                                                   accountExtId = v.AcctExtId,
                                                   accountIntId = supp.acct_noField,
                                                   AccountName = v.ServiceInfoResponse.lob,
                                                   AccountStatus = supAccountStatus, //v.ServiceInfoResponse.serviceStatus,
                                                   IsMISM = supp.IsMISM,
                                                   //SimSerial = supp.PrimSecInd == "S" ? GetsimSerial(supp.Msisdn) : string.Empty,
                                                   AccountType = "S",
                                                   PrinMsisdn = v.ExternalId // != supp.Msisdn ? v.ExternalId : string.Empty
                                                   //IsGreater2GBCompExists = is2GBCompExistOnPkg
                                               });
                                        }

                                    }

                                }
                            }
                            else
                            {
                                if (!v.IsMISM)
                                {
                                    if ((v.ServiceInfoResponse.prinSuppInd != "S" && !v.IsMISM))
                                    {
                                        //if (!lstMsisdnList.Contains(v.ExternalId))
                                        //{
                                        lstMsisdnList.Add(v.ExternalId);
                                        //Add only Principal Lines
                                        SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = v.AcctExtId,
                                                        ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        Category = v.Account != null ? v.Account.Category : string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        //Holder = v.Account != null ? v.Account.Holder : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = v.SusbcrNo,
                                                        SubscribeNoResets = v.SusbcrNoResets,
                                                        externalId = v.ExternalId,
                                                        accountExtId = v.AcctExtId,
                                                        accountIntId = v.AcctIntId,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                                        IsMISM = v.IsMISM,
                                                        SimSerial = string.Empty,
                                                        AccountType = "P",
                                                        PrinMsisdn = string.Empty,
                                                    });
                                        // }
                                        if (v.PrinSuppResponse != null && v.PrinSuppResponse.itemList != null && v.PrinSuppResponse.itemList.Count > 0)
                                        {
                                            //joshi added
                                            Session["KenanACNumber"] = v.AcctExtId;
                                            //end joshi
                                            foreach (var supp in v.PrinSuppResponse.itemList)
                                            {
                                                if (v.ExternalId != supp.msisdnField)
                                                {
                                                    //if (!lstMsisdnList.Contains(supp.msisdnField))
                                                    //{
                                                    var suppAccount = AcctListByICResponse.itemList.Where(i => i.ExternalId == supp.msisdnField && i.ServiceInfoResponse != null && i.ServiceInfoResponse.prinSuppInd == "S" && !i.IsMISM).FirstOrDefault();

                                                    if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                                    {
                                                        if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                            supAccountStatus = "S";
                                                        else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                            supAccountStatus = "D";
                                                        else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                            supAccountStatus = "T";
                                                        else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                            supAccountStatus = "B";
                                                        else
                                                            supAccountStatus = "A";
                                                    }

                                                    lstMsisdnList.Add(supp.msisdnField);
                                                    SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId,  //string.IsNullOrEmpty(supp.acc_AccExtId)? v.AcctExtId :supp.acc_AccExtId,
                                                        ActiveDate = string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        Category = string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        //  Holder = v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = supp.subscr_noField,
                                                        SubscribeNoResets = supp.subscr_no_resetsField,
                                                        externalId = supp.msisdnField,
                                                        accountExtId = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId, //string.IsNullOrEmpty(supp.acc_AccExtId) ? v.AcctExtId : supp.acc_AccExtId,
                                                        accountIntId = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctIntId) ? suppAccount.AcctIntId : supp.acct_noField,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        //Added by Rajender SIMReplacement Issue SIMReplacement Issue
                                                        AccountStatus = supAccountStatus, //v.ServiceInfoResponse.serviceStatus,
                                                        IsMISM = v.IsMISM,
                                                        SimSerial = string.Empty,
                                                        AccountType = "S",
                                                        PrinMsisdn = v.ExternalId != supp.msisdnField ? v.ExternalId : string.Empty,
                                                    });
                                                    //}
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {

                                    //Listing the MISM LIST
                                    if ((v.ServiceInfoResponse.prinSuppInd == "P") && v.IsMISM)
                                    {
                                        //if (!lstMsisdnList.Contains(v.ExternalId))
                                        //{
                                        lstMsisdnList.Add(v.ExternalId);
                                        //Adding primary  line for MISM

                                        SuppListAccounts.SuppListAccounts.Add(
                                                    new AddSuppInquiryAccount
                                                    {
                                                        AccountNumber = v.AcctExtId,
                                                        ActiveDate = v.Account != null ? v.Account.ActiveDate : string.Empty,
                                                        Address = v.Account != null ? v.Account.Address : string.Empty,
                                                        Category = v.Account != null ? v.Account.Category : string.Empty,
                                                        CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                        //   Holder = v.Account != null ? v.Account.Holder : string.Empty,
                                                        Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                        IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                        IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                        MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                        Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                        SubscribeNo = v.SusbcrNo,
                                                        SubscribeNoResets = v.SusbcrNoResets,
                                                        externalId = v.ExternalId,
                                                        accountExtId = v.AcctExtId,
                                                        accountIntId = v.AcctIntId,
                                                        AccountName = v.ServiceInfoResponse.lob,
                                                        AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                                        IsMISM = v.IsMISM,
                                                        SimSerial = string.Empty,
                                                        AccountType = "P",
                                                        PrinMsisdn = string.Empty,
                                                    });
                                        //
                                    }

                                    if (v.ServiceInfoResponse.prinSuppInd == "P" && v.IsMISM && v.SecondarySimList != null)
                                    {

                                        //Adding primary and Secondary line for MISM
                                        foreach (var supp in v.SecondarySimList)
                                        {
                                            //if (!lstMsisdnList.Contains(supp.Msisdn))
                                            //{

                                            lstMsisdnList.Add(supp.Msisdn);
                                            SuppListAccounts.SuppListAccounts.Add(
                                            new AddSuppInquiryAccount
                                            {
                                                AccountNumber = v.AcctExtId,
                                                ActiveDate = string.Empty,
                                                Address = v.Account != null ? v.Account.Address : string.Empty,
                                                Category = string.Empty,
                                                CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                // Holder = v.Account != null ? v.Account.Holder : string.Empty,
                                                Holder = v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                SubscribeNo = supp.FxSubscrNo,
                                                SubscribeNoResets = supp.FxSubscrNoResets,
                                                externalId = supp.Msisdn,
                                                accountExtId = v.AcctExtId,
                                                accountIntId = supp.FxAcctNo,
                                                AccountName = v.ServiceInfoResponse.lob,
                                                AccountStatus = v.ServiceInfoResponse.serviceStatus,
                                                IsMISM = v.IsMISM,
                                                SimSerial = supp.PrimSecInd == "S" ? GetsimSerial(supp.Msisdn) : string.Empty,
                                                //AccountType = v.ExternalId != supp.Msisdn ? "P" : "S",
                                                AccountType = "S",
                                                PrinMsisdn = v.ExternalId != supp.Msisdn ? v.ExternalId : string.Empty,
                                            });
                                            //}
                                        }

                                    }

                                    if (v.IsMISM && v.ServiceInfoResponse.prinSuppInd == "P" && v.PrinSupplimentaryResponse != null && v.PrinSupplimentaryResponse.itemList.Count > 0)
                                    {

                                        foreach (var supp in v.PrinSupplimentaryResponse.itemList)
                                        {
                                            if (!string.IsNullOrEmpty(supp.subscr_statusField))
                                            {
                                                if (supp.subscr_statusField.ToLower().Contains("suspend"))
                                                    supAccountStatus = "S";
                                                else if (supp.subscr_statusField.ToLower().Contains("deactive"))
                                                    supAccountStatus = "D";
                                                else if (supp.subscr_statusField.ToLower().Contains("terminated"))
                                                    supAccountStatus = "T";
                                                else if (supp.subscr_statusField.ToLower().Contains("barred"))
                                                    supAccountStatus = "B";
                                                else
                                                    supAccountStatus = "A";
                                            }

                                            string accountNumber = string.Empty;
                                            var suppAccount = AcctListByICResponse.itemList.Where(s => s.ExternalId == supp.msisdnField && s.ServiceInfoResponse.prinSuppInd == "S" && !s.IsMISM).FirstOrDefault();
                                            accountNumber = suppAccount != null && !string.IsNullOrEmpty(suppAccount.AcctExtId) ? suppAccount.AcctExtId : v.AcctExtId;

                                            SuppListAccounts.SuppListAccounts.Add(
                                               new AddSuppInquiryAccount
                                               {
                                                   AccountNumber = accountNumber,// v.AcctExtId,
                                                   ActiveDate = string.Empty,
                                                   Address = v.Account != null ? v.Account.Address : string.Empty,
                                                   Category = string.Empty,
                                                   CompanyName = v.Account != null ? v.Account.CompanyName : string.Empty,
                                                   Holder = supp.cust_nmField, //v.Customer != null ? !string.IsNullOrEmpty(v.Customer.CustomerName) ? v.Customer.CustomerName : v.Account != null ? v.Account.Holder : string.Empty : v.Account != null ? v.Account.Holder : string.Empty,
                                                   IDNumber = v.Account != null ? v.Account.IDNumber : string.Empty,
                                                   IDType = v.Account != null ? v.Account.IDType : string.Empty,
                                                   MarketCode = v.Account != null ? v.Account.MarketCode : string.Empty,
                                                   Plan = v.Account != null ? v.Account.Plan : string.Empty,
                                                   SubscribeNo = supp.subscr_noField,
                                                   SubscribeNoResets = supp.subscr_no_resetsField,
                                                   externalId = supp.msisdnField,
                                                   accountExtId = v.AcctExtId,
                                                   accountIntId = supp.acct_noField,
                                                   AccountName = v.ServiceInfoResponse.lob,
                                                   AccountStatus = supAccountStatus, //v.ServiceInfoResponse.serviceStatus,
                                                   IsMISM = supp.IsMISM,
                                                   //SimSerial = supp.PrimSecInd == "S" ? GetsimSerial(supp.Msisdn) : string.Empty,
                                                   AccountType = "S",
                                                   PrinMsisdn = v.ExternalId // != supp.Msisdn ? v.ExternalId : string.Empty
                                                   //IsGreater2GBCompExists = is2GBCompExistOnPkg
                                               });
                                        }

                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            //Joshi added for Audit Log
            using (var proxy = new UserServiceProxy())
            {
                proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.DeviceSales, "Select Account", Session["KenanACNumber"].ToString2(), "");
            }
            //End
            return View(SuppListAccounts);
        }

        [HttpPost]
        public ActionResult Accounts(FormCollection collection)
        {
            try
            {
                Session[SessionKey.SelectedAccountNumber.ToString()] = collection["SelectedAccountDtls"].ToString2();
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }

            return RedirectToAction("SelectAccessory");
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DC")]
        public ActionResult SelectAccessory(int? type)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var dropObj = new DropFourObj();
            var accessoriesVM = new AccessoriesVM();
            Session[SessionKey.OrderType.ToString()] = (int)MobileRegType.Accessories;

            if (Session[SessionKey.SelectedAccountNumber.ToString()].ToString2().Length > 0)
            {
                string SelectAccountDtls = Session[SessionKey.SelectedAccountNumber.ToString()].ToString();
                string[] strAccountDtls = SelectAccountDtls.Split(',');
                Session[SessionKey.FxAccNo.ToString()] = strAccountDtls[0].ToString();
				Session[SessionKey.MSISDN.ToString()] = Session[SessionKey.ExternalID.ToString()] = strAccountDtls[1].ToString(); //Acd.externalId
                Session[SessionKey.AccExternalID.ToString()] = strAccountDtls[2].ToString();
                Session[SessionKey.KenanACNumber.ToString()] = strAccountDtls[3].ToString();
                var mobileNos = new List<string>();
                mobileNos.Add(strAccountDtls[1].ToString());
                Session[SessionKey.RegMobileReg_MobileNo.ToString()] = mobileNos;
                Session["SubscribeNo"] = strAccountDtls[4].ToString();
                Session["SubscribeNoResets"] = strAccountDtls[5].ToString();
                Session["AccountType"] = strAccountDtls[6].ToString();
            }

            dropObj.mainFlow.RegType = MobileRegType.Accessories.ToInt();
            Session[SessionKey.DropFourObj.ToString()] = dropObj;

            return View(accessoriesVM);
        }

        [HttpPost]
        public ActionResult SelectAccessory(FormCollection collection, AccessoriesVM accessoriesVM)
        {
            var tabNumber = collection["TabNumber"].ToInt();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];

            var selectedAccessory = Session[SessionKey.Selected_Accessory.ToString()] as List<AccessoryVM> ?? new List<AccessoryVM>();
            DropFourHelpers.UpdateSelectedAccessory(dropObj, selectedAccessory);
            Session[SessionKey.DropFourObj.ToString()] = dropObj;

            return RedirectToAction("PersonalDetails");
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DC")]
        public ActionResult PersonalDetails()
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            var personalDetailsVM = new PersonalDetailsVM();
            dropObj.personalInformation = dropObj.personalInformation ?? new PersonalDetailsVM();

            if (Session["PPID"].ToString2() == "N")
            {
                personalDetailsVM = dropObj.personalInformation;
            }
            else
            {
                personalDetailsVM = Util.FormPersonalDetailsandCustomer(personalDetailsVM);
                personalDetailsVM = Util.CheckDisablePersonalDetailsPage(personalDetailsVM);
            }

            WebHelper.Instance.PopulateDealerInfo(personalDetailsVM);

            return View(personalDetailsVM);
        }

        [HttpPost]
        public ActionResult PersonalDetails(FormCollection collection, PersonalDetailsVM personalDetailsVM)
        {
            var tabNumber = collection["TabNumber"].ToInt();
            var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
            var refStates = MasterDataCache.Instance.State;
            var postCodeCityState = new PosCode();
            var isValid = false;
            var ErrorMessage = string.Empty;

            if (tabNumber == (int)DeviceSaleSteps.Accessory)
            {
                return RedirectToAction("SelectAccessory");
            }

            var idCardTypeID = collection["Customer.IDCardTypeID"].ToInt();
            var idCardNo = collection["InputIDCardNo"].ToString2();
            var dobYear = collection["DOBYear"].ToInt();
            var dobMonth = collection["DOBMonth"].ToInt();
            var dobDay = collection["DOBDay"].ToInt();
            //var dateOfBirth = new DateTime(dobYear, dobMonth, dobDay);

            personalDetailsVM.Customer = personalDetailsVM.Customer ?? new Customer();
            personalDetailsVM.Customer.CustomerTitleID = collection["Customer.CustomerTitleID"].ToInt();
            personalDetailsVM.Customer.FullName = collection["Customer.FullName"].ToString2();
            personalDetailsVM.Customer.IDCardTypeID = collection["Customer.IDCardTypeID"] != null
                                                            ? idCardTypeID
                                                            : Session[SessionKey.IDCardTypeID.ToString()].ToInt();
            personalDetailsVM.Customer.IDCardNo = !string.IsNullOrEmpty(idCardNo)
                                                        ? idCardNo
                                                        : Session[SessionKey.IDCardNo.ToString()].ToString2();
            //personalDetailsVM.Customer.DateOfBirth = dateOfBirth;
            personalDetailsVM.Customer.EmailAddr = collection["Customer.EmailAddr"].ToString2();
            personalDetailsVM.Customer.ContactNo = collection["InputContactNo"].ToString2();
            personalDetailsVM.Address.Line1 = collection["Address.Line1"].ToString2();
            personalDetailsVM.Address.Line2 = collection["Address.Line2"].ToString2();
            personalDetailsVM.Address.Line3 = collection["Address.Line3"].ToString2();

            postCodeCityState = WebHelper.Instance.checkStateCityByPostcode(collection["Address.Postcode"].ToString2());

            personalDetailsVM.Address.Postcode = postCodeCityState.PostCode;
            personalDetailsVM.Address.StateID = postCodeCityState.stateID;
            personalDetailsVM.Address.Town = postCodeCityState.City;

            if (refStates != null && refStates.State.Any() && refStates.State.Where(x => x.ID == postCodeCityState.stateID).Any())
            {
                personalDetailsVM.Address.State = personalDetailsVM.Address.State ?? new State();
                personalDetailsVM.Address.State = refStates.State.Where(x => x.ID == postCodeCityState.stateID).FirstOrDefault();
            }

            personalDetailsVM.QueueNo = collection["QueueNo"].ToString2();
            Session[SessionKey.QueueNo.ToString()] = personalDetailsVM.QueueNo;

            dropObj.personalInformation = personalDetailsVM;
            Session[SessionKey.DropFourObj.ToString()] = dropObj;

            WebHelper.Instance.ValidateDealerInfo(personalDetailsVM, out isValid, out ErrorMessage);
            if (!isValid)
            {
                ViewBag.ErrorMessage = ErrorMessage;
                return View(personalDetailsVM);
            }

            dropObj.personalInformation = personalDetailsVM;
            Session[SessionKey.DropFourObj.ToString()] = dropObj;
            
            return RedirectToAction("CustomerSummary");
        }

        [Authorize(Roles = "MREG_W,MREG_C,MREG_DC")]
        [DoNoTCache]
        public ActionResult MobileRegSuccess(int regID = 0)
        {
            //Save the User TransactionLog
            WebHelper.Instance.SaveUserTransactionLogs(regID);
            PersonalDetailsVM personalDetails = new PersonalDetailsVM();
            var pegaOfferList = (PegaOfferVM)Session["PegaOfferList"];

            personalDetails.RegID = regID;
			bool showPegaDashBoard = ConfigurationManager.AppSettings["PegaRecommendationFlag"].ToBool();
			if (showPegaDashBoard)
			{
				using (var proxy = new RegistrationServiceProxy())
				{
					personalDetails.PegaVM = new PegaRecommendationsVM();
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria = new PegaRecommendationSearchCriteria();
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.MSISDN = "0";
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.IDCardNo = pegaOfferList == null ? "0" : pegaOfferList.idCardNumber;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.Response = Constants.ACCEPT;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedStartDt = DateTime.Now.Date;
					personalDetails.PegaVM.PegaRecommendationsSearchCriteria.CapturedEndtDt = DateTime.Now.Date.AddDays(1).AddTicks(-1);

					personalDetails.PegaVM.PegaRecommendationsSearchResult = proxy.GetPegaRecommendationbyMSISDN(personalDetails.PegaVM.PegaRecommendationsSearchCriteria);
					personalDetails.PegaVM.PegaRecommendationsSearchResult = personalDetails.PegaVM.PegaRecommendationsSearchResult.Where(x => x.Status.ToUpper() == (Constants.OPEN).ToUpper() || x.Status.ToUpper() == (Constants.CHOOSE_ONE).ToUpper()).ToList();
				}
			}
            

            return View(personalDetails);
            //return View(regID);
        }

        [Authorize]
        public ActionResult CustomerSummary(int? regID)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            PersonalDetailsVM personalDetailsVM = new PersonalDetailsVM();
            OrderSummaryVM objSummaryVM = new OrderSummaryVM();
            var regDetails = new Online.Registration.DAL.RegistrationDetails();
            var reg = new DAL.Models.Registration();
            var regMdlGrpModel = new RegMdlGrpModel();
            var regAttributes = new List<RegAttributes>();
            int paymentstatus = -1;
            try
            {
                if (regID.ToInt() > 0)
                {
                    //ClearRegistrationSession();
                    personalDetailsVM.RegID = regID.Value.ToInt();
                    using (var proxy = new RegistrationServiceProxy())
                    {
                        regDetails = proxy.GetRegistrationFullDetails(personalDetailsVM.RegID);
                        regAttributes = proxy.RegAttributesGetByRegID(personalDetailsVM.RegID);
                        if (regDetails.Customers.FirstOrDefault().IsVIP == 1 && !Roles.IsUserInRole("MREG_SV"))
                        {
                            Session["VIPFailMsg"] = true;
                            return RedirectToAction("StoreKeeper", "Registration");
                        }

                        reg = regDetails.Regs.SingleOrDefault();
                        personalDetailsVM.MSISDN1 = reg.MSISDN1.ToString2();
                        paymentstatus = regDetails.PaymentStatus;
                        if (paymentstatus != -1)
                        {
                            if (paymentstatus == 0)
                            {
                                personalDetailsVM.PaymentStatus = "0";
                            }
                            else
                            {
                                personalDetailsVM.PaymentStatus = "1";
                            }
                        }
                        else
                        {
                            personalDetailsVM.PaymentStatus = "0";
                        }
                        //Added by Vahini to show CMSS case details in order summary
                        CMSSComplain objCMSSCase = new CMSSComplain();
                        objCMSSCase = proxy.GetCMSSCaseDetails(reg.ID);
                        if (objCMSSCase != null)
                        {
                            personalDetailsVM.CBRNo = objCMSSCase.CBRNo;
                            personalDetailsVM.CMSSID = objCMSSCase.CMSSID;
                            if (!string.IsNullOrEmpty(objCMSSCase.CMSSID))
                                personalDetailsVM.CMSSCaseStatus = "Success";
                            else
                                personalDetailsVM.CMSSCaseStatus = "Fail";

                        }
                    }
                    personalDetailsVM.BiometricVerify = reg.BiometricVerify;
                    personalDetailsVM.BlacklistExternal = reg.ExternalBlacklisted;
                    personalDetailsVM.BlacklistInternal = reg.InternalBlacklisted;
                    personalDetailsVM.RegIMEINumber = reg.IMEINumber;
                    personalDetailsVM.RegTypeID = reg.RegTypeID;
                    personalDetailsVM.AgeCheckStatus = reg.AgeCheckStatus;
                    personalDetailsVM.RFSalesDT = reg.RFSalesDT;
                    personalDetailsVM.DDMFCheckStatus = reg.DDMFCheckStatus;
                    personalDetailsVM.AddressCheckStatus = reg.AddressCheckStatus;
                    personalDetailsVM.OutStandingCheckStatus = reg.OutStandingCheckStatus;
                    personalDetailsVM.TotalLineCheckStatus = reg.TotalLineCheckStatus;
                    personalDetailsVM.PrincipleLineCheckStatus = reg.PrincipleLineCheckStatus;
                    personalDetailsVM.ContractCheckStatus = reg.ContractCheckStatus;
                    personalDetailsVM.UserType = reg.UserType;
                    personalDetailsVM.KenanAccountNo = reg.KenanAccountNo;
                    personalDetailsVM.OutstandingAcs = reg.OutstandingAcs;
                    personalDetailsVM.PrePortInCheckStatus = reg.PrePortInCheckStatus;
                    personalDetailsVM.MNPServiceIdCheckStatus = reg.MNPServiceIdCheckStatus;
                    personalDetailsVM.MOC_Status = reg.MOC_Status;
                    personalDetailsVM.Whitelist_Status = reg.Whitelist_Status;
                    personalDetailsVM.fxAcctNo = reg.fxAcctNo;
                    Session["KenanACNumber"] = reg.fxAcctNo;
                    personalDetailsVM.FxSubscrNo = reg.fxSubscrNo;
                    personalDetailsVM.FxSubScrNoResets = reg.fxSubScrNoResets;
                    personalDetailsVM.IMPOSFileName = reg.IMPOSFileName;
                    personalDetailsVM.IsVerified = reg.IsVerified;
                    personalDetailsVM.Deposite = reg.AdditionalCharges;
                    personalDetailsVM.StatusID = regDetails.RegStatus.SingleOrDefault().StatusID;
                    using (var configProxy = new ConfigServiceProxy())
                    {
                        personalDetailsVM.StatusCode = configProxy.StatusGet(personalDetailsVM.StatusID).Code;
                    }

                    personalDetailsVM.PenaltyAmount = reg.PenaltyAmount;
                    if (personalDetailsVM.StatusID == 21)
                    {
                        personalDetailsVM.MessageDesc = regDetails.CancelReason;
                    }

                    // MobileNo
                    var mobileNos = new List<string>();
                    mobileNos.Add(reg.MSISDN1);
                    mobileNos.Add(reg.MSISDN2);

                    personalDetailsVM.Customer = regDetails.Customers.SingleOrDefault();
                    personalDetailsVM.Address = regDetails.RegAddresses.SingleOrDefault();
                    regMdlGrpModel = regDetails.RegMdlGrpMdls.SingleOrDefault();


                    if (regMdlGrpModel != null && regMdlGrpModel.ID > 0)
                    {

                        objSummaryVM.SelectedModelImageID = regMdlGrpModel.ModelImageID.ToInt();
                        objSummaryVM = WebHelper.Instance.ConstructPrimaryDeviceDetails(objSummaryVM, 0);

                    }

                    string[] Result = Util.GetMocAndLibStatus(string.IsNullOrEmpty(Session[SessionKey.ExternalID.ToString()].ToString2()) ? string.Empty : Session[SessionKey.ExternalID.ToString()].ToString2(), regID.ToInt());
                    personalDetailsVM.Liberlization_Status = Result[0];
                    personalDetailsVM.MOCStatus = Result[1];
                    personalDetailsVM.QueueNo = reg.QueueNo;
                    personalDetailsVM.objRegAccessory = WebHelper.Instance.getAccessoryDetails(regID.ToInt());
                    personalDetailsVM.RegAttributes = regAttributes;

                    var salesPerson = regDetails.Regs.FirstOrDefault().SalesPerson;
                    var orgID = regDetails.Regs.FirstOrDefault().CenterOrgID;
                    var outletCode = string.Empty;
                    var salesCode = string.Empty;

                    WebHelper.Instance.generateOutletAndSalesCode(orgID, salesPerson, personalDetailsVM.RegAttributes, out outletCode, out salesCode);
                    personalDetailsVM.OutletCode = outletCode;
                    personalDetailsVM.SalesCode = salesCode;

                    objSummaryVM.PersonalDetailsVM = personalDetailsVM;
                }
                else
                {
                    var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                    objSummaryVM.PersonalDetailsVM = dropObj.personalInformation;
                }
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            return View(objSummaryVM);
        }

        [HttpPost]
        public ActionResult CustomerSummary(FormCollection collection)
        {
            //code when back is clicked
            if (collection["TabNumber"].ToInt() == (int)DeviceSaleSteps.PersonalDetails)
            {
                return RedirectToAction("PersonalDetails");
            }
            
            int regId = collection["RegistrationId"].ToInt();
            
            try
            {
                string redirectUrl = string.Empty;
                switch (collection["submit1"].ToString2())
                {
                    case "createAcc":
                        break;

                    case "cancel":
                        break;

                    case "close":
                        redirectUrl = "CustomerSummary";

                        using (var proxy = new RegistrationServiceProxy())
                        {
                            proxy.RegistrationClose(new RegStatus()
                            {
                                RegID = regId,
                                Active = true,
                                CreateDT = DateTime.Now,
                                StartDate = DateTime.Now,
                                LastAccessID = Util.SessionAccess.UserName,
                                StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegClosed)
                            });
                        }

                        break;
                    case "submit":
                        #region Order Submission
                        var dropObj = (DropFourObj)Session[SessionKey.DropFourObj.ToString()];
                        var personalDetailsVM = new PersonalDetailsVM();

                        if (Session["PPID"].ToString2() == "N")
                        {
                            personalDetailsVM = dropObj.personalInformation;
                        }
                        else
                        {
                            personalDetailsVM = Util.FormPersonalDetailsandCustomer(personalDetailsVM);
                        }

                        personalDetailsVM = WebHelper.Instance.updatePersonalDetailsForOutright(personalDetailsVM, dropObj);

                        var resp = new RegistrationCreateResp();
                        var cRegistration = ConstructRegistration(personalDetailsVM.QueueNo);
                        var cCustomer = WebHelper.Instance.ConstructCustomer(personalDetailsVM);
                        var cAddress = WebHelper.Instance.ConstructRegAddress(personalDetailsVM);
                        ///var regStatus = WebHelper.Instance.ConstructRegStatus();
                        var regStatus = new RegStatus()
                        {
                            Active = true,
                            CreateDT = DateTime.Now,
                            StartDate = DateTime.Now,
                            LastAccessID = Util.SessionAccess.UserName,
                            StatusID = Util.GetIDByCode(RefType.Status, Properties.Settings.Default.Status_RegNew)
                        };
                        
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resp = proxy.RegistrationCreate(
                                        cRegistration,
                                        cCustomer,
                                        null,
                                        cAddress,
                                        null,
                                        regStatus,
                                        null, null, null, null
                                    );


                            if (resp.ID > 0)
                            {
                                #region Insert into LnkRegDetails
                                LnkRegDetailsReq objRegDetailsReqobj = new LnkRegDetailsReq();
                                Online.Registration.DAL.Models.LnkRegDetails objRegDetails = ConstructRegDetails(resp.ID, personalDetailsVM);
                                objRegDetailsReqobj.LnkDetails = objRegDetails;
                                proxy.SaveLnkRegistrationDetails(objRegDetailsReqobj);
                                #endregion

                                #region Insert into trnRegAttributes
                                var regAttribList = new List<RegAttributes>();
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCODE, ATT_Value = personalDetailsVM.OutletCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.OutletChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_OUTLETCHANNELID, ATT_Value = personalDetailsVM.OutletChannelId });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCODE, ATT_Value = personalDetailsVM.SalesCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESCHANNELID, ATT_Value = personalDetailsVM.SalesChannelId });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffCode))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCODE, ATT_Value = personalDetailsVM.SalesStaffCode });
                                if (!string.IsNullOrWhiteSpace(personalDetailsVM.SalesStaffChannelId))
                                    regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_KENAN_SALESSTAFFCHANNELID, ATT_Value = personalDetailsVM.SalesStaffChannelId });
                                regAttribList.Add(new RegAttributes() { RegID = resp.ID, ATT_Name = RegAttributes.ATTRIB_USER_DEVICE_AGENT, ATT_Value = Request.UserAgent });

                                proxy.SaveListRegAttributes(regAttribList);
                                #endregion

                                #region Insert into trnRegJustification
                                WebHelper.Instance.saveTrnRegJustification(resp.ID);
                                #endregion

                                #region Insert into trnRegAccessory
                                var serialNumberList = new Dictionary<int, string>();
                                WebHelper.Instance.SaveTrnRegAccessory(dropObj, resp.ID, ref serialNumberList);
                                #endregion

                                #region Send Payment Details to POS & Update Order Status to Ready For Payment
                                bool isPOSFileCreated = WebHelper.Instance.SendPaymentDetailsofRegID(resp.ID);;
                    
                                if (isPOSFileCreated == true)
                                {
                                    var readyForPayment = WebHelper.Instance.ConstructRegStatus(resp.ID, Properties.Settings.Default.Status_ReadyForPayment);
                                    proxy.RegistrationCancel(readyForPayment);
                                }
                                #endregion

                                WebHelper.Instance.ClearRegistrationSession();
                            }
                        }

                        using (var proxy = new UserServiceProxy())
                        {
                            proxy.SaveUserTransactionLog(Session["CurrentUserLogId"].ToInt(), (int)MobileRegType.Accessories, "Registration Done with RegId " + resp.ID, Session["KenanACNumber"].ToString2(), "");
                        }

                        redirectUrl = "MobileRegSuccess";
                        regId = resp.ID;
                        break;
                        #endregion
                        
                    default:
                        break;
                }

                return RedirectToAction(redirectUrl, new { regID = regId });

            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
        }

        public ActionResult PrintAccessoryDetails(int id)
        {
            ViewBag.GSTMark = Settings.Default.GSTMark;
            List<String> LabelReplace = new List<String>();
            LabelReplace.Add("GSTMark");
            ViewBag.GSTnotif = WebHelper.GenerateLabelString(Settings.Default.GSTNotif, LabelReplace);
            var regFormVM = new RegistrationFormVM();
            var modelImageIDs = new List<int>();
            var regMdlGrpModels = new List<RegMdlGrpModel>();

            //Joshi Added for DME Printer
            //if (HttpContext.Current.Request.UserAgent.ToLower().Contains("ipad"))
            if (Request.Headers["User-Agent"].ToLower().Contains("ipad"))
            {
                var UserDMEPrint = new UserDMEPrint();
                UserDMEPrint.RegID = id;
                UserDMEPrint.UserID = Util.SessionAccess.UserID;
                var status = Util.SaveUserDMEPrint(UserDMEPrint);
                Session["StatusDME"] = status;
            }
            using (var proxy = new RegistrationServiceProxy())
            {
                // Registration
                regFormVM.Registration = proxy.RegistrationGet(id);


                // Customer
                regFormVM.Customer = proxy.CustomerGet(new int[] { regFormVM.Registration.CustomerID }).SingleOrDefault();

                // Address
                var regAddrIDs = proxy.RegAddressFind(new AddressFind()
                {
                    Active = true,
                    Address = new Address()
                    {
                        RegID = id
                    }
                }).ToList();
                regFormVM.Addresses = proxy.RegAddressGet(regAddrIDs).ToList();

                // RegMdlGrpModels
                var regMdlGrpModelIDs = proxy.RegMdlGrpModelFind(new RegMdlGrpModelFind()
                {
                    RegMdlGrpModel = new RegMdlGrpModel()
                    {
                        RegID = id
                    }
                }).ToList();
                if (regMdlGrpModelIDs.Count() > 0)
                {
                    regMdlGrpModels = proxy.RegMdlGrpModelGet(regMdlGrpModelIDs).ToList();
                    modelImageIDs = regMdlGrpModels.Select(a => a.ModelImageID).ToList();
                }


            }

            using (var proxy = new CatalogServiceProxy())
            {

                // ModelGroupModel
                if (modelImageIDs.Count() > 0)
                {
                    var modelImages = proxy.BrandArticleModelImageGet(modelImageIDs).ToList();
                    var models = proxy.ModelGet(modelImages.Select(a => a.ModelID).ToList());

                    foreach (var regMdlGrpModel in regMdlGrpModels)
                    {
                        var modelID = modelImages.Where(a => a.ID == regMdlGrpModel.ModelImageID).SingleOrDefault().ModelID;
                        regFormVM.DeviceForms.Add(new DeviceForm()
                        {
                            Brand = Util.GetNameByID(RefType.Brand, models.Where(a => a.ID == modelID).SingleOrDefault().BrandID),
                            Model = models.Where(a => a.ID == modelID).SingleOrDefault().Name,
                            Price = regMdlGrpModel.Price.ToDecimal()
                        });


                    }

                    // Session["RegMobileReg_OrderSummary"] = ConstructOrderSummary();
                }

            }

            try
            {
                if (id.ToInt() > 0)
                {
                    OrderSummaryVM objSummaryVM = new OrderSummaryVM();
                    var regDetails = new Online.Registration.DAL.RegistrationDetails();
                    // var reg = new DAL.Models.Registration();
                    var regMdlGrpModel = new RegMdlGrpModel();

                    using (var proxy = new RegistrationServiceProxy())
                    {
                        regDetails = proxy.GetRegistrationFullDetails(id);
                    }
                    //reg = regDetails.Regs.SingleOrDefault();

                    regMdlGrpModel = regDetails.RegMdlGrpMdls.SingleOrDefault();


                    if (regMdlGrpModel != null && regMdlGrpModel.ID > 0)
                    {
                        objSummaryVM.SelectedModelImageID = regMdlGrpModel.ModelImageID.ToInt();
                        var brandArticleImage = new BrandArticle();
                        using (var proxy = new CatalogServiceProxy())
                        {
                            brandArticleImage = proxy.BrandArticleImageGet(objSummaryVM.SelectedModelImageID);
                        }
                        if (brandArticleImage != null)
                        {
                            Session["ColourName"] = brandArticleImage.ColourID > 0 ? Util.GetNameByID(RefType.Colour, brandArticleImage.ColourID) : string.Empty;
                        }
                    }

                }
                using (var proxyOrg = new OrganizationServiceProxy())
                {
                    if (!ReferenceEquals(regFormVM.Registration, null) && !ReferenceEquals(regFormVM.Registration.CenterOrgID, null) && regFormVM.Registration.CenterOrgID != 0)
                    {
                        regFormVM.Registration.CenterOrg = proxyOrg.OrganizationGet(new int[] { regFormVM.Registration.CenterOrgID }).FirstOrDefault();
                    }
                }

                WebHelper.Instance.MapRegAttributesByRegID(regFormVM, !ReferenceEquals(regFormVM.Registration, null) ? regFormVM.Registration.ID : id);

                regFormVM.RegAccessory = WebHelper.Instance.getAccessoryDetails(id);
            }
            catch (Exception ex)
            {
                WebHelper.Instance.LogExceptions(this.GetType(), ex);
                throw ex;
            }
            return View(regFormVM);
        }

        private string GetsimSerial(string msisdn)
        {
            string SIMSerial = string.Empty;
            Online.Registration.Web.SubscriberICService.retrieveSimDetlsResponse objRetrieveSimDetlsResponse = null;
            SimDetlsList objSimDetlsList = null;
            objRetrieveSimDetlsResponse = new Online.Registration.Web.SubscriberICService.retrieveSimDetlsResponse();
            using (var proxy = new retrieveServiceInfoProxy())
            {
                objRetrieveSimDetlsResponse = proxy.retreiveSIMDetls(msisdn);
            }

            if (objRetrieveSimDetlsResponse != null)
            {
                List<Online.Registration.Web.SubscriberICService.SimDetails> simDetlsFiltered = objRetrieveSimDetlsResponse.itemList.Where(c => c.InactiveDt == null || c.InactiveDt == string.Empty).ToList();
                if (!ReferenceEquals(simDetlsFiltered, null) && simDetlsFiltered.Count > 0)
                {
                    objSimDetlsList = new SimDetlsList(
                    simDetlsFiltered[0].ExternalID.ToString2(),
                    simDetlsFiltered[0].ExternalIDType.ToString2(),
                    simDetlsFiltered[0].InventoryTypeID.ToString2(),
                    simDetlsFiltered[0].InventoryDisplayValue,
                    simDetlsFiltered[0].Reason,
                    simDetlsFiltered[0].ActiveDt,
                    simDetlsFiltered[0].InactiveDt
                   );

                }
            }
            SIMSerial = objSimDetlsList.ExteranalId;
            return SIMSerial;
        }

        private DAL.Models.Registration ConstructRegistration(string queueNo = null)
        {
            var idCardTypeID = Session[SessionKey.IDCardTypeID.ToString()].ToInt();
            DAL.Models.Registration registration = new DAL.Models.Registration();
            var ageCheckStatusCode = Session["RegMobileReg_AgeCheckStatusCode"].ToString2();
            var ddmfCheckStatusCode = Session["RegMobileReg_DDMFCheckStatusCode"].ToString2();
            var addressCheckStatusCode = Session["RegMobileReg_AddressCheckStatusCode"].ToString2();
            var outstandingCheckStatusCode = Session["RegMobileReg_IsOutstandingCreditCheckStatusCode"].ToString2();
            var totalLineCheckStatusCode = Session["RegMobileReg_TotalLineCheckStatusCode"].ToString2();
            var principleLineCheckStatusCode = Session["RegMobileReg_PrincipalLineCheckStatusCode"].ToString2();
            var contractCheckStatusCode = Session["RegMobileReg_ContractCheckStatusCode"].ToString2();
            var biometricVerify = Session["RegMobileReg_BiometricVerify"].ToBool();
            var fxAccNo = Session[SessionKey.FxAccNo.ToString()].ToString2();
            var subscrciberNo = Session["SubscribeNo"].ToString2();
            var subscriberNoReset = Session["SubscribeNoResets"].ToString2();
            
            var isBlacklisted = true;
            registration.K2_Status = Session["RegK2_Status"].ToString2();
            registration.CenterOrgID = Util.SessionAccess.User.OrgID;
            registration.RegTypeID = MobileRegType.Accessories.ToInt();
            registration.Remarks = string.Empty;
            registration.QueueNo = queueNo.ToString2();
            //registration.MSISDN1 = Session[SessionKey.RegMobileReg_MobileNo.ToString()] == null ? "-" : ((List<string>)Session[SessionKey.RegMobileReg_MobileNo.ToString()])[0].ToString2();
            registration.MSISDN1 = "-";
            if (!ReferenceEquals(Request.Cookies["CookieUser"], null))
            {
                registration.SalesPerson = Request.Cookies["CookieUser"].Value;
            }
            registration.RFSalesDT = DateTime.Now;
            registration.AdditionalCharges = 0;
            //registration.KenanAccountNo = !ReferenceEquals(Session[SessionKey.KenanACNumber.ToString()], null) ? Session[SessionKey.KenanACNumber.ToString()].ToString() : "-";
            registration.KenanAccountNo = "-";
            registration.CreateDT = DateTime.Now;
            registration.LastAccessID = Util.SessionAccess.UserName;
            registration.IsVerified = idCardTypeID == 1 ? true : false;
            registration.BiometricVerify = idCardTypeID == 1 ? true : false;
            registration.InternalBlacklisted = Session["RegMobileReg_IsBlacklisted"] != null ? (bool)Session["RegMobileReg_IsBlacklisted"] : true;
            registration.ExternalBlacklisted = Session["RegMobileReg_IsBlacklisted"] != null ? (bool)Session["RegMobileReg_IsBlacklisted"] : true;
            registration.ApprovedBlacklistPerson = isBlacklisted ? Util.SessionAccess.UserName : null;
            registration.SignatureSVG = string.Empty;
            registration.CustomerPhoto = string.Empty;
            registration.SIMSerial = string.Empty;
            registration.AltCustomerPhoto = string.Empty;
            registration.Photo = string.Empty;
            registration.AgeCheckStatus = "AN";
            registration.AddressCheckStatus = "ADN";
            registration.DDMFCheckStatus = "DN";
            registration.OutStandingCheckStatus = "ON";
            registration.TotalLineCheckStatus = "TLN";
            registration.PrincipleLineCheckStatus = "PLN";
            registration.ContractCheckStatus = "CN";
            
            
            registration.PenaltyAmount = 0;
            registration.fxAcctNo = !string.IsNullOrEmpty(fxAccNo) ? fxAccNo : null;
            registration.fxSubscrNo = !string.IsNullOrEmpty(subscrciberNo) ? subscrciberNo : null;
            registration.fxSubScrNoResets = !string.IsNullOrEmpty(subscriberNoReset) ? subscriberNoReset : null;
            registration.IsK2 = false;
            
            
            registration.OfferID = 0;
            registration.K2Type = false;
            registration.Whitelist_Status = !ReferenceEquals(Session["MocandwhitelistSstatus"], null) ? Session["MocandwhitelistSstatus"].ToString() : "0";
            registration.MOC_Status = !ReferenceEquals(Session["MOCIDs"], null) ? Session["MOCIDs"].ToString() : string.Empty;
            
            registration.UserType = Session[SessionKey.PPID.ToString()].ToString() == null ? "N" : Session[SessionKey.PPID.ToString()].ToString();
            registration.PlanAdvance = 0;
            registration.DeviceAdvance = 0;
            registration.PlanDeposit = 0;
            registration.DeviceDeposit = 0;
            registration.OutstandingAcs = Session["FailedAcctIds"] == null ? string.Empty : Session["FailedAcctIds"].ToString();
            registration.PrePortInCheckStatus = !ReferenceEquals(Session["RegMobileReg_IsPortInStatusCode"], null) ? Convert.ToString(Session["RegMobileReg_IsPortInStatusCode"]) : "MPPF";
            registration.MNPServiceIdCheckStatus = !ReferenceEquals(Session["RegMobileReg_IsMNPServiceIdCheckStatusCode"], null) ? Convert.ToString(Session["RegMobileReg_IsMNPServiceIdCheckStatusCode"]) : "MSIF";
            registration.UOMCode = ConfigurationManager.AppSettings["UOMForDeviceSale"].ToString2();
            registration.MOC_Status = !ReferenceEquals(Session["MOCIDs"], null) ? Session["MOCIDs"].ToString() : "No";
            registration.Whitelist_Status = !ReferenceEquals(Session["MocandwhitelistSstatus"], null) ? Session["MocandwhitelistSstatus"].ToString() : "No";
            registration.OrganisationId = Util.SessionAccess.User.Org.OrganisationId;
            
            registration.Trn_Type = ConfigurationManager.AppSettings["Trn_Type"].ToString2();
            registration.IMPOSStatus = 0;
            registration.SimModelId = 0;
            registration.ArticleID = null;
            registration.InternationalRoaming = false;

            return registration;
        }

        private Online.Registration.DAL.Models.LnkRegDetails ConstructRegDetails(int regID, PersonalDetailsVM personalDetailsVM) 
        {
            Online.Registration.DAL.Models.LnkRegDetails regDetails = new Online.Registration.DAL.Models.LnkRegDetails();
            regDetails.RegId = regID;
            regDetails.CreatedDate = DateTime.Now;
            regDetails.IsSuppNewAc = false;
            regDetails.UserName = Util.SessionAccess.UserName;
            regDetails.AccountType = Session["AccountType"].ToString2();
            regDetails.PdpaVersion = personalDetailsVM.PDPDVersion.ToString2();

            string[] Result = Util.GetMocAndLibStatus(Session[SessionKey.ExternalID.ToString()].ToString2(), 0);
            regDetails.Liberlization_Status = Result[0];
            regDetails.MOC_Status = Result[1];
            regDetails.ContractCheckCount = Session["AccountswithContractCount"].ToInt();

            return regDetails;
        }
    }
}
