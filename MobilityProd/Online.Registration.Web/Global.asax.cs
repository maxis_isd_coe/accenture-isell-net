﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Online.Registration.Web.CustomModelBinders;
using System.Web.Security;
using Online.Registration.Web.Helper;
using System.Web.SessionState;
using System.Web.Optimization;
using System.Text.RegularExpressions;
using System.IO;
using log4net;

namespace Online.Registration.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("SalesCreate", // Route name
                "{controller}/{action}/{regID}", // URL with parameters
new { controller = "Sales", action = "Create" } // Parameter defaults
);

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Account", action = "LogOn", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            RegisterBundles(BundleTable.Bundles);

            ModelBinders.Binders.Add(typeof(DateTime), new IceAgeDateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new IceAgeDateTimeModelBinder());

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            ModelMetadataProviders.Current = new DataAnnotationsModelMetadataProvider();

            Online.Registration.Web.ContentRepository.MasterDataCache.Instance.InitializeCache();
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            bundles.Add(new ScriptBundle("~/Bundles/BaseScripts").Include(
                             "~/Scripts/js/modernizr-2.5.3-min.js",
                             "~/Scripts/jquery-1.8.3.min.js",
                             "~/Scripts/jquery-ui-1.8.21.min.js",
                             "~/scripts/js/ui/jquery.ui.core.js",
                             "~/scripts/js/ui/jquery.ui.widget.js",
                             "~/scripts/js/ui/jquery.ui.accordion.js",
                             "~/scripts/js/responsivegridsystem.js",
                             "~/scripts/js/helper-plugins/jquery.mousewheel.min.js",
                             "~/scripts/js/helper-plugins/jquery.touchSwipe.min.js",
                             "~/scripts/js/helper-plugins/jquery.transit.min.js",
                             "~/scripts/js/helper-plugins/jquery.ba-throttle-debounce.min.js",
                             "~/scripts/js/tabcontent.js",
                             "~/scripts/js/jquery.horizontalNav.js",
                             "~/scripts/js/jquery.colorbox.js",
                             "~/Scripts/jquery.validate.js",
                             "~/Scripts/jquery.validate.unobtrusive.min.js",
                             "~/Scripts/jquery.unobtrusive-ajax.js",
                             "~/Scripts/ZeroClipboard.js",
                             "~/Scripts/jquery.blockUI.js",
                             "~/Scripts/jquery-ui-timepicker-addon.js",
                             "~/Scripts/jSignature.min.js",
                             "~/Scripts/flashcanvas.js",
                             "~/Scripts/jquery.ui.rcarousel.js",
                             "~/Scripts/jquery.tabletojson.min.js",
                             "~/Scripts/scripts.js",
                             "~/Scripts/telerik.common.min.js",
                             "~/Scripts/telerik.list.min.js",
                             "~/Scripts/telerik.window.min.js",
                             "~/Scripts/telerik.draganddrop.min.js",
                             "~/Scripts/telerik.grid.min.js",
                             "~/Scripts/telerik.calendar.min.js",
                             "~/Scripts/telerik.datepicker.min.js",
                             "~/Scripts/telerik.grid.resizing.min.js",
                             "~/Scripts/core.js"
         ));

            bundles.Add(new ScriptBundle("~/bundles/LogonScripts").Include(
                     "~/Scripts/modernizr-2.5.3.js",
                     "~/Scripts/jquery-1.8.3.min.js",
                     "~/Scripts/jquery-ui-1.8.21.min.js",
                     "~/Scripts/jquery.validate.js",
                     "~/Scripts/jquery.validate.unobtrusive.min.js",
                     "~/Scripts/jquery.unobtrusive-ajax.js",
                     "~/Scripts/telerik.common.min.js",
                     "~/Scripts/telerik.window.min.js"
                     ));


            bundles.Add(new StyleImagePathBundle("~/Bundles/LogonStyles").Include(
            "~/Content/css/html5reset.css",
            "~/Content/css/responsivegridsystem.css",
            "~/Content/style.css",
            "~/Content/css/1024.css",
            "~/Content/css/768.css",
            "~/Content/2012.2.607/telerik.common.min.css",
            "~/Content/2012.2.607/telerik.office2010silver.min.css"
            ));

            bundles.Add(new StyleImagePathBundle("~/Bundles/BaseStyles").Include(
                 "~/Content/style.css",
                 "~/Content/css/html5reset.css",
                 "~/Content/css/responsivegridsystem.css",
                 "~/Content/css/col.css",
                 "~/Content/css/4cols.css",
                 "~/Content/css/tabcontent.css",
                 "~/Content/css/owl.carousel.css",
                 "~/Content/css/owl.theme.css",
                 "~/Content/css/1024.css",
                 "~/Content/css/768.css",
                 "~/Content/css/colorbox.css",
                 "~/Content/css/jquery-ui/jquery.ui.core.css",
                 "~/Content/css/jquery-ui/jquery.ui.accordion.css",
                 "~/Content/2012.2.607/telerik.common.min.css",
                 "~/Content/2012.2.607/telerik.office2010silver.min.css"
             ));

            BundleTable.EnableOptimizations = false;
        }

        #region SUTAN ADDED CODE 8TH MARCH 2013
        /// <summary>
        /// Handles the PreRequestHandlerExecute event of the Application control. Following will redirect the user to login page
        /// in case where FormsAuthentication cookie is active but session expired due to some unexpected reason.
        /// </summary>
        /// <Author>
        /// Sutan Dan
        /// </Author>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            if (Context.Handler is IRequiresSessionState || Context.Handler is IReadOnlySessionState)
            {
                HttpCookie authenticationCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                if (!ReferenceEquals(authenticationCookie, null))
                {
                    FormsAuthenticationTicket authenticationTicket = FormsAuthentication.Decrypt(authenticationCookie.Value);
                    if (!authenticationTicket.Expired)
                    {
                        if (ReferenceEquals(Util.SessionAccess, null))
                        {
                            FormsAuthentication.SignOut();
                            /*While giving build please comment the below line*/
                            Response.Redirect(FormsAuthentication.LoginUrl, true);
                            /*While giving build please uncomment the below line*/
                            Response.Redirect("../", true);
                            return;
                        }
                    }
                }
            }
        }
        #endregion SUTAN ADDED CODE 8TH MARCH 2013

        #region Added Application_Error event for logging in a separate log file
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MvcApplication));

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            Logger.Fatal(" Requested url: " + Request.RawUrl);
            WebHelper.Instance.LogAppExceptions(this.GetType(), exception);

            // Response.Redirect("~/Account/LogOn");
        }



        #endregion
    }

    public class SessionAuthenticationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool isSessionExist = !string.IsNullOrEmpty(Util.SessionAccess.UserName);
			if (filterContext.ActionDescriptor.ActionName == "TacAuditLog")
				isSessionExist = true;

            if (!isSessionExist)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "LogOut", controller = "Account" }));
            }
        }
    }


    #region [Style Relative Image Path]

    static class HttpContextExtensions
    {
        public static string RelativeFromAbsolutePath(this HttpContextBase context, string path)
        {
            var request = context.Request;
            var applicationPath = request.PhysicalApplicationPath;
            var virtualDir = request.ApplicationPath;
            virtualDir = virtualDir == "/" ? virtualDir : (virtualDir + "/");
            return path.Replace(applicationPath, virtualDir).Replace(@"\", "/");
        }
    }

    class StyleRelativePathTransform : IBundleTransform
    {
        public StyleRelativePathTransform()
        {
        }

        public void Process(BundleContext context, BundleResponse response)
        {
            response.Content = String.Empty;

            Regex pattern = new Regex(@"url\s*\(\s*([""']?)([^:)]+)\1\s*\)", RegexOptions.IgnoreCase);
            // open each of the files
            foreach (FileInfo cssFileInfo in response.Files)
            {
                if (cssFileInfo.Exists)
                {
                    // apply the RegEx to the file (to change relative paths)
                    string contents = File.ReadAllText(cssFileInfo.FullName);
                    MatchCollection matches = pattern.Matches(contents);
                    // Ignore the file if no match 
                    if (matches.Count > 0)
                    {
                        string cssFilePath = cssFileInfo.DirectoryName;
                        string cssVirtualPath = context.HttpContext.RelativeFromAbsolutePath(cssFilePath);
                        foreach (Match match in matches)
                        {
                            // this is a path that is relative to the CSS file
                            string relativeToCSS = match.Groups[2].Value;
                            // combine the relative path to the cssAbsolute
                            string absoluteToUrl = relativeToCSS.Split('?')[0];

                            try
                            {
                                absoluteToUrl = Path.GetFullPath(Path.Combine(cssFilePath, relativeToCSS.Split('?')[0]));
                            }
                            catch (Exception)
                            {

                            }

                            // make this server relative
                            string serverRelativeUrl = context.HttpContext.RelativeFromAbsolutePath(absoluteToUrl) + (relativeToCSS.Split('?').Length > 1 ? '?' + relativeToCSS.Split('?')[1] : "");

                            string quote = match.Groups[1].Value;
                            string replace = String.Format("url({0}{1}{0})", quote, serverRelativeUrl);
                            contents = contents.Replace(match.Groups[0].Value, replace);
                        }
                    }
                    // copy the result into the response.
                    response.Content = String.Format("{0}\r\n{1}", response.Content, contents);
                }
            }
        }
    }

    public class StyleImagePathBundle : Bundle
    {
        public StyleImagePathBundle(string virtualPath)
            : base(virtualPath)
        {
            base.Transforms.Add(new StyleRelativePathTransform());
            base.Transforms.Add(new CssMinify());
        }

        public StyleImagePathBundle(string virtualPath, string cdnPath)
            : base(virtualPath, cdnPath)
        {
            base.Transforms.Add(new StyleRelativePathTransform());
            base.Transforms.Add(new CssMinify());
        }
    }
    #endregion
}
