﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Collections.Specialized;

using Online.Registration.Web.Helper;
using Online.Registration.Web.UserSvc; 

using log4net;

namespace Online.Registration.Web.Providers
{
    public class CustomRoleProvider: RoleProvider
    {
        private string _ApplicationName;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CustomRoleProvider));

        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (string.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            Logger.InfoFormat("Initialize({0}) called!", name);

            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "CustomRoleProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Custom Role Provider");
            }

            base.Initialize(name, config);

            _ApplicationName = GetConfigValue(config["applicationName"],
                          System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);

            Logger.InfoFormat("Initialize({0}) returned!", name);
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get { return _ApplicationName; }
            set { _ApplicationName = value; }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            Logger.InfoFormat("FindUsersInRole({0}, {1}) called!", usernameToMatch, roleName);

            string[] users = null;

            if (!string.IsNullOrEmpty(roleName))
            {
                try
                {
                    using (var proxy = new UserServiceProxy())
                    {
                        var req = new UsersInRoleFindReq() { Active = true, RoleCode = roleName, UserName = usernameToMatch };
                        users = proxy.UsersInRoleFind(req).Users;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(string.Format("FindUsersInRole({0}, {1}):{2}!", usernameToMatch, roleName, ex.Message), ex);
                    throw;
                }
            }

            int cnt = users == null ? 0 : users.Count();
            Logger.InfoFormat("FindUsersInRole({0}, {1}):{2} returned!", usernameToMatch, roleName, cnt);
            return users;
        }

        public override string[] GetAllRoles()
        {
            Logger.Info("GetAllRoles() called!");

            List<string> roles = new List<string>();

            try
            {
                using (var proxy = new UserServiceProxy())
                {
                    var resp = proxy.UserRoleFind(new DAL.Models.UserRole(), true);

                    if (resp != null && resp.Count() > 0)
                    {
                        var roleList = proxy.UserRoleGet( resp) ;
                        if (roleList != null && roleList.Count() > 0)
                        {
                            foreach (var item in roleList) { roles.Add(item.Code); }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("GetAllRoles():{1}!", ex.Message), ex);
                return roles.ToArray();
            }

            Logger.InfoFormat("GetAllRoles():{0} returned!", roles.Count);
            return roles.ToArray();
        }

        public override string[] GetRolesForUser(string username)
        {
            Logger.InfoFormat("GetRolesForUser({0}) called!", username);

            string[] roles = null;

            if (!string.IsNullOrEmpty(username))
            {
                try
                {
                    using (var proxy = new UserServiceProxy())
                    {
                        var req = new RolesForUserGetReq() { Active = true, UserName = username };
                        roles = proxy.GetRolesForUser(req).Roles;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(string.Format("GetRolesForUser({0}):{1}!", username, ex.Message), ex);
                    
                }
            }

            int cnt = roles == null ? 0 : roles.Count();
            Logger.InfoFormat("GetRolesForUser({0}):{1} returned!", username, cnt);
            return roles;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            return FindUsersInRole(roleName, string.Empty);
        }
        
        public override bool IsUserInRole(string username, string roleName)
        {
            var resp = FindUsersInRole(roleName, username);
            return (resp != null && resp.Count() > 0);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            Logger.InfoFormat("RoleExists({0}) called!", roleName);
            bool result = false;
            try
            {
                using (var proxy = new UserServiceProxy())
                {
                    var role = new DAL.Models.UserRole() { Active = true, Code = roleName };
                    var resp = proxy.UserRoleFind(new DAL.Models.UserRole(), true);
                    if (resp != null)
                        result = (resp.Count() > 0);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ChangePassword({0}):{1}!", roleName, ex.Message), ex);
                return result;
            }

            Logger.InfoFormat("ChangePassword({0}):{1} returned!", roleName, result);
            return result;
        }
    }
}