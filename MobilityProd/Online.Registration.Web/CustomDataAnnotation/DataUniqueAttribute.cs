﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.CustomDataAnnotation
{
    /// <summary>
    /// Validation annotation to check value in same object instance
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class DataUniqueAttribute : ValidationAttribute, IClientValidatable
    {
        private const string defaultErrorMessage = "{0} and {1} is the same.";
        private string groupName;
        private string errMsg;

        public DataUniqueAttribute(string _groupName):base(defaultErrorMessage)
        {
            groupName = _groupName;
        }

        public DataUniqueAttribute(string _groupName,string _errMsg):base(_errMsg)
        {
            groupName = _groupName;
            errMsg = _errMsg;
        }

        public string GroupName{
            get{
                return groupName;
            }
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success;
            if(string.IsNullOrEmpty(validationContext.MemberName))
            {
                //MemberName is null in custom attribute, set to continue validation
                validationContext.MemberName = validationContext.ObjectType.GetProperties()
                    .Where(p => p.GetCustomAttributes(false)
                        .OfType<DisplayAttribute>()
                        .Any(a => a.Name == validationContext.DisplayName))
                        .Select(p => p.Name).FirstOrDefault();
            }
            //it is valid when check among all property for same group in this context
            var currentProperty = validationContext.MemberName;
            var properties = validationContext.ObjectInstance.GetType().GetProperties()
                .Where(n => 
                    n.GetCustomAttributes(this.GetType(), true).Length > 0 && 
                    (n.GetCustomAttributes(this.GetType(),true).First() as DataUniqueAttribute).GroupName==GroupName &&
                    n.Name!=currentProperty
                ).ToList();
            if(properties.Count>0){
                foreach(var p in properties){
                    var targetDisplayNameAttr=p.GetCustomAttributes(typeof(DisplayAttribute),true).FirstOrDefault();
                    var targetDisplayName = targetDisplayNameAttr != null ? (targetDisplayNameAttr as DisplayAttribute).Name : p.Name;
                    var targetValue=p.GetValue(validationContext.ObjectInstance,null);
                    if (value != null && targetValue !=null && targetValue.Equals(value))
                        return new ValidationResult(string.Format(ErrorMessageString, validationContext.DisplayName, targetDisplayName), new[] { currentProperty });
                }
            }
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var currentMsg = this.ErrorMessage;
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = currentMsg,
                ValidationType = "dataunique",
            };
            rule.ValidationParameters.Add("groupname", groupName);
            rule.ValidationParameters.Add("displayname", metadata.GetDisplayName());
            yield return rule;
        }
    }
}