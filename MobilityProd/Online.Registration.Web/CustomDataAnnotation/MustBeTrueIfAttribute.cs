﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.CustomDataAnnotation
{
    /// <summary>
    /// Must be true value check when dependet condition is match
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MustBeTrueIfAttribute : ValidationAttribute, IClientValidatable
    {
        private string dependentName;
        private object dependentValue;
        public MustBeTrueIfAttribute(string _dependentName, object _dependentValue)
        {
            dependentName = _dependentName;
            dependentValue = _dependentValue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value.GetType() != typeof(bool))
                throw new Exception("This validator only can use on bool value type");

            if (ConditionMatch(validationContext.ObjectInstance) && (bool)value != true)
                return new ValidationResult(ErrorMessage);
            return ValidationResult.Success;
        }

        private bool ConditionMatch(object instance)
        {
            var pro = instance.GetType().GetProperty(dependentName);
            if (pro == null)
                throw new Exception(string.Format("Property {0} cannot found.",dependentName));
            var actualValue = pro.GetValue(instance, null);
            return dependentValue == actualValue;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = this.ErrorMessage,
                ValidationType = "mustbetrueif",
            };
            rule.ValidationParameters.Add("dependentname", dependentName);
            rule.ValidationParameters.Add("dependentvalue", dependentValue);
            yield return rule;
        }
    }
}