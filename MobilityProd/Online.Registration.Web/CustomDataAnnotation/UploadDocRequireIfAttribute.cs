﻿using Online.Registration.Web.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Online.Registration.Web.CustomDataAnnotation
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true,Inherited=true)]
    public class UploadDocRequireIfAttribute : ValidationAttribute
    {
        private string dependentsPropertyName;
        private string dependentPropertyName;
        private object dependentValue;
        private List<string> requirePropertyLst = new List<string>();

        /// <summary>
        /// RequireIf validator for Upload doc
        /// </summary>
        /// <param name="_dependentsPropertyName">dependents propertis on object</param>
        /// <param name="_requireProperties">require properties on upload doc object, if nothing to put will default add BackIdFile and FrontIdFile</param>
        public UploadDocRequireIfAttribute(string _dependentsPropertyName, params string[] _requireProperties)
        {
            dependentsPropertyName = _dependentsPropertyName;
            requirePropertyLst.AddRange(_requireProperties);
            //add default
            if(requirePropertyLst.Count()==0){
                requirePropertyLst.Add("BackIdFile");
                requirePropertyLst.Add("FrontIdFile");
            }
        }
        /// <summary>
        /// RequireIf validator for upload doc
        /// </summary>
        /// <param name="_dependentPropertyName">first dependent property on object</param>
        /// <param name="value">value to compare</param>
        /// <param name="_requireProperties">require properties on upload doc object, if nothing to put will default add BackIdFile and FrontIdFile</param>
        public UploadDocRequireIfAttribute(string _dependentPropertyName, object value, params string[] _requireProperties)
        {
            dependentPropertyName = _dependentPropertyName;
            dependentValue = value;
            requirePropertyLst.AddRange(_requireProperties);
            if (requirePropertyLst.Count() == 0)
            {
                requirePropertyLst.Add("BackIdFile");
                requirePropertyLst.Add("FrontIdFile");
            }
        }
        //Required if you want to use this attribute multiple times
        private object _typeId = new object();
        public override object TypeId
        {
            get { return _typeId; }
        }

        protected override ValidationResult IsValid(object item, ValidationContext validationContext)
        {
            //scan dependent list
            //will be fail if any one not match, and hasvalidproperty fail
            if (HasValidProperties(item))
                return ValidationResult.Success;
            UploadDocRequireIfParam[] dependents = GetDependents(validationContext.ObjectInstance);
            var errMsgList = new List<string>();
            foreach(var dependent in dependents){
                var pro = validationContext.ObjectInstance.GetType().GetProperty(dependent.DependProperty);
                var value = pro.GetValue(validationContext.ObjectInstance, null);
                if (value.Equals(dependent.DependValue))
                    errMsgList.Add(dependent.ErrorMsg);
            }
            if (errMsgList.Count > 0)
                return new ValidationResult(string.Format(ErrorMessage, string.Join(",", errMsgList)));
            return ValidationResult.Success;
        }

        private UploadDocRequireIfParam[] GetDependents(object obj)
        {
            if(!string.IsNullOrEmpty(dependentsPropertyName)){
                var dependentsProperty = obj.GetType().GetProperty(dependentsPropertyName);
                if (dependentsProperty == null)
                    throw new Exception(string.Format("Cannot find property {0}", dependentsPropertyName));
                return (UploadDocRequireIfParam[])dependentsProperty.GetValue(obj, null);
            }
            if(!string.IsNullOrEmpty(dependentPropertyName)){
                return new[] {new UploadDocRequireIfParam(dependentPropertyName,dependentValue,"")};
            }
            throw new Exception("Cannot get dependents info");
        }

        private bool HasValidProperties(object obj)
        {
            if (obj == null) return false;
            //all object value must be exist or not null
            if (requirePropertyLst.Count == 0)
                throw new Exception("No require property define");
            foreach(var pro in requirePropertyLst){
                var property = obj.GetType().GetProperty(pro);
                if (property == null)
                    throw new Exception(string.Format("Cannot find property {0} in upload doc object",pro));
                var value = property.GetValue(obj, null);
                if (value==null || string.IsNullOrEmpty(value.ToString()))
                    return false;
            }
            return true;
        }
    }
}