﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Online.Registration.Web.CustomExtension;

namespace Online.Registration.Web.CustomDataAnnotation
{
    /// <summary>
    /// Validate the sim serial in kenan by sim model type
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ValidSimSerialAttribute : ValidationAttribute
    {
        private string _simTypeCodeName;
        public ValidSimSerialAttribute(string simTypeCodeName,string errorMsg):base(errorMsg)
        {
            _simTypeCodeName = simTypeCodeName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //only check when got value and simTypeCode Name got value
            //other validation will handle require check
            if (value == null)
                return ValidationResult.Success;
            var simTypeCodeProp = validationContext.ObjectInstance.GetType().GetProperty(_simTypeCodeName);
            if (simTypeCodeProp == null)
                throw new Exception(string.Format("Cannot find property {0} in object",_simTypeCodeName));
            var simTypeCode = (string)simTypeCodeProp.GetValue(validationContext.ObjectInstance, null);
            if (string.IsNullOrEmpty(simTypeCode))
                return ValidationResult.Success;
            string simSerial = value.ToString();
            if (!simSerial.IsValidSimSerial(simTypeCode))
                return new ValidationResult(string.Format(ErrorMessageString, validationContext.DisplayName));
            return ValidationResult.Success;
        }
    }
}