﻿using Online.Registration.Web.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.CustomDataAnnotation
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ValidIdNumAttribute : ValidationAttribute, IClientValidatable
    {
        private string idTypePropertyName;

        private const string defaultErrorMsg = "Invalid {0} format.";
        private const string maxLengthErrorMsg = "{0} is too long";
        private const string minLengthErrorMsg = "{0} is too short";
        public ValidIdNumAttribute(string _idTypePropertyName):base(defaultErrorMsg)
        {
            idTypePropertyName = _idTypePropertyName;
        }

        public ValidIdNumAttribute(string _idTypePropertyName,string _errorMsg):base(_errorMsg)
        {
            idTypePropertyName = _idTypePropertyName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //Will not check when value null
            if (value == null) return ValidationResult.Success;
            var idTypeProperty = validationContext.ObjectInstance.GetType().GetProperty(idTypePropertyName);
            if (idTypeProperty == null)
                throw new Exception(string.Format("Cannot find {0} in object",idTypePropertyName));
            var idTypeValue = idTypeProperty.GetValue(validationContext.ObjectInstance, null);
            //Only support idNum format check for IC at this moment
            var success = false;
            var errorMsg = defaultErrorMsg;
            switch(int.Parse(idTypeValue.ToString())){
                case 1:
                    if (value.ToString().Length > 12)
                        errorMsg = maxLengthErrorMsg;
                    else if (value.ToString().Length < 12)
                        errorMsg = minLengthErrorMsg;
                    success = Util.isValidNewNRIC(value.ToString());//this thing cover length check, so ours side just ignore and do error msg handling
                    break;
                default:
                    //Not support type default to success
                    success=true;
                    break;
            }
            if(!success)
                return new ValidationResult(string.Format(errorMsg, validationContext.DisplayName));
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var currentMsg = this.ErrorMessage;
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = string.Format(ErrorMessageString, metadata.GetDisplayName()),
                ValidationType = "valididnum"
            };
            rule.ValidationParameters.Add("idtypeid", idTypePropertyName);
            rule.ValidationParameters.Add("maxlength", string.Format(maxLengthErrorMsg,metadata.GetDisplayName()));
            rule.ValidationParameters.Add("minlength", string.Format(minLengthErrorMsg, metadata.GetDisplayName()));
            yield return rule;
        }
    }
}