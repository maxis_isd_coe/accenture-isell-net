﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.CustomDataAnnotation
{
    [Serializable()]
    public class UploadDocRequireIfParam
    {
        public string DependProperty { get; set; }
        public object DependValue { get; set; }
        public string ErrorMsg { get; set; }
        public UploadDocRequireIfParam(string _dependProperty,object _dependValue,string _errorMsg)
        {
            DependProperty = _dependProperty;
            DependValue = _dependValue;
            ErrorMsg = _errorMsg;
        }
        public UploadDocRequireIfParam()
        {

        }
    }
}