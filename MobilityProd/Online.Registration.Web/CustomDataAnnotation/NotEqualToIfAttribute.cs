﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.CustomDataAnnotation
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class NotEqualToIfAttribute : NotEqualToAttribute
    {
        private string dependentName;
        private object dependentValue;
        public NotEqualToIfAttribute(string otherProperty, string _dependentName, object _dependentValue):base(otherProperty)
        {
            dependentName = _dependentName;
            dependentValue = _dependentValue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if(ConditionMatch(validationContext.ObjectInstance))
                return base.IsValid(value, validationContext);
            return ValidationResult.Success;
        }

        private bool ConditionMatch(object instance)
        {
            var pro = instance.GetType().GetProperty(dependentName);
            if (pro == null)
                throw new Exception(string.Format("Property {0} cannot found.", dependentName));
            var actualValue = pro.GetValue(instance, null);
            return dependentValue == actualValue;
        }

        public override IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "notequaltoif",
            };
            rule.ValidationParameters.Add("other", otherProperty);
            rule.ValidationParameters.Add("dependentname", dependentName);
            rule.ValidationParameters.Add("dependentvalue", dependentValue);
            yield return rule;
        }
    }
}