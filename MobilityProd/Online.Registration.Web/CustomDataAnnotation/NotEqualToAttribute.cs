﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.CustomDataAnnotation
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class NotEqualToAttribute : ValidationAttribute, IClientValidatable
    {
        private const string defaultErrorMessage = "{0} cannot be the same as {1}.";

        protected string otherProperty;

        public NotEqualToAttribute(string otherProperty)
            : base(defaultErrorMessage)
        {
            if (string.IsNullOrEmpty(otherProperty))
            {
                throw new ArgumentNullException("otherProperty");
            }

            this.otherProperty = otherProperty;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(ErrorMessageString, name, otherProperty);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                PropertyInfo otherPropertyInfo = validationContext.ObjectInstance.GetType().GetProperty(otherProperty);

                if (otherPropertyInfo == null)
                {
                    return new ValidationResult(string.Format("Property '{0}' is undefined.", otherProperty));
                }

                var otherPropertyValue = otherPropertyInfo.GetValue(validationContext.ObjectInstance, null);

                if (otherPropertyValue != null && !string.IsNullOrEmpty(otherPropertyValue.ToString()))
                {
                    if (value.Equals(otherPropertyValue))
                    {
                        return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
                    }
                }
            }

            return ValidationResult.Success;
        }

        public virtual IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var currentMsg = this.ErrorMessage;
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = String.IsNullOrEmpty(currentMsg) ? FormatErrorMessage(metadata.GetDisplayName()) : currentMsg,
                ValidationType = "notequalto",
            };
            rule.ValidationParameters.Add("other", otherProperty);
            yield return rule;
        }
    }
}