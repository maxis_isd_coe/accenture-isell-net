﻿using Online.Registration.Web.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Online.Registration.Web.CustomDataAnnotation
{
    /// <summary>
    /// Check on session object to validate if it is sv or ccc
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class RequiredIfSvCcLoginAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value==null && !IsRoleAllow())
                return false;
            return true;
        }

        protected virtual bool IsRoleAllow()
        {
            return AccessUtil.IsCCC() || AccessUtil.IsSupervisor(); 
        }
    }
}