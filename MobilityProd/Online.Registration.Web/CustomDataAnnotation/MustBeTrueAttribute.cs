﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.CustomDataAnnotation
{
    /// <summary>
    /// Validation attribute that demands that a boolean value must be true.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class MustBeTrueAttribute : ValidationAttribute, IClientValidatable
    {
        public override bool IsValid(object value)
        {
            return value != null && value is bool && (bool)value;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var currentMsg = this.ErrorMessage;
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = String.IsNullOrEmpty(currentMsg)?string.Format("{0} must be true", metadata.GetDisplayName()):currentMsg,
                ValidationType = "mustbetrue",
            };
            yield return rule;
        }
    }
}