﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Online.Registration.Web.CustomDataAnnotation
{
    public class CustomStringLengthAttribute : ValidationAttribute, IClientValidatable
    {
        private string MaxErrorMessage;
        private string MinErrorMessage;
        private int maxLength;
        private int minLength;
        public CustomStringLengthAttribute(int maximumLength, int minimumLength, string maxErrorMessage, string minErrorMessage)
        {
            minLength = minimumLength;
            maxLength = maximumLength;
            MaxErrorMessage = maxErrorMessage;
            MinErrorMessage = minErrorMessage;
        }

        protected override System.ComponentModel.DataAnnotations.ValidationResult IsValid(object value, System.ComponentModel.DataAnnotations.ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            if (value.ToString().Length < minLength)
            {
                return new ValidationResult(MinErrorMessage);
            }
            else if (value.ToString().Length > maxLength)
            {
                return new ValidationResult(MaxErrorMessage);
            }

            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var currentMsg = this.ErrorMessage;
            var rule = new ModelClientValidationRule
            {
                ErrorMessage="invalid format",
                ValidationType = "customstringlength",
            };
            rule.ValidationParameters.Add("maxlength", maxLength);
            rule.ValidationParameters.Add("minlength", minLength);
            rule.ValidationParameters.Add("minlengthmsg", string.Format(MinErrorMessage,metadata.GetDisplayName()));
            rule.ValidationParameters.Add("maxlengthmsg", string.Format(MaxErrorMessage, metadata.GetDisplayName()));
            yield return rule;
        }
    }
}