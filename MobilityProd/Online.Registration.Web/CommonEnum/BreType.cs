﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.CommonEnum
{
    [Serializable]
    [Flags]
    public enum BreType
    {
        AgeCheck=1,
        OutstandingCreditCheck=2,
        AddressCheck=3,
        ContractCheck=4,
        DDMFCheck=5,
        TotalLineCheck=6,
        Biometric=7,
        PrincipleLineCheck=8,
        WriteoffCheck=9
    }
}