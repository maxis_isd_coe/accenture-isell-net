﻿function validateIdCardNo(idCardNo, idCardTypeID, passportTypeId, oldIcCardTypeId, newIcCardTypeId) {
    if (idCardTypeID == "" && idCardNo == "") {
        alert("The ID Type * field is required.\nThe ID Card No * field is required.");
        return false;
    } else if (idCardTypeID == "") {
        alert("The ID Type * field is required.");
        return false;
    } else if (idCardNo == "") {
        alert("The ID Card No * field is required.");
        return false;
    } else {
        if (passportTypeId == idCardTypeID) {
            var tCode = $('#Customer_IDCardNo').val();
            if (/[^a-zA-Z0-9]/.test(tCode)) {
                alert('Passport must be alphanumeric');
                return false;
            }
        }
        if (oldIcCardTypeId == idCardTypeID) {

        }
        if (newIcCardTypeId == idCardTypeID) {
            if (idCardNo.length != 12) {
                alert('NRIC Validation Error: New NRIC length should be 12 characters in length.');
                return false;
            }
            if (isNaN(idCardNo)) {
                alert('NRIC Validation Error: New NRIC should be numbers only');
                return false;
            }
            if (!validateNricDate(idCardNo)) {
                alert('NRIC Validation Error: Characters 3 and 4 should represent Month, Date is not valid, Characters 5 and 6 should represent Day.');
                return false;
            }
        }

    }
    return true;
}

function validateNricDate(nric) {
    var year = nric.substr(0, 2);
    var month = nric.substr(2, 2);
    var day = nric.substr(4, 2);
    var currentYear = (new Date).getFullYear();
    if (currentYear - ("19" + year) <= 100) {
        year = "19" + year;
    } else {
        year = "20" + year;
    }
    return isValidDate(year, month, day);
}

function isValidDate(year, month, day) {
    if (month < 1 || month > 12)
        return false;
    else if (day < 1 || day > 31)
        return false;
    else if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31)
        return false;
    else if (month == 2) {
        var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        if (day > 29 || (day == 29 && !isleap))
            return false;
    }
    return true;
}

$(function () {
    $('table[data-enable-paging=true]').each(function () {
        var pageSize = 10;
        if ($(this).attr('data-page-size') != undefined || $(this).attr('data-page-size') != null) {
            pageSize = $(this).attr('data-page-size');
        }
        $(this).tablePagination({ rowsPerPage: pageSize });
    });
});

function logError(error) {
    if ($.browser.mozilla || $.browser.chrome) {
        try {
            console.error(error.message);
        } catch (e) { }
    }
}

var dataFormatter = function () {
    var aging = function (startDate, endDate) {
        try {
            if (endDate == null) {
                endDate = new Date();
            }
            return DateDiff.inDays(startDate, endDate);
        } catch (e) {
            logError(e);
        }
        return "";
    };
    var scheduledDateWithTimeSlot = function (scheduledDate, timeSlot) {
        try {
            timeSlot = timeSlot == null ? "" : timeSlot;
            if (scheduledDate != undefined) {
                return date(scheduledDate) + " " + timeSlot;
            }
        } catch (e) {
            logError(e);
        }
        return "";
    };
    var packages = function (data) {
        try {
            if (data != undefined) {
                return data.replace(/##/g, '<br /><br />');
            }
        } catch (e) {
            logError(e);
        }
        return "";
    };
    var dateTime = function (data) {
        if (data != undefined) {
            return data.toString('dd MMM yyyy hh:mm tt');
        }
        return "";
    };
    var date = function (data) {
        if (data != undefined) {
            return data.toString('dd MMM yyyy');
        }
        return "";
    };
    var provider = function (providerId) {
        return providerId == 2 ? "HSBA" : "NGBB";
    };
    return { aging: aging, scheduledDateWithTimeSlot: scheduledDateWithTimeSlot, packages: packages, dateTime: dateTime, date: date, provider: provider };
} ();

var DateDiff = {
    inDays: function (d1, d2) {
        var t2 = d2.getTime();
        var t1 = d1.getTime();

        return parseInt((t2 - t1) / (24 * 3600 * 1000));
    },

    inWeeks: function (d1, d2) {
        var t2 = d2.getTime();
        var t1 = d1.getTime();

        return parseInt((t2 - t1) / (24 * 3600 * 1000 * 7));
    },

    inMonths: function (d1, d2) {
        var d1Y = d1.getFullYear();
        var d2Y = d2.getFullYear();
        var d1M = d1.getMonth();
        var d2M = d2.getMonth();

        return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
    },

    inYears: function (d1, d2) {
        return d2.getFullYear() - d1.getFullYear();
    }
};


var exportHelper = function () {
    var kendoGridToExcel = function (containerId, baseUrl) {
        var rows = '',
            $dummayTable = $('table#GridToExcelDummyTable');

        $('div[id=' + containerId + ']>div.k-grid-header>div>table>thead>tr:visible').each(function () {
            rows = rows + '<tr>' + $(this).html() + '</tr>';
        });

        $('div[id=' + containerId + ']>div.k-grid-content>table>tbody>tr:visible').each(function () {
            rows = rows + '<tr>' + $(this).html() + '</tr>';
        });
        $dummayTable.html(rows);
        var jsonData = $dummayTable.tableToJSON({
            ignoreColumns: []
        });
        $dummayTable.empty();
        $.ajax({
            url: baseUrl + 'Registration/ExportToExcel',
            type: 'POST',
            data: { jsonData: JSON.stringify(jsonData) }
        }).done(function (data) {
            window.open(baseUrl + "temp/" + data);
        });
    };

    var telerikGridToExcel = function (tableId, baseUrl) {
        var rows = '',
            $dummayTable = $('table#GridToExcelDummyTable');
        $('table[id=' + tableId + ']>tbody>tr:visible').each(function () {
            rows = rows + '<tr>' + $(this).html() + '</tr>';
        });

        $dummayTable.html(rows);
        var jsonData = $dummayTable.tableToJSON({
            ignoreColumns: []
        });
        $dummayTable.empty();
        $.ajax({
            url: baseUrl + 'Registration/ExportToExcel',
            type: 'POST',
            data: { jsonData: JSON.stringify(jsonData) }
        }).done(function (data) {

            window.open(baseUrl + "temp/" + data);
        });
    };

    return { kendoGridToExcel: kendoGridToExcel, telerikGridToExcel: telerikGridToExcel };
} ();

var kendoGridHelper = function () {
    var commonDataBound = function (grid) {
        var currentRecords = grid.dataSource.view();
        for (var i = 0; i < currentRecords.length; i++) {
            var currentRow = grid.tbody.find("tr[data-uid='" + currentRecords[i].uid + "']");
            currentRow.attr("data-reg-id", currentRecords[i].RegID);
            currentRow.hover(function () {
                $(this).addClass('t-state-hover');
            }, function () {
                $(this).removeClass('t-state-hover');
            });
        }
    };
    return { commonDataBound: commonDataBound };
} ();


function tooltips(target) {
    jQuery(".tooltip2").removeClass('focus');
    jQuery(".tooltip2." + target).addClass('focus');
    setTimeout(function () {
        jQuery(".tooltip2").removeClass('focus');
    }, 5000);

    jQuery(".wrappers").removeClass('focus');
    jQuery(".wrappers." + target).addClass('focus');
    setTimeout(function () {
        jQuery(".wrappers").removeClass('focus');
    }, 5000);
}

jQuery(function () {
    jQuery("#ui-accordion").accordion({
        autoHeight: false
    });
    $("#ui-accordion").accordion("option", "icons",
        		{ 'header': 'ui-icon-down', 'headerSelected': 'ui-icon-up' });

    jQuery(".ui-search").accordion({
        autoHeight: false,
        collapsible: true,
        active: false
    });
    $(".ui-search").accordion("option", "icons",
        		{ 'header': 'ui-icon-down', 'headerSelected': 'ui-icon-up' });

    jQuery(".ui-pega").accordion({
        autoHeight: false,
        collapsible: true,
        active: false
    });
    $(".ui-pega").accordion("option", "icons",
        		{ 'header': 'ui-icon-down', 'headerSelected': 'ui-icon-up' });

    if (document.getElementById('h3PayableSec') != null) {
        $('h3#h3PayableSec').click();
    }

});

function numbersonly(e, decimal) {
    var key;
    var keychar;

    if (window.event) {
        key = window.event.keyCode;
    }
    else if (e) {
        key = e.which;
    }
    else {
        return true;
    }
    keychar = String.fromCharCode(key);

    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27)) {
        return true;
    }
    else if ((("0123456789").indexOf(keychar) > -1)) {
        return true;
    }
    else if (decimal && (keychar == ".")) {
        return true;
    }
    else {
        return false;
    }
}

function isNumber(s) {
    return /^-?\d+$/.test(s);
}


$(document).ready(function () {
//    $(document).ajaxStart(function () {
//        $.blockUI({
//            baseZ: 11000
//        })
//    }); //.ajaxStop($.unblockUI);

    if ($("#viewBody").length > 0 && $(".viewInnerBody").length > 0) {
        $('.viewInnerBody').height($('#viewBody').height() + 90);

        if ($(".orderSummary .content").length > 0) {
            $('.orderSummary .content').height($('.viewInnerBody').height() - 50);
        }
    }
});


window.history.forward();

function noBack() {
    window.history.forward();
}

/*
jQuery Version:				jQuery 1.3.2+
Plugin Name:				aToolTip V 1.5
Plugin by: 					Ara Abcarians: http://ara-abcarians.com
License:					aToolTip is licensed under a Creative Commons Attribution 3.0 Unported License
Read more about this license at --> http://creativecommons.org/licenses/by/3.0/			
*/
(function ($) {
    $.fn.aToolTip = function (options) {
        /**
        setup default settings
        */
        var defaults = {
            // no need to change/override
            closeTipBtn: 'aToolTipCloseBtn',
            toolTipId: 'aToolTip',
            // ok to override
            fixed: false,
            clickIt: false,
            inSpeed: 200,
            outSpeed: 100,
            tipContent: '',
            toolTipClass: 'defaultTheme',
            xOffset: 5,
            yOffset: 5,
            onShow: null,
            onHide: null
        },
        // This makes it so the users custom options overrides the default ones
    	settings = $.extend({}, defaults, options);

        return this.each(function () {
            var obj = $(this);
            /**
            Decide weather to use a title attr as the tooltip content
            */
            if (obj.attr('title')) {
                // set the tooltip content/text to be the obj title attribute
                var tipContent = obj.attr('title');
            } else {
                // if no title attribute set it to the tipContent option in settings
                var tipContent = settings.tipContent;
            }

            /**
            Build the markup for aToolTip
            */
            var buildaToolTip = function () {
                $('body').append("<div id='" + settings.toolTipId + "' class='" + settings.toolTipClass + "'><p class='aToolTipContent'>" + tipContent + "</p></div>");

                if (tipContent && settings.clickIt) {
                    $('#' + settings.toolTipId + ' p.aToolTipContent')
					.append("<a id='" + settings.closeTipBtn + "' href='#' alt='close'>close</a>");
                }
            },
            /**
            Position aToolTip
            */
			positionaToolTip = function () {
			    $('#' + settings.toolTipId).css({
			        top: (obj.offset().top + $('#' + settings.toolTipId).outerHeight() +20 - settings.yOffset) + 'px',
			        left: (obj.offset().left-130 - obj.outerWidth() + settings.xOffset) + 'px'
			    })
				.stop().fadeIn(settings.inSpeed, function () {
				    if ($.isFunction(settings.onShow)) {
				        settings.onShow(obj);
				    }
				});
			},
            /**
            Remove aToolTip
            */
			removeaToolTip = function () {
			    // Fade out
			    $('#' + settings.toolTipId).stop().fadeOut(settings.outSpeed, function () {
			        $(this).remove();
			        if ($.isFunction(settings.onHide)) {
			            settings.onHide(obj);
			        }
			    });
			};

            /**
            Decide what kind of tooltips to display
            */
            // Regular aToolTip
            if (tipContent && !settings.clickIt) {
                // Activate on hover	
                obj.hover(function () {
                    // remove already existing tooltip
                    $('#' + settings.toolTipId).remove();
                    obj.attr({ title: '' });
                    buildaToolTip();
                    positionaToolTip();
                }, function () {
                    removeaToolTip();
                });
            }

            // Click activated aToolTip
            if (tipContent && settings.clickIt) {
                // Activate on click	
                obj.click(function (el) {
                    // remove already existing tooltip
                    $('#' + settings.toolTipId).remove();
                    obj.attr({ title: '' });
                    buildaToolTip();
                    positionaToolTip();
                    // Click to close tooltip
                    $('#' + settings.closeTipBtn).click(function () {
                        removeaToolTip();
                        return false;
                    });
                    return false;
                });
            }

            // Follow mouse if enabled
            if (!settings.fixed && !settings.clickIt) {
                obj.mousemove(function (el) {
                    $('#' + settings.toolTipId).css({
                        top: (el.pageY - $('#' + settings.toolTipId).outerHeight() - settings.yOffset),
                        left: (el.pageX + settings.xOffset)
                    });
                });
            }

        }); // END: return this
    };
})(jQuery);

	
	
