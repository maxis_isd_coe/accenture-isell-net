﻿var passpordTypeID = '';
var oldICTypeID = '';
var newICTypeID = '';
var msisdnTypeID = '';
var accountNumberTypeID = '';


function numbersonly(e, decimal) {
    var key;
    var keychar;

    if (window.event) {
        key = window.event.keyCode;
    }
    else if (e) {
        key = e.which;
    }
    else {
        return true;
    }
    keychar = String.fromCharCode(key);

    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27)) {
        return true;
    }
    else if ((("0123456789").indexOf(keychar) > -1)) {
        return true;
    }
    else if (decimal && (keychar == ".")) {
        return true;
    }
    else {
        return false;
    }
}

function initCheckMandetoryFields(e1, e2, e3, e4, e5) {
    passpordTypeID = e1;
    oldICTypeID = e2;
    newICTypeID = e3;
    msisdnTypeID = e4;
    accountNumberTypeID = e5;
}


function checkMandetoryFields(idCardNo, idCardTypeID, idCardNoFieldId) {
    debugger;
    idCardTypeID = idCardTypeID.toString().split(',')[0];
    if (idCardTypeID == "" && idCardNo == "") {
        alert("The ID Type * field is required.\nThe ID Card No * field is required.");
        return false;
    }

    else if (idCardTypeID == "") {
        alert("The ID Type * field is required.");
        return false;
    }
    else if (idCardNo == "") {
        alert("The ID Card No * field is required.");
        $('#' + idCardNoFieldId).focus();
        return false;
    }
    else {
        if (passpordTypeID == idCardTypeID) {
            if (/[^a-zA-Z0-9]/.test(idCardNo)) {
                alert('Passport must be alphanumeric');
                return false;
            }
            if (idCardNo.length > 15) {
                alert('Passport can not be more than 15 charcters.');
                return false;
            }
        }
        if (oldICTypeID == idCardTypeID) {
            var testmatch = /^[0-9 \/]{1,20}$/;

            if (idCardNo.length > 15) {
                alert('Old IC can not be more than 15 charcters.');
                return false;
            }

        }
        if (newICTypeID == idCardTypeID) {
            if (isNaN(idCardNo)) {
                alert('New IC must be numbers only');
                $('#' + idCardNoFieldId).focus();
                $('#' + idCardNoFieldId).val('');
                return false;
            }
            if (idCardNo.length != 12) {
                alert('New IC must be 12 number.');
                $('#' + idCardNoFieldId).focus();
                $('#' + idCardNoFieldId).val('');
                return false;
            }
        }

        if (msisdnTypeID == idCardTypeID) {

            if (isNaN(idCardNo)) {
                alert('MSISDN must be numbers only');
                $('#' + idCardNoFieldId).focus();
                $('#' + idCardNoFieldId).val('');
                return false;
            }
            if (idCardNo.length > 15) {
                alert('MSISDN can not be more than 15 characters.');
                return false;
            }
            if (idCardNo != "") {
                if (idCardNo.substring(0, 2) != "60") {
                    alert("Please enter correct format i.e 60xxx");
                    return false;
                }
            }
        }

        if (accountNumberTypeID == idCardTypeID) {

            if (isNaN(idCardNo)) {
                alert('Account Number must be numbers only');
                $('#' + idCardNoFieldId).focus();
                $('#' + idCardNoFieldId).val('');
                return false;
            }
        }
        else {
            if (idCardNo.length > 15) {
                alert('Other ID can not be more than 15 charcters.');
                return false;
            }
        }
    }
    return true;
}

function checkThirdPartyFields(ThirdPartyName, ThirdPartyIDNo, ThirdPartyType, ThirdPartyContact, ValidationType)
{
    debugger;
    var mobregex = /^60[0-9]{8,10}$/;
    var ThirdPartyTypeName = ThirdPartyType.split(',')[0];
    var ThirdPartyTypeID = ThirdPartyType.split(',')[1];

    if (ThirdPartyTypeName == '' || ThirdPartyTypeID == '' || ThirdPartyIDNo == '' || ThirdPartyIDNo == '\t')
    {
        $('#Plan_Error').show();
        $('#Plan_Error').html('Please select or enter a valid identification information.');
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    }
    else if (ThirdPartyTypeID == "1")
    {
        if (ThirdPartyIDNo.length != 12)
        {
            alert('Please enter a valid IC number.');
            return false;
        }

        if (isNaN(ThirdPartyIDNo))
        {
            alert('New IC must be numbers only.');
            return false;
        }
    }
    else
    {
        if (passpordTypeID == ThirdPartyTypeName)
        {
            if (/[^a-zA-Z0-9]/.test(ThirdPartyIDNo))
            {
                alert('Passport must be alphanumeric');
                return false;
            }
            else if (ThirdPartyIDNo.length > 15)
            {
                alert('Passport can not be more than 15 characters.');
                return false;
            }
        }
        else
        {
            if (ThirdPartyIDNo.length > 15)
            {
                alert('Other ID can not be more than 15 characters.');
                return false;
            }
        }
    }

    if (ValidationType == 'SubmitRegForm')
    {
        if (ThirdPartyName.trim() == '' || ThirdPartyName == '\t')
        {
            $('#Plan_Error').show();
            $('#Plan_Error').html('Please Enter Authorized Party Name.');
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        }

        if (ThirdPartyContact == '' || ThirdPartyContact == '\t')
        {
            $('#Plan_Error').show();
            $('#Plan_Error').html('Please Enter Authorized Party Contact Number.');
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        }

        if (!mobregex.test(ThirdPartyContact))
        {
            $('#Plan_Error').show();
            $('#Plan_Error').html('Please enter valid Contact number. eg. 60xxxxxxxxx');
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        }
    }
    return true;
}

function validateMyMaxisAppSurvey() {
    var surveyResult = '';

    $('.survey').each(function (i, e) {
        if ($(this).is(":checked")) {
            var dataValue = $(this).data('value');
            //if (dataValue == 'yes') {
                surveyResult = dataValue;
            //}
        }
    });

    if (surveyResult == 'yes') {
        //if ($.trim($("#feedbackID").val()) == '') {
        
        var ratecheck = false;

        if ($('#survey-great').is(":checked") || $('#survey-average').is(":checked") || $('#survey-improve').is(":checked")) {
            ratecheck = true;
        }

        if (!ratecheck) {
            $('#Plan_Error').show();
            $('#Plan_Error').html("Please enter MyMaxis app feedback.");
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        }
    }

    if (surveyResult == '') {
        $('#Plan_Error').show();
        $('#Plan_Error').html("Please enter MyMaxis app feedback.");
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    }

    return true;
}