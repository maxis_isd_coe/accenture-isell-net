﻿
var fromOutrightDevice = $('#isOutrightDevice').val();
var fromOutrightAccessory = $('#isOutrightAccessory').val();

$(document).ready(function () {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: loadAccessoryURL,
        data: {}
    })
        .done(function (data) {
            //debugger;
            showResultTable(data, false);
        })
        .fail(function (xhr) {
            //debugger;
            //alert(xhr.responseText);
        });
});

$('#searchText').keyup(function (e) {
    e.preventDefault();
    if (e.keyCode == 13) {
        $("#btnSearch").click();
    }
});

$('#btnSearch').click(function (e) {
    e.preventDefault();
    var sBy = $('#searchBy').val();
    var sText = $('#searchText').val().trim();

    if (!sBy || sBy == 0) {
        $('#Plan_Error').html("Search type is required");
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    }

    if (!sText) {
        $('#Plan_Error').html("Search text is required");
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    }

    if (sBy == 3 && !validateSearchKeyword(sText)) {
        return false;
    }

    if (sBy == 4) {
        $('#serialNumber').val(sText);
    }
    else {
        $('#serialNumber').val('');
    }

    $('#Plan_Error').html("");

    //debugger;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: searchAccessoryURL,
        data: {
            searchBy: sBy,
            searchText: sText,
            fromOutrightPurchase: (fromOutrightDevice || fromOutrightAccessory)
        }
    })
        .done(function (data) {
            //debugger;
            showSearchResultTable(data);
            $('#searchText').val('');
            $('#searchBy').val(0);
        })
        .fail(function (xhr) {
            //debugger;
            //alert(xhr.responseText);
        });
    return false;
});

function validateSearchKeyword(keyword) {
    //debugger;
    var _result = true;
    var regEx = /^[ A-Za-z0-9_+-]*$/;

    if (keyword.length < 2 || keyword.length > 100) {
        $('#Plan_Error').html("Your search keyword must be between 2 to 100 characters.");
        $("html, body").animate({ scrollTop: 0 }, "slow");
        _result = false;
    }

    if (!regEx.test(keyword)) {
        $('#Plan_Error').html("Invalid character found in search criteria");
        $("html, body").animate({ scrollTop: 0 }, "slow");
        _result = false;
    }

    return _result;
}

function showResultTable(list, showAccessorySection) {
    $('#tempResult').empty();
    var totalSelectedAcc = 0;
    $('#totalAccSelected').val(0);
    $('#totalAccFinancingSelected').val(0);
    $('#totalAccFinancingPrice').val(0);

    if (list.length > 0) {
        $('#tempResult').append('<table id="tempResultTable" width="100%" style="font-size:16px; table-layout:fixed;">'
                + '<tr>'
                + '<td width="43%"></td>'
                + '<td width="13%"></td>'
                + '<td width="32%"></td>'
                + '<td width="12%"></td>'
                + '<tr>'
                + '</table>');
    }
    $.each(list, function (index, item) {
        //debugger;
        var selOffer = item.offerDetail;
        var attributeName = item.Attribute;
        var serialNum = "";
        
        if (!attributeName)
            attributeName = "";

        if (item.serialNumber != null && item.serialNumber != "")
            serialNum = '<span id="serialNumber_' + item.serialNumber + '">Serial Number: ' + item.serialNumber + '</span><br/>';

        $('#tempResultTable').append('<tr style="border-bottom:1px solid black; height:50px;"><td valign="top" style="word-break: break-word;">'
                + 'Article ID: ' + item.articleID + '<br/>'
                + serialNum
                + item.Name + '<br/>' + attributeName
                + '</td><td style="text-align:center;">'
                + '<div id="quantityResult_' + item.articleID + '_' + item.index + '">' + item.quantity + '</div>'
                + '</td><td valign="middle">'
                + '<div id="offerResult_' + item.articleID + '_' + item.index + '"></div>'
                + '<td style="text-align:center;">'
                + '<input type="button" class="btnAddRemove" id="removeAccessory_' + item.articleID + '" onclick="removeAccessory(' + item.index + ', this);" value="Remove"/>'
                + '</td>'
                + '</tr>');

        //debugger;
        $('#totalAccSelected').val(totalSelectedAcc += item.quantity);
        selectedOffer(item.articleID, item.index, item.offerDetail, item.quantity);

        $('#tempResult').show();
        $('#Plan_Error').html("");
    });

    //debugger;
    if (fromOutrightDevice == "True") {
        showOrderSummaryDetails(list, showAccessorySection);
        constructTotalPrice(list);
    }
    else if (fromOutrightAccessory == "True") {
        showAccessoryDetails(list);
    }

    validateAddAccessory(totalSelectedAcc);
}

function selectedOffer(articleID, indexKey, offerList, quantity) {
    //debugger;
    var list = offerList;
    var totalSelectedAccFinancing = parseInt($('#totalAccFinancingSelected').val());
    var totalPriceAccFinancing = parseFloat($('#totalAccFinancingPrice').val());

    $.each(list, function (index, item) {
        //debugger;

        if (item.isSelected) {
            if (item.isAccFinancing == true) {
                var currentPrice = item.rrpPrice - item.discount_amount;
                $('#totalAccFinancingSelected').val(totalSelectedAccFinancing += quantity);
                $('#totalAccFinancingPrice').val(totalPriceAccFinancing += parseFloat(quantity * currentPrice));

                $('#offerResult_' + articleID + '_' + indexKey).append('<div data-price="' + currentPrice + '" id="selectedOffer-' + item.articleID + '_' + item.UOMCode + '" style="margin-top:5px; margin-left:5px; margin-right:5px; text-align:center;">'
                    + item.description + ' (' + item.tenure + 'mths) - (RM' + currentPrice + ')'
                    + '</div>');
            }
            else {
                $('#offerResult_' + articleID + '_' + indexKey).append('<div data-price="' + item.offerPrice + '" id="selectedOffer-' + item.articleID + '_' + item.UOMCode + '" style="margin-top:5px; margin-left:5px; margin-right:5px; text-align:center;">'
                    + item.description + ' - (RM' + item.offerPrice + ')'
                    + '</div>');
            }

            if (fromOutrightDevice == "True" || fromOutrightAccessory == "True") {
                var summaryPrice = $('#summary-price_' + articleID + '_' + indexKey);

                if (summaryPrice)
                    summaryPrice.remove();
                
                $('<input>').attr({
                    type: 'hidden',
                    id: 'summary-price_' + articleID + '_' + indexKey,
                    value: item.offerPrice
                }).appendTo('form');
            }
        }
    });
}

function removeAccessory(indexKey, thisElement) {
    var articleID = thisElement.id;
    //debugger;
    if (fromOutrightDevice) {
        var summaryPrice = $('#summary-price_' + articleID.split('_')[1] + '_' + indexKey);

        if (summaryPrice)
            summaryPrice.remove();
    }
    $.ajax({
        type: "POST",
        dataType: "json",
        url: removeAccessoryURL,
        data: {
            articleID: articleID,
            indexKey: indexKey
        }
    })
    .done(function (data) {
        //debugger;
        showResultTable(data, true);
    })
    .fail(function (xhr) {
        //debugger;
        //alert(xhr.responseText);
    });
}

function showOrderSummaryDetails(list, showAccessorySection) {
    //debugger;
    var appendText = "";
    $('#h3accessoryDetails').empty();

    $.each(list, function (index, itemList) {
        var offerPriceID = 'summary-price_' + itemList.articleID + '_' + itemList.index;
        var offerPrice = $('#' + offerPriceID).val();
        var accessoryPrice = offerPrice * itemList.quantity;
        var attributeName = itemList.Attribute;
        if (!attributeName)
            attributeName = "";
        else
            attributeName = " - " + attributeName;

        appendText += "<div class='col-sm-12 kill-padding' id='summary-accessory_" + itemList.articleID + "_" + itemList.index + "'>";
        appendText += "<div class='col-sm-7 kill-padding' style='width:60%;'>";
        appendText += "<span class='txt-11-grey-normal'>" + itemList.Name + "</span>";
        appendText += "<span class='txt-11-grey-normal'>" + attributeName + " (" + itemList.quantity + ")</span>";
        appendText += "</div>";
        appendText += "<span class='txt-11-grey-normal' style='float: right; padding-right: 20px; padding-top: 6px;'>RM" + accessoryPrice.toFixed(2) + "</span><br />";
        appendText += "</div>";
    });

    //debugger;
    if (list.length > 0) {
        $('#h3accessoryDetails').append(appendText);
        $('#h3accessoryHeader').show();
        $('#h3accessoryDetails').show();
        //$('#h3accessoryDetails').attr('style', 'height:auto');

        if (showAccessorySection) {
            document.getElementById("h3accessoryHeader").click();
        }
        else {
            $('#h3accessoryDetails').slideUp();
        }
    }
    else {
        $('#h3accessoryHeader').hide();
        $('#h3accessoryDetails').hide();
    }
}

function constructTotalPrice(list) {
    //debugger;
    var appendText = "";
    var devicePrice = parseFloat($('#spnDevicePrice').html());
    var accessoriesPrice = 0.00;
    //var totalPrice = parseInt($('#spnTotalPrice').html()) + accessoryPrice;
    var totalPrice = devicePrice;

    $.each(list, function (index, item) {
        var offerPriceID = 'summary-price_' + item.articleID + '_' + item.index;
        var offerPrice = $('#' + offerPriceID).val();
        var accessoryPrice = item.quantity * offerPrice;

        accessoriesPrice += accessoryPrice;
        totalPrice += accessoryPrice;
    });

    $('#payableItemDetails').empty();
    if (list.length > 0) {
        appendText += "<span class='service-payable-label'>Accessories Price</span>";
        appendText += "<span class='service-payable-devider'>:</span>";
        appendText += "<span class='txt-11-grey-normal PriceClass' id='spnDevicePrice'>" + accessoriesPrice.toFixed(2) + "</span>";

        $('#payableItemDetails').append(appendText);
    }
    $('#spnTotalPrice').html(totalPrice.toFixed(2));

}

function showAccessoryDetails(list) {
    //debugger;
    var appendText = "";
    var totalPrice = 0.00;
    $('#h3accessoryDetails').empty();

    $.each(list, function (index, itemList) {
        var offerPriceID = 'summary-price_' + itemList.articleID + '_' + itemList.index;
        var offerPrice = parseFloat($('#' + offerPriceID).val());
        var accessoryPrice = offerPrice * itemList.quantity;
        var attributeName = itemList.Attribute;
        if (!attributeName)
            attributeName = "";
        else
            attributeName = " - " + attributeName;

        totalPrice += accessoryPrice;

        appendText += "<div class='col-sm-12 kill-padding' id='summary-accessory_" + itemList.articleID + "_" + itemList.index + "'>";
        appendText += "<div class='col-sm-8 kill-padding'>";
        appendText += "<h5>" + itemList.Name + " " + attributeName + " (" + itemList.quantity + ")</h5>";
        appendText += "</div>";
        appendText += "<div class='col-sm-4'>";
        appendText += "<p class='price'>";
        appendText += "<span>RM" + accessoryPrice.toFixed(2) + "</span>";
        appendText += "</p>";
        appendText += "</div>";
        appendText += "</div>";
    });

    //debugger;
    if (list.length > 0) {
        $('#h3accessoryDetails').append(appendText);
        //$('#h3accessoryHeader').show();
        $('#h3accessoryDetails').show();
        $('#payableItemDetails').html("RM" + totalPrice.toFixed(2) + GSTMark);
    }
    else {
        //$('#h3accessoryHeader').hide();
        $('#h3accessoryDetails').hide();
    }
}

function showSearchResultTable(list) {
    var nostock = 0;
    var currentstock = 0;
    var searchBySerialNumber = $('#serialNumber').val() != null && $('#serialNumber').val() != "";

    $('#searchResult').empty();
    if (list.length > 0) {
        var appendText = '<table id="searchResultTable" width="100%" style="font-size:16px; table-layout:fixed;">';
        appendText += '<tr>';
        appendText += '<td width="41%"></td>';
        appendText += '<td width="15%"></td>';
        appendText += '<td width="32%"></td>';
        appendText += '<td width="12%"></td>';
        appendText += '<tr>';
        appendText += '</table>';
        
        $('#searchResult').append(appendText);
    }

    $.each(list, function (index, item) {
        //debugger;
        if (currentstock < 10) {
            currentstock++;

            var offerList = item.offerDetail;
            var attributeName = item.Attribute;
            var addAccessorySection = "";

            if (!attributeName)
                attributeName = "";

            if (item.isStockAvailable == true) {
                addAccessorySection = '<td style="text-align:center;">'
                                    + '<div id="selectQuantity_' + item.articleID + '" style="border-radius:34px;"></div>'
                                    + '</td><td>'
                                    + '<div class="offer-accessory" id="selectOffer_' + item.articleID + '_' + item.index + '"></div>'
                                    + '</td><td style="text-align:center;">'
                                    + '<input type="button" class="btnAddRemove" id="addAccessory_' + item.articleID + '" onclick="addAccessory(this);" value="Add"/>'
                                    + '</td>';
            }
            else if (item.isStockAvailable == false) {
                addAccessorySection = '<td style="text-align:center;">'
                                    + '</td><td>'
                                    + 'Out of Stock'
                                    + '<td style="text-align:center;">'
                                    + '</td>';
            }

            $('#searchResultTable').append('<tr style="border-bottom:1px solid black; height:70px;"><td valign="middle" style="word-break: break-word;">'
                    + 'Article ID: ' + item.articleID + '<br/>' + item.Name + '<br/>' + attributeName
                    + '</td>'
                    + addAccessorySection
                    + '</tr>');
            //debugger;
            if (item.isStockAvailable == true) {
                addQuantity(item.articleID);
                addOffer(item.articleID, item.offerDetail, item.index);
            }
            else if (item.isStockAvailable == false) {
                nostock++;
            }
        }
    });

    if (searchBySerialNumber == true) {
        disableQuantity();
    }

    //debugger;
    if (list.length > 0 && nostock == list.length && (fromOutrightDevice == "True" || fromOutrightAccessory == "True")) {
        $('#searchResult').empty();
        $('#Plan_Error').html("No accessories found.");
        $("html, body").animate({ scrollTop: 0 }, "slow");
    }
    else if (list.length >= 10) {
        //$('#searchResult').empty();
        //$('#Plan_Error').html("Accessory searched is currently out of stock");
        $('#Plan_Error').html("Your search returned more than 10 results. Please narrow down the search criteria.");
        $("html, body").animate({ scrollTop: 0 }, "slow");
    }
    else if (list.length == 0) 
    {
        $('#searchResult').empty();
        //$('#Plan_Error').html("Unable to find accessory for EAN code / article ID entered");
        $('#Plan_Error').html("No accessories found.");
        $("html, body").animate({ scrollTop: 0 }, "slow");
    }
    //validateAddAccessory(list);
}

function addOffer(articleID, offerList, indexKey) {
    //debugger;
    var list = offerList;
    $('#selectOffer_' + articleID + '_' + indexKey).append('<select id="offer-' + articleID + '" class="dropdown-accessory"> <option value="0">Please Select an Offer</option> </select>');

    $.each(list, function (index, item) {
        //debugger;
        if (item.UOMCode != null && item.UOMCode != "") {
            if (item.isAccFinancing == true) {
                var currentPrice = item.rrpPrice - item.discount_amount;
                $('#offer-' + articleID).append('<option data-price="' + currentPrice + '" data-isAccFinancing="' + item.isAccFinancing + '" value="' + item.UOMCode + '">' + item.description  + " (" + item.tenure + "mths)" + ' - (RM' + currentPrice + ')</option>');
            }
            else {
                $('#offer-' + articleID).append('<option data-price="' + item.offerPrice + '" data-isAccFinancing="' + item.isAccFinancing + '" value="' + item.UOMCode + '">' + item.description + ' - (RM' + item.offerPrice + ')</option>');
            }
        }
    });
}

function addQuantity(articleID) {
    var appendText = "";
    var additionalStyle = "";

    if (fromOutrightDevice == "True") {
        additionalStyle = "style='padding-left:12px;'";
    }

    appendText += "<table class='tblQty'><tr>";
    appendText += "<td>";
    appendText += "<input type='text' class='txtQty' readonly name='quantity_" + articleID + "' id='quantity_" + articleID + "' value='1' style='width:30px; height:30px; text-align:center;'/>";
    appendText += "</td>";
    appendText += "<td>";
    appendText += "<input type='button' class='btnQty' " + additionalStyle + " id='subtract_" + articleID + "' onclick='javascript: subtractQty(" + articleID + ");' value='–' />";
    appendText += "</td>";
    appendText += "<td>";
    appendText += "<input type='button' class='btnQty' " + additionalStyle + " id='add_" + articleID + "' onclick='javascript: addQty(" + articleID + ");' value='+' />"
    appendText += "</td>";
    appendText += "</tr></table>";
    $('#selectQuantity_' + articleID).append(appendText);

//    $('#selectQuantity_' + articleID).append("<input type='text' class='txtQty' readonly name='quantity_" + articleID + "' id='quantity_" + articleID + "' value='1' style='width:30px; height:30px; text-align:center;'/>"
//            + "<input type='button' class='btnQty' id='subtract_" + articleID + "' onclick='javascript: subtractQty(" + articleID + ");' value='–' />"
//            + "<input type='button' class='btnQty' id='add_" + articleID + "' onclick='javascript: addQty(" + articleID + ");' value='+' />"
//        );
}

function subtractQty(articleID) {
    //debugger;
    var quantity = parseInt(document.getElementById("quantity_" + articleID).value);
    if (quantity - 1 > 0)
        document.getElementById("quantity_" + articleID).value = --quantity;
    else
        return;
}

function addQty(articleID) {
    //debugger;
    var quantity = parseInt(document.getElementById("quantity_" + articleID).value);
    if (quantity + 1 <= 9) {
        $('#Plan_Error').html("");
        document.getElementById("quantity_" + articleID).value = ++quantity;
    }
    else
        return;
}

function addAccessory(thisElement) {
    //debugger;
    var articleID = thisElement.id.split('_')[1];
    var quantityID = "quantity_" + articleID;
    var offerIDs = "offer-" + articleID;
    var offerComponents = $('[id^=' + offerIDs + ']');
    var quantity = parseInt($('#' + quantityID).val());
    var serialNum = $('#serialNumber').val();
    var offerCode = "";
    var offerSelected = false;
    var serialNumberCheck = CheckExistingSerialNumber(serialNum);

    if (quantity <= 0) {
        $('#Plan_Error').html("Please select the quantity");
        return false;
    }
    //debugger;
    for (var i = 0; i < offerComponents.length; i++) {
        if ($('#' + offerComponents[i].id).val() == "0") {
            $('#Plan_Error').html("Please select the offer");
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        }
        else {
            var totalAccFinancing = parseInt($('#totalAccFinancingSelected').val());
            var totalAcc = parseInt($('#totalAccSelected').val());
            var totalPriceAccFinancing = parseFloat($('#totalAccFinancingPrice').val());
            var isAccFinancing = $('#' + offerComponents[i].id).find(":selected").attr('data-isAccFinancing');

            if ((totalAcc + quantity) > 9) {
                $('#Plan_Error').html("Maximum 9 accessories exceeded. Please remove first before adding more.");
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            }

            if (isAccFinancing != null && isAccFinancing == "true") {
                var afPrice = parseFloat($('#' + offerComponents[i].id).find(":selected").attr('data-price'));
                var selectedAFPrice = afPrice * quantity;

                if ((totalAccFinancing + quantity) > 5) {
                    $('#Plan_Error').html("Maximum accessories for Accessories Financing reached");
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;
                }

                //$('#totalAccFinancingPrice').val(totalPriceAccFinancing + selectedAFPrice);
                offerCode = $('#' + offerComponents[i].id).val();
            }
            else {
                offerCode = $('#' + offerComponents[i].id).val();
            }
        }
    }

    if (serialNumberCheck == false) {
        $('#Plan_Error').html("The serial number for this accessory has been added, please use another serial number");
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    }

    $.ajax({
        type: "POST",
        dataType: "json",
        url: addAccessoryURL,
        data: {
            articleID: articleID,
            uomCode: offerCode,
            quantity: quantity,
            serialNumber: serialNum
        }
    })
        .done(function (data) {
            //debugger;
            showResultTable(data, true);
            //$('#' + offerIDs).val(0);
            $('#searchResult').empty();
            $('#serialNumber').val('');
        })
        .fail(function (xhr) {
            //debugger;
            //alert(xhr.responseText);
        });

    return false;
}

function validateAccessoryFinancingPrice(controllerName, isMandatoryForAF) {
    debugger;
    var result = true;
    var totalAccFinancingCount = parseInt($('#totalAccFinancingSelected').val());
    var totalAccFinancingPrice = parseFloat($('#totalAccFinancingPrice').val());

    if (totalAccFinancingCount > 0 && (totalAccFinancingPrice < minPrice || totalAccFinancingPrice > maxPrice)) {
        $('#Plan_Error').html("Total price for Accessories Financing must be between RM" + minPrice + " and RM" + maxPrice);
        $("html, body").animate({ scrollTop: 0 }, "slow");
        result = false;
    }
    if (controllerName != '' && (controllerName == 'AddContract' || controllerName == 'CRP') && isMandatoryForAF != null && isMandatoryForAF == "True") {
        if (totalAccFinancingCount == 0) {
            $('#Plan_Error').html("Please add at least 1 Accessory Financing to proceed");
            $("html, body").animate({ scrollTop: 0 }, "slow");
            result = false;
        }
    }

    return result;
}

function CheckExistingSerialNumber(currentSerialNumber) {
    var result = true;
    $('span[id^=serialNumber_]').each(function () {
        var existingSerialNumber = this.id.split('_')[1];
        
        if (existingSerialNumber == currentSerialNumber) {
            result = false;
        }
    });

    return result;
}

function validateAddAccessory(totalAccessories) {
    //debugger;
    if (totalAccessories >= 9) {
        disableAddAccessory();
    }
    else {
        enableAddAccessory();
    }
}

function disableAddAccessory() {
    //debugger;
    $('#searchBy').attr('disabled', 'disabled');
    $('#searchBy').css("background-color", "#eeeeee");
    $('#searchBy').css("cursor", "not-allowed");
    $('#searchText').attr('disabled', 'disabled');
    var addAccessoryButton = $('[id^=addAccessory_]');
    var offerButton = $('[id^=offer-]');

    disableQuantity();

    if (addAccessoryButton) {
        addAccessoryButton.attr('disabled', 'disabled');
        addAccessoryButton.css("cursor", "not-allowed");
    }

    if (offerButton) {
        offerButton.attr('disabled', 'disabled');
        offerButton.css("background-color", "#eeeeee");
        offerButton.css("cursor", "not-allowed");
    }

    $('#Plan_Error').html("Maximum accessories reached");
    $("html, body").animate({ scrollTop: 0 }, "slow");
}

function disableQuantity() {
    var divQuantity = $('[id^=selectQuantity_]');
    var substractButton = $('[id^=subtract_]');
    var addButton = $('[id^=add_]');

    if (divQuantity) {
        divQuantity.css("background-color", "#eeeeee");
    }

    if (addButton) {
        addButton.attr('disabled', 'disabled');
        addButton.css("cursor", "not-allowed");
    }

    if (substractButton) {
        substractButton.attr('disabled', 'disabled');
        substractButton.css("cursor", "not-allowed");
    }
}

function enableAddAccessory() {
    //$('#Plan_Error').html("");
    $('#searchBy').prop('disabled', false);
    $("#searchBy").css("background-color", "");
    $("#searchBy").css("cursor", "");
    $("#searchText").prop('disabled', false);
    var addAccessoryButton = $('[id^=addAccessory_]');
    var offerButton = $('[id^=offer-]');

    enableQuantity();

    if (addAccessoryButton) {
        addAccessoryButton.prop('disabled', false);
        addAccessoryButton.css("cursor", "");
    }

    if (offerButton) {
        offerButton.prop('disabled', false);
        offerButton.css("background-color", "");
        offerButton.css("cursor", "");
    }
}

function enableQuantity() {
    var divQuantity = $('[id^=selectQuantity_]');
    var substractButton = $('[id^=subtract_]');
    var addButton = $('[id^=add_]');

    if (divQuantity) {
        divQuantity.css("background-color", "");
    }

    if (addButton) {
        addButton.prop('disabled', false);
        addButton.css("cursor", "");
    }

    if (substractButton) {
        substractButton.prop('disabled', false);
        substractButton.css("cursor", "");
    }
}