﻿
function ValidateCDPUDescription() {
    debugger;
    var approval = $("input[type=hidden][id=CDPUStatus]").val();
    var approval_description = $("#CDPUDescription").val();
    if (approval == "Pending" || approval_description == "") {
        var myElem = document.getElementById('Plan_Error');
        if (myElem === null) {
            // sk_mobileregsummary crp
            $('#lblErrMsg').show();
            $('#lblErrMsg').html("*Please enter description for Approve/Reject");
        }
        else {
            // new skin
            $('#Plan_Error').show();
            $('#Plan_Error').html("*Please enter description for Approve/Reject");
        }
        
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    }
    else {
        return true;
    }

};

$(document).ready(function () {

    hideExistingButton();

    // load Status of CDPU Approval (Approved/Rejected/Pending)
    var Status = $('#CDPUStatus').val();
    if (Status == 'Approved' || Status == 'Rejected') {
        $("input[id^='type-cdpu'][data-value='" + Status + "']").attr('checked', 'checked');
    }

    $('.CDPU').click(function (i, e) {
        var dataValue = $(this).data('value');
        clearCDPUButton(i);
        $(this).prop('checked', true);
        setCDPUStatus(dataValue);
    });

    $('#takeLock').click(function (i, e) {
        debugger;
        var dataValue = $(this).data('value');
        $(this).prop('checked', false);
        LockRegID(dataValue);
    });


});

function setCDPUStatus(value) {
    $('#CDPUStatus').val(value);
};

function clearCDPUButton(index) {
    $('.CDPU').each(function (i, e) {
        if (index != i) {
            $(this).prop('checked', false);
        } else {
            $(this).prop('checked', true);
        }
    });
};

// hide existing button, show only new approved
function hideExistingButton() {
    $('#divAccEon').hide();
    $('#divAccEoff').hide();
    $('#divSvcEon').hide();
    $('#divSvcEoff').hide();
    $('#divCDPUApproval').show();
};


    
