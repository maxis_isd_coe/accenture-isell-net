﻿//this is placed for vas page, introduced on drop4 phase.

$(document).ready(function () {

});

// trade up function
function selectTradeUpVAS(compId, isNewView) {
    if (typeof isNewView === 'undefined') { isNewView = 'No'; }
    var id = compId.toString();
    var btnName = compId.toString().split('_')[0];
    var groupId = compId.toString().split('_')[1];
    var price = compId.toString().split('_')[2];

    if ($('#' + id).hasClass('focus') || $('#' + id).hasClass('selected-vas')) {
        clearAllTradeUpButton();
    } else {
        clearAllTradeUpButton();
        selectButton(id,isNewView);
    }
}

function showHideTradeUp(offerID) {
    var isTradeUpOffer = $('#' + offerID).attr('data-istradeup');
    var tradeUpComponentGroupId = offerID.toString().split('_')[1];
    var groupIDs = $('[id^=tradeUpSection_]');

    for (var i = 0; i < groupIDs.length; i++) {
        var groupID = groupIDs[i].id;
        document.getElementById(groupID).style.display = "none";
    }

    if (isTradeUpOffer == "True") {
        $('#tradeUpSection_' + tradeUpComponentGroupId).attr('style', 'display: block');
        $('#tradeUpSection').show();
    }
    else if (isTradeUpOffer == "False") {
        $('#tradeUpSection_' + tradeUpComponentGroupId).attr('style', 'display: none');
        $('#tradeUpSection').hide();
    }

    clearAllTradeUpButton();
}

function selectButton(id, isNewView) {
    if (isNewView == 'Yes') {
        $('#' + id).addClass("selected-vas");
    }
    else {
        $('#' + id).addClass("focus");
    }
    $('#hdnTradeUpAmount').val(id);
    $('#VAS_hdnTradeUpAmount').val(id);
    $('#ValueAddedServicesVM_hdnTradeUpAmount').val(id); // reskin
}

function clearAllTradeUpButton() {
    $('[id^=btnTradeUp_]').removeClass("focus");
    $('[id^=btnTradeUp_]').removeClass("selected-vas");
    $('#hdnTradeUpAmount').val('');
    $('#VAS_hdnTradeUpAmount').val('');
    $('#ValueAddedServicesVM_hdnTradeUpAmount').val(''); // reskin
}

// device financing
function checkDisabledVAS(objBtnId, objvasID, objGroupId, selectedKenanCodes) {
    var oneTimeCharge = $('#' + objBtnId).attr('data-oneTime');

    if (oneTimeCharge != undefined && oneTimeCharge != '0') {
        $('#prompt_objBtnId').val(objBtnId);
        $('#prompt_objvasID').val(objvasID);
        $('#prompt_objGroupId').val(objGroupId);
        $('#prompt_selectedKenanCodes').val(selectedKenanCodes);
        $('#prompt_price').html(oneTimeCharge);

        $('#deselectPromptWindow').modal('show');
    }

}

function checkDisabledVAS_CRP(objBtnValue) {

    var oneTimeCharge = $('[value=' + objBtnValue + ']').attr('data-oneTime');

    if (oneTimeCharge != undefined && oneTimeCharge != '0') {
        $('#prompt_objBtnId').val(objBtnValue);
//        $('#prompt_objvasID').val(objvasID);
//        $('#prompt_objGroupId').val(objGroupId);
//        $('#prompt_selectedKenanCodes').val(selectedKenanCodes);
        $('#prompt_price').html(oneTimeCharge);
        //debugger;
        $('#deselectPromptWindow').modal('show');
    }
    

}



function checkDisabledVAS_AddContract(addchkBox, objGroupId, vasName, price, curKenanCode) {
    $('[id^="btnConVAS_"]').each(function () {
        if ($(this).hasClass('focus') || $(this).hasClass('selected-vas')) {
            var mainComp = '46040';
            var deselectedVasComp = '46041';
            var curContractKenanCode = $('#hdnContractkenanVASID').val();
            if (curContractKenanCode == mainComp) {
                //debugger;
                var currVasID = addchkBox.id;
                var oneTimeCharge = $(this).attr('data-oneTime');
                //var addchkBoxAttr = $('#' + currVasID).prop('checked');
                var currKenanCode = $('#' + currVasID).attr('data-kenancode');
                if (currKenanCode == deselectedVasComp) {
                    if (oneTimeCharge != undefined) {
                        $('#prompt_objGroupId').val(objGroupId);
                        $('#prompt_vasPrice').val(price);
                        $('#prompt_selectedKenanCodes').val(curKenanCode);
                        $('#prompt_vasName').val(vasName);
                        $('#prompt_price').html(oneTimeCharge);
                        $('#prompt_checkbox').val(addchkBox);
                        $('#prompt_objvasID').val(currVasID);
                        $('#prompt_action').val('add');

                        $('#deselectPromptWindow').modal('show');
                    }
                }
            }
        }
    });

}

function checkDisabledVAS_AddContract_remove(removechkBox, vasName, price, groupId){
    $('[id^="btnConVAS_"]').each(function () {
        if ($(this).hasClass('focus') || $(this).hasClass('selected-vas')) {
            var mainComp = '46040';
            var deselectedVasComp = '46041';
            var curContractKenanCode = $('#hdnContractkenanVASID').val();
            if (curContractKenanCode == mainComp) {
                //debugger;
                var currVasID = removechkBox.id;
                var oneTimeCharge = $(this).attr('data-oneTime');
                //var addchkBoxAttr = $('#' + currVasID).prop('checked');
                var currKenanCode = $('#' + currVasID).attr('data-kenancode');
                if (currKenanCode == deselectedVasComp) {
                    if (oneTimeCharge != undefined) {
                        $('#prompt_objGroupId').val(groupId);
                        $('#prompt_vasPrice').val(price);
                        //$('#prompt_selectedKenanCodes').val(curKenanCode);
                        $('#prompt_vasName').val(vasName);
                        $('#prompt_price').html(oneTimeCharge);
                        $('#prompt_checkbox').val(removechkBox);
                        $('#prompt_objvasID').val(currVasID);
                        $('#prompt_action').val('remove');

                        $('#deselectPromptWindow').modal('show');
                    }
                }
            }
        }
    });

}


function checkDisabledOffer_CRP(btnConVASID) {
    var dfOffer = $('#' + btnConVASID).attr('data-dfOffer');
    if (dfOffer == 'True') {
        var oneTimeCharge = $('#' + btnConVASID).attr('data-oneTime');
        $('#deselectPromptWindow').modal('show');
        $('#prompt_price').html(oneTimeCharge);
        return false;
    }
    return true;    
}

// btnConVAS_14296_46040
function checkOfferAgainstVAS_AddContract(deviceModelId, btnConVASID) {
    var mainComp = '46040';
    var deselectedVasComp = '46041';
    var mdpVasComp = '45568,45569,47535';
    var pdpComp = '46153,47536';
    var curConKenanCode = btnConVASID.split('_')[2];
    var pbpcID = btnConVASID.split('_')[1];
    var selectedConVas = $('#' + btnConVASID).hasClass('focus') || $('#' + btnConVASID).hasClass('selected-vas');
    var curContractKenanCode = $('#hdnContractkenanVASID').val();
    var vasID = '';
    var mdp_decision = $('#mdp_decision').val();
    var has_mdp = $('#hasMDP').val();

    var mdpVasID_add = '';
    var mdpVasID_remove = '';

    var price;
    var objGroupId;
    var kenanCode;
    var componentName;
    var thisObj;
    var check;
    var action;

    var mdp_add_price;
    var mdp_add_objGroupId;
    var mdp_add_kenanCode;
    var mdp_add_componentName;
    var mdp_add_thisObj;
    var mdp_add_check;
    var mdp_add_action;

    var mdp_remove_price;
    var mdp_remove_objGroupId;
    var mdp_remove_kenanCode;
    var mdp_remove_componentName;
    var mdp_remove_thisObj;
    var mdp_remove_check;
    var mdp_remove_action;

    var pdpID = '';
    var pdp_check;

    var panel = $('#divAddedVas');
    //var childDivs = $('#frmSelectVAS').find(':checkbox');
    var childDivs = $('li[name=chkbxComponent]');

    for (var i = 0; i < childDivs.length; i++) {
        var componentKenanCode = $('#' + childDivs[i].id).attr('data-kenanCode');
        if (componentKenanCode == deselectedVasComp) {
            vasID = childDivs[i].id;
        }
        if (mdpVasComp.indexOf(componentKenanCode) > -1) {
            if ($('#' + childDivs[i].id).attr('data-action') == 'add') {
                mdpVasID_add = childDivs[i].id;
            }
            else {
                mdpVasID_remove = childDivs[i].id;
            }
        }
        //if (componentKenanCode == pdpComp) {
        if (pdpComp.indexOf(componentKenanCode) > -1) {
        debugger;
            if ($('#' + childDivs[i].id).attr('data-action') == 'remove') {
                pdpID = childDivs[i].id;
            }
        }
    }

    //initialize all needed value
    if (vasID != null && vasID != '') {
        price = $('#' + vasID).attr('data-price');
        objGroupId = $('#' + vasID).attr('data-objGroupId');
        kenanCode = $('#' + vasID).attr('data-kenanCode');
        componentName = $('#' + vasID).attr('data-ComponentName');
        thisObj = $('#' + vasID);
        //check = $('#' + vasID).is(':checked');
        check = $('#' + vasID).hasClass('selected-vas');
        action = $('#' + vasID).attr('data-action');
    }
    if (mdpVasID_add != '') {
        mdp_add_price = $('#' + mdpVasID_add).attr('data-price');
        mdp_add_objGroupId = $('#' + mdpVasID_add).attr('data-objGroupId');
        mdp_add_kenanCode = $('#' + mdpVasID_add).attr('data-kenanCode');
        mdp_add_componentName = $('#' + mdpVasID_add).attr('data-ComponentName');
        mdp_add_thisObj = $('#' + mdpVasID_add);
        mdp_add_check = $('#' + mdpVasID_add).hasClass('selected-vas');
        mdp_add_action = $('#' + mdpVasID_add).attr('data-action');
    }
    if (pdpID != '') {
        pdp_check = $('#' + pdpID).hasClass('selected-vas');
    }

    if ((vasID != null && vasID != '' && has_mdp == 'True' && mdp_decision != "Yes")) {
        if (check && curContractKenanCode != mainComp) {
            //$('#' + vasID).prop('checked', false);
            //thisObj.checked = false;
            //$('#' + vasID).removeClass('selected-vas');
            //thisObj.removeClass('selected-vas');
            if (action == 'add') {
                addedVas(vasID, thisObj, objGroupId, componentName, price, kenanCode);
            } else if (action == 'remove') {
                removeVas(vasID, thisObj, componentName, price, objGroupId);
            }
        }
        if (!check && curContractKenanCode == mainComp) {
            $('#hdndfVASID').val(vasID);
            document.getElementById(vasID).click();
            var result = true;
        }
    }

    if (has_mdp == 'False') {
        //debugger;
        if (curContractKenanCode == mainComp) { //df offer
            debugger;
            if (vasID != null && vasID != '' && !check) { // upgrade fee is not checked
                $('#hdndfVASID').val(vasID);
                document.getElementById(vasID).click();
            }

            if (mdpVasID_add != '' && mdp_add_check) {

                addedVas(mdpVasID_add, mdp_add_thisObj, mdp_add_objGroupId, mdp_add_componentName, mdp_add_price, mdp_add_kenanCode);
            }
       
        }
        else { //non-DF offer
            debugger;
            if (vasID != null && vasID != '' && check) { // upgrade fee is checked
                if (action == 'add') {
                    addedVas(vasID, thisObj, objGroupId, componentName, price, kenanCode);
                } else if (action == 'remove') {
                    removeVas(vasID, thisObj, componentName, price, objGroupId);
                }
            }

            if (mdpVasID_add != '' && !mdp_add_check) {
                if (!pdp_check) {
                    document.getElementById(mdpVasID_add).click();
                }
            }
        }
    }
     
    return result
}

function checkOfferAgainstVAS_CRP(btnConVASID) {
    var mainComp = '46040';
    var deselectedVasComp = '46041';
    var mdpVasComp = '45568,45569,47535';
    var curConKenanCode = btnConVASID.split('_')[2];
    var pbpcID = btnConVASID.split('_')[1];
    var selectedConVas = $('#' + btnConVASID).hasClass('selected-vas');
    var curContractKenanCode = $('#hdnContractkenanVASID').val();
    var mdp_decision = $('#mdp_decision').val();
    var has_mdp = $('#hasMDP').val();

    $('#tobe-plan-accordion li').each(function () {

        var curBtnId = $(this).attr("value");
        var curCompId = curBtnId.toString().split('_')[0];
        var curKenanCode = curBtnId.toString().split('_')[1];
        var selectedVas = $(this).hasClass('selected-vas');
        var contractCriteria = (curContractKenanCode == mainComp); // to define offer selected is DF or not
        var mdpCriteria = (mdpVasComp.indexOf(curKenanCode) > -1); // to define component in loop is MDP
        var arrVasID = $('#SelectedCompIds').val().split(',');
        var arrKenanID = $('#SelectedKenanCodes').val().split(',');

        // rule for device financing upgrade fee
        if ((has_mdp == 'True' && mdp_decision != "Yes") || has_mdp == 'False') {
            if (selectedVas == true && curKenanCode == deselectedVasComp && contractCriteria == false) {
                $(this).removeClass('selected-vas');
                getSelectedKenanCodeVasIDs();
            }
            else {
                if (contractCriteria == true && curKenanCode == deselectedVasComp) {
                    if (!$(this).hasClass('selected-vas')) {
                        $(this).addClass('selected-vas');
                        getSelectedKenanCodeVasIDs();
                    }
                }
            }
        }

        // rule for mdp component
        if (has_mdp == 'False' && mdpCriteria) {
            if (selectedVas == true && contractCriteria == true) {
                $(this).removeClass('selected-vas');
                getSelectedKenanCodeVasIDs();
            }
            else {
                if (selectedVas == false && contractCriteria == false) {
                    $(this).addClass('selected-vas');
                    getSelectedKenanCodeVasIDs();
                }
            }
        }

    });

}

function checkOfferAgainstVAS(btnConVASID) {
    var mainComp = '46040';
    var deselectedVasComp = '46041';
    var mdpVasComp = '45568,45569,47535';
    var curConKenanCode = btnConVASID.split('_')[2];
    var pbpcID = btnConVASID.split('_')[1];
    var selectedConVas = $('#' + btnConVASID).hasClass('selected-vas');
    var curContractKenanCode = $('#hdnContractkenanVASID').val();
    var mdpId = '';
    var mdpKenanCode = '';
    var mdpCompId = '';
    var mdpGroupId = '';

    $('li[id^="btnVAS_"]').each(function () {
        var curBtnId = $(this).attr("id");
        var curKenanCode = curBtnId.toString().split('_')[2]
        var curCompId = curBtnId.toString().split('_')[1]
        var selectedVas = $(this).hasClass('selected-vas');
        var contractCriteria = (curContractKenanCode == mainComp);
        var arrVasID = $('#SelectedVasIDs').val().split(',');
        var arrKenanID = $('#SelectedKenanCodes').val().split(',');
        var arrGroupIds = $('#hdnGroupIds').val().split(',');
        var indexVASID = arrVasID.indexOf(curCompId);
        var indexVASKenanCode = arrKenanID.indexOf(curKenanCode);
        if (mdpVasComp.indexOf(curKenanCode) > -1) {
            mdpId = curBtnId;
            mdpKenanCode = curKenanCode;
            mdpCompId = curCompId;
            mdpGroupId = $('#' + curBtnId).attr('data-groupid');
        }

        if (selectedVas == true && curKenanCode == deselectedVasComp && contractCriteria == false) {
            $('#' + curBtnId).removeClass('selected-vas');
            $('#' + mdpId).addClass('selected-vas');

            if (indexVASID > -1) {
                arrVasID.splice(indexVASID, 1);
            }
            if (indexVASKenanCode > -1) {
                arrKenanID.splice(indexVASKenanCode, 1);
            }
            var newVasIDs = '';
            var newKenanIDs = '';
            var newGroupIDs = '';
            $.each(arrVasID, function (index, value) {
                if (value != "" && value != ',') {
                    newVasIDs = newVasIDs + ',' + value + ',';
                }
            });
            $.each(arrKenanID, function (index, value) {
                if (value != "" && value != ',') {
                    newKenanIDs = newKenanIDs + ',' + value + ',';
                }
            });
            $.each(arrGroupIds, function (index, value) {
                if (value != "" && value != ',') {
                    newGroupIDs = newGroupIDs + ',' + value + ',';
                }
            });

            if (!(newVasIDs.indexOf(mdpCompId) > -1)) {
                newVasIDs = newVasIDs + ',' + mdpCompId;
            }
            if (!(newKenanIDs.indexOf(mdpKenanCode) > -1)) {
                newKenanIDs = newKenanIDs + ',' + mdpKenanCode;
            }
            if (!(newGroupIDs.indexOf(mdpGroupId) > -1)) {
                newGroupIDs = newGroupIDs + ',' + mdpGroupId;
            }

            //debugger;
            $('#SelectedVasIDs').val(newVasIDs);
            $('#SelectedKenanCodes').val(newKenanIDs);
            $('#hdnGroupIds').val(newGroupIDs);

        }
        else {
            if (contractCriteria == true && curKenanCode == deselectedVasComp) {
                //                var temp_SelectedVASIDs = $('#SelectedVasIDs').val();
                //                var temp_SelectedKenanCodes = $('#SelectedKenanCodes').val();

                var indexMdpID = arrVasID.indexOf(mdpCompId);
                var indexMdpKenanCode = arrKenanID.indexOf(mdpKenanCode);
                var indexMdpGroupId = arrGroupIds.indexOf(mdpGroupId);

                if (indexMdpID > -1) {
                    arrVasID.splice(indexMdpID, 1);
                }
                if (indexMdpKenanCode > -1) {
                    arrKenanID.splice(indexMdpKenanCode, 1);
                }
                if (indexMdpGroupId > -1) {
                    arrGroupIds.splice(indexMdpGroupId, 1);
                }

                $('#' + mdpId).removeClass('selected-vas');

                var temp_SelectedVASIDs = '';
                var temp_SelectedKenanCodes = '';
                var temp_SelectedGroupId = '';
                $.each(arrVasID, function (index, value) {
                    if (value != "" && value != ',') {
                        temp_SelectedVASIDs = temp_SelectedVASIDs + ',' + value + ',';
                    }
                });
                $.each(arrKenanID, function (index, value) {
                    if (value != "" && value != ',') {
                        temp_SelectedKenanCodes = temp_SelectedKenanCodes + ',' + value + ',';
                    }
                });
                $.each(arrGroupIds, function (index, value) {
                    if (value != "" && value != ',') {
                        temp_SelectedGroupId = temp_SelectedGroupId + ',' + value + ',';
                    }
                });

                $('#hdnGroupIds').val(temp_SelectedGroupId);

                if (indexVASID == -1) {
                    $('#SelectedVasIDs').val(temp_SelectedVASIDs + ',' + curCompId + ',');
                }
                if (indexVASKenanCode == -1) {
                    $('#SelectedKenanCodes').val(temp_SelectedKenanCodes + ',' + curKenanCode + ',');
                }

                if ($('#' + curBtnId).hasClass('selected-vas') == false) {
                    $('#' + curBtnId).addClass('selected-vas');
                    $('#' + mdpId).removeClass('selected-vas');
                }

            }
        }
    });
}