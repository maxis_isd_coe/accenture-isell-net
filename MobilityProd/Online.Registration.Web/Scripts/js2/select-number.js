﻿
$(document).ready(function () {
    clickNum();
    clickSim();
});
function clickNum() {
    $('.no-list').on('click', function (e) {
        e.preventDefault();
        $('.no-list').removeClass('clicked');
        $('.no-list').removeClass('active');
        $(this).button('toggle').addClass('clicked');
    });
}


function clickSim() {
    $('.sim-list').on('click', function (e) {
        e.preventDefault();
        $('.sim-list').removeClass('clicked');
        $(this).button('toggle').addClass('clicked');
    });
}

