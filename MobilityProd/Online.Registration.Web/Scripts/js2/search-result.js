
$(document).ready(function () {
    isellPanel();
    accountSts();
    //usagePattern();
});
var settings = {

    // Settings.
    pegaRecommend_postURL: GetPath('/Home/GetRecommendations'),
    pegaRecommend_timeout: 10000
    
};
function isellPanel() {
    $('.panel-isell a').on('click', function (e) {
        e.preventDefault();
        if ($(this).children(".arrow").hasClass('arrow-expand')) {
            $(".arrow").removeClass('arrow-expand');
            $(this).children(".arrow").removeClass('arrow-expand');

        } else {
            $(".arrow").removeClass('arrow-expand');
            $(this).children(".arrow").addClass('arrow-expand');
        }
    });
    $('.more-details').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('more-details-active')) {
            $(this).removeClass('more-details-active');
        } else {
            $(this).addClass('more-details-active');
        }
    });

    $('.view-more-checkbox').on('click', function (e) {
        e.preventDefault();
        //debugger;
        if ($(this).hasClass('view-more-checkbox-active')) {
            $(".view-more-checkbox").removeClass('view-more-checkbox-active');
            $(this).removeClass('view-more-checkbox-active');

        } else {
            $(".view-more-checkbox").removeClass('view-more-checkbox-active');
            $(this).addClass('view-more-checkbox-active');
            //getRecommendation(this);
            //openPopUp(this);
        }
    });
}

function openPopUp(obj) {
    var extID = $(obj).attr('data-msisdn-checkbox');
    var subscriberno = $(obj).attr('data-s-checkbox');
    console.log(extID + ' ' + subscriberno)
    $('#pegaRecommendations_' + extID).click();
}

function showAllOffer(offerList, showAllText) {
//    $('#' + offerList).children().show();
    $('#allOfferRbList').show();
    $('#topOfferRbList').show();
    $('#noOfferText').hide();
    $('#' + showAllText).hide();
}

function resetOfferList() {
    //$('[id^=pega-offer-list-]').children().slice(10).hide();
    $('#allOfferRbList').hide();
    $('[id^=full-pega-offer-list-]').show();
}

function responseOfferList(obj, extID, actionButton) {
    var updateURL = GetPath('/Home/UpdateResponse');
    var btnID = $(obj).attr('id');
    var rbID = $('[id^=rbOffer-]:checked').attr('id');
    var offerName = $('#' + rbID).attr('data-offername');
    var caseCategory = $('[id^=pega-offer-list-]').attr('data-casecategory');
    var decisionID = $('[id^=pega-offer-list-]').attr('data-decisionid');

    var msisdn = rbID.toString().split('-')[1]; // rbOffer-60162456338-44890
    var kenanCode = rbID.toString().split('-')[2]; // rbOffer-60162456338-44890
    var prepID = $('#' + rbID).attr('data-prepID');
    console.log(extID + ' ' + kenanCode + ' ' + prepID + ' ' + offerName + ' ' + actionButton);

    $.ajax({
        url: updateURL,
        type: "POST",
        cache: false,
        data: { 
            externalID: extID,
            kenanCode: kenanCode,
            pegaPropID: prepID,
            action: actionButton,
            caseCategory: caseCategory 
        },
        success: function (data) {
            $('#pega-offer-list-' + extID).html(data);
            resetOfferList();
        },
        error: function (xhr, err) {
            alert('There was a technical error during recommendation retrieval \n'
                + 'If the issue still persist, please check your network connection / refresh the page / restart iSELL application');
            ///WHOEVER INTEGRATES THE CODE PLEASE UNCOMMENT THE BELOW CODE IN CASE OF UNEXPECTED RESULTS.                        
            //alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
            //alert("responseText: " + xhr.responseText);
        }
    });

}


function refreshOfferList(obj, extID) {
    //var selectedCategory = $('#selectedOfferList :selected').text();
    var selectedCategory = $('#othRecList-btn').text().trim();
    var reAssessURL = GetPath('/Home/ReassessRecommendations');
    console.log(selectedCategory + ' ' + extID);

    $.ajax({
        url: reAssessURL,
        type: "POST",
        cache: false,
        data: { externalID: extID, caseCategory: selectedCategory },
        success: function (data) {
            $('#pega-offer-list-' + extID).html(data);
            resetOfferList();
            $('[id^=pega-offer-list-]').attr('data-caseCategory', selectedCategory);
        },
        error: function (xhr, err) {
            alert('There was a technical error during recommendation retrieval \n'
                + 'If the issue still persist, please check your network connection / refresh the page / restart iSELL application');
            ///WHOEVER INTEGRATES THE CODE PLEASE UNCOMMENT THE BELOW CODE IN CASE OF UNEXPECTED RESULTS.                        
            //alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
            //alert("responseText: " + xhr.responseText);
        }
    });
}




function accountSts() {



    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/i)) {
        $(".status-red").click(function (e) {
            $('.tooltips-error').toggle();
        });
      }else{
        $(".status-red").hover(
	      function () {
	          $('.tooltips-error').show();
	      },
          function () {
              $('.tooltips-error').hide();
          }
	    );
      }

}

function usagePattern() {
    $(".usage-summary-btn").click(function () {
        $("#toggleUsage").collapse('toggle');
        $(".arrow-icon-active").toggle();
        $(".arrow-icon").toggle();
        //$(".arrow-icon").addClass("arrow-icon-active");
    });
    //$('#UsageModal').modal('show') ;	
}
function portraitIpad() {
    //
}
function landscapeIpad() {

}
function initBottomClass() {
    var $containerWidth = $(window).width();
    if ($containerWidth >= 1024) {
        //landscapeIpad();
    }
    if ($containerWidth <= 1024) {
        // portraitIpad();
    }
}
window.addEventListener('resize', function (event) {
    if (navigator.platform === 'iPad') {
        window.onorientationchange = function () {

            var orientation = window.orientation;

            // Look at the value of window.orientation:
            if (orientation === 0) {
                // iPad is in Portrait mode.

                portraitIpad();

            } else if (orientation === 90) {

                landscapeIpad();
                // iPad is in Landscape mode. The screen is turned to the left.

            } else if (orientation === -90) {
                // iPad is in Landscape mode. The screen is turned to the right.

                landscapeIpad();
            } else if (orientation === 180) {
                // Upside down portrait.

                portraitIpad();
            }
        }
    }
});

$(window).resize(function (e) {
    initBottomClass();
});


/*Browser detection patch*/

if (!jQuery.browser) {

    jQuery.browser = {};
    jQuery.browser.mozilla = false;
    jQuery.browser.webkit = false;
    jQuery.browser.opera = false;
    jQuery.browser.safari = false;
    jQuery.browser.chrome = false;
    jQuery.browser.msie = false;
    jQuery.browser.android = false;
    jQuery.browser.blackberry = false;
    jQuery.browser.ios = false;
    jQuery.browser.operaMobile = false;
    jQuery.browser.windowsMobile = false;
    jQuery.browser.mobile = false;

    var nAgt = navigator.userAgent;
    jQuery.browser.ua = nAgt;

    jQuery.browser.name = navigator.appName;
    jQuery.browser.fullVersion = '' + parseFloat(navigator.appVersion);
    jQuery.browser.majorVersion = parseInt(navigator.appVersion, 10);
    var nameOffset, verOffset, ix;

    // In Opera, the true version is after "Opera" or after "Version"
    if ((verOffset = nAgt.indexOf("Opera")) != -1) {
        jQuery.browser.opera = true;
        jQuery.browser.name = "Opera";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 6);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
    }

    // In MSIE < 11, the true version is after "MSIE" in userAgent
    else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
        jQuery.browser.msie = true;
        jQuery.browser.name = "Microsoft Internet Explorer";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 5);
    }

    // In TRIDENT (IE11) => 11, the true version is after "rv:" in userAgent
    else if (nAgt.indexOf("Trident") != -1) {
        jQuery.browser.msie = true;
        jQuery.browser.name = "Microsoft Internet Explorer";
        var start = nAgt.indexOf("rv:") + 3;
        var end = start + 4;
        jQuery.browser.fullVersion = nAgt.substring(start, end);
    }

    // In Chrome, the true version is after "Chrome"
    else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
        jQuery.browser.webkit = true;
        jQuery.browser.chrome = true;
        jQuery.browser.name = "Chrome";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 7);
    }
    // In Safari, the true version is after "Safari" or after "Version"
    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
        jQuery.browser.webkit = true;
        jQuery.browser.safari = true;
        jQuery.browser.name = "Safari";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 7);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
    }
    // In Safari, the true version is after "Safari" or after "Version"
    else if ((verOffset = nAgt.indexOf("AppleWebkit")) != -1) {
        jQuery.browser.webkit = true;
        jQuery.browser.name = "Safari";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 7);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
    }
    // In Firefox, the true version is after "Firefox"
    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
        jQuery.browser.mozilla = true;
        jQuery.browser.name = "Firefox";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
    }
    // In most other browsers, "name/version" is at the end of userAgent
    else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
        jQuery.browser.name = nAgt.substring(nameOffset, verOffset);
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 1);
        if (jQuery.browser.name.toLowerCase() == jQuery.browser.name.toUpperCase()) {
            jQuery.browser.name = navigator.appName;
        }
    }

    /*Check all mobile environments*/
    jQuery.browser.android = (/Android/i).test(nAgt);
    jQuery.browser.blackberry = (/BlackBerry/i).test(nAgt);
    jQuery.browser.ios = (/iPhone|iPad|iPod/i).test(nAgt);
    jQuery.browser.operaMobile = (/Opera Mini/i).test(nAgt);
    jQuery.browser.windowsMobile = (/IEMobile/i).test(nAgt);
    jQuery.browser.mobile = jQuery.browser.android || jQuery.browser.blackberry || jQuery.browser.ios || jQuery.browser.windowsMobile || jQuery.browser.operaMobile;


    // trim the fullVersion string at semicolon/space if present
    if ((ix = jQuery.browser.fullVersion.indexOf(";")) != -1)
        jQuery.browser.fullVersion = jQuery.browser.fullVersion.substring(0, ix);
    if ((ix = jQuery.browser.fullVersion.indexOf(" ")) != -1)
        jQuery.browser.fullVersion = jQuery.browser.fullVersion.substring(0, ix);

    jQuery.browser.majorVersion = parseInt('' + jQuery.browser.fullVersion, 10);
    if (isNaN(jQuery.browser.majorVersion)) {
        jQuery.browser.fullVersion = '' + parseFloat(navigator.appVersion);
        jQuery.browser.majorVersion = parseInt(navigator.appVersion, 10);
    }
    jQuery.browser.version = jQuery.browser.majorVersion;

    if ((jQuery.browser.name == "Microsoft Internet Explorer" && jQuery.browser.version == 9) || (jQuery.browser.name == "Microsoft Internet Explorer" && jQuery.browser.version == 10)) {
        jQuery('img').removeClass('img-responsive');
    }
}