﻿// this file is created on Drop 4 for submit page, where need imei & sim button script.
// 27-Oct-14 // Create IMEI & SIM button for Supervisor Dealer.

var IMEIMin = 14;

function SIMWindowOnOpen() {
    $('#simWindow').data('tWindow').refresh();
}

function IMEIWindowOnOpen() {
    $('#imeiWindow').data('tWindow').refresh();
}

//function SIMWindowOnOpen() {
//    $('#simWindow').data('tWindow').refresh();
//    $('#dvSimSerial_Error').html("");
//    $('#CapturedSIMSerial').val($('#RegSIMSerial').val());
//}
function SecondarySIMWindowOnOpen() {
    $('#SecondarySIMWindow').data('tWindow').refresh();
    $('#CapturedSIMSerial1').val($('#RegSIMSerialSeco').val());
}

// for supplementary

function SuppSIMWindowOnOpen() {
    $('#suppsimWindow').data('tWindow').refresh();
}

function SuppIMEIWindowOnOpen() {
    $('#tdSupIMEISerial_Error').text('');
    $('#suppIMEIWindow').data('tWindow').refresh();
}


function SIMTypeWindowOnOpen() {
    $('#dvSimSerial_Error').html("");
    $('#simTypeWindow').data('tWindow').refresh();
}

function accessoryWindowOnOpen() {
    $('#accessoryWindow').data('tWindow').refresh();
}
