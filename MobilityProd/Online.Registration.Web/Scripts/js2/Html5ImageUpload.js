﻿function startProcess(fileOrUrl, file, type) {
    var canvas = scaleAndCropImage(fileOrUrl);
    var blob = convertCanvasToBlob(canvas);
    var uploadResult = realUploadImage(blob, file, type);
}

// Wait 30 seconds before timeout.
var MAXIMUM_WAITING_TIME = 60000;

var img_settings = {

    // img_settings
    postUrl: GetPath('/Registration/UploadFiles'),
    cropRatio: 800 / 600,
    maxLength: 800,
    imageUrl: "image",
    watermarkText : "For Maxis use only",

    // Callbacks.
    onDropped: null /* Returns boolean (true = accepted, type is image) */,
    onProcessed: null /* Returns Canvas(success) or null */,
    onUploaded: null /* Returns boolean (true = success), responseText */,
    onUploadProgress: null /* Progress during upload, in percentage .*/
};

function scaleAndCropImage(img) {
    //debugger;
    var cropRatio = img_settings.cropRatio;
    var originalWidth = img.width;
    var originalHeight = img.height;

    // 90 degrees CW or CCW, flip width and height.
    var $img = $(img);
    var orientation = $img.data('orientation') || 0;
    switch (orientation) {
        case 5:
        case 6:
        case 7:
        case 8:
            cropRatio = 1 / cropRatio;
            break;
        default:
    }

    // Calculate width and height based on desired X/Y ratio.
//    var ret = determineCropWidthAndHeight(cropRatio, originalWidth, originalHeight);
    var cropWidth = originalWidth;
    var cropHeight = originalHeight;

    // Determine if longest side exceeds max length.
    ret = determineScaleWidthAndHeight(img_settings.maxLength, originalWidth, originalHeight);
    var scaleWidth = ret.width;
    var scaleHeight = ret.height;
    var scaleRatio = cropWidth / scaleWidth;

    // Crop and scale.
    var x = -1 * (Math.round(((originalWidth - cropWidth) / 2) / scaleRatio));
    var y = -1 * (Math.round(((originalHeight - cropHeight) / 2) / scaleRatio));
    x = Math.min(0, x);
    y = Math.min(0, y);
    var w = Math.round(originalWidth / scaleRatio);
    var h = Math.round(originalHeight / scaleRatio);

    var canvas = document.createElement("canvas");

    // Bepaal de breedte an hoogte, gebaseerd op orientatie.
    switch (orientation) {
        case 5:
        case 6:
        case 7:
        case 8:
            canvas.width = scaleHeight;
            canvas.height = scaleWidth;
            break;
        default:
            canvas.width = scaleWidth;
            canvas.height = scaleHeight;
    }

//    var container = document.getElementById("pageContainer")
//    var origCanvas = document.getElementById("page1");

//    if (container.firstChild)
//        container.insertBefore(canvas, container.firstChild);
//    else
//        container.appendChild(canvas);

    var ctx = canvas.getContext("2d");
    if (orientation) {
        // Transform canvas coordination according to specified frame size and orientation.
        transformCoordinate(ctx, orientation, scaleWidth, scaleHeight);
    }

    // For now just a white background, in the future possibly background color based on dominating image color?
    ctx.fillStyle = "rgba(255,255,255, 0)";
    ctx.fillRect(0, 0, scaleWidth, scaleHeight);
    ctx.drawImage(img, x, y, w, h);
    
    // Try to fix IOS6s image squash bug.
    // Test for transparency. This trick only works with JPEGs.
    var iOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/i);
    if (iOS && $img.data('fileType') == 'image/jpeg') {
        var transparent = detectTransparency(ctx);
        if (transparent) {
            // Redraw image, doubling the height seems to fix the iOS6 issue.
            ctx.drawImage(img, x, y, w, h * 2.041);
        }
    }
    
    ctx.globalAlpha = 0.5;
    ctx.font = "bold 42px Verdana";
    ctx.fillStyle = "black";
    var metrics = ctx.measureText("For Maxis use only");
    var metWidth = metrics.width;
    var metHeight = 50;
    // change the origin coordinate to the middle of the context
    ctx.translate(w / 2, h / 2);
    // rotate the context (so it's rotated around its center)
    ctx.rotate(-Math.atan(canvas.height / canvas.width));
    ctx.fillText("For Maxis use only", -metWidth / 2, metHeight / 2);

    // Notify listeners of scaled and cropped image.
    img_settings.onProcessed && img_settings.onProcessed(canvas);

    return canvas;
}

/**
* Determine max width and height based on fixed x/y ratio.
*
* @param ratio X / Y
* @param width Original width
* @param height Original height
* @return object new width and height (input for cropping)
*/
function determineCropWidthAndHeight(ratio, width, height) {

    var currentRatio = width / height;
    if (currentRatio != ratio) {
        if (currentRatio > ratio) {
            // Cut x
            width = height * ratio;
        } else {
            // Cut y
            height = width / ratio;
        }
    }

    return { width: Math.round(width), height: Math.round(height) };
}

/**
* Determine max width and height of image.
*
* @param maxLength Max length
* @param width Originele breedte
* @param height Originele hoogte
* @return object width en height
*/
function determineScaleWidthAndHeight(maxLength, width, height) {

    if (width > height) {
        if (width > maxLength) {
            height *= maxLength / width;
            width = maxLength;
        }
    } else {
        if (height > maxLength) {
            width *= maxLength / height;
            height = maxLength;
        }
    }
    return { width: Math.round(width), height: Math.round(height) };
}

/**
* Transform canvas coordination according to specified frame size and orientation.
* Orientation value is from EXIF tag.
*
* @param ctx HTMLCanvasElement.context Canvas
* @param orientation EXIF orientation code
* @param width Number Width
* @param height Number Height
*/
function transformCoordinate(ctx, orientation, width, height) {
    switch (orientation) {
        case 1:
            // nothing
            break;
        case 2:
            // horizontal flip
            ctx.translate(width, 0);
            ctx.scale(-1, 1);
            break;
        case 3:
            // 180 rotate left
            ctx.translate(width, height);
            ctx.rotate(Math.PI);
            break;
        case 4:
            // vertical flip
            ctx.translate(0, height);
            ctx.scale(1, -1);
            break;
        case 5:
            // vertical flip + 90 rotate right
            ctx.rotate(0.5 * Math.PI);
            ctx.scale(1, -1);
            break;
        case 6:
            // 90 rotate right
            ctx.rotate(0.5 * Math.PI);
            ctx.translate(0, -height);
            break;
        case 7:
            // horizontal flip + 90 rotate right
            ctx.rotate(0.5 * Math.PI);
            ctx.translate(width, -height);
            ctx.scale(-1, 1);
            break;
        case 8:
            // 90 rotate left
            ctx.rotate(-0.5 * Math.PI);
            ctx.translate(-width, 0);
            break;
        default:
            break;
    }
}

/**
* Detect transparency.
* Fixes a bug which squash image vertically while drawing into canvas for some images.
*
* @param ctx HTMLCanvasElement Canvas
* @return Boolean True als transparent
*/
function detectTransparency(ctx) {
    var canvas = ctx.canvas;
    var height = canvas.height;

    // Returns pixel data for the specified rectangle.
    var data = ctx.getImageData(0, 0, 1, height).data;

    // Search image edge pixel position in case it is squashed vertically.
    for (var i = 0; i < height; i++) {
        var alphaPixel = data[(i * 4) + 3];
        if (alphaPixel == 0) {
            return true;
        }
    }
    return false;
}

/**
* Convert canvas contents to Blob object.
*
* @param canvas Canvas element
* @return Blob object ( https://developer.mozilla.org/en-US/docs/DOM/Blob )
*/
function convertCanvasToBlob(canvas) {
    //debugger;
    if (canvas.mozGetAsFile) {
        // Mozilla implementation (File extends Blob).
        return canvas.mozGetAsFile(null, 'image/jpeg', '0.9');
    } else if (canvas.toBlob) {
        // HTML5 implementation.
        // https://developer.mozilla.org/en/DOM/HTMLCanvasElement
        return canvas.toBlob(null, 'image/jpeg', '0.9');
    } else {
        // WebKit implementation.
        // http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
        return createBlobFromDataUri(canvas.toDataURL('image/jpeg', '0.9'));
    }
}

/**
* Convert WebKit dataURI to Blob.
*/
function createBlobFromDataUri(dataURI) {

    // Convert base64/URLEncoded data component to raw binary data held in a string
    var splitString = dataURI.split(',');
    var splitStringMime = splitString[0];
    var splitStringData = splitString[1];

    var byteString;
    if (splitStringMime.indexOf('base64') >= 0) {
        byteString = atob(splitStringData);
    } else {
        byteString = decodeURIComponent(splitStringData);
    }

    // separate out the mime component
    var mimeString = splitStringMime.split(':')[1].split(';')[0];

    // Write the bytes of the string to an ArrayBuffer
    var length = byteString.length;
    var buf = new ArrayBuffer(length);
    var view = new Uint8Array(buf);
    for (var i = 0; i < length; i++) {
        view[i] = byteString.charCodeAt(i);
    }

    // Detect if Blob object is supported.
    if (typeof Blob !== 'undefined') {
        return new Blob([buf], { type: mimeString });

    } else {
        window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder || window.MSBlobBuilder;
        var bb = new BlobBuilder();
        bb.append(buf);
        return bb.getBlob(mimeString);
    }
}

/**
* Upload image data to server.
*
* @param blob Image as blob
*/
function realUploadImage(blob, file, type) {
    // Start upload.
    //debugger;
    var uploadResult = '';
    $.blockUI();
    $.ajax({
        url: img_settings.postUrl,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Content-Type", "multipart/form-data");
            xhr.setRequestHeader("X-File-Name", file.name);
            xhr.setRequestHeader("X-File-Size", file.size);
            xhr.setRequestHeader("X-File-Type", file.type);
            xhr.setRequestHeader("X-Type", type);
        },
        async: true,
        type: "POST",
        data: blob,
        processData: false,
        contentType: false,
        success: function (Data) {
            //debugger;
            uploadResult = Data;
            if (uploadResult != 'FAILED') { // 'FAILED' used on Html5ImageUpload.js
                populateToHiddenInput(uploadResult, file, type);
                $.unblockUI();
            } else {
                alert('There was a technical error during document upload, please retry to upload the document.\n'
                + 'If the issue still persist, please check your network connection / refresh the page / restart iSELL application');
            }
        },
        error: function (xhr, err) {
            alert('There was a technical error during document upload, please retry to upload the document.\n'
                + 'If the issue still persist, please check your network connection / refresh the page / restart iSELL application');
            ///WHOEVER INTEGRATES THE CODE PLEASE UNCOMMENT THE BELOW CODE IN CASE OF UNEXPECTED RESULTS.       
            //alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
            //alert("responseText: "+xhr.responseText);
        }
    });
    
    if (uploadResult == '') {
        uploadResult = 'FAILED'; // used on _UploadDocuments.cshtml
    }

    return uploadResult;
}


function getFileExtension(type) {
    switch (type.toUpperCase().trim()) {
        case 'FRONT': return $('#idDocumentFront').val().split('.').pop().toLowerCase();
            break;
        case 'BACK': return $('#idDocumentBack').val().split('.').pop().toLowerCase();
            break;
        case 'OTHER': return $('#otherDocuments').val().split('.').pop().toLowerCase();
            break;
        case 'THIRD': return $('#thirdPartyFile').val().split('.').pop().toLowerCase();
            break;

        default: return '';
    }
}

function populateToHiddenInput(uploadResult, file, type) {
    type = type.toUpperCase().trim();
    if (type == 'FRONT') {
        //$("#idDocumentFront").attr("src", uploadResult).fadeIn();
        $("#idDocumentFront").data("name", file.name);
        //$("#frontPhoto").val(uploadResult);
        $('#frontFileName-container').css('display', 'block');
        $('#frontFileName').text(file.name);
        $('#frontPhotoFileName').val(file.name);
        $("#submitDocument").show();
    }

    if (type == 'BACK') {
        //$("#idDocumentBack").attr("src", uploadResult).fadeIn();
        $("#idDocumentBack").data("name", file.name);
        //$("#backPhoto").val(uploadResult);
        $('#backFileName-container').css('display', 'block');
        $('#backFileName').text(file.name);
        $('#backPhotoFileName').val(file.name);
    }

    if (type == 'OTHER') {
        //$("#otherDocuments").attr("src", uploadResult).fadeIn();
        $("#otherDocuments").data("name", file.name);
        //$("#otherPhoto").val(uploadResult);
        $('#otherPhotoFileName-container').css('display', 'block');
        $('#otherPhotoFileName').text(file.name);
        $('#otherFileName').val(file.name);
    }

    if (type == 'THIRD') {
        //$("#thirdPartyFile").attr("src", uploadResult).fadeIn();
        $("#thirdPartyFile").data("name", file.name);
        //$("#thirdPartyPhoto").val(uploadResult);
        $('#thirdPartyFileName-container').css('display', 'block');
        $('#thirdPartyFileNameLabel').text(file.name);
        $('#thirdPartyFileName').val(file.name);
    }

    if (type == 'CRP_IMG_UPLOAD'){
        $("#imgCustPhoto").attr("src", uploadResult).fadeIn();
        $("#imgCustPhoto").data("name", file.name);
        //$("#CustomerPhoto").val(uploadResult);
        $("#lblCustImageUpload").html(file.name);
        $("#frontDocumentName").val(file.name);
        $("#RemoveCustImageUpload").show();
    }

    if (type == 'CRP_IMG_UPLOAD_2'){
        $("#imgAltCustPhoto").attr("src", uploadResult).fadeIn();
        $("#imgAltCustPhoto").data("name", file.name);
        //$("#AltCustomerPhoto").val(uploadResult);
        $("#lblCustImageUpload2").html(file.name);
        $("#backDocumentName").val(file.name);
        $("#RemoveCustImageUpload2").show();
    }

    if (type == 'CRP_IMG_UPLOAD_3'){
        $("#imgPhoto").attr("src", uploadResult).fadeIn();
        $("#imgPhoto").data("name", file.name);
        //$("#Photo").val(uploadResult);
        $("#lblCustImageUpload3").html(file.name);
        $("#othDocumentName").val(file.name);
        $("#RemoveCustImageUpload3").show();
    }   

}