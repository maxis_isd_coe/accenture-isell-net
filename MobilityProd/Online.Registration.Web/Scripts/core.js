﻿function ValidateIDCardNo(val) {
    return val.length != 12 ? false : true;
}

function ValidateMSISDN(val) {

    var mobregex = /^60[0-9]{8,10}$/;

    if (!(val.length >= 11 && val.length <= 12)) {
        return 'MSISDN must be of 11 or 12 numbers.';
    }

    if (MSISDN.substring(0, 3) != "601") {
        return 'MSISDN numbers should start with series 601.';
    }

    if (!mobregex.test(val)) {
        return 'Please enter valid MSISDN number. eg. 60xxxxxxxxx';
    }

    return "";
}


$(document).ready(function () {
    console.log('started');
});