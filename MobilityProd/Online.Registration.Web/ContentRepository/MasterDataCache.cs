﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Online.Registration.Web.Helper;
using Online.Registration.DAL.Models;
using Online.Registration.Web.RegistrationSvc;
using Online.Registration.Web.ConfigSvc;
using Online.Registration.Web.CatalogSvc;
using Online.Registration.Web.OrganizationSvc;
using Online.Registration.Web.ComplainSvc;
using Online.Registration.Web.UserSvc;
using Online.Registration.Web.Models;
using log4net;
using System.Xml.Linq;
using System.Configuration;
using Online.Registration.Web.CommonEnum;
using SNT.Utility;

namespace Online.Registration.Web.ContentRepository
{
    public class MasterDataCache
    {
        private static object lockObject = new object();
        private static MasterDataCache instance;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MasterDataCache));
        private MasterDataCache()
        {

        }
        public static MasterDataCache Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MasterDataCache();
                }
                return instance;
            }
        }
        public void InitializeCache()
        {
            // Pradeep: Add service call cache items that require to be Cached at applciation start
            var packageComponents = Instance.PackageComponents;
            packageComponents = null;
            //  var orgStocks = Instance.OrganizationStocks;
            // orgStocks = null;
            var waiverRules = Instance.WaiverRulesInfo;
            waiverRules = null;
        }

        public void InitializeCacheManual(CacheModels CacheModels)
        {

            if (CacheModels.Access == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Access);
                var value = Instance.Access;
                value = null;
            }
            if (CacheModels.AccountCategory == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.AccountCategory);
                var value = Instance.AccountCategory;
                value = null;
            }
            if (CacheModels.Address == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.AddressType);
                var value = Instance.Address;
                value = null;
            }
            if (CacheModels.Article == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Article);
                var value = Instance.Article;
                value = null;
            }
            if (CacheModels.Brand == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Brand);
                var value = Instance.Brand;
                value = null;
            }
            if (CacheModels.Bundle == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Bundle);
                var value = Instance.Bundle;
                value = null;
            }
            if (CacheModels.CardType == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.CardType);
                var value = Instance.CardType;
                value = null;
            }
            if (CacheModels.Category == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Category);
                var value = Instance.Category;
                value = null;
            }
            if (CacheModels.Colour == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Colour);
                var value = Instance.Colour;
                value = null;
            }
            if (CacheModels.ComplaintIssues == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.ComplaintIssues);
                var value = Instance.ComplaintIssues;
                value = null;
            }
            if (CacheModels.ComponentType == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.ComponentType);
                var value = Instance.ComponentType;
                value = null;
            }
            if (CacheModels.Country == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Country);
                var value = Instance.Country;
                value = null;
            }
            if (CacheModels.CustomerInfoCardType == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.CustomerInfoCardType);
                var value = Instance.CustomerInfoCardType;
                value = null;
            }
            if (CacheModels.CustomerTitle == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.CustomerTitle);
                var value = Instance.CustomerTitle;
                value = null;
            }
            if (CacheModels.Doners == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Doners);
                var value = Instance.Doners;
                value = null;
            }
            if (CacheModels.ExternalIDType == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.ExternalIDType);
                var value = Instance.ExternalIDType;
                value = null;
            }
            if (CacheModels.IDCardType == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.IDCardType);
                var value = Instance.IDCardType;
                value = null;
            }
            if (CacheModels.Language == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Language);
                var value = Instance.Language;
                value = null;
            }
            if (CacheModels.Market == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Market);
                var value = Instance.Market;
                value = null;
            }
            if (CacheModels.Model == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Model);
                var value = Instance.Model;
                value = null;
            }
            if (CacheModels.ModelGroup == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.ModelGroup);
                var value = Instance.ModelGroup;
                value = null;
            }
            if (CacheModels.Nationality == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Nationality);
                var value = Instance.Nationality;
                value = null;
            }
            if (CacheModels.Organization == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Organization);
                var value = Instance.Organization;
                value = null;
            }
            if (CacheModels.OrganizationStocks == true)
            {
                CacheLayer.ClearFromApplication(CacheLayer.DataKey.OrganizationStocks);
                var value = Instance.OrganizationStocks;
                value = null;
            }
            if (CacheModels.OrgType == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.OrgType);
                var value = Instance.OrgType;
                value = null;
            }
            if (CacheModels.Package == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Package);
                var value = Instance.Package;
                value = null;
            }
            if (CacheModels.PackageType == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.PackageType);
                var value = Instance.PackageType;
                value = null;
            }
            if (CacheModels.PaymentMode == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.PaymentMode);
                var value = Instance.PaymentMode;
                value = null;
            }
            if (CacheModels.Program == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Program);
                var value = Instance.Program;
                value = null;
            }
            if (CacheModels.Property == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Property);
                var value = Instance.Property;
                value = null;
            }
            if (CacheModels.Race == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Race);
                var value = Instance.Race;
                value = null;
            }
            if (CacheModels.RegType == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.RegType);
                var value = Instance.RegType;
                value = null;
            }
            if (CacheModels.SimModelType == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.SimModelType);
                var value = Instance.SimModelType;
                value = null;
            }
            if (CacheModels.SimReplacementprePaidReason == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.SimReplacementprePaidReason);
                var value = Instance.SimReplacementprePaidReason;
                value = null;
            }
            if (CacheModels.SimReplacementReason == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.SimReplacementReason);
                var value = Instance.SimReplacementReason;
                value = null;

            }
            if (CacheModels.State == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.State);
                var value = Instance.State;
                value = null;
            }
            if (CacheModels.Status == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.Status);
                var value = Instance.Status;
                value = null;
            }
            if (CacheModels.StatusReason == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.StatusReason);
                var value = Instance.StatusReason;
                value = null;
            }
            if (CacheModels.StatusType == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.StatusType);
                var value = Instance.StatusType;
                value = null;
            }
            if (CacheModels.ThirdPartyAuthType == true)
            {
                CacheLayer.Clear(CacheLayer.DataKey.ThirdPartyAuthType);
                var value = Instance.ThirdPartyAuthType;
                value = null;
            }
        }

        public static void RefreshCacheItems(String k, Object v, System.Web.Caching.CacheItemRemovedReason r)
        {
            // Pradeep: Add service call cache items that require to be updated after a certain time ( When cache expires)

            if (k.Equals(CacheLayer.DataKey.OrganizationStocks.ToString() + "ExpiryFlag"))
            {
                Logger.Info("OrganizationStocks are being reloaded after cache expiration");
                var orgStocks = MasterDataCache.Instance.OrganizationStocks;
                orgStocks = null;
            }


        }


        #region "RegistrationServiceProxy Section"

        public AddressTypeGetResp Address
        {
            get
            {

                AddressTypeGetResp resultList = CacheLayer.Get<AddressTypeGetResp>(CacheLayer.DataKey.AddressType);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.AddressTypeList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<AddressTypeGetResp>(CacheLayer.DataKey.AddressType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }

        }

        public IEnumerable<BrandArticle> Article
        {
            get
            {
                var resultList = CacheLayer.Get<IEnumerable<BrandArticle>>(CacheLayer.DataKey.Article);
                try
                {
                    if (resultList == null)
                    {
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.GetAllArticleIDs().ArticleIDs;
                        }
                        CacheLayer.Add<IEnumerable<BrandArticle>>(CacheLayer.DataKey.Article, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }

        }

        public CardTypeGetResp CardType
        {
            get
            {
                CardTypeGetResp resultList = CacheLayer.Get<CardTypeGetResp>(CacheLayer.DataKey.CardType);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.CardTypeList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<CardTypeGetResp>(CacheLayer.DataKey.CardType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;

            }
        }

        public CountryGetResp Country
        {
            get
            {

                CountryGetResp resultList = CacheLayer.Get<CountryGetResp>(CacheLayer.DataKey.Country);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.CountryList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<CountryGetResp>(CacheLayer.DataKey.Country, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;

            }
        }

        public CustomerTitleGetResp CustomerTitle
        {
            get
            {
                CustomerTitleGetResp resultList = CacheLayer.Get<CustomerTitleGetResp>(CacheLayer.DataKey.CustomerTitle);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.CustomerTitleList();
							resultList.CustomerTitles.Where(x => x.Active).ToList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<CustomerTitleGetResp>(CacheLayer.DataKey.CustomerTitle, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;

            }
        }

        public IDCardTypeGetResp IDCardType
        {
            get
            {

                IDCardTypeGetResp resultList = CacheLayer.Get<IDCardTypeGetResp>(CacheLayer.DataKey.IDCardType);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.IDCardTypeList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<IDCardTypeGetResp>(CacheLayer.DataKey.IDCardType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }

                return resultList;
            }
        }

        public LanguageGetResp Language
        {
            get
            {

                LanguageGetResp resultList = CacheLayer.Get<LanguageGetResp>(CacheLayer.DataKey.Language);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.LanguageGetList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<LanguageGetResp>(CacheLayer.DataKey.Language, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;

            }
        }

        public NationalityGetResp Nationality
        {
            get
            {
                NationalityGetResp resultList = CacheLayer.Get<NationalityGetResp>(CacheLayer.DataKey.Nationality);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.NationalityList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<NationalityGetResp>(CacheLayer.DataKey.Nationality, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }

                return resultList;
            }
        }

        public PaymentModeGetResp PaymentMode
        {
            get
            {

                PaymentModeGetResp resultList = CacheLayer.Get<PaymentModeGetResp>(CacheLayer.DataKey.PaymentMode);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.PaymentModeList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<PaymentModeGetResp>(CacheLayer.DataKey.PaymentMode, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public RaceGetResp Race
        {
            get
            {
                RaceGetResp resultList = CacheLayer.Get<RaceGetResp>(CacheLayer.DataKey.Race);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.RaceList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<RaceGetResp>(CacheLayer.DataKey.Race, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public StateGetResp State
        {
            get
            {
                StateGetResp resultList = CacheLayer.Get<StateGetResp>(CacheLayer.DataKey.State);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.StateList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<StateGetResp>(CacheLayer.DataKey.State, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public RegTypeGetResp RegType
        {
            get
            {

                RegTypeGetResp resultList = CacheLayer.Get<RegTypeGetResp>(CacheLayer.DataKey.RegType);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.RegTypeList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<RegTypeGetResp>(CacheLayer.DataKey.RegType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public CardTypePaymentDetailsGetResp CustomerInfoCardType
        {
            get
            {

                CardTypePaymentDetailsGetResp resultList = CacheLayer.Get<CardTypePaymentDetailsGetResp>(CacheLayer.DataKey.CustomerInfoCardType);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.CardTypePaymentList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<CardTypePaymentDetailsGetResp>(CacheLayer.DataKey.CustomerInfoCardType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public ExtIDTypeGetResp ExternalIDType
        {
            get
            {
                ExtIDTypeGetResp resultList = CacheLayer.Get<ExtIDTypeGetResp>(CacheLayer.DataKey.ExternalIDType);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.ExtIDTypeList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<ExtIDTypeGetResp>(CacheLayer.DataKey.ExternalIDType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public VIPCodeGetResp VIPCode
        {
            get
            {
                VIPCodeGetResp resultList = CacheLayer.Get<VIPCodeGetResp>(CacheLayer.DataKey.VIPCode);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.VIPCodeList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<VIPCodeGetResp>(CacheLayer.DataKey.VIPCode, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        #endregion

        #region "ConfigServiceProxy Section"
        public AccountCategoryGetResp AccountCategory
        {
            get
            {

                AccountCategoryGetResp resultList = CacheLayer.Get<AccountCategoryGetResp>(CacheLayer.DataKey.AccountCategory);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new ConfigServiceProxy())
                        {
                            resultList = proxy.AccountCategoryList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<AccountCategoryGetResp>(CacheLayer.DataKey.AccountCategory, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }

        }
        public MarketGetResp Market
        {
            get
            {

                MarketGetResp resultList = CacheLayer.Get<MarketGetResp>(CacheLayer.DataKey.Market);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new ConfigServiceProxy())
                        {
                            resultList = proxy.MarketList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<MarketGetResp>(CacheLayer.DataKey.Market, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public StatusGetResp Status
        {
            get
            {

                StatusGetResp resultList = CacheLayer.Get<StatusGetResp>(CacheLayer.DataKey.Status);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new ConfigServiceProxy())
                        {
                            resultList = proxy.StatusList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<StatusGetResp>(CacheLayer.DataKey.Status, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public StatusTypeGetResp StatusType
        {
            get
            {

                StatusTypeGetResp resultList = CacheLayer.Get<StatusTypeGetResp>(CacheLayer.DataKey.StatusType);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new ConfigServiceProxy())
                        {
                            resultList = proxy.StatusTypeList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<StatusTypeGetResp>(CacheLayer.DataKey.StatusType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public StatusReasonGetResp StatusReason
        {
            get
            {

                StatusReasonGetResp resultList = CacheLayer.Get<StatusReasonGetResp>(CacheLayer.DataKey.StatusReason);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new ConfigServiceProxy())
                        {
                            resultList = proxy.StatusReasonList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<StatusReasonGetResp>(CacheLayer.DataKey.StatusReason, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        #endregion

        #region "CatalogServiceProxy Section"
        public BrandGetResp Brand
        {
            get
            {

                BrandGetResp resultList = CacheLayer.Get<BrandGetResp>(CacheLayer.DataKey.Brand);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.BrandsList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<BrandGetResp>(CacheLayer.DataKey.Brand, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public BundleGetResp Bundle
        {
            get
            {

                BundleGetResp resultList = CacheLayer.Get<BundleGetResp>(CacheLayer.DataKey.Bundle);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.BundlesList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<BundleGetResp>(CacheLayer.DataKey.Bundle, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public CategoryGetResp Category
        {
            get
            {

                CategoryGetResp resultList = CacheLayer.Get<CategoryGetResp>(CacheLayer.DataKey.Category);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.CategoriesList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<CategoryGetResp>(CacheLayer.DataKey.Category, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public ColourGetResp Colour
        {
            get
            {

                ColourGetResp resultList = CacheLayer.Get<ColourGetResp>(CacheLayer.DataKey.Colour);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.ColoursList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<ColourGetResp>(CacheLayer.DataKey.Colour, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public ModelGetResp Model
        {
            get
            {

                ModelGetResp resultList = CacheLayer.Get<ModelGetResp>(CacheLayer.DataKey.Model);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.ModelsList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<ModelGetResp>(CacheLayer.DataKey.Model, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public PackageGetResp Package
        {
            get
            {

                PackageGetResp resultList = CacheLayer.Get<PackageGetResp>(CacheLayer.DataKey.Package);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.PackagesList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<PackageGetResp>(CacheLayer.DataKey.Package, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public ProgramGetResp Program
        {
            get
            {

                ProgramGetResp resultList = CacheLayer.Get<ProgramGetResp>(CacheLayer.DataKey.Program);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.ProgramsList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<ProgramGetResp>(CacheLayer.DataKey.Program, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public ModelGroupGetResp ModelGroup
        {
            get
            {

                ModelGroupGetResp resultList = CacheLayer.Get<ModelGroupGetResp>(CacheLayer.DataKey.ModelGroup);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.ModelGroupList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<ModelGroupGetResp>(CacheLayer.DataKey.ModelGroup, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public SimModelTypeGetResp SimModelType
        {
            get
            {
                SimModelTypeGetResp resultList = CacheLayer.Get<SimModelTypeGetResp>(CacheLayer.DataKey.SimModelType);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.GetSIMModels();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<SimModelTypeGetResp>(CacheLayer.DataKey.SimModelType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public SimCardTypesGetResp SimCardType
        {
            get
            {
                SimCardTypesGetResp resultList = CacheLayer.Get<SimCardTypesGetResp>(CacheLayer.DataKey.SimCardType);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.GetSIMCardTypes();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<SimCardTypesGetResp>(CacheLayer.DataKey.SimCardType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public SimReplacementReasonGetResp SimReplacementReason
        {
            get
            {
                SimReplacementReasonGetResp resultList = CacheLayer.Get<SimReplacementReasonGetResp>(CacheLayer.DataKey.SimReplacementReason);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.GetSimReplacementReasons(accType: true);
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<SimReplacementReasonGetResp>(CacheLayer.DataKey.SimReplacementReason, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public SimReplacementReasonGetResp SimReplacementprePaidReason
        {
            get
            {
                SimReplacementReasonGetResp resultList = CacheLayer.Get<SimReplacementReasonGetResp>(CacheLayer.DataKey.SimReplacementprePaidReason);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.GetSimReplacementReasons(accType: false);
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<SimReplacementReasonGetResp>(CacheLayer.DataKey.SimReplacementprePaidReason, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public ComponentTypeGetResp ComponentType
        {
            get
            {
                ComponentTypeGetResp resultList = CacheLayer.Get<ComponentTypeGetResp>(CacheLayer.DataKey.ComponentType);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.ComponentTypeList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<ComponentTypeGetResp>(CacheLayer.DataKey.ComponentType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public PackageTypeGetResp PackageType
        {
            get
            {
                PackageTypeGetResp resultList = CacheLayer.Get<PackageTypeGetResp>(CacheLayer.DataKey.PackageType);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.PackageTypesList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<PackageTypeGetResp>(CacheLayer.DataKey.PackageType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public PropertyGetResp Property
        {
            get
            {
                PropertyGetResp resultList = CacheLayer.Get<PropertyGetResp>(CacheLayer.DataKey.Property);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.PropertyList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<PropertyGetResp>(CacheLayer.DataKey.Property, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public Dictionary<string, string> DataComps2GB
        {
            get
            {
                Dictionary<string, string> resultLIst = CacheLayer.Get<Dictionary<string, string>>(CacheLayer.DataKey.DataComps2GB);
                try
                {
                    if (resultLIst == null || resultLIst.Count == 0)
                    {
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultLIst = proxy.GetDCGreater2GB();
                        }
                        CacheLayer.Add<Dictionary<string, string>>(CacheLayer.DataKey.DataComps2GB, resultLIst);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultLIst;
            }
        }


        #endregion

        #region "OrganizationServiceProxy Section"

        //New method added by Vahini to implement Dependecy Cache
        public Dictionary<string, Dictionary<string, Int32>> OrganizationStocks
        {
            get
            {
                XDocument doc = new XDocument();
                var allOrgsStockCounts = CacheLayer.GetFromApplication<Dictionary<string, Dictionary<string, Int32>>>(CacheLayer.DataKey.OrganizationStocks);
                List<stockss> items = null;

                try
                {
                    if (allOrgsStockCounts == null)
                    {
                        Logger.Info("OrganizationStocks are being loaded from xml");
                        lock (lockObject)
                        {
                            string stockfilePath = HttpContext.Current.Server.MapPath("~/StockCounts/stockfile.xml");
                            doc = XDocument.Load(stockfilePath);
                             //doc = XDocument.Load(ConfigurationManager.AppSettings["StockCountPath"] != null ? ConfigurationManager.AppSettings["StockCountPath"].ToString() : string.Empty);
                        }
                        allOrgsStockCounts = new Dictionary<string, Dictionary<string, Int32>>();

                        if (doc.Root != null)
                        {
                            items = (from r in doc.Root.Elements("Stores")
                                     select new stockss
                                     {
                                         Key = (string)r.Element("Key"),
                                         Value = (from j in r.Elements("Value").Elements("StockCounts")
                                                  select new Value
                                                  {
                                                      Key = (string)j.Element("Key"),
                                                      Values = (Int32)j.Element("Value"),
                                                  }).ToList(),
                                     }).ToList();
                        }
                        Dictionary<string, Int32> orgStockCounts = null;
                        foreach (stockss sss in items)
                        {
                            orgStockCounts = new Dictionary<string, Int32>();

                            foreach (Value vall in sss.Value)
                            {

                                orgStockCounts.Add(vall.Key, vall.Values);
                            }
                            allOrgsStockCounts.Add(sss.Key, orgStockCounts);
                        }
                        Logger.Info("OrganizationStocks are loaded from xml");
                        double cacheTimeOutHrs = 24;
                        if (System.Configuration.ConfigurationSettings.AppSettings["cacheTimeOutHrs"] != null)
                            double.TryParse(System.Configuration.ConfigurationSettings.AppSettings["cacheTimeOutHrs"].ToString(), out cacheTimeOutHrs);
                        CacheLayer.Add<Dictionary<string, Dictionary<string, Int32>>>(CacheLayer.DataKey.OrganizationStocks, allOrgsStockCounts, cacheTimeOutHrs, RefreshCacheItems);


                    }
                    else
                    {
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return allOrgsStockCounts;
            }
        }

        public class stockss
        {
            public string Key { get; set; }
            public List<Value> Value { get; set; }
        }

        public class Value
        {
            public string Key { get; set; }
            public Int32 Values { get; set; }
        }

        public Dictionary<string, Dictionary<string, int>> OrganizationStocksOld
        {
            get
            {
                var allOrgsStockCounts = new Dictionary<string, Dictionary<string, int>>();
                // var allOrgsStockCounts = CacheLayer.GetFromApplication<Dictionary<string, Dictionary<string, int>>>(CacheLayer.DataKey.OrganizationStocks);
                try
                {
                    //if (allOrgsStockCounts == null)
                    //{

                    var organizations = Instance.Organization.Organizations.Where(o => o.WSDLUrl != null);
                    var articleTds = Instance.Article.Select(a => a.ArticleID).Distinct();
                    var orgUrls = organizations.Select(o => o.WSDLUrl).Where(s => !string.IsNullOrWhiteSpace(s)).Distinct();
                    var orgIds = organizations.Where(s => !string.IsNullOrWhiteSpace(s.WSDLUrl)).Distinct().Select(o=>o.OrganisationId);
                    if (orgUrls.Any())
                    {
                        foreach (var orgWSDUrl in orgUrls)
                        {
                            int index = 0;
                            Dictionary<string, int> orgStockCounts = new Dictionary<string, int>();
                            int storeId = 0;
                            string orgId = orgIds.ToList()[index];
                            string[] maxisStoreIds = ConfigurationManager.AppSettings["OrganizationsIds"].ToString().Split(",".ToCharArray());
                            if (!maxisStoreIds.Contains(orgId))
                            {
                                storeId = Convert.ToInt32(orgId);
                            }
                            using (var dsproxy = new DynamicServiceProxy())
                            {
                                orgStockCounts = dsproxy.GetStockCounts(articleTds.ToList(), orgWSDUrl,storeId);
                            }
                            allOrgsStockCounts.Add(orgWSDUrl, orgStockCounts);

                            index++;
                        }
                    }
                    Logger.Info("OrganizationStocks are loaded from service call");
                    double cacheTimeOutHrs = 24;
                    if (System.Configuration.ConfigurationSettings.AppSettings["cacheTimeOutHrs"] != null)
                        double.TryParse(System.Configuration.ConfigurationSettings.AppSettings["cacheTimeOutHrs"].ToString(), out cacheTimeOutHrs);
                    CacheLayer.Add<Dictionary<string, Dictionary<string, int>>>(CacheLayer.DataKey.OrganizationStocks, allOrgsStockCounts, cacheTimeOutHrs, RefreshCacheItems);

                    //}
                    //else
                    //{
                    //}
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return allOrgsStockCounts;
            }
        }

        public OrganizationGetResp Organization
        {
            get
            {
                OrganizationGetResp resultList = CacheLayer.Get<OrganizationGetResp>(CacheLayer.DataKey.Organization);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new OrganizationServiceProxy())
                        {
                            resultList = proxy.OrganizationList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<OrganizationGetResp>(CacheLayer.DataKey.Organization, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }
        public OrgTypeGetResp OrgType
        {
            get
            {
                OrgTypeGetResp resultList = CacheLayer.Get<OrgTypeGetResp>(CacheLayer.DataKey.OrgType);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new OrganizationServiceProxy())
                        {
                            resultList = proxy.OrgTypesList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<OrgTypeGetResp>(CacheLayer.DataKey.OrgType, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        #endregion

        #region "ComplainServiceProxy Section"
        //ComplaintIssues:
        public ComplaintIssuesGetResp ComplaintIssues
        {
            get
            {
                ComplaintIssuesGetResp resultList = CacheLayer.Get<ComplaintIssuesGetResp>(CacheLayer.DataKey.ComplaintIssues);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new ComplainServiceProxy())
                        {
                            resultList = proxy.ComplaintIssuesList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<ComplaintIssuesGetResp>(CacheLayer.DataKey.ComplaintIssues, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        #endregion

        #region "UserServiceProxy Section"

        public UserGroupGetResp UserGroup
        {
            get
            {
                UserGroupGetResp resultList = CacheLayer.Get<UserGroupGetResp>(CacheLayer.DataKey.UserGroup);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new UserServiceProxy())
                        {
                            resultList = proxy.UserGroupList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<UserGroupGetResp>(CacheLayer.DataKey.UserGroup, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public UserRoleGetResp UserRole
        {
            get
            {
                UserRoleGetResp resultList = CacheLayer.Get<UserRoleGetResp>(CacheLayer.DataKey.UserRole);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new UserServiceProxy())
                        {
                            resultList = proxy.UserRolesList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<UserRoleGetResp>(CacheLayer.DataKey.UserRole, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public UserGetResp User
        {
            get
            {
                UserGetResp resultList = CacheLayer.Get<UserGetResp>(CacheLayer.DataKey.User);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new UserServiceProxy())
                        {
                            resultList = proxy.UsersList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<UserGetResp>(CacheLayer.DataKey.User, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public AccessGetResp Access
        {
            get
            {
                AccessGetResp resultList = CacheLayer.Get<AccessGetResp>(CacheLayer.DataKey.Access);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new UserServiceProxy())
                        {
                            resultList = proxy.AccessList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<AccessGetResp>(CacheLayer.DataKey.Access, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);

                }
                return resultList;
            }
        }

        public GetDonerMessagesResp Doners
        {
            get
            {
                GetDonerMessagesResp resultList = CacheLayer.Get<GetDonerMessagesResp>(CacheLayer.DataKey.Doners);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new UserServiceProxy())
                        {
                            resultList = proxy.GetDonors();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<GetDonerMessagesResp>(CacheLayer.DataKey.Doners, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);

                }
                return resultList;
            }
        }

        #endregion

        public IEnumerable<PgmBdlPckComponent> PackageComponents
        {
            get
            {
                var resultList = CacheLayer.Get<IEnumerable<PgmBdlPckComponent>>(CacheLayer.DataKey.PackageComponents);
                try
                {
                    if (resultList == null)
                    {
                        using (var proxy = new CatalogServiceProxy())
                        {
                            //Pradeep : Get Active and Inactive components separately and then combine them
                            PgmBdlPckComponentFind req = new PgmBdlPckComponentFind() { Active = true, PgmBdlPckComponent = new PgmBdlPckComponent() { ID = 0 } };
                            List<PgmBdlPckComponent> components = proxy.GetLnkPgmBdlPkgComp(req).ToList();

                            req = new PgmBdlPckComponentFind() { Active = false, PgmBdlPckComponent = new PgmBdlPckComponent() { ID = 0 } };
                            IEnumerable<PgmBdlPckComponent> InactiveComponents = proxy.GetLnkPgmBdlPkgComp(req);

                            components.AddRange(InactiveComponents);
                            resultList = components.AsEnumerable();
                        }
                        CacheLayer.Add<IEnumerable<PgmBdlPckComponent>>(CacheLayer.DataKey.PackageComponents, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }

        }

        public IEnumerable<PgmBdlPckComponent> FilterComponents(PgmBdlPckComponentFind oReq)
        {
            var resultList = PackageComponents;
            if (oReq.PgmBdlPckComponent.ID != 0)
                resultList = resultList.Where(a => a.ID == oReq.PgmBdlPckComponent.ID);
            if (oReq.ParentIDs.Count() > 0)
                resultList = resultList.Where(a => oReq.ParentIDs.Contains(a.ParentID));
            if (oReq.ChildIDs.Count() > 0)
                resultList = resultList.Where(a => oReq.ChildIDs.Contains(a.ChildID));
            if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.LinkType))
                resultList = resultList.Where(a => a.LinkType == oReq.PgmBdlPckComponent.LinkType);
            if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.PlanType))
                resultList = resultList.Where(a => a.PlanType == oReq.PgmBdlPckComponent.PlanType);
            if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.Code))
                resultList = resultList.Where(a => a.Code == oReq.PgmBdlPckComponent.Code);
            if (oReq.PgmBdlPckComponent.ParentID != 0)
                resultList = resultList.Where(a => a.ParentID == oReq.PgmBdlPckComponent.ParentID);
            if (oReq.PgmBdlPckComponent.ChildID != 0)
                resultList = resultList.Where(a => a.ChildID == oReq.PgmBdlPckComponent.ChildID);
            if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.FilterOrgType))
                resultList = resultList.Where(a => ("," + a.FilterOrgType + ",").Contains("," + oReq.PgmBdlPckComponent.FilterOrgType + ","));
            if (oReq.Active.HasValue)
                resultList = resultList.Where(a => a.Active == oReq.Active.Value);
            if (oReq.IsMandatory.HasValue)
                resultList = resultList.Where(a => a.IsMandatory == oReq.IsMandatory.Value);
            if (oReq.NeedProvision.HasValue)
                resultList = resultList.Where(a => a.NeedProvision == oReq.NeedProvision.Value);
            if (oReq.NeedActivation.HasValue)
                resultList = resultList.Where(a => a.NeedActivation == oReq.NeedActivation.Value);
            if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.KenanCode))
                resultList = resultList.Where(a => a.KenanCode == oReq.PgmBdlPckComponent.KenanCode);

            if (oReq.PageSize > 0)
                resultList = resultList.OrderByDescending(a => a.ID).Take(oReq.PageSize);

            //SMEI/CI Implementation on May 16 2014
            if (resultList != null && oReq.PgmBdlPckComponent != null && (oReq.PgmBdlPckComponent.LinkType == Properties.Settings.Default.Bundle_Package || oReq.PgmBdlPckComponent.LinkType == Properties.Settings.Default.SecondaryPlan))
            {
               var session = HttpContext.Current.Session;
                List<int> objPlanIds = new List<int>();
                using (var proxy = new CatalogServiceProxy())
                {
                    string marketCode = string.Empty;
                    List<lnkAcctMktCodePackages> objCorpPlans = new List<lnkAcctMktCodePackages>();
                    if (session[SessionKey.isFromBRNSearch.ToString()] != null && session[SessionKey.CompanyInfoResponse.ToString()] != null)
                    {
                        Online.Registration.Web.ViewModels.BRNVm companyInfo = (Online.Registration.Web.ViewModels.BRNVm)session[SessionKey.CompanyInfoResponse.ToString()];
                        //Modified to allow corporate and SME market codes
                        marketCode = companyInfo.MarketCode;
                        if (marketCode == ConfigurationManager.AppSettings["CorporateParentMarketCode"].ToString2())
                            marketCode = ConfigurationManager.AppSettings["CIParentMarketCode"].ToString2();
                        if (marketCode == ConfigurationManager.AppSettings["SMEParentMarketCode"].ToString2())
                            marketCode = ConfigurationManager.AppSettings["SMEIParentMarketCode"].ToString2();
                        objCorpPlans = proxy.GetPlansbyMarketandAccountCategory(ConfigurationManager.AppSettings["CoporateAccountCategory"].ToInt(), marketCode.ToInt());
                    }
                    else
                    {
                        if (session[SessionKey.AccountCategory.ToString()].ToString2() != string.Empty && session[SessionKey.MarketCode.ToString()].ToString2() != string.Empty)
                        {
                            objCorpPlans = proxy.GetPlansbyMarketandAccountCategory(session[SessionKey.AccountCategory.ToString()].ToInt(), session[SessionKey.MarketCode.ToString()].ToInt());
                        }
                        else
                        {
                            objCorpPlans = proxy.GetPlansbyMarketandAccountCategory(ConfigurationManager.AppSettings["ConsumerAccountCategory"].ToInt(), ConfigurationManager.AppSettings["ConsumerMarketCode"].ToInt());
                        }
                    }

                    objPlanIds = objCorpPlans.Select(a => a.lnkpgmbdlpkgcompID).ToList();
                }

                resultList = resultList.Where(c => objPlanIds.Contains(c.ID));
            }
            //SMEI/CI Implementation on May 16 2014  ends here



            if (resultList != null)
            {
                var catch2resultList = resultList.Where(a => a.ChildID == 39 && a.ParentID == 33);
                if (oReq.PgmBdlPckComponent.ID != 0)
                    resultList = resultList.Where(a => a.ID == oReq.PgmBdlPckComponent.ID);
                if (oReq.ParentIDs.Count() > 0)
                    resultList = resultList.Where(a => oReq.ParentIDs.Contains(a.ParentID));
                if (oReq.ChildIDs.Count() > 0)
                    resultList = resultList.Where(a => oReq.ChildIDs.Contains(a.ChildID));
                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.LinkType))
                    resultList = resultList.Where(a => a.LinkType == oReq.PgmBdlPckComponent.LinkType);
                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.PlanType))
                    resultList = resultList.Where(a => a.PlanType == oReq.PgmBdlPckComponent.PlanType);
                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.Code))
                    resultList = resultList.Where(a => a.Code == oReq.PgmBdlPckComponent.Code);
                if (oReq.PgmBdlPckComponent.ParentID != 0)
                    resultList = resultList.Where(a => a.ParentID == oReq.PgmBdlPckComponent.ParentID);
                if (oReq.PgmBdlPckComponent.ChildID != 0)
                    resultList = resultList.Where(a => a.ChildID == oReq.PgmBdlPckComponent.ChildID);
                if (!string.IsNullOrEmpty(oReq.PgmBdlPckComponent.FilterOrgType))
                    resultList = resultList.Where(a => ("," + a.FilterOrgType + ",").Contains("," + oReq.PgmBdlPckComponent.FilterOrgType + ","));
                if (oReq.Active.HasValue)
                    resultList = resultList.Where(a => a.Active == oReq.Active.Value);
                if (oReq.IsMandatory.HasValue)
                    resultList = resultList.Where(a => a.IsMandatory == oReq.IsMandatory.Value);
                if (oReq.NeedProvision.HasValue)
                    resultList = resultList.Where(a => a.NeedProvision == oReq.NeedProvision.Value);
                if (oReq.NeedActivation.HasValue)
                    resultList = resultList.Where(a => a.NeedActivation == oReq.NeedActivation.Value);
                if (oReq.PageSize > 0)
                    resultList = resultList.OrderByDescending(a => a.ID).Take(oReq.PageSize);
            }
            return resultList;
        }
        public IEnumerable<PgmBdlPckComponent> FilterComponents(IEnumerable<int> IDs)
        {
            var resultList = PackageComponents;

            ////SMEI/CI Implementation on May 16 2014
            //if (resultList != null)
            //{
            //    var session = HttpContext.Current.Session;
            //    List<int> objPlanIds = new List<int>();
            //    using (var proxy = new CatalogServiceProxy())
            //    {
            //        List<lnkAcctMktCodePackages> objCorpPlans = new List<lnkAcctMktCodePackages>();
            //        if (session[SessionKey.isFromBRNSearch.ToString()] != null && session[SessionKey.CompanyInfoResponse.ToString()] != null)
            //        {
            //            Online.Registration.Web.ViewModels.BRNVm companyInfo = (Online.Registration.Web.ViewModels.BRNVm)session[SessionKey.CompanyInfoResponse.ToString()];
            //            objCorpPlans = proxy.GetPlansbyMarketandAccountCategory(ConfigurationManager.AppSettings["CoporateAccountCategory"].ToInt(), companyInfo.MarketCode.ToInt());
            //        }
            //        else
            //        {
            //            if (session[SessionKey.AccountCategory.ToString()].ToString2() != string.Empty && session[SessionKey.MarketCode.ToString()].ToString2() != string.Empty)
            //            {
            //                objCorpPlans = proxy.GetPlansbyMarketandAccountCategory(session[SessionKey.AccountCategory.ToString()].ToInt(), session[SessionKey.MarketCode.ToString()].ToInt());
            //            }
            //            else
            //            {
            //                objCorpPlans = proxy.GetPlansbyMarketandAccountCategory(ConfigurationManager.AppSettings["ConsumerAccountCategory"].ToInt(), ConfigurationManager.AppSettings["ConsumerMarketCode"].ToInt());
            //            }
            //        }

            //        objPlanIds = objCorpPlans.Select(a => a.lnkpgmbdlpkgcompID).ToList();
            //    }

            //    resultList = resultList.Where(c => objPlanIds.Contains(c.ID));
            //}
            ////SMEI/CI Implementation on May 16 2014  ends here

            resultList = resultList.Where(a => IDs.Contains(a.ID));
            return resultList;
        }

        public WaiverRulesResp WaiverRulesInfo
        {
            get
            {
                WaiverRulesResp waiverRules = CacheLayer.Get<WaiverRulesResp>(CacheLayer.DataKey.WaiverRulesInfo);
                try
                {
                    if (waiverRules == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            waiverRules = proxy.GetWaiverRules();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<WaiverRulesResp>(CacheLayer.DataKey.WaiverRulesInfo, waiverRules);

                    }
                }
                catch (Exception ex)
                {
                    waiverRules = new WaiverRulesResp();
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return waiverRules;
            }

        }

        //public List<Address> AddressFilter(Func<List<Address>, bool> predicate)
        //{
        //    return this.Address.Where(predicate);
        //}

        public List<RebateDataContractComponents> RebateContractComponents
        {
            get
            {
                List<RebateDataContractComponents> resultList = CacheLayer.Get<List<RebateDataContractComponents>>(CacheLayer.DataKey.RebateContractComponents);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.getRebateDataContractComponents().ToList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<List<RebateDataContractComponents>>(CacheLayer.DataKey.RebateContractComponents, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public List<BreCheckTreatment> GetBreCheckTreatment {

            get 
            {
                List<BreCheckTreatment> resultList = CacheLayer.Get<List<BreCheckTreatment>>(CacheLayer.DataKey.BreCheckTreatment);
                try
                {
                    if (resultList == null)
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.getBRECheckTreatment().ToList();
                        }
                        CacheLayer.Add<List<BreCheckTreatment>>(CacheLayer.DataKey.BreCheckTreatment, resultList);
                    }

                }

                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }

                return resultList;
            }
        }

		public List<refLovList> GetAllLovList
		{

			get
			{
				List<refLovList> resultList = CacheLayer.Get<List<refLovList>>(CacheLayer.DataKey.LOVList);
				try
				{
					if (resultList == null)
					{
						using (var catProxy = new CatalogServiceProxy())
						{
							resultList = catProxy.GetAllLovList().ToList();
						}
						CacheLayer.Add<List<refLovList>>(CacheLayer.DataKey.LOVList, resultList);
					}

				}

				catch (Exception ex)
				{
					WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
				}

				return resultList;
			}
		}

        public List<PostCodeCityState> GetPostCodeCityState
        {

            get 
            {
                List<PostCodeCityState> resultList = CacheLayer.Get<List<PostCodeCityState>>(CacheLayer.DataKey.PostCodeCityState);
                try
                {
                    if (resultList == null)
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.getPostCodeCityState().ToList();
                        }
                        CacheLayer.Add<List<PostCodeCityState>>(CacheLayer.DataKey.PostCodeCityState, resultList);
                    }

                }

                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }

                return resultList;
            }
        }

        #region 13042015 - Anthony - Drop 5 : Waiver for non-Drop 4 Flow
        public List<AutoWaiverRules> getAutoWaiverRules
        {
            get
            {
                List<AutoWaiverRules> resultList = CacheLayer.Get<List<AutoWaiverRules>>(CacheLayer.DataKey.AutoWaiverRules);
                try
                {
                    if (resultList == null)
                    {
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.getAutoWaiverRules().ToList();
                        }
                        CacheLayer.Add<List<AutoWaiverRules>>(CacheLayer.DataKey.AutoWaiverRules, resultList);
                    }

                }

                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }

                return resultList;
            }
        }
        #endregion

        public DeviceTypesGetResp DeviceType
        {
            get
            {
                DeviceTypesGetResp resultList = CacheLayer.Get<DeviceTypesGetResp>(CacheLayer.DataKey.DeviceType);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.getDeviceType();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<DeviceTypesGetResp>(CacheLayer.DataKey.DeviceType, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public DeviceCapacityGetResp DeviceCapacity
        {
            get
            {
                DeviceCapacityGetResp resultList = CacheLayer.Get<DeviceCapacityGetResp>(CacheLayer.DataKey.DeviceCapacity);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.getDeviceCapacity();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<DeviceCapacityGetResp>(CacheLayer.DataKey.DeviceCapacity, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

        public PriceRangeGetResp PriceRange
        {
            get
            {
                PriceRangeGetResp resultList = CacheLayer.Get<PriceRangeGetResp>(CacheLayer.DataKey.PriceRange);
                try
                {
                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new CatalogServiceProxy())
                        {
                            resultList = proxy.getPriceRange();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<PriceRangeGetResp>(CacheLayer.DataKey.PriceRange, resultList);
                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;
            }
        }

		public List<Contracts> AllContractList
		{
			get
			{
				List<Contracts> resultList = CacheLayer.Get<List<Contracts>>(CacheLayer.DataKey.AllContractList);
				try
				{
					if (resultList == null)
					{
						//Data Fecth Logic Here
						using (var proxy = new RegistrationServiceProxy())
						{
							resultList = proxy.GetContractsList().ToList();
						}
						// Inserting data in to Cache
						CacheLayer.Add<List<Contracts>>(CacheLayer.DataKey.AllContractList, resultList);
					}
				}
				catch (Exception ex)
				{
					WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
				}
				return resultList;
			}
		}

        public ThirdPartyAuthTypeGetResp ThirdPartyAuthType
        {
            get
            {

                ThirdPartyAuthTypeGetResp resultList = CacheLayer.Get<ThirdPartyAuthTypeGetResp>(CacheLayer.DataKey.ThirdPartyAuthType);
                try
                {

                    if (resultList == null)
                    {
                        //Data Fecth Logic Here
                        using (var proxy = new RegistrationServiceProxy())
                        {
                            resultList = proxy.ThirdPartyAuthTypeGetList();
                        }
                        // Inserting data in to Cache
                        CacheLayer.Add<ThirdPartyAuthTypeGetResp>(CacheLayer.DataKey.ThirdPartyAuthType, resultList);

                    }
                }
                catch (Exception ex)
                {
                    WebHelper.Instance.LogAppExceptions(this.GetType(), ex);
                }
                return resultList;

            }
        }
    }
}
