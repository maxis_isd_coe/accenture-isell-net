﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.Caching;

namespace Online.Registration.Web.ContentRepository
{
    public static class CacheLayer 
    {
        private static object lockObject = new object();
        static HttpApplicationState Application = HttpContext.Current.Application;
        public enum DataKey
        {
            AddressType,
            AccountCategory,
            Article,
            Bank,
            Brand,
            Bundle,
            CardType,
            Category,
            Colour,
            ComplaintIssues,
            Component,
            ComponentType,
            Country,
            CustomerTitle,
            DeviceProperty,
            ExternalIDType,
            IDCardType,
            IssueGrpDispatchQ,
            Language,
            Nationality,
            Market,
            Model,
            Organization,
            OrganizationStocks,
            OrgType,
            Package,
            PackageType,
            PaymentMode,
            PgmBdlPkgComp,
            PlanProperty,
            Program,
            Property,
            PropertyValues,
            Race,
            RegType,
            State,
            Status,
            StatusType,
            StatusReason,
            UserGroup,
            User,
            VIPCode,
            Access,
            UserRole,
            ModelGroup,
            CustomerInfoPaymentMode,
            CustomerInfoCardType,
            Doners,
            SimCardType,
            SimModelType, 
            SimReplacementReason, 
            SimReplacementprePaidReason, 
            Msisdn,
            RestrictKeananComponent,
            IdCardTypes, 
            PackageComponents,
            DataComps2GB,
            WaiverRulesInfo,
            AutoWaiverRulesInfo,
            RebateContractComponents,
            DeviceType,
            DeviceCapacity,
            PriceRange,
            BreCheckTreatment,
            BreCheckTreatment_Test, // w.loon - for testing purpose
			AllContractList,
            PostCodeCityState,
            AutoWaiverRules,//13042015 - Anthony - Drop 5 : Waiver for non-Drop 4 Flow
			LOVList,
            ThirdPartyAuthType //30102015 - Ashley - Paperless : added new item
		}
        
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="cacheObject"></param>
       public static void Add<T>(DataKey key, T cacheObject, bool isVolatile = false) where T : class
        {
            if (HttpRuntime.Cache != null && cacheObject != null)
            {
                HttpRuntime.Cache.Insert(
                    key.ToString(),
                    cacheObject,
                    null,
                    isVolatile ? DateTime.Now.AddHours(3) : System.Web.Caching.Cache.NoAbsoluteExpiration,
                     System.Web.Caching.Cache.NoSlidingExpiration);
            }
        }

       public static void Add<T>(DataKey key, T cacheObject, double spanHours, CacheItemRemovedCallback callback) where T : class
       {
           if (Application != null && cacheObject != null)
           {
               Application.Lock();
               Application[key.ToString()] = cacheObject;
               Application.UnLock();
           }
           if (HttpContext.Current != null && cacheObject != null)
           {
               string stockfilePath = HttpContext.Current.Server.MapPath("~/StockCounts/stockfile.xml");
                   //HttpContext.Current.Cache.Insert(
                   //    key.ToString() + "ExpiryFlag",
                   //    true,
                   //    //Modified ny Vahini to implement Cache dependency
                   //    new System.Web.Caching.CacheDependency(ConfigurationManager.AppSettings["StockCountPath"] != null ? ConfigurationManager.AppSettings["StockCountPath"].ToString() : string.Empty),
                   //    DateTime.Now.AddMinutes(spanHours),
                   //     System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.AboveNormal, callback);

               HttpContext.Current.Cache.Insert(
                      key.ToString() + "ExpiryFlag",
                      true,
                   //Modified ny Vahini to implement Cache dependency
                      new System.Web.Caching.CacheDependency(stockfilePath),
                      DateTime.Now.AddMinutes(spanHours),
                       System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.AboveNormal, callback);
             
           }
       }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public static void Clear(DataKey key)
        {
            HttpRuntime.Cache.Remove(key.ToString());
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public static void ClearFromApplication(DataKey key)
        {
            Application.Lock();
            Application.Remove(key.ToString());
            Application.UnLock();
            HttpContext.Current.Cache.Remove(key.ToString() + "ExpiryFlag");
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="key"></param>
       /// <returns></returns>
        public static bool Exists(DataKey key)
        {
            return HttpRuntime.Cache[key.ToString()] != null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(DataKey key) where T : class
        {
            try
            {
                return (T)HttpRuntime.Cache[key.ToString()];
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetFromApplication<T>(DataKey key) where T : class
        {
            try
            {
                if (HttpContext.Current.Cache[key.ToString() + "ExpiryFlag"] != null && (bool)HttpContext.Current.Cache[key.ToString() + "ExpiryFlag"] == true)
                    return (T)Application[key.ToString()];
                else
                    return null;
            }
            catch
            {
                return null;
            }
        }
    }
}
