﻿using Online.Registration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Online.Registration.Web.CustomAuthorize
{
    /// <summary>
    /// Only allow non dealer user
    /// </summary>
    public class AuthorizeNonDealerAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //Temporary hardcode, will look back
            //if (httpContext.Request.RequestContext.RouteData.Values["action"].ToString() == "PrintOrder") return true;
            var isAuthorized = base.AuthorizeCore(httpContext);
            if (!isAuthorized)
            {
                return false;
            }
            var currentLogin = Util.SessionAccess;
            if (currentLogin == null || currentLogin.User==null)
                return false;
           if (currentLogin.User.isDealer)
                return false;
            return true;
        }
    }
}