﻿//Custome form validator behavior, assume only first form, only show first error, scroll to error summary
$(function () {
    var frm = $('form.srForm')[0];
    if (frm) {
        var settings = $.data(frm, 'validator').settings;
        settings.onkeyup = false;
        settings.focusInvalid = false;
        settings.showErrors = function (errMap, errList) {
            clearModelError();
            if (errList.length > 0) {
                //Only display first error
                $(".err-container ul").append(String.format("<li>{0}</li>", errList[0].message));
                scrollToElement($(".err-container"));
            }
        };
    }
});
//common display bre errors, bre display format must match in display template for bre errors
function displayBreErrors(d, elId) {
    var statusEl = $(String.format("#{0} .status-red", elId));
    var liList = $(String.format("#{0} .err-list", elId));
    liList.empty();
    if (d && Object.size(d) > 0) {
        $.each(d, function (index, value) {
            liList.append(String.format("<li>{0}</li>", value));
        });
        statusEl.show();
    } else {
        statusEl.hide();
    }
};
//common clear model error, assume same class name use
function clearModelError() {
    $(".err-container ul").empty();
};
//common display model error, assume only one model error container here where class name='err-container'
function displayModelError(err) {
    var displayErr = "";
    if (typeof err === "string")
        displayErr = err;
    else
        displayErr=getFirstValue(err)[0];
    $(".err-container ul").append(String.format("<li>{0}</li>", displayErr));
    scrollToElement($(".err-container"));
};
//util scroll to element
function scrollToElement(el) {
    $('html,body').animate({
        scrollTop: el.offset().top
    }, 500);
};
//Enhance serialzie object
(function ($) {
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);
//String format function
String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }
    return s;
}
//object function to get size
Object.size = function (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
//reuse existing form to perform submittion, trigger submittionis from outside
function injectFormSubmit($frmEl, url, fn, method) {
    var _oriUrl = $frmEl.attr('action');
    var _oriMethod = $frmEl.attr('method');
    $frmEl.attr('action', url);
    if (method) $frmEl.attr('method', method);
    if(fn!=undefined)fn();
    $frmEl.attr('action', _oriUrl);
    $frmEl.attr('method', _oriMethod);
}
function toMvcDtFmt(d) {
    return [d.getMonth() + 1,
            d.getDate(),
            d.getFullYear()].join('/') + ' ' +
            [d.getHours(),
            d.getMinutes(),
            d.getSeconds()].join(':');
}

// 15-10-2015 - Added by Ashley to allow Custom Required If Condition on the View Model fields. It is used together with the CustomRequiredAttribute.cs code
$.validator.unobtrusive.adapters.add('customrequired', ['requiredpropertyvalue', 'requiredproperty'], function (options) {
    options.rules['customrequired'] = {
        requiredpropertyvalue: options.params['requiredpropertyvalue'],
        requiredproperty: options.params['requiredproperty']
    };
    options.messages['customrequired'] = options.message;
});

$.validator.addMethod('customrequired', function (value, element, params) {
    var requiredpropertyvalue = params['requiredpropertyvalue'];
    var requiredproperty = params['requiredproperty'];
    var field = $('#' + requiredproperty);
    //Only check require when dependentyproperty and value match
    if (field.val() == requiredpropertyvalue)
        return value.length > 0;
    return true;
});
//MustBe jquery validation
$.validator.addMethod("mustbetrue", function (value, element, param) {
    //translat for consistent
    value = value.toUpperCase() == "TRUE";
    // check if dependency is met
    if (!this.depend(param, element))
        return "dependency-mismatch";
    //handle checkbox
    var selector=String.format("#{0}",element.id);
    if ($(selector).is(':checkbox')) value = $(selector).is(":checked");
    return value==true;
});
$.validator.unobtrusive.adapters.add("mustbetrue", function (options) {
    options.rules['mustbetrue'] = {};
    options.messages['mustbetrue'] = options.message;
});
//Must be true if validation
$.validator.addMethod("mustbetrueif", function (value, element, params) {
    var dependValue = $('#' + params.dependentName).val();
    if (dependValue == params.dependentValue) {
        value = value.toUpperCase() == "TRUE";
        var selector = String.format("#{0}", element.id);
        if ($(selector).is(':checkbox')) value = $(selector).is(":checked");
        return value == true;
    }
    return true;
});
$.validator.unobtrusive.adapters.add("mustbetrueif", ['dependentname','dependentvalue'], function (options) {
    options.rules['mustbetrueif'] = {
        dependentName: options.params['dependentname'],
        dependentValue: options.params['dependentvalue']
    };
    options.messages['mustbetrueif'] = options.message;
});
//Notequalto jquery validation
$.validator.addMethod("notequalto", function (value, element, params) {
    var otherFieldValue = $('#' + params['other']).val();
    return otherFieldValue != value;
});
$.validator.unobtrusive.adapters.add("notequalto",['other'], function (options) {
    options.rules['notequalto'] = {
        other: options.params['other']
    };
    options.messages['notequalto'] = options.message;
});
//Not Equal To If jquery validation
$.validator.addMethod("notequaltoif", function (value, element, params) {
    var dependValue = $('#' + params.dependentName).val();
    if (dependValue == params.dependentValue) {
        var otherFieldValue = $('#' + params['other']).val();
        return otherFieldValue != value;
    }
    return true;
});
$.validator.unobtrusive.adapters.add("notequaltoif", ['other', 'dependentname', 'dependentvalue'], function (options) {
    options.rules['notequaltoif'] = {
        other: options.params['other'],
        dependentName: options.params['dependentname'],
        dependentValue: options.params['dependentvalue']
    };
    options.messages['notequaltoif'] = options.message;
});
//valididnum
$.validator.addMethod("valididnum", function (value, element, params) {
    if (!value) return true;
    var idtype = $('#' + params['idtypeid']).val();
    //support check ic type only
    if (idtype == 1) {
        //test length
        if (value.length != 12)
            return false;
        //test number
        if (!$.isNumeric(value)) {
            return false;
        }
        //test month, it is correct when result month is same
        var m = parseInt(value.substr(2, 2));
        var d = parseInt(value.substr(4, 2));
        var testM = (new Date((new Date()).getFullYear(), m - 1, d)).getMonth() + 1;
        return m == testM;
    }
    //default success
    return true;
}, function (param, element) {
    var idType = $("#" + param.idtypeid).val();
    var data = $(element).data();
    if (idType == 1) {
        if (element.value.length > 12)
            return data.valValididnumMaxlength;
        if (element.value.length < 12)
            return data.valValididnumMinlength;
    }
    return data.valValididnum;
});
$.validator.unobtrusive.adapters.add("valididnum", ['idtypeid'], function (options) {
    options.rules['valididnum'] = {
        idtypeid: options.params['idtypeid'],
    };
    //use custom message to handle dynamic
    //options.messages['valididnum'] = options.message;
});
//Custome string length
$.validator.addMethod("customstringlength", function (value, element, params) {
    //check min and max
    if (value == "") return true;
    return value != "" && value.length >= params.minLength && value.length <= params.maxLength;
}, function (param, element) {
    var value = $(element).val();
    if (value.length > param.maxLength)
        return param.maxLengthMsg;
    if (value.length < param.minLength)
        return param.minLengthMsg;
    return "";
});
$.validator.unobtrusive.adapters.add("customstringlength", ['minlength', 'maxlength', 'minlengthmsg', 'maxlengthmsg'], function (options) {
    options.rules['customstringlength'] = {
        minLength: options.params['minlength'],
        maxLength: options.params['maxlength'],
        minLengthMsg: options.params['minlengthmsg'],
        maxLengthMsg: options.params['maxlengthmsg']
    };
});
//Data unique validation
$.validator.addMethod("dataunique", function (value, element, params) {
    //loop all element with same group name
    if (value == "") return true;
    var lst = $(String.format("[data-val-dataunique-groupname='{0}']", params.groupName)).not(String.format("#{0}", element.id));
    var success = true;
    $.each(lst, function (key, value) {
        if (value.value === element.value) {
            success = false;
            return false;
        }
    });
    return success;
});
$.validator.unobtrusive.adapters.add("dataunique", ['groupname', 'displayname'], function (options) {
    options.rules['dataunique'] = {
        groupName: options.params['groupname'],
        displayName: options.params['displayname']
    };
    options.messages['dataunique'] = options.message;
});
//util get first element of dictionary
function getFirstValue(dic) {
    return dic[Object.keys(dic)[0]];
};