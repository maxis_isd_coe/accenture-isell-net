
$(document).ready(function () {
	flipCard();
});
function flipCard(){
	//$("input:checkbox").click(function() {
	//$("input:checkbox").attr("name", "sel-device").click(function() {
	$(".device-checkbox").click(function() {	
	if ($(this).is(":checked")) {
		var group = "input:checkbox[name='" + $(this).attr("name") + "']";
		$(group).prop("checked", false);
		$(this).prop("checked", true);
		if($('.card').hasClass('flip') ){
		$('.card').removeClass('flip');
		$(this).closest( ".card" ).addClass('flip');;
	}else {
		$(this).closest( ".card" ).addClass('flip');;
	}
	} else {
		$(this).prop("checked", false);
		$(this).closest( ".card" ).removeClass('flip');
	}
	});
	$('.back-btn').on('click',function(e) {
		e.preventDefault();
		$(this).closest( ".card" ).removeClass('flip');
	});
}



function activeDevice(){
	    $('.dropdown-toggle').dropdown();
        if ($('#device li:first-child').hasClass('active')){
            $('#the-phone').removeClass('phone-type');
            $('#the-phone').addClass('phone-type-selected');
        }
        else if ($('#device li:first-child').hasClass('')){
            $('#the-phone').addClass('phone-type');
            $('#the-phone').removeClass('phone-type-selected');
            }
    $('.dropdown-menu a').on('click', function (e) {
        e.preventDefault();
    });
    $('.input-group-btn .model').on('click', function (e) {
        e.preventDefault();
            });
}
function portraitIpad(){
	//back -cancel button
	$('#back').addClass('col-sm-2');
	$('#back').removeClass('col-sm-1');
	$('#cancel').addClass('col-sm-8');
	$('#cancel').removeClass('col-sm-9');
	//device page
	$('#choose-device').removeClass('container');
	$('#choose-device').addClass('container-fluid');
	//middle & right contents
	$('#show-device').addClass('col-sm-5');
	$('#show-device').removeClass('col-sm-6');
	$('#order-summary').addClass('col-sm-4');
	$('#order-summary').removeClass('col-sm-3');	
}
function landscapIpad(){
	$('#back').removeClass('col-sm-2');
	$('#back').addClass('col-sm-1');
	$('#cancel').removeClass('col-sm-8');
	$('#cancel').addClass('col-sm-9');	
	$('#choose-device').addClass('container');
	$('#choose-device').removeClass('container-fluid');	
	$('#show-device').addClass('col-sm-6');
	$('#show-device').removeClass('col-sm-5');
	$('#order-summary').addClass('col-sm-3');
	$('#order-summary').removeClass('col-sm-4');		
}
function initBottomClass(){
	var $containerWidth = $(window).width();
    if ($containerWidth >= 1024) { 
        landscapIpad();
    }
    if ($containerWidth <= 1024) { 
        portraitIpad();
    }
}
window.addEventListener('resize', function(event){
  if (navigator.platform === 'iPad') {
      window.onorientationchange = function() {

          var orientation = window.orientation;

          // Look at the value of window.orientation:
          if (orientation === 0) {
              // iPad is in Portrait mode.
            
              portraitIpad();

          } else if (orientation === 90) {
          
            landscapIpad();
            // iPad is in Landscape mode. The screen is turned to the left.

          } else if (orientation === -90) {
              // iPad is in Landscape mode. The screen is turned to the right.
            
              landscapIpad();
          } else if (orientation === 180) {
              // Upside down portrait.
            
              portraitIpad();
          }
      }
  }
});

$(window).resize(function(e) {
   initBottomClass();
});


/*Browser detection patch*/

if(!jQuery.browser){

	jQuery.browser = {};
	jQuery.browser.mozilla = false;
	jQuery.browser.webkit = false;
	jQuery.browser.opera = false;
	jQuery.browser.safari = false;
	jQuery.browser.chrome = false;
	jQuery.browser.msie = false;
	jQuery.browser.android = false;
	jQuery.browser.blackberry = false;
	jQuery.browser.ios = false;
	jQuery.browser.operaMobile = false;
	jQuery.browser.windowsMobile = false;
	jQuery.browser.mobile = false;

	var nAgt = navigator.userAgent;
	jQuery.browser.ua = nAgt;

	jQuery.browser.name  = navigator.appName;
	jQuery.browser.fullVersion  = ''+parseFloat(navigator.appVersion);
	jQuery.browser.majorVersion = parseInt(navigator.appVersion,10);
	var nameOffset,verOffset,ix;

// In Opera, the true version is after "Opera" or after "Version"
	if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
		jQuery.browser.opera = true;
		jQuery.browser.name = "Opera";
		jQuery.browser.fullVersion = nAgt.substring(verOffset+6);
		if ((verOffset=nAgt.indexOf("Version"))!=-1)
			jQuery.browser.fullVersion = nAgt.substring(verOffset+8);
	}

// In MSIE < 11, the true version is after "MSIE" in userAgent
	else if ( (verOffset=nAgt.indexOf("MSIE"))!=-1) {
		jQuery.browser.msie = true;
		jQuery.browser.name = "Microsoft Internet Explorer";
		jQuery.browser.fullVersion = nAgt.substring(verOffset+5);
	}

// In TRIDENT (IE11) => 11, the true version is after "rv:" in userAgent
	else if (nAgt.indexOf("Trident")!=-1 ) {
		jQuery.browser.msie = true;
		jQuery.browser.name = "Microsoft Internet Explorer";
		var start = nAgt.indexOf("rv:")+3;
		var end = start+4;
		jQuery.browser.fullVersion = nAgt.substring(start,end);
	}

// In Chrome, the true version is after "Chrome"
	else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
		jQuery.browser.webkit = true;
		jQuery.browser.chrome = true;
		jQuery.browser.name = "Chrome";
		jQuery.browser.fullVersion = nAgt.substring(verOffset+7);
	}
// In Safari, the true version is after "Safari" or after "Version"
	else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
		jQuery.browser.webkit = true;
		jQuery.browser.safari = true;
		jQuery.browser.name = "Safari";
		jQuery.browser.fullVersion = nAgt.substring(verOffset+7);
		if ((verOffset=nAgt.indexOf("Version"))!=-1)
			jQuery.browser.fullVersion = nAgt.substring(verOffset+8);
	}
// In Safari, the true version is after "Safari" or after "Version"
	else if ((verOffset=nAgt.indexOf("AppleWebkit"))!=-1) {
		jQuery.browser.webkit = true;
		jQuery.browser.name = "Safari";
		jQuery.browser.fullVersion = nAgt.substring(verOffset+7);
		if ((verOffset=nAgt.indexOf("Version"))!=-1)
			jQuery.browser.fullVersion = nAgt.substring(verOffset+8);
	}
// In Firefox, the true version is after "Firefox"
	else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
		jQuery.browser.mozilla = true;
		jQuery.browser.name = "Firefox";
		jQuery.browser.fullVersion = nAgt.substring(verOffset+8);
	}
// In most other browsers, "name/version" is at the end of userAgent
	else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < (verOffset=nAgt.lastIndexOf('/')) ){
		jQuery.browser.name = nAgt.substring(nameOffset,verOffset);
		jQuery.browser.fullVersion = nAgt.substring(verOffset+1);
		if (jQuery.browser.name.toLowerCase()==jQuery.browser.name.toUpperCase()) {
			jQuery.browser.name = navigator.appName;
		}
	}

	/*Check all mobile environments*/
	jQuery.browser.android = (/Android/i).test(nAgt);
	jQuery.browser.blackberry = (/BlackBerry/i).test(nAgt);
	jQuery.browser.ios = (/iPhone|iPad|iPod/i).test(nAgt);
	jQuery.browser.operaMobile = (/Opera Mini/i).test(nAgt);
	jQuery.browser.windowsMobile = (/IEMobile/i).test(nAgt);
	jQuery.browser.mobile = jQuery.browser.android || jQuery.browser.blackberry || jQuery.browser.ios || jQuery.browser.windowsMobile || jQuery.browser.operaMobile;


// trim the fullVersion string at semicolon/space if present
	if ((ix=jQuery.browser.fullVersion.indexOf(";"))!=-1)
		jQuery.browser.fullVersion=jQuery.browser.fullVersion.substring(0,ix);
	if ((ix=jQuery.browser.fullVersion.indexOf(" "))!=-1)
		jQuery.browser.fullVersion=jQuery.browser.fullVersion.substring(0,ix);

	jQuery.browser.majorVersion = parseInt(''+jQuery.browser.fullVersion,10);
	if (isNaN(jQuery.browser.majorVersion)) {
		jQuery.browser.fullVersion  = ''+parseFloat(navigator.appVersion);
		jQuery.browser.majorVersion = parseInt(navigator.appVersion,10);
	}
	jQuery.browser.version = jQuery.browser.majorVersion;

	if((jQuery.browser.name == "Microsoft Internet Explorer" && jQuery.browser.version == 9) || (jQuery.browser.name == "Microsoft Internet Explorer" && jQuery.browser.version == 10)){
		jQuery('img').removeClass('img-responsive');
	}
}