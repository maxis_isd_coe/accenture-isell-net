
$(document).ready(function () {
    flipCard();
    activeTab();
    activeDevice();
    formCheckBox();
    selectPlan();
    selectVas();
    discoverPage();
    selectSRType();
    actionSelection();
});

function selectSRType() {

    $('.sr-sim-button').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('selected-sim')) {
            $(".sr-sim-button").removeClass('selected-sim');
        } else {
            $(".sr-sim-button").removeClass('selected-sim');
            $(this).addClass('selected-sim');
        }
    });
    $('.sr-sim-button button').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('selected-sim')) {
            $(".sr-sim-button").removeClass('selected-sim');
        } else {
            $(".sr-sim-button").removeClass('selected-sim');
            $(this).addClass('selected-sim');
        }
    });
}

function flipCard() {

    var colorId = document.getElementById("getColorId");
    var planId = document.getElementById("getPelanId");
    var contractId = document.getElementById("getContractId");
    var colorcheck = document.getElementById("colorSelected");
    var plancheck = document.getElementById("pelanSelected");
    var contractcheck = document.getElementById("contractSelected");
    var flowtype = document.getElementById("flowType");

    $(".device-checkbox").click(function () {
        
        if (colorcheck.innerHTML != "" && plancheck.innerHTML != "" && contractcheck.innerHTML != "") {
            $(this).prop("checked", true);
        }
        if ($(this).is(":checked")) {
            var group = "input:checkbox[name='" + $(this).attr("name") + "']";
            var liList = document.getElementsByClassName("devices");
            var leng = liList.length;
            //window.alert(leng);
            $(group).prop("checked", false);
            $(this).prop("checked", true);

            
            var planInfo = $("span[id^='plan']");
            if (planInfo.size() > 0) {
                for (var x = 1; x <= planInfo.size(); x++) {
                    if (!(flowtype != null && flowtype.innerHTML == 'Add Contract'))
                        $('#plan' + x).html("Select Plan");
                    
                    $('#color' + x).text('Color');
                    $('#contract' + x).text('Contract');
                }
            }

            if (plancheck.innerHTML == "" && contractcheck.innerHTML == "") {
                var planInfo = $("span[id^='plan']");
                if (planInfo.size() > 0) {
                    for (var x = 1; x <= planInfo.size(); x++) {
                        $('#btnContract' + x).attr('disabled', true);
                    }
                }
            }

            if (flowtype != null && flowtype.innerHTML == 'Add Contract' && colorcheck.innerHTML == "" && contractcheck.innerHTML == "") {
                var planInfo = $("span[id^='plan']");
                if (planInfo.size() > 0) {
                    for (var x = 1; x <= planInfo.size(); x++) {
                        $('#btnContract' + x).attr('disabled', true);
                    }
                }
            }

            //display selected options
            if (colorcheck.innerHTML != "") {
                $('#' + colorId.innerHTML).html(colorcheck.innerHTML);
            }

            if (plancheck.innerHTML != "") {
                $('#' + planId.innerHTML).html("<marquee scrolldelay='125' style='height:auto;padding-top:4px;'>" + plancheck.innerHTML + "</marquee>");
            }
            if (contractcheck.innerHTML != "") {
                $('#' + contractId.innerHTML).html(contractcheck.innerHTML);
            }

            if ($('.card').hasClass('flip')) {
                $('.card').removeClass('flip');
                ClearPriceOrderSummary();
                btnPlanDetails = ""; //Remove varible for ViewPlanDetails

                for (var i = 0; i < leng; i++) {
                    //liList[i].css("z-index", "900");
                    liList[i].style.zIndex = "900";
                }
                $(this).closest("li").css("z-index", "1000");
                $(this).closest(".card").addClass('flip');
            } else {
                for (var i = 0; i < leng; i++) {
                    //liList[i].css("z-index", "900");
                    liList[i].style.zIndex = "900";
                }
                $(this).closest("li").css("z-index", "1000");
                $(this).closest(".card").addClass('flip'); ;
            }
        } else {
            if (colorcheck.innerHTML == "" && plancheck.innerHTML == "" && contractcheck.innerHTML == "") {
            $(this).prop("checked", false);
            }

            var planInfo = $("span[id^='plan']");
            if (planInfo.size() > 0) {
                for (var x = 1; x <= planInfo.size(); x++) {
                    $('#plan' + x).html("Select Plan");
                    $('#color' + x).text('Color');
                    $('#contract' + x).text('Contract');
                }
            }
            
        }
    });
    $('.back-btn').on('click', function (e) {
        e.preventDefault();
        colorcheck = document.getElementById("colorSelected");
        plancheck = document.getElementById("pelanSelected");
        contractcheck = document.getElementById("contractSelected");
        var planInfo = $("span[id^='plan']");
        if (planInfo.size() > 0) {
            for (var x = 1; x <= planInfo.size(); x++) {
                $('#plan' + x).html("Select Plan");
                $('#color' + x).text('Color');
                $('#contract' + x).text('Contract');
            }
        }       
        
        //check if all options selected
        if (colorcheck.innerHTML == "" || plancheck.innerHTML == "" || contractcheck.innerHTML == "") {
            $(".device-checkbox").prop("checked", false);
        }
        $(this).closest(".card").removeClass('flip');
    });
}

//Gregory PN 2484 Clear the selection before
function ClearCardFlip(id, color, plan, contract, flipOnly) {
    
    colorId = document.getElementById("getColorId");
    planId = document.getElementById("getPelanId");
    contractId = document.getElementById("getContractId");
    colorcheck = document.getElementById("colorSelected");
    plancheck = document.getElementById("pelanSelected");
    contractcheck = document.getElementById("contractSelected");

    //20151020 - Samuel Bug 1319 (Error while selecting colour)
    $('#selectedArticleID').val('');
    $('#selectedContractLength').val('');

        //if different device is clicked, clear all value
    if (colorId.innerHTML != color && planId.innerHTML != plan && contractId.innerHTML != contract){
        var planInfo = $("span[id^='plan']");
        if (planInfo.size() > 0) {
            for (var x = 1; x <= planInfo.size(); x++) {
                $('#plan' + x).html("Select Plan");
                $('#color' + x).text('Color');
                $('#contract' + x).text('Contract');
            }
        }
        $('#colorSelected').empty();
        $('#pelanSelected').empty();
        $('#contractSelected').empty();
        $('#getColorId').empty();
        $('#getPelanId').empty();
        $('#getContractId').empty();
    }
    if (colorcheck.innerHTML != "" && plancheck.innerHTML != "" && contractcheck.innerHTML != "" && flipOnly == 'noFlip') {
            var planInfo = $("span[id^='plan']");
            if (planInfo.size() > 0) {
                for (var x = 1; x <= planInfo.size(); x++) {
                    $('#plan' + x).html("Select Plan");
                    $('#color' + x).text('Color');
                    $('#contract' + x).text('Contract');
                }
            }
            $('#colorSelected').empty();
            $('#pelanSelected').empty();
            $('#contractSelected').empty();
            $('#getColorId').empty();
            $('#getPelanId').empty();
            $('#getContractId').empty();
    }
   }

function ClearPriceOrderSummary() {
    $('#summaryDevicePrice').val('');
    $('#summaryDevicePrice').html('');
    $('#summaryPlanPrice').val('');
    $('#summaryPlanPrice').html('');
    $('#summaryPlanAdvancePrice').val('');
    $('#summaryPlanAdvancePrice').html('-');
    $('#summaryDepositPrice').val('');
    $('#summaryDepositPrice').html('-');
    $('#summaryDeviceAdvancePrice').val('');
    $('#summaryDeviceAdvancePrice').html('-');
    $('#summarySubTotalPrice').val('0');
    $('#summarySubTotalPrice').html('RM' + '0');
    $('#summaryGrandTotalPrice').val('0');
    $('#summaryGrandTotalPrice').html('RM' + '0');
    $('#summaryDevice').val('');
    $('#summaryDevice').html('');
    $('#summaryPlan').val('');
    $('#summaryPlan').html('');
    $('#summaryColor').val('');
    $('#summaryColor').html('');
    // clear selection
    $('#selectedArticleID').val('');
    $('#selectedContractLength').val('');

    $('.devicename').text('');
    $('.rrpPrice').text('');
}

function activeTab() {
    $("#line-type a").click(function (e) {
        e.preventDefault();
        $("#line-type li span").removeClass('active-green-arrow');
        $(this).find("span").addClass('active-green-arrow');
        //$( "#line-type .active").find( "span" ).addClass('active-green-arrow');
    });
}

function formCheckBox() {
    $("#type-add-1").click(function () {
        $('#principle').show();
        $('#new-add').hide();
        $("#type-add-2").prop("checked", false);
        $(this).prop("checked", true);
    });
    $("#type-add-2").click(function () {
        $('#principle').hide();
        $('#new-add').show();
        $("#type-add-1").prop("checked", false);
        $(this).prop("checked", true);
    });
    $(".form-checkbox").click(function () {
        
        if ($(this).is(":checked")) {
            var group = ".form-checkbox[name='" + $(this).attr("name") + "']";
            $(group).prop("checked", false);
            $(this).prop("checked", true);
        } else {
            $(this).prop("checked", false);
        }
    });

    $("#cc").click(function () {
        $('#db').prop("checked", false);
        $('.third-party').toggle();
    });
    $("#db").click(function () {
        $('#cc').prop("checked", false);
        $('.third-party').hide();
    });

    $("#exceptional").click(function () {
        if ($(this).is(":checked")) {
            $(this).prop("checked", true);
            $("#loginModal").modal('show');
        } else {
            $(this).prop("checked", false);
        }
    });

}
function activeDevice() {

    //$('.dropdown-toggle').dropdown();
    $("#device li:first-child a").click(function (e) {
        e.preventDefault();
        $("#device a").find("div").removeClass('tablet-selected');
        $(this).find("div").addClass('phone-type-selected');
    });
    $("#device li:last-child a").click(function (e) {
        e.preventDefault();
        $("#device a").find("div").removeClass('phone-type-selected');
        $(this).find("div").addClass('tablet-selected');
    });

    $('.dropdown-menu a').on('click', function (e) {
        e.preventDefault();
    });
    $('.input-group-btn .model').on('click', function (e) {
        e.preventDefault();
    });
}

function actionSelection() {
    $('.action-button').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('selected-action');
//        if ($(this).hasClass('selected-action')) {
//            $(".action-button").removeClass('selected-action');
//            $(this).removeClass('selected-action');
//        } else {
//            $(this).addClass('selected-action');
//        }
    });
}

function selectPlan() {

    $('.plan-details').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('selected-plan')) {
            $(".checkbox-plan").removeClass('checkbox-plan-active');
            $(".plan-details").removeClass('selected-vas');
            $(this).removeClass('selected-vas');
        } else {
            $(".checkbox-plan").removeClass('checkbox-plan-active');
            $(".plan-details").removeClass('selected-vas');
            $(this).find(".checkbox-plan").addClass('checkbox-plan-active');
            $(this).addClass('selected-vas');
        }
    });
    $('.plan-details button').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('selected-plan')) {
            $(".checkbox-plan").removeClass('checkbox-plan-active');
            $(".plan-details").removeClass('selected-plan');
        } else {
            $(".checkbox-plan").removeClass('checkbox-plan-active');
            $(".plan-details").removeClass('selected-plan');
            $(this).find(".checkbox-plan").addClass('checkbox-plan-active');
        }
    });
}

function selectVas() {

    $('.tobe-plan-details').on('click', function (e) {
        e.preventDefault();
        if (!$(this).hasClass('mandatory-vas') && !$(this).hasClass('vas-disabled')) {
            $(this).toggleClass('selected-vas');
            noMultipleSelection(this);
        }
    });

    
    $('.data-list li').on('click', function (e) {
        e.preventDefault();

        //if vas is not selectable, no further action required
        if ($(this).attr("data-is-selectable") == "false")
            return false;

        var method = $(this).attr('data-method');
        eval(method);

        /*
        Ricky: cannot simply add/remove style, has to call the method to check grouping/other rules
        if ($(this).hasClass('selected-vas')) {
            $(".data-list li").removeClass('selected-vas');
            $(this).removeClass('selected-vas');
        } else {
            $(".data-list li").removeClass('selected-vas');
            $(this).addClass('selected-vas');
        }
        */
    });

    $('.other-list li').on('click', function (e) {
        e.preventDefault();

        //if vas is not selectable, no further action required
        if ($(this).attr("data-is-selectable") == "false")
            return false;

        var method = $(this).attr('data-method');
        eval(method);

        /*
        Ricky: cannot simply add/remove style, has to call the method to check grouping/other rules
        if ($(this).hasClass('selected-vas')) {
            $(".data-list li").removeClass('selected-vas');
            $(this).removeClass('selected-vas');
        } else {
            $(".data-list li").removeClass('selected-vas');
            $(this).addClass('selected-vas');
        }
        */
    });
    //remove cursor on selected vas
    $(".included").css('cursor', 'default');

}

function discoverPage() {
    //$("#mega-menu").modal('show');
    /*New PPLESS enc Start*/
    $("#service").on('click', function (e) {
        e.preventDefault();
        $("#main-nav").toggle();
        $("#sub-nav-service").toggle();
    });
    $("#sub-nav-service .submenu.id-check").on('click', function (e) {
        //must customer search
        if (!isPPIdCheckPerformed()) {
            e.preventDefault();
            return false;
        } else {
            //check for form require existing cust only
            if ($(e.target).hasClass("existing-cust") && SessionData.PPID != "E") {
                alert("SR Orders can only be raised for existing Maxis customers.");
                return false;
            }
            //valid-msisdn need any valid msisdn
            if ($(e.target).hasClass("valid-msisdn") &&
                SessionData.HasActivePostGSMLines == "False" && SessionData.HasActivePreGSMLines == "False") {
                alert("No valid MSISDN available for this SR Order type.");
                return false;
            }
            //valid-postmsisdn
            if ($(e.target).hasClass("valid-postmsisdn") && SessionData.HasActivePostGSMLines != "True") {
                alert("No valid MSISDN available for this SR Order type.");
                return false;
            }
            //valid-premsisdn
            if ($(e.target).hasClass("valid-premsisdn") && SessionData.HasActivePreGSMLines != "True") {
                alert("No valid MSISDN available for this SR Order type.");
                return false;
            }
        }
        return true;
    });
    $('#sub-nav-service-header').on('click', function (e) {
        e.preventDefault();
        $("#main-nav").toggle();
        $("#sub-nav-service").toggle();
    });
    $("#sub-nav-service").hide();
    /*New PPLESS enc End*/
    $('#mobile-sme').on('click', function (e) {
        e.preventDefault();
        if (isPPIdCheckPerformed()) {
            $("#main-nav").toggle();
            $("#sub-nav-sme").toggle();
        }
    });
    // Ram:Avoided isPPIdCheckPerformed() for direct Access  to avoid the search of customer first
    $('#mobile').on('click', function (e) {
        e.preventDefault();
        if (isPPIdCheckPerformed()) {
            $("#main-nav").toggle();
            $("#sub-nav").toggle();
        }
    });

    $('#smart').on('click', function (e) {
        e.preventDefault();
        $("#main-nav").toggle();
        $("#sub-nav-smart").toggle();
    });

    $('#sub-nav-mobility-header').on('click', function (e) {
        e.preventDefault();
        $("#main-nav").toggle();
        $("#sub-nav").toggle();
    });

    
    $('#sub-nav-sme-header').on('click', function (e) {
        e.preventDefault();
        $("#main-nav").toggle();
        $("#sub-nav-sme").toggle();
    });

    $('#sub-nav-smart-header').on('click', function (e) {
        e.preventDefault();
        $("#main-nav").toggle();
        $("#sub-nav-smart").toggle();
    });

    //hide the menu by default
    $("#sub-nav-smart").hide();
    $("#sub-nav-sme").hide();
}

function updateDeviceOrderSummary(colorDisplayText, deviceDisplayText, rrpPrice) {
    $('.devicename').text(deviceDisplayText + ' - ' + colorDisplayText);
    $('.rrpPrice').text(rrpPrice);
}


function portraitIpad() {
    //back -cancel button
    $('#back').addClass('col-sm-2');
    $('#back').removeClass('col-sm-1');
    $('#cancel').addClass('col-sm-8');
    $('#cancel').removeClass('col-sm-9');
    //middle & right contents
    $('#show-device').addClass('col-sm-5');
    $('#show-device').removeClass('col-sm-6');
    $('#order-summary').addClass('col-sm-4');
    $('#order-summary').removeClass('col-sm-3');
}
function landscapeIpad() {
    $('#back').removeClass('col-sm-2');
    $('#back').addClass('col-sm-1');
    $('#cancel').removeClass('col-sm-8');
    $('#cancel').addClass('col-sm-9');
    $('#show-device').addClass('col-sm-6');
    $('#show-device').removeClass('col-sm-5');
    $('#order-summary').addClass('col-sm-3');
    $('#order-summary').removeClass('col-sm-4');
}
function initBottomClass() {
    var $containerWidth = $(window).width();
    if ($containerWidth >= 1024) {
        landscapeIpad();
    }
    if ($containerWidth <= 1025) {
        portraitIpad();
    }
}
window.addEventListener('resize', function (event) {
    if (navigator.platform === 'iPad') {
        window.onorientationchange = function () {

            var orientation = window.orientation;

            // Look at the value of window.orientation:
            if (orientation === 0) {
                // iPad is in Portrait mode.

                portraitIpad();

            } else if (orientation === 90) {

                landscapeIpad();
                // iPad is in Landscape mode. The screen is turned to the left.

            } else if (orientation === -90) {
                // iPad is in Landscape mode. The screen is turned to the right.

                landscapeIpad();
            } else if (orientation === 180) {
                // Upside down portrait.

                portraitIpad();
            }
        }
    }
});

$(window).resize(function (e) {
    initBottomClass();
});


/*Browser detection patch*/

if (!jQuery.browser) {

    jQuery.browser = {};
    jQuery.browser.mozilla = false;
    jQuery.browser.webkit = false;
    jQuery.browser.opera = false;
    jQuery.browser.safari = false;
    jQuery.browser.chrome = false;
    jQuery.browser.msie = false;
    jQuery.browser.android = false;
    jQuery.browser.blackberry = false;
    jQuery.browser.ios = false;
    jQuery.browser.operaMobile = false;
    jQuery.browser.windowsMobile = false;
    jQuery.browser.mobile = false;

    var nAgt = navigator.userAgent;
    jQuery.browser.ua = nAgt;

    jQuery.browser.name = navigator.appName;
    jQuery.browser.fullVersion = '' + parseFloat(navigator.appVersion);
    jQuery.browser.majorVersion = parseInt(navigator.appVersion, 10);
    var nameOffset, verOffset, ix;

    // In Opera, the true version is after "Opera" or after "Version"
    if ((verOffset = nAgt.indexOf("Opera")) != -1) {
        jQuery.browser.opera = true;
        jQuery.browser.name = "Opera";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 6);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
    }

    // In MSIE < 11, the true version is after "MSIE" in userAgent
    else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
        jQuery.browser.msie = true;
        jQuery.browser.name = "Microsoft Internet Explorer";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 5);
    }

    // In TRIDENT (IE11) => 11, the true version is after "rv:" in userAgent
    else if (nAgt.indexOf("Trident") != -1) {
        jQuery.browser.msie = true;
        jQuery.browser.name = "Microsoft Internet Explorer";
        var start = nAgt.indexOf("rv:") + 3;
        var end = start + 4;
        jQuery.browser.fullVersion = nAgt.substring(start, end);
    }

    // In Chrome, the true version is after "Chrome"
    else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
        jQuery.browser.webkit = true;
        jQuery.browser.chrome = true;
        jQuery.browser.name = "Chrome";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 7);
    }
    // In Safari, the true version is after "Safari" or after "Version"
    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
        jQuery.browser.webkit = true;
        jQuery.browser.safari = true;
        jQuery.browser.name = "Safari";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 7);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
    }
    // In Safari, the true version is after "Safari" or after "Version"
    else if ((verOffset = nAgt.indexOf("AppleWebkit")) != -1) {
        jQuery.browser.webkit = true;
        jQuery.browser.name = "Safari";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 7);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
    }
    // In Firefox, the true version is after "Firefox"
    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
        jQuery.browser.mozilla = true;
        jQuery.browser.name = "Firefox";
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
    }
    // In most other browsers, "name/version" is at the end of userAgent
    else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
        jQuery.browser.name = nAgt.substring(nameOffset, verOffset);
        jQuery.browser.fullVersion = nAgt.substring(verOffset + 1);
        if (jQuery.browser.name.toLowerCase() == jQuery.browser.name.toUpperCase()) {
            jQuery.browser.name = navigator.appName;
        }
    }

    /*Check all mobile environments*/
    jQuery.browser.android = (/Android/i).test(nAgt);
    jQuery.browser.blackberry = (/BlackBerry/i).test(nAgt);
    jQuery.browser.ios = (/iPhone|iPad|iPod/i).test(nAgt);
    jQuery.browser.operaMobile = (/Opera Mini/i).test(nAgt);
    jQuery.browser.windowsMobile = (/IEMobile/i).test(nAgt);
    jQuery.browser.mobile = jQuery.browser.android || jQuery.browser.blackberry || jQuery.browser.ios || jQuery.browser.windowsMobile || jQuery.browser.operaMobile;


    // trim the fullVersion string at semicolon/space if present
    if ((ix = jQuery.browser.fullVersion.indexOf(";")) != -1)
        jQuery.browser.fullVersion = jQuery.browser.fullVersion.substring(0, ix);
    if ((ix = jQuery.browser.fullVersion.indexOf(" ")) != -1)
        jQuery.browser.fullVersion = jQuery.browser.fullVersion.substring(0, ix);

    jQuery.browser.majorVersion = parseInt('' + jQuery.browser.fullVersion, 10);
    if (isNaN(jQuery.browser.majorVersion)) {
        jQuery.browser.fullVersion = '' + parseFloat(navigator.appVersion);
        jQuery.browser.majorVersion = parseInt(navigator.appVersion, 10);
    }
    jQuery.browser.version = jQuery.browser.majorVersion;

    if ((jQuery.browser.name == "Microsoft Internet Explorer" && jQuery.browser.version == 9) || (jQuery.browser.name == "Microsoft Internet Explorer" && jQuery.browser.version == 10)) {
        jQuery('img').removeClass('img-responsive');
    }
}
