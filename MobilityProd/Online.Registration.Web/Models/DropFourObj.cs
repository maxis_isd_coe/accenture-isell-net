﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using Online.Registration.Web.ViewModels;
using Online.Registration.Web.SubscriberICService;
using Online.Registration.Web.ContentRepository;
using Online.Registration.Web.Helper;
using Online.Registration.Web.CommonEnum;
using SNT.Utility;

namespace Online.Registration.Web.Models
{
    [Serializable]
    public class DropFourObj
    {
		private static readonly ILog Logger = LogManager.GetLogger(typeof(DropFourObj));
        public DropFourObj()
        {
            mainFlow = new Flow();
            mainFlow.RegType = -1;
            suppIndex = 0;
            msimIndex = 0;
            suppFlow = new List<Flow>();
            msimFlow = new List<Flow>();
            currentFlow = DropFourConstant.MAIN_FLOW;
            IsBack = false;
			selectedMobileNoinFlow = new List<string>();
            ApproverID = 0;
            TrnType = "R";//09042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
			DFcustomerEligibility = false;
            AFcustomerEligibility = false;
            ApprovalVM = new ApprovalModels();
        }
        public ApprovalModels ApprovalVM { get; set; }
        public bool IsBack { get; set; }
        public Flow mainFlow { get; set; }
        public string currentFlow { get; set; }
        public List<Flow> suppFlow { get; set; }
        public List<Flow> msimFlow { get; set; }
        public int suppIndex { get; set; }
        public int msimIndex { get; set; }
        public PersonalDetailsVM personalInformation { get; set; }
		public List<string> selectedMobileNoinFlow { get; set; }

        // habs - 20141119 - start
        public String customerLiberalisation { get; set; }
        public String liberalisationRank { get; set; }
        // habs - 20141119 - end

		public bool DFcustomerEligibility { get; set; }

        public bool AFcustomerEligibility { get; set; }
        
		// this is will contains quantity for existing IC search
		public int existingTotalLine { get; set; }
		public int existingContract { get; set; }
		public int additionalContract { get; set; }

        public int additionalDFContract { get; set; }
        public int additionalAFContract { get; set; }

        // 20141202 - Implement number of lines + contract validation - start
        public retrieveAcctListByICResponse allCustomerDetails { get; set; }
        // 20141202 - Implement number of lines + contract validation - end

        //20150115 - temporary Store Card Number variable - start
        public string tempCardNumber{get; set;}
        //20150115 - temporary Store Card Number variable - end

        //20150120 -- temporary store Approver manual waver - start
        public int ApproverID { get; set; }
        //20150120 -- temporary store Approver manual waver - end

		public bool FromMobileDevice { get; set; }

        public string TrnType { get; set; }//09042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow

        public int maxSupp
        {
            get
            {
                return Properties.Settings.Default.MaxSupplimentaryLine;
            }
        }
        public int maxMsim
        {
            get
            {
                return Properties.Settings.Default.MaxMSIMLine;
            }
        }

        // 20141202 - Implement number of lines + contract validation - start
        public int getNewLineCount()
        {
            int count = 0;
            var isOldFlow = this.isOldFlow();//06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow

            if (this.mainFlow != null && this.mainFlow.RegType != -1)
            {
                if (!isOldFlow)//06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
                {
                    count = 1;
                }
            }

            if (this.suppFlow != null)
            {
                count = count + this.suppFlow.Count;
            }

            /* R - 24 Jun 15 - totalline check not count mism
            if (this.msimFlow != null)
            {
                count = count + this.msimFlow.Count;
            }*/

            return count;
        }

        public int getNewContractCount()
        {
            int count = 0;

            if (this.mainFlow != null)
            {
                count = 1;
            }

            if (this.suppFlow != null)
            {
                count = count + this.suppFlow.Count;
            }

            return count;
        }
		
        public int getExistingLineCount()
        {
			// this calculation is exactly same with _displayuserdetails
			// if there's changes to this method, please change to _displayuserdetails
			// need to bring here, because for MEPS they search for msisdn, so only 1 account is listed, so the total line will wrong.
			
			int totalLineCount = 0;
			if (ReferenceEquals(HttpContext.Current.Session["AllServiceDetails"], null))
			{
				return 0;
			}

			List<string> mismList = new List<string>();
			List<string> supplementaryList = new List<string>();
			List<string> principalList = new List<string>();
			List<string> broadbandList = new List<string>();
			List<string> VOIPList = new List<string>();
			List<string> FTTHList = new List<string>();
			
			int suppCount = 0;
			int principalCount = 0;
			int msim = 0;
			int broadband = 0;
			int VOIPCount = 0;
			int FTTHCount = 0;

			retrieveAcctListByICResponse AcctListByICResponse = null;
			if (!ReferenceEquals(HttpContext.Current.Session["AllServiceDetails"], null))
			{
				AcctListByICResponse = (retrieveAcctListByICResponse)HttpContext.Current.Session["AllServiceDetails"];
				var AcctListByICResponseLists = !ReferenceEquals(AcctListByICResponse, null) && !ReferenceEquals(AcctListByICResponse.itemList, null) ? AcctListByICResponse.itemList.Where(x => x.ServiceInfoResponse != null).ToList() : null;

				foreach (string acctExtID in AcctListByICResponseLists.Select(x => x.AcctExtId).Distinct())
				{
					var extIDParentDetails = AcctListByICResponseLists.Where(x => x.AcctExtId.Equals(acctExtID)).ToList();

					foreach (var v in extIDParentDetails)
					{
						string prinSuppInd = !string.IsNullOrEmpty(v.ServiceInfoResponse.prinSuppInd) ? v.ServiceInfoResponse.prinSuppInd : string.Empty;
                        string lob = !string.IsNullOrEmpty(v.ServiceInfoResponse.lob) ? v.ServiceInfoResponse.lob : string.Empty;

						if (!prinSuppInd.Equals("S") && ((lob.Equals("POSTGSM") && string.IsNullOrEmpty(prinSuppInd)) || (lob.Equals("POSTGSM") && !string.IsNullOrEmpty(prinSuppInd)) || (!lob.Equals("POSTGSM") && string.IsNullOrEmpty(prinSuppInd))))
						{
							try
							{
								var extIDChildDetails = AcctListByICResponseLists.Where(x => x.AcctExtId.Equals(v.AcctExtId));
								foreach (var a in extIDChildDetails)
								{
									if (a.SecondarySimList != null && a.SecondarySimList.Count() > 0)
									{
										foreach (var msism1 in a.SecondarySimList)
										{
											if (!mismList.Contains(msism1.Msisdn))
											{
												msim++;
												mismList.Add(msism1.Msisdn);
											}
										}
									}


									foreach (var vi in AcctListByICResponseLists.Where(c => c.ExternalId == a.ExternalId).ToList())
									{
										if (!string.IsNullOrEmpty(vi.ServiceInfoResponse.prinSuppInd) && vi.ServiceInfoResponse.prinSuppInd == "P")
										{
											if (!principalList.Contains(vi.ExternalId))
											{
												principalCount++;
												principalList.Add(vi.ExternalId);
											}

										}
									}

									if (a.PrinSuppResponse != null && a.PrinSuppResponse.itemList.Count > 0)
									{
										foreach (var vi in AcctListByICResponseLists.Where(c => c.ExternalId == a.ExternalId).ToList().Select(i => i.PrinSuppResponse.itemList.ToList()))
										{
											for (int i = 0; i <= vi.Count - 1; i++)
											{
												if (!supplementaryList.Contains(vi[i].msisdnField))
												{
													suppCount++;
													supplementaryList.Add(vi[i].msisdnField);
												}
											}
										}
									}
									if (!string.IsNullOrEmpty(a.ServiceInfoResponse.lob) && a.ServiceInfoResponse.lob.Equals("HSDPA"))
									{
										if (!broadbandList.Contains(a.ExternalId))
										{
											broadband++;
											broadbandList.Add(a.ExternalId);
										}

									}
									if (!string.IsNullOrEmpty(a.ServiceInfoResponse.lob) && a.ServiceInfoResponse.lob.Equals("POSTGSM") && string.IsNullOrEmpty(a.ServiceInfoResponse.prinSuppInd))
									{
										if (!VOIPList.Contains(a.ExternalId))
										{
											VOIPCount++;
											VOIPList.Add(a.ExternalId);
										}
									}

									if (!string.IsNullOrEmpty(a.ServiceInfoResponse.lob) && a.ServiceInfoResponse.lob.Equals("FTTH") && string.IsNullOrEmpty(a.ServiceInfoResponse.prinSuppInd))
									{
										if (!FTTHList.Contains(a.ExternalId))
										{
											FTTHCount++;
											FTTHList.Add(a.ExternalId);
										}

									}
								}
							}
							catch (Exception ex)
							{
								;
							}
						}
					} // end loop for one account
				}
			}
			string searchNumber = HttpContext.Current.Session["cardNumber"].ToString2();
			Logger.Info(string.Format("LINECOUNT={0}", searchNumber));
			Logger.Info(string.Format("principal={0}, suppCount={1}, msim={2}, broadband={3}, suppCount={4}", principalCount, suppCount,msim,broadband,VOIPCount));

			totalLineCount = principalCount + suppCount + broadband + VOIPCount + FTTHCount;
            HttpContext.Current.Session["existingTotalLine"] = totalLineCount;
			return totalLineCount;
        }

        public DropFourObj getExistingContractCount(DropFourObj _dropObj)
        {
			Logger.Info("ContractCount - Start");
			_dropObj.existingContract = 0;
			List<string> contractKenanCodes = new List<string>();
			retrieveAcctListByICResponse AllServiceDetails = null;

			if (ReferenceEquals(HttpContext.Current.Session["AllServiceDetails"], null))
			{
				_dropObj.existingContract = 0;
			}
			if (System.Configuration.ConfigurationManager.AppSettings["ContractKenanCode"] != null)
			{
				contractKenanCodes = System.Configuration.ConfigurationManager.AppSettings["ContractKenanCode"].ToString2().Split(',').ToList();
			}
			var lstContractsAll = MasterDataCache.Instance.AllContractList.ToList();
			IEnumerable<string> lstContracts = lstContractsAll.Select(c => c.KenanCode);
			
			if (!ReferenceEquals(HttpContext.Current.Session["AllServiceDetails"], null))
			{
				AllServiceDetails = (retrieveAcctListByICResponse)HttpContext.Current.Session["AllServiceDetails"];
			}
			bool contractcheckflag = false;
			int contractMonths = 0;
            string kenanCodeString = string.Empty; //for debug
            // below is for existing account
			#region contract check calculation
			if (!ReferenceEquals(AllServiceDetails, null))
			{
				if (AllServiceDetails.itemList.Count() > 0)
				{
					var _serviceList = AllServiceDetails.itemList.Where(p => p.Packages != null);
					if (!ReferenceEquals(_serviceList, null))
					{
						foreach (var _accList in _serviceList)
						{
							List<Online.Registration.Web.SubscriberICService.PackageModel> packages = _accList.Packages;
							foreach (var contract in packages)
							{
								for (int i = 0; i < contract.compList.Count; i++)
								{
									if (contractKenanCodes.Contains(contract.compList[i].componentId))
									{
										DateTime InactDate;
										if (string.IsNullOrEmpty(contract.compList[i].componentInactiveDt))
										{
											DateTime ActiveDate = DateTime.ParseExact(contract.compList[i].componentActiveDt.Substring(0, 8), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
											contractMonths = lstContractsAll.Where(c => c.KenanCode == contract.compList[i].componentId).Select(cd => cd.Duration).FirstOrDefault();
											InactDate = ActiveDate.AddMonths(contractMonths);
										}
										else
										{
											InactDate = DateTime.ParseExact(contract.compList[i].componentInactiveDt.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
										}
										if (DateTime.Compare(InactDate, DateTime.Now) > 0)
										{ //contractCheckCount++; 
											contractcheckflag = true;
										}
									}
									if (contractcheckflag == true)
									{
										contractcheckflag = false;
										_dropObj.existingContract++;
                                        kenanCodeString += ","+contract.compList[i].componentId;
									}
								}
							}
						}
					}
				}
			}
			#endregion

			// below is for existing order in flow
			_dropObj.additionalContract = 0;
			var MasterComponent = MasterDataCache.Instance.PackageComponents.ToList();
            #region Drop 5: BRE Check for non-Drop 4 Flow
            //06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - Start
            var isOldFlow = this.isOldFlow();
            var isSMARTFlow = this.isSMARTFlow();
            var isAssignContract = this.isAssignContract();
            var SMARTContractID = isSMARTFlow ? HttpContext.Current.Session[SessionKey.RegMobileReg_ContractID.ToString()].ToInt() : 0;
            //06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - End
            #endregion

			if (!ReferenceEquals(_dropObj, null))
			{
				// this is for main flow  
				if (!ReferenceEquals(_dropObj.mainFlow, null) && !ReferenceEquals(_dropObj.mainFlow.vas, null))
				{
                    if ((isOldFlow && isAssignContract) || !isOldFlow)//06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow
                    {
                        if (!string.IsNullOrEmpty(_dropObj.mainFlow.vas.SelectedContractID))
                        {
                            var componentList = MasterComponent.Where(x => x.ID == _dropObj.mainFlow.vas.SelectedContractID.ToInt());

                            if (componentList.Any() && componentList.Count() > 0)
                            {
                                if (contractKenanCodes.Contains(componentList.First().KenanCode))
                                    _dropObj.additionalContract++;
                            }
                        }
                    }
                }
                else if (SMARTContractID > 0 && isAssignContract)
                {
                    var componentList = MasterComponent.Where(x => x.ID == SMARTContractID);

                    if (componentList.Any() && componentList.Count() > 0)
                    {
                        if (contractKenanCodes.Contains(componentList.First().KenanCode))
                            _dropObj.additionalContract++;
                    }
                }
                //else if ((isOldFlow || isSMARTFlow) && isAssignContract)
                else if (isSMARTFlow && isAssignContract)
                {
                    _dropObj.additionalContract++;
                }

                if ((isOldFlow && isAssignContract) || !isOldFlow)
                {
                    if (_dropObj.mainFlow.isAccFinancingOrder)
                    {
                        _dropObj.additionalContract++;
                    }
                }
                // this is for supp flow
				if (_dropObj.suppIndex > 0)
				{ 
					foreach(var _supp in _dropObj.suppFlow)
					{
						if (!ReferenceEquals(_supp, null) && !ReferenceEquals(_supp.vas, null))
						{
							if (!string.IsNullOrEmpty(_supp.vas.SelectedContractID))
							{
                                var componentList = _supp.vas.SelectedContractID.Contains(",") ? MasterComponent.Where(x => x.ID == _supp.vas.SelectedContractID.Split(',')[0].ToInt()) : MasterComponent.Where(x => x.ID == _supp.vas.SelectedContractID.ToInt());
                                
								if (componentList.Any() && componentList.Count() > 0)
								{
									if (contractKenanCodes.Contains(componentList.First().KenanCode))
										_dropObj.additionalContract++;
								}
							}

                            if (_supp.isAccFinancingOrder)
                            {
                                _dropObj.additionalContract++;
                            }
						}
					}
				}
			}

			Logger.Info("ContractCount - End");
			return _dropObj;
        }
        // 20141202 - Implement number of lines + contract validation - end

        public DropFourObj GetAdditionalZerolutionContractCount()
        {
            var dropObj = (Online.Registration.Web.Models.DropFourObj)HttpContext.Current.Session[Online.Registration.Web.CommonEnum.SessionKey.DropFourObj.ToString()];
            var isServicingFlow = dropObj.isOldFlow();
            var isAssignContract = dropObj.isAssignContract();
            var DF_Count = 0;
            var AF_Count = 0;

            #region MAIN_FLOW
            if (dropObj.mainFlow != null && (isServicingFlow == false || (isServicingFlow && isAssignContract)))
            {
                if (dropObj.mainFlow.isDeviceFinancingOrder)
                    DF_Count++;

                if (dropObj.mainFlow.isAccFinancingOrder)
                    AF_Count++;
            }
            #endregion

            #region SUPP_FLOW
            var DF_contractSuppCount = 0;
            var AF_contractSuppCount = 0;

            if (dropObj.suppFlow != null && dropObj.suppFlow.Count > 0 && dropObj.suppFlow.Where(x => x.isDeviceFinancingOrder == true).Any())
            {
                DF_contractSuppCount = dropObj.suppFlow.Where(x => x.isDeviceFinancingOrder == true).ToList().Count;
                //DF_Count += DF_contractSuppCount;
            }

            if (dropObj.suppFlow != null && dropObj.suppFlow.Count > 0 && dropObj.suppFlow.Where(x => x.isAccFinancingOrder == true).Any())
            {
                AF_contractSuppCount = dropObj.suppFlow.Where(x => x.isDeviceFinancingOrder == true).ToList().Count;
                //AF_Count += AF_contractSuppCount;
            }

            foreach (var suppFlow in dropObj.suppFlow)
            {
                if (suppFlow.isDeviceFinancingOrder)
                    DF_Count++;

                if (suppFlow.isAccFinancingOrder)
                    AF_Count++;
            }
            #endregion

            dropObj.additionalDFContract = DF_Count;
            dropObj.additionalAFContract = AF_Count;

            return dropObj;
        }

        #region Drop 5: BRE Check for non-Drop 4 Flow
        //06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - Start
        /// <summary>
        /// This method is to determine if it is Old Flow or New Flow
        /// </summary>
        /// <param name="dropObj"></param>
        /// <returns></returns>
        public bool isOldFlow()
        {
            var isOldFlow = false;
            var isSMART = this.isSMARTFlow();

            if (this.mainFlow != null)
            {
                isOldFlow = (this.mainFlow.RegType == MobileRegType.Contract.ToInt() 
                                || this.mainFlow.RegType == MobileRegType.DeviceCRP.ToInt()
                                || this.mainFlow.RegType == MobileRegType.CRP.ToInt()
                                || this.mainFlow.RegType == MobileRegType.AddRemoveVAS.ToInt()
                                || this.mainFlow.RegType == MobileRegType.SimReplacement.ToInt()
                                || isSMART);
            }

            return isOldFlow;
        }

        /// <summary>
        /// This method is to determine whether the flow is SMART or not
        /// </summary>
        /// <returns></returns>
        public bool isSMARTFlow()
        {
            var isSMARTFlow = false;

            if (!string.IsNullOrEmpty(this.TrnType))
            {
                isSMARTFlow = (this.TrnType == "S");
            }

            return isSMARTFlow;
        }

        /// <summary>
        /// This method is t determine if it is Assign Contract or Terminate & Assign New Contract
        /// </summary>
        /// <returns></returns>
        public bool isAssignContract()
        {
            var isAssignContract = false;//Terminate & Assign New / no Contract added
            var contractDetails = HttpContext.Current.Session[SessionKey.ContractDetails.ToString2()] as List<ContractDetails>;
            var SMARTContractDetails = HttpContext.Current.Session[SessionKey.ContractDetails.ToString2()] as List<Online.Registration.Web.SmartViewModels.ContractDetails>;
            var contractType = (HttpContext.Current.Session["ContractType"].ToString2() == "Terminate" || HttpContext.Current.Session["ContractType"].ToString2() == "Terimnate");
            var addContractModel = HttpContext.Current.Session["model"] as DeviceVM;
            var crpModel = HttpContext.Current.Session["modelCRP"] as ContractCheckVM;
            var subAction = string.Empty;

            if (addContractModel != null)
            {
                subAction = addContractModel.subAction;
            }
            else if (crpModel != null)
            {
                subAction = crpModel.SubAction;
            }

            // CRP PtoSup/StoPrin with device
            bool isCRPDevice = (HttpContext.Current.Session["ContractType"].ToString2() == "PtoSupline" || HttpContext.Current.Session["ContractType"].ToString2() == "StoPrinciple") && HttpContext.Current.Session["IsDeviceRequire"].ToString2() == "Yes";

            if (contractDetails != null)
            {
                if ((contractDetails.Count <= 0 && contractType) || isCRPDevice)
                {
                    isAssignContract = true;//Assign Contract
                }
            }
            else if (SMARTContractDetails != null)
            {
                if (SMARTContractDetails.Count <= 0 && contractType)
                {
                    isAssignContract = true;//Assign Contract
                }
            }

            if(!string.IsNullOrEmpty(subAction))
            {
                switch(subAction)
                {
                    case "AssignOrTerminateDeviceOnly":
                        var dfContracts = contractDetails != null && contractDetails.Where(x => x.isellContractGroup == "DF").Any();
                        isAssignContract = dfContracts == false;
                        break;
                    case "AssignOrTerminateAccOnly":
                        var afContracts = contractDetails != null && contractDetails.Where(x => x.isellContractGroup == "AF").Any();
                        isAssignContract = afContracts == false;
                        break;
                    case "AssignOrTerminateAll":
                        var allContracts = contractDetails != null && contractDetails.Any();
                        isAssignContract = allContracts == false;
                        break;
                    default:
                        break;
                }
            }

            return isAssignContract;
        }

        public void SetAdditionalContract()
        {
            HttpContext.Current.Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract();
            
            if (this.isOldFlow() && this.isAssignContract())
            {
                HttpContext.Current.Session["AccountswithContractCount"] = WebHelper.Instance.CountExistContract() + 1;
            }
        }
        //06042015 - Anthony - Drop 5: BRE Check for non-Drop 4 Flow - End
        #endregion
    }
	
	[Serializable]
    public class DropFourConstant
    {
        public const string MAIN_FLOW = "MAIN_FLOW";
        public const string SUPP_FLOW = "SUPP_FLOW";
        public const string MSISM_FLOW = "MSISM_FLOW";
    }

	[Serializable]
    public class DropFourHelpers
    {
        #region device
        public static void UpdateDevice(DropFourObj obj, DeviceOptionVM device)
        {
            OrderSummaryVM orderSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_OrderSummary"];
            if (obj.currentFlow == DropFourConstant.MAIN_FLOW)
            {
                obj.mainFlow.device = device;
                obj.mainFlow.orderSummary = orderSummary;
            }
            else if (obj.currentFlow == DropFourConstant.SUPP_FLOW)
            {
                obj.suppFlow[obj.suppIndex - 1].device = device;
                obj.suppFlow[obj.suppIndex - 1].orderSummary = orderSummary;
            }
            else if (obj.currentFlow == DropFourConstant.MSISM_FLOW)
            {
                obj.msimFlow[obj.msimIndex - 1].device = device;
                obj.msimFlow[obj.msimIndex - 1].orderSummary = orderSummary;
            }
        }
        #endregion

        #region phone
        public static void UpdatePhone(DropFourObj obj, PhoneVM phone)
        {
            OrderSummaryVM orderSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_OrderSummary"];
			decimal RRPDevicePrice = HttpContext.Current.Session["RegMobileReg_RRPDevicePrice"] != null ? (decimal)HttpContext.Current.Session["RegMobileReg_RRPDevicePrice"] : 0;
			decimal OfferDevicePrice = HttpContext.Current.Session["RegMobileReg_OfferDevicePrice"] != null ? (decimal)HttpContext.Current.Session["RegMobileReg_OfferDevicePrice"] : 0;

			if (obj.currentFlow == DropFourConstant.MAIN_FLOW)
			{
				obj.mainFlow.phone = phone;
				obj.mainFlow.orderSummary = orderSummary;
				obj.mainFlow.RRPDevicePrice = Convert.ToDouble(RRPDevicePrice);
				obj.mainFlow.OfferDevicePrice = Convert.ToDouble(OfferDevicePrice);
			}
			else if (obj.currentFlow == DropFourConstant.SUPP_FLOW)
			{
				var sublineSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_SublineSummary"];
				obj.suppFlow[obj.suppIndex - 1].phone = phone;
				obj.suppFlow[obj.suppIndex - 1].orderSummary = sublineSummary != null ? sublineSummary : orderSummary;
				obj.suppFlow[obj.suppIndex - 1].RRPDevicePrice = Convert.ToDouble(RRPDevicePrice);
				obj.suppFlow[obj.suppIndex - 1].OfferDevicePrice = Convert.ToDouble(OfferDevicePrice);
			}
			else if (obj.currentFlow == DropFourConstant.MSISM_FLOW)
			{
				obj.msimFlow[obj.msimIndex - 1].phone = phone;
				obj.msimFlow[obj.msimIndex - 1].orderSummary = orderSummary;
				obj.msimFlow[obj.suppIndex - 1].RRPDevicePrice = Convert.ToDouble(RRPDevicePrice);
				obj.msimFlow[obj.suppIndex - 1].OfferDevicePrice = Convert.ToDouble(OfferDevicePrice);
			}
        }
        #endregion phone

        #region package
        public static void UpdatePackage(DropFourObj obj, PackageVM package)
        {
            OrderSummaryVM orderSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_OrderSummary"];
            int PkgPgmBdlPkgCompID = HttpContext.Current.Session["RegMobileReg_PkgPgmBdlPkgCompID"] != null ? HttpContext.Current.Session["RegMobileReg_PkgPgmBdlPkgCompID"].ToInt() : 0;
            if (obj.currentFlow == DropFourConstant.MAIN_FLOW)
            {
                obj.mainFlow.package = package;
                obj.mainFlow.orderSummary = orderSummary;
                obj.mainFlow.PkgPgmBdlPkgCompID = PkgPgmBdlPkgCompID;
            }
            else if (obj.currentFlow == DropFourConstant.SUPP_FLOW)
            {
                var sublineSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_SublineSummary"];
                obj.suppFlow[obj.suppIndex - 1].package = package;
                obj.suppFlow[obj.suppIndex - 1].orderSummary = sublineSummary != null ? sublineSummary : orderSummary;
                obj.suppFlow[obj.suppIndex - 1].PkgPgmBdlPkgCompID = PkgPgmBdlPkgCompID;
            }
            else if (obj.currentFlow == DropFourConstant.MSISM_FLOW)
            {
                obj.msimFlow[obj.msimIndex - 1].package = package;
                obj.msimFlow[obj.msimIndex - 1].orderSummary = orderSummary;
                obj.msimFlow[obj.msimIndex - 1].PkgPgmBdlPkgCompID = PkgPgmBdlPkgCompID;
            }
        }
        #endregion

		public static void UpdateDeviceVM(DropFourObj obj, DeviceVM deviceVM  )
		{
			if (obj.currentFlow == DropFourConstant.MAIN_FLOW)
			{
				obj.mainFlow.deviceVM = deviceVM;
				obj.mainFlow.subscribedCompKenanCodes = deviceVM.Components.SubscribedComponentIds;
				obj.mainFlow.vas.SelectedVasIDs = deviceVM.VasComponents;
				var tempSelectedVasKenanCodes = WebHelper.Instance.convertPBPCIDtoKenanCode(deviceVM.VasComponents);
				obj.mainFlow.vas.SelectedVasKenanCodes = tempSelectedVasKenanCodes != null && tempSelectedVasKenanCodes.Any() ? string.Join(",", tempSelectedVasKenanCodes) : string.Empty;
			}
		}

		public static void UpdateOrderSummaryOnly(DropFourObj obj)
		{
			OrderSummaryVM orderSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_OrderSummary"];
			if (obj.currentFlow == DropFourConstant.MAIN_FLOW)
			{
				obj.mainFlow.orderSummary = orderSummary;
			}
			else if (obj.currentFlow == DropFourConstant.SUPP_FLOW)
			{
				var sublineSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_SublineSummary"];
				obj.suppFlow[obj.suppIndex - 1].orderSummary = sublineSummary != null ? sublineSummary : orderSummary;
			}
			else if (obj.currentFlow == DropFourConstant.MSISM_FLOW)
			{
				obj.msimFlow[obj.msimIndex - 1].orderSummary = orderSummary;
			}
		}

        #region vas
        public static void UpdateVAS(DropFourObj obj, ValueAddedServicesVM vas)
        {
            OrderSummaryVM orderSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_OrderSummary"];
            List<string> RegMobileReg_VasNames = new List<string>();
            if (null != HttpContext.Current.Session["RegMobileReg_VasNames"])
            {
                if (HttpContext.Current.Session["RegMobileReg_VasNames"].ToString() == typeof(List<AvailableVAS>).ToString())
                {
                    var vasNames = (List<AvailableVAS>)HttpContext.Current.Session["RegMobileReg_VasNames"];
                    if (vasNames != null)
                    {
                        foreach (var name in vasNames)
                        {
                            RegMobileReg_VasNames.Add(name.ComponentName + "RM " + name.Price);
                        }
                    }
                }
                else
                {
                    RegMobileReg_VasNames = (List<string>)HttpContext.Current.Session["RegMobileReg_VasNames"];
                }

                string UOMCodeFromSession = (string)HttpContext.Current.Session["RegMobileReg_UOMCode"];
                var OfferDevicePriceFromSession = HttpContext.Current.Session["RegMobileReg_OfferDevicePrice"].ToInt();

                if (obj.currentFlow == DropFourConstant.MAIN_FLOW)
                {
                    obj.mainFlow.vas = vas;
                    obj.mainFlow.VasNames = RegMobileReg_VasNames;
                    obj.mainFlow.orderSummary = orderSummary;
					obj.mainFlow.UOMCode = vas!= null && !string.IsNullOrEmpty(vas.uomCode)? vas.uomCode : UOMCodeFromSession;
                    obj.mainFlow.SelectedVASID = (string)HttpContext.Current.Session["RegMobileReg_VasIDs"];
                    obj.mainFlow.SelectedContractID = HttpContext.Current.Session["RegMobileReg_ContractID"].ToString2();
                    obj.mainFlow.SelectedDataPlanID = (int[])HttpContext.Current.Session["RegMobileReg_DataplanID"];
					obj.mainFlow.OfferDevicePrice = !string.IsNullOrEmpty(vas.price) ? Convert.ToDecimal(vas.price).ToDouble() : OfferDevicePriceFromSession;
					if (obj.mainFlow.isDeviceFinancingOrder)
					{
						using (var catProxy = new CatalogServiceProxy())
						{
							try {
								var df_MasterList = catProxy.GetAllDeviceFinancingInfo();
								var df_row = df_MasterList.Where(x => x.PlanID == obj.mainFlow.PkgPgmBdlPkgCompID && x.ArticleID == obj.mainFlow.phone.DeviceArticleId
									&& x.UOMCode.Contains(obj.mainFlow.UOMCode)
									).FirstOrDefault();
								obj.mainFlow.monthlyDevicePrice = Convert.ToDecimal(df_row.MonthlyPrice);
								obj.mainFlow.DeviceFinancingOrderDiscount = Convert.ToDecimal(df_row.discount_amount);
							}
							catch(Exception ex)
							{
								//
							}
							
						}
					}
					else {
						//obj.mainFlow.OfferDevicePrice = !string.IsNullOrEmpty(vas.price) ? vas.price.ToInt() : OfferDevicePriceFromSession;
					}

                }
                else if (obj.currentFlow == DropFourConstant.SUPP_FLOW)
                {
                    var sublineSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_SublineSummary"];
                    obj.suppFlow[obj.suppIndex - 1].vas = vas;
                    obj.suppFlow[obj.suppIndex - 1].VasNames = RegMobileReg_VasNames;
                    obj.suppFlow[obj.suppIndex - 1].orderSummary = sublineSummary != null ? sublineSummary : orderSummary;
					obj.suppFlow[obj.suppIndex - 1].UOMCode = vas != null && !string.IsNullOrEmpty(vas.uomCode) ? vas.uomCode : UOMCodeFromSession;
                    obj.suppFlow[obj.suppIndex - 1].SelectedVASID = (string)HttpContext.Current.Session["RegMobileReg_VasIDs"];
					//obj.suppFlow[obj.suppIndex - 1].SelectedContractID = (string)HttpContext.Current.Session["RegMobileReg_ContractID"];
					obj.suppFlow[obj.suppIndex - 1].SelectedContractID = vas.SelectedContractID;
                    obj.suppFlow[obj.suppIndex - 1].SelectedDataPlanID = (int[])HttpContext.Current.Session["RegMobileReg_DataplanID"];
                    obj.suppFlow[obj.suppIndex - 1].OfferDevicePrice = !string.IsNullOrEmpty(vas.price) ? vas.price.ToInt() : OfferDevicePriceFromSession;

					if (obj.suppFlow[obj.suppIndex - 1].isDeviceFinancingOrder)
					{
						using (var catProxy = new CatalogServiceProxy())
						{
							try
							{
								var df_MasterList = catProxy.GetAllDeviceFinancingInfo();
								var df_row = df_MasterList.Where(x => x.PlanID == obj.suppFlow[obj.suppIndex - 1].PkgPgmBdlPkgCompID && x.ArticleID == obj.suppFlow[obj.suppIndex - 1].phone.DeviceArticleId
									&& x.UOMCode.Contains(obj.suppFlow[obj.suppIndex - 1].UOMCode)
									).FirstOrDefault();
								obj.suppFlow[obj.suppIndex - 1].monthlyDevicePrice = Convert.ToDecimal(df_row.MonthlyPrice);
								obj.suppFlow[obj.suppIndex - 1].DeviceFinancingOrderDiscount = Convert.ToDecimal(df_row.discount_amount);
							}
							catch (Exception ex)
							{ 
								//
							}
						}
					}
                }
                else if (obj.currentFlow == DropFourConstant.MSISM_FLOW)
                {
                    obj.msimFlow[obj.msimIndex - 1].vas = vas;
                    obj.msimFlow[obj.msimIndex - 1].VasNames = RegMobileReg_VasNames;
                    obj.msimFlow[obj.msimIndex - 1].orderSummary = orderSummary;
					obj.msimFlow[obj.msimIndex - 1].UOMCode = vas != null && !string.IsNullOrEmpty(vas.uomCode) ? vas.uomCode : UOMCodeFromSession;
                    obj.msimFlow[obj.msimIndex - 1].SelectedVASID = (string)HttpContext.Current.Session["RegMobileReg_VasIDs_Seco"];
                    obj.msimFlow[obj.msimIndex - 1].SelectedContractID = (string)HttpContext.Current.Session["RegMobileReg_ContractID_Seco"];
                    obj.msimFlow[obj.msimIndex - 1].SelectedDataPlanID = (int[])HttpContext.Current.Session["RegMobileReg_DataplanID_Seco"];
                }
            }
        }
        #endregion

        #region mobile
        public static void UpdateMobile(DropFourObj obj, MobileNoVM mobile)
        {
            OrderSummaryVM orderSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_OrderSummary"];
            if (obj.currentFlow == DropFourConstant.MAIN_FLOW)
            {
                obj.mainFlow.mobile = mobile;
                obj.mainFlow.orderSummary = orderSummary;
            }
            else if (obj.currentFlow == DropFourConstant.SUPP_FLOW)
            {
                var sublineSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_SublineSummary"];
                obj.suppFlow[obj.suppIndex - 1].mobile = mobile;
                obj.suppFlow[obj.suppIndex - 1].orderSummary = sublineSummary != null ? sublineSummary : orderSummary;
            }
            else if (obj.currentFlow == DropFourConstant.MSISM_FLOW)
            {
                obj.msimFlow[obj.msimIndex - 1].mobile = mobile;
                obj.msimFlow[obj.msimIndex - 1].orderSummary = orderSummary;
            }
        }
        #endregion


        #region SimSize Normal, Nano, Micro
        public static void SimSize(DropFourObj obj, String simSize)
        {
            OrderSummaryVM orderSummary = (OrderSummaryVM)HttpContext.Current.Session["RegMobileReg_OrderSummary"];
            if (obj.currentFlow == DropFourConstant.MAIN_FLOW)
            {
                obj.mainFlow.simSize = simSize;
            }
            else if (obj.currentFlow == DropFourConstant.SUPP_FLOW)
            {
                obj.suppFlow[obj.suppIndex - 1].simSize = simSize;
            }
            else if (obj.currentFlow == DropFourConstant.MSISM_FLOW)
            {
                obj.msimFlow[obj.msimIndex - 1].simSize = simSize;
            }
        }
        #endregion

        public static void UpdateSelectedAccessory(DropFourObj obj, List<AccessoryVM> selectedAccessory)
        {
            if (obj.currentFlow == DropFourConstant.MAIN_FLOW)
            {
                if (obj.mainFlow != null)
                {
                    obj.mainFlow.SelectedAccessory = selectedAccessory;
					obj.mainFlow.isAccFinancingOrder = selectedAccessory.Where(x => x.offerDetail != null && x.offerDetail.Where(y => y.isAccFinancing == true).Any()).Any();
					if (obj.mainFlow.isAccFinancingOrder)
					{
						//obj.mainFlow.monthlyAccPrice = selectedAccessory.SelectMany(x => x.offerDetail).Where(x => x.isSelected == true).Select(y => y.monthlyPrice).Sum();
                        obj.mainFlow.monthlyAccPrice = 0M;

                        foreach (var selectedAcc in selectedAccessory)
                        {
                            var monthlyPrice = selectedAcc.offerDetail.Where(x => x.isSelected == true).Select(y => y.monthlyPrice).Sum();
                            obj.mainFlow.monthlyAccPrice += (selectedAcc.quantity * monthlyPrice);
                        }

						obj.mainFlow.isAccFinancingExceptionalOrder = obj.AFcustomerEligibility == false;
					}		
                }
            }
            else
            {
                if (obj.suppFlow[obj.suppIndex - 1] != null)
                {
                    obj.suppFlow[obj.suppIndex - 1].SelectedAccessory = selectedAccessory;
					obj.suppFlow[obj.suppIndex - 1].isAccFinancingOrder = selectedAccessory.Where(x => x.offerDetail != null && x.offerDetail.Where(y => y.isAccFinancing == true).Any()).Any();
					if (obj.suppFlow[obj.suppIndex - 1].isAccFinancingOrder)
					{
						//obj.suppFlow[obj.suppIndex - 1].monthlyAccPrice = selectedAccessory.SelectMany(x => x.offerDetail).Where(x => x.isSelected == true).Select(y => y.monthlyPrice).Sum();
                        obj.suppFlow[obj.suppIndex - 1].monthlyAccPrice = 0M;

                        foreach (var selectedAcc in selectedAccessory)
                        {
                            var monthlyPrice = selectedAcc.offerDetail.Where(x => x.isSelected == true).Select(y => y.monthlyPrice).Sum();
                            obj.suppFlow[obj.suppIndex - 1].monthlyAccPrice += (selectedAcc.quantity * monthlyPrice);
                        }

						obj.suppFlow[obj.suppIndex - 1].isAccFinancingExceptionalOrder = obj.AFcustomerEligibility == false;
					}
						
				}
            }
        }
    }
        
    [Serializable]
    public class Flow
    {
		public Flow(){
			isDeviceFinancingOrder = false;
			isDeviceFinancingExceptionalOrder = false;
            SelectedAccessory = new List<AccessoryVM>();
			isAccFinancingOrder = false;
			isAccFinancingExceptionalOrder = false;
		}

        public int RegType { get; set; }
        public DeviceOptionVM device { get; set; }
        public PhoneVM phone { get; set; }
        public double RRPDevicePrice { get; set; }
        public double OfferDevicePrice { get; set; }
		public decimal monthlyDevicePrice { get; set; }
		public decimal monthlyAccPrice { get; set; }
		public DeviceVM deviceVM { get; set; }
		public string subscribedCompKenanCodes { get; set; }

        public PackageVM package { get; set; }
        public ValueAddedServicesVM vas { get; set; }
        public int PkgPgmBdlPkgCompID { get; set; }
        public string UOMCode { get; set; }
        public string OfferName { get; set; }

        public string MandatoryVAS { get; set; }
        public string MandatoryPackage { get; set; }
        public string DataPackage { get; set; }
        public string ExtraPackage { get; set; }
        public string PromotionPackage { get; set; }
        public string InsurancePackage { get; set; }
        public string DiscountPackage { get; set; }
		public string DeviceFinancingPackage { get; set; }
        public bool? isTradeUp { get; set; }//14012015 - Anthony - GST Trade Up
        public bool isDeviceFinancingOrder { get; set; }
		public bool isDeviceFinancingExceptionalOrder { get; set; }
        public decimal DeviceFinancingOrderDiscount { get; set; }
		public bool isAccFinancingOrder { get; set; }
		public bool isAccFinancingExceptionalOrder { get; set; }

        public string SelectedVASID { get; set; }
        public string SelectedContractID { get; set; }
        public int[] SelectedDataPlanID { get; set; }  

        public List<string> VasNames { get; set; }
        public MobileNoVM mobile { get; set; }
        public OrderSummaryVM orderSummary { get; set; }
        public List<AccessoryVM> SelectedAccessory { get; set; }

		public bool isSimRequired { get; set; }
        public string simSize { get; set; }
        public string imeiNumber { get; set; }
        public string simSerial { get; set; }

		public bool _flowCompleted { get; set; }

        public string mismType { get; set; } // to map waiver components to supp lines in LNKREGWAIVERDETAILS and LNKREGSUPPLINE
    }
}
