﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Online.Registration.Web.Models
{
    [Serializable]
    public class ComponentWithPkg
    {               
        public int ComponentId { get; set; }
        public string LinkType { get; set; }

        public ComponentWithPkg()
        {
        }
        public ComponentWithPkg(int cid, string linktype)
        {
            ComponentId = cid;
            LinkType = linktype;
        }

    }

    [Serializable]
    public class VasesWithPkg
    {
        public string VASName { get; set; }
        public string LinkType { get; set; }

        public VasesWithPkg(string vasName, string linktype)
        {
            VASName = vasName;
            LinkType = linktype;
        }
    }
}