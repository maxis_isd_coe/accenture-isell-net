﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Online.Registration.Web.Models
{
    #region New

    [Serializable]
    public class SalesNewModel
    {
        public RegistrationSearchModel SearchCriteria { get; set; }
        public List<SalesNewRegSearchResult> SearchResults { get; set; }

        public SalesNewModel()
        {
            SearchCriteria = new RegistrationSearchModel();
        }
    }

    [Serializable]
    public class RegistrationSearchModel
    {
        [Display(Name = "Registration ID")]
        public int? RegID { get; set; }

        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "IC Number")]
        public string ICNumber { get; set; }

        [Display(Name = "MSISDN")]
        public string MSISDN { get; set; }

        [Display(Name = "Registration Created Date From")]
        public DateTime? RegCreateDateFrom { get; set; }

        [Display(Name = "Registration Created Date To")]
        public DateTime? RegCreateDateTo { get; set; }
    }

    [Serializable]
    public class SalesNewRegSearchResult
    {
        [Display(Name = "Registration ID")]
        public int RegID { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Customer ID")]
        public int CustomerID { get; set; }

        public string MSISDN { get; set; }

        [Display(Name = "Created Date")]
        public string CreatedDate { get; set; }

        [Display(Name = "IC Number")]
        public string ICNumber { get; set; }
    }

    #endregion

    #region Create
    [Serializable]
    public class SalesCreateModel
    {
        [Display(Name = "Registration ID")]
        public int RegID { get; set; }

        public string Name { get; set; }

        public string MSISDN { get; set; }

        [Display(Name = "ID Number")]
        public string ICNumber { get; set; }

        public string Address { get; set; }

        public string Remark { get; set; }
    }

    #endregion
}