﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Online.Registration.Web.Models
{
    [Serializable]
    public class ComplaintCreateModel
    {
        public string MSISDN { get; set; }

        public string Name { get; set; }

        public string AccountNo { get; set; }

        public string IDNumber { get; set; }

        public string IDType { get; set; }

        public string Address { get; set; }

        public int ComplaintIssuesID { get; set; }
        public int Reason1ID { get; set; }
        public int Reason2ID { get; set; }
        public int Reason3ID { get; set; }
        public int DispatchQID { get; set; }
        public int ComplaintIssueGrpID { get; set; }
        public string Comments { get; set; }
        public List<int> CaseDispatchQIDs = new List<int>();
        public string Product { get; set; }
        public string Reason1 { get; set; }
        public string Reason2 { get; set; }
        public string Reason3 { get; set; }

        public string DispatchQ { get; set; }

        public string DynamicFormText { get; set; }

    }

    #region CMSS

    //[Serializable]
    //public class CMSSComplainGet
    //{
    //    public string MSISDN { get; set; }

    //    public string CMSSCaseID { get; set; }

    //    public DateTime CreateDt { get; set; }

    //    public string IDNumber { get; set; }

    //    public string CustomerName { get; set; }

    //    public string Address { get; set; }

    //    public string LastAccessID { get; set; }


    //}

    #endregion

}