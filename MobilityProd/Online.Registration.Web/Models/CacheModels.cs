﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Models
{
    public class CacheModels
    {

        #region "RegistrationServiceProxy Section"

        public bool Address
        {
            get;
            set;

        }

        public bool Article
        {
            get;
            set;

        }

        public bool CardType
        {
            get;
            set;
        }

        public bool Country
        {
            get;
            set;
        }

        public bool CustomerTitle
        {
            get;
            set;
        }

        public bool IDCardType
        {
            get;
            set;
        }

        public bool Language
        {
            get;
            set;
        }

        public bool Nationality
        {
            get;
            set;
        }

        public bool PaymentMode
        {
            get;
            set;
        }

        public bool Race
        {
            get;
            set;
        }

        public bool State
        {
            get;
            set;
        }

        public bool RegType
        {
            get;
            set;
        }

        public bool CustomerInfoCardType
        {
            get;
            set;
        }

        public bool ExternalIDType
        {
            get;
            set;
        }

        public bool VIPCode
        {
            get;
            set;
        }
        #endregion

        #region "ConfigServiceProxy Section"
        public bool AccountCategory
        {
            get;
            set;

        }
        public bool Market
        {
            get;
            set;
        }
        public bool Status
        {
            get;
            set;
        }
        public bool StatusType
        {
            get;
            set;
        }
        public bool StatusReason
        {
            get;
            set;
        }
        #endregion

        #region "CatalogServiceProxy Section"
        public bool Brand
        {
            get;
            set;
        }
        public bool Bundle
        {
            get;
            set;
        }
        public bool Category
        {
            get;
            set;
        }
        public bool Colour
        {
            get;
            set;
        }
        public bool Model
        {
            get;
            set;
        }
        public bool Package
        {
            get;
            set;
        }
        public bool Program
        {
            get;
            set;
        }
        public bool ModelGroup
        {
            get;
            set;
        }
        public bool SimModelType
        {
            get;
            set;
        }
        public bool SimReplacementReason
        {
            get;
            set;
        }
        
        public bool SimReplacementprePaidReason
        {
            get;
            set;
        }
        public bool ComponentType
        {
            get;
            set;
        }

        public bool PackageType
        {
            get;
            set;
        }

        public bool Property
        {
            get;
            set;
        }


        #endregion

        #region "OrganizationServiceProxy Section"

        public bool OrganizationStocks
        {
            get;
            set;
        }

        public bool Organization
        {
            get;
            set;
        }
        public bool OrgType
        {
            get;
            set;
        }

        #endregion

        #region "ComplainServiceProxy Section"
        //ComplaintIssues:
        public bool ComplaintIssues
        {
            get;
            set;
        }

        #endregion

        #region "UserServiceProxy Section"

        public bool UserGroup
        {
            get;
            set;
        }

        public bool UserRole
        {
            get;
            set;
        }

        public bool User
        {
            get;
            set;
        }

        public bool Access
        {
            get;
            set;
        }

        public bool Doners
        {
            get;
            set;
        }

        #endregion

        public bool ThirdPartyAuthType
        {
            get;
            set;
        }
    }
}