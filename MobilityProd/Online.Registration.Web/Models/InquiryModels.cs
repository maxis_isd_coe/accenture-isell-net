﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Online.Registration.DAL.Models;
using Online.Registration.Web.SubscriberICService;

namespace Online.Registration.Web.Models
{
    #region Index

    [Serializable]
    public class InquiryIndexModel
    {
        public string MSISDN { get; set; }
    }

    #endregion

    #region Result

    [Serializable]
    public class InquiryResultModel
    {
        public string AccountNumber { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }
    }

    #endregion

    #region AccountSummary

    [Serializable]
    public class InquiryAccountSummaryModel
    {
        public InquiryAccount AccountInfo { get; set; }
        public InquiryBilling BillingInfo { get; set; }
        public string MSISDN { get; set; }
        public string EBPSLinkUrl { get; set; }

        public InquiryAccountSummaryModel()
        {
            AccountInfo = new InquiryAccount();
            BillingInfo = new InquiryBilling();
            #region"VLT ADDED CODE"
            Customer = new Customer()
            {
                LastAccessID = "SYSTEM"
            };
            Address = new Address()
            {
                LastAccessID = "SYSTEM"
            };
            #endregion"VLT ADDED CODE"
        }
        #region"VLT ADDED CODE"
        public Customer Customer { get; set; }
        public Address Address { get; set; }
        #endregion"VLT ADDED CODE"
    }

    #region SUTAN ADDED CODE 22ndFeb2013
    [Serializable]
    public class SupplementaryListAccounts
    {
        [Display(Name = "MSISDN")]
        public string externalId { get; set; }
        public List<AddSuppInquiryAccount> SuppListAccounts { get; set; }
        public List<string> dataComponentIDMISM { get; set; }
        public Dictionary<string, IList<Online.Registration.Web.SubscriberICService.PackageModel>> packageModelDictionary { get; set; }

        public SupplementaryListAccounts()
        {
            SuppListAccounts = new List<AddSuppInquiryAccount>();
            packageModelDictionary = new Dictionary<string, IList<Online.Registration.Web.SubscriberICService.PackageModel>>();
            dataComponentIDMISM = new List<string>();
        }
    }

    [Serializable]
    public class SupplementaryListAccountsDatails
    {
        [Display(Name = "MSISDN")]
        public string externalId { get; set; }


        public List<AddSuppInquiryAccount> SuppListAccountsDetails { get; set; }
        public SupplementaryListAccountsDatails()
        {

            SuppListAccountsDetails = new List<AddSuppInquiryAccount>();
        }
    }

    [Serializable]
    public class PrincipalAndSupplementaryDetails
    {
        public List<AddSuppInquiryAccount> principalDetails { get; set; }
        public List<AddSuppInquiryAccount> supplementaryDetails { get; set; }

        public PrincipalAndSupplementaryDetails()
        {
            principalDetails = new List<AddSuppInquiryAccount>();
            supplementaryDetails = new List<AddSuppInquiryAccount>();
        }
    }
    [Serializable]
    public class AddSuppInquiryAccount : InquiryAccount
    {
        [Display(Name = "MSISDN")]
        public string externalId { get; set; }

        public int WriteOffAmount { get; set; }

        public string ParentID { get; set; }

        [Display(Name = "Secondary Sim List")]
        public List<MsimDetails> SecondarySimList { get; set; }
    }
    #endregion SUTAN ADDED CODE 22ndFeb2013
    [Serializable]
    public class InquiryAccount
    {
        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Display(Name = "Account Holder")]
        public string Holder { get; set; }

        [Display(Name = "Account Active Date")]
        public string ActiveDate { get; set; }

        [Display(Name = "Account Category")]
        public string Category { get; set; }

        [Display(Name = "Market Code")]
        public string MarketCode { get; set; }

        [Display(Name = "Plan (Package)")]
        public string Plan { get; set; }

        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        public string IDNumber { get; set; }

        public string IDType { get; set; }

        public string SubscribeNo { get; set; }

        public string SubscribeNoResets { get; set; }

        public string accountExtId { get; set; }

        public string accountIntId { get; set; }

        public bool IsMISM { get; set; }

        public string outstandingAmount { get; set; }

        public string AccountName { get; set; }

        public string AccountStatus { get; set; }//Added by sumit for CRP

        public string SimSerial { get; set; }//Added by Himansu for Sim replacement n-m & m-n 

        public string PrinMsisdn { get; set; }//Added by Himansu for Sim replacement n-m & m-n 

        public string AccountType { get; set; }//Added by Himansu for Sim replacement n-m & m-n 

        public bool IsGreater2GBCompExists { get; set; } // Added by Ranjeeth for Sim Replacement n-m & m-n 

        public string AccountKenanCode { get; set; } // Added by Ranjeeth for Sim Replacement n-m & m-n 

        public bool IsPackagesExistsOnAccountLine { get; set; }// Added by Ranjeeth for Sim Replacement n-m & m-n 

        public string AccountPackageName { get; set; }// Added by Ranjeeth for Sim Replacement n-m & m-n 

        public bool HasSupplyAccounts { get; set; } // added by ranjeeth for sim replacement n-m & m-n 

        public int SupplinesCount { get; set; } // added by ranjeeth to get the counter of supplines to the principal

        public bool DataCompsExistOnAccount { get; set; } // added by ranjeeth to check the datacomponents exists prior to proceed to next select vas flow.N-M Primary Vas Case

    }

    [Serializable]
    public class InquiryBilling
    {
        public string TotalOutstanding { get; set; }

        public string Unbilled { get; set; }

        public string CurrentBilled { get; set; }

        public string DueDate { get; set; }

        public string BalanceForward { get; set; }

        public string CreditLimit { get; set; }

        public InquiryPaymentHistory LastPayment { get; set; }

        public InquiryPaymentHistory Last2ndPayment { get; set; }

        public InquiryPaymentHistory Last3rdPayment { get; set; }

        public IEnumerable<InquiryPaymentStatus> PaymentStatuses { get; set; }

        public InquiryBilling()
        {
            LastPayment = new InquiryPaymentHistory();
            Last2ndPayment = new InquiryPaymentHistory();
            Last3rdPayment = new InquiryPaymentHistory();
        }
    }

    [Serializable]
    public class InquiryPaymentStatus
    {
        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Amount")]
        public string Amount { get; set; }

        [Display(Name = "Due Date")]
        public string DueDate { get; set; }

        [Display(Name = "Payment")]
        public string Payment { get; set; }

        [Display(Name = "Balance Due")]
        public string BalanceDue { get; set; }
    }

    [Serializable]
    public class InquiryPaymentHistory
    {
        [Display(Name = "Transaction Date")]
        public string TransactionDate { get; set; }

        [Display(Name = "Amount")]
        public string Amount { get; set; }
    }

    #endregion

    #region Services
    [Serializable]
    public class InquiryServicesModel
    {
        public string MSISDN { get; set; }
        public string SubscribeNo { get; set; }
        public string SubscribeNoResets { get; set; }

        public IEnumerable<InquiryContract> Contracts { get; set; }

        public IEnumerable<InquiryVAS> VAS { get; set; }
    }

    [Serializable]
    public class InquiryContract
    {
        public string ContractID { get; set; }
        public string ContractName { get; set; }
        public string ContractType { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }

    [Serializable]
    public class InquiryVAS
    {
        public string ComponentID { get; set; }
        public string ComponentDescription { get; set; }
        public string ActiveDate { get; set; }
    }
    #endregion

    #region MNPSUPPLIMENTARY LINES
    /// <summary>
    /// CLASS TO STORE THE LIST OF MSISDN'S FOR SUPPLEMENTARYLINES
    /// </summary>
    [Serializable]
    public class MNPSelectPlanForSuppline
    {
        public List<SupplimentaryMSISDN> SupplimentaryMSISDNs { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="MNPSelectPlanForSuppline"/> class.
        /// </summary>
        public MNPSelectPlanForSuppline()
        {
            SupplimentaryMSISDNs = new List<SupplimentaryMSISDN>();
        }
    }

    /// <summary>
    ///  CLASS TO STORE THE MSISDN FOR SUPPLEMENTARYLINE
    /// </summary>
    [Serializable]
    public class SupplimentaryMSISDN
    {
        public string Msisdn { get; set; }
        ///PROPERTY TO DENOTE WHETHER THE INFORMATION IS POPULATED FOR THE SUPPLEMENTARY MSISDN
        [System.ComponentModel.DefaultValue(MNPSupplimentaryDetailsAddState.NOTINITIALIZED)]
        public Int32 Status { get; set; }
    }

    /// <summary>
    /// STATES FOR SupplimentaryMSISDN.Status
    /// </summary>
    public enum MNPSupplimentaryDetailsAddState
    {
        NOTINITIALIZED = 0,
        PROCESSING = 1,
        PROCESSED = 2
    }
    #endregion


    #region PRIMARY SUPPLIMENTARY LINES
    /// <summary>
    /// CLASS TO STORE THE LIST OF MSISDN'S FOR SUPPLEMENTARYLINES
    /// </summary>
    [Serializable]
    public class SelectPlanForSuppline
    {
        public List<PrimarySupplimentaryMSISDN> PrimarySupplimentaryMSISDNs { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="MNPSelectPlanForSuppline"/> class.
        /// </summary>
        public SelectPlanForSuppline()
        {
            PrimarySupplimentaryMSISDNs = new List<PrimarySupplimentaryMSISDN>();
        }
    }

    /// <summary>
    ///  CLASS TO STORE THE MSISDN FOR SUPPLEMENTARYLINE
    /// </summary>
    [Serializable]
    public class PrimarySupplimentaryMSISDN
    {
        public string Msisdn { get; set; }
        public string ProcessingId { get; set; }
        ///PROPERTY TO DENOTE WHETHER THE INFORMATION IS POPULATED FOR THE SUPPLEMENTARY MSISDN
        [System.ComponentModel.DefaultValue(PrimarySupplimentaryDetailsAddState.NOTINITIALIZED)]
        public Int32 Status { get; set; }
    }

    /// <summary>
    /// STATES FOR SupplimentaryMSISDN.Status
    /// </summary>
    public enum PrimarySupplimentaryDetailsAddState
    {
        NOTINITIALIZED = 0,
        PROCESSING = 1,
        PROCESSED = 2
    }
    #endregion
}
