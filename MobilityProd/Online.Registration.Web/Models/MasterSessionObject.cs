﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Online.Registration.Web.Models
{
	[Serializable]
	public class MasterSessionObject
	{
		public MasterSessionObject()
		{
			crpFlow = new CRPFlow();
		}

		public CRPFlow crpFlow { get; set; }
	}

	[Serializable]
	public class CRPFlow
	{
		public bool principalToSuppLine { get; set; }
		public bool suppLineToPrincipal { get; set; }
		public bool contractExtend { get; set; }
		public bool contractTermination { get; set; }
		public bool contractCRP { get; set; }
	}
}