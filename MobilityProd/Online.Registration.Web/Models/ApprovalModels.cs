﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Online.Registration.DAL.Models;

namespace Online.Registration.Web.Models
{
	[Serializable]
	public class ApprovalModels
	{
		public ApprovalModels()
		{
			approvalObjList = new List<RegJustification>();
		}
		public List<RegJustification> approvalObjList;
	}

}