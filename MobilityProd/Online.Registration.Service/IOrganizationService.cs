﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.Service.Models;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IOrganizationService" in both code and config file together.
    [ServiceContract]
    public interface IOrganizationService
    {
        #region Org Type

        [OperationContract]
        OrgTypeCreateResp OrgTypeCreate(OrgTypeCreateReq oReq);
        [OperationContract]
        OrgTypeUpdateResp OrgTypeUpdate(OrgTypeUpdateReq oReq);
        [OperationContract]
        OrgTypeFindResp OrgTypeFind(OrgTypeFindReq oReq);
        [OperationContract]
        OrgTypeGetResp OrgTypeGet(OrgTypeGetReq oReq);
        [OperationContract]
        OrgTypeGetResp OrgTypesList();
        #endregion

        #region Organization

        [OperationContract]
        OrganizationCreateResp OrganizationCreate(OrganizationCreateReq oReq);
        [OperationContract]
        OrganizationUpdateResp OrganizationUpdate(OrganizationUpdateReq oReq);
        [OperationContract]
        OrganizationGetResp OrganizationGet(OrganizationGetReq oReq);
        [OperationContract]
        OrganizationGetResp OrganizationList();
        [OperationContract]
        OrganizationFindResp OrganizationFind(OrganizationFindReq oReq);

        [OperationContract]
        OrganizationGetResp OrganizationGetAll();

        #endregion
    }
}
