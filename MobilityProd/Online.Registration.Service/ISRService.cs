﻿using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.Service.Models;
using Online.Registration.Service.Models.ServiceRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISRService" in both code and config file together.
    [ServiceKnownType(typeof(SRQueryMaster))]
    [ServiceContract]
    public interface ISRService
    {
        [OperationContract]
        SRServiceResp SaveOrder(SRSaveOrderReq request);

        [OperationContract]
        SRServiceResp FindOrderById(SRFindOrderReq request);

        [OperationContract]
        SRServiceResp SaveDocs(SRSaveDocsReq request);

        [OperationContract]
        SRServiceResp ChangeOrderLock(SRChangeLockReq request);

        [OperationContract]
        SRServiceResp UpdateOrderStatus(SRUpdateStatusReq request);

        [OperationContract]
        SRServiceResp GetDistinctCreateByList(SRGetCreateByReq request);

        [OperationContract]
        SRQueryOrderResp QueryOrder(SRQueryOrderReq request);
    }
}
