﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.Service.Models;
using Online.Registration.DAL.Models;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRegistrationService" in both code and config file together.
    [ServiceContract]
    public interface IRegistrationService
    {
        
        #region Admin

        #region Address Type

        [OperationContract]
        AddressTypeCreateResp AddressTypeCreate(AddressTypeCreateReq oReq);
        [OperationContract]
        AddressTypeUpdateResp AddressTypeUpdate(AddressTypeUpdateReq oReq);
        [OperationContract]
        AddressTypeFindResp AddressTypeFind(AddressTypeFindReq oReq);
        [OperationContract]
        AddressTypeGetResp AddressTypeGet(AddressTypeGetReq oReq);
        [OperationContract]
        AddressTypeGetResp AddressTypeList();

        #endregion

        #region Bank

        [OperationContract]
        BankCreateResp BankCreate(BankCreateReq oReq);
        [OperationContract]
        BankUpdateResp BankUpdate(BankUpdateReq oReq);
        [OperationContract]
        BankFindResp BankFind(BankFindReq oReq);
        [OperationContract]
        BankGetResp BankGet(BankGetReq oReq);

        #endregion

        #region Card Type

        [OperationContract]
        CardTypeCreateResp CardTypeCreate(CardTypeCreateReq oReq);
        [OperationContract]
        CardTypeUpdateResp CardTypeUpdate(CardTypeUpdateReq oReq);
        [OperationContract]
        CardTypeFindResp CardTypeFind(CardTypeFindReq oReq);
        [OperationContract]
        CardTypeGetResp CardTypeGet(CardTypeGetReq oReq);
        [OperationContract]
        CardTypeGetResp CardTypeList();

        #endregion

        #region Country

        [OperationContract]
        CountryCreateResp CountryCreate(CountryCreateReq oReq);
        [OperationContract]
        CountryUpdateResp CountryUpdate(CountryUpdateReq oReq);
        [OperationContract]
        CountryFindResp CountryFind(CountryFindReq oReq);
        [OperationContract]
        CountryGetResp CountryGet(CountryGetReq oReq);
        [OperationContract]
        CountryGetResp CountryList();

        #endregion

        #region State

        [OperationContract]
        StateCreateResp StateCreate(StateCreateReq oReq);
        [OperationContract]
        StateUpdateResp StateUpdate(StateUpdateReq oReq);
        [OperationContract]
        StateFindResp StateFind(StateFindReq oReq);
        [OperationContract]
        StateGetResp StateGet(StateGetReq oReq);
        [OperationContract]
        StateGetResp StateList();

        #endregion

        #region ID Card Type

        [OperationContract]
        IDCardTypeCreateResp IDCardTypeCreate(IDCardTypeCreateReq oReq);
        [OperationContract]
        IDCardTypeUpdateResp IDCardTypeUpdate(IDCardTypeUpdateReq oReq);
        [OperationContract]
        IDCardTypeFindResp IDCardTypeFind(IDCardTypeFindReq oReq);
        [OperationContract]
        IDCardTypeGetResp IDCardTypeGet(IDCardTypeGetReq oReq);
        [OperationContract]
        IDCardTypeGetResp IDCardTypeList();

        #endregion

        #region Payment Mode

        [OperationContract]
        PaymentModeCreateResp PaymentModeCreate(PaymentModeCreateReq oReq);
        [OperationContract]
        PaymentModeUpdateResp PaymentModeUpdate(PaymentModeUpdateReq oReq);
        [OperationContract]
        PaymentModeFindResp PaymentModeFind(PaymentModeFindReq oReq);
        [OperationContract]
        PaymentModeGetResp PaymentModeGet(PaymentModeGetReq oReq);
        [OperationContract]
        PaymentModeGetResp PaymentModeList();

        #endregion

        #region Reg Type

        [OperationContract]
        RegTypeCreateResp RegTypeCreate(RegTypeCreateReq oReq);
        [OperationContract]
        RegTypeUpdateResp RegTypeUpdate(RegTypeUpdateReq oReq);
        [OperationContract]
        RegTypeFindResp RegTypeFind(RegTypeFindReq oReq);
        [OperationContract]
        RegTypeGetResp RegTypeGet(RegTypeGetReq oReq);
        [OperationContract]
        RegTypeGetResp RegTypeList();

        #endregion

        #region Race

        [OperationContract]
        RaceCreateResp RaceCreate(RaceCreateReq oReq);
        [OperationContract]
        RaceUpdateResp RaceUpdate(RaceUpdateReq oReq);
        [OperationContract]
        RaceGetResp RaceGet(RaceGetReq oReq);
        [OperationContract]
        RaceGetResp RaceList();
        [OperationContract]
        RaceFindResp RaceFind(RaceFindReq oReq);

        #endregion

        #region Nationality

        [OperationContract]
        NationalityCreateResp NationalityCreate(NationalityCreateReq oReq);
        [OperationContract]
        NationalityUpdateResp NationalityUpdate(NationalityUpdateReq oReq);
        [OperationContract]
        NationalityGetResp NationalityGet(NationalityGetReq oReq);
        [OperationContract]
        NationalityGetResp NationalityList();
        [OperationContract]
        NationalityFindResp NationalityFind(NationalityFindReq oReq);

        #endregion

        #region Language

        [OperationContract]
        LanguageCreateResp LanguageCreate(LanguageCreateReq oReq);
        [OperationContract]
        LanguageUpdateResp LanguageUpdate(LanguageUpdateReq oReq);
        [OperationContract]
        LanguageGetResp LanguageGet(LanguageGetReq oReq);
        [OperationContract]
        LanguageGetResp LanguageGetList();
        [OperationContract]
        LanguageFindResp LanguageFind(LanguageFindReq oReq);

        #endregion

        #region CustomerTitle

        [OperationContract]
        CustomerTitleCreateResp CustomerTitleCreate(CustomerTitleCreateReq oReq);
        [OperationContract]
        CustomerTitleUpdateResp CustomerTitleUpdate(CustomerTitleUpdateReq oReq);
        [OperationContract]
        CustomerTitleGetResp CustomerTitleGet(CustomerTitleGetReq oReq);
        [OperationContract]
        CustomerTitleGetResp CustomerTitleList();
        [OperationContract]
        CustomerTitleFindResp CustomerTitleFind(CustomerTitleFindReq oReq);

        #endregion

        #region ExtIDType

        [OperationContract]
        ExtIDTypeCreateResp ExtIDTypeCreate(ExtIDTypeCreateReq oReq);
        [OperationContract]
        ExtIDTypeUpdateResp ExtIDTypeUpdate(ExtIDTypeUpdateReq oReq);
        [OperationContract]
        ExtIDTypeFindResp ExtIDTypeFind(ExtIDTypeFindReq oReq);
        [OperationContract]
        ExtIDTypeGetResp ExtIDTypeGet(ExtIDTypeGetReq oReq);
        [OperationContract]
        ExtIDTypeGetResp ExtIDTypeList();

        #endregion

        #region VIPCode

        [OperationContract]
        VIPCodeCreateResp VIPCodeCreate(VIPCodeCreateReq oReq);
        [OperationContract]
        VIPCodeUpdateResp VIPCodeUpdate(VIPCodeUpdateReq oReq);
        [OperationContract]
        VIPCodeFindResp VIPCodeFind(VIPCodeFindReq oReq);
        [OperationContract]
        VIPCodeGetResp VIPCodeGet(VIPCodeGetReq oReq);
        [OperationContract]
        VIPCodeGetResp VIPCodeList();
        #endregion

        #endregion

        #region Operation

        #region Registration

        [OperationContract]
        RegistrationCreateResp RegistrationCreate(RegistrationCreateReq oReq);
        [OperationContract]
        HomeRegistrationCreateResp HomeRegistrationCreate(RegistrationCreateReq oReq);
        [OperationContract]
        RegistrationUpdateResp RegistrationUpdate(RegistrationUpdateReq oReq);
        [OperationContract]
        RegistrationUpdateResp RegistrationUpdateMISM(RegistrationUpdateMISMReq oReq);
        [OperationContract]
        RegistrationFindResp RegistrationFind(RegistrationFindReq oReq);
        [OperationContract]
        RegistrationGetResp RegistrationGet(RegistrationGetReq oReq);
        [OperationContract]
        RegistrationSecGetResp RegistrationSecGet(RegistrationSecGetReq oReq);
        [OperationContract]
        RegistrationSearchResp RegistrationSearch(RegistrationSearchReq oReq);
        [OperationContract]
        RegistrationCloseResp RegistrationClose(RegistrationCloseReq oReq);
        [OperationContract]
        RegistrationCancelResp RegistrationCancel(RegistrationCancelReq oReq);
        [OperationContract]
        RegistrationPaymentFailedResp RegistrationPaymentFailed(RegistrationPaymentFailedReq oReq);
        [OperationContract]
        RegistrationPaymentSuccessResp RegistrationPaymentSuccess(RegistrationPaymentSuccessReq oReq);
        #region Added by VLT on 30-04-2013 for saving Kenam customer Info
        [OperationContract]
        KenanCustomerInfoCreateResp KenanCustomerInfoCreate(KenanCustomerInfoCreateReq oReq);
        #endregion
        #region Added by Patanjali on 30-03-2013 for Store Keeper and Cashier
        [OperationContract]
        RegistrationSearchResp StoreKeeperSearch(RegistrationSearchReq oReq);
        [OperationContract]
        AgentCodeByUserNameResp GetAgentCodeByUserName(string userName);
        [OperationContract]
        RegistrationSearchResp CashierSearch(RegistrationSearchReq oReq);
        #endregion
        [OperationContract]
        RegistrationSearchResp OrderStatusReportSearch(RegistrationSearchReq oReq);
        [OperationContract]
        int PaymentStatusGet(int RegID);

        #endregion

        #region Address

        //[OperationContract]
        //AddressCreateResp AddressCreate(AddressCreateReq oReq);
        //[OperationContract]
        //AddressUpdateResp AddressUpdate(AddressUpdateReq oReq);
        [OperationContract]
        AddressFindResp AddressFind(AddressFindReq oReq);
        [OperationContract]
        AddressGetResp AddressGet(AddressGetReq oReq);
        [OperationContract]
        AddressGetResp AddressGet1(AddressFindReq oReq);

        #endregion

        #region RegMdlGrpModel

        //[OperationContract]
        //RegMdlGrpModelCreateResp RegMdlGrpModelCreate(RegMdlGrpModelCreateReq oReq);
        //[OperationContract]
        //RegMdlGrpModelUpdateResp RegMdlGrpModelUpdate(RegMdlGrpModelUpdateReq oReq);
        [OperationContract]
        RegMdlGrpModelFindResp RegMdlGrpModelFind(RegMdlGrpModelFindReq oReq);
        [OperationContract]
        RegMdlGrpModelSecFindResp RegMdlGrpModelSecFind(RegMdlGrpModelSecFindReq oReq);
        [OperationContract]
        RegMdlGrpModelGetResp RegMdlGrpModelGet(RegMdlGrpModelGetReq oReq);
        [OperationContract]
        RegMdlGrpModelSecGetResp RegMdlGrpModelSecGet(RegMdlGrpModelSecGetReq oReq);
        [OperationContract]
        RegMdlGrpModelGetResp RegMdlGrpModelGet1(RegMdlGrpModelFindReq oReq);
        #endregion

        #region RegPgmBdlPkgComponent

        //[OperationContract]
        //RegPgmBdlPkgCompCreateResp RegPgmBdlPkgCompCreate(RegPgmBdlPkgCompCreateReq oReq);
        //[OperationContract]
        //RegPgmBdlPkgCompUpdateResp RegPgmBdlPkgCompUpdate(RegPgmBdlPkgCompUpdateReq oReq);
        [OperationContract]
        RegPgmBdlPkgCompFindResp RegPgmBdlPkgCompFind(RegPgmBdlPkgCompFindReq oReq);
        [OperationContract]
        RegPgmBdlPkgCompSecFindResp RegPgmBdlPkgCompSecFind(RegPgmBdlPkgCompSecFindReq oReq);
        //Added by VLT on 11 Apr 2013
        [OperationContract]
        RegPgmBdlPkgCompFindResp RegPgmBdlPkgComponentFind(RegPgmBdlPkgCompFindReq oReq);
        [OperationContract]
        RegPgmBdlPkgCompGetResp RegPgmBdlPkgCompGet(RegPgmBdlPkgCompGetReq oReq);
        [OperationContract]
        RegPgmBdlPkgCompGetResp RegPgmBdlPkgCompGet1(RegPgmBdlPkgCompFindReq oReq);
        [OperationContract]
        RegPgmBdlPkgCompSecGetResp RegPgmBdlPkgCompSecGet(RegPgmBdlPkgCompSecGetReq oReq);
        //Added by VLT on 11 Apr 2013
        [OperationContract]
        RegPgmBdlPkgComponentGetResp RegPgmBdlPkgComponentGet(RegPgmBdlPkgCompGetReq oReq);

        #endregion

        #region
        [OperationContract]
        Registration.DAL.RegistrationDetails GetRegistrationFullDetails(int regId);
        #endregion


        #region RegSuppLine

        //[OperationContract]
        //RegSuppLineCreateResp RegSuppLineCreate(RegSuppLineCreateReq oReq);
        //[OperationContract]
        //RegSuppLineUpdateResp RegSuppLineUpdate(RegSuppLineUpdateReq oReq);
        [OperationContract]
        RegSuppLineFindResp RegSuppLineFind(RegSuppLineFindReq oReq);
        [OperationContract]
        RegSuppLineGetResp RegSuppLineGet(RegSuppLineGetReq oReq);
        [OperationContract]
        RegSuppLineGetResp RegSuppLineGet1(RegSuppLineFindReq oReq);

        #endregion

        #region RegStatus

        [OperationContract]
        int RegStatusUpdate(int Regid, RegStatus regStatus);
        [OperationContract]
        RegStatusCreateResp RegStatusCreate(RegStatusCreateReq oReq);
        [OperationContract]
        RegStatusFindResp RegStatusFind(RegStatusFindReq oReq);
        [OperationContract]
        RegStatusGetResp RegStatusGet(RegStatusGetReq oReq);
        [OperationContract]
        RegStatusGetResp RegStatusGet1(RegStatusFindReq oReq);
        [OperationContract]
        RegStatusLogGetResp RegStatusLogGet(RegStatusLogGetReq oReq);

        #endregion

        #region Customer

        //[OperationContract]
        //CustomerCreateResp CustomerCreate(CustomerCreateReq oReq);
        //[OperationContract]
        //CustomerUpdateResp CustomerUpdate(CustomerUpdateReq oReq);
        [OperationContract]
        CustomerFindResp CustomerFind(CustomerFindReq oReq);
        [OperationContract]
        CustomerGetResp CustomerGet(CustomerGetReq oReq);
        [OperationContract]
        CustomerGetResp CustomerGet1(CustomerFindReq oReq);

        #endregion

        #region Biometric

        [OperationContract]
        BiometricCreateResp BiometricCreate(BiometricCreateReq oReq);
        [OperationContract]
        BiometricUpdateResp BiometricUpdate(BiometricUpdateReq oReq);
        [OperationContract]
        BiometricFindResp BiometricFind(BiometricFindReq oReq);
        [OperationContract]
        BiometricGetResp BiometricGet(BiometricGetReq oReq);

        #endregion

        #region RegAccount

        [OperationContract]
        RegAccountGetResp RegAccountGet(RegAccountGetReq oReq);

        #endregion

        #region Customerinfo

        [OperationContract]
        CustomerInfoCreateResp CustomerInfoCreate(CustomerInfoCreateReq oReq);
        [OperationContract]
        CustomerInfoUpdateResp CustomerInfoUpdate(CustomerInfoUpdateReq oReq);

        #endregion

        #region Payment Mode Payment Details

        [OperationContract]
        PaymentModePaymentDetailsCreateResp PaymentModePaymentDetailsCreate(PaymentModePaymentDetailsCreateReq oReq);
        [OperationContract]
        PaymentModePaymentDetailsUpdateResp PaymentModePaymentDetailsUpdate(PaymentModePaymentDetailsUpdateReq oReq);
        [OperationContract]
        PaymentModePaymentDetailsFindResp PaymentModePaymentDetailsFind(PaymentModePaymentDetailsFindReq oReq);
        [OperationContract]
        PaymentModePaymentDetailsGetResp PaymentModePaymentDetailsGet(PaymentModePaymentDetailsGetReq oReq);

        #endregion

        #region Card Type Payment Details

        [OperationContract]
        CardTypePaymentDetailsCreateResp CardTypePaymentDetailsCreate(CardTypePaymentDetailsCreateReq oReq);
        [OperationContract]
        CardTypePaymentDetailsUpdateResp CardTypePaymentDetailsUpdate(CardTypePaymentDetailsUpdateReq oReq);
        [OperationContract]
        CardTypePaymentDetailsFindResp CardTypePaymentDetailsFind(CardTypePaymentDetailsFindReq oReq);
        [OperationContract]
        CardTypePaymentDetailsGetResp CardTypePaymentDetailsGet(CardTypePaymentDetailsGetReq oReq);
        [OperationContract]
        CardTypePaymentDetailsGetResp CardTypePaymentList();

        #endregion

        /*Chetan added for displaying the reson for failed transcation*/
        [OperationContract]
        KenanLogDetailsGetResp KenanLogDetailsGet(KenanLogDetailsGetReq oReq);

        [OperationContract]
        KenanLogDetailsGetResp KenanLogHistoryDetailsGet(KenanLogHistoryDetailsGetReq oReq);

        #endregion

        //Spend Limit commented by Ravi as per new flow on june 15 2013
        #region "Spend Limit"
        [OperationContract]
        int SaveSpendLimit(LnkRegSpendLimitReq oReq);

        [OperationContract]
        LnkRegSpendLimitReq SpendLimitGet(int regId);
        #endregion

        #region "Extra Ten"
        [OperationContract]
        int SaveExtraTen(ExtraTenReq oReq);

        [OperationContract]
        int UpdateExtraTen(ExtraTenReq oReq);

        [OperationContract]
        int DeleteExtraTen(int extraTenId);

        [OperationContract]
        int SaveExtraTenLogs(ExtraTenLogReq oReq);

        [OperationContract]
        ExtraTenGetResp GetExtraTenDetails(int regId);

        [OperationContract]
        int DeleteExtraTenDetails(int regId);

        [OperationContract]
        int SaveExtraTenAddEditDetails(ExtraTenAddEditReq oReq);
        #endregion
        [OperationContract]
        RegistrationCancelReasonResp RegistrationCancelCreate(RegistrationCancelReasonReq oReq);
        [OperationContract]
        string CancelReasonGet(int regID);

        [OperationContract]
        int SaveLnkRegistrationDetails(LnkRegDetailsReq objLnkRegDetailsReq);
        [OperationContract]
        SmartGetExistingPackageDetailsResp ExistingPackageDetails(SmartGetExistingPackageDetailsReq oReq);
        [OperationContract]
        Dictionary<int, SmartGetExistingPackageDetailsResp> ExistingPackagesDetails(List<SmartGetExistingPackageDetailsReq> oReq);

        [OperationContract]
        long GetLnkPgmBdlPkgCompIdByKenanCode(string kenanCode, string planType);
        [OperationContract]
        string GetlnkpgmbdlpkgcompidForKenancomponent(string plan_type, int planId, string kenancode);
        [OperationContract]
        bool getSupplineDetails(int regID);
        [OperationContract]
        RegistrationCreateResp RegistrationCreateNew(RegistrationCreateReq oReq);//Added By Nagaraju

        [OperationContract]
        LnkRegDetailsReq GetLnkRegistrationDetails(int regId);

        [OperationContract]
        int Insertusedmobilenos(int regid, long mobileno, int active);
        [OperationContract]
        List<long> selectusedmobilenos();

        [OperationContract]
        int UpdateLnkRegistrationDetails(LnkRegDetailsReq objLnkRegDetailsReq);

        [OperationContract]
        int SaveRegSuppLines(RegSupplinesMapReq objRegSupplinesMapReq);

        [OperationContract]
        DAL.Models.LnkRegDetails LnkRegistrationGetByRegId(int regId);

        [OperationContract]
        RegistrationCreateResp RegistrationCreateWithSec(string oReq);

        [OperationContract]
        RegistrationCreateResp SecondaryLineRegistrationCreate(RegistrationCreateWithSecReq oReq);

        [OperationContract]

        GetPackageDetailsResp SelectedPackageDetail(GetPackageDetailsReq oReq);

        [OperationContract]
        GetActiveContractsListResp GetActiveContractsList();

        #region CRP



        [OperationContract]
        RegistrationCreateResp RegistrationCreate_CRP(RegistrationCreateReq_CRP oReq);

        [OperationContract]
        bool isCRPregistration(int regid);

        #endregion

        #region MNP WITH SUPPLEMENTARY LINE
        /// <summary>
        /// Registrations the get MNP supple.
        /// </summary>
        /// <param name="RegID">The reg ID.</param>
        /// <returns></returns>
        [OperationContract]
        RegistrationMNPSupplementaryGetResp RegistrationGetMnpSupple(Int32 RegID);
        /// <summary>
        /// MNP supplementary lines update.
        /// </summary>
        /// <param name="oReqs">The o reqs.</param>
        /// <returns>TypeOf(int)</returns>
        [OperationContract]
        RegSuppLineUpdateResp RegSuppLineUpdate(RegSuppLineMultiUpdateReq oReqs);
        #endregion MNP WITH SUPPLEMENTARY LINE
        [OperationContract]
        List<Online.Registration.DAL.Models.FxDependChkComps> GetAllComponentsbyPlanId(int PlanID, string mandaotryComps,string linkType);

        [OperationContract]
        List<Online.Registration.DAL.Models.FxDependChkComps> GetAllComponentsbySecondaryPlanId(int PlanID, string mandaotryComps);

        [OperationContract]
        int BlackBerryDiscount(int deviceID, int planID);


        [OperationContract]
        bool IsIMPOSFileExists(int regId, int regType, out string status);

        [OperationContract]
        int GetBBAdvancePrice(string uomCode, int planID, int modelId);

        [OperationContract]
        List<string> GetCompDescByRegid(int regID);
        
        [OperationContract]
        List<string> GetDataPlanComponentsForMISM();

        //Method added by ravi to check whether the customer is whitelist or not
        [OperationContract]
        int CheckWhiteListCustomer(string icNo);

        [OperationContract]
        List<Online.Registration.DAL.Models.lnkofferuomarticleprice> GetPromotionalOffers(int whiteListId);
        [OperationContract]
        Online.Registration.DAL.Models.PlanDetailsandContract GetPlanDetailsbyOfferId(int offerId);
        //end of new method

        #region WhitleList

        [OperationContract]
        List<Online.Registration.DAL.Models.SMECIWhiteList> WhiteListSearchByIC(Online.Registration.DAL.Models.SMECIWhiteList whiteList);

        [OperationContract]
        bool DeleteWhiteListCustomer(Online.Registration.DAL.Models.SMECIWhiteList whiteList);

        [OperationContract]
        bool AddWhiteListCustomer(List<Online.Registration.DAL.Models.SMECIWhiteList> whiteList);

        [OperationContract]
        List<int> GetDependentComp(string planId);

        #endregion

        [OperationContract]
        int GetMarketCodeIdByRegid(int regid);


        [OperationContract]
        List<DAL.Models.WaiverComponents> GetWaiverComponents(int PackageID);
        [OperationContract]
        List<DAL.Models.WaiverComponents> GetWaiverComponentsbyRegID(int RegID);

        #region supervisor waive off

        [OperationContract]
        List<string> GetCreatedUsersForWaiveOff(int OrgId);

        [OperationContract]
        List<string> GetAcknowledgedUsersForWaiveOff(int OrgId, bool isCDPU);

        [OperationContract]
        SupervisorWaiveOffSearchResp SupervisorWaiveOffSearch(SupervisorWaiveOffSearchReq oReq);

        [OperationContract]
        int AcknowledgedUsersForWaiveOffUpdate(AcknowledgedUsersReq details);

        #endregion


        [OperationContract]
        List<DAL.Models.UserSpecificPrintersInfo> GetUserSpecificPrintersInfo(int UserID, int orgID);

        [OperationContract]
        string GetPrintVersionNoByRegid(int regId);

        [OperationContract]
        Online.Registration.DAL.Models.PrintVersion GetPrintTsCsInfo(string versionNo, int regType, string brand, string PlanKenanCode, string Trn_Type = null);

        [OperationContract]
        string GetPrintVersionNum(int regType, string brand, string GetPrintVersionNum, string Trn_Type=null);

        [OperationContract]
        RegistrationSearchResp TLProfileSearch(RegistrationSearchReq oReq);

        [OperationContract]
        int SaveUserPrintLog(UserPrintLog objUserPrintLog);
        [OperationContract]
        List<DAL.Models.UserPrintLog> GetPrintersLog(string File);

        [OperationContract]
        Dictionary<string, string> GetlnkpgmbdlpkgcompidForKenancomponentAll(int planId, List<string> kenancodes);
        
        [OperationContract(Name="PgmBdlCompIdForKenanComponentsAll")]
        Dictionary<string, string> GetlnkpgmbdlpkgcompidForKenancomponentAll(string planType, int planId, List<string> kenancodes);


        #region smart related methods

        
        [OperationContract]
        RegistrationCreateResp RegistrationCreate_Smart(RegistrationCreateReq_smart oReq);

        [OperationContract]
        RegCancellationRes RegCancellationGet();
                
        #endregion

        [OperationContract]
        WaiverRulesResp GetWaiverRules();

        [OperationContract]
        string GetBrandModelIDbyArticleID(string ArticleID);

        [OperationContract]
        List<int> GetSpotifyCopmByPkgKenancode(string KenanCode, int Regid);
        [OperationContract]
        List<int> GetSpotifyCopmIDByCompKenancode(string KenanCode);
        [OperationContract(Name = "GetLnkPgmBdlPkgCompIdByKenanCodeandTypes")]
        long GetLnkPgmBdlPkgCompIdByKenanCode(string kenanCode, string planType, string linkType);

        [OperationContract]
        int GetIdByTitleName(string title);

        [OperationContract]
        int SaveDocumentToDB(string photoData, string custPhoto, string altCustPhoto, int regid);

        /*Changes by chetan related to PDPA implementation*/
        [OperationContract]
        Online.Registration.DAL.Models.PDPAData GetPDPADataInfo(string DocType, string TrnType);

        [OperationContract]
        Online.Registration.DAL.Models.PDPAData GetPDPADataInfoWithVersion(string DocType, string TrnType, string pdpdversion);

        [OperationContract]
        string GetDataKenaCode_Smart(string pkgId);

        [OperationContract]
        RegAccount RegAccountGetByRegID(int regID);

        [OperationContract]
        int RegAccountsUpdate(RegAccount oReq);

        [OperationContract]
        int UpdateSecondaryRequestSent(int regID);


        /*Code for saving CMSS case details by chetan*/
        [OperationContract]
        int SaveCMSSDetails(CMSSComplain objCMSSID);

        [OperationContract]
        CMSSComplain GetCMSSCaseDetails(int regId);

        [OperationContract]
        DateTime GetCMSSCaseDateForAcct(string AccountNumber);

        [OperationContract]
        int GetSIMModelIDByArticleID(string strAticleID);

        [OperationContract]
        int SaveUserDMEPrint(UserDMEPrint objUserDMEPrint);

        [OperationContract]
        List<DAL.Models.UserDMEPrint> GetUserDMEPrinters(int userId);

        [OperationContract]
        string GetCDPUStatus(int regId);

        [OperationContract]
        Online.Registration.DAL.Models.RegBREStatus GetCDPUDetails(Online.Registration.DAL.Models.RegBREStatus req);

        [OperationContract]
        List<DAL.Models.PendingRegBREStatus> PendingOrders();

        // Drop 5
        [OperationContract]
        bool UpsertBRE(RegBREStatus objBre);

        [OperationContract]
        int UpdateBREApproveStatus(int RegId, string Status, string LastAccessID);

        [OperationContract]
        int UpdateBRECancelReason(int RegId, string CReason, string LastAccessID);

        [OperationContract]
        List<DealerPlanIds> GetDealerSpecificPlans(int userId);

        [OperationContract]
        bool IsCallConferenceWaived(int RegID);

        [OperationContract]
        bool IsInternationalRoamingWaived(int RegID);

        [OperationContract]
        string ArticelIdByModelId(int modelID);

        [OperationContract]
        int RegSecondaryAccountsForSimRplc(List<lnkSecondaryAcctDetailsSimRplc> SecAccountLines);

        [OperationContract]
        List<lnkSecondaryAcctDetailsSimRplc> GetSecondaryAccountLinesForSimRplc(int Regid);

        [OperationContract]
        int SaveLnkPackageComponet(int Regid, List<PackageComponents> PackageComponents, int GroupID);
        [OperationContract]
        List<string> getRebateContractIDbyComponentID(string Componentid);
        [OperationContract]
        List<string> getRebateContractIDs();
        [OperationContract]
        List<DAL.Models.RebateDataContractComponents> getRebateDataContractComponents();

        [OperationContract]
        List<DAL.Models.BreCheckTreatment> getBreCheckTreatment();

        [OperationContract]
        List<DAL.Models.PostCodeCityState> getPostCodeCityState();

        [OperationContract]
        List<DAL.Models.AutoWaiverRules> getAutoWaiverRules();

        [OperationContract]
        int SaveLnkRegRebateDatacontractPenalty(LnkLnkRegRebateDatacontractPenaltyReq objRebatePenaltyReq);
        [OperationContract]
        List<DAL.Models.RegRebateDatacontractPenalty> getRebateDatacontractPenalty(int RegID);
        
        //25052015 Method to get Plan Name query by RegId - Lus
        [OperationContract]
        MnpRegDetailsVM getPlanNamebyRegId(int RegId);

        [OperationContract]
        RegistrationSimCardTypeFailedResp UpdateSimCardType(int RegID, string SimCardType);

        [OperationContract]
        RegistrationSimCardTypeFailedResp UpdateMismSimCardType(int RegID, string MismSimCardType);

        [OperationContract]
        void UpdateSecondarySimSerial(int RegID, string sim);

        [OperationContract]
        Online.Registration.DAL.Models.lnkofferuomarticleprice GetOfferDetailsByID(int OfferID);

        [OperationContract]
        void InsertLog(WebPosLog webposlog);

		[OperationContract]
		List<PackageComponents> getDisconnectComponents(int regID);

		[OperationContract]
		int SaveListRegAttributes(List<RegAttributes> objListRegAtributes);

		[OperationContract]
		int SaveRegAttributes(RegAttributes objListRegAtributes);

		[OperationContract]
		List<RegAttributes> RegAttributesGetAll(RegAttributeReq oReq);

		[OperationContract]
		List<RegAttributes> RegAttributesGetByRegID(int _regID);

		[OperationContract]
		List<RegAttributes> RegAttributesGetBySuppLineID(int _suppLineID);

        //11052015 - Add for Write off supervisor approval CR- Lus
        [OperationContract]
        int SaveJustificationDetails(RegJustification objListRegJustification);

        // 20160120 - w.loon - Write to TrnRegBreFailTreatment - for display in Supervisor Dashboard
        [OperationContract]
        int SaveBreFailTreatmentDetails(RegBreFailTreatment objRegBreFailTreatment);

		[OperationContract]
		int SaveSurveyResponse(RegSurveyResponse objRegSurveyResponse);

        [OperationContract]
        List<int> SaveTrnRegAccessory(List<RegAccessory> objRegAccessories);

        [OperationContract]
        List<RegAccessory> GetRegAccessoryByRegID(int regID);

        [OperationContract]
        bool SaveTrnRegAccessoryDetails(List<RegAccessoryDetails> objRegAccessoryDetailsList);

        [OperationContract]
        List<RegAccessoryDetails> GetRegAccessoryDetailsByRegAccessoryID(List<int> regAccessoryIDs);

        [OperationContract]
        int SaveAccessorySerialNumber(List<RegAccessoryDetails> objRegAccessoryDetailsList);

        [OperationContract]
        List<PopUpMessage> GetPopUpMessageByIdCard(string IDCardNo);

        [OperationContract]
        int UpdatePopUpMessageAcceptReject(int ID, string Action);

        [OperationContract]
        List<PopUpMessageAuditLog> SavePopUpMessageAuditLog(List<PopUpMessageAuditLog> objPopUpMessageAuditLog);

        [OperationContract]
        List<PegaRecommendation> GetPegaRecommendationbyMSISDN(PegaRecommendationSearchCriteria criteria);

        [OperationContract]
		List<PegaRecommendation> SavePegaRecommendation(List<PegaRecommendation> objPegaRecommendation);

        [OperationContract]
        int UpdatePegaRecommendation(List<PegaRecommendationVM> objPegaVM);

        [OperationContract]
        TacAuditLogReqResp InsertTacAuditLog(TacAuditLogReq oReq);

		[OperationContract]
		List<TacAuditLog> GetAllTacAuditLog(int rowCount);

        #region ThirdPartyAuthType
        [OperationContract]
        ThirdPartyAuthTypeCreateResp ThirdPartyAuthTypeCreate(ThirdPartyAuthTypeCreateReq oReq);
        [OperationContract]
        ThirdPartyAuthTypeUpdateResp ThirdPartyAuthTypeUpdate(ThirdPartyAuthTypeUpdateReq oReq);
        [OperationContract]
        ThirdPartyAuthTypeGetResp ThirdPartyAuthTypeGet(ThirdPartyAuthTypeGetReq oReq);
        [OperationContract]
        ThirdPartyAuthTypeGetResp ThirdPartyAuthTypeGetList();
        [OperationContract]
        ThirdPartyAuthTypeFindResp ThirdPartyAuthTypeFind(ThirdPartyAuthTypeFindReq oReq);
        #endregion
    }
}
