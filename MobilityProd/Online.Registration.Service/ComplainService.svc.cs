﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using SNT.Utility;
using Online.Registration.Service.Models;
using Online.Registration.DAL;

using log4net;

namespace Online.Registration.Service
{
    public class ComplainService : IComplainService
    {
        private static readonly ILog Logger = LogManager.GetLogger("ComplainService");

        #region Private Methods

        private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
        {
            resp.Code = "-1";
            resp.Message = ex.Message;
            resp.StackTrace = ex.StackTrace;
        }

        #endregion

        #region Complain

        public ComplainCreateResp ComplainCreate(ComplainCreateReq request)
        {
            var response = new ComplainCreateResp();

            try
            {
                Complaint.ComplainCreate(request.Complain);
                response.ID = request.Complain.ID;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public ComplainUpdateResp ComplainUpdate(ComplainUpdateReq request)
        {
            var response = new ComplainUpdateResp();

            try
            {
                Complaint.ComplainUpdate(request.Complain);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public ComplainFindResp ComplainFind(ComplainFindReq request)
        {
            var response = new ComplainFindResp();

            try
            {
                response.IDs = Complaint.ComplainFind(request.FindCriteria);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public ComplainGetResp ComplainGet(ComplainGetReq request)
        {
            var response = new ComplainGetResp();

            try
            {
                response.Complains = Complaint.ComplainGet(request.IDs);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        #endregion

        #region ComplaintIssues

        public ComplaintIssuesCreateResp ComplaintIssuesCreate(ComplaintIssuesCreateReq request)
        {
            var response = new ComplaintIssuesCreateResp();

            try
            {
                Complaint.ComplaintIssuesCreate(request.ComplaintIssues);
                response.ID = request.ComplaintIssues.ID;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public ComplaintIssuesUpdateResp ComplaintIssuesUpdate(ComplaintIssuesUpdateReq request)
        {
            var response = new ComplaintIssuesUpdateResp();

            try
            {
                Complaint.ComplaintIssuesUpdate(request.ComplaintIssues);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public ComplaintIssuesFindResp ComplaintIssuesFind(ComplaintIssuesFindReq request)
        {
            var response = new ComplaintIssuesFindResp();

            try
            {
                response.IDs = Complaint.ComplaintIssuesFind(request.FindCriteria);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public ComplaintIssuesGetResp ComplaintIssuesGet(ComplaintIssuesGetReq request)
        {
            var response = new ComplaintIssuesGetResp();

            try
            {
                response.ComplaintIssuess = Complaint.ComplaintIssuesGet(request.IDs);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public ComplaintIssuesGetResp ComplaintIssuesList()
        {
            var response = new ComplaintIssuesGetResp();

            try
            {
                response.ComplaintIssuess = Complaint.ComplaintIssuesList();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public ComplaintGetResp GetCmssList(Online.Registration.DAL.Models.CMSSComplainGet objComplaint)
        {
            var response = new ComplaintGetResp();

            try
            {
                response.CMSSComplainIssues = Complaint.GetCmssList(objComplaint);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());                
            }

            return response;
        }
        #endregion

        #region Complaint Issues Group

        public ComplaintIssuesGroupCreateResp ComplaintIssuesGroupCreate(ComplaintIssuesGroupCreateReq request)
        {
            var response = new ComplaintIssuesGroupCreateResp();

            try
            {
                Complaint.ComplaintIssuesGroupCreate(request.ComplaintIssuesGroup);
                response.ID = request.ComplaintIssuesGroup.ID;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public ComplaintIssuesGroupUpdateResp ComplaintIssuesGroupUpdate(ComplaintIssuesGroupUpdateReq request)
        {
            var response = new ComplaintIssuesGroupUpdateResp();

            try
            {
                Complaint.ComplaintIssuesGroupUpdate(request.ComplaintIssuesGroup);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public ComplaintIssuesGroupFindResp ComplaintIssuesGroupFind(ComplaintIssuesGroupFindReq request)
        {
            var response = new ComplaintIssuesGroupFindResp();

            try
            {
                response.IDs = Complaint.ComplaintIssuesGroupFind(request.FindCriteria);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public ComplaintIssuesGroupGetResp ComplaintIssuesGroupGet(ComplaintIssuesGroupGetReq request)
        {
            var response = new ComplaintIssuesGroupGetResp();

            try
            {
                response.ComplaintIssuesGroups = Complaint.ComplaintIssuesGroupGet(request.IDs);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        #endregion

        #region Issue Group Dispatch Queue

        public IssueGrpDispatchQCreateResp IssueGrpDispatchQCreate(IssueGrpDispatchQCreateReq request)
        {
            var response = new IssueGrpDispatchQCreateResp();

            try
            {
                Complaint.IssueGrpDispatchQCreate(request.IssueGrpDispatchQ);
                response.ID = request.IssueGrpDispatchQ.ID;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public IssueGrpDispatchQUpdateResp IssueGrpDispatchQUpdate(IssueGrpDispatchQUpdateReq request)
        {
            var response = new IssueGrpDispatchQUpdateResp();

            try
            {
                Complaint.IssueGrpDispatchQUpdate(request.IssueGrpDispatchQ);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public IssueGrpDispatchQFindResp IssueGrpDispatchQFind(IssueGrpDispatchQFindReq request)
        {
            var response = new IssueGrpDispatchQFindResp();

            try
            {
                response.IDs = Complaint.IssueGrpDispatchQFind(request.FindCriteria);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public IssueGrpDispatchQGetResp IssueGrpDispatchQGet(IssueGrpDispatchQGetReq request)
        {
            var response = new IssueGrpDispatchQGetResp();

            try
            {
                response.IssueGrpDispatchQs = Complaint.IssueGrpDispatchQGet(request.IDs);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        #endregion

        public ComplaintIssuesGetResp GetRequredXMLInputs(ComplaintIssuesGroupFindReq request)
        {
            var response = new ComplaintIssuesGetResp();
            try
            {
                response.ComplaintIssuess = Complaint.GetRequredXMLInputs(request.FindCriteria).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                PopulateExceptionResponse(response, ex);
            }
            return response;

        }

        //#region Complaint Product

        //public ComplaintProductCreateResp ComplaintProductCreate(ComplaintProductCreateReq request)
        //{
        //    var response = new ComplaintProductCreateResp();

        //    try
        //    {
        //        Complaint.ComplaintProductCreate(request.ComplaintProduct);
        //        response.ID = request.ComplaintProduct.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintProductUpdateResp ComplaintProductUpdate(ComplaintProductUpdateReq request)
        //{
        //    var response = new ComplaintProductUpdateResp();

        //    try
        //    {
        //        Complaint.ComplaintProductUpdate(request.ComplaintProduct);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintProductFindResp ComplaintProductFind(ComplaintProductFindReq request)
        //{
        //    var response = new ComplaintProductFindResp();

        //    try
        //    {
        //        response.IDs = Complaint.ComplaintProductFind(request.FindCriteria);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintProductGetResp ComplaintProductGet(ComplaintProductGetReq request)
        //{
        //    var response = new ComplaintProductGetResp();

        //    try
        //    {
        //        response.ComplaintProducts = Complaint.ComplaintProductGet(request.IDs);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //#endregion

        //#region Complaint Reason 1

        //public ComplaintReason1CreateResp ComplaintReason1Create(ComplaintReason1CreateReq request)
        //{
        //    var response = new ComplaintReason1CreateResp();

        //    try
        //    {
        //        Complaint.ComplaintReason1Create(request.ComplaintReason1);
        //        response.ID = request.ComplaintReason1.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason1UpdateResp ComplaintReason1Update(ComplaintReason1UpdateReq request)
        //{
        //    var response = new ComplaintReason1UpdateResp();

        //    try
        //    {
        //        Complaint.ComplaintReason1Update(request.ComplaintReason1);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason1FindResp ComplaintReason1Find(ComplaintReason1FindReq request)
        //{
        //    var response = new ComplaintReason1FindResp();

        //    try
        //    {
        //        response.IDs = Complaint.ComplaintReason1Find(request.FindCriteria);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason1GetResp ComplaintReason1Get(ComplaintReason1GetReq request)
        //{
        //    var response = new ComplaintReason1GetResp();

        //    try
        //    {
        //        response.ComplaintReason1s = Complaint.ComplaintReason1Get(request.IDs);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //#endregion

        //#region Complaint Reason 2

        //public ComplaintReason2CreateResp ComplaintReason2Create(ComplaintReason2CreateReq request)
        //{
        //    var response = new ComplaintReason2CreateResp();

        //    try
        //    {
        //        Complaint.ComplaintReason2Create(request.ComplaintReason2);
        //        response.ID = request.ComplaintReason2.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason2UpdateResp ComplaintReason2Update(ComplaintReason2UpdateReq request)
        //{
        //    var response = new ComplaintReason2UpdateResp();

        //    try
        //    {
        //        Complaint.ComplaintReason2Update(request.ComplaintReason2);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason2FindResp ComplaintReason2Find(ComplaintReason2FindReq request)
        //{
        //    var response = new ComplaintReason2FindResp();

        //    try
        //    {
        //        response.IDs = Complaint.ComplaintReason2Find(request.FindCriteria);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason2GetResp ComplaintReason2Get(ComplaintReason2GetReq request)
        //{
        //    var response = new ComplaintReason2GetResp();

        //    try
        //    {
        //        response.ComplaintReason2s = Complaint.ComplaintReason2Get(request.IDs);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //#endregion

        //#region Complaint Reason 3

        //public ComplaintReason3CreateResp ComplaintReason3Create(ComplaintReason3CreateReq request)
        //{
        //    var response = new ComplaintReason3CreateResp();

        //    try
        //    {
        //        Complaint.ComplaintReason3Create(request.ComplaintReason3);
        //        response.ID = request.ComplaintReason3.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason3UpdateResp ComplaintReason3Update(ComplaintReason3UpdateReq request)
        //{
        //    var response = new ComplaintReason3UpdateResp();

        //    try
        //    {
        //        Complaint.ComplaintReason3Update(request.ComplaintReason3);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason3FindResp ComplaintReason3Find(ComplaintReason3FindReq request)
        //{
        //    var response = new ComplaintReason3FindResp();

        //    try
        //    {
        //        response.IDs = Complaint.ComplaintReason3Find(request.FindCriteria);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason3GetResp ComplaintReason3Get(ComplaintReason3GetReq request)
        //{
        //    var response = new ComplaintReason3GetResp();

        //    try
        //    {
        //        response.ComplaintReason3s = Complaint.ComplaintReason3Get(request.IDs);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //#endregion

        //#region Product Reason 1

        //public ProductReason1CreateResp ProductReason1Create(ProductReason1CreateReq request)
        //{
        //    var response = new ProductReason1CreateResp();

        //    try
        //    {
        //        Complaint.ProductReason1Create(request.ProductReason1);
        //        response.ID = request.ProductReason1.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ProductReason1UpdateResp ProductReason1Update(ProductReason1UpdateReq request)
        //{
        //    var response = new ProductReason1UpdateResp();

        //    try
        //    {
        //        Complaint.ProductReason1Update(request.ProductReason1);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ProductReason1FindResp ProductReason1Find(ProductReason1FindReq request)
        //{
        //    var response = new ProductReason1FindResp();

        //    try
        //    {
        //        response.IDs = Complaint.ProductReason1Find(request.FindCriteria);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ProductReason1GetResp ProductReason1Get(ProductReason1GetReq request)
        //{
        //    var response = new ProductReason1GetResp();

        //    try
        //    {
        //        response.ProductReason1s = Complaint.ProductReason1Get(request.IDs);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //#endregion

        //#region Complaint Reason 12

        //public ComplaintReason12CreateResp ComplaintReason12Create(ComplaintReason12CreateReq request)
        //{
        //    var response = new ComplaintReason12CreateResp();

        //    try
        //    {
        //        Complaint.ComplaintReason12Create(request.ComplaintReason12);
        //        response.ID = request.ComplaintReason12.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason12UpdateResp ComplaintReason12Update(ComplaintReason12UpdateReq request)
        //{
        //    var response = new ComplaintReason12UpdateResp();

        //    try
        //    {
        //        Complaint.ComplaintReason12Update(request.ComplaintReason12);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason12FindResp ComplaintReason12Find(ComplaintReason12FindReq request)
        //{
        //    var response = new ComplaintReason12FindResp();

        //    try
        //    {
        //        response.IDs = Complaint.ComplaintReason12Find(request.FindCriteria);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason12GetResp ComplaintReason12Get(ComplaintReason12GetReq request)
        //{
        //    var response = new ComplaintReason12GetResp();

        //    try
        //    {
        //        response.ComplaintReason12s = Complaint.ComplaintReason12Get(request.IDs);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //#endregion

        //#region Complaint Reason 23

        //public ComplaintReason23CreateResp ComplaintReason23Create(ComplaintReason23CreateReq request)
        //{
        //    var response = new ComplaintReason23CreateResp();

        //    try
        //    {
        //        Complaint.ComplaintReason23Create(request.ComplaintReason23);
        //        response.ID = request.ComplaintReason23.ID;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason23UpdateResp ComplaintReason23Update(ComplaintReason23UpdateReq request)
        //{
        //    var response = new ComplaintReason23UpdateResp();

        //    try
        //    {
        //        Complaint.ComplaintReason23Update(request.ComplaintReason23);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason23FindResp ComplaintReason23Find(ComplaintReason23FindReq request)
        //{
        //    var response = new ComplaintReason23FindResp();

        //    try
        //    {
        //        response.IDs = Complaint.ComplaintReason23Find(request.FindCriteria);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //public ComplaintReason23GetResp ComplaintReason23Get(ComplaintReason23GetReq request)
        //{
        //    var response = new ComplaintReason23GetResp();

        //    try
        //    {
        //        response.ComplaintReason23s = Complaint.ComplaintReason23Get(request.IDs);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex.ToString());
        //        PopulateExceptionResponse(response, ex);
        //    }

        //    return response;
        //}

        //#endregion
    }
}
