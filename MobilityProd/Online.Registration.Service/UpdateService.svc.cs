﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using log4net;
using SNT.Utility;

using Online.Registration.DAL.Models;
using Online.Registration.Service.Models;

namespace Online.Registration.Service
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UpdateService" in code, svc and config file together.
	public class UpdateService : IUpdateService
	{
		static string svcName = "UpdateService";
		private static readonly ILog Logger = LogManager.GetLogger(svcName);

		public int saveRegMdlGrpModels(List<RegMdlGrpModel> objRegMdlGrpModels)
		{
			Logger.InfoFormat("Entering {0}.saveRegMdlGrpModels", svcName);

			int outValue;

			try
			{
				outValue = DAL.Registration.SaveRegMdlGrpModels(objRegMdlGrpModels);
			}
			catch (Exception ex)
			{
				outValue = -1;
				Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
			}
			finally
			{
				Logger.InfoFormat("Exiting {0}.saveRegMdlGrpModel", svcName);
			}
			return outValue;
		}

		public int SavePhotoToDB(string photoData, string custPhoto, string altCustPhoto, int regid)
		{
			Logger.InfoFormat("Entering {0}.SavePhotoToDB", svcName);

			int outValue;

			try
			{
				outValue = DAL.Registration.SavePhotoToDB(photoData, custPhoto, altCustPhoto, regid);
			}
			catch (Exception ex)
			{
				outValue = -1;
				Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
			}
			finally
			{
				Logger.InfoFormat("Exiting {0}.SavePhotoToDB", svcName);
			}
			return outValue;
		}

		public bool FindDocument(string criteria, int regID)
		{

			Logger.InfoFormat("Entering {0}.FindDocument", svcName);

			bool outValue;

			try
			{
				outValue = DAL.Registration.FindDocument(criteria, regID);
			}
			catch (Exception ex)
			{
				outValue = false;
				Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
			}
			finally
			{
				Logger.InfoFormat("Exiting {0}.SavePhotoToDB", svcName);
			}
			return outValue;
		
		}
		
		public List<RegUploadDoc>GetAllDocument(int regID)
		{
			Logger.InfoFormat("Entering {0}.getAllDocument", svcName);

			List<RegUploadDoc> resultList = new List<RegUploadDoc>() ;

			try
			{
				resultList = DAL.Registration.getAllDocument(regID);
			}
			catch (Exception ex)
			{
				Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
				return resultList;
			}
			finally
			{
				Logger.InfoFormat("Exiting {0}.getAllDocument", svcName);
			}
			return resultList;
		}

		public int saveRegMdlGrpModel(RegMdlGrpModel objRegMdlGrpModel)
		{
			Logger.InfoFormat("Entering {0}.saveRegMdlGrpModel", svcName);

			int outValue;

			try
			{
				outValue = DAL.Registration.SaveRegMdlGrpModel(objRegMdlGrpModel);
			}
			catch (Exception ex)
			{
				outValue = -1;
				Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
			}
			finally
			{
				Logger.InfoFormat("Exiting {0}.saveRegMdlGrpModel", svcName);
			}
			return outValue;
		}

		public int uploadDocs(RegUploadDoc objRegUploadDoc)
		{
			Logger.InfoFormat("Entering {0}.uploadDocs", svcName);

			int outValue;

			try
			{
				outValue = DAL.Registration.uploadDoc(objRegUploadDoc);
			}
			catch (Exception ex)
			{
				outValue = -1;
				Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
			}
			finally
			{
				Logger.InfoFormat("Exiting {0}.saveRegMdlGrpModel", svcName);
			}
			return outValue;
		}
		
		public CustomerUpdateResp updateTblCustomer(CustomerUpdateReq objTblCustomer)
		{
			Logger.InfoFormat("Entering {0}.RegistrationUpdate({1})", svcName, (objTblCustomer != null) ? objTblCustomer.Customer.ID : 0);
			var resp = new CustomerUpdateResp();

			try
			{
				int count = DAL.Registration.CustomerUpdate(objTblCustomer.Customer);
				Logger.DebugFormat("{0}.TCMRegService.RegistrationUpdate({1}):{2}", svcName, (objTblCustomer != null) ? objTblCustomer.Customer.ID : 0, count);
			}
			catch (Exception ex)
			{
				resp.Code = "-1";
				string errmsg = "", stacktrace = "";
				resp.Message = errmsg;
				resp.StackTrace = stacktrace;
				Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
			}
			finally
			{
				Logger.InfoFormat("Exiting {0}.RegistrationUpdate({1}): {2}", svcName, (objTblCustomer != null) ? objTblCustomer.Customer.ID : 0, resp.Code);
			}

			return resp;
		}

	}
}
