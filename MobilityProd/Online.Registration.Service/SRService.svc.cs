﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Online.Registration.Service.Models.ServiceRequest;
using Online.Registration.DAL.Repositories;
using Online.Registration.DAL.Interfaces;
using Online.Registration.Service.Models.Common;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.Service.Models;
using log4net;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SRService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SRService.svc or SRService.svc.cs at the Solution Explorer and start debugging.
    public class SRService :ServiceBase, ISRService
    {
        private ISRRepository _theSRRepo;
        protected ISRRepository TheSRRepo
        {
            get
            {
                if (_theSRRepo == null)
                    _theSRRepo = new SRRepository();
                return _theSRRepo;
            }
        }

        public SRServiceResp SaveOrder(SRSaveOrderReq request)
        {
            LogInfoStart( request.Order.CreateBy);
            var resp = new SRServiceResp();
            try
            {
                var result = TheSRRepo.SaveOrder(request.Order);
                if (!string.IsNullOrEmpty(result)){
                    resp.Content = result;
                    resp.Code = "1";
                }
                else
                    throw new Exception("Save order fail");
            }
            catch(Exception ex)
            {
                LogError( ex, request.Order.CreateBy);
                resp.FillException(ex);
            }
            LogInfoEnd( request.Order.CreateBy);
            return resp;
        }

        public SRServiceResp FindOrderById(SRFindOrderReq request)
        {
            LogInfoStart(request.OrderId);
            var resp = new SRServiceResp();
            try
            {
                if (request == null || string.IsNullOrEmpty(request.OrderId))
                    throw new Exception("Please provide correct order id");

                var result = TheSRRepo.FindOrderById(request.OrderId);
                if (result == null)
                    throw new Exception("Invalid order id");

                resp.Content = result;
                resp.Code = "1";
            }
            catch (Exception ex)
            {
                LogError( ex, request.OrderId);
                resp.FillException(ex);
            }
            LogInfoEnd( request.OrderId);
            return resp;
        }

        public SRServiceResp SaveDocs(SRSaveDocsReq request)
        {
            LogInfoStart(request.Docs.First().UpdateBy,request.OrderId);
            var resp = new SRServiceResp();
            try
            {
                if(request==null || string.IsNullOrEmpty(request.OrderId))
                    throw new Exception("Please provide correct order id");

                if(request.Docs==null)
                    throw new Exception("Please provide correct registration form");

                var result = TheSRRepo.SaveVerificationDoc(
                    request.OrderId,
                    request.Docs);

                resp.Content = result;
                resp.Code = "1";

            }
            catch (Exception ex)
            {
                LogError(ex, request.Docs.First().UpdateBy, request.OrderId);
                resp.FillException(ex);
            }
            LogInfoEnd(request.Docs.First().UpdateBy, request.OrderId);
            return resp;
        }

        public SRServiceResp ChangeOrderLock(SRChangeLockReq request)
        {
            LogInfoStart(request.LockBy,request.OrderId);
            var resp = new SRServiceResp();
            try
            {
                if (request == null || string.IsNullOrEmpty(request.OrderId))
                    throw new Exception("Please provide correct order id");
                //If lockby is nothing will treat as remove lock
                var result = TheSRRepo.UpdateOrderLock(request.OrderId, request.LockBy);
                resp.Content = result;
                if(!result)
                    throw new Exception("Fail to change order lock");
                resp.Code = "1";//success
            }
            catch (Exception ex)
            {
                LogError(ex, request.LockBy, request.OrderId);
                resp.FillException(ex);
            }
            LogInfoEnd(request.LockBy, request.OrderId);
            return resp;
        }

        public SRServiceResp UpdateOrderStatus(SRUpdateStatusReq request)
        {
            LogInfoStart(request.UpdateContent.UpdateBy,request.UpdateContent.OrderId);
            var resp = new SRServiceResp();
            try
            {
                if (request == null || request.UpdateContent==null)
                    throw new Exception("No content to update");

                var result = TheSRRepo.UpdateStatus(request.UpdateContent);
                resp.Content = result;
                if (!result)
                    throw new Exception("Fail to update order status");
                resp.Code = "1";//success
            }
            catch (Exception ex)
            {
                LogError(ex, request.UpdateContent.UpdateBy, request.UpdateContent.OrderId);
                resp.FillException(ex);
            }
            LogInfoEnd(request.UpdateContent.UpdateBy, request.UpdateContent.OrderId);
            return resp;
        }

        public SRServiceResp GetDistinctCreateByList(SRGetCreateByReq request)
        {
            string[] param = GetParam(request);
            LogInfoStart(param);
            var resp = new SRServiceResp();
            try
            {
                var lst = TheSRRepo.GetDistinctCreateByList(request.CenterOrgId);
                resp.Content = new CollectionContract(lst);
                resp.Code = "1";
            }
            catch (Exception ex)
            {
                LogError(ex, param);
                resp.FillException(ex);
            }
            LogInfoEnd( param);
            return resp;
        }

        public SRQueryOrderResp QueryOrder(SRQueryOrderReq request)
        {
            string[] param = GetParam(request);
            LogInfoStart(param);
            var resp = new SRQueryOrderResp();
            try
            {
                var totalCount = 0;
                var result = TheSRRepo.QueryOrder(request.QueryDto, out totalCount, request.page, request.pageSize).ToList();
                resp.Content = result;
                resp.TotalCount = totalCount;
                resp.Code = "1";
            }
            catch (Exception ex)
            {
                LogError(ex, request.QueryDto.LoginId);
                resp.FillException(ex);
            }
            LogInfoEnd(param);
            return resp;
        }

        #region "Log Helper Method"
        private string[] GetParam(SRGetCreateByReq request)
        {
            var lst = new List<string>() { request.LoginId };
            lst.Add(string.Format("Center={0}", request.CenterOrgId));
            return lst.ToArray();
        }
        private string[] GetParam(SRQueryOrderReq request)
        {
            var lst = new List<string>() { request.QueryDto.LoginId };
            lst.AddRange(request.QueryDto.CriteriaList.Select(n => string.Format("Name={0}+Value={1}", n.GetEntityName(), n.GetValue())));
            return lst.ToArray();
        }
        #endregion
    }
}
