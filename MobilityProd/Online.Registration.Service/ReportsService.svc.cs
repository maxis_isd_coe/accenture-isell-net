﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using log4net;
using Online.Registration.DAL;
using Online.Registration.Service.Models;
using Online.Registration.DAL.ReportModels;

namespace Online.Registration.Service 
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReportsService" in code, svc and config file together.
    public class ReportsService : IReportsService
    {
        static string svcName = "Online.Registration.Report";
        private static readonly ILog Logger = LogManager.GetLogger(svcName);

        public ReportsService() { Logger.InfoFormat("{0} initialized....", svcName); }

        #region Aging Report

        public AgingReportGetResp AgingReportGet(AgingReportGetReq oRq)
        {
            Logger.InfoFormat("Entering {0}.AgingReportGet({1})", svcName, (oRq != null && oRq.Criteria != null) ? oRq.Criteria.RegID : 0);
            var resp = new AgingReportGetResp();
            try
            {
                resp.AgingReports = Reports.AgingReportGet(oRq.Criteria);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            { 
                Logger.InfoFormat("Exiting {0}.AgingReportGet({1}): {2}", svcName, (oRq != null && oRq.Criteria != null) ? oRq.Criteria.RegID : 0); 
            }
            return resp;
        }

        #endregion

        #region Biometrics Report
        public BiometricsReportGetResp BiometricsReportGet(BiometricsReportGetReq oRq)
        {
            Logger.InfoFormat("Entering {0}.BiometricsReportGet({1})", svcName, (oRq != null && oRq.Criteria != null) ? oRq.Criteria.IDCardNo : string.Empty);
            var resp = new BiometricsReportGetResp();
            try
            {
                resp.BiometricsReports = Reports.BiometricsReportGet(oRq.Criteria);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BiometricsReportGet({1}): {2}", svcName, (oRq != null && oRq.Criteria != null) ? oRq.Criteria.IDCardNo : string.Empty); 
            }
            return resp;
        }
        #endregion

        public FailTransactionReportGetResp FailTranReportGet(FailTransactionReportGetReq oRq)
        {
            Logger.InfoFormat("Entering {0}.FailTranReportGet({1})", svcName, (oRq.Criteria != null) ? oRq.Criteria.RegID : "NULL");
            var resp = new FailTransactionReportGetResp();
            try
            {
                resp.FailTransactionReport = Reports.FailedTransactionGet(oRq.Criteria);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AgingReportGet({1}): {2}", svcName, (oRq.Criteria != null) ? oRq.Criteria.RegID : "NULL");
            }
            return resp;
        }

        public RegHistoryGetResp RegHistoryDetailsGet(RegHistoryGetReq oRq)
        {
            Logger.InfoFormat("Entering {0}.RegHistoryDetailsGet({1})", svcName, (oRq.ID != null) ? oRq.ID : 0);

            var resp = new RegHistoryGetResp();

            try {
                resp.RegHistoryResult = Reports.RegHistoryGet(oRq.ID);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegHistoryDetailsGet({1}): {2}", svcName, (oRq.ID != null) ? oRq.ID : 0);
            }
            return resp;
        }
    }
}
