﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using log4net;
using SNT.Utility;
using Online.Registration.Service.Models;
using Online.Registration.DAL.Admin;
using Online.Registration.DAL;
using System.Runtime.InteropServices;
using Online.Registration.DAL.Models;
using System.Security.Principal;
using System.Data.Common;
using System.Xml; 
using System.IO;
using System.Data.SqlClient;

namespace Online.Registration.Service 
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RegistrationService" in code, svc and config file together.
    public class RegistrationService : IRegistrationService
    {
        static string svcName = "RegistrationService";
        private static readonly ILog Logger = LogManager.GetLogger(svcName);
        

        #region Admin

        #region Address Type

        public AddressTypeCreateResp AddressTypeCreate(AddressTypeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressTypeCreate({1})", svcName, (oReq != null) ? oReq.AddressType.Description : "NULL");

            var resp = new AddressTypeCreateResp();

            try { resp.ID = OnlineRegAdmin.AddressTypeCreate(oReq.AddressType); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AddressTypeCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.AddressType.Description : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }
        public AddressTypeUpdateResp AddressTypeUpdate(AddressTypeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressTypeUpdate({1})", svcName, (oReq != null) ? oReq.AddressType.ID : 0);
            var resp = new AddressTypeUpdateResp();

            try
            {
                int count = OnlineRegAdmin.AddressTypeUpdate(oReq.AddressType);
                Logger.DebugFormat("{0}.TCMRegService.AddressTypeUpdate({1}):{2}", svcName, (oReq != null) ? oReq.AddressType.ID : 0, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;  // +"|" + 
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AddressTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.AddressType.ID : 0, resp.Code);
            }

            return resp;
        }
        public AddressTypeFindResp AddressTypeFind(AddressTypeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressTypeFind({1})", svcName, (oReq != null) ? oReq.AddressTypeFind.AddressType.Description : "NULL");
            var resp = new AddressTypeFindResp();

            try { resp.IDs = OnlineRegAdmin.AddressTypeFind(oReq.AddressTypeFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AddressTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.AddressTypeFind.AddressType.Description : "NULL", resp.Code);
            }

            return resp;
        }
        public AddressTypeGetResp AddressTypeGet(AddressTypeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressTypeGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new AddressTypeGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.AddressTypes = OnlineRegAdmin.AddressTypeGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AddressTypeGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }

        public AddressTypeGetResp AddressTypeList()
        {
            Logger.InfoFormat("Entering {0}.AddressTypeList({1})", svcName, "");
            var resp = new AddressTypeGetResp();
            try
            {
                resp.AddressTypes = OnlineRegAdmin.AddressTypeList();
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AddressTypeList({1}): {2}", svcName, "", resp.Code);
            }

            return resp;
        }

        #endregion

        #region Bank

        public BankCreateResp BankCreate(BankCreateReq oReq)
        {
            //Logger.InfoFormat("Entering {0}.BankCreate({1})", svcName, oReq.Bank.Name);
            Logger.InfoFormat("Entering {0}.BankCreate({1})", svcName, (oReq != null) ? oReq.Bank.Name : "NULL");
            BankCreateResp oRp = new BankCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.BankCreate(oReq.Bank);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BankCreate({1}): {2}|{3}", svcName, oReq.Bank.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public BankUpdateResp BankUpdate(BankUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BankUpdate({1})", svcName, oReq.Bank.Name);
            BankUpdateResp oRp = new BankUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.BankUpdate(oReq.Bank);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Card Bank update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BankUpdate({1}): {2}", svcName, oReq.Bank.Name, oRp.Code);
            }
            return oRp;
        }
        public BankGetResp BankGet(BankGetReq oReq)
        {
            //Logger.InfoFormat("Entering {0}.BankGet({1})", svcName, oReq.IDs[0]);
            Logger.InfoFormat("Entering {0}.BankGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            BankGetResp oRp = new BankGetResp();
            try
            {
                oRp.Bank = OnlineRegAdmin.BankGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BankGet({1}): {2}", svcName, (oReq.IDs.Count() > 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public BankFindResp BankFind(BankFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BankFind({1})", svcName, oReq.BankFind);
            BankFindResp oRp = new BankFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.BankFind(oReq.BankFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BankFind({1}): {2}", svcName, oReq.BankFind.Bank.Name, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region Card Type

        public CardTypeCreateResp CardTypeCreate(CardTypeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypeCreate({1})", svcName, oReq.CardType.Name);
            CardTypeCreateResp oRp = new CardTypeCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.CardTypeCreate(oReq.CardType);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CardTypeCreate({1}): {2}|{3}", svcName, oReq.CardType.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public CardTypeUpdateResp CardTypeUpdate(CardTypeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypeUpdate({1})", svcName, oReq.CardType.Name);
            CardTypeUpdateResp oRp = new CardTypeUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.CardTypeUpdate(oReq.CardType);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Card Type update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CardTypeUpdate({1}): {2}", svcName, oReq.CardType.Name, oRp.Code);
            }
            return oRp;
        }
        public CardTypeGetResp CardTypeGet(CardTypeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}CardTypeGet({1})", svcName, oReq.IDs.Count());
            CardTypeGetResp oRp = new CardTypeGetResp();
            try
            {
                oRp.CardType = OnlineRegAdmin.CardTypeGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CardTypeGet({1}): {2}", svcName, (oReq.IDs.Count() > 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public CardTypeGetResp CardTypeList()
        {
            Logger.InfoFormat("Entering {0}CardTypeList({1})", svcName, "");
            CardTypeGetResp oRp = new CardTypeGetResp();
            try
            {
                oRp.CardType = OnlineRegAdmin.CardTypeList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CardTypeList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public CardTypeFindResp CardTypeFind(CardTypeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypeFind({1})", svcName, oReq.CardTypeFind);
            CardTypeFindResp oRp = new CardTypeFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.CardTypeFind(oReq.CardTypeFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CardTypeFind({1}): {2}", svcName, oReq.CardTypeFind.CardType.Name, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region Country

        public CountryCreateResp CountryCreate(CountryCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CountryCreate({1})", svcName, oReq.Country.Name);
            CountryCreateResp oRp = new CountryCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.CountryCreate(oReq.Country);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CountryCreate({1}): {2}|{3}", svcName, oReq.Country.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public CountryUpdateResp CountryUpdate(CountryUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CountryUpdate({1})", svcName, oReq.Country.Name);
            CountryUpdateResp oRp = new CountryUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.CountryUpdate(oReq.Country);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Country update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CountryUpdate({1}): {2}", svcName, oReq.Country.Name, oRp.Code);
            }
            return oRp;
        }
        public CountryGetResp CountryGet(CountryGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CountryGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            CountryGetResp oRp = new CountryGetResp();
            try
            {
                oRp.Country = OnlineRegAdmin.CountryGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CountryGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public CountryGetResp CountryGet1(CountryGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CountryGet1({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            CountryGetResp oRp = new CountryGetResp();
            try
            {
                oRp.Country = OnlineRegAdmin.CountryGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CountryGet1({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public CountryGetResp CountryList()
        {
            Logger.InfoFormat("Entering {0}.CountryList({1})", svcName, "");
            CountryGetResp oRp = new CountryGetResp();
            try
            {
                oRp.Country = OnlineRegAdmin.CountryList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CountryList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public CountryFindResp CountryFind(CountryFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CountryFind({1})", svcName, oReq.CountryFind.Country.Name);
            CountryFindResp oRp = new CountryFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.CountryFind(oReq.CountryFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CountryFind({1}): {2}", svcName, oReq.CountryFind.Country.Name, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region State

        public StateCreateResp StateCreate(StateCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StateCreate({1})", svcName, oReq.State.Name);
            StateCreateResp oRp = new StateCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.StateCreate(oReq.State);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StateCreate({1}): {2}|{3}", svcName, oReq.State.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public StateUpdateResp StateUpdate(StateUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StateUpdate({1})", svcName, oReq.State.Name);
            StateUpdateResp oRp = new StateUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.StateUpdate(oReq.State);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "State update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StateUpdate({1}): {2}", svcName, oReq.State.Name, oRp.Code);
            }
            return oRp;
        }
        public StateGetResp StateGet(StateGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}StateGet({1})", svcName, (oReq.IDs != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            StateGetResp oRp = new StateGetResp();
            try
            {
                oRp.State = OnlineRegAdmin.StateGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StateGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public StateGetResp StateList()
        {
            Logger.InfoFormat("Entering {0}StateList({1})", svcName, "");
            StateGetResp oRp = new StateGetResp();
            try
            {
                oRp.State = OnlineRegAdmin.StateList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StateList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public StateFindResp StateFind(StateFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StateFind({1})", svcName, (oReq.StateFind != null) ? oReq.StateFind.CountryState.Name : "NULL");
            StateFindResp oRp = new StateFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.StateFind(oReq.StateFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StateFind({1}): {2}", svcName, (oReq.StateFind != null) ? oReq.StateFind.CountryState.Name : "NULL", oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region ID Card Type

        public IDCardTypeCreateResp IDCardTypeCreate(IDCardTypeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.IDCardTypeCreate({1})", svcName, oReq.IDCardType.Code);
            IDCardTypeCreateResp oRp = new IDCardTypeCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.IDCardTypeCreate(oReq.IDCardType);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.IDCardTypeCreate({1}): {2}|{3}", svcName, oReq.IDCardType.Code, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public IDCardTypeUpdateResp IDCardTypeUpdate(IDCardTypeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.IDCardTypeUpdate({1})", svcName, oReq.IDCardType.Code);
            IDCardTypeUpdateResp oRp = new IDCardTypeUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.IDCardTypeUpdate(oReq.IDCardType);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Card Type update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.IDCardTypeUpdate({1}): {2}", svcName, oReq.IDCardType.Code, oRp.Code);
            }
            return oRp;
        }
        public IDCardTypeGetResp IDCardTypeGet(IDCardTypeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}IDCardTypeGet({1})", svcName, (oReq.IDs.Count > 0) ? oReq.IDs.Count : 0);
            IDCardTypeGetResp oRp = new IDCardTypeGetResp();
            try
            {
                oRp.IDCardType = OnlineRegAdmin.IDCardTypeGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.IDCardTypeGet({1}): {2}", svcName, (oReq.IDs.Count > 0) ? oReq.IDs.Count : 0, oRp.Code);
            }
            return oRp;
        }

        public IDCardTypeGetResp IDCardTypeList()
        {
            Logger.InfoFormat("Entering {0}IDCardTypeList({1})", svcName, "");
            IDCardTypeGetResp oRp = new IDCardTypeGetResp();
            try
            {
                oRp.IDCardType = OnlineRegAdmin.IDCardTypeList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.IDCardTypeList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public IDCardTypeFindResp IDCardTypeFind(IDCardTypeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.IDCardTypeFind({1})", svcName, (oReq.IDCardTypeFind != null) ? oReq.IDCardTypeFind.IDCardType.Code : "Null");
            IDCardTypeFindResp oRp = new IDCardTypeFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.IDCardTypeFind(oReq.IDCardTypeFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.IDCardTypeFind({1}): {2}", svcName, (oReq.IDCardTypeFind != null) ? oReq.IDCardTypeFind.IDCardType.Code : "Null", oRp.Code);
            }

            return oRp;
        }

        #endregion

        #region Payment Mode

        public PaymentModeCreateResp PaymentModeCreate(PaymentModeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModeCreate({1})", svcName, oReq.PaymentMode.Code);
            PaymentModeCreateResp oRp = new PaymentModeCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.PaymentModeCreate(oReq.PaymentMode);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PaymentModeCreate({1}): {2}|{3}", svcName, oReq.PaymentMode.Code, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public PaymentModeUpdateResp PaymentModeUpdate(PaymentModeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModeUpdate({1})", svcName, oReq.PaymentMode.ID);
            PaymentModeUpdateResp oRp = new PaymentModeUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.PaymentModeUpdate(oReq.PaymentMode);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Payment Mode update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PaymentModeUpdate({1}): {2}", svcName, oReq.PaymentMode.ID, oRp.Code);
            }
            return oRp;
        }
        public PaymentModeGetResp PaymentModeGet(PaymentModeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}PaymentModeGet({1})", svcName, oReq.IDs.Count);
            PaymentModeGetResp oRp = new PaymentModeGetResp();
            try
            {
                oRp.PaymentMode = OnlineRegAdmin.PaymentModeGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PaymentModeGet({1}): {2}", svcName, oReq.IDs.Count, oRp.Code);
            }
            return oRp;
        }

        public PaymentModeGetResp PaymentModeList()
        {
            Logger.InfoFormat("Entering {0}PaymentModeList({1})", svcName, "");
            PaymentModeGetResp oRp = new PaymentModeGetResp();
            try
            {
                oRp.PaymentMode = OnlineRegAdmin.PaymentModeList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PaymentModeList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public PaymentModeFindResp PaymentModeFind(PaymentModeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModeFind({1})", svcName, oReq.PaymentModeFind);
            PaymentModeFindResp oRp = new PaymentModeFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.PaymentModeFind(oReq.PaymentModeFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PaymentModeFind({1}): {2}", svcName, oReq.PaymentModeFind.PaymentMode.ID, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region Reg Type

        public RegTypeCreateResp RegTypeCreate(RegTypeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegTypeCreate({1})", svcName, (oReq != null) ? oReq.RegType.Code : "NULL");

            var resp = new RegTypeCreateResp();

            try { resp.ID = OnlineRegAdmin.RegTypeCreate(oReq.RegType); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegTypeCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.RegType.Code : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }
        public RegTypeUpdateResp RegTypeUpdate(RegTypeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegTypeUpdate({1})", svcName, (oReq != null) ? oReq.RegType.ID : 0);
            var resp = new RegTypeUpdateResp();

            try
            {
                int count = OnlineRegAdmin.RegTypeUpdate(oReq.RegType);
                Logger.DebugFormat("{0}.TCMRegService.RegTypeUpdate({1}):{2}", svcName, (oReq != null) ? oReq.RegType.ID : 0, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;  // +"|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.RegType.ID : 0, resp.Code);
            }

            return resp;
        }
        public RegTypeFindResp RegTypeFind(RegTypeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegTypeFind({1})", svcName, (oReq != null) ? oReq.RegTypeFind.RegType.Code : "NULL");
            var resp = new RegTypeFindResp();

            try { resp.IDs = OnlineRegAdmin.RegTypeFind(oReq.RegTypeFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.RegTypeFind.RegType.Code : "NULL", resp.Code);
            }

            return resp;
        }
        public RegTypeGetResp RegTypeGet(RegTypeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegTypeGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs.Count : 0);
            var resp = new RegTypeGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.RegTypes = OnlineRegAdmin.RegTypeGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegTypeGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs.Count : 0, resp.Code);
            }

            return resp;
        }

        public RegTypeGetResp RegTypeList()
        {
            Logger.InfoFormat("Entering {0}.RegTypeList({1})", svcName, "");
            var resp = new RegTypeGetResp();
            try
            {
                resp.RegTypes = OnlineRegAdmin.RegTypeList();
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegTypeList({1}): {2}", svcName, "", resp.Code);
            }
            return resp;
        }

        #endregion

        #region Race

        public RaceCreateResp RaceCreate(RaceCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RaceCreate({1})", svcName, oReq.Race.Name);
            RaceCreateResp oRp = new RaceCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.RaceCreate(oReq.Race);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RaceCreate({1}): {2}|{3}", svcName, oReq.Race.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public RaceUpdateResp RaceUpdate(RaceUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RaceUpdate({1})", svcName, oReq.Race.Name);
            RaceUpdateResp oRp = new RaceUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.RaceUpdate(oReq.Race);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Card Race update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RaceUpdate({1}): {2}", svcName, oReq.Race.Name, oRp.Code);
            }
            return oRp;
        }
        public RaceGetResp RaceGet(RaceGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}RaceGet({1})", svcName, oReq.IDs.Count);
            RaceGetResp oRp = new RaceGetResp();
            try
            {
                oRp.Races = OnlineRegAdmin.RaceGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RaceGet({1}): {2}", svcName, (oReq.IDs != null) ? oReq.IDs.Count : 0, oRp.Code);
            }
            return oRp;
        }
        public RaceGetResp RaceList()
        {
            Logger.InfoFormat("Entering {0}RaceList({1})", svcName, "");
            RaceGetResp oRp = new RaceGetResp();
            try
            {
                oRp.Races = OnlineRegAdmin.RaceList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RaceList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }
        public RaceFindResp RaceFind(RaceFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RaceFind({1})", svcName, oReq.RaceFind);
            RaceFindResp oRp = new RaceFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.RaceFind(oReq.RaceFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RaceFind({1}): {2}", svcName, oReq.RaceFind.Race.Name, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region Nationality

        public NationalityCreateResp NationalityCreate(NationalityCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.NationalityCreate({1})", svcName, oReq.Nationality.Name);
            NationalityCreateResp oRp = new NationalityCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.NationalityCreate(oReq.Nationality);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.NationalityCreate({1}): {2}|{3}", svcName, oReq.Nationality.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public NationalityUpdateResp NationalityUpdate(NationalityUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.NationalityUpdate({1})", svcName, oReq.Nationality.Name);
            NationalityUpdateResp oRp = new NationalityUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.NationalityUpdate(oReq.Nationality);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Card Nationality update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.NationalityUpdate({1}): {2}", svcName, oReq.Nationality.Name, oRp.Code);
            }
            return oRp;
        }
        public NationalityGetResp NationalityGet(NationalityGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}NationalityGet({1})", svcName, (oReq.IDs.Count > 0) ? oReq.IDs.Count : 0);
            NationalityGetResp oRp = new NationalityGetResp();
            try
            {
                oRp.Nationalities = OnlineRegAdmin.NationalityGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.NationalityGet({1}): {2}", svcName, (oReq.IDs.Count > 0) ? oReq.IDs.Count : 0, oRp.Code);
            }
            return oRp;
        }

        public NationalityGetResp NationalityList()
        {
            Logger.InfoFormat("Entering {0}NationalityList({1})", svcName, "");
            NationalityGetResp oRp = new NationalityGetResp();
            try
            {
                oRp.Nationalities = OnlineRegAdmin.NationalityList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.NationalityList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }


        public NationalityFindResp NationalityFind(NationalityFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.NationalityFind({1})", svcName, oReq.NationalityFind);
            NationalityFindResp oRp = new NationalityFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.NationalityFind(oReq.NationalityFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.NationalityFind({1}): {2}", svcName, oReq.NationalityFind.Nationality.Name, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region Language

        public LanguageCreateResp LanguageCreate(LanguageCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.LanguageCreate({1})", svcName, oReq.Language.Name);
            LanguageCreateResp oRp = new LanguageCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.LanguageCreate(oReq.Language);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.LanguageCreate({1}): {2}|{3}", svcName, oReq.Language.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public LanguageUpdateResp LanguageUpdate(LanguageUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.LanguageUpdate({1})", svcName, oReq.Language.Name);
            LanguageUpdateResp oRp = new LanguageUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.LanguageUpdate(oReq.Language);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Card Language update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.LanguageUpdate({1}): {2}", svcName, oReq.Language.Name, oRp.Code);
            }
            return oRp;
        }
        public LanguageGetResp LanguageGet(LanguageGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}LanguageGet({1})", svcName, (oReq.IDs.Count > 0) ? oReq.IDs.Count : 0);
            LanguageGetResp oRp = new LanguageGetResp();
            try
            {
                oRp.Languages = OnlineRegAdmin.LanguageGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.LanguageGet({1}): {2}", svcName, (oReq.IDs.Count > 0) ? oReq.IDs.Count : 0, oRp.Code);
            }
            return oRp;
        }

        public LanguageGetResp LanguageGetList()
        {
            Logger.InfoFormat("Entering {0}LanguageGetList({1})", svcName, "");
            LanguageGetResp oRp = new LanguageGetResp();
            try
            {
                oRp.Languages = OnlineRegAdmin.LanguageGetList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.LanguageGetList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }


        public LanguageFindResp LanguageFind(LanguageFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.LanguageFind({1})", svcName, oReq.LanguageFind);
            LanguageFindResp oRp = new LanguageFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.LanguageFind(oReq.LanguageFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.LanguageFind({1}): {2}", svcName, oReq.LanguageFind.Language.Name, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region CustomerTitle

        public CustomerTitleCreateResp CustomerTitleCreate(CustomerTitleCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerTitleCreate({1})", svcName, oReq.CustomerTitle.Name);
            CustomerTitleCreateResp oRp = new CustomerTitleCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.CustomerTitleCreate(oReq.CustomerTitle);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CustomerTitleCreate({1}): {2}|{3}", svcName, oReq.CustomerTitle.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public CustomerTitleUpdateResp CustomerTitleUpdate(CustomerTitleUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerTitleUpdate({1})", svcName, oReq.CustomerTitle.Name);
            CustomerTitleUpdateResp oRp = new CustomerTitleUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.CustomerTitleUpdate(oReq.CustomerTitle);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Card CustomerTitle update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CustomerTitleUpdate({1}): {2}", svcName, oReq.CustomerTitle.Name, oRp.Code);
            }
            return oRp;
        }
        public CustomerTitleGetResp CustomerTitleGet(CustomerTitleGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerTitleGet({1})", svcName, oReq.IDs.Count());
            CustomerTitleGetResp oRp = new CustomerTitleGetResp();
            try
            {
                oRp.CustomerTitles = OnlineRegAdmin.CustomerTitleGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CustomerTitleGet({1}): {2}", svcName, (oReq.IDs.Count() > 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public CustomerTitleGetResp CustomerTitleList()
        {
            Logger.InfoFormat("Entering {0}.CustomerTitleList({1})", svcName, "");
            CustomerTitleGetResp oRp = new CustomerTitleGetResp();
            try
            {
                oRp.CustomerTitles = OnlineRegAdmin.CustomerTitleList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CustomerTitleList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public CustomerTitleFindResp CustomerTitleFind(CustomerTitleFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerTitleFind({1})", svcName, oReq.CustomerTitleFind);
            CustomerTitleFindResp oRp = new CustomerTitleFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.CustomerTitleFind(oReq.CustomerTitleFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CustomerTitleFind({1}): {2}", svcName, oReq.CustomerTitleFind.CustomerTitle.Name, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region ExtIDType

        public ExtIDTypeCreateResp ExtIDTypeCreate(ExtIDTypeCreateReq oReq)
        {
            //Logger.InfoFormat("Entering {0}.ExtIDTypeCreate({1})", svcName, oReq.ExtIDType.Name);
            Logger.InfoFormat("Entering {0}.ExtIDTypeCreate({1})", svcName, (oReq != null) ? oReq.ExtIDType.Name : "NULL");
            ExtIDTypeCreateResp oRp = new ExtIDTypeCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.ExtIDTypeCreate(oReq.ExtIDType);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ExtIDTypeCreate({1}): {2}|{3}", svcName, oReq.ExtIDType.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public ExtIDTypeUpdateResp ExtIDTypeUpdate(ExtIDTypeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ExtIDTypeUpdate({1})", svcName, oReq.ExtIDType.Name);
            ExtIDTypeUpdateResp oRp = new ExtIDTypeUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.ExtIDTypeUpdate(oReq.ExtIDType);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Card ExtIDType update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ExtIDTypeUpdate({1}): {2}", svcName, oReq.ExtIDType.Name, oRp.Code);
            }
            return oRp;
        }
        public ExtIDTypeGetResp ExtIDTypeGet(ExtIDTypeGetReq oReq)
        {
            //Logger.InfoFormat("Entering {0}.ExtIDTypeGet({1})", svcName, oReq.IDs[0]);
            Logger.InfoFormat("Entering {0}.ExtIDTypeGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            ExtIDTypeGetResp oRp = new ExtIDTypeGetResp();
            try
            {
                oRp.ExtIDType = OnlineRegAdmin.ExtIDTypeGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ExtIDTypeGet({1}): {2}", svcName, (oReq.IDs.Count() > 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public ExtIDTypeGetResp ExtIDTypeList()
        {
            Logger.InfoFormat("Entering {0}.ExtIDTypeList({1})", svcName, "");
            ExtIDTypeGetResp oRp = new ExtIDTypeGetResp();
            try
            {
                oRp.ExtIDType = OnlineRegAdmin.ExtIDTypeList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ExtIDTypeList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }
        public ExtIDTypeFindResp ExtIDTypeFind(ExtIDTypeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ExtIDTypeFind({1})", svcName, oReq.ExtIDTypeFind);
            ExtIDTypeFindResp oRp = new ExtIDTypeFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ExtIDTypeFind(oReq.ExtIDTypeFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ExtIDTypeFind({1}): {2}", svcName, oReq.ExtIDTypeFind.ExtIDType.Name, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region VIPCode

        public VIPCodeCreateResp VIPCodeCreate(VIPCodeCreateReq oReq)
        {
            //Logger.InfoFormat("Entering {0}.VIPCodeCreate({1})", svcName, oReq.VIPCode.Name);
            Logger.InfoFormat("Entering {0}.VIPCodeCreate({1})", svcName, (oReq != null) ? oReq.VIPCode.Name : "NULL");
            VIPCodeCreateResp oRp = new VIPCodeCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.VIPCodeCreate(oReq.VIPCode);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.VIPCodeCreate({1}): {2}|{3}", svcName, oReq.VIPCode.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public VIPCodeUpdateResp VIPCodeUpdate(VIPCodeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.VIPCodeUpdate({1})", svcName, oReq.VIPCode.Name);
            VIPCodeUpdateResp oRp = new VIPCodeUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.VIPCodeUpdate(oReq.VIPCode);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Card VIPCode update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.VIPCodeUpdate({1}): {2}", svcName, oReq.VIPCode.Name, oRp.Code);
            }
            return oRp;
        }
        public VIPCodeGetResp VIPCodeGet(VIPCodeGetReq oReq)
        {
            //Logger.InfoFormat("Entering {0}.VIPCodeGet({1})", svcName, oReq.IDs[0]);
            Logger.InfoFormat("Entering {0}.VIPCodeGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            VIPCodeGetResp oRp = new VIPCodeGetResp();
            try
            {
                oRp.VIPCode = OnlineRegAdmin.VIPCodeGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.VIPCodeGet({1}): {2}", svcName, (oReq.IDs.Count() > 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public VIPCodeGetResp VIPCodeList()
        {
            Logger.InfoFormat("Entering {0}.VIPCodeList({1})", svcName, "");
            VIPCodeGetResp oRp = new VIPCodeGetResp();
            try
            {
                oRp.VIPCode = OnlineRegAdmin.VIPCodeList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.VIPCodeGet({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public VIPCodeFindResp VIPCodeFind(VIPCodeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.VIPCodeFind({1})", svcName, oReq.VIPCodeFind);
            VIPCodeFindResp oRp = new VIPCodeFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.VIPCodeFind(oReq.VIPCodeFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.VIPCodeFind({1}): {2}", svcName, oReq.VIPCodeFind.VIPCode.Name, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #endregion

        #region Operation

        #region Registration

        public RegistrationCreateResp RegistrationCreate(RegistrationCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCreate({1})", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL");

            var resp = new RegistrationCreateResp();

            try
            {
                resp.ID = DAL.Registration.RegistrationCreate(oReq.Registration, oReq.Customer, oReq.Addresses, oReq.RegPgmBdlPkgComps, oReq.RegMdlGrpModel,
                                                              oReq.RegStatus, oReq.RegSuppLines, oReq.RegSuppLineVASes, oReq.CustPhotos, oReq.RegSmartComponents, oReq.RegWaiverComponents,oReq.isBREFail,oReq.approverID,oReq.TimeApproval);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }

        public RegistrationCreateResp SecondaryLineRegistrationCreate(RegistrationCreateWithSecReq oReq)
        {
            Logger.InfoFormat("Entering {0}.SecondaryLineRegistrationCreate({1})", svcName, (oReq != null) ? oReq.RegistrationSec.LastAccessID : "NULL");

            var resp = new RegistrationCreateResp();

            try
            {
                resp.ID = DAL.Registration.SecondaryLineRegistrationCreate(oReq.RegistrationSec, oReq.RegPgmBdlPkgCompSecs);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SecondaryLineRegistrationCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.RegistrationSec.LastAccessID : "NULL", resp.ID, resp.Code);
            }

            return resp;

        }

        public RegistrationCreateResp RegistrationCreateWithSec(string compressedRequest)
        {
            //RegistrationCreateWithSecReq

            RegistrationCreateWithSecReq oReq;
            using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(SNT.Utility.Compress.Unzip(compressedRequest))))
            {
                var readerQuota = new XmlDictionaryReaderQuotas();
                readerQuota.MaxArrayLength = int.MaxValue;
                readerQuota.MaxBytesPerRead = int.MaxValue;
                readerQuota.MaxDepth = int.MaxValue;
                readerQuota.MaxNameTableCharCount = int.MaxValue;
                readerQuota.MaxStringContentLength = int.MaxValue;
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(memoryStream, Encoding.UTF8, readerQuota, null);
                DataContractSerializer serializer = new DataContractSerializer(typeof(RegistrationCreateWithSecReq));
                oReq = (RegistrationCreateWithSecReq)serializer.ReadObject(reader);
            }

            Logger.InfoFormat("Entering {0}.RegistrationCreateWithSec({1})", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL");

            var resp = new RegistrationCreateResp();

            try
            {
                resp.ID = DAL.Registration.RegistrationCreateWithSec(
                    oReq.Registration,
                    oReq.Customer,
                    oReq.Addresses,
                    oReq.RegPgmBdlPkgComps,
                    oReq.RegMdlGrpModel,
                    oReq.RegStatus,
                    oReq.RegSuppLines,
                    oReq.RegSuppLineVASes,
                    oReq.CustPhotos,
                    oReq.RegistrationSec,
                    oReq.RegPgmBdlPkgCompSecs,
                    oReq.RegMdlGrpModelSec,
                    oReq.RegWaiverComponents,
                    oReq.isBREFail,
                    oReq.approverID,
                    oReq.TimeApproval
                    );
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the RegistrationCreateWithSec method:" + ex);
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationCreateWithSec({1}): {2}|{3}", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }

        /* Begin Kenan CustomerInfo Create added by VLT on 5 Mar 2013*/
        public KenanCustomerInfoCreateResp KenanCustomerInfoCreate(KenanCustomerInfoCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.KenanCustomerInfoCreate({1})", svcName, (oReq != null) ? oReq.KenanCustomerInfo.IDCardNo : "NULL");

            var resp = new KenanCustomerInfoCreateResp();

            try
            {
                resp.ID = DAL.Registration.KenanCustomerInfoCreate(oReq.KenanCustomerInfo);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.KenanCustomerInfoCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.KenanCustomerInfo.IDCardNo : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }

        /// <summary>
        /// MNP supplementary lines update.
        /// </summary>
        /// <param name="oReqs">The o reqs.</param>
        /// <returns>TypeOf(int)</returns>
        public RegSuppLineUpdateResp RegSuppLineUpdate(RegSuppLineMultiUpdateReq oReqs)
        {
            Logger.InfoFormat("Entering {0}.RegSuppLineUpdate({1})", svcName, (oReqs != null) ? oReqs.RegSuppLines.FirstOrDefault().ID : 0);
            var resp = new RegSuppLineUpdateResp();
            var UpdateRequest = oReqs.RegSuppLines;

            try
            {
                resp.Code = Convert.ToString(DAL.Registration.RegSuppLineUpdate(UpdateRequest));
                Logger.DebugFormat("{0}.TCMRegService.RegSuppLineUpdate({1}):{2}", svcName, (oReqs != null) ? UpdateRequest.FirstOrDefault().ID : 0, resp.Code.ToInt());
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;  // +"|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegSuppLineUpdate({1}): {2}", svcName, (oReqs != null) ? oReqs.RegSuppLines.FirstOrDefault().ID : 0, resp.Code);
            }

            return resp;
        }
        /* End Kenan CustomerInfo Create added by VLT on 5 Mar 2013*/
        //Home Registration create
        public HomeRegistrationCreateResp HomeRegistrationCreate(RegistrationCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.HomeRegistrationCreate({1})", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL");

            var resp = new HomeRegistrationCreateResp();

            try
            {
                resp.ID = DAL.Registration.HomeRegistrationCreate(oReq.Registration, oReq.Customer, oReq.Addresses, oReq.RegPgmBdlPkgComps, oReq.RegStatus);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.HomeRegistrationCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }

        public RegistrationUpdateResp RegistrationUpdate(RegistrationUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationUpdate({1})", svcName, (oReq != null) ? oReq.Registration.ID : 0);
            var resp = new RegistrationUpdateResp();

            try
            {
                int count = DAL.Registration.RegistrationUpdate(oReq.Registration);
                Logger.DebugFormat("{0}.TCMRegService.RegistrationUpdate({1}):{2}", svcName, (oReq != null) ? oReq.Registration.ID : 0, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;  // +"|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Registration.ID : 0, resp.Code);
            }

            return resp;
        }

        public RegistrationUpdateResp RegistrationUpdateMISM(RegistrationUpdateMISMReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationUpdateMISM({1})", svcName, (oReq != null) ? oReq.RegistrationSec.RegistrationID : 0);
            var resp = new RegistrationUpdateResp();

            try
            {
                int count = DAL.Registration.RegistrationUpdateMISM(oReq.RegistrationSec);
                Logger.DebugFormat("{0}.TCMRegService.RegistrationUpdateMISM({1}):{2}", svcName, (oReq != null) ? oReq.RegistrationSec.RegistrationID : 0, count);
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the RegistrationUpdateMISM method:" + ex);
                resp.Code = "-1";
                //resp.Message = ex.Message;  // +"|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationUpdate({1}): {2}", svcName, (oReq != null) ? oReq.RegistrationSec.RegistrationID : 0, resp.Code);
            }

            return resp;
        }
        public RegistrationFindResp RegistrationFind(RegistrationFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationFind({1})", svcName, (oReq != null) ? oReq.RegistrationFind.Registration.LastAccessID : "NULL");
            var resp = new RegistrationFindResp();

            try { resp.IDs = DAL.Registration.RegistrationFind(oReq.RegistrationFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationFind({1}): {2}", svcName, (oReq != null) ? oReq.RegistrationFind.Registration.LastAccessID : "NULL", resp.Code);
            }

            return resp;
        }
        public RegistrationGetResp RegistrationGet(RegistrationGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new RegistrationGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.Registrations = DAL.Registration.RegistrationGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the RegistrationGet method:" + ex);
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }
        public RegistrationSecGetResp RegistrationSecGet(RegistrationSecGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationSecGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new RegistrationSecGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.Registrations = DAL.Registration.RegistrationSecGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }
        public RegistrationSearchResp RegistrationSearch(RegistrationSearchReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationSearch({1})", svcName, (oReq != null) ? oReq.Criteria.IDCardNo : "NULL");
            var resp = new RegistrationSearchResp();

            try { resp.RegistrationSearchResults = DAL.Registration.RegistrationSearch(oReq.Criteria); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationSearch({1}): {2}", svcName, (oReq != null) ? oReq.Criteria.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }

      
        #region Added by Patanjali on 30-03-2013 for Store keeper and Cashier flow
            public RegistrationSearchResp StoreKeeperSearch(RegistrationSearchReq oReq)
            {
                Logger.InfoFormat("Entering {0}.RegistrationSearch({1})", svcName, (oReq != null) ? oReq.Criteria.IDCardNo : "NULL");
                var resp = new RegistrationSearchResp();

                try { resp.RegistrationSearchResults = DAL.Registration.StoreKeeperSearch(oReq.Criteria); }
                catch (Exception ex)
                {
                    resp.Code = "-1";
                    resp.Message = ex.Message;  // +"|" + 
                    resp.StackTrace = ex.StackTrace;
                    Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
                }
                finally
                {
                    Logger.InfoFormat("Exiting {0}.RegistrationSearch({1}): {2}", svcName, (oReq != null) ? oReq.Criteria.RegID.ToString2() : "NULL", resp.Code);
                }

                return resp;
        }

            public AgentCodeByUserNameResp GetAgentCodeByUserName(string userName)
            {

                var resp = new AgentCodeByUserNameResp();

                try { 
                    resp.AgentCodeByUserNameResult = DAL.Registration.GetAgentCodeByUserName(userName); 
                }
                catch (Exception ex)
                {
                    resp.Code = "-1";
                    resp.Message = ex.Message;  // +"|" + 
                    resp.StackTrace = ex.StackTrace;
                    Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
                }
                finally
                {
                    //
                }

                return resp;
            }

        public RegistrationSearchResp CashierSearch(RegistrationSearchReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationSearch({1})", svcName, (oReq != null) ? oReq.Criteria.IDCardNo : "NULL");
            var resp = new RegistrationSearchResp();

            try { resp.RegistrationSearchResults = DAL.Registration.CashierSearch(oReq.Criteria); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationSearch({1}): {2}", svcName, (oReq != null) ? oReq.Criteria.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        #endregion

        public RegistrationSearchResp OrderStatusReportSearch(RegistrationSearchReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationSearch({1})", svcName, (oReq != null) ? oReq.Criteria.IDCardNo : "NULL");
            var resp = new RegistrationSearchResp();

            try { resp.RegistrationSearchResults = DAL.Registration.OrderStatusReportSearch(oReq.Criteria); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationSearch({1}): {2}", svcName, (oReq != null) ? oReq.Criteria.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }

        public RegistrationSearchResp TLProfileSearch(RegistrationSearchReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationSearch({1})", svcName, (oReq != null) ? oReq.Criteria.IDCardNo : "NULL");
            var resp = new RegistrationSearchResp();

            try { resp.RegistrationSearchResults = DAL.Registration.TLProfileSearch(oReq.Criteria); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationSearch({1}): {2}", svcName, (oReq != null) ? oReq.Criteria.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }


        public RegistrationCloseResp RegistrationClose(RegistrationCloseReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationClose({1})", svcName, (oReq != null && oReq.RegStatus.RegID > 0) ? oReq.RegStatus.RegID : 0);
            var resp = new RegistrationCloseResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { DAL.Registration.RegistrationClose(oReq.RegStatus); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationClose({1}): {2}", svcName, (oReq != null && oReq.RegStatus.RegID > 0) ? oReq.RegStatus.RegID : 0, resp.Code);
            }

            return resp;
        }
        public RegistrationCancelResp RegistrationCancel(RegistrationCancelReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCancel({1})", svcName, (oReq != null && oReq.RegStatus.RegID > 0) ? oReq.RegStatus.RegID : 0);
            var resp = new RegistrationCancelResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { DAL.Registration.RegistrationCancel(oReq.RegStatus); }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 1205 && !ReferenceEquals(oReq,null))
                {
                    DAL.Registration.RegistrationCancel(oReq.RegStatus);
                }
                else
                {
                    throw;
                }
            }
            catch (Exception ex)
            {   
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationCancel({1}): {2}", svcName, (oReq != null && oReq.RegStatus.RegID > 0) ? oReq.RegStatus.RegID : 0, resp.Code);
            }

            return resp;
        }

        public RegistrationPaymentFailedResp RegistrationPaymentFailed(RegistrationPaymentFailedReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationPaymentFailed({1})", svcName, (oReq != null && oReq.RegStatus.RegID > 0) ? oReq.RegStatus.RegID : 0);
            var resp = new RegistrationPaymentFailedResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { DAL.Registration.RegistrationPaymentFailed(oReq.RegStatus); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationPaymentFailed({1}): {2}", svcName, (oReq != null && oReq.RegStatus.RegID > 0) ? oReq.RegStatus.RegID : 0, resp.Code);
            }

            return resp;
        }

        public RegistrationPaymentSuccessResp RegistrationPaymentSuccess(RegistrationPaymentSuccessReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationPaymentSuccess({1})", svcName, (oReq != null && oReq.RegStatus.RegID > 0) ? oReq.RegStatus.RegID : 0);
            var resp = new RegistrationPaymentSuccessResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { DAL.Registration.RegistrationPaymentSuccess(oReq.RegStatus); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationPaymentSuccess({1}): {2}", svcName, (oReq != null && oReq.RegStatus.RegID > 0) ? oReq.RegStatus.RegID : 0, resp.Code);
            }

            return resp;
        }

        #endregion

        #region RegAddress

        //public AddressCreateResp AddressCreate(AddressCreateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.AddressCreate({1})", svcName, (oReq != null) ? oReq.Address.Code : "NULL");

        //    var resp = new AddressCreateResp();

        //    try { resp.ID = DAL.Registration.AddressCreate(oReq.Addresses); }
        //    catch (Exception ex)
        //    {
        //        resp.Code = "-1";
        //        //resp.Message = ex.Message; //+ "|" + 
        //        //resp.StackTrace = ex.StackTrace;
        //        //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
        //        resp.Message = errmsg;
        //        resp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.AddressCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.Address.Code : "NULL", resp.ID, resp.Code); 
        //    }

        //    return resp;
        //}
        //public AddressUpdateResp AddressUpdate(AddressUpdateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.AddressUpdate({1})", svcName, (oReq != null) ? oReq.Address.ID : 0);
        //    var resp = new AddressUpdateResp();

        //    try
        //    {
        //        int count = DAL.Registration.AddressUpdate(oReq.Addresses);
        //        Logger.DebugFormat("{0}.TCMRegService.AddressUpdate({1}):{2}", svcName, (oReq != null) ? oReq.Address.ID : 0, count);
        //    }
        //    catch (Exception ex)
        //    {
        //        resp.Code = "-1";
        //        //resp.Message = ex.Message;  // +"|" + 
        //        //resp.StackTrace = ex.StackTrace;
        //        //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
        //        resp.Message = errmsg;
        //        resp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.AddressUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Address.ID : 0, resp.Code); 
        //    }

        //    return resp;
        //}
        public AddressFindResp AddressFind(AddressFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressFind({1})", svcName, (oReq != null) ? oReq.AddressFind.Active.ToString2() : "NULL");
            var resp = new AddressFindResp();

            try { resp.IDs = DAL.Registration.AddressFind(oReq.AddressFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AddressFind({1}): {2}", svcName, (oReq != null) ? oReq.AddressFind.Active.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        public AddressGetResp AddressGet(AddressGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new AddressGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.Addresses = DAL.Registration.AddressGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AddressGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }

        public AddressGetResp AddressGet1(AddressFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AddressGet1({1})", svcName, (oReq != null) ? oReq.AddressFind.Active.ToString2() : "NULL");
            var resp = new AddressGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.Addresses = DAL.Registration.AddressGet1(oReq.AddressFind); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AddressGet1({1}): {2}", svcName, oReq.ToString2(), resp.Code);
            }

            return resp;
        }

        #endregion

        #region RegMdlGrpModel

        //public RegMdlGrpModelCreateResp RegMdlGrpModelCreate(RegMdlGrpModelCreateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.RegMdlGrpModelCreate({1})", svcName, (oReq != null) ? oReq.RegMdlGrpModel.Code : "NULL");

        //    var resp = new RegMdlGrpModelCreateResp();

        //    try { resp.ID = DAL.Registration.RegMdlGrpModelCreate(oReq.RegMdlGrpModel); }
        //    catch (Exception ex)
        //    {
        //        resp.Code = "-1";
        //        //resp.Message = ex.Message; //+ "|" + 
        //        //resp.StackTrace = ex.StackTrace;
        //        //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
        //        resp.Message = errmsg;
        //        resp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.RegMdlGrpModelCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.RegMdlGrpModel.Code : "NULL", resp.ID, resp.Code); 
        //    }

        //    return resp;
        //}
        //public RegMdlGrpModelUpdateResp RegMdlGrpModelUpdate(RegMdlGrpModelUpdateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.RegMdlGrpModelUpdate({1})", svcName, (oReq != null) ? oReq.RegMdlGrpModel.ID : 0);
        //    var resp = new RegMdlGrpModelUpdateResp();

        //    try
        //    {
        //        int count = DAL.Registration.RegMdlGrpModelUpdate(oReq.RegMdlGrpModel);
        //        Logger.DebugFormat("{0}.TCMRegService.RegMdlGrpModelUpdate({1}):{2}", svcName, (oReq != null) ? oReq.RegMdlGrpModel.ID : 0, count);
        //    }
        //    catch (Exception ex)
        //    {
        //        resp.Code = "-1";
        //        //resp.Message = ex.Message;  // +"|" + 
        //        //resp.StackTrace = ex.StackTrace;
        //        //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
        //        resp.Message = errmsg;
        //        resp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.RegMdlGrpModelUpdate({1}): {2}", svcName, (oReq != null) ? oReq.RegMdlGrpModel.ID : 0, resp.Code); 
        //    }

        //    return resp;
        //}
        public RegMdlGrpModelFindResp RegMdlGrpModelFind(RegMdlGrpModelFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegMdlGrpModelFind({1})", svcName, (oReq != null) ? oReq.RegMdlGrpModelFind.RegMdlGrpModel.RegID.ToString2() : "NULL");
            var resp = new RegMdlGrpModelFindResp();

            try { resp.IDs = DAL.Registration.RegMdlGrpModelFind(oReq.RegMdlGrpModelFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegMdlGrpModelFind({1}): {2}", svcName, (oReq != null) ? oReq.RegMdlGrpModelFind.RegMdlGrpModel.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        public RegMdlGrpModelSecFindResp RegMdlGrpModelSecFind(RegMdlGrpModelSecFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegMdlGrpModelFind({1})", svcName, (oReq != null) ? oReq.RegMdlGrpModelSecFind.RegMdlGrpModelSec.SecRegID.ToString2() : "NULL");
            var resp = new RegMdlGrpModelSecFindResp();

            try { resp.IDs = DAL.Registration.RegMdlGrpModelSecFind(oReq.RegMdlGrpModelSecFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegMdlGrpModelFind({1}): {2}", svcName, (oReq != null) ? oReq.RegMdlGrpModelSecFind.RegMdlGrpModelSec.SecRegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        public RegMdlGrpModelGetResp RegMdlGrpModelGet(RegMdlGrpModelGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegMdlGrpModelGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new RegMdlGrpModelGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.RegMdlGrpModels = DAL.Registration.RegMdlGrpModelGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegMdlGrpModelGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }
        public RegMdlGrpModelGetResp RegMdlGrpModelGet1(RegMdlGrpModelFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegMdlGrpModelGet1({1})", svcName, (oReq != null) ? oReq.RegMdlGrpModelFind.RegMdlGrpModel.RegID.ToString2() : "NULL");
            var resp = new RegMdlGrpModelGetResp();

            try { resp.RegMdlGrpModels = DAL.Registration.RegMdlGrpModelGet1(oReq.RegMdlGrpModelFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegMdlGrpModelGet1({1}): {2}", svcName, (oReq != null) ? oReq.RegMdlGrpModelFind.RegMdlGrpModel.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        public RegMdlGrpModelSecGetResp RegMdlGrpModelSecGet(RegMdlGrpModelSecGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegMdlGrpModelSecGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new RegMdlGrpModelSecGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.RegMdlGrpModels = DAL.Registration.RegMdlGrpModelSecGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegMdlGrpModelGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }

        #endregion

        #region RegPgmBdlPkgComponent

        //public RegPgmBdlPkgCompCreateResp RegPgmBdlPkgCompCreate(RegPgmBdlPkgCompCreateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompCreate({1})", svcName, (oReq != null) ? oReq.RegPgmBdlPkgComp.Code : "NULL");

        //    var resp = new RegPgmBdlPkgCompCreateResp();

        //    try { resp.ID = DAL.Registration.RegPgmBdlPkgCompCreate(oReq.RegPgmBdlPkgComp); }
        //    catch (Exception ex)
        //    {
        //        resp.Code = "-1";
        //        //resp.Message = ex.Message; //+ "|" + 
        //        //resp.StackTrace = ex.StackTrace;
        //        //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
        //        resp.Message = errmsg;
        //        resp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.RegPgmBdlPkgComp.Code : "NULL", resp.ID, resp.Code); 
        //    }

        //    return resp;
        //}
        //public RegPgmBdlPkgCompUpdateResp RegPgmBdlPkgCompUpdate(RegPgmBdlPkgCompUpdateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompUpdate({1})", svcName, (oReq != null) ? oReq.RegPgmBdlPkgComp.ID : 0);
        //    var resp = new RegPgmBdlPkgCompUpdateResp();

        //    try
        //    {
        //        int count = DAL.Registration.RegPgmBdlPkgCompUpdate(oReq.RegPgmBdlPkgComp);
        //        Logger.DebugFormat("{0}.TCMRegService.RegPgmBdlPkgCompUpdate({1}):{2}", svcName, (oReq != null) ? oReq.RegPgmBdlPkgComp.ID : 0, count);
        //    }
        //    catch (Exception ex)
        //    {
        //        resp.Code = "-1";
        //        //resp.Message = ex.Message;  // +"|" + 
        //        //resp.StackTrace = ex.StackTrace;
        //        //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
        //        resp.Message = errmsg;
        //        resp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompUpdate({1}): {2}", svcName, (oReq != null) ? oReq.RegPgmBdlPkgComp.ID : 0, resp.Code); 
        //    }

        //    return resp;
        //}
        public RegPgmBdlPkgCompFindResp RegPgmBdlPkgCompFind(RegPgmBdlPkgCompFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompFind({1})", svcName, (oReq != null) ? oReq.RegPgmBdlPkgCompFind.RegPgmBdlPkgComp.RegID.ToString2() : "NULL");
            var resp = new RegPgmBdlPkgCompFindResp();

            try { resp.IDs = DAL.Registration.RegPgmBdlPkgCompFind(oReq.RegPgmBdlPkgCompFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompFind({1}): {2}", svcName, (oReq != null) ? oReq.RegPgmBdlPkgCompFind.RegPgmBdlPkgComp.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }

        public RegPgmBdlPkgCompSecFindResp RegPgmBdlPkgCompSecFind(RegPgmBdlPkgCompSecFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompSecFindReq({1})", svcName, (oReq != null) ? oReq.RegPgmBdlPkgCompSecFind.RegPgmBdlPkgCompSec.RegID.ToString2() : "NULL");
            var resp = new RegPgmBdlPkgCompSecFindResp();

            try { resp.IDs = DAL.Registration.RegPgmBdlPkgCompSecFind(oReq.RegPgmBdlPkgCompSecFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompSecFindReq({1}): {2}", svcName, (oReq != null) ? oReq.RegPgmBdlPkgCompSecFind.RegPgmBdlPkgCompSec.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }

        //Added by VLT on 11 Apr 2013
        public RegPgmBdlPkgCompFindResp RegPgmBdlPkgComponentFind(RegPgmBdlPkgCompFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompFind({1})", svcName, (oReq != null) ? oReq.RegPgmBdlPkgCompFind.RegPgmBdlPkgComp.RegID.ToString2() : "NULL");
            var resp = new RegPgmBdlPkgCompFindResp();

            try { resp.IDs = DAL.Registration.RegPgmBdlPkgComponentFind(oReq.RegPgmBdlPkgCompFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompFind({1}): {2}", svcName, (oReq != null) ? oReq.RegPgmBdlPkgCompFind.RegPgmBdlPkgComp.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        //Added by VLT on 11 Apr 2013
        public RegPgmBdlPkgCompGetResp RegPgmBdlPkgCompGet(RegPgmBdlPkgCompGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new RegPgmBdlPkgCompGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.RegPgmBdlPkgComps = DAL.Registration.RegPgmBdlPkgCompGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }
        public RegPgmBdlPkgCompGetResp RegPgmBdlPkgCompGet1(RegPgmBdlPkgCompFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompGet1({1})", svcName, (oReq != null) ? oReq.RegPgmBdlPkgCompFind.RegPgmBdlPkgComp.RegID.ToString2() : "NULL");
            var resp = new RegPgmBdlPkgCompGetResp();

            try { resp.RegPgmBdlPkgComps = DAL.Registration.RegPgmBdlPkgCompGet1(oReq.RegPgmBdlPkgCompFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompGet1({1}): {2}", svcName, (oReq != null) ? oReq.RegPgmBdlPkgCompFind.RegPgmBdlPkgComp.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        public RegPgmBdlPkgCompSecGetResp RegPgmBdlPkgCompSecGet(RegPgmBdlPkgCompSecGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompSecGetReq({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new RegPgmBdlPkgCompSecGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.RegPgmBdlPkgCompsSec = DAL.Registration.RegPgmBdlPkgCompSecGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompSecGetReq({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }
        //Begin Added by VLT on 11 Apr 2013
        public RegPgmBdlPkgComponentGetResp RegPgmBdlPkgComponentGet(RegPgmBdlPkgCompGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegPgmBdlPkgCompGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new RegPgmBdlPkgComponentGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.RegPgmBdlPkgComps = DAL.Registration.RegPgmBdlPkgComponentGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegPgmBdlPkgCompGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }
        //End Added by VLT on 11 Apr 2013
        #endregion

        #region RegSuppLine

        //public RegSuppLineCreateResp RegSuppLineCreate(RegSuppLineCreateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.RegSuppLineCreate({1})", svcName, (oReq != null) ? oReq.RegSuppLine.Code : "NULL");

        //    var resp = new RegSuppLineCreateResp();

        //    try { resp.ID = DAL.Registration.RegSuppLineCreate(oReq.RegSuppLine); }
        //    catch (Exception ex)
        //    {
        //        resp.Code = "-1";
        //        //resp.Message = ex.Message; //+ "|" + 
        //        //resp.StackTrace = ex.StackTrace;
        //        //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
        //        resp.Message = errmsg;
        //        resp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.RegSuppLineCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.RegSuppLine.Code : "NULL", resp.ID, resp.Code); 
        //    }

        //    return resp;
        //}
        //public RegSuppLineUpdateResp RegSuppLineUpdate(RegSuppLineUpdateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.RegSuppLineUpdate({1})", svcName, (oReq != null) ? oReq.RegSuppLine.ID : 0);
        //    var resp = new RegSuppLineUpdateResp();

        //    try
        //    {
        //        int count = DAL.Registration.RegSuppLineUpdate(oReq.RegSuppLine);
        //        Logger.DebugFormat("{0}.TCMRegService.RegSuppLineUpdate({1}):{2}", svcName, (oReq != null) ? oReq.RegSuppLine.ID : 0, count);
        //    }
        //    catch (Exception ex)
        //    {
        //        resp.Code = "-1";
        //        //resp.Message = ex.Message;  // +"|" + 
        //        //resp.StackTrace = ex.StackTrace;
        //        //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
        //        resp.Message = errmsg;
        //        resp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.RegSuppLineUpdate({1}): {2}", svcName, (oReq != null) ? oReq.RegSuppLine.ID : 0, resp.Code); 
        //    }

        //    return resp;
        //}
        public RegSuppLineFindResp RegSuppLineFind(RegSuppLineFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegSuppLineFind({1})", svcName, (oReq != null) ? oReq.RegSuppLineFind.RegSuppLine.RegID.ToString2() : "NULL");
            var resp = new RegSuppLineFindResp();

            try { resp.IDs = DAL.Registration.RegSuppLineFind(oReq.RegSuppLineFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegSuppLineFind({1}): {2}", svcName, (oReq != null) ? oReq.RegSuppLineFind.RegSuppLine.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        public RegSuppLineGetResp RegSuppLineGet(RegSuppLineGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegSuppLineGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new RegSuppLineGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.RegSuppLines = DAL.Registration.RegSuppLineGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegSuppLineGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }
        public RegSuppLineGetResp RegSuppLineGet1(RegSuppLineFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegSuppLineGet1({1})", svcName, (oReq != null) ? oReq.RegSuppLineFind.RegSuppLine.RegID.ToString2() : "NULL");
            var resp = new RegSuppLineGetResp();

            try { resp.RegSuppLines = DAL.Registration.RegSuppLineGet1(oReq.RegSuppLineFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegSuppLineFind({1}): {2}", svcName, (oReq != null) ? oReq.RegSuppLineFind.RegSuppLine.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        #endregion

        #region RegStatus

        public int RegStatusUpdate(int Regid, RegStatus regStatus)
        {
            return DAL.Registration.RegStatusUpdate(Regid, regStatus);
        }

        public RegStatusCreateResp RegStatusCreate(RegStatusCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegStatusCreate({1})", svcName, (oReq != null) ? oReq.RegStatuses.StatusID : 0);

            var resp = new RegStatusCreateResp();

            try
            {
                if (oReq.Registrations == null)
                    resp.ID = DAL.Registration.RegStatusCreate(oReq.RegStatuses);

                else
                    resp.ID = DAL.Registration.RegStatusCreate(oReq.RegStatuses, oReq.Registrations);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegStatusCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.RegStatuses.StatusID : 0, resp.ID, resp.Code);
            }

            return resp;
        }
        public RegStatusFindResp RegStatusFind(RegStatusFindReq oReq)
        {
            var id = (oReq.RegStatusFind.RegistrationIDs.Count() > 0) ? oReq.RegStatusFind.RegistrationIDs.ToString2() : "NULL";
            id = oReq.RegStatusFind.RegStatus != null ? oReq.RegStatusFind.RegStatus.RegID.ToString2() : "NULL";
            Logger.InfoFormat("Entering {0}.RegStatusFind({1})", svcName, (id != null) ? id : "NULL");
            var resp = new RegStatusFindResp();

            try { resp.IDs = DAL.Registration.RegStatusFind(oReq.RegStatusFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegStatusFind({1}): {2}", svcName, (id != null) ? id.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        public RegStatusGetResp RegStatusGet(RegStatusGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegStatusGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new RegStatusGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.RegStatuses = DAL.Registration.RegStatusGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegStatusGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }
        public RegStatusGetResp RegStatusGet1(RegStatusFindReq oReq)
        {
            var id = (oReq.RegStatusFind.RegistrationIDs.Count() > 0) ? oReq.RegStatusFind.RegistrationIDs.ToString2() : "NULL";
            id = oReq.RegStatusFind.RegStatus != null ? oReq.RegStatusFind.RegStatus.RegID.ToString2() : "NULL";
            Logger.InfoFormat("Entering {0}.RegStatusFind({1})", svcName, (id != null) ? id : "NULL");
            var resp = new RegStatusGetResp();

            try { resp.RegStatuses = DAL.Registration.RegStatusGet1(oReq.RegStatusFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegStatusGet1({1}): {2}", svcName, (id != null) ? id.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        public RegStatusSearchResp RegStatusSearch(RegStatusSearchReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegStatusSearch({1})", svcName, (oReq != null) ? oReq.RegID : 0);
            var resp = new RegStatusSearchResp();

            try
            {
                resp.RegStatusResult = DAL.Registration.RegStatusSearch(oReq.RegID);
                //resp = RegStatusSearch(oReq.RegID);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationSearch({1}): {2}", svcName, (oReq != null) ? oReq.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        public RegStatusLogGetResp RegStatusLogGet(RegStatusLogGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegStatusLogGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new RegStatusLogGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.RegStatusResult = DAL.Registration.RegStatusLogGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegStatusLogGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }

        #endregion

        #region Customer

        #region
        public Registration.DAL.RegistrationDetails GetRegistrationFullDetails(int regId)
        {
            Logger.InfoFormat("Entering {0}.GetRegistrationFullDetails({1})", svcName, regId);
            var resp = new Registration.DAL.RegistrationDetails();

            try
            {
                resp = DAL.Registration.GetRegistrationFullDetails(regId);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetRegistrationFullDetails({1})", svcName, regId);
            }

            return resp;
        }
        #endregion

        //public CustomerCreateResp CustomerCreate(CustomerCreateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.CustomerCreate({1})", svcName, (oReq != null) ? oReq.Customer.Code : "NULL");

        //    var resp = new CustomerCreateResp();

        //    try { resp.ID = DAL.Registration.CustomerCreate(oReq.Customer); }
        //    catch (Exception ex)
        //    {
        //        resp.Code = "-1";
        //        //resp.Message = ex.Message; //+ "|" + 
        //        //resp.StackTrace = ex.StackTrace;
        //        //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
        //        resp.Message = errmsg;
        //        resp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.CustomerCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.Customer.Code : "NULL", resp.ID, resp.Code); 
        //    }

        //    return resp;
        //}
        //public CustomerUpdateResp CustomerUpdate(CustomerUpdateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.CustomerUpdate({1})", svcName, (oReq != null) ? oReq.Customer.ID : 0);
        //    var resp = new CustomerUpdateResp();

        //    try
        //    {
        //        int count = DAL.Registration.CustomerUpdate(oReq.Customer);
        //        Logger.DebugFormat("{0}.TCMRegService.CustomerUpdate({1}):{2}", svcName, (oReq != null) ? oReq.Customer.ID : 0, count);
        //    }
        //    catch (Exception ex)
        //    {
        //        resp.Code = "-1";
        //        //resp.Message = ex.Message;  // +"|" + 
        //        //resp.StackTrace = ex.StackTrace;
        //        //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
        //        resp.Message = errmsg;
        //        resp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.CustomerUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Customer.ID : 0, resp.Code); 
        //    }

        //    return resp;
        //}
        public CustomerFindResp CustomerFind(CustomerFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerFind({1})", svcName, (oReq != null) ? oReq.CustomerFind.Customer.FullName : "NULL");
            var resp = new CustomerFindResp();

            try { resp.IDs = DAL.Registration.CustomerFind(oReq.CustomerFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CustomerFind({1}): {2}", svcName, (oReq != null) ? oReq.CustomerFind.Customer.FullName : "NULL", resp.Code);
            }

            return resp;
        }
        public CustomerGetResp CustomerGet(CustomerGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new CustomerGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.Customers = DAL.Registration.CustomerGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CustomerGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }
        public CustomerGetResp CustomerGet1(CustomerFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerGet1({1})", svcName, (oReq != null) ? oReq.CustomerFind.Customer.FullName : "NULL");
            var resp = new CustomerGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.Customers = DAL.Registration.CustomerGet1(oReq.CustomerFind); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CustomerFind({1}): {2}", svcName, (oReq != null) ? oReq.CustomerFind.Customer.FullName : "NULL", resp.Code);
            }

            return resp;
        }

        #endregion

        #region Biometric

        public BiometricCreateResp BiometricCreate(BiometricCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BiometricCreate({1})", svcName, (oReq != null) ? oReq.Biometric.Name : "NULL");

            var resp = new BiometricCreateResp();

            try { resp.ID = DAL.Registration.BiometricCreate(oReq.Biometric); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BiometricCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.Biometric.Name : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }
        public BiometricUpdateResp BiometricUpdate(BiometricUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BiometricUpdate({1})", svcName, (oReq != null) ? oReq.Biometric.ID : 0);
            var resp = new BiometricUpdateResp();

            try
            {
                int count = DAL.Registration.BiometricUpdate(oReq.Biometric);
                Logger.DebugFormat("{0}.TCMRegService.BiometricUpdate({1}):{2}", svcName, (oReq != null) ? oReq.Biometric.ID : 0, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;  // +"|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BiometricUpdate({1}): {2}", svcName, (oReq != null) ? oReq.Biometric.ID : 0, resp.Code);
            }

            return resp;
        }
        public BiometricFindResp BiometricFind(BiometricFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BiometricFind({1})", svcName, (oReq != null) ? oReq.BiometricFind.Biometric.RegID.ToString2() : "NULL");
            var resp = new BiometricFindResp();

            try { resp.IDs = DAL.Registration.BiometricFind(oReq.BiometricFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BiometricFind({1}): {2}", svcName, (oReq != null) ? oReq.BiometricFind.Biometric.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        public BiometricGetResp BiometricGet(BiometricGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BiometricGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new BiometricGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.Biometrics = DAL.Registration.BiometricGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BiometricGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }

        #endregion

        #region RegAccount

        public RegAccountGetResp RegAccountGet(RegAccountGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegAccountGet({1})", svcName, (oReq != null && oReq.RegIDs.Count > 0) ? oReq.RegIDs[0] : 0);
            var resp = new RegAccountGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else
                {
                    var account = DAL.Registration.RegAccountGetByRegID(oReq.RegIDs[0]);
                    resp.RegAccounts.Add(account);
                }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegAccountGet({1}): {2}", svcName, (oReq != null && oReq.RegIDs.Count > 0) ? oReq.RegIDs[0] : 0, resp.Code);
            }

            return resp;
        }

        #endregion


        #region Payment Mode Payment Details

        public PaymentModePaymentDetailsCreateResp PaymentModePaymentDetailsCreate(PaymentModePaymentDetailsCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModeCreate({1})", svcName, oReq.PaymentModePaymentDetails.Code);
            PaymentModePaymentDetailsCreateResp oRp = new PaymentModePaymentDetailsCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.PaymentModePaymentDetailsCreate(oReq.PaymentModePaymentDetails);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PaymentModeCreate({1}): {2}|{3}", svcName, oReq.PaymentModePaymentDetails.Code, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public PaymentModePaymentDetailsUpdateResp PaymentModePaymentDetailsUpdate(PaymentModePaymentDetailsUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModePaymentDetailsUpdate({1})", svcName, oReq.PaymentModePaymentDetails.ID);
            PaymentModePaymentDetailsUpdateResp oRp = new PaymentModePaymentDetailsUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.PaymentModePaymentDetailsUpdate(oReq.PaymentModePaymentDetails);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Payment Mode Payment Details update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PaymentModePaymentDetailsUpdate({1}): {2}", svcName, oReq.PaymentModePaymentDetails.ID, oRp.Code);
            }
            return oRp;
        }
        public PaymentModePaymentDetailsGetResp PaymentModePaymentDetailsGet(PaymentModePaymentDetailsGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}PaymentModeGet({1})", svcName, oReq.IDs.Count);
            PaymentModePaymentDetailsGetResp oRp = new PaymentModePaymentDetailsGetResp();
            try
            {
                oRp.PaymentModePaymentDetails = OnlineRegAdmin.PaymentModePaymentDetailsGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PaymentModePaymentDetailsGet({1}): {2}", svcName, oReq.IDs.Count, oRp.Code);
            }
            return oRp;
        }
        public PaymentModePaymentDetailsFindResp PaymentModePaymentDetailsFind(PaymentModePaymentDetailsFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PaymentModePaymentDetailsFind({1})", svcName, oReq.PaymentModePaymentDetailsFind);
            PaymentModePaymentDetailsFindResp oRp = new PaymentModePaymentDetailsFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.PaymentModePaymentDetailsFind(oReq.PaymentModePaymentDetailsFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PaymentModePaymentDetailsFind({1}): {2}", svcName, oReq.PaymentModePaymentDetailsFind.PaymentModePaymentDetails.ID, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region Card Type Payment Details

        public CardTypePaymentDetailsCreateResp CardTypePaymentDetailsCreate(CardTypePaymentDetailsCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypePaymentDetailsCreate({1})", svcName, oReq.CardTypePaymentDetails.Name);
            CardTypePaymentDetailsCreateResp oRp = new CardTypePaymentDetailsCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.CardTypePaymentDetailsCreate(oReq.CardTypePaymentDetails);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CardTypePaymentDetailsCreate({1}): {2}|{3}", svcName, oReq.CardTypePaymentDetails.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public CardTypePaymentDetailsUpdateResp CardTypePaymentDetailsUpdate(CardTypePaymentDetailsUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypePaymentDetailsUpdate({1})", svcName, oReq.CardTypePaymentDetails.Name);
            CardTypePaymentDetailsUpdateResp oRp = new CardTypePaymentDetailsUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.CardTypePaymentDetailsUpdate(oReq.CardTypePaymentDetails);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Card Type Payment Details update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CardTypePaymentDetailsUpdate({1}): {2}", svcName, oReq.CardTypePaymentDetails.Name, oRp.Code);
            }
            return oRp;
        }
        public CardTypePaymentDetailsGetResp CardTypePaymentDetailsGet(CardTypePaymentDetailsGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}CardTypePaymentDetailsGet({1})", svcName, oReq.IDs.Count());
            CardTypePaymentDetailsGetResp oRp = new CardTypePaymentDetailsGetResp();
            try
            {
                oRp.CardTypePaymentDetails = OnlineRegAdmin.CardTypePaymentDetailsGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CardTypePaymentDetailsGet({1}): {2}", svcName, (oReq.IDs.Count() > 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public CardTypePaymentDetailsGetResp CardTypePaymentList()
        {
            Logger.InfoFormat("Entering {0}CardTypePaymentList({1})", svcName, "");
            CardTypePaymentDetailsGetResp oRp = new CardTypePaymentDetailsGetResp();
            try
            {
                oRp.CardTypePaymentDetails = OnlineRegAdmin.CardTypePaymentList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CardTypePaymentList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }


        public CardTypePaymentDetailsFindResp CardTypePaymentDetailsFind(CardTypePaymentDetailsFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CardTypePaymentDetailsFind({1})", svcName, oReq.CardTypePaymentDetailsFind);
            CardTypePaymentDetailsFindResp oRp = new CardTypePaymentDetailsFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.CardTypePaymentDetailsFind(oReq.CardTypePaymentDetailsFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CardTypePaymentDetailsFind({1}): {2}", svcName, oReq.CardTypePaymentDetailsFind.CardTypePaymentDetails.Name, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region Customer Info
        public CustomerInfoCreateResp CustomerInfoCreate(CustomerInfoCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerInfoCreate({1})", svcName, oReq.CustomerInfo.ID);
            CustomerInfoCreateResp oRp = new CustomerInfoCreateResp();
            try
            {
                oRp.ID = DAL.Registration.CustomerInfoCreate(oReq.CustomerInfo);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CustomerInfoCreate({1}): {2}|{3}", svcName, oReq.CustomerInfo.ID, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public CustomerInfoUpdateResp CustomerInfoUpdate(CustomerInfoUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CustomerInfoUpdate({1})", svcName, oReq.CustomerInfo.ID);
            CustomerInfoUpdateResp oRp = new CustomerInfoUpdateResp();
            int value = 0;
            try
            {
                value = DAL.Registration.CustomerInfoUpdate(oReq.CustomerInfo);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Customer Info update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CustomerInfoUpdate({1}): {2}", svcName, oReq.CustomerInfo.ID, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region Added by Patanjali on 07-4-2013 to get payment status

        public int PaymentStatusGet(int RegID)
        {
            int returnstatus = -1;
            Logger.InfoFormat("Entering {0}.PaymentStatusGet({1})", svcName, (RegID != null) ? RegID : 0);

            try
            {
                if (RegID == null)
                {
                    return returnstatus;
                }
                else
                {
                    returnstatus = DAL.Registration.PaymentStatusGet(RegID);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PaymentStatusGet({1}): {2}", svcName, (RegID != null) ? RegID : 0, 0);
            }

            return returnstatus;
        }

        #endregion

        /*Chetan added for displaying the reson for failed transcation*/
        public KenanLogDetailsGetResp KenanLogDetailsGet(KenanLogDetailsGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.KenanLogDetailsGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new KenanLogDetailsGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.KenanaLogDetails = DAL.Registration.KenanLogDetailsGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.KenanLogDetailsGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }
        public KenanLogDetailsGetResp KenanLogHistoryDetailsGet(KenanLogHistoryDetailsGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.KenanLogHistoryDetailsGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new KenanLogDetailsGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.KenanaLogDetails = DAL.Registration.KenanLogHistoryDetailsGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.KenanLogHistoryDetailsGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }

        #endregion

        //Spend Limit commented by Ravi as per new flow on june 15 2013
        #region "Spend Limit"

        public int SaveSpendLimit(LnkRegSpendLimitReq oReq)
        {
            Logger.InfoFormat("Entering {0}.SaveSpendLimit({1})", svcName,oReq.lnkRegSpendLimitDetails.RegId);

            int outValue;

            try
            {

                outValue = DAL.Registration.SaveSpendLimit(oReq.lnkRegSpendLimitDetails);

            }
            catch (Exception ex)
            {
                outValue = -1;

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveSpendLimit({1})", svcName,oReq.lnkRegSpendLimitDetails.RegId);
            }

            return outValue;
        }

        public LnkRegSpendLimitReq SpendLimitGet(int regId)
        {
            Logger.InfoFormat("Entering {0}.SpendLimitGet({1})", svcName, regId);
            var resp = new LnkRegSpendLimitReq();
            try
            {
                resp.lnkRegSpendLimitDetails = DAL.Registration.SpendLimitGet(regId);
            }
            catch (Exception ex)
            {
                resp = null;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SpendLimitGet({1})", svcName, regId);
            }

            return resp;
        }
        #endregion

        #region "Extra Ten"

        public int SaveExtraTen(ExtraTenReq oReq)
        {
            Logger.InfoFormat("Entering {0}.SaveExtraTen({1})", svcName);

            int outValue;

            try
            {
                outValue = DAL.Registration.SaveExtraTen(oReq.ExtraTenDetails);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveExtraTen({1}):{2}|{3}", svcName);
            }

            return outValue;
        }
        // Extra Ten Add Edit and Delete for external users 
        public int SaveExtraTenAddEditDetails(ExtraTenAddEditReq oReq)
        {
            Logger.InfoFormat("Entering {0}.SaveExtraTenAddEditDetails({1})", svcName);
            int outValue;
            try
            {
                outValue = DAL.Registration.SaveExtraTenAddEditDetails(oReq.ExtraTenAddEditDetails);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveExtraTenAddEditDetails({1}):{2}|{3}", svcName);
            }
            return outValue;
        }

        public int UpdateExtraTen(ExtraTenReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UpdateExtraTen({1})", svcName);

            int outValue;

            try
            {
                outValue = DAL.Registration.UpdateExtraTen(oReq.ExtraTenDetails);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveSpendLimit({1}):{2}|{3}", svcName);
            }

            return outValue;
        }

        public int DeleteExtraTen(int extraTenId)
        {
            Logger.InfoFormat("Entering {0}.DeleteExtraTen({1})", svcName);

            int outValue;

            try
            {
                outValue = DAL.Registration.DeleteExtraTen(extraTenId);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.DeleteExtraTen({1}):{2}|{3}", svcName);
            }

            return outValue;
        }

        public int DeleteExtraTenDetails(int regId)
        {
            Logger.InfoFormat("Entering {0}.DeleteExtraTenDetails({1})", svcName);

            int outValue = 0;

            try
            {
                DAL.Registration.DeleteExtraTenDetails(regId);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.DeleteExtraTenDetails({1}):{2}|{3}", svcName);
            }

            return outValue;


        }
        public ExtraTenGetResp GetExtraTenDetails(int regId)
        {
            // Logger.InfoFormat("Entering {0}.SaveExtraTenLogs({1})", svcName);
            ExtraTenGetResp response = new ExtraTenGetResp();
            try
            {
                response.ExtraTenDetails = DAL.Registration.GetExtraTenDetails(regId);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveExtraTenLogs", svcName);
            }

            return response;

        }
        public int SaveExtraTenLogs(ExtraTenLogReq oReq)
        {
            Logger.InfoFormat("Entering {0}.SaveExtraTenLogs({1})", svcName);

            //  var resp = new LnkRegSpendLimitReq();
            int outValue;

            try
            {
                outValue = DAL.Registration.SaveExtraTenLogs(oReq.ExtraTenConfirmationLog);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveExtraTenLogs({1}):{2}|{3}", svcName);
            }

            return outValue;
        }



        #endregion
        public RegistrationCancelReasonResp RegistrationCancelCreate(RegistrationCancelReasonReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCancelCreate({1})", svcName, (oReq != null) ? oReq.RegistrationCancelReason.CancelReason : "NULL");

            var resp = new RegistrationCancelReasonResp();

            try { resp.ID = DAL.Registration.RegistrationCancelCreate(oReq.RegistrationCancelReason); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                string errmsg = "", stacktrace = "";
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationCancelCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.RegistrationCancelReason.CancelReason : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }

        public string CancelReasonGet(int RegID)
        {
            string returnstatus = string.Empty;
            Logger.InfoFormat("Entering {0}.CancelReasonGet({1})", svcName, RegID);

            try
            {
                returnstatus = DAL.Registration.CancelReasonGet(RegID);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CancelReasonGet({1}): {2}", svcName, RegID, returnstatus);
            }

            return returnstatus;
        }

        #region LnkRegDetails

        public int SaveLnkRegistrationDetails(LnkRegDetailsReq objLnkRegDetailsReq)
        {
            //Logger.InfoFormat("Entering {0}.SaveLnkRegistrationDetails({1})", svcName);

            int outValue;

            try
            {
                outValue = DAL.Registration.SaveLnkRegistrationDetails(objLnkRegDetailsReq.LnkDetails);
            }
            catch (Exception ex)
            {
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveLnkRegistrationDetails", svcName);
            }

            return outValue;
        }

        //Added By Himansu for Sim Replacement

        public int UpdateLnkRegistrationDetails(LnkRegDetailsReq objLnkRegDetailsReq)
        {
            Logger.InfoFormat("Entering {0}.UpdateLnkRegistrationDetails", svcName);

            int outValue;

            try
            {
                outValue = DAL.Registration.UpdateLnkRegistrationDetails(objLnkRegDetailsReq.LnkDetails);
            }
            catch (Exception ex)
            {
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UpdateLnkRegistrationDetails", svcName);
            }

            return outValue;
        }
        public int SaveRegSuppLines(RegSupplinesMapReq objRegSupplinesMapReq)
        {
            Logger.InfoFormat("Entering {0}.SaveRegSuppLines", svcName);

            int outValue;

            try
            {
                outValue = DAL.Registration.SaveRegSuppLines(objRegSupplinesMapReq.RegSupplinesMap);
            }
            catch (Exception ex)
            {
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveRegSuppLines", svcName);
            }
            return outValue;
        }

        public LnkRegDetailsReq GetLnkRegistrationDetails(int regId)
        {
            Logger.InfoFormat("Entering {0}.SaveLnkRegistrationDetails", svcName);
            LnkRegDetailsReq objLnkRegDetailsReq = new LnkRegDetailsReq();

            DAL.Models.LnkRegDetails outValue = new DAL.Models.LnkRegDetails();

            try
            {
                outValue = DAL.Registration.GetLnkRegistrationDetails(regId);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveLnkRegistrationDetails", svcName);
            }
            objLnkRegDetailsReq.LnkDetails = outValue;
            return objLnkRegDetailsReq;
        }

        public DAL.Models.LnkRegDetails LnkRegistrationGetByRegId(int regID)
        {
            //Logger.InfoFormat("Entering {0}.LnkRegistrationGet({1})", svcName);

            DAL.Models.LnkRegDetails outValue;

            try
            {
                outValue = DAL.Registration.LnkRegistrationGetByRegId(regID);
            }
            catch (Exception ex)
            {
                outValue = new DAL.Models.LnkRegDetails();
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.LnkRegistrationGet", svcName);
            }

            return outValue;
        }



        #endregion

        public Dictionary<int, SmartGetExistingPackageDetailsResp> ExistingPackagesDetails(List<SmartGetExistingPackageDetailsReq> oReq)
        {
            // Added by Pradeep. This method is intended to replace 'ExistingPackageDetails' with multiple inputs
            // Modify this accordingly after updating sp in db
            var result = new Dictionary<int, SmartGetExistingPackageDetailsResp>();

            foreach (var req in oReq)
            {
                var SmartGetPackageDetailsResp = new SmartGetExistingPackageDetailsResp();
                Online.Registration.DAL.Registration.ExistingPackagePlanType objIMPOSDetail = new Online.Registration.DAL.Registration.ExistingPackagePlanType();
                objIMPOSDetail = DAL.Registration.ExistingPackagePlanTypes(req.IDs);
                SmartGetPackageDetailsResp.ExistingPackageDetails = objIMPOSDetail;
                if (!result.ContainsKey(req.IDs))
                    result.Add(req.IDs, SmartGetPackageDetailsResp);
            }
            return result;
        }

        public SmartGetExistingPackageDetailsResp ExistingPackageDetails(SmartGetExistingPackageDetailsReq oReq)
        {
            var SmartGetPackageDetailsResp = new SmartGetExistingPackageDetailsResp();
            Online.Registration.DAL.Registration.ExistingPackagePlanType objIMPOSDetail = new Online.Registration.DAL.Registration.ExistingPackagePlanType();

            objIMPOSDetail = DAL.Registration.ExistingPackagePlanTypes(oReq.IDs);
            //foreach (Online.Registration.Smart.DAL.Registration.ExistingPackagePlanType o in objIMPOSDetails)
            //{
            //    objIMPOSDetail.PlanType = o.PlanType;

            //}
            SmartGetPackageDetailsResp.ExistingPackageDetails = objIMPOSDetail;
            return SmartGetPackageDetailsResp;
        }

        public long GetLnkPgmBdlPkgCompIdByKenanCode(string kenanCode, string planType)
        {

            return DAL.Registration.GetLnkPgmBdlPkgCompIdByKenanCode(kenanCode, planType);
        }

        public Dictionary<string, string> GetlnkpgmbdlpkgcompidForKenancomponentAll(int planId, List<string> kenancodes)
        {
            var request = new SmartGetExistingPackageDetailsReq()
            {
                IDs = planId
            };

            var SmartGetPackageDetailsResp = ExistingPackageDetails(request);


            Dictionary<string, string> compIds = new Dictionary<string, string>();

            for (int codeCounter = 0; codeCounter < kenancodes.Count; codeCounter++)
            {
                string compID = null;
                if (SmartGetPackageDetailsResp != null && SmartGetPackageDetailsResp.ExistingPackageDetails.PlanType != null)
                {
                    compID = DAL.Registration.GetlnkpgmbdlpkgcompidForKenancomponent(SmartGetPackageDetailsResp.ExistingPackageDetails.PlanType, planId, kenancodes[codeCounter]);
                }
                compIds.Add(kenancodes[codeCounter], compID);

            }

            return compIds;
        }
        public Dictionary<string, string> GetlnkpgmbdlpkgcompidForKenancomponentAll(string planType, int planId, List<string> kenancodes)
        {
            Dictionary<string, string> compIds = new Dictionary<string, string>();

            foreach (var kenancode in kenancodes)
            {
                compIds.Add(kenancode, DAL.Registration.GetlnkpgmbdlpkgcompidForKenancomponent(planType, planId, kenancode));
            }

            return compIds;
        }

        public string GetlnkpgmbdlpkgcompidForKenancomponent(string plan_type, int planId, string kenancode)
        {

            return DAL.Registration.GetlnkpgmbdlpkgcompidForKenancomponent(plan_type, planId, kenancode);
        }

        #region LnkRegDetails

        public bool getSupplineDetails(int regID)
        {
            //Logger.InfoFormat("Entering {0}.getSupplineDetails", svcName);

            bool outValue;

            try
            {
                outValue = DAL.Registration.getSupplineDetails(regID);
            }
            catch (Exception ex)
            {
                outValue = false;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.getSupplineDetails", svcName);
            }

            return outValue;
        }

        #endregion

        //Added By Nagaraju
        public RegistrationCreateResp RegistrationCreateNew(RegistrationCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCreateNew({1})", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL");

            var resp = new RegistrationCreateResp();

            try
            {
                resp.ID = DAL.Registration.RegistrationCreateNew(oReq.Registration, oReq.Customer, oReq.Addresses, oReq.RegStatus, oReq.RegWaiverComponents,oReq.isBREFail);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                Logger.Info("Exception while excuting the RegistrationCreateNew method:" + ex);
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationCreateNew({1}): {2}|{3}", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }

        public GetPackageDetailsResp SelectedPackageDetail(GetPackageDetailsReq oReq)
        {
            var SmartGetPackageDetailsResp = new GetPackageDetailsResp();
            List<Online.Registration.DAL.Registration.SelectedPackageDetail> objIMPOSDetails = null;
            Online.Registration.DAL.Registration.SelectedPackageDetail objIMPOSDetail = new Online.Registration.DAL.Registration.SelectedPackageDetail();

            objIMPOSDetail = DAL.Registration.SelectedPackageDetails(oReq.IDs);
            //foreach (Online.Registration.Smart.DAL.Registration.SelectedPackageDetail o in objIMPOSDetails)
            //{
            //    objIMPOSDetail.PackageId = o.PackageId;
            //    objIMPOSDetail.packageKenanId = o.packageKenanId;
            //    objIMPOSDetail.MasterPackageId = o.MasterPackageId;
            //    objIMPOSDetail.ExtraPackageId = o.ExtraPackageId;
            //}
            SmartGetPackageDetailsResp.PackageDetails = objIMPOSDetail;
            return SmartGetPackageDetailsResp;
        }

        public GetActiveContractsListResp GetActiveContractsList()
        {
            Logger.InfoFormat("Entering {0}.GetActiveContractsList()", svcName);
            GetActiveContractsListResp oRp = new GetActiveContractsListResp();

            try
            {
                List<DAL.Models.Contracts> value = null;
                value = Registration.DAL.Registration.GetActiveContractsList();
                oRp.Contracts = value;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetActiveContractsList()", svcName);
            }
            return oRp;

        }

        #region CRP



        public RegistrationCreateResp RegistrationCreate_CRP(RegistrationCreateReq_CRP oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCreate({1})", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL");

            var resp = new RegistrationCreateResp();

            try
            {
                resp.ID = DAL.Registration.RegistrationCreate_CRP(oReq.Registration, oReq.Customer, oReq.Addresses, oReq.RegPgmBdlPkgComps, oReq.RegMdlGrpModel,
                                                              oReq.RegStatus, oReq.RegSuppLines, oReq.RegSuppLineVASes, oReq.CustPhotos, oReq.Packages, oReq.CMSSID, oReq.RegSmartComponents, oReq.RegWaiverComponents,oReq.isBREFail);
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the RegistrationCreate_CRP method:" + ex);
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.Smart.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationCreate_CRP({1}): {2}|{3}", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }

        public bool isCRPregistration(int regid)
        {
            bool isCRPreg = false;
            Logger.InfoFormat("Entering {0}.isCRPregistration()", svcName);

            try
            {
                isCRPreg = DAL.Registration.isCRPregistration(regid);
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message, stacktrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.isCRPregistration()", svcName);
            }

            Logger.InfoFormat("Exiting {0}.isCRPregistration()", svcName);
            return isCRPreg;
        }

        #endregion

        #region MNP SUPPLEMENTARY LINES
        /// <summary>
        /// Registrations the get MNP supple.
        /// </summary>
        /// <param name="RegID">The reg ID.</param>
        /// <Author>Sutan Dan</Author>
        /// <returns>TypeOf(RegistrationMNPSupplementaryGetResp)</returns>
        public RegistrationMNPSupplementaryGetResp RegistrationGetMnpSupple(Int32 RegID)
        {
            Logger.InfoFormat("Entering {0}.RegistrationGetMnpSupple({1})", svcName, (RegID > 0) ? RegID : 0);
            var resp = new RegistrationMNPSupplementaryGetResp();

            try
            {
                if (RegID <= 0)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request cannot be less than 0!";
                }
                else { resp.Registrations = DAL.Registration.RegistrationGetMnpSupple(RegID); }
            }
            catch (Exception ex)
            {
                Logger.Info("Exception while excuting the RegistrationGetMnpSupple method:" + ex);
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationGetMnpSupple({1}): {2}", svcName, (RegID > 0) ? RegID : 0, resp.Code);
            }

            return resp;
        }
        #endregion MNP SUPPLEMENTARY LINES

        #region MISM

        public int UpdateSecondaryRequestSent(int regID)
        {
            Logger.InfoFormat("Entering UpdateSecondaryRequestSent");

            int outValue;
			RegistrationSec _req = new RegistrationSec();
			_req.ID = regID;
            try
            {
				outValue = DAL.Registration.UpdateSecondaryRequestSent(_req);
            }
            catch (Exception ex)
            {
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting UpdateSecondaryRequestSent");
            }

            return outValue;
        }

        #endregion

        public List<long> selectusedmobilenos()
        {
            Logger.InfoFormat("Entering {0}.selectusedmobilenos()", svcName);

            List<long> value = null;

            try
            {
                value = DAL.Registration.selectusedmobilenos();
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.selectusedmobilenos()", svcName);
            }

            return value;
        }

        public int Insertusedmobilenos(int regid, long mobileno, int active)
        {
            Logger.InfoFormat("Entering {0}.Insertusedmobilenos({1},{2},{3})", svcName, regid, mobileno, active);

            int value = 0;

            try
            {
                value = DAL.Registration.Insertusedmobilenos(regid, mobileno, active);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.Insertusedmobilenos({1},{2},{3})", svcName, regid, mobileno, active);
            }

            return value;
        }

        public List<Online.Registration.DAL.Models.FxDependChkComps> GetAllComponentsbyPlanId(int PlanID, string mandaotryComps,string linkType)
        {
            Logger.InfoFormat("Entering {0}.GetAllComponentsbyPlanId({1},{2},{3})", svcName, PlanID, mandaotryComps,linkType);

            List<Online.Registration.DAL.Models.FxDependChkComps> Comps = new List<Online.Registration.DAL.Models.FxDependChkComps>();
            try
            {
                Comps = DAL.Registration.GetAllComponentsbyPlanId(PlanID, mandaotryComps, linkType);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetAllComponentsbyPlanId({1},{2},{3})", svcName, PlanID, mandaotryComps,linkType);
            }

            return Comps;
        }

        public List<Online.Registration.DAL.Models.FxDependChkComps> GetAllComponentsbySecondaryPlanId(int PlanID, string mandaotryComps)
        {
            Logger.InfoFormat("Entering {0}.GetAllComponentsbyPlanId({1},{2})", svcName, PlanID, mandaotryComps);

            List<Online.Registration.DAL.Models.FxDependChkComps> Comps = new List<Online.Registration.DAL.Models.FxDependChkComps>();
            try
            {
                Comps = DAL.Registration.GetAllComponentsbySecondaryPlanId(PlanID, mandaotryComps);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetAllComponentsbyPlanId({1},{2})", svcName, PlanID, mandaotryComps);
            }

            return Comps;
        }


        public int BlackBerryDiscount(int deviceID, int planID)
        {
            Logger.InfoFormat("Entering {0}.BlackBerryDiscount({1},{2})", svcName, deviceID, planID);

            int discountPrice = 0;
            try
            {
                discountPrice = DAL.Registration.BlackBerryDiscount(deviceID, planID);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BlackBerryDiscount({1},{2})", svcName, deviceID, planID);
            }

            return discountPrice;
        }

        //Method added by ravi to check whether the customer is whitelist or not
        public int CheckWhiteListCustomer(string icNo)
        {
            //Logger.InfoFormat("Entering {0}.CheckWhiteListCustomer({1})", svcName, icNo);

            int whiteListCustId = 0;
            try
            {
                whiteListCustId = DAL.Registration.CheckWhiteListCustomer(icNo);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CheckWhiteListCustomer({1})", svcName, icNo);
            }

            return whiteListCustId;
        }


        public List<Online.Registration.DAL.Models.lnkofferuomarticleprice> GetPromotionalOffers(int whiteListId)
        {
            Logger.InfoFormat("Entering {0}.GetPromotionalOffers({1},{2})", svcName, whiteListId);

            List<Online.Registration.DAL.Models.lnkofferuomarticleprice> objOffers = new List<Online.Registration.DAL.Models.lnkofferuomarticleprice>();
            try
            {
                objOffers = DAL.Registration.GetPromotionalOffers(whiteListId);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetPromotionalOffers({1},{2})", svcName, whiteListId);
            }

            return objOffers;
        }
        public Online.Registration.DAL.Models.PlanDetailsandContract GetPlanDetailsbyOfferId(int offerId)
        {
            Logger.InfoFormat("Entering {0}.GetPlanDetailsbyOfferId({1})", svcName, offerId);

            Online.Registration.DAL.Models.PlanDetailsandContract objPlanDetails = new Online.Registration.DAL.Models.PlanDetailsandContract();
            try
            {
                objPlanDetails = DAL.Registration.GetPlanDetailsbyOfferId(offerId);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetPlanDetailsbyOfferId({1})", svcName, offerId);
            }

            return objPlanDetails;
        }
        //end of new method

        public int GetBBAdvancePrice(string uomCode, int planID, int modelId)
        {
            Logger.InfoFormat("Entering {0}.GetBBAdvancePrice({1},{2})", svcName, uomCode, planID);

            int advancePrice = 0;
            try
            {
                advancePrice = DAL.Registration.GetBBAdvancePrice(planID, modelId, uomCode);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetBBAdvancePrice({1},{2})", svcName, uomCode, planID);
            }

            return advancePrice;
        }

        public List<string> GetCompDescByRegid(int regID)
        {
            Logger.InfoFormat("Entering {0}.GetCompDescByRegid({1})", svcName, regID);

            var resp = new List<string>();

            try
            {
                resp = DAL.Registration.GetCompDescByRegid(regID);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetCompDescByRegid({1})", svcName, regID);
            }
            return resp;
        }

        public List<string> GetDataPlanComponentsForMISM()
        {
            ///Logger.InfoFormat("Entering {0}.GetDataPlanComponentsForMISM({1})", svcName);

            var resp = new List<string>();

            try
            {
                resp = DAL.Registration.DataPlanComponents();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetDataPlanComponentsForMISM", svcName);
            }
            return resp;
        }

        //Added by Kota Durga Prasad to check if IMPOS file exists at the store location
        #region
        //[DllImport("advapi32.dll", SetLastError = true)]
        //private static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        //[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        //private static extern bool CloseHandle(IntPtr handle);

        //[DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        //public extern static bool DuplicateToken(IntPtr existingTokenHandle,
        //int SECURITY_IMPERSONATION_LEVEL, ref IntPtr duplicateTokenHandle);
        // logon types
        const int LOGON32_LOGON_INTERACTIVE = 2;
        const int LOGON32_LOGON_NETWORK = 3;
        const int LOGON32_LOGON_NEW_CREDENTIALS = 9;

        // logon providers
        const int LOGON32_PROVIDER_DEFAULT = 0;
        const int LOGON32_PROVIDER_WINNT50 = 3;
        const int LOGON32_PROVIDER_WINNT40 = 2;
        const int LOGON32_PROVIDER_WINNT35 = 1;
        public bool IsIMPOSFileExists(int regId, int regType, out string status)
        {
            IntPtr token = IntPtr.Zero;
            status = "";
            bool isFileExists = false;
            bool isSuccess = false;

            string posFilePath = string.Empty;
            string[] posFolderPath;
            string trn_type = string.Empty;
            string simType = string.Empty;
            string orgID = string.Empty;

            string posKey = string.Empty;
            string posNetworkPath = string.Empty;
            string posUserName = string.Empty;
            string posPassword = string.Empty;
            string posDomain = string.Empty;
            string folderName = string.Empty;
            string posFileName = string.Empty;
            string folderProcessed = string.Empty;
            string folderAbort = string.Empty;

            folderProcessed = "Processed";
            folderAbort = "Aborted";
            List<int> reg = new List<int>();
            IMPOSDetails res = OnlineRegAdmin.GetPosKeyandOrgID(regId, "P");
            try
            {
                //Getting the trn_type and orgid
                trn_type = res.Trn_Type;
                orgID = res.OrganisationId;
                if (regType == 2 || regType == 24 || trn_type.ToUpper() == "S")
                    res = OnlineRegAdmin.getIMPOSDetails(regId, orgID, "P");
                else
                    res = OnlineRegAdmin.getIMPOSDetailsForPlanOnly(regId, orgID, "P");
                //usp_GetDetailsForIMPOS_PlanOnly_Test
                //Getting the shared network credentials
                posKey = res.Poskey;
                posNetworkPath = res.POSNetworkPath;
                posDomain = res.POSDomain;
                posUserName = res.POSUserName;
                posPassword = res.POSPassword;

                reg.Add(regId.ToInt());
                var Registrations = Online.Registration.DAL.Registration.RegistrationGet(reg);
                //if (Registrations[0].IMPOSFileName != null && Registrations[0].IMPOSFileName != string.Empty)
                //{
                //    //Getting posfile name
                //    posFileName = Registrations[0].IMPOSFileName;


                //    posFolderPath = posFileName.Split('-');
                //    folderName = posFolderPath[3].ToString();
                //    posFileName = posFileName + ".txt";



                //    isSuccess = LogonUser(posUserName, posDomain, posPassword, LOGON32_LOGON_NEW_CREDENTIALS, LOGON32_PROVIDER_DEFAULT, ref token);
                //    //checking in processed path
                //    using (WindowsImpersonationContext filecopy = new WindowsIdentity(token).Impersonate())
                //    {
                //        //Check if file exists in shared folder
                //        posFilePath = posNetworkPath + folderName + "\\\\" + posFileName;
                //        isFileExists = System.IO.File.Exists(posFilePath);
                //        if (!isFileExists)
                //        {
                //            posFilePath = posNetworkPath + "\\\\" + folderProcessed + "\\\\" + folderName + "\\\\" + posFileName;
                //            isFileExists = System.IO.File.Exists(posFilePath);                            
                //        }
                //        else
                //        {
                //            status = "S"; //File in Input Folder
                //            return false;
                //        }

                //        //Check if file exists in abort folder
                //        if (!isFileExists)
                //        {
                //            posFilePath = posNetworkPath + "\\\\" + folderAbort + "\\\\" + folderName + "\\\\" + posFileName;
                //            isFileExists = System.IO.File.Exists(posFilePath);
                //            if (isFileExists)
                //            {
                //                status = "A"; //File in Abort Folder
                //                return false;
                //            }
                //        }
                //        else
                //        {
                //            return true;
                //        }

                //    }
                //}
                //else
                //    status = "N"; //File not generted

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                throw ex;
            }
            //return isFileExists;
            return true;
        }
        #endregion

        #region WhitleList

        public List<Online.Registration.DAL.Models.SMECIWhiteList> WhiteListSearchByIC(Online.Registration.DAL.Models.SMECIWhiteList whiteList)
        {
            Logger.InfoFormat("Entering {0}.WhiteListSearchByIC({1})", svcName, whiteList);

            var resp = new List<Online.Registration.DAL.Models.SMECIWhiteList>();

            try
            {
                resp = DAL.WhiteList.WhiteListSearchByIC(whiteList);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.WhiteListSearchByIC({1})}", svcName, whiteList);
            }
            return resp;
        }

        public bool DeleteWhiteListCustomer(Online.Registration.DAL.Models.SMECIWhiteList whiteList)
        {
            Logger.InfoFormat("Entering {0}.DeleteWhiteListCustomer({1})", svcName, whiteList);

            var resp = false;

            try
            {
                DAL.WhiteList.DeleteWhiteListCustomer(whiteList);
                resp = true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.DeleteWhiteListCustomer({1})}", svcName, whiteList);
            }
            return resp;
        }
        public bool AddWhiteListCustomer(List<Online.Registration.DAL.Models.SMECIWhiteList> whiteList)
        {
            Logger.InfoFormat("Entering {0}.AddWhiteListCustomer({1})", svcName, whiteList);

            var resp = false;

            try
            {
                DAL.WhiteList.AddWhiteListCustomer(whiteList);
                resp = true;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AddWhiteListCustomer({1})}", svcName, whiteList);
            }
            return resp;
        }

        public List<int> GetDependentComp(string planId)
        {
            Logger.InfoFormat("Entering {0}.GetDependentComp({1})", svcName, planId);

            var resp = new List<int>();

            try
            {
                resp = DAL.WhiteList.GetDependentComp(planId);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetDependentComp({1})}", svcName, planId);
            }
            return resp;
        }


        #endregion
        public int GetMarketCodeIdByRegid(int regid)
        {
            Logger.InfoFormat("Entering {0}.GetMarketCodeIdByRegid({1})", svcName, regid);

            var resp = new int();

            try
            {
                resp = DAL.Registration.GetMarketCodeIdByRegid(regid);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetMarketCodeIdByRegid({1})}", svcName, regid);
            }
            return resp;
        }



        public List<DAL.Models.WaiverComponents> GetWaiverComponents(int PackageID)
        {
            List<DAL.Models.WaiverComponents> WaiverComponents = new List<WaiverComponents>();
            try { WaiverComponents = DAL.Registration.GetWaiverComponents(PackageID); }
            catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
            return WaiverComponents;
        }


        public List<DAL.Models.UserSpecificPrintersInfo> GetUserSpecificPrintersInfo(int UserID, int orgID)
        {

            List<DAL.Models.UserSpecificPrintersInfo> UserSpecificPrinters = null;
            try
            {
                UserSpecificPrinters = DAL.Registration.GetUserSpecificPrinters(UserID, orgID);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

            }

            return UserSpecificPrinters;
        }


        public List<DAL.Models.WaiverComponents> GetWaiverComponentsbyRegID(int RegID)
        {
            List<DAL.Models.WaiverComponents> WaiverComponents = new List<WaiverComponents>();
            try { WaiverComponents = DAL.Registration.GetWaiverComponentsByRegID(RegID); }
            catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
            return WaiverComponents;
        }


        #region supervisor waive off

        public List<string> GetCreatedUsersForWaiveOff(int OrgId)
        {
            Logger.InfoFormat("Entering {0}.GetCreatedUsersForWaiveOff", svcName);

            var resp = new List<string>();

            try
            {
                resp = DAL.Registration.GetCreatedUsersForWaiveOff(OrgId);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetCreatedUsersForWaiveOff", svcName);
            }
            return resp;

        }

        public List<string> GetAcknowledgedUsersForWaiveOff(int OrgId, bool isCDPU)
        {
            Logger.InfoFormat("Entering {0}.GetAcknowledgedUsersForWaiveOff", svcName);

            var resp = new List<string>();

            try
            {
                resp = DAL.Registration.GetAcknowledgedUsersForWaiveOff(OrgId, isCDPU);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetAcknowledgedUsersForWaiveOff", svcName);
            }
            return resp;
        }

        public SupervisorWaiveOffSearchResp SupervisorWaiveOffSearch(SupervisorWaiveOffSearchReq oReq)
        {
            Logger.InfoFormat("Entering {0}.SupervisorWaiveOffSearch", svcName);
            var resp = new SupervisorWaiveOffSearchResp();

            try
            {

                if (oReq.Criteria.TabNo == 0 || oReq.Criteria.TabNo == 1) // Waived components
                    resp.RegistrationSearchResults = DAL.Registration.SupervisorWaiveOffSearch(oReq.Criteria);
                else if (oReq.Criteria.TabNo == 2) // Failed orders
                    resp.RegistrationSearchResults = DAL.Registration.SupervisorWaiveOffSearchForFailedOrders(oReq.Criteria);
            }

            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SupervisorWaiveOffSearch({1}): {2}", svcName, (oReq != null) ? oReq.Criteria.RegID.ToString2() : "NULL", resp.Code);
            }

            return resp;
        }
        public int AcknowledgedUsersForWaiveOffUpdate(AcknowledgedUsersReq oReq)
        {
            int num = 0;
            Logger.InfoFormat("Entering {0}.SupervisorWaiveOffSearch", svcName);
            try { num = DAL.Registration.AcknowledgedUsersForWaiveOffUpdate(oReq.AcknowledgedUsers); }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AcknowledgedUsersForWaiveOffUpdate", svcName);
            }

            return num;

        }

        #endregion


        /// <summary>
        /// Added by Sindhu for Print Version on 11/09/2013
        /// </summary>
        /// <returns></returns>
        public string GetPrintVersionNum(int regType, string brand, string GetPrintVersionNum, string Trn_Type = null)
        {
            string value = null;

            try
            {
                value = DAL.Registration.GetPrintVersionNum(regType, brand, GetPrintVersionNum, Trn_Type);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetPrintVersionNum()", svcName);
            }

            return value;
        }

        /// <summary>
        /// Added by Sindhu for Print Version on 11/09/2013
        /// </summary>
        /// <param name="versionNo"></param>
        /// <param name="regType"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public Online.Registration.DAL.Models.PrintVersion GetPrintTsCsInfo(string versionNo, int regType, string brand, string PlanKenanCode,string Trn_Type = null)
        {
            Online.Registration.DAL.Models.PrintVersion objPrintVersion = new Online.Registration.DAL.Models.PrintVersion();

            try
            {
                objPrintVersion = DAL.Registration.GetPrintTsCsInfo(versionNo, regType, brand, PlanKenanCode, Trn_Type);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetPrintTsCsInfo()", svcName);
            }
            return objPrintVersion;
        }

        /// <summary>
        /// Added by sindhu on 13/09/2013
        /// </summary>
        /// <param name="regId"></param>
        /// <returns></returns>
        public string GetPrintVersionNoByRegid(int regId)
        {
            string value = string.Empty;
            try
            {
                value = DAL.Registration.GetPrintVersionNoByRegid(regId);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetPrintVersionNoByRegid()", svcName);
            }
            return value;

        }

        public int SaveUserPrintLog(UserPrintLog objUserPrintLog)
        {

            int i = 0;
            try { i = DAL.Registration.SaveUserPrintLog(objUserPrintLog); }
            catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
            return i;
        }

        public List<DAL.Models.UserPrintLog> GetPrintersLog(string File)
        {

            List<DAL.Models.UserPrintLog> UserPrintLog = new List<UserPrintLog>();

            try { UserPrintLog = DAL.Registration.GetPrintersLog(File); }
            catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }

            return UserPrintLog;


        }

        public string XmlHelper { get; set; }

        #region smart related methods


        public RegistrationCreateResp RegistrationCreate_Smart(RegistrationCreateReq_smart oReq)
        {
            Logger.InfoFormat("Entering {0}.RegistrationCreate({1})", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL");

            var resp = new RegistrationCreateResp();

            try
            {
                resp.ID = DAL.Registration.RegistrationCreate_Smart(oReq.Registration, oReq.Customer, oReq.Addresses, oReq.RegPgmBdlPkgComps, oReq.RegMdlGrpModel,
                                                              oReq.RegStatus, oReq.RegSuppLines, oReq.RegSuppLineVASes, oReq.CustPhotos, oReq.Packages, oReq.CMSSID, oReq.RegSmartComponents,oReq.RegWaiverComponents);

            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.Smart.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegistrationCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.Registration.LastAccessID : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }

        public RegCancellationRes RegCancellationGet()
        {

            RegCancellationRes oRp = new RegCancellationRes();
            try
            {
                oRp.RegistrationCancellationReasons = DAL.Registration.GetCancellationReasons();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return oRp;
        }

        public WaiverRulesResp GetWaiverRules()
        {

            WaiverRulesResp oRp = new WaiverRulesResp();
            try
            {
                oRp.waiverRules = DAL.Registration.GetWaiverRules();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return oRp;
        }

        #endregion

        public string GetBrandModelIDbyArticleID(string Articleid)
        {
            string value = string.Empty;
            try
            {
                value = DAL.Registration.GetBrandModelIDbyArticleID(Articleid).ToString2();
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetPrintVersionNoByRegid()", svcName);
            }
            return value;

        }

        public List<int> GetSpotifyCopmByPkgKenancode(string kenanCode, int Regid)
        {

            return DAL.Registration.GetSpotifyCopmByPkgKenancode(kenanCode, Regid);
        }
        public List<int> GetSpotifyCopmIDByCompKenancode(string kenanCode)
        {

            return DAL.Registration.GetSpotifyCopmIDByCompKenancode(kenanCode);
        }
        public long GetLnkPgmBdlPkgCompIdByKenanCode(string kenanCode, string planType, string linkType)
        {

            return DAL.Registration.GetLnkPgmBdlPkgCompIdByKenanCode(kenanCode, planType, linkType);
        }
        public int GetIdByTitleName(string title)
        {
            return DAL.Registration.GetIdByTitleName(title);
        }

        /*Changes by chetan related to PDPA implementation*/
        public Online.Registration.DAL.Models.PDPAData GetPDPADataInfo(string DocType, string TrnType)
        {
            Online.Registration.DAL.Models.PDPAData objPDPAData = new Online.Registration.DAL.Models.PDPAData();
            try
            {
                objPDPAData = DAL.Registration.GetPDPADataInfo(DocType, TrnType);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetPDPADataInfo()", svcName);
            }
            return objPDPAData;
        }

        public Online.Registration.DAL.Models.PDPAData GetPDPADataInfoWithVersion(string DocType, string TrnType, string pdpdversion)
        {
            Online.Registration.DAL.Models.PDPAData objPDPAData = new Online.Registration.DAL.Models.PDPAData();
            try
            {
                objPDPAData = DAL.Registration.GetPDPADataInfoWithVersion(DocType, TrnType, pdpdversion);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetPDPADataInfo()", svcName);
            }
            return objPDPAData;
        }
        public string GetDataKenaCode_Smart(string pkgId)
        {
            string id = "0";
            try
            {
                id = DAL.Registration.GetDataKenaCode_Smart(pkgId);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetDataKenaCode_Smart()", svcName);
            }
            return id;
        }
        public int GetSIMModelIDByArticleID(string strAticleID)
        {
            int id = 0;
            try
            {
                id = DAL.Registration.GetSIMModelIDByArticleID(strAticleID);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetSIMModelIDByArticleID()", svcName);
            }
            return id;
        }
        public int SaveDocumentToDB(string photoData, string custPhoto, string altCustPhoto, int regid)
        {
            return DAL.Registration.SaveDocumentToDB(photoData,custPhoto,altCustPhoto,regid);
        }
        public List<string> getRebateContractIDbyComponentID(string ComponentID)
        {
            return DAL.Registration.getRebateContractIDbyComponentID(ComponentID);
        }
        public List<string> getRebateContractIDs()
        {
            return Online.Registration.DAL.Registration.getRebateContractIDs();
        }
        public List<DAL.Models.RebateDataContractComponents> getRebateDataContractComponents()
        {
            return DAL.Registration.getRebateDataContractComponents();
        }
        public List<DAL.Models.BreCheckTreatment> getBreCheckTreatment()
        {
            return DAL.Registration.getBreCheckTreatment();
        }
        public List<DAL.Models.PostCodeCityState> getPostCodeCityState()
        {
            return DAL.Registration.getPostCodeCityState();
        }

        public List<DAL.Models.AutoWaiverRules> getAutoWaiverRules()
        {
            return DAL.Registration.getAutoWaiverRules();
        }
    
        public int SaveLnkRegRebateDatacontractPenalty(LnkLnkRegRebateDatacontractPenaltyReq objRebatePenaltyReq)
        {
            //Logger.InfoFormat("Entering {0}.SaveLnkRegistrationDetails({1})", svcName);
            int outValue;
            try
            {
                outValue = DAL.Registration.SaveLnkRegRebateDatacontractPenalty(objRebatePenaltyReq.rebatePenaltyDetails);
            }
            catch (Exception ex)
            {
                outValue = -1;
                Logger.Error(string.Format("SaveLnkRegRebateDatacontractPenalty: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveLnkRegRebateDatacontractPenalty", svcName);
            }
            return outValue;
        }
        public List<DAL.Models.RegRebateDatacontractPenalty> getRebateDatacontractPenalty(int RegID)
        {
            //Logger.InfoFormat("Entering {0}.SaveLnkRegistrationDetails({1})", svcName);
            List<DAL.Models.RegRebateDatacontractPenalty> outValue;
            try
            {
                outValue = DAL.Registration.getRebateDatacontractPenalty(RegID);
            }
            catch (Exception ex)
            {
                outValue = null;
                Logger.Error(string.Format("getRebateDatacontractPenalty: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.getRebateDatacontractPenalty", svcName);
            }
            return outValue;
        }

        //25052015 Method to get Plan Name query by RegId - Lus
        public MnpRegDetailsVM getPlanNamebyRegId(int RegId)
        {
            //Logger.InfoFormat("Entering {0}.SaveLnkRegistrationDetails({1})", svcName);
            var outValue = new MnpRegDetailsVM();
            try
            {
                outValue = DAL.Registration.getPlanNamebyRegId(RegId);
            }
            catch (Exception ex)
            {
                outValue = null;
                Logger.Error(string.Format("getPlanNamebyRegId: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.getPlanNamebyRegId", svcName);
            }
            return outValue;
        }

        public int RegAccountsUpdate(RegAccount oReq)
        {
            return DAL.Registration.RegAccountUpdate(oReq);
        }

        public RegAccount RegAccountGetByRegID(int regID)
        {
            return DAL.Registration.RegAccountGetByRegID(regID);
        }

        /*Code for saving CMSS case details by chetan*/
        public int SaveCMSSDetails(CMSSComplain objCMSSID)
        {
            int outValue;

            try
            {
                outValue = DAL.Registration.SaveCMSSDetails(objCMSSID);
            }
            catch (Exception ex)
            {
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveLnkRegistrationDetails", svcName);
            }

            return outValue;
        }

        public DateTime GetCMSSCaseDateForAcct(string AccountNumber)
        {
            var value = DateTime.Now.AddDays(-15);
            try
            {
                value = DAL.Registration.GetCMSSCaseDateForAcct(AccountNumber);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetCMSSCaseDateForAcct()", svcName);
            }
            return value;

        }

        public CMSSComplain GetCMSSCaseDetails(int regId)
        {
            CMSSComplain objCMSSCase = new CMSSComplain();
            try
            {
                objCMSSCase = DAL.Registration.GetCMSSCaseDetails(regId);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetCMSSCaseDateForAcct()", svcName);
            }
            return objCMSSCase;

        }

        public int RegSecondaryAccountsForSimRplc(List<lnkSecondaryAcctDetailsSimRplc> SecAccountLines)
        {
            int value = 0;
            try
            {
                value = DAL.Registration.RegSecondaryAccountsForSimRplc(SecAccountLines: SecAccountLines);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RegSecondaryAccountsForSimRplc()", svcName);
            }
            return value;
        }

        public List<lnkSecondaryAcctDetailsSimRplc> GetSecondaryAccountLinesForSimRplc(int Regid)
        {
            var value = new List<lnkSecondaryAcctDetailsSimRplc>();
            try
            {
                value = DAL.Registration.GetSecondaryAccountLinesForSimRplc(regid: Regid);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetSecondaryAccountLinesForSimRplc()", svcName);
            }
            return value;
        }

		public List<PackageComponents> getDisconnectComponents(int regID)
		{
			var value = new List<PackageComponents>();
			try
			{
				value = DAL.Registration.PackageComponentsGet(regID);
			}
			catch (Exception ex)
			{
				Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
			}
			finally
			{
				Logger.InfoFormat("Exiting {0}.getDisconnectComponents()", svcName);
			}
			return value;
		
		}

        public int SaveLnkPackageComponet(int Regid, List<PackageComponents> PackageComponents,int GroupID)
        {
            int value = 0;
            try
            {
                value = DAL.Registration.SaveLnkPackageComponet(regid: Regid, PackageComponents: PackageComponents,GroupID:GroupID);
            }
            catch (Exception ex)
            {
                value = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| SaveLnkPackageComponet StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveLnkPackageComponet()", svcName);
            }
            return value;            
        }
	public RegistrationSimCardTypeFailedResp UpdateSimCardType(int RegID, string SimCardType)
        {
            Logger.InfoFormat("Entering {0}.UpdateSimCardType()", svcName);
            var resp = new RegistrationSimCardTypeFailedResp();

            try
            {
                if (RegID == 0 || string.IsNullOrEmpty(SimCardType))
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Sim Card Type cannot be NULL/Empty!";
                }
                else
                {
                    DAL.Registration.UpdateSimCardType(RegID, SimCardType);
                }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UpdateSimCardType()", svcName);
            }

            return resp;
        }

        public RegistrationSimCardTypeFailedResp UpdateMismSimCardType(int RegID, string MismSimCardType)
        {
            Logger.InfoFormat("Entering {0}.UpdateSimCardType({1})", svcName);
            var resp = new RegistrationSimCardTypeFailedResp();

            try
            {
                if (RegID == 0 || string.IsNullOrEmpty(MismSimCardType))
                {
                    resp.Code = "-1";
                    resp.Message = "Error:Mism Sim Card Type cannot be NULL/Empty!";
                }
                else
                {
                    DAL.Registration.UpdateMismSimCardType(RegID, MismSimCardType);
                }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UpdateMismSimCardType()", svcName);
            }

            return resp;
        }



        public void UpdateSecondarySimSerial(int RegID, string sim)
        {
            Logger.InfoFormat("Entering {0}.UpdateSecondarySimSerial({1})", svcName);
            var resp = new RegistrationSimCardTypeFailedResp();

            try
            {
                if (RegID == 0 || string.IsNullOrEmpty(sim))
                {
                    resp.Code = "-1";
                    resp.Message = "Error:Mism Sim serial cannot be NULL/Empty!";
                }
                else
                {
                    DAL.Registration.UpdateSecondarySimSerial(RegID, sim);
                }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UpdateMismSimCardType()", svcName);
            }

        
        }

        public Online.Registration.DAL.Models.lnkofferuomarticleprice GetOfferDetailsByID(int OfferID)
        {
            Logger.InfoFormat("Enter {0}.lnkOfferUomArticlePrice({1})", svcName, OfferID);
            Online.Registration.DAL.Models.lnkofferuomarticleprice resp = new lnkofferuomarticleprice();
            try
            {
                resp = DAL.Registration.GetOfferDetailsByID(OfferID);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage:{0}|stackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exit {0}.lnkOfferUomArticlePrice({1})", svcName, OfferID);
            }
            return resp;
        }


        public int SaveUserDMEPrint(UserDMEPrint objUserDMEPrint)
        {

            int i = 0;
            try { i = DAL.Registration.SaveDMEUserPrint(objUserDMEPrint); }
            catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
            return i;
        }

        public List<DAL.Models.UserDMEPrint> GetUserDMEPrinters(int userId)
        {
            List<DAL.Models.UserDMEPrint> UserDMEPrint = new List<DAL.Models.UserDMEPrint>();
            try
            {
                UserDMEPrint = DAL.Registration.GetUserDMEPrinters(userId);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            return UserDMEPrint;
        }
        public string GetCDPUStatus(int regId)
        {
            string status = string.Empty;
            try
            {
                status = DAL.Registration.GetCDPUStatus(regId);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetCDPUStatus()", svcName);
            }
            return status;

        }

        public RegBREStatus GetCDPUDetails(RegBREStatus req)
        {
            RegBREStatus details = new RegBREStatus();
            try
            {
                details = DAL.Registration.GetCDPUDetails(req);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetCDPUDetails()", svcName);
            }
            return details;
        }

        public List<DAL.Models.PendingRegBREStatus> PendingOrders()
        {
            List<DAL.Models.PendingRegBREStatus> RegBREStatusList = new List<DAL.Models.PendingRegBREStatus>();
            try
            {
                RegBREStatusList = DAL.Registration.PendingOrders();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            return RegBREStatusList;
        }

        // Drop 5
        public bool UpsertBRE (RegBREStatus objBre)
        {
            bool i = false;
            try { i = DAL.Registration.UpsertBRE(objBre); }
            catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
            return i;
        }


        public int UpdateBREApproveStatus(int RegId, string Status, string LastAccessID)
        {
            int i = 0;
            try { i = DAL.Registration.UpdateBREApproveStatus(RegId, Status, LastAccessID); }
            catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
            return i;
        }

        public int UpdateBRECancelReason(int RegId, string CReason, string LastAccessID)
        {
            int i = 0;
            try { i = DAL.Registration.UpdateBRECancelReason(RegId, CReason, LastAccessID); }
            catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
            return i;
        }

        public List<DAL.Models.DealerPlanIds> GetDealerSpecificPlans(int userId)
        {
            var result = DAL.Registration.GetDealerSpecificPlans(userId);
            return result;
        }
        public bool IsCallConferenceWaived(int RegID)
        {
            bool isWaived = DAL.Registration.IsCallConferenceWaived(RegID);
            return isWaived;
        }

        public bool IsInternationalRoamingWaived(int RegID)
        {
            bool isWaived = DAL.Registration.IsInternationalRoamingWaived(RegID);
            return isWaived;
        }

        public string ArticelIdByModelId(int modelID)
        {
            return DAL.Registration.ArticelIdByModelId(modelID);
        }

        public void InsertLog(WebPosLog webposlog)
        {
            DAL.Registration.InsertLog(webposlog);
        }

		public int SaveListRegAttributes(List<RegAttributes> objListRegAtributes)
		{
			int i = 0;
			try { i = DAL.Registration.SaveListRegAttributes(objListRegAtributes); }
			catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
			return i;
		}

		public int SaveRegAttributes(RegAttributes objListRegAtributes)
		{
			int i = 0;
			try { i = DAL.Registration.SaveRegAttributes(objListRegAtributes); }
			catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
			return i;
		}

		public List<RegAttributes> RegAttributesGetAll(RegAttributeReq oReq)
		{
			List<RegAttributes> result = new List<RegAttributes>();
			try
			{
				result =  DAL.Registration.RegAttributesGetAll(oReq);
			}
			catch (Exception ex)
			{
				Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
			}
			return result;	
		}
		
		public List<RegAttributes> RegAttributesGetByRegID(int _regID)
		{
			List<RegAttributes> result = new List<RegAttributes>();
			try
			{
				result = DAL.Registration.RegAttributesGetByRegID(_regID);
			}
			catch (Exception ex)
			{
				Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
			}
			return result;
		}

		public List<RegAttributes> RegAttributesGetBySuppLineID(int _suppLineID)
		{
			List<RegAttributes> result = new List<RegAttributes>();
			try
			{
				result = DAL.Registration.RegAttributesGetBySuppLineID(_suppLineID);
			}
			catch (Exception ex)
			{
				Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
			}
			return result;
		}

        //11052015 - Add for Write off supervisor approval CR- Lus
        public int SaveJustificationDetails(RegJustification objListRegJustification)
        {
            int i = 0;
            try { i = DAL.Registration.SaveJustificationDetails(objListRegJustification); }
            catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
            return i;
		}

        // 20160120 - w.loon - Write to TrnRegBreFailTreatment - for display in Supervisor Dashboard
        public int SaveBreFailTreatmentDetails(RegBreFailTreatment objRegBreFailTreatment)
        {
            int i = 0;
            try { i = DAL.Registration.SaveBreFailTreatmentDetails(objRegBreFailTreatment); }
            catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
            return i;
        }
		
		public int SaveSurveyResponse(RegSurveyResponse objRegSurveyResponse)
        {
            int i = 0;
			try { i = DAL.Registration.SaveSurveyResponse(objRegSurveyResponse); }
            catch (Exception ex) { Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex); }
            return i;
		}

        public List<int> SaveTrnRegAccessory(List<RegAccessory> objRegAccessories)
        {
            var result = new List<int>();

            try
            {
                result = DAL.Registration.SaveTrnRegAccessory(objRegAccessories);
                Logger.Debug("SaveTrnRegAccessory, parameter: " + objRegAccessories);
                Logger.Debug(result);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in SaveTrnRegAccessory, parameter: " + objRegAccessories);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }

            return result;
        }

        public List<RegAccessory> GetRegAccessoryByRegID(int regID)
        {
            var result = new List<RegAccessory>();

            try
            {
                result = DAL.Registration.GetRegAccessoryByRegID(regID);
                Logger.Debug("SaveTrnRegAccessory, parameter: " + regID);
                Logger.Debug(result);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in SaveTrnRegAccessory, parameter: " + regID);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }

            return result;
        }

        public bool SaveTrnRegAccessoryDetails(List<RegAccessoryDetails> objRegAccessoryDetailsList)
        {
            var result = false;

            try
            {
                result = DAL.Registration.SaveTrnRegAccessoryDetails(objRegAccessoryDetailsList);
                Logger.Debug("SaveTrnRegAccessoryDetails, parameter: " + objRegAccessoryDetailsList);
                Logger.Debug(result);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in SaveTrnRegAccessoryDetails, parameter: " + objRegAccessoryDetailsList);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }

            return result;
        }

        public List<RegAccessoryDetails> GetRegAccessoryDetailsByRegAccessoryID(List<int> regAccessoryIDs)
        {
            var result = new List<RegAccessoryDetails>();

            try
            {
                result = DAL.Registration.GetRegAccessoryDetailsByRegAccessoryID(regAccessoryIDs);
                Logger.Debug("GetRegAccessoryDetailsByRegAcessoryID, parameter: " + regAccessoryIDs);
                Logger.Debug(result);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetRegAccessoryDetailsByRegAcessoryID, parameter: " + regAccessoryIDs);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }

            return result;
        }

        public int SaveAccessorySerialNumber(List<RegAccessoryDetails> objRegAccessoryDetailsList)
        {
            var result = 0;

            try
            {
                result = DAL.Registration.SaveAccessorySerialNumber(objRegAccessoryDetailsList);
                Logger.Debug("SaveAccessorySerialNumber, parameter: " + objRegAccessoryDetailsList);
                Logger.Debug(result);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in SaveAccessorySerialNumber, parameter: " + objRegAccessoryDetailsList);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }

            return result;
        }

        public List<PopUpMessage> GetPopUpMessageByIdCard(string IDCardNo)
        {
            var result = new List<PopUpMessage>();
            try
            {
                result = DAL.Registration.GetPopUpMessage(IDCardNo);
                Logger.Debug("GetPopUpMessageByIdCard, parameter: " + IDCardNo);
                Logger.Debug(result);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetPopUpMessageByIdCard, parameter: " + IDCardNo);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            return result;
        }

        public int UpdatePopUpMessageAcceptReject(int ID, string Action)
        {
            var result = 0;
            try
            {
                Logger.Debug("UpdatePopUpMessageAcceptReject, parameter: " + ID + ":" + Action);
                result = DAL.Registration.UpdatePopUpMessageAcceptReject(ID, Action);
                Logger.Debug(result);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in UpdatePopUpMessageAcceptReject, parameter: " + ID + ":" + Action);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            return result;
        }

        public List<PopUpMessageAuditLog> SavePopUpMessageAuditLog(List<PopUpMessageAuditLog> objPopUpMsgAuditLog)
        {
            var result = new List<PopUpMessageAuditLog>();

            try
            {
                result = DAL.Registration.SavePopUpMessageAuditLog(objPopUpMsgAuditLog);
                Logger.Debug("SavePopUpMessageAuditLog, parameter: " + objPopUpMsgAuditLog);
                Logger.Debug(result);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in SavePopUpMessageAuditLog, parameter: " + objPopUpMsgAuditLog);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }

            return result;
        }

        public List<PegaRecommendation> GetPegaRecommendationbyMSISDN(PegaRecommendationSearchCriteria criteria)
        {
            var result = new List<PegaRecommendation>();
            try
            {
                result = DAL.Registration.GetPegaRecommendationbyMSISDN(criteria);
                Logger.Debug("GetPegaRecommendationbyMSISDN, parameter: " + criteria.MSISDN);
                Logger.Debug(result);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetPegaRecommendationbyMSISDN, parameter: " + criteria.MSISDN);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            return result;
        }

		public List<PegaRecommendation> SavePegaRecommendation(List<PegaRecommendation> objPegaRecommendation)
        {
			var result = new List<PegaRecommendation>();

            try
            {
                result = DAL.Registration.SavePegaRecommendation(objPegaRecommendation);
                //Logger.Debug("SavePegaRecommendation, parameter: " + objPegaRecommendation);
                Logger.Debug("SavePegaRecommendation = " + result.Select(x=> x.ID).ToList());
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }

            return result;
        }

        public int UpdatePegaRecommendation(List<PegaRecommendationVM> objPegaVM)
        {
            var result = new int();
            try
            {
                result = DAL.Registration.UpdatePegaRecommendation(objPegaVM);
                Logger.Debug("UpdatePegaRecommendation, parameter: " + objPegaVM);
                Logger.Debug(result);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in UpdatePegaRecommendation, parameter: " + objPegaVM);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }

            return result;
        }

        public TacAuditLogReqResp InsertTacAuditLog(TacAuditLogReq oReq)
        {
            Logger.InfoFormat("Entering {0}.InsertTacAuditLog({1})", svcName, (oReq != null) ? oReq.TacAuditLog.MSISDN : "NULL");

            var resp = new TacAuditLogReqResp();

            try
            {
                resp.ID = DAL.Registration.InsertTacAuditLog(oReq.TacAuditLog);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.InsertTacAuditLog({1}): {2}|{3}", svcName, (oReq != null) ? oReq.TacAuditLog.MSISDN : "NULL", resp.ID, resp.Code);
            }

            return resp;
        }


		public List<TacAuditLog> GetAllTacAuditLog(int rowCount)
		{
			var responseList = new List<TacAuditLog>();
			try
			{
				responseList = DAL.Registration.getAllTacAuditLog(rowCount);
			}
			catch
			{
				responseList = null;
			}
			return responseList;
		}

        public ThirdPartyAuthTypeCreateResp ThirdPartyAuthTypeCreate(ThirdPartyAuthTypeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ThirdPartyAuthTypeCreate({1})", svcName, oReq.ThirdPartyAuthType.Name);
            ThirdPartyAuthTypeCreateResp oRp = new ThirdPartyAuthTypeCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.ThirdPartyAuthTypeCreate(oReq.ThirdPartyAuthType);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ThirdPartyAuthTypeCreate({1}): {2}|{3}", svcName, oReq.ThirdPartyAuthType.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }

        public ThirdPartyAuthTypeUpdateResp ThirdPartyAuthTypeUpdate(ThirdPartyAuthTypeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ThirdPartyAuthTypeUpdate({1})", svcName, oReq.ThirdPartyAuthType.Name);
            ThirdPartyAuthTypeUpdateResp oRp = new ThirdPartyAuthTypeUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.ThirdPartyAuthTypeUpdate(oReq.ThirdPartyAuthType);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Third Party Authorization Type update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ThirdPartyAuthTypeUpdate({1}): {2}", svcName, oReq.ThirdPartyAuthType.Name, oRp.Code);
            }
            return oRp;
        }

        public ThirdPartyAuthTypeGetResp ThirdPartyAuthTypeGet(ThirdPartyAuthTypeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}ThirdPartyAuthTypeGet({1})", svcName, (oReq.IDs.Count > 0) ? oReq.IDs.Count : 0);
            ThirdPartyAuthTypeGetResp oRp = new ThirdPartyAuthTypeGetResp();
            try
            {
                oRp.ThirdPartyAuthTypes = OnlineRegAdmin.ThirdPartyAuthTypeGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ThirdPartyAuthTypeGet({1}): {2}", svcName, (oReq.IDs.Count > 0) ? oReq.IDs.Count : 0, oRp.Code);
            }
            return oRp;
        }

        public ThirdPartyAuthTypeGetResp ThirdPartyAuthTypeGetList()
        {
            Logger.InfoFormat("Entering {0}ThirdPartyAuthTypeGetList({1})", svcName, "");
            ThirdPartyAuthTypeGetResp oRp = new ThirdPartyAuthTypeGetResp();
            try
            {
                oRp.ThirdPartyAuthTypes = OnlineRegAdmin.ThirdPartyAuthTypeGetList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ThirdPartyAuthTypeGetList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public ThirdPartyAuthTypeFindResp ThirdPartyAuthTypeFind(ThirdPartyAuthTypeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ThirdPartyAuthTypeFind({1})", svcName, oReq.ThirdPartyAuthTypeFind);
            ThirdPartyAuthTypeFindResp oRp = new ThirdPartyAuthTypeFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ThirdPartyAuthTypeFind(oReq.ThirdPartyAuthTypeFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ThirdPartyAuthTypeFind({1}): {2}", svcName, oReq.ThirdPartyAuthTypeFind.ThirdPartyAuthType, oRp.Code);
            }
            return oRp;
        }
	}
}

