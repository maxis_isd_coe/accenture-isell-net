﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.Service.Models;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReportsService" in both code and config file together.
    [ServiceContract]
    public interface IReportsService
    {
        [OperationContract]
        AgingReportGetResp AgingReportGet(AgingReportGetReq oReq);

        [OperationContract]
        BiometricsReportGetResp BiometricsReportGet(BiometricsReportGetReq oReq);

        [OperationContract]
        FailTransactionReportGetResp FailTranReportGet(FailTransactionReportGetReq oReq);

        [OperationContract]
        RegHistoryGetResp RegHistoryDetailsGet(RegHistoryGetReq oReq);
    }
}
