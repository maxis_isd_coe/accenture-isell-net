﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.Service.Models;
using Online.Registration.DAL.Models;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserService" in both code and config file together.
    [ServiceContract]
    public interface IUserService
    {
        #region User Group Access

        [OperationContract]
        UserGrpAccessCreateResp UserGrpAccessCreate(UserGrpAccessCreateReq oReq);
        [OperationContract]
        UserGrpAccessUpdateResp UserGrpAccessUpdate(UserGrpAccessUpdateReq oReq);
        [OperationContract]
        UserGrpAccessFindResp UserGrpAccessFind(UserGrpAccessFindReq oReq);
        [OperationContract]
        UserGrpAccessGetResp UserGrpAccessGet(UserGrpAccessGetReq oReq);

        #endregion

        #region User Group Role

        [OperationContract]
        UserGrpRoleCreateResp UserGrpRoleCreate(UserGrpRoleCreateReq oReq);
        [OperationContract]
        UserGrpRoleUpdateResp UserGrpRoleUpdate(UserGrpRoleUpdateReq oReq);
        [OperationContract]
        UserGrpRoleFindResp UserGrpRoleFind(UserGrpRoleFindReq oReq);
        [OperationContract]
        UserGrpRoleGetResp UserGrpRoleGet(UserGrpRoleGetReq oReq);

        #endregion

        #region User Group

        [OperationContract]
        UserGroupCreateResp UserGroupCreate(UserGroupCreateReq oReq);
        [OperationContract]
        UserGroupUpdateResp UserGroupUpdate(UserGroupUpdateReq oReq);
        [OperationContract]
        UserGroupFindResp UserGroupFind(UserGroupFindReq oReq);
        [OperationContract]
        UserGroupGetResp UserGroupGet(UserGroupGetReq oReq);
        [OperationContract]
        UserGroupGetResp UserGroupList();

        #endregion

        #region User Role

        [OperationContract]
        UserRoleCreateResp UserRoleCreate(UserRoleCreateReq oReq);
        [OperationContract]
        UserRoleUpdateResp UserRoleUpdate(UserRoleUpdateReq oReq);
        [OperationContract]
        UserRoleFindResp UserRoleFind(UserRoleFindReq oReq);
        [OperationContract]
        UserRoleGetResp UserRoleGet(UserRoleGetReq oReq);
        [OperationContract]
        UserRoleGetResp UserRolesList();
        #endregion

        #region Access

        [OperationContract]
        AccessCreateResp AccessCreate(AccessCreateReq oReq);
        [OperationContract]
        AccessUpdateResp AccessUpdate(AccessUpdateReq oReq);
        [OperationContract]
        AccessUpdateResp AccessPasswordUpdate(AccessUpdateReq oReq);
        [OperationContract]
        AccessUpdateResp AccessFailedPasswordAttemptAdd(int accessID);
        [OperationContract]
        AccessUpdateResp AccessFailedPasswordAttemptReset(int accessID);
        [OperationContract]
        AccessUpdateResp AccessLockedOut(int accessID);
        [OperationContract]
        AccessUpdateResp AccessUnlock(int accessID, string lastAccessID);
        [OperationContract]
        AccessFindResp AccessFind(AccessFindReq oReq);
        [OperationContract]
        AccessGetResp AccessGet(AccessGetReq oReq);
        [OperationContract]
        AccessGetResp AccessList();

        #endregion

        #region User

        [OperationContract]
        UserCreateResp UserCreate(UserCreateReq oReq);
        [OperationContract]
        UserUpdateResp UserUpdate(UserUpdateReq oReq);
        [OperationContract]
        UserFindResp UserFind(UserFindReq oReq);
        [OperationContract]
        UserFindResp UserFindbyName(UserFindbyNameReq oReq);
        [OperationContract]
        UserGetResp UserGet(UserGetReq oReq);
        [OperationContract]
        UserGetResp UsersList();

        #endregion

        [OperationContract]
        UsersInRoleFindResp UsersInRoleFind(UsersInRoleFindReq oReq);
        [OperationContract]
        RolesForUserGetResp RolesForUserGet(RolesForUserGetReq oReq);

        #region Apistatusandmessages
        [OperationContract]
        GetAPIStatusMessagesResp GetAPIStatusMessages(GetAPIStatusMessagesReq oReq);
        #endregion

        #region VLT ADDED CODE
        [OperationContract]
        APISaveCallStatusResp APISaveCallStatus(APISaveCallStatusReq oReq);
        [OperationContract]
        APISaveCallStatusResp APIUpdateCallStatus(APISaveCallStatusReq oReq);

        [OperationContract]
        PlanSearchItemResp PlanSearchItemMinRange(PlanSearchItemReq oReq);

        [OperationContract]
        PlanSearchItemResp PlanSearchItemMaxRange(PlanSearchItemReq oReq);
        #endregion VLT ADDED CODE

        #region VLT ADDED CODE by Rajeswari on Feb 25th for Logging user information

        [OperationContract]
        UserTransactionLogResp UserLogInfo(UserTransactionLogReq oReq);

        #endregion


        [OperationContract]
        bool MocICcheckwithwhitelistcustomers(string userName, string strIcno);

        [OperationContract]
        GetDonerMessagesResp GetDonors();

        [OperationContract]
        int GetPPIDCountSearch(string userName, string action);

        [OperationContract]
        List<UserConfigurations> GetUserConfigurations();

        [OperationContract]
        int SaveUserAuditLog(string userName, string sessionId, string ipAddress, int loggedOut);

        [OperationContract]
        int SaveUserTransactionLog(int usrAuditLogId, int moduleId, string actionDone, string accountNo, string dealerNo);

    }
}
