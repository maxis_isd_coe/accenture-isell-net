﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using Online.Registration.DAL.Models;

namespace Online.Registration.Service.Models
{
    #region Complain

    #region Create

    [DataContract]
    [Serializable]
    public class ComplainCreateReq
    {
        [DataMember]
        public Complain Complain { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComplainCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    #endregion

    #region Update

    [DataContract]
    [Serializable]
    public class ComplainUpdateReq
    {
        [DataMember]
        public Complain Complain { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComplainUpdateResp : WCFResponse { }

    #endregion

    #region Find

    [DataContract]
    [Serializable]
    public class ComplainFindReq
    {
        [DataMember]
        public ComplainFind FindCriteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComplainFindResp : FindResponse { }

    #endregion

    #region Get

    [DataContract]
    [Serializable]
    public class ComplainGetReq : GetRequest { }

    [DataContract]
    [Serializable]
    public class ComplainGetResp : WCFResponse
    {
        [DataMember]
        public List<Complain> Complains { get; set; }
    }

    #endregion

    #endregion

    #region Complaint Issues

    #region Create

    [DataContract]
    [Serializable]
    public class ComplaintIssuesCreateReq
    {
        [DataMember]
        public ComplaintIssues ComplaintIssues { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComplaintIssuesCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    #endregion

    #region Update

    [DataContract]
    [Serializable]
    public class ComplaintIssuesUpdateReq
    {
        [DataMember]
        public ComplaintIssues ComplaintIssues { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComplaintIssuesUpdateResp : WCFResponse { }

    #endregion

    #region Find

    [DataContract]
    [Serializable]
    public class ComplaintIssuesFindReq
    {
        [DataMember]
        public ComplaintIssuesFind FindCriteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComplaintIssuesFindResp : FindResponse { }

    #endregion

    #region Get

    [DataContract]
    [Serializable]
    public class ComplaintIssuesGetReq : GetRequest { }

    [DataContract]
    [Serializable]
    public class ComplaintIssuesGetResp : WCFResponse
    {
        [DataMember]
        public List<ComplaintIssues> ComplaintIssuess { get; set; }
    }

    #endregion

    #endregion

    #region Complaint Issues Group

    #region Create

    [DataContract]
    [Serializable]
    public class ComplaintIssuesGroupCreateReq
    {
        [DataMember]
        public ComplaintIssuesGroup ComplaintIssuesGroup { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComplaintIssuesGroupCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    #endregion

    #region Update

    [DataContract]
    [Serializable]
    public class ComplaintIssuesGroupUpdateReq
    {
        [DataMember]
        public ComplaintIssuesGroup ComplaintIssuesGroup { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComplaintIssuesGroupUpdateResp : WCFResponse { }

    #endregion

    #region Find

    [DataContract]
    [Serializable]
    public class ComplaintIssuesGroupFindReq
    {
        [DataMember]
        public ComplaintIssuesGroupFind FindCriteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComplaintIssuesGroupFindResp : FindResponse { }

    #endregion

    #region Get

    [DataContract]
    [Serializable]
    public class ComplaintIssuesGroupGetReq : GetRequest { }

    [DataContract]
    [Serializable]
    public class ComplaintIssuesGroupGetResp : WCFResponse
    {
        [DataMember]
        public List<ComplaintIssuesGroup> ComplaintIssuesGroups { get; set; }
    }

    #endregion

    #endregion

    #region Issue Grouop Dispatch Queue

    #region Create

    [DataContract]
    [Serializable]
    public class IssueGrpDispatchQCreateReq
    {
        [DataMember]
        public IssueGrpDispatchQ IssueGrpDispatchQ { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IssueGrpDispatchQCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    #endregion

    #region Update

    [DataContract]
    [Serializable]
    public class IssueGrpDispatchQUpdateReq
    {
        [DataMember]
        public IssueGrpDispatchQ IssueGrpDispatchQ { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IssueGrpDispatchQUpdateResp : WCFResponse { }

    #endregion

    #region Find

    [DataContract]
    [Serializable]
    public class IssueGrpDispatchQFindReq
    {
        [DataMember]
        public IssueGrpDispatchQFind FindCriteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IssueGrpDispatchQFindResp : FindResponse { }

    #endregion

    #region Get

    [DataContract]
    [Serializable]
    public class IssueGrpDispatchQGetReq : GetRequest { }

    [DataContract]
    [Serializable]
    public class IssueGrpDispatchQGetResp : WCFResponse
    {
        [DataMember]
        public List<IssueGrpDispatchQ> IssueGrpDispatchQs { get; set; }
    }

    #endregion

    #endregion



    [DataContract]
    [Serializable]
    public class ComplaintGetResp : WCFResponse
    {
        [DataMember]
        public List<CMSSComplainGet> CMSSComplainIssues { get; set; }
    }
}