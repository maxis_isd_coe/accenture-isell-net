﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using Online.Registration.DAL.Models;

namespace Online.Registration.Service.Models
{
    #region Admin

    #region Address Type

    [DataContract]
    [Serializable]
    public class AddressTypeCreateReq
    {
        [DataMember]
        public AddressType AddressType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressTypeUpdateReq
    {
        [DataMember]
        public AddressType AddressType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressTypeFindReq
    {
        [DataMember]
        public AddressTypeFind AddressTypeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressTypeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressTypeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressTypeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class AddressTypeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<AddressType> AddressTypes { get; set; }
    }

    #endregion

    #region Bank

    [DataContract]
    [Serializable]
    public class BankCreateReq
    {
        [DataMember]
        public Bank Bank { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BankUpdateReq
    {
        [DataMember]
        public Bank Bank { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BankFindReq
    {
        [DataMember]
        public BankFind BankFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BankGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BankCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BankUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class BankFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BankGetResp : WCFResponse
    {
        [DataMember]
        public List<Bank> Bank { get; set; }
    }

    #endregion

    #region Card Type

    [DataContract]
    [Serializable]
    public class CardTypeCreateReq
    {
        [DataMember]
        public CardType CardType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypeUpdateReq
    {
        [DataMember]
        public CardType CardType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypeFindReq
    {
        [DataMember]
        public CardTypeFind CardTypeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class CardTypeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<CardType> CardType { get; set; }
    }

    #endregion

    #region Country

    [DataContract]
    [Serializable]
    public class CountryCreateReq
    {
        [DataMember]
        public Country Country { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CountryUpdateReq
    {
        [DataMember]
        public Country Country { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CountryFindReq
    {
        [DataMember]
        public CountryFind CountryFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CountryGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CountryCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CountryUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class CountryFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CountryGetResp : WCFResponse
    {
        [DataMember]
        public List<Country> Country { get; set; }
    }

    #endregion

    #region Country State

    [DataContract]
    [Serializable]
    public class StateCreateReq
    {
        [DataMember]
        public State State { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StateUpdateReq
    {
        [DataMember]
        public State State { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StateFindReq
    {
        [DataMember]
        public StateFind StateFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StateGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StateCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StateUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class StateFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StateGetResp : WCFResponse
    {
        [DataMember]
        public List<State> State { get; set; }
    }

    #endregion

    #region ID Card Type

    [DataContract]
    [Serializable]
    public class IDCardTypeCreateReq
    {
        [DataMember]
        public IDCardType IDCardType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IDCardTypeUpdateReq
    {
        [DataMember]
        public IDCardType IDCardType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IDCardTypeFindReq
    {
        [DataMember]
        public IDCardTypeFind IDCardTypeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IDCardTypeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IDCardTypeCreateResp : WCFResponse
    {
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IDCardTypeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class IDCardTypeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class IDCardTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<IDCardType> IDCardType { get; set; }
    }

    #endregion

    #region Payment Mode

    [DataContract]
    [Serializable]
    public class PaymentModeCreateReq
    {
        [DataMember]
        public PaymentMode PaymentMode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModeUpdateReq
    {
        [DataMember]
        public PaymentMode PaymentMode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModeFindReq
    {
        [DataMember]
        public PaymentModeFind PaymentModeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class PaymentModeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModeGetResp : WCFResponse
    {
        [DataMember]
        public List<PaymentMode> PaymentMode { get; set; }
    }

    #endregion

    #region Reg Type

    [DataContract]
    [Serializable]
    public class RegTypeCreateReq
    {
        [DataMember]
        public RegType RegType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegTypeUpdateReq
    {
        [DataMember]
        public RegType RegType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegTypeFindReq
    {
        [DataMember]
        public RegTypeFind RegTypeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegTypeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegTypeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegTypeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegTypeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<RegType> RegTypes { get; set; }
    }

    #endregion

    #region Race

    [DataContract]
    [Serializable]
    public class RaceCreateReq
    {
        [DataMember]
        public Race Race { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RaceUpdateReq
    {
        [DataMember]
        public Race Race { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RaceFindReq
    {
        [DataMember]
        public RaceFind RaceFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RaceGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RaceCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RaceUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RaceFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RaceGetResp : WCFResponse
    {
        [DataMember]
        public List<Race> Races { get; set; }
    }

    #endregion

    #region CustomerTitle

    [DataContract]
    [Serializable]
    public class CustomerTitleCreateReq
    {
        [DataMember]
        public CustomerTitle CustomerTitle { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerTitleUpdateReq
    {
        [DataMember]
        public CustomerTitle CustomerTitle { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerTitleFindReq
    {
        [DataMember]
        public CustomerTitleFind CustomerTitleFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerTitleGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerTitleCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerTitleUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class CustomerTitleFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerTitleGetResp : WCFResponse
    {
        [DataMember]
        public List<CustomerTitle> CustomerTitles { get; set; }
    }

    #endregion

    #region Nationality

    [DataContract]
    [Serializable]
    public class NationalityCreateReq
    {
        [DataMember]
        public Nationality Nationality { get; set; }
    }

    [DataContract]
    [Serializable]
    public class NationalityUpdateReq
    {
        [DataMember]
        public Nationality Nationality { get; set; }
    }

    [DataContract]
    [Serializable]
    public class NationalityFindReq
    {
        [DataMember]
        public NationalityFind NationalityFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class NationalityGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class NationalityCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class NationalityUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class NationalityFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class NationalityGetResp : WCFResponse
    {
        [DataMember]
        public List<Nationality> Nationalities { get; set; }
    }

    #endregion

    #region Language

    [DataContract]
    [Serializable]
    public class LanguageCreateReq
    {
        [DataMember]
        public Language Language { get; set; }
    }

    [DataContract]
    [Serializable]
    public class LanguageUpdateReq
    {
        [DataMember]
        public Language Language { get; set; }
    }

    [DataContract]
    [Serializable]
    public class LanguageFindReq
    {
        [DataMember]
        public LanguageFind LanguageFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class LanguageGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class LanguageCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class LanguageUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class LanguageFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class LanguageGetResp : WCFResponse
    {
        [DataMember]
        public List<Language> Languages { get; set; }
    }

    #endregion

    #region ExtIDType

    [DataContract]
    [Serializable]
    public class ExtIDTypeCreateReq
    {
        [DataMember]
        public ExtIDType ExtIDType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExtIDTypeUpdateReq
    {
        [DataMember]
        public ExtIDType ExtIDType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExtIDTypeFindReq
    {
        [DataMember]
        public ExtIDTypeFind ExtIDTypeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExtIDTypeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExtIDTypeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExtIDTypeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ExtIDTypeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExtIDTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<ExtIDType> ExtIDType { get; set; }
    }

    #endregion

    #region VIPCode

    [DataContract]
    [Serializable]
    public class VIPCodeCreateReq
    {
        [DataMember]
        public VIPCode VIPCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class VIPCodeUpdateReq
    {
        [DataMember]
        public VIPCode VIPCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class VIPCodeFindReq
    {
        [DataMember]
        public VIPCodeFind VIPCodeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class VIPCodeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class VIPCodeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class VIPCodeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class VIPCodeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class VIPCodeGetResp : WCFResponse
    {
        [DataMember]
        public List<VIPCode> VIPCode { get; set; }
    }

    #endregion

    #endregion 

    #region Address

    [DataContract]
    [Serializable]
    public class AddressCreateReq
    {
        [DataMember]
        public Address Addresses { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressUpdateReq
    {
        [DataMember]
        public Address Addresses { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressFindReq
    {
        [DataMember]
        public AddressFind AddressFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class AddressFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddressGetResp : WCFResponse
    {
        [DataMember]
        public List<Address> Addresses { get; set; }
    }

    #endregion

    #region Customer

    [DataContract]
    [Serializable]
    public class CustomerCreateReq
    {
        [DataMember]
        public Customer Customer { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerUpdateReq
    {
        [DataMember]
        public Customer Customer { get; set; }
        //[DataMember]
        //public Registration Registration { get; set; }
        //[DataMember]
        //public RegUsername RegUsername { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerFindReq
    {
        [DataMember]
        public CustomerFind CustomerFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class CustomerFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerGetResp : WCFResponse
    {
        [DataMember]
        public List<Customer> Customers { get; set; }
    }

    #endregion

    #region Registration

    [DataContract]
    [Serializable]
    public class RegistrationPendingOrdersReq
    {
        [DataMember]
        public RegistrationPendingOrderslist Criteria { get; set; }
    }



    [DataContract]
    [Serializable]
    public class RegistrationCreateReq
    {
        [DataMember]
        public DAL.Models.Registration Registration { get; set; }
        [DataMember]
        public Customer Customer { get; set; }
        [DataMember]
        public List<Address> Addresses { get; set; }
        [DataMember]
        public RegMdlGrpModel RegMdlGrpModel { get; set; }
        [DataMember]
        public List<RegPgmBdlPkgComp> RegPgmBdlPkgComps { get; set; }
        [DataMember]
        public RegStatus RegStatus { get; set; }
        [DataMember]
        public List<RegSuppLine> RegSuppLines { get; set; }
        [DataMember]
        public List<RegPgmBdlPkgComp> RegSuppLineVASes { get; set; }
        [DataMember]
        public List<string> CustPhotos { get; set; }
        [DataMember]
        public List<RegSmartComponents> RegSmartComponents{ get; set; }
        [DataMember]
        public List<WaiverComponents> RegWaiverComponents { get; set; }

        [DataMember]
        public bool isBREFail { get; set; }

        [DataMember]
        public int approverID { get; set; }

        [DataMember]
        public DateTime? TimeApproval { get; set; }

    }

    [DataContract]
    [Serializable]
    public class RegistrationCreateWithSecReq
    {
        [DataMember]
        public DAL.Models.Registration Registration { get; set; }
        [DataMember]
        public DAL.Models.RegistrationSec RegistrationSec { get; set; }
        [DataMember]
        public Customer Customer { get; set; }
        [DataMember]
        public List<Address> Addresses { get; set; }
        [DataMember]
        public RegMdlGrpModel RegMdlGrpModel { get; set; }
        [DataMember]
        public RegMdlGrpModelSec RegMdlGrpModelSec { get; set; } //Device plan
        [DataMember]
        public List<RegPgmBdlPkgComp> RegPgmBdlPkgComps { get; set; }
        [DataMember]
        public RegStatus RegStatus { get; set; }
        [DataMember]
        public List<RegSuppLine> RegSuppLines { get; set; }
        [DataMember]
        public List<RegPgmBdlPkgComp> RegSuppLineVASes { get; set; }
        [DataMember]
        public List<string> CustPhotos { get; set; }
        [DataMember]
        public List<RegPgmBdlPkgCompSec> RegPgmBdlPkgCompSecs { get; set; }

        [DataMember]
        public List<RegSmartComponents> RegSmartComponents { get; set; }

        [DataMember]
        public List<WaiverComponents> RegWaiverComponents { get; set; }

        [DataMember]
        public bool isBREFail { get; set; }

        [DataMember]
        public int approverID { get; set; }
        
        [DataMember]
        public DateTime? TimeApproval { get; set; }
    }

    [DataContract]
    [Serializable]
    public class HomeRegistrationCreateReq
    {
        [DataMember]
        public DAL.Models.Registration Registration { get; set; }
        [DataMember]
        public Customer Customer { get; set; }
        [DataMember]
        public List<Address> Addresses { get; set; }
        [DataMember]
        public RegMdlGrpModel RegMdlGrpModel { get; set; }
        [DataMember]
        public List<RegPgmBdlPkgComp> RegPgmBdlPkgComps { get; set; }
        [DataMember]
        public RegStatus RegStatus { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationUpdateReq
    {
        [DataMember]
        public DAL.Models.Registration Registration { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationUpdateMISMReq
    {
        [DataMember]
        public DAL.Models.RegistrationSec RegistrationSec { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationCancelReq
    {
        [DataMember]
        public DAL.Models.RegStatus RegStatus { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationPaymentSuccessReq
    {
        [DataMember]
        public DAL.Models.RegStatus RegStatus { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationPaymentFailedReq
    {
        [DataMember]
        public DAL.Models.RegStatus RegStatus { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationVerifyReq
    {
        [DataMember]
        public DAL.Models.Registration Registration { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationFindReq
    {
        [DataMember]
        public RegistrationFind RegistrationFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationSecGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationSubmitReq
    {
        //[DataMember]
        //public RegStatusHist OldRegStatus { get; set; }
        //[DataMember]
        //public RegStatusHist NewRegStatus { get; set; }
        //[DataMember]
        //public WorkOrder WorkOrder { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationSearchReq
    {
        [DataMember]
        public RegistrationSearchCriteria Criteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationCloseReq
    {
        [DataMember]
        public RegStatus RegStatus { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }
    [DataContract]
    [Serializable]
    public class GetPackageDetailsReq
    {
        [DataMember]
        public int IDs { get; set; }
    }
    [DataContract]
    [Serializable]
    public class GetPackageDetailsResp : WCFResponse
    {
        [DataMember]
        public DAL.Registration.SelectedPackageDetail PackageDetails { get; set; }
    }
    [DataContract]
    [Serializable]
    public class GetExistingPackageDetailsReq
    {
        [DataMember]
        public int IDs { get; set; }
    }
    [DataContract]
    [Serializable]
    public class GetExistingPackageDetailsResp : WCFResponse
    {
        [DataMember]
        public DAL.Registration.ExistingPackagePlanType ExistingPackageDetails { get; set; }
    }

    [DataContract]
    [Serializable]
    public class HomeRegistrationCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegSuppLineUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegSuppLineMultiUpdateReq
    {
        [DataMember]
        public List<DAL.Models.RegSuppLine> RegSuppLines { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationCancelResp : WCFResponse
    {

    }


    [DataContract]
    [Serializable]
    public class RegistrationPaymentSuccessResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegistrationPaymentFailedResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegistrationVerifyResp : WCFResponse { }

    [DataContract]
    [Serializable]
    public class RegistrationFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.Registration> Registrations { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationSecGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.RegistrationSec> Registrations { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationSubmitResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegistrationSearchResp : WCFResponse
    {
        [DataMember]
        public List<RegistrationSearchResult> RegistrationSearchResults { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AgentCodeByUserNameResp : WCFResponse
    {
        [DataMember]
        public AgentCodeByUserNameResult AgentCodeByUserNameResult { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationPendingOrdersResp : WCFResponse
    {
        [DataMember]
        public List<RegistrationPendingOrdersResults> RegistrationPendingOrdersResults { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationCloseResp : WCFResponse
    {
    }
    #region RegDeviceInfo


    [DataContract]
    [Serializable]
    public class RegDeviceInfoGetResp : WCFResponse
    {
        [DataMember]
        public DAL.Models.DeviceInfo RegDeviceInfo { get; set; }
    }
    #endregion
    #endregion

    #region RegMdlGrpModel

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelCreateReq
    {
        [DataMember]
        public DAL.Models.RegMdlGrpModel RegMdlGrpModel { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelUpdateReq
    {
        [DataMember]
        public DAL.Models.RegMdlGrpModel RegMdlGrpModel { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelCancelReq
    {
        [DataMember]
        public DAL.Models.RegMdlGrpModel RegMdlGrpModel { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelVerifyReq
    {
        [DataMember]
        public DAL.Models.RegMdlGrpModel RegMdlGrpModel { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelFindReq
    {
        [DataMember]
        public RegMdlGrpModelFind RegMdlGrpModelFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelSecFindReq
    {
        [DataMember]
        public RegMdlGrpModelSecFind RegMdlGrpModelSecFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelSecGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelSubmitReq
    {
        //[DataMember]
        //public RegStatusHist OldRegStatus { get; set; }
        //[DataMember]
        //public RegStatusHist NewRegStatus { get; set; }
        //[DataMember]
        //public WorkOrder WorkOrder { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelSearchReq
    {
        //[DataMember]
        //public RegMdlGrpModelSearchCriteria Criteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelCancelResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelVerifyResp : WCFResponse { }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelSecFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.RegMdlGrpModel> RegMdlGrpModels { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelSecGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.RegMdlGrpModelSec> RegMdlGrpModels { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelSubmitResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegMdlGrpModelSearchResp : WCFResponse
    {
        //[DataMember]
        //public List<RegMdlGrpModelSearch> RegMdlGrpModelSearchs { get; set; }
    }

    #endregion

    #region RegPgmBdlPkgComp

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompCreateReq
    {
        [DataMember]
        public DAL.Models.RegPgmBdlPkgComp RegPgmBdlPkgComp { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompUpdateReq
    {
        [DataMember]
        public DAL.Models.RegPgmBdlPkgComp RegPgmBdlPkgComp { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompCancelReq
    {
        [DataMember]
        public DAL.Models.RegPgmBdlPkgComp RegPgmBdlPkgComp { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompVerifyReq
    {
        [DataMember]
        public DAL.Models.RegPgmBdlPkgComp RegPgmBdlPkgComp { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompFindReq
    {
        [DataMember]
        public RegPgmBdlPkgCompFind RegPgmBdlPkgCompFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompSecFindReq
    {
        [DataMember]
        public RegPgmBdlPkgCompSecFind RegPgmBdlPkgCompSecFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompSecGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompSubmitReq
    {
        //[DataMember]
        //public RegStatusHist OldRegStatus { get; set; }
        //[DataMember]
        //public RegStatusHist NewRegStatus { get; set; }
        //[DataMember]
        //public WorkOrder WorkOrder { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompSearchReq
    {
        //[DataMember]
        //public RegPgmBdlPkgCompSearchCriteria Criteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompCancelResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompVerifyResp : WCFResponse { }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompSecFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.RegPgmBdlPkgComp> RegPgmBdlPkgComps { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompSecGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.RegPgmBdlPkgCompSec> RegPgmBdlPkgCompsSec { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgComponentGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.PgmBdlPckComponent> RegPgmBdlPkgComps { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompSubmitResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegPgmBdlPkgCompSearchResp : WCFResponse
    {
        //[DataMember]
        //public List<RegPgmBdlPkgCompSearch> RegPgmBdlPkgCompSearchs { get; set; }
    }

    #endregion

    #region RegSuppLine

    [DataContract]
    [Serializable]
    public class RegSuppLineCreateReq
    {
        [DataMember]
        public DAL.Models.RegSuppLine RegSuppLine { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSuppLineUpdateReq
    {
        [DataMember]
        public DAL.Models.RegSuppLine RegSuppLine { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSuppLineCancelReq
    {
        [DataMember]
        public DAL.Models.RegSuppLine RegSuppLine { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSuppLineVerifyReq
    {
        [DataMember]
        public DAL.Models.RegSuppLine RegSuppLine { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSuppLineFindReq
    {
        [DataMember]
        public RegSuppLineFind RegSuppLineFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSuppLineGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSuppLineSubmitReq
    {
        //[DataMember]
        //public RegStatusHist OldRegStatus { get; set; }
        //[DataMember]
        //public RegStatusHist NewRegStatus { get; set; }
        //[DataMember]
        //public WorkOrder WorkOrder { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSuppLineSearchReq
    {
        //[DataMember]
        //public RegSuppLineSearchCriteria Criteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSuppLineCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }
    

    [DataContract]
    [Serializable]
    public class RegSuppLineCancelResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegSuppLineVerifyResp : WCFResponse { }

    [DataContract]
    [Serializable]
    public class RegSuppLineFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSuppLineGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.RegSuppLine> RegSuppLines { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSuppLineSubmitResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class RegSuppLineSearchResp : WCFResponse
    {
        //[DataMember]
        //public List<RegSuppLineSearch> RegSuppLineSearchs { get; set; }
    }

    #endregion

    #region RegStatus

    [DataContract]
    [Serializable]
    public class RegStatusCreateReq
    {
        [DataMember]
        public RegStatus RegStatuses { get; set; }

        [DataMember]
        public DAL.Models.Registration Registrations { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegStatusFindReq
    {
        [DataMember]
        public RegStatusFind RegStatusFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegStatusGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegStatusLogGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegStatusSearchReq
    {
        [DataMember]
        public int RegID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegStatusCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegStatusFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegStatusGetResp : WCFResponse
    {
        [DataMember]
        public List<RegStatus> RegStatuses { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegStatusLogGetResp : WCFResponse
    {
        [DataMember]
        public List<RegStatusResult> RegStatusResult { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegStatusSearchResp : WCFResponse
    {
        [DataMember]
        public List<RegStatusResult> RegStatusResult { get; set; }
    }

    #endregion

    #region Biometric

    [DataContract]
    [Serializable]
    public class BiometricCreateReq
    {
        [DataMember]
        public DAL.Models.Biometrics Biometric { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BiometricUpdateReq
    {
        [DataMember]
        public DAL.Models.Biometrics Biometric { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BiometricFindReq
    {
        [DataMember]
        public BiometricFind BiometricFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BiometricGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BiometricCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BiometricUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class BiometricFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BiometricGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.Biometrics> Biometrics { get; set; }
    }

    #endregion

    #region RegAccount

    [DataContract]
    [Serializable]
    public class RegAccountGetReq
    {
        [DataMember]
        public List<int> RegIDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegAccountGetResp : WCFResponse
    {
        public RegAccountGetResp()
        {
            RegAccounts = new List<RegAccount>();
        }

        [DataMember]
        public List<RegAccount> RegAccounts { get; set; }
    }

    #endregion

    #region Payment Mode Payment Details

    [DataContract]
    [Serializable]
    public class PaymentModePaymentDetailsCreateReq
    {
        [DataMember]
        public PaymentModePaymentDetails PaymentModePaymentDetails { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModePaymentDetailsUpdateReq
    {
        [DataMember]
        public PaymentModePaymentDetails PaymentModePaymentDetails { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModePaymentDetailsFindReq
    {
        [DataMember]
        public PaymentModePaymentDetailsFind PaymentModePaymentDetailsFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModePaymentDetailsGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModePaymentDetailsCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModePaymentDetailsUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class PaymentModePaymentDetailsFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PaymentModePaymentDetailsGetResp : WCFResponse
    {
        [DataMember]
        public List<PaymentModePaymentDetails> PaymentModePaymentDetails { get; set; }
    }

    #endregion

    #region Card Type

    [DataContract]
    [Serializable]
    public class CardTypePaymentDetailsCreateReq
    {
        [DataMember]
        public CardTypePaymentDetails CardTypePaymentDetails { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypePaymentDetailsUpdateReq
    {
        [DataMember]
        public CardTypePaymentDetails CardTypePaymentDetails { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypePaymentDetailsFindReq
    {
        [DataMember]
        public CardTypePaymentDetailsFind CardTypePaymentDetailsFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypePaymentDetailsGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypePaymentDetailsCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypePaymentDetailsUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class CardTypePaymentDetailsFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CardTypePaymentDetailsGetResp : WCFResponse
    {
        [DataMember]
        public List<CardTypePaymentDetails> CardTypePaymentDetails { get; set; }
    }

    #endregion

    #region CustomerInfo

    [DataContract]
    [Serializable]
    public class CustomerInfoCreateReq
    {
        [DataMember]
        public CustomerInfo CustomerInfo { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerInfoUpdateReq
    {
        [DataMember]
        public CustomerInfo CustomerInfo { get; set; }

    }
    
    [DataContract]
    [Serializable]
    public class CustomerInfoCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CustomerInfoUpdateResp : WCFResponse
    {

    }
    #endregion


    //Spend Limit commented by Ravi as per new flow on june 15 2013
    #region "Spend Limit"
    [DataContract]
    [Serializable]
    public class LnkRegSpendLimitReq
    {
        [DataMember]
        public DAL.Models.lnkRegSpendLimit lnkRegSpendLimitDetails { get; set; }
    }
    #endregion

    //Added by VLT on 5 Mar 2013 for adding Kenan customer Info//
    
    #region KenanCustomerInfo
    [DataContract]
    [Serializable]
    public class KenanCustomerInfoCreateReq : WCFResponse
    {
        [DataMember]
        public KenamCustomerInfo KenanCustomerInfo { get; set; }
    }
    [DataContract]
    [Serializable]
    public class KenanCustomerInfoCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }
#endregion
    //Added by VLT on 5 Mar 2013 for adding Kenan customer Info//


    /*Chetan added for displaying the reson for failed transcation*/
    [DataContract]
    [Serializable]
    public class KenanLogDetailsGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.KenanaLogDetails> KenanaLogDetails { get; set; }
    }

    [DataContract]
    [Serializable]
    public class KenanLogDetailsGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }
    
    [DataContract]
    [Serializable]
    public class KenanLogHistoryDetailsGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    #region "Extra Ten"
    [DataContract]
    [Serializable]
    public class ExtraTenReq
    {
        [DataMember]
        public DAL.Models.lnkExtraTenDetails ExtraTenDetails { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExtraTenGetResp
    {
        [DataMember]
        public List<DAL.Models.lnkExtraTenDetails> ExtraTenDetails { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExtraTenLogReq
    {
        [DataMember]
        public DAL.Models.LnkExtraTenConfirmationLog ExtraTenConfirmationLog { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExtraTenAddEditReq
    {
        [DataMember]
        public DAL.Models.lnkExtraTenAddEditDetails ExtraTenAddEditDetails { get; set; }
    }
    #endregion

    [DataContract]
    [Serializable]
    public class RegistrationCancelReasonReq
    {
        [DataMember]
        public RegistrationCancelReason RegistrationCancelReason { get; set; }

    }

    [DataContract]
    [Serializable]
    public class RegistrationCancelReasonResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }


    [DataContract]
    [Serializable]
    public class LnkRegDetailsReq
    {
        [DataMember]
        public DAL.Models.LnkRegDetails LnkDetails { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegSupplinesMapReq
    {
        [DataMember]
        public List<DAL.Models.RegSuppLinesMap> RegSupplinesMap { get; set; }
    }


    [DataContract]
    [Serializable]
    public class SmartGetExistingPackageDetailsReq
    {
        [DataMember]
        public int IDs { get; set; }
    }
   
    [DataContract]
    [Serializable]
    public class SmartGetExistingPackageDetailsResp : WCFResponse
    {
        [DataMember]
        public DAL.Registration.ExistingPackagePlanType ExistingPackageDetails { get; set; }
    }

    [DataContract]
    [Serializable]
    public class GetActiveContractsListResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.Contracts> Contracts { get; set; }
    }

    #region CRP

    [DataContract]
    [Serializable]
    public class RegistrationCreateReq_CRP
    {
        [DataMember]
        public DAL.Models.Registration Registration { get; set; }
        [DataMember]
        public Customer Customer { get; set; }
        [DataMember]
        public List<Address> Addresses { get; set; }
        [DataMember]
        public RegMdlGrpModel RegMdlGrpModel { get; set; }
        [DataMember]
        public List<RegPgmBdlPkgComp> RegPgmBdlPkgComps { get; set; }
        [DataMember]
        public RegStatus RegStatus { get; set; }
        [DataMember]
        public List<RegSuppLine> RegSuppLines { get; set; }
        [DataMember]
        public List<RegPgmBdlPkgComp> RegSuppLineVASes { get; set; }
        [DataMember]
        public List<string> CustPhotos { get; set; }
        [DataMember]
        public List<PackageComponents> Packages { get; set; }
        [DataMember]
        public CMSSComplain CMSSID { get; set; }
        [DataMember]
        public List<RegSmartComponents> RegSmartComponents { get; set; }
        [DataMember]
        public List<WaiverComponents> RegWaiverComponents { get; set; }
        [DataMember]
        public bool isBREFail { get; set; }
    }

    #endregion

    #region MNP SUPPLEMENTARY LINES
    [DataContract]
    [Serializable]
    public class RegistrationMNPSupplementaryGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.RegSuppLine> Registrations { get; set; }
    }
    #endregion MNP SUPPLEMENTARY LINES

    [DataContract]
    [Serializable]
    public class SupervisorWaiveOffSearchReq
    {
        [DataMember]
        public RegistrationSearchCriteriaForWaiverOff Criteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SupervisorWaiveOffSearchResp : WCFResponse
    {
        [DataMember]
        public List<SearchResultForSuperVisorWaieverOff> RegistrationSearchResults { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AcknowledgedUsersReq
    {

        [DataMember]
        public List<RegDetails> AcknowledgedUsers { get; set; }
    }


    #region smart related methods

    

    [DataContract]
    [Serializable]
    public class RegistrationCreateReq_smart
    {
        [DataMember]
        public DAL.Models.Registration Registration { get; set; }
        [DataMember]
        public Customer Customer { get; set; }
        [DataMember]
        public List<Address> Addresses { get; set; }
        [DataMember]
        public RegMdlGrpModel RegMdlGrpModel { get; set; }
        [DataMember]
        public List<RegPgmBdlPkgComp> RegPgmBdlPkgComps { get; set; }
        [DataMember]
        public RegStatus RegStatus { get; set; }
        [DataMember]
        public List<RegSuppLine> RegSuppLines { get; set; }
        [DataMember]
        public List<RegPgmBdlPkgComp> RegSuppLineVASes { get; set; }
        [DataMember]
        public List<string> CustPhotos { get; set; }
        [DataMember]
        public List<PackageComponents> Packages { get; set; }
        [DataMember]
        public CMSSComplain CMSSID { get; set; }
        [DataMember]
        public List<RegSmartComponents> RegSmartComponents { get; set; }

        [DataMember]
      public  List<WaiverComponents> RegWaiverComponents { get; set; }

    }
    [DataContract]
    [Serializable]
    public class RegCancellationRes : WCFResponse
    {
        [DataMember]
        public List<RegistrationCancellationReasons> RegistrationCancellationReasons { get; set; }
    }

    [DataContract]
    [Serializable]
    public class WaiverRulesResp : WCFResponse
    {
        [DataMember]
        public List<WaiverRules> waiverRules { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SmartRegistrationCancelReasonReq
    {
        [DataMember]
        public RegistrationCancelReason RegistrationCancelReason { get; set; }

    }

    [DataContract]
    [Serializable]
    public class SmartRegistrationCancelReasonResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }
    [DataContract]
    [Serializable]
    public class SmartGetPackageDetailsResp : WCFResponse
    {
        [DataMember]
        public DAL.Registration.SelectedPackageDetail PackageDetails { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegistrationSimCardTypeFailedResp : WCFResponse
    {

    }

    #endregion

    [DataContract]
    [Serializable] 
    public class LnkLnkRegRebateDatacontractPenaltyReq
    {
        [DataMember]
        public List<DAL.Models.RegRebateDatacontractPenalty> rebatePenaltyDetails { get; set; } 
    }
   
    [DataContract]
    [Serializable]
    public class TacAuditLogReq
    {
        [DataMember]
        public TacAuditLog TacAuditLog { get; set; }
    }   

    [DataContract]
    [Serializable]
    public class TacAuditLogReqResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    #region ThirdPartyAuthType
    [DataContract]
    [Serializable]
    public class ThirdPartyAuthTypeCreateReq
    {
        [DataMember]
        public ThirdPartyAuthType ThirdPartyAuthType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ThirdPartyAuthTypeUpdateReq
    {
        [DataMember]
        public ThirdPartyAuthType ThirdPartyAuthType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ThirdPartyAuthTypeFindReq
    {
        [DataMember]
        public ThirdPartyAuthTypeFind ThirdPartyAuthTypeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ThirdPartyAuthTypeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ThirdPartyAuthTypeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ThirdPartyAuthTypeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ThirdPartyAuthTypeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ThirdPartyAuthTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<ThirdPartyAuthType> ThirdPartyAuthTypes { get; set; }
    }
    #endregion
}
