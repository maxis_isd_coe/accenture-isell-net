﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Runtime.Serialization;
using Online.Registration.DAL.ReportModels;

namespace Online.Registration.Service.Models
{
    #region Aging Report

    [DataContract]
    [Serializable]
    public class AgingReportGetReq
    {
        [DataMember]
        public AgingReportCriteria Criteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AgingReportGetResp : WCFResponse
    {
        [DataMember]
        public List<AgingReport> AgingReports { get; set; }
    }

    #endregion

    #region Biometrics Report

    [DataContract]
    [Serializable]
    public class BiometricsReportGetReq
    {
        [DataMember]
        public BiometricsReportCriteria Criteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BiometricsReportGetResp : WCFResponse
    {
        [DataMember]
        public List<BiometricsReport> BiometricsReports { get; set; }
    }

    #endregion

    #region Fail Transaction Report

    [DataContract]
    [Serializable]
    public class FailTransactionReportGetReq
    {
        [DataMember]
        public FailTransactionsSearchCriteria Criteria { get; set; }
    }

    [DataContract]
    [Serializable]
    public class FailTransactionReportGetResp : WCFResponse
    {
        [DataMember]
        public List<FailTransactionsSearchResult> FailTransactionReport { get; set; }
    }

    #endregion

    #region Registraton History
    [DataContract]
    [Serializable]
    public class RegHistoryGetResp : WCFResponse
    {
        [DataMember]
        public List<RegHistoryResult> RegHistoryResult { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RegHistoryGetReq
    {
        [DataMember]
        public int ID { get; set; }
    }
    #endregion
}