﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using Online.Registration.DAL.Models;

namespace Online.Registration.Service.Models
{
    #region User Group Access

    [DataContract]
    [Serializable]
    public class UserGrpAccessCreateReq
    {
        [DataMember]
        public UserGrpAccess UserGrpAccess { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpAccessUpdateReq
    {
        [DataMember]
        public UserGrpAccess UserGrpAccess { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpAccessFindReq
    {
        [DataMember]
        public UserGrpAccessFind UserGrpAccessFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpAccessGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpAccessCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpAccessUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class UserGrpAccessFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpAccessGetResp : WCFResponse
    {
        [DataMember]
        public List<UserGrpAccess> UserGrpAccesss { get; set; }
    }

    #endregion

    #region User Group Role

    [DataContract]
    [Serializable]
    public class UserGrpRoleCreateReq
    {
        [DataMember]
        public UserGrpRole UserGrpRole { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpRoleUpdateReq
    {
        [DataMember]
        public UserGrpRole UserGrpRole { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpRoleFindReq
    {
        [DataMember]
        public UserGrpRoleFind UserGrpRoleFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpRoleGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpRoleCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpRoleUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class UserGrpRoleFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGrpRoleGetResp : WCFResponse
    {
        [DataMember]
        public List<UserGrpRole> UserGrpRoles { get; set; }
    }

    #endregion

    #region Org Type

    [DataContract]
    [Serializable]
    public class OrgTypeCreateReq
    {
        [DataMember]
        public OrgType OrgType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class OrgTypeUpdateReq
    {
        [DataMember]
        public OrgType OrgType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class OrgTypeFindReq
    {
        [DataMember]
        public OrgTypeFind OrgTypeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class OrgTypeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class OrgTypeCreateResp : WCFResponse
    {
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class OrgTypeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class OrgTypeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class OrgTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<OrgType> OrgTypes { get; set; }
    }

    #endregion

    #region User Group

    [DataContract]
    [Serializable]
    public class UserGroupCreateReq
    {
        [DataMember]
        public UserGroup UserGroup { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGroupUpdateReq
    {
        [DataMember]
        public UserGroup UserGroup { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGroupFindReq
    {
        [DataMember]
        public UserGroupFind UserGroupFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGroupGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGroupCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGroupUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class UserGroupFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGroupGetResp : WCFResponse
    {
        [DataMember]
        public List<UserGroup> UserGroups { get; set; }
    }

    #endregion

    #region User Role

    [DataContract]
    [Serializable]
    public class UserRoleCreateReq
    {
        [DataMember]
        public UserRole UserRole { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserRoleUpdateReq
    {
        [DataMember]
        public UserRole UserRole { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserRoleFindReq
    {
        [DataMember]
        public UserRoleFind UserRoleFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserRoleGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserRoleCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserRoleUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class UserRoleFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserRoleGetResp : WCFResponse
    {
        [DataMember]
        public List<UserRole> UserRoles { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UsersInRoleFindResp : WCFResponse
    {
        [DataMember]
        public string[] Users { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UsersInRoleFindReq
    {
        [DataMember]
        public string RoleCode { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public bool Active { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RolesForUserGetResp : WCFResponse
    {
        [DataMember]
        public string[] Roles { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RolesForUserGetReq
    {
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public bool Active { get; set; }
    }

    #endregion

    #region Access

    [DataContract]
    [Serializable]
    public class AccessCreateReq
    {
        [DataMember]
        public Access Access { get; set; }
        [DataMember]
        public List<int> UserGroupIDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccessUpdateReq
    {
        [DataMember]
        public Access Access { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccessFindReq
    {
        [DataMember]
        public AccessFind AccessFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccessGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccessCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccessUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class AccessFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccessGetResp : WCFResponse
    {
        [DataMember]
        public List<Access> Accesss { get; set; }
    }

    #endregion

    #region Organization

    [DataContract]
    [Serializable]
    public class OrganizationCreateReq
    {
        [DataMember]
        public Organization Organization { get; set; }
    }
    [DataContract]
    [Serializable]
    public class OrganizationUpdateReq
    {
        [DataMember]
        public Organization Organization { get; set; }
    }
    [DataContract]
    [Serializable]
    public class OrganizationGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }
    [DataContract]
    [Serializable]
    public class OrganizationFindReq
    {
        [DataMember]
        public OrganizationFind OrganizationFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class OrganizationCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }
    [DataContract]
    [Serializable]
    public class OrganizationUpdateResp : WCFResponse
    {
    }
    [DataContract]
    [Serializable]
    public class OrganizationGetResp : WCFResponse
    {
        [DataMember]
        public List<Organization> Organizations { get; set; }
    }
    [DataContract]
    [Serializable]
    public class OrganizationFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    #endregion

    #region User

    [DataContract]
    [Serializable]
    public class UserCreateReq
    {
        [DataMember]
        public User User { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserUpdateReq
    {
        [DataMember]
        public User User { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserFindReq
    {
        [DataMember]
        public UserFind UserFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserFindbyNameReq
    {
        [DataMember]
        public UserFindbyName UserFindbyName { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class UserFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserGetResp : WCFResponse
    {
        [DataMember]
        public List<User> Users { get; set; }
    }

    #endregion

    #region Market

    #region Create

    [DataContract]
    [Serializable]
    public class MarketCreateReq
    {
        [DataMember]
        public Market Market { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MarketCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    #endregion

    #region Update

    [DataContract]
    [Serializable]
    public class MarketUpdateReq
    {
        [DataMember]
        public Market Market { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MarketUpdateResp : WCFResponse { }

    #endregion

    #region Find

    [DataContract]
    [Serializable]
    public class MarketFindReq
    {
        [DataMember]
        public MarketFind MarketFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MarketFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }
    [DataContract]
    [Serializable]
    public class MarketSearchResp : WCFResponse
    {
        [DataMember]
        public List<Market> Markets { get; set; }
    }
    #endregion

    #region Get

    [DataContract]
    [Serializable]
    public class MarketGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MarketGetResp : WCFResponse
    {
        [DataMember]
        public List<Market> Markets { get; set; }
    }

    #endregion

#endregion

    #region Account Category

    #region Create

    [DataContract]
    [Serializable]
    public class AccountCategoryCreateReq
    {
        [DataMember]
        public AccountCategory AccountCategory { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccountCategoryCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    #endregion

    #region Update

    [DataContract]
    [Serializable]
    public class AccountCategoryUpdateReq
    {
        [DataMember]
        public AccountCategory AccountCategory { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccountCategoryUpdateResp : WCFResponse { }

    #endregion

    #region Find

    [DataContract]
    [Serializable]
    public class AccountCategoryFindReq
    {
        [DataMember]
        public AccountCategoryFind AccountCategoryFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccountCategoryFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    #endregion

    #region Get

    [DataContract]
    [Serializable]
    public class AccountCategoryGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AccountCategoryGetResp : WCFResponse
    {
        [DataMember]
        public List<AccountCategory> AccountCategories { get; set; }
    }

    #endregion

#endregion

    #region Status
    [DataContract]
    [Serializable]
    public class StatusCreateReq
    {
        [DataMember]
        public Status Status { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusUpdateReq
    {
        [DataMember]
        public Status Status { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusFindReq
    {
        [DataMember]
        public StatusFind StatusFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class StatusFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusGetResp : WCFResponse
    {
        [DataMember]
        public List<Status> Statuss { get; set; }
    }
    #endregion

    #region StatusType
    [DataContract]
    [Serializable]
    public class StatusTypeCreateReq
    {
        [DataMember]
        public StatusType StatusType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusTypeUpdateReq
    {
        [DataMember]
        public StatusType StatusType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusTypeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusTypeFindReq
    {
        [DataMember]
        public StatusTypeFind StatusTypeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusTypeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusTypeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class StatusTypeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<StatusType> StatusTypes { get; set; }
    }
    #endregion

    #region Status Change
    [DataContract]
    [Serializable]
    public class StatusChangeCreateReq
    {
        [DataMember]
        public StatusChange StatusChange { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusChangeUpdateReq
    {
        [DataMember]
        public StatusChange StatusChange { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusChangeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusChangeFindReq
    {
        [DataMember]
        public StatusChangeFind StatusChangeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusChangeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusChangeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class StatusChangeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusChangeGetResp : WCFResponse
    {
        [DataMember]
        public List<StatusChange> StatusChanges { get; set; }
    }
    #endregion

    #region Status Reason
    [DataContract]
    [Serializable]
    public class StatusReasonCreateReq
    {
        [DataMember]
        public StatusReason StatusReason { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonUpdateReq
    {
        [DataMember]
        public StatusReason StatusReason { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonFindReq
    {
        [DataMember]
        public StatusReasonFind StatusReasonFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class StatusReasonFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonGetResp : WCFResponse
    {
        [DataMember]
        public List<StatusReason> StatusReasons { get; set; }
    }
    #endregion

    #region Status Reason Code
    [DataContract]
    [Serializable]
    public class StatusReasonCodeCreateReq
    {
        [DataMember]
        public StatusReasonCode StatusReasonCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonCodeUpdateReq
    {
        [DataMember]
        public StatusReasonCode StatusReasonCode { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonCodeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonCodeFindReq
    {
        [DataMember]
        public StatusReasonCodeFind StatusReasonCodeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonCodeIDsFindReq
    {
        
    }

    [DataContract]
    [Serializable]
    public class StatusReasonCodeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonCodeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class StatusReasonCodeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonCodeIDsFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class StatusReasonCodeGetResp : WCFResponse
    {
        [DataMember]
        public List<StatusReasonCode> StatusReasonCodes { get; set; }
    }
    #endregion

    #region VLT ADDED CODE
    #region Apistatusandmessages

    [DataContract]
    [Serializable]
    public class GetAPIStatusMessagesResp : WCFResponse
    {
        [DataMember]
        public List<tblAPIStatusMessages> tblAPIStatusMessageslst { get; set; }
    }

    [DataContract]
    [Serializable]
    public class GetDonerMessagesResp : WCFResponse
    {
        [DataMember]
        public List<tblDonor> tblDonorlst { get; set; }
    }

    [DataContract]
    [Serializable]
    public class GetAPIStatusMessagesReq
    {
        [DataMember]
        public tblAPIStatusMessages tblAPIStatusMessagesval { get; set; }
    }
    #endregion
    #endregion API call save

    #region VLT ADDED CODE
    #region API call save
    [DataContract]
    [Serializable]
    public class APISaveCallStatusResp : WCFResponse
    {
        [DataMember]
        public int? APIID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class APISaveCallStatusReq
    {
        [DataMember]
        public tblAPICallStatus tblAPICallStatusMessagesval { get; set; }
    }
    #endregion API call save

    #region Plan Search
    [DataContract]
    [Serializable]
    public class PlanSearchItemResp : WCFResponse
    {
        [DataMember]
        public string VoiceUsage { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PlanSearchItemReq
    {
        [DataMember]
        public string planSearchVal { get; set; }
    }
    #endregion Plan Search
    #endregion VLT ADDED CODE

    #region VLT ADDED CODE by Rajeswari on 25th Feb for logging user information

    [DataContract]
    [Serializable]
    public class UserTransactionLogResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class UserTransactionLogReq
    {
        [DataMember]
        public UserTransactionLog UserInfoVal { get; set; }
    }

    #endregion

    #region smart related methods

    [DataContract]
    [Serializable]
    public class TransactionTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<TransactionTypes> Statuss { get; set; }
    }

    #endregion
}