﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace Online.Registration.Service.Models.Common
{
    public abstract class ServiceBase
    {
        readonly ILog logger = null;
        string svcName;

        public ServiceBase()
        {
            svcName = this.GetType().Name;
            logger = LogManager.GetLogger(svcName);
        }
        protected void LogError(Exception ex, params string[] para)
        {
            logger.ErrorFormat("Error in {0}.{1} | Msg : {2} | StackTacek : {3} | Param : {4}", svcName, GetCurrentMethod(), ex.Message, ex.StackTrace, string.Join(",", para));
        }
        protected void LogInfoEnd(params string[] para)
        {
            logger.InfoFormat("End {0}.{1} | Param : {2} ", svcName, GetCurrentMethod(), string.Join(",", para));
        }
        protected void LogInfoStart(params string[] para)
        {
            logger.InfoFormat("Start {0}.{1} | Param : {2}", svcName, GetCurrentMethod(), string.Join(",", para));
        }
        [MethodImpl(MethodImplOptions.NoInlining)]
        protected string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(2);

            return sf.GetMethod().Name;
        }
    }
}