﻿using Online.Registration.DAL.Models.ServiceRequest;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Online.Registration.Service.Models.Common
{
    [KnownType(typeof(List<SRQueryMaster>))]
    [KnownType(typeof(SRQueryMaster))]
    [CollectionDataContract]
    public class CollectionContract:List<object>
    {
        public CollectionContract()
        {

        }

        public CollectionContract(IEnumerable lst)
        {
            var newList = lst.Cast<object>().ToList();
            AddRange(newList);
        }
    }
}