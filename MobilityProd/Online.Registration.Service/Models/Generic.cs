﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.IO;

namespace Online.Registration.Service.Models
{
    [DataContract]
    [Serializable]
    //[KnownType(typeof(InventoryGetResp))]
    public class WCFResponse
    {
        public WCFResponse()
        {
            Code = Properties.Settings.Default.SuccessCode;
            Message = Properties.Settings.Default.SuccessMessage;
        }

        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string StackTrace { get; set; }
    }

    [DataContract]
    [Serializable]
    public class FindResponse : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class GetRequest
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    public class Util
    {
        public static string ConvertObjectToXml(object objInput)
        {
            //System.Xml.Serialization.XmlSerializer objSer = new System.Xml.Serialization.XmlSerializer(objInput.GetType());
            //System.IO.StringWriter objStringWriter = new System.IO.StringWriter();
            try
            {
                //objSer.Serialize(objStringWriter, objInput);
                //return objStringWriter.ToString();
                #region "testing purpose to test the performance"

                string serializedContent = string.Empty;
                DataContractSerializer serializer = new DataContractSerializer(objInput.GetType());
                using (MemoryStream ms = new MemoryStream())
                {
                    serializer.WriteObject(ms, objInput);
                    ms.Position = 0;

                    using (StreamReader sr = new StreamReader(ms))
                    {
                        serializedContent = sr.ReadToEnd();
                    }
                }
                serializer = null;
                return serializedContent;

                #endregion


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //objStringWriter.Close();
            }
        }
        public static string LogException(Exception objException)
        {
            System.Text.StringBuilder strException = new System.Text.StringBuilder();
            strException.AppendFormat("Exception Found:\n{0}", objException.GetType().FullName);
            strException.AppendFormat("\n{0}", objException.Message);
            strException.AppendFormat("\n{0}", objException.Source);
            strException.AppendFormat("\n{0}", objException.StackTrace);
            if (objException.InnerException != null)
            {
                strException.AppendFormat("Inner Exception Found:\n{0}", objException.InnerException.GetType().FullName);
                strException.AppendFormat("\n{0}", objException.InnerException.Message);
                strException.AppendFormat("\n{0}", objException.InnerException.Source);
                strException.AppendFormat("\n{0}", objException.InnerException.StackTrace);
            }
            return strException.ToString();

        }
    }
}