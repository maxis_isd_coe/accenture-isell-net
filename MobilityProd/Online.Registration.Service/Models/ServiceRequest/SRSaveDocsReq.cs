﻿using Online.Registration.DAL.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.Service.Models.ServiceRequest
{
    [Serializable]
    [DataContract]
    public class SRSaveDocsReq : SRServiceReq
    {
        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public List<VerificationDoc> Docs { get; set; }
    }
}
