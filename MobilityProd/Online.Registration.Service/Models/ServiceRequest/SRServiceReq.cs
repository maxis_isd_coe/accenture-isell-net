﻿using Online.Registration.DAL.Models.ServiceRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Online.Registration.Service.Models.ServiceRequest
{
    [Serializable]
    [DataContract]
    public class SRServiceReq
    {
        [DataMember]
        public DateTime RequestDt { get; private set; }

        public SRServiceReq()
        {
            RequestDt = DateTime.Now;
        }
    }
}