﻿using Online.Registration.DAL.Models.ServiceRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Online.Registration.Service.Models.ServiceRequest
{
    [Serializable]
    [DataContract]
    public class SRSaveOrderReq:SRServiceReq
    {
        [DataMember]
        public SROrderMaster Order { get; set; }
    }
}