﻿using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.Service.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Online.Registration.Service.Models.ServiceRequest
{
    /// <summary>
    /// ServiceResp object, 
    /// Use set all possible know type here to successfull serialize
    /// </summary>
    [KnownType(typeof(SROrderCorpSimRepDtl))]
    [KnownType(typeof(SROrderMaster))]
    [KnownType(typeof(SRQueryMaster))]
    [KnownType(typeof(CollectionContract))]
    [Serializable]
    [DataContract]
    public class SRServiceResp:WCFResponse
    {
        [DataMember]
        public object Content { get; set; }

        public void FillException(Exception ex)
        {
            Code = "0";
            StackTrace = ex.StackTrace;
            Message = ex.Message;
        }
    }
} 