﻿using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.Service.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Online.Registration.Service.Models.ServiceRequest
{
    [KnownType(typeof(SRQueryMaster))]
    [Serializable]
    [DataContract]
    public class SRQueryOrderResp:SRServiceResp
    {
        [DataMember]
        public int TotalCount { get; set; }
    }
}