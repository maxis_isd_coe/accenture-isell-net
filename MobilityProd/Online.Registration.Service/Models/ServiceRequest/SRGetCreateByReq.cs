﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Online.Registration.Service.Models.ServiceRequest
{
    [Serializable]
    [DataContract]
    public class SRGetCreateByReq : SRServiceReq
    {
        [DataMember]
        public int CenterOrgId { get; set; }

        [DataMember]
        public string LoginId { get; set; }
    }
}