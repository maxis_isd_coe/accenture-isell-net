﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.DAL.Models.ServiceRequest.Dto;

namespace Online.Registration.Service.Models.ServiceRequest
{
    [Serializable]
    [DataContract]
    public class SRUpdateStatusReq:SRServiceReq
    {
        [DataMember]
        public SRUpdateStatusDto UpdateContent { get; set; }
    }
}
