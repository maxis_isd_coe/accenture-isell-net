﻿using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Models.FilterCriterias;
using Online.Registration.DAL.Models.ServiceRequest.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.Service.Models.ServiceRequest
{
    [KnownType(typeof(IFilterCriteria))]
    [KnownType(typeof(FilterCriteria))]
    [KnownType(typeof(TodayFilterCriteria))]
    [KnownType(typeof(StartDateFilterCriteria))]
    [KnownType(typeof(EndDateFilterCriteria))]
    [Serializable]
    [DataContract]
    public class SRQueryOrderReq:SRServiceReq
    {
        [DataMember]
        public SRQueryOrderDto QueryDto { get; set; }

        [DataMember]
        public int page { get; set; }

        [DataMember]
        public int pageSize { get; set; }
    }
}
