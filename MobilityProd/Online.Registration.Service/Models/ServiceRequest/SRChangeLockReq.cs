﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Online.Registration.Service.Models.ServiceRequest
{
    [Serializable]
    [DataContract]
    public class SRChangeLockReq : SRServiceReq
    {
        [DataMember]
        public string OrderId { get; set; }

        [DataMember]
        public string LockBy { get; set; }
    }
}
