﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using Online.Registration.DAL.Models;

namespace Online.Registration.Service.Models
{
    #region Program

    [DataContract]
    [Serializable]
    public class ProgramCreateReq
    {
        [DataMember]
        public DAL.Models.Program Program { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ProgramUpdateReq
    {
        [DataMember]
        public DAL.Models.Program Program { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ProgramFindReq
    {
        [DataMember]
        public ProgramFind ProgramFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ProgramGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ProgramCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ProgramUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ProgramFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ProgramGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.Program> Programs { get; set; }
    }

    #endregion

    #region Brand
    [DataContract]
    [Serializable]
    public class BrandCreateReq
    {
        [DataMember]
        public Brand Brand { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BrandUpdateReq
    {
        [DataMember]
        public Brand Brand { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BrandGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }
    [DataContract]
    [Serializable]
    public class BrandGetReqSmart
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BrandFindReq
    {
        [DataMember]
        public BrandFind BrandFind { get; set; }
    }
    [DataContract]
    [Serializable]
    public class BrandFindReqSmart
    {
        [DataMember]
        public BrandFindSmart BrandFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BrandCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BrandUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class BrandFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }
    [DataContract]
    [Serializable]
    public class BrandFindRespSmart : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BrandGetResp : WCFResponse
    {
        [DataMember]
        public List<Brand> Brands { get; set; }
    }
    [DataContract]
    [Serializable]
    public class BrandGetRespSmart : WCFResponse
    {
        [DataMember]
        public List<BrandSmart> Brands { get; set; }
    }
    #endregion

    #region Bundle

    [DataContract]
    [Serializable]
    public class BundleCreateReq
    {
        [DataMember]
        public DAL.Models.Bundle Bundle { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BundleUpdateReq
    {
        [DataMember]
        public DAL.Models.Bundle Bundle { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BundleFindReq
    {
        [DataMember]
        public BundleFind BundleFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BundleGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BundleCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BundleUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class BundleFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BundleGetResp : WCFResponse
    {
        [DataMember]
        public List<Bundle> Bundles { get; set; }
    }

    #endregion

    #region Colour
    [DataContract]
    [Serializable]
    public class ColourCreateReq
    {
        [DataMember]
        public Colour Colour { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ColourUpdateReq
    {
        [DataMember]
        public Colour Colour { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ColourFindReq
    {
        [DataMember]
        public ColourFind ColourFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ColourGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ColourCreateResp : WCFResponse
    {
        [DataMember]
        public int ColourID { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ColourUpdateResp : WCFResponse
    {
    }

    [DataContract]
    [Serializable]
    public class ColourFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ColourGetResp : WCFResponse
    {
        [DataMember]
        public List<Colour> Colours { get; set; }
    }
    #endregion

    #region Category
    [DataContract]
    [Serializable]
    public class DataPlanIdsResp : WCFResponse
    {
        [DataMember]
        public List<PkgDataPlanId> values { get; set; }
    }
    [DataContract]
    [Serializable]
    public class BundlepackageResp : WCFResponse
    {
        [DataMember]
        public List<PkgData> values { get; set; }
    }
    [DataContract]
    [Serializable]
    public class CategoryCreateReq
    {
        [DataMember]
        public Category Category { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageComponentsResp : WCFResponse
    {
        [DataMember]
        public string ComponentsZippedData { get; set; }
    }


    [DataContract]
    [Serializable]
    public class CategoryUpdateReq
    {
        [DataMember]
        public Category Category { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CategoryGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CategoryFindReq
    {
        [DataMember]
        public CategoryFind CategoryFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CategoryCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CategoryUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class CategoryFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CategoryGetResp : WCFResponse
    {
        [DataMember]
        public List<Category> Categorys { get; set; }
    }
    #endregion

    #region Package

    [DataContract]
    [Serializable]
    public class PackageCreateReq
    {
        [DataMember]
        public Package Package { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageUpdateReq
    {
        [DataMember]
        public Package Package { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageFindReq
    {
        [DataMember]
        public PackageFind PackageFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class PackageFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageGetResp : WCFResponse
    {
        [DataMember]
        public List<Package> Packages { get; set; }
    }

    #endregion

    

    #region Package Type

    [DataContract]
    [Serializable]
    public class PackageTypeCreateReq
    {
        [DataMember]
        public PackageType PackageType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageTypeUpdateReq
    {
        [DataMember]
        public PackageType PackageType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageTypeFindReq
    {
        [DataMember]
        public PackageTypeFind PackageTypeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageTypeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageTypeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageTypeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class PackageTypeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PackageTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<PackageType> PackageTypes { get; set; }
    }

    #endregion

    #region Component
    [DataContract]
    [Serializable]
    public class ComponentCreateReq
    {
        [DataMember]
        public Component Component { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentUpdateReq
    {
        [DataMember]
        public Component Component { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentFindReq
    {
        [DataMember]
        public ComponentFind ComponentFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ContractComponentGetReq
    {

    }

    [DataContract]
    [Serializable]
    public class ComponentCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ComponentFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentGetResp : WCFResponse
    {
        [DataMember]
        public List<Component> Components { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ContractComponentGetResp : WCFResponse
    {
        [DataMember]
        public List<Component> Components { get; set; }
    }

    #endregion

    #region Component Type
    [DataContract]
    [Serializable]
    public class ComponentTypeCreateReq
    {
        [DataMember]
        public ComponentType ComponentType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentTypeUpdateReq
    {
        [DataMember]
        public ComponentType ComponentType { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentTypeGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentTypeFindReq
    {
        [DataMember]
        public ComponentTypeFind ComponentTypeFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentTypeCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentTypeUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ComponentTypeFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ComponentTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<ComponentType> ComponentTypes { get; set; }
    }

    #endregion

    #region Model
    [DataContract]
    [Serializable]
    public class ModelCreateReq
    {
        [DataMember]
        public Model Model { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelUpdateReq
    {
        [DataMember]
        public Model Model { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelFindReq
    {
        [DataMember]
        public ModelFind ModelFind { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ModelFindReqSmart
    {
        [DataMember]
        public ModelFindSmart ModelFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ModelFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGetResp : WCFResponse
    {
        [DataMember]
        public List<Model> Models { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ModelGetRespSmart : WCFResponse
    {
        [DataMember]
        public List<ModelSmart> Models { get; set; }
    }
    #endregion

    #region ModelImage
    [DataContract]
    [Serializable]
    public class ModelImageCreateReq
    {
        [DataMember]
        public ModelImage ModelImage { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelImageUpdateReq
    {
        [DataMember]
        public ModelImage ModelImage { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelImageGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelImageFindReq
    {
        [DataMember]
        public ModelImageFind ModelImageFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelImageCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelImageUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ModelImageFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelImageGetResp : WCFResponse
    {
        [DataMember]
        public List<ModelImage> ModelImages { get; set; }
    }
    #endregion

    #region Model Group
    [DataContract]
    [Serializable]
    public class ModelGroupCreateReq
    {
        [DataMember]
        public ModelGroup ModelGroup { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupUpdateReq
    {
        [DataMember]
        public ModelGroup ModelGroup { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupFindReq
    {
        [DataMember]
        public ModelGroupFind ModelGroupFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ModelGroupGetResp : WCFResponse
    {
        [DataMember]
        public List<ModelGroup> ModelGroups { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    #endregion

    #region Program Bundle Package Component
    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentCreateReq
    {
        [DataMember]
        public PgmBdlPckComponent PgmBdlPckComponent { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentUpdateReq
    {
        [DataMember]
        public PgmBdlPckComponent PgmBdlPckComponent { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PkgCompContractGetReq
    {

    }

    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentFindReq
    {
        [DataMember]
        public PgmBdlPckComponentFind PgmBdlPckComponentFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentGetResp : WCFResponse
    {
        [DataMember]
        public List<PgmBdlPckComponent> PgmBdlPckComponents { get; set; }
    }

    [DataContract]
    [Serializable]
    public class MISMMandatoryComponentGetResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.MISMMandatoryComponents> MISMComps { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PkgCompContractGetResp : WCFResponse
    {
        [DataMember]
        public Dictionary<string, string> PgmBdlPckComponents { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BdlPkgGetResp : WCFResponse
    {
        [DataMember]
        public Dictionary<string, string> PgmBdlPckComponents { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }
    #endregion

    #region Model Group Model
    [DataContract]
    [Serializable]
    public class ModelGroupModelCreateReq
    {
        [DataMember]
        public ModelGroupModel ModelGroupModel { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupModelUpdateReq
    {
        [DataMember]
        public ModelGroupModel ModelGroupModel { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupModelGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupModelFindReq
    {
        [DataMember]
        public ModelGroupModelFind ModelGroupModelFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupModelCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupModelUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ModelGroupModelGetResp : WCFResponse
    {
        [DataMember]
        public List<ModelGroupModel> ModelGroupModels { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelGroupModelFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }
    #endregion

    #region Model Part No

    [DataContract]
    [Serializable]
    public class ModelPartNoCreateReq
    {
        [DataMember]
        public ModelPartNo ModelPartNo { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelPartNoUpdateReq
    {
        [DataMember]
        public ModelPartNo ModelPartNo { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelPartNoFindReq
    {
        [DataMember]
        public ModelPartNoFind ModelPartNoFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelPartNoGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelPartNoCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelPartNoUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ModelPartNoFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ModelPartNoGetResp : WCFResponse
    {
        [DataMember]
        public List<ModelPartNo> ModelPartNos { get; set; }
    }

    #endregion

    #region Item Price
    [DataContract]
    [Serializable]
    public class ItemPriceCreateReq
    {
        [DataMember]
        public ItemPrice ItemPrice { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ItemPriceUpdateReq
    {
        [DataMember]
        public ItemPrice ItemPrice { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ItemPriceGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ItemPriceFindReq
    {
        [DataMember]
        public ItemPriceFind ItemPriceFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ItemPriceCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ItemPriceUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class ItemPriceFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ItemPriceGetResp : WCFResponse
    {
        [DataMember]
        public List<ItemPrice> ItemPrices { get; set; }
    }
    #endregion

    #region Advance & Deposit Price for Device
    //added by Deepika
    [DataContract]
    [Serializable]
    public class AdvDepositPriceCreateReq
    {
        [DataMember]
        public AdvDepositPrice AdvDepositPrice { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AdvDepositPriceUpdateReq
    {
        [DataMember]
        public AdvDepositPrice AdvDepositPrice { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AdvDepositPriceGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    public class AdvDepositPriceGetReqStr
    {
        [DataMember]
        public List<string> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AdvDepositPriceCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AdvDepositPriceUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class AdvDepositPriceFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AdvDepositPriceGetResp : WCFResponse
    {
        [DataMember]
        public List<AdvDepositPrice> AdvDepositPrices { get; set; }
    }
    //end Deepika
    #endregion



    #region Property
    [DataContract]
    [Serializable]
    public class PropertyCreateReq
    {
        [DataMember]
        public Property Property { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyUpdateReq
    {
        [DataMember]
        public Property Property { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyFindReq
    {
        [DataMember]
        public PropertyFind PropertyFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class PropertyFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyGetResp : WCFResponse
    {
        [DataMember]
        public List<Property> Properties { get; set; }
    }
    #endregion

    #region PropertyValue
    [DataContract]
    [Serializable]
    public class PropertyValueCreateReq
    {
        [DataMember]
        public PropertyValue PropertyValue { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyValueUpdateReq
    {
        [DataMember]
        public PropertyValue PropertyValue { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyValueGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyValueFindReq
    {
        [DataMember]
        public PropertyValueFind PropertyValueFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyValueCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyValueUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class PropertyValueFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PropertyValueGetResp : WCFResponse
    {
        [DataMember]
        public List<PropertyValue> PropertyValues { get; set; }
    }
    #endregion

    #region PlanProperty
    [DataContract]
    [Serializable]
    public class PlanPropertyCreateReq
    {
        [DataMember]
        public PlanProperty PlanProperty { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PlanPropertyUpdateReq
    {
        [DataMember]
        public PlanProperty PlanProperty { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PlanPropertyGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PlanPropertyFindReq
    {
        [DataMember]
        public PlanPropertyFind PlanPropertyFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PlanPropertyCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PlanPropertyUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class PlanPropertyFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PlanPropertyGetResp : WCFResponse
    {
        [DataMember]
        public List<PlanProperty> PlanProperties { get; set; }
    }
    #endregion

    #region DeviceProperty
    [DataContract]
    [Serializable]
    public class DevicePropertyCreateReq
    {
        [DataMember]
        public DeviceProperty DeviceProperty { get; set; }
    }

    [DataContract]
    [Serializable]
    public class DevicePropertyUpdateReq
    {
        [DataMember]
        public DeviceProperty DeviceProperty { get; set; }
    }

    [DataContract]
    [Serializable]
    public class DevicePropertyGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class DevicePropertyFindReq
    {
        [DataMember]
        public DevicePropertyFind DevicePropertyFind { get; set; }
    }

    [DataContract]
    [Serializable]
    public class DevicePropertyCreateResp : WCFResponse
    {
        [DataMember]
        public int ID { get; set; }
    }

    [DataContract]
    [Serializable]
    public class DevicePropertyUpdateResp : WCFResponse
    {

    }

    [DataContract]
    [Serializable]
    public class DevicePropertyFindResp : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class DevicePropertyGetResp : WCFResponse
    {
        [DataMember]
        public List<DeviceProperty> DeviceProperties { get; set; }
    }
    #endregion

    #region BrandArticle

    [DataContract]
    [Serializable]
    public class ArticleIDGetResp : WCFResponse
    {
        [DataMember]
        public List<BrandArticle> ArticleIDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ArticleIDGetReq
    {
        [DataMember]
        public List<BrandArticle> ArticleID { get; set; }
    }
    [DataContract]
    [Serializable]
    public class BrandArticleImageGetResp : WCFResponse
    {
        [DataMember]
        public List<BrandArticle> BarndArticleImages { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BrandArticleModelImageGetReq
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    #endregion
    #region BrandArticleSmart

    [DataContract]
    [Serializable]
    public class ArticleIDGetRespSmart : WCFResponse
    {
        [DataMember]
        public List<BrandArticleSmart> ArticleIDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ArticleIDGetReqSmart
    {
        [DataMember]
        public List<BrandArticleSmart> ArticleID { get; set; }
    }
    [DataContract]
    [Serializable]
    public class BrandArticleImageGetRespSmart : WCFResponse
    {
        [DataMember]
        public List<BrandArticleSmart> BarndArticleImages { get; set; }
    }

    [DataContract]
    [Serializable]
    public class BrandArticleModelImageGetReqSmart
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    #endregion

    #region IMPOSDetails
    [DataContract]
    [Serializable]
    public class IMPOSDetailsResp
    {
        [DataMember]
        public Int32 RegID { get; set; }
        [DataMember]
        public string ArticleID { get; set; }
        [DataMember]
        public string MSISDN1 { get; set; }
        [DataMember]
        public string KenanAccountNo { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public string Line1 { get; set; }
        [DataMember]
        public string Line2 { get; set; }
        [DataMember]
        public string Line3 { get; set; }
        [DataMember]

        public string Town { get; set; }
        [DataMember]
        public string Postcode { get; set; }
        [DataMember]
        public string IMEINumber { get; set; }
        [DataMember]
        public int ModelID { get; set; }
        [DataMember]
        public string UOMCODE { get; set; }
        [DataMember]
        public string SimNo { get; set; }

    }
    #endregion

    #region IMPOSDetails
    [DataContract]
    [Serializable]
    public class WhiteListDetailsResp
    {

        [DataMember]
        public string Description { get; set; }


    }
    #endregion

    //[DataContract]
    //[Serializable]
    //public class SimModelTypeFindReq
    //{
    //    [DataMember]
    //    public SimModelTypeFind SimModelTypeFind { get; set; }
    //}

    #region SimModel Added by VLT 09 Apr 2013
    [DataContract]
    [Serializable]
    public class SimModelTypeGetResp : WCFResponse
    {
        [DataMember]
        public List<SimModels> SimModels { get; set; }
    }
    #endregion

    #region SimReplacement Added by VLT 17 June 2013
    [DataContract]
    [Serializable]
    public class SimReplacementReasonGetResp : WCFResponse
    {
        [DataMember]
        public List<SimReplacementReasons> ReplacementReasons { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SimReplacementReasonResp : WCFResponse
    {
        [DataMember]
        public SimReplacementReasons ReplacementReasonsDetails { get; set; }
    }
    #endregion

    [DataContract]
    [Serializable]
    public class MOCOfferDetailsRes
    {
        [DataMember]
        public Int32 ID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string OfferDescription { get; set; }
        [DataMember]
        public double DiscountAmount { get; set; }


    }

    //Pavan
        [DataContract]
    [Serializable]
 
    public class DiscountPricesRes
    {
        [DataMember]
        
        public int ID { get; set; }

        [DataMember]
        public int modelId { get; set; }

        [DataMember]
       
        public int PlanID
        {
            get;
            set;
        }

        [DataMember]
        
        public string NewPlanName
        {
            get;
            set;

        }

        [DataMember]
       
        public int DataPlanID
        {
            get;
            set;
        }

        [DataMember]
       
        public string OrgPrice
        {
            get;
            set;
        }
        [DataMember]

        public string DiscountPrice
        {
            get;
            set;
        }
        [DataMember]

        public string ContractID
        {
            get;
            set;
        }
        
    }

 

    //Pavan
    [DataContract]
    [Serializable]
    public class lnkofferuomarticlepriceResp : WCFResponse
    {
        [DataMember]
        public List<lnkofferuomarticleprice> values { get; set; }
    }

    //Added by VLT on 17 May 2013 to get device image details dynamically

    [DataContract]
    [Serializable]
    public class BrandArticleGetResp : WCFResponse
    {
        [DataMember]
        public BrandArticle brandArticle { get; set; }
    }
    //Added by VLT on 17 May 2013 to get device image details dynamically


    //Added by VLT on 17 May 2013 to get device image details dynamically

    [DataContract]
    [Serializable]
    public class GetDeviceImageDetailsResp : WCFResponse
    {
        [DataMember]
        public DeviceDetails deviceDetails { get; set; }
    }
    [DataContract]
    [Serializable]
    public class GetDeviceImageDetailsRespSmart : WCFResponse
    {
        [DataMember]
        public DeviceDetailsSmart deviceDetails { get; set; }
    }
    //Added by VLT on 17 May 2013 to get device image details dynamically


    #region "Add Remove VAS"
    [DataContract]
    [Serializable]
    public class AddRemoveVASReq
    {
        [DataMember]
        public DAL.Models.AddRemoveVASComponents AddRemoveVASComponents { get; set; }
    }

    [DataContract]
    [Serializable]
    public class AddRemoveVASResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.AddRemoveVASComponents> AddRemoveVASComponents { get; set; }
    }
#endregion


    [DataContract]
    [Serializable]
    public class RegSmartComponentsResp : WCFResponse
    {
        [DataMember]
        public List<RegSmartComponents> regsmartComponents { get; set; }        

    }

    [DataContract]
    [Serializable]
    public class ExtendPlanFindReq
    {
        [DataMember]
        public ContractPlans ContractPlan { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ExtendPlanFindResp : WCFResponse
    {
        [DataMember]
        public List<string> IDs { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ExtendBrandFindReq
    {
        [DataMember]
        public ContractBundleModels ContractBundleModels { get; set; }
    }

    [DataContract]
    [Serializable]
    public class ExtendBrandFindResp : WCFResponse
    {
        [DataMember]
        public List<string> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class CRPIMPOSDetailsResp
    {
        [DataMember]
        public Int32 RegID { get; set; }
        [DataMember]
        public string ArticleID { get; set; }
        [DataMember]
        public string MSISDN1 { get; set; }
        [DataMember]
        public string KenanAccountNo { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string IDCardNo { get; set; }
        [DataMember]
        public string Line1 { get; set; }
        [DataMember]
        public string Line2 { get; set; }
        [DataMember]
        public string Line3 { get; set; }
        [DataMember]

        public string Town { get; set; }
        [DataMember]
        public string Postcode { get; set; }
        [DataMember]
        public string IMEINumber { get; set; }
        [DataMember]
        public int ModelID { get; set; }
        [DataMember]
        public string UOMCODE { get; set; }
        [DataMember]
        public string SimNo { get; set; }

    }

    
    [DataContract]
    [Serializable]
    public class ContractDataPlanResp : WCFResponse
    {
        [DataMember]
        public List<DAL.Models.ContractDataplan> ContractDataPlans1 { get; set; }
    }

    [DataContract]
    [Serializable]
    public class RestrictedComponentsResp : WCFResponse
    {
        [DataMember]
        public List<RestrictedComponent> RestrictedComponents { get; set; }
    }

    #region smart related methods

    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentGetReqSmart
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }
    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentFindReqSmart
    {
        [DataMember]
        public PgmBdlPckComponentFindSmart PgmBdlPckComponentFindSmart { get; set; }
    }
    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentGetRespSmart : WCFResponse
    {
        [DataMember]
        public List<PgmBdlPckComponentSmart> PgmBdlPckComponents { get; set; }
    }
    [DataContract]
    [Serializable]
    public class PgmBdlPckComponentFindRespSmart : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class SmartRebateRes : WCFResponse
    {
        [DataMember]
        public string ID { get; set; }
    }
    [DataContract]
    [Serializable]
    public class SmartRebateReq
    {
        [DataMember]
        public SmartRebates SmartRebates { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ModelGroupModelGetResp_Smart : WCFResponse
    {
        [DataMember]
        public List<ModelGroupModel_Smart> ModelGroupModels_Smart { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ModelGroupModelFindResp_Smart : WCFResponse
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ModelGroupModelGetReq_Smart
    {
        [DataMember]
        public List<int> IDs { get; set; }
    }
    [DataContract]
    [Serializable]
    public class ModelGroupModelFindReq_Smart
    {
        [DataMember]
        public ModelGroupModelFind_Smart ModelGroupModelFind_Smart { get; set; }
    }
       
    #endregion

    [DataContract]
    [Serializable]
    public class VoiceContractDetailsResp : WCFResponse
    {
        [DataMember]
        public List<VoiceContractDetails> voiceContractDetails { get; set; }
    }

    public class VoiceContractDetailsReqStr
    {
        [DataMember]
        public List<string> IDs { get; set; }
    }

    [DataContract]
    [Serializable]
    public class VoiceContractDetailsGetResp : WCFResponse
    {
        [DataMember]
        public List<VoiceContractDetails> lstVoiceContractDetails { get; set; }
    }

    #region SimTypes
    [DataContract]
    [Serializable]
    public class SimCardTypesGetResp : WCFResponse
    {
        [DataMember]
        public List<SIMCardTypes> SIMCardTypes { get; set; }
    }
    #endregion

    #region DeviceTypes
    [DataContract]
    [Serializable]
    public class DeviceTypesGetResp : WCFResponse
    {
        [DataMember]
        public List<DeviceTypes> DeviceTypes { get; set; }
    }
    #endregion

    #region DeviceCapacity
    [DataContract]
    [Serializable]
    public class DeviceCapacityGetResp : WCFResponse
    {
        [DataMember]
        public List<DeviceCapacity> DeviceCapacity { get; set; }
    }
    #endregion

    #region PriceRange
    [DataContract]
    [Serializable]
    public class PriceRangeGetResp : WCFResponse
    {
        [DataMember]
        public List<PriceRange> PriceRange { get; set; }
    }
    #endregion

	#region Plan Device VAS Validation
	[DataContract]
	[Serializable]
	public class PlanDeviceValidationRulesGetReq
	{
		[DataMember]
		public DAL.Models.PlanDeviceValidationRules PlanDeviceValidationRules { get; set; }
	}

	[DataContract]
	[Serializable]
	public class PlanDeviceValidationRulesGetResp : WCFResponse
	{
		[DataMember]
		public List<PlanDeviceValidationRules> PlanDeviceValidationRulesGet { get; set; }
	}
	#endregion

}
